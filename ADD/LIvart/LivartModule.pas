unit LivartModule;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sButton, Mask, sMaskEdit, sEdit,
  sComboBox, Grids, DBGrids, acDBGrid, ComCtrls, sPageControl,
  sSkinProvider, DB, ADODB, StrUtils, DateUtils, sCheckBox,Clipbrd;

type
  TLivartModule_frm = class(TForm)
    sPanel1: TsPanel;
    sButton2: TsButton;
    sButton13: TsButton;
    Shape1: TShape;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel3: TsPanel;
    sButton3: TsButton;
    sPanel2: TsPanel;
    sDBGrid1: TsDBGrid;
    sTabSheet2: TsTabSheet;
    sPanel4: TsPanel;
    sButton6: TsButton;
    sPanel5: TsPanel;
    sComboBox1: TsComboBox;
    edtINF700: TsEdit;
    sButton8: TsButton;
    mskINF700: TsMaskEdit;
    sDBGrid2: TsDBGrid;
    sTabSheet3: TsTabSheet;
    sPanel7: TsPanel;
    sPanel8: TsPanel;
    sDBGrid3: TsDBGrid;
    sTabSheet4: TsTabSheet;
    sPanel9: TsPanel;
    sButton12: TsButton;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    sComboBox2: TsComboBox;
    edtINF707: TsEdit;
    sButton14: TsButton;
    mskINF707: TsMaskEdit;
    sDBGrid4: TsDBGrid;
    sTabSheet5: TsTabSheet;
    sPanel12: TsPanel;
    edtDOANTC_DATE: TsEdit;
    sButton15: TsButton;
    sPanel13: TsPanel;
    sPanel14: TsPanel;
    sComboBox3: TsComboBox;
    sButton17: TsButton;
    mskDOANTC_DATE: TsMaskEdit;
    sDBGrid5: TsDBGrid;
    sButton4: TsButton;
    sSkinProvider1: TsSkinProvider;
    Shape2: TShape;
    qryAPP700: TADOQuery;
    qryAPP700CO_GBCD: TStringField;
    qryAPP700IMPTNO: TStringField;
    qryAPP700IMPTSQ: TIntegerField;
    qryAPP700USER_ID: TStringField;
    qryAPP700MESSAGE1: TStringField;
    qryAPP700MESSAGE2: TStringField;
    qryAPP700APP_DATE: TStringField;
    qryAPP700CHK2: TStringField;
    qryAPP700CHK3: TStringField;
    qryAPP700DATEE: TStringField;
    qryAPP700IN_MATHOD: TStringField;
    qryAPP700AP_BANK: TStringField;
    qryAPP700AP_BANK1: TStringField;
    qryAPP700AP_BANK2: TStringField;
    qryAPP700AP_BANK3: TStringField;
    qryAPP700AP_BANK4: TStringField;
    qryAPP700AP_BANK5: TStringField;
    qryAPP700AD_BANK: TStringField;
    qryAPP700AD_BANK1: TStringField;
    qryAPP700AD_BANK2: TStringField;
    qryAPP700AD_BANK3: TStringField;
    qryAPP700AD_BANK4: TStringField;
    qryAPP700AD_PAY: TStringField;
    qryAPP700IMP_CD1: TStringField;
    qryAPP700IMP_CD2: TStringField;
    qryAPP700IMP_CD3: TStringField;
    qryAPP700IMP_CD4: TStringField;
    qryAPP700IMP_CD5: TStringField;
    qryAPP700IL_NO1: TStringField;
    qryAPP700IL_NO2: TStringField;
    qryAPP700IL_NO3: TStringField;
    qryAPP700IL_NO4: TStringField;
    qryAPP700IL_NO5: TStringField;
    qryAPP700IL_CUR1: TStringField;
    qryAPP700IL_CUR2: TStringField;
    qryAPP700IL_CUR3: TStringField;
    qryAPP700IL_CUR4: TStringField;
    qryAPP700IL_CUR5: TStringField;
    qryAPP700IL_AMT1: TBCDField;
    qryAPP700IL_AMT2: TBCDField;
    qryAPP700IL_AMT3: TBCDField;
    qryAPP700IL_AMT4: TBCDField;
    qryAPP700IL_AMT5: TBCDField;
    qryAPP700AD_INFO1: TStringField;
    qryAPP700AD_INFO2: TStringField;
    qryAPP700AD_INFO3: TStringField;
    qryAPP700AD_INFO4: TStringField;
    qryAPP700AD_INFO5: TStringField;
    qryAPP700EX_NAME1: TStringField;
    qryAPP700EX_NAME2: TStringField;
    qryAPP700EX_NAME3: TStringField;
    qryAPP700EX_ADDR1: TStringField;
    qryAPP700EX_ADDR2: TStringField;
    qryAPP700DOC_CD: TStringField;
    qryAPP700EX_DATE: TStringField;
    qryAPP700EX_PLACE: TStringField;
    qryAPP700APPLIC1: TStringField;
    qryAPP700APPLIC2: TStringField;
    qryAPP700APPLIC3: TStringField;
    qryAPP700APPLIC4: TStringField;
    qryAPP700APPLIC5: TStringField;
    qryAPP700BENEFC: TStringField;
    qryAPP700BENEFC1: TStringField;
    qryAPP700BENEFC2: TStringField;
    qryAPP700BENEFC3: TStringField;
    qryAPP700BENEFC4: TStringField;
    qryAPP700BENEFC5: TStringField;
    qryAPP700CD_AMT: TBCDField;
    qryAPP700CD_CUR: TStringField;
    qryAPP700CD_PERP: TIntegerField;
    qryAPP700CD_PERM: TIntegerField;
    qryAPP700CD_MAX: TStringField;
    qryAPP700TERM_PR: TStringField;
    qryAPP700TERM_PR_M: TStringField;
    qryAPP700PL_TERM: TStringField;
    qryAPP700AA_CV1: TStringField;
    qryAPP700AA_CV2: TStringField;
    qryAPP700AA_CV3: TStringField;
    qryAPP700AA_CV4: TStringField;
    qryAPP700DRAFT1: TStringField;
    qryAPP700DRAFT2: TStringField;
    qryAPP700DRAFT3: TStringField;
    qryAPP700MIX_PAY1: TStringField;
    qryAPP700MIX_PAY2: TStringField;
    qryAPP700MIX_PAY3: TStringField;
    qryAPP700MIX_PAY4: TStringField;
    qryAPP700DEF_PAY1: TStringField;
    qryAPP700DEF_PAY2: TStringField;
    qryAPP700DEF_PAY3: TStringField;
    qryAPP700DEF_PAY4: TStringField;
    qryAPP700DESGOOD: TStringField;
    qryAPP700DESGOOD_1: TMemoField;
    qryAPP700LST_DATE: TStringField;
    qryAPP700SHIP_PD1: TStringField;
    qryAPP700SHIP_PD2: TStringField;
    qryAPP700SHIP_PD3: TStringField;
    qryAPP700SHIP_PD4: TStringField;
    qryAPP700SHIP_PD5: TStringField;
    qryAPP700SHIP_PD6: TStringField;
    qryAPP700PSHIP: TStringField;
    qryAPP700TSHIP: TStringField;
    qryAPP700CARRIAGE: TStringField;
    qryAPP700SUNJUK_PORT: TStringField;
    qryAPP700DOCHAK_PORT: TStringField;
    qryAPP700LOAD_ON: TStringField;
    qryAPP700FOR_TRAN: TStringField;
    qryAPP700ORIGIN: TStringField;
    qryAPP700ORIGIN_M: TStringField;
    qryAPP700DOC_380: TStringField;
    qryAPP700DOC_380_1: TIntegerField;
    qryAPP700DOC_705_YN: TStringField;
    qryAPP700DOC_705_GUBUN: TStringField;
    qryAPP700DOC_705_1: TStringField;
    qryAPP700DOC_705_2: TStringField;
    qryAPP700DOC_705_3: TStringField;
    qryAPP700DOC_705_4: TStringField;
    qryAPP700DOC_740: TStringField;
    qryAPP700DOC_740_1: TStringField;
    qryAPP700DOC_740_2: TStringField;
    qryAPP700DOC_740_3: TStringField;
    qryAPP700DOC_740_4: TStringField;
    qryAPP700DOC_760: TStringField;
    qryAPP700DOC_760_1: TStringField;
    qryAPP700DOC_760_2: TStringField;
    qryAPP700DOC_760_3: TStringField;
    qryAPP700DOC_760_4: TStringField;
    qryAPP700DOC_530: TStringField;
    qryAPP700DOC_530_1: TStringField;
    qryAPP700DOC_530_2: TStringField;
    qryAPP700DOC_271: TStringField;
    qryAPP700DOC_271_1: TIntegerField;
    qryAPP700DOC_861: TStringField;
    qryAPP700DOC_2AA: TStringField;
    qryAPP700DOC_2AA_1: TMemoField;
    qryAPP700ACD_2AA: TStringField;
    qryAPP700ACD_2AA_1: TStringField;
    qryAPP700ACD_2AB: TStringField;
    qryAPP700ACD_2AC: TStringField;
    qryAPP700ACD_2AD: TStringField;
    qryAPP700ACD_2AE: TStringField;
    qryAPP700ACD_2AE_1: TMemoField;
    qryAPP700CHARGE: TStringField;
    qryAPP700PERIOD: TIntegerField;
    qryAPP700CONFIRMM: TStringField;
    dsAPP700: TDataSource;
    qryAPP707: TADOQuery;
    qryAPP707CO_GBCD: TStringField;
    qryAPP707IMPTNO: TStringField;
    qryAPP707IMPTSQ: TIntegerField;
    qryAPP707AMNDSQ: TIntegerField;
    qryAPP707MESSAGE1: TStringField;
    qryAPP707MESSAGE2: TStringField;
    qryAPP707CHK2: TStringField;
    qryAPP707CHK3: TStringField;
    qryAPP707DATEE: TStringField;
    qryAPP707APP_DATE: TStringField;
    qryAPP707IN_MATHOD: TStringField;
    qryAPP707AP_BANK: TStringField;
    qryAPP707AP_BANK1: TStringField;
    qryAPP707AP_BANK2: TStringField;
    qryAPP707AP_BANK3: TStringField;
    qryAPP707AP_BANK4: TStringField;
    qryAPP707AP_BANK5: TStringField;
    qryAPP707AD_BANK: TStringField;
    qryAPP707AD_BANK1: TStringField;
    qryAPP707AD_BANK2: TStringField;
    qryAPP707AD_BANK3: TStringField;
    qryAPP707AD_BANK4: TStringField;
    qryAPP707AD_PAY: TStringField;
    qryAPP707IMP_CD1: TStringField;
    qryAPP707IMP_CD2: TStringField;
    qryAPP707IMP_CD3: TStringField;
    qryAPP707IMP_CD4: TStringField;
    qryAPP707IMP_CD5: TStringField;
    qryAPP707IL_NO1: TStringField;
    qryAPP707IL_NO2: TStringField;
    qryAPP707IL_NO3: TStringField;
    qryAPP707IL_NO4: TStringField;
    qryAPP707IL_NO5: TStringField;
    qryAPP707IL_AMT1: TBCDField;
    qryAPP707IL_AMT2: TBCDField;
    qryAPP707IL_AMT3: TBCDField;
    qryAPP707IL_AMT4: TBCDField;
    qryAPP707IL_AMT5: TBCDField;
    qryAPP707IL_CUR1: TStringField;
    qryAPP707IL_CUR2: TStringField;
    qryAPP707IL_CUR3: TStringField;
    qryAPP707IL_CUR4: TStringField;
    qryAPP707IL_CUR5: TStringField;
    qryAPP707EX_NAME1: TStringField;
    qryAPP707EX_NAME2: TStringField;
    qryAPP707EX_NAME3: TStringField;
    qryAPP707AD_INFO1: TStringField;
    qryAPP707AD_INFO2: TStringField;
    qryAPP707AD_INFO3: TStringField;
    qryAPP707AD_INFO4: TStringField;
    qryAPP707AD_INFO5: TStringField;
    qryAPP707CD_NO: TStringField;
    qryAPP707ISS_DATE: TStringField;
    qryAPP707APPLIC1: TStringField;
    qryAPP707APPLIC2: TStringField;
    qryAPP707APPLIC3: TStringField;
    qryAPP707APPLIC4: TStringField;
    qryAPP707APPLIC5: TStringField;
    qryAPP707BENEFC: TStringField;
    qryAPP707BENEFC1: TStringField;
    qryAPP707BENEFC2: TStringField;
    qryAPP707BENEFC3: TStringField;
    qryAPP707BENEFC4: TStringField;
    qryAPP707BENEFC5: TStringField;
    qryAPP707EX_DATE: TStringField;
    qryAPP707INCD_CUR: TStringField;
    qryAPP707INCD_AMT: TBCDField;
    qryAPP707DECD_CUR: TStringField;
    qryAPP707DECD_AMT: TBCDField;
    qryAPP707NWCD_CUR: TStringField;
    qryAPP707NWCD_AMT: TBCDField;
    qryAPP707BFCD_CUR: TStringField;
    qryAPP707BFCD_AMT: TBCDField;
    qryAPP707CD_PERP: TIntegerField;
    qryAPP707CD_PERM: TIntegerField;
    qryAPP707CD_MAX: TStringField;
    qryAPP707AA_CV1: TStringField;
    qryAPP707AA_CV2: TStringField;
    qryAPP707AA_CV3: TStringField;
    qryAPP707AA_CV4: TStringField;
    qryAPP707SUNJUCK_PORT: TStringField;
    qryAPP707DOCHAK_PORT: TStringField;
    qryAPP707LOAD_ON: TStringField;
    qryAPP707FOR_TRAN: TStringField;
    qryAPP707LST_DATE: TStringField;
    qryAPP707SHIP_PD1: TStringField;
    qryAPP707SHIP_PD2: TStringField;
    qryAPP707SHIP_PD3: TStringField;
    qryAPP707SHIP_PD4: TStringField;
    qryAPP707SHIP_PD5: TStringField;
    qryAPP707SHIP_PD6: TStringField;
    qryAPP707CARRIAGE: TStringField;
    qryAPP707NARRAT: TStringField;
    qryAPP707NARRAT_1: TMemoField;
    qryAPP707EX_ADDR1: TStringField;
    qryAPP707EX_ADDR2: TStringField;
    dsAPP707: TDataSource;
    qryDOANTC: TADOQuery;
    qryDOANTCMAINT_NO: TStringField;
    qryDOANTCUSER_ID: TStringField;
    qryDOANTCDATEE: TStringField;
    qryDOANTCMESSAGE1: TStringField;
    qryDOANTCMESSAGE2: TStringField;
    qryDOANTCAPP_CODE: TStringField;
    qryDOANTCAPP_NAME1: TStringField;
    qryDOANTCAPP_NAME2: TStringField;
    qryDOANTCAPP_NAME3: TStringField;
    qryDOANTCBANK_CODE: TStringField;
    qryDOANTCBANK1: TStringField;
    qryDOANTCBANK2: TStringField;
    qryDOANTCLC_G: TStringField;
    qryDOANTCLC_NO: TStringField;
    qryDOANTCBL_G: TStringField;
    qryDOANTCBL_NO: TStringField;
    qryDOANTCAMT: TBCDField;
    qryDOANTCAMTC: TStringField;
    qryDOANTCCHRG: TBCDField;
    qryDOANTCCHRGC: TStringField;
    qryDOANTCRES_DATE: TStringField;
    qryDOANTCSET_DATE: TStringField;
    qryDOANTCREMARK1: TMemoField;
    qryDOANTCBK_NAME1: TStringField;
    qryDOANTCBK_NAME2: TStringField;
    qryDOANTCBK_NAME3: TStringField;
    qryDOANTCCHK1: TBooleanField;
    qryDOANTCCHK2: TStringField;
    qryDOANTCCHK3: TStringField;
    qryDOANTCPRNO: TIntegerField;
    dsDOANTC: TDataSource;
    qryINF700: TADOQuery;
    qryINF700MAINT_NO: TStringField;
    qryINF700MESSAGE1: TStringField;
    qryINF700MESSAGE2: TStringField;
    qryINF700USER_ID: TStringField;
    qryINF700DATEE: TStringField;
    qryINF700APP_DATE: TStringField;
    qryINF700IN_MATHOD: TStringField;
    qryINF700AP_BANK: TStringField;
    qryINF700AP_BANK1: TStringField;
    qryINF700AP_BANK2: TStringField;
    qryINF700AP_BANK3: TStringField;
    qryINF700AP_BANK4: TStringField;
    qryINF700AP_BANK5: TStringField;
    qryINF700AD_BANK: TStringField;
    qryINF700AD_BANK1: TStringField;
    qryINF700AD_BANK2: TStringField;
    qryINF700AD_BANK3: TStringField;
    qryINF700AD_BANK4: TStringField;
    qryINF700AD_PAY: TStringField;
    qryINF700IL_NO1: TStringField;
    qryINF700IL_NO2: TStringField;
    qryINF700IL_NO3: TStringField;
    qryINF700IL_NO4: TStringField;
    qryINF700IL_NO5: TStringField;
    qryINF700IL_AMT1: TBCDField;
    qryINF700IL_AMT2: TBCDField;
    qryINF700IL_AMT3: TBCDField;
    qryINF700IL_AMT4: TBCDField;
    qryINF700IL_AMT5: TBCDField;
    qryINF700IL_CUR1: TStringField;
    qryINF700IL_CUR2: TStringField;
    qryINF700IL_CUR3: TStringField;
    qryINF700IL_CUR4: TStringField;
    qryINF700IL_CUR5: TStringField;
    qryINF700AD_INFO1: TStringField;
    qryINF700AD_INFO2: TStringField;
    qryINF700AD_INFO3: TStringField;
    qryINF700AD_INFO4: TStringField;
    qryINF700AD_INFO5: TStringField;
    qryINF700DOC_CD: TStringField;
    qryINF700CD_NO: TStringField;
    qryINF700REF_PRE: TStringField;
    qryINF700ISS_DATE: TStringField;
    qryINF700EX_DATE: TStringField;
    qryINF700EX_PLACE: TStringField;
    qryINF700CHK1: TStringField;
    qryINF700CHK2: TStringField;
    qryINF700CHK3: TStringField;
    qryINF700prno: TIntegerField;
    qryINF700F_INTERFACE: TStringField;
    qryINF700IMP_CD1: TStringField;
    qryINF700IMP_CD2: TStringField;
    qryINF700IMP_CD3: TStringField;
    qryINF700IMP_CD4: TStringField;
    qryINF700IMP_CD5: TStringField;
    qryINF700MAINT_NO_1: TStringField;
    qryINF700APP_BANK: TStringField;
    qryINF700APP_BANK1: TStringField;
    qryINF700APP_BANK2: TStringField;
    qryINF700APP_BANK3: TStringField;
    qryINF700APP_BANK4: TStringField;
    qryINF700APP_BANK5: TStringField;
    qryINF700APP_ACCNT: TStringField;
    qryINF700APPLIC1: TStringField;
    qryINF700APPLIC2: TStringField;
    qryINF700APPLIC3: TStringField;
    qryINF700APPLIC4: TStringField;
    qryINF700APPLIC5: TStringField;
    qryINF700BENEFC1: TStringField;
    qryINF700BENEFC2: TStringField;
    qryINF700BENEFC3: TStringField;
    qryINF700BENEFC4: TStringField;
    qryINF700BENEFC5: TStringField;
    qryINF700CD_AMT: TBCDField;
    qryINF700CD_CUR: TStringField;
    qryINF700CD_PERP: TBCDField;
    qryINF700CD_PERM: TBCDField;
    qryINF700CD_MAX: TStringField;
    qryINF700AA_CV1: TStringField;
    qryINF700AA_CV2: TStringField;
    qryINF700AA_CV3: TStringField;
    qryINF700AA_CV4: TStringField;
    qryINF700AVAIL: TStringField;
    qryINF700AVAIL1: TStringField;
    qryINF700AVAIL2: TStringField;
    qryINF700AVAIL3: TStringField;
    qryINF700AVAIL4: TStringField;
    qryINF700AV_ACCNT: TStringField;
    qryINF700AV_PAY: TStringField;
    qryINF700DRAFT1: TStringField;
    qryINF700DRAFT2: TStringField;
    qryINF700DRAFT3: TStringField;
    qryINF700DRAWEE: TStringField;
    qryINF700DRAWEE1: TStringField;
    qryINF700DRAWEE2: TStringField;
    qryINF700DRAWEE3: TStringField;
    qryINF700DRAWEE4: TStringField;
    qryINF700DR_ACCNT: TStringField;
    qryINF700MAINT_NO_2: TStringField;
    qryINF700PSHIP: TStringField;
    qryINF700TSHIP: TStringField;
    qryINF700LOAD_ON: TStringField;
    qryINF700FOR_TRAN: TStringField;
    qryINF700LST_DATE: TStringField;
    qryINF700SHIP_PD: TBooleanField;
    qryINF700SHIP_PD1: TStringField;
    qryINF700SHIP_PD2: TStringField;
    qryINF700SHIP_PD3: TStringField;
    qryINF700SHIP_PD4: TStringField;
    qryINF700SHIP_PD5: TStringField;
    qryINF700SHIP_PD6: TStringField;
    qryINF700DESGOOD: TBooleanField;
    qryINF700DESGOOD_1: TMemoField;
    qryINF700TERM_PR: TStringField;
    qryINF700TERM_PR_M: TStringField;
    qryINF700PL_TERM: TStringField;
    qryINF700ORIGIN: TStringField;
    qryINF700ORIGIN_M: TStringField;
    qryINF700DOC_380: TBooleanField;
    qryINF700DOC_380_1: TBCDField;
    qryINF700DOC_705: TBooleanField;
    qryINF700DOC_705_1: TStringField;
    qryINF700DOC_705_2: TStringField;
    qryINF700DOC_705_3: TStringField;
    qryINF700DOC_705_4: TStringField;
    qryINF700DOC_740: TBooleanField;
    qryINF700DOC_740_1: TStringField;
    qryINF700DOC_740_2: TStringField;
    qryINF700DOC_740_3: TStringField;
    qryINF700DOC_740_4: TStringField;
    qryINF700DOC_530: TBooleanField;
    qryINF700DOC_530_1: TStringField;
    qryINF700DOC_530_2: TStringField;
    qryINF700DOC_271: TBooleanField;
    qryINF700DOC_271_1: TBCDField;
    qryINF700DOC_861: TBooleanField;
    qryINF700DOC_2AA: TBooleanField;
    qryINF700DOC_2AA_1: TMemoField;
    qryINF700ACD_2AA: TBooleanField;
    qryINF700ACD_2AA_1: TStringField;
    qryINF700ACD_2AB: TBooleanField;
    qryINF700ACD_2AC: TBooleanField;
    qryINF700ACD_2AD: TBooleanField;
    qryINF700ACD_2AE: TBooleanField;
    qryINF700ACD_2AE_1: TMemoField;
    qryINF700CHARGE: TStringField;
    qryINF700PERIOD: TBCDField;
    qryINF700CONFIRMM: TStringField;
    qryINF700DEF_PAY1: TStringField;
    qryINF700DEF_PAY2: TStringField;
    qryINF700DEF_PAY3: TStringField;
    qryINF700DEF_PAY4: TStringField;
    qryINF700DOC_705_GUBUN: TStringField;
    qryINF700MAINT_NO_3: TStringField;
    qryINF700REI_BANK: TStringField;
    qryINF700REI_BANK1: TStringField;
    qryINF700REI_BANK2: TStringField;
    qryINF700REI_BANK3: TStringField;
    qryINF700REI_BANK4: TStringField;
    qryINF700REI_BANK5: TStringField;
    qryINF700REI_ACNNT: TStringField;
    qryINF700INSTRCT: TBooleanField;
    qryINF700INSTRCT_1: TMemoField;
    qryINF700AVT_BANK: TStringField;
    qryINF700AVT_BANK1: TStringField;
    qryINF700AVT_BANK2: TStringField;
    qryINF700AVT_BANK3: TStringField;
    qryINF700AVT_BANK4: TStringField;
    qryINF700AVT_BANK5: TStringField;
    qryINF700AVT_ACCNT: TStringField;
    qryINF700SND_INFO1: TStringField;
    qryINF700SND_INFO2: TStringField;
    qryINF700SND_INFO3: TStringField;
    qryINF700SND_INFO4: TStringField;
    qryINF700SND_INFO5: TStringField;
    qryINF700SND_INFO6: TStringField;
    qryINF700EX_NAME1: TStringField;
    qryINF700EX_NAME2: TStringField;
    qryINF700EX_NAME3: TStringField;
    qryINF700EX_ADDR1: TStringField;
    qryINF700EX_ADDR2: TStringField;
    qryINF700EX_ADDR3: TStringField;
    qryINF700OP_BANK1: TStringField;
    qryINF700OP_BANK2: TStringField;
    qryINF700OP_BANK3: TStringField;
    qryINF700OP_ADDR1: TStringField;
    qryINF700OP_ADDR2: TStringField;
    qryINF700OP_ADDR3: TStringField;
    qryINF700MIX_PAY1: TStringField;
    qryINF700MIX_PAY2: TStringField;
    qryINF700MIX_PAY3: TStringField;
    qryINF700MIX_PAY4: TStringField;
    qryINF700APPLICABLE_RULES_1: TStringField;
    qryINF700APPLICABLE_RULES_2: TStringField;
    qryINF700DOC_760: TBooleanField;
    qryINF700DOC_760_1: TStringField;
    qryINF700DOC_760_2: TStringField;
    qryINF700DOC_760_3: TStringField;
    qryINF700DOC_760_4: TStringField;
    qryINF700SUNJUCK_PORT: TStringField;
    qryINF700DOCHACK_PORT: TStringField;
    dsINF700: TDataSource;
    qryINF707: TADOQuery;
    qryINF707MAINT_NO: TStringField;
    qryINF707MSEQ: TIntegerField;
    qryINF707AMD_NO: TIntegerField;
    qryINF707MESSAGE1: TStringField;
    qryINF707MESSAGE2: TStringField;
    qryINF707USER_ID: TStringField;
    qryINF707DATEE: TStringField;
    qryINF707APP_DATE: TStringField;
    qryINF707IN_MATHOD: TStringField;
    qryINF707AP_BANK: TStringField;
    qryINF707AP_BANK1: TStringField;
    qryINF707AP_BANK2: TStringField;
    qryINF707AP_BANK3: TStringField;
    qryINF707AP_BANK4: TStringField;
    qryINF707AP_BANK5: TStringField;
    qryINF707AD_BANK: TStringField;
    qryINF707AD_BANK1: TStringField;
    qryINF707AD_BANK2: TStringField;
    qryINF707AD_BANK3: TStringField;
    qryINF707AD_BANK4: TStringField;
    qryINF707IL_NO1: TStringField;
    qryINF707IL_NO2: TStringField;
    qryINF707IL_NO3: TStringField;
    qryINF707IL_NO4: TStringField;
    qryINF707IL_NO5: TStringField;
    qryINF707IL_AMT1: TBCDField;
    qryINF707IL_AMT2: TBCDField;
    qryINF707IL_AMT3: TBCDField;
    qryINF707IL_AMT4: TBCDField;
    qryINF707IL_AMT5: TBCDField;
    qryINF707IL_CUR1: TStringField;
    qryINF707IL_CUR2: TStringField;
    qryINF707IL_CUR3: TStringField;
    qryINF707IL_CUR4: TStringField;
    qryINF707IL_CUR5: TStringField;
    qryINF707AD_INFO1: TStringField;
    qryINF707AD_INFO2: TStringField;
    qryINF707AD_INFO3: TStringField;
    qryINF707AD_INFO4: TStringField;
    qryINF707AD_INFO5: TStringField;
    qryINF707CD_NO: TStringField;
    qryINF707RCV_REF: TStringField;
    qryINF707IBANK_REF: TStringField;
    qryINF707ISS_BANK1: TStringField;
    qryINF707ISS_BANK2: TStringField;
    qryINF707ISS_BANK3: TStringField;
    qryINF707ISS_BANK4: TStringField;
    qryINF707ISS_BANK5: TStringField;
    qryINF707ISS_ACCNT: TStringField;
    qryINF707ISS_DATE: TStringField;
    qryINF707AMD_DATE: TStringField;
    qryINF707EX_DATE: TStringField;
    qryINF707EX_PLACE: TStringField;
    qryINF707CHK1: TStringField;
    qryINF707CHK2: TStringField;
    qryINF707CHK3: TStringField;
    qryINF707F_INTERFACE: TStringField;
    qryINF707IMP_CD1: TStringField;
    qryINF707IMP_CD2: TStringField;
    qryINF707IMP_CD3: TStringField;
    qryINF707IMP_CD4: TStringField;
    qryINF707IMP_CD5: TStringField;
    qryINF707Prno: TIntegerField;
    qryINF707APPLIC1: TStringField;
    qryINF707APPLIC2: TStringField;
    qryINF707APPLIC3: TStringField;
    qryINF707APPLIC4: TStringField;
    qryINF707APPLIC5: TStringField;
    qryINF707BENEFC1: TStringField;
    qryINF707BENEFC2: TStringField;
    qryINF707BENEFC3: TStringField;
    qryINF707BENEFC4: TStringField;
    qryINF707BENEFC5: TStringField;
    qryINF707INCD_CUR: TStringField;
    qryINF707INCD_AMT: TBCDField;
    qryINF707DECD_CUR: TStringField;
    qryINF707DECD_AMT: TBCDField;
    qryINF707NWCD_CUR: TStringField;
    qryINF707NWCD_AMT: TBCDField;
    qryINF707CD_PERP: TBCDField;
    qryINF707CD_PERM: TBCDField;
    qryINF707CD_MAX: TStringField;
    qryINF707AA_CV1: TStringField;
    qryINF707AA_CV2: TStringField;
    qryINF707AA_CV3: TStringField;
    qryINF707AA_CV4: TStringField;
    qryINF707LOAD_ON: TStringField;
    qryINF707FOR_TRAN: TStringField;
    qryINF707LST_DATE: TStringField;
    qryINF707SHIP_PD: TBooleanField;
    qryINF707SHIP_PD1: TStringField;
    qryINF707SHIP_PD2: TStringField;
    qryINF707SHIP_PD3: TStringField;
    qryINF707SHIP_PD4: TStringField;
    qryINF707SHIP_PD5: TStringField;
    qryINF707SHIP_PD6: TStringField;
    qryINF707NARRAT: TBooleanField;
    qryINF707NARRAT_1: TMemoField;
    qryINF707SR_INFO1: TStringField;
    qryINF707SR_INFO2: TStringField;
    qryINF707SR_INFO3: TStringField;
    qryINF707SR_INFO4: TStringField;
    qryINF707SR_INFO5: TStringField;
    qryINF707SR_INFO6: TStringField;
    qryINF707EX_NAME1: TStringField;
    qryINF707EX_NAME2: TStringField;
    qryINF707EX_NAME3: TStringField;
    qryINF707EX_ADDR1: TStringField;
    qryINF707EX_ADDR2: TStringField;
    qryINF707OP_BANK1: TStringField;
    qryINF707OP_BANK2: TStringField;
    qryINF707OP_BANK3: TStringField;
    qryINF707OP_ADDR1: TStringField;
    qryINF707OP_ADDR2: TStringField;
    qryINF707BFCD_AMT: TBCDField;
    qryINF707BFCD_CUR: TStringField;
    qryINF707SUNJUCK_PORT: TStringField;
    qryINF707DOCHACK_PORT: TStringField;
    dsINF707: TDataSource;
    qryTest: TADOQuery;
    ADOQuery1: TADOQuery;
    ADOQuery2: TADOQuery;
    DataSource2: TDataSource;
    DataSource1: TDataSource;
    dsTest: TDataSource;
    sPanel15: TsPanel;
    sMaskEdit1: TsMaskEdit;
    sButton1: TsButton;
    qryAPP700PERIOD_IDX: TStringField;
    qryAPP700PERIOD_TXT: TStringField;
    qryAPP700CONFIRM_BANKNM: TStringField;
    qryAPP700CONFIRM_BANKBR: TStringField;
    qryAPP700CHARGE_1: TStringField;
    qryAPP700SPECIAL_PAY: TStringField;
    qryAPP700FRST_REG_USER_ID: TStringField;
    qryAPP700FRST_REG_DTM: TDateTimeField;
    qryAPP700LAST_MOD_USER_ID: TStringField;
    qryAPP700LAST_MOD_DTM: TDateTimeField;
    chk_SvFDINFO: TsCheckBox;
    qryAPP707IS_CANCEL: TStringField;
    qryAPP707APPLIC_CHG1: TStringField;
    qryAPP707APPLIC_CHG2: TStringField;
    qryAPP707APPLIC_CHG3: TStringField;
    qryAPP707APPLIC_CHG4: TStringField;
    qryAPP707EXPLOC: TStringField;
    qryAPP707CONFIRM_BANKNM: TStringField;
    qryAPP707CONFIRM_BANKBR: TStringField;
    qryAPP707CHARGE: TStringField;
    qryAPP707CHARGE_1: TStringField;
    qryAPP707AMD_CHARGE: TStringField;
    qryAPP707AMD_CHARGE_1: TStringField;
    qryAPP707SPECIAL_PAY: TStringField;
    qryAPP707GOODS_DESC: TStringField;
    qryAPP707DOC_REQ: TStringField;
    qryAPP707ADD_CONDITION: TStringField;
    qryAPP707PERIOD_DAYS: TIntegerField;
    qryAPP707PERIOD_IDX: TStringField;
    qryAPP707PERIOD_TXT: TStringField;
    qryAPP707FRST_REG_USER_ID: TStringField;
    qryAPP707FRST_REG_DTM: TDateTimeField;
    qryAPP707LAST_MOD_USER_ID: TStringField;
    qryAPP707LAST_MOD_DTM: TDateTimeField;
    qryAPP707PSHIP: TStringField;
    qryAPP707TSHIP: TStringField;
    sPanel6: TsPanel;
    sMaskEdit2: TsMaskEdit;
    sButton7: TsButton;
    sButton10: TsButton;
    qryAPP707CONFIRMM: TStringField;
    qryINF700AVAIL_BIC: TStringField;
    qryINF700DRAWEE_BIC: TStringField;
    qryINF700CHARGE_1: TMemoField;
    qryINF700CONFIRM_BICCD: TStringField;
    qryINF700CONFIRM_BANKNM: TStringField;
    qryINF700CONFIRM_BANKBR: TStringField;
    qryINF700PERIOD_IDX: TIntegerField;
    qryINF700PERIOD_TXT: TStringField;
    qryINF700SPECIAL_PAY: TMemoField;
    qryINF707IS_CANCEL: TStringField;
    qryINF707DOC_CD: TStringField;
    qryINF707PSHIP: TStringField;
    qryINF707TSHIP: TStringField;
    qryINF707MAINT_NO_1: TStringField;
    qryINF707MSEQ_1: TIntegerField;
    qryINF707AMD_NO_1: TIntegerField;
    qryINF707CHARGE: TStringField;
    qryINF707CHARGE_1: TMemoField;
    qryINF707AMD_CHARGE: TStringField;
    qryINF707AMD_CHARGE_1: TMemoField;
    qryINF707SPECIAL_PAY: TMemoField;
    qryINF707GOODS_DESC: TMemoField;
    qryINF707DOC_REQ: TMemoField;
    qryINF707ADD_CONDITION: TMemoField;
    qryINF707DRAFT1: TStringField;
    qryINF707DRAFT2: TStringField;
    qryINF707DRAFT3: TStringField;
    qryINF707MIX1: TStringField;
    qryINF707MIX2: TStringField;
    qryINF707MIX3: TStringField;
    qryINF707MIX4: TStringField;
    qryINF707DEFPAY1: TStringField;
    qryINF707DEFPAY2: TStringField;
    qryINF707DEFPAY3: TStringField;
    qryINF707DEFPAY4: TStringField;
    qryINF707PERIOD_DAYS: TIntegerField;
    qryINF707PERIOD_IDX: TIntegerField;
    qryINF707PERIOD_DETAIL: TStringField;
    qryINF707CONFIRM: TStringField;
    qryINF707CONFIRM_BIC: TStringField;
    qryINF707CONFIRM1: TStringField;
    qryINF707CONFIRM2: TStringField;
    qryINF707CONFIRM3: TStringField;
    qryINF707CONFIRM4: TStringField;
    qryINF707TXT_78: TMemoField;
    qryINF707APPLIC_CHG1: TStringField;
    qryINF707APPLIC_CHG2: TStringField;
    qryINF707APPLIC_CHG3: TStringField;
    qryINF707APPLIC_CHG4: TStringField;
    qryAPP707MIX1: TStringField;
    qryAPP707MIX2: TStringField;
    qryAPP707MIX3: TStringField;
    qryAPP707MIX4: TStringField;
    qryAPP707DRAFT1: TStringField;
    qryAPP707DRAFT2: TStringField;
    qryAPP707DRAFT3: TStringField;
    qryAPP707DEFPAY1: TStringField;
    qryAPP707DEFPAY2: TStringField;
    qryAPP707DEFPAY3: TStringField;
    qryAPP707DEFPAY4: TStringField;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    procedure sButton2Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure sButton13Click(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
    procedure sMaskEdit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure APP700_getData;
    function CompareBANKCD(sERP_BANK_CD: String): String;
    procedure AddLog(obj, sData: String);
    function getCodeContent(sPrefix, sCode: String): String;
    procedure APP707_getData;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LivartModule_frm: TLivartModule_frm;

implementation

uses
  ICON, LivartConfig, KISCalendarV2, dlg_ERPKIS_BK_CONN, MSSQL, SQLCreator, VarDefine, TypeDefine, Commonlib;

{$R *.dfm}

procedure TLivartModule_frm.sButton2Click(Sender: TObject);
begin
    LivartConfig_frm.ShowModal;
end;

procedure TLivartModule_frm.sButton1Click(Sender: TObject);
begin
  KISCalendarV2_frm := TKISCalendarV2_frm.Create(Self);
  try
    Case sPageControl1.ActivePageIndex of
      0: sMaskEdit1.Text := FormatDateTime('YYYYMMDD',KISCalendarV2_frm.OpenCalendar(sMaskEdit1.Text));
      2: sMaskEdit2.Text := FormatDateTime('YYYYMMDD',KISCalendarV2_frm.OpenCalendar(sMaskEdit2.Text));
    end;
    sButton3Click(nil);
  finally
    FreeAndNil(KISCalendarV2_frm);
  end;
end;

procedure TLivartModule_frm.FormShow(Sender: TObject);
begin
  LivartConfig_frm := TLivartConfig_frm.Create(Self);
  LivartConfig_frm.ReadInfo;
  LivartConfig_frm.LivartConn.ConnectionString := LivartConfig_frm.setConnectionString;
  IF LivartConfig_frm.LivartConn.ConnectionString = '' Then
  begin
    ShowMessage('연결설정을 진행하세요');
  end;
//  sPageControl1.ActivePageIndex := 0;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheWeek(Now));
  sMaskEdit3.Text := FormatDateTime('YYYYMMDD', EndOfTheWeek(Now));

  sMaskEdit2.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit3.Text;
end;

procedure TLivartModule_frm.sButton3Click(Sender: TObject);
begin
  Case sPageControl1.ActivePageIndex of
    0:
    begin
      qryAPP700.Close;
      qryAPP700.SQL.Text := 'SELECT * FROM TC_KIS_LCROER_SEND_IF WHERE DATEE between '+ QuotedStr(sMaskEdit1.Text) + ' AND '+QuotedStr(sMaskEdit3.Text);
      qryAPP700.Open;
    end;
    2:
    begin
      qryAPP707.Close;
      qryAPP707.SQL.Text := 'SELECT * FROM TC_KIS_LCRCHR_SEND_IF WHERE APP_DATE between '+ QuotedStr(sMaskEdit2.Text) + ' AND '+QuotedStr(sMaskEdit4.Text);
      qryAPP707.open;
    end;
//    0: APP700_ReadList;
//    1: INF700_ReadList;
//    2: APP707_ReadList;
//    3: INF707_ReadList;
//    4: DOANTC_ReadList;
  end;
end;

procedure TLivartModule_frm.FormDestroy(Sender: TObject);
begin
  IF Assigned( LivartConfig_frm ) Then FreeAndNil(LivartConfig_frm);
end;

procedure TLivartModule_frm.sButton13Click(Sender: TObject);
begin
  IF not Assigned( dlg_ERPKIS_BK_CONN_frm ) Then dlg_ERPKIS_BK_CONN_frm := Tdlg_ERPKIS_BK_CONN_frm.Create(Self);
  try
    dlg_ERPKIS_BK_CONN_frm.ShowModal;
  finally
    FreeAndNil(dlg_ERPKIS_BK_CONN_frm);
  end;
end;

procedure TLivartModule_frm.APP700_getData;
var
  SC : TSQLCreate;
  __MAINT_NO : String;
  AP_BANK_STRING : String;
  AD_BANK_STRING : String;
  POS_INDEX : Integer;
  TEMP_STRING : String;
begin
  SC := TSQLCreate.Create;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      //------------------------------------------------------------------------------
      // APP700_1
      //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('APP700_1');
//      SC.ADDValue('MAINT_NO',qryAPP700IMPTNO.AsString);
      //------------------------------------------------------------------------------
      // 2018-02-05
      // 수입품의서번호+_+일련번호
      //------------------------------------------------------------------------------
//      __MAINT_NO := qryAPP700IMPTNO.AsString+'_'+qryAPP700IMPTSQ.AsString;
        __MAINT_NO := qryAPP700IMPTNO.AsString;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := 'SELECT 1 FROM APP700_1 WHERE MAINT_NO = '+QuotedStr(__MAINT_NO)+' AND MSEQ = '+qryAPP700IMPTSQ.AsString;
          Open;

          IF RecordCount > 0 Then
          begin
            IF MessageBox(Self.Handle, '해당 신청서는 이미 존재합니다. 기존자료 삭제 후 가져오시겠습니까?','이미 존재하는 신청서', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

            Close;
            SQL.Text :=  'DELETE FROM APP700_1 WHERE MAINT_NO =' + QuotedStr(__MAINT_NO)+' AND MSEQ = '+qryAPP700IMPTSQ.AsString;
            ExecSQL;

            SQL.Text :=  'DELETE FROM APP700_2 WHERE MAINT_NO =' + QuotedStr(__MAINT_NO)+' AND MSEQ = '+qryAPP700IMPTSQ.AsString;
            ExecSQL;

            SQL.Text :=  'DELETE FROM APP700_3 WHERE MAINT_NO =' + QuotedStr(__MAINT_NO)+' AND MSEQ = '+qryAPP700IMPTSQ.AsString;
            ExecSQL;
          end;
        finally
          Close;
          Free;
        end;
      end;

      SC.ADDValue('MAINT_NO', __MAINT_NO);
      SC.ADDValue('MSEQ', qryAPP700IMPTSQ.AsString, vtInteger);
//      SC.ADDValue('MESSAGE1',qryAPP700MESSAGE1.AsString);
//      SC.ADDValue('MESSAGE2',qryAPP700MESSAGE2.AsString);
      SC.ADDValue('MESSAGE1','9');
      SC.ADDValue('MESSAGE2','AB');
      SC.ADDValue('USER_ID',LoginData.sID);
      SC.ADDValue('DATEE',qryAPP700DATEE.AsString);
      SC.ADDValue('APP_DATE',qryAPP700APP_DATE.AsString);
      SC.ADDValue('IN_MATHOD',qryAPP700IN_MATHOD.AsString);
      //------------------------------------------------------------------------------
      // 2017-12-18
      // 코드는 그냥 사용
      // AP_BANK1를 사용, 자릿수 잘라내기
      //------------------------------------------------------------------------------
//      SC.ADDValue('AP_BANK',qryAPP700AP_BANK.AsString);
      //------------------------------------------------------------------------------
      // 2018-02-06
      // 연계코드 확인하여 AP_BANK CODE 넣어주기
      //------------------------------------------------------------------------------
//      SC.ADDValue('AP_BANK',LeftStr(qryAPP700AP_BANK.AsString,4));
      SC.ADDValue('AP_BANK',CompareBANKCD(qryAPP700AP_BANK.AsString));
      AP_BANK_STRING := qryAPP700AP_BANK1.AsString;
      SC.ADDValue('AP_BANK1',LeftStr(AP_BANK_STRING,35));
      SC.ADDValue('AP_BANK2',MidStr(AP_BANK_STRING,36,35));
//      SC.ADDValue('AP_BANK1',qryAPP700AP_BANK1.AsString);
//      SC.ADDValue('AP_BANK2',qryAPP700AP_BANK2.AsString);
      AP_BANK_STRING := qryAPP700AP_BANK3.AsString;
      SC.ADDValue('AP_BANK3',LeftStr(AP_BANK_STRING,35));
      SC.ADDValue('AP_BANK4',MidStr(AP_BANK_STRING,36,35));
//      SC.ADDValue('AP_BANK3',qryAPP700AP_BANK3.AsString);
//      SC.ADDValue('AP_BANK4',qryAPP700AP_BANK4.AsString);
//      SC.ADDValue('AP_BANK5',qryAPP700AP_BANK5.AsString);

      SC.ADDValue('AD_BANK',qryAPP700AD_BANK.AsString);
      
     //------------------------------------------------------------------------------
     // 2017-12-17
     // 통지은행 하이픈으로 은행명/지점명을 구분합니다
     //------------------------------------------------------------------------------
      AD_BANK_STRING := qryAPP700AD_BANK1.asString;
      POS_INDEX := POS('-', AD_BANK_STRING);
      IF POS_INDEX > 0 Then
      begin
        TEMP_STRING := LEFTSTR(AD_BANK_STRING,POS_INDEX-1);
        SC.ADDValue('AD_BANK1',LeftStr(TEMP_STRING,35));
        SC.ADDValue('AD_BANK2',MidStr(TEMP_STRING,36,35));
        TEMP_STRING := MIDSTR(AD_BANK_STRING,POS_INDEX+1,100);
        SC.ADDValue('AD_BANK3',LeftStr(TEMP_STRING,35));
        SC.ADDValue('AD_BANK4',MidStr(TEMP_STRING,36,35));
      end
      else
      begin
        TEMP_STRING := qryAPP700AD_BANK1.AsString;
        SC.ADDValue('AD_BANK1',LeftStr(TEMP_STRING,35));
        SC.ADDValue('AD_BANK2',MidStr(TEMP_STRING,36,35));
        TEMP_STRING := qryAPP700AD_BANK3.AsString;
        SC.ADDValue('AD_BANK3',LeftStr(TEMP_STRING, 35));
        SC.ADDValue('AD_BANK4',MidStr(TEMP_STRING,36,35));
      end;
      
      SC.ADDValue('AD_PAY',qryAPP700AD_PAY.AsString);
      //------------------------------------------------------------------------------
      // 2019-01-16
      // 수입용도쪽은 넘어오지 않음 사용안함
      // 이야기가 다되었음
      //------------------------------------------------------------------------------
      SC.ADDValue('IL_NO1','');
      SC.ADDValue('IL_NO2','');
      SC.ADDValue('IL_NO3','');
      SC.ADDValue('IL_NO4','');
      SC.ADDValue('IL_NO5','');
      SC.ADDValue('IL_AMT1',0);
      SC.ADDValue('IL_AMT2',0);
      SC.ADDValue('IL_AMT3',0);
      SC.ADDValue('IL_AMT4',0);
      SC.ADDValue('IL_AMT5',0);
      SC.ADDValue('IL_CUR1','');
      SC.ADDValue('IL_CUR2','');
      SC.ADDValue('IL_CUR3','');
      SC.ADDValue('IL_CUR4','');
      SC.ADDValue('IL_CUR5','');
//      SC.ADDValue('IL_NO1',qryAPP700IL_NO1.AsString);
//      SC.ADDValue('IL_NO2',qryAPP700IL_NO2.AsString);
//      SC.ADDValue('IL_NO3',qryAPP700IL_NO3.AsString);
//      SC.ADDValue('IL_NO4',qryAPP700IL_NO4.AsString);
//      SC.ADDValue('IL_NO5',qryAPP700IL_NO5.AsString);
//      SC.ADDValue('IL_AMT1',qryAPP700IL_AMT1.AsString,vtInteger);
//      SC.ADDValue('IL_AMT2',qryAPP700IL_AMT2.AsString,vtInteger);
//      SC.ADDValue('IL_AMT3',qryAPP700IL_AMT3.AsString,vtInteger);
//      SC.ADDValue('IL_AMT4',qryAPP700IL_AMT4.AsString,vtInteger);
//      SC.ADDValue('IL_AMT5',qryAPP700IL_AMT5.AsString,vtInteger);
//      SC.ADDValue('IL_CUR1',qryAPP700IL_CUR1.AsString);
//      SC.ADDValue('IL_CUR2',qryAPP700IL_CUR2.AsString);
//      SC.ADDValue('IL_CUR3',qryAPP700IL_CUR3.AsString);
//      SC.ADDValue('IL_CUR4',qryAPP700IL_CUR4.AsString);
//      SC.ADDValue('IL_CUR5',qryAPP700IL_CUR5.AsString);
      SC.ADDValue('AD_INFO1',qryAPP700AD_INFO1.AsString);
      SC.ADDValue('AD_INFO2',qryAPP700AD_INFO2.AsString);
      SC.ADDValue('AD_INFO3',qryAPP700AD_INFO3.AsString);
      SC.ADDValue('AD_INFO4',qryAPP700AD_INFO4.AsString);
      SC.ADDValue('AD_INFO5',qryAPP700AD_INFO5.AsString);
      SC.ADDValue('DOC_CD',qryAPP700DOC_CD.AsString);
      SC.ADDValue('EX_DATE',qryAPP700EX_DATE.AsString);
      SC.ADDValue('EX_PLACE',qryAPP700EX_PLACE.AsString);
      SC.ADDValue('CHK1','0',vtInteger);    //Boolean
      SC.ADDValue('CHK2','0',vtInteger);
      SC.ADDValue('CHK3', 'NULL', vtVariant);
      SC.ADDValue('prno', '0', vtInteger);
      SC.ADDValue('F_INTERFACE', 'NULL', vtVariant);
      SC.ADDValue('IMP_CD1',qryAPP700IMP_CD1.AsString);
      SC.ADDValue('IMP_CD2',qryAPP700IMP_CD2.AsString);
      SC.ADDValue('IMP_CD3',qryAPP700IMP_CD3.AsString);
      SC.ADDValue('IMP_CD4',qryAPP700IMP_CD4.AsString);
      SC.ADDValue('IMP_CD5',qryAPP700IMP_CD5.AsString);
      AddLog('APP700_1',SC.FieldList);      
      SQL.Text := SC.CreateSQL;  
      Clipboard.AsText := SQL.Text;
      ExecSQL;
      
      //------------------------------------------------------------------------------
      // APP700_2
      //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('APP700_2');
      SC.ADDValue('MAINT_NO', __MAINT_NO);
      SC.ADDValue('MSEQ', qryAPP700IMPTSQ.AsString, vtInteger);
//      SC.ADDValue('MAINT_NO',TEST_MAINT_NO);
      TEMP_STRING := qryAPP700APPLIC1.AsString+
                     qryAPP700APPLIC2.AsString+
                     qryAPP700APPLIC3.AsString+
                     qryAPP700APPLIC4.AsString+
                     qryAPP700APPLIC5.AsString;
      SC.ADDValue('APPLIC1', LeftStr(TEMP_STRING, 35));
      SC.ADDValue('APPLIC2', MidStr(TEMP_STRING, 36 ,35));
      SC.ADDValue('APPLIC3', MidStr(TEMP_STRING, 71 ,35));
      SC.ADDValue('APPLIC4', MidStr(TEMP_STRING, 106 ,35));
      SC.ADDValue('APPLIC5', MidStr(TEMP_STRING, 141 ,35));
//      SC.ADDValue('APPLIC1', qryAPP700APPLIC1.AsString);
//      SC.ADDValue('APPLIC2', qryAPP700APPLIC2.AsString);
//      SC.ADDValue('APPLIC3', qryAPP700APPLIC3.AsString);
//      SC.ADDValue('APPLIC4', qryAPP700APPLIC4.AsString);
//      SC.ADDValue('APPLIC5', qryAPP700APPLIC5.AsString);
      SC.ADDValue('BENEFC', qryAPP700BENEFC.AsString);
      //------------------------------------------------------------------------------
      // 2019-07-31
      // "，" = #163 #172 수정
      //------------------------------------------------------------------------------      
      SC.ADDValue('BENEFC1', AnsiReplaceText(AnsiReplaceText( qryAPP700BENEFC1.AsString ,'  ', ' '),'，',','));
      SC.ADDValue('BENEFC2', AnsiReplaceText(AnsiReplaceText( qryAPP700BENEFC2.AsString ,'  ', ' '),'，',','));
      SC.ADDValue('BENEFC3', AnsiReplaceText(AnsiReplaceText( qryAPP700BENEFC3.AsString ,'  ', ' '),'，',','));
      SC.ADDValue('BENEFC4', AnsiReplaceText(AnsiReplaceText( qryAPP700BENEFC4.AsString ,'  ', ' '),'，',','));
      SC.ADDValue('BENEFC5', AnsiReplaceText(AnsiReplaceText( qryAPP700BENEFC5.AsString ,'  ', ' '),'，',','));
//      SC.ADDValue('BENEFC1', qryAPP700BENEFC1.AsString);
//      SC.ADDValue('BENEFC2', qryAPP700BENEFC2.AsString);
//      SC.ADDValue('BENEFC3', qryAPP700BENEFC3.AsString);
//      SC.ADDValue('BENEFC4', qryAPP700BENEFC4.AsString);
//      SC.ADDValue('BENEFC5', qryAPP700BENEFC5.AsString);
      SC.ADDValue('CD_AMT', qryAPP700CD_AMT.AsString,vtInteger);
      SC.ADDValue('CD_CUR', qryAPP700CD_CUR.AsString);
      SC.ADDValue('CD_PERP', qryAPP700CD_PERP.AsString,vtInteger);
      SC.ADDValue('CD_PERM', qryAPP700CD_PERM.AsString,vtInteger);
      SC.ADDValue('CD_MAX', qryAPP700CD_MAX.AsString);
      SC.ADDValue('AA_CV1', qryAPP700AA_CV1.AsString);
      SC.ADDValue('AA_CV2', qryAPP700AA_CV2.AsString);
      SC.ADDValue('AA_CV3', qryAPP700AA_CV3.AsString);
      SC.ADDValue('AA_CV4', qryAPP700AA_CV4.AsString);
      SC.ADDValue('DRAFT1', qryAPP700DRAFT1.AsString);
      SC.ADDValue('DRAFT2', qryAPP700DRAFT2.AsString);
      SC.ADDValue('DRAFT3', qryAPP700DRAFT3.AsString);
      SC.ADDValue('MIX_PAY1', qryAPP700MIX_PAY1.AsString);
      SC.ADDValue('MIX_PAY2', qryAPP700MIX_PAY2.AsString);
      SC.ADDValue('MIX_PAY3', qryAPP700MIX_PAY3.AsString);
      SC.ADDValue('MIX_PAY4', qryAPP700MIX_PAY4.AsString);
      SC.ADDValue('DEF_PAY1', qryAPP700DEF_PAY1.AsString);
      SC.ADDValue('DEF_PAY2', qryAPP700DEF_PAY2.AsString);
      SC.ADDValue('DEF_PAY3', qryAPP700DEF_PAY3.AsString);
      SC.ADDValue('DEF_PAY4', qryAPP700DEF_PAY4.AsString);
      SC.ADDValue('PSHIP', qryAPP700PSHIP.AsString);
      SC.ADDValue('TSHIP', qryAPP700TSHIP.AsString);
      SC.ADDValue('LOAD_ON', qryAPP700LOAD_ON.AsString);
      SC.ADDValue('FOR_TRAN', qryAPP700FOR_TRAN.AsString);
      SC.ADDValue('LST_DATE', qryAPP700LST_DATE.AsString);
      SC.ADDValue('SHIP_PD1', qryAPP700SHIP_PD1.AsString);
      SC.ADDValue('SHIP_PD2', qryAPP700SHIP_PD2.AsString);
      SC.ADDValue('SHIP_PD3', qryAPP700SHIP_PD3.AsString);
      SC.ADDValue('SHIP_PD4', qryAPP700SHIP_PD4.AsString);
      SC.ADDValue('SHIP_PD5', qryAPP700SHIP_PD5.AsString);
      SC.ADDValue('SHIP_PD6', qryAPP700SHIP_PD6.AsString);
      SC.ADDValue('DESGOOD_1', qryAPP700DESGOOD_1.AsString);
      AddLog('APP700_2',SC.FieldList);      
      SQL.Text := SC.CreateSQL;
      Clipboard.AsText := SQL.Text;
      ExecSQL;

      //------------------------------------------------------------------------------
      // APP700_3
      //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('APP700_3');
      SC.ADDValue('MAINT_NO', __MAINT_NO);
//      SC.ADDValue('MAINT_NO',TEST_MAINT_NO);
      SC.ADDValue('MSEQ', qryAPP700IMPTSQ.AsString, vtInteger);
      IF AnsiMatchText( UpperCase(qryAPP700DOC_380.AsString) , TrueValues ) Then
        SC.ADDValue('DOC_380', '1')
      else
      IF AnsiMatchText( UpperCase(qryAPP700DOC_380.AsString) , FalseValues ) Then
        SC.ADDValue('DOC_380', '0')
      else
        SC.ADDValue('DOC_380', 'NULL', vtVariant);

      SC.ADDValue('DOC_380_1', qryAPP700DOC_380_1.AsString,vtInteger);


      IF AnsiMatchText( UpperCase(qryAPP700DOC_705_YN.AsString) , TrueValues ) Then
        SC.ADDValue('DOC_705', '1', vtinteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700DOC_705_YN.AsString) , FalseValues ) Then
        SC.ADDValue('DOC_705', '0', vtinteger)
      else
        SC.ADDValue('DOC_705', 'NULL', vtVariant);

      SC.ADDValue('DOC_705_GUBUN', qryAPP700DOC_705_GUBUN.AsString);

      SC.ADDValue('DOC_705_1', qryAPP700DOC_705_1.AsString);
      SC.ADDValue('DOC_705_2', qryAPP700DOC_705_2.AsString);
      SC.ADDValue('DOC_705_3', qryAPP700DOC_705_3.AsString);
      SC.ADDValue('DOC_705_4', qryAPP700DOC_705_4.AsString);

//      IF UpperCase(qryAPP700DOC_740.AsString) = 'Y' Then
      IF AnsiMatchText( UpperCase(qryAPP700DOC_740.AsString) , TrueValues ) Then
        SC.ADDValue('DOC_740','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700DOC_740.AsString) , FalseValues ) Then
        SC.ADDValue('DOC_740','0',vtInteger)
      else
        SC.ADDValue('DOC_740', 'NULL', vtVariant);

      SC.ADDValue('DOC_740_1', qryAPP700DOC_740_1.AsString);
      SC.ADDValue('DOC_740_2', qryAPP700DOC_740_2.AsString);
      SC.ADDValue('DOC_740_3', qryAPP700DOC_740_3.AsString);
      SC.ADDValue('DOC_740_4', qryAPP700DOC_740_4.AsString);

      IF AnsiMatchText( UpperCase(qryAPP700DOC_530.AsString) , TrueValues ) Then
        SC.ADDValue('DOC_530','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700DOC_530.AsString) , FalseValues ) Then
        SC.ADDValue('DOC_530','0',vtInteger)
      else
        SC.ADDValue('DOC_530', 'NULL', vtVariant);
      SC.ADDValue('DOC_530_1', qryAPP700DOC_530_1.AsString);
      SC.ADDValue('DOC_530_2', qryAPP700DOC_530_2.AsString);

      IF AnsiMatchText( UpperCase(qryAPP700DOC_271.AsString) , TrueValues ) Then
        SC.ADDValue('DOC_271','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700DOC_271.AsString) , FalseValues ) Then
        SC.ADDValue('DOC_271','0',vtInteger)
      else
        SC.ADDValue('DOC_271', 'NULL', vtVariant);
      SC.ADDValue('DOC_271_1', qryAPP700DOC_271_1.AsString,vtInteger);

      IF AnsiMatchText( UpperCase(qryAPP700DOC_861.AsString) , TrueValues ) Then
        SC.ADDValue('DOC_861','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700DOC_861.AsString) , FalseValues ) Then
        SC.ADDValue('DOC_861','0',vtInteger)
      else
        SC.ADDValue('DOC_861', 'NULL', vtVariant);

      IF AnsiMatchText( UpperCase(qryAPP700DOC_2AA.AsString) , TrueValues ) Then
        SC.ADDValue('DOC_2AA','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700DOC_2AA.AsString) , FalseValues ) Then
        SC.ADDValue('DOC_2AA','0',vtInteger)
      else
        SC.ADDValue('DOC_2AA', 'NULL', vtVariant);
      SC.ADDValue('DOC_2AA_1', qryAPP700DOC_2AA_1.AsString);

      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AA.AsString) , TrueValues ) Then
        SC.ADDValue('ACD_2AA','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AA.AsString) , FalseValues ) Then
        SC.ADDValue('ACD_2AA','0',vtInteger)
      else
        SC.ADDValue('ACD_2AA', 'NULL', vtVariant);
      SC.ADDValue('ACD_2AA_1', qryAPP700ACD_2AA_1.AsString);

      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AB.AsString) , TrueValues ) Then
        SC.ADDValue('ACD_2AB','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AB.AsString) , FalseValues ) Then
        SC.ADDValue('ACD_2AB','0',vtInteger)
      else
        SC.ADDValue('ACD_2AB', 'NULL', vtVariant);

      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AC.AsString) , TrueValues ) Then
        SC.ADDValue('ACD_2AC','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AC.AsString) , FalseValues ) Then
        SC.ADDValue('ACD_2AC','0',vtInteger)
      else
        SC.ADDValue('ACD_2AC', 'NULL', vtVariant);
//      SC.ADDValue('ACD_2AC', qryAPP700ACD_2AC.AsString);

      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AD.AsString) , TrueValues ) Then
        SC.ADDValue('ACD_2AD','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AD.AsString) , FalseValues ) Then
        SC.ADDValue('ACD_2AD','0',vtInteger)
      else
        SC.ADDValue('ACD_2AD', 'NULL', vtVariant);
//      SC.ADDValue('ACD_2AD', qryAPP700ACD_2AD.AsString);\

      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AE.AsString) , TrueValues ) Then
        SC.ADDValue('ACD_2AE','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700ACD_2AE.AsString) , FalseValues ) Then
        SC.ADDValue('ACD_2AE','0',vtInteger)
      else
        SC.ADDValue('ACD_2AE', 'NULL', vtVariant);
      SC.ADDValue('ACD_2AE_1', qryAPP700ACD_2AE_1.AsString);

      SC.ADDValue('CHARGE', qryAPP700CHARGE.AsString);
      SC.ADDValue('PERIOD', qryAPP700PERIOD.AsString,vtInteger);
      SC.ADDValue('CONFIRMM', qryAPP700CONFIRMM.AsString);
//      SC.ADDValue('INSTRCT', qryAPP700
//      SC.ADDValue('INSTRCT_1',
      SC.ADDValue('EX_NAME1', qryAPP700EX_NAME1.AsString);
      SC.ADDValue('EX_NAME2', qryAPP700EX_NAME2.AsString);
      SC.ADDValue('EX_NAME3', qryAPP700EX_NAME3.AsString);
      SC.ADDValue('EX_ADDR1', qryAPP700EX_ADDR1.AsString);
      SC.ADDValue('EX_ADDR2', qryAPP700EX_ADDR2.AsString);
      SC.ADDValue('ORIGIN', qryAPP700ORIGIN.AsString);
      SC.ADDValue('ORIGIN_M', qryAPP700ORIGIN_M.AsString);
      SC.ADDValue('PL_TERM', qryAPP700PL_TERM.AsString);
      SC.ADDValue('TERM_PR', qryAPP700TERM_PR.AsString);
      //------------------------------------------------------------------------
      // 2017-12-19
      // TERM_PR명칭을 찾아서 입력
      //------------------------------------------------------------------------
      SC.ADDValue('TERM_PR_M', getCodeContent('가격조건',qryAPP700TERM_PR.AsString));
//      SC.ADDValue('TERM_PR_M', qryAPP700TERM_PR_M.AsString);
//      SC.ADDValue('SRBUHO', qryAPP700
//      SC.ADDValue('DOC_705_GUBUN', qryAPP700DOC_705_GUBUN.AsString);
      SC.ADDValue('CARRIAGE', qryAPP700CARRIAGE.AsString);

      IF AnsiMatchText( UpperCase(qryAPP700DOC_760.AsString) , TrueValues ) Then
        SC.ADDValue('DOC_760','1',vtInteger)
      else
      IF AnsiMatchText( UpperCase(qryAPP700DOC_760.AsString) , FalseValues ) Then
        SC.ADDValue('DOC_760','0',vtInteger)
      else
        SC.ADDValue('DOC_760', 'NULL', vtVariant);
      SC.ADDValue('DOC_760_1', qryAPP700DOC_760_1.AsString);
      SC.ADDValue('DOC_760_2', qryAPP700DOC_760_2.AsString);
      SC.ADDValue('DOC_760_3', qryAPP700DOC_760_3.AsString);
      SC.ADDValue('DOC_760_4', qryAPP700DOC_760_4.AsString);
      SC.ADDValue('SUNJUCK_PORT', qryAPP700SUNJUK_PORT.AsString);
      SC.ADDValue('DOCHACK_PORT', qryAPP700DOCHAK_PORT.AsString);

      //------------------------------------------------------------------------------
      // 18-11-19 SWITFT 변경
      //------------------------------------------------------------------------------
      SC.ADDValue('PERIOD_IDX', qryAPP700PERIOD_IDX.AsString, vtInteger);
      SC.ADDValue('PERIOD_TXT', qryAPP700PERIOD_TXT.AsString);
      SC.ADDValue('CONFIRM_BANKNM', qryAPP700CONFIRM_BANKNM.AsString);
      SC.ADDValue('CONFIRM_BANKBR', qryAPP700CONFIRM_BANKBR.AsString);
      SC.ADDValue('SPECIAL_PAY', qryAPP700SPECIAL_PAY.AsString);
      SC.ADDValue('CHARGE_1', qryAPP700CHARGE_1.AsString);

      AddLog('APP700_3',SC.FieldList);
      SQL.Text := SC.CreateSQL;  
      Clipboard.AsText := SQL.Text;
      ExecSQL;

    finally
      Close;
      Free;
      SC.Free;
    end;
  end;
end;

procedure TLivartModule_frm.sButton4Click(Sender: TObject);
begin
  DMMssql.BeginTrans;
  try
    Case sPageControl1.ActivePageIndex of
      0: APP700_getData;
      2: APP707_getData;
    end;
    ShowMessage('가져오기가 완료되었습니다');
    DMMssql.CommitTrans;
  except
    on E:Exception do
    begin
      DMMssql.RollbackTrans;
      Case sPageControl1.ActivePageIndex of
        0,2 :
          MessageBox(Self.Handle,PChar('에러가 발생하여 가져오기에 실패했습니다'#13#10'ERROR : '+E.Message),'가져오기 오류',MB_OK+MB_ICONERROR);
      else
          MessageBox(Self.Handle,PChar('에러가 발생하여 재전송에 실패했습니다'#13#10'ERROR : '+E.Message),'가져오기 오류',MB_OK+MB_ICONERROR);
      end;
    end;
  end;
end;

function TLivartModule_frm.CompareBANKCD(sERP_BANK_CD: String): String;
begin
  Result := '';
  with TADOQuery.Create(self) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT KIS_CODE, ENAME1 as BKNM, ENAME3 as BKBR'#13#10+
                  'FROM ERP_KIS_BANKCONN LEFT JOIN BANKCODE ON KIS_CODE = BANKCODE.CODE'#13#10+
                  'WHERE IS_USE = 1 AND ERP_CODE = '+QuotedStr(sERP_BANK_CD);
      Open;
      IF RecordCount > 0 Then
      begin
        Result := FieldByName('KIS_CODE').AsString;
      end;

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TLivartModule_frm.AddLog(obj, sData : String);
var
  LOG_PATH : String;
  LOG_DATA : TStringList;
begin
  IF not chk_SvFDINFO.Checked Then Exit;
  
  LOG_PATH := ExtractFilePath(Application.ExeName)+'LOG\';
  ForceDirectories(LOG_PATH);

  LOG_DATA := TStringList.Create;
  try
    LOG_DATA.Text := sData;
    LOG_DATA.SaveToFile(LOG_PATH+FormatDateTime('YYYYMMDDHHNNSS',Now)+'_'+obj+'_log.txt');
  finally
    LOG_DATA.Free;
  end;
end;

function TLivartModule_frm.getCodeContent(sPrefix, sCode: String): String;
begin
  with TADOQuery.Create(self) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT [CODE], [NAME] FROM [CODE2NDD] WHERE Prefix = '+QuotedStr(sPrefix)+' AND CODE = '+QuotedStr(sCode);
      Open;

      Result := '';

      IF RecordCount > 0 Then
      begin
        Result := Fields[1].AsString;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TLivartModule_frm.sMaskEdit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key = VK_RETURN Then sButton3Click(nil);
end;

procedure TLivartModule_frm.APP707_getData;
var
  SC : TSQLCreate;
  __MAINT_NO, __MSEQ, __AMDNO : String;
  AD_BANK_TEMP : String;
  TempStr : string;
  TEMP_STRING : String;
begin
  SC := TSQLCreate.Create;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SC.DMLType := dmlInsert;
      SC.SQLHeader('APP707_1');
      // .....................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
      //------------------------------------------------------------------------------
      // APP707_1
      //------------------------------------------------------------------------------
//      __MAINT_NO :=  qryAPP707IMPTNO.AsString+'_'+qryAPP707IMPTSQ.AsString+'__'+qryAPP707AMNDSQ.AsString;
      __MAINT_NO :=  qryAPP707IMPTNO.AsString;
      __MSEQ := qryAPP707IMPTSQ.AsString;
      __AMDNO := qryAPP707AMNDSQ.AsString;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := 'SELECT 1 FROM APP707_1 WHERE MAINT_NO = '+QuotedStr(__MAINT_NO)+' AND MSEQ = '+QuotedStr(__MSEQ)+' AND AMD_NO = '+QuotedStr(__AMDNO);
          Open;

          IF RecordCount > 0 Then
          begin
            IF MessageBox(Self.Handle, '해당 신청서는 이미 존재합니다. 기존자료 삭제 후 가져오시겠습니까?','이미 존재하는 신청서', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

            Close;
            SQL.Text :=  'DELETE FROM APP707_1 WHERE MAINT_NO = '+QuotedStr(__MAINT_NO)+' AND MSEQ = '+QuotedStr(__MSEQ)+' AND AMD_NO = '+QuotedStr(__AMDNO);
            ExecSQL;

            SQL.Text :=  'DELETE FROM APP707_2 WHERE MAINT_NO = '+QuotedStr(__MAINT_NO)+' AND MSEQ = '+QuotedStr(__MSEQ)+' AND AMD_NO = '+QuotedStr(__AMDNO);
            ExecSQL;
          end;
        finally
          Close;
          Free;
        end;
      end;
      SC.ADDValue('MAINT_NO', __MAINT_NO);
      SC.ADDValue('MSeq', __MSEQ);
      SC.ADDValue('AMD_NO', qryAPP707AMNDSQ.AsString,vtInteger);
      SC.ADDValue('MESSAGE1', qryAPP707MESSAGE1.AsString);
      SC.ADDValue('MESSAGE2', qryAPP707MESSAGE2.AsString);
      SC.ADDValue('USER_ID', LoginData.sID);
      SC.ADDValue('DATEE', qryAPP707APP_DATE.AsString);
//      SC.ADDValue('APP_DATE', qryAPP707APP_DATE.AsString);
      SC.ADDValue('APP_DATE', FormatDateTime('YYYYMMDD',Now));
      SC.ADDValue('IN_MATHOD', qryAPP707IN_MATHOD.AsString);
//      SC.ADDValue('AP_BANK', qryAPP707AP_BANK.AsString);

      SC.ADDValue('AP_BANK',CompareBANKCD(qryAPP707AP_BANK.AsString));
      SC.ADDValue('AP_BANK1', qryAPP707AP_BANK1.AsString);

      SC.ADDValue('AP_BANK2', qryAPP707AP_BANK2.AsString);
      SC.ADDValue('AP_BANK3', qryAPP707AP_BANK3.AsString);
      SC.ADDValue('AP_BANK4', qryAPP707AP_BANK4.AsString);
      SC.ADDValue('AP_BANK5', qryAPP707AP_BANK5.AsString);
      SC.ADDValue('AD_BANK', qryAPP707AD_BANK.AsString);

    //------------------------------------------------------------------------------
    // 2018-02-06
    // AD_BANK 35자리 끊어서 넣기
    //------------------------------------------------------------------------------
      AD_BANK_TEMP := qryAPP707AD_BANK1.AsString;
      SC.ADDValue('AD_BANK1', LeftStr(AD_BANK_TEMP,35));
      SC.ADDValue('AD_BANK2', MidStr(AD_BANK_TEMP,36,35));
      AD_BANK_TEMP := qryAPP707AD_BANK3.AsString;
      SC.ADDValue('AD_BANK3', LeftStr(AD_BANK_TEMP,35));
      SC.ADDValue('AD_BANK4', MidStr(AD_BANK_TEMP,36,35));
//      SC.ADDValue('AD_BANK1', qryAPP707AD_BANK1.AsString);
//      SC.ADDValue('AD_BANK2', qryAPP707AD_BANK2.AsString);
//      SC.ADDValue('AD_BANK3', qryAPP707AD_BANK3.AsString);
//      SC.ADDValue('AD_BANK4', qryAPP707AD_BANK4.AsString);


      SC.ADDValue('AD_PAY', qryAPP707AD_PAY.AsString);
      SC.ADDValue('IL_NO1', qryAPP707IL_NO1.AsString);
      SC.ADDValue('IL_NO2', qryAPP707IL_NO2.AsString);
      SC.ADDValue('IL_NO3', qryAPP707IL_NO3.AsString);
      SC.ADDValue('IL_NO4', qryAPP707IL_NO4.AsString);
      SC.ADDValue('IL_NO5', qryAPP707IL_NO5.AsString);
      SC.ADDValue('IL_AMT1', qryAPP707IL_AMT1.AsString,vtInteger);
      SC.ADDValue('IL_AMT2', qryAPP707IL_AMT2.AsString,vtInteger);
      SC.ADDValue('IL_AMT3', qryAPP707IL_AMT3.AsString,vtInteger);
      SC.ADDValue('IL_AMT4', qryAPP707IL_AMT4.AsString,vtInteger);
      SC.ADDValue('IL_AMT5', qryAPP707IL_AMT5.AsString,vtInteger);
      SC.ADDValue('IL_CUR1', qryAPP707IL_CUR1.AsString);
      SC.ADDValue('IL_CUR2', qryAPP707IL_CUR2.AsString);

      SC.ADDValue('IL_CUR3', qryAPP707IL_CUR3.AsString);
      SC.ADDValue('IL_CUR4', qryAPP707IL_CUR4.AsString);
      SC.ADDValue('IL_CUR5', qryAPP707IL_CUR5.AsString);
      SC.ADDValue('AD_INFO1', qryAPP707AD_INFO1.AsString);
      SC.ADDValue('AD_INFO2', qryAPP707AD_INFO2.AsString);
      SC.ADDValue('AD_INFO3', qryAPP707AD_INFO3.AsString);
      SC.ADDValue('AD_INFO4', qryAPP707AD_INFO4.AsString);
      SC.ADDValue('AD_INFO5', qryAPP707AD_INFO5.AsString);
      SC.ADDValue('CD_NO', qryAPP707CD_NO.AsString);
      SC.ADDValue('ISS_DATE', qryAPP707ISS_DATE.AsString);
      SC.ADDValue('EX_DATE', qryAPP707EX_DATE.AsString);
      SC.ADDValue('EX_PLACE', qryAPP707EXPLOC.AsString);
      SC.ADDValue('CHK1','0',vtInteger);    //Boolean
      SC.ADDValue('CHK2','0',vtInteger);
      SC.ADDValue('CHK3', 'NULL', vtVariant);
      SC.ADDValue('prno', '0',vtInteger);
      SC.ADDValue('F_INTERFACE', 'NULL',vtVariant);
      SC.ADDValue('IMP_CD1', qryAPP707IMP_CD1.AsString);
      SC.ADDValue('IMP_CD2', qryAPP707IMP_CD2.AsString);
      SC.ADDValue('IMP_CD3', qryAPP707IMP_CD3.AsString);
      SC.ADDValue('IMP_CD4', qryAPP707IMP_CD4.AsString);
      SC.ADDValue('IMP_CD5', qryAPP707IMP_CD5.AsString);

      //------------------------------------------------------------------------------
      // 18-11-19 SWITFT 변경
      //------------------------------------------------------------------------------
      IF UpperCase(qryAPP707IS_CANCEL.AsString) = 'CANCEL' Then
        SC.ADDValue('IS_CANCEL', 'CR')
      else
        SC.ADDValue('IS_CANCEL', qryAPP707IS_CANCEL.AsString);

      SC.ADDValue('PSHIP', qryAPP707PSHIP.AsString);
      SC.ADDValue('TSHIP', qryAPP707TSHIP.AsString);

      SQL.Text := SC.CreateSQL;
      AddLog('APP707_1',sc.FieldList);
      ExecSQL;

      SC.SQLHeader('APP707_2');
      //------------------------------------------------------------------------------
      // APP707_2
      //------------------------------------------------------------------------------
//      TEST_MAINT_NO := qryAPP707IMPTNO.AsString;
//      TEST_MAINT_NO := TEST_MAINT_NO+'_T'+FormatDateTime('SS',Now);
//      SC.ADDValue('MAINT_NO', TEST_MAINT_NO);

      SC.ADDValue('MAINT_NO', __MAINT_NO);
      SC.ADDValue('MSeq', __MSEQ);
      SC.ADDValue('Amd_No', qryAPP707AMNDSQ.AsString,vtInteger);

      {SC.ADDValue('APPLIC1', qryAPP707APPLIC1.AsString);
      SC.ADDValue('APPLIC2', qryAPP707APPLIC2.AsString);
      SC.ADDValue('APPLIC3', qryAPP707APPLIC3.AsString);
      SC.ADDValue('APPLIC4', qryAPP707APPLIC4.AsString);
      SC.ADDValue('APPLIC5', qryAPP707APPLIC5.AsString);}

      //20200203 수정
      TEMP_STRING := qryAPP707APPLIC1.AsString+
                     qryAPP707APPLIC2.AsString+
                     qryAPP707APPLIC3.AsString+
                     qryAPP707APPLIC4.AsString+
                     qryAPP707APPLIC5.AsString;
      SC.ADDValue('APPLIC1', LeftStr(TEMP_STRING, 35));
      SC.ADDValue('APPLIC2', MidStr(TEMP_STRING, 36 ,35));
      SC.ADDValue('APPLIC3', MidStr(TEMP_STRING, 71 ,35));
      SC.ADDValue('APPLIC4', MidStr(TEMP_STRING, 106 ,35));
      SC.ADDValue('APPLIC5', MidStr(TEMP_STRING, 141 ,35));

      SC.ADDValue('BENEFC', qryAPP707BENEFC.AsString);
      SC.ADDValue('BENEFC1', qryAPP707BENEFC1.AsString);
      SC.ADDValue('BENEFC2', qryAPP707BENEFC2.AsString);
      SC.ADDValue('BENEFC3', qryAPP707BENEFC3.AsString);
      SC.ADDValue('BENEFC4', qryAPP707BENEFC4.AsString);
      SC.ADDValue('BENEFC5', qryAPP707BENEFC5.AsString);
      SC.ADDValue('INCD_CUR', qryAPP707INCD_CUR.AsString);
      SC.ADDValue('INCD_AMT', qryAPP707INCD_AMT.AsString,vtInteger);
      SC.ADDValue('DECD_CUR', qryAPP707DECD_CUR.AsString);
      SC.ADDValue('DECD_AMT', qryAPP707DECD_AMT.AsString,vtInteger);
      SC.ADDValue('NWCD_CUR', qryAPP707NWCD_CUR.AsString);
      SC.ADDValue('NWCD_AMT', qryAPP707NWCD_AMT.AsString,vtInteger);
      SC.ADDValue('CD_PERP', qryAPP707CD_PERP.AsString,vtInteger);
      SC.ADDValue('CD_PERM', qryAPP707CD_PERM.AsString,vtInteger);
      SC.ADDValue('CD_MAX', qryAPP707CD_MAX.AsString);
      SC.ADDValue('AA_CV1', qryAPP707AA_CV1.AsString);
      SC.ADDValue('AA_CV2', qryAPP707AA_CV2.AsString);
      SC.ADDValue('AA_CV3', qryAPP707AA_CV3.AsString);
      SC.ADDValue('AA_CV4', qryAPP707AA_CV4.AsString);
      SC.ADDValue('LOAD_ON', qryAPP707LOAD_ON.AsString);
      SC.ADDValue('FOR_TRAN', qryAPP707FOR_TRAN.AsString);
      SC.ADDValue('LST_DATE', qryAPP707LST_DATE.AsString);

      SC.ADDValue('SHIP_PD1', qryAPP707SHIP_PD1.AsString);
      SC.ADDValue('SHIP_PD2', qryAPP707SHIP_PD2.AsString);
      SC.ADDValue('SHIP_PD3', qryAPP707SHIP_PD3.AsString);
      SC.ADDValue('SHIP_PD4', qryAPP707SHIP_PD4.AsString);
      SC.ADDValue('SHIP_PD5', qryAPP707SHIP_PD5.AsString);
      SC.ADDValue('SHIP_PD6', qryAPP707SHIP_PD6.AsString);
      SC.ADDValue('NARRAT', qryAPP707NARRAT.AsString);
      SC.ADDValue('NARRAT_1', qryAPP707NARRAT_1.AsString);
      SC.ADDValue('EX_NAME1', qryAPP707EX_NAME1.AsString);
      SC.ADDValue('EX_NAME2', qryAPP707EX_NAME2.AsString);
      SC.ADDValue('EX_NAME3', qryAPP707EX_NAME3.AsString);
      //추가예정
      SC.ADDValue('EX_ADDR1', qryAPP707EX_ADDR1.AsString);
      SC.ADDValue('EX_ADDR2', qryAPP707EX_ADDR2.AsString);
      //삭제
//      SC.ADDValue('SRBUHO', qryapp707SRBUHO
      SC.ADDValue('BFCD_AMT', qryAPP707BFCD_AMT.AsString,vtInteger);
      SC.ADDValue('BFCD_CUR', qryAPP707BFCD_CUR.AsString);
      SC.ADDValue('CARRIAGE', qryAPP707CARRIAGE.AsString);
      SC.ADDValue('SUNJUCK_PORT', qryAPP707SUNJUCK_PORT.AsString);
      SC.ADDValue('DOCHACK_PORT', qryAPP707DOCHAK_PORT.AsString);

      //------------------------------------------------------------------------------
      // 18-11-19 SWITFT 변경
      //------------------------------------------------------------------------------
      SC.ADDValue('CHARGE',       qryAPP707CHARGE.AsString );
      SC.ADDValue('CHARGE_1',     qryAPP707CHARGE_1.AsString );
      SC.ADDValue('AMD_CHARGE',   qryAPP707AMD_CHARGE.AsString );
      SC.ADDValue('AMD_CHARGE_1', qryAPP707AMD_CHARGE_1.AsString );
      SC.ADDValue('SPECIAL_PAY',  qryAPP707SPECIAL_PAY.AsString );
      SC.ADDValue('GOODS_DESC',   qryAPP707GOODS_DESC.AsString );
      SC.ADDValue('DOC_REQ',      qryAPP707DOC_REQ.AsString );
      SC.ADDValue('ADD_CONDITION', qryAPP707ADD_CONDITION.AsString );
      SC.ADDValue('DRAFT1', qryAPP707DRAFT1.AsString );
      SC.ADDValue('DRAFT2', qryAPP707DRAFT2.AsString );
      SC.ADDValue('DRAFT3', qryAPP707DRAFT3.AsString );
      SC.ADDValue('MIX1', qryAPP707MIX1.AsString );
      SC.ADDValue('MIX2', qryAPP707MIX2.AsString );
      SC.ADDValue('MIX3', qryAPP707MIX3.AsString );
      SC.ADDValue('MIX4', qryAPP707MIX4.AsString );
      SC.ADDValue('DEFPAY1', qryAPP707DEFPAY1.AsString );
      SC.ADDValue('DEFPAY2', qryAPP707DEFPAY2.AsString );
      SC.ADDValue('DEFPAY3', qryAPP707DEFPAY3.AsString );
      SC.ADDValue('DEFPAY4', qryAPP707DEFPAY4.AsString );
      SC.ADDValue('PERIOD_DAYS', qryAPP707PERIOD_DAYS.AsString, vtInteger );
      SC.ADDValue('PERIOD_IDX', qryAPP707PERIOD_IDX.AsString, vtInteger );
      SC.ADDValue('PERIOD_DETAIL', qryAPP707PERIOD_TXT.AsString );
      SC.ADDValue('CONFIRM', qryAPP707CONFIRMM.AsString);
//      SC.ADDValue('CONFIRM_BIC',
      SC.ADDValue('CONFIRM1', CopyK( qryAPP707CONFIRM_BANKNM.AsString, 1, 35) );
      SC.ADDValue('CONFIRM2', CopyK( qryAPP707CONFIRM_BANKNM.AsString, 36, 35) );
      SC.ADDValue('CONFIRM3', CopyK( qryAPP707CONFIRM_BANKBR.AsString, 1, 35) );
      SC.ADDValue('CONFIRM4', CopyK( qryAPP707CONFIRM_BANKBR.AsString, 36, 35) );

      TempStr := Trim( qryAPP707APPLIC_CHG1.AsString+qryAPP707APPLIC_CHG2.AsString+qryAPP707APPLIC_CHG3.AsString+qryAPP707APPLIC_CHG4.AsString );
      IF TempStr = '' Then
      begin
        SC.ADDValue('APPLIC_CHG', '0', vtInteger);
      {  SC.ADDValue('APPLIC_CHG1', qryAPP707APPLIC1.AsString);
        SC.ADDValue('APPLIC_CHG2', qryAPP707APPLIC2.AsString);
        SC.ADDValue('APPLIC_CHG3', qryAPP707APPLIC3.AsString);
        SC.ADDValue('APPLIC_CHG4', qryAPP707APPLIC4.AsString);}

        TEMP_STRING := qryAPP707APPLIC1.AsString+
                       qryAPP707APPLIC2.AsString+
                       qryAPP707APPLIC3.AsString+
                       qryAPP707APPLIC4.AsString;

        SC.ADDValue('APPLIC_CHG1', LeftStr(TEMP_STRING, 35));
        SC.ADDValue('APPLIC_CHG2', MidStr(TEMP_STRING, 36 ,35));
        SC.ADDValue('APPLIC_CHG3', MidStr(TEMP_STRING, 71 ,35));
        SC.ADDValue('APPLIC_CHG4', MidStr(TEMP_STRING, 106 ,35));

//        SC.ADDValue('APPLIC_CHG5', qryAPP707APPLIC5.AsString);
      end
      else
      begin
        SC.ADDValue('APPLIC_CHG', '1', vtInteger);

        {SC.ADDValue('APPLIC_CHG1', qryAPP707APPLIC_CHG1.AsString);
        SC.ADDValue('APPLIC_CHG2', qryAPP707APPLIC_CHG2.AsString);
        SC.ADDValue('APPLIC_CHG3', qryAPP707APPLIC_CHG3.AsString);
        SC.ADDValue('APPLIC_CHG4', qryAPP707APPLIC_CHG4.AsString);}

        TEMP_STRING := qryAPP707APPLIC_CHG1.AsString+
                       qryAPP707APPLIC_CHG2.AsString+
                       qryAPP707APPLIC_CHG3.AsString+
                       qryAPP707APPLIC_CHG4.AsString;

        SC.ADDValue('APPLIC_CHG1', LeftStr(TEMP_STRING, 35));
        SC.ADDValue('APPLIC_CHG2', MidStr(TEMP_STRING, 36 ,35));
        SC.ADDValue('APPLIC_CHG3', MidStr(TEMP_STRING, 71 ,35));
        SC.ADDValue('APPLIC_CHG4', MidStr(TEMP_STRING, 106 ,35));

      end;

      TempStr := Trim( qryAPP707BENEFC1.AsString + qryAPP707BENEFC2.AsString + qryAPP707BENEFC3.AsString + qryAPP707BENEFC4.AsString );
      IF TempStr = '' Then
        SC.ADDValue('BENEFC_CHG', '0', vtInteger)
      else
        SC.ADDValue('BENEFC_CHG', '1', vtinteger);

      SQL.Text := SC.CreateSQL;
      AddLog('APP707_2',sc.FieldList);
      ExecSQL;
    finally
      Close;
      Free;
      SC.Free;
    end;
  end;
end;

end.
