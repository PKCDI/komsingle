object dlg_KIS_BK_List_frm: Tdlg_KIS_BK_List_frm
  Left = 820
  Top = 161
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = 'KIS '#51008#54665#53076#46300' '#47785#47197
  ClientHeight = 461
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 15
  object sDBGrid1: TsDBGrid
    Left = 0
    Top = 0
    Width = 644
    Height = 461
    Align = alClient
    Color = clWhite
    Ctl3D = False
    DataSource = dsBK
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Color = clBtnFace
        Expanded = False
        FieldName = 'CODE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        Title.Alignment = taCenter
        Title.Caption = #51008#54665#53076#46300
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BK_NM'
        Title.Caption = #51008#54665#47749
        Width = 267
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NK_BR'
        Title.Caption = #51648#51216#47749
        Width = 288
        Visible = True
      end>
  end
  object qryBK: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, ENAME1 as BK_NM, ENAME3 as NK_BR FROM BANKCODE')
    Left = 40
    Top = 16
  end
  object dsBK: TDataSource
    DataSet = qryBK
    Left = 72
    Top = 16
  end
end
