unit dlg_KIS_BK_List;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, Grids, DBGrids, acDBGrid;

type
  TKIS_BK_INFO = record
    BK_CD : String;
    BK_NM : String;
    BK_BR : String;
  end;
  
  Tdlg_KIS_BK_List_frm = class(TForm)
    sDBGrid1: TsDBGrid;
    qryBK: TADOQuery;
    dsBK: TDataSource;
    procedure sDBGrid1DblClick(Sender: TObject);
  private
    function getBKINFO: TKIS_BK_INFO;
    procedure ReadList;
    { Private declarations }
  public
    { Public declarations }
    function RunDialog(sCODE :String = ''):TModalResult;
    property KIS_BK_INFO : TKIS_BK_INFO read getBKINFO;
  end;

var
  dlg_KIS_BK_List_frm: Tdlg_KIS_BK_List_frm;

implementation

uses
  MSSQL;

{$R *.dfm}

function Tdlg_KIS_BK_List_frm.getBKINFO: TKIS_BK_INFO;
begin
  Result.BK_CD := qryBK.Fields[0].AsString;
  Result.BK_NM := qryBK.Fields[1].AsString;
  Result.BK_BR := qryBK.Fields[2].AsString;
end;

procedure Tdlg_KIS_BK_List_frm.ReadList;
begin
  qryBK.Close;
  qryBK.Open;
end;

function Tdlg_KIS_BK_List_frm.RunDialog(sCODE: String): TModalResult;
begin
  ReadList;
  IF Trim(sCode) <> '' Then
  begin
    qryBK.Locate('CODE', sCODE, []);
  end;

  Result := Self.ShowModal;
end;

procedure Tdlg_KIS_BK_List_frm.sDBGrid1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
