object dlg_ERPKIS_BK_CONN_frm: Tdlg_ERPKIS_BK_CONN_frm
  Left = 450
  Top = 244
  BorderIcons = []
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = 'ERP-KIS '#51008#54665#53076#46300' '#50672#44228
  ClientHeight = 516
  ClientWidth = 610
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 610
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      610
      41)
    object sSpeedButton4: TsSpeedButton
      Left = 235
      Top = 5
      Width = 16
      Height = 32
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sButton1: TsButton
      Left = 4
      Top = 4
      Width = 117
      Height = 34
      Cursor = crHandPoint
      Caption = #50672#44228#53076#46300#46321#47197
      TabOrder = 0
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 35
    end
    object sButton2: TsButton
      Left = 546
      Top = 4
      Width = 60
      Height = 34
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      ModalResult = 2
      TabOrder = 1
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 34
    end
    object sButton3: TsButton
      Tag = 1
      Left = 123
      Top = 4
      Width = 53
      Height = 34
      Cursor = crHandPoint
      Caption = #49688#51221
      TabOrder = 2
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
    end
    object sButton4: TsButton
      Tag = 2
      Left = 178
      Top = 4
      Width = 53
      Height = 34
      Cursor = crHandPoint
      Caption = #49325#51228
      TabOrder = 3
      OnClick = sButton4Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
    end
    object sButton5: TsButton
      Tag = 1
      Left = 255
      Top = 4
      Width = 78
      Height = 34
      Cursor = crHandPoint
      Caption = #50672#44228#54644#51228
      TabOrder = 4
      OnClick = sButton5Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 36
    end
  end
  object sPanel2: TsPanel
    Left = 0
    Top = 41
    Width = 610
    Height = 1
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'TRANSPARENT'
  end
  object sDBGrid1: TsDBGrid
    Left = 0
    Top = 42
    Width = 610
    Height = 474
    Align = alClient
    Color = clWhite
    Ctl3D = True
    DataSource = dsBK
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Color = clBtnFace
        Expanded = False
        FieldName = 'ERP_CODE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        Title.Alignment = taCenter
        Title.Caption = 'ERP '#51008#54665#53076#46300
        Width = 93
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BK_NM'
        Title.Caption = #51008#54665#47749
        Width = 190
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BK_BRANCH'
        Title.Caption = #51648#51216#47749
        Width = 190
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'KIS_CODE'
        Title.Alignment = taCenter
        Title.Caption = #50672#44228#53076#46300
        Width = 69
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'IS_USE'
        Title.Alignment = taCenter
        Title.Caption = #49324#50857
        Width = 41
        Visible = True
      end>
  end
  object qryBK: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryBKAfterOpen
    Parameters = <>
    SQL.Strings = (
      'SELECT ERP_CODE, BK_NM, BK_BRANCH, KIS_CODE, IS_USE'
      'FROM ERP_KIS_BANKCONN'
      'ORDER BY ERP_CODE')
    Left = 32
    Top = 72
    object qryBKERP_CODE: TStringField
      FieldName = 'ERP_CODE'
      Size = 8
    end
    object qryBKBK_NM: TStringField
      FieldName = 'BK_NM'
      Size = 150
    end
    object qryBKBK_BRANCH: TStringField
      FieldName = 'BK_BRANCH'
      Size = 150
    end
    object qryBKKIS_CODE: TStringField
      FieldName = 'KIS_CODE'
      Size = 6
    end
    object qryBKIS_USE: TBooleanField
      FieldName = 'IS_USE'
      OnGetText = qryBKIS_USEGetText
    end
  end
  object dsBK: TDataSource
    DataSet = qryBK
    Left = 64
    Top = 72
  end
  object qryCreateTable: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABL' +
        'E_NAME = '#39'ERP_KIS_BANKCONN'#39')'
      'BEGIN'
      '    CREATE TABLE [dbo].[ERP_KIS_BANKCONN]('
      #9'    [ERP_CODE] [varchar](8) NOT NULL,'
      #9'    [BK_NM] [varchar](150) NOT NULL,'
      #9'    [BK_BRANCH] [varchar](150) NULL,'
      #9'    [KIS_CODE] [varchar](6) NULL,'
      
        #9'    [IS_USE] [bit] NOT NULL CONSTRAINT [DF_ERP_KIS_BANKCONN_IS_' +
        'USE]  DEFAULT ((1)),'
      '     CONSTRAINT [PK_ERP_KIS_BANKCONN] PRIMARY KEY CLUSTERED '
      '    ('
      #9'    [ERP_CODE] ASC'
      
        '    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE' +
        '_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON ' +
        '[PRIMARY]'
      '    ) ON [PRIMARY]'
      ''
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'110000'#39', N'#39#45453#54801#51473#50521#54924#48376#51216#39', N'#39#39', NUL' +
        'L, 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'1111'#39', N'#39#54788#45824#52264#53804#51088#51613#44428#39', N'#39#39', NULL,' +
        ' 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'120000'#39', N'#39#45800#50948#45453#54801#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'200000'#39', N'#39#50864#47532#51008#54665#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'230000'#39', N'#39'SC'#51228#51068#51008#54665#48376#51216#39', N'#39#39', NU' +
        'LL, 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'270000'#39', N'#39#54620#44397#50472#54000#51008#54665#48376#51216#39', N'#39#39', NU' +
        'LL, 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'30000'#39', N'#39'INDUSTRIAL BANK OF ' +
        'KOREA'#39', N'#39'HOCHIMINH BRANCH'#39', NULL, 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'310000'#39', N'#39#44221#45224#51008#54665#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'320000'#39', N'#39#48512#49328#51008#54665#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'340000'#39', N'#39#44305#51452#51008#54665#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'350000'#39', N'#39#51228#51452#51008#54665#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'370000'#39', N'#39#51204#48513#51008#54665#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'390000'#39', N'#39#54616#45208#51008#54665#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'40000'#39', N'#39#44397#48124#51008#54665#48376#51216#39', N'#39#39', NULL,' +
        ' 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'450000'#39', N'#39#49352#47560#51012#44552#44256#48376#51216#39', N'#39#39', NUL' +
        'L, 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'480000'#39', N'#39#49888#54801#48376#51216#39', N'#39#39', NULL, ' +
        '1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'50000'#39', N'#39#50808#54872#51008#54665#48376#51216#39', N'#39#39', NULL,' +
        ' 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'710000'#39', N'#39#50864#52404#44397#48376#51216#39', N'#39#39', NULL,' +
        ' 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'880000'#39', N'#39#49888#54620#51008#54665#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'910000'#39', N'#39#44592#50629#51008#54665#39', N'#39#39', NULL, ' +
        '1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'A10000'#39', N'#39#45824#44417#45712#54665#48376#51216#39', N'#39#39', NULL' +
        ', 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'A20000'#39', N'#39#44592#53440#51008#54665#39', N'#39#39', NULL, ' +
        '1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'A30000'#39', N'#39'KOREA EXCHANGE BAN' +
        'K'#39', N'#39#39', NULL, 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'A40000'#39', N'#39'BANK OF MONTREAL'#39',' +
        ' N'#39#39', NULL, 1)'
      
        'INSERT [dbo].[ERP_KIS_BANKCONN] ([ERP_CODE], [BK_NM], [BK_BRANCH' +
        '], [KIS_CODE], [IS_USE]) VALUES (N'#39'A50000'#39', N'#39'JSC BANK CENTERCRE' +
        'DIT'#39', N'#39#39', NULL, 1)'
      'END')
    Left = 32
    Top = 112
  end
end
