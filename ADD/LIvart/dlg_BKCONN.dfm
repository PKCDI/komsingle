object dlg_BKCONN_frm: Tdlg_BKCONN_frm
  Left = 568
  Top = 197
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = #50672#44228#46321#47197'/'#49688#51221
  ClientHeight = 310
  ClientWidth = 462
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 462
    Height = 310
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sLabel1: TsLabel
      Left = 16
      Top = 16
      Width = 78
      Height = 17
      Caption = #50672#44228#51008#54665#49444#51221
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel2: TsLabel
      Left = 16
      Top = 32
      Width = 406
      Height = 15
      Caption = 'ERP'#50640#49436' '#49324#50857#54616#45716' '#51008#54665#53076#46300#50752' EDI '#51008#54665#53076#46300#47484' '#50672#44228#54616#50668' '#48320#54872' '#49884' '#48152#50689#54633#45768#45796
    end
    object sLabel3: TsLabel
      Left = 16
      Top = 48
      Width = 295
      Height = 15
      Caption = #49688#51221' '#54624' '#49884' ERP'#51008#54665#53076#46300#45716' '#48320#44221#48520#44032', '#49325#51228' '#54980' '#45796#49884' '#51077#47141
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 1118702
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      UseSkinColor = False
    end
    object edt_ERP_CD: TsEdit
      Left = 96
      Top = 72
      Width = 81
      Height = 23
      Color = 13041663
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
      MaxLength = 8
      ParentFont = False
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #51008#54665#53076#46300
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object edt_ERP_NM: TsEdit
      Left = 96
      Top = 96
      Width = 329
      Height = 23
      Color = 13041663
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #51008#54665#47749
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object edt_ERP_BR: TsEdit
      Left = 96
      Top = 120
      Width = 329
      Height = 23
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 150
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #51648#51216#47749
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object edt_KIS_CD: TsEdit
      Left = 96
      Top = 152
      Width = 57
      Height = 23
      Color = 13041663
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
      MaxLength = 6
      ParentFont = False
      TabOrder = 3
      OnChange = edt_KIS_CDChange
      OnExit = edt_KIS_CDExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #50672#44228#51008#54665#53076#46300
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object sCheckBox1: TsCheckBox
      Left = 93
      Top = 232
      Width = 76
      Height = 19
      Caption = #50672#44228#49324#50857
      TabOrder = 4
      ImgChecked = 0
      ImgUnchecked = 0
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edt_KIS_NM: TsEdit
      Left = 96
      Top = 176
      Width = 329
      Height = 23
      TabStop = False
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #51008#54665#47749
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object edt_KIS_BR: TsEdit
      Left = 96
      Top = 200
      Width = 329
      Height = 23
      TabStop = False
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 8
      ParentFont = False
      ReadOnly = True
      TabOrder = 6
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #51648#51216#47749
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object sButton1: TsButton
      Left = 154
      Top = 152
      Width = 23
      Height = 23
      Caption = '...'
      TabOrder = 7
      TabStop = False
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sButton2: TsButton
      Left = 272
      Top = 256
      Width = 97
      Height = 34
      Caption = #51200#51109
      TabOrder = 8
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sButton3: TsButton
      Left = 374
      Top = 256
      Width = 51
      Height = 34
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 9
      TabStop = False
      SkinData.SkinSection = 'BUTTON'
    end
  end
end
