unit dlg_ERPKIS_BK_CONN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, DB, ADODB, Grids, DBGrids, acDBGrid,
  ExtCtrls, sPanel, Buttons, sSpeedButton;

type
  Tdlg_ERPKIS_BK_CONN_frm = class(TForm)
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    sDBGrid1: TsDBGrid;
    qryBK: TADOQuery;
    dsBK: TDataSource;
    sButton1: TsButton;
    sButton2: TsButton;
    sButton3: TsButton;
    sButton4: TsButton;
    qryCreateTable: TADOQuery;
    qryBKERP_CODE: TStringField;
    qryBKBK_NM: TStringField;
    qryBKBK_BRANCH: TStringField;
    qryBKKIS_CODE: TStringField;
    qryBKIS_USE: TBooleanField;
    sButton5: TsButton;
    sSpeedButton4: TsSpeedButton;
    procedure sButton1Click(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryBKAfterOpen(DataSet: TDataSet);
    procedure qryBKIS_USEGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sButton5Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReadList;
  public
    { Public declarations }
  end;

var
  dlg_ERPKIS_BK_CONN_frm: Tdlg_ERPKIS_BK_CONN_frm;

implementation

uses
  dlg_BKCONN, MSSQL, ICON;

{$R *.dfm}

procedure Tdlg_ERPKIS_BK_CONN_frm.ReadList;
begin
  qryBK.Close;
  qryBK.Open;
end;

procedure Tdlg_ERPKIS_BK_CONN_frm.sButton1Click(Sender: TObject);
begin
  IF not Assigned( dlg_BKCONN_frm ) Then dlg_BKCONN_frm := Tdlg_BKCONN_frm.Create(Self);
  try
    IF (Sender as TsButton).Tag = 0 Then
    begin
      IF dlg_BKCONN_frm.RunDialog(wtINS,qryBK.Fields) = mrOK Then
      begin
        ReadList;
        qryBK.Locate('ERP_CODE',dlg_BKCONN_frm.CODE,[]);
      end;
    end
    else
    IF (Sender as TsButton).Tag = 1 Then
    begin
      IF dlg_BKCONN_frm.RunDialog(wtMOD,qryBK.Fields) = mrOK Then
      begin
        ReadList;
        qryBK.Locate('ERP_CODE',dlg_BKCONN_frm.CODE,[]);
      end;
    end;
  finally
    FreeAndNil(dlg_BKCONN_frm);
  end;
end;

procedure Tdlg_ERPKIS_BK_CONN_frm.sButton4Click(Sender: TObject);
var
  MSG : string;
begin
  MSG := '다음 은행연계코드를 삭제하시겠습니까?'#13#10+
         'ERP은행코드: '+qryBK.Fields[0].AsString+'['+Trim(qryBK.Fields[1].AsString+' '+qryBK.Fields[2].AsString)+']'#13#10+
         'KIS은행코드: '+qryBK.Fields[3].AsString;
  IF MessageBox(Self.Handle,PChar(MSG),'은행연계코드 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'DELETE FROM ERP_KIS_BANKCONN WHERE ERP_CODE = '+QuotedStr(qryBK.Fields[0].AsString);
      ExecSQL;

      ReadList;
      ShowMessage('삭제가 완료되었습니다');
    finally
      Close;
      Free;
    end;
  end;
end;

procedure Tdlg_ERPKIS_BK_CONN_frm.FormShow(Sender: TObject);
begin
  qryCreateTable.ExecSQL;

  ReadList;
end;

procedure Tdlg_ERPKIS_BK_CONN_frm.qryBKAfterOpen(DataSet: TDataSet);
begin
  sButton3.Enabled := DataSet.RecordCount > 0;
  sButton4.Enabled := DataSet.RecordCount > 0;
end;

procedure Tdlg_ERPKIS_BK_CONN_frm.qryBKIS_USEGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  IF qryBKIS_USE.AsBoolean Then Text := 'Y'
  else Text := '';
end;

procedure Tdlg_ERPKIS_BK_CONN_frm.sButton5Click(Sender: TObject);
var
  CurrIDX : Integer;
begin
  IF MessageBox(Self.Handle, '해당 은행의 데이터연계를 해제 하시겠습니까?', '연결해제 확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_NO then Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'UPDATE ERP_KIS_BANKCONN SET KIS_CODE = NULL, IS_USE = 0 WHERE ERP_CODE = '+QuotedStr(qryBKERP_CODE.AsString);
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;

  CurrIDX := qryBK.RecNo;

  qryBK.Close;
  qryBK.Open;

  IF qryBK.RecordCount <= CurrIDX Then
    qryBK.Last
  else
    qryBK.MoveBy(CurrIDX-1);

end;

end.
