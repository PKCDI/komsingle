object LivartConfig_frm: TLivartConfig_frm
  Left = 721
  Top = 369
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = #50724#46972#53364' '#50672#44208#49444#51221
  ClientHeight = 102
  ClientWidth = 505
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 505
    Height = 102
    SkinData.SkinSection = 'GROUPBOX'
    Align = alClient
    
    TabOrder = 0
    object edtDataSource: TsEdit
      Left = 88
      Top = 64
      Width = 145
      Height = 23
      Color = clWhite
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Data Source'
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object edtUserID: TsEdit
      Left = 88
      Top = 16
      Width = 145
      Height = 23
      Color = clWhite
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'User ID'
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object edtPwd: TsEdit
      Left = 88
      Top = 40
      Width = 145
      Height = 23
      Color = clWhite
      PasswordChar = '*'
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = 'Password'
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object sButton1: TsButton
      Left = 248
      Top = 16
      Width = 97
      Height = 25
      Caption = 'Connect Test'
      TabOrder = 3
      TabStop = False
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sButton3: TsButton
      Left = 248
      Top = 48
      Width = 249
      Height = 38
      Caption = 'Save Configuration'
      TabOrder = 4
      TabStop = False
      OnClick = sButton3Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object LivartConn: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Password=7895123;Persist Security Info=True;U' +
      'ser ID=KISLVT;Data Source=LIVART'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 8
    Top = 8
  end
  object qryCreateTable: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      
        'IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES W' +
        'HERE TABLE_NAME = '#39'LIVART_CFG'#39')'
      'BEGIN'
      '    DECLARE @HOSTNAME varchar(100)'
      '           ,@NET varchar(100) '
      ''
      
        '    SELECT @HOSTNAME = hostname FROM sys.sysprocesses WHERE spid' +
        ' = @@SPID'
      
        '    SELECT @NET = client_net_address FROM sys.dm_exec_connection' +
        's WHERE Session_id = @@SPID'
      ''
      '    CREATE TABLE [dbo].[LIVART_CFG]('
      #9'    [USERID] [varchar](20) NOT NULL,'
      #9'    [PWD] [varchar](20) NOT NULL,'
      #9'    [DATASOURCE] [varchar](20) NOT NULL,'
      #9'    [CHGDATE] [datetime] NOT NULL,'
      #9'    [SPID] varchar(10) NOT NULL,'
      '        [HOSTNAME] varchar(50) NOT NULL,'
      '        [NET] varchar(50) NOT NULL'
      '     CONSTRAINT [PK_LIVART_CFG] PRIMARY KEY CLUSTERED '
      '    ('
      #9'    [USERID] ASC'
      '    )) ON [PRIMARY]'
      ''
      
        '    ALTER TABLE [dbo].[LIVART_CFG] ADD  CONSTRAINT [DF_LIVART_CF' +
        'G_CHGDATE]  DEFAULT (getdate()) FOR [CHGDATE]'
      ''
      
        '    INSERT INTO LIVART_CFG(USERID, PWD, DATASOURCE, CHGDATE, SPI' +
        'D, HOSTNAME, NET)'
      
        '    VALUES('#39'USERID'#39','#39#39','#39'DATASOURCE'#39', getdate(), @@SPID, @HOSTNAM' +
        'E, @NET)'
      'END')
    Left = 40
    Top = 8
  end
  object qryUpdateConfig: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'USERID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'PWD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'DATASOURCE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @HOSTNAME varchar(100)'
      '       ,@NET varchar(100) '
      '       ,@USERID varchar(30)'
      '       ,@PWD varchar(30)'
      '       ,@DATASOURCE varchar(30)'
      ''
      
        'SELECT @HOSTNAME = hostname FROM sys.sysprocesses WHERE spid = @' +
        '@SPID'
      
        'SELECT @NET = client_net_address FROM sys.dm_exec_connections WH' +
        'ERE Session_id = @@SPID'
      'SET @USERID = :USERID'
      'SET @PWD = :PWD'
      'SET @DATASOURCE = :DATASOURCE'
      ''
      'IF EXISTS(SELECT 1 FROM LIVART_CFG)'
      'BEGIN'
      
        '    UPDATE [LIVART_CFG] SET USERID = @USERID, PWD = @PWD, DATASO' +
        'URCE = @DATASOURCE, CHGDATE = getdate(), SPID = @@SPID, HOSTNAME' +
        ' = @HOSTNAME, NET = @NET'
      'END'
      'ELSE'
      'BEGIN'
      '    INSERT INTO LIVART_CFG'
      
        '    VALUES(@USERID, @PWD, @DATASOURCE, getdate(), @@SPID, @HOSTN' +
        'AME, @NET)'
      'END')
    Left = 40
    Top = 40
  end
end
