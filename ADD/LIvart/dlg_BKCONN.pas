unit dlg_BKCONN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sCheckBox, sLabel, sEdit, sButton, ADODB, DB;

type
  TBK_INFO = record
    BK_CD : String;
    BK_NM : String;
    BK_BR : String;
  end;

  TWorkType = (wtINS, wtMOD);

  Tdlg_BKCONN_frm = class(TForm)
    sPanel1: TsPanel;
    edt_ERP_CD: TsEdit;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    edt_ERP_NM: TsEdit;
    edt_ERP_BR: TsEdit;
    edt_KIS_CD: TsEdit;
    sCheckBox1: TsCheckBox;
    edt_KIS_NM: TsEdit;
    edt_KIS_BR: TsEdit;
    sButton1: TsButton;
    sButton2: TsButton;
    sButton3: TsButton;
    sLabel3: TsLabel;
    procedure sButton2Click(Sender: TObject);
    procedure edt_KIS_CDExit(Sender: TObject);
    procedure edt_KIS_CDChange(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
  private
    { Private declarations }
    FFields : TFields;
    FWork : TWorkType;
    function getBKINFO(CODE : String):TBK_INFO;
    procedure setData;
    procedure InsertData;
    procedure UpdateData;
    procedure ExecuteSQL(sSQL : String);
    function getCODE: String;
  public
    { Public declarations }
    function RunDialog(work : TWorkType; uFields : TFields):TModalResult;
    property CODE:String read getCODE;
  end;

var
  dlg_BKCONN_frm: Tdlg_BKCONN_frm;

implementation

uses dlg_KIS_BK_List, SQLCreator, MSSQL;

{$R *.dfm}

{ Tdlg_BKCONN_frm }

procedure Tdlg_BKCONN_frm.ExecuteSQL(sSQL: String);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := sSQL;
      ExecSQL; 
    finally
      Close;
      Free;
    end;
  end;
end;

function Tdlg_BKCONN_frm.getBKINFO(CODE: String): TBK_INFO;
begin
  Result.BK_CD := '';
  Result.BK_NM := '';
  Result.BK_BR := '';

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT CODE, ENAME1, ENAME3 FROM BANKCODE WHERE CODE = '+QuotedStr(CODE);
      Open;

      IF RecordCount > 0 then
      begin
        Result.BK_CD := Fields[0].AsString;
        Result.BK_NM := Fields[1].AsString;
        Result.BK_BR := Fields[2].AsString;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

function Tdlg_BKCONN_frm.getCODE: String;
begin
  Result := edt_ERP_CD.Text;
end;

procedure Tdlg_BKCONN_frm.InsertData;
var
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;
  try
    SC.DMLType := dmlInsert;
    SC.SQLHeader('ERP_KIS_BANKCONN');
    SC.ADDValue('ERP_CODE', Trim(edt_ERP_CD.Text));
    SC.ADDValue('BK_NM', Trim(edt_ERP_NM.Text));
    SC.ADDValue('BK_BRANCH', Trim(edt_ERP_BR.Text));
    SC.ADDValue('KIS_CODE', Trim(edt_KIS_CD.Text));
    IF sCheckBox1.Checked Then
      SC.ADDValue('IS_USE', 1 )
    else
      SC.ADDValue('IS_USE', 0 );
    ExecuteSQL(SC.CreateSQL);
  finally
    SC.Free;
  end;
end;

function Tdlg_BKCONN_frm.RunDialog(work : TWorkType; uFields: TFields): TModalResult;
begin
  FFields := uFields;
  FWork := work;

  IF FWork = wtMOD Then setData;
  edt_ERP_CD.Enabled := FWork = wtINS;
  edt_ERP_NM.Enabled := edt_ERP_CD.Enabled;
  edt_ERP_BR.Enabled := edt_ERP_CD.Enabled;
  sLabel3.Visible := not edt_ERP_CD.Enabled;

  IF Self.ShowModal = mrOK Then
  begin
    //데이터 작업 진행
    Case FWork of
      wtINS : InsertData;
      wtMOD : UpdateData;
    end;
    Result := mrOk;
  end
  else
  begin
    Result := mrCancel;
  end;


end;

procedure Tdlg_BKCONN_frm.setData;
var
  TMP_INFO : TBK_INFO;
begin
//  ERP_CODE, BK_NM, BK_BRANCH, KIS_CODE, IS_USE
  edt_ERP_CD.Text := FFields.FieldByName('ERP_CODE').AsString;
  edt_ERP_NM.Text := FFields.FieldByName('BK_NM').AsString;
  edt_ERP_BR.Text := FFields.FieldByName('BK_BRANCH').AsString;

  edt_KIS_CD.Text := Trim(FFields.FieldByName('KIS_CODE').AsString);

  TMP_INFO := getBKINFO(edt_KIS_CD.Text);
  edt_KIS_NM.Text := TMP_INFO.BK_NM;
  edt_KIS_BR.Text := TMP_INFO.BK_BR;

  sCheckBox1.Checked := FFields.FieldByName('IS_USE').AsBoolean;
end;

procedure Tdlg_BKCONN_frm.UpdateData;
var
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;
  try
    SC.DMLType := dmlUpdate;
    SC.SQLHeader('ERP_KIS_BANKCONN');
    SC.ADDValue('BK_NM', Trim(edt_ERP_NM.Text));
    SC.ADDValue('BK_BRANCH', Trim(edt_ERP_BR.Text));
    SC.ADDValue('KIS_CODE', Trim(edt_KIS_CD.Text));
    IF sCheckBox1.Checked Then
      SC.ADDValue('IS_USE', 1 )
    else
      SC.ADDValue('IS_USE', 0 );

    SC.ADDWhere('ERP_CODE', Trim(edt_ERP_CD.Text));

    ExecuteSQL(SC.CreateSQL);
  finally
    SC.Free;
  end;
end;

procedure Tdlg_BKCONN_frm.sButton2Click(Sender: TObject);
begin
  IF Trim(edt_ERP_CD.Text) = '' Then
  begin
    edt_ERP_CD.SetFocus;
    ShowMessage('ERP은행코드를 입력해주세요');
    Exit;    
  end
  else
  IF Trim(edt_KIS_CD.Text) = '' then
  begin
    edt_KIS_CD.SetFocus;
    ShowMessage('연계은행코드를 입력하거나 검색해주세요');
    Exit;
  end;

  ModalResult := mrOK;


end;

procedure Tdlg_BKCONN_frm.edt_KIS_CDExit(Sender: TObject);
var
  TMP_INFO : TBK_INFO;
begin
  TMP_INFO := getBKINFO(edt_KIS_CD.Text);
  edt_KIS_NM.Text := TMP_INFO.BK_NM;
  edt_KIS_BR.Text := TMP_INFO.BK_BR;
end;

procedure Tdlg_BKCONN_frm.edt_KIS_CDChange(Sender: TObject);
begin
  IF Trim(edt_KIS_CD.Text) = '' Then
  begin
    edt_KIS_NM.Clear;
    edt_KIS_BR.Clear;
  end;
end;

procedure Tdlg_BKCONN_frm.sButton1Click(Sender: TObject);
begin
  dlg_KIS_BK_List_frm := Tdlg_KIS_BK_List_frm.Create(Self);
  try
    IF dlg_KIS_BK_List_frm.RunDialog(edt_KIS_CD.Text) = mrOK Then
    begin
      edt_KIS_CD.Text := dlg_KIS_BK_List_frm.KIS_BK_INFO.BK_CD;
      edt_KIS_NM.Text := dlg_KIS_BK_List_frm.KIS_BK_INFO.BK_NM;
      edt_KIS_BR.Text := dlg_KIS_BK_List_frm.KIS_BK_INFO.BK_BR;
    end;
  finally
    FreeAndNil( dlg_KIS_BK_List_frm );
  end;
end;

end.
