unit UI_test;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, Grids, DBGrids, acDBGrid, sSkinProvider;

type
  TUI_test_frm = class(TChildForm_frm)
    sDBGrid1: TsDBGrid;
    sDBGrid2: TsDBGrid;
    sDBGrid3: TsDBGrid;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_test_frm: TUI_test_frm;

implementation

{$R *.dfm}

uses LivartData; 

procedure TUI_test_frm.FormShow(Sender: TObject);
begin
  inherited;
  sDBGrid1.Visible := sDBGrid1.DataSource <> nil;
  sDBGrid2.Visible := sDBGrid2.DataSource <> nil;
  sDBGrid3.Visible := sDBGrid3.DataSource <> nil;
end;

end.
