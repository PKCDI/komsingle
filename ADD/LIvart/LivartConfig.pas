unit LivartConfig;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, sEdit, ExtCtrls, sPanel, sButton;

type
  TDataType = (dtID, dtPWD, dtDatasource);
  TLivartConfig_frm = class(TForm)
    LivartConn: TADOConnection;
    sPanel1: TsPanel;
    edtDataSource: TsEdit;
    edtUserID: TsEdit;
    edtPwd: TsEdit;
    sButton1: TsButton;
    sButton3: TsButton;
    qryCreateTable: TADOQuery;
    qryUpdateConfig: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
  private
    function getData(DataType : TDataType):String;
    function getID: String;
    function getPWD: String;
    function getDataSource : String;
    { Private declarations }
  public
    { Public declarations }
    procedure ExistsTable;
    procedure ReadInfo;
    function setConnectionString:String;
    property ID:String read getID;
    property PWD:String read getPWD;
    property DataSourceLivart:String read getDataSource;

  end;

var
  LivartConfig_frm: TLivartConfig_frm;

implementation

uses
  MSSQL;

{$R *.dfm}

{ TLivartConfig_frm }

procedure TLivartConfig_frm.ExistsTable;
begin
  qryCreateTable.Close;
  qryCreateTable.ExecSQL;
end;

procedure TLivartConfig_frm.FormShow(Sender: TObject);
begin
//------------------------------------------------------------------------------
// 테이블이 존재하는지 확인 없으면 생성
//------------------------------------------------------------------------------
  ExistsTable;

//------------------------------------------------------------------------------
// 기본셋
//------------------------------------------------------------------------------
  ReadInfo;

end;

function TLivartConfig_frm.getData(DataType: TDataType): String;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      case DataType of
        dtID : SQL.Text := 'SELECT USERID';
        dtPWD : SQL.Text := 'SELECT PWD';
        dtDatasource : SQL.Text := 'SELECT DATASOURCE';
      end;

      SQL.Text := SQL.Text+' FROM LIVART_CFG';
      Open;

      Result := Fields[0].AsString;
    finally
      Close;
      Free;
    end;
  end;
end;

function TLivartConfig_frm.getDataSource: String;
begin
  Result := getData(dtDatasource);
end;

function TLivartConfig_frm.getID: String;
begin
  Result := getData(dtID);
end;

function TLivartConfig_frm.getPWD: String;
begin
  Result := getData(dtPWD);
end;

function TLivartConfig_frm.setConnectionString: String;
var
  ConnList : TStringList;
begin
  ReadInfo;

  IF Trim(edtUserID.Text) = '' then
  begin
    Result := '';
    exit;
  end;

  ConnList := TStringList.Create;
  try
    ConnList.Values['Provider'] := 'MSDASQL.1';
    ConnList.Values['Password'] := edtPwd.Text;
    ConnList.Values['Persist Security Info'] := 'True';
    ConnList.Values['User ID'] := edtUserID.Text;
    ConnList.Values['Data Source'] := edtDataSource.Text;
//Provider=OraOLEDB.Oracle.1;Password=7895123;Persist Security Info=True;User ID=KISLVT;Data Source=192.168.29.105:1521/XE
//    ConnList.Values['Provider'] := 'OraOLEDB.Oracle.1';
//    ConnList.Values['Password'] := edtPwd.Text;
//    ConnList.Values['Persist Security Info'] := 'True';
//    ConnList.Values['User ID'] := edtUserID.Text;
//    ConnList.Values['Data Source'] := edtDataSource.Text;

    Result := ConnList.Strings[0]+';'+ConnList.Strings[1]+';'+ConnList.Strings[2]+';'+ConnList.Strings[3]+';'+ConnList.Strings[4];

    IF LivartConn.Connected Then LivartConn.Close;
    LivartConn.ConnectionString := Result;
  finally
    ConnList.Free;
  end;
end;

procedure TLivartConfig_frm.sButton1Click(Sender: TObject);
begin
  LivartConn.Close;
  LivartConn.ConnectionString := setConnectionString;
  try
    try
      LivartConn.Open;
      ShowMessage('연결에 성공하였습니다');
    except
      on E:Exception do
      begin
        MessageBox(Self.Handle,PChar('연결에 실패하였습니다'#13#10'ERROR : '+E.Message),'연결오류',MB_OK+MB_ICONERROR);
      end;
    end;
  finally
    LivartConn.Close;
  end;
//  ShowMessage(setConnectionString);
end;

procedure TLivartConfig_frm.sButton3Click(Sender: TObject);
begin
  with qryUpdateConfig do
  begin
    Close;
    //USERID
    Parameters[0].Value := edtUserID.Text;
    //PWD
    Parameters[1].Value := edtPwd.Text;
    //DATASOURCE
    Parameters[2].Value := edtDataSource.Text;
    try
      ExecSQL;
      ShowMessage('저장이 완료되었습니다');
      Self.Close;
    except
      on E:Exception do
      begin
        MessageBox(Self.Handle,PChar('오류가 발생하여 저장실패'#13#10'ERROR : '+E.Message),'저장오류',MB_OK+MB_ICONERROR);
      end;
    end;
  end;
end;

procedure TLivartConfig_frm.ReadInfo;
begin
  edtUserID.Text := getID;
  edtPwd.Text := getPWD;
  edtDataSource.Text := getDataSource;
end;

end.
