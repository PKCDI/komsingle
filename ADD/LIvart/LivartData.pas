unit LivartData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sButton, ComCtrls, sPageControl,
  Grids, DBGrids, acDBGrid, DB, ADODB, Buttons, sSpeedButton, sEdit,
  sComboBox, Mask, sMaskEdit, StrUtils, sSkinProvider;

type
  TLivartData_frm = class(TForm)
    sPanel1: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    sTabSheet4: TsTabSheet;
    sTabSheet5: TsTabSheet;
    sButton1: TsButton;
    sPanel3: TsPanel;
    sButton3: TsButton;
    qryAPP700: TADOQuery;
    dsAPP700: TDataSource;
    sDBGrid1: TsDBGrid;
    qryAPP700CO_GBCD: TStringField;
    qryAPP700IMPTNO: TStringField;
    qryAPP700IMPTSQ: TIntegerField;
    qryAPP700USER_ID: TStringField;
    qryAPP700MESSAGE1: TStringField;
    qryAPP700MESSAGE2: TStringField;
    qryAPP700APP_DATE: TStringField;
    qryAPP700CHK2: TStringField;
    qryAPP700CHK3: TStringField;
    qryAPP700DATEE: TStringField;
    qryAPP700IN_MATHOD: TStringField;
    qryAPP700AP_BANK: TStringField;
    qryAPP700AP_BANK1: TStringField;
    qryAPP700AP_BANK2: TStringField;
    qryAPP700AP_BANK3: TStringField;
    qryAPP700AP_BANK4: TStringField;
    qryAPP700AP_BANK5: TStringField;
    qryAPP700AD_BANK: TStringField;
    qryAPP700AD_BANK1: TStringField;
    qryAPP700AD_BANK2: TStringField;
    qryAPP700AD_BANK3: TStringField;
    qryAPP700AD_BANK4: TStringField;
    qryAPP700AD_PAY: TStringField;
    qryAPP700IMP_CD1: TStringField;
    qryAPP700IMP_CD2: TStringField;
    qryAPP700IMP_CD3: TStringField;
    qryAPP700IMP_CD4: TStringField;
    qryAPP700IMP_CD5: TStringField;
    qryAPP700IL_NO1: TStringField;
    qryAPP700IL_NO2: TStringField;
    qryAPP700IL_NO3: TStringField;
    qryAPP700IL_NO4: TStringField;
    qryAPP700IL_NO5: TStringField;
    qryAPP700IL_CUR1: TStringField;
    qryAPP700IL_CUR2: TStringField;
    qryAPP700IL_CUR3: TStringField;
    qryAPP700IL_CUR4: TStringField;
    qryAPP700IL_CUR5: TStringField;
    qryAPP700IL_AMT1: TBCDField;
    qryAPP700IL_AMT2: TBCDField;
    qryAPP700IL_AMT3: TBCDField;
    qryAPP700IL_AMT4: TBCDField;
    qryAPP700IL_AMT5: TBCDField;
    qryAPP700AD_INFO1: TStringField;
    qryAPP700AD_INFO2: TStringField;
    qryAPP700AD_INFO3: TStringField;
    qryAPP700AD_INFO4: TStringField;
    qryAPP700AD_INFO5: TStringField;
    qryAPP700EX_NAME1: TStringField;
    qryAPP700EX_NAME2: TStringField;
    qryAPP700EX_NAME3: TStringField;
    qryAPP700EX_ADDR1: TStringField;
    qryAPP700EX_ADDR2: TStringField;
    qryAPP700DOC_CD: TStringField;
    qryAPP700EX_DATE: TStringField;
    qryAPP700EX_PLACE: TStringField;
    qryAPP700APPLIC1: TStringField;
    qryAPP700APPLIC2: TStringField;
    qryAPP700APPLIC3: TStringField;
    qryAPP700APPLIC4: TStringField;
    qryAPP700APPLIC5: TStringField;
    qryAPP700BENEFC: TStringField;
    qryAPP700BENEFC1: TStringField;
    qryAPP700BENEFC2: TStringField;
    qryAPP700BENEFC3: TStringField;
    qryAPP700BENEFC4: TStringField;
    qryAPP700BENEFC5: TStringField;
    qryAPP700CD_AMT: TBCDField;
    qryAPP700CD_CUR: TStringField;
    qryAPP700CD_PERP: TIntegerField;
    qryAPP700CD_PERM: TIntegerField;
    qryAPP700CD_MAX: TStringField;
    qryAPP700TERM_PR: TStringField;
    qryAPP700TERM_PR_M: TStringField;
    qryAPP700PL_TERM: TStringField;
    qryAPP700AA_CV1: TStringField;
    qryAPP700AA_CV2: TStringField;
    qryAPP700AA_CV3: TStringField;
    qryAPP700AA_CV4: TStringField;
    qryAPP700DRAFT1: TStringField;
    qryAPP700DRAFT2: TStringField;
    qryAPP700DRAFT3: TStringField;
    qryAPP700MIX_PAY1: TStringField;
    qryAPP700MIX_PAY2: TStringField;
    qryAPP700MIX_PAY3: TStringField;
    qryAPP700MIX_PAY4: TStringField;
    qryAPP700DEF_PAY1: TStringField;
    qryAPP700DEF_PAY2: TStringField;
    qryAPP700DEF_PAY3: TStringField;
    qryAPP700DEF_PAY4: TStringField;
    qryAPP700DESGOOD: TStringField;
    qryAPP700DESGOOD_1: TMemoField;
    qryAPP700LST_DATE: TStringField;
    qryAPP700SHIP_PD1: TStringField;
    qryAPP700SHIP_PD2: TStringField;
    qryAPP700SHIP_PD3: TStringField;
    qryAPP700SHIP_PD4: TStringField;
    qryAPP700SHIP_PD5: TStringField;
    qryAPP700SHIP_PD6: TStringField;
    qryAPP700PSHIP: TStringField;
    qryAPP700TSHIP: TStringField;
    qryAPP700CARRIAGE: TStringField;
    qryAPP700SUNJUK_PORT: TStringField;
    qryAPP700DOCHAK_PORT: TStringField;
    qryAPP700LOAD_ON: TStringField;
    qryAPP700FOR_TRAN: TStringField;
    qryAPP700ORIGIN: TStringField;
    qryAPP700ORIGIN_M: TStringField;
    qryAPP700DOC_380: TStringField;
    qryAPP700DOC_380_1: TIntegerField;
    qryAPP700DOC_705_YN: TStringField;
    qryAPP700DOC_705_GUBUN: TStringField;
    qryAPP700DOC_705_1: TStringField;
    qryAPP700DOC_705_2: TStringField;
    qryAPP700DOC_705_3: TStringField;
    qryAPP700DOC_705_4: TStringField;
    qryAPP700DOC_740: TStringField;
    qryAPP700DOC_740_1: TStringField;
    qryAPP700DOC_740_2: TStringField;
    qryAPP700DOC_740_3: TStringField;
    qryAPP700DOC_740_4: TStringField;
    qryAPP700DOC_760: TStringField;
    qryAPP700DOC_760_1: TStringField;
    qryAPP700DOC_760_2: TStringField;
    qryAPP700DOC_760_3: TStringField;
    qryAPP700DOC_760_4: TStringField;
    qryAPP700DOC_530: TStringField;
    qryAPP700DOC_530_1: TStringField;
    qryAPP700DOC_530_2: TStringField;
    qryAPP700DOC_271: TStringField;
    qryAPP700DOC_271_1: TIntegerField;
    qryAPP700DOC_861: TStringField;
    qryAPP700DOC_2AA: TStringField;
    qryAPP700DOC_2AA_1: TMemoField;
    qryAPP700ACD_2AA: TStringField;
    qryAPP700ACD_2AA_1: TStringField;
    qryAPP700ACD_2AB: TStringField;
    qryAPP700ACD_2AC: TStringField;
    qryAPP700ACD_2AD: TStringField;
    qryAPP700ACD_2AE: TStringField;
    qryAPP700ACD_2AE_1: TMemoField;
    qryAPP700CHARGE: TStringField;
    qryAPP700PERIOD: TIntegerField;
    qryAPP700CONFIRMM: TStringField;
    sPanel2: TsPanel;
    sPanel4: TsPanel;
    sButton6: TsButton;
    sPanel5: TsPanel;
    sDBGrid2: TsDBGrid;
    sPanel6: TsPanel;
    sComboBox1: TsComboBox;
    edtINF700: TsEdit;
    sButton8: TsButton;
    sPanel7: TsPanel;
    sButton9: TsButton;
    sPanel8: TsPanel;
    sDBGrid3: TsDBGrid;
    qryAPP707: TADOQuery;
    dsAPP707: TDataSource;
    sPanel9: TsPanel;
    sButton12: TsButton;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    sComboBox2: TsComboBox;
    edtINF707: TsEdit;
    sButton14: TsButton;
    sDBGrid4: TsDBGrid;
    sPanel12: TsPanel;
    sButton15: TsButton;
    sPanel13: TsPanel;
    sPanel14: TsPanel;
    sComboBox3: TsComboBox;
    edtDOANTC_DATE: TsEdit;
    sButton17: TsButton;
    sDBGrid5: TsDBGrid;
    qryDOANTC: TADOQuery;
    dsDOANTC: TDataSource;
    sButton4: TsButton;
    sButton5: TsButton;
    sSpeedButton4: TsSpeedButton;
    qryAPP707CO_GBCD: TStringField;
    qryAPP707IMPTNO: TStringField;
    qryAPP707IMPTSQ: TIntegerField;
    qryAPP707AMNDSQ: TIntegerField;
    qryAPP707MESSAGE1: TStringField;
    qryAPP707MESSAGE2: TStringField;
    qryAPP707CHK2: TStringField;
    qryAPP707CHK3: TStringField;
    qryAPP707DATEE: TStringField;
    qryAPP707APP_DATE: TStringField;
    qryAPP707IN_MATHOD: TStringField;
    qryAPP707AP_BANK: TStringField;
    qryAPP707AP_BANK1: TStringField;
    qryAPP707AP_BANK2: TStringField;
    qryAPP707AP_BANK3: TStringField;
    qryAPP707AP_BANK4: TStringField;
    qryAPP707AP_BANK5: TStringField;
    qryAPP707AD_BANK: TStringField;
    qryAPP707AD_BANK1: TStringField;
    qryAPP707AD_BANK2: TStringField;
    qryAPP707AD_BANK3: TStringField;
    qryAPP707AD_BANK4: TStringField;
    qryAPP707AD_PAY: TStringField;
    qryAPP707IMP_CD1: TStringField;
    qryAPP707IMP_CD2: TStringField;
    qryAPP707IMP_CD3: TStringField;
    qryAPP707IMP_CD4: TStringField;
    qryAPP707IMP_CD5: TStringField;
    qryAPP707IL_NO1: TStringField;
    qryAPP707IL_NO2: TStringField;
    qryAPP707IL_NO3: TStringField;
    qryAPP707IL_NO4: TStringField;
    qryAPP707IL_NO5: TStringField;
    qryAPP707IL_AMT1: TBCDField;
    qryAPP707IL_AMT2: TBCDField;
    qryAPP707IL_AMT3: TBCDField;
    qryAPP707IL_AMT4: TBCDField;
    qryAPP707IL_AMT5: TBCDField;
    qryAPP707IL_CUR1: TStringField;
    qryAPP707IL_CUR2: TStringField;
    qryAPP707IL_CUR3: TStringField;
    qryAPP707IL_CUR4: TStringField;
    qryAPP707IL_CUR5: TStringField;
    qryAPP707EX_NAME1: TStringField;
    qryAPP707EX_NAME2: TStringField;
    qryAPP707EX_NAME3: TStringField;
    qryAPP707AD_INFO1: TStringField;
    qryAPP707AD_INFO2: TStringField;
    qryAPP707AD_INFO3: TStringField;
    qryAPP707AD_INFO4: TStringField;
    qryAPP707AD_INFO5: TStringField;
    qryAPP707CD_NO: TStringField;
    qryAPP707ISS_DATE: TStringField;
    qryAPP707APPLIC1: TStringField;
    qryAPP707APPLIC2: TStringField;
    qryAPP707APPLIC3: TStringField;
    qryAPP707APPLIC4: TStringField;
    qryAPP707APPLIC5: TStringField;
    qryAPP707BENEFC: TStringField;
    qryAPP707BENEFC1: TStringField;
    qryAPP707BENEFC2: TStringField;
    qryAPP707BENEFC3: TStringField;
    qryAPP707BENEFC4: TStringField;
    qryAPP707BENEFC5: TStringField;
    qryAPP707EX_DATE: TStringField;
    qryAPP707INCD_CUR: TStringField;
    qryAPP707INCD_AMT: TBCDField;
    qryAPP707DECD_CUR: TStringField;
    qryAPP707DECD_AMT: TBCDField;
    qryAPP707NWCD_CUR: TStringField;
    qryAPP707NWCD_AMT: TBCDField;
    qryAPP707BFCD_CUR: TStringField;
    qryAPP707BFCD_AMT: TBCDField;
    qryAPP707CD_PERP: TIntegerField;
    qryAPP707CD_PERM: TIntegerField;
    qryAPP707CD_MAX: TStringField;
    qryAPP707AA_CV1: TStringField;
    qryAPP707AA_CV2: TStringField;
    qryAPP707AA_CV3: TStringField;
    qryAPP707AA_CV4: TStringField;
    qryAPP707SUNJUCK_PORT: TStringField;
    qryAPP707DOCHAK_PORT: TStringField;
    qryAPP707LOAD_ON: TStringField;
    qryAPP707FOR_TRAN: TStringField;
    qryAPP707LST_DATE: TStringField;
    qryAPP707SHIP_PD1: TStringField;
    qryAPP707SHIP_PD2: TStringField;
    qryAPP707SHIP_PD3: TStringField;
    qryAPP707SHIP_PD4: TStringField;
    qryAPP707SHIP_PD5: TStringField;
    qryAPP707SHIP_PD6: TStringField;
    qryAPP707CARRIAGE: TStringField;
    qryAPP707NARRAT: TStringField;
    qryAPP707NARRAT_1: TMemoField;
    sButton16: TsButton;
    sSpeedButton3: TsSpeedButton;
    qryDOANTCMAINT_NO: TStringField;
    qryDOANTCUSER_ID: TStringField;
    qryDOANTCDATEE: TStringField;
    qryDOANTCMESSAGE1: TStringField;
    qryDOANTCMESSAGE2: TStringField;
    qryDOANTCAPP_CODE: TStringField;
    qryDOANTCAPP_NAME1: TStringField;
    qryDOANTCAPP_NAME2: TStringField;
    qryDOANTCAPP_NAME3: TStringField;
    qryDOANTCBANK_CODE: TStringField;
    qryDOANTCBANK1: TStringField;
    qryDOANTCBANK2: TStringField;
    qryDOANTCLC_G: TStringField;
    qryDOANTCLC_NO: TStringField;
    qryDOANTCBL_G: TStringField;
    qryDOANTCBL_NO: TStringField;
    qryDOANTCAMT: TBCDField;
    qryDOANTCAMTC: TStringField;
    qryDOANTCCHRG: TBCDField;
    qryDOANTCCHRGC: TStringField;
    qryDOANTCRES_DATE: TStringField;
    qryDOANTCSET_DATE: TStringField;
    qryDOANTCREMARK1: TMemoField;
    qryDOANTCBK_NAME1: TStringField;
    qryDOANTCBK_NAME2: TStringField;
    qryDOANTCBK_NAME3: TStringField;
    qryDOANTCCHK1: TBooleanField;
    qryDOANTCCHK2: TStringField;
    qryDOANTCCHK3: TStringField;
    qryDOANTCPRNO: TIntegerField;
    qryINF707: TADOQuery;
    dsINF707: TDataSource;
    qryINF707MAINT_NO: TStringField;
    qryINF707MSEQ: TIntegerField;
    qryINF707AMD_NO: TIntegerField;
    qryINF707MESSAGE1: TStringField;
    qryINF707MESSAGE2: TStringField;
    qryINF707USER_ID: TStringField;
    qryINF707DATEE: TStringField;
    qryINF707APP_DATE: TStringField;
    qryINF707IN_MATHOD: TStringField;
    qryINF707AP_BANK: TStringField;
    qryINF707AP_BANK1: TStringField;
    qryINF707AP_BANK2: TStringField;
    qryINF707AP_BANK3: TStringField;
    qryINF707AP_BANK4: TStringField;
    qryINF707AP_BANK5: TStringField;
    qryINF707AD_BANK: TStringField;
    qryINF707AD_BANK1: TStringField;
    qryINF707AD_BANK2: TStringField;
    qryINF707AD_BANK3: TStringField;
    qryINF707AD_BANK4: TStringField;
    qryINF707IL_NO1: TStringField;
    qryINF707IL_NO2: TStringField;
    qryINF707IL_NO3: TStringField;
    qryINF707IL_NO4: TStringField;
    qryINF707IL_NO5: TStringField;
    qryINF707IL_AMT1: TBCDField;
    qryINF707IL_AMT2: TBCDField;
    qryINF707IL_AMT3: TBCDField;
    qryINF707IL_AMT4: TBCDField;
    qryINF707IL_AMT5: TBCDField;
    qryINF707IL_CUR1: TStringField;
    qryINF707IL_CUR2: TStringField;
    qryINF707IL_CUR3: TStringField;
    qryINF707IL_CUR4: TStringField;
    qryINF707IL_CUR5: TStringField;
    qryINF707AD_INFO1: TStringField;
    qryINF707AD_INFO2: TStringField;
    qryINF707AD_INFO3: TStringField;
    qryINF707AD_INFO4: TStringField;
    qryINF707AD_INFO5: TStringField;
    qryINF707CD_NO: TStringField;
    qryINF707RCV_REF: TStringField;
    qryINF707IBANK_REF: TStringField;
    qryINF707ISS_BANK1: TStringField;
    qryINF707ISS_BANK2: TStringField;
    qryINF707ISS_BANK3: TStringField;
    qryINF707ISS_BANK4: TStringField;
    qryINF707ISS_BANK5: TStringField;
    qryINF707ISS_ACCNT: TStringField;
    qryINF707ISS_DATE: TStringField;
    qryINF707AMD_DATE: TStringField;
    qryINF707EX_DATE: TStringField;
    qryINF707EX_PLACE: TStringField;
    qryINF707CHK1: TStringField;
    qryINF707CHK2: TStringField;
    qryINF707CHK3: TStringField;
    qryINF707F_INTERFACE: TStringField;
    qryINF707IMP_CD1: TStringField;
    qryINF707IMP_CD2: TStringField;
    qryINF707IMP_CD3: TStringField;
    qryINF707IMP_CD4: TStringField;
    qryINF707IMP_CD5: TStringField;
    qryINF707Prno: TIntegerField;
    qryINF707APPLIC1: TStringField;
    qryINF707APPLIC2: TStringField;
    qryINF707APPLIC3: TStringField;
    qryINF707APPLIC4: TStringField;
    qryINF707APPLIC5: TStringField;
    qryINF707BENEFC1: TStringField;
    qryINF707BENEFC2: TStringField;
    qryINF707BENEFC3: TStringField;
    qryINF707BENEFC4: TStringField;
    qryINF707BENEFC5: TStringField;
    qryINF707INCD_CUR: TStringField;
    qryINF707INCD_AMT: TBCDField;
    qryINF707DECD_CUR: TStringField;
    qryINF707DECD_AMT: TBCDField;
    qryINF707NWCD_CUR: TStringField;
    qryINF707NWCD_AMT: TBCDField;
    qryINF707CD_PERP: TBCDField;
    qryINF707CD_PERM: TBCDField;
    qryINF707CD_MAX: TStringField;
    qryINF707AA_CV1: TStringField;
    qryINF707AA_CV2: TStringField;
    qryINF707AA_CV3: TStringField;
    qryINF707AA_CV4: TStringField;
    qryINF707LOAD_ON: TStringField;
    qryINF707FOR_TRAN: TStringField;
    qryINF707LST_DATE: TStringField;
    qryINF707SHIP_PD: TBooleanField;
    qryINF707SHIP_PD1: TStringField;
    qryINF707SHIP_PD2: TStringField;
    qryINF707SHIP_PD3: TStringField;
    qryINF707SHIP_PD4: TStringField;
    qryINF707SHIP_PD5: TStringField;
    qryINF707SHIP_PD6: TStringField;
    qryINF707NARRAT: TBooleanField;
    qryINF707NARRAT_1: TMemoField;
    qryINF707SR_INFO1: TStringField;
    qryINF707SR_INFO2: TStringField;
    qryINF707SR_INFO3: TStringField;
    qryINF707SR_INFO4: TStringField;
    qryINF707SR_INFO5: TStringField;
    qryINF707SR_INFO6: TStringField;
    qryINF707EX_NAME1: TStringField;
    qryINF707EX_NAME2: TStringField;
    qryINF707EX_NAME3: TStringField;
    qryINF707EX_ADDR1: TStringField;
    qryINF707EX_ADDR2: TStringField;
    qryINF707OP_BANK1: TStringField;
    qryINF707OP_BANK2: TStringField;
    qryINF707OP_BANK3: TStringField;
    qryINF707OP_ADDR1: TStringField;
    qryINF707OP_ADDR2: TStringField;
    qryINF707BFCD_AMT: TBCDField;
    qryINF707BFCD_CUR: TStringField;
    qryINF707SUNJUCK_PORT: TStringField;
    qryINF707DOCHACK_PORT: TStringField;
    mskINF707: TsMaskEdit;
    mskDOANTC_DATE: TsMaskEdit;
    qryINF700: TADOQuery;
    dsINF700: TDataSource;
    qryINF700MAINT_NO: TStringField;
    qryINF700MESSAGE1: TStringField;
    qryINF700MESSAGE2: TStringField;
    qryINF700USER_ID: TStringField;
    qryINF700DATEE: TStringField;
    qryINF700APP_DATE: TStringField;
    qryINF700IN_MATHOD: TStringField;
    qryINF700AP_BANK: TStringField;
    qryINF700AP_BANK1: TStringField;
    qryINF700AP_BANK2: TStringField;
    qryINF700AP_BANK3: TStringField;
    qryINF700AP_BANK4: TStringField;
    qryINF700AP_BANK5: TStringField;
    qryINF700AD_BANK: TStringField;
    qryINF700AD_BANK1: TStringField;
    qryINF700AD_BANK2: TStringField;
    qryINF700AD_BANK3: TStringField;
    qryINF700AD_BANK4: TStringField;
    qryINF700AD_PAY: TStringField;
    qryINF700IL_NO1: TStringField;
    qryINF700IL_NO2: TStringField;
    qryINF700IL_NO3: TStringField;
    qryINF700IL_NO4: TStringField;
    qryINF700IL_NO5: TStringField;
    qryINF700IL_AMT1: TBCDField;
    qryINF700IL_AMT2: TBCDField;
    qryINF700IL_AMT3: TBCDField;
    qryINF700IL_AMT4: TBCDField;
    qryINF700IL_AMT5: TBCDField;
    qryINF700IL_CUR1: TStringField;
    qryINF700IL_CUR2: TStringField;
    qryINF700IL_CUR3: TStringField;
    qryINF700IL_CUR4: TStringField;
    qryINF700IL_CUR5: TStringField;
    qryINF700AD_INFO1: TStringField;
    qryINF700AD_INFO2: TStringField;
    qryINF700AD_INFO3: TStringField;
    qryINF700AD_INFO4: TStringField;
    qryINF700AD_INFO5: TStringField;
    qryINF700DOC_CD: TStringField;
    qryINF700CD_NO: TStringField;
    qryINF700REF_PRE: TStringField;
    qryINF700ISS_DATE: TStringField;
    qryINF700EX_DATE: TStringField;
    qryINF700EX_PLACE: TStringField;
    qryINF700CHK1: TStringField;
    qryINF700CHK2: TStringField;
    qryINF700CHK3: TStringField;
    qryINF700prno: TIntegerField;
    qryINF700F_INTERFACE: TStringField;
    qryINF700IMP_CD1: TStringField;
    qryINF700IMP_CD2: TStringField;
    qryINF700IMP_CD3: TStringField;
    qryINF700IMP_CD4: TStringField;
    qryINF700IMP_CD5: TStringField;
    qryINF700MAINT_NO_1: TStringField;
    qryINF700APP_BANK: TStringField;
    qryINF700APP_BANK1: TStringField;
    qryINF700APP_BANK2: TStringField;
    qryINF700APP_BANK3: TStringField;
    qryINF700APP_BANK4: TStringField;
    qryINF700APP_BANK5: TStringField;
    qryINF700APP_ACCNT: TStringField;
    qryINF700APPLIC1: TStringField;
    qryINF700APPLIC2: TStringField;
    qryINF700APPLIC3: TStringField;
    qryINF700APPLIC4: TStringField;
    qryINF700APPLIC5: TStringField;
    qryINF700BENEFC1: TStringField;
    qryINF700BENEFC2: TStringField;
    qryINF700BENEFC3: TStringField;
    qryINF700BENEFC4: TStringField;
    qryINF700BENEFC5: TStringField;
    qryINF700CD_AMT: TBCDField;
    qryINF700CD_CUR: TStringField;
    qryINF700CD_PERP: TBCDField;
    qryINF700CD_PERM: TBCDField;
    qryINF700CD_MAX: TStringField;
    qryINF700AA_CV1: TStringField;
    qryINF700AA_CV2: TStringField;
    qryINF700AA_CV3: TStringField;
    qryINF700AA_CV4: TStringField;
    qryINF700AVAIL: TStringField;
    qryINF700AVAIL1: TStringField;
    qryINF700AVAIL2: TStringField;
    qryINF700AVAIL3: TStringField;
    qryINF700AVAIL4: TStringField;
    qryINF700AV_ACCNT: TStringField;
    qryINF700AV_PAY: TStringField;
    qryINF700DRAFT1: TStringField;
    qryINF700DRAFT2: TStringField;
    qryINF700DRAFT3: TStringField;
    qryINF700DRAWEE: TStringField;
    qryINF700DRAWEE1: TStringField;
    qryINF700DRAWEE2: TStringField;
    qryINF700DRAWEE3: TStringField;
    qryINF700DRAWEE4: TStringField;
    qryINF700DR_ACCNT: TStringField;
    qryINF700MAINT_NO_2: TStringField;
    qryINF700PSHIP: TStringField;
    qryINF700TSHIP: TStringField;
    qryINF700LOAD_ON: TStringField;
    qryINF700FOR_TRAN: TStringField;
    qryINF700LST_DATE: TStringField;
    qryINF700SHIP_PD: TBooleanField;
    qryINF700SHIP_PD1: TStringField;
    qryINF700SHIP_PD2: TStringField;
    qryINF700SHIP_PD3: TStringField;
    qryINF700SHIP_PD4: TStringField;
    qryINF700SHIP_PD5: TStringField;
    qryINF700SHIP_PD6: TStringField;
    qryINF700DESGOOD: TBooleanField;
    qryINF700DESGOOD_1: TMemoField;
    qryINF700TERM_PR: TStringField;
    qryINF700TERM_PR_M: TStringField;
    qryINF700PL_TERM: TStringField;
    qryINF700ORIGIN: TStringField;
    qryINF700ORIGIN_M: TStringField;
    qryINF700DOC_380: TBooleanField;
    qryINF700DOC_380_1: TBCDField;
    qryINF700DOC_705: TBooleanField;
    qryINF700DOC_705_1: TStringField;
    qryINF700DOC_705_2: TStringField;
    qryINF700DOC_705_3: TStringField;
    qryINF700DOC_705_4: TStringField;
    qryINF700DOC_740: TBooleanField;
    qryINF700DOC_740_1: TStringField;
    qryINF700DOC_740_2: TStringField;
    qryINF700DOC_740_3: TStringField;
    qryINF700DOC_740_4: TStringField;
    qryINF700DOC_530: TBooleanField;
    qryINF700DOC_530_1: TStringField;
    qryINF700DOC_530_2: TStringField;
    qryINF700DOC_271: TBooleanField;
    qryINF700DOC_271_1: TBCDField;
    qryINF700DOC_861: TBooleanField;
    qryINF700DOC_2AA: TBooleanField;
    qryINF700DOC_2AA_1: TMemoField;
    qryINF700ACD_2AA: TBooleanField;
    qryINF700ACD_2AA_1: TStringField;
    qryINF700ACD_2AB: TBooleanField;
    qryINF700ACD_2AC: TBooleanField;
    qryINF700ACD_2AD: TBooleanField;
    qryINF700ACD_2AE: TBooleanField;
    qryINF700ACD_2AE_1: TMemoField;
    qryINF700CHARGE: TStringField;
    qryINF700PERIOD: TBCDField;
    qryINF700CONFIRMM: TStringField;
    qryINF700DEF_PAY1: TStringField;
    qryINF700DEF_PAY2: TStringField;
    qryINF700DEF_PAY3: TStringField;
    qryINF700DEF_PAY4: TStringField;
    qryINF700DOC_705_GUBUN: TStringField;
    qryINF700MAINT_NO_3: TStringField;
    qryINF700REI_BANK: TStringField;
    qryINF700REI_BANK1: TStringField;
    qryINF700REI_BANK2: TStringField;
    qryINF700REI_BANK3: TStringField;
    qryINF700REI_BANK4: TStringField;
    qryINF700REI_BANK5: TStringField;
    qryINF700REI_ACNNT: TStringField;
    qryINF700INSTRCT: TBooleanField;
    qryINF700INSTRCT_1: TMemoField;
    qryINF700AVT_BANK: TStringField;
    qryINF700AVT_BANK1: TStringField;
    qryINF700AVT_BANK2: TStringField;
    qryINF700AVT_BANK3: TStringField;
    qryINF700AVT_BANK4: TStringField;
    qryINF700AVT_BANK5: TStringField;
    qryINF700AVT_ACCNT: TStringField;
    qryINF700SND_INFO1: TStringField;
    qryINF700SND_INFO2: TStringField;
    qryINF700SND_INFO3: TStringField;
    qryINF700SND_INFO4: TStringField;
    qryINF700SND_INFO5: TStringField;
    qryINF700SND_INFO6: TStringField;
    qryINF700EX_NAME1: TStringField;
    qryINF700EX_NAME2: TStringField;
    qryINF700EX_NAME3: TStringField;
    qryINF700EX_ADDR1: TStringField;
    qryINF700EX_ADDR2: TStringField;
    qryINF700EX_ADDR3: TStringField;
    qryINF700OP_BANK1: TStringField;
    qryINF700OP_BANK2: TStringField;
    qryINF700OP_BANK3: TStringField;
    qryINF700OP_ADDR1: TStringField;
    qryINF700OP_ADDR2: TStringField;
    qryINF700OP_ADDR3: TStringField;
    qryINF700MIX_PAY1: TStringField;
    qryINF700MIX_PAY2: TStringField;
    qryINF700MIX_PAY3: TStringField;
    qryINF700MIX_PAY4: TStringField;
    qryINF700APPLICABLE_RULES_1: TStringField;
    qryINF700APPLICABLE_RULES_2: TStringField;
    qryINF700DOC_760: TBooleanField;
    qryINF700DOC_760_1: TStringField;
    qryINF700DOC_760_2: TStringField;
    qryINF700DOC_760_3: TStringField;
    qryINF700DOC_760_4: TStringField;
    qryINF700SUNJUCK_PORT: TStringField;
    qryINF700DOCHACK_PORT: TStringField;
    mskINF700: TsMaskEdit;
    sButton7: TsButton;
    qryAPP707EX_ADDR1: TStringField;
    qryAPP707EX_ADDR2: TStringField;
    sButton10: TsButton;
    qryTest: TADOQuery;
    dsTest: TDataSource;
    sButton11: TsButton;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    ADOQuery2: TADOQuery;
    DataSource2: TDataSource;
    sSkinProvider1: TsSkinProvider;
    sButton2: TsButton;
    sButton13: TsButton;
    procedure sButton3Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
    procedure sComboBox3Select(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sButton16Click(Sender: TObject);
    procedure sComboBox2Select(Sender: TObject);
    procedure sComboBox1Select(Sender: TObject);
    procedure sButton7Click(Sender: TObject);
    procedure sButton10Click(Sender: TObject);
    procedure sButton13Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    //------------------------------------------------------------------------------
    // ERP->EDI부분
    //------------------------------------------------------------------------------
    procedure APP700_ReadList;
    procedure APP700_getData;
    procedure APP707_ReadList;
    procedure APP707_getData;
    function CompareBANKCD(sERP_BANK_CD : String):String;
    //------------------------------------------------------------------------------
    // EDI->ERP부분
    //------------------------------------------------------------------------------
    procedure DOANTC_ReadList;
    procedure DOANTC_upload;
    procedure INF700_ReadList;
    procedure INF700_upload;
    procedure INF707_ReadList;
    procedure INF707_upload;

    //--------------------------------------------------------------------------
    // 기타 함수
    //--------------------------------------------------------------------------
    function  getCodeContent(sPrefix, sCode : String):String;
    procedure AddLog(obj, sData : String);
  public
    { Public declarations }
  end;

var
  LivartData_frm: TLivartData_frm;

const
  UPLOAD_SID : String = 'LDF_KIS';
  SYSDATE : string = 'SYSDATE';
implementation

uses
  ICON, LivartConfig, MSSQL, SQLCreator, VarDefine, dlg_ERPKIS_BK_CONN,
  UI_test;

{$R *.dfm}

{ TLivartData_frm }

procedure TLivartData_frm.APP700_ReadList;
begin
  qryAPP700.Close;
  qryAPP700.Open;
end;

procedure TLivartData_frm.sButton3Click(Sender: TObject);
begin
  Case sPageControl1.ActivePageIndex of
    0: APP700_ReadList;
    1: INF700_ReadList;
    2: APP707_ReadList;
    3: INF707_ReadList;
    4: DOANTC_ReadList;
  end;
end;

procedure TLivartData_frm.sButton2Click(Sender: TObject);
begin
  LivartConfig_frm.ShowModal;
end;

procedure TLivartData_frm.FormShow(Sender: TObject);
begin
  LivartConfig_frm := TLivartConfig_frm.Create(Self);
  LivartConfig_frm.ReadInfo;
  LivartConfig_frm.LivartConn.ConnectionString := LivartConfig_frm.setConnectionString;
//  LivartConfig_frm.LivartConn.Open;
  sPageControl1.ActivePageIndex := 0;
//------------------------------------------------------------------------------
// 초기화
//------------------------------------------------------------------------------
  mskDOANTC_DATE.Text := FormatDateTime('YYYYMMDD',Now);
  mskINF700.Text := FormatDateTime('YYYYMMDD',Now);
  mskINF707.Text := FormatDateTime('YYYYMMDD',Now);

end;

procedure TLivartData_frm.APP700_getData;
var
  SC : TSQLCreate;
  __MAINT_NO : String;
  AP_BANK_STRING : String;
  AD_BANK_STRING : String;
  POS_INDEX : Integer;
  TEMP_STRING : String;
begin
  SC := TSQLCreate.Create;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      //------------------------------------------------------------------------------
      // APP700_1
      //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('APP700_1');
//      SC.ADDValue('MAINT_NO',qryAPP700IMPTNO.AsString);
      //------------------------------------------------------------------------------
      // 2018-02-05
      // 수입품의서번호+_+일련번호
      //------------------------------------------------------------------------------
      __MAINT_NO := qryAPP700IMPTNO.AsString+'_'+qryAPP700IMPTSQ.AsString;
      SC.ADDValue('MAINT_NO', __MAINT_NO);

//      SC.ADDValue('MESSAGE1',qryAPP700MESSAGE1.AsString);
//      SC.ADDValue('MESSAGE2',qryAPP700MESSAGE2.AsString);
      SC.ADDValue('MESSAGE1','9');
      SC.ADDValue('MESSAGE2','AB');
      SC.ADDValue('USER_ID',LoginData.sID);
      SC.ADDValue('DATEE',qryAPP700DATEE.AsString);
      SC.ADDValue('APP_DATE',qryAPP700APP_DATE.AsString);
      SC.ADDValue('IN_MATHOD',qryAPP700IN_MATHOD.AsString);
      //------------------------------------------------------------------------------
      // 2017-12-18
      // 코드는 그냥 사용
      // AP_BANK1를 사용, 자릿수 잘라내기
      //------------------------------------------------------------------------------
//      SC.ADDValue('AP_BANK',qryAPP700AP_BANK.AsString);
      //------------------------------------------------------------------------------
      // 2018-02-06
      // 연계코드 확인하여 AP_BANK CODE 넣어주기
      //------------------------------------------------------------------------------
//      SC.ADDValue('AP_BANK',LeftStr(qryAPP700AP_BANK.AsString,4));
      SC.ADDValue('AP_BANK',CompareBANKCD(qryAPP700AP_BANK.AsString));
      AP_BANK_STRING := qryAPP700AP_BANK1.AsString;
      SC.ADDValue('AP_BANK1',LeftStr(AP_BANK_STRING,35));
      SC.ADDValue('AP_BANK2',MidStr(AP_BANK_STRING,36,35));
//      SC.ADDValue('AP_BANK1',qryAPP700AP_BANK1.AsString);
//      SC.ADDValue('AP_BANK2',qryAPP700AP_BANK2.AsString);
      AP_BANK_STRING := qryAPP700AP_BANK3.AsString;
      SC.ADDValue('AP_BANK3',LeftStr(AP_BANK_STRING,35));
      SC.ADDValue('AP_BANK4',MidStr(AP_BANK_STRING,36,35));
//      SC.ADDValue('AP_BANK3',qryAPP700AP_BANK3.AsString);
//      SC.ADDValue('AP_BANK4',qryAPP700AP_BANK4.AsString);
//      SC.ADDValue('AP_BANK5',qryAPP700AP_BANK5.AsString);

      SC.ADDValue('AD_BANK',qryAPP700AD_BANK.AsString);
     //------------------------------------------------------------------------------
     // 2017-12-17
     // 통지은행 하이픈으로 은행명/지점명을 구분합니다
     //------------------------------------------------------------------------------
      AD_BANK_STRING := qryAPP700AD_BANK1.asString;
      POS_INDEX := POS('-', AD_BANK_STRING);
      IF POS_INDEX > 0 Then
      begin
        TEMP_STRING := LEFTSTR(AD_BANK_STRING,POS_INDEX-1);
        SC.ADDValue('AD_BANK1',LeftStr(TEMP_STRING,35));
        SC.ADDValue('AD_BANK2',MidStr(TEMP_STRING,36,35));
        TEMP_STRING := MIDSTR(AD_BANK_STRING,POS_INDEX+1,100);
        SC.ADDValue('AD_BANK3',LeftStr(TEMP_STRING,35));
        SC.ADDValue('AD_BANK4',MidStr(TEMP_STRING,36,35));
      end
      else
      begin
        TEMP_STRING := qryAPP700AD_BANK1.AsString;
        SC.ADDValue('AD_BANK1',LeftStr(TEMP_STRING,35));
        SC.ADDValue('AD_BANK2',MidStr(TEMP_STRING,36,35));
        TEMP_STRING := qryAPP700AD_BANK3.AsString;
        SC.ADDValue('AD_BANK3',LeftStr(TEMP_STRING, 35));
        SC.ADDValue('AD_BANK4',MidStr(TEMP_STRING,36,35));
      end;
      SC.ADDValue('AD_PAY',qryAPP700AD_PAY.AsString);
      SC.ADDValue('IL_NO1',qryAPP700IL_NO1.AsString);
      SC.ADDValue('IL_NO2',qryAPP700IL_NO2.AsString);
      SC.ADDValue('IL_NO3',qryAPP700IL_NO3.AsString);
      SC.ADDValue('IL_NO4',qryAPP700IL_NO4.AsString);
      SC.ADDValue('IL_NO5',qryAPP700IL_NO5.AsString);
      SC.ADDValue('IL_AMT1',qryAPP700IL_AMT1.AsString,vtInteger);
      SC.ADDValue('IL_AMT2',qryAPP700IL_AMT2.AsString,vtInteger);
      SC.ADDValue('IL_AMT3',qryAPP700IL_AMT3.AsString,vtInteger);
      SC.ADDValue('IL_AMT4',qryAPP700IL_AMT4.AsString,vtInteger);
      SC.ADDValue('IL_AMT5',qryAPP700IL_AMT5.AsString,vtInteger);
      SC.ADDValue('IL_CUR1',qryAPP700IL_CUR1.AsString);
      SC.ADDValue('IL_CUR2',qryAPP700IL_CUR2.AsString);
      SC.ADDValue('IL_CUR3',qryAPP700IL_CUR3.AsString);
      SC.ADDValue('IL_CUR4',qryAPP700IL_CUR4.AsString);
      SC.ADDValue('IL_CUR5',qryAPP700IL_CUR5.AsString);
      SC.ADDValue('AD_INFO1',qryAPP700AD_INFO1.AsString);
      SC.ADDValue('AD_INFO2',qryAPP700AD_INFO2.AsString);
      SC.ADDValue('AD_INFO3',qryAPP700AD_INFO3.AsString);
      SC.ADDValue('AD_INFO4',qryAPP700AD_INFO4.AsString);
      SC.ADDValue('AD_INFO5',qryAPP700AD_INFO5.AsString);
      SC.ADDValue('DOC_CD',qryAPP700DOC_CD.AsString);
      SC.ADDValue('EX_DATE',qryAPP700EX_DATE.AsString);
      SC.ADDValue('EX_PLACE',qryAPP700EX_PLACE.AsString);
      SC.ADDValue('CHK1','0',vtInteger);    //Boolean
      SC.ADDValue('CHK2','0',vtInteger);
      SC.ADDValue('CHK3', 'NULL', vtVariant);
      SC.ADDValue('prno', '0', vtInteger);
      SC.ADDValue('F_INTERFACE', 'NULL', vtVariant);
      SC.ADDValue('IMP_CD1',qryAPP700IMP_CD1.AsString);
      SC.ADDValue('IMP_CD2',qryAPP700IMP_CD2.AsString);
      SC.ADDValue('IMP_CD3',qryAPP700IMP_CD3.AsString);
      SC.ADDValue('IMP_CD4',qryAPP700IMP_CD4.AsString);
      SC.ADDValue('IMP_CD5',qryAPP700IMP_CD5.AsString);
      SQL.Text := SC.CreateSQL;
      AddLog('APP700_1',SQL.Text);
      ExecSQL;

      //------------------------------------------------------------------------------
      // APP700_2
      //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('APP700_2');
      SC.ADDValue('MAINT_NO', __MAINT_NO);
//      SC.ADDValue('MAINT_NO',TEST_MAINT_NO);
      SC.ADDValue('APPLIC1', qryAPP700APPLIC1.AsString);
      SC.ADDValue('APPLIC2', qryAPP700APPLIC2.AsString);
      SC.ADDValue('APPLIC3', qryAPP700APPLIC3.AsString);
      SC.ADDValue('APPLIC4', qryAPP700APPLIC4.AsString);
      SC.ADDValue('APPLIC5', qryAPP700APPLIC5.AsString);
      SC.ADDValue('BENEFC', qryAPP700BENEFC.AsString);
      SC.ADDValue('BENEFC1', qryAPP700BENEFC1.AsString);
      SC.ADDValue('BENEFC2', qryAPP700BENEFC2.AsString);
      SC.ADDValue('BENEFC3', qryAPP700BENEFC3.AsString);
      SC.ADDValue('BENEFC4', qryAPP700BENEFC4.AsString);
      SC.ADDValue('BENEFC5', qryAPP700BENEFC5.AsString);
      SC.ADDValue('CD_AMT', qryAPP700CD_AMT.AsString,vtInteger);
      SC.ADDValue('CD_CUR', qryAPP700CD_CUR.AsString);
      SC.ADDValue('CD_PERP', qryAPP700CD_PERP.AsString,vtInteger);
      SC.ADDValue('CD_PERM', qryAPP700CD_PERM.AsString,vtInteger);
      SC.ADDValue('CD_MAX', qryAPP700CD_MAX.AsString);
      SC.ADDValue('AA_CV1', qryAPP700AA_CV1.AsString);
      SC.ADDValue('AA_CV2', qryAPP700AA_CV2.AsString);
      SC.ADDValue('AA_CV3', qryAPP700AA_CV3.AsString);
      SC.ADDValue('AA_CV4', qryAPP700AA_CV4.AsString);
      SC.ADDValue('DRAFT1', qryAPP700DRAFT1.AsString);
      SC.ADDValue('DRAFT2', qryAPP700DRAFT2.AsString);
      SC.ADDValue('DRAFT3', qryAPP700DRAFT3.AsString);
      SC.ADDValue('MIX_PAY1', qryAPP700MIX_PAY1.AsString);
      SC.ADDValue('MIX_PAY2', qryAPP700MIX_PAY2.AsString);
      SC.ADDValue('MIX_PAY3', qryAPP700MIX_PAY3.AsString);
      SC.ADDValue('MIX_PAY4', qryAPP700MIX_PAY4.AsString);
      SC.ADDValue('DEF_PAY1', qryAPP700DEF_PAY1.AsString);
      SC.ADDValue('DEF_PAY2', qryAPP700DEF_PAY2.AsString);
      SC.ADDValue('DEF_PAY3', qryAPP700DEF_PAY3.AsString);
      SC.ADDValue('DEF_PAY4', qryAPP700DEF_PAY4.AsString);
      SC.ADDValue('PSHIP', qryAPP700PSHIP.AsString);
      SC.ADDValue('TSHIP', qryAPP700TSHIP.AsString);
      SC.ADDValue('LOAD_ON', qryAPP700LOAD_ON.AsString);
      SC.ADDValue('FOR_TRAN', qryAPP700FOR_TRAN.AsString);
      SC.ADDValue('LST_DATE', qryAPP700LST_DATE.AsString);
      SC.ADDValue('SHIP_PD1', qryAPP700SHIP_PD1.AsString);
      SC.ADDValue('SHIP_PD2', qryAPP700SHIP_PD2.AsString);
      SC.ADDValue('SHIP_PD3', qryAPP700SHIP_PD3.AsString);
      SC.ADDValue('SHIP_PD4', qryAPP700SHIP_PD4.AsString);
      SC.ADDValue('SHIP_PD5', qryAPP700SHIP_PD5.AsString);
      SC.ADDValue('SHIP_PD6', qryAPP700SHIP_PD6.AsString);
      SC.ADDValue('DESGOOD_1', qryAPP700DESGOOD_1.AsString);
      SQL.Text := SC.CreateSQL;
      AddLog('APP700_2',SQL.Text);
      ExecSQL;

      //------------------------------------------------------------------------------
      // APP700_3
      //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('APP700_3');
      SC.ADDValue('MAINT_NO', __MAINT_NO);
//      SC.ADDValue('MAINT_NO',TEST_MAINT_NO);

      IF UpperCase(qryAPP700DOC_380.AsString) = 'Y' Then
        SC.ADDValue('DOC_380', '1')
      else
      IF UpperCase(qryAPP700DOC_380.AsString) = 'N' Then
        SC.ADDValue('DOC_380', '0')
      else
        SC.ADDValue('DOC_380', 'NULL', vtVariant);

      SC.ADDValue('DOC_380_1', qryAPP700DOC_380_1.AsString,vtInteger);

      IF UpperCase( qryAPP700DOC_705_GUBUN.AsString ) = 'Y' then
      begin
        SC.ADDValue('DOC_705_GUBUN', '705');
      end;

      IF UpperCase(qryAPP700DOC_705_GUBUN.AsString) = 'Y' Then
        SC.ADDValue('DOC_705','1',vtInteger)
      else
      IF UpperCase(qryAPP700DOC_705_GUBUN.AsString) = 'N' Then
        SC.ADDValue('DOC_705','0',vtInteger)
      else
        SC.ADDValue('DOC_705', 'NULL', vtVariant);
        
      SC.ADDValue('DOC_705_1', qryAPP700DOC_705_1.AsString);
      SC.ADDValue('DOC_705_2', qryAPP700DOC_705_2.AsString);
      SC.ADDValue('DOC_705_3', qryAPP700DOC_705_3.AsString);
      SC.ADDValue('DOC_705_4', qryAPP700DOC_705_4.AsString);

      IF UpperCase(qryAPP700DOC_740.AsString) = 'Y' Then
        SC.ADDValue('DOC_740','1',vtInteger)
      else
      IF UpperCase(qryAPP700DOC_740.AsString) = 'N' Then
        SC.ADDValue('DOC_740','0',vtInteger)
      else
        SC.ADDValue('DOC_740', 'NULL', vtVariant);
        
      SC.ADDValue('DOC_740_1', qryAPP700DOC_740_1.AsString);
      SC.ADDValue('DOC_740_2', qryAPP700DOC_740_2.AsString);
      SC.ADDValue('DOC_740_3', qryAPP700DOC_740_3.AsString);
      SC.ADDValue('DOC_740_4', qryAPP700DOC_740_4.AsString);

      IF UpperCase(qryAPP700DOC_530.AsString) = 'Y' Then
        SC.ADDValue('DOC_530','1',vtInteger)
      else
      IF UpperCase(qryAPP700DOC_530.AsString) = 'N' Then
        SC.ADDValue('DOC_530','0',vtInteger)
      else
        SC.ADDValue('DOC_530', 'NULL', vtVariant);
      SC.ADDValue('DOC_530_1', qryAPP700DOC_530_1.AsString);
      SC.ADDValue('DOC_530_2', qryAPP700DOC_530_2.AsString);

      IF UpperCase(qryAPP700DOC_271.AsString) = 'Y' Then
        SC.ADDValue('DOC_271','1',vtInteger)
      else
      IF UpperCase(qryAPP700DOC_271.AsString) = 'N' Then
        SC.ADDValue('DOC_271','0',vtInteger)
      else
        SC.ADDValue('DOC_271', 'NULL', vtVariant);
      SC.ADDValue('DOC_271_1', qryAPP700DOC_271_1.AsString,vtInteger);

      IF UpperCase(qryAPP700DOC_861.AsString) = 'Y' Then
        SC.ADDValue('DOC_861','1',vtInteger)
      else
      IF UpperCase(qryAPP700DOC_861.AsString) = 'N' Then
        SC.ADDValue('DOC_861','0',vtInteger)
      else
        SC.ADDValue('DOC_861', 'NULL', vtVariant);

      IF UpperCase(qryAPP700DOC_2AA.AsString) = 'Y' Then
        SC.ADDValue('DOC_2AA','1',vtInteger)
      else
      IF UpperCase(qryAPP700DOC_2AA.AsString) = 'N' Then
        SC.ADDValue('DOC_2AA','0',vtInteger)
      else
        SC.ADDValue('DOC_2AA', 'NULL', vtVariant);
      SC.ADDValue('DOC_2AA_1', qryAPP700DOC_2AA_1.AsString);

      IF UpperCase(qryAPP700ACD_2AA.AsString) = 'Y' Then
        SC.ADDValue('ACD_2AA','1',vtInteger)
      else
      IF UpperCase(qryAPP700ACD_2AA.AsString) = 'N' Then
        SC.ADDValue('ACD_2AA','0',vtInteger)
      else
        SC.ADDValue('ACD_2AA', 'NULL', vtVariant);
      SC.ADDValue('ACD_2AA_1', qryAPP700ACD_2AA_1.AsString);

      IF UpperCase(qryAPP700ACD_2AB.AsString) = 'Y' Then
        SC.ADDValue('ACD_2AB','1',vtInteger)
      else
      IF UpperCase(qryAPP700ACD_2AB.AsString) = 'N' Then
        SC.ADDValue('ACD_2AB','0',vtInteger)
      else
        SC.ADDValue('ACD_2AB', 'NULL', vtVariant);

      IF UpperCase(qryAPP700ACD_2AC.AsString) = 'Y' Then
        SC.ADDValue('ACD_2AC','1',vtInteger)
      else
      IF UpperCase(qryAPP700ACD_2AC.AsString) = 'N' Then
        SC.ADDValue('ACD_2AC','0',vtInteger)
      else
        SC.ADDValue('ACD_2AC', 'NULL', vtVariant);
//      SC.ADDValue('ACD_2AC', qryAPP700ACD_2AC.AsString);

      IF UpperCase(qryAPP700ACD_2AD.AsString) = 'Y' Then
        SC.ADDValue('ACD_2AD','1',vtInteger)
      else
      IF UpperCase(qryAPP700ACD_2AD.AsString) = 'N' Then
        SC.ADDValue('ACD_2AD','0',vtInteger)
      else
        SC.ADDValue('ACD_2AD', 'NULL', vtVariant);
//      SC.ADDValue('ACD_2AD', qryAPP700ACD_2AD.AsString);\

      IF UpperCase(qryAPP700ACD_2AE.AsString) = 'Y' Then
        SC.ADDValue('ACD_2AE','1',vtInteger)
      else
      IF UpperCase(qryAPP700ACD_2AE.AsString) = 'N' Then
        SC.ADDValue('ACD_2AE','0',vtInteger)
      else
        SC.ADDValue('ACD_2AE', 'NULL', vtVariant);
      SC.ADDValue('ACD_2AE_1', qryAPP700ACD_2AE_1.AsString);
      SC.ADDValue('CHARGE', qryAPP700CHARGE.AsString);
      SC.ADDValue('PERIOD', qryAPP700PERIOD.AsString,vtInteger);
      SC.ADDValue('CONFIRMM', qryAPP700CONFIRMM.AsString);
//      SC.ADDValue('INSTRCT', qryAPP700
//      SC.ADDValue('INSTRCT_1',
      SC.ADDValue('EX_NAME1', qryAPP700EX_NAME1.AsString);
      SC.ADDValue('EX_NAME2', qryAPP700EX_NAME2.AsString);
      SC.ADDValue('EX_NAME3', qryAPP700EX_NAME3.AsString);
      SC.ADDValue('EX_ADDR1', qryAPP700EX_ADDR1.AsString);
      SC.ADDValue('EX_ADDR2', qryAPP700EX_ADDR1.AsString);
      SC.ADDValue('ORIGIN', qryAPP700ORIGIN.AsString);
      SC.ADDValue('ORIGIN_M', qryAPP700ORIGIN_M.AsString);
      SC.ADDValue('PL_TERM', qryAPP700PL_TERM.AsString);
      SC.ADDValue('TERM_PR', qryAPP700TERM_PR.AsString);
      //------------------------------------------------------------------------
      // 2017-12-19
      // TERM_PR명칭을 찾아서 입력
      //------------------------------------------------------------------------
      SC.ADDValue('TERM_PR_M', getCodeContent('가격조건',qryAPP700TERM_PR.AsString));
//      SC.ADDValue('TERM_PR_M', qryAPP700TERM_PR_M.AsString);
//      SC.ADDValue('SRBUHO', qryAPP700
//      SC.ADDValue('DOC_705_GUBUN', qryAPP700DOC_705_GUBUN.AsString);
      SC.ADDValue('CARRIAGE', qryAPP700CARRIAGE.AsString);

      IF UpperCase(qryAPP700DOC_760.AsString) = 'Y' Then
        SC.ADDValue('DOC_760','1',vtInteger)
      else
      IF UpperCase(qryAPP700DOC_760.AsString) = 'N' Then
        SC.ADDValue('DOC_760','0',vtInteger)
      else
        SC.ADDValue('DOC_760', 'NULL', vtVariant);
      SC.ADDValue('DOC_760_1', qryAPP700DOC_760_1.AsString);
      SC.ADDValue('DOC_760_2', qryAPP700DOC_760_2.AsString);
      SC.ADDValue('DOC_760_3', qryAPP700DOC_760_3.AsString);
      SC.ADDValue('DOC_760_4', qryAPP700DOC_760_4.AsString);
      SC.ADDValue('SUNJUCK_PORT', qryAPP700SUNJUK_PORT.AsString);
      SC.ADDValue('DOCHACK_PORT', qryAPP700DOCHAK_PORT.AsString);
      SQL.Text := SC.CreateSQL;
      AddLog('APP700_3',SQL.Text);
      ExecSQL;

    finally
      Close;
      Free;
      SC.Free;
    end;
  end;
end;

procedure TLivartData_frm.sButton4Click(Sender: TObject);
begin
  DMMssql.BeginTrans;
  try
    Case sPageControl1.ActivePageIndex of
      0: APP700_getData;
      2: APP707_getData;
    end;
    ShowMessage('가져오기가 완료되었습니다');
    DMMssql.CommitTrans;
  except
    on E:Exception do
    begin
      DMMssql.RollbackTrans;
      Case sPageControl1.ActivePageIndex of
        0,2 :
          MessageBox(Self.Handle,PChar('에러가 발생하여 가져오기에 실패했습니다'#13#10'ERROR : '+E.Message),'가져오기 오류',MB_OK+MB_ICONERROR);
      else
          MessageBox(Self.Handle,PChar('에러가 발생하여 재전송에 실패했습니다'#13#10'ERROR : '+E.Message),'가져오기 오류',MB_OK+MB_ICONERROR);
      end;
    end;
  end;

end;

procedure TLivartData_frm.APP707_getData;
var
  SC : TSQLCreate;
  __MAINT_NO : String;
  AD_BANK_TEMP : String;  
begin
  SC := TSQLCreate.Create;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SC.DMLType := dmlInsert;
      SC.SQLHeader('APP707_1');
      // .....................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................
      //------------------------------------------------------------------------------
      // APP707_1
      //------------------------------------------------------------------------------
      __MAINT_NO :=  qryAPP707IMPTNO.AsString+'_'+qryAPP707IMPTSQ.AsString+'__'+qryAPP707AMNDSQ.AsString;
      SC.ADDValue('MAINT_NO', __MAINT_NO);
//      TEST_MAINT_NO := qryAPP707IMPTNO.AsString;
//      TEST_MAINT_NO := TEST_MAINT_NO+'_T'+FormatDateTime('SS',Now);
//      SC.ADDValue('MAINT_NO', TEST_MAINT_NO);
      SC.ADDValue('MSeq', qryAPP707AMNDSQ.AsString,vtInteger);
      SC.ADDValue('AMD_NO', qryAPP707AMNDSQ.AsString,vtInteger);
      SC.ADDValue('MESSAGE1', qryAPP707MESSAGE1.AsString);
      SC.ADDValue('MESSAGE2', qryAPP707MESSAGE2.AsString);
      SC.ADDValue('USER_ID', LoginData.sID);
      SC.ADDValue('DATEE', qryAPP707DATEE.AsString);
//      SC.ADDValue('APP_DATE', qryAPP707APP_DATE.AsString);
      SC.ADDValue('APP_DATE', FormatDateTime('YYYYMMDD',Now));
      SC.ADDValue('IN_MATHOD', qryAPP707IN_MATHOD.AsString);
      SC.ADDValue('AP_BANK', qryAPP707AP_BANK.AsString);
      SC.ADDValue('AP_BANK1', qryAPP707AP_BANK1.AsString);
      SC.ADDValue('AP_BANK2', qryAPP707AP_BANK2.AsString);
      SC.ADDValue('AP_BANK3', qryAPP707AP_BANK3.AsString);
      SC.ADDValue('AP_BANK4', qryAPP707AP_BANK4.AsString);
      SC.ADDValue('AP_BANK5', qryAPP707AP_BANK5.AsString);
      SC.ADDValue('AD_BANK', qryAPP707AD_BANK.AsString);
    //------------------------------------------------------------------------------
    // 2018-02-06
    // AD_BANK 35자리 끊어서 넣기
    //------------------------------------------------------------------------------
      AD_BANK_TEMP := qryAPP707AD_BANK1.AsString;
      SC.ADDValue('AD_BANK1', LeftStr(AD_BANK_TEMP,35));
      SC.ADDValue('AD_BANK2', MidStr(AD_BANK_TEMP,36,35));
      AD_BANK_TEMP := qryAPP707AD_BANK3.AsString;
      SC.ADDValue('AD_BANK3', LeftStr(AD_BANK_TEMP,35));
      SC.ADDValue('AD_BANK4', MidStr(AD_BANK_TEMP,36,35));
//      SC.ADDValue('AD_BANK1', qryAPP707AD_BANK1.AsString);
//      SC.ADDValue('AD_BANK2', qryAPP707AD_BANK2.AsString);
//      SC.ADDValue('AD_BANK3', qryAPP707AD_BANK3.AsString);
//      SC.ADDValue('AD_BANK4', qryAPP707AD_BANK4.AsString);

      SC.ADDValue('AD_PAY', qryAPP707AD_PAY.AsString);
      SC.ADDValue('IL_NO1', qryAPP707IL_NO1.AsString);
      SC.ADDValue('IL_NO2', qryAPP707IL_NO2.AsString);
      SC.ADDValue('IL_NO3', qryAPP707IL_NO3.AsString);
      SC.ADDValue('IL_NO4', qryAPP707IL_NO4.AsString);
      SC.ADDValue('IL_NO5', qryAPP707IL_NO5.AsString);
      SC.ADDValue('IL_AMT1', qryAPP707IL_AMT1.AsString,vtInteger);
      SC.ADDValue('IL_AMT2', qryAPP707IL_AMT2.AsString,vtInteger);
      SC.ADDValue('IL_AMT3', qryAPP707IL_AMT3.AsString,vtInteger);
      SC.ADDValue('IL_AMT4', qryAPP707IL_AMT4.AsString,vtInteger);
      SC.ADDValue('IL_AMT5', qryAPP707IL_AMT5.AsString,vtInteger);
      SC.ADDValue('IL_CUR1', qryAPP707IL_CUR1.AsString);
      SC.ADDValue('IL_CUR2', qryAPP707IL_CUR2.AsString);
      SC.ADDValue('IL_CUR3', qryAPP707IL_CUR3.AsString);
      SC.ADDValue('IL_CUR4', qryAPP707IL_CUR4.AsString);
      SC.ADDValue('IL_CUR5', qryAPP707IL_CUR5.AsString);
      SC.ADDValue('AD_INFO1', qryAPP707AD_INFO1.AsString);
      SC.ADDValue('AD_INFO2', qryAPP707AD_INFO2.AsString);
      SC.ADDValue('AD_INFO3', qryAPP707AD_INFO3.AsString);
      SC.ADDValue('AD_INFO4', qryAPP707AD_INFO4.AsString);
      SC.ADDValue('AD_INFO5', qryAPP707AD_INFO5.AsString);
      SC.ADDValue('CD_NO', qryAPP707CD_NO.AsString);
      SC.ADDValue('ISS_DATE', qryAPP707ISS_DATE.AsString);
      SC.ADDValue('EX_DATE', qryAPP707EX_DATE.AsString);
//      SC.ADDValue('EX_PLACE', qryAPP707EX_PLA
      SC.ADDValue('CHK1','0',vtInteger);    //Boolean
      SC.ADDValue('CHK2','0',vtInteger);
      SC.ADDValue('CHK3', 'NULL', vtVariant);
      SC.ADDValue('prno', '0',vtInteger);
      SC.ADDValue('F_INTERFACE', 'NULL',vtVariant);
      SC.ADDValue('IMP_CD1', qryAPP707IMP_CD1.AsString);
      SC.ADDValue('IMP_CD2', qryAPP707IMP_CD2.AsString);
      SC.ADDValue('IMP_CD3', qryAPP707IMP_CD3.AsString);
      SC.ADDValue('IMP_CD4', qryAPP707IMP_CD4.AsString);
      SC.ADDValue('IMP_CD5', qryAPP707IMP_CD5.AsString);
      SQL.Text := SC.CreateSQL;
      AddLog('APP707_1',SQL.Text);
      ExecSQL;

      SC.SQLHeader('APP707_2');
      //------------------------------------------------------------------------------
      // APP707_2
      //------------------------------------------------------------------------------
//      TEST_MAINT_NO := qryAPP707IMPTNO.AsString;
//      TEST_MAINT_NO := TEST_MAINT_NO+'_T'+FormatDateTime('SS',Now);
//      SC.ADDValue('MAINT_NO', TEST_MAINT_NO);

      SC.ADDValue('MAINT_NO', __MAINT_NO);
      SC.ADDValue('MSeq', qryAPP707AMNDSQ.AsString,vtInteger);
      SC.ADDValue('Amd_No', qryAPP707AMNDSQ.AsString,vtInteger);
      SC.ADDValue('APPLIC1', qryAPP707APPLIC1.AsString);
      SC.ADDValue('APPLIC2', qryAPP707APPLIC2.AsString);
      SC.ADDValue('APPLIC3', qryAPP707APPLIC3.AsString);
      SC.ADDValue('APPLIC4', qryAPP707APPLIC4.AsString);
      SC.ADDValue('APPLIC5', qryAPP707APPLIC5.AsString);
      SC.ADDValue('BENEFC', qryAPP707BENEFC.AsString);
      SC.ADDValue('BENEFC1', qryAPP707BENEFC1.AsString);
      SC.ADDValue('BENEFC2', qryAPP707BENEFC2.AsString);
      SC.ADDValue('BENEFC3', qryAPP707BENEFC3.AsString);
      SC.ADDValue('BENEFC4', qryAPP707BENEFC4.AsString);
      SC.ADDValue('BENEFC5', qryAPP707BENEFC5.AsString);
      SC.ADDValue('INCD_CUR', qryAPP707INCD_CUR.AsString);
      SC.ADDValue('INCD_AMT', qryAPP707INCD_AMT.AsString,vtInteger);
      SC.ADDValue('DECD_CUR', qryAPP707DECD_CUR.AsString);
      SC.ADDValue('DECD_AMT', qryAPP707DECD_AMT.AsString,vtInteger);
      SC.ADDValue('NWCD_CUR', qryAPP707NWCD_CUR.AsString);
      SC.ADDValue('NWCD_AMT', qryAPP707NWCD_AMT.AsString,vtInteger);
      SC.ADDValue('CD_PERP', qryAPP707CD_PERP.AsString,vtInteger);
      SC.ADDValue('CD_PERM', qryAPP707CD_PERM.AsString,vtInteger);
      SC.ADDValue('CD_MAX', qryAPP707CD_MAX.AsString);
      SC.ADDValue('AA_CV1', qryAPP707AA_CV1.AsString);
      SC.ADDValue('AA_CV2', qryAPP707AA_CV2.AsString);
      SC.ADDValue('AA_CV3', qryAPP707AA_CV3.AsString);
      SC.ADDValue('AA_CV4', qryAPP707AA_CV4.AsString);
      SC.ADDValue('LOAD_ON', qryAPP707LOAD_ON.AsString);
      SC.ADDValue('FOR_TRAN', qryAPP707FOR_TRAN.AsString);
      SC.ADDValue('LST_DATE', qryAPP707LST_DATE.AsString);
//      SC.ADDValue('SHIP_PD', qryAPP707SHIP_PD1
      SC.ADDValue('SHIP_PD1', qryAPP707SHIP_PD1.AsString);
      SC.ADDValue('SHIP_PD2', qryAPP707SHIP_PD2.AsString);
      SC.ADDValue('SHIP_PD3', qryAPP707SHIP_PD3.AsString);
      SC.ADDValue('SHIP_PD4', qryAPP707SHIP_PD4.AsString);
      SC.ADDValue('SHIP_PD5', qryAPP707SHIP_PD5.AsString);
      SC.ADDValue('SHIP_PD6', qryAPP707SHIP_PD6.AsString);
      SC.ADDValue('NARRAT', qryAPP707NARRAT.AsString);
      SC.ADDValue('NARRAT_1', qryAPP707NARRAT_1.AsString);
      SC.ADDValue('EX_NAME1', qryAPP707EX_NAME1.AsString);
      SC.ADDValue('EX_NAME2', qryAPP707EX_NAME2.AsString);
      SC.ADDValue('EX_NAME3', qryAPP707EX_NAME3.AsString);
      //추가예정
      SC.ADDValue('EX_ADDR1', qryAPP707EX_ADDR1.AsString);
      SC.ADDValue('EX_ADDR2', qryAPP707EX_ADDR2.AsString);
      //삭제
//      SC.ADDValue('SRBUHO', qryapp707SRBUHO
      SC.ADDValue('BFCD_AMT', qryAPP707BFCD_AMT.AsString,vtInteger);
      SC.ADDValue('BFCD_CUR', qryAPP707BFCD_CUR.AsString);
      SC.ADDValue('CARRIAGE', qryAPP707CARRIAGE.AsString);
      SC.ADDValue('SUNJUCK_PORT', qryAPP707SUNJUCK_PORT.AsString);
      SC.ADDValue('DOCHACK_PORT', qryAPP707DOCHAK_PORT.AsString);
      SQL.Text := SC.CreateSQL;
      AddLog('APP707_2',SQL.Text);
      ExecSQL;
    finally
      Close;
      Free;
      SC.Free;
    end;
  end;
end;

procedure TLivartData_frm.APP707_ReadList;
begin
  qryAPP707.Close;
  qryAPP707.Open;
end;

procedure TLivartData_frm.DOANTC_ReadList;
begin
  with qryDOANTC do
  begin
    Close;
    SQL.Text := 'SELECT * FROM DOANTC';
    Case sComboBox3.ItemIndex of
      //관리번호
      0: SQL.Add( 'WHERE MAINT_NO LIKE '+QuotedStr('%'+edtDOANTC_DATE.Text+'%') );
      //통지일자
      1: SQL.Add( 'WHERE RES_DATE LIKE '+QuotedStr('%'+mskDOANTC_DATE.Text+'%') );
    end;
    Open;
  end;
end;

procedure TLivartData_frm.sComboBox3Select(Sender: TObject);
begin
  mskDOANTC_DATE.Visible := sComboBox3.ItemIndex = 1;
end;

procedure TLivartData_frm.sPageControl1Change(Sender: TObject);
begin
//  ShowMessage(IntToStr(sPageControl1.ActivePageIndex));
  sButton4.Enabled := sPageControl1.ActivePageIndex in [0,2];
  sButton5.Enabled := sButton4.Enabled;
  sButton16.Enabled := not sButton4.Enabled;
end;

procedure TLivartData_frm.sButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TLivartData_frm.DOANTC_upload;
var
  SC : TSQLCreate;
begin
  IF qryDOANTC.RecordCount = 0 Then
  begin
    raise Exception.Create('업로드할 데이터가 없습니다');
  end;

  SC := TSQLCreate.Create;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := LivartConfig_frm.LivartConn;
      SC.DMLType := dmlInsert;
      SC.SQLHeader('TC_KIS_BL_DTL_RCV_IF');

      SC.ADDValue('CO_GBCD', '11');
      SC.ADDValue('MAINT_NO',qryDOANTCMAINT_NO.AsString);
//      SC.ADDValue('IMPTNO', LEFTSTR(qryDOANTCMAINT_NO.AsString,13));
//      SC.ADDValue('IMPTSQ', '1',vtInteger);
      SC.ADDValue('IMPTNO', '');
      SC.ADDValue('IMPTSQ', '');
      SC.ADDValue('USER_ID', LoginData.sID);
      SC.ADDValue('DATEE', qryDOANTCDATEE.AsString);
      SC.ADDValue('MASSAGE1', qryDOANTCMESSAGE1.AsString);
      SC.ADDValue('MESSAGE2', qryDOANTCMESSAGE2.AsString);
      SC.ADDValue('APP_CODE', qryDOANTCAPP_CODE.AsString);
      SC.ADDValue('APP_NAME1', qryDOANTCAPP_NAME1.AsString);
      SC.ADDValue('APP_NAME2', qryDOANTCAPP_NAME2.AsString);
      SC.ADDValue('APP_NAME3', qryDOANTCAPP_NAME3.AsString);
      SC.ADDValue('BANK_CODE', qryDOANTCBANK_CODE.AsString);
      SC.ADDValue('BANK1', qryDOANTCBANK1.AsString);
      SC.ADDValue('BANK2', qryDOANTCBANK2.AsString);
      SC.ADDValue('LC_G', qryDOANTCLC_G.AsString);
      SC.ADDValue('LC_NO', qryDOANTCLC_NO.AsString);
      SC.ADDValue('BL_G', qryDOANTCBL_G.AsString);
      SC.ADDValue('BL_NO', qryDOANTCBL_NO.AsString);
      SC.ADDValue('AMT', qryDOANTCAMT.AsString,vtInteger);
      SC.ADDValue('AMTC', qryDOANTCAMTC.AsString);
      SC.ADDValue('CHRG', qryDOANTCCHRG.AsString,vtInteger);
      SC.ADDValue('CHRGC', qryDOANTCCHRGC.AsString);
      SC.ADDValue('RES_DATE', qryDOANTCRES_DATE.AsString);
      SC.ADDValue('SET_DATE', qryDOANTCSET_DATE.AsString);
      SC.ADDValue('REMARK1', qryDOANTCREMARK1.AsString);
      SC.ADDValue('BK_NAME1', qryDOANTCBK_NAME1.AsString);
      SC.ADDValue('BK_NAME2', qryDOANTCBK_NAME2.AsString);
      SC.ADDValue('BK_NAME3', qryDOANTCBK_NAME3.AsString);

      SC.ADDValue('FRST_REG_USER_ID',UPLOAD_SID);
      SC.ADDValue('FRST_REG_DTM',SYSDATE,vtVariant);
      SC.ADDValue('LAST_MOD_USER_ID',UPLOAD_SID);
      SC.ADDValue('LAST_MOD_DTM',SYSDATE,vtVariant);

      SQL.Text := SC.CreateSQL;
      AddLog('DOANTC',SQL.Text);
      ExecSQL;
    finally
      Close;
      Free;
      SC.Free;
    end;
  end;
end;

procedure TLivartData_frm.sButton16Click(Sender: TObject);
begin
  try
    Case sPageControl1.ActivePageIndex of
      1: INF700_upload;
      3: INF707_upload;
      4: DOANTC_upload;
    end;
  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,Pchar('재전송중 오류가 발생하여 취소되었습니다'#13#10'ERROR : '+E.Message),'재전송오류',MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TLivartData_frm.INF707_ReadList;
begin
  with qryINF707 do
  begin
    Close;
    SQL.Text := 'SELECT * FROM INF707_1 I1 INNER JOIN INF707_2 I2 ON I1.MAINT_NO = I2.MAINT_NO AND I1.MSEQ = I2.MSEQ';
    Case sComboBox2.ItemIndex of
      //관리번호
      0: SQL.Add( 'WHERE I1.MAINT_NO LIKE '+QuotedStr('%'+edtINF707.Text+'%') );
      //통지일자
      1: SQL.Add( 'WHERE APP_DATE LIKE '+QuotedStr('%'+mskINF707.Text+'%') );
    end;
    Open;
  end;
end;

procedure TLivartData_frm.INF707_upload;
var
  SC : TSQLCreate;
  __MAINT_NO : String;
  _POS, __POS : Integer;
begin
  IF qryINF707.RecordCount = 0 Then
  begin
    raise Exception.Create('업로드할 데이터가 없습니다');
  end;

  SC := TSQLCreate.Create;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := LivartConfig_frm.LivartConn;
      SC.DMLType := dmlInsert;
      SC.SQLHeader('TC_KIS_LCRCHR_RCV_IF');

      SC.ADDValue('CO_GBCD','11');
      __MAINT_NO := qryINF707MAINT_NO.AsString;
      _POS := POS('_',__MAINT_NO);
      __POS := POS('__',__MAINT_NO);
      SC.ADDValue('IMPTNO',LeftStr(__MAINT_NO, _POS-1));
      SC.ADDValue('IMPTSQ',MidStr(__MAINT_NO, _POS+1, (__POS-_POS)-1),vtInteger);
      SC.ADDValue('AMNDSQ',qryINF707AMD_NO.AsString,vtInteger);
//      ShowMessage(LeftStr(__MAINT_NO, _POS-1)+#13#10+MidStr(__MAINT_NO, _POS+1, (__POS-_POS)-1)+#13#10+qryINF707AMD_NO.AsString);
//      Exit;
//      SC.ADDValue('IMPTNO',qryINF707MAINT_NO.AsString);
//      SC.ADDValue('IMPTSQ','1',vtInteger);
//      SC.ADDValue('AMNDSQ',qryINF707AMD_NO.AsString);
      SC.ADDValue('USER_ID',LoginData.sID);
      SC.ADDValue('MESSAGE1', qryINF707MESSAGE1.AsString);
      SC.ADDValue('MESSAGE2', qryINF707MESSAGE2.AsString);
      SC.ADDValue('DATEE', qryINF707DATEE.AsString);
      SC.ADDValue('APP_DATE', qryINF707APP_DATE.AsString);
      SC.ADDValue('IN_MATHOD', qryINF707IN_MATHOD.AsString);
      SC.ADDValue('AP_BANK1', qryINF707AP_BANK1.AsString);
      SC.ADDValue('AP_BANK2', qryINF707AP_BANK2.AsString);
      SC.ADDValue('AP_BANK3', qryINF707AP_BANK3.AsString);
      SC.ADDValue('AP_BANK4', qryINF707AP_BANK4.AsString);
      SC.ADDValue('AP_BANK5', qryINF707AP_BANK5.AsString);
      SC.ADDValue('AD_BANK1', qryINF707AD_BANK1.AsString);
      SC.ADDValue('AD_BANK2', qryINF707AD_BANK2.AsString);
      SC.ADDValue('AD_BANK3', qryINF707AD_BANK3.AsString);
      SC.ADDValue('AD_BANK4', qryINF707AD_BANK4.AsString);
      SC.ADDValue('IL_NO1', qryINF707IL_NO1.AsString);
      SC.ADDValue('IL_NO2', qryINF707IL_NO2.AsString);
      SC.ADDValue('IL_NO3', qryINF707IL_NO3.AsString);
      SC.ADDValue('IL_NO4', qryINF707IL_NO4.AsString);
      SC.ADDValue('IL_NO5', qryINF707IL_NO5.AsString);
      SC.ADDValue('IL_AMT1', qryINF707IL_AMT1.AsString,vtInteger);
      SC.ADDValue('IL_AMT2', qryINF707IL_AMT2.AsString,vtInteger);
      SC.ADDValue('IL_AMT3', qryINF707IL_AMT3.AsString,vtInteger);
      SC.ADDValue('IL_AMT4', qryINF707IL_AMT4.AsString,vtInteger);
      SC.ADDValue('IL_AMT5', qryINF707IL_AMT5.AsString,vtInteger);
//      SC.ADDValue('IL_AMT6', qryINF707IL_AMT6.AsString,vtInteger);
      SC.ADDValue('IL_CUR1', qryINF707IL_CUR1.AsString);
      SC.ADDValue('IL_CUR2', qryINF707IL_CUR2.AsString);
      SC.ADDValue('IL_CUR3', qryINF707IL_CUR3.AsString);
      SC.ADDValue('IL_CUR4', qryINF707IL_CUR4.AsString);
      SC.ADDValue('IL_CUR5', qryINF707IL_CUR5.AsString);
//      SC.ADDValue('IL_CUR6', qryINF707IL_CUR6.AsString);
      SC.ADDValue('AD_INFO1', qryINF707AD_INFO1.AsString);
      SC.ADDValue('AD_INFO2', qryINF707AD_INFO2.AsString);
      SC.ADDValue('AD_INFO3', qryINF707AD_INFO3.AsString);
      SC.ADDValue('AD_INFO4', qryINF707AD_INFO4.AsString);
      SC.ADDValue('AD_INFO5', qryINF707AD_INFO5.AsString);
      SC.ADDValue('CD_NO', qryINF707CD_NO.AsString);
      SC.ADDValue('RCV_REF', qryINF707RCV_REF.AsString);
      SC.ADDValue('ISS_BANK1', qryINF707ISS_BANK1.AsString);
      SC.ADDValue('ISS_BANK2', qryINF707ISS_BANK2.AsString);
      SC.ADDValue('ISS_BANK3', qryINF707ISS_BANK3.AsString);
      SC.ADDValue('ISS_BANK4', qryINF707ISS_BANK4.AsString);
      SC.ADDValue('ISS_BANK5', qryINF707ISS_BANK5.AsString);
      SC.ADDValue('ISS_ACCNT', qryINF707ISS_ACCNT.AsString);
      SC.ADDValue('ISS_DATE', qryINF707ISS_DATE.AsString);
      SC.ADDValue('AMD_DATE', qryINF707AMD_DATE.AsString);
      SC.ADDValue('EX_DATE', qryINF707EX_DATE.AsString);
      SC.ADDValue('APPLIC1', qryINF707APPLIC1.AsString);
      SC.ADDValue('APPLIC2', qryINF707APPLIC2.AsString);
      SC.ADDValue('APPLIC3', qryINF707APPLIC3.AsString);
      SC.ADDValue('APPLIC4', qryINF707APPLIC4.AsString);
      SC.ADDValue('APPLIC5', qryINF707APPLIC5.AsString);
      SC.ADDValue('BENEFC1', qryINF707BENEFC1.AsString);
      SC.ADDValue('BENEFC2', qryINF707BENEFC2.AsString);
      SC.ADDValue('BENEFC3', qryINF707BENEFC3.AsString);
      SC.ADDValue('BENEFC4', qryINF707BENEFC4.AsString);
      SC.ADDValue('BENEFC5', qryINF707BENEFC5.AsString);
      SC.ADDValue('INCD_CUR', qryINF707INCD_CUR.AsString);
      SC.ADDValue('INCD_AMT', qryINF707INCD_AMT.AsString);
      SC.ADDValue('DECO_CUR', qryINF707DECD_CUR.AsString);
      SC.ADDValue('DECO_AMT', qryINF707DECD_AMT.AsString,vtInteger);
      SC.ADDValue('NWCD_CUR', qryINF707NWCD_CUR.AsString);
      SC.ADDValue('NWCD_AMT', qryINF707NWCD_AMT.AsString,vtInteger);
      SC.ADDValue('BFCD_CUR', qryINF707BFCD_CUR.AsString);
      SC.ADDValue('BFCD_AMT', qryINF707BFCD_AMT.AsString,vtInteger);
      SC.ADDValue('CD_PERP', qryINF707CD_PERP.AsString,vtInteger);
      SC.ADDValue('CD_PERM', qryINF707CD_PERM.AsString,vtInteger);
      SC.ADDValue('CD_MAX', qryINF707CD_MAX.AsString);
      SC.ADDValue('AA_CV1', qryINF707AA_CV1.AsString);
      SC.ADDValue('AA_CV2', qryINF707AA_CV2.AsString);
      SC.ADDValue('AA_CV3', qryINF707AA_CV3.AsString);
      SC.ADDValue('AA_CV4', qryINF707AA_CV4.AsString);
      //------------------------------------------------------------------------
      // 2017-12-19
      // INF707 For Oracle 필드에 SUNJUCK_PORT, DOCHAK_PORT Varchar(35) 추가
      //------------------------------------------------------------------------
      SC.ADDValue('SUNJUCK_PORT', qryINF707SUNJUCK_PORT.AsString);
      SC.ADDValue('DOCHAK_PORT', qryINF707DOCHACK_PORT.AsString);
      SC.ADDValue('LOAD_ON', qryINF707LOAD_ON.AsString);
      SC.ADDValue('FOR_TRAN', qryINF707FOR_TRAN.AsString);
      SC.ADDValue('LST_DATE', qryINF707LST_DATE.AsString);
      SC.ADDValue('SHIP_PD1', qryINF707SHIP_PD1.AsString);
      SC.ADDValue('SHIP_PD2', qryINF707SHIP_PD2.AsString);
      SC.ADDValue('SHIP_PD3', qryINF707SHIP_PD3.AsString);
      SC.ADDValue('SHIP_PD4', qryINF707SHIP_PD4.AsString);
      SC.ADDValue('SHIP_PD5', qryINF707SHIP_PD5.AsString);
      SC.ADDValue('SHIP_PD6', qryINF707SHIP_PD6.AsString);
      IF qryINF707NARRAT.AsBoolean Then
        SC.ADDValue('NARRT', '1')
      else
        SC.ADDValue('NARRT', '0');
      SC.ADDValue('NARRT_1',qryINF707NARRAT_1.AsString);
      SC.ADDValue('SR_INFO1', qryINF707SR_INFO1.AsString);
      SC.ADDValue('SR_INFO2', qryINF707SR_INFO2.AsString);
      SC.ADDValue('SR_INFO3', qryINF707SR_INFO3.AsString);
      SC.ADDValue('SR_INFO4', qryINF707SR_INFO4.AsString);
      SC.ADDValue('SR_INFO5', qryINF707SR_INFO5.AsString);
      SC.ADDValue('SR_INFO6', qryINF707SR_INFO6.AsString);
      SC.ADDValue('EX_NAME', qryINF707EX_NAME1.AsString);
      SC.ADDValue('EX_NAME2', qryINF707EX_NAME2.AsString);
      SC.ADDValue('EX_NAME3', qryINF707EX_NAME3.AsString);
      SC.ADDValue('EX_ADDR1', qryINF707EX_ADDR1.AsString);
      SC.ADDValue('EX_ADDR2', qryINF707EX_ADDR2.AsString);
      SC.ADDValue('OP_BANK1', qryINF707OP_BANK1.AsString);
      SC.ADDValue('OP_BANK2', qryINF707OP_BANK2.AsString);
      SC.ADDValue('OP_BANK3', qryINF707OP_BANK3.AsString);
      //DP->OP 로 잘못표기함
      SC.ADDValue('DP_ADDR1', qryINF707OP_ADDR1.AsString);
      SC.ADDValue('DP_ADDR2', qryINF707OP_ADDR2.AsString);

      SC.ADDValue('FRST_REG_USER_ID',UPLOAD_SID);
      SC.ADDValue('FRST_REG_DTM',SYSDATE,vtVariant);
      SC.ADDValue('LAST_MOD_USER_ID',UPLOAD_SID);
      SC.ADDValue('LAST_MOD_DTM',SYSDATE,vtVariant);

      SQL.Text := SC.CreateSQL;
      AddLog('INF707',SQL.Text);
      try
       ExecSQL;
      except
        on e:exception do
        begin
          ShowMessage(e.Message);
        end;
      end;
    finally
      Close;
      Free;
      SC.Free;
    end;
  end;

end;

procedure TLivartData_frm.sComboBox2Select(Sender: TObject);
begin
  mskINF707.Visible := sComboBox2.ItemIndex = 1;
end;

procedure TLivartData_frm.sComboBox1Select(Sender: TObject);
begin
  mskINF700.Visible := sComboBox1.ItemIndex = 1;
end;

procedure TLivartData_frm.INF700_ReadList;
begin
  with qryINF700 do
  begin
    Close;
    SQL.Text := 'SELECT * FROM INF700_1 I1 LEFT JOIN INF700_2 ON I1.MAINT_NO = INF700_2.MAINT_NO'#13+
                '          LEFT JOIN INF700_3 ON I1.MAINT_NO = INF700_3.MAINT_NO'#13+
                '          LEFT JOIN INF700_4 ON I1.MAINT_NO = INF700_4.MAINT_NO';

    Case sComboBox1.ItemIndex of
      //관리번호
      0: SQL.Add( 'WHERE I1.MAINT_NO LIKE '+QuotedStr('%'+edtINF700.Text+'%') );
      //신청일자
      1: SQL.Add( 'WHERE APP_DATE LIKE '+QuotedStr('%'+mskINF700.Text+'%') );
    end;
    Open;
  end;  
end;

procedure TLivartData_frm.INF700_upload;
var
  SC : TSQLCreate;
  __MAINT_NO : String;
begin
  IF qryINF700.RecordCount = 0 Then
  begin
    raise Exception.Create('업로드할 데이터가 없습니다');
  end;

  SC := TSQLCreate.Create;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := LivartConfig_frm.LivartConn;
      SC.DMLType := dmlInsert;
      SC.SQLHeader('TC_KIS_LCROER_RCV_IF');

      SC.ADDValue('CO_GBCD', '11');
//------------------------------------------------------------------------------
// 2018-02-05
// MAINT_NO 하이픈으로 품의서번호와 일련번호 분리
//------------------------------------------------------------------------------
      __MAINT_NO := qryINF700MAINT_NO.AsString;
      SC.ADDValue('IMPTNO', LeftStr(__MAINT_NO, POS('_',__MAINT_NO)-1));
      SC.ADDValue('IMPTSQ', MidStr(__MAINT_NO, POS('_',__MAINT_NO)+1, Length(__MAINT_NO)-POS('_',__MAINT_NO)),vtInteger);
//      ShowMessage(LeftStr(__MAINT_NO, POS('_',__MAINT_NO)-1)+#13#10+MidStr(__MAINT_NO, POS('_',__MAINT_NO)+1, Length(__MAINT_NO)-POS('_',__MAINT_NO)));
//      SC.ADDValue('IMPTNO', qryINF700MAINT_NO.AsString);
//      SC.ADDValue('IMPTS1Q', '1');


      SC.ADDValue('MESSAGE1', qryINF700MESSAGE1.AsString);
      SC.ADDValue('MESSAGE2', qryINF700MESSAGE2.AsString);
      SC.ADDValue('DATEE', qryINF700DATEE.AsString);
      SC.ADDValue('APP_DATE', qryINF700APP_DATE.AsString);
      SC.ADDValue('IN_MATHOD', qryINF700IN_MATHOD.AsString);
      SC.ADDValue('AP_BANK', qryINF700AP_BANK.AsString);
      SC.ADDValue('AP_BANK1', qryINF700AP_BANK1.AsString);
      SC.ADDValue('AP_BANK2', qryINF700AP_BANK2.AsString);
      SC.ADDValue('AP_BANK3', qryINF700AP_BANK3.AsString);
      SC.ADDValue('AP_BANK4', qryINF700AP_BANK4.AsString);
      SC.ADDValue('AP_BANK5', qryINF700AP_BANK5.AsString);
      SC.ADDValue('AD_BANK', qryINF700AD_BANK.asString);
      SC.ADDValue('AD_BANK1', qryINF700AD_BANK1.asString);
      SC.ADDValue('AD_BANK2', qryINF700AD_BANK2.asString);
      SC.ADDValue('AD_BANK3', qryINF700AD_BANK3.asString);
      SC.ADDValue('AD_BANK4', qryINF700AD_BANK4.asString);
      SC.ADDValue('AD_PAY', qryINF700AD_PAY.asString);
      SC.ADDValue('IMP_CD1', qryINF700IMP_CD1.AsString);
      SC.ADDValue('IMP_CD2', qryINF700IMP_CD2.AsString);
      SC.ADDValue('IMP_CD3', qryINF700IMP_CD3.AsString);
      SC.ADDValue('IMP_CD4', qryINF700IMP_CD4.AsString);
      SC.ADDValue('IMP_CD5', qryINF700IMP_CD5.AsString);
      SC.ADDValue('IL_NO1',  qryINF700IL_NO1.AsString);
      SC.ADDValue('IL_NO2',  qryINF700IL_NO2.AsString);
      SC.ADDValue('IL_NO3', qryINF700IL_NO3.AsString);
      SC.ADDValue('IL_NO4', qryINF700IL_NO4.AsString);
      SC.ADDValue('IL_NO5', qryINF700IL_NO5.AsString);
      SC.ADDValue('IL_CUR1', qryINF700IL_CUR1.AsString);
      SC.ADDValue('IL_CUR2', qryINF700IL_CUR2.AsString);
      SC.ADDValue('IL_CUR3', qryINF700IL_CUR3.AsString);
      SC.ADDValue('IL_CUR4', qryINF700IL_CUR4.AsString);
      SC.ADDValue('IL_CUR5', qryINF700IL_CUR5.AsString);
      SC.ADDValue('IL_AMT1', qryINF700IL_AMT1.AsString,vtInteger);
      SC.ADDValue('IL_AMT2', qryINF700IL_AMT2.AsString,vtInteger);
      SC.ADDValue('IL_AMT3', qryINF700IL_AMT3.AsString,vtInteger);
      SC.ADDValue('IL_AMT4', qryINF700IL_AMT4.AsString,vtInteger);
      SC.ADDValue('IL_AMT5', qryINF700IL_AMT5.AsString,vtInteger);
      SC.ADDValue('AD_INFO1', qryINF700AD_INFO1.AsString);
      SC.ADDValue('AD_INFO2', qryINF700AD_INFO2.AsString);
      SC.ADDValue('AD_INFO3', qryINF700AD_INFO3.AsString);
      SC.ADDValue('AD_INFO4', qryINF700AD_INFO4.AsString);
      SC.ADDValue('AD_INFO5', qryINF700AD_INFO5.AsString);
      SC.ADDValue('EX_NAME1', qryINF700EX_NAME1.AsString);
      SC.ADDValue('EX_NAME2', qryINF700EX_NAME2.AsString);
      SC.ADDValue('EX_NAME3', qryINF700EX_NAME3.AsString);
      SC.ADDValue('EX_ADDR1', qryINF700EX_ADDR1.AsString);
      SC.ADDValue('EX_ADDR2', qryINF700EX_ADDR2.AsString);
      SC.ADDValue('DOC_CD', qryINF700DOC_CD.AsString);
      SC.ADDValue('CD_NO', qryINF700CD_NO.AsString);
      SC.ADDValue('REF_PRE', qryINF700REF_PRE.AsString);
      SC.ADDValue('ISS_DATE', qryINF700ISS_DATE.AsString);
      SC.ADDValue('EX_DATE', qryINF700EX_DATE.AsString);
      SC.ADDValue('EX_PLACE', qryINF700EX_PLACE.AsString);
      SC.ADDValue('CHK2', qryINF700CHK2.AsString);
      SC.ADDValue('CHK3', qryINF700CHK3.AsString);
      SC.ADDValue('APPLICABLE_RULES_1', qryINF700APPLICABLE_RULES_1.AsString);
      SC.ADDValue('APPLICABLE_RULES_2', qryINF700APPLICABLE_RULES_2.AsString);
      SC.ADDValue('APP_BANK', qryINF700APP_BANK.AsString);
      SC.ADDValue('APP_BANK1', qryINF700APP_BANK1.AsString);
      SC.ADDValue('APP_BANK2', qryINF700APP_BANK2.AsString);
      SC.ADDValue('APP_BANK3', qryINF700APP_BANK3.AsString);
      SC.ADDValue('APP_BANK4', qryINF700APP_BANK4.AsString);
      SC.ADDValue('APP_BANK5', qryINF700APP_BANK5.AsString);
      SC.ADDValue('APP_ACCNT', qryINF700APP_ACCNT.AsString);
      SC.ADDValue('APPLIC1', qryINF700APPLIC1.AsString);
      SC.ADDValue('APPLIC2', qryINF700APPLIC2.AsString);
      SC.ADDValue('APPLIC3', qryINF700APPLIC3.AsString);
      SC.ADDValue('APPLIC4', qryINF700APPLIC4.AsString);
      SC.ADDValue('APPLIC5', qryINF700APPLIC5.AsString);
      SC.ADDValue('BEBEFC1', qryINF700BENEFC1.AsString);
      SC.ADDValue('BEBEFC2', qryINF700BENEFC2.AsString);
      SC.ADDValue('BEBEFC3', qryINF700BENEFC3.AsString);
      SC.ADDValue('BEBEFC4', qryINF700BENEFC4.AsString);
      SC.ADDValue('BEBEFC5', qryINF700BENEFC5.AsString);
      SC.ADDValue('CD_AMT', qryINF700CD_AMT.AsString,vtInteger);
      SC.ADDValue('CD_CUR', qryINF700CD_CUR.AsString);
      SC.ADDValue('CD_PERP', qryINF700CD_PERP.AsString);
      SC.ADDValue('CD_PERM', qryINF700CD_PERM.AsString);
      SC.ADDValue('CD_MAX', qryINF700CD_MAX.AsString);
      SC.ADDValue('TERM_PR', qryINF700TERM_PR.AsString);
      SC.ADDValue('TERM_PR_M', qryINF700TERM_PR_M.AsString);
      SC.ADDValue('PL_TERM', qryINF700PL_TERM.AsString);
      SC.ADDValue('AA_CV1', qryINF700AA_CV1.AsString);
      SC.ADDValue('AA_CV2', qryINF700AA_CV2.AsString);
      SC.ADDValue('AA_CV3', qryINF700AA_CV3.AsString);
      SC.ADDValue('AA_CV4', qryINF700AA_CV4.AsString);
      SC.ADDValue('DRAFT1', qryINF700DRAFT1.AsString);
      SC.ADDValue('DRAFT2', qryINF700DRAFT2.AsString);
      SC.ADDValue('DRAFT3', qryINF700DRAFT3.AsString);
      SC.ADDValue('MIX_PAY1', qryINF700MIX_PAY1.AsString);
      SC.ADDValue('MIX_PAY2', qryINF700MIX_PAY1.AsString);
      SC.ADDValue('MIX_PAY3', qryINF700MIX_PAY1.AsString);
      SC.ADDValue('MIX_PAY4', qryINF700MIX_PAY1.AsString);
      SC.ADDValue('DEF_PAY1', qryINF700DEF_PAY1.AsString);
      SC.ADDValue('DEF_PAY2', qryINF700DEF_PAY2.AsString);
      SC.ADDValue('DEF_PAY3', qryINF700DEF_PAY3.AsString);
      SC.ADDValue('DEF_PAY4', qryINF700DEF_PAY4.AsString);
      SC.ADDValue('AVT_BANK1', qryINF700AVT_BANK1.AsString);
      SC.ADDValue('AVT_BANK2', qryINF700AVT_BANK2.AsString);
      SC.ADDValue('AVT_BANK3', qryINF700AVT_BANK3.AsString);
      SC.ADDValue('AVT_BANK4', qryINF700AVT_BANK4.AsString);
      SC.ADDValue('AVT_BANK5', qryINF700AVT_BANK5.AsString);
      SC.ADDValue('AVT_ACCNT', qryINF700AVT_ACCNT.AsString);
      SC.ADDValue('SND_INFO1', qryINF700SND_INFO1.asString);
      SC.ADDValue('SND_INFO2', qryINF700SND_INFO2.asString);
      SC.ADDValue('SND_INFO3', qryINF700SND_INFO3.asString);
      SC.ADDValue('SND_INFO4', qryINF700SND_INFO4.asString);
      SC.ADDValue('SND_INFO5', qryINF700SND_INFO5.asString);
      SC.ADDValue('SND_INFO6', qryINF700SND_INFO6.asString);
      SC.ADDValue('AVAIL1', qryINF700AVAIL1.AsString);
      SC.ADDValue('AVAIL2', qryINF700AVAIL2.AsString);
      SC.ADDValue('AVAIL3', qryINF700AVAIL3.AsString);
      SC.ADDValue('AVAIL4', qryINF700AVAIL4.AsString);
      SC.ADDValue('AV_ACCNT', qryINF700AV_ACCNT.AsString);
      SC.ADDValue('AV_PAY', qryINF700AV_PAY.AsString);
      SC.ADDValue('DRAWEE1', qryINF700DRAWEE1.AsString);
      SC.ADDValue('DRAWEE2', qryINF700DRAWEE2.AsString);
      SC.ADDValue('DRAWEE3', qryINF700DRAWEE3.AsString);
      SC.ADDValue('DRAWEE4', qryINF700DRAWEE4.AsString);
      SC.ADDValue('DR_ACCNT', qryINF700DR_ACCNT.AsString);
      SC.ADDValue('REI_BANK', qryINF700REI_BANK.AsString);
      SC.ADDValue('REI_BANK1', qryINF700REI_BANK1.AsString);
      SC.ADDValue('REI_BANK2', qryINF700REI_BANK2.AsString);
      SC.ADDValue('REI_BANK3', qryINF700REI_BANK3.AsString);
      SC.ADDValue('REI_BANK4', qryINF700REI_BANK4.AsString);
      SC.ADDValue('REI_BANK5', qryINF700REI_BANK5.AsString);
      SC.ADDValue('REI_ACCNT', qryINF700REI_ACNNT.AsString);
      SC.ADDValue('OP_BANK1', qryINF700OP_BANK1.AsString);
      SC.ADDValue('OP_BANK2', qryINF700OP_BANK2.AsString);
      SC.ADDValue('OP_BANK3', qryINF700OP_BANK3.AsString);
      SC.ADDValue('OP_ADDR1', qryINF700OP_ADDR1.AsString);
      SC.ADDValue('OP_ADDR2', qryINF700OP_ADDR2.AsString);
      SC.ADDValue('OP_ADDR3', qryINF700OP_ADDR3.AsString);
      SC.ADDValue('PSHIP', qryINF700PSHIP.AsString);
      SC.ADDValue('TSHIP', qryINF700TSHIP.AsString);
      SC.ADDValue('SUNJUCK_PORT', qryINF700SUNJUCK_PORT.AsString);
      SC.ADDValue('DOCHACK_PORT', qryINF700DOCHACK_PORT.AsString);
      SC.ADDValue('LOAD_ON', qryINF700LOAD_ON.AsString);
      SC.ADDValue('FOR_TRAN', qryINF700FOR_TRAN.AsString);
      SC.ADDValue('LST_DATE', qryINF700LST_DATE.AsString);
      SC.ADDValue('SHIP_PD1', qryINF700SHIP_PD1.AsString);
      SC.ADDValue('SHIP_PD2', qryINF700SHIP_PD2.AsString);
      SC.ADDValue('SHIP_PD3', qryINF700SHIP_PD3.AsString);
      SC.ADDValue('SHIP_PD4', qryINF700SHIP_PD4.AsString);
      SC.ADDValue('SHIP_PD5', qryINF700SHIP_PD5.AsString);
      SC.ADDValue('SHIP_PD6', qryINF700SHIP_PD6.AsString);
      SC.ADDValue('ORIGIN', qryINF700ORIGIN.AsString);
      SC.ADDValue('ORIGIN_M', qryINF700ORIGIN_M.AsString);
      IF qryINF700DOC_380.AsBoolean Then
        SC.ADDValue('DOC_380', '1')
      else
        SC.ADDValue('DOC_380', '0');
      SC.ADDValue('DOC_380_1', qryINF700DOC_380_1.AsString);

      IF qryINF700DOC_705.AsBoolean Then
        SC.ADDValue('DOC_705', '1')
      else
        SC.ADDValue('DOC_705', '0');

      SC.ADDValue('DOC_705_GUBUN',qryINF700DOC_705_GUBUN.AsString);
      SC.ADDValue('DOC_705_1', qryINF700DOC_705_1.AsString);
      SC.ADDValue('DOC_705_2', qryINF700DOC_705_2.AsString);
      SC.ADDValue('DOC_705_3', qryINF700DOC_705_3.AsString);
      SC.ADDValue('DOC_705_4', qryINF700DOC_705_4.AsString);

      IF qryINF700DOC_740.AsBoolean Then
        SC.ADDValue('DOC_740','1')
      else
        SC.ADDValue('DOC_740','0');
      SC.ADDValue('DOC_740_1', qryINF700DOC_740_1.AsString);
      SC.ADDValue('DOC_740_2', qryINF700DOC_740_2.AsString);
      SC.ADDValue('DOC_740_3', qryINF700DOC_740_3.AsString);
      SC.ADDValue('DOC_740_4', qryINF700DOC_740_4.AsString);
      IF qryINF700DOC_760.AsBoolean Then
        SC.ADDValue('DOC_760','1')
      else
        SC.ADDValue('DOC_760','0');

      SC.ADDValue('DOC_760_1', qryINF700DOC_760_1.AsString);
      SC.ADDValue('DOC_760_2', qryINF700DOC_760_2.AsString);
      SC.ADDValue('DOC_760_3', qryINF700DOC_760_3.AsString);
      SC.ADDValue('DOC_760_4', qryINF700DOC_760_4.AsString);

      IF qryINF700DOC_530.AsBoolean Then
        SC.ADDValue('DOC_530','1')
      else
        SC.ADDValue('DOC_530','0');

      SC.ADDValue('DOC_530_1', qryINF700DOC_530_1.AsString);
      SC.ADDValue('DOC_530_2', qryINF700DOC_530_2.AsString);

      IF qryINF700DOC_271.AsBoolean Then
        SC.ADDValue('DOC_271','1')
      else
        SC.ADDValue('DOC_271','0');
      SC.ADDValue('DOC_271_1', qryINF700DOC_271_1.AsString);

      IF qryINF700DOC_861.AsBoolean Then
        SC.ADDValue('DOC_861','1')
      else
        SC.ADDValue('DOC_861','0');

      if qryINF700DOC_2AA.AsBoolean Then
        SC.ADDValue('DOC_2AA','1')
      else
        SC.ADDValue('DOC_2AA','0');

      SC.ADDValue('DOC_2AA_1', qryINF700DOC_2AA_1.AsString);

      if qryINF700ACD_2AA.AsBoolean Then
        SC.ADDValue('ACD_2AA','1')
      else
        SC.ADDValue('ACD_2AA','0');
      SC.ADDValue('ACD_2AA_1', qryINF700ACD_2AA_1.AsString);

      if qryINF700ACD_2AB.AsBoolean Then
        SC.ADDValue('ACD_2AB','1')
      else
        SC.ADDValue('ACD_2AB','0');

      if qryINF700ACD_2AC.AsBoolean Then
        SC.ADDValue('ACD_2AC','1')
      else
        SC.ADDValue('ACD_2AC','0');

      if qryINF700ACD_2AD.AsBoolean Then
        SC.ADDValue('ACD_2AD','1')
      else
        SC.ADDValue('ACD_2AD','0');

      if qryINF700ACD_2AE.AsBoolean Then
        SC.ADDValue('ACD_2AE','1')
      else
        SC.ADDValue('ACD_2AE','0');
      SC.ADDValue('ACD_2AE_1',qryINF700ACD_2AE_1.AsString);
      IF qryINF700INSTRCT.AsBoolean Then
        SC.ADDValue('INSTRCT','1')
      else
        SC.ADDValue('INSTRCT','0');

      SC.ADDValue('INSTRCT_1', qryINF700INSTRCT_1.AsString);
      SC.ADDValue('CHARGE', qryINF700CHARGE.AsString);
      SC.ADDValue('PERIOD', qryINF700PERIOD.AsString);
      SC.ADDValue('CONFIRM', qryINF700CONFIRMM.AsString);
//      SC.ADDValue('CONFIRMM', qryINF700CONFIRMM.AsString);
      IF qryINF700DESGOOD.AsBoolean Then
        SC.ADDValue('DESGOOD', '1')
      else
        SC.ADDValue('DESGOOD', '0');

      SC.ADDValue('DESGOOD_1', qryINF700DESGOOD_1.AsString);

      SC.ADDValue('FRST_REG_USER_ID',UPLOAD_SID);
      SC.ADDValue('FRST_REG_DTM',SYSDATE,vtVariant);
      SC.ADDValue('LAST_MOD_USER_ID',UPLOAD_SID);
      SC.ADDValue('LAST_MOD_DTM',SYSDATE,vtVariant);
      SQL.Text := SC.CreateSQL;
      AddLog('INF700',SQL.Text);
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;

end;

procedure TLivartData_frm.sButton7Click(Sender: TObject);
begin

  UI_test_frm := TUI_test_frm.Create(Self);
  try
  case sPageControl1.ActivePageIndex  of
    0: UI_test_frm.sDBGrid1.DataSource := dsAPP700;
    1: UI_test_frm.sDBGrid1.DataSource := dsINF700;
    2: UI_test_frm.sDBGrid1.DataSource := dsAPP707;
    3: UI_test_frm.sDBGrid1.DataSource := dsINF707;
    4: UI_test_frm.sDBGrid1.DataSource := dsDOANTC;
  end;
  UI_test_frm.sDBGrid2.DataSource := nil;
  UI_test_frm.sDBGrid3.DataSource := nil;
  UI_test_frm.ShowModal;
  finally
    FreeAndNil(UI_test_frm);
  end;

end;

procedure TLivartData_frm.sButton10Click(Sender: TObject);
begin
  UI_test_frm := TUI_test_frm.Create(Self);
  try
    qryTest.Close;
    ADOQuery1.Close;
    ADOQuery2.Close;
    case (Sender as TsButton).tag  of
      0: begin
           qryTest.SQL.Text := 'SELECT * FROM APP700_1';
           ADOQuery1.SQL.Text := 'SELECT * FROM APP700_2';
           ADOQuery2.SQL.Text := 'SELECT * FROM APP700_3';
           ADOQuery1.Open;
           ADOQuery2.Open;
           UI_test_frm.sDBGrid2.DataSource := DataSource1;
           UI_test_frm.sDBGrid3.DataSource := DataSource2;
         end;
      1: begin
           qryTest.SQL.Text := 'SELECT * FROM APP707_1';
           ADOQuery1.SQL.Text := 'SELECT * FROM APP707_2';
           ADOQuery1.Open;
           UI_test_frm.sDBGrid2.DataSource := DataSource1;
           UI_test_frm.sDBGrid3.DataSource := nil;
         end;
    end;
    qryTest.Open;
    UI_test_frm.sDBGrid1.DataSource := dsTest;
    UI_test_frm.ShowModal;
  finally
    FreeAndNil(UI_test_frm);
  end;
end;

procedure TLivartData_frm.AddLog(obj, sData : String);
var
  LOG_PATH : String;
  LOG_DATA : TStringList;
begin
  LOG_PATH := ExtractFilePath(Application.ExeName)+'LOG\';
  ForceDirectories(LOG_PATH);

  LOG_DATA := TStringList.Create;
  try
    LOG_DATA.Text := sData;
    LOG_DATA.SaveToFile(LOG_PATH+FormatDateTime('YYYYMMDDHHNNSS',Now)+'_'+obj+'_log.txt');
  finally
    LOG_DATA.Free;
  end;
end;

function TLivartData_frm.getCodeContent(sPrefix, sCode: String): String;
begin
  with TADOQuery.Create(self) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT [CODE], [NAME] FROM [CODE2NDD] WHERE Prefix = '+QuotedStr(sPrefix)+' AND CODE = '+QuotedStr(sCode);
      Open;

      Result := '';

      IF RecordCount > 0 Then
      begin
        Result := Fields[1].AsString;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TLivartData_frm.sButton13Click(Sender: TObject);
begin
  IF not Assigned( dlg_ERPKIS_BK_CONN_frm ) Then dlg_ERPKIS_BK_CONN_frm := Tdlg_ERPKIS_BK_CONN_frm.Create(Self);
  try
    dlg_ERPKIS_BK_CONN_frm.ShowModal;
  finally
    FreeAndNil(dlg_ERPKIS_BK_CONN_frm);
  end;
end;

function TLivartData_frm.CompareBANKCD(sERP_BANK_CD: String): String;
begin
  Result := '';
  with TADOQuery.Create(self) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT KIS_CODE, ENAME1 as BKNM, ENAME3 as BKBR'#13#10+
                  'FROM ERP_KIS_BANKCONN LEFT JOIN BANKCODE ON KIS_CODE = BANKCODE.CODE'#13#10+
                  'WHERE IS_USE = 1 AND ERP_CODE = '+QuotedStr(sERP_BANK_CD);
      Open;
      IF RecordCount > 0 Then
      begin
        Result := FieldByName('KIS_CODE').AsString;
      end;

    finally
      Close;
      Free;
    end;
  end;
end;


Const
  H = 50;
procedure TLivartData_frm.FormCreate(Sender: TObject);
begin
//  TmyEdit(mskINF700).SetBounds(mskINF700.Left, mskINF700.Top, mskINF700.Width, mskINF700.Height);
//  TmyEdit(mskINF700).SetBounds(100,100,200,h);
//  TmyEdit(mskINF700).BorderWidth := (H - Abs(TmyEdit(mskINF700).Font.Height*2)) div 2;
//ShowMessage(IntToStr(TmyEdit(mskINF700).BorderWidth) );
end;

end.
