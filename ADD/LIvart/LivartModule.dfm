object LivartModule_frm: TLivartModule_frm
  Left = 467
  Top = 118
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #45936#51060#53552' '#44032#51256#50724#44592' - '#47532#48148#53944
  ClientHeight = 635
  ClientWidth = 1075
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  Scaled = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 233
    Height = 635
    Align = alLeft
    
    TabOrder = 0
    object Shape1: TShape
      Left = 6
      Top = 51
      Width = 220
      Height = 2
      Brush.Color = clGray
      Pen.Color = clNone
      Pen.Style = psClear
    end
    object Shape2: TShape
      Left = 7
      Top = 108
      Width = 220
      Height = 2
      Brush.Color = clGray
      Pen.Color = clNone
      Pen.Style = psClear
    end
    object sButton2: TsButton
      Left = 6
      Top = 6
      Width = 91
      Height = 37
      Cursor = crHandPoint
      Caption = #50672#44208#49444#51221
      TabOrder = 0
      OnClick = sButton2Click
      Reflected = True
      Images = DMICON.System24
      ImageIndex = 3
    end
    object sButton13: TsButton
      Left = 101
      Top = 6
      Width = 125
      Height = 37
      Cursor = crHandPoint
      Caption = #51008#54665#50672#44228#53076#46300
      TabOrder = 1
      OnClick = sButton13Click
      Reflected = True
      Images = DMICON.System24
      ImageIndex = 33
    end
    object sButton4: TsButton
      Left = 6
      Top = 61
      Width = 220
      Height = 37
      Cursor = crHandPoint
      Caption = #49440#53469#45936#51060#53552' '#44032#51256#50724#44592
      TabOrder = 2
      OnClick = sButton4Click
      Reflected = True
      Images = DMICON.System24
      ImageIndex = 2
    end
    object chk_SvFDINFO: TsCheckBox
      Left = 8
      Top = 120
      Width = 147
      Height = 19
      Caption = 'Save Field Information'
      TabOrder = 3
    end
  end
  object sPageControl1: TsPageControl
    Left = 233
    Top = 0
    Width = 842
    Height = 635
    ActivePage = sTabSheet3
    Align = alClient
    TabHeight = 30
    TabOrder = 1
    AccessibleDisabledPages = False
    TabPadding = 10
    object sTabSheet1: TsTabSheet
      BorderWidth = 2
      Caption = '[APP700] '#49688#51077'LC'#49888#52397#49436
      object sPanel3: TsPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 34
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 0
        object sButton3: TsButton
          Left = 362
          Top = 2
          Width = 66
          Height = 30
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 0
          OnClick = sButton3Click
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 6
        end
        object sPanel2: TsPanel
          Left = 0
          Top = 2
          Width = 81
          Height = 30
          Caption = 'ERP > EDI'
          
          TabOrder = 1
        end
        object sPanel15: TsPanel
          Left = 82
          Top = 2
          Width = 55
          Height = 30
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          Caption = #49888#52397#51068#51088
          Color = clGray
          
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object sMaskEdit1: TsMaskEdit
          Left = 138
          Top = 2
          Width = 103
          Height = 30
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 3
          OnKeyUp = sMaskEdit1KeyUp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20171207'
        end
        object sButton1: TsButton
          Left = 520
          Top = 2
          Width = 30
          Height = 30
          Cursor = crHandPoint
          TabOrder = 4
          Visible = False
          OnClick = sButton1Click
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 32
        end
        object sMaskEdit3: TsMaskEdit
          Left = 258
          Top = 2
          Width = 103
          Height = 30
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 5
          OnKeyUp = sMaskEdit1KeyUp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20171207'
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 34
        Width = 830
        Height = 557
        Align = alClient
        Color = clWhite
        Ctl3D = False
        DataSource = dsAPP700
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Color = clBtnFace
            Expanded = False
            FieldName = 'IMPTNO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 173
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clBtnFace
            Expanded = False
            FieldName = 'IMPTSQ'
            Title.Alignment = taCenter
            Title.Caption = #49692#48264
            Width = 37
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#49888#52397#51068#51088
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665#47749
            Width = 136
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AD_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #55148#47581#53685#51648#51008#54665
            Width = 198
            Visible = True
          end
          item
            Color = 14548991
            Expanded = False
            FieldName = 'CD_AMT'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529
            Width = 116
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CD_CUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 39
            Visible = True
          end>
      end
    end
    object sTabSheet2: TsTabSheet
      BorderWidth = 2
      Caption = '[INF700] '#49688#51077'LC'#51025#45813#49436
      TabVisible = False
      object sPanel4: TsPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 34
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 0
        object sButton6: TsButton
          Left = 944
          Top = 2
          Width = 97
          Height = 30
          Cursor = crHandPoint
          Caption = #49352#47196#44256#52840
          TabOrder = 0
          SkinData.SkinSection = 'BUTTON'
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 31
        end
        object sPanel5: TsPanel
          Left = 0
          Top = 2
          Width = 81
          Height = 30
          SkinData.SkinSection = 'PANEL'
          Caption = 'EDI > ERP'
          
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object sComboBox1: TsComboBox
          Left = 82
          Top = 2
          Width = 119
          Height = 30
          SkinData.SkinSection = 'COMBOBOX'
          VerticalAlignment = taVerticalCenter
          Style = csOwnerDrawFixed
          ItemHeight = 24
          ItemIndex = 1
          TabOrder = 2
          Text = #44060#49444#49888#52397#51068#51088
          Items.Strings = (
            #44288#47532#48264#54840
            #44060#49444#49888#52397#51068#51088)
        end
        object edtINF700: TsEdit
          Left = 202
          Top = 2
          Width = 199
          Height = 30
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object sButton8: TsButton
          Left = 402
          Top = 2
          Width = 63
          Height = 30
          Cursor = crHandPoint
          Caption = #51312#54924
          TabOrder = 4
          SkinData.SkinSection = 'BUTTON'
          Reflected = True
        end
        object mskINF700: TsMaskEdit
          Left = 202
          Top = 2
          Width = 199
          Height = 30
          AutoSize = False
          Ctl3D = True
          
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 5
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20171207'
        end
      end
      object sDBGrid2: TsDBGrid
        Left = 0
        Top = 34
        Width = 830
        Height = 557
        Align = alClient
        Color = clWhite
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 119
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51088
            Width = 42
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #49888#52397#51068#51088
            Width = 94
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Caption = #44060#49444#51008#54665
            Width = 249
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AD_BANK1'
            Title.Caption = #53685#51648#51008#54665
            Width = 289
            Visible = True
          end>
      end
    end
    object sTabSheet3: TsTabSheet
      BorderWidth = 2
      Caption = '[APP707] '#51312#44148#48320#44221#49888#52397#49436
      object sPanel7: TsPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 34
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 0
        object sPanel8: TsPanel
          Left = 0
          Top = 2
          Width = 81
          Height = 30
          Caption = 'ERP > EDI'
          
          TabOrder = 0
        end
        object sPanel6: TsPanel
          Left = 82
          Top = 2
          Width = 53
          Height = 30
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          Caption = #49888#52397#51068#51088
          Color = clGray
          
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object sMaskEdit2: TsMaskEdit
          Left = 138
          Top = 2
          Width = 103
          Height = 30
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 2
          OnKeyUp = sMaskEdit1KeyUp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20171207'
        end
        object sButton7: TsButton
          Left = 504
          Top = 2
          Width = 30
          Height = 30
          Cursor = crHandPoint
          TabOrder = 3
          Visible = False
          OnClick = sButton1Click
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 32
        end
        object sButton10: TsButton
          Left = 363
          Top = 2
          Width = 66
          Height = 30
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 4
          OnClick = sButton3Click
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 6
        end
        object sMaskEdit4: TsMaskEdit
          Left = 258
          Top = 2
          Width = 103
          Height = 30
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 5
          OnKeyUp = sMaskEdit1KeyUp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20171207'
        end
      end
      object sDBGrid3: TsDBGrid
        Left = 0
        Top = 34
        Width = 830
        Height = 557
        Align = alClient
        Color = clWhite
        Ctl3D = False
        DataSource = dsAPP707
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Color = clBtnFace
            Expanded = False
            FieldName = 'IMPTNO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 125
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMNDSQ'
            Title.Alignment = taCenter
            Title.Caption = #48320#44221#49888#52397
            Width = 57
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 72
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #48320#44221#49888#52397#51068#51088
            Width = 117
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'IN_MATHOD'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#48169#48277
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665#47749
            Width = 173
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AD_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #55148#47581#53685#51648#51008#54665
            Width = 193
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AD_BANK'
            Width = 57
            Visible = True
          end
          item
            Expanded = False
            Width = 64
            Visible = True
          end>
      end
    end
    object sTabSheet4: TsTabSheet
      BorderWidth = 2
      Caption = '[INF707] '#51312#44148#48320#44221#51025#45813#49436
      TabVisible = False
      object sPanel9: TsPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 34
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 0
        object sButton12: TsButton
          Left = 944
          Top = 2
          Width = 97
          Height = 30
          Cursor = crHandPoint
          Caption = #49352#47196#44256#52840
          TabOrder = 0
          SkinData.SkinSection = 'BUTTON'
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 31
        end
        object sPanel10: TsPanel
          Left = 0
          Top = 2
          Width = 81
          Height = 30
          SkinData.SkinSection = 'PANEL'
          Caption = 'EDI > ERP'
          
          TabOrder = 1
        end
        object sPanel11: TsPanel
          Left = 82
          Top = 2
          Width = 39
          Height = 30
          SkinData.SkinSection = 'PANEL'
          Caption = #51312#54924
          
          TabOrder = 2
        end
        object sComboBox2: TsComboBox
          Left = 122
          Top = 2
          Width = 119
          Height = 30
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          SkinData.SkinSection = 'COMBOBOX'
          Style = csOwnerDrawFixed
          ItemHeight = 24
          ItemIndex = 1
          TabOrder = 3
          Text = #48320#44221#49888#52397#51068#51088
          Items.Strings = (
            #44288#47532#48264#54840
            #48320#44221#49888#52397#51068#51088)
        end
        object edtINF707: TsEdit
          Left = 242
          Top = 2
          Width = 199
          Height = 30
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object sButton14: TsButton
          Left = 442
          Top = 2
          Width = 63
          Height = 30
          Cursor = crHandPoint
          Caption = #51312#54924
          TabOrder = 5
          SkinData.SkinSection = 'BUTTON'
          Reflected = True
        end
        object mskINF707: TsMaskEdit
          Left = 242
          Top = 2
          Width = 199
          Height = 30
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20171207'
        end
      end
      object sDBGrid4: TsDBGrid
        Left = 0
        Top = 34
        Width = 830
        Height = 557
        Align = alClient
        Color = clWhite
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 132
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'MSEQ'
            Title.Alignment = taCenter
            Title.Caption = #48320#44221#49692#48264
            Width = 57
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51088
            Width = 54
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #48320#44221#49888#52397#51068#51088
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Caption = #44060#49444#51032#47280#51008#54665
            Width = 249
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AD_BANK1'
            Title.Caption = #53685#51648#51008#54665
            Width = 271
            Visible = True
          end>
      end
    end
    object sTabSheet5: TsTabSheet
      BorderWidth = 2
      Caption = '[DOANTC] '#49440#51201#49436#47448#46020#52265#53685#48372#49436
      object sPanel12: TsPanel
        Left = 0
        Top = 0
        Width = 830
        Height = 34
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 0
        object edtDOANTC_DATE: TsEdit
          Left = 242
          Top = 2
          Width = 199
          Height = 30
          AutoSize = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object sButton15: TsButton
          Left = 944
          Top = 2
          Width = 97
          Height = 30
          Cursor = crHandPoint
          Caption = #49352#47196#44256#52840
          TabOrder = 0
          SkinData.SkinSection = 'BUTTON'
          Reflected = True
          Images = DMICON.System18
          ImageIndex = 31
        end
        object sPanel13: TsPanel
          Left = 0
          Top = 2
          Width = 81
          Height = 30
          SkinData.SkinSection = 'PANEL'
          Caption = 'EDI > ERP'
          
          TabOrder = 1
        end
        object sPanel14: TsPanel
          Left = 82
          Top = 2
          Width = 39
          Height = 30
          SkinData.SkinSection = 'PANEL'
          Caption = #51312#54924
          
          TabOrder = 2
        end
        object sComboBox3: TsComboBox
          Left = 122
          Top = 2
          Width = 119
          Height = 30
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          SkinData.SkinSection = 'COMBOBOX'
          Style = csOwnerDrawFixed
          ItemHeight = 24
          ItemIndex = 1
          TabOrder = 3
          Text = #53685#51648#51068#51088
          Items.Strings = (
            #44288#47532#48264#54840
            #53685#51648#51068#51088)
        end
        object sButton17: TsButton
          Left = 442
          Top = 2
          Width = 63
          Height = 30
          Cursor = crHandPoint
          Caption = #51312#54924
          TabOrder = 5
          SkinData.SkinSection = 'BUTTON'
          Reflected = True
        end
        object mskDOANTC_DATE: TsMaskEdit
          Left = 242
          Top = 2
          Width = 199
          Height = 30
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20171207'
        end
      end
      object sDBGrid5: TsDBGrid
        Left = 0
        Top = 34
        Width = 830
        Height = 557
        Align = alClient
        Color = clWhite
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 248
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#50976#51200
            Width = 52
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'RES_DATE'
            Title.Alignment = taCenter
            Title.Caption = #53685#51648#51068#51088
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'APP_NAME1'
            Title.Caption = #49688#51077#50629#52404
            Width = 182
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LC_G'
            Title.Alignment = taCenter
            Title.Caption = #49888#50857#51109#44396#48516
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LC_NO'
            Title.Alignment = taCenter
            Title.Caption = #49888#50857#51109#48264#54840
            Width = 150
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'BL_G'
            Title.Alignment = taCenter
            Title.Caption = #49440#54616#51613#44428#44396#48516
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BL_NO'
            Title.Alignment = taCenter
            Title.Caption = #49440#54616#51613#44428#48264#54840
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT'
            Title.Alignment = taCenter
            Title.Caption = #50612#51020#44552#50529
            Width = 137
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 28
            Visible = True
          end>
      end
    end
  end
  object sSkinProvider1: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'MS Sans Serif'
    AddedTitle.Font.Style = []
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 528
    Top = 312
  end
  object qryAPP700: TADOQuery
    Connection = LivartConfig_frm.LivartConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM TC_KIS_LCROER_SEND_IF')
    Left = 24
    Top = 232
    object qryAPP700CO_GBCD: TStringField
      FieldName = 'CO_GBCD'
      Size = 3
    end
    object qryAPP700IMPTNO: TStringField
      FieldName = 'IMPTNO'
    end
    object qryAPP700IMPTSQ: TIntegerField
      FieldName = 'IMPTSQ'
    end
    object qryAPP700USER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 15
    end
    object qryAPP700MESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 5
    end
    object qryAPP700MESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 5
    end
    object qryAPP700APP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 12
    end
    object qryAPP700CHK2: TStringField
      FieldName = 'CHK2'
      Size = 2
    end
    object qryAPP700CHK3: TStringField
      FieldName = 'CHK3'
      Size = 15
    end
    object qryAPP700DATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 12
    end
    object qryAPP700IN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 5
    end
    object qryAPP700AP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 6
    end
    object qryAPP700AP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 53
    end
    object qryAPP700AP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 53
    end
    object qryAPP700AP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 53
    end
    object qryAPP700AP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 53
    end
    object qryAPP700AP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 53
    end
    object qryAPP700AD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 6
    end
    object qryAPP700AD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 53
    end
    object qryAPP700AD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 53
    end
    object qryAPP700AD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 53
    end
    object qryAPP700AD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 53
    end
    object qryAPP700AD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 5
    end
    object qryAPP700IMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 5
    end
    object qryAPP700IMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 5
    end
    object qryAPP700IMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 5
    end
    object qryAPP700IMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 5
    end
    object qryAPP700IMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 5
    end
    object qryAPP700IL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 98
    end
    object qryAPP700IL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 98
    end
    object qryAPP700IL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 98
    end
    object qryAPP700IL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 98
    end
    object qryAPP700IL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 98
    end
    object qryAPP700IL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 5
    end
    object qryAPP700IL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 5
    end
    object qryAPP700IL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 5
    end
    object qryAPP700IL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 5
    end
    object qryAPP700IL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 5
    end
    object qryAPP700IL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 20
      Size = 0
    end
    object qryAPP700IL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 20
      Size = 0
    end
    object qryAPP700IL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 20
      Size = 0
    end
    object qryAPP700IL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 20
      Size = 0
    end
    object qryAPP700IL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 20
      Size = 0
    end
    object qryAPP700AD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 105
    end
    object qryAPP700AD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 105
    end
    object qryAPP700AD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 105
    end
    object qryAPP700AD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 105
    end
    object qryAPP700AD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 105
    end
    object qryAPP700EX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 53
    end
    object qryAPP700EX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 53
    end
    object qryAPP700EX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 53
    end
    object qryAPP700EX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 53
    end
    object qryAPP700EX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 53
    end
    object qryAPP700DOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 5
    end
    object qryAPP700EX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 12
    end
    object qryAPP700EX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 44
    end
    object qryAPP700APPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 53
    end
    object qryAPP700APPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 53
    end
    object qryAPP700APPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 53
    end
    object qryAPP700APPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 53
    end
    object qryAPP700APPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 38
    end
    object qryAPP700BENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 15
    end
    object qryAPP700BENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 53
    end
    object qryAPP700BENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 53
    end
    object qryAPP700BENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 53
    end
    object qryAPP700BENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 53
    end
    object qryAPP700BENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 53
    end
    object qryAPP700CD_AMT: TBCDField
      FieldName = 'CD_AMT'
      DisplayFormat = '#,0'
      Precision = 15
      Size = 0
    end
    object qryAPP700CD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 5
    end
    object qryAPP700CD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryAPP700CD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryAPP700CD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 5
    end
    object qryAPP700TERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 5
    end
    object qryAPP700TERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 98
    end
    object qryAPP700PL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 98
    end
    object qryAPP700AA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 53
    end
    object qryAPP700AA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 53
    end
    object qryAPP700AA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 53
    end
    object qryAPP700AA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 53
    end
    object qryAPP700DRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 53
    end
    object qryAPP700DRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 53
    end
    object qryAPP700DRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 53
    end
    object qryAPP700MIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 53
    end
    object qryAPP700MIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 53
    end
    object qryAPP700MIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 53
    end
    object qryAPP700MIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 53
    end
    object qryAPP700DEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 53
    end
    object qryAPP700DEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 53
    end
    object qryAPP700DEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 53
    end
    object qryAPP700DEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 53
    end
    object qryAPP700DESGOOD: TStringField
      FieldName = 'DESGOOD'
      Size = 5
    end
    object qryAPP700DESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryAPP700LST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 12
    end
    object qryAPP700SHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 98
    end
    object qryAPP700SHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 98
    end
    object qryAPP700SHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 98
    end
    object qryAPP700SHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 98
    end
    object qryAPP700SHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 98
    end
    object qryAPP700SHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 98
    end
    object qryAPP700PSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 5
    end
    object qryAPP700TSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 5
    end
    object qryAPP700CARRIAGE: TStringField
      FieldName = 'CARRIAGE'
      Size = 98
    end
    object qryAPP700SUNJUK_PORT: TStringField
      FieldName = 'SUNJUK_PORT'
      Size = 98
    end
    object qryAPP700DOCHAK_PORT: TStringField
      FieldName = 'DOCHAK_PORT'
      Size = 98
    end
    object qryAPP700LOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 98
    end
    object qryAPP700FOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 98
    end
    object qryAPP700ORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 5
    end
    object qryAPP700ORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 98
    end
    object qryAPP700DOC_380: TStringField
      FieldName = 'DOC_380'
      Size = 2
    end
    object qryAPP700DOC_380_1: TIntegerField
      FieldName = 'DOC_380_1'
    end
    object qryAPP700DOC_705_YN: TStringField
      FieldName = 'DOC_705_YN'
      Size = 2
    end
    object qryAPP700DOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 5
    end
    object qryAPP700DOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 53
    end
    object qryAPP700DOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 53
    end
    object qryAPP700DOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 5
    end
    object qryAPP700DOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 53
    end
    object qryAPP700DOC_740: TStringField
      FieldName = 'DOC_740'
      Size = 2
    end
    object qryAPP700DOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 53
    end
    object qryAPP700DOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 53
    end
    object qryAPP700DOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 5
    end
    object qryAPP700DOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 53
    end
    object qryAPP700DOC_760: TStringField
      FieldName = 'DOC_760'
      Size = 2
    end
    object qryAPP700DOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 53
    end
    object qryAPP700DOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 53
    end
    object qryAPP700DOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 5
    end
    object qryAPP700DOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 53
    end
    object qryAPP700DOC_530: TStringField
      FieldName = 'DOC_530'
      Size = 2
    end
    object qryAPP700DOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 98
    end
    object qryAPP700DOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 98
    end
    object qryAPP700DOC_271: TStringField
      FieldName = 'DOC_271'
      Size = 2
    end
    object qryAPP700DOC_271_1: TIntegerField
      FieldName = 'DOC_271_1'
    end
    object qryAPP700DOC_861: TStringField
      FieldName = 'DOC_861'
      Size = 2
    end
    object qryAPP700DOC_2AA: TStringField
      FieldName = 'DOC_2AA'
      Size = 3
    end
    object qryAPP700DOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
    end
    object qryAPP700ACD_2AA: TStringField
      FieldName = 'ACD_2AA'
      Size = 2
    end
    object qryAPP700ACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 53
    end
    object qryAPP700ACD_2AB: TStringField
      FieldName = 'ACD_2AB'
      Size = 2
    end
    object qryAPP700ACD_2AC: TStringField
      FieldName = 'ACD_2AC'
      Size = 2
    end
    object qryAPP700ACD_2AD: TStringField
      FieldName = 'ACD_2AD'
      Size = 2
    end
    object qryAPP700ACD_2AE: TStringField
      FieldName = 'ACD_2AE'
      Size = 3
    end
    object qryAPP700ACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
    end
    object qryAPP700CHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 5
    end
    object qryAPP700PERIOD: TIntegerField
      FieldName = 'PERIOD'
    end
    object qryAPP700CONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 5
    end
    object qryAPP700PERIOD_IDX: TStringField
      FieldName = 'PERIOD_IDX'
      Size = 1
    end
    object qryAPP700PERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 50
    end
    object qryAPP700CONFIRM_BANKNM: TStringField
      FieldName = 'CONFIRM_BANKNM'
      Size = 100
    end
    object qryAPP700CONFIRM_BANKBR: TStringField
      FieldName = 'CONFIRM_BANKBR'
      Size = 100
    end
    object qryAPP700CHARGE_1: TStringField
      FieldName = 'CHARGE_1'
      Size = 4000
    end
    object qryAPP700SPECIAL_PAY: TStringField
      FieldName = 'SPECIAL_PAY'
      Size = 4000
    end
    object qryAPP700FRST_REG_USER_ID: TStringField
      FieldName = 'FRST_REG_USER_ID'
      Size = 10
    end
    object qryAPP700FRST_REG_DTM: TDateTimeField
      FieldName = 'FRST_REG_DTM'
    end
    object qryAPP700LAST_MOD_USER_ID: TStringField
      FieldName = 'LAST_MOD_USER_ID'
      Size = 10
    end
    object qryAPP700LAST_MOD_DTM: TDateTimeField
      FieldName = 'LAST_MOD_DTM'
    end
  end
  object dsAPP700: TDataSource
    DataSet = qryAPP700
    Left = 56
    Top = 232
  end
  object qryAPP707: TADOQuery
    Connection = LivartConfig_frm.LivartConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM TC_KIS_LCRCHR_SEND_IF')
    Left = 24
    Top = 272
    object qryAPP707CO_GBCD: TStringField
      FieldName = 'CO_GBCD'
      Size = 3
    end
    object qryAPP707IMPTNO: TStringField
      FieldName = 'IMPTNO'
    end
    object qryAPP707IMPTSQ: TIntegerField
      FieldName = 'IMPTSQ'
    end
    object qryAPP707AMNDSQ: TIntegerField
      FieldName = 'AMNDSQ'
    end
    object qryAPP707MESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 5
    end
    object qryAPP707MESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 5
    end
    object qryAPP707CHK2: TStringField
      FieldName = 'CHK2'
      Size = 2
    end
    object qryAPP707CHK3: TStringField
      FieldName = 'CHK3'
      Size = 15
    end
    object qryAPP707DATEE: TStringField
      FieldName = 'DATEE'
      Size = 12
    end
    object qryAPP707APP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 12
    end
    object qryAPP707IN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 5
    end
    object qryAPP707AP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 6
    end
    object qryAPP707AP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 53
    end
    object qryAPP707AP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 53
    end
    object qryAPP707AP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 53
    end
    object qryAPP707AP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 53
    end
    object qryAPP707AP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 53
    end
    object qryAPP707AD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 6
    end
    object qryAPP707AD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 53
    end
    object qryAPP707AD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 53
    end
    object qryAPP707AD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 53
    end
    object qryAPP707AD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 53
    end
    object qryAPP707AD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 5
    end
    object qryAPP707IMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 5
    end
    object qryAPP707IMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 5
    end
    object qryAPP707IMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 5
    end
    object qryAPP707IMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 5
    end
    object qryAPP707IMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 5
    end
    object qryAPP707IL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 53
    end
    object qryAPP707IL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 53
    end
    object qryAPP707IL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 53
    end
    object qryAPP707IL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 53
    end
    object qryAPP707IL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 53
    end
    object qryAPP707IL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 14
      Size = 2
    end
    object qryAPP707IL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 14
      Size = 2
    end
    object qryAPP707IL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 14
      Size = 2
    end
    object qryAPP707IL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 14
      Size = 2
    end
    object qryAPP707IL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 14
      Size = 2
    end
    object qryAPP707IL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 5
    end
    object qryAPP707IL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 5
    end
    object qryAPP707IL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 5
    end
    object qryAPP707IL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 5
    end
    object qryAPP707IL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 5
    end
    object qryAPP707EX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 53
    end
    object qryAPP707EX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 53
    end
    object qryAPP707EX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 53
    end
    object qryAPP707AD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 53
    end
    object qryAPP707AD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 105
    end
    object qryAPP707AD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 105
    end
    object qryAPP707AD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 105
    end
    object qryAPP707AD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 105
    end
    object qryAPP707CD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 53
    end
    object qryAPP707ISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 12
    end
    object qryAPP707APPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 53
    end
    object qryAPP707APPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 53
    end
    object qryAPP707APPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 53
    end
    object qryAPP707APPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 53
    end
    object qryAPP707APPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 38
    end
    object qryAPP707BENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 15
    end
    object qryAPP707BENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 53
    end
    object qryAPP707BENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 53
    end
    object qryAPP707BENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 53
    end
    object qryAPP707BENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 53
    end
    object qryAPP707BENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 53
    end
    object qryAPP707EX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 12
    end
    object qryAPP707INCD_CUR: TStringField
      FieldName = 'INCD_CUR'
      Size = 5
    end
    object qryAPP707INCD_AMT: TBCDField
      FieldName = 'INCD_AMT'
      Precision = 15
      Size = 0
    end
    object qryAPP707DECD_CUR: TStringField
      FieldName = 'DECD_CUR'
      Size = 5
    end
    object qryAPP707DECD_AMT: TBCDField
      FieldName = 'DECD_AMT'
      Precision = 15
      Size = 0
    end
    object qryAPP707NWCD_CUR: TStringField
      FieldName = 'NWCD_CUR'
      Size = 5
    end
    object qryAPP707NWCD_AMT: TBCDField
      FieldName = 'NWCD_AMT'
      Precision = 15
      Size = 0
    end
    object qryAPP707BFCD_CUR: TStringField
      FieldName = 'BFCD_CUR'
      Size = 5
    end
    object qryAPP707BFCD_AMT: TBCDField
      FieldName = 'BFCD_AMT'
      Precision = 15
      Size = 0
    end
    object qryAPP707CD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryAPP707CD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryAPP707CD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 5
    end
    object qryAPP707AA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 53
    end
    object qryAPP707AA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 53
    end
    object qryAPP707AA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 53
    end
    object qryAPP707AA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 53
    end
    object qryAPP707SUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 98
    end
    object qryAPP707DOCHAK_PORT: TStringField
      FieldName = 'DOCHAK_PORT'
      Size = 98
    end
    object qryAPP707LOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 105
    end
    object qryAPP707FOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 105
    end
    object qryAPP707LST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 12
    end
    object qryAPP707SHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 98
    end
    object qryAPP707SHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 98
    end
    object qryAPP707SHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 98
    end
    object qryAPP707SHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 98
    end
    object qryAPP707SHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 98
    end
    object qryAPP707SHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 98
    end
    object qryAPP707CARRIAGE: TStringField
      FieldName = 'CARRIAGE'
      Size = 98
    end
    object qryAPP707NARRAT: TStringField
      FieldName = 'NARRAT'
      Size = 3
    end
    object qryAPP707NARRAT_1: TMemoField
      FieldName = 'NARRAT_1'
      BlobType = ftMemo
    end
    object qryAPP707EX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 53
    end
    object qryAPP707EX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 53
    end
    object qryAPP707IS_CANCEL: TStringField
      FieldName = 'IS_CANCEL'
      Size = 10
    end
    object qryAPP707APPLIC_CHG1: TStringField
      FieldName = 'APPLIC_CHG1'
      Size = 50
    end
    object qryAPP707APPLIC_CHG2: TStringField
      FieldName = 'APPLIC_CHG2'
      Size = 50
    end
    object qryAPP707APPLIC_CHG3: TStringField
      FieldName = 'APPLIC_CHG3'
      Size = 50
    end
    object qryAPP707APPLIC_CHG4: TStringField
      FieldName = 'APPLIC_CHG4'
      Size = 50
    end
    object qryAPP707EXPLOC: TStringField
      FieldName = 'EXPLOC'
      Size = 50
    end
    object qryAPP707CONFIRM_BANKNM: TStringField
      FieldName = 'CONFIRM_BANKNM'
      Size = 100
    end
    object qryAPP707CONFIRM_BANKBR: TStringField
      FieldName = 'CONFIRM_BANKBR'
      Size = 100
    end
    object qryAPP707CHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryAPP707CHARGE_1: TStringField
      FieldName = 'CHARGE_1'
      Size = 4000
    end
    object qryAPP707AMD_CHARGE: TStringField
      FieldName = 'AMD_CHARGE'
      Size = 3
    end
    object qryAPP707AMD_CHARGE_1: TStringField
      FieldName = 'AMD_CHARGE_1'
      Size = 4000
    end
    object qryAPP707SPECIAL_PAY: TStringField
      FieldName = 'SPECIAL_PAY'
      Size = 4000
    end
    object qryAPP707GOODS_DESC: TStringField
      FieldName = 'GOODS_DESC'
      Size = 4000
    end
    object qryAPP707DOC_REQ: TStringField
      FieldName = 'DOC_REQ'
      Size = 4000
    end
    object qryAPP707ADD_CONDITION: TStringField
      FieldName = 'ADD_CONDITION'
      Size = 4000
    end
    object qryAPP707PERIOD_DAYS: TIntegerField
      FieldName = 'PERIOD_DAYS'
    end
    object qryAPP707PERIOD_IDX: TStringField
      FieldName = 'PERIOD_IDX'
      Size = 1
    end
    object qryAPP707PERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 50
    end
    object qryAPP707FRST_REG_USER_ID: TStringField
      FieldName = 'FRST_REG_USER_ID'
      Size = 10
    end
    object qryAPP707FRST_REG_DTM: TDateTimeField
      FieldName = 'FRST_REG_DTM'
    end
    object qryAPP707LAST_MOD_USER_ID: TStringField
      FieldName = 'LAST_MOD_USER_ID'
      Size = 10
    end
    object qryAPP707LAST_MOD_DTM: TDateTimeField
      FieldName = 'LAST_MOD_DTM'
    end
    object qryAPP707PSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 2
    end
    object qryAPP707TSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 2
    end
    object qryAPP707CONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object qryAPP707MIX1: TStringField
      FieldName = 'MIX1'
      Size = 50
    end
    object qryAPP707MIX2: TStringField
      FieldName = 'MIX2'
      Size = 50
    end
    object qryAPP707MIX3: TStringField
      FieldName = 'MIX3'
      Size = 50
    end
    object qryAPP707MIX4: TStringField
      FieldName = 'MIX4'
      Size = 50
    end
    object qryAPP707DRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 50
    end
    object qryAPP707DRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 50
    end
    object qryAPP707DRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 50
    end
    object qryAPP707DEFPAY1: TStringField
      FieldName = 'DEFPAY1'
      Size = 50
    end
    object qryAPP707DEFPAY2: TStringField
      FieldName = 'DEFPAY2'
      Size = 50
    end
    object qryAPP707DEFPAY3: TStringField
      FieldName = 'DEFPAY3'
      Size = 50
    end
    object qryAPP707DEFPAY4: TStringField
      FieldName = 'DEFPAY4'
      Size = 50
    end
  end
  object dsAPP707: TDataSource
    DataSet = qryAPP707
    Left = 56
    Top = 272
  end
  object qryDOANTC: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM DOANTC')
    Left = 24
    Top = 304
    object qryDOANTCMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryDOANTCUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryDOANTCDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryDOANTCMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryDOANTCMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryDOANTCAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryDOANTCAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryDOANTCAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryDOANTCAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryDOANTCBANK_CODE: TStringField
      FieldName = 'BANK_CODE'
      Size = 10
    end
    object qryDOANTCBANK1: TStringField
      FieldName = 'BANK1'
      Size = 70
    end
    object qryDOANTCBANK2: TStringField
      FieldName = 'BANK2'
      Size = 70
    end
    object qryDOANTCLC_G: TStringField
      FieldName = 'LC_G'
      Size = 3
    end
    object qryDOANTCLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryDOANTCBL_G: TStringField
      FieldName = 'BL_G'
      Size = 3
    end
    object qryDOANTCBL_NO: TStringField
      FieldName = 'BL_NO'
      Size = 35
    end
    object qryDOANTCAMT: TBCDField
      FieldName = 'AMT'
      Precision = 18
    end
    object qryDOANTCAMTC: TStringField
      FieldName = 'AMTC'
      Size = 3
    end
    object qryDOANTCCHRG: TBCDField
      FieldName = 'CHRG'
      Precision = 18
    end
    object qryDOANTCCHRGC: TStringField
      FieldName = 'CHRGC'
      Size = 3
    end
    object qryDOANTCRES_DATE: TStringField
      FieldName = 'RES_DATE'
      Size = 8
    end
    object qryDOANTCSET_DATE: TStringField
      FieldName = 'SET_DATE'
      Size = 8
    end
    object qryDOANTCREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryDOANTCBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 35
    end
    object qryDOANTCBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 35
    end
    object qryDOANTCBK_NAME3: TStringField
      FieldName = 'BK_NAME3'
      Size = 35
    end
    object qryDOANTCCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryDOANTCCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryDOANTCCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryDOANTCPRNO: TIntegerField
      FieldName = 'PRNO'
    end
  end
  object dsDOANTC: TDataSource
    DataSet = qryDOANTC
    Left = 56
    Top = 304
  end
  object qryINF700: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT * FROM INF700_1 I1 LEFT JOIN INF700_2 ON I1.MAINT_NO = IN' +
        'F700_2.MAINT_NO'
      
        '                          LEFT JOIN INF700_3 ON I1.MAINT_NO = IN' +
        'F700_3.MAINT_NO'
      
        '                          LEFT JOIN INF700_4 ON I1.MAINT_NO = IN' +
        'F700_4.MAINT_NO')
    Left = 24
    Top = 336
    object qryINF700MAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryINF700MESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryINF700MESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryINF700USER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 15
    end
    object qryINF700DATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryINF700APP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryINF700IN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryINF700AP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryINF700AP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryINF700AP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryINF700AP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryINF700AP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryINF700AP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryINF700AD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryINF700AD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryINF700AD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryINF700AD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryINF700AD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryINF700AD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object qryINF700IL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryINF700IL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryINF700IL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryINF700IL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryINF700IL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryINF700IL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryINF700IL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryINF700IL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryINF700IL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryINF700IL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryINF700IL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryINF700IL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryINF700IL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryINF700IL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryINF700IL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryINF700AD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryINF700AD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryINF700AD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryINF700AD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryINF700AD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryINF700DOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object qryINF700CD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryINF700REF_PRE: TStringField
      FieldName = 'REF_PRE'
      Size = 35
    end
    object qryINF700ISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryINF700EX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryINF700EX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryINF700CHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryINF700CHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryINF700CHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryINF700prno: TIntegerField
      FieldName = 'prno'
    end
    object qryINF700F_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryINF700IMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryINF700IMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryINF700IMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryINF700IMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryINF700IMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryINF700MAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryINF700APP_BANK: TStringField
      FieldName = 'APP_BANK'
      Size = 11
    end
    object qryINF700APP_BANK1: TStringField
      FieldName = 'APP_BANK1'
      Size = 35
    end
    object qryINF700APP_BANK2: TStringField
      FieldName = 'APP_BANK2'
      Size = 35
    end
    object qryINF700APP_BANK3: TStringField
      FieldName = 'APP_BANK3'
      Size = 35
    end
    object qryINF700APP_BANK4: TStringField
      FieldName = 'APP_BANK4'
      Size = 35
    end
    object qryINF700APP_BANK5: TStringField
      FieldName = 'APP_BANK5'
      Size = 35
    end
    object qryINF700APP_ACCNT: TStringField
      FieldName = 'APP_ACCNT'
      Size = 35
    end
    object qryINF700APPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryINF700APPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryINF700APPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryINF700APPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryINF700APPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryINF700BENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryINF700BENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryINF700BENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryINF700BENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryINF700BENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryINF700CD_AMT: TBCDField
      FieldName = 'CD_AMT'
      Precision = 18
    end
    object qryINF700CD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryINF700CD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryINF700CD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryINF700CD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryINF700AA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryINF700AA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryINF700AA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryINF700AA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryINF700AVAIL: TStringField
      FieldName = 'AVAIL'
      Size = 11
    end
    object qryINF700AVAIL1: TStringField
      FieldName = 'AVAIL1'
      Size = 35
    end
    object qryINF700AVAIL2: TStringField
      FieldName = 'AVAIL2'
      Size = 35
    end
    object qryINF700AVAIL3: TStringField
      FieldName = 'AVAIL3'
      Size = 35
    end
    object qryINF700AVAIL4: TStringField
      FieldName = 'AVAIL4'
      Size = 35
    end
    object qryINF700AV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Size = 35
    end
    object qryINF700AV_PAY: TStringField
      FieldName = 'AV_PAY'
      Size = 35
    end
    object qryINF700DRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryINF700DRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryINF700DRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryINF700DRAWEE: TStringField
      FieldName = 'DRAWEE'
      Size = 11
    end
    object qryINF700DRAWEE1: TStringField
      FieldName = 'DRAWEE1'
      Size = 35
    end
    object qryINF700DRAWEE2: TStringField
      FieldName = 'DRAWEE2'
      Size = 35
    end
    object qryINF700DRAWEE3: TStringField
      FieldName = 'DRAWEE3'
      Size = 35
    end
    object qryINF700DRAWEE4: TStringField
      FieldName = 'DRAWEE4'
      Size = 35
    end
    object qryINF700DR_ACCNT: TStringField
      FieldName = 'DR_ACCNT'
      Size = 35
    end
    object qryINF700MAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryINF700PSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object qryINF700TSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object qryINF700LOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryINF700FOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryINF700LST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryINF700SHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object qryINF700SHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryINF700SHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryINF700SHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryINF700SHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryINF700SHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryINF700SHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryINF700DESGOOD: TBooleanField
      FieldName = 'DESGOOD'
    end
    object qryINF700DESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryINF700TERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object qryINF700TERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object qryINF700PL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object qryINF700ORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object qryINF700ORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object qryINF700DOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object qryINF700DOC_380_1: TBCDField
      FieldName = 'DOC_380_1'
      Precision = 18
    end
    object qryINF700DOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object qryINF700DOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object qryINF700DOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object qryINF700DOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object qryINF700DOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object qryINF700DOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object qryINF700DOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object qryINF700DOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object qryINF700DOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object qryINF700DOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object qryINF700DOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object qryINF700DOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object qryINF700DOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object qryINF700DOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object qryINF700DOC_271_1: TBCDField
      FieldName = 'DOC_271_1'
      Precision = 18
    end
    object qryINF700DOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object qryINF700DOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object qryINF700DOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
    end
    object qryINF700ACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object qryINF700ACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object qryINF700ACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object qryINF700ACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object qryINF700ACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object qryINF700ACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object qryINF700ACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
    end
    object qryINF700CHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryINF700PERIOD: TBCDField
      FieldName = 'PERIOD'
      Precision = 18
    end
    object qryINF700CONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object qryINF700DEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryINF700DEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryINF700DEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryINF700DEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryINF700DOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object qryINF700MAINT_NO_3: TStringField
      FieldName = 'MAINT_NO_3'
      Size = 35
    end
    object qryINF700REI_BANK: TStringField
      FieldName = 'REI_BANK'
      Size = 11
    end
    object qryINF700REI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Size = 35
    end
    object qryINF700REI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Size = 35
    end
    object qryINF700REI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Size = 35
    end
    object qryINF700REI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Size = 35
    end
    object qryINF700REI_BANK5: TStringField
      FieldName = 'REI_BANK5'
      Size = 35
    end
    object qryINF700REI_ACNNT: TStringField
      FieldName = 'REI_ACNNT'
      Size = 35
    end
    object qryINF700INSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object qryINF700INSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryINF700AVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Size = 11
    end
    object qryINF700AVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Size = 35
    end
    object qryINF700AVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Size = 35
    end
    object qryINF700AVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Size = 35
    end
    object qryINF700AVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Size = 35
    end
    object qryINF700AVT_BANK5: TStringField
      FieldName = 'AVT_BANK5'
      Size = 35
    end
    object qryINF700AVT_ACCNT: TStringField
      FieldName = 'AVT_ACCNT'
      Size = 35
    end
    object qryINF700SND_INFO1: TStringField
      FieldName = 'SND_INFO1'
      Size = 35
    end
    object qryINF700SND_INFO2: TStringField
      FieldName = 'SND_INFO2'
      Size = 35
    end
    object qryINF700SND_INFO3: TStringField
      FieldName = 'SND_INFO3'
      Size = 35
    end
    object qryINF700SND_INFO4: TStringField
      FieldName = 'SND_INFO4'
      Size = 35
    end
    object qryINF700SND_INFO5: TStringField
      FieldName = 'SND_INFO5'
      Size = 35
    end
    object qryINF700SND_INFO6: TStringField
      FieldName = 'SND_INFO6'
      Size = 35
    end
    object qryINF700EX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryINF700EX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryINF700EX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryINF700EX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryINF700EX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryINF700EX_ADDR3: TStringField
      FieldName = 'EX_ADDR3'
      Size = 35
    end
    object qryINF700OP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Size = 35
    end
    object qryINF700OP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Size = 35
    end
    object qryINF700OP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Size = 35
    end
    object qryINF700OP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Size = 35
    end
    object qryINF700OP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Size = 35
    end
    object qryINF700OP_ADDR3: TStringField
      FieldName = 'OP_ADDR3'
      Size = 35
    end
    object qryINF700MIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryINF700MIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryINF700MIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryINF700MIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryINF700APPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Size = 30
    end
    object qryINF700APPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Size = 35
    end
    object qryINF700DOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object qryINF700DOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object qryINF700DOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object qryINF700DOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object qryINF700DOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object qryINF700SUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryINF700DOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryINF700AVAIL_BIC: TStringField
      FieldName = 'AVAIL_BIC'
      Size = 11
    end
    object qryINF700DRAWEE_BIC: TStringField
      FieldName = 'DRAWEE_BIC'
      Size = 11
    end
    object qryINF700CHARGE_1: TMemoField
      FieldName = 'CHARGE_1'
      BlobType = ftMemo
    end
    object qryINF700CONFIRM_BICCD: TStringField
      FieldName = 'CONFIRM_BICCD'
      Size = 11
    end
    object qryINF700CONFIRM_BANKNM: TStringField
      FieldName = 'CONFIRM_BANKNM'
      Size = 70
    end
    object qryINF700CONFIRM_BANKBR: TStringField
      FieldName = 'CONFIRM_BANKBR'
      Size = 70
    end
    object qryINF700PERIOD_IDX: TIntegerField
      FieldName = 'PERIOD_IDX'
    end
    object qryINF700PERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 35
    end
    object qryINF700SPECIAL_PAY: TMemoField
      FieldName = 'SPECIAL_PAY'
      BlobType = ftMemo
    end
  end
  object dsINF700: TDataSource
    DataSet = qryINF700
    Left = 56
    Top = 336
  end
  object qryINF707: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT * FROM INF707_1 I1 INNER JOIN INF707_2 I2 ON I1.MAINT_NO ' +
        '= I2.MAINT_NO AND I1.MSEQ = I2.MSEQ')
    Left = 24
    Top = 368
    object qryINF707MAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryINF707MSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryINF707AMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryINF707MESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryINF707MESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryINF707USER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryINF707DATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryINF707APP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryINF707IN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryINF707AP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryINF707AP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryINF707AP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryINF707AP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryINF707AP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryINF707AP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryINF707AD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryINF707AD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryINF707AD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryINF707AD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryINF707AD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryINF707IL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryINF707IL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryINF707IL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryINF707IL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryINF707IL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryINF707IL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryINF707IL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryINF707IL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryINF707IL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryINF707IL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryINF707IL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryINF707IL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryINF707IL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryINF707IL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryINF707IL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryINF707AD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryINF707AD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryINF707AD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryINF707AD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryINF707AD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryINF707CD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryINF707RCV_REF: TStringField
      FieldName = 'RCV_REF'
      Size = 35
    end
    object qryINF707IBANK_REF: TStringField
      FieldName = 'IBANK_REF'
      Size = 35
    end
    object qryINF707ISS_BANK1: TStringField
      FieldName = 'ISS_BANK1'
      Size = 35
    end
    object qryINF707ISS_BANK2: TStringField
      FieldName = 'ISS_BANK2'
      Size = 35
    end
    object qryINF707ISS_BANK3: TStringField
      FieldName = 'ISS_BANK3'
      Size = 35
    end
    object qryINF707ISS_BANK4: TStringField
      FieldName = 'ISS_BANK4'
      Size = 35
    end
    object qryINF707ISS_BANK5: TStringField
      FieldName = 'ISS_BANK5'
      Size = 35
    end
    object qryINF707ISS_ACCNT: TStringField
      FieldName = 'ISS_ACCNT'
      Size = 35
    end
    object qryINF707ISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryINF707AMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      Size = 8
    end
    object qryINF707EX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryINF707EX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryINF707CHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryINF707CHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryINF707CHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryINF707F_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryINF707IMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryINF707IMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryINF707IMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryINF707IMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryINF707IMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryINF707Prno: TIntegerField
      FieldName = 'Prno'
    end
    object qryINF707APPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryINF707APPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryINF707APPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryINF707APPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryINF707APPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryINF707BENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryINF707BENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryINF707BENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryINF707BENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryINF707BENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryINF707INCD_CUR: TStringField
      FieldName = 'INCD_CUR'
      Size = 3
    end
    object qryINF707INCD_AMT: TBCDField
      FieldName = 'INCD_AMT'
      Precision = 18
    end
    object qryINF707DECD_CUR: TStringField
      FieldName = 'DECD_CUR'
      Size = 3
    end
    object qryINF707DECD_AMT: TBCDField
      FieldName = 'DECD_AMT'
      Precision = 18
    end
    object qryINF707NWCD_CUR: TStringField
      FieldName = 'NWCD_CUR'
      Size = 3
    end
    object qryINF707NWCD_AMT: TBCDField
      FieldName = 'NWCD_AMT'
      Precision = 18
    end
    object qryINF707CD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryINF707CD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryINF707CD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryINF707AA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryINF707AA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryINF707AA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryINF707AA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryINF707LOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryINF707FOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryINF707LST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryINF707SHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object qryINF707SHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryINF707SHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryINF707SHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryINF707SHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryINF707SHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryINF707SHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryINF707NARRAT: TBooleanField
      FieldName = 'NARRAT'
    end
    object qryINF707NARRAT_1: TMemoField
      FieldName = 'NARRAT_1'
      BlobType = ftMemo
    end
    object qryINF707SR_INFO1: TStringField
      FieldName = 'SR_INFO1'
      Size = 35
    end
    object qryINF707SR_INFO2: TStringField
      FieldName = 'SR_INFO2'
      Size = 35
    end
    object qryINF707SR_INFO3: TStringField
      FieldName = 'SR_INFO3'
      Size = 35
    end
    object qryINF707SR_INFO4: TStringField
      FieldName = 'SR_INFO4'
      Size = 35
    end
    object qryINF707SR_INFO5: TStringField
      FieldName = 'SR_INFO5'
      Size = 35
    end
    object qryINF707SR_INFO6: TStringField
      FieldName = 'SR_INFO6'
      Size = 35
    end
    object qryINF707EX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryINF707EX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryINF707EX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryINF707EX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryINF707EX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryINF707OP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Size = 35
    end
    object qryINF707OP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Size = 35
    end
    object qryINF707OP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Size = 35
    end
    object qryINF707OP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Size = 35
    end
    object qryINF707OP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Size = 35
    end
    object qryINF707BFCD_AMT: TBCDField
      FieldName = 'BFCD_AMT'
      Precision = 18
    end
    object qryINF707BFCD_CUR: TStringField
      FieldName = 'BFCD_CUR'
      Size = 3
    end
    object qryINF707SUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryINF707DOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryINF707IS_CANCEL: TStringField
      FieldName = 'IS_CANCEL'
      Size = 10
    end
    object qryINF707DOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 2
    end
    object qryINF707PSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 2
    end
    object qryINF707TSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 2
    end
    object qryINF707MAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryINF707MSEQ_1: TIntegerField
      FieldName = 'MSEQ_1'
    end
    object qryINF707AMD_NO_1: TIntegerField
      FieldName = 'AMD_NO_1'
    end
    object qryINF707CHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 4
    end
    object qryINF707CHARGE_1: TMemoField
      FieldName = 'CHARGE_1'
      BlobType = ftMemo
    end
    object qryINF707AMD_CHARGE: TStringField
      FieldName = 'AMD_CHARGE'
      Size = 4
    end
    object qryINF707AMD_CHARGE_1: TMemoField
      FieldName = 'AMD_CHARGE_1'
      BlobType = ftMemo
    end
    object qryINF707SPECIAL_PAY: TMemoField
      FieldName = 'SPECIAL_PAY'
      BlobType = ftMemo
    end
    object qryINF707GOODS_DESC: TMemoField
      FieldName = 'GOODS_DESC'
      BlobType = ftMemo
    end
    object qryINF707DOC_REQ: TMemoField
      FieldName = 'DOC_REQ'
      BlobType = ftMemo
    end
    object qryINF707ADD_CONDITION: TMemoField
      FieldName = 'ADD_CONDITION'
      BlobType = ftMemo
    end
    object qryINF707DRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryINF707DRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryINF707DRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryINF707MIX1: TStringField
      FieldName = 'MIX1'
      Size = 35
    end
    object qryINF707MIX2: TStringField
      FieldName = 'MIX2'
      Size = 35
    end
    object qryINF707MIX3: TStringField
      FieldName = 'MIX3'
      Size = 35
    end
    object qryINF707MIX4: TStringField
      FieldName = 'MIX4'
      Size = 35
    end
    object qryINF707DEFPAY1: TStringField
      FieldName = 'DEFPAY1'
      Size = 35
    end
    object qryINF707DEFPAY2: TStringField
      FieldName = 'DEFPAY2'
      Size = 35
    end
    object qryINF707DEFPAY3: TStringField
      FieldName = 'DEFPAY3'
      Size = 35
    end
    object qryINF707DEFPAY4: TStringField
      FieldName = 'DEFPAY4'
      Size = 35
    end
    object qryINF707PERIOD_DAYS: TIntegerField
      FieldName = 'PERIOD_DAYS'
    end
    object qryINF707PERIOD_IDX: TIntegerField
      FieldName = 'PERIOD_IDX'
    end
    object qryINF707PERIOD_DETAIL: TStringField
      FieldName = 'PERIOD_DETAIL'
      Size = 35
    end
    object qryINF707CONFIRM: TStringField
      FieldName = 'CONFIRM'
      Size = 2
    end
    object qryINF707CONFIRM_BIC: TStringField
      FieldName = 'CONFIRM_BIC'
      Size = 11
    end
    object qryINF707CONFIRM1: TStringField
      FieldName = 'CONFIRM1'
      Size = 35
    end
    object qryINF707CONFIRM2: TStringField
      FieldName = 'CONFIRM2'
      Size = 35
    end
    object qryINF707CONFIRM3: TStringField
      FieldName = 'CONFIRM3'
      Size = 35
    end
    object qryINF707CONFIRM4: TStringField
      FieldName = 'CONFIRM4'
      Size = 35
    end
    object qryINF707TXT_78: TMemoField
      FieldName = 'TXT_78'
      BlobType = ftMemo
    end
    object qryINF707APPLIC_CHG1: TStringField
      FieldName = 'APPLIC_CHG1'
      Size = 35
    end
    object qryINF707APPLIC_CHG2: TStringField
      FieldName = 'APPLIC_CHG2'
      Size = 35
    end
    object qryINF707APPLIC_CHG3: TStringField
      FieldName = 'APPLIC_CHG3'
      Size = 35
    end
    object qryINF707APPLIC_CHG4: TStringField
      FieldName = 'APPLIC_CHG4'
      Size = 35
    end
  end
  object dsINF707: TDataSource
    DataSet = qryINF707
    Left = 56
    Top = 368
  end
  object qryTest: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 528
    Top = 344
  end
  object ADOQuery1: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 528
    Top = 384
  end
  object ADOQuery2: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 528
    Top = 416
  end
  object DataSource2: TDataSource
    DataSet = ADOQuery2
    Left = 560
    Top = 416
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 560
    Top = 384
  end
  object dsTest: TDataSource
    DataSet = qryTest
    Left = 560
    Top = 344
  end
end
