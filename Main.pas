unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sSkinProvider, sSkinManager, acTitleBar, Menus, ComCtrls,
  sTrackBar,TypeDefine, StdCtrls, Buttons, sBitBtn, sStatusBar, sComboBox,
  sButton, QuickRpt, pngimage, ExtCtrls, jpeg, Clipbrd, StrUtils, ShellAPI,
  acImage;

type
  TMain_frm = class(TForm)
    sTitleBar1: TsTitleBar;
    MainMenu1: TMainMenu;
    d1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    d2: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    d3: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    d4: TMenuItem;
    N30: TMenuItem;
    N31: TMenuItem;
    d5: TMenuItem;
    N32: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    d6: TMenuItem;
    L1: TMenuItem;
    N35: TMenuItem;
    d7: TMenuItem;
    L2: TMenuItem;
    N36: TMenuItem;
    APP7001: TMenuItem;
    N37: TMenuItem;
    d8: TMenuItem;
    APP7071: TMenuItem;
    d9: TMenuItem;
    N8: TMenuItem;
    N38: TMenuItem;
    GENRER1: TMenuItem;
    r1: TMenuItem;
    N39: TMenuItem;
    N40: TMenuItem;
    N41: TMenuItem;
    sStatusBar1: TsStatusBar;
    sButton1: TsButton;
    sComboBox1: TsComboBox;
    Image1: TImage;
    d10: TMenuItem;
    N42: TMenuItem;
    N43: TMenuItem;
    a1: TMenuItem;
    N45: TMenuItem;
    DOMOFC1: TMenuItem;
    N46: TMenuItem;
    t1: TMenuItem;
    t2: TMenuItem;
    N47: TMenuItem;
    N48: TMenuItem;
    t3: TMenuItem;
    T4: TMenuItem;
    T5: TMenuItem;
    N49: TMenuItem;
    N50: TMenuItem;
    t6: TMenuItem;
    L3: TMenuItem;
    N51: TMenuItem;
    LOGUARF1: TMenuItem;
    PopupMenu1: TPopupMenu;
    w1: TMenuItem;
    d11: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    sSkinProvider1: TsSkinProvider;
    d12: TMenuItem;
    d13: TMenuItem;
    N19: TMenuItem;
    a2: TMenuItem;
    N44: TMenuItem;
    N52: TMenuItem;
    N53: TMenuItem;
    N54: TMenuItem;
    s1: TMenuItem;
    s2: TMenuItem;
    N55: TMenuItem;
    N58: TMenuItem;
    LOCAM11: TMenuItem;
    N60: TMenuItem;
    APPPCR1: TMenuItem;
    PCRLIC1: TMenuItem;
    D14: TMenuItem;
    N63: TMenuItem;
    LOCRCT1: TMenuItem;
    N65: TMenuItem;
    d15: TMenuItem;
    APPSPC1: TMenuItem;
    SPCNTC1: TMenuItem;
    N56: TMenuItem;
    N57: TMenuItem;
    N59: TMenuItem;
    N61: TMenuItem;
    N62: TMenuItem;
    d16: TMenuItem;
    N64: TMenuItem;
    w2: TMenuItem;
    N66: TMenuItem;
    d17: TMenuItem;
    N67: TMenuItem;
    W3: TMenuItem;
    d18: TMenuItem;
    d19: TMenuItem;
    N68: TMenuItem;
    N70: TMenuItem;
    N69: TMenuItem;
    d20: TMenuItem;
    v1: TMenuItem;
    N71: TMenuItem;
    d21: TMenuItem;
    d22: TMenuItem;
    procedure N1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MenuClick(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure d10Click(Sender: TObject);
    procedure N43Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure w1Click(Sender: TObject);
    procedure W3Click(Sender: TObject);
    procedure N68Click(Sender: TObject);
    procedure v1Click(Sender: TObject);

  private
    function CreateChildForm(iMenuID: Integer): TForm;
    procedure ShowChildForm(myChildForm: TForm);
    procedure setProgramList;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Main_frm: TMain_frm;
  
implementation

uses Config_MSSQL, VarDefine, CustomerAdminMSSQL, CODE_MSSQL,
  APPPCR_N_MSSQL, PCRLIC_N_MSSQL, NormalRecv_MSSQL,
  DocumentRecv, DocumentSend, PCRLIC_PRINT, PCRLIC_PRINT2, FINBIL_N_MSSQL,
  CodeContents, Dlg_Upgrade, RecvDocumentsV1, ICON,
  Dialog_FixedNo, UI_APP700, UI_INF700, UI_APP707, UI_INF707,
  HSCODE_MSSQL, UI_DOMOFC, UI_DOMOFB, UI_LOCRC2, UI_LOCRC1,
  UI_VATBI3, UI_SPCNTC, UI_VATBI1, UI_VATBI4, UI_DOANTC, UI_ADV700, Login, PERSON_MSSQL,
  UI_ADV707,
  //NEW FORM
  UI_DOMOFC_NEW, UI_DOMOFB_NEW, UI_LOCAPP_NEW, UI_LOCADV_NEW, UI_LOCAMR_NEW, UI_LOCAMA_NEW, UI_APP700_NEW, UI_APP707_NEW, UI_ADV700_NEW, UI_INF700_NEW, UI_INF707_NEW, KISCalendarV2, KISCalendar, UI_DOANTC_NEW, dlg_AUTOINC, UI_CREADV_NEW, dlg_SendCustomer, KISEncryption, UI_LOCRC2_NEW, UI_ITEM, UI_BANKCODE, UI_MEMO, UI_ATTNTC_NEW, UI_RUNSQL, UI_LDANTC_NEW, UI_SPCNTC_NEW, UI_VATBI2_NEW,
  UI_LOCAD1_NEW, UI_LOCAM1_NEW, UI_APPLOG_NEW, UI_LOGUAR_NEW, UI_DEBADV_NEW, UI_APPSPC_NEW, UI_PAYORD_NEW, UI_APPCOP_NEW, UI_APPPCR_NEW, UI_PCRLIC_NEW, UI_ADV707_NEW, UI_LOCRC1_NEW, UI_ADV710_NEW, UI_APPRMI_NEW, UI_BANKCODE_SWIFT;

{$R *.dfm}

procedure TMain_frm.N1Click(Sender: TObject);
begin
  Config_MSSQL_frm := TConfig_MSSQL_frm.Create(Self);
  Config_MSSQL_frm.ShowModal;
  FreeAndNil(Config_MSSQL_frm);
end;

procedure TMain_frm.FormShow(Sender: TObject);
begin
  ConnectionType := ctMSSQL;

  //LoginData.sID = '3';

  ShowWindow(Self.Handle, SW_SHOW);

  Login_frm := TLogin_frm.Create(Application);
  Login_frm.ShowModal;

  if UpperCase(LoginData.sID) = 'SYSTEM'  then N49.Visible := True;

  Self.Caption := '외환상역 - '

end;

procedure TMain_frm.MenuClick(Sender: TObject);
var
  myChildForm: TForm;
begin
  DMCodeContents.CODEREFRESH;
  if DMCodeContents.Config.RecordCount = 0 Then
    raise Exception.Create('사용자 환경설정이 입력되어있지 않습니다'#13#10+'[기본설정]-[환경설정]');

  FProgramName := (Sender as TMenuItem).Name;
  myChildForm := CreateChildForm((Sender as TMenuItem).Tag);
  if Assigned(myChildForm) then  ShowChildForm(myChildForm)

  else                           //MessageDlg('해당 기능은 곧 지원될 예정입니다.', mtConfirmation, [mbYes], 0);
end;

function TMain_frm.CreateChildForm(iMenuID: Integer): TForm;
begin
  Result := nil;

  case iMenuID of
    //거래처코드
    100 : begin
            if not Assigned(CustomerAdminMSSQL_frm) then
              CustomerAdminMSSQL_frm := TCustomerAdminMSSQL_frm.Create(Application);
            Result := CustomerAdminMSSQL_frm;
          end;
    //은행코드
    101 : begin
            if not Assigned(UI_BANKCODE_frm) then
            UI_BANKCODE_frm := TUI_BANKCODE_frm.Create(Application);
            Result := UI_BANKCODE_frm;
//            if not Assigned(Bank_MSSQL_frm) then
//              Bank_MSSQL_frm := TBank_MSSQL_frm.Create(Application);
//            Result := Bank_MSSQL_frm;
          end;
    //일반/표준코드
//    102 : begin
//            if not Assigned(CODE_MSSQL_frm) then
//              CODE_MSSQL_frm := TCODE_MSSQL_frm.Create(Application);
//            Result := CODE_MSSQL_frm;
//          end;

    //HS코드
    103 :begin
            if not Assigned(HSCODE_MSSQL_frm) then
              HSCODE_MSSQL_frm := THSCODE_MSSQL_frm.Create(Application);
            Result := HSCODE_MSSQL_frm;
          end;
    //품명규격코드
    104 : begin
            if not Assigned(UI_ITEM_frm) then
              UI_ITEM_frm := TUI_ITEM_frm.Create(Application);
            Result := UI_ITEM_frm;

//            if not Assigned(ITEM_MSSQL_frm) then
//              ITEM_MSSQL_frm := TITEM_MSSQL_frm.Create(Application);
//            Result := ITEM_MSSQL_frm;
          end;
//    은행계좌번호/비밀번호
    105 :;
    //메모코드
    106 :begin
            if not Assigned(UI_MEMO_frm) then
              UI_MEMO_frm := TUI_MEMO_frm.Create(Application);
            Result := UI_MEMO_frm;
         end;
    //암호변경
    107 :;
    //사용자관리
    108: BEGIN
            if not Assigned(PERSON_MSSQL_frm) then
              PERSON_MSSQL_frm := TPERSON_MSSQL_frm.Create(Application);
            Result := PERSON_MSSQL_frm;
         end;
    // 송수신 거래처
    109: begin
            if not Assigned(dlg_SendCustomer_frm) then
              dlg_SendCustomer_frm := Tdlg_SendCustomer_frm.Create(Application);
            Result := dlg_SendCustomer_frm;
         end;
    // 해외은행 코드
    110: begin
           if not Assigned(UI_BANKCODE_SWIFT_frm) then
             UI_BANKCODE_SWIFT_frm := TUI_BANKCODE_SWIFT_frm.Create(Application);
           Result := UI_BANKCODE_SWIFT_frm;
         end;
    //내국신용장 개설신청
    203 : begin
            if not Assigned(UI_LOCAPP_NEW_frm) Then
              UI_LOCAPP_NEW_frm := TUI_LOCAPP_NEW_frm.Create(Application);
            Result := UI_LOCAPP_NEW_frm;
          end;
    //내국신용장 응답서
    204 : begin
            if not Assigned(UI_LOCADV_NEW_frm) Then
              UI_LOCADV_NEW_frm := TUI_LOCADV_NEW_frm.Create(Application);
            Result := UI_LOCADV_NEW_frm;
          end;
    //내국신용장 조건변경 신청서
    205 : begin
            if not Assigned(UI_LOCAMR_NEW_frm) Then
              UI_LOCAMR_NEW_frm := TUI_LOCAMR_NEW_frm.Create(Application);
            Result := UI_LOCAMR_NEW_frm;
          end;
    //내국신용장 조건변경 통지서
    206 : begin
            if not Assigned(UI_LOCAMA_NEW_frm) Then
              UI_LOCAMA_NEW_frm := TUI_LOCAMA_NEW_frm.Create(Application);
            Result := UI_LOCAMA_NEW_frm;
          end;
    //내국신용장 물품수령증명서(송신)
    207 : begin
            if not Assigned(UI_LOCRC2_NEW_frm) Then
              UI_LOCRC2_NEW_frm := TUI_LOCRC2_NEW_frm.Create(Application);
            Result := UI_LOCRC2_NEW_frm;
//            if not Assigned(UI_LOCRC2_frm) then
//              UI_LOCRC2_frm := TUI_LOCRC2_frm.Create(Application);
//            Result := UI_LOCRC2_frm;
          end;
    //외화획득용 원료구매 (공급)확인신청서(송신) - APPPCR
    208 : begin
            if not Assigned(UI_APPPCR_NEW_frm) then
              UI_APPPCR_NEW_frm := TUI_APPPCR_NEW_frm.Create(Application);
            Result := UI_APPPCR_NEW_frm;

//            if not Assigned(APPPCR_N_MSSQL_frm) then
//              APPPCR_N_MSSQL_frm := TAPPPCR_N_MSSQL_frm.Create(Application);
//            Result := APPPCR_N_MSSQL_frm;
          end;
    209 : begin
            if not Assigned(UI_PCRLIC_NEW_frm) Then
              UI_PCRLIC_NEW_frm := TUI_PCRLIC_NEW_frm.Create(Application);
              Result := UI_PCRLIC_NEW_frm;
          end;
    //(개설자)국내발행물품매도확약서(송신)
    210 : begin
            if not Assigned(UI_DOMOFC_NEW_frm) Then
              UI_DOMOFC_NEW_frm := TUI_DOMOFC_NEW_frm.Create(Application);
              Result := UI_DOMOFC_NEW_frm;

//            if not Assigned(UI_DOMOFC_frm) Then
//              UI_DOMOFC_frm := TUI_DOMOFC_frm.Create(Application);
//              Result := UI_DOMOFC_frm;
          end;
    //(개설자)국내발행물품매도확약서 확인(수신)
    211 : begin
            IF not Assigned(UI_DOMOFB_NEW_frm) Then
              UI_DOMOFB_NEW_frm := TUI_DOMOFB_NEW_frm.Create(Application);
            Result := UI_DOMOFB_NEW_frm;

//            if not Assigned(UI_DOMOFB_frm) Then
//              UI_DOMOFB_frm := TUI_DOMOFB_frm.Create(Application);
//              Result := UI_DOMOFB_frm;
          end;
    //(개설자)세금계산서(송신)
    212 : begin
            if not Assigned(UI_VATBI1_frm) Then
              UI_VATBI1_frm := TUI_VATBI1_frm.Create(Application);
              Result := UI_VATBI1_frm;
          end;
    //(개설자)세금계산서(수신)
    213 : begin
            if not Assigned(UI_VATBI4_frm) Then
              UI_VATBI4_frm := TUI_VATBI4_frm.Create(Application);
              Result := UI_VATBI4_frm;
          end;
    //(수혜업자)내국신용장통지서
    303 : begin
            if not Assigned(UI_LOCAD1_NEW_frm) Then
              UI_LOCAD1_NEW_frm := TUI_LOCAD1_NEW_frm.Create(Application);
              Result := UI_LOCAD1_NEW_frm;
//            if not Assigned(UI_LOCAD1_frm) Then
//              UI_LOCAD1_frm := TUI_LOCAD1_frm.Create(Application);
//              Result := UI_LOCAD1_frm;
          end;
    //(수혜업자)내국신용장조건변경통지서
    304 : begin
            if not Assigned(UI_LOCAM1_NEW_frm) Then
              UI_LOCAM1_NEW_frm := TUI_LOCAM1_NEW_frm.Create(Application);
              Result := UI_LOCAM1_NEW_frm;
          end;
    //(수혜업자)세금계산서(수신)
    305 : begin
            if not Assigned(UI_VATBI2_NEW_frm) Then
              UI_VATBI2_NEW_frm := TUI_VATBI2_NEW_frm.Create(Application);
              Result := UI_VATBI2_NEW_frm;

//            if not Assigned(UI_VATBI2_frm) Then
//              UI_VATBI2_frm := TUI_VATBI2_frm.Create(Application);
//              Result := UI_VATBI2_frm;

          end;
    //(수혜업자)세금계산서(송신)
    306 : begin
            if not Assigned(UI_VATBI3_frm) Then
              UI_VATBI3_frm := TUI_VATBI3_frm.Create(Application);
              Result := UI_VATBI3_frm;

          end;
    //(수혜업자)내국신용장 물품수령증명서(수신)
    307 : begin
            if not Assigned(UI_LOCRC1_NEW_frm) Then
              UI_LOCRC1_NEW_frm := TUI_LOCRC1_NEW_frm.Create(Application);
              Result := UI_LOCRC1_frm;
          end;
    //(수혜업자)판매대금추심(매입)의뢰서(송신)
    308 : begin
            if not Assigned(UI_APPSPC_NEW_frm) Then
              UI_APPSPC_NEW_frm := TUI_APPSPC_NEW_frm.Create(Application);
              Result := UI_APPSPC_NEW_frm;
          end;
    //(수혜업자)판매대금추심(매입)응답서(수신)
    309 : begin
            if not Assigned(UI_SPCNTC_NEW_frm) Then
              UI_SPCNTC_NEW_frm := TUI_SPCNTC_NEW_frm.Create(Application);
              Result := UI_SPCNTC_NEW_frm;
//            if not Assigned(UI_SPCNTC_frm) Then
//              UI_SPCNTC_frm := TUI_SPCNTC_frm.Create(Application);
//              Result := UI_SPCNTC_frm;
          end;
    //수출업무 - 수출신용장 통지서
    401 : begin
            if not Assigned(UI_ADV700_NEW_frm) then
              UI_ADV700_NEW_frm := TUI_ADV700_NEW_frm.Create(Application);
              Result := UI_ADV700_NEW_frm;
          end;
    //수출업무 - 수출신용장 조건변경 통지서
    402 : begin
            if not Assigned(UI_ADV707_NEW_frm) then
              UI_ADV707_NEW_frm := TUI_ADV707_NEW_frm.Create(Application);
              Result := UI_ADV707_NEW_frm;
          end;
    403  :begin
            if not Assigned(UI_ADV710_NEW_frm) Then
              UI_ADV710_NEW_frm := TUI_ADV710_NEW_frm.Create(Application);
            Result := UI_ADV710_NEW_frm;
          end;

    //취소불능화환 신용장 개설 신청서
    501 : begin
            IF not Assigned(UI_APP700_NEW_frm) Then
              UI_APP700_NEW_frm := TUI_APP700_NEW_frm.Create(Application);
            Result := UI_APP700_NEW_frm;

//            IF not Assigned(UI_APP700_frm) Then
//              UI_APP700_frm := TUI_APP700_frm.Create(Application);
//            Result := UI_APP700_frm;
          end;
    //취소불능화환 신용장 개설 응답서
    502 : begin
            if not Assigned(UI_INF700_NEW_frm) Then
              UI_INF700_NEW_frm := TUI_INF700_NEW_frm.Create(Application);
            Result := UI_INF700_NEW_frm;
          end;
    //취소불능화환 신용장 변경 신청서
    503 : begin
            if not  Assigned(UI_APP707_NEW_frm) then
              UI_APP707_NEW_frm := TUI_APP707_NEW_frm.Create(Application);
            Result := UI_APP707_NEW_frm;
          end;
    //취소불능화환 신용장 변경 신청서
    504 : begin
            if not Assigned(UI_INF707_NEW_frm) then
              UI_INF707_NEW_frm := TUI_INF707_NEW_frm.Create(Application);
            Result := UI_INF707_NEW_frm;
          end;
    //수입화물선취보증(인도승락)신청서
    511 : begin
            if not Assigned(UI_APPLOG_NEW_frm) then
              UI_APPLOG_NEW_frm := TUI_APPLOG_NEW_frm.Create(Application);
            Result := UI_APPLOG_NEW_frm;

//            if not Assigned(UI_APPLOG_frm) then
//              UI_APPLOG_frm := TUI_APPLOG_frm.Create(Application);
//            Result := UI_APPLOG_frm;
          end;
    //수입화물선취보증(인도승락)서
    512 : begin
            if not Assigned(UI_LOGUAR_NEW_frm) then
              UI_LOGUAR_NEW_frm := TUI_LOGUAR_NEW_frm.Create(Application);
            Result := UI_LOGUAR_NEW_frm;
//            if not Assigned(UI_LOGUAR_frm) then
//              UI_LOGUAR_frm := TUI_LOGUAR_frm.Create(Application);
//            Result := UI_LOGUAR_frm;
          end;
    //선적서류 도착 통보서
    601 : begin
            IF not Assigned(UI_DOANTC_NEW_frm) Then
              UI_DOANTC_NEW_frm := TUI_DOANTC_NEW_frm.Create(Application);
            Result := UI_DOANTC_NEW_frm;
//            if not Assigned(UI_DOANTC_frm) Then
//              UI_DOANTC_frm := TUI_DOANTC_frm.Create(Application);
//            Result := UI_DOANTC_frm;
          end;
    //내국신용장 판매대금추심도착통보서
    602 : begin
            if not Assigned(UI_LDANTC_NEW_frm) Then
              UI_LDANTC_NEW_frm := TUI_LDANTC_NEW_frm.Create(Application);
            Result := UI_LDANTC_NEW_frm;
//            if not Assigned(UI_LDANTC_frm) Then
//               UI_LDANTC_frm := TUI_LDANTC_frm.Create(Application);
//            Result := UI_LDANTC_frm;
          end;

    //계산서
    605 : begin
            if not Assigned(FINBIL_N_MSSQL_frm) Then
              FINBIL_N_MSSQL_frm := TFINBIL_N_MSSQL_frm.Create(Application);
            Result := FINBIL_N_MSSQL_frm;
          end;

    //일반응답서(수신)
    609 : begin
            if not Assigned(NormalRecv_MSSQL_frm) Then
              NormalRecv_MSSQL_frm := TNormalRecv_MSSQL_frm.Create(Application);
              Result := NormalRecv_MSSQL_frm;
          end;

    //입금통지서
    610 : begin
            if not Assigned(UI_CREADV_NEW_frm) Then
              UI_CREADV_NEW_frm := TUI_CREADV_NEW_frm.Create(Application);
            Result := UI_CREADV_NEW_frm;
          end;

    //근거서류첨부 응답서
    611 : begin
            if not Assigned(UI_ATTNTC_NEW_frm) Then
              UI_ATTNTC_NEW_frm := TUI_ATTNTC_NEW_frm.Create(Application);
            Result := UI_ATTNTC_NEW_frm;
          end;

    //출금통지서
    612 :begin
            if not Assigned(UI_DEBADV_NEW_frm) Then
              UI_DEBADV_NEW_frm := TUI_DEBADV_NEW_frm.Create(Application);
            Result := UI_DEBADV_NEW_frm;
         end;

    // 지급지시서
    613 :begin
            if not Assigned(UI_PAYORD_NEW_frm) Then
              UI_PAYORD_NEW_frm := TUI_PAYORD_NEW_frm.Create(Application);
            Result := UI_PAYORD_NEW_frm;
         end;
    // 지급(변경)확인신청서
    614: begin
            if not Assigned(UI_APPCOP_NEW_frm) Then
              UI_APPCOP_NEW_frm := TUI_APPCOP_NEW_frm.Create(Application);
            Result := UI_APPCOP_NEW_frm;
         end;

    615: begin
            if not Assigned(UI_APPRMI_NEW_frm) then
              UI_APPRMI_NEW_frm := TUI_APPRMI_NEW_frm.Create(Application);
            Result := UI_APPRMI_NEW_frm;
         end;

    //문서송신
    701 : begin
            IF not Assigned(DocumentSend_frm) Then
              DocumentSend_frm := TDocumentSend_frm.Create(Application);
            Result := DocumentSend_frm;
          end;
    //문서수신
    702   :
          begin
            IF not Assigned(RecvDocumentsV1_frm) Then
              RecvDocumentsV1_frm := TRecvDocumentsV1_frm.Create(Application);
            Result := RecvDocumentsV1_frm;
          end;

    //디버깅
    9999 :
    begin
      IF not Assigned(UI_RUNSQL_frm) Then
        UI_RUNSQL_frm := TUI_RUNSQL_frm.Create(Application);
      Result := UI_RUNSQL_frm;
    end;
  end;
end;

procedure TMain_frm.ShowChildForm(myChildForm: TForm);
begin
//  myChildForm.Left := 0;
//  myChildForm.Top := 0;
  myChildForm.Show;
  myChildForm.BringToFront;
end;

procedure TMain_frm.N11Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TMain_frm.d10Click(Sender: TObject);
begin
//  Dlg_Upgrade_frm.Show;
end;

procedure TMain_frm.N43Click(Sender: TObject);
begin
  Dialog_FixedNo_frm := TDialog_FixedNo_frm.Create(Application);
  try
    Dialog_FixedNo_frm.ShowModal;
  finally
    FreeAndNil(Dialog_FixedNo_frm);
  end;
end;

procedure TMain_frm.N4Click(Sender: TObject);
begin
  CODE_MSSQL_frm := TCODE_MSSQL_frm.Create(Application);
  try
   CODE_MSSQL_frm.ShowModal;
  finally
    FreeAndNil(CODE_MSSQL_frm);
  end;
end;

procedure TMain_frm.setProgramList;
var
  i,j,k : integer;
begin
  //Menu Item Count.
  for i:=0 to MainMenu1.Items.Count-1 do
  begin
    for j := 0 to MainMenu1.Items[i].Count-1 do
    begin
//      IF MainMenu1.Items[i].Items[j].Count = 0 Then
        ShowMessage(MainMenu1.Items[i].Items[j].Caption)
//      else
//      begin
//        ShowMessage(MainMenu1.Items[i].Items[j].Caption)
//      end;
    end;
  end;
end;

procedure TMain_frm.w1Click(Sender: TObject);
begin
  dlg_AUTOINC_frm := Tdlg_AUTOINC_frm.Create(Application);
  try
    dlg_AUTOINC_frm.ShowModal;
  finally
    FreeAndNil(dlg_AUTOINC_frm);
  end;

end;

procedure TMain_frm.W3Click(Sender: TObject);
var
  WINMATE_PATH : String;
begin
  WINMATE_PATH := GetEnvironmentVariable('KTNET');
  IF RightStr(WINMATE_PATH, 1) <> '\' Then
    WINMATE_PATH := WINMATE_PATH+'\BIN\WINMATE.exe'
  else
    WINMATE_PATH := WINMATE_PATH+'BIN\WINMATE.exe';

  if FileExists(WINMATE_PATH) Then
    ShellExecute(Handle, 'open', PChar(WINMATE_PATH), nil, nil, SW_SHOW)
  else
    MessageBox(Self.Handle, 'WINMATE를 찾을 수 없습니다', '실행오류', MB_OK+MB_ICONERROR);
end;

procedure TMain_frm.N68Click(Sender: TObject);
var
  sUrl : string;
begin
  case (Sender as TMenuItem).Tag of
    0: sUrl := 'http://ikis21.com/';
    1: sUrl := 'http://cs1472.com/customer/';
  end;

  ShellExecute(Handle, 'open', PChar(sUrl), nil, nil, SW_SHOW);
end;

procedure TMain_frm.v1Click(Sender: TObject);
begin
//  SetCurrentDir()

  WinExec(Pchar(ExtractFilePath(Application.ExeName)+'Paradox2SQL.exe'),SW_SHOW);
//  ShellExecute(Handle, 'open', PChar())

//  ShowMessage(Application.ExeName);
end;

end.
