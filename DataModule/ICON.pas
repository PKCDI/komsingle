unit ICON;

interface

uses
  SysUtils, Classes, ImgList, Controls, acAlphaImageList, sSkinManager;

type
  TDMICON = class(TDataModule)
    System18: TsAlphaImageList;
    System26: TsAlphaImageList;
    System32: TsAlphaImageList;
    System24: TsAlphaImageList;
    System16: TsAlphaImageList;
    img_sysicon: TsAlphaImageList;
    sSkinManager1: TsSkinManager;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMICON: TDMICON;

implementation

{$R *.dfm}

end.
