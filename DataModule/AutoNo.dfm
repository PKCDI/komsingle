object DMAutoNo: TDMAutoNo
  OldCreateOrder = False
  Left = 643
  Top = 306
  Height = 158
  Width = 215
  object qryCheck: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      'SELECT DOCID, DEPTCODE, REGDATE, LASTSEQ'
      'FROM AUTOINCR')
    Left = 8
    Top = 8
  end
  object qryIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'DEPTCODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'REGDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'LASTSEQ'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO AUTOINCR(DOCID, DEPTCODE, REGDATE, LASTSEQ)'
      'VALUES(:DOCID, :DEPTCODE, :REGDATE, :LASTSEQ)')
    Left = 72
    Top = 8
  end
  object qryMod: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'LASTSEQ'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'DEPTCODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'REGDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE AUTOINCR'
      'SET LASTSEQ = :LASTSEQ'
      'WHERE DOCID = :DOCID'
      'AND DEPTCODE = :DEPTCODE'
      'AND REGDATE = :REGDATE')
    Left = 120
    Top = 8
  end
  object qryAutoInc: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'DEPTCODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'REGDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'LASTSEQ'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO AUTOINCR(DOCID, DEPTCODE, REGDATE, LASTSEQ)'
      'VALUES(:DOCID, :DEPTCODE, :REGDATE, :LASTSEQ)')
    Left = 72
    Top = 56
  end
end
