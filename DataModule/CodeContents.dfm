object DMCodeContents: TDMCodeContents
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 1214
  Top = 142
  Height = 577
  Width = 349
  object APPPCR_GUBUN: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE,NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39'APPPCR'#44396#48516#39
      'AND Remark = 1')
    Left = 64
    Top = 8
  end
  object GEOLAECHEO: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT CODE,TRAD_NO,SAUP_NO,ENAME,REP_NAME,REP_TEL,ADDR1,ADDR2,A' +
        'DDR3,NAT,ESU1,ESU2,Jenja,EMAIL_ID,EMAIL_DOMAIN'
      'FROM CUSTOM'
      'ORDER BY ENAME')
    Left = 192
    Top = 8
  end
  object BYEONDONG_GUBUN: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#48320#46041#44396#48516#39
      'AND Remark = 1')
    Left = 64
    Top = 104
  end
  object TONGHWA: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#53685#54868#39
      'AND Remark = 1')
    Left = 64
    Top = 152
  end
  object NAEGUKSIN4025: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#45236#44397#49888'4025'#39
      'AND Remark = 1')
    Left = 64
    Top = 352
  end
  object BANKCODE: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE,'
      #9'   ENAME1 as BANKNAME,'
      #9'   ENAME3 as BANKBRANCH '
      'FROM BANKCODE')
    Left = 64
    Top = 248
  end
  object DANWI: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#45800#50948#39
      'AND Remark = 1')
    Left = 64
    Top = 200
  end
  object APPPCR_DOCUMENTS: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39'APPPCR'#49436#47448#39
      'AND Remark = 1')
    Left = 64
    Top = 56
  end
  object NATION: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#44397#44032#39
      'AND Remark = 1')
    Left = 192
    Top = 56
  end
  object ITEM: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT ITEM_CODE.CODE'
      '      ,HSCODE'
      '      ,FNAME'
      '      ,SNAME'
      '      ,MSIZE'
      '      ,QTYC'
      #9'  ,ISNULL(DANWI.NAME,'#39#39') as QTYC_NAME'
      '      ,PRICEG'
      #9'  ,ISNULL(TONGHWA.NAME,'#39#39') as PRICEG_NAME'
      '      ,PRICE'
      '      ,QTY_U'
      '      ,QTY_UG'
      #9'  ,ISNULL(DANWI2.NAME,'#39#39') as QTY_UG_NAME'
      '  FROM ITEM_CODE LEFT JOIN (SELECT CODE, NAME'
      #9#9#9#9#9#9#9'FROM CODE2NDD'
      
        #9#9#9#9#9#9#9'WHERE Prefix = '#39#53685#54868#39') TONGHWA ON ITEM_CODE.PRICEG = TONGHW' +
        'A.CODE'
      #9#9#9#9' LEFT JOIN (SELECT CODE, NAME'
      #9#9#9#9#9#9#9'FROM CODE2NDD'
      #9#9#9#9#9#9#9'WHERE Prefix = '#39#45800#50948#39') DANWI ON ITEM_CODE.QTYC = DANWI.CODE'
      #9#9#9#9' LEFT JOIN (SELECT CODE, NAME'
      #9#9#9#9#9#9#9'FROM CODE2NDD'
      
        #9#9#9#9#9#9#9'WHERE Prefix = '#39#45800#50948#39') DANWI2 ON ITEM_CODE.QTY_UG = DANWI2.' +
        'CODE')
    Left = 192
    Top = 104
    object ITEMCODE: TStringField
      FieldName = 'CODE'
    end
    object ITEMHSCODE: TStringField
      FieldName = 'HSCODE'
      EditMask = '9999-99.9999;0;'
      Size = 10
    end
    object ITEMFNAME: TMemoField
      FieldName = 'FNAME'
      BlobType = ftMemo
    end
    object ITEMSNAME: TStringField
      FieldName = 'SNAME'
      Size = 35
    end
    object ITEMMSIZE: TMemoField
      FieldName = 'MSIZE'
      BlobType = ftMemo
    end
    object ITEMQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object ITEMPRICEG: TStringField
      FieldName = 'PRICEG'
      Size = 3
    end
    object ITEMPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object ITEMQTY_U: TBCDField
      FieldName = 'QTY_U'
      Precision = 18
    end
    object ITEMQTY_UG: TStringField
      FieldName = 'QTY_UG'
      Size = 3
    end
    object ITEMQTYC_NAME: TStringField
      FieldName = 'QTYC_NAME'
      ReadOnly = True
      Size = 100
    end
    object ITEMPRICEG_NAME: TStringField
      FieldName = 'PRICEG_NAME'
      ReadOnly = True
      Size = 100
    end
    object ITEMQTY_UG_NAME: TStringField
      FieldName = 'QTY_UG_NAME'
      ReadOnly = True
      Size = 100
    end
  end
  object PCRLIC_GUBUN: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE,NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39'PCRLIC'#44396#48516#39
      'AND Remark = 1')
    Left = 192
    Top = 152
  end
  object Config: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT NAME1, NAME2, TRAD_NO, SAUP_NO, ADDR1, ADDR2, ADDR3, ENAM' +
        'E1, ENAME2, ENAME3, EADDR1, EADDR2, EADDR3, SIGN_C, SIGN, NMLEN,' +
        ' SZLEN, HDMRG, BDLEN, LFMRG, OP_NO, APP_SAUP, APP_NAME1, APP_NAM' +
        'E2, APP_NAME3, APP_NAME4, APP_NO, APP_CST, MNF_SAUP, MNF_NAME1, ' +
        'MNF_NAME2, MNF_NAME3, MNF_NAME4, BANK_CD, BANK, BANK_ACC, CST1, ' +
        'CST2, PrtNm, PrtNo, AutoGubun, Jecw, CST3'
      'FROM CONFIG')
    Left = 8
    Top = 8
    object ConfigNAME1: TStringField
      FieldName = 'NAME1'
      Size = 35
    end
    object ConfigNAME2: TStringField
      FieldName = 'NAME2'
      Size = 35
    end
    object ConfigTRAD_NO: TStringField
      FieldName = 'TRAD_NO'
      Size = 10
    end
    object ConfigSAUP_NO: TStringField
      FieldName = 'SAUP_NO'
      Size = 10
    end
    object ConfigADDR1: TStringField
      FieldName = 'ADDR1'
      Size = 35
    end
    object ConfigADDR2: TStringField
      FieldName = 'ADDR2'
      Size = 35
    end
    object ConfigADDR3: TStringField
      FieldName = 'ADDR3'
      Size = 35
    end
    object ConfigENAME1: TStringField
      FieldName = 'ENAME1'
      Size = 35
    end
    object ConfigENAME2: TStringField
      FieldName = 'ENAME2'
      Size = 35
    end
    object ConfigENAME3: TStringField
      FieldName = 'ENAME3'
      Size = 35
    end
    object ConfigEADDR1: TStringField
      FieldName = 'EADDR1'
      Size = 35
    end
    object ConfigEADDR2: TStringField
      FieldName = 'EADDR2'
      Size = 35
    end
    object ConfigEADDR3: TStringField
      FieldName = 'EADDR3'
      Size = 35
    end
    object ConfigSIGN_C: TStringField
      FieldName = 'SIGN_C'
      Size = 1
    end
    object ConfigSIGN: TStringField
      FieldName = 'SIGN'
      Size = 10
    end
    object ConfigNMLEN: TStringField
      FieldName = 'NMLEN'
      Size = 2
    end
    object ConfigSZLEN: TStringField
      FieldName = 'SZLEN'
      Size = 2
    end
    object ConfigHDMRG: TStringField
      FieldName = 'HDMRG'
      Size = 2
    end
    object ConfigBDLEN: TStringField
      FieldName = 'BDLEN'
      Size = 2
    end
    object ConfigLFMRG: TStringField
      FieldName = 'LFMRG'
      Size = 2
    end
    object ConfigOP_NO: TStringField
      FieldName = 'OP_NO'
      Size = 35
    end
    object ConfigAPP_SAUP: TStringField
      FieldName = 'APP_SAUP'
      Size = 10
    end
    object ConfigAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 40
    end
    object ConfigAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 40
    end
    object ConfigAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 40
    end
    object ConfigAPP_NAME4: TStringField
      FieldName = 'APP_NAME4'
      Size = 40
    end
    object ConfigAPP_NO: TStringField
      FieldName = 'APP_NO'
      Size = 40
    end
    object ConfigAPP_CST: TStringField
      FieldName = 'APP_CST'
      Size = 40
    end
    object ConfigMNF_SAUP: TStringField
      FieldName = 'MNF_SAUP'
      Size = 10
    end
    object ConfigMNF_NAME1: TStringField
      FieldName = 'MNF_NAME1'
      Size = 40
    end
    object ConfigMNF_NAME2: TStringField
      FieldName = 'MNF_NAME2'
      Size = 40
    end
    object ConfigMNF_NAME3: TStringField
      FieldName = 'MNF_NAME3'
      Size = 40
    end
    object ConfigMNF_NAME4: TStringField
      FieldName = 'MNF_NAME4'
      Size = 40
    end
    object ConfigBANK_CD: TStringField
      FieldName = 'BANK_CD'
      Size = 6
    end
    object ConfigBANK: TStringField
      FieldName = 'BANK'
      Size = 30
    end
    object ConfigBANK_ACC: TStringField
      FieldName = 'BANK_ACC'
    end
    object ConfigCST1: TStringField
      FieldName = 'CST1'
      Size = 3
    end
    object ConfigCST2: TStringField
      FieldName = 'CST2'
      Size = 10
    end
    object ConfigPrtNm: TStringField
      FieldName = 'PrtNm'
      Size = 1
    end
    object ConfigPrtNo: TIntegerField
      FieldName = 'PrtNo'
    end
    object ConfigAutoGubun: TStringField
      FieldName = 'AutoGubun'
      Size = 1
    end
    object ConfigJecw: TStringField
      FieldName = 'Jecw'
      Size = 1
    end
    object ConfigCST3: TStringField
      FieldName = 'CST3'
      Size = 3
    end
  end
  object NAEGUKSIN4487: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#45236#44397#49888'4487'#39
      'AND Remark = 1')
    Left = 64
    Top = 448
  end
  object PSHIP: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39'PSHIP'#39
      'AND Remark = 1')
    Left = 192
    Top = 208
  end
  object NAEGUKSIN1001: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#45236#44397#49888'1001'#39
      'AND Remark = 1')
    Left = 64
    Top = 304
  end
  object NAEGUKSIN4277: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#45236#44397#49888'4277'#39
      'AND Remark = 1')
    Left = 64
    Top = 400
  end
end
