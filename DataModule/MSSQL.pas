unit MSSQL;

interface

uses
  SysUtils, Classes, DB, ADODB, IniFiles, Forms, Dialogs, ClipBrd, VarDefine,
  DBTables;

type
  TDMMssql = class(TDataModule)
    KISConnect: TADOConnection;
    Database1: TDatabase;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure BeginTrans;
    procedure RollbackTrans;
    procedure CommitTrans;
    function inTrans:Boolean;
  end;

var
  DMMssql: TDMMssql;

implementation

{$R *.dfm}

{ TDMMssql }

procedure TDMMssql.BeginTrans;
begin
  KISConnect.BeginTrans;
end;

procedure TDMMssql.CommitTrans;
begin
  KISConnect.CommitTrans;
end;

procedure TDMMssql.RollbackTrans;
begin
  KISConnect.RollbackTrans;
end;

{
procedure TDMMssql.DataModuleCreate(Sender: TObject);
var
  INIFILE : TIniFile;
  FINIPATH : String;
  FConnection : String;
begin
  FINIPATH := ExtractFilePath( Application.ExeName )+'ldbinfo.ini';
  IF not FileExists( FINIPATH ) Then
  begin
    ShowMessage('LOCAL DB의 정보가 입력되어 있지 않습니다'#13#10'설치폴더의 "SQLConfig_Local.exe"를 실행하여 설정해주세요');
    Application.Terminate;
  end
  else
  begin
    INIFILE := TIniFile.Create(FINIPATH);
    FConnection := 'Provider=SQLNCLI11.1;Integrated Security=SSPI;Persist Security Info=False;User ID="";Initial Catalog="KIS_COMM";'+
                   'Data Source=(localDb)\'+INIFILE.ReadString('LOCAL','INSTANCE_NAME','')+';'+
                   'Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;'+
                   'Initial File Name='+INIFILE.ReadString('LOCAL','PATH','')+';'+
                   'Server SPN="";Application Intent=READWRITE';
    IF KISConnect.Connected Then KISConnect.Close;
    KISConnect.ConnectionString := FConnection;
//    Clipboard.AsText := FConnection;
    KISConnect.Open;
//    'Use Encryption for Data=False;Tag with column collation when possible=False;MARS Connection=False;DataTypeCompatibility=0;Trust Server Certificate=False;
  end;
end;
}

function TDMMssql.inTrans: Boolean;
begin
  Result := KISConnect.InTransaction;
end;

procedure TDMMssql.DataModuleCreate(Sender: TObject);
begin
  If KISConnect.Connected Then KISConnect.Close;
end;

end.
