unit CodeContents;

interface

uses
  SysUtils, Classes, DB, ADODB, Dialogs, TypeDefine, Clipbrd;

type
  TDMCodeContents = class(TDataModule)
    APPPCR_GUBUN: TADOQuery;
    GEOLAECHEO: TADOQuery;
    BYEONDONG_GUBUN: TADOQuery;
    TONGHWA: TADOQuery;
    NAEGUKSIN4025: TADOQuery;
    BANKCODE: TADOQuery;
    DANWI: TADOQuery;
    APPPCR_DOCUMENTS: TADOQuery;
    NATION: TADOQuery;
    ITEM: TADOQuery;
    ITEMCODE: TStringField;
    ITEMHSCODE: TStringField;
    ITEMFNAME: TMemoField;
    ITEMSNAME: TStringField;
    ITEMMSIZE: TMemoField;
    ITEMQTYC: TStringField;
    ITEMPRICEG: TStringField;
    ITEMPRICE: TBCDField;
    ITEMQTY_U: TBCDField;
    ITEMQTY_UG: TStringField;
    ITEMQTYC_NAME: TStringField;
    ITEMPRICEG_NAME: TStringField;
    ITEMQTY_UG_NAME: TStringField;
    PCRLIC_GUBUN: TADOQuery;
    Config: TADOQuery;
    ConfigNAME1: TStringField;
    ConfigNAME2: TStringField;
    ConfigTRAD_NO: TStringField;
    ConfigSAUP_NO: TStringField;
    ConfigADDR1: TStringField;
    ConfigADDR2: TStringField;
    ConfigADDR3: TStringField;
    ConfigENAME1: TStringField;
    ConfigENAME2: TStringField;
    ConfigENAME3: TStringField;
    ConfigEADDR1: TStringField;
    ConfigEADDR2: TStringField;
    ConfigEADDR3: TStringField;
    ConfigSIGN_C: TStringField;
    ConfigSIGN: TStringField;
    ConfigNMLEN: TStringField;
    ConfigSZLEN: TStringField;
    ConfigHDMRG: TStringField;
    ConfigBDLEN: TStringField;
    ConfigLFMRG: TStringField;
    ConfigOP_NO: TStringField;
    ConfigAPP_SAUP: TStringField;
    ConfigAPP_NAME1: TStringField;
    ConfigAPP_NAME2: TStringField;
    ConfigAPP_NAME3: TStringField;
    ConfigAPP_NAME4: TStringField;
    ConfigAPP_NO: TStringField;
    ConfigAPP_CST: TStringField;
    ConfigMNF_SAUP: TStringField;
    ConfigMNF_NAME1: TStringField;
    ConfigMNF_NAME2: TStringField;
    ConfigMNF_NAME3: TStringField;
    ConfigMNF_NAME4: TStringField;
    ConfigBANK_CD: TStringField;
    ConfigBANK: TStringField;
    ConfigBANK_ACC: TStringField;
    ConfigCST1: TStringField;
    ConfigCST2: TStringField;
    ConfigPrtNm: TStringField;
    ConfigPrtNo: TIntegerField;
    ConfigAutoGubun: TStringField;
    ConfigJecw: TStringField;
    ConfigCST3: TStringField;
    NAEGUKSIN4487: TADOQuery;
    PSHIP: TADOQuery;
    NAEGUKSIN1001: TADOQuery;
    NAEGUKSIN4277: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CODEREFRESH;
    Procedure OPENTABLE(qry : TADOQuery);        
    function AutoNumberingForLOCAPP:TAutoNumbering;
    function getCodeName(sGroup, sCode : String):TCodeRecord;
  end;

var
  DMCodeContents: TDMCodeContents;

implementation

uses MSSQL;

{$R *.dfm}

function TDMCodeContents.AutoNumberingForLOCAPP: TAutoNumbering;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT CODE, Remark, NAME FROM CODE2NDD WHERE Prefix IN (N''LOCAPP����'',N''LOCRCT����'')';
      Open;

      Result.LOCAPP_NO   := '';
      Result.LOCAPP_BANK := '';
      Result.LOCRCT_NO   := '';

      while not eof do
      begin
        IF (FieldByName('CODE').AsString = 'LA_BGM1') AND (FieldByName('Remark').AsString = '1') Then
          Result.LOCAPP_NO   := FieldByName('NAME').AsString;

        IF (FieldByName('CODE').AsString = 'LA_BGM2') AND (FieldByName('Remark').AsString = '1') Then
          Result.LOCAPP_BANK := FieldByName('NAME').AsString;

        IF (FieldByName('CODE').AsString = 'LC_BGM1') AND (FieldByName('Remark').AsString = '1') Then
          Result.LOCRCT_NO   := FieldByName('NAME').AsString;

        Next;
      end;

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TDMCodeContents.CODEREFRESH;
begin
  DataModuleCreate(nil);
end;

procedure TDMCodeContents.DataModuleCreate(Sender: TObject);
var
  i : integer;
begin
  for i := 0 to Self.ComponentCount-1 do
  begin
    IF Self.Components[i] is TADOQuery Then
      OPENTABLE((Self.Components[i] as TADOQuery));
  end;
//  OPENTABLE(Config);
//  OPENTABLE(APPPCR_GUBUN);
//  OPENTABLE(APPPCR_DOCUMENTS);
//  OPENTABLE(BYEONDONG_GUBUN);
//  OPENTABLE(TONGHWA);
//  OPENTABLE(NAEGUKSIN4025);
//  OPENTABLE(GEOLAECHEO);
//  OPENTABLE(BANKCODE);
//  OPENTABLE(DANWI);
//  OPENTABLE(NATION);
//  OPENTABLE(ITEM);
//  OPENTABLE(PCRLIC_GUBUN);
end;

function TDMCodeContents.getCodeName(sGroup, sCode: String): TCodeRecord;
begin
  Result.sCode := sCode;
  Result.sName := '';

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT [NAME] FROM CODE2NDD WHERE Prefix = N'+QuotedStr(sGroup)+' AND [CODE] = N'+QuotedStr(sCode);
//      Clipboard.AsText := SQL.Text;
      Open;

      IF RecordCount > 0 Then
      begin
        Result.sName := FieldByName('NAME').AsString;
      end
      else
        Result.sName := 'NOT FOUND('+Result.sCode+')';
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TDMCodeContents.OPENTABLE(qry: TADOQuery);
begin
  qry.Close;
  qry.Open;
end;

end.
