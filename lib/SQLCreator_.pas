unit SQLCreator;

interface

uses
  Windows, Dialogs, StrUtils, DateUtils, Controls, SysUtils, Classes;
Type
  TDMLType = (dmlNone, dmlInsert, dmlUpdate, dmlDelete);
  TSQLCreate = class
    private
      FSQLBody : String;
      FDMLType : TDMLType;
      FFieldList : TStringList;
      FValueList : TStringList;
      FWhereFieldList : TStringList;
      FWhereValueList : TStringList;
    protected
    public
      constructor Create;
//      destructor Destroy;
      procedure SQLClear;
      procedure SQLHeader(TableName : String);
      procedure ADDValue(sField,sValue : String);
      procedure ADDWhere(sField,sValue : String); 
      function CreateSQL:String;

      property DMLType:TDMLType read FDMLType write FDMLType;
//      property SQL:String read CreateSQL;
  end;

implementation

{ TSQLCreate }

procedure TSQLCreate.ADDValue(sField, sValue: String);
begin
  FFieldList.Add(sField);
  FValueList.Add(sValue);
end;

procedure TSQLCreate.ADDWhere(sField, sValue: String);
begin
  FWhereFieldList.Add( sField );
  FWhereValueList.Add( sValue );
end;

constructor TSQLCreate.Create;
begin
  FDMLType := dmlNone;
  FFieldList := TStringList.Create;
  FValueList := TStringList.Create;
  FWhereFieldList := TStringList.Create;
  FWhereValueList := TStringList.Create;
end;

function TSQLCreate.CreateSQL: String;
var
  i : integer;
begin
  Result := '';

  Case FDMLType of
    dmlInsert:
    begin
      FSQLBody := FSQLBody+'(';
      //------------------------------------------------------------------------------
      // 필드
      //------------------------------------------------------------------------------
      for i := 0 to FFieldList.Count-1 do
      begin
        IF i = FFieldList.Count-1 Then
          FSQLBody := FSQLBody+FFieldList.Strings[i]+') VALUES('
        else
          FSQLBody := FSQLBody+FFieldList.Strings[i]+',';
      end;
      //------------------------------------------------------------------------------
      // 값
      //------------------------------------------------------------------------------
      for i := 0 to FValueList.Count-1 do
      begin
        IF i = FValueList.Count-1 Then
          FSQLBody := FSQLBody+QuotedStr(FValueList.Strings[i])+')'
        else
          FSQLBody := FSQLBody+QuotedStr(FValueList.Strings[i])+',';
      end;
    end;

    dmlUpdate:
    begin
      FSQLBody := FSQLBody + ' SET '#13#10;
      //------------------------------------------------------------------------------
      // 업데이트부
      //------------------------------------------------------------------------------
      for i := 0 to FFieldList.Count-1 do
      begin
        if i = FFieldList.Count-1 Then
          FSQLBody := FSQLBody + FFieldList.Strings[i]+' = '+QuotedStr(FValueList.Strings[i])+#13#10
        else
          FSQLBody := FSQLBody + FFieldList.Strings[i]+' = '+QuotedStr(FValueList.Strings[i])+','#13#10;
      end;
      //------------------------------------------------------------------------------
      // 조건부
      //------------------------------------------------------------------------------
      for i := 0 to FWhereFieldList.Count-1 do
      begin
        if i = 0 Then
          FSQLBody := FSQLBody +'WHERE '+FWhereFieldList.Strings[i]+' = '+QuotedStr(FWhereValueList.Strings[i])+#13#10
        else
          FSQLBody := FSQLBody +'AND '+FWhereFieldList.Strings[i]+' = '+QuotedStr(FWhereValueList.Strings[i]);
      end;
    end;

    dmlDelete:
    begin
      FSQLBody := FSQLBody+#13#10;
      //------------------------------------------------------------------------------
      // 조건부
      //------------------------------------------------------------------------------
      for i := 0 to FWhereFieldList.Count-1 do
      begin
        if i = 0 Then
          FSQLBody := FSQLBody +'WHERE '+FWhereFieldList.Strings[i]+' = '+QuotedStr(FWhereValueList.Strings[i])+#13#10
        else
          FSQLBody := FSQLBody +'AND '+FWhereFieldList.Strings[i]+' = '+QuotedStr(FWhereValueList.Strings[i])
      end;

    end;
  end;
  Result := FSQLBody;
end;

//destructor TSQLCreate.Destroy;
//begin
//  inherited;
//  FFieldList.Free;
//  FValueList.Free;
//end;

procedure TSQLCreate.SQLClear;
begin
  FSQLBody := '';
  FFieldList.Clear;
  FValueList.Clear;
  FWhereFieldList.Clear;
  FWhereValueList.Clear;  
end;

procedure TSQLCreate.SQLHeader(TableName: String);
begin
  IF FDMLType = dmlNone Then
    raise Exception.Create('작성하려는 작업을 선택하세요');

  IF Trim(FSQLBody) <> '' Then SQLClear;

  Case FDMLType of
    dmlInsert: FSQLBody := 'INSERT INTO '+TableName;
    dmlUpdate: FSQLBody := 'UPDATE '+TableName;
    dmlDelete: FSQLBody := 'DELETE FROM '+TableName;
  end;
end;

end.
