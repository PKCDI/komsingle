unit SQLCreator;

interface

uses
  Windows, Dialogs, StrUtils, DateUtils, Controls, SysUtils, Classes;
Type
  TDMLType = (dmlNone, dmlInsert, dmlUpdate, dmlDelete);
  TValueType = (vtString=0, vtInteger=1, vtVariant=2);
  TSQLCreate = class
    private
      FSQLBody : String;
      FDMLType : TDMLType;
      FFieldList : TStringList;
      FValueList : TStringList;
      FTypeList : TStringList;
      FWhereFieldList : TStringList;
      FWhereValueList : TStringList;
      FWhereTypeList : TStringList;
    protected
    public
      constructor Create;
      destructor Destroy;
      procedure SQLClear;
      procedure SQLHeader(TableName : String);
      procedure ADDValue(sField,sValue : String; ValueType : TValueType = vtString); overload;
      procedure ADDValue(sField : String; nValue : Integer); overload;
      procedure ADDWhere(sField,sValue : String; ValueType : TValueType = vtString); overload;
      procedure ADDWhere(sField : String; nValue : Integer); overload;

      function CreateSQL:String;

      property DMLType:TDMLType read FDMLType write FDMLType;
//      property SQL:String read CreateSQL;
  end;

implementation

{ TSQLCreate }

procedure TSQLCreate.ADDValue(sField, sValue: String; ValueType : TValueType);
begin
  FFieldList.Add(sField);
  FValueList.Add(sValue);
  Case ValueType of
    vtString : FTypeList.Add('S');
    vtInteger : FTypeList.Add('I');
    vtVariant : FTypeList.Add('V');
  end;
end;

procedure TSQLCreate.ADDValue(sField: String; nValue: Integer);
begin
  FFieldList.Add( sField );
  FValueList.Add( IntToStr(nValue) );
  FTypeList.Add('I');
end;

procedure TSQLCreate.ADDWhere(sField, sValue: String; ValueType : TValueType);
begin
  FWhereFieldList.Add( sField );
  FWhereValueList.Add( sValue );
  Case ValueType of
    vtString : FWhereTypeList.Add('S');
    vtInteger : FWhereTypeList.Add('I');
    vtVariant : FWhereTypeList.Add('V');
  end;
end;

procedure TSQLCreate.ADDWhere(sField: String; nValue: Integer);
begin
  FWhereFieldList.Add( sField );
  FWhereValueList.Add( IntToStr(nValue) );
  FWhereTypeList.Add('I');
end;

constructor TSQLCreate.Create;
begin
  FDMLType := dmlNone;

  FFieldList := TStringList.Create;
  FValueList := TStringList.Create;
  FTypeList := TStringList.Create;

  FWhereFieldList := TStringList.Create;
  FWhereValueList := TStringList.Create;
  FWhereTypeList  := TStringList.Create;
end;

function TSQLCreate.CreateSQL: String;
var
  i : integer;
  TempValue : String;
begin
  Result := '';

  Case FDMLType of
    dmlInsert:
    begin
      FSQLBody := FSQLBody+'(';
      //------------------------------------------------------------------------------
      // 필드
      //------------------------------------------------------------------------------
      for i := 0 to FFieldList.Count-1 do
      begin
        IF i = FFieldList.Count-1 Then
          FSQLBody := FSQLBody+FFieldList.Strings[i]+') VALUES('
        else
          FSQLBody := FSQLBody+FFieldList.Strings[i]+',';
      end;
      //------------------------------------------------------------------------------
      // 값
      //------------------------------------------------------------------------------
      for i := 0 to FValueList.Count-1 do
      begin
        Case AnsiIndexText( FTypeList.Strings[i] , ['S','I','V'] ) of
          0  : TempValue := QuotedStr(FValueList.Strings[i]);
          1  :
          begin
            IF Trim(FValueList.Strings[i]) = '' Then TempValue := '0'
            else TempValue := FValueList.Strings[i];
          end;
          2:
          begin
            IF Trim(FValueList.Strings[i]) = '' Then TempValue := 'NULL'
            else TempValue := FValueList.Strings[i];
          end;
        end;

        IF i = FValueList.Count-1 Then
          FSQLBody := FSQLBody+TempValue+')'
        else
          FSQLBody := FSQLBody+TempValue+',';
      end;
    end;

    dmlUpdate:
    begin
      FSQLBody := FSQLBody + ' SET '#13#10;
      //------------------------------------------------------------------------------
      // 업데이트부
      //------------------------------------------------------------------------------
      for i := 0 to FFieldList.Count-1 do
      begin
        Case AnsiIndexText( FTypeList.Strings[i] , ['S','I','V'] ) of
          0  : TempValue := QuotedStr(FValueList.Strings[i]);
          1  :
          begin
            IF Trim(FValueList.Strings[i]) = '' Then TempValue := '0'
            else TempValue := FValueList.Strings[i];
          end;
          2:
          begin
            IF Trim(FValueList.Strings[i]) = '' Then TempValue := 'NULL'
            else TempValue := FValueList.Strings[i];
          end;
        end;		


        if i = FFieldList.Count-1 Then
          FSQLBody := FSQLBody + FFieldList.Strings[i]+' = '+TempValue+#13#10
        else
          FSQLBody := FSQLBody + FFieldList.Strings[i]+' = '+TempValue+','#13#10;
      end;
      //------------------------------------------------------------------------------
      // 조건부
      //------------------------------------------------------------------------------
      for i := 0 to FWhereFieldList.Count-1 do
      begin
        Case AnsiIndexText( FWhereTypeList.Strings[i] , ['S','I','V'] ) of
          0  : TempValue := QuotedStr(FWhereValueList.Strings[i]);
          1  :
          begin
            IF Trim(FWhereValueList.Strings[i]) = '' Then TempValue := '0'
            else TempValue := FWhereValueList.Strings[i];
          end;

          2:
          begin
            IF Trim(FWhereValueList.Strings[i]) = '' Then TempValue := 'NULL'
            else TempValue := FWhereValueList.Strings[i];
          end;	
        end;

        if i = 0 Then
          FSQLBody := FSQLBody +'WHERE '+FWhereFieldList.Strings[i]+' = '+TempValue+#13#10
        else
          FSQLBody := FSQLBody +'AND '+FWhereFieldList.Strings[i]+' = '+TempValue;
      end;
    end;

    dmlDelete:
    begin
      FSQLBody := FSQLBody+#13#10;
      //------------------------------------------------------------------------------
      // 조건부
      //------------------------------------------------------------------------------
      for i := 0 to FWhereFieldList.Count-1 do
      begin
        Case AnsiIndexText( FWhereTypeList.Strings[i] , ['S','I','V'] ) of
          0  : TempValue := QuotedStr(FWhereValueList.Strings[i]);
          1  :
          begin
            IF Trim(FWhereValueList.Strings[i]) = '' Then TempValue := '0'
            else TempValue := FWhereValueList.Strings[i];
        end;
          2:
          begin
            IF Trim(FWhereValueList.Strings[i]) = '' Then TempValue := 'NULL'
            else TempValue := FWhereValueList.Strings[i];
          end;
        end;		

        if i = 0 Then
          FSQLBody := FSQLBody +'WHERE '+FWhereFieldList.Strings[i]+' = '+TempValue+#13#10
        else
          FSQLBody := FSQLBody +'AND '+FWhereFieldList.Strings[i]+' = '+TempValue
      end;

    end;
  end;
  Result := FSQLBody;
end;

destructor TSQLCreate.Destroy;
begin
  FFieldList.Free;
  FValueList.Free;
  FTypeList.Free;

  FWhereFieldList.Free;
  FWhereValueList.Free;
  FWhereTypeList.Free;
end;

procedure TSQLCreate.SQLClear;
begin
  FSQLBody := '';

  FFieldList.Clear;
  FValueList.Clear;
  FTypeList.Clear;

  FWhereFieldList.Clear;
  FWhereValueList.Clear;
  FWhereTypeList.Clear;
end;

procedure TSQLCreate.SQLHeader(TableName: String);
begin
  IF FDMLType = dmlNone Then
    raise Exception.Create('작성하려는 작업을 선택하세요');

  IF Trim(FSQLBody) <> '' Then SQLClear;

  Case FDMLType of
    dmlInsert: FSQLBody := 'INSERT INTO '+TableName;
    dmlUpdate: FSQLBody := 'UPDATE '+TableName;
    dmlDelete: FSQLBody := 'DELETE FROM '+TableName;
  end;
end;


end.
