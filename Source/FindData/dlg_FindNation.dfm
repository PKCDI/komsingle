object dlg_FindNation_frm: Tdlg_FindNation_frm
  Left = 696
  Top = 411
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = #44397#44032#53076#46300' '#51312#54924
  ClientHeight = 394
  ClientWidth = 426
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object sLabel1: TsLabel
    Left = 0
    Top = 56
    Width = 105
    Height = 12
    Caption = #9654#44397#44032' '#53076#46300' '#51312#54924
    ParentFont = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = [fsBold]
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 426
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      426
      41)
    object sComboBox1: TsComboBox
      Left = 24
      Top = 10
      Width = 81
      Height = 21
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ItemHeight = 15
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = #44397#44032#53076#46300
      Items.Strings = (
        #44397#44032#53076#46300
        #44397#44032#47749'('#50689#47928')')
    end
    object sEdit1: TsEdit
      Left = 106
      Top = 10
      Width = 159
      Height = 21
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnKeyUp = sEdit1KeyUp
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sButton1: TsButton
      Left = 266
      Top = 10
      Width = 75
      Height = 21
      Caption = #44160#49353
      TabOrder = 2
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System16
      ImageIndex = 0
      ContentMargin = 14
    end
    object sBitBtn3: TsBitBtn
      Left = 342
      Top = 10
      Width = 75
      Height = 21
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52628#44032
      TabOrder = 3
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 2
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid
    Left = 0
    Top = 72
    Width = 425
    Height = 281
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    OnKeyUp = sDBGrid1KeyUp
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #44397#44032#53076#46300
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Title.Caption = #44397#44032#47749'('#50689#47928')'
        Width = 313
        Visible = True
      end>
  end
  object sButton2: TsButton
    Left = 136
    Top = 362
    Width = 75
    Height = 29
    Caption = #54869#51064
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = sButton2Click
    SkinData.SkinSection = 'BUTTON'
    Images = DMICON.System18
    ImageIndex = 17
  end
  object sButton3: TsButton
    Left = 216
    Top = 362
    Width = 75
    Height = 29
    Cancel = True
    Caption = #52712#49548
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = sButton3Click
    SkinData.SkinSection = 'BUTTON'
    Images = DMICON.System18
    ImageIndex = 18
  end
  object sSkinProvider1: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'MS Sans Serif'
    AddedTitle.Font.Style = []
    SkinData.SkinSection = 'FORM'
    ShowAppIcon = False
    TitleButtons = <>
    Top = 200
  end
  object qryList_MSSQL: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryList_MSSQLAfterOpen
    Parameters = <
      item
        Name = 'Prefix'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'SELECT CODE,NAME'
      'FROM CODE2NDD'
      'WHERE Prefix = :Prefix'
      '')
    Left = 128
    Top = 56
  end
  object qryList_Paradox: TQuery
    SQL.Strings = (
      'SELECT CODE,NAME'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#44397#44032#39
      #9'  AND'
      #9'  Remark = 1')
    Left = 160
    Top = 56
  end
  object dsList: TDataSource
    DataSet = qryList_MSSQL
    Left = 128
    Top = 88
  end
end
