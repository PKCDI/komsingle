unit dlg_FindCity;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DBTables, DB, ADODB, Grids, DBGrids, acDBGrid,
  StdCtrls, sLabel, sButton, sEdit, sComboBox, ExtCtrls, sPanel,
  sSkinProvider, Buttons, sBitBtn;

type
  Tdlg_FindCity_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    com_SearchKeyword: TsComboBox;
    edt_Search: TsEdit;
    sButton1: TsButton;
    sLabel1: TsLabel;
    sDBGrid1: TsDBGrid;
    sButton2: TsButton;
    sButton3: TsButton;
    qryList: TADOQuery;
    dsList: TDataSource;
    sBitBtn3: TsBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure edt_SearchKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    FSQL: string;
    procedure ReadList;
  public
    { Public declarations }
     FGroup: string;
    function openDialog(sGroup: string; DefaultKeyWord: string = ''): TModalResult;

  end;
var
  dlg_FindCity_frm: Tdlg_FindCity_frm;

implementation

uses CODE_MSSQL;

{$R *.dfm}

{ Tdlg_FindCity_frm }



procedure Tdlg_FindCity_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure Tdlg_FindCity_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Parameters.ParamByName('Prefix').Value := FGroup;
    case com_SearchKeyword.ItemIndex of
      0:
        SQL.Add('AND [CODE] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
      1:
        SQL.Add('AND [NAME] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
    end;
    Open;
  end;
end;

procedure Tdlg_FindCity_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure Tdlg_FindCity_frm.edt_SearchKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      ReadList;
    VK_DOWN:
      sDBGrid1.SetFocus;
  end;
end;

procedure Tdlg_FindCity_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_ESCAPE then
    ModalResult := mrCancel;
end;
procedure Tdlg_FindCity_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure Tdlg_FindCity_frm.sDBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sDBGrid1DblClick(sDBGrid1);
end;

procedure Tdlg_FindCity_frm.FormShow(Sender: TObject);
begin
  inherited;
  sDBGrid1.SetFocus;
end;

procedure Tdlg_FindCity_frm.sBitBtn3Click(Sender: TObject);
begin
  inherited;

  CODE_MSSQL_frm := TCODE_MSSQL_frm.Create(Self);

  try
    if CODE_MSSQL_frm.Run(FGroup) = mrOk then
    begin
      qryList.Close;
      qryList.Open;
    end;
  finally
    FreeAndNil(CODE_MSSQL_frm);
  end;

end;

function Tdlg_FindCity_frm.openDialog(sGroup,
  DefaultKeyWord: string): TModalResult;
begin
  FGroup := sGroup;
  ReadList;
  if Trim(DefaultKeyWord) <> '' then
  begin
    qryList.Locate('CODE', DefaultKeyWord, []);
  end;

  Result := ShowModal;
end;

end.
