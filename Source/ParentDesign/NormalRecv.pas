unit NormalRecv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, DBCtrls, sDBMemo, sDBEdit, sGroupBox,
  Grids, DBGrids, acDBGrid, sButton, sEdit, sComboBox, Mask, sMaskEdit,
  ComCtrls, sPageControl, Buttons, sSpeedButton, sLabel, ExtCtrls,
  sSplitter, sPanel, sSkinProvider, DateUtils;

type
  TNormalRecv_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSplitter2: TsSplitter;
    sLabel7: TsLabel;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sLabel6: TsLabel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sMaskEdit6: TsMaskEdit;
    sMaskEdit7: TsMaskEdit;
    sEdit3: TsEdit;
    sButton1: TsButton;
    sDBGrid1: TsDBGrid;
    sTabSheet3: TsTabSheet;
    sPanel2: TsPanel;
    sDBEdit1: TsDBEdit;
    sDBEdit23: TsDBEdit;
    sSplitter1: TsSplitter;
    sPanel3: TsPanel;
    sLabel1: TsLabel;
    sDBEdit33: TsDBEdit;
    sDBEdit34: TsDBEdit;
    sDBEdit35: TsDBEdit;
    sDBEdit36: TsDBEdit;
    sDBEdit37: TsDBEdit;
    sDBEdit38: TsDBEdit;
    sDBEdit39: TsDBEdit;
    sPanel4: TsPanel;
    sLabel2: TsLabel;
    sDBEdit40: TsDBEdit;
    sDBEdit41: TsDBEdit;
    sDBEdit42: TsDBEdit;
    sDBEdit43: TsDBEdit;
    sDBEdit44: TsDBEdit;
    sPanel5: TsPanel;
    sLabel3: TsLabel;
    sDBEdit45: TsDBEdit;
    sDBEdit46: TsDBEdit;
    sDBEdit47: TsDBEdit;
    sDBEdit48: TsDBEdit;
    sDBEdit49: TsDBEdit;
    sPanel6: TsPanel;
    sSplitter4: TsSplitter;
    sLabel4: TsLabel;
    sDBMemo1: TsDBMemo;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    Btn_Print: TsSpeedButton;
    Btn_Ready: TsSpeedButton;
    Btn_Save: TsSpeedButton;
    Btn_Cancel: TsSpeedButton;
    Btn_Temp: TsSpeedButton;
    sSpeedButton26: TsSpeedButton;
    sSpeedButton27: TsSpeedButton;
    sEdit1: TsEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NormalRecv_frm: TNormalRecv_frm;

implementation

{$R *.dfm}

procedure TNormalRecv_frm.FormCreate(Sender: TObject);
begin
  inherited;
  sMaskEdit6.Text := FormatDateTime('YYYYMMDD',StartOfTheMonth(Now));
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD',Now);
end;

end.
