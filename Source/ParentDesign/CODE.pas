unit CODE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sLabel, ExtCtrls, sSplitter,
  Buttons, sSpeedButton, sPanel, Grids, DBGrids, acDBGrid, sBitBtn, sEdit,
  sComboBox;

type
  TCODE_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton3: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sSplitter2: TsSplitter;
    sLabel1: TsLabel;
    sPanel2: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton6: TsSpeedButton;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel6: TsPanel;
    sPanel5: TsPanel;
    sDBGrid2: TsDBGrid;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    Btn_DetailNew: TsSpeedButton;
    Btn_DetailMod: TsSpeedButton;
    Btn_DetailDel: TsSpeedButton;
    sSplitter1: TsSplitter;
    sLabel3: TsLabel;
    sSpeedButton9: TsSpeedButton;
    sSpeedButton10: TsSpeedButton;
    Btn_DetailCheck: TsSpeedButton;
    Btn_DetailNoCheck: TsSpeedButton;
    sSplitter3: TsSplitter;
    sLabel4: TsLabel;
    sPanel7: TsPanel;
    sEdit1: TsEdit;
    sBitBtn1: TsBitBtn;
    sComboBox1: TsComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CODE_frm: TCODE_frm;

implementation

uses MSSQL;

{$R *.dfm}

end.
