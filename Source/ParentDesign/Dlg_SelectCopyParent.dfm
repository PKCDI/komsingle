inherited Dlg_SelectCopyParent_frm: TDlg_SelectCopyParent_frm
  Left = 534
  Top = 379
  Caption = '['#44060#49444#50629#52404']'#45236#44397#49888#50857#51109' '#48373#49324#54624' '#50896#48376#49440#53469
  ClientHeight = 423
  ClientWidth = 790
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 790
    Height = 423
    object sPanel7: TsPanel
      Left = 1
      Top = 1
      Width = 788
      Height = 32
      Align = alTop
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'PAGECONTROL'
      DesignSize = (
        788
        32)
      object sMaskEdit6: TsMaskEdit
        Tag = 1
        Left = 64
        Top = 6
        Width = 73
        Height = 20
        Color = clWhite
        EditMask = '9999-99-99;0;'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = []
        ImeName = 'Microsoft IME 2010'
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '20130101'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 4276545
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #44404#47548#52404
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.CustomColor = True
        SkinData.SkinSection = 'PAGECONTROL'
      end
      object sComboBox5: TsComboBox
        Left = 272
        Top = 6
        Width = 105
        Height = 20
        Alignment = taLeftJustify
        BoundLabel.Active = True
        BoundLabel.Caption = #44620#51648
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 4276545
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #44404#47548#52404
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = []
        ImeName = 'Microsoft IME 2010'
        ItemHeight = 14
        ItemIndex = 0
        ParentFont = False
        TabOrder = 1
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840
          #44277#44553#51088)
      end
      object sMaskEdit7: TsMaskEdit
        Tag = 1
        Left = 168
        Top = 6
        Width = 73
        Height = 20
        Color = clWhite
        EditMask = '9999-99-99;0;'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = []
        ImeName = 'Microsoft IME 2010'
        MaxLength = 10
        ParentFont = False
        TabOrder = 2
        Text = '20130101'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #48512#53552
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 4276545
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #44404#47548#52404
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.CustomColor = True
        SkinData.SkinSection = 'PAGECONTROL'
      end
      object sEdit3: TsEdit
        Left = 378
        Top = 6
        Width = 183
        Height = 20
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = []
        ImeName = 'Microsoft IME 2010'
        ParentFont = False
        TabOrder = 3
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object sButton1: TsButton
        Left = 562
        Top = 6
        Width = 75
        Height = 20
        Caption = #51312#54924
        TabOrder = 4
        SkinData.SkinSection = 'BUTTON'
      end
      object sButton2: TsButton
        Left = 1007
        Top = 6
        Width = 75
        Height = 20
        Anchors = [akTop, akRight]
        Caption = #45803#44592
        TabOrder = 5
        SkinData.SkinSection = 'BUTTON'
      end
      object sButton3: TsButton
        Left = 709
        Top = 6
        Width = 75
        Height = 20
        Anchors = [akTop, akRight]
        Caption = #45803#44592
        TabOrder = 6
        OnClick = sButton3Click
        SkinData.SkinSection = 'BUTTON'
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 33
      Width = 788
      Height = 389
      Align = alClient
      Color = clWhite
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ImeName = 'Microsoft IME 2010'
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDblClick = sDBGrid1DblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 64
    Top = 48
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 96
    Top = 48
  end
end
