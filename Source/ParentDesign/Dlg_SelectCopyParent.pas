unit Dlg_SelectCopyParent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, Grids, DBGrids, acDBGrid, StdCtrls, sButton,
  sEdit, sComboBox, Mask, sMaskEdit, sSkinProvider, ExtCtrls, sPanel, DB,
  ADODB, DateUtils;

type
  TDlg_SelectCopyParent_frm = class(TDialogParent_frm)
    sPanel7: TsPanel;
    sMaskEdit6: TsMaskEdit;
    sComboBox5: TsComboBox;
    sMaskEdit7: TsMaskEdit;
    sEdit3: TsEdit;
    sButton1: TsButton;
    sButton2: TsButton;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    sButton3: TsButton;
    procedure FormCreate(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
  private
    { Private declarations }
  protected
    FSQL : String;
    procedure ReadList; virtual;
  public
    { Public declarations }
  end;

var
  Dlg_SelectCopyParent_frm: TDlg_SelectCopyParent_frm;

implementation

uses MSSQL;

{$R *.dfm}

procedure TDlg_SelectCopyParent_frm.FormCreate(Sender: TObject);
begin
  inherited;
  sMaskEdit6.Text := FormatDateTime('YYYYMMDD',StartOfTheMonth(Now));
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD',Now);

  FSQL := qryList.SQL.Text;
end;

procedure TDlg_SelectCopyParent_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  IF qryList.RecordCount = 0 Then Exit;
end;

procedure TDlg_SelectCopyParent_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_SelectCopyParent_frm.ReadList;
begin
end;

end.
