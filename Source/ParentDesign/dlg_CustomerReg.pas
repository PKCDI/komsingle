unit dlg_CustomerReg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, Mask, DBCtrls, sDBEdit,
  Buttons, sSpeedButton, ExtCtrls, sPanel, sEdit, sBevel, sButton,TypeDefine,
  sMaskEdit, StrUtils, DB, ADODB, sCheckBox, OleCtrls, sSplitter;

type
  Tdlg_CustomerReg_frm = class(TChildForm_frm)
    sPanel2: TsPanel;
    edt_Sangho: TsEdit;
    edt_CODE: TsEdit;
    edt_TradeNo: TsEdit;
    edt_MstID: TsEdit;
    edt_DtID: TsEdit;
    edt_Zipcode: TsEdit;
    edt_Addr1: TsEdit;
    edt_Addr2: TsEdit;
    edt_Addr3: TsEdit;
    edt_City: TsEdit;
    sSpeedButton1: TsSpeedButton;
    edt_CityContents: TsEdit;
    edt_Nation: TsEdit;
    sSpeedButton2: TsSpeedButton;
    edt_NationContents: TsEdit;
    edt_Name: TsEdit;
    edt_Tel: TsEdit;
    edt_FAX: TsEdit;
    edt_DamName: TsEdit;
    edt_DAMPart: TsEdit;
    edt_DAMTel: TsEdit;
    edt_DAMFAX: TsEdit;
    edt_Jenja: TsEdit;
    edt_Email1: TsEdit;
    edt_Email2: TsEdit;
    sButton1: TsButton;
    sButton2: TsButton;
    sBevel1: TsBevel;
    edt_SaupNo: TsEdit;
    qryInsMSSQL: TADOQuery;
    qryModMSSQL: TADOQuery;
    qryDelMSSQL: TADOQuery;
    sCheckBox1: TsCheckBox;
    qryCheck: TADOQuery;
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure edt_NationExit(Sender: TObject);
    procedure edt_SaupNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt_SaupNoEnter(Sender: TObject);
    procedure edt_SaupNoExit(Sender: TObject);
    procedure edt_SaupNoKeyPress(Sender: TObject; var Key: Char);
    procedure sButton7Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure sSpeedButton3Click(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    FProgramControlType : TProgramControlType;
    FFields : TFields;
    procedure SetParamAndRun(DataSet : TADOQuery);
    procedure SetData;
    function GetNewCODE:String;
    procedure CloseFindAddress;
    procedure OpenFindAddress;
    function CheckOverlab:Boolean;
  public
    { Public declarations }
    function Run(userType : TProgramControlType;userFields : TFields):TModalResult;
    function DeleteData(userCODE : String):TModalResult;
  end;

var
  dlg_CustomerReg_frm: Tdlg_CustomerReg_frm;

implementation

uses dlg_FindNation, MSSQL, MessageDefine, Commonlib, dlg_FindCity;

{$R *.dfm}

procedure Tdlg_CustomerReg_frm.sSpeedButton2Click(Sender: TObject);
begin
  inherited;
  dlg_FindNation_frm := Tdlg_FindNation_frm.Create(Self);  
  try
    IF dlg_FindNation_frm.RUN('국가',edt_Nation.Text) = mrok then
    begin
      edt_Nation.Text := dlg_FindNation_frm.SelectCODE;
      edt_NationContents.Text := dlg_FindNation_frm.SelectNAME;
    end;
  except
    FreeAndNil(dlg_FindNation_frm);
  end;
end;
procedure Tdlg_CustomerReg_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure Tdlg_CustomerReg_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  if edt_CODE.Text = '' then
  begin
    MessageBox(Self.Handle,MSG_CustomerReg_CODE_EMPRY,'저장오류',MB_OK+MB_ICONINFORMATION);
    edt_CODE.SetFocus;
  end
  else IF CheckOverlab AND (FProgramControlType = ctInsert) Then
  begin
    raise Exception.Create('코드 ['+edt_CODE.Text+']는 이미 사용중인 코드입니다.');
    edt_CODE.SetFocus;
  end
  else
    ModalResult := mrOk;

end;

procedure Tdlg_CustomerReg_frm.edt_NationExit(Sender: TObject);
begin
  inherited;
  dlg_FindNation_frm := Tdlg_FindNation_frm.Create(Self);
  try
    IF (not dlg_FindNation_frm.isValidData('국가',edt_Nation.Text)) OR (edt_Nation.Text = '' ) Then
    begin
      IF dlg_FindNation_frm.RUN('국가','') = mrok then
      begin
        edt_Nation.Text := dlg_FindNation_frm.SelectCODE;
        edt_NationContents.Text := dlg_FindNation_frm.SelectNAME;
      end
      else
      begin
        edt_NationContents.Text := 'NOT FOUND DATA!';
      end;

    end
    else
    begin
      edt_NationContents.Text := dlg_FindNation_frm.GetContent('국가',edt_Nation.Text)
    end;
  finally
    FreeAndNil(dlg_FindNation_frm);
  end;
end;

procedure Tdlg_CustomerReg_frm.edt_SaupNoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Length(edt_SaupNo.Text) = 10 Then
  begin
    SelectNext(ActiveControl, True, True);
  end;
end;

procedure Tdlg_CustomerReg_frm.edt_SaupNoEnter(Sender: TObject);
var
  TempStr : String;
begin
  inherited;
  TempStr := AnsiReplaceText(edt_SaupNo.Text,'-','');
  IF (TempStr <> '') AND (Length(TempStr)=10) Then
  begin
    edt_SaupNo.Text := TempStr;
  end;
end;

procedure Tdlg_CustomerReg_frm.edt_SaupNoExit(Sender: TObject);
var
  TempStr : String;
begin
  inherited;
  IF (edt_SaupNo.Text <> '') AND (Length(edt_SaupNo.Text)=10) Then
  begin
    TempStr := edt_SaupNo.Text;
    edt_SaupNo.Text := LeftStr(TempStr,3)+'-'+
                   MidStr(TempStr,4,2)+'-'+
                   RightStr(TempStr,5);
  end;
end;

procedure Tdlg_CustomerReg_frm.edt_SaupNoKeyPress(Sender: TObject;
  var Key: Char);
begin
//  inherited;

end;

function Tdlg_CustomerReg_frm.Run(userType : TProgramControlType;userFields : TFields):TModalResult;
begin
 Result := mrCancel;

  FProgramControlType := userType;
  FFields := userFields;

  sCheckBox1.Enabled := FProgramControlType = ctInsert;

  Case FProgramControlType of
    ctInsert :
    begin
     // edt_CODE.Text := '자동생성';
    end;
    ctModify :
    begin
      SetData;
    end;  
    ctDelete :
    begin
      IF ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'[코드] : '+FFields.FieldByName('CODE').AsString+#13#10'삭제한 데이터를 복구가 불가능 합니다') Then
      begin
        DeleteData(FFields.FieldByName('CODE').AsString);
        Result := mrOk;
        Exit;
      end
      else
      begin
        Result := mrCancel;
        Exit;
      end;
    end;

  end;

  if Self.ShowModal = mrOK Then
  begin
    Case FProgramControlType of
      ctInsert : SetParamAndRun(qryInsMSSQL);
      ctModify : SetParamAndRun(qryModMSSQL);
    end;
    Result := mrOk;
  end;
end;

procedure Tdlg_CustomerReg_frm.SetParamAndRun(DataSet : TADOQuery);
begin
  with DataSet do
  begin
    Close;
//    IF sCheckBox1.Checked Then
//      Parameters.ParamByName('CODE'     ).Value := '00000'
//    else
//    begin
//      CASE FProgramControlType OF
//        ctInsert : Parameters.ParamByName('CODE'     ).Value := GetNewCODE;
//        ctModify : Parameters.ParamByName('CODE'     ).Value := edt_CODE.Text;
//      END;
//    end;
    parameters.ParamByName('CODE'     ).Value := edt_CODE.Text;
    Parameters.ParamByName('TRAD_NO'  ).Value := edt_TradeNo.Text;
    Parameters.ParamByName('SAUP_NO'  ).Value := AnsiReplaceText( edt_SaupNo.Text , '-', '');
    Parameters.ParamByName('ENAME'    ).Value := edt_Sangho.Text;
    Parameters.ParamByName('REP_NAME' ).Value := edt_Name.Text;
    Parameters.ParamByName('REP_TEL'  ).Value := edt_Tel.Text;
    Parameters.ParamByName('REP_FAX'  ).Value := edt_FAX.Text;
    Parameters.ParamByName('DDAN_DEPT').Value := edt_DAMPart.Text;
    Parameters.ParamByName('DDAN_NAME').Value := edt_DamName.Text;
    Parameters.ParamByName('DDAN_TEL' ).Value := edt_DAMTel.Text;
    Parameters.ParamByName('DDAN_FAX' ).Value := edt_DAMFAX.Text;
    Parameters.ParamByName('ZIPCD'    ).Value := edt_Zipcode.Text;
    Parameters.ParamByName('ADDR1'    ).Value := edt_Addr1.Text;
    Parameters.ParamByName('ADDR2'    ).Value := edt_Addr2.Text;
    Parameters.ParamByName('ADDR3'    ).Value := edt_Addr3.Text;
    Parameters.ParamByName('CITY'     ).Value := edt_City.Text;
    Parameters.ParamByName('NAT'      ).Value := edt_Nation.Text;
    Parameters.ParamByName('ESU1'     ).Value := edt_MstID.Text;
    Parameters.ParamByName('ESU2'     ).Value := edt_DtID.Text;
    Parameters.ParamByName('ESU3'     ).Value := '';
    Parameters.ParamByName('Jenja'    ).Value := edt_Jenja.Text;
    Parameters.ParamByName('ADDR4'    ).Value := '';
    Parameters.ParamByName('ADDR5'    ).Value := '';
    Parameters.ParamByName('EMAIL_ID' ).Value := edt_Email1.Text;
    Parameters.ParamByName('EMAIL_DOMAIN').Value := edt_Email2.Text;
    ExecSQL;
  end;
end;

procedure Tdlg_CustomerReg_frm.SetData;
begin
  //수정시 모든 데이터 출력
  edt_CODE.Text     :=  FFields.FieldByName('CODE'  ).asString;
  edt_TradeNo.Text  :=  FFields.FieldByName('TRAD_NO'  ).asString;
  edt_SaupNo.Text   :=  FFields.FieldByName('SAUP_NO'  ).asString;
  edt_Sangho.Text   :=  FFields.FieldByName('ENAME'    ).asString;
  edt_Name.Text     :=  FFields.FieldByName('REP_NAME' ).asString;
  edt_Tel.Text      :=  FFields.FieldByName('REP_TEL'  ).asString;
  edt_FAX.Text      :=  FFields.FieldByName('REP_FAX'  ).asString;
  edt_DAMPart.Text  :=  FFields.FieldByName('DDAN_DEPT').asString;
  edt_DamName.Text  :=  FFields.FieldByName('DDAN_NAME').asString;
  edt_DAMTel.Text   :=  FFields.FieldByName('DDAN_TEL' ).asString;
  edt_DAMFAX.Text   :=  FFields.FieldByName('DDAN_FAX' ).asString;
  edt_Zipcode.Text  :=  FFields.FieldByName('ZIPCD'    ).asString;
  edt_Addr1.Text    :=  FFields.FieldByName('ADDR1'    ).asString;
  edt_Addr2.Text    :=  FFields.FieldByName('ADDR2'    ).asString;
  edt_Addr3.Text    :=  FFields.FieldByName('ADDR3'    ).asString;
  edt_City.Text     :=  FFields.FieldByName('CITY'     ).asString;
  edt_Nation.Text   :=  FFields.FieldByName('NAT'      ).asString;
  edt_MstID.Text    :=  FFields.FieldByName('ESU1'     ).asString;
  edt_DtID.Text     :=  FFields.FieldByName('ESU2'     ).asString;
  edt_Jenja.Text    :=  FFields.FieldByName('Jenja'    ).asString;
  edt_Email1.Text   :=  FFields.FieldByName('EMAIL_ID' ).asString;
  edt_Email2.Text   :=  FFields.FieldByName('EMAIL_DOMAIN').asString;
  dlg_FindNation_frm := Tdlg_FindNation_frm.Create(Self);
  try
    edt_NationContents.Text := dlg_FindNation_frm.GetContent('국가',edt_Nation.Text);
  finally
    FreeAndNil(dlg_FindNation_frm);
  end;
end;

function Tdlg_CustomerReg_frm.GetNewCODE: String;
var
  MAXQUERY : TADOQuery;
  TempNo : integer;
begin
  MAXQUERY := TADOQuery.Create(Self);
  try
    with MAXQUERY do
    begin
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT MAX(CODE) as MAXCODE FROM CUSTOM';
      Open;

      IF FieldByName('MAXCODE').IsNull Then
      begin
        Result := '00001';
      end
      else
      begin
        TempNo := MAXQUERY.FieldByName('MAXCODE').AsInteger;
        Result := FormatFloat('00000',TempNo+1);
      end;
      MAXQUERY.Close;
    end;
  finally
    MAXQUERY.Free;
  end;
end;

function Tdlg_CustomerReg_frm.DeleteData(userCODE: String): TModalResult;
begin
  Result := mrCancel;
  if ConfirmMessage('해당 데이터를 정말 삭제하시겠습니까?'#13#10'코드 : '+userCODE) then
  begin
    with qryDelMSSQL do
    begin
      Close;
      Parameters.ParamByName('CODE').Value := userCODE;
      ExecSQL;
    end;
    Result := mrOk;
  end;
end;

procedure Tdlg_CustomerReg_frm.OpenFindAddress;
begin
//  WebBrowser1.Navigate('http://ikis21.com/FIND_POST.html');
//  sPanel3.Visible := True;
end;

procedure Tdlg_CustomerReg_frm.CloseFindAddress;
begin
//  sPanel3.Visible := False;
end;


procedure Tdlg_CustomerReg_frm.sButton7Click(Sender: TObject);
var
  TempADDR : String;
begin
  inherited;
//  edt_Zipcode.Text := WebBrowser1.OleObject.Document.getElementById('kis_post5').Value;

//  TempADDR := WebBrowser1.OleObject.Document.getElementById('kis_roadAddr').Value+' '+WebBrowser1.OleObject.Document.getElementById('kis_detailAddr').Value;
//  edt_Addr1.Text := CopyK(TempADDR,1,35);
//  edt_Addr2.Text := CopyK(TempADDR,36,Length(TempADDR));
//  CloseFindAddress;
end;
   
procedure Tdlg_CustomerReg_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  CloseFindAddress;
end;

procedure Tdlg_CustomerReg_frm.sSpeedButton3Click(Sender: TObject);
begin
  inherited;
  OpenFindAddress;
end;

function Tdlg_CustomerReg_frm.CheckOverlab: Boolean;
begin
  with qryCheck do
  begin
    Close;
    SQL.Text := 'SELECT 1 FROM  [CUSTOM] WHERE [CODE] = '+QuotedStr(edt_CODE.Text);
    Open;

    Result := qryCheck.RecordCount > 0;
  end;
end;

procedure Tdlg_CustomerReg_frm.sSpeedButton1Click(Sender: TObject);
begin
  inherited;
  dlg_FindCity_frm := Tdlg_FindCity_frm.Create(Self);
  try
      if dlg_FindCity_frm.openDialog('도시') = mrOK then
      begin
          //선택된 코드,도시명 출력
          edt_City.Text := dlg_FindCity_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
          edt_CityContents.Text := dlg_FindCity_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
      end;
  finally
    FreeAndNil(dlg_FindCity_frm);
  end;
end;

end.
