inherited Bank_frm: TBank_frm
  Left = 843
  Top = 210
  Caption = #51008#54665#53076#46300' '#44288#47532
  ClientHeight = 568
  ClientWidth = 660
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 660
    Height = 62
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton3: TsSpeedButton
      Left = 377
      Top = 1
      Width = 11
      Height = 60
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Close: TsSpeedButton
      Left = 604
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #45803#44592
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 20
      Images = DMICON.System32
      Reflected = True
    end
    object sSpeedButton4: TsSpeedButton
      Left = 388
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #52636#47141
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Visible = False
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 21
      Images = DMICON.System32
      Reflected = True
    end
    object sSpeedButton5: TsSpeedButton
      Left = 593
      Top = 1
      Width = 11
      Height = 60
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSplitter2: TsSplitter
      Left = 1
      Top = 1
      Width = 200
      Height = 60
      Cursor = crHSplit
      AutoSnap = False
      Enabled = False
      SkinData.SkinSection = 'SPLITTER'
    end
    object sLabel1: TsLabel
      Left = 50
      Top = 21
      Width = 102
      Height = 21
      Caption = #51008#54665#53076#46300' '#44288#47532
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object Btn_New: TsSpeedButton
      Left = 212
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #49888#44508
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 5
      Images = DMICON.System32
      Reflected = True
    end
    object Btn_Modify: TsSpeedButton
      Tag = 1
      Left = 267
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #49688#51221
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 8
      Images = DMICON.System32
      Reflected = True
    end
    object Btn_Del: TsSpeedButton
      Tag = 2
      Left = 322
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #49325#51228
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 19
      Images = DMICON.System32
      Reflected = True
    end
    object sSpeedButton1: TsSpeedButton
      Left = 201
      Top = 1
      Width = 11
      Height = 60
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
  end
  object sPanel7: TsPanel [1]
    Left = 0
    Top = 62
    Width = 660
    Height = 32
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object sEdit1: TsEdit
      Left = 94
      Top = 6
      Width = 121
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sBitBtn1: TsBitBtn
      Left = 216
      Top = 6
      Width = 47
      Height = 21
      Caption = #51312#54924
      TabOrder = 1
      SkinData.SkinSection = 'BUTTON'
    end
    object sComboBox1: TsComboBox
      Left = 4
      Top = 6
      Width = 89
      Height = 21
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ItemHeight = 15
      ItemIndex = 0
      ParentFont = False
      TabOrder = 2
      Text = #51008#54665#53076#46300
      Items.Strings = (
        #51008#54665#53076#46300
        #51008#54665#47749)
    end
  end
  object sDBGrid1: TsDBGrid [2]
    Left = 0
    Top = 94
    Width = 660
    Height = 474
    Align = alClient
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    SkinData.SkinSection = 'EDIT'
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 24
  end
end
