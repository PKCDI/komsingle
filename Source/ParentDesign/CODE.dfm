inherited CODE_frm: TCODE_frm
  Left = 923
  Top = 204
  Caption = #54364#51456'/'#51068#48152#53076#46300' '#44288#47532
  ClientHeight = 604
  ClientWidth = 772
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 772
    Height = 62
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton3: TsSpeedButton
      Left = 201
      Top = 1
      Width = 11
      Height = 60
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Close: TsSpeedButton
      Left = 716
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #45803#44592
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 20
      Images = DMICON.System32
      Reflected = True
    end
    object sSpeedButton4: TsSpeedButton
      Left = 212
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #52636#47141
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 21
      Images = DMICON.System32
      Reflected = True
    end
    object sSpeedButton5: TsSpeedButton
      Left = 705
      Top = 1
      Width = 11
      Height = 60
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSplitter2: TsSplitter
      Left = 1
      Top = 1
      Width = 200
      Height = 60
      Cursor = crHSplit
      AutoSnap = False
      Enabled = False
      SkinData.SkinSection = 'SPLITTER'
    end
    object sLabel1: TsLabel
      Left = 31
      Top = 21
      Width = 141
      Height = 21
      Caption = #54364#51456'/'#51068#48152#53076#46300' '#44288#47532
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
  end
  object sPanel2: TsPanel [1]
    Left = 0
    Top = 62
    Width = 772
    Height = 41
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object sSpeedButton2: TsSpeedButton
      Left = 4
      Top = 8
      Width = 89
      Height = 25
      GroupIndex = 1
      Down = True
      Caption = #54364#51456#53076#46300
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton6: TsSpeedButton
      Left = 94
      Top = 8
      Width = 89
      Height = 25
      GroupIndex = 1
      Caption = #51068#48152#53076#46300
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel2: TsLabel
      Left = 192
      Top = 13
      Width = 311
      Height = 15
      Caption = #54364#51456#53076#46300#45716' '#51221#54644#51256' '#51080#45716' '#53076#46300#51060#48064#47196' '#49688#51221#51060' '#48520#44032#45733#54633#45768#45796'.'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
  end
  object sPanel3: TsPanel [2]
    Left = 0
    Top = 103
    Width = 313
    Height = 501
    Align = alLeft
    TabOrder = 2
    SkinData.SkinSection = 'GROUPBOX'
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 89
      Width = 311
      Height = 411
      Align = alClient
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #44404#47548#52404
      TitleFont.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object sPanel6: TsPanel
      Left = 1
      Top = 1
      Width = 311
      Height = 56
      Align = alTop
      TabOrder = 1
      SkinData.SkinSection = 'DIVIDER'
      object Btn_New: TsSpeedButton
        Left = 89
        Top = 1
        Width = 55
        Height = 54
        Cursor = crHandPoint
        Caption = #49888#44508
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        Align = alLeft
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 5
        Images = DMICON.System32
        Reflected = True
      end
      object Btn_Modify: TsSpeedButton
        Tag = 1
        Left = 144
        Top = 1
        Width = 55
        Height = 54
        Cursor = crHandPoint
        Caption = #49688#51221
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        Align = alLeft
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 8
        Images = DMICON.System32
        Reflected = True
      end
      object Btn_Del: TsSpeedButton
        Tag = 2
        Left = 199
        Top = 1
        Width = 55
        Height = 54
        Cursor = crHandPoint
        Caption = #49325#51228
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        Align = alLeft
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 19
        Images = DMICON.System32
        Reflected = True
      end
      object sSplitter3: TsSplitter
        Left = 1
        Top = 1
        Width = 88
        Height = 54
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sLabel4: TsLabel
        Left = 26
        Top = 18
        Width = 38
        Height = 21
        Caption = #48516' '#47448
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
    end
    object sPanel7: TsPanel
      Left = 1
      Top = 57
      Width = 311
      Height = 32
      Align = alTop
      TabOrder = 2
      SkinData.SkinSection = 'GROUPBOX'
      object sEdit1: TsEdit
        Left = 136
        Top = 6
        Width = 121
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = #44404#47548#52404
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        SkinData.CustomColor = True
        SkinData.SkinSection = 'GROUPBOX'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object sBitBtn1: TsBitBtn
        Left = 258
        Top = 6
        Width = 47
        Height = 21
        Caption = #51312#54924
        TabOrder = 1
        SkinData.SkinSection = 'BUTTON'
      end
      object sComboBox1: TsComboBox
        Left = 46
        Top = 6
        Width = 89
        Height = 21
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #44404#47548#52404
        Font.Style = []
        ItemHeight = 15
        ItemIndex = 0
        ParentFont = False
        TabOrder = 2
        Text = #44396#48516#53076#46300
        Items.Strings = (
          #44396#48516#53076#46300
          #47749#52845)
      end
    end
  end
  object sPanel4: TsPanel [3]
    Left = 313
    Top = 103
    Width = 459
    Height = 501
    Align = alClient
    TabOrder = 3
    SkinData.SkinSection = 'GROUPBOX'
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 457
      Height = 56
      Align = alTop
      TabOrder = 0
      SkinData.SkinSection = 'DIVIDER'
      object Btn_DetailNew: TsSpeedButton
        Left = 99
        Top = 1
        Width = 55
        Height = 54
        Cursor = crHandPoint
        Caption = #49888#44508
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        Align = alLeft
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 5
        Images = DMICON.System32
        Reflected = True
      end
      object Btn_DetailMod: TsSpeedButton
        Tag = 1
        Left = 154
        Top = 1
        Width = 55
        Height = 54
        Cursor = crHandPoint
        Caption = #49688#51221
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        Align = alLeft
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 8
        Images = DMICON.System32
        Reflected = True
      end
      object Btn_DetailDel: TsSpeedButton
        Tag = 2
        Left = 209
        Top = 1
        Width = 55
        Height = 54
        Cursor = crHandPoint
        Caption = #49325#51228
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        Align = alLeft
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 19
        Images = DMICON.System32
        Reflected = True
      end
      object sSplitter1: TsSplitter
        Left = 1
        Top = 1
        Width = 88
        Height = 54
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sLabel3: TsLabel
        Left = 5
        Top = 18
        Width = 80
        Height = 21
        Caption = #49345#49464#45936#51060#53552
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sSpeedButton9: TsSpeedButton
        Left = 89
        Top = 1
        Width = 10
        Height = 54
        Cursor = crHandPoint
        Layout = blGlyphTop
        Spacing = 0
        Align = alLeft
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
      end
      object sSpeedButton10: TsSpeedButton
        Left = 264
        Top = 1
        Width = 10
        Height = 54
        Cursor = crHandPoint
        Layout = blGlyphTop
        Spacing = 0
        Align = alLeft
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
      end
      object Btn_DetailCheck: TsSpeedButton
        Tag = 2
        Left = 274
        Top = 1
        Width = 55
        Height = 54
        Cursor = crHandPoint
        Caption = #49324#50857
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        Align = alLeft
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 17
        Images = DMICON.System32
        Reflected = True
      end
      object Btn_DetailNoCheck: TsSpeedButton
        Tag = 2
        Left = 329
        Top = 1
        Width = 55
        Height = 54
        Cursor = crHandPoint
        Caption = #54644#51228
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        Align = alLeft
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 18
        Images = DMICON.System32
        Reflected = True
      end
    end
    object sDBGrid2: TsDBGrid
      Left = 1
      Top = 57
      Width = 457
      Height = 443
      Align = alClient
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #44404#47548#52404
      TitleFont.Style = []
      SkinData.SkinSection = 'EDIT'
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 496
    Top = 152
  end
end
