unit APPPCR_N;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, StdCtrls, sButton, DBCtrls, sDBMemo,
  sLabel, sDBEdit, sGroupBox, ExtCtrls, sSplitter, Grids, DBGrids,
  acDBGrid, sEdit, sComboBox, Mask, sMaskEdit, ComCtrls, sPageControl,
  sPanel, sSkinProvider, Buttons, sSpeedButton, sMemo;

type
  TAPPPCR_N_frm = class(TChildForm_frm)
    sPanel2: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sMaskEdit6: TsMaskEdit;
    sComboBox5: TsComboBox;
    sMaskEdit7: TsMaskEdit;
    sEdit3: TsEdit;
    sDBGrid1: TsDBGrid;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    sTabSheet4: TsTabSheet;
    sTabSheet5: TsTabSheet;
    sPanel1: TsPanel;
    sPanel3: TsPanel;
    Edt_User: TsEdit;
    EdtMk_RegDate: TsMaskEdit;
    Com_DocumentFunc: TsComboBox;
    sComboBox2: TsComboBox;
    sPanel11: TsPanel;
    sSplitter1: TsSplitter;
    sPanel12: TsPanel;
    sGroupBox1: TsGroupBox;
    Edt_ApplicationCode: TsEdit;
    Edt_ApplicationName1: TsEdit;
    Edt_ApplicationName2: TsEdit;
    Edt_ApplicationAddr1: TsEdit;
    Edt_ApplicationAddr2: TsEdit;
    Edt_ApplicationAddr3: TsEdit;
    Edt_ApplicationAXNAME1: TsEdit;
    Edt_ApplicationSAUPNO: TsEdit;
    Edt_ApplicationElectronicSign: TsEdit;
    sGroupBox2: TsGroupBox;
    sLabel1: TsLabel;
    Edt_DocumentsSection: TsEdit;
    sEdit17: TsEdit;
    Edt_BuyConfirmDocuments: TsEdit;
    Edt_SupplyCode: TsEdit;
    Edt_SupplyName: TsEdit;
    Edt_SupplyCEO: TsEdit;
    Edt_SupplyEID: TsEdit;
    sPanel8: TsPanel;
    Edt_SupplyEID2: TsEdit;
    Edt_SupplyAddr1: TsEdit;
    Edt_SupplyAddr2: TsEdit;
    Edt_SupplySaupNo: TsEdit;
    sGroupBox3: TsGroupBox;
    sLabel3: TsLabel;
    sEdit28: TsEdit;
    sEdit29: TsEdit;
    sEdit31: TsEdit;
    sEdit32: TsEdit;
    sEdit27: TsEdit;
    sGroupBox4: TsGroupBox;
    sEdit33: TsEdit;
    sEdit34: TsEdit;
    sEdit35: TsEdit;
    sEdit37: TsEdit;
    sEdit39: TsEdit;
    sSplitter3: TsSplitter;
    sPanel13: TsPanel;
    sPanel14: TsPanel;
    sSplitter4: TsSplitter;
    Pan_Detail: TsPanel;
    Btn_DetailAdd: TsSpeedButton;
    LIne1: TsSpeedButton;
    Line4: TsSpeedButton;
    Btn_DetailDel: TsSpeedButton;
    Line3: TsSpeedButton;
    Btn_DetailMod: TsSpeedButton;
    LIne2: TsSpeedButton;
    sDBGrid2: TsDBGrid;
    sGroupBox5: TsGroupBox;
    sDBEdit33: TsDBEdit;
    sDBEdit36: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBMemo3: TsDBMemo;
    sDBEdit34: TsDBEdit;
    sDBEdit35: TsDBEdit;
    sDBEdit37: TsDBEdit;
    sDBEdit38: TsDBEdit;
    sDBEdit39: TsDBEdit;
    sDBEdit40: TsDBEdit;
    sDBEdit41: TsDBEdit;
    sDBEdit42: TsDBEdit;
    sDBEdit43: TsDBEdit;
    sDBEdit44: TsDBEdit;
    sDBEdit45: TsDBEdit;
    sDBEdit46: TsDBEdit;
    sDBEdit47: TsDBEdit;
    sDBEdit1: TsDBEdit;
    sSplitter5: TsSplitter;
    sPanel15: TsPanel;
    sPanel16: TsPanel;
    sPanel9: TsPanel;
    Btn_DocAdd: TsSpeedButton;
    sSpeedButton10: TsSpeedButton;
    sSpeedButton11: TsSpeedButton;
    Btn_DocDel: TsSpeedButton;
    sSpeedButton13: TsSpeedButton;
    Btn_DocMod: TsSpeedButton;
    sSpeedButton15: TsSpeedButton;
    sDBGrid3: TsDBGrid;
    sGroupBox6: TsGroupBox;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    sDBEdit48: TsDBEdit;
    sDBEdit49: TsDBEdit;
    sDBMemo5: TsDBMemo;
    sDBMemo6: TsDBMemo;
    sDBEdit50: TsDBEdit;
    sDBEdit51: TsDBEdit;
    sDBEdit52: TsDBEdit;
    sDBEdit58: TsDBEdit;
    sDBEdit59: TsDBEdit;
    sDBEdit64: TsDBEdit;
    sDBEdit65: TsDBEdit;
    sDBEdit66: TsDBEdit;
    Pan_DocNO: TsPanel;
    sDBEdit63: TsDBEdit;
    sPanel17: TsPanel;
    sSplitter6: TsSplitter;
    sPanel18: TsPanel;
    sPanel10: TsPanel;
    Btn_TaxAdd: TsSpeedButton;
    sSpeedButton17: TsSpeedButton;
    sSpeedButton18: TsSpeedButton;
    Btn_TaxDel: TsSpeedButton;
    sSpeedButton20: TsSpeedButton;
    Btn_TaxMod: TsSpeedButton;
    sSpeedButton22: TsSpeedButton;
    sDBGrid4: TsDBGrid;
    sGroupBox7: TsGroupBox;
    sDBEdit53: TsDBEdit;
    sDBMemo7: TsDBMemo;
    sDBEdit55: TsDBEdit;
    sDBEdit60: TsDBEdit;
    sDBEdit68: TsDBEdit;
    sDBEdit54: TsDBEdit;
    sDBEdit62: TsDBEdit;
    sDBEdit70: TsDBEdit;
    sDBEdit56: TsDBEdit;
    sDBEdit57: TsDBEdit;
    sDBEdit61: TsDBEdit;
    sDBEdit67: TsDBEdit;
    sDBEdit23: TsDBEdit;
    sDBEdit72: TsDBEdit;
    sGroupBox8: TsGroupBox;
    sEdit41: TsEdit;
    sEdit36: TsEdit;
    sEdit40: TsEdit;
    sEdit42: TsEdit;
    sEdit43: TsEdit;
    Edt_ApplicationAPPDATE: TsMaskEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  APPPCR_N_frm: TAPPPCR_N_frm;

implementation

uses MSSQL;

{$R *.dfm}


end.
