inherited dlg_CustomerReg_frm: Tdlg_CustomerReg_frm
  Left = 987
  Top = 120
  BorderWidth = 4
  Caption = #44144#47000#52376#46321#47197
  ClientHeight = 621
  ClientWidth = 399
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poMainFormCenter
  Visible = False
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel2: TsPanel [0]
    Left = 0
    Top = 0
    Width = 399
    Height = 621
    SkinData.SkinSection = 'PANEL'
    Align = alRight
    
    TabOrder = 0
    object sSpeedButton1: TsSpeedButton
      Left = 154
      Top = 264
      Width = 23
      Height = 21
      OnClick = sSpeedButton1Click
      SkinData.SkinSection = 'SPEEDBUTTON'
      Images = DMICON.System16
      ImageIndex = 0
    end
    object sSpeedButton2: TsSpeedButton
      Left = 154
      Top = 288
      Width = 23
      Height = 21
      OnClick = sSpeedButton2Click
      SkinData.SkinSection = 'SPEEDBUTTON'
      Images = DMICON.System16
      ImageIndex = 0
    end
    object sBevel1: TsBevel
      Left = 8
      Top = 552
      Width = 385
      Height = 10
      Shape = bsBottomLine
    end
    object edt_Sangho: TsEdit
      Left = 120
      Top = 40
      Width = 257
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49345#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_CODE: TsEdit
      Left = 120
      Top = 16
      Width = 121
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 5
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #53076#46300
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_TradeNo: TsEdit
      Left = 120
      Top = 64
      Width = 97
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 12
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47924#50669#50629#54728#44032#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_MstID: TsEdit
      Left = 120
      Top = 112
      Width = 257
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 35
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49688#48156#49888#51064#49885#48324#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_DtID: TsEdit
      Left = 120
      Top = 136
      Width = 257
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 35
      ParentFont = False
      TabOrder = 5
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49345#49464#49688#48156#49888#51064#49885#48324#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_Zipcode: TsEdit
      Left = 120
      Top = 168
      Width = 97
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 6
      ParentFont = False
      TabOrder = 6
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50864#54200#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_Addr1: TsEdit
      Left = 120
      Top = 192
      Width = 257
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 36
      ParentFont = False
      TabOrder = 7
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #51452#49548
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_Addr2: TsEdit
      Left = 120
      Top = 216
      Width = 257
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 36
      ParentFont = False
      TabOrder = 8
      SkinData.SkinSection = 'EDIT'
    end
    object edt_Addr3: TsEdit
      Left = 120
      Top = 240
      Width = 257
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 36
      ParentFont = False
      TabOrder = 9
      SkinData.SkinSection = 'EDIT'
    end
    object edt_City: TsEdit
      Left = 120
      Top = 264
      Width = 33
      Height = 21
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46020#49884
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_CityContents: TsEdit
      Left = 178
      Top = 264
      Width = 199
      Height = 21
      TabStop = False
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 11
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object edt_Nation: TsEdit
      Left = 120
      Top = 288
      Width = 33
      Height = 21
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      OnExit = edt_NationExit
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44397#44032#53076#46300
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_NationContents: TsEdit
      Left = 178
      Top = 288
      Width = 199
      Height = 21
      TabStop = False
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 13
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object edt_Name: TsEdit
      Left = 120
      Top = 320
      Width = 257
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45824#54364#51088#47749
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_Tel: TsEdit
      Left = 120
      Top = 344
      Width = 145
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45824#54364#51204#54868#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_FAX: TsEdit
      Left = 120
      Top = 368
      Width = 145
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 16
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45824#54364#54057#49828
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_DamName: TsEdit
      Left = 120
      Top = 392
      Width = 257
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 17
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45812#45817#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_DAMPart: TsEdit
      Left = 120
      Top = 416
      Width = 145
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 18
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45812#45817#48512#49436
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_DAMTel: TsEdit
      Left = 120
      Top = 440
      Width = 145
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 19
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45812#45817#51088#51204#54868#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_DAMFAX: TsEdit
      Left = 120
      Top = 464
      Width = 145
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 20
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45812#45817#51088#54057#49828
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_Jenja: TsEdit
      Left = 120
      Top = 496
      Width = 73
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 21
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #51204#51088#49436#47749
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_Email1: TsEdit
      Left = 120
      Top = 528
      Width = 113
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 35
      ParentFont = False
      TabOrder = 22
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #51060#47700#51068
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object edt_Email2: TsEdit
      Left = 248
      Top = 528
      Width = 129
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 35
      ParentFont = False
      TabOrder = 23
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = '@'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -13
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sButton1: TsButton
      Left = 107
      Top = 573
      Width = 104
      Height = 33
      Caption = #51200#51109
      TabOrder = 24
      TabStop = False
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sButton2: TsButton
      Left = 216
      Top = 573
      Width = 75
      Height = 33
      Caption = #52712#49548
      TabOrder = 25
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_SaupNo: TsEdit
      Left = 120
      Top = 88
      Width = 97
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 3
      OnEnter = edt_SaupNoEnter
      OnExit = edt_SaupNoExit
      OnKeyPress = edt_SaupNoKeyPress
      OnKeyUp = edt_SaupNoKeyUp
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sCheckBox1: TsCheckBox
      Left = 248
      Top = 16
      Width = 118
      Height = 16
      Caption = #53076#46300' 00000 '#49324#50857
      TabOrder = 26
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
    end
  end
  object qryInsMSSQL: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'TRAD_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end
      item
        Name = 'SAUP_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ENAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'REP_NAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'REP_TEL'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 18
        Value = Null
      end
      item
        Name = 'REP_FAX'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 18
        Value = Null
      end
      item
        Name = 'DDAN_DEPT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'DDAN_NAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'DDAN_TEL'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 18
        Value = Null
      end
      item
        Name = 'DDAN_FAX'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 18
        Value = Null
      end
      item
        Name = 'ZIPCD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CITY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'NAT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ESU1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ESU2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ESU3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Jenja'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ADDR4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR5'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EMAIL_ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EMAIL_DOMAIN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [CUSTOM]'
      '([CODE]'
      ',[TRAD_NO]'
      ',[SAUP_NO]'
      ',[ENAME]'
      ',[REP_NAME]'
      ',[REP_TEL]'
      ',[REP_FAX]'
      ',[DDAN_DEPT]'
      ',[DDAN_NAME]'
      ',[DDAN_TEL]'
      ',[DDAN_FAX]'
      ',[ZIPCD]'
      ',[ADDR1]'
      ',[ADDR2]'
      ',[ADDR3]'
      ',[CITY]'
      ',[NAT]'
      ',[ESU1]'
      ',[ESU2]'
      ',[ESU3]'
      ',[Jenja]'
      ',[ADDR4]'
      ',[ADDR5]'
      ',[EMAIL_ID]'
      ',[EMAIL_DOMAIN])'
      'VALUES'
      '( :CODE'
      ', :TRAD_NO'
      ', :SAUP_NO'
      ', :ENAME'
      ', :REP_NAME'
      ', :REP_TEL'
      ', :REP_FAX'
      ', :DDAN_DEPT'
      ', :DDAN_NAME'
      ', :DDAN_TEL'
      ', :DDAN_FAX'
      ', :ZIPCD'
      ', :ADDR1'
      ', :ADDR2'
      ', :ADDR3'
      ', :CITY'
      ', :NAT'
      ', :ESU1'
      ', :ESU2'
      ', :ESU3'
      ', :Jenja'
      ', :ADDR4'
      ', :ADDR5'
      ', :EMAIL_ID'
      ', :EMAIL_DOMAIN)')
    Left = 296
    Top = 160
  end
  object qryModMSSQL: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'TRAD_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end
      item
        Name = 'SAUP_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ENAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'REP_NAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'REP_TEL'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 18
        Value = Null
      end
      item
        Name = 'REP_FAX'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 18
        Value = Null
      end
      item
        Name = 'DDAN_DEPT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'DDAN_NAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'DDAN_TEL'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 18
        Value = Null
      end
      item
        Name = 'DDAN_FAX'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 18
        Value = Null
      end
      item
        Name = 'ZIPCD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CITY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'NAT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ESU1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ESU2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ESU3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Jenja'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ADDR4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR5'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EMAIL_ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EMAIL_DOMAIN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [CUSTOM]'
      'SET '
      '[TRAD_NO]'#9#9'=:TRAD_NO'
      ',[SAUP_NO]'#9#9'=:SAUP_NO'
      ',[ENAME]'#9#9'=:ENAME'
      ',[REP_NAME]'#9#9'=:REP_NAME'
      ',[REP_TEL]'#9#9'=:REP_TEL'
      ',[REP_FAX]'#9#9'=:REP_FAX'
      ',[DDAN_DEPT]'#9'=:DDAN_DEPT'
      ',[DDAN_NAME]'#9'=:DDAN_NAME'
      ',[DDAN_TEL]'#9#9'=:DDAN_TEL'
      ',[DDAN_FAX]'#9#9'=:DDAN_FAX'
      ',[ZIPCD]'#9#9'=:ZIPCD'
      ',[ADDR1]'#9#9'=:ADDR1'
      ',[ADDR2]'#9#9'=:ADDR2'
      ',[ADDR3]'#9#9'=:ADDR3'
      ',[CITY]'#9#9#9'=:CITY'
      ',[NAT]'#9#9#9'=:NAT'
      ',[ESU1]'#9#9#9'=:ESU1'
      ',[ESU2]'#9#9#9'=:ESU2'
      ',[ESU3]'#9#9#9'=:ESU3'
      ',[Jenja]'#9#9'=:Jenja'
      ',[ADDR4]'#9#9'=:ADDR4'
      ',[ADDR5]'#9#9'=:ADDR5'
      ',[EMAIL_ID]'#9#9'=:EMAIL_ID'
      ',[EMAIL_DOMAIN]'#9'=:EMAIL_DOMAIN'
      'WHERE [CODE] = :CODE')
    Left = 328
    Top = 160
  end
  object qryDelMSSQL: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM [CUSTOM]'
      'WHERE [CODE] = :CODE')
    Left = 360
    Top = 160
  end
  object qryCheck: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 264
    Top = 160
  end
end
