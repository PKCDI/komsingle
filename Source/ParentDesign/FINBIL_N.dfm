inherited FINBIL_frm: TFINBIL_frm
  Left = 590
  Caption = #44228#49328#49436
  ClientHeight = 631
  ClientWidth = 796
  OldCreateOrder = True
  DesignSize = (
    796
    631)
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 796
    Height = 57
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sSplitter2: TsSplitter
      Left = 1
      Top = 1
      Width = 176
      Height = 55
      Cursor = crHSplit
      AutoSnap = False
      Enabled = False
      SkinData.SkinSection = 'SPLITTER'
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 29
      Width = 36
      Height = 15
      Caption = #44228#49328#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton3: TsSpeedButton
      Left = 177
      Top = 1
      Width = 2
      Height = 55
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSpeedButton5: TsSpeedButton
      Left = 729
      Top = 1
      Width = 11
      Height = 55
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Close: TsSpeedButton
      Left = 740
      Top = 1
      Width = 55
      Height = 55
      Cursor = crHandPoint
      Caption = #45803#44592
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 20
      Images = DMICON.System24
      Reflected = True
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 7
      Width = 59
      Height = 25
      Caption = 'FINBIL'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object Btn_New: TsSpeedButton
      Left = 179
      Top = 1
      Width = 54
      Height = 55
      Cursor = crHandPoint
      Caption = #49888#44508
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Visible = False
      Align = alLeft
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 5
      Images = DMICON.System24
      Reflected = True
    end
    object Btn_Modify: TsSpeedButton
      Tag = 1
      Left = 233
      Top = 1
      Width = 53
      Height = 55
      Cursor = crHandPoint
      Caption = #49688#51221
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Visible = False
      Align = alLeft
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 8
      Images = DMICON.System24
      Reflected = True
    end
    object Btn_Del: TsSpeedButton
      Tag = 2
      Left = 286
      Top = 1
      Width = 53
      Height = 55
      Cursor = crHandPoint
      Caption = #49325#51228
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Align = alLeft
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 19
      Images = DMICON.System24
      Reflected = True
    end
    object sSpeedButton1: TsSpeedButton
      Left = 500
      Top = 1
      Width = 2
      Height = 55
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSpeedButton2: TsSpeedButton
      Left = 339
      Top = 1
      Width = 2
      Height = 55
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Print: TsSpeedButton
      Left = 447
      Top = 1
      Width = 53
      Height = 55
      Cursor = crHandPoint
      Caption = #52636#47141
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Align = alLeft
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 21
      Images = DMICON.System24
      Reflected = True
    end
    object Btn_Ready: TsSpeedButton
      Left = 394
      Top = 1
      Width = 53
      Height = 55
      Cursor = crHandPoint
      Caption = #51456#48708
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Visible = False
      Align = alLeft
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 22
      Images = DMICON.System24
      Reflected = True
    end
    object Btn_Save: TsSpeedButton
      Tag = 1
      Left = 502
      Top = 1
      Width = 53
      Height = 55
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Visible = False
      Align = alLeft
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 7
      Images = DMICON.System24
      Reflected = True
    end
    object Btn_Cancel: TsSpeedButton
      Tag = 2
      Left = 557
      Top = 1
      Width = 53
      Height = 55
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Visible = False
      Align = alLeft
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 18
      Images = DMICON.System24
      Reflected = True
    end
    object Btn_Temp: TsSpeedButton
      Left = 341
      Top = 1
      Width = 53
      Height = 55
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Visible = False
      Align = alLeft
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 12
      Images = DMICON.System24
      Reflected = True
    end
    object sSpeedButton26: TsSpeedButton
      Left = 610
      Top = 1
      Width = 2
      Height = 55
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Visible = False
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSpeedButton27: TsSpeedButton
      Left = 555
      Top = 1
      Width = 2
      Height = 55
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Visible = False
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
  end
  object sPageControl1: TsPageControl [1]
    Left = 0
    Top = 57
    Width = 796
    Height = 574
    ActivePage = sTabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 25
    TabIndex = 0
    TabOrder = 1
    TabStop = False
    TabWidth = 95
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      ImageIndex = 15
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object sPanel2: TsPanel
        Left = 0
        Top = 0
        Width = 788
        Height = 32
        Align = alTop
        TabOrder = 0
        SkinData.CustomColor = True
        SkinData.SkinSection = 'PAGECONTROL'
        object sMaskEdit1: TsMaskEdit
          Tag = 1
          Left = 64
          Top = 6
          Width = 73
          Height = 20
          Color = clWhite
          EditMask = '9999-99-99;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          Text = '20130101'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 4276545
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PAGECONTROL'
        end
        object sMaskEdit2: TsMaskEdit
          Tag = 1
          Left = 168
          Top = 6
          Width = 73
          Height = 20
          Color = clWhite
          EditMask = '9999-99-99;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          MaxLength = 10
          ParentFont = False
          TabOrder = 1
          Text = '20130101'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.Caption = #48512#53552
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 4276545
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PAGECONTROL'
        end
        object sEdit1: TsEdit
          Left = 306
          Top = 6
          Width = 159
          Height = 20
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          ParentFont = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sButton2: TsButton
          Left = 466
          Top = 6
          Width = 75
          Height = 20
          Caption = #51312#54924
          TabOrder = 3
          SkinData.SkinSection = 'BUTTON'
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 32
        Width = 788
        Height = 507
        Align = alClient
        Color = clWhite
        Ctl3D = False
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ImeName = 'Microsoft IME 2010'
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            Title.Caption = #44288#47532#48264#54840
            Width = 200
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #48156#49888#51068
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BNKNAME1'
            Title.Caption = #48156#44553#51008#54665
            Width = 227
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TRN_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44144#47000#51068#51088
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'BUS'
            Title.Alignment = taCenter
            Title.Caption = #44228#49328#49436#50857#46020
            Width = 64
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FINAMT2'
            Title.Alignment = taCenter
            Title.Caption = #44144#47000#44552#50529
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'FINAMT2C'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 30
            Visible = True
          end>
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #49345#49464#45236#50669
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object sSplitter1: TsSplitter
        Left = 0
        Top = 33
        Width = 788
        Height = 4
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 0
        Width = 788
        Height = 33
        Align = alTop
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object sDBEdit1: TsDBEdit
          Left = 66
          Top = 5
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'Maint_No'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit23: TsDBEdit
          Left = 344
          Top = 5
          Width = 65
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'User_Id'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49324#50857#51088
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sEdit2: TsEdit
          Left = 736
          Top = 5
          Width = 41
          Height = 23
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          Text = '9'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.Caption = #47928#49436#44592#45733
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
      end
      object sPanel4: TsPanel
        Left = 0
        Top = 37
        Width = 788
        Height = 502
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        object sDBEdit33: TsDBEdit
          Left = 74
          Top = 21
          Width = 55
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'BANK'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #48156#44553#51008#54665
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit34: TsDBEdit
          Left = 74
          Top = 45
          Width = 383
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'BNKNAME1'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Caption = 'sDBEdit34'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit35: TsDBEdit
          Left = 74
          Top = 69
          Width = 383
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'BNKNAME2'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Caption = 'sDBEdit35'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit36: TsDBEdit
          Left = 74
          Top = 101
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'BUS'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#49328#49436#50857#46020
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit37: TsDBEdit
          Left = 114
          Top = 101
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'BUS_NAME'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit38: TsDBEdit
          Left = 74
          Top = 125
          Width = 383
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'BUS_DESC'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49464#48512#44144#47000#47749
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit39: TsDBEdit
          Left = 538
          Top = 21
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CUSTNAME1'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #44144#47000#44256#44061
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit2: TsDBEdit
          Left = 538
          Top = 45
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CUSTNAME2'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit3: TsDBEdit
          Left = 538
          Top = 69
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CUSTNAME3'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit4: TsDBEdit
          Left = 278
          Top = 189
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'DOC_NO1'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49888#50857#51109'('#44228#50557#49436')'#48264#54840
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit5: TsDBEdit
          Left = 278
          Top = 213
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'DOC_NO2'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49888#52397#48264#54840
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit6: TsDBEdit
          Left = 278
          Top = 237
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'DOC_NO3'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #52280#51312#48264#54840
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit7: TsDBEdit
          Left = 278
          Top = 261
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'DOC_NO4'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #44592#53440#48264#54840
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit8: TsDBEdit
          Left = 278
          Top = 293
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'TERM'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #44208#51228#48169#48277
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit9: TsDBEdit
          Left = 278
          Top = 317
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'PAYMENT'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #50612#51020#51312#44148
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit10: TsDBEdit
          Left = 278
          Top = 341
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'SPECIAL1'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #54616#51088#50668#48512
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit11: TsDBEdit
          Left = 278
          Top = 365
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'SPECIAL2'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #51116#47588#51077#50668#48512
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit12: TsDBEdit
          Left = 278
          Top = 397
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CUSTSIGN1'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit13: TsDBEdit
          Left = 278
          Top = 421
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CUSTSIGN2'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit14: TsDBEdit
          Left = 278
          Top = 445
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CUSTSIGN3'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit15: TsDBEdit
          Left = 318
          Top = 293
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'TERM_NAME'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit16: TsDBEdit
          Left = 318
          Top = 317
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYMENT_NAME'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit17: TsDBEdit
          Left = 318
          Top = 341
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'SPECIAL1_NAME'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sPanel5: TsPanel
          Left = 9
          Top = 169
          Width = 769
          Height = 2
          TabOrder = 23
          SkinData.SkinSection = 'PANEL'
        end
        object sDBEdit66: TsDBEdit
          Left = 318
          Top = 365
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'SPECIAL2_NAME'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
      end
    end
    object sTabSheet2: TsTabSheet
      BorderWidth = 4
      Caption = #44552#50529
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object sPanel6: TsPanel
        Left = 0
        Top = 0
        Width = 780
        Height = 531
        Align = alClient
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object sDBEdit18: TsDBEdit
          Left = 96
          Top = 9
          Width = 77
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'TRN_DATE'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #44144#47000#51068#51088
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit19: TsDBEdit
          Left = 242
          Top = 9
          Width = 79
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'ADV_DATE'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #53685#51648#51068#51088
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sPanel7: TsPanel
          Left = 6
          Top = 41
          Width = 769
          Height = 2
          TabOrder = 2
          SkinData.SkinSection = 'PANEL'
        end
        object sDBMemo1: TsDBMemo
          Left = 96
          Top = 56
          Width = 673
          Height = 89
          Color = clWhite
          DataField = 'REMARK1'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 3
          BoundLabel.Active = True
          BoundLabel.Caption = #44592#53440#51221#48372
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeftTop
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel8: TsPanel
          Left = 6
          Top = 472
          Width = 769
          Height = 2
          TabOrder = 4
          SkinData.SkinSection = 'PANEL'
        end
        object sDBEdit20: TsDBEdit
          Left = 96
          Top = 165
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'PAYORD1C'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #50808#54868#52509#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit21: TsDBEdit
          Left = 136
          Top = 165
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD1'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit22: TsDBEdit
          Left = 96
          Top = 189
          Width = 97
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'RATE1'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #51201#50857#54872#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit24: TsDBEdit
          Left = 96
          Top = 213
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD11'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #50896#54868#54872#49328#44552#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit25: TsDBEdit
          Left = 96
          Top = 245
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'PAYORD2C'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #50808#54868#52509#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit26: TsDBEdit
          Left = 136
          Top = 245
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD2'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit27: TsDBEdit
          Left = 96
          Top = 269
          Width = 97
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'RATE2'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #51201#50857#54872#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit28: TsDBEdit
          Left = 96
          Top = 293
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD21'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #50896#54868#54872#49328#44552#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit29: TsDBEdit
          Left = 464
          Top = 165
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'PAYORD3C'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #54788#47932#54872
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit30: TsDBEdit
          Left = 504
          Top = 165
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD3'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit31: TsDBEdit
          Left = 464
          Top = 189
          Width = 97
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'RATE3'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #51201#50857#54872#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit32: TsDBEdit
          Left = 464
          Top = 213
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD31'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #50896#54868#54872#49328#44552#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit40: TsDBEdit
          Left = 464
          Top = 245
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'PAYORD4C'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49440#47932#54872
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit41: TsDBEdit
          Left = 504
          Top = 245
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD4'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit42: TsDBEdit
          Left = 464
          Top = 269
          Width = 97
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'RATE4'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #51201#50857#54872#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit43: TsDBEdit
          Left = 464
          Top = 293
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD41'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #50896#54868#54872#49328#44552#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit44: TsDBEdit
          Left = 464
          Top = 325
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'PAYORD5C'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#51077#48372#51613#44552
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit45: TsDBEdit
          Left = 504
          Top = 325
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD5'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit46: TsDBEdit
          Left = 464
          Top = 349
          Width = 97
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'RATE5'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #51201#50857#54872#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit47: TsDBEdit
          Left = 464
          Top = 373
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'PAYORD51'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #50896#54868#54872#49328#44552#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit48: TsDBEdit
          Left = 464
          Top = 405
          Width = 97
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'FOBRATE'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = 'FOB'#54872#49328#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit49: TsDBEdit
          Left = 464
          Top = 429
          Width = 97
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'RATEIN'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #48372#51613#44552#51201#47549#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit50: TsDBEdit
          Left = 119
          Top = 493
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'FINAMT2C'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#49688#47308'('#51060#51088')'#54633#44228
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit51: TsDBEdit
          Left = 159
          Top = 493
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'FINAMT2'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit52: TsDBEdit
          Left = 495
          Top = 493
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'FINAMT1C'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #52572#51333#51648#44553'('#49688#51077')'#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit53: TsDBEdit
          Left = 535
          Top = 493
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'FINAMT1'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #49688#49688#47308
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object sDBGrid2: TsDBGrid
        Left = 0
        Top = 0
        Width = 788
        Height = 288
        Align = alClient
        Color = clWhite
        Ctl3D = False
        DataSource = dsDetail
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SEQ'
            Title.Alignment = taCenter
            Title.Caption = #49692#48264
            Width = 30
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHCODE'
            Title.Alignment = taCenter
            Title.Caption = #50976#54805
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BILLNO'
            Title.Caption = #44228#49328#49436#48264#54840
            Width = 156
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHRATE'
            Title.Alignment = taCenter
            Title.Caption = #51201#50857#50836#50984
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT1'
            Title.Caption = #45824#49345#44552#50529
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT1C'
            Title.Caption = #45800#50948
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT2'
            Title.Caption = #49328#52636#44552#50529
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT2C'
            Title.Caption = #45800#50948
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KRWRATE'
            Title.Caption = #54872#50984
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHDAY'
            Title.Caption = #51201#50857#51068#49688
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHDATE1'
            Title.Caption = #49884#51089#44592#44036
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHDATE2'
            Title.Caption = #51333#47308#44592#44036
            Width = 80
            Visible = True
          end>
      end
      object sGroupBox5: TsGroupBox
        Left = 0
        Top = 288
        Width = 788
        Height = 251
        Align = alBottom
        Color = clGray
        ParentColor = False
        TabOrder = 1
        SkinData.SkinSection = 'GROUPBOX'
        object sLabel1: TsLabel
          Left = 486
          Top = 208
          Width = 24
          Height = 15
          Caption = #44620#51648
        end
        object sDBEdit54: TsDBEdit
          Left = 278
          Top = 29
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'CHCODE'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#49688#47308#50976#54805
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit55: TsDBEdit
          Left = 318
          Top = 29
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CHCODE_NAME'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit56: TsDBEdit
          Left = 278
          Top = 53
          Width = 231
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'BILLNO'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#49688#47308#44228#49328#49436#48264#54840
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit57: TsDBEdit
          Left = 278
          Top = 77
          Width = 65
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CHRATE'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #51201#50857#50836#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit58: TsDBEdit
          Left = 278
          Top = 101
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'AMT1C'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #45824#49345#44552#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit59: TsDBEdit
          Left = 318
          Top = 101
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'AMT1'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit60: TsDBEdit
          Left = 278
          Top = 125
          Width = 39
          Height = 23
          TabStop = False
          Color = clBtnFace
          DataField = 'AMT2C'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #49328#52636#44552#50529
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = False
        end
        object sDBEdit61: TsDBEdit
          Left = 318
          Top = 125
          Width = 191
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'AMT2'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit62: TsDBEdit
          Left = 278
          Top = 157
          Width = 65
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'KRWRATE'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #54872#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit63: TsDBEdit
          Left = 278
          Top = 181
          Width = 62
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CHDAY'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #51201#50857#51068#49688
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit64: TsDBEdit
          Left = 278
          Top = 205
          Width = 83
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CHDATE1'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #51201#50857#44592#44036
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sDBEdit65: TsDBEdit
          Left = 398
          Top = 205
          Width = 83
          Height = 23
          TabStop = False
          Color = clWhite
          DataField = 'CHDATE2'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #48512#53552
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
      end
    end
  end
  object sPanel9: TsPanel [2]
    Left = 637
    Top = 59
    Width = 130
    Height = 24
    Anchors = [akRight]
    TabOrder = 2
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
  end
  object sPanel10: TsPanel [3]
    Left = 611
    Top = 59
    Width = 25
    Height = 24
    Anchors = [akRight]
    TabOrder = 3
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    object sSpeedButton4: TsSpeedButton
      Left = 1
      Top = 1
      Width = 23
      Height = 22
      Caption = #9664
      OnClick = sSpeedButton4Click
      Align = alClient
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
  end
  object sPanel11: TsPanel [4]
    Left = 768
    Top = 59
    Width = 25
    Height = 24
    Anchors = [akRight]
    TabOrder = 4
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    object sSpeedButton8: TsSpeedButton
      Left = 1
      Top = 1
      Width = 23
      Height = 22
      Caption = #9654
      OnClick = sSpeedButton8Click
      Align = alClient
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 664
    Top = 16
  end
  object qryList: TADOQuery
    Active = True
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[USER_ID]'
      '      ,[DATEE]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[BUS]'
      #9'  ,ISNULL(FIN4025.NAME,'#39#39') as BUS_NAME'
      '      ,[BUS_DESC]'
      '      ,[DOC_NO1]'
      '      ,[DOC_NO2]'
      '      ,[DOC_NO3]'
      '      ,[DOC_NO4]'
      '      ,[TRN_DATE]'
      '      ,[ADV_DATE]'
      '      ,[TERM]'
      #9'  ,ISNULL(FIN4277.NAME,'#39#39') as TERM_NAME'
      '      ,[PAYMENT]'
      #9'  ,ISNULL(FIN2475.NAME,'#39#39') as PAYMENT_NAME'
      '      ,[SPECIAL1]'
      #9'  ,ISNULL(FIN4183.NAME,'#39#39') as SPECIAL1_NAME'
      '      ,[SPECIAL2]'
      #9'  ,ISNULL(FIN4183_2.NAME,'#39#39') as SPECIAL2_NAME'
      '      ,[REMARK]'
      '      ,[REMARK1]'
      '      ,[BANK]'
      '      ,[BNKNAME1]'
      '      ,[BNKNAME2]'
      '      ,[CUSTNAME1]'
      '      ,[CUSTNAME2]'
      '      ,[CUSTNAME3]'
      '      ,[CUSTSIGN1]'
      '      ,[CUSTSIGN2]'
      '      ,[CUSTSIGN3]'
      '      ,[PAYORD1]'
      '      ,[PAYORD11]'
      '      ,[PAYORD1C]'
      '      ,[RATE1]'
      '      ,[PAYORD2]'
      '      ,[PAYORD21]'
      '      ,[PAYORD2C]'
      '      ,[RATE2]'
      '      ,[PAYORD3]'
      '      ,[PAYORD31]'
      '      ,[PAYORD3C]'
      '      ,[RATE3]'
      '      ,[PAYORD4]'
      '      ,[PAYORD41]'
      '      ,[PAYORD4C]'
      '      ,[RATE4]'
      '      ,[PAYORD5]'
      '      ,[PAYORD51]'
      '      ,[PAYORD5C]'
      '      ,[RATE5]'
      '      ,[FOBRATE]'
      '      ,[RATEIN]'
      '      ,[FINAMT1]'
      '      ,[FINAMT1C]'
      '      ,[FINAMT2]'
      '      ,[FINAMT2C]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[PRNO]'
      
        '  FROM [FINBIL_H] LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHE' +
        'RE Prefix = '#39'4025FINBIL'#39') FIN4025 ON FINBIL_H.BUS = FIN4025.CODE'
      
        #9#9#9#9'  LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ' +
        #39#45236#44397#49888'4277'#39') FIN4277 ON FINBIL_H.TERM = FIN4277.CODE'
      
        #9#9#9#9'  LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ' +
        #39'2475FINBIL'#39') FIN2475 ON FINBIL_H.PAYMENT = FIN2475.CODE'
      
        #9#9#9#9'  LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ' +
        #39'4183FINBIL'#39') FIN4183 ON FINBIL_H.SPECIAL1 = FIN4183.CODE'
      
        #9#9#9#9'  LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ' +
        #39'4183FINBIL'#39') FIN4183_2 ON FINBIL_H.SPECIAL2 = FIN4183_2.CODE')
    Left = 160
    Top = 128
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUS: TStringField
      FieldName = 'BUS'
      Size = 3
    end
    object qryListBUS_DESC: TStringField
      FieldName = 'BUS_DESC'
      Size = 70
    end
    object qryListDOC_NO1: TStringField
      FieldName = 'DOC_NO1'
      Size = 35
    end
    object qryListDOC_NO2: TStringField
      FieldName = 'DOC_NO2'
      Size = 35
    end
    object qryListDOC_NO3: TStringField
      FieldName = 'DOC_NO3'
      Size = 35
    end
    object qryListDOC_NO4: TStringField
      FieldName = 'DOC_NO4'
      Size = 35
    end
    object qryListTRN_DATE: TStringField
      FieldName = 'TRN_DATE'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryListTERM: TStringField
      FieldName = 'TERM'
      Size = 3
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListSPECIAL1: TStringField
      FieldName = 'SPECIAL1'
      Size = 3
    end
    object qryListSPECIAL2: TStringField
      FieldName = 'SPECIAL2'
      Size = 3
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListBANK: TStringField
      FieldName = 'BANK'
      Size = 11
    end
    object qryListBNKNAME1: TStringField
      FieldName = 'BNKNAME1'
      Size = 70
    end
    object qryListBNKNAME2: TStringField
      FieldName = 'BNKNAME2'
      Size = 70
    end
    object qryListCUSTNAME1: TStringField
      FieldName = 'CUSTNAME1'
      Size = 35
    end
    object qryListCUSTNAME2: TStringField
      FieldName = 'CUSTNAME2'
      Size = 35
    end
    object qryListCUSTNAME3: TStringField
      FieldName = 'CUSTNAME3'
      Size = 35
    end
    object qryListCUSTSIGN1: TStringField
      FieldName = 'CUSTSIGN1'
      Size = 35
    end
    object qryListCUSTSIGN2: TStringField
      FieldName = 'CUSTSIGN2'
      Size = 35
    end
    object qryListCUSTSIGN3: TStringField
      FieldName = 'CUSTSIGN3'
      Size = 35
    end
    object qryListPAYORD1: TBCDField
      FieldName = 'PAYORD1'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListPAYORD11: TBCDField
      FieldName = 'PAYORD11'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListPAYORD1C: TStringField
      FieldName = 'PAYORD1C'
      Size = 3
    end
    object qryListRATE1: TBCDField
      FieldName = 'RATE1'
      DisplayFormat = '#,0.00'
      Precision = 18
    end
    object qryListPAYORD2: TBCDField
      FieldName = 'PAYORD2'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListPAYORD21: TBCDField
      FieldName = 'PAYORD21'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListPAYORD2C: TStringField
      FieldName = 'PAYORD2C'
      Size = 3
    end
    object qryListRATE2: TBCDField
      FieldName = 'RATE2'
      DisplayFormat = '#,0.00'
      Precision = 18
    end
    object qryListPAYORD3: TBCDField
      FieldName = 'PAYORD3'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListPAYORD31: TBCDField
      FieldName = 'PAYORD31'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListPAYORD3C: TStringField
      FieldName = 'PAYORD3C'
      Size = 3
    end
    object qryListRATE3: TBCDField
      FieldName = 'RATE3'
      DisplayFormat = '#,0.00'
      Precision = 18
    end
    object qryListPAYORD4: TBCDField
      FieldName = 'PAYORD4'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListPAYORD41: TBCDField
      FieldName = 'PAYORD41'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListPAYORD4C: TStringField
      FieldName = 'PAYORD4C'
      Size = 3
    end
    object qryListRATE4: TBCDField
      FieldName = 'RATE4'
      DisplayFormat = '#,0.00'
      Precision = 18
    end
    object qryListPAYORD5: TBCDField
      FieldName = 'PAYORD5'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListPAYORD51: TBCDField
      FieldName = 'PAYORD51'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListPAYORD5C: TStringField
      FieldName = 'PAYORD5C'
      Size = 3
    end
    object qryListRATE5: TBCDField
      FieldName = 'RATE5'
      DisplayFormat = '#,0.00'
      Precision = 18
    end
    object qryListFOBRATE: TBCDField
      FieldName = 'FOBRATE'
      DisplayFormat = '#,0.0'
      Precision = 18
    end
    object qryListRATEIN: TBCDField
      FieldName = 'RATEIN'
      DisplayFormat = '#,0.0'
      Precision = 18
    end
    object qryListFINAMT1: TBCDField
      FieldName = 'FINAMT1'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListFINAMT1C: TStringField
      FieldName = 'FINAMT1C'
      Size = 3
    end
    object qryListFINAMT2: TBCDField
      FieldName = 'FINAMT2'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListFINAMT2C: TStringField
      FieldName = 'FINAMT2C'
      Size = 3
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListBUS_NAME: TStringField
      FieldName = 'BUS_NAME'
      ReadOnly = True
      Size = 100
    end
    object qryListTERM_NAME: TStringField
      FieldName = 'TERM_NAME'
      ReadOnly = True
      Size = 100
    end
    object qryListPAYMENT_NAME: TStringField
      FieldName = 'PAYMENT_NAME'
      ReadOnly = True
      Size = 100
    end
    object qryListSPECIAL1_NAME: TStringField
      FieldName = 'SPECIAL1_NAME'
      ReadOnly = True
      Size = 100
    end
    object qryListSPECIAL2_NAME: TStringField
      FieldName = 'SPECIAL2_NAME'
      ReadOnly = True
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 192
    Top = 128
  end
  object qryDetail: TADOQuery
    Active = True
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = '0071FBO050800001_001'
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, CHCODE,ISNULL(FIN5189.NAME,'#39#39') as  CHCODE_NAME' +
        ', BILLNO, CHRATE, AMT1, AMT1C, AMT2, AMT2C, KRWRATE, CHDAY, CHDA' +
        'TE1, CHDATE2'
      
        'FROM FINBIL_D LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE P' +
        'refix = '#39'5189FINBIL'#39') FIN5189 ON FINBIL_D.CHCODE = FIN5189.CODE'
      'WHERE KEYY = :KEYY')
    Left = 160
    Top = 168
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TStringField
      FieldName = 'SEQ'
      Size = 3
    end
    object qryDetailCHCODE: TStringField
      FieldName = 'CHCODE'
      Size = 3
    end
    object qryDetailBILLNO: TStringField
      FieldName = 'BILLNO'
      Size = 35
    end
    object qryDetailCHRATE: TBCDField
      FieldName = 'CHRATE'
      DisplayFormat = '#,0.00'
      Precision = 18
    end
    object qryDetailAMT1: TBCDField
      FieldName = 'AMT1'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryDetailAMT1C: TStringField
      FieldName = 'AMT1C'
      Size = 3
    end
    object qryDetailAMT2: TBCDField
      FieldName = 'AMT2'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryDetailAMT2C: TStringField
      FieldName = 'AMT2C'
      Size = 3
    end
    object qryDetailKRWRATE: TBCDField
      FieldName = 'KRWRATE'
      DisplayFormat = '#,0.00'
      Precision = 18
    end
    object qryDetailCHDAY: TSmallintField
      FieldName = 'CHDAY'
    end
    object qryDetailCHDATE1: TStringField
      FieldName = 'CHDATE1'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryDetailCHDATE2: TStringField
      FieldName = 'CHDATE2'
      EditMask = '9999.99.99;0'
      Size = 8
    end
    object qryDetailCHCODE_NAME: TStringField
      DisplayWidth = 100
      FieldName = 'CHCODE_NAME'
      ReadOnly = True
      Size = 100
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 192
    Top = 168
  end
end
