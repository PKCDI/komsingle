unit PCRLIC_N;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, Buttons, sSpeedButton, StdCtrls,
  DBCtrls, sDBMemo, sDBEdit, sLabel, sGroupBox, ExtCtrls, sSplitter, Grids,
  DBGrids, acDBGrid, ComCtrls, sPageControl, sComboBox, Mask, sMaskEdit,
  sEdit, sPanel, sBevel;

type
  TPCRLIC_N_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sPanel3: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sMaskEdit6: TsMaskEdit;
    sComboBox5: TsComboBox;
    sMaskEdit7: TsMaskEdit;
    sEdit3: TsEdit;
    sDBGrid1: TsDBGrid;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    sTabSheet4: TsTabSheet;
    sTabSheet5: TsTabSheet;
    sPanel4: TsPanel;
    sPanel5: TsPanel;
    sSplitter5: TsSplitter;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBEdit4: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sEdit17: TsEdit;
    sDBEdit12: TsDBEdit;
    sDBEdit13: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit15: TsDBEdit;
    sDBEdit16: TsDBEdit;
    sDBEdit17: TsDBEdit;
    sDBEdit18: TsDBEdit;
    sDBEdit19: TsDBEdit;
    sDBEdit20: TsDBEdit;
    sPanel8: TsPanel;
    sDBEdit25: TsDBEdit;
    sEdit34: TsEdit;
    sDBEdit66: TsDBEdit;
    sDBEdit26: TsDBEdit;
    sDBEdit27: TsDBEdit;
    sDBEdit28: TsDBEdit;
    sDBEdit29: TsDBEdit;
    sDBEdit30: TsDBEdit;
    sDBEdit31: TsDBEdit;
    sDBEdit21: TsDBEdit;
    sEdit28: TsEdit;
    sDBEdit22: TsDBEdit;
    sDBEdit24: TsDBEdit;
    sDBEdit48: TsDBEdit;
    sSpeedButton1: TsSpeedButton;
    sBevel3: TsBevel;
    sPanel6: TsPanel;
    sPanel9: TsPanel;
    sDBGrid2: TsDBGrid;
    sGroupBox5: TsGroupBox;
    sDBEdit33: TsDBEdit;
    sDBEdit36: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBMemo3: TsDBMemo;
    sDBEdit34: TsDBEdit;
    sDBEdit35: TsDBEdit;
    sDBEdit37: TsDBEdit;
    sDBEdit38: TsDBEdit;
    sDBEdit39: TsDBEdit;
    sDBEdit40: TsDBEdit;
    sDBEdit41: TsDBEdit;
    sDBEdit42: TsDBEdit;
    sDBEdit43: TsDBEdit;
    sDBEdit44: TsDBEdit;
    sDBEdit45: TsDBEdit;
    sDBEdit46: TsDBEdit;
    sDBEdit47: TsDBEdit;
    sDBEdit1: TsDBEdit;
    sDBEdit32: TsDBEdit;
    sSplitter3: TsSplitter;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    sPanel12: TsPanel;
    sDBMemo4: TsDBMemo;
    sDBEdit51: TsDBEdit;
    sDBEdit69: TsDBEdit;
    sDBEdit52: TsDBEdit;
    sDBEdit71: TsDBEdit;
    sDBEdit59: TsDBEdit;
    sDBEdit58: TsDBEdit;
    sPanel13: TsPanel;
    sDBGrid4: TsDBGrid;
    sGroupBox7: TsGroupBox;
    sDBEdit53: TsDBEdit;
    sDBMemo7: TsDBMemo;
    sDBEdit55: TsDBEdit;
    sDBEdit60: TsDBEdit;
    sDBEdit68: TsDBEdit;
    sDBEdit54: TsDBEdit;
    sDBEdit62: TsDBEdit;
    sDBEdit70: TsDBEdit;
    sDBEdit56: TsDBEdit;
    sDBEdit57: TsDBEdit;
    sDBEdit61: TsDBEdit;
    sDBEdit67: TsDBEdit;
    sDBEdit23: TsDBEdit;
    sDBEdit72: TsDBEdit;
    sSplitter4: TsSplitter;
    sSplitter6: TsSplitter;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PCRLIC_N_frm: TPCRLIC_N_frm;

implementation

{$R *.dfm}

end.
