unit LOCAPP_N;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, DBCtrls, sDBMemo, sDBEdit,
  sComboBox, sButton, sEdit, Mask, sMaskEdit, Grids, DBGrids, acDBGrid,
  ComCtrls, sPageControl, Buttons, sSpeedButton, sLabel, ExtCtrls,
  sSplitter, sPanel, sDBComboBox;

type
  TLOCAPP_N_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSplitter2: TsSplitter;
    sLabel7: TsLabel;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sLabel6: TsLabel;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    Btn_Print: TsSpeedButton;
    Btn_Ready: TsSpeedButton;
    Btn_Save: TsSpeedButton;
    Btn_Cancel: TsSpeedButton;
    Btn_Temp: TsSpeedButton;
    sSpeedButton26: TsSpeedButton;
    sSpeedButton27: TsSpeedButton;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sDBGrid1: TsDBGrid;
    sPanel7: TsPanel;
    sMaskEdit6: TsMaskEdit;
    sMaskEdit7: TsMaskEdit;
    sEdit3: TsEdit;
    sButton1: TsButton;
    sComboBox1: TsComboBox;
    sTabSheet3: TsTabSheet;
    sSplitter1: TsSplitter;
    sPanel2: TsPanel;
    sDBEdit23: TsDBEdit;
    sDBEdit2: TsDBEdit;
    DB_MESSAGE1: TsDBEdit;
    DB_MESSAGE2: TsDBEdit;
    sPanel3: TsPanel;
    sLabel3: TsLabel;
    sLabel5: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sPanel4: TsPanel;
    DB_OWNERCOMPANYNAME: TsDBEdit;
    DB_OWNERNAME: TsDBEdit;
    DB_OWNERADDR1: TsDBEdit;
    DB_OWNERADDR2: TsDBEdit;
    DB_OWNERADDR3: TsDBEdit;
    sDBEdit40: TsDBEdit;
    sDBEdit41: TsDBEdit;
    sDBEdit42: TsDBEdit;
    sDBEdit43: TsDBEdit;
    sDBEdit44: TsDBEdit;
    sTabSheet2: TsTabSheet;
    sPanel8: TsPanel;
    sLabel12: TsLabel;
    sLabel13: TsLabel;
    sLabel15: TsLabel;
    sDBEdit45: TsDBEdit;
    sDBEdit46: TsDBEdit;
    sDBEdit50: TsDBEdit;
    sDBEdit51: TsDBEdit;
    sDBEdit52: TsDBEdit;
    sDBEdit53: TsDBEdit;
    sPanel9: TsPanel;
    sDBEdit54: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBMemo3: TsDBMemo;
    sTabSheet4: TsTabSheet;
    sPanel10: TsPanel;
    sLabel14: TsLabel;
    sDBEdit55: TsDBEdit;
    sDBEdit56: TsDBEdit;
    sDBEdit57: TsDBEdit;
    sDBEdit58: TsDBEdit;
    sDBEdit60: TsDBEdit;
    sDBEdit61: TsDBEdit;
    sDBEdit62: TsDBEdit;
    sDBEdit63: TsDBEdit;
    sDBEdit64: TsDBEdit;
    sDBEdit65: TsDBEdit;
    sDBEdit66: TsDBEdit;
    sDBEdit67: TsDBEdit;
    sDBEdit68: TsDBEdit;
    sDBEdit69: TsDBEdit;
    sDBEdit70: TsDBEdit;
    sDBEdit71: TsDBEdit;
    sPanel11: TsPanel;
    sDBMemo4: TsDBMemo;
    sPanel12: TsPanel;
    sDBEdit72: TsDBEdit;
    sDBEdit73: TsDBEdit;
    sDBEdit74: TsDBEdit;
    DB_OWNERSAUPNO: TsDBEdit;
    DB_OWNERCODE: TsDBEdit;
    DB_BNFCOMPANYNAME: TsDBEdit;
    DB_BNFNAME: TsDBEdit;
    DB_BNFADDR1: TsDBEdit;
    DB_BNFADDR2: TsDBEdit;
    DB_BNFADDR3: TsDBEdit;
    DB_BNFSAUPNO: TsDBEdit;
    DB_BNFCODE: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBEdit4: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sDBEdit12: TsDBEdit;
    sDBEdit13: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit15: TsDBEdit;
    sDBEdit16: TsDBEdit;
    sDBEdit17: TsDBEdit;
    sDBEdit18: TsDBEdit;
    sDBEdit19: TsDBEdit;
    sDBEdit20: TsDBEdit;
    sDBEdit21: TsDBEdit;
    sDBEdit22: TsDBEdit;
    sDBEdit24: TsDBEdit;
    sDBEdit25: TsDBEdit;
    sDBEdit26: TsDBEdit;
    sDBEdit27: TsDBEdit;
    sDBEdit28: TsDBEdit;
    sDBEdit59: TsDBEdit;
    sDBComboBox1: TsDBComboBox;
    sDBEdit1: TsDBEdit;
    edt_MAINT_NO1: TsEdit;
    edt_MAINT_NO2: TsEdit;
    edt_MAINT_NO3: TsEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LOCAPP_N_frm: TLOCAPP_N_frm;

implementation

{$R *.dfm}

end.
