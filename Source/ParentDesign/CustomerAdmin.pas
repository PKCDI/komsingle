unit CustomerAdmin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, ExtCtrls, sPanel, sSkinProvider, Buttons,
  sSpeedButton, sSplitter, StdCtrls, sEdit, sComboBox, sBevel, Grids,
  DBGrids, acDBGrid, Mask, DBCtrls, sDBEdit,StrUtils;

type
  TCustomerAdmin_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sSplitter1: TsSplitter;
    edt_Find: TsEdit;
    sComboBox1: TsComboBox;
    sDBGrid1: TsDBGrid;
    sPanel2: TsPanel;
    sSpeedButton6: TsSpeedButton;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBEdit4: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sDBEdit12: TsDBEdit;
    sDBEdit13: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit15: TsDBEdit;
    sDBEdit16: TsDBEdit;
    sDBEdit17: TsDBEdit;
    sDBEdit18: TsDBEdit;
    sDBEdit19: TsDBEdit;
    sDBEdit20: TsDBEdit;
    sDBEdit21: TsDBEdit;
    sDBEdit22: TsDBEdit;
    procedure Btn_CloseClick(Sender: TObject);
    procedure edt_FindEnter(Sender: TObject);
    procedure edt_FindExit(Sender: TObject);
  private
    { Private declarations }
    procedure ClearText;
    procedure SetText;
  protected
    procedure ReadData; virtual; Abstract;
  public
    { Public declarations }
  end;

var
  CustomerAdmin_frm: TCustomerAdmin_frm;

implementation

uses ICON, MessageDefine;

{$R *.dfm}

procedure TCustomerAdmin_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;


procedure TCustomerAdmin_frm.ClearText;
begin
  IF AnsiCompareText(edt_Find.Text,MSG_INPUTAFTERENTER) = 0 Then
  begin
    edt_Find.Clear;
    edt_Find.Font.Color := clBlack;
  end;
end;

procedure TCustomerAdmin_frm.SetText;
begin
  IF AnsiCompareText(Trim(edt_Find.Text),'') = 0 Then
  begin
    edt_Find.Text := MSG_INPUTAFTERENTER;
    edt_Find.Font.Color := clgray;
  end;
end;

procedure TCustomerAdmin_frm.edt_FindEnter(Sender: TObject);
begin
  inherited;
  //컨트롤에 포커스가 들어오면 클리어
  ClearText;
end;

procedure TCustomerAdmin_frm.edt_FindExit(Sender: TObject);
begin
  inherited;
  //포커스를 해제 할때 텍스트박스가 비어있으면 셋팅초기화
  SetText;
end;


end.
