unit FINBIL_N;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, DBCtrls, sDBMemo, sDBEdit, Grids, DBGrids,
  acDBGrid, sComboBox, sButton, sEdit, Mask, sMaskEdit, ComCtrls,
  sPageControl, Buttons, sSpeedButton, sLabel, ExtCtrls, sSplitter, sPanel,
  sSkinProvider, sGroupBox, DB, ADODB;

type
  TFINBIL_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSplitter2: TsSplitter;
    sLabel7: TsLabel;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sLabel6: TsLabel;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    Btn_Print: TsSpeedButton;
    Btn_Ready: TsSpeedButton;
    Btn_Save: TsSpeedButton;
    Btn_Cancel: TsSpeedButton;
    Btn_Temp: TsSpeedButton;
    sSpeedButton26: TsSpeedButton;
    sSpeedButton27: TsSpeedButton;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel2: TsPanel;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sEdit1: TsEdit;
    sButton2: TsButton;
    sDBGrid1: TsDBGrid;
    sTabSheet3: TsTabSheet;
    sSplitter1: TsSplitter;
    sPanel3: TsPanel;
    sDBEdit1: TsDBEdit;
    sDBEdit23: TsDBEdit;
    sEdit2: TsEdit;
    sPanel4: TsPanel;
    sDBEdit33: TsDBEdit;
    sDBEdit34: TsDBEdit;
    sDBEdit35: TsDBEdit;
    sDBEdit36: TsDBEdit;
    sDBEdit37: TsDBEdit;
    sDBEdit38: TsDBEdit;
    sDBEdit39: TsDBEdit;
    sTabSheet2: TsTabSheet;
    sTabSheet4: TsTabSheet;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBEdit4: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sDBEdit12: TsDBEdit;
    sDBEdit13: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit15: TsDBEdit;
    sDBEdit16: TsDBEdit;
    sDBEdit17: TsDBEdit;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sDBEdit18: TsDBEdit;
    sDBEdit19: TsDBEdit;
    sPanel7: TsPanel;
    sDBMemo1: TsDBMemo;
    sPanel8: TsPanel;
    sDBEdit20: TsDBEdit;
    sDBEdit21: TsDBEdit;
    sDBEdit22: TsDBEdit;
    sDBEdit24: TsDBEdit;
    sDBEdit25: TsDBEdit;
    sDBEdit26: TsDBEdit;
    sDBEdit27: TsDBEdit;
    sDBEdit28: TsDBEdit;
    sDBEdit29: TsDBEdit;
    sDBEdit30: TsDBEdit;
    sDBEdit31: TsDBEdit;
    sDBEdit32: TsDBEdit;
    sDBEdit40: TsDBEdit;
    sDBEdit41: TsDBEdit;
    sDBEdit42: TsDBEdit;
    sDBEdit43: TsDBEdit;
    sDBEdit44: TsDBEdit;
    sDBEdit45: TsDBEdit;
    sDBEdit46: TsDBEdit;
    sDBEdit47: TsDBEdit;
    sDBEdit48: TsDBEdit;
    sDBEdit49: TsDBEdit;
    sDBEdit50: TsDBEdit;
    sDBEdit51: TsDBEdit;
    sDBEdit52: TsDBEdit;
    sDBEdit53: TsDBEdit;
    sDBGrid2: TsDBGrid;
    sGroupBox5: TsGroupBox;
    sDBEdit54: TsDBEdit;
    sDBEdit55: TsDBEdit;
    sDBEdit56: TsDBEdit;
    sDBEdit57: TsDBEdit;
    sDBEdit58: TsDBEdit;
    sDBEdit59: TsDBEdit;
    sDBEdit60: TsDBEdit;
    sDBEdit61: TsDBEdit;
    sDBEdit62: TsDBEdit;
    sDBEdit63: TsDBEdit;
    sDBEdit64: TsDBEdit;
    sDBEdit65: TsDBEdit;
    sLabel1: TsLabel;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUS: TStringField;
    qryListBUS_DESC: TStringField;
    qryListDOC_NO1: TStringField;
    qryListDOC_NO2: TStringField;
    qryListDOC_NO3: TStringField;
    qryListDOC_NO4: TStringField;
    qryListTRN_DATE: TStringField;
    qryListADV_DATE: TStringField;
    qryListTERM: TStringField;
    qryListPAYMENT: TStringField;
    qryListSPECIAL1: TStringField;
    qryListSPECIAL2: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListBANK: TStringField;
    qryListBNKNAME1: TStringField;
    qryListBNKNAME2: TStringField;
    qryListCUSTNAME1: TStringField;
    qryListCUSTNAME2: TStringField;
    qryListCUSTNAME3: TStringField;
    qryListCUSTSIGN1: TStringField;
    qryListCUSTSIGN2: TStringField;
    qryListCUSTSIGN3: TStringField;
    qryListPAYORD1: TBCDField;
    qryListPAYORD11: TBCDField;
    qryListPAYORD1C: TStringField;
    qryListRATE1: TBCDField;
    qryListPAYORD2: TBCDField;
    qryListPAYORD21: TBCDField;
    qryListPAYORD2C: TStringField;
    qryListRATE2: TBCDField;
    qryListPAYORD3: TBCDField;
    qryListPAYORD31: TBCDField;
    qryListPAYORD3C: TStringField;
    qryListRATE3: TBCDField;
    qryListPAYORD4: TBCDField;
    qryListPAYORD41: TBCDField;
    qryListPAYORD4C: TStringField;
    qryListRATE4: TBCDField;
    qryListPAYORD5: TBCDField;
    qryListPAYORD51: TBCDField;
    qryListPAYORD5C: TStringField;
    qryListRATE5: TBCDField;
    qryListFOBRATE: TBCDField;
    qryListRATEIN: TBCDField;
    qryListFINAMT1: TBCDField;
    qryListFINAMT1C: TStringField;
    qryListFINAMT2: TBCDField;
    qryListFINAMT2C: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    sDBEdit66: TsDBEdit;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TStringField;
    qryDetailCHCODE: TStringField;
    qryDetailBILLNO: TStringField;
    qryDetailCHRATE: TBCDField;
    qryDetailAMT1: TBCDField;
    qryDetailAMT1C: TStringField;
    qryDetailAMT2: TBCDField;
    qryDetailAMT2C: TStringField;
    qryDetailKRWRATE: TBCDField;
    qryDetailCHDAY: TSmallintField;
    qryDetailCHDATE1: TStringField;
    qryDetailCHDATE2: TStringField;
    qryListBUS_NAME: TStringField;
    qryListTERM_NAME: TStringField;
    qryListPAYMENT_NAME: TStringField;
    qryListSPECIAL1_NAME: TStringField;
    qryListSPECIAL2_NAME: TStringField;
    sPanel9: TsPanel;
    sPanel10: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sPanel11: TsPanel;
    sSpeedButton8: TsSpeedButton;
    qryDetailCHCODE_NAME: TStringField;
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sSpeedButton8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FINBIL_frm: TFINBIL_frm;

implementation

uses MSSQL, Commonlib;

{$R *.dfm}


procedure TFINBIL_frm.sSpeedButton4Click(Sender: TObject);
begin
  inherited;
  qryList.Prior;
end;

procedure TFINBIL_frm.sSpeedButton8Click(Sender: TObject);
begin
  inherited;
  qryList.Next;
end;

end.
