IF NOT EXISTS(SELECT CODE FROM CODE2NDD WHERE Prefix = 'INP_4401')
BEGIN
    INSERT INTO CODE2NDD(Prefix, CODE, Remark, Map, Name) VALUES('INP_4401', 'DW', '', '', 'ISSU')
    INSERT INTO CODE2NDD(Prefix, CODE, Remark, Map, Name) VALUES('INP_4401', 'DV', '', '', 'ADVI')
    INSERT INTO CODE2NDD(Prefix, CODE, Remark, Map, Name) VALUES('INP_4401', 'DU', '', '', 'ACNF')
    INSERT INTO CODE2NDM(Prefix, Name, Gubun) VALUES('INP_4401', '문서목적구분','표준')
END