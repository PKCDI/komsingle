IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ADV7002' AND COLUMN_NAME = 'PD_PRSNT5')
BEGIN
    ALTER TABLE [ADV7002] ADD [PD_PRSNT5] varchar(35) NULL
    ALTER TABLE [ADV7002] ADD CO_BANK   varchar(11)
    ALTER TABLE [ADV7002] ADD CO_BANK1  varchar(35)
    ALTER TABLE [ADV7002] ADD CO_BANK2  varchar(35)
    ALTER TABLE [ADV7002] ADD CO_BANK3  varchar(35)
    ALTER TABLE [ADV7002] ADD CO_BANK4  varchar(35)
    ALTER TABLE [ADV7002] ADD SPECIAL_DESC  varchar(1)
    ALTER TABLE [ADV7002] ADD SPECIAL_DESC_1    text
END

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ADV707' AND COLUMN_NAME = 'GOODS_DESC')
BEGIN
    ALTER TABLE [ADV707] ADD GOODS_DESC    varchar(1)
    ALTER TABLE [ADV707] ADD GOODS_DESC_1  text
    ALTER TABLE [ADV707] ADD DOC_DESC      varchar(1)
    ALTER TABLE [ADV707] ADD DOC_DESC_1    text
    ALTER TABLE [ADV707] ADD ADD_DESC      varchar(1)
    ALTER TABLE [ADV707] ADD ADD_DESC_1    text
    ALTER TABLE [ADV707] ADD SPECIAL_DESC  varchar(1)
    ALTER TABLE [ADV707] ADD SPECIAL_DESC_1 text
    ALTER TABLE [ADV707] ADD INST_DESC     varchar(1)
    ALTER TABLE [ADV707] ADD INST_DESC_1   text
END

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ADV7072' AND COLUMN_NAME = 'PURP_MSG')
BEGIN
    ALTER TABLE [ADV7072] ADD PURP_MSG varchar(35)
    ALTER TABLE [ADV7072] ADD CAN_REQ varchar(35)
    ALTER TABLE [ADV7072] ADD CON_INST varchar(35)
    ALTER TABLE [ADV7072] ADD DOC_TYPE varchar(35)
    ALTER TABLE [ADV7072] ADD CHARGE varchar(3)
    ALTER TABLE [ADV7072] ADD CHARGE_NUM1 varchar(35)
    ALTER TABLE [ADV7072] ADD CHARGE_NUM2 varchar(35)
    ALTER TABLE [ADV7072] ADD CHARGE_NUM3 varchar(35)
    ALTER TABLE [ADV7072] ADD CHARGE_NUM4 varchar(35)
    ALTER TABLE [ADV7072] ADD CHARGE_NUM5 varchar(35)
    ALTER TABLE [ADV7072] ADD CHARGE_NUM6 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_CHARGE varchar(3)
    ALTER TABLE [ADV7072] ADD AMD_CHARGE_NUM1 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_CHARGE_NUM2 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_CHARGE_NUM3 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_CHARGE_NUM4 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_CHARGE_NUM5 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_CHARGE_NUM6 varchar(35)
    ALTER TABLE [ADV7072] ADD TSHIP varchar(35)
    ALTER TABLE [ADV7072] ADD PSHIP varchar(35)
    ALTER TABLE [ADV7072] ADD DRAFT1 varchar(35)
    ALTER TABLE [ADV7072] ADD DRAFT2 varchar(35)
    ALTER TABLE [ADV7072] ADD DRAFT3 varchar(35)
    ALTER TABLE [ADV7072] ADD MIX_PAY1 varchar(35)
    ALTER TABLE [ADV7072] ADD MIX_PAY2 varchar(35)
    ALTER TABLE [ADV7072] ADD MIX_PAY3 varchar(35)
    ALTER TABLE [ADV7072] ADD MIX_PAY4 varchar(35)
    ALTER TABLE [ADV7072] ADD DEF_PAY1 varchar(35)
    ALTER TABLE [ADV7072] ADD DEF_PAY2 varchar(35)
    ALTER TABLE [ADV7072] ADD DEF_PAY3 varchar(35)
    ALTER TABLE [ADV7072] ADD DEF_PAY4 varchar(35)
    ALTER TABLE [ADV7072] ADD APPLICABLE_RULES_1 varchar(35)
    ALTER TABLE [ADV7072] ADD APPLICABLE_RULES_2 varchar(35)
    ALTER TABLE [ADV7072] ADD AVAIL varchar(11)
    ALTER TABLE [ADV7072] ADD AVAIL1 varchar(35)
    ALTER TABLE [ADV7072] ADD AVAIL2 varchar(35)
    ALTER TABLE [ADV7072] ADD AVAIL3 varchar(35)
    ALTER TABLE [ADV7072] ADD AVAIL4 varchar(35)
    ALTER TABLE [ADV7072] ADD AV_ACCNT varchar(17)
    ALTER TABLE [ADV7072] ADD DRAWEE varchar(11)
    ALTER TABLE [ADV7072] ADD DRAWEE1 varchar(35)
    ALTER TABLE [ADV7072] ADD DRAWEE2 varchar(35)
    ALTER TABLE [ADV7072] ADD DRAWEE3 varchar(35)
    ALTER TABLE [ADV7072] ADD DRAWEE4 varchar(35)
    ALTER TABLE [ADV7072] ADD CO_BANK varchar(11)
    ALTER TABLE [ADV7072] ADD CO_BANK1 varchar(35)
    ALTER TABLE [ADV7072] ADD CO_BANK2 varchar(35)
    ALTER TABLE [ADV7072] ADD CO_BANK3 varchar(35)
    ALTER TABLE [ADV7072] ADD CO_BANK4 varchar(35)
    ALTER TABLE [ADV7072] ADD REI_BANK varchar(11)
    ALTER TABLE [ADV7072] ADD REI_BANK1 varchar(35)
    ALTER TABLE [ADV7072] ADD REI_BANK2 varchar(35)
    ALTER TABLE [ADV7072] ADD REI_BANK3 varchar(35)
    ALTER TABLE [ADV7072] ADD REI_BANK4 varchar(35)
    ALTER TABLE [ADV7072] ADD AVT_BANK varchar(11)
    ALTER TABLE [ADV7072] ADD AVT_BANK1 varchar(35)
    ALTER TABLE [ADV7072] ADD AVT_BANK2 varchar(35)
    ALTER TABLE [ADV7072] ADD AVT_BANK3 varchar(35)
    ALTER TABLE [ADV7072] ADD AVT_BANK4 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_APPLIC1 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_APPLIC2 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_APPLIC3 varchar(35)
    ALTER TABLE [ADV7072] ADD AMD_APPLIC4 varchar(35)
    ALTER TABLE [ADV7072] ADD PERIOD int
    ALTER TABLE [ADV7072] ADD PERIOD_TXT varchar(35)
    ALTER TABLE [ADV7072] ADD NBANK_ISS varchar(11)
    ALTER TABLE [ADV7072] ADD NBANK_ISS1 varchar(35)
    ALTER TABLE [ADV7072] ADD NBANK_ISS2 varchar(35)
    ALTER TABLE [ADV7072] ADD NBANK_ISS3 varchar(35)
    ALTER TABLE [ADV7072] ADD NBANK_ISS4 varchar(35)
END

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ADV710' AND COLUMN_NAME = 'DRAFT1')
BEGIN
    ALTER TABLE [ADV710] ADD DRAFT1 varchar(35)
    ALTER TABLE [ADV710] ADD DRAFT2 varchar(35)
    ALTER TABLE [ADV710] ADD MIX_PAY1 varchar(35)
    ALTER TABLE [ADV710] ADD MIX_PAY2 varchar(35)
    ALTER TABLE [ADV710] ADD MIX_PAY3 varchar(35)
    ALTER TABLE [ADV710] ADD DEF_PAY1 varchar(35)
    ALTER TABLE [ADV710] ADD DEF_PAY2 varchar(35)
    ALTER TABLE [ADV710] ADD DEF_PAY3 varchar(35)
    ALTER TABLE [ADV710] ADD CO_BANK varchar(11)
    ALTER TABLE [ADV710] ADD CO_BANK1 varchar(35)
    ALTER TABLE [ADV710] ADD CO_BANK2 varchar(35)
    ALTER TABLE [ADV710] ADD CO_BANK3 varchar(35)
    ALTER TABLE [ADV710] ADD CO_BANK4 varchar(35)
    ALTER TABLE [ADV710] ADD PERIOD_TXT varchar(35)
    ALTER TABLE [ADV710] ADD SPECIAL_DESC varchar(1)
    ALTER TABLE [ADV710] ADD SPECIAL_DESC_1 text
END

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'APPPCRD1' AND COLUMN_NAME = 'COMPANY_CD')
BEGIN
    ALTER TABLE [APPPCRD1] ADD COMPANY_CD varchar(35)
END

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'BANKCODE' AND COLUMN_NAME = 'BICCODE')
BEGIN
    ALTER TABLE [BANKCODE] ADD BICCODE varchar(11)
END

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'DEBADV_H1' AND COLUMN_NAME = 'ORD1RFFACCNO')
BEGIN
    ALTER TABLE [DEBADV_H1] ADD ORD1RFFACCNO varchar(35)
    ALTER TABLE [DEBADV_H1] ADD ORD2RFFACCNO varchar(35)
    ALTER TABLE [DEBADV_H1] ADD ORD3RFFACCNO varchar(35)
    ALTER TABLE [DEBADV_H1] ADD ORD4AMT decimal(16,4)
    ALTER TABLE [DEBADV_H1] ADD ORD4AMTC varchar(3)
    ALTER TABLE [DEBADV_H1] ADD ORD4RFF varchar(3)
    ALTER TABLE [DEBADV_H1] ADD ORD4RFFNO varchar(35)
    ALTER TABLE [DEBADV_H1] ADD EXCRATE4 decimal(16,5)
    ALTER TABLE [DEBADV_H1] ADD EXCDATE41 varchar(8)
    ALTER TABLE [DEBADV_H1] ADD EXCDATE42 varchar(8)
    ALTER TABLE [DEBADV_H1] ADD RATECD41 varchar(3)
    ALTER TABLE [DEBADV_H1] ADD RATECD42 varchar(2)
    ALTER TABLE [DEBADV_H1] ADD ORD4RFFACCNO varchar(35)
END

IF NOT EXISTS(SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'PCRLICD1' AND COLUMN_NAME = 'COMPANY_CD')
BEGIN
    ALTER TABLE PCRLICD1 ADD COMPANY_CD varchar(35)
END