unit Unit1;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, sLabel, sButton, sCheckBox, sEdit;

type
  TOKBottomDlg = class(TForm)
    Bevel1: TBevel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sEdit1: TsEdit;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sCheckBox1: TsCheckBox;
    sButton1: TsButton;
    sButton2: TsButton;
    procedure sButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OKBottomDlg: TOKBottomDlg;

implementation

{$R *.dfm}

procedure TOKBottomDlg.sButton1Click(Sender: TObject);
begin
  IF MessageBox(TForm(Owner).Handle, '정말 진행하시겠습니까?', '가져오기 확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;
end;

end.
