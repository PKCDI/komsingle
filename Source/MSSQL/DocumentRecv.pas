unit DocumentRecv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sComboBox, sButton, sEdit,
  Mask, sMaskEdit, sLabel, ExtCtrls, sSplitter, Buttons, sSpeedButton,
  sPanel, Grids, DBGrids, acDBGrid, sMemo, sCheckBox, StrUtils, DB, ADODB, DateUtils,
  sListBox;

type
  TRecvDocuments_frm = class(TChildForm_frm)
    sDBGrid1: TsDBGrid;
    sPanel1: TsPanel;
    sSpeedButton3: TsSpeedButton;
    sSplitter2: TsSplitter;
    sSpeedButton5: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sPanel7: TsPanel;
    sMaskEdit6: TsMaskEdit;
    sMaskEdit7: TsMaskEdit;
    sEdit3: TsEdit;
    sButton1: TsButton;
    sComboBox1: TsComboBox;
    sCheckBox1: TsCheckBox;
    sCheckBox2: TsCheckBox;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListSRDATE: TStringField;
    qryListSunbun: TAutoIncField;
    qryListSRTIME: TStringField;
    qryListDOCID: TStringField;
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListCONTROLNO: TStringField;
    qryListRECEIVEUSER: TStringField;
    qryListMig: TMemoField;
    qryListSRVENDER: TStringField;
    qryListCON: TStringField;
    qryListDBAPPLY: TStringField;
    qryListDOCNAME: TStringField;
    qryListNAME: TStringField;
    qryDocList: TADOQuery;
    qryIns: TADOQuery;
    qryCheckDoc: TADOQuery;
    sButton2: TsButton;
    sPanel2: TsPanel;
    sLabel1: TsLabel;
    sCheckBox3: TsCheckBox;
    sLabel2: TsLabel;
    sListBox1: TsListBox;
    sButton3: TsButton;
    sLabel3: TsLabel;
    sComboBox2: TsComboBox;
    procedure sSpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sComboBox1Select(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    FOverlabCount : Integer;
    procedure AlreadyFileMove;
    procedure DivideDoc;
    procedure FileCheck(FileName : String);
    procedure InsertRecvTable(MIG : TStringList);
    function RecvDoc:Integer;

    procedure ReadList(bNoneChange:Boolean; bToday : Boolean=False);
    procedure SetDocList;
    function GetFileCountINFolder(sDir : String):Integer;

    function GetIDX(Seg: string; MIG : TStringList): integer;
    function CheckDoc(MAINT_NO : String):Boolean;

    procedure SaveData;
    procedure DocumentsProcess(DOCID,MIG : String);
  public
    { Public declarations }
  end;

var
  RecvDocuments_frm: TRecvDocuments_frm;

implementation

uses VarDefine, Commonlib, MSSQL, MIGVIEW, GENRES, FINBIL, PCRLIC;

{$R *.dfm}

{ TRecvDocuments_frm }

procedure TRecvDocuments_frm.AlreadyFileMove;
var
  nLoop,nFindRst : Integer;
  SrchRec : TSearchRec;
  sPath, sTempPath: String;
begin
  //기존 IN폴더에 있는 파일은 TEMP폴더로 옮긴다

  sPath := WINMATE_IN;
  sTempPath := WINMATE_IN + 'hm_tmp\';

  IF not DirectoryExists(sTempPath) Then
    ForceDirectories(sTempPath);

  nFindRst := FindFirst(sPath+'*.*',faAnyFile - faDirectory,SrchRec);
  while nFindRst = 0 do
  begin
    {
     The CopyFile function copies an existing file to a new file.

     CopyFile(
      lpExistingFileName : PChar, // name of an existing file
      lpNewFileName : PChar,      // name of new file
      bFailIfExists : Boolean);   // operation if file exists

    bFailIfExists:
      Specifies how this operation is to proceed if a file of the same name as
      that specified by lpNewFileName already exists.
      If this parameter is TRUE and the new file already exists, the function fails.
      If this parameter is FALSE and the new file already exists,
      the function overwrites the existing file and succeeds.
    }

    IF SrchRec.Name <> 'RECEIVE.LST' Then
    begin
//      CopyFile( PCHAR(sPath+SrchRec.Name),
//                PCHAR(sTempPath+SrchRec.Name),
//                False );
      IF FileExists(sTempPath+SrchRec.Name) Then DeleteFile(sTempPath+SrchRec.Name);

//      MoveFile( PCHAR(sPath+SrchRec.Name),
//                PCHAR(sTempPath+SrchRec.Name) );
      CopyFile( PCHAR(sPath+SrchRec.Name),
                PCHAR(sTempPath+SrchRec.Name) , True);
    end;
    nFindRst := FindNext(SrchRec);
  end;
  FindClose(SrchRec);
end;

procedure TRecvDocuments_frm.DivideDoc;
var
  nLoop,nFindRst : Integer;
  SrchRec : TSearchRec;
  sTempPath : String;
begin
  //다운로드 또는 이미 있는 파일을 hm_tmp로 이동
  AlreadyFileMove;

  sTempPath := WINMATE_TEMP;

  //분할
  nFindRst := FindFirst(sTempPath + '*.*',faAnyFile - faDirectory,SrchRec);

  while nFindRst = 0 do
  begin
    FileCheck(sTempPath + SrchRec.Name);
    nFindRst := FindNext(SrchRec);
  end;

  FindClose(SrchRec);
end;


procedure TRecvDocuments_frm.FileCheck(FileName: String);
var
  TmpList,MigList : TStringList;
  nLoop : Integer;
  FileCount : integer;
  CutPath : String;
begin
  TmpList := TStringList.Create;
  MigList := TStringList.Create;

  CutPath := WINMATE_TEMP + FormatDateTime('YYMMDD',Now)+'\';
  ForceDirectories(CutPath);

  FileCount := GetFileCountINFolder(CutPath)+1;

  try
    TmpList.LoadFromFile(FileName);

    TmpList.Text := AnsiReplaceText(TmpList.Text,'''',#13#10);

    //전송받은 자료 분할
    FOverlabCount := 0;
    sListBox1.Items.Clear;
    for nLoop := 0 to TmpList.Count-1 do
    begin
      IF AnsiMatchText( LeftStr(TmpList.Strings[nLoop],3) , ['UNB','UNG','UNE','UNZ'] ) Then Continue;

      MigList.Add(TmpList.Strings[nLoop]);

      IF LeftStr(TmpList.Strings[nLoop],4) = 'UNT+' Then
      begin
        //RecvTable에 INSERT
        InsertRecvTable(MigList);

        MigList.SaveToFile(CutPath + 'R'+FormatFloat('00000',FileCount)+'.dat');
        MigList.Clear;
        Inc(FileCount);
      end;
    end;

    MoveFile(PChar(FileName),PCHAR(CutPath + ExtractFileName(FileName)));

  finally
    TmpList.Free;
    MigList.Free;
  end;
end;

function TRecvDocuments_frm.GetIDX(Seg: string; MIG : TStringList): integer;
var
  nLoop : Integer;
begin
  Result := -1;
  for nLoop := 0 to MIG.Count - 1 do
  begin
    IF LeftStr(MIG.Strings[nLoop],3) = UpperCase(Seg) Then
    begin
       Result := nLoop;

       Break;
    end;
  end;
end;

procedure TRecvDocuments_frm.InsertRecvTable(MIG: TStringList);
var
  TempList : TStringList;
  nLoop,
  UNHIDX, BGMIDX : integer;
  DocID, DocNo, Sender, Consignee, RecvDate, RecvTime : String;
begin
  //UNH항목 찾음
  UNHIDX := GetIDX('UNH',MIG);
  //BGM항목
  BGMIDX := GetIDX('BGM',MIG);

  IF UNHIDX = -1 Then Raise Exception.Create('문서변환을 실행 할 수 없습니다.'#13#10'ERROR MSG : UNHIDX is -1');
  IF BGMIDX = -1 Then Raise Exception.Create('문서변환을 실행 할 수 없습니다.'#13#10'ERROR MSG : BGMIDX is -1');

  //UNG를 조사하여 RecvTable에 INSERT
  TempList := TStringList.Create;
  try
    with TempList,qryIns do
    begin
      Delimiter := '+';
      DelimitedText := MIG.Strings[UNHIDX];
      //Documetns Header
      DocID := Strings[2];
      DocID := MidStr(Strings[2],1,POS(':',Strings[2])-1);
//      Sender := Strings[2];
//      Consignee := Strings[4];
//      RecvDate := Strings[6];
//      RecvTime := Strings[7];

      DelimitedText := MIG.Strings[BGMIDX];
      DocNo := Strings[2];

      IF CheckDoc(DocNo) and sCheckBox3.Checked Then
      begin
        Inc(FOverlabCount);
        sListBox1.Items.Add(DocID+' | '+DocNo);
        Exit;
      end;

      Parameters.ParamByName('SRDATE'   ).Value := FormatDateTime('YYYYMMDD',Now);
      Parameters.ParamByName('SRTIME'   ).Value := FormatDateTime('HH:NN:SS',Now);
      Parameters.ParamByName('DOCID'    ).Value := DocID;
      Parameters.ParamByName('MAINT_NO' ).Value := DocNo;
      Parameters.ParamByName('MSEQ'     ).Value := '0';
      Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
      Parameters.ParamByName('Mig'      ).Value := MIG.Text;
      Parameters.ParamByName('DOCNAME'  ).Value := DocID;
      ExecSQL;

    end;
  finally
    TempList.Free;
  end;
end;

function TRecvDocuments_frm.RecvDoc: Integer;
begin
  //수신문서 다운로드
  IF not sCheckBox2.Checked Then
    WinExecAndWait32(WINMATE_HERMES,SW_NORMAL);

  //다운로드된 파일을 분할 하여 hm_tmp에 저장 및 Table에 저장
  DivideDoc;

  WinExecAndWait32(WINMATE_CONVERTEXE_IN,SW_NORMAL);

  //중복문서 확인
  sPanel2.Visible := FOverlabCount > 0;

  //데이터변환 및 DB입력
  SaveData;

  //확인
  ReadList(False);

  ShowMessage('다운로드 완료');
end;

procedure TRecvDocuments_frm.sSpeedButton1Click(Sender: TObject);
begin
  inherited;
  RecvDoc;
//  sSpeedButton4Click(nil);
end;

procedure TRecvDocuments_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
  sMaskEdit6.Text := '20000101';
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD',EndOfTheMonth(Now));
  SetDocList;
  ReadList(False);
end;

procedure TRecvDocuments_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList(False);
end;

procedure TRecvDocuments_frm.ReadList(bNoneChange:Boolean; bToday : Boolean=False);
begin
  IF bToday Then
  begin
    sMaskEdit6.Text := FormatDateTime('YYYYMMDD',Now);
    sMaskEdit7.Text := FormatDateTime('YYYYMMDD',NOw);
  end;

  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE SRDATE Between '+QuotedStr(sMaskEdit6.Text)+' AND '+QuotedStr(sMaskEdit7.Text));
    IF sComboBox1.ItemIndex > 0 Then
      SQL.Add('AND DOCID = '+QuotedStr(sComboBox1.Text));
    IF bNoneChange Then
      SQL.Add('AND CON NOT IN ('+QuotedStr('N')+','+QuotedStr('Y')+')');
    SQL.Add('ORDER BY SRDATE DESC, SRTIME DESC');
    Open;
  end;
end;

procedure TRecvDocuments_frm.SetDocList;
begin
  sCombobox1.Items.Clear;

  qryDocList.Close;
  qryDocList.Open;
  try
    sComboBox1.items.Add('전체');
    while not qryDocList.Eof do
    begin
      sComboBox1.Items.Add(Trim(qryDocList.FieldByName('DOCID').AsString));
      qryDocList.Next;
    end;
  finally
    qryDocList.Close;
  end;
  sComboBox1.ItemIndex := 0;
end;

procedure TRecvDocuments_frm.sComboBox1Select(Sender: TObject);
begin
  inherited;
  Readlist(False);
end;

function TRecvDocuments_frm.GetFileCountINFolder(sDir: String): Integer;
var
  nFindRst : integer;
  SrchRec : TSearchRec;  
begin
  //분할
  Result := 0;
  nFindRst := FindFirst(sDir + '*.*',faAnyFile - faDirectory,SrchRec);
  while nFindRst = 0 do
  begin
    Result := Result + 1;
    nFindRst := FindNext(SrchRec);
  end;
end;

function TRecvDocuments_frm.CheckDoc(MAINT_NO: String): Boolean;
begin
  with qryCheckDoc do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
    Open;

    Result := qryCheckDoc.RecordCount > 0;
    Close;
  end;
end;

procedure TRecvDocuments_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  MIGVIEW_frm := TMIGVIEW_frm.Create(Self);
  try
    MIGVIEW_frm.Run(qryListMig.AsString);
  finally
    FreeAndNil(MIGVIEW_frm);
  end;
end;

procedure TRecvDocuments_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  ReadList(False,True);
end;

procedure TRecvDocuments_frm.SaveData;
begin
  ReadList(True);
  qryList.First;

  while not qryList.Eof do
  begin
    DocumentsProcess(qryListDOCID.AsString,qryListMig.AsString);
    qryList.Next;
  end;
end;

procedure TRecvDocuments_frm.DocumentsProcess(DOCID, MIG: String);
begin
  DOCID := UpperCase(DOCID);

  IF DOCID = 'GENRES' THEN
  begin
    DOC_GENRES := TDOC_GENRES.Create(Self);
    try
      DOC_GENRES.sMemo1.Lines.Text := MIG;
      DOC_GENRES.sEdit1.Text := qryListSunbun.AsString;
      DOC_GENRES.Run;
    finally
      FreeAndNil(DOC_GENRES);
    end;
  end
  Else
  IF DOCID = 'FINBIL' THEN
  BEGIN
    DOC_FINBIL := TDOC_FINBIL.Create(Self);
    try
       DOC_FINBIL.sMemo1.Lines.Text := MIG;
       DOC_FINBIL.sEdit1.Text := qryListSunbun.AsString;
       DOC_FINBIL.Run;
    finally
      FreeAndNil(DOC_FINBIL);
    end;
  end
  Else
  IF DOCID = 'PCRLIC' then
  BEGIN
    DOC_PCRLIC := TDOC_PCRLIC.Create(Self);
    try
      DOC_PCRLIC.sMemo1.Lines.Text := MIG;
      DOC_PCRLIC.sEdit1.Text := qryListSunbun.AsString;
      DOC_PCRLIC.Run2;
    finally
      FreeAndNil(DOC_PCRLIC);
    end;
  end;
end;

procedure TRecvDocuments_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TRecvDocuments_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  RecvDocuments_frm := nil;
end;

procedure TRecvDocuments_frm.sSpeedButton4Click(Sender: TObject);
begin
  inherited;
  DocumentsProcess(qryListDOCID.AsString,qryListMig.AsString);
  ShowMessage('변환완료');
end;

procedure TRecvDocuments_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  sPanel2.Visible := False;
end;

end.
