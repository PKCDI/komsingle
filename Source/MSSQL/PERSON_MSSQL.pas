unit PERSON_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sButton, ComCtrls,
  sPageControl, Grids, DBGrids, acDBGrid, ExtCtrls, sPanel, DB, ADODB,
  sComboBox;

type
  TPERSON_MSSQL_frm = class(TChildForm_frm)
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet2: TsTabSheet;
    sButton1: TsButton;
    sPanel1: TsPanel;
    sDBGrid1: TsDBGrid;
    sButton2: TsButton;
    sButton3: TsButton;
    sButton4: TsButton;
    qryPerson: TADOQuery;
    dsPerson: TDataSource;
    qryPersonUSERID: TStringField;
    qryPersonNAME: TStringField;
    qryPersonDEPT: TStringField;
    qryPersonPASSWORD: TStringField;
    qryPersonDEPTCODE: TStringField;
    qryPersonACTIVE: TStringField;
    qryPersonJOIN_DT: TDateTimeField;
    sPanel2: TsPanel;
    sComboBox1: TsComboBox;
    sButton5: TsButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure setUserList;
  public
    { Public declarations }
  end;

var
  PERSON_MSSQL_frm: TPERSON_MSSQL_frm;

implementation

uses
  ICON, MSSQL;

{$R *.dfm}

{ TPERSON_MSSQL_frm }

procedure TPERSON_MSSQL_frm.setUserList;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT USERID FROM PERSON ORDER BY USERID';
      Open;

      sComboBox1.Clear;
      while not eof do
      begin
        sComboBox1.Items.Add(FieldByName('USERID').AsString);
        Next;
      end;

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TPERSON_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  setUserList;
end;

procedure TPERSON_MSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  PERSON_MSSQL_frm := nil;
end;

procedure TPERSON_MSSQL_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  Close;
end;


end.
