unit RecvDocumentsV1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sListBox, Grids, DBGrids, acDBGrid,
  sComboBox, sButton, sEdit, Mask, sMaskEdit, sCheckBox, sLabel, ExtCtrls,
  sSplitter, Buttons, sSpeedButton, sPanel, sSkinProvider, DB, ADODB, DateUtils, StrUtils,
  sRadioButton, sMemo;

type
  TRecvDocumentsV1_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton3: TsSpeedButton;
    sSplitter2: TsSplitter;
    sSpeedButton5: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sCheckBox2: TsCheckBox;
    sCheckBox3: TsCheckBox;
    sPanel7: TsPanel;
    sMaskEdit6: TsMaskEdit;
    sMaskEdit7: TsMaskEdit;
    sEdit3: TsEdit;
    sButton1: TsButton;
    sComboBox1: TsComboBox;
    sButton2: TsButton;
    sDBGrid1: TsDBGrid;
    sPanel2: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sListBox1: TsListBox;
    sButton3: TsButton;
    qryList: TADOQuery;
    qryListSRDATE: TStringField;
    qryListSunbun: TAutoIncField;
    qryListSRTIME: TStringField;
    qryListDOCID: TStringField;
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListCONTROLNO: TStringField;
    qryListRECEIVEUSER: TStringField;
    qryListMig: TMemoField;
    qryListSRVENDER: TStringField;
    qryListCON: TStringField;
    qryListDBAPPLY: TStringField;
    qryListDOCNAME: TStringField;
    qryListNAME: TStringField;
    dsList: TDataSource;
    qryDocList: TADOQuery;
    qryIns: TADOQuery;
    qryCheckDoc: TADOQuery;
    sButton4: TsButton;
    sCheckBox1: TsCheckBox;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    sSpeedButton9: TsSpeedButton;
    sMemo1: TsMemo;
    sSplitter1: TsSplitter;
    procedure FormCreate(Sender: TObject);
    procedure sComboBox1Select(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sButton1Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Btn_CloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sRadioButton1Click(Sender: TObject);
    procedure sRadioButton2Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sSpeedButton6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sPanel1DblClick(Sender: TObject);
    procedure sSpeedButton8Click(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList(bNoneChange:Boolean; bToday : Boolean=False);
    procedure SetDocList;
    procedure ClearFLATIN;
    procedure FileCheck(FileName : String);
    procedure readMig;
  public
    { Public declarations }
  end;

var
  RecvDocumentsV1_frm: TRecvDocumentsV1_frm;

implementation

uses Commonlib, VarDefine, LOCADV, PCRLIC_NEW, MessageDefine, FINBIL_NEW,
  GENRES_NEW, INF700, INF707, DOANTC, LOGUAR, DOMOFB, LOCAMA, VATBI4, VATBI2,
  LOCRC1, LOCAD1, LOCAM1, SPCNTC , ADV700, ADV707, RECVDOC, EDIFACT_RECV, EDIFACT, UI_ATTNTC_NEW, MSSQL,
  dlg_AUTHPWD;

{$R *.dfm}

{ TRecvDocumentsV1_frm }

procedure TRecvDocumentsV1_frm.ReadList(bNoneChange, bToday: Boolean);
begin
  IF bToday Then
  begin
    sMaskEdit6.Text := FormatDateTime('YYYYMMDD',Now);
    sMaskEdit7.Text := FormatDateTime('YYYYMMDD',NOw);
  end;

  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE SRDATE Between '+QuotedStr(sMaskEdit6.Text)+' AND '+QuotedStr(sMaskEdit7.Text));
    IF sComboBox1.ItemIndex > 0 Then
      SQL.Add('AND DOCID = '+QuotedStr(sComboBox1.Text));
    IF bNoneChange Then
      SQL.Add('AND CON != '+QuotedStr('Y')+' OR CON IS NULL');
//      SQL.Add('AND CON NOT IN ('+QuotedStr('N')+','+QuotedStr('Y')+')');
    SQL.Add('ORDER BY SRDATE DESC, SRTIME DESC');
    Open;
  end;
end;

procedure TRecvDocumentsV1_frm.FormCreate(Sender: TObject);
begin
  inherited;
  {$IFDEF DEBUG}
  sCheckBox2.Checked := True;
  {$ELSE}
  sCheckBox2.Checked := False;
  {$ENDIF}
  FSQL := qryList.SQL.Text;
  sMaskEdit6.Text := FormatDateTime('YYYYMMDD', Now);
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD', Now);
//  sMaskEdit6.Text := FormatDateTime('YYYYMMDD',EndOfTheMonth(Now));
//  sMaskEdit7.Text := FormatDateTime('YYYYMMDD',EndOfTheMonth(Now));
//  SetDocList;
  ReadList(False);
end;

procedure TRecvDocumentsV1_frm.SetDocList;
begin
  sCombobox1.Items.Clear;

  qryDocList.Close;
  qryDocList.Open;
  try
    sComboBox1.items.Add('전체');
    while not qryDocList.Eof do
    begin
      sComboBox1.Items.Add(Trim(qryDocList.FieldByName('DOCID').AsString));
      qryDocList.Next;
    end;
  finally
    qryDocList.Close;
  end;
  sComboBox1.ItemIndex := 0;
end;

procedure TRecvDocumentsV1_frm.sComboBox1Select(Sender: TObject);
begin
  inherited;
  Readlist(False);
end;

procedure TRecvDocumentsV1_frm.sSpeedButton1Click(Sender: TObject);
var
  SearchRec : TSearchRec;
begin
  inherited;

  //수신프로그램 실행
  IF not sCheckBox2.Checked Then
  begin
    ClearFLATIN;
    WinExecAndWait32(WINMATE_HERMES,SW_NORMAL);
  end;

  //IN폴더의 파일들 변환
  WinExecAndWait32(WINMATE_CONVERTEXE_IN,SW_NORMAL);

  //변환파일 확인
  if FindFirst(WINMATE_FLATIN+'*.unh',faAnyFile,SearchRec) = 0 Then
  begin
    repeat
      FileCheck(WINMATE_FLATIN+SearchRec.Name);
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
  end;

  sPanel2.Visible := sListBox1.Items.Count > 0;

  qryList.DisableControls;
  ReadList(False,True);
  qryList.EnableControls;

  //자동변환이라면
  IF sCheckBox1.Checked Then
  begin
    qryList.First;
    while not qryList.Eof do
    begin
      if qryListCON.AsString <> 'Y' Then
        sSpeedButton4Click(nil);
      qryList.Next;
    end;
  end;

  qryList.DisableControls;
  ReadList(False,True);
  qryList.EnableControls;

end;

procedure TRecvDocumentsV1_frm.ClearFLATIN;
var
  SearchRec : TSearchRec;
begin
  if FindFirst(WINMATE_FLATIN+'*.*',faAnyFile,SearchRec) = 0 Then
  begin
    repeat
      DeleteFile(WINMATE_FLATIN+SearchRec.Name);
    until FindNext(SearchRec) <> 0;
    FindClose(SearchRec);
  end;
end;

procedure TRecvDocumentsV1_frm.FileCheck(FileName: String);
var
  MIG : TStringList;
  SendID, DocID, Msg1Gubun : String;
  uEDIFACT : TEDIFACT_RECV;
begin
  MIG := TStringList.Create;
  try
    MIG.LoadFromFile(FileName);
    MIG.Insert(0,ExtractFileName( FileName ));
    SendID := Trims(MIG.Strings[1],1,18);
    DocID  := Trims(MIG.Strings[1],37,7);
    Msg1Gubun := Trims(MIG.Strings[6],5,2);

    //ShowMessage(Msg1Gubun);
    //개설업자(LOCADV) 미그에 들어오는 값중 Message1이 43이아닌 나머지값
    //수혜업자(LOCAD1) 미그에 들어오는 값중  Message1이 43인 경우
    IF (DocID = 'LOCADV') and (Trim(Msg1Gubun) <> '43')  Then
    begin
      LOCADV_DOC := TLOCADV.Create(MIG);
      LOCADV_DOC.Run_Ready(sCheckBox3.Checked);
      FreeAndNil(LOCADV_DOC);
    end
    else IF (DocID = 'LOCADV') and (Trim(Msg1Gubun) = '43') Then
    begin
      LOCAD1_DOC := TLOCAD1.Create(MIG);
      LOCAD1_DOC.Run_Ready(sCheckBox3.Checked);
      FreeAndNil(LOCAD1_DOC);
    end
    else IF DocID = 'PCRLIC' Then
    begin
      PCRLIC_NEW_DOC := TPCRLIC_NEW.Create(MIG);
      PCRLIC_NEW_DOC.Run_Ready(sCheckBox3.Checked);
      FreeAndNil(PCRLIC_NEW_DOC);
    end
    else IF DocID = 'FINBIL' Then
    begin
      FINBIL_NEW_DOC := TFINBIL_NEW.Create(MIG);
      FINBIL_NEW_DOC.Run_Ready(sCheckBox3.Checked);
      FreeAndNil(FINBIL_NEW_DOC);
    end
    else IF DocID = 'GENRES' Then
    begin
      GENRES_NEW_DOC := TGENRES_NEW.Create(MIG);
      GENRES_NEW_DOC.Run_Ready(False);
      FreeAndNil(GENRES_NEW_DOC);
    end
    else if DocID = 'DOANTC' then
    begin
      DOANTC_DOC := TDOANTC.Create(MIG);
      DOANTC_DOC.Run_Ready(False);
      FreeAndNil(DOANTC_DOC);
    end
    else if DocID = 'DOMOFR' then //DOMOFB문서임
    begin
      DOMOFB_DOC := TDOMOFB.Create(MIG);
      DOMOFB_DOC.Run_Ready(False);
      FreeAndNil(DOMOFB_DOC);
    end
    //개설업자(LOCAMA) 미그에 들어오는 값중 Message1이 43이아닌 나머지값
    //수혜업자(LOCAM1) 미그에 들어오는 값중  Message1이 43인 경우
    else if (DocID = 'LOCAMA') and (Trim(Msg1Gubun) <> '43') then
    begin
      LOCAMA_DOC := TLOCAMA.Create(MIG);
      LOCAMA_DOC.Run_Ready(False);
      FreeAndNil(LOCAMA_DOC);
    end
    else if (DocID = 'LOCAMA') and (Trim(Msg1Gubun) = '43') then
    begin
      LOCAM1_DOC := TLOCAM1.Create(MIG);
      LOCAM1_DOC.Run_Ready(False);
      FreeAndNil(LOCAM1_DOC);
    end
    else
    if DocID = 'LOCRCT' then
    begin
      LOCRC1_DOC := TLOCRC1.Create(MIG);
      LOCRC1_DOC.Run_Ready(False);
      FreeAndNil(LOCRC1_DOC);
    end
    else
    if DocID = 'ADV700' then
    begin
      ADV700_DOC := TADV700.Create(MIG);
      ADV700_DOC.Run_Ready(False);
      FreeAndNil(ADV700_DOC);
    end
    else
    if DocID = 'ADV707' then
    begin
      ADV707_DOC := TADV707.Create(MIG);
      ADV707_DOC.Run_Ready(False);
      FreeAndNil(ADV707_DOC);
    end;
//------------------------------------------------------------------------------
// 2018-11-19
// 입금통지서
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// 2018-11-30
// 근거서류첨부 통지서
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// 2018-12-04
// 내국신용장 판매대금 추심 통보서
//------------------------------------------------------------------------------
    try
           IF DocID = 'CREADV' Then uEDIFACT := TCREADV_NEW.Create(FileName)
      else IF DocID = 'ATTNTC' Then uEDIFACT := TATTNTC_NEW.Create(FileName)
      else IF DocID = 'LDANTC' Then uEDIFACT := TLDANTC_NEW.Create(FileName)
      else if DocID = 'SPCNTC' then uEDIFACT := TSPCNTC_NEW.Create(FileName)
      else IF DocID = 'INF700' then uEDIFACT := TINF700_NEW.Create(FileName)
      else if DocID = 'INF707' then uEDIFACT := TINF707_NEW.Create(FileName)
      else if DocID = 'LOGUAR' then uEDIFACT := TLOGUAR_NEW.Create(FileName)
      else IF DocID = 'VATBIL' then uEDIFACT := TVATBIL_NEW.Create(FileName)
      else if DocID = 'DEBADV' then uEDIFACT := TDEBADV_NEW.Create(FileName)
      else if DocID = 'ADV710' then uEDIFACT := TADV710_NEW.Create(FileName);
      IF uEDIFACT <> nil Then uEDIFACT.DefineList;
    finally
      IF uEDIFACT <> nil Then uEDIFACT.Free;
    end;

    DeleteFile(FileName);    
  finally
    MIG.Free;
  end;
end;


procedure TRecvDocumentsV1_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  qryList.Properties['Unique Table'].Value := 'R_HST';
//  sSpeedButton1.Enabled := (DataSet.RecordCount > 0);
  sSpeedButton4.Enabled := (DataSet.RecordCount > 0);
  sSpeedButton5.Enabled := (DataSet.RecordCount > 0);
  sSpeedButton6.Enabled := (DataSet.RecordCount > 0) AND (DataSet.FieldByName('CON').AsString <> 'Y');
  SetDocList;  
end;

procedure TRecvDocumentsV1_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList(False);
end;

procedure TRecvDocumentsV1_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  ReadList(False,True);
end;

procedure TRecvDocumentsV1_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  sListBox1.Clear;
  sPanel2.Visible := False;
end;

procedure TRecvDocumentsV1_frm.sSpeedButton4Click(Sender: TObject);
var
  TempList : TStringList;
  ControlNo : String;
  ConvertOK : Boolean;
  uEDIFACT : TEDIFACT_RECV;
begin
  inherited;
  TempList := TStringList.Create;
  try
    TempList.Text := qryListMig.AsString;
    ControlNo := qryListCONTROLNO.AsString;

    //개설업자(LOCADV)와 수혜업자(LOCAD1)에 내국신용장 응답서 구분을 위해서 DOCNAME의 값으로 확인함
    IF qryListDOCNAME.AsString = 'LOCADV' Then
    begin
      LOCADV_DOC := TLOCADV.Create(TempList);
      ConvertOK := LOCADV_DOC.Run_Convert;
      FreeAndNil(LOCADV_DOC);
    end
    else
    if qryListDOCNAME.AsString = 'LOCAD1' then
    begin
      LOCAD1_DOC := TLOCAD1.Create(TempList);
      ConvertOK := LOCAD1_DOC.Run_Convert;
      FreeAndNil(LOCAD1_DOC);
    end
    else
    IF qryListDOCID.AsString = 'PCRLIC' Then
    begin
      PCRLIC_NEW_DOC := TPCRLIC_NEW.Create(TempList);
      ConvertOK := PCRLIC_NEW_DOC.Run_Convert;
      FreeAndNil(PCRLIC_NEW_DOC);
    end
    else
    IF qryListDOCID.AsString = 'FINBIL' Then
    begin
      FINBIL_NEW_DOC := TFINBIL_NEW.Create(TempList);
      ConvertOK := FINBIL_NEW_DOC.Run_Convert;
      FreeAndNil(FINBIL_NEW_DOC);
    end
    else
    if qryListDOCID.AsString = 'GENRES' Then
    begin
      GENRES_NEW_DOC := TGENRES_NEW.Create(TempList);
      ConvertOK := GENRES_NEW_DOC.Run_Convert;
      FreeAndNil(GENRES_NEW_DOC);
    end;
    if qryListDOCID.AsString = 'DOANTC' then
    begin
      DOANTC_DOC := TDOANTC.Create(TempList);
      ConvertOK := DOANTC_DOC.Run_Convert;
      FreeAndNil(DOANTC_DOC);
    end;
    if qryListDOCID.AsString = 'DOMOFR' then //DOMOFB문서임
    begin
      DOMOFB_DOC := TDOMOFB.Create(TempList);
      ConvertOK := DOMOFB_DOC.Run_Convert;
      FreeAndNil(DOMOFB_DOC);
    end;
    //개설업자(LOCAMA)와 수혜업자(LOCAM1)에 내국신용장 조건변경통지서 구분을 위해서 DOCNAME의 값으로 확인함
    if qryListDOCNAME.AsString = 'LOCAMA' then
    begin
      LOCAMA_DOC := TLOCAMA.Create(TempList);
      ConvertOK := LOCAMA_DOC.Run_Convert;
      FreeAndNil(LOCAMA_DOC);
    end;
    if qryListDOCNAME.AsString = 'LOCAM1' then
    begin
      LOCAM1_DOC := TLOCAM1.Create(TempList);
      ConvertOK := LOCAM1_DOC.Run_Convert;
      FreeAndNil(LOCAM1_DOC);
    end;
    if qryListDOCID.AsString = 'LOCRC1' then
    begin
      LOCRC1_DOC := TLOCRC1.Create(TempList);
      try
        IF LOCRC1_DOC.isReConvert(qryListMAINT_NO.AsString) Then
          ConvertOK := LOCRC1_DOC.Run_Convert;
      finally
        FreeAndNil(LOCRC1_DOC);
      end;
    end;
    if qryListDOCID.AsString = 'ADV700' then
    begin
      ADV700_DOC := TADV700.Create(TempList);
      ConvertOK := ADV700_DOC.Run_Convert;
      FreeAndNil(ADV700_DOC);
    end;
    if qryListDOCID.AsString = 'ADV707' then
    begin
      ADV707_DOC := TADV707.Create(TempList);
      ConvertOK := ADV707_DOC.Run_Convert;
      FreeAndNil(ADV707_DOC);
    end;

//------------------------------------------------------------------------------
// 2018-11-19
// 입금통지서 추가
// 2018-12-04
// 근거서류 첨부 응답서 - ATTNTC
// 2018-12-04
// 내국신용장 판매대금 추심 도착통보서 - LDANTC
// 2018-12-05
// 판매대금추심 처리 응답서 - SPCNTC
//------------------------------------------------------------------------------
    try
           if qryListDOCID.AsString = 'INF700' then uEDIFACT := TINF700_NEW.CreateMig(TempList.Text)
      else if qryListDOCID.AsString = 'INF707' then uEDIFACT := TINF707_NEW.CreateMig(TempList.Text)
      else IF qryListDOCID.AsString = 'LDANTC' Then uEDIFACT := TLDANTC_NEW.CreateMig(TempList.Text)
      else IF qryListDOCID.AsString = 'SPCNTC' Then uEDIFACT := TSPCNTC_NEW.CreateMig(TempList.Text)
      else if qryListDOCID.AsString = 'ATTNTC' Then uEDIFACT := TATTNTC_NEW.CreateMig(TempList.Text)
      else if qryListDOCID.AsString = 'CREADV' Then uEDIFACT := TCREADV_NEW.CreateMig(TempList.Text)
      else if qryListDOCID.AsString = 'LOGUAR' Then uEDIFACT := TLOGUAR_NEW.CreateMig(TempList.Text)
      else if qryListDOCID.AsString = 'VATBIL' Then uEDIFACT := TVATBIL_NEW.CreateMig(TempList.Text)
      else if qryListDOCID.AsString = 'DEBADV' Then uEDIFACT := TDEBADV_NEW.CreateMig(TempList.Text)
      else if qryListDOCID.AsString = 'ADV710' Then uEDIFACT := TADV710_NEW.CreateMig(TempList.Text);
      IF uEDIFACT <> nil Then ConvertOK := uEDIFACT.ParseData;
    finally
      IF uEDIFACT <> nil Then
      FreeAndNil(uEDIFACT);
    end;

//------------------------------------------------------------------------------
// 변환완료 체크
//------------------------------------------------------------------------------
    qryList.Edit;
    IF ConvertOK Then
      qryListCON.AsString := 'Y'
    else
      qryListCON.AsString := 'N';
    qryList.Post;

  finally
    TempList.Free;
  end;
end;

procedure TRecvDocumentsV1_frm.sDBGrid1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF KEY = VK_SPACE THEN
  begin
    sSpeedButton4Click(nil);
    qryList.Next;    
  end;
end;

procedure TRecvDocumentsV1_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TRecvDocumentsV1_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  RecvDocumentsV1_frm := nil;
  FAuthOK := False;  
end;

procedure TRecvDocumentsV1_frm.sRadioButton1Click(Sender: TObject);
begin
  inherited;
  ReadList(False);
end;

procedure TRecvDocumentsV1_frm.sRadioButton2Click(Sender: TObject);
begin
  inherited;
  ReadList(True,True);
end;

procedure TRecvDocumentsV1_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  sSpeedButton1.Enabled := (DataSet.RecordCount > 0);
  sSpeedButton4.Enabled := (DataSet.RecordCount > 0);
  sSpeedButton5.Enabled := (DataSet.RecordCount > 0);
  sSpeedButton6.Enabled := (DataSet.RecordCount > 0) AND ((DataSet.FieldByName('CON').AsString <> 'Y') OR FAuthOK);
  readmig;
end;

procedure TRecvDocumentsV1_frm.sSpeedButton6Click(Sender: TObject);
var
  nIDX : integer;
begin
  inherited;
  IF MessageBox(Self.Handle, MSG_SYSTEM_DEL_RECVDOC, '수신문서 삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL THEN Exit;

  nIDX := qryList.RecNo;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'DELETE FROM R_HST WHERE Sunbun = '+qryListSunbun.AsString;
      ExecSQL;
      ReadList(False);
      IF nIDX > 1 Then
        qryList.MoveBy(nIDX-1);
          
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TRecvDocumentsV1_frm.FormShow(Sender: TObject);
begin
  inherited;
  sSpeedButton6.Enabled := FAuthOK;
end;

procedure TRecvDocumentsV1_frm.sPanel1DblClick(Sender: TObject);
begin
  inherited;
  FAuthOK := dlg_AUTHPWD_frm.ShowModal = mrOk;
  sSpeedButton6.Enabled := FAuthOK;
end;

procedure TRecvDocumentsV1_frm.sSpeedButton8Click(Sender: TObject);
begin
  inherited;
  sMemo1.Width := 500;
  sMemo1.Visible := not sMemo1.Visible;
  sSplitter1.Visible := sMemo1.Visible;

  readMig;
end;

procedure TRecvDocumentsV1_frm.readMig;
begin
  if sMemo1.Visible Then
  begin
    sMemo1.Lines.Text := qryListMig.AsString;
  end;
end;

end.
