inherited dlg_CreateDocumentChoice_frm: Tdlg_CreateDocumentChoice_frm
  Left = 1959
  Top = 161
  Caption = #45236#44397#49888#50857#51109' '#51089#49457
  ClientHeight = 269
  ClientWidth = 326
  KeyPreview = True
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 326
    Height = 269
    inherited sLabel6: TsLabel
      Left = 13
      Width = 74
      Caption = 'LOCAPP'
    end
    inherited sLabel7: TsLabel
      Left = 13
      Width = 124
      Caption = #45236#44397#49888#50857#51109' '#44060#49444#49888#52397#49436
    end
    inherited sBevel1: TsBevel
      Left = 12
      Width = 300
      Anchors = [akLeft, akTop, akRight]
    end
    inherited sLabel1: TsLabel
      Left = 13
    end
    inherited sBevel2: TsBevel
      Left = 12
      Top = 188
      Width = 300
      Anchors = [akLeft, akTop, akRight]
    end
    inherited sButton2: TsButton
      Left = 13
      Top = 72
      Width = 300
      Caption = #49888#44508#46321#47197'(&1)'
      Font.Height = -13
      Font.Style = [fsBold]
      ModalResult = 1
      ParentFont = False
      CommandLinkHint = #49324#50857#51088#44032' '#51649#51217' '#51077#47141#54633#45768#45796
      Style = bsPushButton
    end
    inherited sButton3: TsButton
      Left = 13
      Top = 198
      Width = 300
      Caption = #51089#49457#52712#49548'(ESC)'
      Font.Height = -13
      ModalResult = 2
      ParentFont = False
      Style = bsPushButton
    end
    inherited sButton1: TsButton
      Left = 13
      Top = 128
      Width = 300
      Caption = #47932#54408#47588#46020#54869#50557#49436'('#44060#49444#51088'-'#49688#49888')'#50640#49436' '#49440#53469'(&2)'
      Font.Height = -13
      ModalResult = 6
      ParentFont = False
      CommandLinkHint = #49688#49888#48155#51008' '#47932#54408#47588#46020#54869#50557#49436#47484' '#44592#51456#51004#47196' '#47928#49436#47484' '#51089#49457#54633#45768#45796
      ImageIndex = 31
      Style = bsPushButton
    end
  end
end
