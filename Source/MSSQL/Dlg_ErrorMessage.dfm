inherited Dlg_ErrorMessage_frm: TDlg_ErrorMessage_frm
  Left = 534
  Top = 150
  Caption = #44160#51613' '#47700#49464#51648
  ClientHeight = 424
  ClientWidth = 680
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Top = 387
    Width = 680
    Height = 37
    Align = alBottom
    DesignSize = (
      680
      37)
    object sButton1: TsButton
      Left = 585
      Top = 6
      Width = 87
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #54869#51064
      ModalResult = 1
      TabOrder = 0
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 17
      ContentMargin = 18
    end
    object sButton2: TsButton
      Left = 6
      Top = 6
      Width = 137
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #53364#47549#48372#46300#47196' '#48373#49324
      TabOrder = 1
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 24
    end
  end
  object sMemo1: TsMemo [1]
    Left = 0
    Top = 37
    Width = 680
    Height = 350
    Align = alClient
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Lines.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9'
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9'
      '0')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
    Text = '1'#13#10'2'#13#10'3'#13#10'4'#13#10'5'#13#10'6'#13#10'7'#13#10'8'#13#10'9'#13#10'0'#13#10'1'#13#10'2'#13#10'3'#13#10'4'#13#10'5'#13#10'6'#13#10'7'#13#10'8'#13#10'9'#13#10'0'#13#10
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
  end
  object sPanel2: TsPanel [2]
    Left = 0
    Top = 0
    Width = 680
    Height = 37
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object sLabel6: TsLabel
      Left = 8
      Top = 6
      Width = 73
      Height = 25
      Caption = 'APPPCR'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 408
    Top = 256
  end
end
