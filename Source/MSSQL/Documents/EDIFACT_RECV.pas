//==============================================================================

//==============================================================================

unit EDIFACT_RECV;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Dialogs,
  StdCtrls, ExtCtrls, FileCtrl, StrUtils;

type
  TSEGDATA = record
    SEGMENT: string;
    ID: string;
    Count: integer;
    Items: array[0..29] of string;
  end;

  TEDIFACT_RECV = class
  private
    FQUA: string;
    FFilePath: string;
    FFileData: TStringList;
    FSEGDATA: TSEGDATA;
    nLine: integer;
    FControlNo: string;
    FNextStep: Integer;
    function getHeader(index: integer): string;
    function getMigData: string;
    function getFileName: string;
    function getItems(index: integer): String;
  protected
  public
    function FindSegment(sSEG, sID: string; nSamLine: Integer = 4): Boolean;
    function NextSegment(StepLines: Integer; CompareSeg, CompareID: string; SegAcceptLineCount: Integer = 2): Boolean; overload;
    function NextSegment(CompareSeg, CompareID: string; SegAcceptLineCount: Integer = 2): Boolean; overload;
    function EdiDateFormat(Value: string; FormatQua: string = '101'): string;
    property SEGDATA: TSEGDATA read FSEGDATA;
    property SegItem[index: integer]: String  read getItems;
    property Header[index: integer]: string read getHeader;
    property MigData: string read getMigData;
    property FileName: string read getFileName;
    property ControlNo: string read FControlNo write FControlNo;
    procedure DefineList; virtual; abstract;
    function ParseData: Boolean; virtual; abstract;
  published
    constructor Create(UNHFile: string); overload;
    constructor CreateMig(sMigData: string); overload;
    destructor Destroy; override;
  end;

implementation

{ TEDIFACT_RECV }

constructor TEDIFACT_RECV.Create(UNHFile: string);
begin
  FFilePath := UNHFile;
  FFileData := TStringList.Create;

  if not FileExists(UNHFile) then
    raise Exception.Create(UNHFile + '을 찾을 수 없습니다');

  FFileData.LoadFromFile(UNHFile);
  nLine := 0;
end;

constructor TEDIFACT_RECV.CreateMig(sMigData: string);
begin
  FFileData := TStringList.Create;

  FFileData.Text := sMigData;
  if Trim(FFileData.Text) = '' then
    raise Exception.Create('MIG DATA가 없습니다');
  nLine := 0;
end;

destructor TEDIFACT_RECV.Destroy;
begin
  FFileData.Free;
  inherited;
end;

function TEDIFACT_RECV.EdiDateFormat(Value, FormatQua: string): string;
begin
  if FormatQua = '101' then
    Result := '20' + Value
  else if FormatQua = '202' then
    Result := '20' + Value;
end;

function TEDIFACT_RECV.FindSegment(sSEG, sID: string; nSamLine: Integer): Boolean;
var
  i, j: Integer;
  TMP_LINEDATA: string;
  TMP_SEG, TMP_CNT, TMP_ID: string;
begin
  Result := False;

  FNextStep := nSamLine;

  for i := nLine to FFileData.Count - 1 do
  begin
    TMP_LINEDATA := FFileData.Strings[i];
    TMP_SEG := Trim(LeftStr(TMP_LINEDATA, 4));
    TMP_CNT := Trim(MidStr(TMP_LINEDATA, 5, 7));
    TMP_ID := Trim(MidStr(TMP_LINEDATA, 12, 2));
    if (TMP_SEG = sSEG) and (TMP_ID = sID) then
    begin
      Result := True;
      nLine := i + 1;
      with SEGDATA do
      begin
        SEGMENT := sSEG;
        ID := sID;
        Count := StrToIntDef(TMP_CNT, -1);
//        nSamLine := nSamLine;

        for j := 0 to 29 do
          Items[j] := '';

        for j := 1 to nSamLine do
        begin
          Items[j - 1] := Trim(FFileData.Strings[i + j]);
        end;
        Break;
      end;
    end;
  end;

end;

function TEDIFACT_RECV.getFileName: string;
begin
  Result := ExtractFileName(FFilePath);
end;

function TEDIFACT_RECV.getHeader(index: integer): string;
var
  HeaderInfo: array[0..4] of string;
  TMP_HEADER: string;
begin
  TMP_HEADER := FFileData.Strings[0];

  HeaderInfo[0] := LeftStr(TMP_HEADER, 18);
  HeaderInfo[1] := MidStr(TMP_HEADER, 19, 18);
  HeaderInfo[2] := MidStr(TMP_HEADER, 37, 7);
  HeaderInfo[3] := MidStr(TMP_HEADER, 44, 4);
  HeaderInfo[4] := MidStr(TMP_HEADER, 48, 3);

  Result := Trim(HeaderInfo[index]);
end;

function TEDIFACT_RECV.getItems(index: integer): String;
begin
  Result := Trim(SEGDATA.items[index]);
end;

function TEDIFACT_RECV.getMigData: string;
begin
  Result := FFileData.Text;
end;

function TEDIFACT_RECV.NextSegment(StepLines: Integer; CompareSeg, CompareID: string; SegAcceptLineCount: Integer = 2): Boolean;
var
  TMP_LINEDATA: string;
  TMP_SEG, TMP_ID: string;
begin
//  Result := False;

  TMP_LINEDATA := FFileData.Strings[nLine + StepLines];
  TMP_SEG := Trim(LeftStr(TMP_LINEDATA, 4));
//  TMP_CNT := Trim(MidStr(TMP_LINEDATA,5,7));
  TMP_ID := Trim(MidStr(TMP_LINEDATA, 12, 2));

  Result := (TMP_SEG = CompareSeg) and (TMP_ID = CompareID);

  if Result then
  begin
    FindSegment(CompareSeg, CompareID, SegAcceptLineCount);
  end;
end;

function TEDIFACT_RECV.NextSegment(CompareSeg, CompareID: string; SegAcceptLineCount: Integer): Boolean;
begin
  Result := NextSegment(FNextStep, CompareSeg, CompareID, SegAcceptLineCount);
end;

end.

