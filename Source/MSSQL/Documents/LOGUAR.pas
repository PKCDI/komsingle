unit LOGUAR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TLOGUAR = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      LOGUARInsQuery  : TADOQuery;
      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  LOGUAR_DOC: TLOGUAR;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine;

{ TLOGUAR }

constructor TLOGUAR.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  LOGUARInsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TLOGUAR.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  LOGUARInsQuery.Free;
  DuplicateQuery.Free;
end;

function TLOGUAR.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('LOGUAR')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TLOGUAR.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TLOGUAR.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TLOGUAR.Run_Convert: Boolean;
var
  i : Integer;
  DocumentCount : integer;
  Qualifier : string;
  DocGubun : String;
  ILGubun : string;
begin
  Result := false;

    LOGUARInsQuery.Close;
    LOGUARInsQuery.Connection := DMMssql.KISConnect;
    LOGUARInsQuery.SQL.Text := SQL_LOGUAR;
    LOGUARInsQuery.Open;

    //APPEND
    try
      try
        LOGUARInsQuery.Append;
        //Default Values
        LOGUARInsQuery.FieldByName('User_Id').AsString := LoginData.sID;
        LOGUARInsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
        LOGUARInsQuery.FieldByName('CHK1').AsString := '';
        LOGUARInsQuery.FieldByName('CHK2').AsString := '';
        LOGUARInsQuery.FieldByName('CHK3').AsString := '';
        LOGUARInsQuery.FieldByName('PRNO').AsString := '0';

        //값 초기화
        DocumentCount := 1;

        //분석시작
        for i:= 1 to FMig.Count-1 do
        begin
          IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
            Continue
          else
          begin
            SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
            SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
            SEGMENT_NO := RightStr(FMig.Strings[i],2);
          end;

          IF SEGMENT('BGM','1','10') Then
          begin
            LOGUARInsQuery.FieldByName('MESSAGE1').AsString := getString(i+1);
            LOGUARInsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            LOGUARInsQuery.FieldByName('MESSAGE2').AsString := getString(i+3);
            LOGUARInsQuery.FieldByName('MESSAGE3').AsString := getString(i+4);
          end
          else if SEGMENT('NAD','','11') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'CA' then //선박회사명
            begin
              LOGUARInsQuery.FieldByName('CR_NAME1').AsString := getString(i+2);
              LOGUARInsQuery.FieldByName('CR_NAME2').AsString := getString(i+3);
              LOGUARInsQuery.FieldByName('CR_NAME3').AsString := getString(i+4);
            end;
            if Qualifier = 'SE' then //송하인
            begin
              LOGUARInsQuery.FieldByName('SE_NAME1').AsString := getString(i+2);
              LOGUARInsQuery.FieldByName('SE_NAME2').AsString := getString(i+3);
              LOGUARInsQuery.FieldByName('SE_NAME3').AsString := getString(i+4);
            end;
            if Qualifier = 'MS' then //신청인
            begin
              LOGUARInsQuery.FieldByName('MS_NAME1').AsString := getString(i+2);
              LOGUARInsQuery.FieldByName('MS_NAME2').AsString := getString(i+3);
              LOGUARInsQuery.FieldByName('MS_NAME3').AsString := getString(i+4);
            end;
            if Qualifier = 'AX' then //발급은행(승락권자(명의인))
            begin
              LOGUARInsQuery.FieldByName('AX_NAME1').AsString := getString(i+2);
              LOGUARInsQuery.FieldByName('AX_NAME2').AsString := getString(i+3);
              LOGUARInsQuery.FieldByName('AX_NAME3').AsString := getString(i+4);
            end;
            if Qualifier = 'CN' then //수하인
            begin
              LOGUARInsQuery.FieldByName('CN_NAME1').AsString := getString(i+2);
              LOGUARInsQuery.FieldByName('CN_NAME2').AsString := getString(i+3);
              LOGUARInsQuery.FieldByName('CN_NAME3').AsString := getString(i+4);
            end;
            if Qualifier = 'B5' then //인수예정자
            begin
              LOGUARInsQuery.FieldByName('B5_NAME1').AsString := getString(i+2);
              LOGUARInsQuery.FieldByName('B5_NAME2').AsString := getString(i+3);
              LOGUARInsQuery.FieldByName('B5_NAME3').AsString := getString(i+4);
            end;
          end
          else if SEGMENT('MOA','1','12') then //상업송장금액
          begin
            LOGUARInsQuery.FieldByName('INV_AMT').AsCurrency := StrToCurr(getString(i+2));
            LOGUARInsQuery.FieldByName('INV_AMTC').AsString := getString(i+3);
          end
          else if SEGMENT('RFF','','13') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AW' then  //수입화물선취보증(인도승락)서 번호
            begin
              LOGUARInsQuery.FieldByName('LG_NO').AsString := getString(i+2);
            end;
            if Qualifier = 'AAC' then  //신용장(계약서)번호
            begin
              LOGUARInsQuery.FieldByName('LC_G').AsString := getString(i+1);
              LOGUARInsQuery.FieldByName('LC_NO').AsString := getString(i+2);
            end;
            if Qualifier = 'BM' then  //선하증권번호
            begin
              LOGUARInsQuery.FieldByName('BL_G').AsString := getString(i+1);
              LOGUARInsQuery.FieldByName('BL_NO').AsString := getString(i+2);
            end;
            if Qualifier = 'DM' then  //업체신청번호
            begin
              LOGUARInsQuery.FieldByName('USERMAINT_NO').AsString := getString(i+2);
            end;
          end
          else if SEGMENT('TDT','1','14') then   //운송정보(선박명/기명 , 항해번호/비행편번호)
          begin
            LOGUARInsQuery.FieldByName('CARRIER1').AsString := getString(i+2); //선기명
            LOGUARInsQuery.FieldByName('CARRIER2').AsString := getString(i+3); //항해번호
          end
          else if SEGMENT('DTM','','15') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '132' then //도착예정일
            begin
              LOGUARInsQuery.FieldByName('AR_DATE').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '95' then //선하증권 발급일자
            begin
              LOGUARInsQuery.FieldByName('BL_DATE').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '55' then //수입화물선취보증서 발급일자
            begin
              LOGUARInsQuery.FieldByName('LG_DATE').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '182' then //발급일자
            begin
              LOGUARInsQuery.FieldByName('LC_DATE').AsString := '20' + getString(i+2);
            end;
          end
          else if SEGMENT('LOC','','16') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '9' then //  선적항
            begin
              LOGUARInsQuery.FieldByName('LOAD_LOC').AsString := getString(i+2);
              LOGUARInsQuery.FieldByName('LOAD_TXT').AsString := getString(i+5);
            end;
            if Qualifier = '8' then // 도착항
            begin
              LOGUARInsQuery.FieldByName('ARR_LOC').AsString := getString(i+2);
              LOGUARInsQuery.FieldByName('ARR_TXT').AsString := getString(i+5);
            end;
          end
          else if SEGMENT('PCI','1','17') then //화물표시 및 번호
          begin
            LOGUARInsQuery.FieldByName('SPMARK1').AsString := getString(i+2);
            LOGUARInsQuery.FieldByName('SPMARK2').AsString := getString(i+3);
            LOGUARInsQuery.FieldByName('SPMARK3').AsString := getString(i+4);
            LOGUARInsQuery.FieldByName('SPMARK4').AsString := getString(i+5);
            LOGUARInsQuery.FieldByName('SPMARK5').AsString := getString(i+6);
            LOGUARInsQuery.FieldByName('SPMARK6').AsString := getString(i+7);
            LOGUARInsQuery.FieldByName('SPMARK7').AsString := getString(i+8);
            LOGUARInsQuery.FieldByName('SPMARK8').AsString := getString(i+9);
            LOGUARInsQuery.FieldByName('SPMARK9').AsString := getString(i+10);
            LOGUARInsQuery.FieldByName('SPMARK10').AsString := getString(i+11);
          end
          else if SEGMENT('PAC','1','18') then //포장수
          begin
            LOGUARInsQuery.FieldByName('PAC_QTY').AsCurrency := StrToCurr(getString(i+1));
            LOGUARInsQuery.FieldByName('PAC_QTYC').AsString := getString(i+2);
          end
          else if SEGMENT('FTX','1','19') then //상품명세1~5
          begin
            LOGUARInsQuery.FieldByName('GOODS1').AsString := getString(i+2);
            LOGUARInsQuery.FieldByName('GOODS2').AsString := getString(i+3);
            LOGUARInsQuery.FieldByName('GOODS3').AsString := getString(i+4);
            LOGUARInsQuery.FieldByName('GOODS4').AsString := getString(i+5);
            LOGUARInsQuery.FieldByName('GOODS5').AsString := getString(i+6);
          end
          else if SEGMENT('FTX','2','19') then //상품명세6~10
          begin
            LOGUARInsQuery.FieldByName('GOODS6').AsString := getString(i+2);
            LOGUARInsQuery.FieldByName('GOODS7').AsString := getString(i+3);
            LOGUARInsQuery.FieldByName('GOODS8').AsString := getString(i+4);
            LOGUARInsQuery.FieldByName('GOODS9').AsString := getString(i+5);
            LOGUARInsQuery.FieldByName('GOODS10').AsString := getString(i+6);
          end
          else if SEGMENT('PAT','1','1A') then  //대금결제조건(결제기간)
          begin
            LOGUARInsQuery.FieldByName('TRM_PAYC').AsString := getString(i+2);
            LOGUARInsQuery.FieldByName('TRM_PAY').AsString := getString(i+3);
          end
          else if SEGMENT('FII','1','1B') then   //발급은행
          begin
            LOGUARInsQuery.FieldByName('BANK_CD').AsString := getString(i+2);
            LOGUARInsQuery.FieldByName('BANK_TXT').AsString := getString(i+5);
            LOGUARInsQuery.FieldByName('BANK_BR').AsString := getString(i+6);
          end; 
        end;

        LOGUARInsQuery.Post;
        Result := True;
      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
        end;
      end;
    finally
      LOGUARInsQuery.Close;
    end;
end;

procedure TLOGUAR.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'LOGUAR';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'LOGUAR';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TLOGUAR.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO);
end;

end.
