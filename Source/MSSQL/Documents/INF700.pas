unit INF700;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TINF700 = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      INF7001InsQuery  : TADOQuery;
      INF7002InsQuery  : TADOQuery;
      INF7003InsQuery  : TADOQuery;
      INF7004InsQuery  : TADOQuery;
      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  INF700_DOC: TINF700;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine;

{ TINFG700 }

constructor TINF700.Create;
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  INF7001InsQuery := TADOQuery.Create(nil);
  INF7002InsQuery := TADOQuery.Create(nil);
  INF7003InsQuery := TADOQuery.Create(nil);
  INF7004InsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TINF700.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  INF7001InsQuery.Free;
  INF7002InsQuery.Free;
  INF7003InsQuery.Free;
  INF7004InsQuery.Free;
  DuplicateQuery.Free;
end;

function TINF700.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('INF707')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TINF700.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TINF700.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TINF700.Run_Convert: Boolean;
var
  i : Integer;
  DocumentCount : integer;
  Qualifier : string;
  DocGubun : String;
  ILGubun : string;
begin
  Result := false;

    INF7001InsQuery.Close;
    INF7001InsQuery.Connection := DMMssql.KISConnect;
    INF7001InsQuery.SQL.Text := SQL_INF700_1;
    INF7001InsQuery.Open;

    INF7002InsQuery.Close;
    INF7002InsQuery.Connection := DMMssql.KISConnect;
    INF7002InsQuery.SQL.Text := SQL_INF700_2;
    INF7002InsQuery.Open;

    INF7003InsQuery.Close;
    INF7003InsQuery.Connection := DMMssql.KISConnect;
    INF7003InsQuery.SQL.Text := SQL_INF700_3;
    INF7003InsQuery.Open;

    INF7004InsQuery.Close;
    INF7004InsQuery.Connection := DMMssql.KISConnect;
    INF7004InsQuery.SQL.Text := SQL_INF700_4;
    INF7004InsQuery.Open;

    //APPEND
    try
      try

        INF7001InsQuery.Append;
        INF7002InsQuery.Append;
        INF7003InsQuery.Append;
        INF7004InsQuery.Append;

        //Default Values
        INF7001InsQuery.FieldByName('User_Id').AsString := LoginData.sID;
        INF7001InsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
        INF7001InsQuery.FieldByName('CHK1').AsString := '';
        INF7001InsQuery.FieldByName('CHK2').AsString := '';
        INF7001InsQuery.FieldByName('CHK3').AsString := '';

        INF7001InsQuery.FieldByName('IL_AMT1').AsCurrency := 0;
        INF7001InsQuery.FieldByName('IL_AMT2').AsCurrency := 0;
        INF7001InsQuery.FieldByName('IL_AMT3').AsCurrency := 0;
        INF7001InsQuery.FieldByName('IL_AMT4').AsCurrency := 0;
        INF7001InsQuery.FieldByName('IL_AMT5').AsCurrency := 0;

        INF7001InsQuery.FieldByName('PRNO').AsString := '0';

        INF7004InsQuery.FieldByName('INSTRCT').AsBoolean := FALSE;
        INF7003InsQuery.FieldByName('DESGOOD').AsBoolean := FALSE;

        //SHIPMENT BY
        INF7003InsQuery.FieldByName('ACD_2AA').AsBoolean := FALSE;
        //ACCEPTANCE COMMISSION DISCOUNT ...
        INF7003InsQuery.FieldByName('ACD_2AB').AsBoolean := FALSE;
        //ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER
        INF7003InsQuery.FieldByName('ACD_2AC').AsBoolean := FALSE;
        //LATE PRESENTATION B/L ACCEPTABLE
        INF7003InsQuery.FieldByName('ACD_2AD').AsBoolean := FALSE;
        //OTHER CONDITION(S)    ( if any )
        INF7003InsQuery.FieldByName('ACD_2AE').AsBoolean := FALSE;

        INF7003InsQuery.FieldByName('DOC_380').AsBoolean := FALSE;
        INF7003InsQuery.FieldByName('DOC_705').AsBoolean := FALSE;
        INF7003InsQuery.FieldByName('DOC_740').AsBoolean := FALSE;
        INF7004InsQuery.FieldByName('DOC_760').AsBoolean := FALSE;
        INF7003InsQuery.FieldByName('DOC_530').AsBoolean := FALSE;
        INF7003InsQuery.FieldByName('DOC_271').AsBoolean := FALSE;
        INF7003InsQuery.FieldByName('DOC_861').AsBoolean := FALSE;
        INF7003InsQuery.FieldByName('DOC_2AA').AsBoolean := FALSE;

        //값 초기화
        DocumentCount := 1;

        //분석시작
        for i:= 1 to FMig.Count-1 do
        begin
          IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
            Continue
          else
          begin
            SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
            SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
            SEGMENT_NO := RightStr(FMig.Strings[i],2);
          end;

          IF SEGMENT('BGM','1','10') Then
          begin
            INF7001InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            INF7002InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            INF7003InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            INF7004InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);

            INF7001InsQuery.FieldByName('MESSAGE1').AsString := getString(i+3);
            INF7001InsQuery.FieldByName('MESSAGE2').AsString := getString(i+4);
          end
          Else IF SEGMENT('BUS','1','11') then //신용장종류(Form of Documentary Credit)를 표시
          begin
            INF7001InsQuery.FieldByName('DOC_CD').AsString := getString(i+1);
          end
          else IF SEGMENT('INP','','12') then
          begin
             Qualifier := getString(i+2);
            //개설방법
            IF Qualifier = '5'  Then INF7001InsQuery.FieldByName('IN_MATHOD').AsString := getString(i+3);
            //확인지시문언(Confirmation Instructions)
            IF Qualifier = '4'  Then INF7003InsQuery.FieldByName('CONFIRMM').AsString := getString(i+3);
          end
          else IF SEGMENT('FCA','1','13') then
          begin
            //Charges (수수료부담자)
            INF7003InsQuery.FieldByName('CHARGE').AsString := getString(i+1);
          end
          else IF SEGMENT('RFF','','14') then
          begin
            Qualifier := getString(i+1);
            //Documentary credit number
            IF Qualifier = 'AAC'  Then INF7001InsQuery.FieldByName('CD_NO').AsString := getString(i+2);
            //Reference to Pre-Advice
            IF Qualifier = '2AF'  Then INF7001InsQuery.FieldByName('REF_PRE').AsString := getString(i+2);
          end
          ELSE if SEGMENT('DTM','','15') THEN
          begin
            Qualifier := getString(i+1);
            //신청일자
            if Qualifier = '2AA' then INF7001InsQuery.FieldByName('APP_DATE').AsString := '20' + getString(i+2);
            //개설일
            if Qualifier = '182' then INF7001InsQuery.FieldByName('ISS_DATE').AsString := '20' +getString(i+2);
            //Period for Presentation (서류제시기간)
            if Qualifier = '272' then INF7003InsQuery.FieldByName('PERIOD').AsString := getString(i+2);
          end
          else if SEGMENT('TSR','1','16') then
          begin
            //Transhipment(환적)
            INF7003InsQuery.FieldByName('TSHIP').AsString := getString(i+1);
            //Partial Shipments(분활선적)
            INF7003InsQuery.FieldByName('PSHIP').AsString := getString(i+2);
          end
          else if SEGMENT('PAI','1','17') then
          begin
            //신용공여
            INF7001InsQuery.FieldByName('AD_PAY').AsString := getString(i+1);
          end
          else if SEGMENT('PAT','1','18') then
          begin
            Qualifier := getString(i+1);
            //Drafts at …(화환어음조건)
            if Qualifier = '2AB' then INF7002InsQuery.FieldByName('DRAFT1').AsString := getString(i+3);
            //Mixed Payment Detail 혼합지급조건명세
            if Qualifier = '6' then INF7004InsQuery.FieldByName('MIX_PAY1').AsString := getString(i+3);
            //Deferred Payment Details 연지급조건명세
            if Qualifier = '4' then INF7004InsQuery.FieldByName('DEF_PAY1').AsString := getString(i+3);
          end
          else if SEGMENT('PAT','2','18') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AB' then INF7002InsQuery.FieldByName('DRAFT2').AsString := getString(i+3);
            if Qualifier = '6' then INF7004InsQuery.FieldByName('MIX_PAY2').AsString := getString(i+3);
            if Qualifier = '4' then INF7003InsQuery.FieldByName('DEF_PAY2').AsString := getString(i+3);
          end
          else if SEGMENT('PAT','3','18') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AB' then INF7002InsQuery.FieldByName('DRAFT3').AsString := getString(i+3);
            if Qualifier = '6' then INF7004InsQuery.FieldByName('MIX_PAY3').AsString := getString(i+3);
            if Qualifier = '4' then INF7003InsQuery.FieldByName('DEF_PAY3').AsString := getString(i+3);
          end
          else if SEGMENT('PAT','4','18') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '6' then INF7004InsQuery.FieldByName('MIX_PAY3').AsString := getString(i+3);
            if Qualifier = '4' then INF7003InsQuery.FieldByName('DEF_PAY3').AsString := getString(i+3);
          end
          else if SEGMENT('FTX','','19') then
          begin
            Qualifier := getString(i+1);
            //기타정보
            if Qualifier = 'ACB' then
            begin
              INF7001InsQuery.FieldByName('AD_INFO1').AsString := getString(i+2);
              INF7001InsQuery.FieldByName('AD_INFO2').AsString := getString(i+3);
              INF7001InsQuery.FieldByName('AD_INFO3').AsString := getString(i+4);
              INF7001InsQuery.FieldByName('AD_INFO4').AsString := getString(i+5);
              INF7001InsQuery.FieldByName('AD_INFO5').AsString := getString(i+6);
            end;
            // Applicable Rules
            if Qualifier = '3FC' then
            begin
              INF7004InsQuery.FieldByName('APPLICABLE_RULES_1').AsString := getString(i+2);
              INF7004InsQuery.FieldByName('APPLICABLE_RULES_2').AsString := getString(i+3);
            end;
            //Instructions to the ...
            if Qualifier = '2AB' then
            begin
              if INF7004InsQuery.FieldByName('INSTRCT_1').AsString = '' then
              begin
                INF7004InsQuery.FieldByName('INSTRCT').AsString := 'TRUE';
                INF7004InsQuery.FieldByName('INSTRCT_1').AsString := getString(i+2)+#13#10+
                                                                     getString(i+3)+#13#10+
                                                                     getString(i+4)+#13#10+
                                                                     getString(i+5)+#13#10+
                                                                     getString(i+6);

              end
              else
              begin
                INF7004InsQuery.FieldByName('INSTRCT_1').AsString := INF7004InsQuery.FieldByName('INSTRCT_1').AsString+#1310+
                                                                     getString(i+2)+#13#10+
                                                                     getString(i+3)+#13#10+
                                                                     getString(i+4)+#13#10+
                                                                     getString(i+5)+#13#10+
                                                                     getString(i+6);
              end;
            end;
            //Sender to Receive Information (수신은행 앞 정보)
            if Qualifier = '2AE' then
            begin
              INF7001InsQuery.FieldByName('SND_INFO1').AsString := getString(i+2);
              INF7001InsQuery.FieldByName('SND_INFO2').AsString := getString(i+3);
              INF7001InsQuery.FieldByName('SND_INFO3').AsString := getString(i+4);
              INF7001InsQuery.FieldByName('SND_INFO4').AsString := getString(i+5);
              INF7001InsQuery.FieldByName('SND_INFO5').AsString := getString(i+6);
            end;
          end
          else if SEGMENT('FII','','1A') then
          begin
            Qualifier := getString(i+1);
            //Applicant's bank (개설은행)
            if Qualifier = 'AW' then
            begin
              INF7001InsQuery.FieldByName('AP_BANK').AsString := getString(i+3);

              INF7001InsQuery.FieldByName('AP_BANK1').AsString := Trim(Copy(getString(i+7),1,35));
              INF7001InsQuery.FieldByName('AP_BANK2').AsString := Trim(Copy(getString(i+7),36,70));
              INF7001InsQuery.FieldByName('AP_BANK3').AsString := Trim(Copy(getString(i+8),1,35));
              INF7001InsQuery.FieldByName('AP_BANK4').AsString := Trim(Copy(getString(i+8),36,70));
            end;
            //희망통지은행
            if Qualifier = '2AA' then
            begin
              INF7001InsQuery.FieldByName('AD_BANK').AsString := getString(i+3);
              INF7001InsQuery.FieldByName('AD_BANK1').AsString := Trim(Copy(getString(i+7),1,35));
              INF7001InsQuery.FieldByName('AD_BANK2').AsString := Trim(Copy(getString(i+7),36,70));
              INF7001InsQuery.FieldByName('AD_BANK3').AsString := Trim(Copy(getString(i+8),1,35));
              INF7001InsQuery.FieldByName('AD_BANK4').AsString := Trim(Copy(getString(i+8),36,70));
            end;
            //개설의뢰인 은행 
            if Qualifier = '2AC' then
            begin
              INF7002InsQuery.FieldByName('APP_BANK1').AsString := Trim(Copy(getString(i+7),1,35));
              INF7002InsQuery.FieldByName('APP_BANK2').AsString := Trim(Copy(getString(i+7),36,70));
              INF7002InsQuery.FieldByName('APP_BANK3').AsString := Trim(Copy(getString(i+8),1,35));
              INF7002InsQuery.FieldByName('APP_BANK4').AsString := Trim(Copy(getString(i+8),36,70));
            end;
            //Available With ... By ...
            if Qualifier = 'DA' then
            begin
              INF7002InsQuery.FieldByName('AV_ACCNT').AsString := getString(i+2);
              INF7002InsQuery.FieldByName('AVAIL1').AsString := Trim(Copy(getString(i+7),1,35));
              INF7002InsQuery.FieldByName('AVAIL2').AsString := Trim(Copy(getString(i+7),36,70));
              INF7002InsQuery.FieldByName('AVAIL3').AsString := Trim(Copy(getString(i+8),1,35));
              INF7002InsQuery.FieldByName('AVAIL4').AsString := Trim(Copy(getString(i+8),36,70));
            end;
            //Drawee
            if Qualifier = 'DW' then
            begin
              INF7002InsQuery.FieldByName('DR_ACCNT').AsString := getString(i+2);
              INF7002InsQuery.FieldByName('DRAWEE').AsString := getString(i+3);
              INF7002InsQuery.FieldByName('DRAWEE1').AsString := Trim(Copy(getString(i+7),1,35));
              INF7002InsQuery.FieldByName('DRAWEE2').AsString := Trim(Copy(getString(i+7),36,70));
              INF7002InsQuery.FieldByName('DRAWEE3').AsString := Trim(Copy(getString(i+8),1,35));
              INF7002InsQuery.FieldByName('DRAWEE4').AsString := Trim(Copy(getString(i+8),36,70));
            end;
            //Reimbursement Bank
            if Qualifier = 'BD' then
            begin
              INF7004InsQuery.FieldByName('REI_ACNNT').AsString := getString(i+2);
              INF7004InsQuery.FieldByName('REI_BANK').AsString := getString(i+3);
              INF7004InsQuery.FieldByName('REI_BANK1').AsString := Trim(Copy(getString(i+7),1,35));
              INF7004InsQuery.FieldByName('REI_BANK2').AsString := Trim(Copy(getString(i+7),36,70));
              INF7004InsQuery.FieldByName('REI_BANK3').AsString := Trim(Copy(getString(i+8),1,35));
              INF7004InsQuery.FieldByName('REI_BANK4').AsString := Trim(Copy(getString(i+8),36,70));
            end;
            //"Advise Through" Bank
            if Qualifier = '2AB' then
            begin
              INF7004InsQuery.FieldByName('AVT_ACCNT').AsString := getString(i+2);
              INF7004InsQuery.FieldByName('AVT_BANK').AsString := getString(i+3);
              INF7004InsQuery.FieldByName('AVT_BANK1').AsString := Trim(Copy(getString(i+7),1,35));
              INF7004InsQuery.FieldByName('AVT_BANK2').AsString := Trim(Copy(getString(i+7),36,70));
              INF7004InsQuery.FieldByName('AVT_BANK3').AsString := Trim(Copy(getString(i+8),1,35));
              INF7004InsQuery.FieldByName('AVT_BANK4').AsString := Trim(Copy(getString(i+8),36,70));
            end;
          end
          //개설은행 전화번호를 표시 (Page2 Applicant Bank (APP_BANK)부분)
          else if SEGMENT('COM','1','1A') then
          begin
            INF7002InsQuery.FieldByName('APP_BANK5').AsString := getString(i+1);
          end
          else if SEGMENT('NAD','','1B') then
          begin
            Qualifier := getString(i+1);
            //PAGE2 - Applicant
            if Qualifier = 'DF' then
            begin
              INF7002InsQuery.FieldByName('APPLIC1').AsString := getString(i+2);
              INF7002InsQuery.FieldByName('APPLIC2').AsString := getString(i+3);
              INF7002InsQuery.FieldByName('APPLIC3').AsString := getString(i+4);
              INF7002InsQuery.FieldByName('APPLIC4').AsString := getString(i+5);
            end;
            //PAGE2 - Beneficiary
            if Qualifier = 'DG' then
            begin
              INF7002InsQuery.FieldByName('BENEFC1').AsString := getString(i+2);
              INF7002InsQuery.FieldByName('BENEFC2').AsString := getString(i+3);
              INF7002InsQuery.FieldByName('BENEFC3').AsString := getString(i+4);
              INF7002InsQuery.FieldByName('BENEFC4').AsString := getString(i+5);
              INF7002InsQuery.FieldByName('BENEFC5').AsString := getString(i+6);
            end;
            //PAGE1 - 신청업체
            if Qualifier = '2AE' then
            begin
              INF7004InsQuery.FieldByName('EX_NAME1').AsString := getString(i+7);
              INF7004InsQuery.FieldByName('EX_NAME2').AsString := getString(i+8);
              INF7004InsQuery.FieldByName('EX_NAME3').AsString := getString(i+9);
              INF7004InsQuery.FieldByName('EX_ADDR1').AsString := getString(i+10);
              INF7004InsQuery.FieldByName('EX_ADDR2').AsString := getString(i+11);
            end;
            //PAGE4 - 개설은행
            if Qualifier = '2AF' then
            begin
              INF7004InsQuery.FieldByName('OP_BANK1').AsString := getString(i+7);
              INF7004InsQuery.FieldByName('OP_BANK2').AsString := getString(i+8);
              INF7004InsQuery.FieldByName('OP_BANK3').AsString := getString(i+9);
              INF7004InsQuery.FieldByName('OP_ADDR1').AsString := getString(i+10);
              INF7004InsQuery.FieldByName('OP_ADDR2').AsString := getString(i+11);
            end;
          end
          // PAGE2 - APPLICANT의 tel.
          else if SEGMENT('COM','1','1B') then
          begin
            INF7002InsQuery.FieldByName('APPLIC5').AsString := getString(i+1);
          end
          //PAGE2 - Date and Place of Expiry
          else if SEGMENT('DTM','1','1C') then
          begin
            INF7001InsQuery.FieldByName('EX_DATE').AsString := '20' + getString(i+2);
          end
          //PAGE2 - Date and Place of Expiry
          else if SEGMENT('LOC','1','1C') then
          begin
            INF7001InsQuery.FieldByName('EX_PLACE').AsString := getString(i+2);
          end
          //Currency Code,Amount
          else if SEGMENT('MOA','1','1D') then
          begin
            INF7002InsQuery.FieldByName('CD_AMT').AsCurrency := StrToCurr(getString(i+2));
            INF7002InsQuery.FieldByName('CD_CUR').AsString := getString(i+3);
          end
          //Page3 - Additional Amounts Covered
          else if SEGMENT('FTX','1','1D') then
          begin
            INF7002InsQuery.FieldByName('AA_CV1').AsString := getString(i+2);
            INF7002InsQuery.FieldByName('AA_CV2').AsString := getString(i+3);
            INF7002InsQuery.FieldByName('AA_CV3').AsString := getString(i+4);
            INF7002InsQuery.FieldByName('AA_CV4').AsString := getString(i+5);
          end
          //Maximum Credit Amount (수신시 저장되는 것이 없음)
          else if SEGMENT('ALC','1','20') then
          begin

          end
          else if SEGMENT('PCD','1','20') then
          begin
            INF7002InsQuery.FieldByName('CD_PERP').AsString := LeftStr(getString(i+2), Pos('.',getString(i+2))-1);
            INF7002InsQuery.FieldByName('CD_PERM').AsString := MidStr(getString(i+2),Pos('.',getString(i+2))+1,3);
          end
          else if SEGMENT('LOC','','1E') then
          begin
            Qualifier := getString(i+1);
            //수탁발송지
            if Qualifier = '149' then INF7003InsQuery.FieldByName('LOAD_ON').AsString := getString(i+2);
            //최종목적지
            if Qualifier = '148' then INF7003InsQuery.FieldByName('FOR_TRAN').AsString := getString(i+2);
            //선적항
            if Qualifier = '76' then INF7004InsQuery.FieldByName('SUNJUCK_PORT').AsString := getString(i+2);
            //도착항
            if Qualifier = '12' then INF7004InsQuery.FieldByName('DOCHACK_PORT').AsString := getString(i+2);
          end
          //PAGE5 - Latest Date of Shipment
          else if SEGMENT('DTM','1','1E') then
          begin
            INF7003InsQuery.FieldByName('LST_DATE').AsString := '20' + getString(i+2);
          end
          //선적기간(Shipment Period)을 표시
          else if SEGMENT('FTX','1','21') then
          begin
            INF7003InsQuery.FieldByName('SHIP_PD1').AsString := getString(i+2);
            INF7003InsQuery.FieldByName('SHIP_PD2').AsString := getString(i+3);
            INF7003InsQuery.FieldByName('SHIP_PD3').AsString := getString(i+4);
          end
          else if SEGMENT('FTX','2','21') then
          begin
            INF7003InsQuery.FieldByName('SHIP_PD4').AsString := getString(i+2);
            INF7003InsQuery.FieldByName('SHIP_PD5').AsString := getString(i+3);
            INF7003InsQuery.FieldByName('SHIP_PD6').AsString := getString(i+4);
          end
          else if SEGMENT('TOD','1','1F') then
          begin
            INF7003InsQuery.FieldByName('TERM_PR').AsString := getString(i+1);
            INF7003InsQuery.FieldByName('TERM_PR_M').AsString := getString(i+2);
          end
          else if SEGMENT('LOC','','22') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '1' then
              INF7003InsQuery.FieldByName('PL_TERM').AsString := getString(i+5);
            if Qualifier = '27' then
            begin
              INF7003InsQuery.FieldByName('ORIGIN').AsString := getString(i+2);
              INF7003InsQuery.FieldByName('ORIGIN_M').AsString := getString(i+5);
            end;
          end
          // Description of Goods and/or Services
          else if SEGMENT('FTX','','23') then
          begin
            if (getString(i+1)) = 'AAA' then
            begin
              if INF7003InsQuery.FieldByName('DESGOOD_1').AsString = '' then
              begin
                INF7003InsQuery.FieldByName('DESGOOD').AsString := 'TRUE';
                INF7003InsQuery.FieldByName('DESGOOD_1').AsString := Trim(Copy(getString(i+2),2,65))+#13#10+
                                                                     Trim(Copy(getString(i+3),2,65))+#13#10+
                                                                     Trim(Copy(getString(i+4),2,65))+#13#10+
                                                                     Trim(Copy(getString(i+5),2,65))+#13#10+
                                                                     Trim(Copy(getString(i+6),2,65));

              end
              else
              begin
                INF7003InsQuery.FieldByName('DESGOOD_1').AsString := INF7003InsQuery.FieldByName('DESGOOD_1').AsString+#1310+
                                                                     Trim(Copy(getString(i+2),2,65))+#13#10+
                                                                     Trim(Copy(getString(i+3),2,65))+#13#10+
                                                                     Trim(Copy(getString(i+4),2,65))+#13#10+
                                                                     Trim(Copy(getString(i+5),2,65))+#13#10+
                                                                     Trim(Copy(getString(i+6),2,65));
              end;
            end;
          end
          else if SEGMENT('ALI','','1G') then
          begin
            Qualifier := getString(i+1);
            //SHIPMENT BY
            if Qualifier = '2AA' THEN INF7003InsQuery.FieldByName('ACD_2AA').AsBoolean := TRUE;
            //ACCEPTANCE COMMISSION DISCOUNT ...
            if Qualifier = '2AB' THEN INF7003InsQuery.FieldByName('ACD_2AB').AsBoolean := TRUE;
            //ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER
            if Qualifier = '2AC' THEN INF7003InsQuery.FieldByName('ACD_2AC').AsBoolean := TRUE;
            //LATE PRESENTATION B/L ACCEPTABLE
            if Qualifier = '2AD' THEN INF7003InsQuery.FieldByName('ACD_2AD').AsBoolean := TRUE;
            //OTHER CONDITION(S)    ( if any )
            if Qualifier = '2AE' THEN INF7003InsQuery.FieldByName('ACD_2AE').AsBoolean := TRUE;
          end
          else if SEGMENT('NAD','1','1G') then
          begin
            // SHIPMENT BY(SEGMENT('ALI','','1G'))가 2AA일경우
            INF7003InsQuery.FieldByName('ACD_2AA_1').AsString := getString(i+2);
          end
          else if SEGMENT('FTX','','24') then
          begin
            //OTHER CONDITION(S)    ( if any ) -> memo필드
            if (getString(i+1)) = 'ABS' then
            begin
              if INF7003InsQuery.FieldByName('ACD_2AE_1').AsString = '' then
              begin
                INF7003InsQuery.FieldByName('ACD_2AE_1').AsString := getString(i+2)+#13#10+
                                                                     getString(i+3)+#13#10+
                                                                     getString(i+4)+#13#10+
                                                                     getString(i+5)+#13#10+
                                                                     getString(i+6);

              end
              else
              begin
                INF7003InsQuery.FieldByName('ACD_2AE_1').AsString := INF7003InsQuery.FieldByName('ACD_2AE_1').AsString+#1310+
                                                                     getString(i+2)+#13#10+
                                                                     getString(i+3)+#13#10+
                                                                     getString(i+4)+#13#10+
                                                                     getString(i+5)+#13#10+
                                                                     getString(i+6);
              end;
            end;
          end
          //Documents Required
          else if  SEGMENT('DOC','','1H') then
          begin
            DocGubun := getString(i+1);
            if DocGubun = '380' then
            begin
              INF7003InsQuery.FieldByName('DOC_380').AsBoolean := TRUE;
              INF7003InsQuery.FieldByName('DOC_380_1').AsString := getString(i+2);
            end;
            if (DocGubun = '705') or (DocGubun = '706') or (DocGubun = '717') or (DocGubun = '718') or (DocGubun = '707') then
            begin
              INF7003InsQuery.FieldByName('DOC_705').AsBoolean := TRUE;
              INF7003InsQuery.FieldByName('DOC_705_GUBUN').AsString := DocGubun;
            end;
            if DocGubun = '740' then INF7003InsQuery.FieldByName('DOC_740').AsBoolean := TRUE;
            if DocGubun = '760' then INF7004InsQuery.FieldByName('DOC_760').AsBoolean := TRUE;
            if DocGubun = '530' then INF7003InsQuery.FieldByName('DOC_530').AsBoolean := TRUE;
            if DocGubun = '271' then
            begin
              INF7003InsQuery.FieldByName('DOC_271').AsBoolean := TRUE;
              INF7003InsQuery.FieldByName('DOC_271_1').AsString := getString(i+2);
            end;
            if DocGubun = '861' then INF7003InsQuery.FieldByName('DOC_861').AsBoolean := TRUE;
            if DocGubun = '2AA' then INF7003InsQuery.FieldByName('DOC_2AA').AsBoolean := TRUE;
          end
          else if SEGMENT('FTX','','25') then
          begin
             Qualifier := getString(i+1);
             if Qualifier = 'ABX' then
             begin
                if INF7003InsQuery.FieldByName('DOC_2AA_1').AsString = '' then
                begin
                  INF7003InsQuery.FieldByName('DOC_2AA_1').AsString := getString(i+2)+#13#10+
                                                                       getString(i+3)+#13#10+
                                                                       getString(i+4)+#13#10+
                                                                       getString(i+5)+#13#10+
                                                                       getString(i+6);

                end
                else
                begin
                  INF7003InsQuery.FieldByName('DOC_2AA_1').AsString := INF7003InsQuery.FieldByName('DOC_2AA_1').AsString+#1310+
                                                                       getString(i+2)+#13#10+
                                                                       getString(i+3)+#13#10+
                                                                       getString(i+4)+#13#10+
                                                                       getString(i+5)+#13#10+
                                                                       getString(i+6);
                end;
             end;
             if Qualifier = 'INS' then
             begin
              INF7003InsQuery.FieldByName('DOC_530_1').AsString := getString(i+2);
              INF7003InsQuery.FieldByName('DOC_530_2').AsString := getString(i+3);
             end;

          end
          else if SEGMENT('ALI','1','26') then
          begin
            if (DocGubun = '705') or (DocGubun = '706') or (DocGubun = '717') or (DocGubun = '718') or (DocGubun = '707') then
            begin
              INF7003InsQuery.FieldByName('DOC_705_3').AsString := getString(i+1);
            end;
            if DocGubun = '740' then INF7003InsQuery.FieldByName('DOC_740_3').AsString := getString(i+1);
            if DocGubun = '760' then INF7003InsQuery.FieldByName('DOC_760_3').AsString := getString(i+1);
          end
          else if SEGMENT('NAD','','30') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'CN' then
            begin
              if DocGubun = '705' then
              begin
                INF7003InsQuery.FieldByName('DOC_705_1').AsString := getString(i+2);
                INF7003InsQuery.FieldByName('DOC_705_2').AsString := getString(i+3);
              end
              else if DocGubun = '740' then
              begin
                INF7003InsQuery.FieldByName('DOC_740_1').AsString := getString(i+2);
                INF7003InsQuery.FieldByName('DOC_740_2').AsString := getString(i+3);
              end
              else if DocGubun = '760' then
              begin
                INF7004InsQuery.FieldByName('DOC_760_1').AsString := getString(i+2);
                INF7004InsQuery.FieldByName('DOC_760_2').AsString := getString(i+3);
              end;
            end;
            //NOTIFY 부분
            if Qualifier = 'NI' then
            begin
              if DocGubun = '705' then
              begin
                INF7003InsQuery.FieldByName('DOC_705_4').AsString := getString(i+2);
              end
              else if DocGubun = '740' then
              begin
                INF7003InsQuery.FieldByName('DOC_740_4').AsString := getString(i+2);
              end
              else if DocGubun = '760' then
              begin
                INF7004InsQuery.FieldByName('DOC_760_4').AsString := getString(i+2);
              end;
            end;
          end
          else if SEGMENT('RFF','','1I') then
          begin
            ILGubun := SEGMENT_COUNT;
            if ILGubun = '1' then
            begin
              INF7001InsQuery.FieldByName('IMP_CD1').AsString:= getString(i+1);
              INF7001InsQuery.FieldByName('IL_NO1').AsString:= getString(i+2);
            end
            else if ILGubun = '2' then
            begin
              INF7001InsQuery.FieldByName('IMP_CD2').AsString:= getString(i+1);
              INF7001InsQuery.FieldByName('IL_NO2').AsString:= getString(i+2);
            end
            else if ILGubun = '3' then
            begin
              INF7001InsQuery.FieldByName('IMP_CD3').AsString:= getString(i+1);
              INF7001InsQuery.FieldByName('IL_NO3').AsString:= getString(i+2);
            end
            else if ILGubun = '4' then
            begin
              INF7001InsQuery.FieldByName('IMP_CD4').AsString:= getString(i+1);
              INF7001InsQuery.FieldByName('IL_NO4').AsString:= getString(i+2);
            end
            else if ILGubun = '5' then
            begin
              INF7001InsQuery.FieldByName('IMP_CD5').AsString:= getString(i+1);
              INF7001InsQuery.FieldByName('IL_NO5').AsString:= getString(i+2);
            end;
          end
          else if SEGMENT('MOA','','1I') then
          begin
            if ILGubun = '1' then
            begin
              INF7001InsQuery.FieldByName('IL_AMT1').AsCurrency:=  StrToCurr(getString(i+2));
              INF7001InsQuery.FieldByName('IL_CUR1').AsString:= getString(i+3);
            end
            else if ILGubun = '2' then
            begin
              INF7001InsQuery.FieldByName('IL_AMT2').AsCurrency:=  StrToCurr(getString(i+2));
              INF7001InsQuery.FieldByName('IL_CUR2').AsString:= getString(i+3);
            end
            else if ILGubun = '3' then
            begin
              INF7001InsQuery.FieldByName('IL_AMT3').AsCurrency:=  StrToCurr(getString(i+2));
              INF7001InsQuery.FieldByName('IL_CUR3').AsString:= getString(i+3);
            end
            else if ILGubun = '4' then
            begin
              INF7001InsQuery.FieldByName('IL_AMT4').AsCurrency:=  StrToCurr(getString(i+2));
              INF7001InsQuery.FieldByName('IL_CUR4').AsString:= getString(i+3);
            end
            else if ILGubun = '5' then
            begin
              INF7001InsQuery.FieldByName('IL_AMT5').AsCurrency:=  StrToCurr(getString(i+2));
              INF7001InsQuery.FieldByName('IL_CUR5').AsString:= getString(i+3);
            end;
          end;
            
        end;

        INF7001InsQuery.Post;
        INF7002InsQuery.Post;
        INF7003InsQuery.Post;
        INF7004InsQuery.Post;

        Result := True;
      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
        end;
      end;
    finally
      INF7001InsQuery.Close;
      INF7002InsQuery.Close;
      INF7003InsQuery.Close;
      INF7004InsQuery.Close;
    end;

end;

procedure TINF700.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'INF700';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'INF700';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TINF700.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO);
end;

end.
