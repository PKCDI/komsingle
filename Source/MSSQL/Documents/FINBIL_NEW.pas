unit FINBIL_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils, RecvParent;

type
  TFINBIL_NEW = class(TRecvParent)
    protected
      KEY_MAINT_NO : String;
    public
      constructor Create(Mig: TStringList);
      function Run_Convert: Boolean; override;
      procedure Run_Ready(bDuplicate: Boolean); override;
      procedure TEST;
  end;
var
  FINBIL_NEW_DOC : TFINBIL_NEW;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine;

{ TFINBIL_NEW }

constructor TFINBIL_NEW.Create(Mig: TStringList);
begin
  inherited;
  FDOCID := 'FINBIL';
end;

function TFINBIL_NEW.Run_Convert: Boolean;
var
  i,k, SegIDX : integer;
  SelectFieldNo : String;
  DocCount : integer;
begin
  Result := False;

  with InsertQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;

    DMMssql.BeginTrans;

    try
      try
//------------------------------------------------------------------------------
// 공통사항
//------------------------------------------------------------------------------
        SQL.Text := SQL_FINBIL_MASTER;
        Open;

        InsertQuery.Append;

        FieldByName('USER_ID').AsString := LoginData.sID;
        FieldByName('CHK1').AsString := '';
        FieldByName('CHK2').AsString := '';
        FieldByName('CHK3').AsString := '';
        FieldByName('DATEE').AsString := FormatDateTime('YYYYMMDD',Now);

        //문서의 시작
        SegIDX := GetSegmentIDX('BGM','1','10');
        KEY_MAINT_NO := getString(SegIDX+2);

        DocCount := AlreadyDocumentCount('FINBIL_H','MAINT_NO',KEY_MAINT_NO);

        IF DocCount > 0 Then
        begin
          KEY_MAINT_NO := KEY_MAINT_NO+'_'+FormatFloat('000',DocCount);
        end;

        FieldByName('MAINT_NO').AsString := KEY_MAINT_NO;

        FMAINT_NO := KEY_MAINT_NO;

        FieldByName('MESSAGE1').AsString := getString(SegIDX+3);
        IF Trim(getString(SegIDX+4)) = '' Then
          FieldByName('MESSAGE2').AsString := 'NA'
        else
          FieldByName('MESSAGE2').AsString := getString(SegIDX+4);

        //계산서용도
        SegIDX := GetSegmentIDX('BUS','1','11');
        FieldByName('BUS').AsString := getString(SegIDX+2);
        FieldByName('BUS_DESC').AsString := getString(SegIDX+3);

        //문서번호
        for i := 1 to 4 do
        begin
          SegIDX := GetSegmentIDX('RFF',IntToStr(i),'12');
          IF SegIDX = -1 THEN Continue;
          Qualifier := getString(SegIDX+1);

          case AnsiIndexText(Qualifier,['AAC','DM','ACK','ACD']) of
            0: FieldByName('DOC_NO1').AsString := getString(SegIDX+2);
            1: FieldByName('DOC_NO2').AsString := getString(SegIDX+2);
            2: FieldByName('DOC_NO3').AsString := getString(SegIDX+2);
            3: FieldByName('DOC_NO4').AsString := getString(SegIDX+2);
          end;
        end;

        //거래일자 통지일자
        for i := 1 to 2 do
        begin
          SegIDX := GetSegmentIDX('DTM',IntToStr(i),'13');
          IF SegIDX = -1 THEN Continue;
          Qualifier := getString(SegIDX+1);

          Case AnsiIndexText(Qualifier,['97','137']) of
            //거래일자
            0: FieldByName('TRN_DATE').AsString := '20'+getString(SegIDX+2);
            //통지일자
            1: FieldByName('ADV_DATE').AsString := '20'+getString(SegIDX+2);
          end;
        end;

        //결제방법 어음조건
        SegIDX := GetSegmentIDX('PAT','1','14');
        IF SegIDX > -1 THEN
        BEGIN
          //결제방법
          FieldByName('TERM').AsString := getString(SegIDX+2);
          //어음조건
          FieldByName('PAYMENT').AsString := getString(SegIDX+3);
        END;

        //하자여부 재매입여부
        SegIDX := GetSegmentIDX('ALI','1','15');
        IF SegIDX > -1 THEN
        BEGIN
          //하자여부
          FieldByName('SPECIAL1').AsString := getString(SegIDX+1);
          //재매입여부
          FieldByName('SPECIAL2').AsString := getString(SegIDX+2);
        END;

        //기타정보
        SegIDX := GetSegmentIDX('FTX','1','16');
        IF SegIDX > -1 THEN
        BEGIN
          FieldByName('REMARK1').AsString := getString(SegIDX+2)+#13#10+
                                             getString(SegIDX+3)+#13#10+
                                             getString(SegIDX+4)+#13#10+
                                             getString(SegIDX+5)+#13#10+
                                             getString(SegIDX+6);
        END;

        //발급은행
        SegIDX := GetSegmentIDX('FII','1','17');
        FieldByName('BANK'    ).AsString := getString(SegIDX+2);
        FieldByName('BNKNAME1').AsString := getString(SegIDX+5);
        FieldByName('BNKNAME2').AsString := getString(SegIDX+6);

        //당사자유형 (거래고객)
        for i := 1 to 2 do
        begin
          SegIDX := GetSegmentIDX('NAD',IntToStr(i),'18');
          IF SegIDX = -1 THEN Continue; 
          Qualifier := getString(SegIDX+1);
          //거래고객
          IF Qualifier = 'MR' Then
          begin
            FieldByName('CUSTNAME1').AsString := getString(SegIDX+2);
            FieldByName('CUSTNAME2').AsString := getString(SegIDX+3);
            FieldByName('CUSTNAME3').AsString := '';
          end
          else
          //발신기관
          IF Qualifier = 'AX' then
          begin
            FieldByName('CUSTSIGN1').AsString := getString(SegIDX+2);
            FieldByName('CUSTSIGN2').AsString := getString(SegIDX+3);
            FieldByName('CUSTSIGN3').AsString := getString(SegIDX+4);
          end;
        end;

        //금액유형
        for i := GetSegmentIDX('PAI','1','19') to GetSegmentIDX('FCA','1','1A')-1 do
        begin
          IF not AnsiMatchText( Trim(LeftStr(getString(i),3)) , ['PAI','PCD','MOA','CUX'] ) THen
            Continue
          else
          begin
            SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
            SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
            SEGMENT_NO := RightStr(FMig.Strings[i],2);
          end;

          IF SEGMENT('PAI','','19') Then
          begin
            Qualifier := getString(i+1);
            Case AnsiIndexText(Qualifier,['2AC','2AD','2AE','2AF','2AG']) of
              0: SelectFieldNo := '1';
              1: SelectFieldNo := '2';
              2: SelectFieldNo := '3';
              3: SelectFieldNo := '4';
              4: SelectFieldNo := '5';
            end;
          end;

          IF SEGMENT('PCD','','') THEN
          begin
            Qualifier := getString(i+1);
            //FOB환산율
            IF Qualifier = '2AA' Then
              FieldByName('FOBRATE').AsString := getString(i+2);
            //보증금적립율
            IF Qualifier = '2AB' Then
              FieldByName('RATEIN').AsString := getString(i+2);
          end;

          IF SEGMENT('MOA','','') THEN
          begin
            Qualifier := getString(i+1);
            //외화금액
            IF Qualifier = '2AD' Then
            begin
              FieldByName('PAYORD'+SelectFieldNo+'1').AsString := getString(i+2);
              FieldByName('PAYORD'+SelectFieldNo+'C').AsString := getString(i+3);
            end;
            //원화금액
            IF Qualifier = '2AE' Then
              FieldByName('PAYORD'+SelectFieldNo+'1').AsString := getString(i+2);
          end;

          IF SEGMENT('CUX','','') Then
          //적용환율
          FieldByName('RATE'+SelectFieldNo).AsString := getString(i+1);
        end;

        //최종지급액, 수수료합계
        for i := 1 to 2 do
        begin
          SegIDX := GetSegmentIDX('MOA',IntToStr(i),'23');
          IF SegIDX = -1 THEN Continue;
          Qualifier := getString(SegIDX+1);
          IF Qualifier = '60' Then
          begin
            FieldByName('FINAMT1').AsString  := getString(SegIDX+2);
            FieldByName('FINAMT1C').AsString := getString(SegIDX+3);
          end;
          IF Qualifier = '131' Then
          begin
            FieldByName('FINAMT2').AsString  := getString(SegIDX+2);
            FieldByName('FINAMT2C').AsString := getString(SegIDX+3);
          end;
        end;

        InsertQuery.Post;

//------------------------------------------------------------------------------
// 수수료사항
//------------------------------------------------------------------------------
        InsertQuery.Close;
        InsertQuery.SQL.Text := SQL_FINBIL_DETAIL;
        InsertQuery.Open;

        IF GetSegmentIDX('ALC','1','24') > -1 Then
        begin
          for i := GetSegmentIDX('ALC','1','24') to FMig.Count -1 do
          begin
            //기본값 설정

            IF AnsiMatchText( Trim(LeftStr(getString(i),3)) , ['','UNT'] ) THen
              Continue
            else
            begin
              SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
              SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
              SEGMENT_NO := RightStr(FMig.Strings[i],2);
            end;

            IF SEGMENT('ALC','','24') then
            begin
              IF SEGMENT_COUNT <> '1' Then InsertQuery.Post;

              InsertQuery.Append;

              FieldByName('KEYY').AsString := FMAINT_NO;
              FieldByName('SEQ').AsString := SEGMENT_COUNT;
              FieldByName('CHRATE').AsString := '0';
              FieldByName('KRWRATE').AsString := '0';
              FieldByName('CHDAY').AsString := '0';

              //수수료유형
              FieldByName('CHCODE').AsString := getString(i+3);
              //계산서번호
              FieldByName('BILLNO').AsString := getString(i+2);
            end;

            IF SEGMENT('PCD','1','30') Then
            begin
              Qualifier := getString(i+1);
              IF Qualifier = '2' Then
                FieldByName('CHRATE').AsString := getString(i+2);
            end;

            IF SEGMENT('MOA','','31') THEN
            begin
              Qualifier := getString(i+1);
              //대상금액
              IF Qualifier = '25' Then
              begin
                FieldByName('AMT1').AsString  := getString(i+2);
                FieldByName('AMT1C').AsString := getString(i+3);
              end;
              //산출금액
              IF Qualifier = '23' Then
              begin
                FieldByName('AMT2').AsString  := getString(i+2);
                FieldByName('AMT2C').AsString := 'KRW';
              end;
            end;

            //적용환율
            IF SEGMENT('CUX','1','32') then
            begin
              FieldByName('KRWRATE').AsString := getString(i+1);
            end;

            IF SEGMENT('DTM','','33') Then
            begin
              Qualifier := getString(i+1);
              //적용일수
              IF Qualifier = '221' THen
                FieldByName('CHDAY').AsString := getString(i+2);
              //적용기간
              IF Qualifier = '257' THen
              begin
                FieldByName('CHDATE1').AsString := '20'+LeftStr(getString(i+2),6);
                FieldByName('CHDATE2').AsString := '20'+RightStr(getString(i+2),6);
              end;
            end;
          end;

          IF InsertQuery.State in [dsInsert] Then
            InsertQuery.Post;
          IF InsertQuery.Active Then
          InsertQuery.Close;
        end;
        Result := True;
        DMMssql.CommitTrans;
      except
        on E:Exception do
        begin
          DMMssql.RollbackTrans;
          ShowMessage(E.Message);
        end;
      end;
    finally
      InsertQuery.Close;
    end;
  end;
end;

procedure TFINBIL_NEW.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
  SegIDX : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];

    Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
    Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
    Parameters.ParamByName('DOCID').Value := 'FINBIL';
    Parameters.ParamByName('Mig').Value := FMig.Text;
    Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
    Parameters.ParamByName('CONTROLNO').Value := '';
    Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
    Parameters.ParamByName('CON').Value := '';
    Parameters.ParamByName('DBAPPLY').Value := '';
    Parameters.ParamByName('DOCNAME').Value := 'FINBIL';

    SegIDX := GetSegmentIDX('BGM','1','10');
    IF SegIDX > -1 THEN
    BEGIN
      //관리번호 중복 확인
      IF DuplicateData(Trim(FMig.Strings[SegIDX+2])) AND bDuplicate Then
      begin
        FDuplicateDoc := Trim(FMig.Strings[SegIDX+2]);
        FDuplicate := True;
        Exit;
      end
      else
      begin
        FDuplicateDoc := '';
        FDuplicate := false;
        Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[SegIDX+2]);
      end;
    END;


    ExecSQL;
  end;
end;

procedure TFINBIL_NEW.TEST;
begin
//  ShowMessage(IntToStr(GetSegmentIDX('MOA','1','23')));
end;

end.
