unit PCRLIC_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils, RecvParent;

type
  TPCRLIC_NEW = class(TRecvParent)
    protected
      KEY_LAN : String;
    public
      constructor Create(Mig: TStringList);
      procedure Run_Ready(bDuplicate :Boolean); override;
      function Run_Convert:Boolean; override;
      procedure TEST;
  end;
var
  PCRLIC_NEW_DOC : TPCRLIC_NEW;
implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine;

{ TPCRLIC_NEW }

constructor TPCRLIC_NEW.Create(Mig: TStringList);
begin
  inherited;
  FDOCID := 'PCRLIC';
end;

const
  ITM_GBN_CODE : array [0..9] of string = ('2AA','2AB','2AC','2AD','2AE','2AF','2AJ','2AK','2AL','2AM');
function TPCRLIC_NEW.Run_Convert: Boolean;
var
  i : Integer;
//  DocumentCount : integer;
  SegIDX : Integer;
  KEY_IDX : Integer;
  DocCount : Integer;
begin
  Result := False;

  with InsertQuery do
  begin
//------------------------------------------------------------------------------
// PLCLIC_H
//------------------------------------------------------------------------------
    Close;
    Connection := DMMssql.KISConnect;
    try
      try
      SQL.Text := SQL_PCRLIC_MASTER;
      Open;

      InsertQuery.Append;

      FieldByName('USER_ID').AsString := LoginData.sID;
      FieldByName('CHK1').AsString := '';
      FieldByName('CHK2').AsString := '';
      FieldByName('CHK3').AsString := '';
      FieldByName('DATEE').AsString := FormatDateTime('YYYYMMDD',Now);
      //기본값이 NULL로 들어가면 안되는 필드들
      FieldByName('C1_AMT').AsCurrency := 0;
      FieldByName('C2_AMT').AsCurrency := 0;
      FieldByName('AC1_AMT').AsCurrency := 0;
      FieldByName('AC2_AMT').AsCurrency := 0;
      FieldByName('PRNO').AsCurrency := 0;
      FieldByName('BAMT').AsCurrency := 0;
      FieldByName('BAMT2').AsCurrency := 0;
      FieldByName('BAMT3').AsCurrency := 0;

      for i := 1 to 2 do
      begin
        SegIDX := GetSegmentIDX('RFF',IntToStr(i),'1A');
        Qualifier := getString(SegIDX+1);
        IF Qualifier = 'DM' Then
        begin
          //키
          FMAINT_NO := getString(SegIDX+2);
        end
        else
        IF Qualifier = '2BD' Then
          //구매확인서번호
          FieldByName('LIC_NO').AsString := getString(SegIDX+2);
      end;

      DocCount := AlreadyDocumentCount('PCRLIC_H','MAINT_NO',FMAINT_NO);

      IF DocCount > 0 Then
      begin
        FMAINT_NO := FMAINT_NO+'_'+FormatFloat('000',DocCount);
      end;

      FieldByName('MAINT_NO').AsString := FMAINT_NO;

      //문서의 시작
      SegIDX := GetSegmentIDX('BGM','1','10');
      FieldByName('ChgCd').AsString := getString(SegIDX+1);
      FieldByName('BankSend_No').AsString := getString(SegIDX+3);
      FieldByName('MESSAGE1').AsString := getString(SegIDX+4);
      IF Trim(getString(SegIDX+5)) = '' Then
        FieldByName('MESSAGE2').AsString := 'NA'
      else
        FieldByName('MESSAGE2').AsString := getString(SegIDX+5);

      //신청인, 공급자
      for i := 1 to 2 do
      begin
        SegIDX := GetSegmentIDX('NAD',IntToStr(i),'11');
        Qualifier := getString(SegIDX+1);

        IF Qualifier = 'MS' Then
        begin
          FieldByName('APP_NAME1').AsString := getString(SegIDX+4);
          FieldByName('APP_NAME2').AsString := getString(SegIDX+5);
          FieldByName('AX_NAME2' ).AsString := getString(SegIDX+6);
          FieldByName('APP_ADDR1').AsString := getString(SegIDX+7);
          FieldByName('APP_ADDR2').AsString := getString(SegIDX+8);
          FieldByName('APP_ADDR3').AsString := getString(SegIDX+9);
        end
        else
        IF Qualifier = 'SU' Then
        begin
          FieldByName('EMAIL_ID').AsString := getString(SegIDX+2);
          FieldByName('EMAIL_DOMAIN').AsString := getString(SegIDX+3);
          FieldByName('SUP_NAME1').AsString := getString(SegIDX+4);
          FieldByName('SUP_NAME2').AsString := getString(SegIDX+5);
          FieldByName('SUP_ADDR3').AsString := getString(SegIDX+6);
          FieldByName('SUP_ADDR1').AsString := getString(SegIDX+7);
          FieldByName('SUP_ADDR2').AsString := getString(SegIDX+8);
        end;
      end;

      //공급물품명세구분
      SegIDX := GetSegmentIDX('BUS','1','12');
      IF AnsiMatchText(getString(SegIDX+2),ITM_GBN_CODE) Then
      begin
        FieldByName('ITM_GBN').AsString := getString(SegIDX+2);
        FieldByName('ITM_GNAME').AsString := '';
      end
      else
      begin
        FieldByName('ITM_GBN').AsString := 'ZZZ';
        FieldByName('ITM_GNAME').AsString := getString(SegIDX+3);
      end;

      //확인일자
      SegIDX := GetSegmentIDX('DTM','1','13');
      FieldByName('APP_DATE').AsString := getString(SegIDX+2);

      //총수량/단위
      SegIDX := GetSegmentIDX('CNT','1','15');
      FieldByName('TQTY' ).AsString := getString(SegIDX+2);
      FieldByName('TQTYC').AsString := getString(SegIDX+3);

      for i := 1 to 2 do
      begin
        SegIDX := GetSegmentIDX('MOA',IntToStr(i),'16');
        Qualifier := getString(SegIDX+1);

        IF Qualifier = '128' then
        begin
          FieldByName('TAMT1' ).AsString := getString(SegIDX+2);
          FieldByName('TAMT1C').AsString := getString(SegIDX+3);
          FieldByName('TAMT2').AsString := '0';
        end;
        IF Qualifier = '2AD' then
        begin
          //총금액의 통화단위가 KRW 또는 USD가 아니면 반드시 본항목이 있음
          FieldByName('TAMT2').AsString := getString(SegIDX+2);
        end;
      end;

      //할인,할증,변동사항

      SegIDX := GetSegmentIDX('ALC','1','17');
      IF SegIDX > -1 THEN
      BEGIN
        FieldByName('CHG_G').AsString := getString(SegIDX+1);
      END;

      for i := 1 to 2 do
      begin
        SegIDX := GetSegmentIDX('MOA',IntToStr(i),'28');
        IF SegIDX = -1 THEN Continue;
        Qualifier := getString(SegIDX+1);

        IF Qualifier = '131' Then
          FieldByName('C2_AMT').AsString := getString(i+2);
        IF Qualifier = '2AD' Then
        begin
          FieldByName('C1_AMT').AsString := getString(i+2);
          FieldByName('C1_C').AsString := getString(i+2);
        end;
      end;

      //발급조건등기타사항
      SegIDX := GetSegmentIDX('FTX','1','19');
      IF SegIDX > -1 THEN
      BEGIN
            FieldByName('LIC_INFO').AsString := getString(SegIDX+2)+#13#10+
                                                getString(SegIDX+3)+#13#10+
                                                getString(SegIDX+4)+#13#10+
                                                getString(SegIDX+5)+#13#10+
                                                getString(SegIDX+6);
      end;

      SegIDX := GetSegmentIDX('FII','1','1B');
      IF SegIDX > -1 THEN
      BEGIN
        //확인기관 코드
        IF getString(SegIDX+2) = 'KTNET' Then
        begin
          FieldByName('BK_CD').AsString := 'KTNET';
          FieldByName('BK_NAME1').AsString := '한국무역정보통신';
          FieldByName('BK_NAME2').AsString := '';
        end
        else
        begin
          FieldByName('BK_CD').AsString := getString(SegIDX+2);
          FieldByName('BK_NAME1').AsString := getString(SegIDX+5);
          FieldByName('BK_NAME2').AsString := getString(SegIDX+6);
        end;
      END;

      SegIDX := GetSegmentIDX('NAD','1','1C');
      IF SegIDX > -1 THEN
      BEGIN
        FieldByName('BK_NAME3').AsString := getString(SegIDX+2);
        FieldByName('BK_NAME4').AsString := getString(SegIDX+3);
        FieldByName('BK_NAME5').AsString := getString(SegIDX+4);
      END;
      InsertQuery.Post;

//------------------------------------------------------------------------------
// 세부사항
//------------------------------------------------------------------------------
      Close;
      SQL.Text := SQL_PCRLIC_DETAIL;
      Open;

      for i := GetSegmentIDX('LIN','1','14') to GetSegmentIDX('CNT','1','15') - 1 do
      begin
        IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
          Continue
        else
        begin
          SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
          SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
          SEGMENT_NO := RightStr(FMig.Strings[i],2);
        end;

        IF SEGMENT('LIN','','14') Then
        begin
          //이전에 입력된 레코드 저장
          IF SEGMENT_COUNT <> '1' Then InsertQuery.Post;

          InsertQuery.Append;
          FieldByName('KEYY').AsString := FMAINT_NO;
          FieldByName('SEQ').AsString := getString(i+1);
        end;
        //세번부호
        IF SEGMENT('PIA','1','20') Then
          FieldByName('HS_NO').AsString := getString(i+2);
        //품목
        IF SEGMENT('IMD','1','21') Then
          FieldByName('NAME1').AsString := getString(i+2);
        //수량
        IF SEGMENT('QTY','1','22') Then
        begin
          FieldByName('QTY').AsString := getString(i+2);
          FieldByName('QTYC').AsString := getString(i+3);
        end;

        //규격과 비고
        IF SEGMENT('FTX','','23') Then
        begin
          Qualifier := getString(i+1);
          IF Qualifier = 'AAA' Then
          begin
            //규격
            FieldByName('SIZE1').AsString := getString(i+2)+#13#10+
                                             getString(i+3)+#13#10+
                                             getString(i+4)+#13#10+
                                             getString(i+5)+#13#10+
                                             getString(i+6);
          end
          else
          IF Qualifier = 'AAI' Then
          begin
            //비고
            FieldByName('REMARK').AsString := getString(i+2)+#13#10+
                                              getString(i+3)+#13#10+
                                              getString(i+4)+#13#10+
                                              getString(i+5)+#13#10+
                                              getString(i+6);
          end;
        end;

        IF SEGMENT('MOA','','24') Then
        begin
          Qualifier := getString(i+1);
          IF Qualifier = '203' Then
          begin
            FieldByName('AMT1').AsString := getString(i+2);
            FieldByName('AMT1C').AsString := getString(i+3);
          end
          else
          IF Qualifier = '2AD' Then
          begin
            FieldByName('AMT2').AsString := getString(i+2);
//            FieldByName('AMT2C').AsString := getString(i+3);
          end
          else
          IF Qualifier = '146' Then
          begin
            FieldByName('PRI1').AsString := getString(i+2);
          end;
        end;

        //단가, 단가기준수량
        IF SEGMENT('PRI','1','25') Then
        begin
          //단가
          FieldByName('PRI2').AsString := getString(i+2);
          //단가기준수량
          FieldByName('PRI_BASE').AsString := getString(i+5);
          //단가기준수량단위
          FieldByName('PRI_BASEC').AsString := getString(i+6);
        end;

        //구매(공급)일
        IF SEGMENT('DTM','1','26') Then
          FieldByName('APP_DATE').AsString := getString(i+2);

        //할인/할증,변동
        IF SEGMENT('ALC','1','27') Then
          FieldByName('ACHG_G').AsString := getString(i+1);

        //할인/할증/변동 금액(외화금액)
        IF SEGMENT('MOA','','30') Then
        begin
          Qualifier := getString(i+1);
          IF Qualifier = '8' Then
          begin
            FieldByName('AC2_AMT').AsString := getString(i+2);
          end
          else
          IF Qualifier = '2AD' Then
          begin
            FieldByName('AC1_AMT').AsString := getString(i+2);
            FieldByName('AC1_C').AsString := getString(i+3);
          end;
        end;
      end;

      InsertQuery.Post;

//------------------------------------------------------------------------------
// 세금계산서
//------------------------------------------------------------------------------
      Close;
      SQL.Text := SQL_PCRLIC_TAX;
      Open;
      KEY_IDX := 1;
      for i := GetSegmentIDX('RFF','1','18') to GetSegmentIDX('UNS','1','') - 1 do
      begin
        IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
          Continue
        else
        begin
          SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
          SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
          SEGMENT_NO := RightStr(FMig.Strings[i],2);
        end;

        IF SEGMENT('RFF','','18') Then
        begin
          //이전에 입력된 레코드 저장
          IF SEGMENT_COUNT <> '1' Then InsertQuery.Post;

          InsertQuery.Append;
          FieldByName('KEYY').AsString := FMAINT_NO;
          FieldByName('SEQ').AsInteger := KEY_IDX;
          FieldByName('BILL_NO').AsString := getString(i+2);
          Inc(KEY_IDX);
        end;

        //발급일자
        IF SEGMENT('DTM','1','29') Then
          FieldByName('BILL_DATE').AsString := getString(i+2);

        //공급가액 세액
        IF SEGMENT('MOA','','2A') then
        begin
          Qualifier := getString(i+1);
          IF Qualifier = '11' Then
          begin
            FieldByName('BILL_AMOUNT').AsString := getString(i+2);
            FieldByName('BILL_AMOUNT_UNIT').AsString := getString(i+3);
          end;

          IF Qualifier = '150' Then
          begin
            //기본값
            IF Trim(getString(i+2)) = '' Then
              FieldByName('TAX_AMOUNT').AsString := '0'
            else
              FieldByName('TAX_AMOUNT').AsString := getString(i+2);
            FieldByName('TAX_AMOUNT_UNIT').AsString := getString(i+3);
          end;
        end;

        //품목
        IF SEGMENT('MOA','','2B') then
        begin
          FieldByName('ITEM_NAME'+SEGMENT_COUNT).AsString := getString(i+2);
        end;

        //품목규격
        IF SEGMENT('FTX','1','2C') then
        begin
          FieldByName('ITEM_DESC').AsString := getString(i+2)+#13#10+
                                               getString(i+3)+#13#10+
                                               getString(i+4)+#13#10+
                                               getString(i+5)+#13#10+
                                               getString(i+6);
        end;

        IF SEGMENT('QTY','1','2D') then
        begin
          FieldByName('QUANTITY').AsString := getString(i+2);
          FieldByName('QUANTITY_UNIT').AsString := getString(i+3);
        end;

      end;

      IF InsertQuery.State in [dsEdit, dsInsert] Then
        InsertQuery.Post;

      Result := True;
      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
        end;
      end;
    finally
      InsertQuery.Close;
    end;
  end;

end;

procedure TPCRLIC_NEW.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'PCRLIC';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'PCRLIC';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+3])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+3]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+3]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;
procedure TPCRLIC_NEW.TEST;
var
  i : integer;
begin
//  ShowMessage(IntToStr(GetSegmentIDX('UNS','1','')));
end;

end.
