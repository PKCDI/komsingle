unit RecvParent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TRecvParent = class
    protected
      FMAINT_NO : string;
      FMig : TStringList;
      FGroupStart : Boolean;
      FDOCID : String;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      InsertQuery  : TADOQuery;
      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      Qualifier : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function GetSegmentIDX(sID,sCnt,sNo : String):Integer;                    //세그먼트의 위치를 찾는 함수
      function GetSegCount(sID,sNo : String):Integer;                           //해당 세그먼트의 반복 횟수를 찾는 함수
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
      function AlreadyDocumentCount(TableName, KeyField, KeyValue : String):integer;
    public
      constructor Create(Mig : TStringList);
      destructor Destroy;      
      procedure Run_Ready(bDuplicate :Boolean); virtual; abstract;
      function Run_Convert:Boolean; virtual; abstract;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
  end;

implementation

uses MSSQL, Commonlib;

{ TRecvParent }

constructor TRecvParent.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  FGroupStart := False;  
  InsertQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TRecvParent.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  InsertQuery.Free;
  DuplicateQuery.Free;
end;

function TRecvParent.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr(FDOCID)+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TRecvParent.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TRecvParent.GetSegmentIDX(sID,sCnt,sNo : String):Integer;
var
  i : integer;
  SEGMENT_ID , SEGMENT_NO : String;
begin
  Result := -1;
  for i := 0 to FMig.Count - 1 do
  begin
    IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH'] )Then Continue;

    SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
    SEGMENT_COUNT := Trims(FMig.Strings[i],5,7);
    SEGMENT_NO := Trim(RightStr(FMig.Strings[i],2));

    IF (sID <> '') AND (sCnt = '') and (sNO = '') Then
    begin
      IF (SEGMENT_ID = sID) Then
      begin
        Result := i
      end;
    end
    else
    IF (SEGMENT_ID = sID) AND (SEGMENT_COUNT = sCnt) AND (SEGMENT_NO = sNo) Then
    begin
      Result := i;
      Break;
    end;
  end;
end;

function TRecvParent.GetSegCount(sID, sNo: String): Integer;
var
  i : integer;
  SEGMENT_ID , SEGMENT_NO : String;
begin
  Result := 0;
  for i := 0 to FMig.Count - 1 do
  begin
    IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then Continue;

    SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
    SEGMENT_NO := RightStr(Trim(FMig.Strings[i]),2);

    IF (SEGMENT_ID = sID) AND (SEGMENT_NO = sNo) Then
    begin
      Inc(Result);
    end;
  end;
end;


function TRecvParent.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TRecvParent.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO);
end;

function TRecvParent.AlreadyDocumentCount(TableName, KeyField, KeyValue : String): Integer;
var
  ValueLength : Integer;
  TempStr : String;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;

    ValueLength := Length(KeyValue);

    SQL.Text := 'SELECT TOP 10 '+KeyField+' FROM '+TableName+' WHERE LEFT(['+KeyField+'],'+IntToStr(ValueLength)+') = '+QuotedStr(KeyValue)+' ORDER BY '+KeyField+' DESC';
    Open;

    IF DuplicateQuery.RecordCount in [0,1] Then
      Result := DuplicateQuery.RecordCount
    else
    begin
      TempStr := RightStr( DuplicateQuery.FieldByName(KeyField).AsString , 3);
      Result := StrToIntDef(TempStr,0)+1;
    end;
    DuplicateQuery.Close;
  end;
end;

end.
