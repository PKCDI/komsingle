unit INF707;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TINF707 = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      INF7071InsQuery  : TADOQuery;
      INF7072InsQuery  : TADOQuery;
      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  INF707_DOC: TINF707;


implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine;

{ TINF707 }

constructor TINF707.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  INF7071InsQuery := TADOQuery.Create(nil);
  INF7072InsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TINF707.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  INF7071InsQuery.Free;
  INF7072InsQuery.Free;
  DuplicateQuery.Free;
end;

function TINF707.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('INF707')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TINF707.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TINF707.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TINF707.Run_Convert: Boolean;
var
  i : Integer;
  DocumentCount : integer;
  Qualifier : string;
  DocGubun : String;
  ILGubun : string;
  ShipPeriodCount,sRINfoCount : Integer;
begin
  Result := false;

    INF7071InsQuery.Close;
    INF7071InsQuery.Connection := DMMssql.KISConnect;
    INF7071InsQuery.SQL.Text := SQL_INF707_1;
    INF7071InsQuery.Open;

    INF7072InsQuery.Close;
    INF7072InsQuery.Connection := DMMssql.KISConnect;
    INF7072InsQuery.SQL.Text := SQL_INF707_2;
    INF7072InsQuery.Open;

    //APPEND
    try
      try

        INF7071InsQuery.Append;
        INF7072InsQuery.Append;

        //Default Values
        INF7071InsQuery.FieldByName('User_Id').AsString := LoginData.sID;
        INF7071InsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
        INF7071InsQuery.FieldByName('CHK1').AsString := '';
        INF7071InsQuery.FieldByName('CHK2').AsString := '';
        INF7071InsQuery.FieldByName('CHK3').AsString := '';

        INF7072InsQuery.FieldByName('NARRAT').AsBoolean := False;
        INF7071InsQuery.FieldByName('AMD_NO').AsInteger := 1;
        INF7072InsQuery.FieldByName('AMD_NO').AsInteger := 1;
        INF7071InsQuery.FieldByName('MSEQ').AsInteger := 1;
        INF7072InsQuery.FieldByName('MSEQ').AsInteger := 1;

        //값 초기화
        DocumentCount := 1;
        ShipPeriodCount := 1; // shipment Period (1,2,3) / (4,5,6)필드 구분을 위해 사용
        sRINfoCount := 1; //Sender to Receive Information (1,2,3) / (4,5,6)필드  구분을 위해사용
        //분석시작
        for i:= 1 to FMig.Count-1 do
        begin
          IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
            Continue
          else
          begin
            SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
            SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
            SEGMENT_NO := RightStr(FMig.Strings[i],2);
          end;

          IF SEGMENT('BGM','1','10') Then
          begin
            INF7071InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            INF7072InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);

            INF7071InsQuery.FieldByName('MESSAGE1').AsString := getString(i+3);
            INF7071InsQuery.FieldByName('MESSAGE2').AsString := getString(i+4);
          end
          Else IF SEGMENT('INP','1','11') then //개설방법
          begin
            INF7071InsQuery.FieldByName('IN_MATHOD').AsString := getString(i+3);
          end
          else IF SEGMENT('RFF','','12') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AD' then  //Sender's Reference (L/C No)
              INF7071InsQuery.FieldByName('CD_NO').AsString := getString(i+2);
            if Qualifier = '2AB' then  // Number Of Amendment
            begin
              //조건변경횟수
              INF7071InsQuery.FieldByName('AMD_NO').AsString := getString(i+2);
              //조건변경횟수 MSEQ가 순번(키값)이지만 따로 들어오는곳이 없어서 조건변경횟수 값으로 처리
              INF7071InsQuery.FieldByName('MSEQ').AsString := getString(i+2);
            end;
            if Qualifier = '2AE' then  //Receive's Reference (L/C No)
              INF7071InsQuery.FieldByName('RCV_REF').AsString := getString(i+2);
            if Qualifier = '2AC' then ; //개설은행 참조사항 (해당값이 들어오지 않음)
          end
          else if SEGMENT('DTM','','13') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AA' then  //신청일자
              INF7071InsQuery.FieldByName('APP_DATE').AsString := '20' + getString(i+2);
            if Qualifier = '182' then  //Date Of Issue 개설일자
              INF7071InsQuery.FieldByName('ISS_DATE').AsString := '20' + getString(i+2);
            if Qualifier = '2AB' then  //조건변경일자(Date of Amendment)
              INF7071InsQuery.FieldByName('AMD_DATE').AsString := '20' + getString(i+2);
            if Qualifier = '123' then  //유효기일 Date/time/period format qualifier
              INF7071InsQuery.FieldByName('EX_DATE').AsString := '20' + getString(i+2);
            if Qualifier = '38' then  // 최종선적일자(Latest Date of Shipment) 
              INF7072InsQuery.FieldByName('LST_DATE').AsString := '20' + getString(i+2);
          end
          else if SEGMENT('LOC','','14') then
          begin
            Qualifier := getString(i+1);
            if Qualifier ='76' then // 선적항
              INF7072InsQuery.FieldByName('SUNJUCK_PORT').AsString := getString(i+2);
            if Qualifier ='12' then // 도착항
              INF7072InsQuery.FieldByName('DOCHACK_PORT').AsString := getString(i+2);
            if Qualifier ='149' then // 수탁발송지
              INF7072InsQuery.FieldByName('LOAD_ON').AsString := getString(i+2);
            if Qualifier ='148' then // 최종목적지
              INF7072InsQuery.FieldByName('FOR_TRAN').AsString := getString(i+2);
          end
          else if SEGMENT('FTX','','15') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'ACB' then //기타정보
            begin
              INF7071InsQuery.FieldByName('AD_INFO1').AsString := getString(i+2);
              INF7071InsQuery.FieldByName('AD_INFO2').AsString := getString(i+3);
              INF7071InsQuery.FieldByName('AD_INFO3').AsString := getString(i+4);
              INF7071InsQuery.FieldByName('AD_INFO4').AsString := getString(i+5);
              INF7071InsQuery.FieldByName('AD_INFO5').AsString := getString(i+6);
            end;
            if Qualifier = 'ABT' then //Additional Amounts Covered (부가금액부담)
            begin
              INF7072InsQuery.FieldByName('AA_CV1').AsString := getString(i+2);
              INF7072InsQuery.FieldByName('AA_CV2').AsString := getString(i+3);
              INF7072InsQuery.FieldByName('AA_CV3').AsString := getString(i+4);
              INF7072InsQuery.FieldByName('AA_CV4').AsString := getString(i+5);
            end;
            if Qualifier = '2AF' then //선적기간(Shipment Period)
            begin
              if ShipPeriodCount = 1 then // 2AF로 처음 들어왔을경우
              begin
                INF7072InsQuery.FieldByName('SHIP_PD1').AsString := getString(i+2);
                INF7072InsQuery.FieldByName('SHIP_PD2').AsString := getString(i+3);
                INF7072InsQuery.FieldByName('SHIP_PD3').AsString := getString(i+4);
                Inc(ShipPeriodCount); //ShipPeriodCount를 증가시켜 다음번에 들어올경우 else로 처리되도록한다.
              end
              else  //// 2AF에 처음으로 들어오지않은경우
              begin
                INF7072InsQuery.FieldByName('SHIP_PD4').AsString := getString(i+2);
                INF7072InsQuery.FieldByName('SHIP_PD5').AsString := getString(i+3);
                INF7072InsQuery.FieldByName('SHIP_PD6').AsString := getString(i+4);
              end;
            end;
            if Qualifier = '2AD' then  //기타 조건변경사항(Narrative)
            begin
              if INF7072InsQuery.FieldByName('NARRAT_1').AsString = '' then
              begin
                INF7072InsQuery.FieldByName('NARRAT_1').AsString := getString(i+2)+#13#10+
                                                                    getString(i+3)+#13#10+
                                                                    getString(i+4)+#13#10+
                                                                    getString(i+5)+#13#10+
                                                                    getString(i+6);
              end
              else
              begin
                INF7072InsQuery.FieldByName('NARRAT_1').AsString := INF7072InsQuery.FieldByName('NARRAT_1').AsString+#1310+
                                                                    getString(i+2)+#13#10+
                                                                    getString(i+3)+#13#10+
                                                                    getString(i+4)+#13#10+
                                                                    getString(i+5)+#13#10+
                                                                    getString(i+6);
              end;
            end;
            if Qualifier = '2AE' then // 수신은행앞 정보(Sender to Receiver Information)
            begin
              if sRINfoCount = 1 then  // 2AE로 처음 들어왔을경우
              begin
                INF7072InsQuery.FieldByName('SR_INFO1').AsString := getString(i+2);
                INF7072InsQuery.FieldByName('SR_INFO2').AsString := getString(i+3);
                INF7072InsQuery.FieldByName('SR_INFO3').AsString := getString(i+4);
                Inc(sRINfoCount); //sRINfoCount를 증가시켜 다음번에 들어올경우 else로 처리되도록한다.
              end
              else // 2AE로 처음들어오는 것이 아닌경우
              begin
                INF7072InsQuery.FieldByName('SR_INFO4').AsString := getString(i+2);
                INF7072InsQuery.FieldByName('SR_INFO5').AsString := getString(i+3);
                INF7072InsQuery.FieldByName('SR_INFO6').AsString := getString(i+4);
              end;
            end;
          end
          else if SEGMENT('FII','','16') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'AW' then //개설은행
            begin
              if Trim(getString(i+3)) = 'NONE' then
                INF7071InsQuery.FieldByName('AP_BANK').AsString := ''
              else
                INF7071InsQuery.FieldByName('AP_BANK').AsString := getString(i+3);

              INF7071InsQuery.FieldByName('AP_BANK1').AsString := Trim(Copy(getString(i+6),1,35));
              INF7071InsQuery.FieldByName('AP_BANK2').AsString := Trim(Copy(getString(i+6),36,70));
              INF7071InsQuery.FieldByName('AP_BANK3').AsString := Trim(Copy(getString(i+7),1,35));
              INF7071InsQuery.FieldByName('AP_BANK4').AsString := Trim(Copy(getString(i+7),36,70));
            end;
            if Qualifier = '2AA' then //통지은행
            begin
              if Trim(getString(i+3)) = 'NONE' then
                INF7071InsQuery.FieldByName('AD_BANK').AsString := ''
              else
                INF7071InsQuery.FieldByName('AD_BANK').AsString := getString(i+3);

              INF7071InsQuery.FieldByName('AD_BANK1').AsString := Trim(Copy(getString(i+6),1,35));
              INF7071InsQuery.FieldByName('AD_BANK2').AsString := Trim(Copy(getString(i+6),36,70));
              INF7071InsQuery.FieldByName('AD_BANK3').AsString := Trim(Copy(getString(i+7),1,35));
              INF7071InsQuery.FieldByName('AD_BANK4').AsString := Trim(Copy(getString(i+7),36,70));
            end;
            if Qualifier ='AZ' then   //Issuing Bank's Reference
            begin
              INF7071InsQuery.FieldByName('ISS_BANK1').AsString := Trim(Copy(getString(i+6),1,35));
              INF7071InsQuery.FieldByName('ISS_BANK2').AsString := Trim(Copy(getString(i+6),36,70));
              INF7071InsQuery.FieldByName('ISS_BANK3').AsString := Trim(Copy(getString(i+7),1,35));
              INF7071InsQuery.FieldByName('ISS_BANK4').AsString := Trim(Copy(getString(i+7),36,70));
            end;
          end
          else if SEGMENT('COM','1','16') then //개설은행 전화번호를 표시 (AP_BANK)
          begin
            INF7071InsQuery.FieldByName('AP_BANK5').AsString := getString(i+1);
          end
          else if SEGMENT('NAD','','17') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'DF' then  //Applicant
            begin
              INF7072InsQuery.FieldByName('APPLIC1').AsString := getString(i+2);
              INF7072InsQuery.FieldByName('APPLIC2').AsString := getString(i+3);
              INF7072InsQuery.FieldByName('APPLIC3').AsString := getString(i+4);
              INF7072InsQuery.FieldByName('APPLIC4').AsString := getString(i+5);
            end;
            if Qualifier = 'DG' then //Beneficiary
            begin
              INF7072InsQuery.FieldByName('BENEFC1').AsString := getString(i+2);
              INF7072InsQuery.FieldByName('BENEFC2').AsString := getString(i+3);
              INF7072InsQuery.FieldByName('BENEFC3').AsString := getString(i+4);
              INF7072InsQuery.FieldByName('BENEFC4').AsString := getString(i+5);
              INF7072InsQuery.FieldByName('BENEFC5').AsString := getString(i+6);
            end;
            if Qualifier = '2AE' then  //신청업체
            begin
              INF7072InsQuery.FieldByName('EX_NAME1').AsString := getString(i+7);
              INF7072InsQuery.FieldByName('EX_NAME2').AsString := getString(i+8);
              INF7072InsQuery.FieldByName('EX_NAME3').AsString := getString(i+9);
              INF7072InsQuery.FieldByName('EX_ADDR1').AsString := getString(i+10);
              INF7072InsQuery.FieldByName('EX_ADDR2').AsString := getString(i+11);
            end;
            if Qualifier = '2AF' then  //page3 개설은행
            begin
              INF7072InsQuery.FieldByName('OP_BANK1').AsString := getString(i+7);
              INF7072InsQuery.FieldByName('OP_BANK2').AsString := getString(i+8);
              INF7072InsQuery.FieldByName('OP_BANK3').AsString := getString(i+9);
              INF7072InsQuery.FieldByName('OP_ADDR1').AsString := getString(i+10);
              INF7072InsQuery.FieldByName('OP_ADDR2').AsString := getString(i+11);
            end;
          end
          else if SEGMENT('COM','1','17') then //개설의뢰인 은행 Applicant
          begin
            INF7072InsQuery.FieldByName('APPLIC5').AsString := getString(i+1);
          end
          else if SEGMENT('MOA','','18') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AA' then //신용장 증액분(Increase of Documentary Credit Amount)
            begin
              INF7072InsQuery.FieldByName('INCD_AMT').AsCurrency:= StrToCurr(getString(i+2));
              INF7072InsQuery.FieldByName('INCD_CUR').AsString := getString(i+3);
            end;
            if Qualifier = '2AB' then //신용장 감액분(Decrease of Documentary Credit Amount)
            begin
              INF7072InsQuery.FieldByName('DECD_AMT').AsCurrency:= StrToCurr(getString(i+2));
              INF7072InsQuery.FieldByName('DECD_CUR').AsString := getString(i+3);
            end;
            if Qualifier = '212' then //변경후 최종금액(New Documentary Credit Amount after Amendment)
            begin
              INF7072InsQuery.FieldByName('NWCD_AMT').AsCurrency:= StrToCurr(getString(i+2));
              INF7072InsQuery.FieldByName('NWCD_CUR').AsString := getString(i+3);
            end;
            if Qualifier = 'ZZZ' then //변경전 최종금액(Documentary Credit Amount Before Amendment)을 표시
            begin
              INF7072InsQuery.FieldByName('BFCD_AMT').AsCurrency:= StrToCurr(getString(i+2));
              INF7072InsQuery.FieldByName('BFCD_CUR').AsString := getString(i+3);
            end;
          end
          else if SEGMENT('ALC','1','20') then  //Maximum Credit Amount
          begin
            INF7072InsQuery.FieldByName('CD_MAX').AsString := getString(i+1);
          end
          else if SEGMENT('PCD','1','20') then
          begin
            INF7072InsQuery.FieldByName('CD_PERP').AsString := LeftStr(getString(i+2), Pos('.',getString(i+2))-1);
            INF7072InsQuery.FieldByName('CD_PERM').AsString := MidStr(getString(i+2),Pos('.',getString(i+2))+1,3);
          end
          else if SEGMENT('RFF','','19') then
          begin
            ILGubun := SEGMENT_COUNT;
            if ILGubun = '1' then
            begin
              INF7071InsQuery.FieldByName('IMP_CD1').AsString:= getString(i+1);
              INF7071InsQuery.FieldByName('IL_NO1').AsString:= getString(i+2);
            end
            else if ILGubun = '2' then
            begin
              INF7071InsQuery.FieldByName('IMP_CD2').AsString:= getString(i+1);
              INF7071InsQuery.FieldByName('IL_NO2').AsString:= getString(i+2);
            end
            else if ILGubun = '3' then
            begin
              INF7071InsQuery.FieldByName('IMP_CD3').AsString:= getString(i+1);
              INF7071InsQuery.FieldByName('IL_NO3').AsString:= getString(i+2);
            end
            else if ILGubun = '4' then
            begin
              INF7071InsQuery.FieldByName('IMP_CD4').AsString:= getString(i+1);
              INF7071InsQuery.FieldByName('IL_NO4').AsString:= getString(i+2);
            end
            else if ILGubun = '5' then
            begin
              INF7071InsQuery.FieldByName('IMP_CD5').AsString:= getString(i+1);
              INF7071InsQuery.FieldByName('IL_NO5').AsString:= getString(i+2);
            end;
          end
          else if SEGMENT('MOA','','19') then
          begin
            if ILGubun = '1' then
            begin
              INF7071InsQuery.FieldByName('IL_AMT1').AsCurrency:= StrToCurr(getString(i+2));
              INF7071InsQuery.FieldByName('IL_CUR1').AsString:= getString(i+3);
            end
            else if ILGubun = '2' then
            begin
              INF7071InsQuery.FieldByName('IL_AMT2').AsCurrency:= StrToCurr(getString(i+2));
              INF7071InsQuery.FieldByName('IL_CUR2').AsString:= getString(i+3);
            end
            else if ILGubun = '3' then
            begin
              INF7071InsQuery.FieldByName('IL_AMT3').AsCurrency:= StrToCurr(getString(i+2));
              INF7071InsQuery.FieldByName('IL_CUR3').AsString:= getString(i+3);
            end
            else if ILGubun = '4' then
            begin
              INF7071InsQuery.FieldByName('IL_AMT4').AsCurrency:= StrToCurr(getString(i+2));
              INF7071InsQuery.FieldByName('IL_CUR4').AsString:= getString(i+3);
            end
            else if ILGubun = '5' then
            begin
              INF7071InsQuery.FieldByName('IL_AMT5').AsCurrency:= StrToCurr(getString(i+2));
              INF7071InsQuery.FieldByName('IL_CUR5').AsString:= getString(i+3);
            end
          end;
        end;

        INF7071InsQuery.Post;
        INF7072InsQuery.Post;

        Result := True;
      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
        end;
      end;
    finally
      INF7071InsQuery.Close;
      INF7072InsQuery.Close;
    end;
end;

procedure TINF707.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'INF707';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'INF707';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TINF707.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO);
end;

end.
