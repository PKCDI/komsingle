unit LOCAM1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TLOCAM1 = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      LOCAM1_InsQuery  : TADOQuery;

      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  LOCAM1_DOC: TLOCAM1;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine, RecvParent;

{ TLOCAM1 }

constructor TLOCAM1.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  LOCAM1_InsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TLOCAM1.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  LOCAM1_InsQuery.Free;
  DuplicateQuery.Free;
end;

function TLOCAM1.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCNAME = '+QuotedStr('LOCAM1')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TLOCAM1.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TLOCAM1.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TLOCAM1.Run_Convert: Boolean;
var
  i , countIdx : Integer;
  DocumentCount : integer;
  Qualifier : string;
  DocGubun : String;
  ILGubun : string;
begin
  Result := false;

  LOCAM1_InsQuery.Close;
  LOCAM1_InsQuery.Connection := DMMssql.KISConnect;
  LOCAM1_InsQuery.SQL.Text := SQL_LOCAM1;
  LOCAM1_InsQuery.Open;

  //APPEND
  try
    try
      with LOCAM1_InsQuery do
      begin
        Append;
        //Default Values
        FieldByName('User_Id').AsString := LoginData.sID;
        FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
        FieldByName('CHK1').AsString := '';
        FieldByName('CHK2').AsString := '';
        FieldByName('CHK3').AsString := '';
        FieldByName('PRNO').AsString := '0';

        //값 초기화
        DocumentCount := 1;

        //분석시작
        for i:= 1 to FMig.Count-1 do
        begin
         // ShowMessage(IntToStr(i));
          IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
            Continue
          else
          begin
            SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
            SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
            SEGMENT_NO := RightStr(FMig.Strings[i],2);
          end;
          IF SEGMENT('BGM','1','10') Then
          begin
            FieldByName('MAINT_NO').AsString := getString(i+2);
            FieldByName('MESSAGE1').AsString := getString(i+3);
            FieldByName('MESSAGE2').AsString := getString(i+4);
          end
          else if SEGMENT('RFF','','11') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'DM' then //신청번호
            begin
              FieldByName('BGM_REF').AsString := getString(i+2);
            end;
            if Qualifier = 'LC' then //내국신용장번호
            begin
              FieldByName('LC_NO').AsString := getString(i+2);
            end;
          end;
          for countIdx := 3 to 12 do  //최종 물품매도확약서번호
          begin
            if SEGMENT('RFF',IntToStr(countIdx),'11') then
            begin
              FieldByName('OFFERNO'+IntToStr(countIdx-2)).AsString := getString(i+2);
            end;
          end;
          if SEGMENT('DTM','','12') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '137' then //조건변경통지일자
            begin
              FieldByName('ADV_DATE').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '2AB' then //조건변경일자
            begin
              FieldByName('AMD_DATE').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '182' then //개설일자
            begin
              FieldByName('ISS_DATE').AsString := '20' + getString(i+2);
            end;
          end
          else if SEGMENT('FTX','1','13') then // 기타정보
          begin
            if FieldByName('REMARK1').AsString = '' then
              begin
                FieldByName('REMARK').AsString := 'Y';
                FieldByName('REMARK1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
              end;
          end
          else if SEGMENT('FII','1','14') then //개설은행
          begin
             FieldByName('ISSBANK').AsString := getString(i+2);
             FieldByName('ISSBANK1').AsString := getString(i+5);
             FieldByName('ISSBANK2').AsString := getString(i+6);
          end
          else if SEGMENT('NAD','','15') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'DF' then  // 개설의뢰인
            begin
              FieldByName('APPLIC1').AsString := getString(i+7);
              FieldByName('APPLIC2').AsString := getString(i+8);
              FieldByName('APPLIC3').AsString := getString(i+9);
              FieldByName('APPADDR1').AsString := getString(i+10);
              FieldByName('APPADDR2').AsString := getString(i+11);
              FieldByName('APPADDR3').AsString := getString(i+12);
            end;
            if Qualifier = 'DG' then //수혜자
            begin
              FieldByName('BENEFC1').AsString := getString(i+7);
              FieldByName('BENEFC2').AsString := getString(i+8);
              FieldByName('BENEFC3').AsString := getString(i+9);
              FieldByName('BNFADDR1').AsString := getString(i+10);
              FieldByName('BNFADDR2').AsString := getString(i+11);
              FieldByName('BNFADDR3').AsString := getString(i+12);
            end;
            if Qualifier = 'AX' then //전자서명
            begin
              FieldByName('EXNAME1').AsString := getString(i+7);
              FieldByName('EXNAME2').AsString := getString(i+8);
              FieldByName('EXNAME3').AsString := getString(i+9);
            end;
          end
          else if SEGMENT('RFF','1','16') then //조건변경횟수
          begin
            FieldByName('AMD_NO').AsString := getString(i+2);
          end
          else if SEGMENT('DTM','','20') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2' then //변경후 물품인도기일
            begin
              FieldByName('DELIVERY').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '123' then //변경후 유효기일
            begin
              FieldByName('EXPIRY').AsString := '20' + getString(i+2);
            end;
          end
          else if SEGMENT('FTX','','21') then //기타 조건변경사항
          begin
            if FieldByName('CHGINFO1').AsString = '' then
            begin
              FieldByName('CHGINFO').AsString := 'Y';
              FieldByName('CHGINFO1').AsString := getString(i+2)+#13#10+
                                                  getString(i+3)+#13#10+
                                                  getString(i+4)+#13#10+
                                                  getString(i+5)+#13#10+
                                                  getString(i+6);
            end
            else
            begin
              FieldByName('CHGINFO1').AsString := FieldByName('CHGINFO1').AsString+#13#10+
                                                  getString(i+2)+#13#10+
                                                  getString(i+3)+#13#10+
                                                  getString(i+4)+#13#10+
                                                  getString(i+5)+#13#10+
                                                  getString(i+6);
            end;
          end
          else if SEGMENT('BUS','1','22') then //내국신용장 종류
          begin
            FieldByName('LOC_TYPE').AsString := getString(i+1);
          end
          else if SEGMENT('MOA','','30') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AD' then //변경후 외화금액
            begin
              FieldByName('LOC1AMT').AsCurrency := StrToCurr(getString(i+2));
              FieldByName('LOC1AMTC').AsString := getString(i+3);
            end;
            if Qualifier = '2AE' then //변경후 원화금액
            begin
              FieldByName('LOC2AMT').AsCurrency := StrToCurr(getString(i+2));
              FieldByName('LOC2AMTC').AsString := getString(i+3);
            end;
          end
          else if SEGMENT('PCD','1','31') then //허용오차
          begin
            FieldByName('CD_PERP').AsString := LeftStr(getString(i+2), Pos('.',getString(i+2))-1);
            FieldByName('CD_PERM').AsString := MidStr(getString(i+2),Pos('.',getString(i+2))+1,3);
          end
          else if SEGMENT('CUX','1','') then //매매기준율
          begin
            FieldByName('EX_RATE').AsCurrency := StrToCurr(getString(i+1));
          end;
        end; //for end

        Post;

      end;  //with end
      Result := True;
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    LOCAM1_InsQuery.Close;
  end;
end;

procedure TLOCAM1.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'LOCAMA';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'LOCAM1';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;

end;

function TLOCAM1.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT);
end;

end.
 