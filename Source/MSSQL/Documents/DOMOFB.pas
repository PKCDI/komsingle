unit DOMOFB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TDOMOFB = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      DOMOFB_H1InsQuery  : TADOQuery;
      DOMOFB_H2InsQuery  : TADOQuery;
      DOMOFB_H3InsQuery  : TADOQuery;
      DOMOFB_DInsQuery   : TADOQuery;
      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  DOMOFB_DOC: TDOMOFB;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine, RecvParent;

{ TDOMOFB }

constructor TDOMOFB.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  DOMOFB_H1InsQuery := TADOQuery.Create(nil);
  DOMOFB_H2InsQuery := TADOQuery.Create(nil);
  DOMOFB_H3InsQuery := TADOQuery.Create(nil);
  DOMOFB_DInsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TDOMOFB.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  DOMOFB_H1InsQuery.Free;
  DOMOFB_H2InsQuery.Free;
  DOMOFB_H3InsQuery.Free;
  DOMOFB_DInsQuery.Free;
  DuplicateQuery.Free;
end;

function TDOMOFB.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('DOMOFR')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TDOMOFB.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TDOMOFB.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TDOMOFB.Run_Convert: Boolean;
var
  i  : Integer;
  DocumentCount : integer;
  Qualifier : string;
  FMAINT_NO : String;
  MEMOString : TStringList;
  UNITAMOUNT_UNIT : string;
begin
  Result := false;


  DOMOFB_H1InsQuery.Close;
  DOMOFB_H1InsQuery.Connection := DMMssql.KISConnect;
  DOMOFB_H1InsQuery.SQL.Text := SQL_DOMOFB_H1;
  DOMOFB_H1InsQuery.Open;

  DOMOFB_H2InsQuery.Close;
  DOMOFB_H2InsQuery.Connection := DMMssql.KISConnect;
  DOMOFB_H2InsQuery.SQL.Text := SQL_DOMOFB_H2;
  DOMOFB_H2InsQuery.Open;

  DOMOFB_H3InsQuery.Close;
  DOMOFB_H3InsQuery.Connection := DMMssql.KISConnect;
  DOMOFB_H3InsQuery.SQL.Text := SQL_DOMOFB_H3;
  DOMOFB_H3InsQuery.Open;

  DOMOFB_DInsQuery.Close;
  DOMOFB_DInsQuery.Connection := DMMssql.KISConnect;
  DOMOFB_DInsQuery.SQL.Text := SQL_DOMOFB_D;
  DOMOFB_DInsQuery.Open;

  MEMOString := TStringList.Create;
  //APPEND
  try
      try
        DOMOFB_H1InsQuery.Append;
        DOMOFB_H2InsQuery.Append;
        DOMOFB_H3InsQuery.Append;
        
        //Default Values
        DOMOFB_H1InsQuery.FieldByName('User_Id').AsString := LoginData.sID;
        DOMOFB_H1InsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
        DOMOFB_H1InsQuery.FieldByName('CHK1').AsString := '';
        DOMOFB_H1InsQuery.FieldByName('CHK2').AsString := '';
        DOMOFB_H1InsQuery.FieldByName('CHK3').AsString := '';
        DOMOFB_H1InsQuery.FieldByName('PRNO').AsString := '0';

        //값 초기화
        DocumentCount := 1;

        //분석시작
        for i:= 1 to FMig.Count-1 do
        begin
         // ShowMessage(IntToStr(i));
          IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
            Continue
          else
          begin
            SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
            SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
            SEGMENT_NO := RightStr(FMig.Strings[i],2);
          end;
          IF SEGMENT('BGM','1','10') Then
          begin
            DOMOFB_H1InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            DOMOFB_H1InsQuery.FieldByName('Maint_Rff').AsString := getString(i+2);
            DOMOFB_H2InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            DOMOFB_H3InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            DOMOFB_H1InsQuery.FieldByName('DOC_TYPE').AsString := getString(i+1);
            FMAINT_NO := getString(i+2);
            DOMOFB_H1InsQuery.FieldByName('MESSAGE1').AsString := getString(i+3);
            DOMOFB_H1InsQuery.FieldByName('MESSAGE2').AsString := getString(i+4);

            //------------------------------------------------------------------------------
            // 기존들어온 데이터 삭제
            //------------------------------------------------------------------------------
            with TADOQuery.Create(nil) do
            begin
              try
                Connection := DMMssql.KISConnect;
                Close;
                SQL.Text := 'DELETE FROM DOMOFB_D WHERE KEYY = '+QuotedStr(FMAINT_NO);
                ExecSQL;
                SQL.Text := 'DELETE FROM DOMOFB_H3 WHERE MAINT_NO = '+QuotedStr(FMAINT_NO);
                ExecSQL;
                SQL.Text := 'DELETE FROM DOMOFB_H2 WHERE MAINT_NO = '+QuotedStr(FMAINT_NO);
                ExecSQL;
                SQL.Text := 'DELETE FROM DOMOFB_H1 WHERE MAINT_NO = '+QuotedStr(FMAINT_NO);
                ExecSQL;
              finally
                Close;
                Free;
              end;
            end;

          end
          //원산지코드,지방명---------------------------------------------------
          else if SEGMENT('LOC','1','12') then
          begin
            DOMOFB_H3InsQuery.FieldByName('ORGN1N').AsString := getString(i+2);
            DOMOFB_H3InsQuery.FieldByName('ORGN1LOC').AsString := getString(i+5);
          end
          else if SEGMENT('LOC','2','12') then
          begin
            DOMOFB_H3InsQuery.FieldByName('ORGN2N').AsString := getString(i+2);
            DOMOFB_H3InsQuery.FieldByName('ORGN2LOC').AsString := getString(i+5);
          end
          else if SEGMENT('LOC','3','12') then
          begin
            DOMOFB_H3InsQuery.FieldByName('ORGN3N').AsString := getString(i+2);
            DOMOFB_H3InsQuery.FieldByName('ORGN3LOC').AsString := getString(i+5);
          end
          else if SEGMENT('LOC','4','12') then
          begin
            DOMOFB_H3InsQuery.FieldByName('ORGN4N').AsString := getString(i+2);
            DOMOFB_H3InsQuery.FieldByName('ORGN4LOC').AsString := getString(i+5);
          end
          else if SEGMENT('LOC','5','12') then
          begin
            DOMOFB_H3InsQuery.FieldByName('ORGN5N').AsString := getString(i+2);
            DOMOFB_H3InsQuery.FieldByName('ORGN5LOC').AsString := getString(i+5);
          end
          //--------------------------------------------------------------------
          else if SEGMENT('FTX','','13') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '1AB' then //유효기간
            begin
              DOMOFB_H1InsQuery.FieldByName('OFR_SQ').AsString := 'Y';
              DOMOFB_H1InsQuery.FieldByName('OFR_SQ1').AsString := getString(i+2);
              DOMOFB_H1InsQuery.FieldByName('OFR_SQ2').AsString := getString(i+3);
              DOMOFB_H1InsQuery.FieldByName('OFR_SQ3').AsString := getString(i+4);
              DOMOFB_H1InsQuery.FieldByName('OFR_SQ4').AsString := getString(i+5);
              DOMOFB_H1InsQuery.FieldByName('OFR_SQ5').AsString := getString(i+6);
            end;
            if Qualifier = 'ITS' then //검사방법
            begin
              DOMOFB_H2InsQuery.FieldByName('TSTINST').AsString := 'Y';
              DOMOFB_H2InsQuery.FieldByName('TSTINST1').AsString := getString(i+2);
              DOMOFB_H2InsQuery.FieldByName('TSTINST2').AsString := getString(i+3);
              DOMOFB_H2InsQuery.FieldByName('TSTINST3').AsString := getString(i+4);
              DOMOFB_H2InsQuery.FieldByName('TSTINST4').AsString := getString(i+5);
              DOMOFB_H2InsQuery.FieldByName('TSTINST5').AsString := getString(i+6);
            end;
            if Qualifier = 'AAB' then //결제방법
            begin
              DOMOFB_H2InsQuery.FieldByName('PAY_ETC').AsString := 'Y';
              DOMOFB_H2InsQuery.FieldByName('PAY_ETC1').AsString := getString(i+2);
              DOMOFB_H2InsQuery.FieldByName('PAY_ETC2').AsString := getString(i+3);
              DOMOFB_H2InsQuery.FieldByName('PAY_ETC3').AsString := getString(i+4);
              DOMOFB_H2InsQuery.FieldByName('PAY_ETC4').AsString := getString(i+5);
              DOMOFB_H2InsQuery.FieldByName('PAY_ETC5').AsString := getString(i+6);
            end;
            if Qualifier = 'PKG' then //포장방법
            begin
              DOMOFB_H2InsQuery.FieldByName('PCKINST').AsString := 'Y';
              DOMOFB_H2InsQuery.FieldByName('PCKINST1').AsString := getString(i+2);
              DOMOFB_H2InsQuery.FieldByName('PCKINST2').AsString := getString(i+3);
              DOMOFB_H2InsQuery.FieldByName('PCKINST3').AsString := getString(i+4);
              DOMOFB_H2InsQuery.FieldByName('PCKINST4').AsString := getString(i+5);
              DOMOFB_H2InsQuery.FieldByName('PCKINST5').AsString := getString(i+6);
            end;
          end
          else if SEGMENT('RFF','','14') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'AAG' then //발행번호
            begin
              DOMOFB_H1InsQuery.FieldByName('OFR_NO').AsString := getString(i+2);
            end;
            if Qualifier = 'CR' then //수요자(개설업체)가 참조하기 위한 번호를 수혜업체가 기재하는 전송항목.
            begin
              DOMOFB_H1InsQuery.FieldByName('UD_RENO').AsString := getString(i+2);
            end;
            if Qualifier ='HS' then //대표 공급물품 HS 부호를 표시
            begin
              DOMOFB_H1InsQuery.FieldByName('HS_CODE').AsString := getString(i+2);
            end;
          end
          else if SEGMENT('DTM','1','20') then //발행일자
          begin
            DOMOFB_H1InsQuery.FieldByName('OFR_DATE').AsString := '20'+getString(i+2);
          end
          else if SEGMENT('NAD','','15') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'SE' then //발행자
            begin
              DOMOFB_H1InsQuery.FieldByName('SR_NO').AsString := getString(i+2);//무역대리점등록번호
              DOMOFB_H1InsQuery.FieldByName('SR_NAME1').AsString := getString(i+5);//상호
              DOMOFB_H1InsQuery.FieldByName('SR_NAME2').AsString := getString(i+6);//대표자명
              DOMOFB_H1InsQuery.FieldByName('SR_NAME3').AsString := getString(i+7);//전자서명
              DOMOFB_H1InsQuery.FieldByName('SR_ADDR1').AsString := getString(i+8);//주소
              DOMOFB_H1InsQuery.FieldByName('SR_ADDR2').AsString := getString(i+9);
              DOMOFB_H1InsQuery.FieldByName('SR_ADDR3').AsString := getString(i+10);
            end;
            if Qualifier = 'BY' then //수요자
            begin
              DOMOFB_H1InsQuery.FieldByName('UD_NO').AsString := getString(i+2);//사업자등록번호
              DOMOFB_H1InsQuery.FieldByName('UD_NAME1').AsString := getString(i+5);//상호
              DOMOFB_H1InsQuery.FieldByName('UD_NAME2').AsString := getString(i+6);//대표자명
              DOMOFB_H1InsQuery.FieldByName('UD_NAME3').AsString := getString(i+7);//전자서명
              DOMOFB_H1InsQuery.FieldByName('UD_ADDR1').AsString := getString(i+8);//주소
              DOMOFB_H1InsQuery.FieldByName('UD_ADDR2').AsString := getString(i+9);
              DOMOFB_H1InsQuery.FieldByName('UD_ADDR3').AsString := getString(i+10);
            end;
          end
          else if SEGMENT('TDT','1','16') then //운송수단
          begin
            DOMOFB_H3InsQuery.FieldByName('TRNS_ID').AsString := getString(i+2);
          end
          else if SEGMENT('LOC','','21') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '9' then  //선적지
            begin
              DOMOFB_H3InsQuery.FieldByName('LOADD').AsString := getString(i+2);
              DOMOFB_H3InsQuery.FieldByName('LOADLOC').AsString := getString(i+5);
            end;
            if Qualifier = '8' then //도착지
            begin
              DOMOFB_H3InsQuery.FieldByName('DEST').AsString := getString(i+2);
              DOMOFB_H3InsQuery.FieldByName('DESTLOC').AsString := getString(i+5);
            end;
          end
          else if SEGMENT('FTX','','22') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '1AC' then //선적시기
            begin
              DOMOFB_H3InsQuery.FieldByName('LOADTXT').AsString := 'Y';
              DOMOFB_H3InsQuery.FieldByName('LOADTXT1').AsString := getString(i+2);
              DOMOFB_H3InsQuery.FieldByName('LOADTXT2').AsString := getString(i+3);
              DOMOFB_H3InsQuery.FieldByName('LOADTXT3').AsString := getString(i+4);
              DOMOFB_H3InsQuery.FieldByName('LOADTXT4').AsString := getString(i+5);
              DOMOFB_H3InsQuery.FieldByName('LOADTXT5').AsString := getString(i+6);
            end;
            if Qualifier = '1AD' then //도착시기
            begin
              DOMOFB_H3InsQuery.FieldByName('DESTTXT').AsString := 'Y';
              DOMOFB_H3InsQuery.FieldByName('DESTTXT1').AsString := getString(i+2);
              DOMOFB_H3InsQuery.FieldByName('DESTTXT2').AsString := getString(i+3);
              DOMOFB_H3InsQuery.FieldByName('DESTTXT3').AsString := getString(i+4);
              DOMOFB_H3InsQuery.FieldByName('DESTTXT4').AsString := getString(i+5);
              DOMOFB_H3InsQuery.FieldByName('DESTTXT5').AsString := getString(i+6);
            end;
          end
//세부사항-------------------------------------------------------------------------------------
          else IF SEGMENT('LIN','','19') Then  //품목입력시 순번
          begin
            MEMOString.Clear;
            //이전에 입력된 레코드 저장
            IF getString(i+1) <> '1' Then DOMOFB_DInsQuery.Post; //SEQ가 1번 즉 처음이 아닐경우 이전의 내용을 post

            DOMOFB_DInsQuery.Append;
            DOMOFB_DInsQuery.FieldByName('KEYY').AsString := FMAINT_NO;
            DOMOFB_DInsQuery.FieldByName('SEQ').AsString := getString(i+1);

          end
          else if SEGMENT('PIA','1','25') then //품목의 HS번호
          begin
            DOMOFB_DInsQuery.FieldByName('HS_NO').AsString := getString(i+2);
          end
          else if SEGMENT('IMD','','26') then //물품의 품명을 기재
          begin
            Qualifier := getString(i+1);
            if Qualifier = '1AA' then
            begin
              if Trim(MEMOString.Text) = '' then
                MEMOString.Text := getString(i+2)
              else
                MEMOString.Text := MEMOString.Text + getString(i+2);
              DOMOFB_DInsQuery.FieldByName('NAME1').AsString := MEMOString.Text;
            end;
          end
          else if SEGMENT('QTY','','27') then
          begin
            Qualifier := getString(i+1);
            if (Qualifier = '1') and (Trim(getString(i+2)) <> '') then // 수량
            begin
              DOMOFB_DInsQuery.FieldByName('QTY').AsCurrency := StrToCurr(getString(i+2));
              DOMOFB_DInsQuery.FieldByName('QTY_G').AsString := getString(i+3);
            end;
            if (Qualifier = '3') and (Trim(getString(i+2)) <> '') then //수량소계
            begin
              DOMOFB_DInsQuery.FieldByName('STQTY').AsCurrency := StrToCurr(getString(i+2));
              DOMOFB_DInsQuery.FieldByName('STQTY_G').AsString := getString(i+3);
            end;
          end
          else if SEGMENT('MOA','','28') then
          begin
            Qualifier := getString(i+1);
            if (Qualifier = '203') and (Trim(getString(i+2)) <> '') then //금액
            begin
              DOMOFB_DInsQuery.FieldByName('AMT').AsCurrency := StrToCurr(getString(i+2));
              DOMOFB_DInsQuery.FieldByName('AMT_G').AsString := getString(i+3);
              UNITAMOUNT_UNIT := getString(i+3);
            end;
            if (Qualifier = '17') and (Trim(getString(i+2)) <> '') then //금액소계
            begin
              DOMOFB_DInsQuery.FieldByName('STAMT').AsCurrency := StrToCurr(getString(i+2));
              DOMOFB_DInsQuery.FieldByName('STAMT_G').AsString := getString(i+3);
            end;
          end
          else if SEGMENT('FTX','','29') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'AAA' then  //물품의 규격
            begin
              if DOMOFB_DInsQuery.FieldByName('SIZE1').AsString = '' then
              begin
                DOMOFB_DInsQuery.FieldByName('SIZE1').AsString := getString(i+2)+#13#10+
                                                                      getString(i+3)+#13#10+
                                                                      getString(i+4)+#13#10+
                                                                      getString(i+5)+#13#10+
                                                                      getString(i+6);

              end
              else
              begin
                DOMOFB_DInsQuery.FieldByName('SIZE1').AsString := DOMOFB_DInsQuery.FieldByName('SIZE1').AsString+#13#10+
                                                                      getString(i+2)+#13#10+
                                                                      getString(i+3)+#13#10+
                                                                      getString(i+4)+#13#10+
                                                                      getString(i+5)+#13#10+
                                                                      getString(i+6);
              end;
            end;
          end
          else if SEGMENT('PRI','1','2A') then //품목의 단가관련
          begin
            if Trim(getString(i+2)) <> '' then  //단가
            begin
              DOMOFB_DInsQuery.FieldByName('PRICE').AsCurrency := StrToCurr(getString(i+2));
              IF UNITAMOUNT_UNIT = '' THen UNITAMOUNT_UNIT := 'USD';
              DOMOFB_DInsQuery.FieldByName('PRICE_G').AsString := UNITAMOUNT_UNIT;              
//              DOMOFB_DInsQuery.FieldByName('PRICE_G').AsString := getString(i+3);
            end;
            if Trim(getString(i+5)) <> '' then //단가단위기준
            begin
              DOMOFB_DInsQuery.FieldByName('QTYG').AsCurrency := StrToCurr(getString(i+5)); 
              DOMOFB_DInsQuery.FieldByName('QTYG_G').AsString := getString(i+6);
            end;
          end;
          if (SEGMENT('UNS','1','')) and (getString(i+1) = 'S') then
          begin
              DOMOFB_DInsQuery.Post; //마지막 순번의 내용을 post 시킴
          end
//------------------------------------------------------------------------------------
          else if SEGMENT('RFF','1','1A') then
          begin

          end
          else if SEGMENT('FTX','','1B') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'AAI' then  //기타참조사항
            begin
              if DOMOFB_H1InsQuery.FieldByName('REMARK_1').AsString = '' then
              begin
                DOMOFB_H1InsQuery.FieldByName('REMARK').AsString := 'Y';
                DOMOFB_H1InsQuery.FieldByName('REMARK_1').AsString := getString(i+2)+#13#10+
                                                                      getString(i+3)+#13#10+
                                                                      getString(i+4)+#13#10+
                                                                      getString(i+5)+#13#10+
                                                                      getString(i+6);

              end
              else
              begin
                DOMOFB_H1InsQuery.FieldByName('REMARK_1').AsString := DOMOFB_H1InsQuery.FieldByName('REMARK_1').AsString+#13#10+
                                                                      getString(i+2)+#13#10+
                                                                      getString(i+3)+#13#10+
                                                                      getString(i+4)+#13#10+
                                                                      getString(i+5)+#13#10+
                                                                      getString(i+6);
              end;
            end;
          end
          else if SEGMENT('MOA','1','1C') then //물품의 총금액
          begin
            if Trim(getString(i+2)) <> '' then
            begin
              DOMOFB_H3InsQuery.FieldByName('TAMT').AsCurrency := StrToCurr(getString(i+2));
              DOMOFB_H3InsQuery.FieldByName('TAMTCUR').AsString := getString(i+3);
            end;
          end
          else if SEGMENT('CNT','1','1D') then //물품의 총수량
          begin
            if Trim(getString(i+2)) <> '' then
            begin
              DOMOFB_H3InsQuery.FieldByName('TQTY').AsCurrency := StrToCurr(getString(i+2));
              DOMOFB_H3InsQuery.FieldByName('TQTYCUR').AsString := getString(i+3);
            end;
          end;
        //  ShowMessage(IntToStr(i)+'두번째');
        end; //for end
        //  ShowMessage(IntToStr(i) + '마지막');
        
        DOMOFB_H1InsQuery.Post;
        DOMOFB_H2InsQuery.Post;
        DOMOFB_H3InsQuery.Post;

        Result := True;
      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
        end;
      end;
  finally
    DOMOFB_H1InsQuery.Close;
    DOMOFB_H2InsQuery.Close;
    DOMOFB_H3InsQuery.Close;
    DOMOFB_DInsQuery.Close;
  end;
end;

procedure TDOMOFB.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'DOMOFR';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'DOMOFR';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TDOMOFB.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT);
end;

end.
