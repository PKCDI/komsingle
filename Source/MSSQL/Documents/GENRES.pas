//##############################################################################
//
//     일반응답서(GENRES)
//     2015-03-27
//     MIG데이터를 입력
//
//##############################################################################
unit GENRES;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, sMemo, sLabel, StrUtils, sEdit;

type
  TDOC_GENRES = class(TForm)
    sLabel1: TsLabel;
    sMemo1: TsMemo;
    qryIns: TADOQuery;
    sEdit1: TsEdit;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Run;
  end;

var
  DOC_GENRES: TDOC_GENRES;
Const
  USE_SEG : array [0..7] of String = ('UNH','BGM','FTX','DTM','NAD','RFF','AUT','UNT');
implementation

uses MSSQL, VarDefine;

{$R *.dfm}

{ TDOC_GENRES }

procedure TDOC_GENRES.Run;
var
  DetailList : TStringList;
  NewLineList : TStringList;
  nLoop, nSubLoop : integer;
  SEGMENT : string;
begin

  DetailList := TStringList.Create;
  DetailList.Delimiter := '+';

  NewLineList := TStringList.Create;
  NewLineList.Delimiter := ':';

  try
    qryIns.Close;
    qryIns.Parameters.ParamByName('User_Id').Value := LoginData.sID;
    qryIns.Parameters.ParamByName('Descc').Value := '0';
    qryIns.Parameters.ParamByName('Chk1').Value := '0';
    qryIns.Parameters.ParamByName('Chk2').Value := '';
    qryIns.Parameters.ParamByName('Chk3').Value := '';
    qryIns.Parameters.ParamByName('PRNO').Value := '0';
    qryIns.Parameters.ParamByName('Sunbun').Value := sEdit1.Text; 

    for nLoop := 0 to sMemo1.Lines.Count - 1 do
    begin
      IF LeftStr(sMemo1.Lines.Strings[nLoop],7) = 'FTX+AAP' THEN
        sMemo1.Lines.Strings[nLoop] := AnsiReplaceText(sMemo1.Lines.Strings[nLoop],' ','|');

      DetailList.DelimitedText := sMemo1.Lines.Strings[nLoop];
      SEGMENT := DetailList.Strings[0];

      IF AnsiMatchText(SEGMENT,USE_SEG) Then
      begin
        IF SEGMENT = 'BGM' THEN
        BEGIN
          //BGM+961+GENRES2015031626806+9+RE
          //BGM_CODE  961
          qryIns.Parameters.ParamByName('BGM_CODE').Value := DetailList.Strings[1];

          //MAINT_NO  GENRES2015031626806
          qryIns.Parameters.ParamByName('MAINT_NO').Value := DetailList.Strings[2];
        END
        ELSE
        IF (SEGMENT = 'FTX') THEN
        BEGIN
          IF (UpperCase(DetailList.Strings[1]) = 'AAP') then
          begin
            //FTX+AAP+++MESSAGE
            //Desc_1  MESSAGE
            DetailList.Strings[4] := AnsiReplaceText(AnsiReplaceText(DetailList.Strings[4],'|',' '),':',#13#10);
            qryIns.Parameters.ParamByName('Desc_1').Value := DetailList.Strings[4];
          end
          else
          IF (UpperCase(DetailList.Strings[1]) = 'ALL') then
          begin
            //FTX+ALL+++APPSPC 일반응답과 관련된 서류명표시
            //Doc1
            NewLineList.DelimitedText := DetailList.Strings[4];
            for nSubLoop := 1 to 5 do
            begin
              IF nSubLoop <= NewLineList.Count Then
                qryIns.Parameters.ParamByName('Doc'+IntToStr(nSubLoop)).Value := NewLineList.Strings[nSubLoop-1]
              else
                qryIns.Parameters.ParamByName('Doc'+IntToStr(nSubLoop)).Value := '';
            end;
          end
        end
        ELSE
        IF (SEGMENT = 'DTM') THEN
        BEGIN
          NewLineList.DelimitedText := DetailList.Strings[1];
          IF NewLineList.Strings[0] = '137' Then
          BEGIN
            //DTM+137:150316:101
            //DATEE  발신일자150316
            qryIns.Parameters.ParamByName('Datee').Value := '20'+NewLineList.Strings[1];
          end;
        END
        ELSE
        IF (SEGMENT = 'NAD') THEN
        BEGIN
          IF (UpperCase(DetailList.Strings[1]) = 'MR') THEN
          BEGIN
            //NAD+MR+++NET45  수신인
            //SR_NAME1  수신인
            NewLineList.DelimitedText := DetailList.Strings[4];
            for nSubLoop := 1 to 3 do
            begin
              IF nSubLoop <= NewLineList.Count Then
                qryIns.Parameters.ParamByName('SR_Name'+IntToStr(nSubLoop)).Value := NewLineList.Strings[nSubLoop-1]
              else
                qryIns.Parameters.ParamByName('SR_Name'+IntToStr(nSubLoop)).Value := '';
            end;
          end
          ELSE
          IF (UpperCase(DetailList.Strings[1]) = 'MS') THEN
          BEGIN
            //NAD+MS+++XETEX1 발신인
            //EX_NAME1  발신인
            NewLineList.DelimitedText := DetailList.Strings[4];
            for nSubLoop := 1 to 3 do
            begin
              IF nSubLoop <= NewLineList.Count Then
                qryIns.Parameters.ParamByName('EX_Name'+IntToStr(nSubLoop)).Value := NewLineList.Strings[nSubLoop-1]
              else
                qryIns.Parameters.ParamByName('EX_Name'+IntToStr(nSubLoop)).Value := '';
            end;
          END;
        END
        ELSE
        IF (SEGMENT = 'RFF') THEN
        BEGIN
          NewLineList.DelimitedText := DetailList.Strings[1];
          IF UpperCase(NewLineList.Strings[0]) = 'MR' THEN
          begin
            //RFF+MR:KTNET  수신자번호
            //SR_ID KTNET
            qryIns.Parameters.ParamByName('SR_ID').Value := NewLineList.Strings[1];
          end
          else
          IF UpperCase(NewLineList.Strings[0]) = 'MS' THEN
          begin
            //RFF+MS:XETEX1-450615031100117
            //EX_ID XETEX1-450615031100117
            qryIns.Parameters.ParamByName('EX_ID').Value := NewLineList.Strings[1];
          end
          else
          IF UpperCase(NewLineList.Strings[0]) = 'ACD' THEN
          begin
            //RFF+ACD:20150311L-1
            //XX_ID 20150311L-1
            qryIns.Parameters.ParamByName('XX_ID').Value := NewLineList.Strings[1];
          end;
        END

      end
      else
      begin
        Raise Exception.Create('SEG NOT FOUND'#13#10+DetailList.Strings[0]);
      end;
    end;

    qryIns.ExecSQL;

  finally
    DetailList.Free;
  end;
end;

end.
