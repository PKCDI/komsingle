unit GENRES_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils, RecvParent;

type
  TGENRES_NEW = class(TRecvParent)
    public
      constructor Create(Mig: TStringList);
      function Run_Convert: Boolean; override;
      procedure Run_Ready(bDuplicate: Boolean); override;
  end;
var
  GENRES_NEW_DOC : TGENRES_NEW;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine;

{ TGENRES_NEW }

constructor TGENRES_NEW.Create(Mig: TStringList);
begin
  inherited;
  FDOCID := 'GENRES';
end;

function TGENRES_NEW.Run_Convert: Boolean;
var
  i : Integer;
  SegIDX : Integer;
  KEY_IDX : Integer;
  DocCount : Integer;
begin
  Result := False;

  with InsertQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;

    DMMssql.BeginTrans;
    try
      try
        SQL.Text := SQL_GENRES_MASTER;
        Open;

        InsertQuery.Append;

        SegIDX := GetSegmentIDX('BGM','1','10');
        FMAINT_NO := getString(SegIDX+2);

        DocCount := AlreadyDocumentCount('GENRESS','MAINT_NO',FMAINT_NO);

        IF DocCount > 0 Then
          FMAINT_NO := FMAINT_NO + '_'+FormatFloat('000',DocCount);
          
        FieldByName('MAINT_NO').AsString := FMAINT_NO;
        FieldByName('User_Id').AsString := LoginData.sID;
//        FieldByName('Datee').AsString := FormatDateTime('YYYYMMDD',Now);
        FieldByName('CHK1').AsString := '';
        FieldByName('CHK2').AsString := '';
        FieldByName('CHK3').AsString := '';
        FieldByName('PRNO').AsString := '0';

        //문서코드
        FieldByName('BGM_CODE').AsString := getString(SegIDX+1);


        //전달내역
        SegIDX := GetSegmentIDX('FTX','1','11');
        FieldByName('Desc_1').AsString := getString(SegIDX+2)+#13#10+
                                          getString(SegIDX+3)+#13#10+
                                          getString(SegIDX+4)+#13#10+
                                          getString(SegIDX+5)+#13#10+
                                          getString(SegIDX+6);

        //통보일시
        SegIDX := GetSegmentIDX('DTM','1','12');
        IF SegIDX > -1 THEN
        BEGIN
          FieldByName('Datee').AsString := '20'+getString(SegIDX+2);
        END;

        //수신인/발신인
        for i := 1 to 2 do
        begin
          SegIDX := GetSegmentIDX('NAD',IntToStr(i),'20');
          IF SegIDX = -1 THEN Continue;
          Qualifier := getString(SegIDX+1);
          //수신인
          IF Qualifier = 'MR' Then
          begin
            FieldByName('SR_Name1').AsString := getString(SegIDX+4);
          end;
          //발신인
          IF Qualifier = 'MS' Then
          begin
            FieldByName('EX_Name1').AsString := getString(SegIDX+4);
            FieldByName('EX_Name2').AsString := getString(SegIDX+5);
            FieldByName('EX_Name3').AsString := getString(SegIDX+6);
          end;
        end;

        for i := 1 to 3 do
        begin
          SegIDX := GetSegmentIDX('RFF',IntToStr(i),'21');
          IF SegIDX = -1 THEN Continue;
          Qualifier := getString(SegIDX+1);

          IF Qualifier = 'MR' Then
            FieldByName('SR_Id').AsString := getString(SegIDX+2);
          IF Qualifier = 'MS' Then
            FieldByName('EX_Id').AsString := getString(SegIDX+2);
          IF Qualifier = 'ACD' Then
            FieldByName('XX_Id').AsString := getString(SegIDX+2);
        end;

        SegIDX := GetSegmentIDX('FTX','1','22');
        IF SegIDX > -1 THEN
        BEGIN
          FieldByName('Doc1').AsString := getString(SegIDX+2);
          FieldByName('Doc2').AsString := getString(SegIDX+3);
          FieldByName('Doc3').AsString := getString(SegIDX+4);
          FieldByName('Doc4').AsString := getString(SegIDX+5);
          FieldByName('Doc5').AsString := getString(SegIDX+6);
        END;

        InsertQuery.Post;
        Result := True;
        DMMssql.CommitTrans;
      except
        on E:Exception do
        begin
          DMMssql.RollbackTrans;
          ShowMessage(E.Message);
        end;
      end;
    finally
      InsertQuery.Close;
    end;
  end;
end;

procedure TGENRES_NEW.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
  SegIDX : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];

    Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
    Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
    Parameters.ParamByName('DOCID').Value := 'GENRES';
    Parameters.ParamByName('Mig').Value := FMig.Text;
    Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
    Parameters.ParamByName('CONTROLNO').Value := '';
    Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
    Parameters.ParamByName('CON').Value := '';
    Parameters.ParamByName('DBAPPLY').Value := '';
    Parameters.ParamByName('DOCNAME').Value := 'GENRES';

    SegIDX := GetSegmentIDX('BGM','1','10');
    IF SegIDX > -1 THEN
    BEGIN
      //관리번호 중복 확인
      IF DuplicateData(Trim(FMig.Strings[SegIDX+2])) AND bDuplicate Then
      begin
        FDuplicateDoc := Trim(FMig.Strings[SegIDX+2]);
        FDuplicate := True;
        Exit;
      end
      else
      begin
        FDuplicateDoc := '';
        FDuplicate := false;
        Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[SegIDX+2]);
      end;
    END;


    ExecSQL;
  end;
end;

end.
 