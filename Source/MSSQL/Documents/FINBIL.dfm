object DOC_FINBIL: TDOC_FINBIL
  Left = 379
  Top = 91
  Width = 417
  Height = 363
  Caption = 'DOC_FINBIL'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    401
    324)
  PixelsPerInch = 96
  TextHeight = 15
  object sLabel1: TsLabel
    Left = 10
    Top = 8
    Width = 57
    Height = 15
    Caption = 'MIG DATA'
  end
  object sMemo1: TsMemo
    Left = 10
    Top = 24
    Width = 381
    Height = 257
    Anchors = [akLeft, akTop, akRight]
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Lines.Strings = (
      'UNH+1+FINBIL:1:911:KE'
      'BGM+2AR+S15032715164172+9'
      'BUS+1:2DH'
      'RFF+AAC:ITTC732501084'
      'RFF+ACK:030821500642447'
      'DTM+97:150327:101'
      'DTM+137:150327:101'
      'FII+BC++45AO:25:BOK::::'#54620#44397#50472#54000#51008#54665':'#53580#54756#46976#47196#44592#50629#44552#50997#49468#53552
      'NAD+MR+++'#54620#44397#48148#51060#47536#51452#49885#54924#49324
      'NAD+AX+++'#54620#44397#50472#54000#51008#54665':'#53580#54756#46976#47196#44592#50629#44552#50997#49468#53552':1208542295'
      'PAI+::2AC'
      'MOA+2AD:13132.84:EUR'
      'PAI+::2AD'
      'MOA+2AD:13132.84:EUR'
      'MOA+2AE:15778056:KRW'
      'CUX+++1201.42'
      'FCA+6'
      'MOA+60:15788056.00:KRW'
      'MOA+131:10000.00:KRW'
      'ALC+C+:2BM'
      'MOA+23:10000:KRW'
      'MOA+25:13132.84:EUR'
      'CUX+++1188.67'
      'AUT+2726797920'
      'DTM+218:150327151713:202'
      'UNT+26+1')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    WordWrap = False
    Text = 
      'UNH+1+FINBIL:1:911:KE'#13#10'BGM+2AR+S15032715164172+9'#13#10'BUS+1:2DH'#13#10'RFF' +
      '+AAC:ITTC732501084'#13#10'RFF+ACK:030821500642447'#13#10'DTM+97:150327:101'#13#10 +
      'DTM+137:150327:101'#13#10'FII+BC++45AO:25:BOK::::'#54620#44397#50472#54000#51008#54665':'#53580#54756#46976#47196#44592#50629#44552#50997#49468#53552#13#10'NA' +
      'D+MR+++'#54620#44397#48148#51060#47536#51452#49885#54924#49324#13#10'NAD+AX+++'#54620#44397#50472#54000#51008#54665':'#53580#54756#46976#47196#44592#50629#44552#50997#49468#53552':1208542295'#13#10'PAI+::2' +
      'AC'#13#10'MOA+2AD:13132.84:EUR'#13#10'PAI+::2AD'#13#10'MOA+2AD:13132.84:EUR'#13#10'MOA+2' +
      'AE:15778056:KRW'#13#10'CUX+++1201.42'#13#10'FCA+6'#13#10'MOA+60:15788056.00:KRW'#13#10'M' +
      'OA+131:10000.00:KRW'#13#10'ALC+C+:2BM'#13#10'MOA+23:10000:KRW'#13#10'MOA+25:13132.' +
      '84:EUR'#13#10'CUX+++1188.67'#13#10'AUT+2726797920'#13#10'DTM+218:150327151713:202'#13 +
      #10'UNT+26+1'#13#10
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
  end
  object sEdit1: TsEdit
    Left = 88
    Top = 288
    Width = 121
    Height = 23
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Active = True
    BoundLabel.Caption = #49688#49888#51901' '#49692#48264
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = ANSI_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -12
    BoundLabel.Font.Name = #47569#51008' '#44256#46357
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object qryMIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'Maint_No'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'User_Id'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Datee'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'MESSAGE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'MESSAGE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'BUS'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'BUS_DESC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'DOC_NO1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOC_NO2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOC_NO3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOC_NO4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'TRN_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'ADV_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'TERM'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'PAYMENT'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'SPECIAL1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'SPECIAL2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'REMARK'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'REMARK1'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'BANK'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end
      item
        Name = 'BNKNAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BNKNAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'CUSTNAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CUSTNAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CUSTNAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CUSTSIGN1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CUSTSIGN2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CUSTSIGN3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'PAYORD1'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD11'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD1C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'RATE1'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD21'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD2C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'RATE2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD3'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD31'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD3C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'RATE3'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD4'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD41'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD4C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'RATE4'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD5'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD51'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PAYORD5C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'RATE5'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'FOBRATE'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'RATEIN'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'FINAMT1'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'FINAMT1C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'FINAMT2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'FINAMT2C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'Chk1'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'Chk2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'Chk3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'PRNO'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Sunbun'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO FINBIL_H('
      '[MAINT_NO]'
      ',[USER_ID]'
      ',[DATEE]'
      ',[MESSAGE1]'
      ',[MESSAGE2]'
      ',[BUS]'
      ',[BUS_DESC]'
      ',[DOC_NO1]'
      ',[DOC_NO2]'
      ',[DOC_NO3]'
      ',[DOC_NO4]'
      ',[TRN_DATE]'
      ',[ADV_DATE]'
      ',[TERM]'
      ',[PAYMENT]'
      ',[SPECIAL1]'
      ',[SPECIAL2]'
      ',[REMARK]'
      ',[REMARK1]'
      ',[BANK]'
      ',[BNKNAME1]'
      ',[BNKNAME2]'
      ',[CUSTNAME1]'
      ',[CUSTNAME2]'
      ',[CUSTNAME3]'
      ',[CUSTSIGN1]'
      ',[CUSTSIGN2]'
      ',[CUSTSIGN3]'
      ',[PAYORD1]'
      ',[PAYORD11]'
      ',[PAYORD1C]'
      ',[RATE1]'
      ',[PAYORD2]'
      ',[PAYORD21]'
      ',[PAYORD2C]'
      ',[RATE2]'
      ',[PAYORD3]'
      ',[PAYORD31]'
      ',[PAYORD3C]'
      ',[RATE3]'
      ',[PAYORD4]'
      ',[PAYORD41]'
      ',[PAYORD4C]'
      ',[RATE4]'
      ',[PAYORD5]'
      ',[PAYORD51]'
      ',[PAYORD5C]'
      ',[RATE5]'
      ',[FOBRATE]'
      ',[RATEIN]'
      ',[FINAMT1]'
      ',[FINAMT1C]'
      ',[FINAMT2]'
      ',[FINAMT2C]'
      ',[CHK1]'
      ',[CHK2]'
      ',[CHK3]'
      ',[PRNO]'
      ')VALUES('
      '  :MAINT_NO'
      ', :USER_ID'
      ', :DATEE'
      ', :MESSAGE1'
      ', :MESSAGE2'
      ', :BUS'
      ', :BUS_DESC'
      ', :DOC_NO1'
      ', :DOC_NO2'
      ', :DOC_NO3'
      ', :DOC_NO4'
      ', :TRN_DATE'
      ', :ADV_DATE'
      ', :TERM'
      ', :PAYMENT'
      ', :SPECIAL1'
      ', :SPECIAL2'
      ', :REMARK'
      ', :REMARK1'
      ', :BANK'
      ', :BNKNAME1'
      ', :BNKNAME2'
      ', :CUSTNAME1'
      ', :CUSTNAME2'
      ', :CUSTNAME3'
      ', :CUSTSIGN1'
      ', :CUSTSIGN2'
      ', :CUSTSIGN3'
      ', :PAYORD1'
      ', :PAYORD11'
      ', :PAYORD1C'
      ', :RATE1'
      ', :PAYORD2'
      ', :PAYORD21'
      ', :PAYORD2C'
      ', :RATE2'
      ', :PAYORD3'
      ', :PAYORD31'
      ', :PAYORD3C'
      ', :RATE3'
      ', :PAYORD4'
      ', :PAYORD41'
      ', :PAYORD4C'
      ', :RATE4'
      ', :PAYORD5'
      ', :PAYORD51'
      ', :PAYORD5C'
      ', :RATE5'
      ', :FOBRATE'
      ', :RATEIN'
      ', :FINAMT1'
      ', :FINAMT1C'
      ', :FINAMT2'
      ', :FINAMT2C'
      ', :CHK1'
      ', :CHK2'
      ', :CHK3'
      ', :PRNO'
      ')'
      ''
      'UPDATE R_HST'
      'SET CON = '#39'Y'#39
      '      ,DBAPPLY = '#39'Y'#39
      'WHERE Sunbun = :Sunbun'
      '')
    Left = 16
    Top = 224
  end
  object qryDIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'KEYY'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'CHCODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'BILLNO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CHRATE'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'AMT1'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'AMT1C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'AMT2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'AMT2C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'KRWRATE'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'CHDAY'
        Attributes = [paSigned, paNullable]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'CHDATE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'CHDATE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO FINBIL_D('
      'KEYY'
      ',SEQ'
      ',CHCODE'
      ',BILLNO'
      ',CHRATE'
      ',AMT1'
      ',AMT1C'
      ',AMT2'
      ',AMT2C'
      ',KRWRATE'
      ',CHDAY'
      ',CHDATE1'
      ',CHDATE2'
      ')VALUES('
      ':KEYY'
      ', :SEQ'
      ', :CHCODE'
      ', :BILLNO'
      ', :CHRATE'
      ', :AMT1'
      ', :AMT1C'
      ', :AMT2'
      ', :AMT2C'
      ', :KRWRATE'
      ', :CHDAY'
      ', :CHDATE1'
      ', :CHDATE2)')
    Left = 48
    Top = 224
  end
  object qryBus: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end>
    SQL.Strings = (
      'SELECT CODE,NAME'
      'FROM CODE2NDD'
      'WHERE Prefix LIKE '#39'4025FINBIL'#39
      'AND CODE = :CODE')
    Left = 88
    Top = 224
  end
  object qryFinMaintNo: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      'FROM FINBIL_H'
      'WHERE MAINT_NO LIKE :MAINT_NO')
    Left = 88
    Top = 192
  end
end
