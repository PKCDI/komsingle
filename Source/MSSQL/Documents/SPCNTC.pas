unit SPCNTC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TSPCNTC = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      SPCNTCH_InsQuery  : TADOQuery;
      SPCNTCD_InsQuery  : TADOQuery;

      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  SPCNTC_DOC: TSPCNTC;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine, RecvParent;

{ TSPCNTC }

constructor TSPCNTC.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  SPCNTCH_InsQuery := TADOQuery.Create(nil);
  SPCNTCD_InsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TSPCNTC.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  SPCNTCH_InsQuery.Free;
  SPCNTCD_InsQuery.Free;
  DuplicateQuery.Free;
end;

function TSPCNTC.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('SPCNTC')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TSPCNTC.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TSPCNTC.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TSPCNTC.Run_Convert: Boolean;
var
  i , k , SPCNTCDCount : Integer;
  DocumentCount,TaxSeq : integer;
  Qualifier : string;
  FMAINT_NO : String;
  MEMOString : TStringList;
begin
  Result := false;

  SPCNTCH_InsQuery.Close;
  SPCNTCH_InsQuery.Connection := DMMssql.KISConnect;
  SPCNTCH_InsQuery.SQL.Text := SQL_SPCNTCH;
  SPCNTCH_InsQuery.Open;

  SPCNTCD_InsQuery.Close;
  SPCNTCD_InsQuery.Connection := DMMssql.KISConnect;
  SPCNTCD_InsQuery.SQL.Text := SQL_SPCNTCD;
  SPCNTCD_InsQuery.Open;

  MEMOString := TStringList.Create;
  //APPEND
  try
    try
      SPCNTCH_InsQuery.Append;

      //Default Values
      SPCNTCH_InsQuery.FieldByName('User_Id').AsString := LoginData.sID;
      SPCNTCH_InsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
      SPCNTCH_InsQuery.FieldByName('CHK1').AsString := '';
      SPCNTCH_InsQuery.FieldByName('CHK2').AsString := '';
      SPCNTCH_InsQuery.FieldByName('CHK3').AsString := '';

      //값 초기화
      DocumentCount := 1;

      //분석시작
      for i:= 1 to FMig.Count-1 do
      begin
       // ShowMessage(IntToStr(i));
        //IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        //세금계산서 SPCNTCD_InsQuery를 Post 시키기위하여 UNT 세그먼트를 사용한것
        IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH'] )Then
          Continue
        else
        begin
          SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
          SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
          SEGMENT_NO := RightStr(FMig.Strings[i],2);
        end;
        IF SEGMENT('BGM','1','10') Then
        begin
          SPCNTCH_InsQuery.FieldByName('BGM_GUBUN').AsString := getString(i+1);
          FMAINT_NO := getString(i+5);
          SPCNTCH_InsQuery.FieldByName('MAINT_NO').AsString := FMAINT_NO;
          SPCNTCH_InsQuery.FieldByName('MESSAGE1').AsString := getString(i+6);
          if Trim(getString(i+7)) = '' then
            SPCNTCH_InsQuery.FieldByName('MESSAGE2').AsString := 'AB'
          else
            SPCNTCH_InsQuery.FieldByName('MESSAGE2').AsString := getString(i+7);
        end
        else if SEGMENT('DTM','','11') then
        begin
          Qualifier := getString(i+1);
          //통지일자
          if Qualifier = '691' then
            SPCNTCH_InsQuery.FieldByName('NT_DATE').AsString := '20' + getString(i+2);
          //매입일자
          if Qualifier = '2AD' then
            SPCNTCH_InsQuery.FieldByName('CP_DATE').AsString :=  getString(i+2);
        end
        else if SEGMENT('RFF','','12') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'DM' then  //신청인이 송신한 추심(매입)의뢰서의 전자문서번호(APPSPC의 BGM항목)
            SPCNTCH_InsQuery.FieldByName('DM_NO').AsString := getString(i+2)
          else if Qualifier = 'LC' then //내국신용장 번호
            SPCNTCH_InsQuery.FieldByName('LC_NO').AsString := getString(i+2)
          else if Qualifier = '2BA' then //매입 번호
            SPCNTCH_InsQuery.FieldByName('CP_NO').AsString := getString(i+2)
          else if Qualifier = 'REN' then  //물품수령증명서(인수증) 번호
          begin
            //if문 반복을 대체할수있는 방법 찾아보기
            if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO').AsString) = '' then
            begin
              SPCNTCH_InsQuery.FieldByName('RC_NO').AsString := getString(i+2)
            end
            else if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO').AsString) <> '' then
            begin
              if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO2').AsString) = '' then
              begin
                SPCNTCH_InsQuery.FieldByName('RC_NO2').AsString := getString(i+2)
              end
              else if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO2').AsString) <> '' then
              begin
                if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO3').AsString) = '' then
                begin
                  SPCNTCH_InsQuery.FieldByName('RC_NO3').AsString := getString(i+2)
                end
                else if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO3').AsString) <> '' then
                begin
                  if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO4').AsString) = '' then
                  begin
                    SPCNTCH_InsQuery.FieldByName('RC_NO4').AsString := getString(i+2)
                  end
                  else if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO4').AsString) <> '' then
                  begin
                    if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO5').AsString) = '' then
                    begin
                      SPCNTCH_InsQuery.FieldByName('RC_NO5').AsString := getString(i+2);
                    end
                    else if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO5').AsString) <> '' then
                    begin
                      if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO6').AsString) = '' then
                      begin
                        SPCNTCH_InsQuery.FieldByName('RC_NO6').AsString := getString(i+2);
                      end
                      else if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO6').AsString) <> '' then
                      begin
                        if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO7').AsString) = '' then
                        begin
                          SPCNTCH_InsQuery.FieldByName('RC_NO7').AsString := getString(i+2);
                        end
                        else if Trim(SPCNTCH_InsQuery.FieldByName('RC_NO7').AsString) <> '' then
                        begin
                          SPCNTCH_InsQuery.FieldByName('RC_NO8').AsString := getString(i+2);
                        end;
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end
          else if Qualifier = '2AI' then //세금계산서번호
          begin
            if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO').AsString) = '' then
             begin
              SPCNTCH_InsQuery.FieldByName('FIN_NO').AsString := getString(i+2)
             end
            else if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO').AsString) <> '' then
            begin
              if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO2').AsString) = '' then
              begin
                SPCNTCH_InsQuery.FieldByName('FIN_NO2').AsString := getString(i+2)
              end
              else if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO2').AsString) <> '' then
              begin
                if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO3').AsString) = '' then
                begin
                  SPCNTCH_InsQuery.FieldByName('FIN_NO3').AsString := getString(i+2)
                end
                else if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO3').AsString) <> '' then
                begin
                  if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO4').AsString) = '' then
                  begin
                    SPCNTCH_InsQuery.FieldByName('FIN_NO4').AsString := getString(i+2)
                  end
                  else if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO4').AsString) <> '' then
                  begin
                    if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO5').AsString) = '' then
                    begin
                      SPCNTCH_InsQuery.FieldByName('FIN_NO5').AsString := getString(i+2);
                    end
                    else if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO5').AsString) <> '' then
                    begin
                      if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO6').AsString) = '' then
                      begin
                        SPCNTCH_InsQuery.FieldByName('FIN_NO6').AsString := getString(i+2);
                      end
                      else if Trim(SPCNTCH_InsQuery.FieldByName('FIN_NO6').AsString) <> '' then
                      begin
                        SPCNTCH_InsQuery.FieldByName('FIN_NO7').AsString := getString(i+2);
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end
        else if SEGMENT('FII','1','13') then
        begin //추심매입은행
          SPCNTCH_InsQuery.FieldByName('CP_BANK').AsString := getString(i+6);
          SPCNTCH_InsQuery.FieldByName('CP_BANKNAME').AsString := getString(i+12);
          SPCNTCH_InsQuery.FieldByName('CP_BANKBU').AsString := getString(i+13);
        end
        else if SEGMENT('NAD','','14') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '4AA' then //추심(매입) 신청인의  상호, 대표자명
          begin
            SPCNTCH_InsQuery.FieldByName('APP_NO').AsString := getString(i+2);
            SPCNTCH_InsQuery.FieldByName('APP_SNAME').AsString := getString(i+10);
            SPCNTCH_InsQuery.FieldByName('APP_NAME').AsString := getString(i+11);
          end;
          if Qualifier = 'OM' then //추심(매입)은행 전자서명
          begin
            SPCNTCH_InsQuery.FieldByName('CP_BANKELEC').AsString := getString(i+10);
          end;
        end
        else if SEGMENT('MOA','','15') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '2AD' then //추심(매입) 금액(외화)
          begin
            SPCNTCH_InsQuery.FieldByName('CP_AMTU').AsCurrency := StrToCurr(getString(i+2));
            SPCNTCH_InsQuery.FieldByName('CP_AMTC').AsString := getString(i+3);
          end;
          if Qualifier = '2AE' then //추심(매입) 금액(원화)
          begin
            SPCNTCH_InsQuery.FieldByName('CP_AMT').AsInteger := StrToInt(getString(i+2));
          end;
          if Qualifier = '60' then  //최종지급액
          begin
            SPCNTCH_InsQuery.FieldByName('CP_TOTAMT').AsCurrency := StrToCurr(getString(i+2));
            SPCNTCH_InsQuery.FieldByName('CP_TOTAMTC').AsString := getString(i+3);
          end;
          if Qualifier = '131' then //수수료(이자)합계
          begin
            SPCNTCH_InsQuery.FieldByName('CP_TOTCHARGE').AsCurrency := StrToCurr(getString(i+2));
            SPCNTCH_InsQuery.FieldByName('CP_TOTCHARGEC').AsString := getString(i+3);
          end;
        end
        else if SEGMENT('FTX','','16') then
        begin
          if getString(i+1) = 'ZZZ' then
          begin //통보내용
            if SPCNTCH_InsQuery.FieldByName('CP_FTX1').AsString = '' then
            begin
              SPCNTCH_InsQuery.FieldByName('CP_FTX1').AsString := getString(i+6)+#13#10+
                                                                  getString(i+7)+#13#10+
                                                                  getString(i+8)+#13#10+
                                                                  getString(i+9)+#13#10+
                                                                  getString(i+10);
            end
            else
            begin
              SPCNTCH_InsQuery.FieldByName('CP_FTX1').AsString := SPCNTCH_InsQuery.FieldByName('CP_FTX1').AsString+#13#10+
                                                                  getString(i+6)+#13#10+
                                                                  getString(i+7)+#13#10+
                                                                  getString(i+8)+#13#10+
                                                                  getString(i+9)+#13#10+
                                                                  getString(i+10);
            end;
          end;
        end;
        //세금계산서------------------------------------------------------------
        //ALC-------------------------------------------------------------------
        if SEGMENT('ALC','','17') then //수수료(이자)유형
        begin
          if SEGMENT_COUNT <> '1' then
            SPCNTCD_InsQuery.Post;

          SPCNTCD_InsQuery.Append;
          
          SPCNTCD_InsQuery.FieldByName('KEYY').AsString := FMAINT_NO;
          SPCNTCD_InsQuery.FieldByName('CHARGE_NO').AsString := getString(i+2);
          SPCNTCD_InsQuery.FieldByName('CHARGE_TYPE').AsString := getString(i+3);
          SPCNTCD_InsQuery.FieldByName('SEQ').AsString := SEGMENT_COUNT;
          ShowMessage(SEGMENT_COUNT);
        end
        //PCD-------------------------------------------------------------------
        else if SEGMENT('PCD','','20') then //적용요율
        begin
          Qualifier := getString(i+1);
          if Qualifier = '2' then
          begin
            SPCNTCD_InsQuery.FieldByName('CHARGE_RATE').AsString := getString(i+2);
          end;
        end
        //MOA-------------------------------------------------------------------
        else if SEGMENT('MOA','','21') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '25' then  //대상금액
          begin
            SPCNTCD_InsQuery.FieldByName('BAS_AMT').AsCurrency := StrToCurr(getString(i+2));
            SPCNTCD_InsQuery.FieldByName('BAS_AMTC').AsString := getString(i+3);
          end;
          if Qualifier = '23' then //산출금액
          begin
            SPCNTCD_InsQuery.FieldByName('CHA_AMT').AsCurrency := StrToCurr(getString(i+2));
            SPCNTCD_InsQuery.FieldByName('CHA_AMTC').AsString := getString(i+3);
          end;
        end
        //CUX-------------------------------------------------------------------
        //적용환율
        else if SEGMENT('CUX','1','22') then
        begin
          SPCNTCD_InsQuery.FieldByName('CUX_RATE').AsCurrency := StrToCurr(getString(i+9));
        end
        //DTM-------------------------------------------------------------------
        else if SEGMENT('DTM','','23') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '221' then //적용일수
            SPCNTCD_InsQuery.FieldByName('SES_DAY').AsString := getString(i+2);
          if Qualifier = '257' then //적용기간
          begin
            SPCNTCD_InsQuery.FieldByName('SES_SDATE').AsString := '20' + Trim(LeftStr(getString(i+2),6));
            SPCNTCD_InsQuery.FieldByName('SES_EDATE').AsString := '20' + Trim(RightStr(getString(i+2),6));
          end;
        end
        //UNT------------------------------------------------------------------
        else if SEGMENT('UNT','','') then
        begin
          SPCNTCD_InsQuery.Post;
        end;
      end; //for end
      SPCNTCH_InsQuery.Post;
      Result := True;
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SPCNTCH_InsQuery.Close;
    SPCNTCD_InsQuery.Close;
  end;
end;

procedure TSPCNTC.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'SPCNTC';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'SPCNTC';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+5]);
        end;
      end;
    end;
    ExecSQL;
  end;

end;

function TSPCNTC.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT);
end;

end.
