unit LOCAMA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TLOCAMA = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      LOCAMA_InsQuery  : TADOQuery;

      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  LOCAMA_DOC: TLOCAMA;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine, RecvParent;

{ TLOCAMA }

constructor TLOCAMA.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  LOCAMA_InsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TLOCAMA.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  LOCAMA_InsQuery.Free;
  DuplicateQuery.Free;
end;

function TLOCAMA.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCNAME = '+QuotedStr('LOCAMA')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TLOCAMA.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TLOCAMA.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TLOCAMA.Run_Convert: Boolean;
var
  i , countIdx : Integer;
  DocumentCount : integer;
  Qualifier : string;
  DocGubun : String;
  ILGubun : string;
begin
  Result := false;

  LOCAMA_InsQuery.Close;
  LOCAMA_InsQuery.Connection := DMMssql.KISConnect;
  LOCAMA_InsQuery.SQL.Text := SQL_LOCAMA;
  LOCAMA_InsQuery.Open;

  //APPEND
  try
      try
        LOCAMA_InsQuery.Append;

        
        //Default Values
        LOCAMA_InsQuery.FieldByName('User_Id').AsString := LoginData.sID;
        LOCAMA_InsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
        LOCAMA_InsQuery.FieldByName('CHK1').AsString := '';
        LOCAMA_InsQuery.FieldByName('CHK2').AsString := '';
        LOCAMA_InsQuery.FieldByName('CHK3').AsString := '';
        LOCAMA_InsQuery.FieldByName('PRNO').AsString := '0';

        //값 초기화
        DocumentCount := 1;

        //분석시작
        for i:= 1 to FMig.Count-1 do
        begin
         // ShowMessage(IntToStr(i));
          IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
            Continue
          else
          begin
            SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
            SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
            SEGMENT_NO := RightStr(FMig.Strings[i],2);
          end;
          IF SEGMENT('BGM','1','10') Then
          begin
            LOCAMA_InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
            LOCAMA_InsQuery.FieldByName('MESSAGE1').AsString := getString(i+3);
            LOCAMA_InsQuery.FieldByName('MESSAGE2').AsString := getString(i+4);
          end
          else if SEGMENT('RFF','','11') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'DM' then //신청번호
            begin
              LOCAMA_InsQuery.FieldByName('BGM_REF').AsString := getString(i+2);
            end;
            if Qualifier = 'LC' then //내국신용장번호
            begin
              LOCAMA_InsQuery.FieldByName('LC_NO').AsString := getString(i+2);
            end;
          end;
          for countIdx := 3 to 12 do  //최종 물품매도확약서번호
          begin
            if SEGMENT('RFF',IntToStr(countIdx),'11') then
            begin
              LOCAMA_InsQuery.FieldByName('OFFERNO'+IntToStr(countIdx-2)).AsString := getString(i+2);
            end;
          end;
          if SEGMENT('DTM','','12') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '137' then //조건변경통지일자
            begin
              LOCAMA_InsQuery.FieldByName('ADV_DATE').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '2AB' then //조건변경일자
            begin
              LOCAMA_InsQuery.FieldByName('AMD_DATE').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '182' then //개설일자
            begin
              LOCAMA_InsQuery.FieldByName('ISS_DATE').AsString := '20' + getString(i+2);
            end;
          end
          else if SEGMENT('FTX','1','13') then // 기타정보
          begin
            if LOCAMA_InsQuery.FieldByName('REMARK1').AsString = '' then
              begin
                LOCAMA_InsQuery.FieldByName('REMARK').AsString := 'Y';
                LOCAMA_InsQuery.FieldByName('REMARK1').AsString := getString(i+2)+#13#10+
                                                                   getString(i+3)+#13#10+
                                                                   getString(i+4)+#13#10+
                                                                   getString(i+5)+#13#10+
                                                                   getString(i+6);
              end;
          end
          else if SEGMENT('FII','1','14') then //개설은행
          begin
             LOCAMA_InsQuery.FieldByName('ISSBANK').AsString := getString(i+2);
             LOCAMA_InsQuery.FieldByName('ISSBANK1').AsString := getString(i+5);
             LOCAMA_InsQuery.FieldByName('ISSBANK2').AsString := getString(i+6);
          end
          else if SEGMENT('NAD','','15') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = 'DF' then  // 개설의뢰인
            begin
              LOCAMA_InsQuery.FieldByName('APPLIC1').AsString := getString(i+7);
              LOCAMA_InsQuery.FieldByName('APPLIC2').AsString := getString(i+8);
              LOCAMA_InsQuery.FieldByName('APPLIC3').AsString := getString(i+9);
              LOCAMA_InsQuery.FieldByName('APPADDR1').AsString := getString(i+10);
              LOCAMA_InsQuery.FieldByName('APPADDR2').AsString := getString(i+11);
              LOCAMA_InsQuery.FieldByName('APPADDR3').AsString := getString(i+12);
            end;
            if Qualifier = 'DG' then //수혜자
            begin
              LOCAMA_InsQuery.FieldByName('BENEFC1').AsString := getString(i+7);
              LOCAMA_InsQuery.FieldByName('BENEFC2').AsString := getString(i+8);
              LOCAMA_InsQuery.FieldByName('BENEFC3').AsString := getString(i+9);
              LOCAMA_InsQuery.FieldByName('BNFADDR1').AsString := getString(i+10);
              LOCAMA_InsQuery.FieldByName('BNFADDR2').AsString := getString(i+11);
              LOCAMA_InsQuery.FieldByName('BNFADDR3').AsString := getString(i+12);
            end;
            if Qualifier = 'AX' then //전자서명
            begin
              LOCAMA_InsQuery.FieldByName('EXNAME1').AsString := getString(i+7);
              LOCAMA_InsQuery.FieldByName('EXNAME2').AsString := getString(i+8);
              LOCAMA_InsQuery.FieldByName('EXNAME3').AsString := getString(i+9);
            end;
          end
          else if SEGMENT('RFF','1','16') then //조건변경횟수
          begin
            LOCAMA_InsQuery.FieldByName('AMD_NO').AsString := getString(i+2);
          end
          else if SEGMENT('DTM','','20') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2' then //변경후 물품인도기일
            begin
              LOCAMA_InsQuery.FieldByName('DELIVERY').AsString := '20' + getString(i+2);
            end;
            if Qualifier = '123' then //변경후 유효기일
            begin
              LOCAMA_InsQuery.FieldByName('EXPIRY').AsString := '20' + getString(i+2);
            end;
          end
          else if SEGMENT('FTX','','21') then //기타 조건변경사항
          begin
            if LOCAMA_InsQuery.FieldByName('CHGINFO1').AsString = '' then
            begin
              LOCAMA_InsQuery.FieldByName('CHGINFO').AsString := 'Y';
              LOCAMA_InsQuery.FieldByName('CHGINFO1').AsString := getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end
            else
            begin
              LOCAMA_InsQuery.FieldByName('CHGINFO1').AsString := LOCAMA_InsQuery.FieldByName('CHGINFO1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end
          else if SEGMENT('BUS','1','22') then //내국신용장 종류
          begin
            LOCAMA_InsQuery.FieldByName('LOC_TYPE').AsString := getString(i+1);
          end
          else if SEGMENT('MOA','','30') then
          begin
            Qualifier := getString(i+1);
            if Qualifier = '2AD' then //변경후 외화금액
            begin
              LOCAMA_InsQuery.FieldByName('LOC1AMT').AsCurrency := StrToCurr(getString(i+2));
              LOCAMA_InsQuery.FieldByName('LOC1AMTC').AsString := getString(i+3);
            end;
            if Qualifier = '2AE' then //변경후 원화금액
            begin
              LOCAMA_InsQuery.FieldByName('LOC2AMT').AsCurrency := StrToCurr(getString(i+2));
              LOCAMA_InsQuery.FieldByName('LOC2AMTC').AsString := getString(i+3);
            end;
          end
          else if SEGMENT('PCD','1','31') then //허용오차
          begin
            LOCAMA_InsQuery.FieldByName('CD_PERP').AsString := LeftStr(getString(i+2), Pos('.',getString(i+2))-1);
            LOCAMA_InsQuery.FieldByName('CD_PERM').AsString := MidStr(getString(i+2),Pos('.',getString(i+2))+1,3);
          end
          else if SEGMENT('CUX','1','') then //매매기준율
          begin
            LOCAMA_InsQuery.FieldByName('EX_RATE').AsCurrency := StrToCurr(getString(i+1));
          end;
        end;

        LOCAMA_InsQuery.Post;

        Result := True;
      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
        end;
      end;
  finally
    LOCAMA_InsQuery.Close;
  end;
end;

procedure TLOCAMA.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'LOCAMA';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'LOCAMA';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TLOCAMA.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT);
end;

end.
 