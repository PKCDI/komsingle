//##############################################################################
//
//     세금계산서(FINBIL)
//     2015-03-30
//     MIG데이터를 입력
//
//##############################################################################
unit FINBIL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, sEdit, sMemo, sLabel, StrUtils, DateUtils;

type
  TDOC_FINBIL = class(TForm)
    sLabel1: TsLabel;
    sMemo1: TsMemo;
    sEdit1: TsEdit;
    qryMIns: TADOQuery;
    qryDIns: TADOQuery;
    qryBus: TADOQuery;
    qryFinMaintNo: TADOQuery;
  private
    { Private declarations }
    procedure MParam(ParamName : String; Value : Variant);
    procedure DParam(ParamName : String; Value : Variant);
//    procedure ParameterReset( var qry : TADOQuery);
    function GetNo(MAINT_NO : string):String;
  public
    { Public declarations }
    procedure Run;
  end;

var
  DOC_FINBIL: TDOC_FINBIL;
Const
  USE_SEG : array [0..18] of String = ('UNH','BGM','BUS','RFF','DTM','PAT','ALI','FTX','FII','COM','NAD','PAI','PCD','MOA','CUX','FCA','ALC','AUT','UNT');
//중복 DTM COM PCD MOA CUX

implementation

uses Commonlib, VarDefine;

{$R *.dfm}

{ TDOC_FINBIL }
procedure TDOC_FINBIL.DParam(ParamName: String; Value: Variant);
begin
  qryDIns.Parameters.ParamByName(ParamName).Value := Value;
end;

function TDOC_FINBIL.GetNo(MAINT_NO : string):String;
begin
  Result := '';
  with qryFinMaintNo do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := MAINT_NO+'%';
    Open;
    IF qryFinMaintNo.RecordCount > 0 Then
      Result := '_'+FormatFloat('000',qryFinMaintNo.RecordCount);
    Close;
  end;
end;

procedure TDOC_FINBIL.MParam(ParamName: String; Value: Variant);
begin
  qryMIns.Parameters.ParamByName(ParamName).Value := Value;
end;

{procedure TDOC_FINBIL.ParameterReset(var qry: TADOQuery);
var
  nLoop : integer;
begin
  for nLoop := 0 to qry.Parameters.Count - 1 do
  begin
    IF paNullable in qry.Parameters[nLoop].Attributes Then
    begin
      qry.Parameters[nLoop].Value := Null;
    end
    else
    begin
      if qry.Parameters[nLoop].DataType IN [ftsmallint, ftBCD, ftfloat, ftinteger] then
        qry.Parameters[nLoop].Value := 0
      else
        qry.Parameters[nLoop].Value := '';
    end;
  end;
end;}

procedure TDOC_FINBIL.Run;
var
  DetailList : TStringList;
  nLoop, nSubLoop : integer;
  SEGMENT : string;
  MAINT_NO, BUS, DTM : String;
  DetailSEQ, TempSeq, PAI : Integer;
begin
  DetailList := TStringList.Create;
  DetailList.Delimiter := '+';

  DetailSEQ := 0;
  PAI := -1;

  try
    MParam('USER_ID',LoginData.sID);
    MParam('DATEE',FormatDateTime('YYYYMMDD',Now));
    MParam('Sunbun',sEdit1.Text);

    for nLoop := 0 to sMemo1.Lines.Count -1 do
    begin
      DetailList.DelimitedText := sMemo1.Lines.Strings[nLoop];
      SEGMENT := UpperCase(DetailList.Strings[0]);

      IF AnsiMatchText(SEGMENT,USE_SEG) then
      begin
        IF SEGMENT = 'BGM' THEN
        BEGIN
          //BGM+2AR+S15032715164172+9
          MAINT_NO := DetailList.Strings[2];
          MAINT_NO := MAINT_NO+GetNo(MAINT_NO);
          MParam('MAINT_NO',MAINT_NO);
          Case DetailList.Count of
            4:
            begin
              MParam('MESSAGE1',DetailList.Strings[3]);
              MParam('MESSAGE2','NA');
            end;

            5:
            begin
              MParam('MESSAGE1',DetailList.Strings[3]);
              MParam('MESSAGE2',DetailList.Strings[4]);
            end;
          end;
        end
        else
        IF SEGMENT = 'BUS' Then
        begin
          //BUS+1:2DH
          BUS := DivideColon(DetailList.Strings[1],1);
          MParam('BUS',BUS);

          with qryBus do
          begin
            Close;
            Parameters.ParamByName('CODE').Value := BUS;
            Open;

            IF qryBus.RecordCount > 0 Then
              MParam('BUS_DESC',qryBus.FieldByName('NAME').AsString)
            Else
              MParam('BUS_DESC','');

            Close;
          end;
        end
        Else
        if SEGMENT = 'RFF' Then
        begin
          //RFF+AAC:ITTC732501084
          //RFF+ACK:030821500642447
          TempSeq := AnsiIndexText(DivideColon(DetailList.Strings[1],0),['AAC','DM','ACK','ACD'])+1;
          MParam('DOC_NO'+IntToStr(TempSeq),DivideColon(DetailList.Strings[1],1));
        end
        else
        if SEGMENT = 'DTM' Then
        begin
          //DTM+97:150327:101
          //DTM+137:150327:101
          BUS := DivideColon(DetailList.Strings[1],0);
          DTM := '20'+DivideColon(DetailList.Strings[1],1);
          IF BUS = '97' Then
            MParam('TRN_DATE',DTM)
          ELSE
          IF BUS = '137' THEN
            MPARAM('ADV_DATE',DTM);
        end
        else
        IF SEGMENT = 'PAT' Then
        begin
          //PAT+1+2AB+sAG
          Case DetailList.Count of
            2:
            begin
              MParam('TERM','');
              MParam('PAYMENT','');
            end;
            3:
            begin
              MParam('TERM',DetailList.Strings[2]);
              MParam('PAYMENT','');
            end;
            4:
            begin
              MParam('TERM',DetailList.Strings[2]);
              MParam('PAYMENT',DetailList.Strings[3]);
            end;
          end;
        end
        else
        if SEGMENT = 'ALI' then
        begin
          //ALI+++2AF+2AH
          MParam('SPECIAL1',DetailList.Strings[3]);
          MParam('SPECIAL2',DetailList.Strings[4]);
        end
        else
        if SEGMENT = 'FTX' Then
        begin
          //FTX+ACB+++기타번호는 수표번호를 의미
          IF DetailList.Strings[1] = 'ACB' Then
            MParam('REMARK1',DivideColon(DetailList.Strings[4],-1));
        end
        else
        if SEGMENT = 'FII' Then
        begin
          //FII+BC++45AO:25:BOK::::한국씨티은행:테헤란로기업금융센터
          MParam('BANK',DivideColon( DetailList.Strings[3] , 0 ) );
          MParam('BNKNAME1',DivideColon( DetailList.Strings[3] , 6 ) );
          MParam('BNKNAME2',DivideColon( DetailList.Strings[3] , 7 ) );
        end
        else
        IF SEGMENT = 'NAD' THEN
        begin
          Case AnsiIndexText(DetailList.Strings[1],['MR','AX']) of
            0:
            begin
              //거래고객
              //NAD+MR
              MParam('CUSTNAME1',DivideColon(DetailList.Strings[4],0));
              MParam('CUSTNAME2','');
              MParam('CUSTNAME3','');
            end;

            1:
            begin
              //전자서명
              MParam('CUSTSIGN1',DivideColon(DetailList.Strings[4],0));
              MParam('CUSTSIGN2',DivideColon(DetailList.Strings[4],1));
              MParam('CUSTSIGN3',DivideColon(DetailList.Strings[4],2));
            end;
          end;
        end
        else
        //여기서 부터 SEGMENT GROUP 3-------------------------------------------
        if SEGMENT = 'PAI' THEN
        begin
          PAI := AnsiIndexText( DivideColon(DetailList.Strings[1],2) , ['2AC','2AD','2AE','2AF','2AG'] ) + 1;
        end
        else
        if (SEGMENT = 'PCD') AND (PAI = 5) Then
        begin
          //수입보증금에 사용하는 FOB환산률
          //PCD+2AA:1.0000  FOB환산률
          //PCD+2AB:10      보증금적립률
          IF DivideColon(DetailList.Strings[1],0) = '2AA' THEN
          BEGIN
            MParam('FOBRATE',DivideColon(DetailList.Strings[1],1));
          end
          Else
          IF DivideColon(DetailList.Strings[1],0) = '2AB' THEN
          BEGIN
            MParam('RATEIN',DivideColon(DetailList.Strings[1],1));
          end;
        end
        else
        if SEGMENT = 'MOA' THEN
        begin
          IF DivideColon(DetailList.Strings[1],0) = '2AD' Then
          begin
            //MOA+2AD:26000.00:USD
            //단위
            MParam('PAYORD'+IntToStr(PAI)+'C' , DivideColon(DetailList.Strings[1],2) );
            //외화금액
            MParam('PAYORD'+IntToStr(PAI) , DivideColon(DetailList.Strings[1],1) );
          end
          Else
          IF DivideColon(DetailList.Strings[1],0) = '2AE' Then
          begin
            //MOA+2AE:21970000:KRW
            //원화금액
            MParam('PAYORD'+IntToStr(PAI)+'1', DivideColon(DetailList.Strings[1],1) );
          end
          else
          IF DivideColon(DetailList.Strings[1],0) = '60' Then
          begin
            //SEG GROUP 4
            //최종지급(수입)액
            //MOA+60:1640700:KRW
            MParam('FINAMT1',DivideColon(DetailList.Strings[1],1) );
            MParam('FINAMT1C',DivideColon(DetailList.Strings[1],2) );
          end
          else
          IF DivideColon(DetailList.Strings[1],0) = '131' Then
          begin
            //SEG GROUP 4          
            //수수료(이자)합계
            //MOA+131:21970000:KRW
            MParam('FINAMT2',DivideColon(DetailList.Strings[1],1) );
            MParam('FINAMT2C',DivideColon(DetailList.Strings[1],2) );
          end
          else
          IF DivideColon( DetailList.Strings[1] , 0 ) = '25' Then
          begin
            //대상금액
            //MOA+25:26000.00:USD
            DParam('AMT1', DivideColon( DetailList.Strings[1] , 1 ) );
            DParam('AMT1C', DivideColon( DetailList.Strings[1] , 2 ) );
          end
          else
          IF DivideColon( DetailList.Strings[1] , 0 ) = '23' Then
          begin
            //산출금액
            //MOA+23:103.22:USD
            DParam('AMT2', DivideColon( DetailList.Strings[1] , 1 ) );
            DParam('AMT2C', DivideColon( DetailList.Strings[1] , 2 ) );
          end;
        end
        Else
        iF SEGMENT = 'CUX' Then
        begin
          //적용환율
          //CUX+++845.00
          MParam('RATE'+IntToStr(PAI),DetailList.Strings[3] );
        end
        else
        if SEGMENT = 'FCA' Then
        begin
          //수수료부담자
          //FCA+6
        end
        else
        //----------------------------------------------------------------------
        //Segment Group 5
        //----------------------------------------------------------------------
        if SEGMENT = 'ALC' THEN
        begin
          IF DetailSEQ > 0 Then
          begin
          //ALC가 나오면 이전에 저장해준 Parameters를 INSERT
            qryDIns.ExecSQL;
          //Parameter Reset
            ParameterReset( qryDIns );
          end;
          //수수료(이자) 유형
          //ALC+C+:2AM
          Inc(DetailSEQ);
          DParam('KEYY',MAINT_NO );
          DParam('SEQ', DetailSEQ );
          DParam('BILLNO',DivideColon( DetailList.Strings[2] , 0 ));            
          DParam('CHCODE',DivideColon( DetailList.Strings[2] , 1 ));
        end
        else
        if SEGMENT = 'PCD' THEN
        begin
          //적용요율
          //PCD+2:7.8750
          DParam('CHRATE',DivideColon(DetailList.Strings[1] , 1) );
        end
        else
        if SEGMENT = 'CUX' then
        begin
          //적용환율
          //CUX+++845.00
          DParam('KRWRATE', DetailList.Strings[3] );
        end
        else
        if SEGMENT = 'DTM' then
        begin
          IF DivideColon( DetailList.Strings[1] , 0 ) = '221' Then
            //적용일수
            //DMT+221:90:804
            DParam('CHDAY', DivideColon( DetailList.Strings[1] , 1 ) )
          else
          IF DivideColon( DetailList.Strings[1] , 0 ) = '257' Then
          begin
            //적용일자
            //DMT+257:980907981206:717
            DParam('CHDATE1', LeftStr( DivideColon( DetailList.Strings[1] , 1) , 6 ) );
            DParam('CHDATE2', RightStr( DivideColon( DetailList.Strings[1] , 1) , 6 ) );
          end;
        end;

      end
      else
      begin
        Raise Exception.Create('SEG NOT FOUND'#13#10+SEGMENT);
      end;
    end;

    IF DetailSEQ > 0 Then
      qryDIns.ExecSQL;
      
    qryMIns.ExecSQL;

  finally
    DetailList.Free;
  end;

end;

end.
