unit VATBI2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TVATBI2 = class
    private
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      VATBI2H_InsQuery  : TADOQuery;
      VATBI2D_InsQuery  : TADOQuery;

      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  VATBI2_DOC: TVATBI2;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine, RecvParent;

{ TVATBI2 }

constructor TVATBI2.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  VATBI2H_InsQuery := TADOQuery.Create(nil);
  VATBI2D_InsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TVATBI2.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  VATBI2H_InsQuery.Free;
  VATBI2D_InsQuery.Free;
  DuplicateQuery.Free;
end;

function TVATBI2.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('VATBI2')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TVATBI2.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TVATBI2.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TVATBI2.Run_Convert: Boolean;
var
  i , countIdx : Integer;
  DocumentCount : integer;
  Qualifier : string;
  DocGubun : String;
  IMDGubun : String;
  MOAGubun : String;
  FMAINTNO : String;
begin
  Result := false;

  VATBI2H_InsQuery.Close;
  VATBI2H_InsQuery.Connection := DMMssql.KISConnect;
  VATBI2H_InsQuery.SQL.Text := SQL_VATBI2H;
  VATBI2H_InsQuery.Open;

  VATBI2D_InsQuery.Close;
  VATBI2D_InsQuery.Connection := DMMssql.KISConnect;
  VATBI2D_InsQuery.SQL.Text := SQL_VATBI2D;
  VATBI2D_InsQuery.Open;

  //APPEND
  try
    try
      VATBI2H_InsQuery.Append;

      //Default Values
      VATBI2H_InsQuery.FieldByName('User_Id').AsString := LoginData.sID;
      VATBI2H_InsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
      VATBI2H_InsQuery.FieldByName('CHK1').AsString := '';
      VATBI2H_InsQuery.FieldByName('CHK2').AsString := '';
      VATBI2H_InsQuery.FieldByName('CHK3').AsString := '';
      VATBI2H_InsQuery.FieldByName('PRNO').AsString := '0';
      VATBI2H_InsQuery.FieldByName('MESSAGE1').AsString := '9';

      //값 초기화
      DocumentCount := 1;

      //분석시작
      for i:= 1 to FMig.Count-1 do
      begin
       // ShowMessage(IntToStr(i));
        IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
          Continue
        else
        begin
          SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
          SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
          SEGMENT_NO := RightStr(FMig.Strings[i],2);
        end;
        IF SEGMENT('BGM','1','10') Then
        begin
          VATBI2H_InsQuery.FieldByName('VAT_CODE').AsString := getString(i+1);
          VATBI2H_InsQuery.FieldByName('VAT_TYPE').AsString := getString(i+2);
          VATBI2H_InsQuery.FieldByName('MAINT_NO').AsString := getString(i+3);
          FMAINTNO := getString(i+3);
          VATBI2H_InsQuery.FieldByName('MESSAGE1').AsString := getString(i+4);
          VATBI2H_InsQuery.FieldByName('MESSAGE2').AsString := getString(i+5);
        end
        else if SEGMENT('GIS','1','11') then
        begin
          VATBI2H_InsQuery.FieldByName('NEW_INDICATOR').AsString := getString(i+2);
        end
        else if SEGMENT('RFF','','12') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'RE' then  //세금계산서의 책번호의 권
          begin
            VATBI2H_InsQuery.FieldByName('RE_NO').AsString := getString(i+2)
          end;
          if Qualifier = 'SE' then //세금계산서의 책번호의 호
          begin
            VATBI2H_InsQuery.FieldByName('SE_NO').AsString := getString(i+2);
          end;
          if Qualifier = 'FS' then //세금계산서의 일련번호
          begin
            VATBI2H_InsQuery.FieldByName('FS_NO').AsString := getString(i+2);
          end;
          if Qualifier = 'ACE' then //세금계산서의 관련참조번호
          begin
            VATBI2H_InsQuery.FieldByName('ACE_NO').AsString := getString(i+2);
          end;
          if Qualifier = 'DM' then //마이너스 세금계산서
          begin
            VATBI2H_InsQuery.FieldByName('RFF_NO').AsString := getString(i+2);
          end;
        end
        else if SEGMENT('NAD','','13') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'SE' then //물품공급자
          begin
            VATBI2H_InsQuery.FieldByName('SE_SAUP').AsString := getString(i+2); //사업자등록번호
            VATBI2H_InsQuery.FieldByName('SE_ADDR1').AsString := getString(i+4); //주소
            VATBI2H_InsQuery.FieldByName('SE_ADDR2').AsString := getString(i+5);
            VATBI2H_InsQuery.FieldByName('SE_ADDR3').AsString := getString(i+6);
            VATBI2H_InsQuery.FieldByName('SE_ADDR4').AsString := getString(i+7);
            VATBI2H_InsQuery.FieldByName('SE_ADDR5').AsString := getString(i+8);
            VATBI2H_InsQuery.FieldByName('SE_NAME1').AsString := getString(i+9); //상호
            VATBI2H_InsQuery.FieldByName('SE_NAME2').AsString := getString(i+10);
            VATBI2H_InsQuery.FieldByName('SE_NAME3').AsString := getString(i+11); //대표자명
            VATBI2H_InsQuery.FieldByName('SE_SAUP1').AsString := getString(i+12); //종사업장번호
          end;
          if Qualifier = 'BY' then //공급받는자
          begin
            VATBI2H_InsQuery.FieldByName('BY_SAUP').AsString := getString(i+2); //사업자등록번호
            VATBI2H_InsQuery.FieldByName('BY_SAUP_CODE').AsString := getString(i+3);
            VATBI2H_InsQuery.FieldByName('BY_ADDR1').AsString := getString(i+4); //주소
            VATBI2H_InsQuery.FieldByName('BY_ADDR2').AsString := getString(i+5);
            VATBI2H_InsQuery.FieldByName('BY_ADDR3').AsString := getString(i+6);
            VATBI2H_InsQuery.FieldByName('BY_ADDR4').AsString := getString(i+7);
            VATBI2H_InsQuery.FieldByName('BY_ADDR5').AsString := getString(i+8);
            VATBI2H_InsQuery.FieldByName('BY_NAME1').AsString := getString(i+9); //상호
            VATBI2H_InsQuery.FieldByName('BY_NAME2').AsString := getString(i+10);
            VATBI2H_InsQuery.FieldByName('BY_NAME3').AsString := getString(i+11); //대표자명
            VATBI2H_InsQuery.FieldByName('BY_SAUP1').AsString := getString(i+12); //종사업장번호
          end;
          if Qualifier = 'AG' then //수탁자
          begin
            VATBI2H_InsQuery.FieldByName('AG_SAUP').AsString := getString(i+2); //사업자등록번호
            VATBI2H_InsQuery.FieldByName('AG_ADDR1').AsString := getString(i+4); //주소
            VATBI2H_InsQuery.FieldByName('AG_ADDR2').AsString := getString(i+5);
            VATBI2H_InsQuery.FieldByName('AG_ADDR3').AsString := getString(i+6);
            VATBI2H_InsQuery.FieldByName('AG_ADDR4').AsString := getString(i+7);
            VATBI2H_InsQuery.FieldByName('AG_ADDR5').AsString := getString(i+8);
            VATBI2H_InsQuery.FieldByName('AG_NAME1').AsString := getString(i+9); //상호
            VATBI2H_InsQuery.FieldByName('AG_NAME2').AsString := getString(i+10);
            VATBI2H_InsQuery.FieldByName('AG_NAME3').AsString := getString(i+11); //대표자명
            VATBI2H_InsQuery.FieldByName('AG_SAUP1').AsString := getString(i+12); //종사업장번호
          end;
        end
        else if SEGMENT('FTX','1','20') then
        begin
          if Qualifier = 'SE' then //공급자의 담당부서 및 담당자
          begin
            VATBI2H_InsQuery.FieldByName('SE_FTX1').AsString := getString(i+2);
            VATBI2H_InsQuery.FieldByName('SE_FTX2').AsString := getString(i+3);
            VATBI2H_InsQuery.FieldByName('SE_FTX3').AsString := getString(i+4);
            VATBI2H_InsQuery.FieldByName('SE_FTX4').AsString := getString(i+5);
            VATBI2H_InsQuery.FieldByName('SE_FTX5').AsString := getString(i+6);
          end;
          if Qualifier = 'BY' then //공급받는자의 담당부서 및 담당자
          begin
            VATBI2H_InsQuery.FieldByName('BY_FTX1').AsString := getString(i+2);
            VATBI2H_InsQuery.FieldByName('BY_FTX2').AsString := getString(i+3);
            VATBI2H_InsQuery.FieldByName('BY_FTX3').AsString := getString(i+4);
            VATBI2H_InsQuery.FieldByName('BY_FTX4').AsString := getString(i+5);
            VATBI2H_InsQuery.FieldByName('BY_FTX5').AsString := getString(i+6);
          end;
          if Qualifier = 'AG' then //수탁자의 담당부서 및 담당자
          begin
            VATBI2H_InsQuery.FieldByName('AG_FTX1').AsString := getString(i+2);
            VATBI2H_InsQuery.FieldByName('AG_FTX2').AsString := getString(i+3);
            VATBI2H_InsQuery.FieldByName('AG_FTX3').AsString := getString(i+4);
            VATBI2H_InsQuery.FieldByName('AG_FTX4').AsString := getString(i+5);
            VATBI2H_InsQuery.FieldByName('AG_FTX5').AsString := getString(i+6);
          end;
        end
        else if SEGMENT('FTX','2','20') then
        begin
          if Qualifier = 'BY' then //공급받는자의 담당부서 및 담당자 2번(공급받는자같은겨우 2번반복)
          begin
            VATBI2H_InsQuery.FieldByName('BY_FTX1_1').AsString := getString(i+2);
            VATBI2H_InsQuery.FieldByName('BY_FTX2_1').AsString := getString(i+3);
            VATBI2H_InsQuery.FieldByName('BY_FTX3_1').AsString := getString(i+4);
            VATBI2H_InsQuery.FieldByName('BY_FTX4_1').AsString := getString(i+5);
            VATBI2H_InsQuery.FieldByName('BY_FTX5_1').AsString := getString(i+6);
          end;
        end
        else if SEGMENT('IMD','','21') then
        begin
          IMDGubun := getString(i+1);
          if Qualifier = 'SE' then //공급자
          begin
            if IMDGubun = 'SG' then //공급자의 업태
            begin
              if VATBI2H_InsQuery.FieldByName('SE_UPTA1').AsString = '' then
                VATBI2H_InsQuery.FieldByName('SE_UPTA1').AsString := getString(i+2)
              else
              begin
                VATBI2H_InsQuery.FieldByName('SE_UPTA1').AsString := VATBI2H_InsQuery.FieldByName('SE_UPTA1').AsString+#13#10+getString(i+2);
              end;
            end;
            if IMDGubun = 'HN' then //공급자의 종목
            begin
              if VATBI2H_InsQuery.FieldByName('SE_ITEM1').AsString = '' then
                VATBI2H_InsQuery.FieldByName('SE_ITEM1').AsString := getString(i+2)
              else
              begin
                VATBI2H_InsQuery.FieldByName('SE_ITEM1').AsString := VATBI2H_InsQuery.FieldByName('SE_ITEM1').AsString+#13#10+getString(i+2);
              end;
            end;
          end;
          if Qualifier = 'BY' then //공급받는자
          begin
            if IMDGubun = 'SG' then //공급받는자의 업태
            begin
              if VATBI2H_InsQuery.FieldByName('BY_UPTA1').AsString = '' then
                VATBI2H_InsQuery.FieldByName('BY_UPTA1').AsString := getString(i+2)
              else
              begin
                VATBI2H_InsQuery.FieldByName('BY_UPTA1').AsString := VATBI2H_InsQuery.FieldByName('BY_UPTA1').AsString+#13#10+getString(i+2);
              end;
            end;
            if IMDGubun = 'HN' then //공급받는자의 종목
            begin
              if VATBI2H_InsQuery.FieldByName('BY_ITEM1').AsString = '' then
                VATBI2H_InsQuery.FieldByName('BY_ITEM1').AsString := getString(i+2)
              else
              begin
                VATBI2H_InsQuery.FieldByName('BY_ITEM1').AsString := VATBI2H_InsQuery.FieldByName('BY_ITEM1').AsString+#13#10+getString(i+2);
              end;
            end;
          end;
          if Qualifier = 'AG' then //수탁자
          begin
            if IMDGubun = 'SG' then// 수탁자의 업태
            begin
              if VATBI2H_InsQuery.FieldByName('AG_UPTA1').AsString = '' then
                VATBI2H_InsQuery.FieldByName('AG_UPTA1').AsString := getString(i+2)
              else
              begin
                VATBI2H_InsQuery.FieldByName('AG_UPTA1').AsString := VATBI2H_InsQuery.FieldByName('AG_UPTA1').AsString+#13#10+getString(i+2);
              end;
            end;
            if IMDGubun = 'HN' then //수탁자의 종목
            begin
              if VATBI2H_InsQuery.FieldByName('AG_ITEM1').AsString = '' then
                VATBI2H_InsQuery.FieldByName('AG_ITEM1').AsString := getString(i+2)
              else
              begin
                VATBI2H_InsQuery.FieldByName('AG_ITEM1').AsString := VATBI2H_InsQuery.FieldByName('AG_ITEM1').AsString + getString(i+2);
              end;
            end;
          end;
        end
        else if SEGMENT('DTM','1','14') then  //세금계산서 작성일자
        begin
          VATBI2H_InsQuery.FieldByName('DRAW_DAT').AsString := '20' + getString(i+2);
        end
        else if SEGMENT('SEQ','1','22') then //공란수
        begin
          VATBI2H_InsQuery.FieldByName('DETAILNO').AsString := getString(i+1);
        end
        else if SEGMENT('MOA','','23') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '79' then //공급가액
          begin
            VATBI2H_InsQuery.FieldByName('SUP_AMT').AsCurrency := StrToCurr(getString(i+2));
          end;
          if Qualifier = '176' then //세액
          begin
            VATBI2H_InsQuery.FieldByName('TAX_AMT').AsCurrency := StrToCurr(getString(i+2));
          end;
        end
        else if SEGMENT('FTX','','15') then
        begin
          if VATBI2H_InsQuery.FieldByName('REMARK1').AsString = '' then
          begin
            VATBI2H_InsQuery.FieldByName('REMARK').AsString := 'Y';
            VATBI2H_InsQuery.FieldByName('REMARK1').AsString := getString(i+2)+#13#10+
                                                                getString(i+3)+#13#10+
                                                                getString(i+4)+#13#10+
                                                                getString(i+5)+#13#10+
                                                                getString(i+6);
          end
          else
          begin
            VATBI2H_InsQuery.FieldByName('REMARK1').AsString := VATBI2H_InsQuery.FieldByName('REMARK1').AsString+#13#10+
                                                                getString(i+2)+#13#10+
                                                                getString(i+3)+#13#10+
                                                                getString(i+4)+#13#10+
                                                                getString(i+5)+#13#10+
                                                                getString(i+6);
          end;
        end
        //품목내역--------------------------------------------------------------
        else IF SEGMENT('LIN','','16') Then  //품목입력시 순번
        begin
          //이전에 입력된 레코드 저장
          IF getString(i+1) <> '1' Then VATBI2D_InsQuery.Post; //SEQ가 1번 즉 처음이 아닐경우 이전의 내용을 post

          VATBI2D_InsQuery.Append;
          VATBI2D_InsQuery.FieldByName('KEYY').AsString := FMAINTNO;
          VATBI2D_InsQuery.FieldByName('SEQ').AsString := getString(i+1);
        end
        else if SEGMENT('DTM','1','24') then //공급일자
        begin
          VATBI2D_InsQuery.FieldByName('DE_DATE').AsString := '20' + getString(i+2);
        end
        else if SEGMENT('IMD','','25') then //품명
        begin
          if VATBI2D_InsQuery.FieldByName('NAME1').AsString = '' then
            VATBI2D_InsQuery.FieldByName('NAME1').AsString := getString(i+2)
          else
            VATBI2D_InsQuery.FieldByName('NAME1').AsString := VATBI2D_InsQuery.FieldByName('NAME1').AsString + getString(i+2);
        end
        else if SEGMENT('FTX','','26') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'AAA' then //규격
          begin
            if VATBI2D_InsQuery.FieldByName('SIZE1').AsString = '' then
            begin
              VATBI2D_InsQuery.FieldByName('SIZE1').AsString := getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end
            else
            begin
              VATBI2D_InsQuery.FieldByName('SIZE1').AsString := VATBI2D_InsQuery.FieldByName('SIZE1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
          if Qualifier = 'ACB' then //비고(REMARK)
          begin
            if VATBI2D_InsQuery.FieldByName('DE_REM1').AsString = '' then
            begin
              VATBI2D_InsQuery.FieldByName('DE_REM1').AsString := getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end
            else
            begin
              VATBI2D_InsQuery.FieldByName('DE_REM1').AsString := VATBI2D_InsQuery.FieldByName('DE_REM1').AsString+#13#10+
                                                                  getString(i+2)+#13#10+
                                                                  getString(i+3)+#13#10+
                                                                  getString(i+4)+#13#10+
                                                                  getString(i+5)+#13#10+
                                                                  getString(i+6);
            end;
          end;
        end
        else if SEGMENT('QTY','','27') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '1' then //수량
          begin
            VATBI2D_InsQuery.FieldByName('QTY').AsCurrency := StrToCurr( getString(i+2) );
            VATBI2D_InsQuery.FieldByName('QTY_G').AsString := getString(i+3);
          end;
          if Qualifier = '3' then //수량소계
          begin
            VATBI2D_InsQuery.FieldByName('STQTY').AsCurrency := StrToCurr( getString(i+2) );
            VATBI2D_InsQuery.FieldByName('STQTY_G').AsString := getString(i+3);
          end;
        end
        else if SEGMENT('PRI','1','28') then  //각 규격별 단가
        begin
          VATBI2D_InsQuery.FieldByName('PRICE').AsCurrency := StrToCurr( getString(i+2) );
          VATBI2D_InsQuery.FieldByName('PRICE_G').AsString := getString(i+3);
          VATBI2D_InsQuery.FieldByName('QTYG').AsCurrency := StrToCurr( getString(i+5) );
          VATBI2D_InsQuery.FieldByName('QTYG_G').AsString := getString(i+6);
        end
        else if SEGMENT('MOA','','29') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '203' then  //공급가액 (원화)
            VATBI2D_InsQuery.FieldByName('SUPAMT').AsCurrency := StrToCurr( getString(i+2) );
          if Qualifier = '124' then // 세액 (원화)
            VATBI2D_InsQuery.FieldByName('TAXAMT').AsCurrency := StrToCurr( getString(i+2) );
          if Qualifier = '261' then // 공급가액 (외화)
          begin
            VATBI2D_InsQuery.FieldByName('USAMT').AsCurrency := StrToCurr( getString(i+2) );
            VATBI2D_InsQuery.FieldByName('USAMT_G').AsString := getString(i+3);
          end;
          if Qualifier = '17' then //공급가액소계(원화)
            VATBI2D_InsQuery.FieldByName('SUPSTAMT').AsCurrency := StrToCurr( getString(i+2) );
          if Qualifier = '168' then //세액소계(원화)
            VATBI2D_InsQuery.FieldByName('TAXSTAMT').AsCurrency := StrToCurr( getString(i+2) );
          if Qualifier = '289' then //공급가액소계(외화)
          begin
            VATBI2D_InsQuery.FieldByName('USSTAMT').AsCurrency := StrToCurr( getString(i+2) );
            VATBI2D_InsQuery.FieldByName('USSTAMT_G').AsString := getString(i+3);
          end;
        end
        else if SEGMENT('CUX','1','2A') then //환율
        begin
          VATBI2D_InsQuery.FieldByName('RATE').AsCurrency := StrToCurr(getString(i+1));
        end
        else if (SEGMENT('UNS','1','')) and (getString(i+1) = 'S') then
        begin
          VATBI2D_InsQuery.Post; //마지막 순번의 내용을 post 시킴
        end
        //----------------------------------------------------------------------
        else if SEGMENT('MOA','','17') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '128' then  // 총금액
            VATBI2H_InsQuery.FieldByName('TAMT').AsCurrency := StrToCurr(getString(i+2));
          if Qualifier = '79' then //총공급가액
            VATBI2H_InsQuery.FieldByName('SUPTAMT').AsCurrency := StrToCurr(getString(i+2));
          if Qualifier = '176' then //총세액
            VATBI2H_InsQuery.FieldByName('TAXTAMT').AsCurrency := StrToCurr(getString(i+2));
          if Qualifier = '14' then //총외화공급가액
          begin
            VATBI2H_InsQuery.FieldByName('USTAMT').AsCurrency := StrToCurr(getString(i+2));
            VATBI2H_InsQuery.FieldByName('USTAMTC').AsString := getString(i+3);
          end;
        end
        else if SEGMENT('CNT','1','18') then //총수량
        begin
          VATBI2H_InsQuery.FieldByName('TQTY').AsCurrency := StrToCurr(getString(i+2));
          VATBI2H_InsQuery.FieldByName('TQTYC').AsString := getString(i+3);
        end
        else if SEGMENT('PAI','','19') then //지급지시사항 종류
          Qualifier := getString(i+1)
        else if SEGMENT('MOA','','2B') then
        begin
          MOAGubun := getString(i+1);
          if Qualifier = '10' then //현금
          begin
            if MOAGubun = '2AE' then // 원화
              VATBI2H_InsQuery.FieldByName('AMT12').AsCurrency := StrToCurr(getString(i+2));
            if MOAGubun = '2AD' then //외화
            begin
              VATBI2H_InsQuery.FieldByName('AMT11').AsCurrency := StrToCurr(getString(i+2));
              VATBI2H_InsQuery.FieldByName('AMT11C').AsString := getString(i+3);
            end;
          end;
          if Qualifier = '20' then //수표
          begin
            if MOAGubun = '2AE' then // 원화
              VATBI2H_InsQuery.FieldByName('AMT22').AsCurrency := StrToCurr(getString(i+2));
            if MOAGubun = '2AD' then //외화
            begin
              VATBI2H_InsQuery.FieldByName('AMT21').AsCurrency := StrToCurr(getString(i+2));
              VATBI2H_InsQuery.FieldByName('AMT21C').AsString := getString(i+3);
            end;
          end;
          if Qualifier = '2AA' then //어음
          begin
            if MOAGubun = '2AE' then // 원화
              VATBI2H_InsQuery.FieldByName('AMT32').AsCurrency := StrToCurr(getString(i+2));
            if MOAGubun = '2AD' then //외화
            begin
              VATBI2H_InsQuery.FieldByName('AMT31').AsCurrency := StrToCurr(getString(i+2));
              VATBI2H_InsQuery.FieldByName('AMT31C').AsString := getString(i+3);
            end;
          end;
          if Qualifier = '2AA' then //외상미수금
          begin
            if MOAGubun = '2AE' then // 원화
              VATBI2H_InsQuery.FieldByName('AMT42').AsCurrency := StrToCurr(getString(i+2));
            if MOAGubun = '2AD' then //외화
            begin
              VATBI2H_InsQuery.FieldByName('AMT41').AsCurrency := StrToCurr(getString(i+2));
              VATBI2H_InsQuery.FieldByName('AMT41C').AsString := getString(i+3);
            end;
          end;
        end
        else if SEGMENT('GIS','1','1A') then //청구의 구분을 기재
        begin
          VATBI2H_InsQuery.FieldByName('INDICATOR').AsString := getString(i+1);
        end;

//        ShowMessage( IntToStr(i));
      end;

      VATBI2H_InsQuery.Post;

      Result := True;
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    VATBI2H_InsQuery.Close;
    VATBI2D_InsQuery.Close;
  end;
end;

procedure TVATBI2.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'VATBI2';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'VATBI2';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+3])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+3]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+3]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TVATBI2.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT);
end;

END.
