object DOC_PCRLIC: TDOC_PCRLIC
  Left = 884
  Top = 147
  Width = 407
  Height = 345
  Caption = 'DOC_PCRLIC'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    391
    306)
  PixelsPerInch = 96
  TextHeight = 13
  object sMemo1: TsMemo
    Left = 10
    Top = 8
    Width = 372
    Height = 257
    Anchors = [akLeft, akTop, akRight]
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Lines.Strings = (
      'UNH+1+FINBIL:1:911:KE'
      'BGM+2AR+S15032715164172+9'
      'BUS+1:2DH'
      'RFF+AAC:ITTC732501084'
      'RFF+ACK:030821500642447'
      'DTM+97:150327:101'
      'DTM+137:150327:101'
      'FII+BC++45AO:25:BOK::::'#54620#44397#50472#54000#51008#54665':'#53580#54756#46976#47196#44592#50629#44552#50997#49468#53552
      'NAD+MR+++'#54620#44397#48148#51060#47536#51452#49885#54924#49324
      'NAD+AX+++'#54620#44397#50472#54000#51008#54665':'#53580#54756#46976#47196#44592#50629#44552#50997#49468#53552':1208542295'
      'PAI+::2AC'
      'MOA+2AD:13132.84:EUR'
      'PAI+::2AD'
      'MOA+2AD:13132.84:EUR'
      'MOA+2AE:15778056:KRW'
      'CUX+++1201.42'
      'FCA+6'
      'MOA+60:15788056.00:KRW'
      'MOA+131:10000.00:KRW'
      'ALC+C+:2BM'
      'MOA+23:10000:KRW'
      'MOA+25:13132.84:EUR'
      'CUX+++1188.67'
      'AUT+2726797920'
      'DTM+218:150327151713:202'
      'UNT+26+1')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    WordWrap = False
    Text = 
      'UNH+1+FINBIL:1:911:KE'#13#10'BGM+2AR+S15032715164172+9'#13#10'BUS+1:2DH'#13#10'RFF' +
      '+AAC:ITTC732501084'#13#10'RFF+ACK:030821500642447'#13#10'DTM+97:150327:101'#13#10 +
      'DTM+137:150327:101'#13#10'FII+BC++45AO:25:BOK::::'#54620#44397#50472#54000#51008#54665':'#53580#54756#46976#47196#44592#50629#44552#50997#49468#53552#13#10'NA' +
      'D+MR+++'#54620#44397#48148#51060#47536#51452#49885#54924#49324#13#10'NAD+AX+++'#54620#44397#50472#54000#51008#54665':'#53580#54756#46976#47196#44592#50629#44552#50997#49468#53552':1208542295'#13#10'PAI+::2' +
      'AC'#13#10'MOA+2AD:13132.84:EUR'#13#10'PAI+::2AD'#13#10'MOA+2AD:13132.84:EUR'#13#10'MOA+2' +
      'AE:15778056:KRW'#13#10'CUX+++1201.42'#13#10'FCA+6'#13#10'MOA+60:15788056.00:KRW'#13#10'M' +
      'OA+131:10000.00:KRW'#13#10'ALC+C+:2BM'#13#10'MOA+23:10000:KRW'#13#10'MOA+25:13132.' +
      '84:EUR'#13#10'CUX+++1188.67'#13#10'AUT+2726797920'#13#10'DTM+218:150327151713:202'#13 +
      #10'UNT+26+1'#13#10
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
  end
  object sEdit1: TsEdit
    Left = 256
    Top = 272
    Width = 121
    Height = 23
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Active = True
    BoundLabel.Caption = #49688#49888#51901' '#49692#48264
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = ANSI_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -12
    BoundLabel.Font.Name = #47569#51008' '#44256#46357
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object qryFinMaintNo: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      'FROM PCRLIC_H'
      'WHERE MAINT_NO LIKE :MAINT_NO')
    Left = 88
    Top = 192
  end
  object qryMIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'Maint_No'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'User_Id'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Chk1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'Chk2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'Chk3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Datee'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'MESSAGE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'MESSAGE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'APP_CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'APP_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'AX_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'AX_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'AX_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SUP_CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SUP_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ITM_G1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ITM_G2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ITM_G3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'TQTY'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'TQTYC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'TAMT1'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'TAMT1C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'TAMT2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'TAMT2C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'CHG_G'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'C1_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'C1_C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'C2_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'LIC_INFO'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'LIC_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BK_CD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'BK_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BK_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BK_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BK_NAME4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BK_NAME5'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'AC1_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'AC1_C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'AC2_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'Comf_dat'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'PRNO'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'PCrLic_No'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ChgCd'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'BankSend_No'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'DOC_G'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'DOC_D'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BHS_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'BNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'BNAME1'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'BAMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'BAMTC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'EXP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SHIP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'BSIZE1'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'BAL_CD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 4
        Value = Null
      end
      item
        Name = 'BAL_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BAL_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'F_INTERFACE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'ITM_GBN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ITM_GNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'ISSUE_GBN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'DOC_GBN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'DOC_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BAMT2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'BAMTC2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'BAMT3'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'BAMTC3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'PCRLIC_ISS_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BK_CD2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'BAL_CD2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'EMAIL_ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EMAIL_DOMAIN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'Sunbun'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO PCRLIC_H('
      '[MAINT_NO]'
      ',[USER_ID]'
      ',[CHK1]'
      ',[CHK2]'
      ',[CHK3]'
      ',[DATEE]'
      ',[MESSAGE1]'
      ',[MESSAGE2]'
      ',[APP_CODE]'
      ',[APP_NAME1]'
      ',[APP_NAME2]'
      ',[APP_NAME3]'
      ',[APP_ADDR1]'
      ',[APP_ADDR2]'
      ',[APP_ADDR3]'
      ',[AX_NAME1]'
      ',[AX_NAME2]'
      ',[AX_NAME3]'
      ',[SUP_CODE]'
      ',[SUP_NAME1]'
      ',[SUP_NAME2]'
      ',[SUP_NAME3]'
      ',[SUP_ADDR1]'
      ',[SUP_ADDR2]'
      ',[SUP_ADDR3]'
      ',[ITM_G1]'
      ',[ITM_G2]'
      ',[ITM_G3]'
      ',[TQTY]'
      ',[TQTYC]'
      ',[TAMT1]'
      ',[TAMT1C]'
      ',[TAMT2]'
      ',[TAMT2C]'
      ',[CHG_G]'
      ',[C1_AMT]'
      ',[C1_C]'
      ',[C2_AMT]'
      ',[LIC_INFO]'
      ',[LIC_NO]'
      ',[BK_CD]'
      ',[BK_NAME1]'
      ',[BK_NAME2]'
      ',[BK_NAME3]'
      ',[BK_NAME4]'
      ',[BK_NAME5]'
      ',[AC1_AMT]'
      ',[AC1_C]'
      ',[AC2_AMT]'
      ',[Comf_dat]'
      ',[PRNO]'
      ',[PCrLic_No]'
      ',[ChgCd]'
      ',[BankSend_No]'
      ',[APP_DATE]'
      ',[DOC_G]'
      ',[DOC_D]'
      ',[BHS_NO]'
      ',[BNAME]'
      ',[BNAME1]'
      ',[BAMT]'
      ',[BAMTC]'
      ',[EXP_DATE]'
      ',[SHIP_DATE]'
      ',[BSIZE1]'
      ',[BAL_CD]'
      ',[BAL_NAME1]'
      ',[BAL_NAME2]'
      ',[F_INTERFACE]'
      ',[ITM_GBN]'
      ',[ITM_GNAME]'
      ',[ISSUE_GBN]'
      ',[DOC_GBN]'
      ',[DOC_NO]'
      ',[BAMT2]'
      ',[BAMTC2]'
      ',[BAMT3]'
      ',[BAMTC3]'
      ',[PCRLIC_ISS_NO]'
      ',[BK_CD2]'
      ',[BAL_CD2]'
      ',[EMAIL_ID]'
      ',[EMAIL_DOMAIN]'
      ')VALUES('
      ' :MAINT_NO'
      ', :USER_ID'
      ', :CHK1'
      ', :CHK2'
      ', :CHK3'
      ', :DATEE'
      ', :MESSAGE1'
      ', :MESSAGE2'
      ', :APP_CODE'
      ', :APP_NAME1'
      ', :APP_NAME2'
      ', :APP_NAME3'
      ', :APP_ADDR1'
      ', :APP_ADDR2'
      ', :APP_ADDR3'
      ', :AX_NAME1'
      ', :AX_NAME2'
      ', :AX_NAME3'
      ', :SUP_CODE'
      ', :SUP_NAME1'
      ', :SUP_NAME2'
      ', :SUP_NAME3'
      ', :SUP_ADDR1'
      ', :SUP_ADDR2'
      ', :SUP_ADDR3'
      ', :ITM_G1'
      ', :ITM_G2'
      ', :ITM_G3'
      ', :TQTY'
      ', :TQTYC'
      ', :TAMT1'
      ', :TAMT1C'
      ', :TAMT2'
      ', :TAMT2C'
      ', :CHG_G'
      ', :C1_AMT'
      ', :C1_C'
      ', :C2_AMT'
      ', :LIC_INFO'
      ', :LIC_NO'
      ', :BK_CD'
      ', :BK_NAME1'
      ', :BK_NAME2'
      ', :BK_NAME3'
      ', :BK_NAME4'
      ', :BK_NAME5'
      ', :AC1_AMT'
      ', :AC1_C'
      ', :AC2_AMT'
      ', :Comf_dat'
      ', :PRNO'
      ', :PCrLic_No'
      ', :ChgCd'
      ', :BankSend_No'
      ', :APP_DATE'
      ', :DOC_G'
      ', :DOC_D'
      ', :BHS_NO'
      ', :BNAME'
      ', :BNAME1'
      ', :BAMT'
      ', :BAMTC'
      ', :EXP_DATE'
      ', :SHIP_DATE'
      ', :BSIZE1'
      ', :BAL_CD'
      ', :BAL_NAME1'
      ', :BAL_NAME2'
      ', :F_INTERFACE'
      ', :ITM_GBN'
      ', :ITM_GNAME'
      ', :ISSUE_GBN'
      ', :DOC_GBN'
      ', :DOC_NO'
      ', :BAMT2'
      ', :BAMTC2'
      ', :BAMT3'
      ', :BAMTC3'
      ', :PCRLIC_ISS_NO'
      ', :BK_CD2'
      ', :BAL_CD2'
      ', :EMAIL_ID'
      ', :EMAIL_DOMAIN'
      ')'
      ''
      'UPDATE R_HST'
      'SET CON = '#39'Y'#39
      '      ,DBAPPLY = '#39'Y'#39
      'WHERE Sunbun = :Sunbun'
      '')
    Left = 16
    Top = 224
  end
  object qryDIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'HS_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'NAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'NAME1'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'SIZE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'SIZE1'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'REMARK'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'QTY'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'QTYC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'AMT1'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'AMT1C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'AMT2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PRI1'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PRI2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PRI_BASE'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'PRI_BASEC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ACHG_G'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'AC1_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'AC1_C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'AC2_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'LOC_TYPE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'NAMESS'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'SIZESS'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'APP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'ITM_NUM'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [PCRLICD1]('
      '[KEYY]'
      ',[SEQ]'
      ',[HS_NO]'
      ',[NAME]'
      ',[NAME1]'
      ',[SIZE]'
      ',[SIZE1]'
      ',[REMARK]'
      ',[QTY]'
      ',[QTYC]'
      ',[AMT1]'
      ',[AMT1C]'
      ',[AMT2]'
      ',[PRI1]'
      ',[PRI2]'
      ',[PRI_BASE]'
      ',[PRI_BASEC]'
      ',[ACHG_G]'
      ',[AC1_AMT]'
      ',[AC1_C]'
      ',[AC2_AMT]'
      ',[LOC_TYPE]'
      ',[NAMESS]'
      ',[SIZESS]'
      ',[APP_DATE]'
      ',[ITM_NUM]'
      ')VALUES('
      ':KEYY'
      ', :SEQ'
      ', :HS_NO'
      ', :NAME'
      ', :NAME1'
      ', :SIZE'
      ', :SIZE1'
      ', :REMARK'
      ', :QTY'
      ', :QTYC'
      ', :AMT1'
      ', :AMT1C'
      ', :AMT2'
      ', :PRI1'
      ', :PRI2'
      ', :PRI_BASE'
      ', :PRI_BASEC'
      ', :ACHG_G'
      ', :AC1_AMT'
      ', :AC1_C'
      ', :AC2_AMT'
      ', :LOC_TYPE'
      ', :NAMESS'
      ', :SIZESS'
      ', :APP_DATE'
      ', :ITM_NUM'
      ')')
    Left = 48
    Top = 224
  end
end
