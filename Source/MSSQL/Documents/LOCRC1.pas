unit LOCRC1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TLOCRC1 = class
    private
      procedure RunSQL(sSQL : String);
    protected
      FMig : TStringList;
      FOriginFileName : String;
      FDuplicateDoc : string;
      FDuplicate : Boolean;

      LOCRC1H_InsQuery  : TADOQuery;
      LOCRC1D_InsQuery  : TADOQuery;
      LOCRC1TAX_InsQuery  : TADOQuery;
      ReadyQuery   : TADOQuery;
      DuplicateQuery : TADOQuery;
      SEGMENT_ID, SEGMENT_COUNT,SEGMENT_NO : string;
      function SEGMENT(sID,sCOUNT,sNO : string):Boolean;
      function DuplicateData(AdminNo : String):Boolean;
      function getString(idx : Integer):String;
      function getInteger(idx : Integer):Integer;
    published
      constructor Create(Mig : TStringList);
      destructor Destroy;
    public
      procedure Run_Ready(bDuplicate :Boolean);
      function Run_Convert:Boolean;
      function isReConvert(sDocNo : string):Boolean;
      property DuplicateDoc: String Read FDuplicateDoc;
      property Duplicate: Boolean  read FDuplicate;
    end;
var
  LOCRC1_DOC: TLOCRC1;

implementation

uses MSSQL, Commonlib, VarDefine, SQLDefine, RecvParent, MessageDefine, SQLCreator;
{ TLOCRC1 }

constructor TLOCRC1.Create(Mig: TStringList);
begin
  FMig := Mig;
  FDuplicateDoc := '';
  FDuplicate := False;
  LOCRC1H_InsQuery := TADOQuery.Create(nil);
  LOCRC1D_InsQuery := TADOQuery.Create(nil);
  LOCRC1TAX_InsQuery := TADOQuery.Create(nil);
  ReadyQuery  := TADOQuery.Create(nil);
  DuplicateQuery := TADOQuery.Create(nil);
end;

destructor TLOCRC1.Destroy;
begin
  FMig.Free;
  ReadyQuery.Free;
  LOCRC1H_InsQuery.Free;
  LOCRC1D_InsQuery.Free;
  LOCRC1TAX_InsQuery.Free;
  DuplicateQuery.Free;
end;

function TLOCRC1.DuplicateData(AdminNo: String): Boolean;
begin
  with DuplicateQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'SELECT * FROM R_HST WHERE DOCID = '+QuotedStr('LOCRC1')+' AND MAINT_NO = '+QuotedStr(AdminNo);
    Open;

    Result := DuplicateQuery.RecordCount > 0;
    DuplicateQuery.Close;
  end;
end;

function TLOCRC1.getInteger(idx: Integer): Integer;
begin
  Result := StrToInt( Trim(FMig.Strings[idx]) );
end;

function TLOCRC1.getString(idx: Integer): String;
begin
  Result := Trim(FMig.Strings[idx]);
end;

function TLOCRC1.isReConvert(sDocNo: string): Boolean;
var
  SC : TSQLCreate;
begin
  Result := False;
  with TADOQuery.Create(nil) do
  begin
    SC := TSQLCreate.Create;
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT * FROM LOCRC1_H WHERE MAINT_NO = '+QuotedStr(sDocNo);
      Open;

      IF RecordCount > 0 Then
      begin
        IF MessageBox(Application.Handle, MSG_QUESTION_DUPLICATE_DATA, '재변환 확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;
        DMMssql.BeginTrans;
        try
          SC.DMLType := dmlDelete; SC.SQLHeader('LOCRC1_H'); SC.ADDWhere('MAINT_NO', sDocNo); RunSQL(SC.CreateSQL);
          SC.DMLType := dmlDelete; SC.SQLHeader('LOCRC1_D'); SC.ADDWhere('KEYY', sDocNo); RunSQL(SC.CreateSQL);
          SC.DMLType := dmlDelete; SC.SQLHeader('LOCRC1_TAX'); SC.ADDWhere('KEYY', sDocNo); RunSQL(SC.CreateSQL);
          DMMssql.CommitTrans;
          Result := True;
        except
          on E:Exception do
          begin
            DMMssql.RollbackTrans;
            ShowMessage(E.Message);
          end;
        end;
      end
      else
        Result := True;
    finally
      SC.Free;
      Close;
      Free;
    end;
  end;
end;

procedure TLOCRC1.RunSQL(sSQL: String);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := sSQL;
      ExecSQL; 
    finally
      Close;
      Free;
    end;
  end;
end;

function TLOCRC1.Run_Convert: Boolean;
var
  i : Integer;
  DocumentCount,TaxSeq : integer;
  Qualifier : string;
  FMAINT_NO : String;
  MEMOString : TStringList;
begin
  Result := false;

  LOCRC1H_InsQuery.Close;
  LOCRC1H_InsQuery.Connection := DMMssql.KISConnect;
  LOCRC1H_InsQuery.SQL.Text := SQL_LOCRC1H;
  LOCRC1H_InsQuery.Open;

  LOCRC1D_InsQuery.Close;
  LOCRC1D_InsQuery.Connection := DMMssql.KISConnect;
  LOCRC1D_InsQuery.SQL.Text := SQL_LOCRC1D;
  LOCRC1D_InsQuery.Open;

  LOCRC1TAX_InsQuery.Close;
  LOCRC1TAX_InsQuery.Connection := DMMssql.KISConnect;
  LOCRC1TAX_InsQuery.SQL.Text := SQL_LOCRC1TAX;
  LOCRC1TAX_InsQuery.Open;

  MEMOString := TStringList.Create;
  //APPEND
  try
    try
      LOCRC1H_InsQuery.Append;

      //Default Values
      LOCRC1H_InsQuery.FieldByName('User_Id').AsString := LoginData.sID;
      LOCRC1H_InsQuery.FieldByName('DATEE'  ).AsString := FormatDateTime('YYYYMMDD',Now);
      LOCRC1H_InsQuery.FieldByName('CHK1').AsString := '';
      LOCRC1H_InsQuery.FieldByName('CHK2').AsString := '';
      LOCRC1H_InsQuery.FieldByName('CHK3').AsString := '';
      LOCRC1H_InsQuery.FieldByName('PRNO').AsString := '0';

      //값 초기화
      DocumentCount := 1;

      //분석시작
      for i:= 1 to FMig.Count-1 do
      begin
       // ShowMessage(IntToStr(i));
        //IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH'] )Then
          Continue
        else
        begin
          SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
          SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
          SEGMENT_NO := RightStr(FMig.Strings[i],2);
        end;
        IF SEGMENT('BGM','1','10') Then
        begin
          LOCRC1H_InsQuery.FieldByName('MAINT_NO').AsString := getString(i+2);
          FMAINT_NO := getString(i+2);

          LOCRC1H_InsQuery.FieldByName('MESSAGE1').AsString := getString(i+3);
          LOCRC1H_InsQuery.FieldByName('MESSAGE2').AsString := getString(i+4);
        end
        else if SEGMENT('RFF','','11') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'DM' then  //발급번호
            LOCRC1H_InsQuery.FieldByName('RFF_NO').AsString := getString(i+2);
          if Qualifier = 'HS' then  //HS부호
            LOCRC1H_InsQuery.FieldByName('BSN_HSCODE').AsString := getString(i+2);
        end
        else if SEGMENT('DTM','','12') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '50' then  //인수일자
            LOCRC1H_InsQuery.FieldByName('GET_DAT').AsString := '20' + getString(i+2);
          if Qualifier = '182' then //발급일자
            LOCRC1H_InsQuery.FieldByName('ISS_DAT').AsString := '20' + getString(i+2);
          if Qualifier = '36' then  //유효기일
            LOCRC1H_InsQuery.FieldByName('EXP_DAT').AsString := '20' + getString(i+2);
        end
        else if SEGMENT('NAD','','13') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'SU' then  //물품공급자
          begin
            LOCRC1H_InsQuery.FieldByName('BENEFC2').AsString := getString(i+2);
            LOCRC1H_InsQuery.FieldByName('BENEFC1').AsString := getString(i+3);
          end;
          if Qualifier = 'AP' then  //물품수령자
          begin
            LOCRC1H_InsQuery.FieldByName('APPLIC7').AsString := getString(i+2);
            LOCRC1H_InsQuery.FieldByName('APPLIC1').AsString := getString(i+3);
            LOCRC1H_InsQuery.FieldByName('APPLIC2').AsString := getString(i+4);
            LOCRC1H_InsQuery.FieldByName('APPLIC3').AsString := getString(i+5);
            LOCRC1H_InsQuery.FieldByName('APPLIC4').AsString := getString(i+6);
            LOCRC1H_InsQuery.FieldByName('APPLIC5').AsString := getString(i+7);
            LOCRC1H_InsQuery.FieldByName('APPLIC6').AsString := getString(i+8);
          end;
        end
        else if SEGMENT('MOA','','14') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '2AD' then //인수금액 외화
          begin
            LOCRC1H_InsQuery.FieldByName('RCT_AMT1').AsCurrency := StrToCurr(getString(i+2));
            LOCRC1H_InsQuery.FieldByName('RCT_AMT1C').AsString := getString(i+3);
          end;
          if Qualifier = '2AE' then //인수금액 원화
          begin
            LOCRC1H_InsQuery.FieldByName('RCT_AMT2').AsCurrency := StrToCurr(getString(i+2));
            LOCRC1H_InsQuery.FieldByName('RCT_AMT2C').AsString := getString(i+3);
          end;
        end
        else if SEGMENT('CUX','1','15') then
        begin //인수금액환율
          LOCRC1H_InsQuery.FieldByName('RATE').AsCurrency := StrToCurr(getString(i+1));
        end
        else if SEGMENT('FTX','1','16') then
        begin //기타조건
          LOCRC1H_InsQuery.FieldByName('REMARK').AsString := 'Y';
          LOCRC1H_InsQuery.FieldByName('REMARK1').AsString := getString(i+2)+#13#10+
                                                              getString(i+3)+#13#10+
                                                              getString(i+4)+#13#10+
                                                              getString(i+5)+#13#10+
                                                              getString(i+6);
        end
        //상품내역--------------------------------------------------------------
        else IF SEGMENT('LIN','','17') Then
        begin //상품내역 순번
//          ShowMessage( SEGMENT_ID + '/' + SEGMENT_COUNT + '/' + SEGMENT_NO );
          IF getString(i+1) <> '1' Then
            LOCRC1D_InsQuery.Post; //SEQ가 1번 즉 처음이 아닐경우 이전의 내용을 post

          LOCRC1D_InsQuery.Append;
          LOCRC1D_InsQuery.FieldByName('KEYY').AsString := FMAINT_NO;
          LOCRC1D_InsQuery.FieldByName('SEQ').AsString := getString(i+1);
        end
        else if SEGMENT('PIA','1','20') then //품목의 HS번호
        begin
          LOCRC1D_InsQuery.FieldByName('HS_NO').AsString := getString(i+2);
        end
        else if SEGMENT('IMD','','21') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '1AA' then  //품목의 품명
          begin
            if LOCRC1D_InsQuery.FieldByName('NAME1').AsString = '' then
              LOCRC1D_InsQuery.FieldByName('NAME1').AsString := getString(i+2)
            else
              LOCRC1D_InsQuery.FieldByName('NAME1').AsString := LOCRC1D_InsQuery.FieldByName('NAME1').AsString + getString(i+2);
          end;
        end
        else if SEGMENT('FTX','','22') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = 'AAA' then
          begin //물품의 규격
            if LOCRC1D_InsQuery.FieldByName('SIZE1').AsString = '' then
            begin
              LOCRC1D_InsQuery.FieldByName('SIZE1').AsString := getString(i+2)+#13#10+
                                                                getString(i+3)+#13#10+
                                                                getString(i+4)+#13#10+
                                                                getString(i+5)+#13#10+
                                                                getString(i+6);
            end
            else
            begin
              LOCRC1D_InsQuery.FieldByName('SIZE1').AsString := LOCRC1D_InsQuery.FieldByName('SIZE1').AsString+#13#10+
                                                                getString(i+2)+#13#10+
                                                                getString(i+3)+#13#10+
                                                                getString(i+4)+#13#10+
                                                                getString(i+5)+#13#10+
                                                                getString(i+6);
            end;
          end;
        end
        else if SEGMENT('QTY','','23') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '1' then //수량
          begin
            LOCRC1D_InsQuery.FieldByName('QTY').AsCurrency := StrToCurr(getString(i+2));
            LOCRC1D_InsQuery.FieldByName('QTY_G').AsString := getString(i+3);
          end;
          if Qualifier = '3' then //수량소계
          begin
            LOCRC1D_InsQuery.FieldByName('STQTY').AsCurrency := StrToCurr(getString(i+2));
            LOCRC1D_InsQuery.FieldByName('STQTY_G').AsString := getString(i+3);
          end;
        end
        else if SEGMENT('PRI','1','24') then
        begin //단가관련
          LOCRC1D_InsQuery.FieldByName('PRICE').AsCurrency := StrToCurr(getString(i+2));
          LOCRC1D_InsQuery.FieldByName('QTYG').AsCurrency := StrToCurr(getString(i+5));
          LOCRC1D_InsQuery.FieldByName('QTYG_G').AsString := getString(i+6);
        end
        else if SEGMENT('MOA','','25') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '203' then //금액
          begin
            LOCRC1D_InsQuery.FieldByName('AMT').AsCurrency := StrToCurr(getString(i+2));
            LOCRC1D_InsQuery.FieldByName('AMT_G').AsString := getString(i+3);
          end;
          if Qualifier = '17' then //금액소계
          begin
            LOCRC1D_InsQuery.FieldByName('STAMT').AsCurrency := StrToCurr(getString(i+2));
            LOCRC1D_InsQuery.FieldByName('STAMT_G').AsString := getString(i+3);
          end;
        end
        else if (SEGMENT('UNS','1','')) and (getString(i+1) = 'S') then
        begin
            LOCRC1D_InsQuery.Post; //마지막 순번의 내용을 post 시킴
        end
        //----------------------------------------------------------------------
        else if SEGMENT('CNT','1','18') then
        begin //총수량
          LOCRC1H_InsQuery.FieldByName('TQTY').AsCurrency := StrToCurr(getString(i+2));
          LOCRC1H_InsQuery.FieldByName('TQTY_G').AsString := getString(i+3);
        end
        else if SEGMENT('MOA','1','19') then
        begin //총금액
          LOCRC1H_InsQuery.FieldByName('TAMT').AsCurrency := StrToCurr(getString(i+2));
          LOCRC1H_InsQuery.FieldByName('TAMT_G').AsString := getString(i+3);
        end
        else if SEGMENT('RFF','1','1A') then
        begin //내국신용장번호
          LOCRC1H_InsQuery.FieldByName('LOC_NO').AsString := getString(i+2);
        end
        else if SEGMENT('FII','1','26') then
        begin //개설은행
          LOCRC1H_InsQuery.FieldByName('AP_BANK').AsString := getString(i+2);
          LOCRC1H_InsQuery.FieldByName('AP_NAME').AsString := getString(i+5);
          LOCRC1H_InsQuery.FieldByName('AP_BANK1').AsString := Copy(getString(i+6),1,35);
          LOCRC1H_InsQuery.FieldByName('AP_BANK2').AsString := Copy(getString(i+6),36,70);
          LOCRC1H_InsQuery.FieldByName('AP_BANK3').AsString := Copy(getString(i+7),1,35);
          LOCRC1H_InsQuery.FieldByName('AP_BANK4').AsString := Copy(getString(i+7),36,70);
        end
        else if SEGMENT('MOA','','27') then
        begin //내국신용장의 개설금액
          Qualifier := getString(i+1);
          if Qualifier = '2AD' then
          begin // 외화금액
            LOCRC1H_InsQuery.FieldByName('LOC1AMT').AsCurrency := StrToCurr( getString(i+2) );
            LOCRC1H_InsQuery.FieldByName('LOC1AMTC').AsString  := getString(i+3);
          end;
          if Qualifier = '2AE' then
          begin //원화금액
            LOCRC1H_InsQuery.FieldByName('LOC2AMT').AsCurrency := StrToCurr( getString(i+2) );
            LOCRC1H_InsQuery.FieldByName('LOC2AMTC').AsString  := getString(i+3);
          end;
        end
        else if SEGMENT('CUX','1','28') then
        begin //내국신용장 매매기준율
          LOCRC1H_InsQuery.FieldByName('EX_RATE').AsCurrency := StrToCurr( getString(i+1) );
        end
        else if SEGMENT('DTM','','29') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '2' then
          begin //물품인도기일
            LOCRC1H_InsQuery.FieldByName('LOADDATE').AsString := '20' + getString(i+2);
          end;
          if Qualifier = '123' then
          begin // 유기일
            LOCRC1H_InsQuery.FieldByName('EXPDATE').AsString := '20' + getString(i+2);
          end;
        end
        else if SEGMENT('FTX','1','2A') then
        begin // 참고사항
          LOCRC1H_InsQuery.FieldByName('LOC_REM1').AsString := getString(i+2)+#1310+
                                                               getString(i+3)+#1310+
                                                               getString(i+4)+#1310+
                                                               getString(i+5)+#1310+
                                                               getString(i+6)+#1310;
        end
        //세금계산서------------------------------------------------------------
        else if SEGMENT('DOC','','1B') then
        begin
          //ShowMessage( SEGMENT_ID + '/' + SEGMENT_COUNT + '/' + SEGMENT_NO );

          if SEGMENT_COUNT <> '1' then
            LOCRC1TAX_InsQuery.Post;

          LOCRC1TAX_InsQuery.Append;

          LOCRC1TAX_InsQuery.FieldByName('KEYY').AsString := FMAINT_NO; 
          LOCRC1TAX_InsQuery.FieldByName('BILL_NO').AsString := getString(i+2);
          LOCRC1TAX_InsQuery.FieldByName('SEQ').AsString := SEGMENT_COUNT;
        end
        else if SEGMENT('MOA','','2B') then
        begin
          Qualifier := getString(i+1);
          if Qualifier = '79' then
          begin
            LOCRC1TAX_InsQuery.FieldByName('BILL_AMOUNT').AsCurrency := StrToCurr(getString(i+2));
            LOCRC1TAX_InsQuery.FieldByName('BILL_AMOUNT_UNIT').AsString := getString(i+3);
          end;
          if Qualifier = '176' then
          begin
            LOCRC1TAX_InsQuery.FieldByName('TAX_AMOUNT').AsCurrency := StrToCurr(getString(i+2));
            LOCRC1TAX_InsQuery.FieldByName('TAX_AMOUNT_UNIT').AsString := getString(i+3);
          end;
        end
        else if SEGMENT('DTM','1','2C') then
        begin
          LOCRC1TAX_InsQuery.FieldByName('BILL_DATE').AsString := '20' + getString(i+2);
        end
        else if SEGMENT('UNT','','') then
        begin
          LOCRC1TAX_InsQuery.Post;
        end;                      

      end; //for end
      LOCRC1H_InsQuery.Post;
      Result := True;
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    LOCRC1H_InsQuery.Close;
    LOCRC1D_InsQuery.Close;
    LOCRC1TAX_InsQuery.Close;
  end;
end;

procedure TLOCRC1.Run_Ready(bDuplicate: Boolean);
var
  i : Integer;
begin
  with ReadyQuery do
  begin
    Close;
    Connection := DMMssql.KISConnect;
    SQL.Text := 'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, CONTROLNO, RECEIVEUSER, Mig, SRVENDER, CON, DBAPPLY, DOCNAME)'#13+
                'VALUES ( :SRDATE, :SRTIME, :DOCID, :MAINT_NO, 0, :CONTROLNO, :RECEIVEUSER, :Mig, :SRVENDER, :CON, :DBAPPLY, :DOCNAME )';

    FOriginFileName := FMig.Strings[0];
    for i := 1 to FMig.Count-1 do
    begin
      IF i = 1 Then
      begin
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD',Now);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HH:NN:SS',Now);
        Parameters.ParamByName('DOCID').Value := 'LOCRC1';
        Parameters.ParamByName('Mig').Value := FMig.Text;
        Parameters.ParamByName('SRVENDER').Value := Trims(FMig.Strings[1],1,18);
        Parameters.ParamByName('CONTROLNO').Value := '';
        Parameters.ParamByName('RECEIVEUSER').Value := LoginData.sID;
        Parameters.ParamByName('CON').Value := '';
        Parameters.ParamByName('DBAPPLY').Value := '';
        Parameters.ParamByName('DOCNAME').Value := 'LOCRC1';
        Continue;
      end;

      IF AnsiMatchText( Trim(LeftStr(FMig.Strings[i],3)) , ['','UNH','UNT'] )Then
        Continue
      else
      begin
        SEGMENT_ID := Trim(LeftStr(FMig.Strings[i],3));
        SEGMENT_COUNT := Trims(FMig.strings[i],5,7);
        SEGMENT_NO := RightStr(FMig.Strings[i],2);
      end;

      IF SEGMENT('BGM','1','10') Then
      begin
        //관리번호 중복 확인
        IF DuplicateData(Trim(FMig.Strings[i+2])) AND bDuplicate Then
        begin
          FDuplicateDoc := Trim(FMig.Strings[i+2]);
          FDuplicate := True;
          Exit;
        end
        else
        begin
          FDuplicateDoc := '';
          FDuplicate := false;
          Parameters.ParamByName('MAINT_NO').Value := Trim(FMig.Strings[i+2]);
        end;
      end;
    end;
    ExecSQL;
  end;
end;

function TLOCRC1.SEGMENT(sID, sCOUNT, sNO: string): Boolean;
begin
  IF (sID <> '') AND (sCOUNT = '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID)
  else
  IF (sID <> '') AND (sCOUNT = '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO <> '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT) AND (sNo = SEGMENT_NO)
  else
  IF (sID <> '') AND (sCOUNT <> '') and (sNO = '') Then
    Result := (sID = SEGMENT_ID) AND (sCOUNT = SEGMENT_COUNT);
end;

end.
