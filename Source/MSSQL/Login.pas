unit Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sEdit, sButton, TypeDefine, VarDefine, ADODB, DB , MSSQL,
  Buttons, sSpeedButton, sLabel, acPNG, acImage, sComboBox ;

type
  TLogin_frm = class(TForm)
    sPanel1: TsPanel;
    sPanel4: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_ID: TsEdit;
    sPanel5: TsPanel;
    sSpeedButton2: TsSpeedButton;
    edt_Pwd: TsEdit;
    sImage1: TsImage;
    sImage2: TsImage;
    sPanel2: TsPanel;
    sButton1: TsButton;
    sButton2: TsButton;
    lbl_info: TsLabel;
    sButton3: TsButton;
    sPanel3: TsPanel;
    sSpeedButton3: TsSpeedButton;
    sImage3: TsImage;
    com_ServerList: TsComboBox;
    sLabel1: TsLabel;
    procedure sButton2Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt_IDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt_PwdExit(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sLabel1MouseEnter(Sender: TObject);
    procedure sLabel1MouseLeave(Sender: TObject);
    procedure sLabel1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Login_frm: TLogin_frm;

implementation

uses Main,MessageDefine, KISEncryption, ICON, LocalSetting, CodeContents, DocumentQry,
  AutoNo, CreateTable, dlgUserAdmin;

{$R *.dfm}

procedure TLogin_frm.sButton2Click(Sender: TObject);
begin
  IF MessageBox(Self.Handle,MSG_PROGRAM_CLOSE,'프로그램 종료',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
  begin
    Main_frm.Close; 
  end;
end;

procedure TLogin_frm.sButton1Click(Sender: TObject);
var
  userID , userPW : String;
  KISEN : TKISEncryption;
begin

  IF com_ServerList.ItemIndex = 0 Then
  begin
    com_ServerList.SetFocus;
    ShowMessage('서버를 선택하세요');
    exit;
  end;

  if not Assigned(DMMssql) Then
  begin
    DMMssql := TDMMssql.Create(Application);

    LocalSetting_frm := TLocalSetting_frm.Create(Self);
    try
      DMMssql.KISConnect.ConnectionString := LocalSetting_frm.GetLocalConnectionString(com_ServerList.ItemIndex);
    finally
      FreeAndNil(LocalSetting_frm);
    end;
  end;

//------------------------------------------------------------------------------
// 테이블/ 필드 수정사항 적용
//------------------------------------------------------------------------------
  DMCreateTable.Run;

  KISEN := TKISEncryption.Create;
  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    userID := edt_ID.Text;
    userPW := edt_Pwd.Text;
    try
      Close;
      sql.Clear;
      SQL.Text := 'SELECT USERID, NAME, DEPT, PASSWORD, DEPTCODE FROM PERSON WHERE USERID = ' + QuotedStr(userID)
                                                                         + ' AND PASSWORD = ' + QuotedStr(KISEN.SHA256_EN(userPW));
      Open;

      if RecordCount = 0 then
      begin
        lbl_info.Caption := MSG_LOGIN_DISCORD;
        edt_ID.SetFocus;
      END
      else
      begin
        DMCodeContents := TDMCodeContents.Create(Application);
        DMDocumentQry := TDMDocumentQry.Create(Application);
        DMAutoNo := TDMAutoNo.Create(Application);
        LoginData.sID := FieldByName('USERID').AsString;
        LoginData.sName := FieldByName('NAME').AsString;
        LoginData.sCompanyNo := DMCodeContents.ConfigSAUP_NO.AsString;
        Login_frm.Close;
      end;
    finally
      Close;
      Free;
      KISEN.Free;
    end;
  end;
end;

procedure TLogin_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if Key = VK_RETURN then
   begin
//SelectNext(sender);
   end;
end;

procedure TLogin_frm.edt_IDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key = 13 Then
  begin
    Case (Sender as TsEdit).Tag of
      0: edt_Pwd.SetFocus;
      1: sButton1Click(sButton1);
    end;
  end;
end;

Const
  sKey : string = '8B6B01A0805D97E00AAC39FF5410ECE2DA3559A3E8BF1777DB32D0F196D59B37';
procedure TLogin_frm.edt_PwdExit(Sender: TObject);
var
  KISEN : TKISEncryption;
begin
  KISEN := TKISEncryption.Create;
  try
    sButton3.Visible := (UpperCase(edt_ID.Text) = 'SYSTEM')
                        AND
                        (KISEN.SHA256_EN(UpperCase(edt_Pwd.Text)) = sKey );
    sLabel1.Visible := sButton3.Visible;
  finally
    KISEN.Free;
  end;
end;

procedure TLogin_frm.sButton3Click(Sender: TObject);
begin
  LocalSetting_frm := TLocalSetting_frm.Create(Self);
  try
    IF LocalSetting_frm.ShowModal = mrOK Then
    begin
      LocalSetting_frm.getServerList(com_ServerList);
      IF com_ServerList.Items.Count = 2 Then
        com_ServerList.ItemIndex := 1
      else
        com_ServerList.ItemIndex := 0;
    end;
  finally
    FreeAndNil(LocalSetting_frm);
  end;
end;

procedure TLogin_frm.FormShow(Sender: TObject);
begin
  LocalSetting_frm := TLocalSetting_frm.Create(Self);
  try
    LocalSetting_frm.getServerList(com_ServerList);
    IF com_ServerList.Items.Count = 2 Then
      com_ServerList.ItemIndex := 1
    else
      com_ServerList.ItemIndex := 0;
  finally
    FreeAndNil(LocalSetting_frm);
  end;
end;

procedure TLogin_frm.sLabel1MouseEnter(Sender: TObject);
begin
  sLabel1.Font.Style := [fsUnderline];
  sLabel1.Font.Color := clBlue;
end;

procedure TLogin_frm.sLabel1MouseLeave(Sender: TObject);
begin
  sLabel1.Font.Style := [];
  sLabel1.Font.Color := clBlack;  
end;

procedure TLogin_frm.sLabel1Click(Sender: TObject);
begin
  IF com_ServerList.ItemIndex = 0 Then
  begin
    com_ServerList.SetFocus;
    ShowMessage('서버를 선택하세요');
    exit;
  end;

  if not Assigned(DMMssql) Then
  begin
    DMMssql := TDMMssql.Create(Application);

    LocalSetting_frm := TLocalSetting_frm.Create(Self);
    try
      DMMssql.KISConnect.ConnectionString := LocalSetting_frm.GetLocalConnectionString(com_ServerList.ItemIndex);
    finally
      FreeAndNil(LocalSetting_frm);
    end;
  end;

  dlgUserAdmin_frm := TdlgUserAdmin_frm.Create(Self);
  try
    dlgUserAdmin_frm.ShowModal;
  finally
    FreeAndNil( dlgUserAdmin_frm );
  end;
end;

end.
