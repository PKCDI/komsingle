unit UI_INF700_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_APP700_NEW, DB, ADODB, sSkinProvider, StdCtrls, sCheckBox,
  sMemo, sComboBox, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, StrUtils, DateUtils,
  Menus;

type
  TUI_INF700_NEW_frm = class(TUI_APP700_NEW_frm)
    qryINF700: TADOQuery;
    qryINF700MAINT_NO: TStringField;
    qryINF700MESSAGE1: TStringField;
    qryINF700MESSAGE2: TStringField;
    qryINF700USER_ID: TStringField;
    qryINF700DATEE: TStringField;
    qryINF700APP_DATE: TStringField;
    qryINF700IN_MATHOD: TStringField;
    qryINF700AP_BANK: TStringField;
    qryINF700AP_BANK1: TStringField;
    qryINF700AP_BANK2: TStringField;
    qryINF700AP_BANK3: TStringField;
    qryINF700AP_BANK4: TStringField;
    qryINF700AP_BANK5: TStringField;
    qryINF700AD_BANK: TStringField;
    qryINF700AD_BANK1: TStringField;
    qryINF700AD_BANK2: TStringField;
    qryINF700AD_BANK3: TStringField;
    qryINF700AD_BANK4: TStringField;
    qryINF700AD_PAY: TStringField;
    qryINF700IL_NO1: TStringField;
    qryINF700IL_NO2: TStringField;
    qryINF700IL_NO3: TStringField;
    qryINF700IL_NO4: TStringField;
    qryINF700IL_NO5: TStringField;
    qryINF700IL_AMT1: TBCDField;
    qryINF700IL_AMT2: TBCDField;
    qryINF700IL_AMT3: TBCDField;
    qryINF700IL_AMT4: TBCDField;
    qryINF700IL_AMT5: TBCDField;
    qryINF700IL_CUR1: TStringField;
    qryINF700IL_CUR2: TStringField;
    qryINF700IL_CUR3: TStringField;
    qryINF700IL_CUR4: TStringField;
    qryINF700IL_CUR5: TStringField;
    qryINF700AD_INFO1: TStringField;
    qryINF700AD_INFO2: TStringField;
    qryINF700AD_INFO3: TStringField;
    qryINF700AD_INFO4: TStringField;
    qryINF700AD_INFO5: TStringField;
    qryINF700DOC_CD: TStringField;
    qryINF700CD_NO: TStringField;
    qryINF700REF_PRE: TStringField;
    qryINF700ISS_DATE: TStringField;
    qryINF700EX_DATE: TStringField;
    qryINF700EX_PLACE: TStringField;
    qryINF700CHK1: TStringField;
    qryINF700CHK2: TStringField;
    qryINF700CHK3: TStringField;
    qryINF700prno: TIntegerField;
    qryINF700F_INTERFACE: TStringField;
    qryINF700IMP_CD1: TStringField;
    qryINF700IMP_CD2: TStringField;
    qryINF700IMP_CD3: TStringField;
    qryINF700IMP_CD4: TStringField;
    qryINF700IMP_CD5: TStringField;
    qryINF700MAINT_NO_1: TStringField;
    qryINF700APP_BANK: TStringField;
    qryINF700APP_BANK1: TStringField;
    qryINF700APP_BANK2: TStringField;
    qryINF700APP_BANK3: TStringField;
    qryINF700APP_BANK4: TStringField;
    qryINF700APP_BANK5: TStringField;
    qryINF700APP_ACCNT: TStringField;
    qryINF700APPLIC1: TStringField;
    qryINF700APPLIC2: TStringField;
    qryINF700APPLIC3: TStringField;
    qryINF700APPLIC4: TStringField;
    qryINF700APPLIC5: TStringField;
    qryINF700BENEFC1: TStringField;
    qryINF700BENEFC2: TStringField;
    qryINF700BENEFC3: TStringField;
    qryINF700BENEFC4: TStringField;
    qryINF700BENEFC5: TStringField;
    qryINF700CD_AMT: TBCDField;
    qryINF700CD_CUR: TStringField;
    qryINF700CD_PERP: TBCDField;
    qryINF700CD_PERM: TBCDField;
    qryINF700CD_MAX: TStringField;
    qryINF700AA_CV1: TStringField;
    qryINF700AA_CV2: TStringField;
    qryINF700AA_CV3: TStringField;
    qryINF700AA_CV4: TStringField;
    qryINF700AVAIL: TStringField;
    qryINF700AVAIL1: TStringField;
    qryINF700AVAIL2: TStringField;
    qryINF700AVAIL3: TStringField;
    qryINF700AVAIL4: TStringField;
    qryINF700AV_ACCNT: TStringField;
    qryINF700AV_PAY: TStringField;
    qryINF700DRAFT1: TStringField;
    qryINF700DRAFT2: TStringField;
    qryINF700DRAFT3: TStringField;
    qryINF700DRAWEE: TStringField;
    qryINF700DRAWEE1: TStringField;
    qryINF700DRAWEE2: TStringField;
    qryINF700DRAWEE3: TStringField;
    qryINF700DRAWEE4: TStringField;
    qryINF700DR_ACCNT: TStringField;
    qryINF700MAINT_NO_2: TStringField;
    qryINF700PSHIP: TStringField;
    qryINF700TSHIP: TStringField;
    qryINF700LOAD_ON: TStringField;
    qryINF700FOR_TRAN: TStringField;
    qryINF700LST_DATE: TStringField;
    qryINF700SHIP_PD: TBooleanField;
    qryINF700SHIP_PD1: TStringField;
    qryINF700SHIP_PD2: TStringField;
    qryINF700SHIP_PD3: TStringField;
    qryINF700SHIP_PD4: TStringField;
    qryINF700SHIP_PD5: TStringField;
    qryINF700SHIP_PD6: TStringField;
    qryINF700DESGOOD: TBooleanField;
    qryINF700DESGOOD_1: TMemoField;
    qryINF700TERM_PR: TStringField;
    qryINF700TERM_PR_M: TStringField;
    qryINF700PL_TERM: TStringField;
    qryINF700ORIGIN: TStringField;
    qryINF700ORIGIN_M: TStringField;
    qryINF700DOC_380: TBooleanField;
    qryINF700DOC_380_1: TBCDField;
    qryINF700DOC_705: TBooleanField;
    qryINF700DOC_705_1: TStringField;
    qryINF700DOC_705_2: TStringField;
    qryINF700DOC_705_3: TStringField;
    qryINF700DOC_705_4: TStringField;
    qryINF700DOC_740: TBooleanField;
    qryINF700DOC_740_1: TStringField;
    qryINF700DOC_740_2: TStringField;
    qryINF700DOC_740_3: TStringField;
    qryINF700DOC_740_4: TStringField;
    qryINF700DOC_530: TBooleanField;
    qryINF700DOC_530_1: TStringField;
    qryINF700DOC_530_2: TStringField;
    qryINF700DOC_271: TBooleanField;
    qryINF700DOC_271_1: TBCDField;
    qryINF700DOC_861: TBooleanField;
    qryINF700DOC_2AA: TBooleanField;
    qryINF700DOC_2AA_1: TMemoField;
    qryINF700ACD_2AA: TBooleanField;
    qryINF700ACD_2AA_1: TStringField;
    qryINF700ACD_2AB: TBooleanField;
    qryINF700ACD_2AC: TBooleanField;
    qryINF700ACD_2AD: TBooleanField;
    qryINF700ACD_2AE: TBooleanField;
    qryINF700ACD_2AE_1: TMemoField;
    qryINF700CHARGE: TStringField;
    qryINF700PERIOD: TBCDField;
    qryINF700CONFIRMM: TStringField;
    qryINF700DEF_PAY1: TStringField;
    qryINF700DEF_PAY2: TStringField;
    qryINF700DEF_PAY3: TStringField;
    qryINF700DEF_PAY4: TStringField;
    qryINF700DOC_705_GUBUN: TStringField;
    qryINF700MAINT_NO_3: TStringField;
    qryINF700REI_BANK: TStringField;
    qryINF700REI_BANK1: TStringField;
    qryINF700REI_BANK2: TStringField;
    qryINF700REI_BANK3: TStringField;
    qryINF700REI_BANK4: TStringField;
    qryINF700REI_BANK5: TStringField;
    qryINF700REI_ACNNT: TStringField;
    qryINF700INSTRCT: TBooleanField;
    qryINF700INSTRCT_1: TMemoField;
    qryINF700AVT_BANK: TStringField;
    qryINF700AVT_BANK1: TStringField;
    qryINF700AVT_BANK2: TStringField;
    qryINF700AVT_BANK3: TStringField;
    qryINF700AVT_BANK4: TStringField;
    qryINF700AVT_BANK5: TStringField;
    qryINF700AVT_ACCNT: TStringField;
    qryINF700SND_INFO1: TStringField;
    qryINF700SND_INFO2: TStringField;
    qryINF700SND_INFO3: TStringField;
    qryINF700SND_INFO4: TStringField;
    qryINF700SND_INFO5: TStringField;
    qryINF700SND_INFO6: TStringField;
    qryINF700EX_NAME1: TStringField;
    qryINF700EX_NAME2: TStringField;
    qryINF700EX_NAME3: TStringField;
    qryINF700EX_ADDR1: TStringField;
    qryINF700EX_ADDR2: TStringField;
    qryINF700EX_ADDR3: TStringField;
    qryINF700OP_BANK1: TStringField;
    qryINF700OP_BANK2: TStringField;
    qryINF700OP_BANK3: TStringField;
    qryINF700OP_ADDR1: TStringField;
    qryINF700OP_ADDR2: TStringField;
    qryINF700OP_ADDR3: TStringField;
    qryINF700MIX_PAY1: TStringField;
    qryINF700MIX_PAY2: TStringField;
    qryINF700MIX_PAY3: TStringField;
    qryINF700MIX_PAY4: TStringField;
    qryINF700APPLICABLE_RULES_1: TStringField;
    qryINF700APPLICABLE_RULES_2: TStringField;
    qryINF700DOC_760: TBooleanField;
    qryINF700DOC_760_1: TStringField;
    qryINF700DOC_760_2: TStringField;
    qryINF700DOC_760_3: TStringField;
    qryINF700DOC_760_4: TStringField;
    qryINF700SUNJUCK_PORT: TStringField;
    qryINF700DOCHACK_PORT: TStringField;
    qryINF700mathod_Name: TStringField;
    qryINF700pay_Name: TStringField;
    qryINF700Imp_Name_1: TStringField;
    qryINF700Imp_Name_2: TStringField;
    qryINF700Imp_Name_3: TStringField;
    qryINF700Imp_Name_4: TStringField;
    qryINF700Imp_Name_5: TStringField;
    qryINF700DOC_Name: TStringField;
    qryINF700CDMAX_Name: TStringField;
    qryINF700pship_Name: TStringField;
    qryINF700tship_Name: TStringField;
    qryINF700doc705_Name: TStringField;
    qryINF700doc740_Name: TStringField;
    qryINF700doc760_Name: TStringField;
    qryINF700CHARGE_Name: TStringField;
    qryINF700CONFIRM_Name: TStringField;
    qryINF700CHARGE_1: TMemoField;
    qryINF700CONFIRM_BICCD: TStringField;
    qryINF700CONFIRM_BANKNM: TStringField;
    qryINF700CONFIRM_BANKBR: TStringField;
    qryINF700PERIOD_IDX: TIntegerField;
    qryINF700PERIOD_TXT: TStringField;
    qryINF700SPECIAL_PAY: TMemoField;
    memo_Adinfo: TsMemo;
    sPanel90: TsPanel;
    sPanel91: TsPanel;
    msk_dateOfIssue: TsMaskEdit;
    sPanel92: TsPanel;
    sPanel93: TsPanel;
    edt_docCredutNo: TsEdit;
    sPanel94: TsPanel;
    sPanel95: TsPanel;
    edt_refPreAdvice: TsEdit;
    sPanel96: TsPanel;
    sPanel97: TsPanel;
    edt_appRules1: TsEdit;
    edt_appRules2: TsEdit;
    sTabSheet7: TsTabSheet;
    sPanel102: TsPanel;
    sPanel103: TsPanel;
    sPanel104: TsPanel;
    edt_Avail1: TsEdit;
    edt_Avail2: TsEdit;
    edt_Avail5: TsEdit;
    edt_AvAccnt: TsEdit;
    sPanel105: TsPanel;
    sPanel106: TsPanel;
    sPanel107: TsPanel;
    sPanel108: TsPanel;
    edt_AvPay: TsEdit;
    edt_Avail4: TsEdit;
    edt_Avail3: TsEdit;
    sPanel109: TsPanel;
    sPanel110: TsPanel;
    edt_ReiBank1: TsEdit;
    edt_ReiBank2: TsEdit;
    sPanel111: TsPanel;
    edt_ReiBank4: TsEdit;
    edt_ReiBank3: TsEdit;
    sPanel112: TsPanel;
    edt_ReiBank5: TsEdit;
    sPanel113: TsPanel;
    edt_ReiAccnt: TsEdit;
    sPanel114: TsPanel;
    sPanel115: TsPanel;
    edt_Drawee1: TsEdit;
    edt_Drawee2: TsEdit;
    edt_Drawee_bic: TsEdit;
    edt_DrAccnt: TsEdit;
    sPanel116: TsPanel;
    sPanel117: TsPanel;
    sPanel118: TsPanel;
    edt_Drawee4: TsEdit;
    edt_Drawee3: TsEdit;
    sPanel98: TsPanel;
    sPanel99: TsPanel;
    edt_Avtbank1: TsEdit;
    edt_Avtbank2: TsEdit;
    sPanel119: TsPanel;
    edt_Avtbank4: TsEdit;
    edt_Avtbank3: TsEdit;
    sPanel120: TsPanel;
    edt_Avtbank5: TsEdit;
    sPanel121: TsPanel;
    edt_AvtAccnt: TsEdit;
    sPanel100: TsPanel;
    sPanel101: TsPanel;
    edt_sndInfo1: TsEdit;
    edt_sndInfo2: TsEdit;
    edt_sndInfo3: TsEdit;
    sPanel122: TsPanel;
    edt_OpBank1: TsEdit;
    edt_OpBank2: TsEdit;
    edt_OpBank3: TsEdit;
    edt_OpAddr1: TsEdit;
    edt_OpAddr2: TsEdit;
    sPanel123: TsPanel;
    sPanel124: TsPanel;
    sPanel125: TsPanel;
    qryINF700AVAIL_BIC: TStringField;
    qryINF700DRAWEE_BIC: TStringField;
    qryINF700MSEQ: TIntegerField;
    procedure qryINF700AfterScroll(DataSet: TDataSet);
    procedure qryINF700AfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sButton13Click(Sender: TObject);
  private
    procedure INF700_UPLOAD_LIVART;
  protected
    procedure ReadList(SortField: TColumn = nil); override;
    procedure ReadData; override;
    { Private declarations }
    
  public
    { Public declarations }
  end;

var
  UI_INF700_NEW_frm: TUI_INF700_NEW_frm;

implementation

uses MSSQL, MessageDefine, INF700_PRINT, Preview, LivartConfig, SQLCreator;

{$R *.dfm}

procedure TUI_INF700_NEW_frm.ReadData;
begin
  inherited;
  IF not qryINF700.Active Then Exit;
  //------------------------------------------------------------------------------
  // 헤더
  //------------------------------------------------------------------------------
  edt_MAINT_NO.Text := qryINF700MAINT_NO.AsString;
  edt_MSEQ.Text := qryINF700MSEQ.AsString;
  msk_Datee.Text := qryINF700DATEE.AsString;
  edt_userno.Text := qryINF700USER_ID.AsString;
  //문서기능
  CM_INDEX(com_func, [':'], 0, qryINF700MESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryINF700MESSAGE2.AsString);

//------------------------------------------------------------------------------
// 문서공통
//------------------------------------------------------------------------------
  msk_APP_DATE.Text := qryINF700APP_DATE.AsString;
  edt_IN_MATHOD.Text := qryINF700IN_MATHOD.AsString;
  edt_IN_MATHOD_NM.Text := qryINF700mathod_Name.AsString;
  //개설은행
  edt_ApBank.Text := qryINF700AP_BANK.AsString;
  edt_ApBank1.Text := qryINF700AP_BANK1.AsString + qryINF700AP_BANK2.AsString;
  edt_ApBank3.Text := qryINF700AP_BANK3.AsString + qryINF700AP_BANK4.AsString;
  edt_ApBank5.Text := qryINF700AP_BANK5.AsString;

  //통지은행
//  edt_AdBank.Text := qryINF700AD_BANK.AsString;
  edt_AdBank1.Text := qryINF700Ad_BANK1.AsString + qryINF700Ad_BANK2.AsString;
  edt_ADBank3.Text := qryINF700Ad_BANK3.AsString + qryINF700Ad_BANK4.AsString;

  //통지은행 BIC코드(추가)
  edt_AdBank_BIC.Text := qryINF700AD_BANK.AsString;
  IF Trim(edt_AdBank_BIC.Text) = '' Then sEdit2.Clear;

  edt_AdPay.Text := qryINF700AD_PAY.AsString;
  edt_AdPay1.Text := qryINF700pay_Name.AsString;
  //수입용도
  edt_ImpCd1.Text := qryINF700IMP_CD1.AsString;
  edt_ImpCd1_NM.Text := qryINF700Imp_Name_1.AsString;
  edt_ImpCd2.Text := qryINF700IMP_CD2.AsString;
  edt_ImpCd2_NM.Text := qryINF700Imp_Name_2.AsString;
  edt_ImpCd3.Text := qryINF700IMP_CD3.AsString;
  edt_ImpCd3_NM.Text := qryINF700Imp_Name_3.AsString;
  edt_ImpCd4.Text := qryINF700IMP_CD4.AsString;
  edt_ImpCd4_NM.Text := qryINF700Imp_Name_4.AsString;
  edt_ImpCd5.Text := qryINF700IMP_CD5.AsString;
  edt_ImpCd5_NM.Text := qryINF700Imp_Name_5.AsString;
  //기타정보
//  edt_AdInfo1.Text := qryINF700AD_INFO1.AsString;
//  edt_AdInfo2.Text := qryINF700AD_INFO2.AsString;
//  edt_AdInfo3.Text := qryINF700AD_INFO3.AsString;
//  edt_AdInfo4.Text := qryINF700AD_INFO4.AsString;
//  edt_AdInfo5.Text := qryINF700AD_INFO5.AsString;
    IF qryINF700AD_INFO1.AsString <> '' Then
      memo_Adinfo.Lines.Add(qryINF700AD_INFO1.AsString);
    IF qryINF700AD_INFO2.AsString <> '' Then
      memo_Adinfo.Lines.Add(qryINF700AD_INFO2.AsString);
    IF qryINF700AD_INFO3.AsString <> '' Then
      memo_Adinfo.Lines.Add(qryINF700AD_INFO3.AsString);
    IF qryINF700AD_INFO4.AsString <> '' Then
      memo_Adinfo.Lines.Add(qryINF700AD_INFO4.AsString);
    IF qryINF700AD_INFO5.AsString <> '' Then
      memo_Adinfo.Lines.Add(qryINF700AD_INFO5.AsString);
  //운송수단
//  CM_INDEX(com_Carriage, [':'], 0, qryINF700CARRIAGE.AsString, 0);
//  edt_Carriage.Text := qryINF700CARRIAGE.AsString;
//  edt_Carriage1.Text := qryINF700CARRIAGE_NAME.AsString;
  //IL번호
  edt_ILno1.Text := qryINF700IL_NO1.AsString;
  edt_ILCur1.Text := qryINF700IL_CUR1.AsString;
  edt_ILAMT1.Value := qryINF700IL_AMT1.AsCurrency;
  edt_ILno2.Text := qryINF700IL_NO2.AsString;
  edt_ILCur2.Text := qryINF700IL_CUR2.AsString;
  edt_ILAMT2.Value := qryINF700IL_AMT2.AsCurrency;
  edt_ILno3.Text := qryINF700IL_NO3.AsString;
  edt_ILCur3.Text := qryINF700IL_CUR3.AsString;
  edt_ILAMT3.Value := qryINF700IL_AMT3.AsCurrency;
  edt_ILno4.Text := qryINF700IL_NO4.AsString;
  edt_ILCur4.Text := qryINF700IL_CUR4.AsString;
  edt_ILAMT4.Value := qryINF700IL_AMT4.AsCurrency;
  edt_ILno5.Text := qryINF700IL_NO5.AsString;
  edt_ILCur5.Text := qryINF700IL_CUR5.AsString;
  edt_ILAMT5.Value := qryINF700IL_AMT5.AsCurrency;

  edt_EXName1.Text := qryINF700EX_NAME1.AsString;
  edt_EXName2.Text := qryINF700EX_NAME2.AsString;
  edt_EXName1.Text := qryINF700EX_NAME3.AsString;
  edt_EXAddr1.Text := qryINF700EX_ADDR1.AsString;
  edt_EXAddr2.Text := qryINF700EX_ADDR2.AsString;

//------------------------------------------------------------------------------
// SWIFT PAGE1
//------------------------------------------------------------------------------
  msk_dateOfIssue.Text := qryINF700ISS_DATE.AsString;

  edt_Doccd.Text   := qryINF700DOC_CD.AsString;
  edt_Doccd1.Text  := qryINF700DOC_Name.AsString;
  msk_exDate.Text  := qryINF700EX_DATE.AsString;
  edt_exPlace.Text := qryINF700EX_PLACE.AsString;

  //LCNO
  edt_docCredutNo.Text := qryINF700CD_NO.AsString;
  edt_refPreAdvice.Text := qryINF700REF_PRE.AsString;

  edt_appRules1.Text := qryINF700APPLICABLE_RULES_1.AsString;
  edt_appRules2.Text := qryINF700APPLICABLE_RULES_2.AsString;

  edt_Applic1.Text := qryINF700APPLIC1.AsString;
  edt_Applic2.Text := qryINF700APPLIC2.AsString;
  edt_Applic3.Text := qryINF700APPLIC3.AsString;
  edt_Applic4.Text := qryINF700APPLIC4.AsString;
  edt_Applic5.Text := qryINF700APPLIC5.AsString;

//  edt_Benefc.Text  := qryINF700BENEFC.AsString;
  edt_Benefc1.Text := qryINF700BENEFC1.AsString;
  edt_Benefc2.Text := qryINF700BENEFC2.AsString;
  edt_Benefc3.Text := qryINF700BENEFC3.AsString;
  edt_Benefc4.Text := qryINF700BENEFC4.AsString;
  edt_Benefc5.Text := qryINF700BENEFC5.AsString;

  edt_Cdcur.Text := qryINF700CD_CUR.AsString;
  edt_Cdamt.Value := qryINF700cd_amt.AsCurrency;
  edt_cdPerP.Value := qryINF700CD_PERP.AsCurrency;
  edt_cdPerM.Value := qryINF700CD_PERM.AsCurrency;
  edt_TermPR.Text := qryINF700TERM_PR.AsString;
  edt_TermPR_M.Text := qryINF700TERM_PR_M.AsString;
  edt_plTerm.Text := qryINF700PL_TERM.AsString;
  edt_Origin.Text := qryINF700ORIGIN.AsString;
  edt_Origin_M.Text := qryINF700ORIGIN_M.AsString;
//  memo_Desgood1.Text := qryINF700DESGOOD_1.AsString;

//------------------------------------------------------------------------------
// SWIFT PAGE2
//------------------------------------------------------------------------------
  edt_aaCcv1.Text := qryINF700AA_CV1.AsString;
  edt_aaCcv2.Text := qryINF700AA_CV2.AsString;
  edt_aaCcv3.Text := qryINF700AA_CV3.AsString;
  edt_aaCcv4.Text := qryINF700AA_CV4.AsString;

  edt_Draft1.Text := qryINF700DRAFT1.AsString;
  edt_Draft2.Text := qryINF700DRAFT2.AsString;
  edt_Draft3.Text := qryINF700DRAFT3.AsString;

  edt_mixPay1.Text := qryINF700MIX_PAY1.AsString;
  edt_mixPay2.Text := qryINF700MIX_PAY2.AsString;
  edt_mixPay3.Text := qryINF700MIX_PAY3.AsString;
  edt_mixPay4.Text := qryINF700MIX_PAY4.AsString;

  edt_defPay1.Text := qryINF700DEF_PAY1.AsString;
  edt_defPay2.Text := qryINF700DEF_PAY2.AsString;
  edt_defPay3.Text := qryINF700DEF_PAY3.AsString;
  edt_defPay4.Text := qryINF700DEF_PAY4.AsString;

//  7: ALLOWED (Transhipments are allowed)
//  8: NOT ALLOWED (Transhipments are not allowed)
  com_Pship.ItemIndex := AnsiIndexText(Trim(qryINF700PSHIP.AsString), ['', '9', '10']);
//9: ALLOWED (Partial shipments are allowed)
//10: NOT ALLOWED (Partial shipments are not allowed)
  com_TShip.ItemIndex := AnsiIndexText(Trim(qryINF700TSHIP.AsString), ['', '7', '8']);

  edt_loadOn.Text := qryINF700LOAD_ON.AsString;
  edt_forTran.Text := qryINF700FOR_TRAN.AsString;
  msk_lstDate.Text := qryINF700LST_DATE.AsString;

  edt_shipPD1.Text := qryINF700SHIP_PD1.AsString;
  edt_shipPD2.Text := qryINF700SHIP_PD2.AsString;
  edt_shipPD3.Text := qryINF700SHIP_PD3.AsString;
  edt_shipPD4.Text := qryINF700SHIP_PD4.AsString;
  edt_shipPD5.Text := qryINF700SHIP_PD5.AsString;

  edt_SunjukPort.Text := qryINF700SUNJUCK_PORT.AsString;
  edt_dochackPort.Text := qryINF700DOCHACK_PORT.AsString;

//------------------------------------------------------------------------------
// SWIFT PAGE3
//------------------------------------------------------------------------------
  edt_Avail1.Text := qryINF700AVAIL1.AsString;
  edt_Avail2.Text := qryINF700AVAIL2.AsString;
  edt_Avail3.Text := qryINF700AVAIL3.AsString;
  edt_Avail4.Text := qryINF700AVAIL4.AsString;
  edt_Avail5.Text := qryINF700AVAIL_BIC.AsString;
  edt_AvAccnt.Text := qryINF700AV_ACCNT.AsString;
  edt_AvPay.Text := qryINF700AV_PAY.AsString;

  edt_Drawee1.Text := qryINF700DRAWEE1.AsString;
  edt_Drawee2.Text := qryINF700DRAWEE2.AsString;
  edt_Drawee3.Text := qryINF700DRAWEE3.AsString;
  edt_Drawee4.Text := qryINF700DRAWEE4.AsString;
  edt_Drawee_bic.Text := qryINF700DRAWEE_BIC.AsString;
  edt_DrAccnt.Text := qryINF700DR_ACCNT.AsString;

  edt_ReiBank1.Text := qryINF700REI_BANK1.AsString;
  edt_ReiBank2.Text := qryINF700REI_BANK2.AsString;
  edt_ReiBank3.Text := qryINF700REI_BANK3.AsString;
  edt_ReiBank4.Text := qryINF700REI_BANK4.AsString;
  edt_ReiBank5.Text := qryINF700REI_BANK5.AsString;
  edt_ReiAccnt.Text := qryINF700REI_ACNNT.AsString;

  edt_Avtbank1.Text := qryINF700AVT_BANK1.AsString;
  edt_Avtbank2.Text := qryINF700AVT_BANK2.AsString;
  edt_Avtbank3.Text := qryINF700AVT_BANK3.AsString;
  edt_Avtbank4.Text := qryINF700AVT_BANK4.AsString;
  edt_Avtbank5.Text := qryINF700AVT_BANK5.AsString;
  edt_AvtAccnt.Text := qryINF700AVT_ACCNT.AsString;

  edt_sndInfo1.Text := qryINF700SND_INFO1.AsString;
  edt_sndInfo2.Text := qryINF700SND_INFO2.AsString;
  edt_sndInfo3.Text := qryINF700SND_INFO3.AsString;

  edt_OpBank1.Text := qryINF700OP_BANK1.AsString;
  edt_OpBank2.Text := qryINF700OP_BANK2.AsString;
  edt_OpBank3.Text := qryINF700OP_BANK3.AsString;

  edt_OpAddr1.Text := qryINF700OP_ADDR1.AsString;
  edt_OpAddr2.Text := qryINF700OP_ADDR2.AsString;


//------------------------------------------------------------------------------
// SWIFT PAGE4
//------------------------------------------------------------------------------
  chk_380.Checked := qryINF700DOC_380.AsBoolean;
  edt_380_1.Text := qryINF700DOC_380_1.AsString;

  chk_705.Checked := qryINF700DOC_705.AsBoolean;
  com_705Gubun.ItemIndex := AnsiIndexText( qryINF700DOC_705_GUBUN.AsString, ['','705','706','717','718','707'] );
  com_705GubunSelect(com_705Gubun);
  edt_705_1.Text := qryINF700DOC_705_1.AsString;
  edt_705_2.Text := qryINF700DOC_705_2.AsString;
  com_705_3.ItemIndex := AnsiIndexText(qryINF700DOC_705_3.AsString, ['','31','32']);
  edt_705_4.Text := qryINF700DOC_705_4.AsString;

  chk_740.Checked := qryINF700DOC_740.AsBoolean;
  edt_740_1.Text  := qryINF700DOC_740_1.AsString;
  edt_740_2.Text  := qryINF700DOC_740_2.AsString;
  com_740_3.ItemIndex := AnsiIndexText(qryINF700DOC_740_3.AsString, ['','31','32']);
  edt_740_4.Text  := qryINF700DOC_740_4.AsString;

  chk_760.Checked := qryINF700DOC_760.AsBoolean;
  edt_760_1.Text  := qryINF700DOC_760_1.AsString;
  edt_760_2.Text  := qryINF700DOC_760_2.AsString;
  com_760_3.ItemIndex := AnsiIndexText(qryINF700DOC_760_3.AsString, ['','31','32']);
  edt_760_4.Text  := qryINF700DOC_760_4.AsString;

  chk_530.Checked := qryINF700DOC_530.AsBoolean;
  edt_530_1.Text := qryINF700DOC_530_1.AsString;
  edt_530_2.Text := qryINF700DOC_530_2.AsString;

  chk_271.Checked := qryINF700DOC_271.AsBoolean;
  edt_271_1.Text := qryINF700DOC_271_1.AsString;

  chk_861.Checked := qryINF700DOC_861.AsBoolean;

  chk_2AA.Checked := qryINF700DOC_2AA.AsBoolean;
  memo_2AA_1.Text := qryINF700DOC_2AA_1.AsString;

//------------------------------------------------------------------------------
// SWIFT PAGE5
//------------------------------------------------------------------------------
  chk_ACD2AA.Checked := qryINF700ACD_2AA.AsBoolean;
  edt_ACD2AA_1.Text := qryINF700ACD_2AA_1.AsString;

  chk_ACD2AB.Checked := qryINF700ACD_2AB.AsBoolean;
  chk_ACD2AC.Checked := qryINF700ACD_2AC.AsBoolean;
  chk_ACD2AD.Checked := qryINF700ACD_2AD.AsBoolean;
  chk_ACD2AE.Checked := qryINF700ACD_2AE.AsBoolean;
  memo_ACD2AE_1.Text := qryINF700ACD_2AE_1.AsString;

//2AF: APPLICANT
//2AG: BENEFICIARY
//2AH: 직접입력
  com_Charge.ItemIndex := AnsiIndexText(UpperCase(Trim(qryINF700CHARGE.AsString)), ['','2AF','2AG','2AH']);
  memo_Charge_1.Text := qryINF700CHARGE_1.AsString;

  edt_period.Text := qryINF700PERIOD.AsString;
  if qryINF700PERIOD_IDX.IsNull then
    com_PeriodIndex.ItemIndex := 0
  else
    com_PeriodIndex.ItemIndex := qryINF700PERIOD_IDX.AsInteger;
  edt_PeriodDetail.Text := qryINF700PERIOD_TXT.AsString;

  com_Confirm.ItemIndex := AnsiIndexText(qryINF700CONFIRMM.AsString, ['','DA','DB','DC'] );
  edt_CONFIRM_BICCD.Text := qryINF700CONFIRM_BICCD.AsString;
  edt_CONFIRM_BANKNM.Text := qryINF700CONFIRM_BANKNM.AsString;
  edt_CONFIRM_BANKBR.Text := qryINF700CONFIRM_BANKBR.AsString;

  memo_SPECIAL_PAY.Text := qryINF700SPECIAL_PAY.AsString;

//2페이지부터 UI작성 해야함
end;

procedure TUI_INF700_NEW_frm.ReadList(SortField: TColumn);
var
  tmpFieldNm : String;
begin
  with qryINF700 do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'LOCAMR.MAINT_NO';

      IF LeftStr(qryINF700.SQL.Strings[qryINF700.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryINF700.SQL.Strings[qryINF700.SQL.Count-1],4)) = 'ASC' Then
          qryINF700.SQL.Strings[qryINF700.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryINF700.SQL.Strings[qryINF700.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryINF700.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_INF700_NEW_frm.qryINF700AfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_INF700_NEW_frm.qryINF700AfterOpen(DataSet: TDataSet);
begin
  inherited;
  btnDel.Enabled   := DataSet.RecordCount > 0;
  btnPrint.Enabled := DataSet.RecordCount > 0;

  IF DataSet.RecordCount = 0 Then
    qryINF700AfterScroll(DataSet);

 sButton13.Enabled := DataSet.RecordCount > 0;    
end;

procedure TUI_INF700_NEW_frm.FormShow(Sender: TObject);
begin
//  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  sBitBtn1Click(nil);

  ReadOnlyControl(sPanel6);
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel27);
  ReadOnlyControl(sPanel34);
  ReadOnlyControl(sPanel35);
  ReadOnlyControl(sPanel71);
  ReadOnlyControl(sPanel102);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_INF700_NEW_frm.btnDelClick(Sender: TObject);
var
  maint_no : String;
  nCursor : Integer;
begin
//  inherited;
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := qryINF700MAINT_NO.AsString;
   try
     try
       IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
       begin
         nCursor := qryINF700.RecNo;
         SQL.Text :=  'DELETE FROM INF700_1 WHERE MAINT_NO =' + QuotedStr(maint_no);
         ExecSQL;

         SQL.Text :=  'DELETE FROM INF700_2 WHERE MAINT_NO =' + QuotedStr(maint_no);
         ExecSQL;

         SQL.Text :=  'DELETE FROM INF700_3 WHERE MAINT_NO =' + QuotedStr(maint_no);
         ExecSQL;

         SQL.Text :=  'DELETE FROM INF700_4 WHERE MAINT_NO =' + QuotedStr(maint_no);
         ExecSQL;

         //트랜잭션 커밋
         DMMssql.KISConnect.CommitTrans;

         qryINF700.Close;
         qryINF700.Open;

         if (qryINF700.RecordCount > 0 ) AND (qryINF700.RecordCount >= nCursor) Then
         begin
           qryINF700.MoveBy(nCursor-1);
         end
         else
           qryINF700.First;
       end
       else
         DMMssql.KISConnect.RollbackTrans;

     except
       on E:Exception do
       begin
         DMMssql.KISConnect.RollbackTrans;
         MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
       end;
     end;

   finally
     if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
     Close;
     Free;
   end;
  end;
end;

procedure TUI_INF700_NEW_frm.btnPrintClick(Sender: TObject);
begin
//  inherited;
  Preview_frm := TPreview_frm.Create(Self);
  INF700_PRINT_frm := TINF700_PRINT_frm.Create(Self);
  try
    INF700_PRINT_frm.PrintDocument(qryINF700.Fields);
    Preview_frm.Report := INF700_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(INF700_PRINT_frm);
  end;

end;

procedure TUI_INF700_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_INF700_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  inherited;
  Action := caFree;
  UI_INF700_NEW_frm := nil;
end;

const
  UPLOAD_SID : String = 'LDF_KIS';
  SYSDATE : string = 'SYSDATE';
procedure TUI_INF700_NEW_frm.INF700_UPLOAD_LIVART;
var
  SC : TSQLCreate;
  __MAINT_NO : String;
begin
  IF qryINF700.RecordCount = 0 Then
  begin
    raise Exception.Create('업로드할 데이터가 없습니다');
  end;

  SC := TSQLCreate.Create;

  with TADOQuery.Create(nil) do
  begin
    try
      IF not Assigned(LivartConfig_frm) Then LivartConfig_frm := TLivartConfig_frm.Create(Self);
      LivartConfig_frm.setConnectionString;
      Connection := LivartConfig_frm.LivartConn;
      SC.DMLType := dmlInsert;
      SC.SQLHeader('TC_KIS_LCROER_RCV_IF');

      SC.ADDValue('CO_GBCD', '11');
//------------------------------------------------------------------------------
// 2018-02-05
// MAINT_NO 하이픈으로 품의서번호와 일련번호 분리
//------------------------------------------------------------------------------
// 2018-09-14
// MSQ필드 추가하여 순번을 받게 변경
//------------------------------------------------------------------------------
      __MAINT_NO := qryINF700MAINT_NO.AsString;
      SC.ADDValue('IMPTNO', __MAINT_NO);
      SC.ADDValue('IMPTSQ', qryINF700MSEQ.AsString);
//      SC.ADDValue('IMPTNO', LeftStr(__MAINT_NO, POS('_',__MAINT_NO)-1));
//      SC.ADDValue('IMPTSQ', MidStr(__MAINT_NO, POS('_',__MAINT_NO)+1, Length(__MAINT_NO)-POS('_',__MAINT_NO)),vtInteger);

      SC.ADDValue('MESSAGE1', qryINF700MESSAGE1.AsString);
      SC.ADDValue('MESSAGE2', qryINF700MESSAGE2.AsString);
      SC.ADDValue('DATEE', qryINF700DATEE.AsString);
      SC.ADDValue('APP_DATE', qryINF700APP_DATE.AsString);
      SC.ADDValue('IN_MATHOD', qryINF700IN_MATHOD.AsString);
      SC.ADDValue('AP_BANK', qryINF700AP_BANK.AsString);
      SC.ADDValue('AP_BANK1', qryINF700AP_BANK1.AsString);
      SC.ADDValue('AP_BANK2', qryINF700AP_BANK2.AsString);
      SC.ADDValue('AP_BANK3', qryINF700AP_BANK3.AsString);
      SC.ADDValue('AP_BANK4', qryINF700AP_BANK4.AsString);
      SC.ADDValue('AP_BANK5', qryINF700AP_BANK5.AsString);
      SC.ADDValue('AD_BANK', qryINF700AD_BANK.asString);
      SC.ADDValue('AD_BANK1', qryINF700AD_BANK1.asString);
      SC.ADDValue('AD_BANK2', qryINF700AD_BANK2.asString);
      SC.ADDValue('AD_BANK3', qryINF700AD_BANK3.asString);
      SC.ADDValue('AD_BANK4', qryINF700AD_BANK4.asString);
      SC.ADDValue('AD_PAY', qryINF700AD_PAY.asString);
      SC.ADDValue('IMP_CD1', qryINF700IMP_CD1.AsString);
      SC.ADDValue('IMP_CD2', qryINF700IMP_CD2.AsString);
      SC.ADDValue('IMP_CD3', qryINF700IMP_CD3.AsString);
      SC.ADDValue('IMP_CD4', qryINF700IMP_CD4.AsString);
      SC.ADDValue('IMP_CD5', qryINF700IMP_CD5.AsString);
      SC.ADDValue('IL_NO1',  qryINF700IL_NO1.AsString);
      SC.ADDValue('IL_NO2',  qryINF700IL_NO2.AsString);
      SC.ADDValue('IL_NO3', qryINF700IL_NO3.AsString);
      SC.ADDValue('IL_NO4', qryINF700IL_NO4.AsString);
      SC.ADDValue('IL_NO5', qryINF700IL_NO5.AsString);
      SC.ADDValue('IL_CUR1', qryINF700IL_CUR1.AsString);
      SC.ADDValue('IL_CUR2', qryINF700IL_CUR2.AsString);
      SC.ADDValue('IL_CUR3', qryINF700IL_CUR3.AsString);
      SC.ADDValue('IL_CUR4', qryINF700IL_CUR4.AsString);
      SC.ADDValue('IL_CUR5', qryINF700IL_CUR5.AsString);
      SC.ADDValue('IL_AMT1', qryINF700IL_AMT1.AsString,vtInteger);
      SC.ADDValue('IL_AMT2', qryINF700IL_AMT2.AsString,vtInteger);
      SC.ADDValue('IL_AMT3', qryINF700IL_AMT3.AsString,vtInteger);
      SC.ADDValue('IL_AMT4', qryINF700IL_AMT4.AsString,vtInteger);
      SC.ADDValue('IL_AMT5', qryINF700IL_AMT5.AsString,vtInteger);
      SC.ADDValue('AD_INFO1', qryINF700AD_INFO1.AsString);
      SC.ADDValue('AD_INFO2', qryINF700AD_INFO2.AsString);
      SC.ADDValue('AD_INFO3', qryINF700AD_INFO3.AsString);
      SC.ADDValue('AD_INFO4', qryINF700AD_INFO4.AsString);
      SC.ADDValue('AD_INFO5', qryINF700AD_INFO5.AsString);
      SC.ADDValue('EX_NAME1', qryINF700EX_NAME1.AsString);
      SC.ADDValue('EX_NAME2', qryINF700EX_NAME2.AsString);
      SC.ADDValue('EX_NAME3', qryINF700EX_NAME3.AsString);
      SC.ADDValue('EX_ADDR1', qryINF700EX_ADDR1.AsString);
      SC.ADDValue('EX_ADDR2', qryINF700EX_ADDR2.AsString);
      SC.ADDValue('DOC_CD', qryINF700DOC_CD.AsString);
      SC.ADDValue('CD_NO', qryINF700CD_NO.AsString);
      SC.ADDValue('REF_PRE', qryINF700REF_PRE.AsString);
      SC.ADDValue('ISS_DATE', qryINF700ISS_DATE.AsString);
      SC.ADDValue('EX_DATE', qryINF700EX_DATE.AsString);
      SC.ADDValue('EX_PLACE', qryINF700EX_PLACE.AsString);
      SC.ADDValue('CHK2', qryINF700CHK2.AsString);
      SC.ADDValue('CHK3', qryINF700CHK3.AsString);
      SC.ADDValue('APPLICABLE_RULES_1', qryINF700APPLICABLE_RULES_1.AsString);
      SC.ADDValue('APPLICABLE_RULES_2', qryINF700APPLICABLE_RULES_2.AsString);
      SC.ADDValue('APP_BANK', qryINF700APP_BANK.AsString);
      SC.ADDValue('APP_BANK1', qryINF700APP_BANK1.AsString);
      SC.ADDValue('APP_BANK2', qryINF700APP_BANK2.AsString);
      SC.ADDValue('APP_BANK3', qryINF700APP_BANK3.AsString);
      SC.ADDValue('APP_BANK4', qryINF700APP_BANK4.AsString);
      SC.ADDValue('APP_BANK5', qryINF700APP_BANK5.AsString);
      SC.ADDValue('APP_ACCNT', qryINF700APP_ACCNT.AsString);
      SC.ADDValue('APPLIC1', qryINF700APPLIC1.AsString);
      SC.ADDValue('APPLIC2', qryINF700APPLIC2.AsString);
      SC.ADDValue('APPLIC3', qryINF700APPLIC3.AsString);
      SC.ADDValue('APPLIC4', qryINF700APPLIC4.AsString);
      SC.ADDValue('APPLIC5', qryINF700APPLIC5.AsString);
      SC.ADDValue('BEBEFC1', qryINF700BENEFC1.AsString);
      SC.ADDValue('BEBEFC2', qryINF700BENEFC2.AsString);
      SC.ADDValue('BEBEFC3', qryINF700BENEFC3.AsString);
      SC.ADDValue('BEBEFC4', qryINF700BENEFC4.AsString);
      SC.ADDValue('BEBEFC5', qryINF700BENEFC5.AsString);
      SC.ADDValue('CD_AMT', qryINF700CD_AMT.AsString,vtInteger);
      SC.ADDValue('CD_CUR', qryINF700CD_CUR.AsString);
      SC.ADDValue('CD_PERP', qryINF700CD_PERP.AsString);
      SC.ADDValue('CD_PERM', qryINF700CD_PERM.AsString);
      SC.ADDValue('CD_MAX', qryINF700CD_MAX.AsString);
      SC.ADDValue('TERM_PR', qryINF700TERM_PR.AsString);
      SC.ADDValue('TERM_PR_M', qryINF700TERM_PR_M.AsString);
      SC.ADDValue('PL_TERM', qryINF700PL_TERM.AsString);
      SC.ADDValue('AA_CV1', qryINF700AA_CV1.AsString);
      SC.ADDValue('AA_CV2', qryINF700AA_CV2.AsString);
      SC.ADDValue('AA_CV3', qryINF700AA_CV3.AsString);
      SC.ADDValue('AA_CV4', qryINF700AA_CV4.AsString);
      SC.ADDValue('DRAFT1', qryINF700DRAFT1.AsString);
      SC.ADDValue('DRAFT2', qryINF700DRAFT2.AsString);
      SC.ADDValue('DRAFT3', qryINF700DRAFT3.AsString);
      SC.ADDValue('MIX_PAY1', qryINF700MIX_PAY1.AsString);
      SC.ADDValue('MIX_PAY2', qryINF700MIX_PAY1.AsString);
      SC.ADDValue('MIX_PAY3', qryINF700MIX_PAY1.AsString);
      SC.ADDValue('MIX_PAY4', qryINF700MIX_PAY1.AsString);
      SC.ADDValue('DEF_PAY1', qryINF700DEF_PAY1.AsString);
      SC.ADDValue('DEF_PAY2', qryINF700DEF_PAY2.AsString);
      SC.ADDValue('DEF_PAY3', qryINF700DEF_PAY3.AsString);
      SC.ADDValue('DEF_PAY4', qryINF700DEF_PAY4.AsString);
      SC.ADDValue('AVT_BANK1', qryINF700AVT_BANK1.AsString);
      SC.ADDValue('AVT_BANK2', qryINF700AVT_BANK2.AsString);
      SC.ADDValue('AVT_BANK3', qryINF700AVT_BANK3.AsString);
      SC.ADDValue('AVT_BANK4', qryINF700AVT_BANK4.AsString);
      SC.ADDValue('AVT_BANK5', qryINF700AVT_BANK5.AsString);
      SC.ADDValue('AVT_ACCNT', qryINF700AVT_ACCNT.AsString);
      SC.ADDValue('SND_INFO1', qryINF700SND_INFO1.asString);
      SC.ADDValue('SND_INFO2', qryINF700SND_INFO2.asString);
      SC.ADDValue('SND_INFO3', qryINF700SND_INFO3.asString);
      SC.ADDValue('SND_INFO4', qryINF700SND_INFO4.asString);
      SC.ADDValue('SND_INFO5', qryINF700SND_INFO5.asString);
      SC.ADDValue('SND_INFO6', qryINF700SND_INFO6.asString);
      SC.ADDValue('AVAIL1', qryINF700AVAIL1.AsString);
      SC.ADDValue('AVAIL2', qryINF700AVAIL2.AsString);
      SC.ADDValue('AVAIL3', qryINF700AVAIL3.AsString);
      SC.ADDValue('AVAIL4', qryINF700AVAIL4.AsString);
      SC.ADDValue('AV_ACCNT', qryINF700AV_ACCNT.AsString);
      SC.ADDValue('AV_PAY', qryINF700AV_PAY.AsString);
      SC.ADDValue('DRAWEE1', qryINF700DRAWEE1.AsString);
      SC.ADDValue('DRAWEE2', qryINF700DRAWEE2.AsString);
      SC.ADDValue('DRAWEE3', qryINF700DRAWEE3.AsString);
      SC.ADDValue('DRAWEE4', qryINF700DRAWEE4.AsString);
      SC.ADDValue('DR_ACCNT', qryINF700DR_ACCNT.AsString);
      SC.ADDValue('REI_BANK', qryINF700REI_BANK.AsString);
      SC.ADDValue('REI_BANK1', qryINF700REI_BANK1.AsString);
      SC.ADDValue('REI_BANK2', qryINF700REI_BANK2.AsString);
      SC.ADDValue('REI_BANK3', qryINF700REI_BANK3.AsString);
      SC.ADDValue('REI_BANK4', qryINF700REI_BANK4.AsString);
      SC.ADDValue('REI_BANK5', qryINF700REI_BANK5.AsString);
      SC.ADDValue('REI_ACCNT', qryINF700REI_ACNNT.AsString);
      SC.ADDValue('OP_BANK1', qryINF700OP_BANK1.AsString);
      SC.ADDValue('OP_BANK2', qryINF700OP_BANK2.AsString);
      SC.ADDValue('OP_BANK3', qryINF700OP_BANK3.AsString);
      SC.ADDValue('OP_ADDR1', qryINF700OP_ADDR1.AsString);
      SC.ADDValue('OP_ADDR2', qryINF700OP_ADDR2.AsString);
      SC.ADDValue('OP_ADDR3', qryINF700OP_ADDR3.AsString);
      SC.ADDValue('PSHIP', qryINF700PSHIP.AsString);
      SC.ADDValue('TSHIP', qryINF700TSHIP.AsString);
      SC.ADDValue('SUNJUCK_PORT', qryINF700SUNJUCK_PORT.AsString);
      SC.ADDValue('DOCHACK_PORT', qryINF700DOCHACK_PORT.AsString);
      SC.ADDValue('LOAD_ON', qryINF700LOAD_ON.AsString);
      SC.ADDValue('FOR_TRAN', qryINF700FOR_TRAN.AsString);
      SC.ADDValue('LST_DATE', qryINF700LST_DATE.AsString);
      SC.ADDValue('SHIP_PD1', qryINF700SHIP_PD1.AsString);
      SC.ADDValue('SHIP_PD2', qryINF700SHIP_PD2.AsString);
      SC.ADDValue('SHIP_PD3', qryINF700SHIP_PD3.AsString);
      SC.ADDValue('SHIP_PD4', qryINF700SHIP_PD4.AsString);
      SC.ADDValue('SHIP_PD5', qryINF700SHIP_PD5.AsString);
      SC.ADDValue('SHIP_PD6', qryINF700SHIP_PD6.AsString);
      SC.ADDValue('ORIGIN', qryINF700ORIGIN.AsString);
      SC.ADDValue('ORIGIN_M', qryINF700ORIGIN_M.AsString);
      IF qryINF700DOC_380.AsBoolean Then
        SC.ADDValue('DOC_380', '1')
      else
        SC.ADDValue('DOC_380', '0');
      SC.ADDValue('DOC_380_1', qryINF700DOC_380_1.AsString);

      IF qryINF700DOC_705.AsBoolean Then
        SC.ADDValue('DOC_705', '1')
      else
        SC.ADDValue('DOC_705', '0');

      SC.ADDValue('DOC_705_GUBUN',qryINF700DOC_705_GUBUN.AsString);
      SC.ADDValue('DOC_705_1', qryINF700DOC_705_1.AsString);
      SC.ADDValue('DOC_705_2', qryINF700DOC_705_2.AsString);
      SC.ADDValue('DOC_705_3', qryINF700DOC_705_3.AsString);
      SC.ADDValue('DOC_705_4', qryINF700DOC_705_4.AsString);

      IF qryINF700DOC_740.AsBoolean Then
        SC.ADDValue('DOC_740','1')
      else
        SC.ADDValue('DOC_740','0');
      SC.ADDValue('DOC_740_1', qryINF700DOC_740_1.AsString);
      SC.ADDValue('DOC_740_2', qryINF700DOC_740_2.AsString);
      SC.ADDValue('DOC_740_3', qryINF700DOC_740_3.AsString);
      SC.ADDValue('DOC_740_4', qryINF700DOC_740_4.AsString);
      IF qryINF700DOC_760.AsBoolean Then
        SC.ADDValue('DOC_760','1')
      else
        SC.ADDValue('DOC_760','0');

      SC.ADDValue('DOC_760_1', qryINF700DOC_760_1.AsString);
      SC.ADDValue('DOC_760_2', qryINF700DOC_760_2.AsString);
      SC.ADDValue('DOC_760_3', qryINF700DOC_760_3.AsString);
      SC.ADDValue('DOC_760_4', qryINF700DOC_760_4.AsString);

      IF qryINF700DOC_530.AsBoolean Then
        SC.ADDValue('DOC_530','1')
      else
        SC.ADDValue('DOC_530','0');

      SC.ADDValue('DOC_530_1', qryINF700DOC_530_1.AsString);
      SC.ADDValue('DOC_530_2', qryINF700DOC_530_2.AsString);

      IF qryINF700DOC_271.AsBoolean Then
        SC.ADDValue('DOC_271','1')
      else
        SC.ADDValue('DOC_271','0');
      SC.ADDValue('DOC_271_1', qryINF700DOC_271_1.AsString);

      IF qryINF700DOC_861.AsBoolean Then
        SC.ADDValue('DOC_861','1')
      else
        SC.ADDValue('DOC_861','0');

      if qryINF700DOC_2AA.AsBoolean Then
        SC.ADDValue('DOC_2AA','1')
      else
        SC.ADDValue('DOC_2AA','0');

      SC.ADDValue('DOC_2AA_1', qryINF700DOC_2AA_1.AsString);

      if qryINF700ACD_2AA.AsBoolean Then
        SC.ADDValue('ACD_2AA','1')
      else
        SC.ADDValue('ACD_2AA','0');
      SC.ADDValue('ACD_2AA_1', qryINF700ACD_2AA_1.AsString);

      if qryINF700ACD_2AB.AsBoolean Then
        SC.ADDValue('ACD_2AB','1')
      else
        SC.ADDValue('ACD_2AB','0');

      if qryINF700ACD_2AC.AsBoolean Then
        SC.ADDValue('ACD_2AC','1')
      else
        SC.ADDValue('ACD_2AC','0');

      if qryINF700ACD_2AD.AsBoolean Then
        SC.ADDValue('ACD_2AD','1')
      else
        SC.ADDValue('ACD_2AD','0');

      if qryINF700ACD_2AE.AsBoolean Then
        SC.ADDValue('ACD_2AE','1')
      else
        SC.ADDValue('ACD_2AE','0');
      SC.ADDValue('ACD_2AE_1',qryINF700ACD_2AE_1.AsString);
      IF qryINF700INSTRCT.AsBoolean Then
        SC.ADDValue('INSTRCT','1')
      else
        SC.ADDValue('INSTRCT','0');

      SC.ADDValue('INSTRCT_1', qryINF700INSTRCT_1.AsString);
      SC.ADDValue('CHARGE', qryINF700CHARGE.AsString);
      SC.ADDValue('PERIOD', qryINF700PERIOD.AsString);
      SC.ADDValue('CONFIRM', qryINF700CONFIRMM.AsString);
//      SC.ADDValue('CONFIRMM', qryINF700CONFIRMM.AsString);
      IF qryINF700DESGOOD.AsBoolean Then
        SC.ADDValue('DESGOOD', '1')
      else
        SC.ADDValue('DESGOOD', '0');

      SC.ADDValue('DESGOOD_1', qryINF700DESGOOD_1.AsString);
      //------------------------------------------------------------------------------
      // 18-11-19 SWITFT 변경
      //------------------------------------------------------------------------------
      SC.ADDValue('PERIOD_IDX', qryINF700PERIOD_IDX.AsString, vtInteger);
      SC.ADDValue('PERIOD_TXT', qryINF700PERIOD_TXT.AsString);
      SC.ADDValue('CONFIRM_BANKNM', qryINF700CONFIRM_BANKNM.AsString);
      SC.ADDValue('CONFIRM_BANKBR', qryINF700CONFIRM_BANKBR.AsString);
      SC.ADDValue('CHARGE_1', qryINF700CHARGE_1.AsString);
      SC.ADDValue('SPECIAL_PAY', qryINF700SPECIAL_PAY.AsString);

      SC.ADDValue('FRST_REG_USER_ID',UPLOAD_SID);
      SC.ADDValue('FRST_REG_DTM',SYSDATE,vtVariant);
      SC.ADDValue('LAST_MOD_USER_ID',UPLOAD_SID);
      SC.ADDValue('LAST_MOD_DTM',SYSDATE,vtVariant);
      AddLog('INF700',SC.FieldList);      
      SQL.Text := SC.CreateSQL;
      ExecSQL;
    finally
      LivartConfig_frm.LivartConn.Close;
      FreeAndNil(LivartConfig_frm);
      Close;
      Free;
    end;
  end;

end;

procedure TUI_INF700_NEW_frm.sButton13Click(Sender: TObject);
begin
//  inherited;
  INF700_UPLOAD_LIVART;
end;

end.
