unit UI_LOCAMA_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_LOCAMR_NEW, DB, ADODB, sSkinProvider, StdCtrls, sComboBox,
  sMemo, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, StrUtils, TypeDefine;

type
  TUI_LOCAMA_NEW_frm = class(TUI_LOCAMR_NEW_frm)
    qryLOCAMA: TADOQuery;
    qryLOCAMAMAINT_NO: TStringField;
    qryLOCAMAMSEQ: TIntegerField;
    qryLOCAMADOCNO: TStringField;
    qryLOCAMAAMD_NO: TIntegerField;
    qryLOCAMAUSER_ID: TStringField;
    qryLOCAMADATEE: TStringField;
    qryLOCAMABGM_REF: TStringField;
    qryLOCAMAMESSAGE1: TStringField;
    qryLOCAMAMESSAGE2: TStringField;
    qryLOCAMARFF_NO: TStringField;
    qryLOCAMALC_NO: TStringField;
    qryLOCAMAOFFERNO1: TStringField;
    qryLOCAMAOFFERNO2: TStringField;
    qryLOCAMAOFFERNO3: TStringField;
    qryLOCAMAOFFERNO4: TStringField;
    qryLOCAMAOFFERNO5: TStringField;
    qryLOCAMAOFFERNO6: TStringField;
    qryLOCAMAOFFERNO7: TStringField;
    qryLOCAMAOFFERNO8: TStringField;
    qryLOCAMAOFFERNO9: TStringField;
    qryLOCAMAAMD_DATE: TStringField;
    qryLOCAMAADV_DATE: TStringField;
    qryLOCAMAISS_DATE: TStringField;
    qryLOCAMAREMARK: TStringField;
    qryLOCAMAREMARK1: TMemoField;
    qryLOCAMAISSBANK: TStringField;
    qryLOCAMAISSBANK1: TStringField;
    qryLOCAMAISSBANK2: TStringField;
    qryLOCAMAAPPLIC1: TStringField;
    qryLOCAMAAPPLIC2: TStringField;
    qryLOCAMAAPPLIC3: TStringField;
    qryLOCAMABENEFC1: TStringField;
    qryLOCAMABENEFC2: TStringField;
    qryLOCAMABENEFC3: TStringField;
    qryLOCAMAEXNAME1: TStringField;
    qryLOCAMAEXNAME2: TStringField;
    qryLOCAMAEXNAME3: TStringField;
    qryLOCAMADELIVERY: TStringField;
    qryLOCAMAEXPIRY: TStringField;
    qryLOCAMACHGINFO: TStringField;
    qryLOCAMACHGINFO1: TMemoField;
    qryLOCAMALOC_TYPE: TStringField;
    qryLOCAMALOC1AMT: TBCDField;
    qryLOCAMALOC1AMTC: TStringField;
    qryLOCAMALOC2AMT: TBCDField;
    qryLOCAMALOC2AMTC: TStringField;
    qryLOCAMAEX_RATE: TBCDField;
    qryLOCAMACHK1: TStringField;
    qryLOCAMACHK2: TStringField;
    qryLOCAMACHK3: TStringField;
    qryLOCAMAPRNO: TIntegerField;
    qryLOCAMAAPPADDR1: TStringField;
    qryLOCAMAAPPADDR2: TStringField;
    qryLOCAMAAPPADDR3: TStringField;
    qryLOCAMABNFADDR1: TStringField;
    qryLOCAMABNFADDR2: TStringField;
    qryLOCAMABNFADDR3: TStringField;
    qryLOCAMABNFEMAILID: TStringField;
    qryLOCAMABNFDOMAIN: TStringField;
    qryLOCAMACD_PERP: TIntegerField;
    qryLOCAMACD_PERM: TIntegerField;
    qryLOCAMALOC_TYPENAME: TStringField;
    edt_LOC2AMTC: TsEdit;
    cur_LOC2AMT: TsCurrencyEdit;
    edt_EX_RATE: TsEdit;
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  protected
    procedure ReadList(SortField: TColumn = nil); override;
    procedure ReadData; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_LOCAMA_NEW_frm: TUI_LOCAMA_NEW_frm;

implementation

uses MSSQL, MessageDefine, LOCAMA_PRINT, Preview;

{$R *.dfm}

{ TUI_LOCAMA_NEW_frm }

procedure TUI_LOCAMA_NEW_frm.ReadData;
var
  TempStr : String;
begin
  ClearControl(sPanel6);
  ClearControl(sPanel7);
  ClearControl(sPanel27);
  
  with qryLOCAMA do
  begin
    if RecordCount > 0 then
    begin
      edt_MAINT_NO.Text := qryLOCAMAMAINT_NO.AsString;
      edt_MSEQ.Text := qryLOCAMAMSEQ.AsString;

      msk_Datee.Text := qryLOCAMADATEE.AsString;
      edt_userno.Text := qryLOCAMAUSER_ID.AsString;

      CM_INDEX(com_func, [':'], 0, qryLOCAMAMESSAGE1.AsString, 5);
      CM_INDEX(com_type, [':'], 0, qryLOCAMAMESSAGE2.AsString);      

      //개설일자
      sMaskEdit5.Text := qryLOCAMAISS_DATE.AsString;
      //조건변경신청일
      sMaskEdit6.Text := qryLOCAMAAMD_DATE.AsString;
      //조건변경차수
      cur_chasu.Text := qryLOCAMAAMD_NO.AsString;

      //개설의뢰인
//      edt_wcd.Text     := qryLOCAMAAPPLIC.AsString;
      edt_wsangho.Text := qryLOCAMAAPPLIC1.AsString;
      edt_wceo.Text    := qryLOCAMAAPPLIC2.AsString;
      edt_waddr1.Text  := qryLOCAMAAPPADDR1.AsString;
      edt_waddr2.Text  := qryLOCAMAAPPADDR2.AsString;
      edt_waddr3.Text  := qryLOCAMAAPPADDR3.AsString;
      edt_wsaupno.Text := qryLOCAMAAPPLIC3.AsString;

      //수혜자
//      edt_rcd.Text     := qryLOCAMABENEFC.AsString;
      edt_rsangho.Text := qryLOCAMABENEFC1.AsString;
      edt_rceo.Text    := qryLOCAMABENEFC2.AsString;
      edt_raddr1.Text  := qryLOCAMABNFADDR1.AsString;
      edt_raddr2.Text  := qryLOCAMABNFADDR2.AsString;
      edt_raddr3.Text  := qryLOCAMABNFADDR3.AsString;
      edt_rsaupno.Text := qryLOCAMABENEFC3.AsString;
      
      TempStr := qryLOCAMABNFEMAILID.AsString+ '@' +qryLOCAMABNFDOMAIN.AsString;
      IF TempStr = '@' Then
        edt_remail.Clear
      else
        edt_remail.Text  := TempStr;

      //수발신인식별자
//      edt_iden1.Text := qryLOCAMACHKNAME1.AsString;
//      edt_iden2.Text := qryLOCAMACHKNAME2.AsString;

      //개설은행
      edt_cbank.Text   := qryLOCAMAISSBANK.AsString;
      edt_cbanknm.Text := qryLOCAMAISSBANK1.AsString;
      edt_cbrunch.Text := qryLOCAMAISSBANK2.AsString;

      //신용장종류
      edt_cType.Text := qryLOCAMALOC_TYPE.AsString;
      sEdit8.Text := qryLOCAMALOC_TYPENAME.AsString;

      //변경후금액(외화)
      edt_LOCAMT_UNIT.Text := qryLOCAMALOC1AMTC.AsString;
      cur_LOCAMT.Value := qryLOCAMALOC1AMT.AsCurrency;
      //원화
      edt_LOC2AMTC.Text := qryLOCAMALOC2AMTC.AsString;
      cur_LOC2AMT.value := qryLOCAMALOC2AMT.AsCurrency;
      //매매기준율
      edt_EX_RATE.Text := qryLOCAMAEX_RATE.AsString;

      //허용오차
      //+
      IF Trim(qryLOCAMACD_PERP.AsString) = '0' Then
        edt_perr.Clear
      else
        edt_perr.Text := qryLOCAMACD_PERP.AsString;
      //-
      IF Trim(qryLOCAMACD_PERM.AsString) = '0' Then
        edt_merr.Clear
      else
        edt_merr.Text := qryLOCAMACD_PERM.AsString;

      //LC번호
      edt_lcno.Text := qryLOCAMALC_NO.AsString;

      //매도확약서
      edt_doc1.Text := qryLOCAMAOFFERNO1.AsString;
      edt_doc2.Text := qryLOCAMAOFFERNO2.AsString;
      edt_doc3.Text := qryLOCAMAOFFERNO3.AsString;
      edt_doc4.Text := qryLOCAMAOFFERNO4.AsString;
      edt_doc5.Text := qryLOCAMAOFFERNO5.AsString;
      edt_doc6.Text := qryLOCAMAOFFERNO6.AsString;
      edt_doc7.Text := qryLOCAMAOFFERNO7.AsString;
      edt_doc8.Text := qryLOCAMAOFFERNO8.AsString;
      edt_doc9.Text := qryLOCAMAOFFERNO9.AsString;

      memo_Etc.Text    := qryLOCAMAREMARK1.AsString;
      memo_etcamd.Text := qryLOCAMACHGINFO1.AsString;

      //물품인도기일
      msk_IndoDt.Text := qryLOCAMADELIVERY.AsString;
      //유효기일
      msk_ExpiryDT.Text := qryLOCAMAEXPIRY.AsString;

      //발신기관 전자서명
      edt_Sign1.Text := qryLOCAMAEXNAME1.AsString;
      edt_Sign2.Text := qryLOCAMAEXNAME2.AsString;
      edt_Sign3.Text := qryLOCAMAEXNAME3.AsString;
    end;
  end;
end;

procedure TUI_LOCAMA_NEW_frm.ReadList(SortField: TColumn);
var
  tmpFieldNm : String;
begin
  with qryLOCAMA do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'LOCAMA.MAINT_NO';

      IF LeftStr(qryLOCAMA.SQL.Strings[qryLOCAMA.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryLOCAMA.SQL.Strings[qryLOCAMA.SQL.Count-1],4)) = 'ASC' Then
          qryLOCAMA.SQL.Strings[qryLOCAMA.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryLOCAMA.SQL.Strings[qryLOCAMA.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryLOCAMA.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_LOCAMA_NEW_frm.btnDelClick(Sender: TObject);
var
  DocNo : TDOCNO;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;

    DocNo.MAINT_NO := qryLOCAMAMAINT_NO.AsString;
    DocNo.MSEQ := qryLOCAMAMSEQ.AsInteger;
    DocNo.AMD_NO := qryLOCAMAAMD_NO.AsInteger;

    try
      try
        IF MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_LINE+'내국신용장 조건변경응답서'#13#10+DocNo.MAINT_NO+'/'+inttostr(DocNo.MSEQ)+'/'+inttostr(DocNo.AMD_NO)+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryLOCAMA.RecNo;
          SQL.Text := 'DELETE FROM LOCAMA WHERE MAINT_NO = '+QuotedStr(DocNo.MAINT_NO)+' AND MSEQ = '+IntToStr(DocNo.MSEQ)+' AND AMD_NO = '+IntToStr(DocNo.AMD_NO);
          ExecSQL;
          DMMssql.KISConnect.CommitTrans;

          qryLOCAMA.Close;
          qryLOCAMA.open;

          IF (qryLOCAMA.RecordCount > 0) AND (qryLOCAMA.RecordCount >= nCursor) Then
          begin
            qryLOCAMA.MoveBy(nCursor-1);
          end
          else
            qryLOCAMA.Last;
        end
        else
        begin
          if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans; 
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;
procedure TUI_LOCAMA_NEW_frm.btnPrintClick(Sender: TObject);
begin
//  inherited;
  Preview_frm := TPreview_frm.Create(Self);
  LOCAMA_PRINT_frm := TLOCAMA_PRINT_frm.Create(Self);
  try
    LOCAMA_PRINT_frm.PrintDocument(qryLOCAMA.Fields);
    Preview_frm.Report := LOCAMA_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(LOCAMA_PRINT_frm);
  end;

end;

procedure TUI_LOCAMA_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  inherited;
  Action := caFree;
  UI_LOCAMA_NEW_frm := nil;
end;

end.
