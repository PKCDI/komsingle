unit UI_ADV759_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sMemo, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, ComCtrls, sPageControl, sComboBox, Buttons, sBitBtn, Mask,
  sMaskEdit, sEdit, Grids, DBGrids, acDBGrid, sButton, sLabel,
  sSpeedButton, ExtCtrls, sPanel, sSkinProvider, dbcgrids, acDBCtrlGrid,
  DBCtrls, sDBEdit, sDBMemo, DB, DBClient;

type
  TUI_ADV759_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sHeader: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    edt_MSEQ: TsEdit;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPage1: TsPanel;
    sPanel2: TsPanel;
    msk_NTC_DT: TsMaskEdit;
    sPanel8: TsPanel;
    edt_ApBank: TsEdit;
    edt_ApBank1: TsEdit;
    edt_ApBank2: TsEdit;
    edt_ApBank3: TsEdit;
    edt_ApBank4: TsEdit;
    sPanel91: TsPanel;
    sTabSheet3: TsTabSheet;
    sPage2: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    sPanel12: TsPanel;
    sEdit2: TsEdit;
    sMemo1: TsMemo;
    sPanel1: TsPanel;
    sPanel6: TsPanel;
    sPanel7: TsPanel;
    sEdit3: TsEdit;
    sEdit4: TsEdit;
    sEdit5: TsEdit;
    sEdit6: TsEdit;
    sEdit7: TsEdit;
    sPanel9: TsPanel;
    sPanel10: TsPanel;
    sPanel14: TsPanel;
    edt_Applic1: TsEdit;
    edt_Applic2: TsEdit;
    edt_Applic3: TsEdit;
    edt_Applic4: TsEdit;
    sPanel11: TsPanel;
    sEdit8: TsEdit;
    sEdit9: TsEdit;
    sEdit10: TsEdit;
    sEdit11: TsEdit;
    sPanel13: TsPanel;
    sEdit12: TsEdit;
    sPanel82: TsPanel;
    edt_EXName1: TsEdit;
    edt_EXName2: TsEdit;
    edt_EXName3: TsEdit;
    edt_EXAddr1: TsEdit;
    edt_EXAddr2: TsEdit;
    sPanel83: TsPanel;
    sPanel84: TsPanel;
    sPanel85: TsPanel;
    sPanel86: TsPanel;
    sDBCtrlGrid1: TsDBCtrlGrid;
    sPanel15: TsPanel;
    sDBEdit1: TsDBEdit;
    sPanel16: TsPanel;
    sPanel17: TsPanel;
    sDBEdit2: TsDBEdit;
    sPanel18: TsPanel;
    sDBEdit3: TsDBEdit;
    sPanel19: TsPanel;
    sDBEdit4: TsDBEdit;
    sPanel21: TsPanel;
    sDBEdit5: TsDBEdit;
    sPanel22: TsPanel;
    sPanel23: TsPanel;
    sDBEdit6: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sPanel25: TsPanel;
    sDBEdit11: TsDBEdit;
    sDBEdit12: TsDBEdit;
    sPanel26: TsPanel;
    sDBEdit13: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit15: TsDBEdit;
    sPanel27: TsPanel;
    sDBMemo1: TsDBMemo;
    ClientDataSet1: TClientDataSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_ADV759_NEW_frm: TUI_ADV759_NEW_frm;

implementation

{$R *.dfm}

end.
