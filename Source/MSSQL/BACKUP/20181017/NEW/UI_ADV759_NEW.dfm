inherited UI_ADV759_NEW_frm: TUI_ADV759_NEW_frm
  Left = 232
  Top = 159
  BorderWidth = 4
  Caption = 'UI_ADV759_NEW_frm'
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    object sSpeedButton4: TsSpeedButton
      Left = 1027
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 6
      Width = 91
      Height = 17
      Caption = #48372#51312#47924#50669#53685#51648#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 21
      Width = 41
      Height = 13
      Caption = 'ADV759'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 248
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1034
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 113
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 179
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 41
    Width = 341
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 339
      Height = 574
      TabStop = False
      Align = alClient
      Color = clGray
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 175
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MSEQ'
          Title.Alignment = taCenter
          Title.Caption = #49692#48264
          Width = 32
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 94
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 339
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 242
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 61
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 61
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 154
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sBitBtn1: TsBitBtn
        Left = 261
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 341
    Top = 41
    Width = 773
    Height = 632
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sHeader: TsPanel
      Left = 1
      Top = 1
      Width = 771
      Height = 55
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton1: TsSpeedButton
        Left = 280
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 4
        Width = 183
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
      object msk_Datee: TsMaskEdit
        Left = 64
        Top = 28
        Width = 89
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object com_func: TsComboBox
        Left = 360
        Top = 4
        Width = 121
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 360
        Top = 28
        Width = 211
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 216
        Top = 28
        Width = 57
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object edt_MSEQ: TsEdit
        Left = 248
        Top = 4
        Width = 25
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentFont = False
        TabOrder = 5
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 56
      Width = 771
      Height = 575
      ActivePage = sTabSheet1
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabHeight = 26
      TabIndex = 0
      TabOrder = 2
      TabPadding = 10
      object sTabSheet1: TsTabSheet
        Caption = #53685#51648#51221#48372
        object sPage1: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel2: TsPanel
            Left = 12
            Top = 4
            Width = 79
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #53685#51648#51068#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
          object msk_NTC_DT: TsMaskEdit
            Left = 92
            Top = 4
            Width = 96
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 0
            Text = '20180621'
            CheckOnExit = True
          end
          object sPanel8: TsPanel
            Left = 68
            Top = 55
            Width = 111
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #51204#47928#48156#49888#51008#54665
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 7
          end
          object edt_ApBank: TsEdit
            Tag = 1001
            Left = 258
            Top = 55
            Width = 121
            Height = 23
            Hint = '4'#51088#47532'('#48376#51216'+'#51648#51216')'
            Color = 12775866
            MaxLength = 4
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank1: TsEdit
            Left = 68
            Top = 79
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank2: TsEdit
            Left = 68
            Top = 103
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank3: TsEdit
            Left = 68
            Top = 127
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ApBank4: TsEdit
            Left = 68
            Top = 151
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel91: TsPanel
            Tag = -1
            Left = 12
            Top = 55
            Width = 55
            Height = 119
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'MT759 Header'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 8
          end
          object sPanel12: TsPanel
            Left = 12
            Top = 28
            Width = 79
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #53685#51648#48264#54840
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 9
          end
          object sEdit2: TsEdit
            Left = 92
            Top = 28
            Width = 231
            Height = 23
            Color = clWhite
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sMemo1: TsMemo
            Left = 92
            Top = 178
            Width = 654
            Height = 86
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 11
          end
          object sPanel1: TsPanel
            Left = 12
            Top = 178
            Width = 79
            Height = 86
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44592#53440#51221#48372
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 12
          end
          object sPanel6: TsPanel
            Tag = -1
            Left = 180
            Top = 55
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'BIC'#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 13
          end
          object sPanel7: TsPanel
            Left = 436
            Top = 55
            Width = 111
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #51204#47928#49688#49888#51008#54665
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 14
          end
          object sEdit3: TsEdit
            Tag = 1001
            Left = 626
            Top = 55
            Width = 121
            Height = 23
            Hint = '4'#51088#47532'('#48376#51216'+'#51648#51216')'
            Color = 12775866
            MaxLength = 4
            ParentShowHint = False
            ShowHint = True
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit4: TsEdit
            Left = 436
            Top = 79
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit5: TsEdit
            Left = 436
            Top = 103
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit6: TsEdit
            Left = 436
            Top = 127
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit7: TsEdit
            Left = 436
            Top = 151
            Width = 311
            Height = 23
            Color = clWhite
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel9: TsPanel
            Tag = -1
            Left = 380
            Top = 55
            Width = 55
            Height = 119
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'MT759 Header'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 20
          end
          object sPanel10: TsPanel
            Tag = -1
            Left = 548
            Top = 55
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'BIC'#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 21
          end
          object sPanel14: TsPanel
            Left = 12
            Top = 268
            Width = 79
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #44060#49444#51032#47280#51064
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 22
          end
          object edt_Applic1: TsEdit
            Left = 12
            Top = 292
            Width = 365
            Height = 23
            Color = clWhite
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic2: TsEdit
            Left = 12
            Top = 316
            Width = 365
            Height = 23
            Color = clWhite
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic3: TsEdit
            Left = 12
            Top = 340
            Width = 365
            Height = 23
            Color = clWhite
            TabOrder = 25
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_Applic4: TsEdit
            Left = 12
            Top = 364
            Width = 365
            Height = 23
            Color = clWhite
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel11: TsPanel
            Left = 380
            Top = 268
            Width = 79
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49688#51061#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object sEdit8: TsEdit
            Left = 380
            Top = 292
            Width = 365
            Height = 23
            Color = clWhite
            TabOrder = 28
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit9: TsEdit
            Left = 380
            Top = 316
            Width = 365
            Height = 23
            Color = clWhite
            TabOrder = 29
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit10: TsEdit
            Left = 380
            Top = 340
            Width = 365
            Height = 23
            Color = clWhite
            TabOrder = 30
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sEdit11: TsEdit
            Left = 380
            Top = 364
            Width = 365
            Height = 23
            Color = clWhite
            TabOrder = 31
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel13: TsPanel
            Tag = -1
            Left = 380
            Top = 388
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51204#54868#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object sEdit12: TsEdit
            Left = 458
            Top = 388
            Width = 287
            Height = 23
            Color = clWhite
            TabOrder = 33
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel82: TsPanel
            Left = 90
            Top = 390
            Width = 153
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #53685#51648#51008#54665' '#51204#51088#49436#47749
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 34
          end
          object edt_EXName1: TsEdit
            Left = 90
            Top = 414
            Width = 251
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 35
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_EXName2: TsEdit
            Left = 90
            Top = 438
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 36
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_EXName3: TsEdit
            Left = 90
            Top = 462
            Width = 92
            Height = 23
            Color = clWhite
            MaxLength = 10
            TabOrder = 37
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_EXAddr1: TsEdit
            Left = 90
            Top = 486
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 38
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_EXAddr2: TsEdit
            Left = 90
            Top = 510
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 39
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel83: TsPanel
            Tag = -1
            Left = 12
            Top = 390
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 40
          end
          object sPanel84: TsPanel
            Tag = -1
            Left = 12
            Top = 414
            Width = 77
            Height = 47
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #47749#51032#51064
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 41
          end
          object sPanel85: TsPanel
            Tag = -1
            Left = 12
            Top = 462
            Width = 77
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49885#48324#48512#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 42
          end
          object sPanel86: TsPanel
            Tag = -1
            Left = 12
            Top = 486
            Width = 77
            Height = 46
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51452#49548
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 43
          end
        end
      end
      object sTabSheet3: TsTabSheet
        Caption = #49345#49464#51221#48372
        object sPage2: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 539
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          BorderWidth = 4
          
          TabOrder = 0
          object sDBCtrlGrid1: TsDBCtrlGrid
            Left = 5
            Top = 5
            Width = 753
            Height = 529
            Align = alClient
            AllowDelete = False
            AllowInsert = False
            ColCount = 1
            Color = clWhite
            PanelHeight = 264
            PanelWidth = 736
            ParentColor = False
            TabOrder = 0
            RowCount = 2
            SkinData.CustomColor = True
            PanelSkin = 'GROUPBOX'
            object sPanel15: TsPanel
              Left = 0
              Top = 0
              Width = 736
              Height = 264
              SkinData.SkinSection = 'TRANSPARENT'
              Align = alClient
              
              TabOrder = 0
              object sDBEdit1: TsDBEdit
                Left = 84
                Top = 4
                Width = 47
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 0
              end
              object sPanel16: TsPanel
                Left = 4
                Top = 4
                Width = 79
                Height = 23
                SkinData.CustomColor = True
                SkinData.SkinSection = 'PANEL'
                Caption = #47928#49436#51068#47144#48264#54840
                Color = 16042877
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 1
              end
              object sPanel17: TsPanel
                Left = 148
                Top = 4
                Width = 95
                Height = 23
                SkinData.CustomColor = True
                SkinData.SkinSection = 'PANEL'
                Caption = #44144#47000#52280#51312#48264#54840
                Color = 16042877
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 2
              end
              object sDBEdit2: TsDBEdit
                Left = 244
                Top = 4
                Width = 190
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 3
              end
              object sPanel18: TsPanel
                Left = 436
                Top = 4
                Width = 95
                Height = 23
                SkinData.CustomColor = True
                SkinData.SkinSection = 'PANEL'
                Caption = #44288#47144#52280#51312#48264#54840
                Color = 16042877
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 4
              end
              object sDBEdit3: TsDBEdit
                Left = 532
                Top = 4
                Width = 197
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 5
              end
              object sPanel19: TsPanel
                Left = 436
                Top = 28
                Width = 95
                Height = 23
                SkinData.CustomColor = True
                SkinData.SkinSection = 'PANEL'
                Caption = #54869#50557#51333#47448
                Color = 16042877
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 6
              end
              object sDBEdit4: TsDBEdit
                Left = 532
                Top = 28
                Width = 85
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 7
              end
              object sPanel21: TsPanel
                Left = 436
                Top = 52
                Width = 95
                Height = 23
                SkinData.CustomColor = True
                SkinData.SkinSection = 'PANEL'
                Caption = #54869#50557#48264#54840
                Color = 16042877
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 8
              end
              object sDBEdit5: TsDBEdit
                Left = 532
                Top = 52
                Width = 197
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 9
              end
              object sPanel22: TsPanel
                Left = 436
                Top = 77
                Width = 111
                Height = 23
                SkinData.CustomColor = True
                SkinData.SkinSection = 'PANEL'
                Caption = #54869#50557#47928#49436' '#44060#49444#51008#54665
                Color = 16042877
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 10
              end
              object sPanel23: TsPanel
                Tag = -1
                Left = 548
                Top = 77
                Width = 77
                Height = 23
                SkinData.CustomColor = True
                SkinData.CustomFont = True
                SkinData.SkinSection = 'DRAGBAR'
                Caption = 'BIC'#53076#46300
                Color = clGray
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWhite
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 11
              end
              object sDBEdit6: TsDBEdit
                Left = 626
                Top = 77
                Width = 103
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 12
              end
              object sDBEdit7: TsDBEdit
                Left = 436
                Top = 101
                Width = 293
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 13
              end
              object sDBEdit8: TsDBEdit
                Left = 436
                Top = 125
                Width = 293
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 14
              end
              object sDBEdit9: TsDBEdit
                Left = 436
                Top = 149
                Width = 293
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 15
              end
              object sDBEdit10: TsDBEdit
                Left = 436
                Top = 173
                Width = 293
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 16
              end
              object sPanel25: TsPanel
                Left = 4
                Top = 28
                Width = 79
                Height = 23
                SkinData.CustomColor = True
                SkinData.SkinSection = 'PANEL'
                Caption = #47928#49436#44592#45733
                Color = 16042877
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 17
              end
              object sDBEdit11: TsDBEdit
                Left = 84
                Top = 28
                Width = 87
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 18
              end
              object sDBEdit12: TsDBEdit
                Left = 172
                Top = 28
                Width = 261
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 19
              end
              object sPanel26: TsPanel
                Left = 4
                Top = 52
                Width = 79
                Height = 47
                SkinData.CustomColor = True
                SkinData.SkinSection = 'PANEL'
                Caption = #54028#51068#51204#45804#50976#54805
                Color = 16042877
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 20
              end
              object sDBEdit13: TsDBEdit
                Left = 84
                Top = 52
                Width = 87
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 21
              end
              object sDBEdit14: TsDBEdit
                Left = 172
                Top = 52
                Width = 261
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 22
              end
              object sDBEdit15: TsDBEdit
                Left = 84
                Top = 76
                Width = 349
                Height = 23
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 23
              end
              object sPanel27: TsPanel
                Left = 4
                Top = 197
                Width = 79
                Height = 63
                SkinData.CustomColor = True
                SkinData.SkinSection = 'PANEL'
                Caption = #44592#53440#49324#54637
                Color = 16042877
                
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 24
              end
              object sDBMemo1: TsDBMemo
                Left = 84
                Top = 197
                Width = 645
                Height = 63
                Color = clWhite
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 25
              end
            end
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 763
          Height = 507
          TabStop = False
          Align = alClient
          Color = clGray
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CHK2'
              Title.Alignment = taCenter
              Title.Caption = #49345#54889
              Width = 36
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CHK3'
              Title.Alignment = taCenter
              Title.Caption = #52376#47532
              Width = 60
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'MSeq'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 32
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'AMD_NO'
              Title.Alignment = taCenter
              Title.Caption = #48320#44221#52264#49688
              Width = 58
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'APPLIC1'
              Title.Caption = #44060#49444'('#51032#47280')'#51008#54665
              Width = 246
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 32
          Align = alTop
          
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 0
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 1
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 432
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 152
    Top = 184
  end
end
