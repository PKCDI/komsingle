unit UI_DOMOFC_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sButton, sLabel, Buttons, sSpeedButton, ExtCtrls,
  sPanel, sSkinProvider, Grids, DBGrids, acDBGrid, sComboBox, sBitBtn, Mask,
  sMaskEdit, sEdit, ComCtrls, sPageControl, sMemo, DBCtrls, sDBMemo, sDBEdit,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, DB, ADODB, StrUtils, DateUtils,
  sCheckBox, Clipbrd, Menus, sDialogs;

type
  TUI_DOMOFC_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sSpeedButton9: TsSpeedButton;
    sBitBtn1: TsBitBtn;
    sPanel20: TsPanel;
    qryList: TADOQuery;
    dsList: TDataSource;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sSpeedButton10: TsSpeedButton;
    msk_pubdt: TsMaskEdit;
    edt_pubno: TsEdit;
    edt_pubcd: TsEdit;
    edt_tradeno: TsEdit;
    edt_pubnm: TsEdit;
    edt_pubceo: TsEdit;
    edt_pubsign: TsEdit;
    edt_pubaddr1: TsEdit;
    edt_pubaddr2: TsEdit;
    edt_pubaddr3: TsEdit;
    edt_demCd: TsEdit;
    edt_demSaupno: TsEdit;
    edt_demNm: TsEdit;
    edt_demCeo: TsEdit;
    edt_demSign: TsEdit;
    edt_demAddr1: TsEdit;
    edt_demAddr2: TsEdit;
    edt_demAddr3: TsEdit;
    edt_demRef: TsEdit;
    edt_HSCD: TsEdit;
    sPanel8: TsPanel;
    edt_eff1: TsEdit;
    edt_eff2: TsEdit;
    edt_eff3: TsEdit;
    edt_eff4: TsEdit;
    edt_eff5: TsEdit;
    edt_chk1: TsEdit;
    edt_chk2: TsEdit;
    edt_chk3: TsEdit;
    edt_chk4: TsEdit;
    edt_chk5: TsEdit;
    sPanel9: TsPanel;
    edt_pkg1: TsEdit;
    edt_pkg2: TsEdit;
    edt_pkg3: TsEdit;
    edt_pkg4: TsEdit;
    edt_pkg5: TsEdit;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    edt_pay1: TsEdit;
    edt_pay2: TsEdit;
    edt_pay3: TsEdit;
    edt_pay4: TsEdit;
    edt_pay5: TsEdit;
    sTabSheet3: TsTabSheet;
    sTabSheet2: TsTabSheet;
    sPanel17: TsPanel;
    sSpeedButton11: TsSpeedButton;
    sPanel21: TsPanel;
    sPanel22: TsPanel;
    sPanel23: TsPanel;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBEdit3: TsDBEdit;
    sBitBtn17: TsBitBtn;
    sDBEdit4: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sBitBtn18: TsBitBtn;
    sDBEdit5: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sBitBtn19: TsBitBtn;
    sDBEdit8: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sBitBtn20: TsBitBtn;
    sDBEdit9: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sBitBtn21: TsBitBtn;
    sDBEdit12: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sBitBtn23: TsBitBtn;
    sDBEdit13: TsDBEdit;
    sBitBtn26: TsButton;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    qryListMAINT_NO: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListChk1: TStringField;
    qryListChk2: TStringField;
    qryListChk3: TStringField;
    qryListMaint_Rff: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListD_CHK: TStringField;
    qryListSR_CODE: TStringField;
    qryListSR_NO: TStringField;
    qryListSR_NAME1: TStringField;
    qryListSR_NAME2: TStringField;
    qryListSR_NAME3: TStringField;
    qryListSR_ADDR1: TStringField;
    qryListSR_ADDR2: TStringField;
    qryListSR_ADDR3: TStringField;
    qryListUD_CODE: TStringField;
    qryListUD_NO: TStringField;
    qryListUD_NAME1: TStringField;
    qryListUD_NAME2: TStringField;
    qryListUD_NAME3: TStringField;
    qryListUD_ADDR1: TStringField;
    qryListUD_ADDR2: TStringField;
    qryListUD_ADDR3: TStringField;
    qryListUD_RENO: TStringField;
    qryListOFR_NO: TStringField;
    qryListOFR_DATE: TStringField;
    qryListOFR_SQ: TStringField;
    qryListOFR_SQ1: TStringField;
    qryListOFR_SQ2: TStringField;
    qryListOFR_SQ3: TStringField;
    qryListOFR_SQ4: TStringField;
    qryListOFR_SQ5: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK_1: TMemoField;
    qryListPRNO: TIntegerField;
    qryListHS_CODE: TStringField;
    qryListMAINT_NO_1: TStringField;
    qryListTSTINST: TStringField;
    qryListTSTINST1: TStringField;
    qryListTSTINST2: TStringField;
    qryListTSTINST3: TStringField;
    qryListTSTINST4: TStringField;
    qryListTSTINST5: TStringField;
    qryListPCKINST: TStringField;
    qryListPCKINST1: TStringField;
    qryListPCKINST2: TStringField;
    qryListPCKINST3: TStringField;
    qryListPCKINST4: TStringField;
    qryListPCKINST5: TStringField;
    qryListPAY: TStringField;
    qryListPAY_ETC: TStringField;
    qryListPAY_ETC1: TStringField;
    qryListPAY_ETC2: TStringField;
    qryListPAY_ETC3: TStringField;
    qryListPAY_ETC4: TStringField;
    qryListPAY_ETC5: TStringField;
    qryListMAINT_NO_2: TStringField;
    qryListTERM_DEL: TStringField;
    qryListTERM_NAT: TStringField;
    qryListTERM_LOC: TStringField;
    qryListORGN1N: TStringField;
    qryListORGN1LOC: TStringField;
    qryListORGN2N: TStringField;
    qryListORGN2LOC: TStringField;
    qryListORGN3N: TStringField;
    qryListORGN3LOC: TStringField;
    qryListORGN4N: TStringField;
    qryListORGN4LOC: TStringField;
    qryListORGN5N: TStringField;
    qryListORGN5LOC: TStringField;
    qryListTRNS_ID: TStringField;
    qryListLOADD: TStringField;
    qryListLOADLOC: TStringField;
    qryListLOADTXT: TStringField;
    qryListLOADTXT1: TStringField;
    qryListLOADTXT2: TStringField;
    qryListLOADTXT3: TStringField;
    qryListLOADTXT4: TStringField;
    qryListLOADTXT5: TStringField;
    qryListDEST: TStringField;
    qryListDESTLOC: TStringField;
    qryListDESTTXT: TStringField;
    qryListDESTTXT1: TStringField;
    qryListDESTTXT2: TStringField;
    qryListDESTTXT3: TStringField;
    qryListDESTTXT4: TStringField;
    qryListDESTTXT5: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYCUR: TStringField;
    qryListTAMT: TBCDField;
    qryListTAMTCUR: TStringField;
    qryListCODE: TStringField;
    qryListDOC_NAME: TStringField;
    qryListCODE_1: TStringField;
    qryListDOC_NAME_1: TStringField;
    qryListCODE_2: TStringField;
    qryListDOC_NAME_2: TStringField;
    qryListTRANSNME: TStringField;
    qryListLOADDNAME: TStringField;
    qryListDESTNAME: TStringField;
    sEdit1: TsEdit;
    sSpeedButton12: TsSpeedButton;
    qryDetail: TADOQuery;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_CHK: TStringField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME_CHK: TStringField;
    qryDetailNAME_COD: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTY_G: TStringField;
    qryDetailQTYG: TBCDField;
    qryDetailQTYG_G: TStringField;
    qryDetailPRICE: TBCDField;
    qryDetailPRICE_G: TStringField;
    qryDetailAMT: TBCDField;
    qryDetailAMT_G: TStringField;
    qryDetailSTQTY: TBCDField;
    qryDetailSTQTY_G: TStringField;
    qryDetailSTAMT: TBCDField;
    qryDetailSTAMT_G: TStringField;
    dsDetail: TDataSource;
    sPanel25: TsPanel;
    sDBGrid2: TsDBGrid;
    sPanel26: TsPanel;
    btnDnew: TsBitBtn;
    btnDEdit: TsBitBtn;
    btnDDel: TsBitBtn;
    sSpeedButton13: TsSpeedButton;
    btnDok: TsBitBtn;
    btnDcancel: TsBitBtn;
    edt_TQTYCUR: TsEdit;
    sBitBtn24: TsBitBtn;
    edt_TQTY: TsCurrencyEdit;
    edt_TAMT: TsCurrencyEdit;
    edt_TAMTCUR: TsEdit;
    sBitBtn25: TsBitBtn;
    sPanel27: TsPanel;
    edt_ori1: TsEdit;
    edt_ori1nm: TsEdit;
    sPanel12: TsPanel;
    btnOri1: TsBitBtn;
    edt_ori2: TsEdit;
    edt_ori2nm: TsEdit;
    btnOri2: TsBitBtn;
    edt_ori3: TsEdit;
    edt_ori3nm: TsEdit;
    btnOri3: TsBitBtn;
    edt_ori4: TsEdit;
    edt_ori4nm: TsEdit;
    btnOri4: TsBitBtn;
    edt_ori5: TsEdit;
    edt_ori5nm: TsEdit;
    btnOri5: TsBitBtn;
    sPanel13: TsPanel;
    mm_remark1: TsMemo;
    edt_Trans: TsEdit;
    sBitBtn14: TsBitBtn;
    edt_TransNm: TsEdit;
    edt_shipNat: TsEdit;
    sBitBtn13: TsBitBtn;
    edt_shipPort: TsEdit;
    edt_LOADTXT1: TsEdit;
    edt_LOADTXT2: TsEdit;
    edt_LOADTXT3: TsEdit;
    edt_LOADTXT4: TsEdit;
    edt_LOADTXT5: TsEdit;
    edt_destNat: TsEdit;
    sBitBtn16: TsBitBtn;
    edt_destPort: TsEdit;
    edt_DESTTXT1: TsEdit;
    edt_DESTTXT2: TsEdit;
    edt_DESTTXT3: TsEdit;
    edt_DESTTXT4: TsEdit;
    edt_DESTTXT5: TsEdit;
    sPanel14: TsPanel;
    sPanel15: TsPanel;
    sPanel18: TsPanel;
    sPanel16: TsPanel;
    sPanel19: TsPanel;
    sPanel28: TsPanel;
    sDBEdit15: TsDBEdit;
    sButton1: TsButton;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sCheckBox1: TsCheckBox;
    spCopyDOMOFC: TADOStoredProc;
    sPanel30: TsPanel;
    sLabel3: TsLabel;
    btnImport: TsBitBtn;
    btnSample: TsBitBtn;
    qryReady: TADOQuery;
    OpenDialog_Excel: TsOpenDialog;
    procedure qryListCHK2GetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edt_pubcdDblClick(Sender: TObject);
    procedure edt_ori1DblClick(Sender: TObject);
    procedure btnOri1Click(Sender: TObject);
    procedure edt_TransDblClick(Sender: TObject);
    procedure sDBEdit3DblClick(Sender: TObject);
    procedure sDBEdit5DblClick(Sender: TObject);
    procedure sDBEdit1DblClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnDnewClick(Sender: TObject);
    procedure qryDetailAfterInsert(DataSet: TDataSet);
    procedure sBitBtn26Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCopyClick(Sender: TObject);
    procedure sCheckBox1Click(Sender: TObject);
    procedure sDBGrid3TitleClick(Column: TColumn);
    procedure btnSampleClick(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure edt_SearchNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edt_TQTYCURDblClick(Sender: TObject);
    procedure edt_TAMTCURDblClick(Sender: TObject);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    FTEMPKEY: string;
    IS_COPY : Boolean;
    procedure ReadList(SortField : TColumn=nil);
    procedure ReadData;
    procedure ReadDetail(TempKey: string = '');
//    function CM_INDEX(var SearchBox: TsComboBox; sDelimiter: array of Char; CodeSize: Integer; value: string; nDefualtIndex: integer = 0): Integer;
    function MAX_SEQ: Integer;
    procedure CALC_TOTAL;
    function CHECK_VALIDITY: String;
    function DataProcess(nType : integer):string;
    procedure ReadyDocument(DocNo: String);
  protected
    procedure ButtonEnable; override;
    procedure ButtonEnableDetail;
  public
    { Public declarations }
  end;

var
  UI_DOMOFC_NEW_frm: TUI_DOMOFC_NEW_frm;

implementation

uses
  MSSQL, CodeDialogParent, CD_CUST, CD_NATION, CD_TRANS, CD_UNIT, CD_AMT_UNIT,
  ICON, CD_ITEMLIST, VarDefine, TypeDefine, Dlg_ErrorMessage, SQLCreator,
  Dialog_CopyDOMOFC, MessageDefine, AutoNo, NewExcelWriter, DOMOFC_PRINT, Preview,
  DocumentSend, Dlg_RecvSelect, CreateDocuments, NewExcelReader;

{$R *.dfm}

procedure TUI_DOMOFC_NEW_frm.qryListCHK2GetText(Sender: TField; var Text: string; DisplayText: Boolean);
var
  nIndex: integer;
begin
  inherited;
  nIndex := StrToIntDef(qryListCHK2.AsString, -1);
  case nIndex of
    -1:
      Text := '';
    0:
      Text := '임시';
    1:
      Text := '저장';
    2:
      Text := '결재';
    3:
      Text := '반려';
    4:
      Text := '접수';
    5:
      Text := '준비';
    6:
      Text := '취소';
    7:
      Text := '전송';
    8:
      Text := '오류';
    9:
      Text := '승인';
  end;
end;

procedure TUI_DOMOFC_NEW_frm.ReadList(SortField : TColumn);
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'H1.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;

    {$IFDEF DEBUG}
      Clipboard.AsText := qryList.SQL.Text;
    {$ENDIF}

    Open;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then
  ReadList;
end;

procedure TUI_DOMOFC_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  {$IFDEF DEBUG}
  sMaskEdit1.Text := '20100101';
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  {$ELSE}
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  {$ENDIF}
  sBitBtn1Click(nil);

  EnableControl(sPanel6, false);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);
  EnableControl(sPanel17, False);

  sPageControl1.ActivePageIndex := 0;

end;

procedure TUI_DOMOFC_NEW_frm.ReadData;
begin
  with qryList do
  begin
    if RecordCount > 0 then
    begin
      //------------------------------------------------------------------------------
      // 데이터 가져오기
      //------------------------------------------------------------------------------
      edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
      msk_Datee.Text := qryListDATEE.AsString;
      edt_userno.Text := qryListUSER_ID.AsString;

      CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 3);
      CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

      //발행일자
      msk_pubdt.Text := qryListOFR_DATE.AsString;
      //발행번호
      edt_pubno.Text := qryListOFR_NO.AsString;
      //발행자코드
      edt_pubcd.Text := qryListSR_CODE.AsString;
      edt_tradeno.Text := qryListSR_NO.AsString;
      edt_pubnm.Text := qryListSR_NAME1.AsString;
      edt_pubceo.Text := qryListSR_NAME2.AsString;
      edt_pubsign.Text := qryListSR_NAME3.AsString;
      edt_pubaddr1.Text := qryListSR_ADDR1.AsString;
      edt_pubaddr2.Text := qryListSR_ADDR2.AsString;
      edt_pubaddr3.Text := qryListSR_ADDR3.AsString;

      //수요자
      edt_demCd.Text := qryListUD_CODE.AsString;
      edt_demSaupno.Text := qryListUD_NO.AsString;
      edt_demNm.Text := qryListUD_NAME1.AsString;
      edt_demCeo.Text := qryListUD_NAME2.AsString;
      edt_demSign.Text := qryListUD_NAME3.AsString;
      edt_demAddr1.Text := qryListUD_ADDR1.AsString;
      edt_demAddr2.Text := qryListUD_ADDR2.AsString;
      edt_demAddr3.Text := qryListUD_ADDR3.AsString;
      edt_demRef.Text := qryListUD_RENO.AsString;
      edt_HSCD.Text := qryListHS_CODE.AsString;

      //유효기간
      edt_eff1.Text := qryListOFR_SQ1.AsString;
      edt_eff2.Text := qryListOFR_SQ2.AsString;
      edt_eff3.Text := qryListOFR_SQ3.AsString;
      edt_eff4.Text := qryListOFR_SQ4.AsString;
      edt_eff5.Text := qryListOFR_SQ5.AsString;

      //검사방법
      edt_chk1.Text := qryListTSTINST1.AsString;
      edt_chk2.Text := qryListTSTINST2.AsString;
      edt_chk3.Text := qryListTSTINST3.AsString;
      edt_chk4.Text := qryListTSTINST4.AsString;
      edt_chk5.Text := qryListTSTINST5.AsString;

      //포장방법
      edt_pkg1.Text := qryListPCKINST1.AsString;
      edt_pkg2.Text := qryListPCKINST2.AsString;
      edt_pkg3.Text := qryListPCKINST3.AsString;
      edt_pkg4.Text := qryListPCKINST4.AsString;
      edt_pkg5.Text := qryListPCKINST5.AsString;

      //결제방법
      edt_pay1.Text := qryListPAY_ETC1.AsString;
      edt_pay2.Text := qryListPAY_ETC2.AsString;
      edt_pay3.Text := qryListPAY_ETC3.AsString;
      edt_pay4.Text := qryListPAY_ETC4.AsString;
      edt_pay5.Text := qryListPAY_ETC5.AsString;

      //원산지국가/지방명
      edt_ori1.Text := qryListORGN1N.AsString;
      edt_ori2.Text := qryListORGN2N.AsString;
      edt_ori3.Text := qryListORGN3N.AsString;
      edt_ori4.Text := qryListORGN4N.AsString;
      edt_ori5.Text := qryListORGN5N.AsString;

      edt_ori1nm.Text := qryListORGN1LOC.AsString;
      edt_ori2nm.Text := qryListORGN2LOC.AsString;
      edt_ori3nm.Text := qryListORGN3LOC.AsString;
      edt_ori4nm.Text := qryListORGN4LOC.AsString;
      edt_ori5nm.Text := qryListORGN5LOC.AsString;

      //기타참조사항
      mm_remark1.Lines.Text := qryListREMARK_1.AsString;

      //운송방법
      edt_Trans.Text := qryListTRNS_ID.AsString;
      edt_TransNm.Text := qryListTRANSNME.AsString;

      //선적국가
      edt_shipNat.Text := qryListLOADD.AsString;
      edt_shipPort.Text := qryListLOADLOC.AsString;
      edt_LOADTXT1.Text := qryListLOADTXT1.AsString;
      edt_LOADTXT2.Text := qryListLOADTXT2.AsString;
      edt_LOADTXT3.Text := qryListLOADTXT3.AsString;
      edt_LOADTXT4.Text := qryListLOADTXT4.AsString;
      edt_LOADTXT5.Text := qryListLOADTXT5.AsString;

      //도착국가
      edt_destNat.Text := qryListDEST.AsString;
      edt_destPort.Text := qryListDESTLOC.AsString;
      edt_DESTTXT1.Text := qryListDESTTXT1.AsString;
      edt_DESTTXT2.Text := qryListDESTTXT2.AsString;
      edt_DESTTXT3.Text := qryListDESTTXT3.AsString;
      edt_DESTTXT4.Text := qryListDESTTXT4.AsString;
      edt_DESTTXT5.Text := qryListDESTTXT5.AsString;

      //총합계
      edt_TQTYCUR.Text := qryListTQTYCUR.AsString;
      edt_TQTY.Value := qryListTQTY.AsVariant;
      edt_TAMTCUR.Text := qryListTAMTCUR.AsString;
      edt_TAMT.Value := qryListTAMT.AsVariant;
    end;
  end;

end;

procedure TUI_DOMOFC_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
  ReadDetail;
end;

procedure TUI_DOMOFC_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then
    qryListAfterScroll(DataSet);
end;

{
function TUI_DOMOFC_NEW_frm.CM_INDEX(var SearchBox: TsComboBox; sDelimiter: array of Char; CodeSize: Integer; value: string; nDefualtIndex: integer = 0): Integer;
var
  TempStr: string;
  PIDX, i: integer;
begin
  Result := -1;

  if High(sDelimiter) = 0 then
  begin
    for i := 0 to SearchBox.Items.Count - 1 do
    begin
      TempStr := SearchBox.Items.Strings[i];
      PIDX := POS(sDelimiter[0], TempStr);
      if PIDX = 0 then
        Continue;
      if AnsiCompareText(value, LeftStr(TempStr, PIDX - 1)) = 0 then
      begin
        Result := i;
        SearchBox.ItemIndex := i;
        Break;
      end;
    end;
  end;

  if Result = -1 then
  begin
    SearchBox.ItemIndex := nDefualtIndex;
  end;

end;
}

procedure TUI_DOMOFC_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $0054CEFC;
      Canvas.Font.Color := clBlack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.ReadDetail;
begin
  with qryDetail do
  begin
    Close;
    if Trim(TempKey) = '' then
      Parameters[0].value := qryListMAINT_NO.AsString
    else
      Parameters[0].value := TempKey;
    Open;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.edt_pubcdDblClick(Sender: TObject);
begin
  inherited;
  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_CUST_frm.Values[0];

      case (Sender as TsEdit).Tag of
        101:
          begin
            edt_tradeno.Text := CD_CUST_frm.FieldValues('TRAD_NO');
            edt_pubnm.Text := CD_CUST_frm.FieldValues('ENAME');
            edt_pubceo.Text := CD_CUST_frm.FieldValues('REP_NAME');
            edt_pubsign.Text := CD_CUST_frm.FieldValues('Jenja');
            edt_pubaddr1.Text := CD_CUST_frm.FieldValues('ADDR1');
            edt_pubaddr2.Text := CD_CUST_frm.FieldValues('ADDR2');
            edt_pubaddr3.Text := CD_CUST_frm.FieldValues('ADDR3');
          end;

        102:
          begin
            edt_demSaupno.Text := CD_CUST_frm.FieldValues('SAUP_NO');
            edt_demnm.Text := CD_CUST_frm.FieldValues('ENAME');
            edt_demceo.Text := CD_CUST_frm.FieldValues('REP_NAME');
            edt_demsign.Text := CD_CUST_frm.FieldValues('Jenja');
            edt_demaddr1.Text := CD_CUST_frm.FieldValues('ADDR1');
            edt_demaddr2.Text := CD_CUST_frm.FieldValues('ADDR2');
            edt_demaddr3.Text := CD_CUST_frm.FieldValues('ADDR3');
          end;
      end;

    end;

  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.edt_ori1DblClick(Sender: TObject);
begin
  inherited;
  CD_NATION_frm := TCD_NATION_frm.Create(Self);
  try
    if CD_NATION_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_NATION_frm.Values[1];
    end;
  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.btnOri1Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    0:
      edt_ori1DblClick(edt_ori1);
    1:
      edt_ori1DblClick(edt_ori2);
    2:
      edt_ori1DblClick(edt_ori3);
    3:
      edt_ori1DblClick(edt_ori4);
    4:
      edt_ori1DblClick(edt_ori5);
    5:
      edt_TransDblClick(edt_Trans);
    6:
      edt_ori1DblClick(edt_shipNat);
    7:
      edt_ori1DblClick(edt_destNat);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.edt_TransDblClick(Sender: TObject);
begin
  inherited;
  CD_TRANS_frm := TCD_TRANS_frm.Create(Self);
  try
    if CD_TRANS_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_TRANS_frm.Values[1];
      edt_TransNm.Text := CD_TRANS_frm.Values[4];
    end;
  finally
    FreeAndNil(CD_TRANS_frm);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.sDBEdit3DblClick(Sender: TObject);
begin
  inherited;
  if (Sender as TsDBEdit).DataSource.DataSet.State <> dsBrowse then
  begin
    CD_UNIT_frm := TCD_UNIT_frm.Create(Self);
    try
      if CD_UNIT_frm.OpenDialog(0, (Sender as TsDBEdit).Text) then
      begin
        (Sender as TsDBEdit).DataSource.DataSet.FieldByName((Sender as TsDBEdit).Field.FieldName).AsVariant := CD_UNIT_frm.Values[0];
      end;
    finally
      FreeAndNil(CD_UNIT_frm);
    end;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.sDBEdit5DblClick(Sender: TObject);
begin
  inherited;
  if (Sender as TsDBEdit).DataSource.DataSet.State <> dsBrowse then
  begin
    CD_AMT_UNIT_frm := TCD_AMT_UNIT_frm.Create(Self);
    try
      if CD_AMT_UNIT_frm.OpenDialog(0, (Sender as TsDBEdit).Text) then
      begin
        (Sender as TsDBEdit).DataSource.DataSet.FieldByName((Sender as TsDBEdit).Field.FieldName).AsVariant := CD_AMT_UNIT_frm.Values[0];
      end;
    finally
      FreeAndNil(CD_AMT_UNIT_frm);
    end;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.sDBEdit1DblClick(Sender: TObject);
begin
  inherited;
  if (Sender as TsDBEdit).DataSource.DataSet.State <> dsBrowse then
  begin
    CD_ITEMLIST_frm := TCD_ITEMLIST_frm.Create(Self);
    try
      if CD_ITEMLIST_frm.OpenDialog(0, sDBEdit1.Text) then
      begin
        //선택된 코드 품명 규격 출력
        qryDetail.FieldByName('NAME_COD').AsString := CD_ITEMLIST_frm.FieldValues('CODE');
        //HSCODE
        qryDetail.FieldByName('HS_NO').AsString := CD_ITEMLIST_frm.FieldValues('HSCODE');
        //품명
        qryDetail.FieldByName('NAME1').AsString := CD_ITEMLIST_frm.FieldValues('FNAME');
        //규격
        qryDetail.FieldByName('SIZE1').AsString := CD_ITEMLIST_frm.FieldValues('MSIZE');
        //수량단위
        qryDetail.FieldByName('QTY_G').AsString := CD_ITEMLIST_frm.FieldValues('QTYC');
        qryDetail.FieldByName('STQTY_G').AsString := CD_ITEMLIST_frm.FieldValues('QTYC');
        //수량
        qryDetail.FieldByName('QTY').AsString := CD_ITEMLIST_frm.FieldValues('QTY_U');
        //단가단위
        qryDetail.FieldByName('PRICE_G').AsString := CD_ITEMLIST_frm.FieldValues('PRICEG');
        //단가
        qryDetail.FieldByName('PRICE').AsString := CD_ITEMLIST_frm.FieldValues('PRICE');
        //단가기준수량단위
        qryDetail.FieldByName('QTYG_G').AsString := CD_ITEMLIST_frm.FieldValues('QTY_UG');
        //금액단위
        qryDetail.FieldByName('AMT_G').AsString := CD_ITEMLIST_frm.FieldValues('PRICEG');
        qryDetail.FieldByName('STAMT_G').AsString := CD_ITEMLIST_frm.FieldValues('PRICEG');

      end;
    finally
      FreeAndNil(CD_ITEMLIST_frm);
    end;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  EnableControl(sPanel6);
  EnableControl(sPanel7);
  EnableControl(sPanel27);
//  EnableControl(sPanel17);

  case (Sender as TsButton).Tag of
    0:
      ProgramControlType := ctInsert;
    1:
      ProgramControlType := ctModify;
  end;

  if ProgramControlType = ctInsert then
  begin
    FTEMPKEY := FormatDateTime('YYYYMMDDHHNNSS', Now);
    ClearControl(sPanel6);
    ClearControl(sPanel7);
    ClearControl(sPanel27);
    sPanel29.Visible := True;
  end
  else
  begin
    IF IS_COPY Then
    begin
      edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('DOMOFC');

      edt_MAINT_NO.Enabled := Trim(edt_MAINT_NO.Text) = '';
    end
    else
      FTEMPKEY := qryListMAINT_NO.AsString;
  end;

  ButtonEnable;
  ButtonEnableDetail;

//------------------------------------------------------------------------------
// 헤더
//------------------------------------------------------------------------------
  msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text := LoginData.sID;
  com_func.ItemIndex := 3;
  com_type.ItemIndex := 0;

//------------------------------------------------------------------------------
// 임시 품목내역
//------------------------------------------------------------------------------
  if ProgramControlType = ctInsert then
  begin
    ReadDetail(FTEMPKEY);
//------------------------------------------------------------------------------
// 관리번호 보여주기(임시는 HIDDEN)
//------------------------------------------------------------------------------
    edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('DOMOFC');

    edt_MAINT_NO.Enabled := Trim(edt_MAINT_NO.Text) = '';
//------------------------------------------------------------------------------
// 발행일자
//------------------------------------------------------------------------------
    msk_pubdt.Text := FormatDateTime('YYYYMMDD', Now);

//------------------------------------------------------------------------------
// 코드가 00000 인경우 자기자신
//------------------------------------------------------------------------------
    CD_CUST_frm := TCD_CUST_frm.Create(Self);
    try
      if CD_CUST_frm.SearchData('00000') > 0 then
      begin
        edt_pubcd.Text := CD_CUST_frm.FieldValues('CODE');
        edt_tradeno.Text := CD_CUST_frm.FieldValues('TRAD_NO');
        edt_pubnm.Text := CD_CUST_frm.FieldValues('ENAME');
        edt_pubceo.Text := CD_CUST_frm.FieldValues('REP_NAME');
        edt_pubsign.Text := CD_CUST_frm.FieldValues('Jenja');
        edt_pubaddr1.Text := CD_CUST_frm.FieldValues('ADDR1');
        edt_pubaddr2.Text := CD_CUST_frm.FieldValues('ADDR2');
        edt_pubaddr3.Text := CD_CUST_frm.FieldValues('ADDR3');
      end;
    finally
      FreeAndNil(CD_CUST_frm);
    end;
  end
  else
    ReadDetail;
end;

procedure TUI_DOMOFC_NEW_frm.ButtonEnable;
begin
  inherited;
  btnNew.Enabled := ProgramControlType = ctView;
  btnEdit.Enabled := btnNew.Enabled;
  btnDel.Enabled := btnNew.Enabled;
  btnCopy.Enabled := btnNew.Enabled;
  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := not btnNew.Enabled;
  btnCancel.Enabled := not btnNew.Enabled;
  btnPrint.Enabled := btnNew.Enabled;
  btnReady.Enabled := btnNew.Enabled;
  btnSend.Enabled := btnNew.Enabled;

  sDBGrid1.Enabled := btnNew.Enabled;
  sDBGrid3.Enabled := btnNew.Enabled;
end;

procedure TUI_DOMOFC_NEW_frm.btnCancelClick(Sender: TObject);
var
  DOCNO : String;
begin
  inherited;
  if sDBGrid2.DataSource.DataSet.State in [dsInsert, dsEdit] then
  begin
    MessageBox(Self.Handle, '상품내역이 작성중입니다. 확인 후 다시 시도하세요', '취소', MB_OK + MB_ICONERROR);
    Exit;
  end;

//------------------------------------------------------------------------------
// 유효성 검사
//------------------------------------------------------------------------------
  if (Sender as TsButton).Tag = 1 then
  begin
    Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
    if Dlg_ErrorMessage_frm.Run_ErrorMessage( '국내발행물품매도확약서' ,CHECK_VALIDITY ) Then
    begin
      FreeAndNil(Dlg_ErrorMessage_frm);
      Exit;
    end;
  end;

  try
    DOCNO := DataProcess((Sender as TsButton).Tag);
  except
    on E:Exception do
    begin
      MessageBox(Self.Handle, PChar(E.Message), '데이터처리 오류', MB_OK+MB_ICONERROR);
      IF DMMssql.inTrans Then DMMssql.RollbackTrans;
    end;
  end;

  ProgramControlType := ctView;
  ButtonEnable;
  ButtonEnableDetail;

  EnableControl(sPanel6, false);
  EnableControl(sPanel7, false);
  EnableControl(sPanel27, false);
  EnableControl(sPanel17, false);

  IF DMMssql.inTrans Then
  begin
    Case (Sender as TsButton).Tag of
      0,1: DMMssql.CommitTrans;
      2: DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  IS_COPY := False;

  Readlist;
  qryList.Locate('MAINT_NO',DOCNO,[]);
end;

procedure TUI_DOMOFC_NEW_frm.btnDelClick(Sender: TObject);
var
  maint_no : String;
  nCursor : integer;
begin
  inherited;
  if sDBGrid1.DataSource.DataSet.RecordCount = 0 then
  begin
    MessageBox(Self.Handle, '데이터를 선택 후 삭제하세요', '삭제오류', MB_OK + MB_ICONERROR);
    Exit;
  end;

//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := qryListMAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM DOMOFC_D WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFC_H1 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFC_H2 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFC_H3 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) Then
        begin
          If qryList.RecordCount < nCursor Then
            qryList.Last
          else
            qryList.MoveBy(nCursor-1);
        end
        else if qryList.RecordCount = 0 then
        begin
          ClearControl(sPanel6);
          ClearControl(sPanel7);
          ClearControl(sPanel27);
          ClearControl(sPanel17);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;


end;

procedure TUI_DOMOFC_NEW_frm.ButtonEnableDetail;
begin
  btnDnew.Enabled := (not btnNew.Enabled) and (sDBGrid2.DataSource.DataSet.State = dsBrowse);
  btnDEdit.Enabled := btnDnew.Enabled;
  btnDDel.Enabled := btnDnew.Enabled;
  btnImport.Enabled := btnDnew.Enabled;
  btnSample.Enabled := btnDnew.Enabled;

  btnDok.Enabled := (not btnDnew.Enabled) and (sDBGrid2.DataSource.DataSet.State <> dsBrowse);
  btnDcancel.Enabled := (not btnDnew.Enabled) and (sDBGrid2.DataSource.DataSet.State <> dsBrowse);

  sDBGrid2.Enabled := sDBGrid2.DataSource.DataSet.State = dsBrowse;
  EnableControl(sPanel17, not sDBGrid2.Enabled);
end;

procedure TUI_DOMOFC_NEW_frm.btnDnewClick(Sender: TObject);
begin
  inherited;
  IF ((Sender as TsBitBtn).Tag in [0,1,2]) AND (not DMMssql.inTrans) then
  begin
    DMMssql.BeginTrans;
  end;

  case (Sender as TsBitBtn).Tag of
    0:
      sDBGrid2.DataSource.DataSet.Append;
    1:
      sDBGrid2.DataSource.DataSet.Edit;
    2:
      sDBGrid2.DataSource.DataSet.Delete;
    3:
      sDBGrid2.DataSource.DataSet.Post;
    4:
      sDBGrid2.DataSource.DataSet.Cancel;
  end;
  ButtonEnableDetail;
end;

procedure TUI_DOMOFC_NEW_frm.qryDetailAfterInsert(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('KEYY').AsString := FTEMPKEY;
  DataSet.FieldByName('SEQ').asinteger := MAX_SEQ;
end;

function TUI_DOMOFC_NEW_frm.MAX_SEQ: Integer;
begin
  Result := -1;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT ISNULL(MAX(SEQ),0)+1 as MAX_SEQ FROM DOMOFC_D'#13 + 'WHERE KEYY = ' + QuotedStr(FTEMPKEY);
      Open;
      Result := FieldByName('MAX_SEQ').AsInteger;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.sBitBtn26Click(Sender: TObject);
begin
  inherited;
  if qryDetail.State in [dsInsert, dsEdit] then
  begin
    //수량단위 비교
    if UpperCase(qryDetailQTY_G.AsString) <> UpperCase(qryDetailSTQTY_G.AsString) then
      qryDetailSTQTY_G.AsString := qryDetailQTY_G.AsString;

    //금액통화 비교
    if UpperCase(qryDetailPRICE_G.AsString) <> UpperCase(qryDetailAMT_G.AsString) then
    begin
      qryDetailAMT_G.AsString := qryDetailPRICE_G.AsString;
      qryDetailSTAMT_G.AsString := qryDetailPRICE_G.AsString;
    end;

    //수량계산
    qryDetailSTQTY.AsVariant := qryDetailQTY.AsVariant;

    //금액계산
    qrydetailAMT.AsVariant := qryDetailQTY.AsVariant * qryDetailPRICE.AsVariant;
    qryDetailSTAMT.AsVariant := qryDetailQTY.AsVariant * qryDetailPRICE.AsVariant;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.CALC_TOTAL;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT SUM(STQTY) as SUM_STQTY, MAX(STQTY_G) as STQTY_G, SUM(STAMT) as SUM_STAMT, MAX(STAMT_G) as STAMT_G FROM DOMOFC_D'#13 + 'WHERE KEYY = ' + QuotedStr(FTEMPKEY) + #13 + 'GROUP BY KEYY';
      Open;

      if RecordCount = 0 then
      begin
        edt_TQTYCUR.Clear;
        edt_TAMTCUR.Clear;
        edt_TQTY.Value := 0;
        edt_TAMT.Value := 0;
      end
      else
      begin
        edt_TQTYCUR.Text := FieldByName('STQTY_G').AsString;
        edt_TAMTCUR.Text := FieldByName('STAMT_G').AsString;
        edt_TQTY.Value := FieldByName('SUM_STQTY').AsVariant;
        edt_TAMT.Value := FieldByName('SUM_STAMT').AsVariant;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  CALC_TOTAL;
end;

procedure TUI_DOMOFC_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  IF DMMssql.inTrans Then DMMssql.RollbackTrans;

  Action := caFree;
  UI_DOMOFC_NEW_frm := nil;
end;

function TUI_DOMOFC_NEW_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  nYear,nMonth,nDay : Word;
begin
  ErrMsg := TStringList.Create;

  Try
    IF Trim(edt_MAINT_NO.Text) = '' Then
      ErrMsg.Add('[공통] 관리번호를 입력하세요');

    IF Trim(edt_pubno.Text) = '' THEN
      ErrMsg.Add('[Page1] 발행번호를 입력해야합니다.');

    IF Trim(msk_pubdt.Text) = '' THEN
      ErrMsg.Add('[Page1] 발행일자를 입력해야합니다.');

    IF Trim(edt_tradeno.Text) = '' THEN
      ErrMsg.Add('[Page1] 무역대리점등록번호를 입력해야합니다.');

    IF Trim(edt_pubnm.Text) = '' THEN
      ErrMsg.Add('[Page1] 발행자 상호를 입력해야합니다.');

    IF Trim(edt_demSaupno.Text) = '' THEN
      ErrMsg.Add('[Page1]  수요자 사업자번호를 입력해야합니다.');

    IF Trim(edt_demNm.Text) = '' THEN
      ErrMsg.Add('[Page1] 수요자 상호를 입력해야합니다.');

    IF Trim(edt_TQTYCUR.Text) = '' THEN
      ErrMsg.Add('[Page5] 총수량단위를 입력해야합니다.');

    IF Trim(edt_TQTY.Text) = '' THEN
      ErrMsg.Add('[Page5] 총수량을 입력해야합니다.');

    IF Trim(edt_TAMTCUR.Text) = '' THEN
      ErrMsg.Add('[Page5] 총합계 단위를 입력해야합니다.');

    IF Trim(edt_TAMT.Text) = '' THEN
      ErrMsg.Add('[Page5] 총합계를 입력해야합니다.');

    if qryDetail.RecordCount = 0 then
      ErrMsg.Add('[Page5] 제품을 등록해야 합니다.');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

function TUI_DOMOFC_NEW_frm.DataProcess(nType : integer):string;
var
  MAINT_NO : string;

  SQLCreate : TSQLCreate;
begin
  IF nType = 2 Then Exit;
  //------------------------------------------------------------------------------
  // SQL생성기
  //------------------------------------------------------------------------------
  SQLCreate := TSQLCreate.Create;
  try
    MAINT_NO := edt_MAINT_NO.Text;
    Result := MAINT_NO;
      with SQLCreate do
      begin
      //----------------------------------------------------------------------
      // 문서헤더 및 DOMOFC_H1.DB
      //----------------------------------------------------------------------
        Case ProgramControlType of
          ctInsert :
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO',MAINT_NO);
          end;

          ctModify :
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO',MAINT_NO);
          end;
        end;

        SQLHeader('DOMOFC_H1');

        //문서저장구분(0:임시, 1:저장)
        ADDValue('CHK2',nType);
        //등록일자
        ADDValue('DATEE',msk_Datee.Text);
        //유저ID
        ADDValue('USER_ID',edt_userno.Text);
        //메세지1
        ADDValue('MESSAGE1',CM_TEXT(com_func,[':']));
        //메세지2
        ADDValue('MESSAGE2',CM_TEXT(com_type,[':']));
        //발행일자
        ADDValue('OFR_DATE',msk_pubdt.Text);
        //발행번호
        ADDValue('OFR_NO',edt_pubno.Text);
        //발행자
        ADDValue('SR_CODE',edt_pubcd.Text);
        //무역대리점 등록번호
        ADDValue('SR_NO',edt_tradeno.Text);
        //상호
        ADDValue('SR_NAME1',edt_pubnm.Text);
        ADDValue('SR_NAME2',edt_pubceo.Text);
        //전자서명
        ADDValue('SR_NAME3',edt_pubsign.Text);
        //주소
        ADDValue('SR_ADDR1',edt_pubaddr1.Text);
        ADDValue('SR_ADDR2',edt_pubaddr2.Text);
        ADDValue('SR_ADDR3',edt_pubaddr3.Text);
        //수혜자
        ADDValue('UD_CODE',edt_demCd.Text);
        //사업자등록번호
        ADDValue('UD_NO',edt_demSaupno.Text);
        //상호
        ADDValue('UD_NAME1',edt_demNm.Text);
        ADDValue('UD_NAME2',edt_demCeo.Text);
        //전자서명
        ADDValue('UD_NAME3',edt_demSign.Text);
        //주소
        ADDValue('UD_ADDR1',edt_demAddr1.Text);
        ADDValue('UD_ADDR2',edt_demAddr2.Text);
        ADDValue('UD_ADDR3',edt_demAddr3.Text);
        //수요자참조번호
        ADDValue('UD_RENO',edt_demRef.Text);
        //대표공급물품 HS코드
        ADDValue('HS_CODE',edt_HSCD.Text);
        //유호기간
        ADDValue('OFR_SQ1',edt_eff1.Text);
        ADDValue('OFR_SQ2',edt_eff2.Text);
        ADDValue('OFR_SQ3',edt_eff3.Text);
        ADDValue('OFR_SQ4',edt_eff4.Text);
        ADDValue('OFR_SQ5',edt_eff5.Text);
        //기타참조사항
        ADDValue('REMARK_1',mm_remark1.Text);

      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate.CreateSQL;
          if sCheckBox1.Checked then
            Clipboard.AsText := SQL.Text;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;

  //--------------------------------------------------------------------------
  // DOMOFC_H2
  //--------------------------------------------------------------------------
    with SQLCreate do
    begin

      SQLHeader('DOMOFC_H2');

      Case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO', MAINT_NO);
        end;

        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO', MAINT_NO);
        end;
      end;

      //검사방법
      ADDValue('TSTINST1',edt_chk1.Text);
      ADDValue('TSTINST2',edt_chk2.Text);
      ADDValue('TSTINST3',edt_chk3.Text);
      ADDValue('TSTINST4',edt_chk4.Text);
      ADDValue('TSTINST5',edt_chk5.Text);
      //포장방법
      ADDValue('PCKINST1',edt_pkg1.Text);
      ADDValue('PCKINST2',edt_pkg2.Text);
      ADDValue('PCKINST3',edt_pkg3.Text);
      ADDValue('PCKINST4',edt_pkg4.Text);
      ADDValue('PCKINST5',edt_pkg5.Text);
      //결제조건
      ADDValue('PAY_ETC1',edt_pay1.Text);
      ADDValue('PAY_ETC2',edt_pay2.Text);
      ADDValue('PAY_ETC3',edt_pay3.Text);
      ADDValue('PAY_ETC4',edt_pay4.Text);
      ADDValue('PAY_ETC5',edt_pay5.Text);

    end;

    with TADOQuery.Create(nil) do
     begin
       try
         Connection := DMMssql.KISConnect;
         SQL.Text := SQLCreate.CreateSQL;
         if sCheckBox1.Checked then
           Clipboard.AsText := SQL.Text;
         ExecSQL;
       finally
         Close;
         Free;
       end;
     end;

  //--------------------------------------------------------------------------
  // DOMOFC_H3
  //--------------------------------------------------------------------------
    with SQLCreate do
    begin
      SQLHeader('DOMOFC_H3');

      Case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO',MAINT_NO);
        end;

        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO',MAINT_NO);
        end;
      end;

      //원산지국가 , 지방명
      ADDValue('ORGN1N',edt_ori1.Text);
      ADDValue('ORGN1LOC',edt_ori1nm.Text);
      ADDValue('ORGN2N',edt_ori2.Text);
      ADDValue('ORGN2LOC',edt_ori2nm.Text);
      ADDValue('ORGN3N',edt_ori3.Text);
      ADDValue('ORGN3LOC',edt_ori3nm.Text);
      ADDValue('ORGN4N',edt_ori4.Text);
      ADDValue('ORGN4LOC',edt_ori4nm.Text);
      ADDValue('ORGN5N',edt_ori5.Text);
      ADDValue('ORGN5LOC',edt_ori5nm.Text);

      //운송방법
      ADDValue('TRNS_ID',edt_Trans.Text);
      //선적국가
      ADDValue('LOADD',edt_shipNat.Text);
      //선적항
      ADDValue('LOADLOC',edt_shipPort.Text);
      //선적시기
      ADDValue('LOADTXT1',edt_LOADTXT1.Text);
      ADDValue('LOADTXT2',edt_LOADTXT2.Text);
      ADDValue('LOADTXT3',edt_LOADTXT3.Text);
      ADDValue('LOADTXT4',edt_LOADTXT4.Text);
      ADDValue('LOADTXT5',edt_LOADTXT5.Text);
      //도착국가
      ADDValue('DEST',edt_destNat.Text);
      //도착항
      ADDValue('DESTLOC',edt_destPort.Text);
      //도착시기
      ADDValue('DESTTXT1',edt_DESTTXT1.Text);
      ADDValue('DESTTXT2',edt_DESTTXT2.Text);
      ADDValue('DESTTXT3',edt_DESTTXT3.Text);
      ADDValue('DESTTXT4',edt_DESTTXT4.Text);
      ADDValue('DESTTXT5',edt_DESTTXT5.Text);
      //총수량단위
      ADDValue('TQTYCUR',edt_TQTYCUR.Text);
      //총수량
      ADDValue('TQTY',edt_TQTY.Text);
      //총금액단위
      ADDValue('TAMTCUR',edt_TAMTCUR.Text);
      //총금액
      ADDValue('TAMT',edt_TAMT.Text);
    end;

    with TADOQuery.Create(nil) do
     begin
       try
         Connection := DMMssql.KISConnect;
         SQL.Text := SQLCreate.CreateSQL;
         if sCheckBox1.Checked then
           Clipboard.AsText := SQL.Text;
         ExecSQL;

//------------------------------------------------------------------------------
// 관리번호 업데이트
//------------------------------------------------------------------------------
          Close;
          SQL.Text := 'UPDATE DOMOFC_D SET KEYY = '+QuotedStr(MAINT_NO)+' WHERE KEYY = '+QuotedStr(FTEMPKEY);
          ExecSQL;
//------------------------------------------------------------------------------
// 복사시에는 MAINT_NO UPDATE
//------------------------------------------------------------------------------
          IF IS_COPY Then
          begin
            Close;
            SQL.Text := 'UPDATE DOMOFC_H1 SET MAINT_NO = '+QuotedStr(MAINT_NO)+' WHERE MAINT_NO = '+QuotedStr(FTEMPKEY);
            ExecSQL;
            SQL.Text := 'UPDATE DOMOFC_H2 SET MAINT_NO = '+QuotedStr(MAINT_NO)+' WHERE MAINT_NO = '+QuotedStr(FTEMPKEY);
            ExecSQL;
            SQL.Text := 'UPDATE DOMOFC_H3 SET MAINT_NO = '+QuotedStr(MAINT_NO)+' WHERE MAINT_NO = '+QuotedStr(FTEMPKEY);
            ExecSQL;
          end;
       finally
         Close;
         Free;
       end;
     end;
  finally
    SQLCreate.Free;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.btnCopyClick(Sender: TObject);
var
  copyDocNo : String;
  TempVal1, TempVal2 : String;
begin
  inherited;
//  if qryList.RecordCount = 0 then Exit;

  Dialog_CopyDOMOFC_frm := TDialog_CopyDOMOFC_frm.Create(Self);
  try
    FTEMPKEY := FormatDateTime('YYYYMMMDDHHNNSS',Now);
    copyDocNo := Dialog_CopyDOMOFC_frm.openDialog;

    IF Trim(copyDocNo) = '' then Exit;

    IS_COPY := True;
    //------------------------------------------------------------------------------
    // COPY
    //------------------------------------------------------------------------------
    with spCopyDOMOFC do
    begin
      Close;
      Parameters.ParamByName('@CopyDocNo').Value := copyDocNo;
      Parameters.ParamByName('@NewDocNo').Value :=  FTEMPKEY;
      Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

      if not DMMssql.inTrans then
        DMMssql.BeginTrans;

      try
        ExecProc;
      except
        on e:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
        end;
      end;
    end;

//------------------------------------------------------------------------------
// 복사된 데이터 찾기
//------------------------------------------------------------------------------
    sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
    sMaskEdit2.Text := sMaskEdit1.Text;
    sMaskEdit3.Text := sMaskEdit1.Text;
    sMaskEdit4.Text := sMaskEdit1.Text;
      
    edt_SearchNo.Text := FTEMPKEY;

    ReadList;

    edt_SearchNo.Clear;

    btnNewClick(btnEdit);
  finally
    FreeAndNil(Dialog_CopyDOMOFC_frm);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.sCheckBox1Click(Sender: TObject);
begin
  inherited;
  sLabel3.Caption := FTEMPKEY;
  sPanel30.Visible := sCheckBox1.Checked;
end;

procedure TUI_DOMOFC_NEW_frm.sDBGrid3TitleClick(Column: TColumn);
begin
  inherited;
  ReadList(Column);
end;

procedure TUI_DOMOFC_NEW_frm.btnSampleClick(Sender: TObject);
var
  EW: TExcelWriter;
begin
  inherited;
  EW := TExcelWriter.Create;
  try
    EW.CreateData(16);
    EW.RowData[1]  := '품명코드';
    EW.RowData[2]  := '품명';
    EW.RowData[3]  := '규격';
    EW.RowData[4]  := 'HS코드';
    EW.RowData[5]  := '수량';
    EW.RowData[6]  := '수량단위';
    EW.RowData[7]  := '단가';
    EW.RowData[8]  := '단가통화';
    EW.RowData[9]  := '단가기준수량';
    EW.RowData[10] := '단위';
    EW.RowData[11] := '금액';
    EW.RowData[12] := '금액통화';
    EW.RowData[13] := '수량소계';
    EW.RowData[14] := '소계단위';
    EW.RowData[15] := '금액소계';
    EW.RowData[16] := '소계단위';
    EW.AddData(1);

    EW.RowData[1]  := '';
    EW.RowData[2]  := 'PRODUCT NAME(Mandatory)';
    EW.RowData[3]  := '145*250'#13#10'DO NOT OPEND GOODS(Optional)';
    EW.RowData[4]  := '2710.19-7130';
    EW.RowData[5]  := '1000';
    EW.RowData[6]  := 'EA';
    EW.RowData[7]  := '50';
    EW.RowData[8]  := 'USD';
    EW.RowData[9]  := '';
    EW.RowData[10] := '';
    EW.RowData[11] := '50000';
    EW.RowData[12] := 'USD';
    EW.RowData[13] := '1000';
    EW.RowData[14] := 'EA';
    EW.RowData[15] := '50000';
    EW.RowData[16] := 'USD';
    EW.AddData(2);

    Ew.RowHeight(1, 18);
    EW.RowHeight(2, 24);

    EW.CellAlign('A1:P1', tacenter);

    EW.ColumnWidth('B', 33);
    EW.ColumnWidth('C', 25);
    EW.ColumnWidth('D', 16);

    EW.CellColor('A1:P1',RGB(217,217,217));
    EW.CellColor(2,2,RGB(255,255,0));
    EW.CellColor('D2:H2',RGB(255,255,0));
    EW.CellColor('K2:P2',RGB(255,255,0));

    EW.CellBorder('A1:P2',[loAll],2);

    Ew.ShowExcel;
  finally
    Ew.Free;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.btnImportClick(Sender: TObject);
var
  EW : TExcelReader;
  SC : TSQLCreate;
  i, RIDX : Integer;
  TempStr : String;
begin
  inherited;
  IF qryDetail.RecordCount > 0 then
  begin
    IF not MessageBox(Self.Handle,'이미 상품내역이 입력되어있습니다. 가져오기를 선택하면 기존에 입력된 상품목록은 삭제됩니다. 계속하시겠습니까?','가져오기 확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;
  end;

  IF not OpenDialog_Excel.Execute Then Exit;

//------------------------------------------------------------------------------
// 상품내역 삭제
//------------------------------------------------------------------------------
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'DELETE FROM DOMOFC_D WHERE KEYY =' + QuotedStr(FTEMPKEY);
      ExecSQL;

      SC := TSQLCreate.Create;
      Ew := TExcelReader.Create(OpenDialog_Excel.Files.Strings[0]);
      try
        RIDX := 2;
        for i := RIDX to EW.MaxRow('B', '') do
        begin
          SC.DMLType := dmlInsert;
          SC.SQLHeader('DOMOFC_D');
          SC.ADDValue('KEYY',     FTEMPKEY);
          SC.ADDValue('SEQ',      RIDX-1);
          SC.ADDValue('NAME_COD', EW.asString(RIDX, 1) );
          SC.ADDValue('NAME1',    EW.asString(RIDX, 2));
          SC.ADDValue('SIZE1',    EW.asString(RIDX, 3));
          TempStr := AnsiReplaceText( AnsiReplaceText( EW.Excelvalue(RIDX, 4) , '-', '') , '.', '');
          SC.ADDValue('HS_NO',    TempStr);
          SC.ADDValue('QTY',      EW.Excelvalue(RIDX, 5), vtVariant);
          SC.ADDValue('QTY_G',    EW.asString(RIDX, 6));
          SC.ADDValue('PRICE',    EW.Excelvalue(RIDX, 7), vtVariant );
          SC.ADDValue('PRICE_G',  EW.asString(RIDX, 8));
          SC.ADDValue('QTYG',     EW.Excelvalue(RIDX, 9), vtVariant );
          SC.ADDValue('QTYG_G',   EW.asString(RIDX, 10));
          SC.ADDValue('AMT',      EW.Excelvalue(RIDX, 11), vtVariant );
          SC.ADDValue('AMT_G',    EW.asString(RIDX, 12));
          SC.ADDValue('STQTY',    EW.Excelvalue(RIDX, 13), vtVariant );
          SC.ADDValue('STQTY_G',  EW.asString(RIDX, 14));
          SC.ADDValue('STAMT',    EW.Excelvalue(RIDX, 15), vtVariant );
          SC.ADDValue('STAMT_G',  EW.asString(RIDX, 16));

          Close;
          SQL.Text := SC.CreateSQL;
          ExecSQL;

          Inc(RIDX);
        end;
      finally
        SC.Free;
        EW.Free;
      end;
    finally
      Close;
      Free;

      sDBGrid2.DataSource.DataSet.Close;
      sDBGrid2.DataSource.DataSet.Open;      
    end;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  Preview_frm := TPreview_frm.Create(Self);
  DOMOFC_PRINT_frm := TDOMOFC_PRINT_frm.Create(Self);
  try
    DOMOFC_PRINT_frm.FKEY := qryListMAINT_NO.AsString;
    DOMOFC_PRINT_frm.PrintDocument(qryList.Fields,qryDetail.Fields ,True);
    Preview_frm.Report := DOMOFC_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(DOMOFC_PRINT_frm);
    FreeAndNil(Preview_frm);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := DOMOFC(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'DOMOFC';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'DOMOFC_H1';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        ReadList;
//        Readlist(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;

end;

procedure TUI_DOMOFC_NEW_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_DOMOFC_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_DOMOFC_NEW_frm.edt_SearchNoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF KEY = VK_RETURN Then sBitBtn1Click(nil);
end;

procedure TUI_DOMOFC_NEW_frm.edt_TQTYCURDblClick(Sender: TObject);
begin
  inherited;
  CD_UNIT_frm := TCD_UNIT_frm.Create(Self);
  try
    if CD_UNIT_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_UNIT_frm.Values[1];
    end;
  finally
    FreeAndNil(CD_UNIT_frm);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.edt_TAMTCURDblClick(Sender: TObject);
begin
  inherited;
  CD_AMT_UNIT_frm := TCD_AMT_UNIT_frm.Create(Self);
  try
    if CD_AMT_UNIT_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_AMT_UNIT_frm.Values[0];
    end;
  finally
    FreeAndNil(CD_AMT_UNIT_frm);
  end;
end;

procedure TUI_DOMOFC_NEW_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $0054CEFC;
      Canvas.Font.Color := clBlack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

end.

