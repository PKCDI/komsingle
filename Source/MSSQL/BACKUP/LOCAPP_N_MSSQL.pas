unit LOCAPP_N_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LOCADV_N_MSSQL, DB, ADODB, sSkinProvider, Buttons, sSpeedButton,
  StdCtrls, DBCtrls, sDBMemo, sLabel, sDBEdit, ExtCtrls, sSplitter,
  sComboBox, sButton, sEdit, Mask, sMaskEdit, sPanel, Grids, DBGrids,
  acDBGrid, ComCtrls, sPageControl, TypeDefine;

type
  TLOCAPP_N_MSSQL_frm = class(TLOCADV_N_MSSQL_frm)
    qryLOCAPP: TADOQuery;
    dsLOCAPP: TDataSource;
    qryLOCAPPMAINT_NO: TStringField;
    qryLOCAPPDATEE: TStringField;
    qryLOCAPPUSER_ID: TStringField;
    qryLOCAPPMESSAGE1: TStringField;
    qryLOCAPPMESSAGE2: TStringField;
    qryLOCAPPBUSINESS: TStringField;
    qryLOCAPPOFFERNO1: TStringField;
    qryLOCAPPOFFERNO2: TStringField;
    qryLOCAPPOFFERNO3: TStringField;
    qryLOCAPPOFFERNO4: TStringField;
    qryLOCAPPOFFERNO5: TStringField;
    qryLOCAPPOFFERNO6: TStringField;
    qryLOCAPPOFFERNO7: TStringField;
    qryLOCAPPOFFERNO8: TStringField;
    qryLOCAPPOFFERNO9: TStringField;
    qryLOCAPPOPEN_NO: TBCDField;
    qryLOCAPPAPP_DATE: TStringField;
    qryLOCAPPDOC_PRD: TBCDField;
    qryLOCAPPDELIVERY: TStringField;
    qryLOCAPPEXPIRY: TStringField;
    qryLOCAPPTRANSPRT: TStringField;
    qryLOCAPPGOODDES: TStringField;
    qryLOCAPPGOODDES1: TMemoField;
    qryLOCAPPREMARK: TStringField;
    qryLOCAPPREMARK1: TMemoField;
    qryLOCAPPAP_BANK: TStringField;
    qryLOCAPPAP_BANK1: TStringField;
    qryLOCAPPAP_BANK2: TStringField;
    qryLOCAPPAPPLIC: TStringField;
    qryLOCAPPAPPLIC1: TStringField;
    qryLOCAPPAPPLIC2: TStringField;
    qryLOCAPPAPPLIC3: TStringField;
    qryLOCAPPBENEFC: TStringField;
    qryLOCAPPBENEFC1: TStringField;
    qryLOCAPPBENEFC2: TStringField;
    qryLOCAPPBENEFC3: TStringField;
    qryLOCAPPEXNAME1: TStringField;
    qryLOCAPPEXNAME2: TStringField;
    qryLOCAPPEXNAME3: TStringField;
    qryLOCAPPDOCCOPY1: TBCDField;
    qryLOCAPPDOCCOPY2: TBCDField;
    qryLOCAPPDOCCOPY3: TBCDField;
    qryLOCAPPDOCCOPY4: TBCDField;
    qryLOCAPPDOCCOPY5: TBCDField;
    qryLOCAPPDOC_ETC: TStringField;
    qryLOCAPPDOC_ETC1: TMemoField;
    qryLOCAPPLOC_TYPE: TStringField;
    qryLOCAPPLOC_AMT: TBCDField;
    qryLOCAPPLOC_AMTC: TStringField;
    qryLOCAPPDOC_DTL: TStringField;
    qryLOCAPPDOC_NO: TStringField;
    qryLOCAPPDOC_AMT: TBCDField;
    qryLOCAPPDOC_AMTC: TStringField;
    qryLOCAPPLOADDATE: TStringField;
    qryLOCAPPEXPDATE: TStringField;
    qryLOCAPPIM_NAME: TStringField;
    qryLOCAPPIM_NAME1: TStringField;
    qryLOCAPPIM_NAME2: TStringField;
    qryLOCAPPIM_NAME3: TStringField;
    qryLOCAPPDEST: TStringField;
    qryLOCAPPISBANK1: TStringField;
    qryLOCAPPISBANK2: TStringField;
    qryLOCAPPPAYMENT: TStringField;
    qryLOCAPPEXGOOD: TStringField;
    qryLOCAPPEXGOOD1: TMemoField;
    qryLOCAPPCHKNAME1: TStringField;
    qryLOCAPPCHKNAME2: TStringField;
    qryLOCAPPCHK1: TStringField;
    qryLOCAPPCHK2: TStringField;
    qryLOCAPPCHK3: TStringField;
    qryLOCAPPPRNO: TIntegerField;
    qryLOCAPPLCNO: TStringField;
    qryLOCAPPBSN_HSCODE: TStringField;
    qryLOCAPPAPPADDR1: TStringField;
    qryLOCAPPAPPADDR2: TStringField;
    qryLOCAPPAPPADDR3: TStringField;
    qryLOCAPPBNFADDR1: TStringField;
    qryLOCAPPBNFADDR2: TStringField;
    qryLOCAPPBNFADDR3: TStringField;
    qryLOCAPPBNFEMAILID: TStringField;
    qryLOCAPPBNFDOMAIN: TStringField;
    qryLOCAPPCD_PERP: TIntegerField;
    qryLOCAPPCD_PERM: TIntegerField;
    procedure Btn_NewClick(Sender: TObject);
    procedure sPageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure sPageControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure Btn_TempClick(Sender: TObject);
  private
    { Private declarations }
    FCurrentWork : TProgramControlType;
    FBeforePageIndex : integer;
    procedure CreateReady;
    procedure PanelReadOnly(var UserPanel: TsPanel; bValue: Boolean);
  public
    { Public declarations }
  end;

var
  LOCAPP_N_MSSQL_frm: TLOCAPP_N_MSSQL_frm;

implementation

uses LOCADV_N, Commonlib;

{$R *.dfm}

{ TLOCAPP_N_MSSQL_frm }

procedure TLOCAPP_N_MSSQL_frm.CreateReady;
begin
  //관리번호 ReadOnly
  sDBEdit1.ReadOnly := not (FCurrentWork = ctInsert);

  //Button State
  Btn_New.Enabled    := FCurrentWork = ctView;
  Btn_Modify.Enabled := FCurrentWork = ctView;
  Btn_Del.Enabled    := FCurrentWork = ctView;

  Btn_Temp.Enabled   := FCurrentWork IN [ctInsert,ctModify];
  Btn_Save.Enabled   := FCurrentWork IN [ctInsert,ctModify];
  Btn_Cancel.Enabled := FCurrentWork IN [ctInsert,ctModify];

  Btn_Print.Enabled  := FCurrentWork = ctView;
  Btn_Ready.Enabled  := FCurrentWork = ctView;

  //Lock ListGrid
  IF sPageControl1.ActivePageIndex = 0 Then
    sPageControl1.ActivePageIndex := 1;

  sPageControl1.Pages[0].Enabled := FCurrentWork = ctView;
  sDBGrid1.Enabled := FCurrentWork = ctView;

  PanelReadOnly(sPanel2, (FCurrentWork = ctView));
  PanelReadOnly(sPanel3, (FCurrentWork = ctView));
  PanelReadOnly(sPanel8, (FCurrentWork = ctView));
  PanelReadOnly(sPanel10,(FCurrentWork = ctView));


end;

procedure TLOCAPP_N_MSSQL_frm.PanelReadOnly(var UserPanel: TsPanel;
  bValue: Boolean);
var
  nLoop ,
  MaxCount : integer;
  UserDBEdit : TsDBEdit;
  UserDBMemo : TsDBMemo;
begin
  MaxCount := UserPanel.ControlCount;
  for nLoop := 0 to MaxCount -1 do
  begin
    IF UserPanel.Controls[nLoop] is TsDBEdit Then
    begin
      UserDBEdit := TsDBEdit(UserPanel.Controls[nLoop]);
      UserDBEdit.ReadOnly := bValue;
    end
    else
    IF UserPanel.Controls[nLoop] is TsDBMemo Then
    begin
      UserDBMemo := TsDBMemo(UserPanel.Controls[nLoop]);
      UserDBMemo.ReadOnly := bValue;
    end;

  end;
end;

procedure TLOCAPP_N_MSSQL_frm.Btn_NewClick(Sender: TObject);
begin
  inherited;
  Case (Sender as TsSpeedButton).Tag of
    0:
    begin
      FCurrentWork := ctInsert;
      CreateReady;
      qryList.Append;
    end;

    1:
    begin
      FCurrentWork := ctModify;
      CreateReady;
      qryList.Edit;      
    end;

    2:
    begin
      FCurrentWork := ctDelete;
    end;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.sPageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  IF FCurrentWork IN [ctInsert, ctModify] Then
    FBeforePageIndex := sPageControl1.ActivePageIndex
end;

procedure TLOCAPP_N_MSSQL_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  IF FCurrentWork IN [ctInsert, ctModify] Then
  begin
    IF sPageControl1.ActivePageIndex = 0 Then
      sPageControl1.ActivePageIndex := FBeforePageIndex;
  end;
end;

procedure TLOCAPP_N_MSSQL_frm.FormCreate(Sender: TObject);
begin
//  inherited;
  FCurrentWork := ctView;
end;

procedure TLOCAPP_N_MSSQL_frm.Btn_CancelClick(Sender: TObject);
begin
  inherited;
  IF not ConfirmMessage('작업을 취소하시겠습니까?'#13#10'지금까지 작성했던 데이터들은 삭제됩니다.') Then Exit; 

  FCurrentWork := ctView;
//  DMMssql.RollbackTrans;
  CreateReady;
  qryList.Cancel;
end;

procedure TLOCAPP_N_MSSQL_frm.Btn_TempClick(Sender: TObject);
begin
  inherited;
  qryList.Post;
  FCurrentWork := ctView;
  CreateReady;
end;

end.
