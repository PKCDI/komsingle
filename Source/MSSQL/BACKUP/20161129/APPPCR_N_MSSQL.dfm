inherited APPPCR_N_MSSQL_frm: TAPPPCR_N_MSSQL_frm
  Left = 549
  Top = 67
  Caption = ''
  ClientHeight = 701
  ClientWidth = 880
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel2: TsPanel
    Width = 880
    Height = 701
    inherited sPageControl1: TsPageControl
      Top = 86
      Width = 878
      Height = 614
      ActivePage = sTabSheet3
      TabIndex = 2
      OnChange = sPageControl1Change
      OnChanging = sPageControl1Changing
      inherited sTabSheet1: TsTabSheet
        inherited sPanel7: TsPanel
          Width = 870
          object sLabel8: TsLabel [0]
            Left = 752
            Top = 10
            Width = 108
            Height = 12
            AutoSize = False
            Caption = 'INS:'#49888#44508'  DEL:'#49325#51228
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = []
          end
          inherited sEdit3: TsEdit
            OnKeyPress = sEdit3KeyPress
          end
          object sButton1: TsButton
            Left = 562
            Top = 6
            Width = 75
            Height = 20
            Caption = #51312#54924
            TabOrder = 4
            OnClick = sButton1Click
            SkinData.SkinSection = 'BUTTON'
          end
        end
        inherited sDBGrid1: TsDBGrid
          Width = 870
          Height = 547
          Color = clWhite
          DataSource = dsList
          Font.Name = #47569#51008' '#44256#46357
          TitleFont.Name = #47569#51008' '#44256#46357
          OnDrawColumnCell = sDBGrid1DrawColumnCell
          OnDblClick = sDBGrid1DblClick
          OnKeyDown = sDBGrid1KeyDown
          Columns = <
            item
              Expanded = False
              Title.Alignment = taCenter
              Title.Caption = #49440#53469
              Width = -1
              Visible = False
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHK2'
              Title.Alignment = taCenter
              Title.Caption = #49345#54889
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Caption = #44288#47532#48264#54840
              Width = 129
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUP_NAME1'
              Title.Caption = #44277#44553#51088
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TQTY'
              Title.Caption = #52509#49688#47049
              Width = 99
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TQTYC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAMT1'
              Title.Caption = #52509#44552#50529
              Width = 99
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TAMT1C'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'USER_ID'
              Title.Caption = #49324#50857#51088
              Width = 42
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51068#51088
              Width = 90
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHK3'
              Title.Alignment = taCenter
              Title.Caption = #49569#49888#54788#54889
              Width = 129
              Visible = True
            end>
        end
      end
      inherited sTabSheet2: TsTabSheet
        inherited sSplitter3: TsSplitter
          Left = 377
          Height = 353
        end
        inherited sGroupBox1: TsGroupBox
          Width = 377
          Height = 353
          inherited Edt_ApplicationCode: TsEdit
            OnDblClick = Edt_ApplicationCodeDblClick
            OnExit = Edt_ApplicationCodeExit
          end
          inherited Edt_ApplicationSAUPNO: TsEdit
            BoundLabel.Font.Style = [fsBold]
          end
          inherited Edt_ApplicationElectronicSign: TsEdit
            BoundLabel.Font.Style = [fsBold]
          end
          inherited Edt_ApplicationAPPDATE: TsEdit
            BoundLabel.Font.Style = [fsBold]
          end
        end
        inherited sGroupBox2: TsGroupBox
          Left = 381
          Width = 481
          Height = 353
          inherited Edt_DocumentsSection: TsEdit
            OnDblClick = Edt_DocumentsSectionDblClick
            OnExit = Edt_DocumentsSectionExit
          end
          inherited Edt_SupplyCode: TsEdit
            OnDblClick = Edt_ApplicationCodeDblClick
          end
          inherited Edt_SupplyName: TsEdit
            BoundLabel.Font.Style = [fsBold]
          end
          inherited Edt_SupplySaupNo: TsEdit
            BoundLabel.Font.Style = [fsBold]
          end
        end
        inherited sGroupBox3: TsGroupBox
          Top = 353
          Width = 862
          Height = 89
          inherited sEdit29: TsEdit
            Top = 38
            BoundLabel.Caption = #52509#44552#50529'(USD)'
          end
          inherited sEdit31: TsEdit
            Top = 60
          end
          inherited sEdit32: TsEdit
            Top = 60
            OnDblClick = Edt_DocumentsSectionExit
            OnExit = Edt_DocumentsSectionExit
          end
          inherited sEdit27: TsEdit
            OnDblClick = Edt_DocumentsSectionDblClick
            OnExit = Edt_DocumentsSectionExit
          end
        end
        inherited sGroupBox4: TsGroupBox
          Top = 442
          Width = 862
          Height = 129
          inherited sEdit36: TsEdit
            Left = 528
            Top = 50
            OnDblClick = Edt_DocumentsSectionDblClick
            OnExit = Edt_DocumentsSectionExit
          end
          inherited sEdit40: TsEdit
            Left = 528
            Top = 70
            OnDblClick = Edt_DocumentsSectionDblClick
            OnExit = Edt_DocumentsSectionExit
          end
          inherited sEdit33: TsEdit
            OnDblClick = Edt_DocumentsSectionDblClick
            OnExit = Edt_DocumentsSectionExit
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sEdit39: TsEdit
            OnDblClick = sEdit39DblClick
            OnExit = sEdit39Exit
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sEdit41: TsEdit
            Tag = 2
            Left = 392
            Top = 50
            Color = 13303807
          end
          inherited sEdit42: TsEdit
            Tag = 2
            Left = 392
            Top = 70
            Color = 13303807
          end
          inherited sEdit43: TsEdit
            Left = 392
            Top = 90
            Color = 13303807
            BoundLabel.Caption = #52509#44552#50529'(USD)'
          end
          object sButton2: TsButton
            Left = 562
            Top = 50
            Width = 75
            Height = 61
            Caption = #44228#49328
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 10
            OnClick = sButton2Click
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System16
            ImageIndex = 7
            Reflected = True
          end
        end
      end
      inherited sTabSheet3: TsTabSheet
        inherited sGroupBox5: TsGroupBox
          Top = 302
          Width = 870
          Height = 277
          object sSpeedButton6: TsSpeedButton [0]
            Left = 154
            Top = 55
            Width = 23
            Height = 23
            OnClick = sSpeedButton6Click
            SkinData.SkinSection = 'SPEEDBUTTON'
            ImageIndex = 0
            Images = DMICON.System16
          end
          inherited sDBEdit33: TsDBEdit
            Top = 55
            Width = 68
            Height = 23
            Ctl3D = True
            DataField = 'NAMESS'
            DataSource = dsDetail
            ReadOnly = True
            OnDblClick = sDBEdit65DblClick
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit36: TsDBEdit
            Left = 309
            Top = 31
            Width = 108
            Height = 23
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBMemo1: TsDBMemo
            Left = 85
            Top = 79
            Width = 332
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 3
          end
          inherited sDBMemo2: TsDBMemo
            Top = 134
            Width = 332
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 4
          end
          inherited sDBMemo3: TsDBMemo
            Top = 189
            Width = 332
            Height = 57
            Ctl3D = True
            DataField = 'REMARK'
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 5
          end
          inherited sDBEdit34: TsDBEdit
            Top = 55
            Width = 40
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 6
            OnDblClick = sDBEdit34DblClick
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit35: TsDBEdit
            Top = 79
            Width = 40
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 8
            OnDblClick = sDBEdit34DblClick
          end
          inherited sDBEdit37: TsDBEdit
            Top = 103
            Width = 184
            Height = 23
            Color = 13303807
            Ctl3D = True
            DataSource = dsDetail
            Font.Style = [fsBold]
            ReadOnly = True
            TabOrder = 10
            OnEnter = sDBEdit45Enter
            OnExit = sDBEdit43Exit
            SkinData.CustomFont = True
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit38: TsDBEdit
            Top = 127
            Width = 184
            Height = 23
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 11
            OnEnter = sDBEdit45Enter
          end
          inherited sDBEdit39: TsDBEdit
            Top = 151
            Width = 40
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 12
            OnDblClick = sDBEdit34DblClick
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit40: TsDBEdit
            Top = 175
            Width = 184
            Height = 23
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 14
            OnEnter = sDBEdit45Enter
          end
          inherited sDBEdit41: TsDBEdit
            Top = 199
            Width = 40
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 15
            OnDblClick = sDBEdit34DblClick
          end
          inherited sDBEdit42: TsDBEdit
            Top = 223
            Width = 40
            Height = 23
            CharCase = ecUpperCase
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 17
            OnDblClick = sDBEdit34DblClick
          end
          inherited sDBEdit43: TsDBEdit
            Left = 590
            Top = 55
            Width = 143
            Height = 23
            Color = 13303807
            Ctl3D = True
            DataSource = dsDetail
            Font.Style = [fsBold]
            ReadOnly = True
            TabOrder = 7
            OnEnter = sDBEdit45Enter
            OnExit = sDBEdit43Exit
            SkinData.CustomFont = True
          end
          inherited sDBEdit44: TsDBEdit
            Left = 590
            Top = 79
            Width = 143
            Height = 23
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 9
            OnEnter = sDBEdit45Enter
          end
          inherited sDBEdit45: TsDBEdit
            Left = 590
            Top = 151
            Width = 143
            Height = 23
            Color = 13303807
            Ctl3D = True
            DataSource = dsDetail
            Font.Style = [fsBold]
            ReadOnly = True
            TabOrder = 13
            OnEnter = sDBEdit45Enter
            SkinData.CustomFont = True
          end
          inherited sDBEdit46: TsDBEdit
            Left = 590
            Top = 199
            Width = 143
            Height = 23
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            OnEnter = sDBEdit45Enter
          end
          inherited sDBEdit47: TsDBEdit
            Left = 590
            Top = 223
            Width = 143
            Height = 23
            Ctl3D = True
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 18
            OnEnter = sDBEdit45Enter
          end
          inherited sDBEdit1: TsDBEdit
            Left = 309
            Top = 55
            Width = 108
            Height = 23
            Ctl3D = True
            DataField = 'APP_DATE'
            DataSource = dsDetail
            ReadOnly = True
            TabOrder = 2
            BoundLabel.Caption = #44396#47588'('#44277#44553')'#51068
            BoundLabel.Font.Style = [fsBold]
          end
        end
        inherited Pan_Detail: TsPanel
          Width = 870
          inherited Btn_DetailAdd: TsSpeedButton
            Enabled = False
            OnClick = Btn_DetailAddClick
          end
          inherited Btn_DetailDel: TsSpeedButton
            Tag = 2
            Left = 174
            Width = 67
            Enabled = False
            OnClick = Btn_DetailAddClick
          end
          inherited Line3: TsSpeedButton
            Left = 169
          end
          inherited Btn_DetailMod: TsSpeedButton
            Tag = 1
            Width = 67
            Enabled = False
            OnClick = Btn_DetailAddClick
          end
          inherited LIne2: TsSpeedButton
            Left = 241
          end
          object Btn_DetailOk: TsSpeedButton
            Left = 246
            Top = 1
            Width = 67
            Height = 23
            Caption = #54869#51064
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailAddClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            ImageIndex = 17
            Images = DMICON.System18
          end
          object Btn_DetailCancel: TsSpeedButton
            Left = 313
            Top = 1
            Width = 67
            Height = 23
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailAddClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            ImageIndex = 18
            Images = DMICON.System18
          end
          object sSpeedButton14: TsSpeedButton
            Left = 380
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
        end
        inherited sDBGrid2: TsDBGrid
          Width = 870
          Height = 277
          DataSource = dsDetail
          Font.Name = #47569#51008' '#44256#46357
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TitleFont.Name = #47569#51008' '#44256#46357
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'HS_NO'
              Title.Caption = 'HS'#48264#54840
              Width = 87
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME1'
              Title.Caption = #54408#47749
              Width = 192
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Caption = #49688#47049
              Width = 95
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTYC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT1'
              Title.Caption = #44552#50529
              Width = 95
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'AMT1C'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRI2'
              Title.Caption = #45800#44032
              Width = 95
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRI_BASE'
              Title.Caption = #45800#44032#44592#51456#49688#47049
              Width = 137
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'PRI_BASEC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end>
        end
      end
      inherited sTabSheet4: TsTabSheet
        inherited sGroupBox6: TsGroupBox
          Top = 262
          Width = 870
          inherited sLabel4: TsLabel
            Left = 508
            Top = 42
            Width = 218
            Height = 13
            Color = clGreen
            ParentColor = False
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            UseSkinColor = True
          end
          inherited sLabel5: TsLabel
            Top = 295
            Width = 273
            Height = 13
            Color = clGreen
            ParentColor = False
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            UseSkinColor = True
          end
          object sSpeedButton7: TsSpeedButton [2]
            Left = 194
            Top = 73
            Width = 23
            Height = 23
            OnClick = sSpeedButton7Click
            SkinData.SkinSection = 'SPEEDBUTTON'
            ImageIndex = 0
            Images = DMICON.System16
          end
          object sLabel2: TsLabel [3]
            Left = 508
            Top = 90
            Width = 351
            Height = 26
            Caption = 
              #44540#44144#49436#47448' '#49345#51032' '#49440#51201#44592#51068#51060' '#50668#47084' '#44060#51068' '#44221#50864' '#52572#51109' '#49440#51201#44592#51068' '#51077#47141#13#10#44540#44144#49436#47448' '#51333#47448#44032' '#44396#47588#54869#51064#49436#51068' '#44221#50864', '#49440#51201#44592#51068#51008' '#47932#54408#44277 +
              #44553'('#50696#51221')'#51068' '#51077#47141
            Color = clGreen
            ParentColor = False
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sSpeedButton16: TsSpeedButton [4]
            Left = 162
            Top = 210
            Width = 23
            Height = 23
            OnClick = sSpeedButton7Click
            SkinData.SkinSection = 'SPEEDBUTTON'
            ImageIndex = 0
            Images = DMICON.System16
          end
          inherited sDBEdit48: TsDBEdit
            Height = 23
            CharCase = ecUpperCase
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            OnDblClick = sDBEdit34DblClick
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit49: TsDBEdit
            Left = 341
            Width = 92
            Height = 23
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 3
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBMemo5: TsDBMemo
            Width = 316
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 4
            BoundLabel.Active = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBMemo6: TsDBMemo
            Top = 152
            Width = 316
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 5
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit50: TsDBEdit
            Left = 509
            Top = 18
            Height = 23
            CharCase = ecUpperCase
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 10
            OnDblClick = sDBEdit34DblClick
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit51: TsDBEdit
            Top = 210
            Height = 23
            CharCase = ecUpperCase
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 6
            OnDblClick = sDBEdit51DblClick
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit52: TsDBEdit
            Top = 290
            Height = 23
            CharCase = ecUpperCase
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 9
            OnDblClick = sDBEdit34DblClick
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit58: TsDBEdit
            Left = 554
            Top = 18
            Height = 23
            Color = 13303807
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ReadOnly = True
            TabOrder = 11
            OnEnter = sDBEdit45Enter
            SkinData.CustomFont = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit59: TsDBEdit
            Left = 117
            Top = 234
            Width = 316
            Height = 23
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 7
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit64: TsDBEdit
            Width = 316
            Height = 23
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 1
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Font.Style = [fsBold]
          end
          inherited sDBEdit65: TsDBEdit
            Height = 23
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 2
            OnDblClick = sDBEdit65DblClick
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit66: TsDBEdit
            Left = 117
            Top = 258
            Width = 316
            Height = 23
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            ReadOnly = True
            TabOrder = 8
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited Pan_DocNO: TsPanel
            Left = 170
            Width = 263
            Height = 23
            Font.Color = clBlack
            Font.Name = #47569#51008' '#44256#46357
            ParentFont = False
            TabOrder = 13
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit63: TsDBEdit
            Left = 509
            Top = 66
            Height = 23
            DataField = 'SHIP_DATE'
            DataSource = dsDocument
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 12
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Font.Style = [fsBold]
          end
        end
        inherited sPanel9: TsPanel
          Width = 870
          inherited Btn_DocAdd: TsSpeedButton
            Enabled = False
            OnClick = Btn_DocAddClick
          end
          inherited sSpeedButton11: TsSpeedButton
            Left = 169
          end
          inherited Btn_DocDel: TsSpeedButton
            Tag = 2
            Left = 174
            Width = 67
            Enabled = False
            OnClick = Btn_DocAddClick
          end
          inherited Btn_DocMod: TsSpeedButton
            Tag = 1
            Width = 67
            Enabled = False
            OnClick = Btn_DocAddClick
          end
          inherited sSpeedButton15: TsSpeedButton
            Left = 241
          end
          object Btn_DocOk: TsSpeedButton
            Left = 246
            Top = 1
            Width = 67
            Height = 23
            Caption = #54869#51064
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DocAddClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            ImageIndex = 17
            Images = DMICON.System18
          end
          object Btn_DocCancel: TsSpeedButton
            Left = 313
            Top = 1
            Width = 67
            Height = 23
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DocAddClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            ImageIndex = 18
            Images = DMICON.System18
          end
          object sSpeedButton12: TsSpeedButton
            Left = 380
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
        end
        inherited sDBGrid3: TsDBGrid
          Width = 870
          Height = 237
          DataSource = dsDocument
          Font.Name = #47569#51008' '#44256#46357
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TitleFont.Name = #47569#51008' '#44256#46357
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DOC_G'
              Title.Alignment = taCenter
              Title.Caption = #47928#49436
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DOC_NAME'
              Title.Caption = #47928#49436#47749
              Width = 159
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DOC_D'
              Title.Caption = #44540#44144#49436#47448#48264#54840
              Width = 114
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BHS_NO'
              Title.Caption = 'HS'#48264#54840
              Width = 86
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BNAME1'
              Title.Caption = #54408#47749
              Width = 158
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BAMT'
              Title.Alignment = taCenter
              Title.Caption = #44552#50529
              Width = 142
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BAMTC'
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SHIP_DATE'
              Title.Alignment = taCenter
              Title.Caption = #49440#51201#44592#51068
              Width = 73
              Visible = True
            end>
        end
      end
      inherited sTabSheet5: TsTabSheet
        inherited sGroupBox7: TsGroupBox
          Top = 317
          Width = 870
          Height = 262
          inherited sDBEdit53: TsDBEdit
            Height = 23
            DataField = 'BILL_NO'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBMemo7: TsDBMemo
            Top = 187
            DataField = 'ITEM_DESC'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 7
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit55: TsDBEdit
            Height = 23
            DataField = 'BILL_AMOUNT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 8
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit60: TsDBEdit
            Height = 23
            DataField = 'BILL_AMOUNT_UNIT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 9
            OnDblClick = sDBEdit60DblClick
            OnExit = sDBEdit60Exit
            OnKeyUp = sDBEdit60KeyUp
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit68: TsDBEdit
            Top = 67
            Height = 23
            DataField = 'ITEM_NAME1'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 2
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit54: TsDBEdit
            Top = 91
            Height = 23
            DataField = 'ITEM_NAME2'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 3
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit62: TsDBEdit
            Top = 115
            Height = 23
            DataField = 'ITEM_NAME3'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 4
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit70: TsDBEdit
            Top = 139
            Height = 23
            DataField = 'ITEM_NAME4'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 5
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit56: TsDBEdit
            Height = 23
            DataField = 'TAX_AMOUNT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 10
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit57: TsDBEdit
            Height = 23
            DataField = 'TAX_AMOUNT_UNIT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 11
            OnDblClick = sDBEdit60DblClick
            OnExit = sDBEdit60Exit
            OnKeyUp = sDBEdit60KeyUp
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit61: TsDBEdit
            Height = 23
            DataField = 'QUANTITY'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 12
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit67: TsDBEdit
            Height = 23
            DataField = 'QUANTITY_UNIT'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 13
            OnDblClick = sDBEdit60DblClick
            OnExit = sDBEdit60Exit
            OnKeyUp = sDBEdit60KeyUp
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit23: TsDBEdit
            Top = 163
            Height = 23
            DataField = 'ITEM_NAME5'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            TabOrder = 6
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
          inherited sDBEdit72: TsDBEdit
            Top = 43
            Height = 23
            DataField = 'BILL_DATE'
            DataSource = dsTax
            Font.Color = 4013373
            Font.Name = #47569#51008' '#44256#46357
            MaxLength = 10
            TabOrder = 1
            OnEnter = sDBEdit53Enter
            OnExit = sDBEdit53Exit
            SkinData.SkinSection = 'GROUPBOX'
          end
        end
        inherited sPanel10: TsPanel
          Width = 870
          inherited Btn_TaxAdd: TsSpeedButton
            Enabled = False
            OnClick = Btn_TaxAddClick
          end
          inherited sSpeedButton18: TsSpeedButton
            Left = 169
          end
          inherited Btn_TaxDel: TsSpeedButton
            Left = 174
            Width = 67
            Enabled = False
            OnClick = Btn_TaxAddClick
          end
          inherited Btn_TaxMod: TsSpeedButton
            Width = 67
            Enabled = False
            OnClick = Btn_TaxAddClick
          end
          inherited sSpeedButton22: TsSpeedButton
            Left = 380
          end
          object Btn_TaxOk: TsSpeedButton
            Left = 246
            Top = 1
            Width = 67
            Height = 23
            Caption = #54869#51064
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_TaxAddClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            ImageIndex = 17
            Images = DMICON.System18
          end
          object Btn_TaxCancel: TsSpeedButton
            Left = 313
            Top = 1
            Width = 67
            Height = 23
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_TaxAddClick
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            ImageIndex = 18
            Images = DMICON.System18
          end
          object sSpeedButton9: TsSpeedButton
            Left = 241
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
        end
        inherited sDBGrid4: TsDBGrid
          Width = 870
          Height = 292
          DataSource = dsTax
          Font.Name = #47569#51008' '#44256#46357
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TitleFont.Name = #47569#51008' '#44256#46357
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_DATE'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51068#51088
              Width = 71
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_NO'
              Title.Caption = #49464#44552#44228#49328#49436#48264#54840
              Width = 214
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ITEM_NAME1'
              Title.Caption = #54408#47785'1'
              Width = 153
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_AMOUNT'
              Title.Caption = #44277#44553#44032#50529
              Width = 90
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAX_AMOUNT'
              Title.Caption = #49464#50529
              Width = 90
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TAX_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUANTITY'
              Title.Caption = #49688#47049
              Width = 90
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QUANTITY_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end>
        end
      end
    end
    inherited sPanel1: TsPanel
      Width = 878
      Height = 53
      OnClick = sPanel1Click
      object sSpeedButton3: TsSpeedButton
        Left = 177
        Top = 1
        Width = 2
        Height = 51
        Cursor = crHandPoint
        Layout = blGlyphTop
        Spacing = 0
        Align = alLeft
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
      end
      object sSplitter2: TsSplitter
        Left = 1
        Top = 1
        Width = 176
        Height = 51
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object Btn_New: TsSpeedButton
        Left = 179
        Top = 1
        Width = 54
        Height = 51
        Cursor = crHandPoint
        Caption = #49888#44508
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        OnClick = Btn_NewClick
        Align = alLeft
        Alignment = taLeftJustify
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 5
        Images = DMICON.System24
        Reflected = True
      end
      object Btn_Modify: TsSpeedButton
        Tag = 1
        Left = 233
        Top = 1
        Width = 53
        Height = 51
        Cursor = crHandPoint
        Caption = #49688#51221
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        OnClick = Btn_NewClick
        Align = alLeft
        Alignment = taLeftJustify
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 8
        Images = DMICON.System24
        Reflected = True
      end
      object Btn_Del: TsSpeedButton
        Tag = 2
        Left = 286
        Top = 1
        Width = 53
        Height = 51
        Cursor = crHandPoint
        Caption = #49325#51228
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        OnClick = Btn_NewClick
        Align = alLeft
        Alignment = taLeftJustify
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 19
        Images = DMICON.System24
        Reflected = True
      end
      object sSpeedButton1: TsSpeedButton
        Left = 339
        Top = 1
        Width = 2
        Height = 51
        Cursor = crHandPoint
        Layout = blGlyphTop
        Spacing = 0
        Align = alLeft
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
      end
      object sSpeedButton5: TsSpeedButton
        Left = 811
        Top = 1
        Width = 11
        Height = 51
        Cursor = crHandPoint
        Layout = blGlyphTop
        Spacing = 0
        Align = alRight
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
      end
      object Btn_Close: TsSpeedButton
        Left = 822
        Top = 1
        Width = 55
        Height = 51
        Cursor = crHandPoint
        Caption = #45803#44592
        Layout = blGlyphTop
        Spacing = 0
        OnClick = Btn_CloseClick
        Align = alRight
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 20
        Images = DMICON.System24
        Reflected = True
      end
      object sSpeedButton2: TsSpeedButton
        Left = 610
        Top = 1
        Width = 2
        Height = 51
        Cursor = crHandPoint
        Layout = blGlyphTop
        Spacing = 0
        Align = alLeft
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
      end
      object Btn_Print: TsSpeedButton
        Left = 502
        Top = 1
        Width = 53
        Height = 51
        Cursor = crHandPoint
        Caption = #52636#47141
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        OnClick = Btn_PrintClick
        Align = alLeft
        Alignment = taLeftJustify
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 21
        Images = DMICON.System24
        Reflected = True
      end
      object sLabel6: TsLabel
        Left = 8
        Top = 7
        Width = 73
        Height = 25
        Caption = 'APPPCR'
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sLabel7: TsLabel
        Left = 8
        Top = 29
        Width = 148
        Height = 15
        Caption = #50808#54868#54925#46301#50857' '#44396#47588#54869#51064#49888#52397#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object Btn_Ready: TsSpeedButton
        Left = 557
        Top = 1
        Width = 53
        Height = 51
        Cursor = crHandPoint
        Caption = #51456#48708
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        OnClick = Btn_ReadyClick
        Align = alLeft
        Alignment = taLeftJustify
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 22
        Images = DMICON.System24
        Reflected = True
      end
      object Btn_Save: TsSpeedButton
        Tag = 1
        Left = 394
        Top = 1
        Width = 53
        Height = 51
        Cursor = crHandPoint
        Caption = #51200#51109
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        OnClick = Btn_SaveClick
        Align = alLeft
        Alignment = taLeftJustify
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 7
        Images = DMICON.System24
        Reflected = True
      end
      object Btn_Cancel: TsSpeedButton
        Tag = 2
        Left = 447
        Top = 1
        Width = 53
        Height = 51
        Cursor = crHandPoint
        Caption = #52712#49548
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        OnClick = Btn_CancelClick
        Align = alLeft
        Alignment = taLeftJustify
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 18
        Images = DMICON.System24
        Reflected = True
      end
      object Btn_Temp: TsSpeedButton
        Left = 341
        Top = 1
        Width = 53
        Height = 51
        Cursor = crHandPoint
        Caption = #51076#49884
        Enabled = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = [fsBold]
        Layout = blGlyphTop
        ParentFont = False
        Spacing = 0
        OnClick = Btn_SaveClick
        Align = alLeft
        Alignment = taLeftJustify
        SkinData.CustomFont = True
        SkinData.SkinSection = 'TRANSPARENT'
        ImageIndex = 12
        Images = DMICON.System24
        Reflected = True
      end
      object sSpeedButton26: TsSpeedButton
        Left = 555
        Top = 1
        Width = 2
        Height = 51
        Cursor = crHandPoint
        Layout = blGlyphTop
        Spacing = 0
        Align = alLeft
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
      end
      object sSpeedButton27: TsSpeedButton
        Left = 500
        Top = 1
        Width = 2
        Height = 51
        Cursor = crHandPoint
        Layout = blGlyphTop
        Spacing = 0
        Align = alLeft
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
      end
    end
    inherited sPanel3: TsPanel
      Top = 54
      Width = 878
      inherited Edt_User: TsEdit
        Left = 416
      end
      inherited EdtMk_RegDate: TsMaskEdit
        Left = 544
      end
      inherited Com_DocumentFunc: TsComboBox
        Left = 680
      end
      inherited sComboBox2: TsComboBox
        Left = 808
      end
      object Edt_DocNo: TsEdit
        Left = 64
        Top = 6
        Width = 209
        Height = 21
        Hint = 'AX_NAME2'
        Color = clWhite
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        SkinData.SkinSection = 'GROUPBOX'
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #44404#47548#52404
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
    end
    object sPanel4: TsPanel
      Left = 723
      Top = 88
      Width = 130
      Height = 24
      Anchors = [akRight]
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
    end
    object sPanel5: TsPanel
      Left = 697
      Top = 88
      Width = 25
      Height = 24
      Anchors = [akRight]
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      object sSpeedButton4: TsSpeedButton
        Left = 1
        Top = 1
        Width = 23
        Height = 22
        Caption = #9664
        OnClick = sSpeedButton4Click
        Align = alClient
        SkinData.SkinSection = 'SPEEDBUTTON'
      end
    end
    object sPanel6: TsPanel
      Left = 854
      Top = 88
      Width = 25
      Height = 24
      Anchors = [akRight]
      TabOrder = 5
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      object sSpeedButton8: TsSpeedButton
        Left = 1
        Top = 1
        Width = 23
        Height = 22
        Caption = #9654
        OnClick = sSpeedButton8Click
        Align = alClient
        SkinData.SkinSection = 'SPEEDBUTTON'
      end
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[USER_ID]'
      '      ,[DATEE]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[APP_CODE]'
      '      ,[APP_NAME1]'
      '      ,[APP_NAME2]'
      '      ,[APP_NAME3]'
      '      ,[APP_ADDR1]'
      '      ,[APP_ADDR2]'
      '      ,[APP_ADDR3]'
      '      ,[AX_NAME1]'
      '      ,[AX_NAME2]'
      '      ,[AX_NAME3]'
      '      ,[SUP_CODE]'
      '      ,[SUP_NAME1]'
      '      ,[SUP_NAME2]'
      '      ,[SUP_NAME3]'
      '      ,[SUP_ADDR1]'
      '      ,[SUP_ADDR2]'
      '      ,[SUP_ADDR3]'
      '      ,[ITM_G1]'
      '      ,[ITM_G2]'
      '      ,[ITM_G3]'
      '      ,[TQTY]'
      '      ,[TQTYC]'
      '      ,[TAMT1]'
      '      ,[TAMT1C]'
      '      ,[TAMT2]'
      '      ,[TAMT2C]'
      '      ,[CHG_G]'
      '      ,[C1_AMT]'
      '      ,[C1_C]'
      '      ,[C2_AMT]'
      '      ,[BK_NAME1]'
      '      ,[BK_NAME2]'
      '      ,[PRNO]'
      '      ,[PCrLic_No]'
      '      ,[ChgCd]'
      '      ,[APP_DATE]'
      '      ,[DOC_G]'
      '      ,[DOC_D]'
      '      ,[BHS_NO]'
      '      ,[BNAME]'
      '      ,[BNAME1]'
      '      ,[BAMT]'
      '      ,[BAMTC]'
      '      ,[EXP_DATE]'
      '      ,[SHIP_DATE]'
      '      ,[BSIZE1]'
      '      ,[BAL_CD]'
      '      ,[BAL_NAME1]'
      '      ,[BAL_NAME2]'
      '      ,[F_INTERFACE]'
      '      ,[SRBUHO]'
      '      ,[ITM_GBN]'
      '      ,[ITM_GNAME]'
      '      ,[ISSUE_GBN]'
      '      ,[DOC_GBN]'
      '      ,[DOC_NO]'
      '      ,[BAMT2]'
      '      ,[BAMTC2]'
      '      ,[BAMT3]'
      '      ,[BAMTC3]'
      '      ,[PCRLIC_ISS_NO]'
      '      ,[FIN_CODE]'
      '      ,[FIN_NAME]'
      '      ,[FIN_NAME2]'
      '      ,[NEGO_APPDT]'
      '      ,[EMAIL_ID]'
      '      ,[EMAIL_DOMAIN]'
      '      ,[BK_CD]'
      'FROM APPPCR_H with(nolock)')
    Left = 24
    Top = 224
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      OnGetText = qryListDATEEGetText
      Size = 8
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryListAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryListAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryListAPP_ADDR1: TStringField
      FieldName = 'APP_ADDR1'
      Size = 35
    end
    object qryListAPP_ADDR2: TStringField
      FieldName = 'APP_ADDR2'
      Size = 35
    end
    object qryListAPP_ADDR3: TStringField
      FieldName = 'APP_ADDR3'
      Size = 35
    end
    object qryListAX_NAME1: TStringField
      FieldName = 'AX_NAME1'
      Size = 35
    end
    object qryListAX_NAME2: TStringField
      FieldName = 'AX_NAME2'
      Size = 35
    end
    object qryListAX_NAME3: TStringField
      FieldName = 'AX_NAME3'
      Size = 10
    end
    object qryListSUP_CODE: TStringField
      FieldName = 'SUP_CODE'
      Size = 10
    end
    object qryListSUP_NAME1: TStringField
      FieldName = 'SUP_NAME1'
      Size = 35
    end
    object qryListSUP_NAME2: TStringField
      FieldName = 'SUP_NAME2'
      Size = 35
    end
    object qryListSUP_NAME3: TStringField
      FieldName = 'SUP_NAME3'
      Size = 35
    end
    object qryListSUP_ADDR1: TStringField
      FieldName = 'SUP_ADDR1'
      Size = 35
    end
    object qryListSUP_ADDR2: TStringField
      FieldName = 'SUP_ADDR2'
      Size = 35
    end
    object qryListSUP_ADDR3: TStringField
      FieldName = 'SUP_ADDR3'
      Size = 35
    end
    object qryListITM_G1: TStringField
      FieldName = 'ITM_G1'
      Size = 3
    end
    object qryListITM_G2: TStringField
      FieldName = 'ITM_G2'
      Size = 3
    end
    object qryListITM_G3: TStringField
      FieldName = 'ITM_G3'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListTAMT1: TBCDField
      FieldName = 'TAMT1'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListTAMT1C: TStringField
      FieldName = 'TAMT1C'
      Size = 3
    end
    object qryListTAMT2: TBCDField
      FieldName = 'TAMT2'
      Precision = 18
    end
    object qryListTAMT2C: TStringField
      FieldName = 'TAMT2C'
      Size = 3
    end
    object qryListCHG_G: TStringField
      FieldName = 'CHG_G'
      Size = 3
    end
    object qryListC1_AMT: TBCDField
      FieldName = 'C1_AMT'
      Precision = 18
    end
    object qryListC1_C: TStringField
      FieldName = 'C1_C'
      Size = 3
    end
    object qryListC2_AMT: TBCDField
      FieldName = 'C2_AMT'
      Precision = 18
    end
    object qryListBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 70
    end
    object qryListBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 70
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListPCrLic_No: TStringField
      FieldName = 'PCrLic_No'
      Size = 35
    end
    object qryListChgCd: TStringField
      FieldName = 'ChgCd'
      Size = 3
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListDOC_G: TStringField
      FieldName = 'DOC_G'
      Size = 3
    end
    object qryListDOC_D: TStringField
      FieldName = 'DOC_D'
      Size = 35
    end
    object qryListBHS_NO: TStringField
      FieldName = 'BHS_NO'
      Size = 10
    end
    object qryListBNAME: TStringField
      FieldName = 'BNAME'
    end
    object qryListBNAME1: TMemoField
      FieldName = 'BNAME1'
      BlobType = ftMemo
    end
    object qryListBAMT: TBCDField
      FieldName = 'BAMT'
      Precision = 18
    end
    object qryListBAMTC: TStringField
      FieldName = 'BAMTC'
      Size = 3
    end
    object qryListEXP_DATE: TStringField
      FieldName = 'EXP_DATE'
      Size = 8
    end
    object qryListSHIP_DATE: TStringField
      FieldName = 'SHIP_DATE'
      Size = 8
    end
    object qryListBSIZE1: TMemoField
      FieldName = 'BSIZE1'
      BlobType = ftMemo
    end
    object qryListBAL_CD: TStringField
      FieldName = 'BAL_CD'
      Size = 4
    end
    object qryListBAL_NAME1: TStringField
      FieldName = 'BAL_NAME1'
      Size = 70
    end
    object qryListBAL_NAME2: TStringField
      FieldName = 'BAL_NAME2'
      Size = 70
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListSRBUHO: TStringField
      FieldName = 'SRBUHO'
      Size = 35
    end
    object qryListITM_GBN: TStringField
      FieldName = 'ITM_GBN'
      Size = 3
    end
    object qryListITM_GNAME: TStringField
      FieldName = 'ITM_GNAME'
      Size = 70
    end
    object qryListISSUE_GBN: TStringField
      FieldName = 'ISSUE_GBN'
      Size = 3
    end
    object qryListDOC_GBN: TStringField
      FieldName = 'DOC_GBN'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListBAMT2: TBCDField
      FieldName = 'BAMT2'
      Precision = 18
    end
    object qryListBAMTC2: TStringField
      FieldName = 'BAMTC2'
      Size = 3
    end
    object qryListBAMT3: TBCDField
      FieldName = 'BAMT3'
      Precision = 18
    end
    object qryListBAMTC3: TStringField
      FieldName = 'BAMTC3'
      Size = 3
    end
    object qryListPCRLIC_ISS_NO: TStringField
      FieldName = 'PCRLIC_ISS_NO'
      Size = 35
    end
    object qryListFIN_CODE: TStringField
      FieldName = 'FIN_CODE'
      Size = 11
    end
    object qryListFIN_NAME: TStringField
      FieldName = 'FIN_NAME'
      Size = 70
    end
    object qryListFIN_NAME2: TStringField
      FieldName = 'FIN_NAME2'
      Size = 70
    end
    object qryListNEGO_APPDT: TStringField
      FieldName = 'NEGO_APPDT'
      Size = 8
    end
    object qryListEMAIL_ID: TStringField
      FieldName = 'EMAIL_ID'
      Size = 35
    end
    object qryListEMAIL_DOMAIN: TStringField
      FieldName = 'EMAIL_DOMAIN'
      Size = 35
    end
    object qryListBK_CD: TStringField
      FieldName = 'BK_CD'
      Size = 11
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 224
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterInsert = qryDetailAfterInsert
    AfterEdit = qryDetailAfterEdit
    AfterPost = qryDetailAfterEdit
    AfterCancel = qryDetailAfterEdit
    AfterScroll = qryDetailAfterScroll
    Parameters = <
      item
        Name = 'KEY'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT [KEYY]'
      '      ,[SEQ]'
      '      ,[HS_NO]'
      '      ,[NAME]'
      '      ,[NAME1]'
      '      ,[SIZE]'
      '      ,[SIZE1]'
      '      ,[REMARK]'
      '      ,[QTY]'
      '      ,[QTYC]'
      '      ,[AMT1]'
      '      ,[AMT1C]'
      '      ,[AMT2]'
      '      ,[PRI1]'
      '      ,[PRI2]'
      '      ,[PRI_BASE]'
      '      ,[PRI_BASEC]'
      '      ,[ACHG_G]'
      '      ,[AC1_AMT]'
      '      ,[AC1_C]'
      '      ,[AC2_AMT]'
      '      ,[LOC_TYPE]'
      '      ,[NAMESS]'
      '      ,[SIZESS]'
      '      ,[APP_DATE]'
      '      ,[ITM_NUM]'
      '  FROM [APPPCRD1] with(nolock)'
      ' WHERE [KEYY] = :KEY')
    Left = 24
    Top = 256
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailHS_NO: TStringField
      FieldName = 'HS_NO'
      EditMask = '0000.00-0000;0;'
      Size = 10
    end
    object qryDetailNAME: TStringField
      FieldName = 'NAME'
    end
    object qryDetailNAME1: TMemoField
      FieldName = 'NAME1'
      OnGetText = qryDetailNAME1GetText
      BlobType = ftMemo
    end
    object qryDetailSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryDetailSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryDetailREMARK: TMemoField
      FieldName = 'REMARK'
      BlobType = ftMemo
    end
    object qryDetailQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryDetailAMT1: TBCDField
      FieldName = 'AMT1'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailAMT1C: TStringField
      FieldName = 'AMT1C'
      Size = 3
    end
    object qryDetailAMT2: TBCDField
      FieldName = 'AMT2'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailPRI1: TBCDField
      FieldName = 'PRI1'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailPRI2: TBCDField
      FieldName = 'PRI2'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailPRI_BASE: TBCDField
      FieldName = 'PRI_BASE'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailPRI_BASEC: TStringField
      FieldName = 'PRI_BASEC'
      Size = 3
    end
    object qryDetailACHG_G: TStringField
      FieldName = 'ACHG_G'
      Size = 3
    end
    object qryDetailAC1_AMT: TBCDField
      FieldName = 'AC1_AMT'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailAC1_C: TStringField
      FieldName = 'AC1_C'
      Size = 3
    end
    object qryDetailAC2_AMT: TBCDField
      FieldName = 'AC2_AMT'
      DisplayFormat = '#,0.0000'
      Precision = 18
    end
    object qryDetailLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryDetailNAMESS: TStringField
      FieldName = 'NAMESS'
    end
    object qryDetailSIZESS: TStringField
      FieldName = 'SIZESS'
    end
    object qryDetailAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryDetailITM_NUM: TStringField
      FieldName = 'ITM_NUM'
      Size = 35
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 56
    Top = 256
  end
  object qryDocument: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryDocumentAfterOpen
    AfterInsert = qryDocumentAfterInsert
    AfterEdit = qryDocumentAfterEdit
    AfterPost = qryDocumentAfterEdit
    AfterCancel = qryDocumentAfterEdit
    AfterScroll = qryDocumentAfterScroll
    Parameters = <
      item
        Name = 'KEY'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R-21101'
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, DOC_G,ISNULL(DOC_NAME,'#39#39') DOC_NAME, DOC_D, BHS' +
        '_NO, BNAME, BNAME1, BAMT, BAMTC, EXP_DATE, SHIP_DATE, BSIZE1, BA' +
        'L_NAME1, BAL_NAME2, BAL_CD, NAT'
      
        'FROM APPPCRD2 with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME'
      #9#9#9#9#9#9'FROM CODE2NDD with(nolock)'
      
        #9#9#9#9#9#9'WHERE Prefix = '#39'APPPCR'#49436#47448#39') DOC ON APPPCRD2.DOC_G = DOC.COD' +
        'E'
      'WHERE [KEYY] = :KEY')
    Left = 24
    Top = 288
    object qryDocumentKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDocumentSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDocumentDOC_G: TStringField
      FieldName = 'DOC_G'
      Size = 3
    end
    object qryDocumentDOC_D: TStringField
      FieldName = 'DOC_D'
      Size = 35
    end
    object qryDocumentBHS_NO: TStringField
      FieldName = 'BHS_NO'
      EditMask = '9999.99-9999;0;'
      Size = 10
    end
    object qryDocumentBNAME: TStringField
      FieldName = 'BNAME'
    end
    object qryDocumentBNAME1: TMemoField
      FieldName = 'BNAME1'
      OnGetText = qryDocumentBNAME1GetText
      BlobType = ftMemo
    end
    object qryDocumentBAMT: TBCDField
      FieldName = 'BAMT'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryDocumentBAMTC: TStringField
      FieldName = 'BAMTC'
      Size = 3
    end
    object qryDocumentEXP_DATE: TStringField
      FieldName = 'EXP_DATE'
      Size = 8
    end
    object qryDocumentSHIP_DATE: TStringField
      FieldName = 'SHIP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryDocumentBSIZE1: TMemoField
      FieldName = 'BSIZE1'
      BlobType = ftMemo
    end
    object qryDocumentBAL_NAME1: TStringField
      FieldName = 'BAL_NAME1'
      Size = 70
    end
    object qryDocumentBAL_NAME2: TStringField
      FieldName = 'BAL_NAME2'
      Size = 70
    end
    object qryDocumentBAL_CD: TStringField
      FieldName = 'BAL_CD'
      Size = 11
    end
    object qryDocumentNAT: TStringField
      FieldName = 'NAT'
      Size = 4
    end
    object qryDocumentDOC_NAME: TStringField
      FieldName = 'DOC_NAME'
      ReadOnly = True
      Size = 100
    end
  end
  object dsDocument: TDataSource
    DataSet = qryDocument
    Left = 56
    Top = 288
  end
  object qryTax: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterInsert = qryTaxAfterInsert
    AfterEdit = qryTaxAfterEdit
    AfterPost = qryTaxAfterEdit
    AfterCancel = qryTaxAfterEdit
    Parameters = <
      item
        Name = 'KEY'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R-71216-'#50672#49845
      end>
    SQL.Strings = (
      'SELECT [KEYY]'
      '      ,[SEQ]'
      '      ,[DATEE]'
      '      ,[BILL_NO]'
      '      ,[BILL_DATE]'
      '      ,[ITEM_NAME1]'
      '      ,[ITEM_NAME2]'
      '      ,[ITEM_NAME3]'
      '      ,[ITEM_NAME4]'
      '      ,[ITEM_NAME5]'
      '      ,[ITEM_DESC]'
      '      ,[BILL_AMOUNT]'
      '      ,[BILL_AMOUNT_UNIT]'
      '      ,[TAX_AMOUNT]'
      '      ,[TAX_AMOUNT_UNIT]'
      '      ,[QUANTITY]'
      '      ,[QUANTITY_UNIT]'
      '  FROM [APPPCR_TAX] with(nolock)'
      'WHERE [KEYY] = :KEY')
    Left = 24
    Top = 320
    object qryTaxKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryTaxBILL_NO: TStringField
      FieldName = 'BILL_NO'
      Size = 35
    end
    object qryTaxBILL_DATE: TStringField
      FieldName = 'BILL_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryTaxITEM_NAME1: TStringField
      FieldName = 'ITEM_NAME1'
      Size = 35
    end
    object qryTaxITEM_NAME2: TStringField
      FieldName = 'ITEM_NAME2'
      Size = 35
    end
    object qryTaxITEM_NAME3: TStringField
      FieldName = 'ITEM_NAME3'
      Size = 35
    end
    object qryTaxITEM_NAME4: TStringField
      FieldName = 'ITEM_NAME4'
      Size = 35
    end
    object qryTaxITEM_NAME5: TStringField
      FieldName = 'ITEM_NAME5'
      Size = 35
    end
    object qryTaxITEM_DESC: TMemoField
      FieldName = 'ITEM_DESC'
      BlobType = ftMemo
    end
    object qryTaxBILL_AMOUNT: TBCDField
      FieldName = 'BILL_AMOUNT'
      DisplayFormat = '#,0;0'
      Precision = 18
    end
    object qryTaxBILL_AMOUNT_UNIT: TStringField
      FieldName = 'BILL_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxTAX_AMOUNT: TBCDField
      FieldName = 'TAX_AMOUNT'
      DisplayFormat = '#,0;0'
      Precision = 18
    end
    object qryTaxTAX_AMOUNT_UNIT: TStringField
      FieldName = 'TAX_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxQUANTITY: TBCDField
      FieldName = 'QUANTITY'
      DisplayFormat = '#,0;0'
      Precision = 18
    end
    object qryTaxQUANTITY_UNIT: TStringField
      FieldName = 'QUANTITY_UNIT'
      Size = 3
    end
    object qryTaxSEQ: TIntegerField
      FieldName = 'SEQ'
    end
  end
  object dsTax: TDataSource
    DataSet = qryTax
    Left = 56
    Top = 320
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APPPCR_H '
      'SET CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 104
    Top = 224
  end
  object qryDetailDel: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM APPPCRD1'
      'WHERE KEYY = :KEYY'
      'AND SEQ = :SEQ')
    Left = 344
    Top = 224
  end
  object qryIns: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'DATEE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'CHK1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'CHK2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESSAGE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'MESSAGE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'APP_CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'APP_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'AX_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'AX_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'AX_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SUP_CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SUP_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ITM_G1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ITM_G2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ITM_G3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'TQTY'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'TQTYC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'TAMT1'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'TAMT1C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'TAMT2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'TAMT2C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'CHG_G'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'C1_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'C1_C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'C2_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'BK_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BK_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'PRNO'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'PCrLic_No'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ChgCd'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'APP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'ITM_GBN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ITM_GNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'EMAIL_ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EMAIL_DOMAIN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BK_CD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end>
    SQL.Strings = (
      
        'INSERT INTO APPPCR_H(MAINT_NO, [USER_ID], DATEE, CHK1, CHK2, CHK' +
        '3, MESSAGE1, MESSAGE2, APP_CODE, APP_NAME1, APP_NAME2, APP_NAME3' +
        ', APP_ADDR1, APP_ADDR2, APP_ADDR3, AX_NAME1, AX_NAME2, AX_NAME3,' +
        ' SUP_CODE, SUP_NAME1, SUP_NAME2, SUP_NAME3, SUP_ADDR1, SUP_ADDR2' +
        ', SUP_ADDR3, ITM_G1, ITM_G2, ITM_G3, TQTY, TQTYC, TAMT1, TAMT1C,' +
        ' TAMT2, TAMT2C, CHG_G, C1_AMT, C1_C, C2_AMT, BK_NAME1, BK_NAME2,' +
        ' PRNO, PCrLic_No, ChgCd, APP_DATE, ITM_GBN, ITM_GNAME, EMAIL_ID,' +
        ' EMAIL_DOMAIN, BK_CD)'
      
        'VALUES(:MAINT_NO, :USER_ID, :DATEE, :CHK1, :CHK2, :CHK3, :MESSAG' +
        'E1, :MESSAGE2, :APP_CODE, :APP_NAME1, :APP_NAME2, :APP_NAME3, :A' +
        'PP_ADDR1, :APP_ADDR2, :APP_ADDR3, :AX_NAME1, :AX_NAME2, :AX_NAME' +
        '3, :SUP_CODE, :SUP_NAME1, :SUP_NAME2, :SUP_NAME3, :SUP_ADDR1, :S' +
        'UP_ADDR2, :SUP_ADDR3, :ITM_G1, :ITM_G2, :ITM_G3, :TQTY, :TQTYC, ' +
        ':TAMT1, :TAMT1C, :TAMT2, :TAMT2C, :CHG_G, :C1_AMT, :C1_C, :C2_AM' +
        'T, :BK_NAME1, :BK_NAME2, :PRNO, :PCrLic_No, :ChgCd, :APP_DATE, :' +
        'ITM_GBN, :ITM_GNAME, :EMAIL_ID, :EMAIL_DOMAIN, :BK_CD)')
    Left = 144
    Top = 224
  end
  object qryMod: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'DATEE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'CHK1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'CHK2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESSAGE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'MESSAGE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'APP_CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'APP_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'AX_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'AX_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'AX_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SUP_CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SUP_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SUP_ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ITM_G1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ITM_G2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ITM_G3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'TQTY'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'TQTYC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'TAMT1'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'TAMT1C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'TAMT2'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'TAMT2C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'CHG_G'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'C1_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'C1_C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'C2_AMT'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'BK_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'BK_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'PRNO'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'PCrLic_No'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ChgCd'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'APP_DATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'ITM_GBN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'ITM_GNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 70
        Value = Null
      end
      item
        Name = 'EMAIL_ID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EMAIL_DOMAIN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'BK_CD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 11
        Value = Null
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [APPPCR_H]'
      'SET  DATEE'#9#9#9'= :DATEE'
      #9',CHK1'#9#9#9'= :CHK1'
      #9',CHK2'#9#9#9'= :CHK2'
      #9',CHK3 '#9#9#9'= :CHK3 '
      #9',MESSAGE1'#9#9'= :MESSAGE1'
      #9',MESSAGE2 '#9#9'= :MESSAGE2 '
      #9',APP_CODE '#9#9'= :APP_CODE '
      #9',APP_NAME1 '#9#9'= :APP_NAME1 '
      #9',APP_NAME2 '#9#9'= :APP_NAME2 '
      #9',APP_NAME3 '#9#9'= :APP_NAME3 '
      #9',APP_ADDR1 '#9#9'= :APP_ADDR1 '
      #9',APP_ADDR2 '#9#9'= :APP_ADDR2 '
      #9',APP_ADDR3 '#9#9'= :APP_ADDR3 '
      #9',AX_NAME1 '#9#9'= :AX_NAME1 '
      #9',AX_NAME2 '#9#9'= :AX_NAME2 '
      #9',AX_NAME3 '#9#9'= :AX_NAME3 '
      #9',SUP_CODE '#9#9'= :SUP_CODE '
      #9',SUP_NAME1 '#9#9'= :SUP_NAME1 '
      #9',SUP_NAME2 '#9#9'= :SUP_NAME2 '
      #9',SUP_NAME3 '#9#9'= :SUP_NAME3 '
      #9',SUP_ADDR1 '#9#9'= :SUP_ADDR1 '
      #9',SUP_ADDR2 '#9#9'= :SUP_ADDR2 '
      #9',SUP_ADDR3 '#9#9'= :SUP_ADDR3 '
      #9',ITM_G1 '#9#9'= :ITM_G1 '
      #9',ITM_G2 '#9#9'= :ITM_G2 '
      #9',ITM_G3 '#9#9'= :ITM_G3 '
      #9',TQTY '#9#9#9'= :TQTY '
      #9',TQTYC '#9#9#9'= :TQTYC '
      #9',TAMT1 '#9#9#9'= :TAMT1 '
      #9',TAMT1C '#9#9'= :TAMT1C '
      #9',TAMT2 '#9#9#9'= :TAMT2 '
      #9',TAMT2C '#9#9'= :TAMT2C '
      #9',CHG_G '#9#9#9'= :CHG_G '
      #9',C1_AMT'#9#9#9'= :C1_AMT'
      #9',C1_C'#9#9#9'= :C1_C'
      #9',C2_AMT'#9#9#9'= :C2_AMT'
      #9',BK_NAME1'#9#9'= :BK_NAME1'
      #9',BK_NAME2'#9#9'= :BK_NAME2'
      #9',PRNO'#9#9#9'= :PRNO'
      #9',PCrLic_No'#9#9'= :PCrLic_No'
      #9',ChgCd'#9#9#9'= :ChgCd'
      #9',APP_DATE'#9#9'= :APP_DATE'
      #9',ITM_GBN'#9#9'= :ITM_GBN'
      #9',ITM_GNAME'#9#9'= :ITM_GNAME'
      #9',EMAIL_ID'#9#9'= :EMAIL_ID'
      #9',EMAIL_DOMAIN'#9'= :EMAIL_DOMAIN'
      #9',BK_CD'#9#9#9'= :BK_CD'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 144
    Top = 256
  end
  object qryDel: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE APPPCR_H '
      'SET CHK2 = 2'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 144
    Top = 288
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = A4
    Left = 312
    Top = 224
  end
  object qryCopy: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'NEW_DOCNO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35),'
      '       @NEW_DOCNO varchar(35)'
      'SET @MAINT_NO = :MAINT_NO'
      'SET @NEW_DOCNO = :NEW_DOCNO'
      ''
      'INSERT INTO APPPCR_TAX'
      
        'SELECT @NEW_DOCNO, SEQ, DATEE, BILL_NO, BILL_DATE, ITEM_NAME1, I' +
        'TEM_NAME2, ITEM_NAME3, ITEM_NAME4, ITEM_NAME5, ITEM_DESC, BILL_A' +
        'MOUNT, BILL_AMOUNT_UNIT, TAX_AMOUNT, TAX_AMOUNT_UNIT, QUANTITY, ' +
        'QUANTITY_UNIT'
      'FROM APPPCR_TAX'
      'WHERE KEYY = @MAINT_NO'
      ''
      'INSERT INTO APPPCRD1'
      
        'SELECT @NEW_DOCNO, SEQ, HS_NO, NAME, NAME1, SIZE, SIZE1, REMARK,' +
        ' QTY, QTYC, AMT1, AMT1C, AMT2, PRI1, PRI2, PRI_BASE, PRI_BASEC, ' +
        'ACHG_G, AC1_AMT, AC1_C, AC2_AMT, LOC_TYPE, NAMESS, SIZESS, APP_D' +
        'ATE, ITM_NUM'
      'FROM APPPCRD1'
      'WHERE KEYY = @MAINT_NO'
      ''
      'INSERT INTO APPPCRD2'
      
        'SELECT @NEW_DOCNO, SEQ, DOC_G, DOC_D, BHS_NO, BNAME, BNAME1, BAM' +
        'T, BAMTC, EXP_DATE, SHIP_DATE, BSIZE1, BAL_NAME1, BAL_NAME2, BAL' +
        '_CD, NAT'
      'FROM APPPCRD2'
      'WHERE KEYY = @MAINT_NO'
      ''
      'INSERT INTO APPPCR_H'
      
        'SELECT @NEW_DOCNO, [USER_ID], Replace(Convert(varchar(10),getdat' +
        'e(),121),'#39'-'#39','#39#39'), CHK1, 0, '#39#39', MESSAGE1, MESSAGE2, APP_CODE, APP' +
        '_NAME1, APP_NAME2, APP_NAME3, APP_ADDR1, APP_ADDR2, APP_ADDR3, A' +
        'X_NAME1, AX_NAME2, AX_NAME3, SUP_CODE, SUP_NAME1, SUP_NAME2, SUP' +
        '_NAME3, SUP_ADDR1, SUP_ADDR2, SUP_ADDR3, ITM_G1, ITM_G2, ITM_G3,' +
        ' TQTY, TQTYC, TAMT1, TAMT1C, TAMT2, TAMT2C, CHG_G, C1_AMT, C1_C,' +
        ' C2_AMT, BK_NAME1, BK_NAME2, PRNO, PCrLic_No, ChgCd, APP_DATE, D' +
        'OC_G, DOC_D, BHS_NO, BNAME, BNAME1, BAMT, BAMTC, EXP_DATE, SHIP_' +
        'DATE, BSIZE1, BAL_CD, BAL_NAME1, BAL_NAME2, F_INTERFACE, SRBUHO,' +
        ' ITM_GBN, ITM_GNAME, ISSUE_GBN, DOC_GBN, DOC_NO, BAMT2, BAMTC2, ' +
        'BAMT3, BAMTC3, PCRLIC_ISS_NO, FIN_CODE, FIN_NAME, FIN_NAME2, NEG' +
        'O_APPDT, EMAIL_ID, EMAIL_DOMAIN, BK_CD'
      'FROM APPPCR_H'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 104
    Top = 256
  end
  object qry_UpdateTempNo2NewNo: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'TEMP_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35),'
      '       @TEMP_NO varchar(35)'
      'SET @MAINT_NO = :MAINT_NO'
      'SET @TEMP_NO = :TEMP_NO'
      ''
      'UPDATE APPPCR_TAX SET KEYY = @MAINT_NO WHERE KEYY = @TEMP_NO'
      'UPDATE APPPCRD1 SET KEYY = @MAINT_NO WHERE KEYY = @TEMP_NO'
      'UPDATE APPPCRD2 SET KEYY = @MAINT_NO WHERE KEYY = @TEMP_NO'
      
        'UPDATE APPPCR_H SET MAINT_NO = @MAINT_NO WHERE MAINT_NO = @TEMP_' +
        'NO')
    Left = 104
    Top = 288
  end
  object qryCalcTotal: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT KEYY,'
      '       MAX(QTYC) as QTYC,'
      '       SUM(QTY) as TQTY,'
      '       MAX(AMT1C) as AMT1C,'
      '       SUM(AMT1) as TAMT1'
      '  FROM [APPPCRD1] with(nolock)'
      '  GROUP BY KEYY'
      '  HAVING [KEYY] = :KEY')
    Left = 144
    Top = 320
  end
  object qryMAX_SEQ: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 176
    Top = 224
  end
end
