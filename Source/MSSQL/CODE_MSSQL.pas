unit CODE_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CODE, sSkinProvider, Grids, DBGrids, acDBGrid, StdCtrls, sLabel,
  ExtCtrls, sSplitter, Buttons, sSpeedButton, sPanel, DB, ADODB, StrUtils,
  sBitBtn, sEdit, sComboBox, TypeDefine;

type
  TMasterType = (mtStandard,mtGeneral);
  TCODE_MSSQL_frm = class(TCODE_frm)
    qryMst: TADOQuery;
    dsMst: TDataSource;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    qryDetailCODE: TStringField;
    qryDetailMAP: TStringField;
    qryDetailNAME: TStringField;
    qryDetailRemark: TStringField;
    qryMstPrefix: TStringField;
    qryMstName: TStringField;
    qryMstGubun: TStringField;
    sSpeedButton13: TsSpeedButton;
    qryCheck: TADOQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    qryDetailPrefix: TStringField;
    sPanel8: TsPanel;
    sEdit2: TsEdit;
    sBitBtn2: TsBitBtn;
    procedure qryDetailRemarkGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure qryMstAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure qryMstAfterOpen(DataSet: TDataSet);
    procedure qryDetailAfterOpen(DataSet: TDataSet);
    procedure qryDetailAfterScroll(DataSet: TDataSet);
    procedure Btn_DetailCheckClick(Sender: TObject);
    procedure sDBGrid2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Btn_NewClick(Sender: TObject);
    procedure Btn_DelClick(Sender: TObject);
    procedure Btn_DetailDelClick(Sender: TObject);
    procedure sDBGrid2DblClick(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
    procedure sEdit2Change(Sender: TObject);
  private
    { Private declarations }
    FMasterSQL : String;
    FDetailSQL : String;
    FMasterType : TMasterType;
    procedure ReadDetail(Prefix : String);
    procedure ReadMaster(GUBUN : TMasterType);
    function DelData(Prefix, Code : String):Boolean;
    procedure DataUsed(bCheck : Boolean);
    procedure doRefresh(qry : TADOQuery);
  public
    { Public declarations }
    function Run(preFix : String):TModalResult;

  end;

var
  CODE_MSSQL_frm: TCODE_MSSQL_frm;

implementation

uses ICON, MSSQL, Dlg_CODE, Commonlib, Dialog_CodeList, dlg_FindCity;

{$R *.dfm}

procedure TCODE_MSSQL_frm.qryDetailRemarkGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;

  IF AnsiMatchText(Trim(qryDetailRemark.AsString),['0','1']) Then
  begin
    Case AnsiIndexText(Trim(qryDetailRemark.AsString),['0','1']) of
      0: Text := '';
      1: Text := '◎';
    end;
  end
  else
  begin
    Text := '';
  end;

end;

procedure TCODE_MSSQL_frm.ReadDetail(Prefix: String);
begin
  with qryDetail do
  begin
    Close;
    Parameters.ParamByName('Prefix').Value := Prefix;
    Open;
  end;
end;

procedure TCODE_MSSQL_frm.ReadMaster(GUBUN: TMasterType);
begin
  with qryMst do
  begin
    Close;
    SQl.Text := FMasterSQL;
    Case sComboBox1.ItemIndex of
      0: SQL.Add('AND prefix LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
      1: SQL.Add('AND [Name] LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Case GUBUN of
      mtStandard : Parameters.ParamByName('GUBUN').Value := '표준';
      mtGeneral : Parameters.ParamByName('GUBUN').Value := '일반';
    end;
    Open;
  end;
end;

procedure TCODE_MSSQL_frm.sSpeedButton2Click(Sender: TObject);
begin
  inherited;
  Case (Sender as TsSpeedButton).Tag of
    0: FMasterType := mtStandard;
    1: FMasterType := mtGeneral;
  end;

  ReadMaster(FMasterType);
end;

procedure TCODE_MSSQL_frm.qryMstAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadDetail(qryMstPrefix.AsString);
end;

procedure TCODE_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  FMasterSQL := qryMst.SQL.Text;
  FDetailSQL := qryDetail.SQL.Text;

  if sSpeedButton2.Down = True then
    ReadMaster(mtStandard)
  else if sSpeedButton6.Down = True then
    ReadMaster(mtGeneral);


end;

procedure TCODE_MSSQL_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadMaster(FMasterType);
end;

procedure TCODE_MSSQL_frm.qryMstAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Btn_New.Enabled := (FMasterType = mtGeneral) AND (qryMst.RecordCount >0);
  Btn_Modify.Enabled := (FMasterType = mtGeneral) AND (qryMst.RecordCount >0);
  Btn_Del.Enabled := (FMasterType = mtGeneral) AND (qryMst.RecordCount >0);

  IF qryMst.RecordCount = 0 then
  ReadDetail(qryMstPrefix.AsString);
end;

procedure TCODE_MSSQL_frm.qryDetailAfterOpen(DataSet: TDataSet);
begin
  inherited;
//  Btn_DetailNew.Enabled := qryDetail.RecordCount > 1;
  Btn_DetailMod.Enabled := qryDetail.RecordCount > 1;
  Btn_DetailDel.Enabled := qryDetail.RecordCount > 1;
end;

procedure TCODE_MSSQL_frm.qryDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  Btn_DetailCheck.Enabled   := (qryDetail.RecordCount > 1) AND (qryDetailRemark.AsString <> '1');
  Btn_DetailNoCheck.Enabled := (qryDetail.RecordCount > 1) AND (qryDetailRemark.AsString = '1');
end;

procedure TCODE_MSSQL_frm.DataUsed(bCheck: Boolean);
var
  BMK : TBookmark;
begin
  BMK := qryDetail.GetBookmark;

  with qryCheck do
  begin
    Close;
    IF bCheck Then
      Parameters.ParamByName('Remark').Value := '1'
    else
      Parameters.ParamByName('Remark').Value := '0';

    Parameters.ParamByName('Prefix').Value := qryDetailPrefix.AsString;
    Parameters.ParamByName('CODE').Value := qryDetailCODE.AsString;
    ExecSQL;
  end;

  ReadDetail(qryMstPrefix.AsString);

  IF qryDetail.BookmarkValid(BMK) Then
    qryDetail.GotoBookmark(BMK);

  qryDetail.FreeBookmark(BMK);
end;

procedure TCODE_MSSQL_frm.Btn_DetailCheckClick(Sender: TObject);
begin
  inherited;
  DataUsed((Sender as TsSpeedButton).Tag = 3);
end;

procedure TCODE_MSSQL_frm.sDBGrid2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Key = VK_SPACE THEN
  Case AnsiIndexText(Trim(qryDetailRemark.AsString),['','0','1']) of
    0,1 : DataUsed(True);
    2:    DataUsed(False);
  end;

end;

procedure TCODE_MSSQL_frm.Btn_NewClick(Sender: TObject);
var
  FGUBUN : TGroupType;
  UserResult : TModalResult;
begin
  inherited;
  Dlg_CODE_frm := TDlg_CODE_frm.Create(Self);
  try
    Case (Sender as TsSpeedButton).Tag of
      0..2 : FGUBUN := gtGubun;
      3..5 : FGUBUN := gtCode;
    end;

    Case (Sender as TsSpeedButton).Tag of
      0: UserResult := Dlg_CODE_frm.Run(gtGubun,ctInsert,qryMst.Fields,qryDetail.Fields);
      1: UserResult := Dlg_CODE_frm.Run(gtGubun,ctModify,qryMst.Fields,qryDetail.Fields);
      2: ;
      3: UserResult := Dlg_CODE_frm.Run(gtCode,ctInsert,qryMst.Fields,qryDetail.Fields);
      4: UserResult := Dlg_CODE_frm.Run(gtCode,ctModify,qryMst.Fields,qryDetail.Fields);
      5: ;
    end;

    IF UserResult = mrOk Then
    begin
      Case FGUBUN of
        gtGubun :
        begin
          doRefresh(qryMst);
        end;
        gtCode :
        begin
          doRefresh(qryDetail);
        end;
      end;
    end;
  finally
    FreeAndNil(Dlg_CODE_frm);
  end;
end;

procedure TCODE_MSSQL_frm.doRefresh(qry: TADOQuery);
var
  BMK : TBookmark;
begin
  BMK := qry.GetBookmark;

  qry.Close;
  qry.Open;

  IF qry.BookmarkValid(BMK) Then
    qry.GotoBookmark(BMK);

  qry.FreeBookmark(BMK);
end;

function TCODE_MSSQL_frm.DelData(Prefix, Code: String): Boolean;
var
  DelQuery : TADOQuery;
begin
  DelQuery := TADOQuery.Create(Self);
  DelQuery.Connection := DMMssql.KISConnect;
  try
    try
    with DelQuery do
    begin
      //대분류삭제
      IF Code = '' Then
      begin
        SQL.Text := 'DELETE FROM [CODE2NDM] WHERE [Prefix] = '+QuotedStr(Prefix);
      end
      //대분류 하위삭제
      else if (Prefix <> '') AND (Code = 'ALL') Then
      begin
        SQL.Text := 'DELETE FROM [CODE2NDD] WHERE [Prefix] = '+QuotedStr(Prefix);
      end
      //소분류삭제
      else if (Prefix <> '') AND (Code <> '') Then
      begin
        SQL.Text := 'DELETE FROM [CODE2NDD] WHERE [Prefix] = '+QuotedStr(Prefix)+' AND [CODE] = '+QuotedStr(Code);
      end
      else
      begin
        Raise Exception.Create('Error!'#13#10'[Prefix] : '+Prefix+#13#10'[CODE] : '+Code);
      end;
      ExecSQL;
    end;
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    DelQuery.Free;
  end;
end;

procedure TCODE_MSSQL_frm.Btn_DelClick(Sender: TObject);
begin
  inherited;
  IF WarningMessage('해당 코드를 삭제하시겠습니까?'#13#13'분류코드를 삭제하게 되면 입력되어있는 상세 데이터들도 모두 삭제 되며, 복구가 불가능합니다.'#13#10'[삭제예정 코드] : '+qryMstPrefix.AsString) Then
  begin
    DelData(qryMstPrefix.AsString,'ALL');
    DelData(qryMstPrefix.AsString,'');
    doRefresh(qryMst);
  end;
end;

procedure TCODE_MSSQL_frm.Btn_DetailDelClick(Sender: TObject);
begin
  inherited;
  IF WarningMessage('해당 코드를 삭제하시겠습니까?'#13#10+'[삭제 예정 코드] : '+qryDetailCODE.AsString) Then
  begin
    DelData(qryDetailPrefix.AsString,qryDetailCODE.AsString);
    doRefresh(qryDetail);
  end;
end;

procedure TCODE_MSSQL_frm.sDBGrid2DblClick(Sender: TObject);
begin
  inherited;
  Case AnsiIndexText(Trim(qryDetailRemark.AsString),['','0','1']) of
    0,1 : DataUsed(True);
    2:    DataUsed(False);
  end;

end;

procedure TCODE_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
   ModalResult := mrOk;
end;

procedure TCODE_MSSQL_frm.sEdit2Change(Sender: TObject);
begin
  inherited;
  sDBGrid2.DataSource.DataSet.Locate('CODE',sEdit2.Text,[loPartialKey]);
end;

function TCODE_MSSQL_frm.Run(preFix: String): TModalResult;
var
  TempStr : String;
begin

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT Gubun FROM CODE2NDM WHERE Prefix = '+QuotedStr(preFix);
      Open;

      TempStr := FieldByName('Gubun').AsString;
    finally
      Close;
      Free;
    end;
  end;

  sEdit1.Text := preFix;
  FMasterSQL := qryMst.SQL.Text;

  IF TempStr = '표준' Then
  begin
    ReadMaster(mtStandard);
    sSpeedButton2.Down := True;
  end
  else
  begin
    ReadMaster(mtGeneral);
    sSpeedButton6.Down := True;
  end;
  qryMst.Close;
  qryMst.Open;
  qryMst.Locate('Prefix',preFix,[]);

  Result := Self.ShowModal;

end;

end.

