unit DocumentSend;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sLabel, ExtCtrls, sSplitter,
  Buttons, sSpeedButton, sPanel, DB, ADODB, Grids, DBGrids, acDBGrid, StrUtils, Clipbrd;

type
  TDocumentSend_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton3: TsSpeedButton;
    sSplitter2: TsSplitter;
    Btn_Del: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton6: TsSpeedButton;
    qryReadyList: TADOQuery;
    dsReadyList: TDataSource;
    qryReadyListDOCID: TStringField;
    qryReadyListMAINT_NO: TStringField;
    qryReadyListMSEQ: TIntegerField;
    qryReadyListSRDATE: TStringField;
    qryReadyListSRTIME: TStringField;
    qryReadyListSRVENDOR: TStringField;
    qryReadyListSRSTATE: TStringField;
    qryReadyListSRCONTROL: TStringField;
    qryReadyListSRFLAT: TMemoField;
    qryReadyListTag: TBooleanField;
    qryReadyListTableName: TStringField;
    qryReadyListReadyUser: TStringField;
    qryReadyListReadyDept: TStringField;
    qryReadyDel: TADOQuery;
    sDBGrid1: TsDBGrid;
    sPanel2: TsPanel;
    sLabel2: TsLabel;
    lbl_Step1: TsLabel;
    lbl_Step3: TsLabel;
    lbl_Step2: TsLabel;
    lbl_Step4: TsLabel;
    qryDelAll: TADOQuery;
    qryUpdateState: TADOQuery;
    qryCompleteList: TADOQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    IntegerField1: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    MemoField1: TMemoField;
    BooleanField1: TBooleanField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    sSpeedButton7: TsSpeedButton;
    procedure qryReadyListSRDATEGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure Btn_DelClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure qryReadyListTagGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryReadyListAfterScroll(DataSet: TDataSet);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure sSpeedButton7Click(Sender: TObject);
  private
    { Private declarations }
    FUPLOADFILE : String;
    procedure CheckInTransaction;

    procedure ReadList;
    procedure DocumentDel;
    procedure DocumentDelAll;
    procedure ReturnData(MAINT_NO, TableName : String);
    procedure CompleteData;
    procedure SendDocuments;
    procedure Step(CurrIDX :Integer);
    procedure CreateUNHFiles;
    procedure WriteSendHistory(sTableName,sMAINT_NO : String);
    procedure DelAllList;
  public
    { Public declarations }
  end;

var
  DocumentSend_frm: TDocumentSend_frm;

const
  MSG_ERROR : string = '변환오류';
  MSG_OK : string = '변환완료';  
implementation

uses ICON, MSSQL, Commonlib, VarDefine, MessageDefine, dlg_ErrView;

{$R *.dfm}

procedure TDocumentSend_frm.DocumentDel;
begin
  DMMssql.BeginTrans;

  try
    with qryReadyDel do
    begin
      Close;
      Parameters.ParamByName('MAINT_NO').Value := qryReadyListMAINT_NO.AsString;
      Parameters.ParamByName('DOCID').Value := qryReadyListDOCID.AsString;
      ExecSQL;
    end;

    ReturnData(qryReadyListMAINT_NO.AsString,qryReadyListTableName.AsString);
    DMMssql.CommitTrans;
         
    ReadList;

  except
    on E:Exception do
    begin
      DMMssql.RollbackTrans;
      ShowMessage(E.Message);
    end;
  end;

end;

procedure TDocumentSend_frm.qryReadyListSRDATEGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  sDate : String;
begin
  inherited;
  sDate := qryReadyListSRDATE.AsString;

  sDate := qryReadyListSRDATE.AsString;
  IF sDate = '' Then Exit;
  Text := FormatDateTime('YYYY.MM.DD',decodeCharDate(LeftStr(sDate,8)));
end;

//------------------------------------------------------------------------------
// chK3 맨뒤에 붙는 값으로 DBGrid에 표시되는 내용이 달라진다.
//------------------------------------------------------------------------------

procedure TDocumentSend_frm.ReturnData(MAINT_NO, TableName: String);
begin
//------------------------------------------------------------------------------
// 삭제시 보낼 테이블 준비취소
//------------------------------------------------------------------------------
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      //삭제시 CHK2에 값에 6을 넣고 , CHK3뒤에 송신날짜+2를 붙임
      SQL.Text := 'UPDATE '+TableName+' SET CHK2 = '+QuotedStr('6')+', CHK3 = '+QuotedStr(FormatDateTime('YYYYMMDD',Now) + '2')+#13+
                  'WHERE MAINT_NO = '+QuotedStr(MAINT_NO);
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TDocumentSend_frm.CompleteData;
var
  sCHK2, sCHK3 : String;
begin
//------------------------------------------------------------------------------
// 전송완료시 원본테이블 스텝수정
//------------------------------------------------------------------------------
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      //전송완료(변환완료)시 CHK2에 값에 7을 넣고 , CHK3뒤에 송신날짜+5를 붙임
      qryCompleteList.Close;
      qryCompleteList.Parameters[0].Value := LoginData.sID;
      qryCompleteList.Open;

      while not qryCompleteList.Eof do
      begin
        IF qryCompleteList.FieldByName('SRSTATE').AsString = '변환완료' Then
        begin
          sCHK2 := '7';
          sCHK3 := '5';
        end
        else
        IF qryCompleteList.FieldByName('SRSTATE').AsString = '전송실패' Then
        begin
          sCHK2 := '8';
          sCHK3 := '4';
        end
        else
        IF qryCompleteList.FieldByName('SRSTATE').AsString = MSG_ERROR Then
        begin
          sCHK2 := '8';
          sCHK3 := '3';
        end
        else
        begin
          sCHK2 := '8';
          sCHK3 := '9';
        end;


        IF qryCompleteList.FieldByName('DOCID').AsString = 'APP700' Then
        begin
          SQL.Text := 'UPDATE '+qryCompleteList.FieldByName('TableName').AsString+' SET CHK2 = '+QuotedStr(sCHK2)+', CHK3 = LEFT(CHK3,8)+'+QuotedStr(sCHK3)+
                      #13#10' WHERE MAINT_NO = '+QuotedStr(qryCompleteList.FieldByName('MAINT_NO').AsString)+' AND MSEQ = '+qryCompleteList.FieldByName('MSEQ').AsString;
        end
        else
        IF qryCompleteList.FieldByName('DOCID').AsString = 'APP707' Then
        begin
          SQL.Text := 'UPDATE '+qryCompleteList.FieldByName('TableName').AsString+' SET CHK2 = '+QuotedStr(sCHK2)+', CHK3 = LEFT(CHK3,8)+'+QuotedStr(sCHK3)+
                      #13#10' WHERE MAINT_NO = '+QuotedStr(qryCompleteList.FieldByName('MAINT_NO').AsString)+' AND MSEQ = '+qryCompleteList.FieldByName('MSEQ').AsString+' AND AMD_NO = '+qryCompleteList.FieldByName('SRCONTROL').AsString;
        end
        else
        IF qryCompleteList.FieldByName('DOCID').AsString = 'LOCAMR' Then
        begin
          SQL.Text := 'UPDATE '+qryCompleteList.FieldByName('TableName').AsString+' SET CHK2 = '+QuotedStr(sCHK2)+', CHK3 = LEFT(CHK3,8)+'+QuotedStr(sCHK3)+
                      #13#10' WHERE MAINT_NO = '+QuotedStr(qryCompleteList.FieldByName('MAINT_NO').AsString)+' AND MSEQ = '+qryCompleteList.FieldByName('MSEQ').AsString+' AND AMD_NO = '+qryCompleteList.FieldByName('SRCONTROL').AsString;
        end
        else
        begin
          // LOCAPP / APPLOG
          SQL.Text := 'UPDATE '+qryCompleteList.FieldByName('TableName').AsString+' SET CHK2 = '+QuotedStr(sCHK2)+', CHK3 = LEFT(CHK3,8)+'+QuotedStr(sCHK3)+
                      #13#10' WHERE MAINT_NO = '+QuotedStr(qryCompleteList.FieldByName('MAINT_NO').AsString);
        end;

        IF Trim(SQL.Text) <> '' Then
        begin
          ExecSQL;
        end;
        qryCompleteList.Next;
      end;

      //------------------------------------------------------------------------------
      // 전송완료항목은 삭제
      //------------------------------------------------------------------------------
      SQL.Text := 'DELETE FROM SR_READY WHERE SRSTATE = ''변환완료'' AND ReadyUser = '+QuotedStr(LoginData.sID);
      Execsql;
      ReadList;

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TDocumentSend_frm.Btn_DelClick(Sender: TObject);
begin
  inherited;
  IF qryReadyList.RecordCount = 0 Then Exit;
  DocumentDel;
end;

procedure TDocumentSend_frm.ReadList;
begin
  qryReadyList.Close;
  qryReadyList.Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
  qryReadyList.Open;
end;

procedure TDocumentSend_frm.FormActivate(Sender: TObject);
var
  BMK : Pointer;
begin
  inherited;
  BMK := qryReadyList.GetBookmark;
  ReadList;
  try
  IF qryReadyList.BookmarkValid(BMK) Then
    qryReadyList.GotoBookmark(BMK);
  finally
    qryReadyList.FreeBookmark(BMK);
  end;
end;

procedure TDocumentSend_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TDocumentSend_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  DocumentSend_frm := nil;
end;

Const
  EXISTS_UNH_FILES = 'C:\WINMATE\Flatout\*.unh';
procedure TDocumentSend_frm.sSpeedButton1Click(Sender: TObject);
begin
  inherited;
  IF not ConfirmMessage('선택한 문서를 전송하시겠습니까?') Then Exit;

  sPanel2.Visible := True;

  //선택된 파일들만 UNH파일을 만듦
  // 예정
  Step(1);
  CreateUNHFiles;

  //FLATOUT폴더안에 UNH파일이 있는지 검사함 없으면 전송할 항목이 없는것
  IF FileExists(EXISTS_UNH_FILES) Then
  Begin
    SendDocuments;
  end
  Else
  Begin
    IF FileExists(WINMATE_OUT + 'UPLOAD.xxx') THEN
      SendDocuments
    else
    begin
      sPanel2.Visible := False;    
      Raise Exception.Create('전송 할 문서가 없습니다');
    end;
  end;
end;

procedure TDocumentSend_frm.SendDocuments;
var
  UFile, DOCID, MAINT_NO, MSEQ, AMDNO, sCHECK : string;
  SENDLIST : TStringList;
  i : Integer;
  TMP_STR : string;

begin

  UFile := WINMATE_OUT + 'UPLOAD.xxx';
  ForceDirectories(WINMATE_OUT);

  // C:\WINMATE\OUT\UPLOAD.xxx 파일존재 여부 확인후 있으면 삭제
  IF FileExists(UFile) THEN
    DeleteFile(UFile);

  //SNDLIST
  UFile := WINMATE_FLATOUT+'SEND.LST';
  IF FileExists(UFile) Then
  DeleteFile(UFile);

  //UNH파일들을 WINMATE TXOUT.exe를 이용하여 변환
  Step(2);
  WinExecAndWait32(WINMATE_CONVERTEXE_OUT,SW_NORMAL);

  IF not FileExists(UFile) Then
  begin
    ShowMessage('전송보낼 문서가 없습니다'#13#10'변환오류를 확인해주세요');
    Exit;
  end
  else
  begin
    SENDLIST := TStringList.Create;
    try
      SENDLIST.LoadFromFile(UFile);
      //변환됐는지 확인
      for i:= 0 to SENDLIST.Count-1 do
      begin
        DOCID := Trim(MidStr(SENDLIST.Strings[i],37,7));
        MAINT_NO := AnsiReplaceText(Trim(MidStr(SENDLIST.Strings[i],52,51)),DOCID,'');
        MSEQ := '0';
        AMDNO := '';
        //------------------------------------------------------------------------------
        // 내국신용장의 관리번호는 고정
        //------------------------------------------------------------------------------
        IF DOCID = 'LOCAPP' Then MAINT_NO := 'LOCAPP'+MAINT_NO;
        //------------------------------------------------------------------------------
        // 내국신용장 조건변경 신청서 는 맨뒤에 MSEQ가 있다
        //------------------------------------------------------------------------------
        IF DOCID = 'LOCAMR' Then
        begin
          TMP_STR := RightStr(MAINT_NO,4);
          // 1-41
          MSEQ := MidStr(TMP_STR, Pos('-',TMP_STR)+1,4-Pos('-',TMP_STR));
        end;
        //------------------------------------------------------------------------------
        // 수입LC의 순번은 UNH안에 포함 번호_차수
        //------------------------------------------------------------------------------
//        IF DOCID = 'APP700' then MSEQ := '1';
        IF DOCID = 'APP700' then
        begin
          MSEQ := MidStr(MAINT_NO, Pos('_', MAINT_NO)+1, Length(MAINT_NO)-Pos('_', MAINT_NO));
          MAINT_NO := LeftStr(MAINT_NO, Pos('_', MAINT_NO)-1);
        end;
        IF DOCID = 'APP707' then
        begin
          //문서번호_차수_정정차수
          AMDNO := RightStr(MAINT_NO, Length(MAINT_NO)-(POS('__', MAINT_NO)+1));
          MSEQ  := MidStr(MAINT_NO, Pos('_', MAINT_NO)+1, Pos('__', MAINT_NO)-Pos('_', MAINT_NO)-1);
          MAINT_NO := LeftStr(MAINT_NO, Pos('_', MAINT_NO)-1);
        end;
        //------------------------------------------------------------------------------
        // 내국신용장 물품수령 증명서 DOCID 변환
        //------------------------------------------------------------------------------
        IF DOCID = 'LOCRCT' Then
        begin
          DOCID := 'LOCRC2';
          MAINT_NO := AnsiReplaceText(MAINT_NO, 'LOCRC2', 'LOCRCT');
        end;

        //변환여부 체크 Y, N
        sCHECK := RightStr(SENDLIST.Strings[i],1);

        with qryUpdateState do
        begin
          Close;
          Parameters.ParamByName('DOCID'   ).Value := DOCID;
          Parameters.ParamByName('MAINT_NO').Value := MAINT_NO;
          Parameters.ParamByName('MSEQ').Value := StrToIntDef(MSEQ, 0);
          Parameters.ParamByName('AMDNO').Value := AMDNO;

          IF sCHECK  = 'N' Then
            Parameters.ParamByName('STATE_MESSAGE').Value := MSG_ERROR
          else
            Parameters.ParamByName('STATE_MESSAGE').Value := MSG_OK;

          ExecSQL;
        end;
      end;

      ReadList;

//      IF sCHECK = 'N' Then
//      begin
//        MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+'['+DOCID+']'#13#10+MAINT_NO+#13#10+MSG_SYSTEM_LINE+MSG_SYSTEM_CONVERT_ERR),'변환오류',MB_OK+MB_ICONERROR);
//        exit;
//      end;

    finally
      SENDLIST.Free;
    end;
  end;


  // C:\WINMATE\OUT\UPLOAD.xxx 파일이 생성(전송보낼 문서 정보)
  //UPLOAD.xxx파일 백업
  ////UPLOADS폴더생성
  UFile := WINMATE_OUT + 'UPLOAD.xxx';
  ForceDirectories(WINMATE_UPLOADS);
  FUPLOADFILE := FormatDateTime('YYYYMMDDHHNNSS',Now)+'.xxx';
  CopyFile(PChar(UFile),Pchar(WINMATE_UPLOADS+FUPLOADFILE),True);

  Step(3);

  // Hermes.exe -m2를 이용하여 전송
  Step(4);

//  UFile := WINMATE_OUT + 'UPLOAD.xxx';
  IF FileExists(UFile) then
  begin
     WinExecAndWait32(WINMATE_SENDEXE,SW_NORMAL);

    IF FileExists(UFile) then
    begin
      //체크된 항목들 전부 전송실패로 변환
      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := 'UPDATE SR_READY SET SRSTATE = ''전송실패'' WHERE Tag = 1 AND ReadyUser = '+QuotedStr(LoginData.sID)+' AND SRSTATE != '+QuotedStr(MSG_ERROR);
          ExecSQL;
          //UPLOAD.xxx파일이 남아있으면 전송이 실패한것
          MessageBox(Self.Handle,MSG_SYSTEM_SEND_FAIL1,'전송실패',MB_OK+MB_ICONERROR);
        finally
          ReadList;
          CompleteData;          
          Close;
          Free;
        end;
      end;

    end
    else
    begin
      //각 항목 전송 완료처리
      CompleteData;
      MessageBox(Self.Handle,MSG_SYSTEM_SEND_OK,'전송완료',MB_OK+MB_ICONINFORMATION);
    end;
  end
  else
  begin
    //체크된 항목들 전부 전송실패로 변환
    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := 'UPDATE SR_READY SET SRSTATE = ''전송실패'' WHERE Tag = 1 AND ReadyUser = '+QuotedStr(LoginData.sID)+' AND SRSTATE != '+QuotedStr(MSG_ERROR);
        ExecSQL;
        //UPLOAD.xxx파일이 남아있으면 전송이 실패한것
        MessageBox(Self.Handle,MSG_SYSTEM_SEND_FAIL2,'전송실패',MB_OK+MB_ICONERROR);
      finally
        ReadList;
        CompleteData;          
        Close;
        Free;
      end;
    end;
  end;

  sPanel2.Visible := False;
end;

procedure TDocumentSend_frm.Step(CurrIDX: Integer);
var
  nLoop : Integer;
begin
  for nLoop := 1 to 4 do
  begin
    IF nLoop = CurrIDX Then
      (FindComponent('lbl_Step'+IntToStr(nLoop)) as TsLabel).Font.Style := [fsBold]
    else
      (FindComponent('lbl_Step'+IntToStr(nLoop)) as TsLabel).Font.Style := [];
  end;
end;

procedure TDocumentSend_frm.CreateUNHFiles;
var
  TempFLAT : TStringList;
  FILENAME : STring;
  TempDate : TDateTime;
begin
  TempFLAT := TStringList.Create;
  try
    qryReadyList.First;
    while not qryReadyList.Eof do
    begin
      IF not qryReadyListTag.AsBoolean Then
      begin
        qryReadyList.Next;
        Continue;
      end;

      IF qryReadyListDOCID.AsString = 'APP700' Then
        FILENAME := qryReadyListDOCID.AsString + qryReadyListMAINT_NO.AsString+'_'+qryReadyListMSEQ.AsString+'.UNH'
      else IF qryReadyListDOCID.AsString = 'APP707' Then
        FILENAME := qryReadyListDOCID.AsString + qryReadyListMAINT_NO.AsString+'_'+qryReadyListMSEQ.AsString+'__'+qryReadyListSRCONTROL.AsString+'.UNH'
      else
        FILENAME := qryReadyListDOCID.AsString + qryReadyListMAINT_NO.AsString +'.UNH';
        
      TempFLAT.Text := qryReadyListSRFLAT.AsString;
      TempFLAT.SaveToFile(WINMATE_FLATOUT+FILENAME);

      qryReadyList.Next;
    end;
  finally
    TempFLAT.Free;
  end;
end;


procedure TDocumentSend_frm.DelAllList;
begin
  
end;

procedure TDocumentSend_frm.WriteSendHistory(sTableName,
  sMAINT_NO: String);
begin

end;

procedure TDocumentSend_frm.DocumentDelAll;
begin
  with qryDelAll do
  begin
    Close;
    Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
    ExecSQL;
  end;
  ReadList;
end;

procedure TDocumentSend_frm.CheckInTransaction;
begin
  IF DMMssql.KISConnect.InTransaction Then
  begin
    ShowMessage('현재 다른 문서를 작성중이라면 해당 문서를 완료 후 다시 진행해주세요');
  end;
end;

procedure TDocumentSend_frm.sSpeedButton4Click(Sender: TObject);
begin
  inherited;
  CheckInTransaction;

  with qryReadyList do
  begin
    Edit;
    qryReadyListTag.AsBoolean := not qryReadyListTag.AsBoolean;
    Post;
    qryReadyListAfterScroll(qryReadyList);
  end;
end;

procedure TDocumentSend_frm.qryReadyListTagGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  IF qryReadyListTag.AsBoolean then
    Text := 'ν'
  else
    Text := '';
end;

procedure TDocumentSend_frm.qryReadyListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  IF qryReadyListTag.AsBoolean Then
  begin
    sSpeedButton4.Caption := '해제';
    sSpeedButton4.ImageIndex := 18;
  end
  else
  begin
    sSpeedButton4.Caption := '선택';
    sSpeedButton4.ImageIndex := 17
  end;

end;

procedure TDocumentSend_frm.sDBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Key = VK_SPACE Then
    sSpeedButton4Click(sSpeedButton4);
end;


procedure TDocumentSend_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  {
  IF qryReadyListTag.AsBoolean Then
  begin
    sDBGrid1.Canvas.Font.Style := [fsBold];
  end
  else
  begin
    sDBGrid1.Canvas.Font.Style := [];
  end;

  sDBGrid1.Canvas.FillRect(Rect);
  sDBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  }
end;

procedure TDocumentSend_frm.sSpeedButton7Click(Sender: TObject);
begin
  inherited;
  dlg_ErrView_frm := Tdlg_ErrView_frm.Create(Self);
  try
    dlg_ErrView_frm.ShowModal;
  finally
    FreeAndNil( dlg_ErrView_frm );
  end;
end;

end.
