//------------------------------------------------------------------------------
// 엑셀 생성 라이브러리(COM-OLE)
// 작성자 이덕수
//------------------------------------------------------------------------------
// 변경이력
// 2017-03-08
// VarArray를 이용한 데이터입력 추가
//------------------------------------------------------------------------------
// 2017-05-16
// 셀 라인입력 추가
//------------------------------------------------------------------------------
// 2017-05-26
// ColumnWidth 함수의 Value 타입 Integer > Byte로 변경
//------------------------------------------------------------------------------
// 2017-06-29
// RowHeight 함수의 오류 수정
//------------------------------------------------------------------------------
// 2017-07-12
// CellBorder 숫자파라미터 메소드 추가
// CellCenterAcross 함수추가(선택한 영역 중간으로)
//------------------------------------------------------------------------------
// 2017-07-14
// SelectWorkSheet 수정
//------------------------------------------------------------------------------
// 2018-04-27
// CellColor Range 추가
// CellAlign Range 추가
// 색상은 RGB(R,G,B)로 넣어도 됨
//------------------------------------------------------------------------------
unit NewExcelWriter;

interface

uses
  ComObj, SysUtils, StrUtils, DateUtils, Variants, sDialogs, ShellAPI, Windows,OleCtrls, Dialogs, Graphics;

Const
  xlNone  = -4142;  

  // SheetType
  xlChart = -4109;
  xlWorksheet = -4167;
  // WBATemplate
  xlWBATWorksheet = -4167;
  xlWBATChart = -4109;
  // Page Setup
  xlPortrait = 1;
  xlLandscape = 2;
  xlPaperA4 = 9;
  // Format Cells
  xlBottom = -4107;
  xlLeft = -4131;
  xlRight = -4152;
  xlTop = -4160;
  xlCenter = -4108;
  // Text Alignment
  xlHAlignCenter = -4108;
  xlVAlignCenter = -4108;

  xlThin   = 2;         //----선두께 (Normal)
  xlMedium = -4138;
  xlThick  = 4;

  xlContinuous    = 1;           //+선 스타일 (Normal)
  xlDash          = -4115;       //+-
  xlDashDot       = 4;           //+-
  xlDashDotDot    = 5;           //+-
  xlDot           = -4118;       //+- 점선
  xlDouble        = -4119;       //+-
  xlSlantDashDot  = 13;          //+-
  xlLineStyleNone = -4142;       //+-

  xlEdgeBottom = 9;
  xlEdgeLeft   = 7;
  xlEdgeRight  = 10;
  xlEdgeTop    = 8;

  xlCenterAcrossSelection = 7;

  xlContext = -5002;
Const
  FM_B_COMMA : String = '_-* #,##0_-;-* #,##0_-;_-* ""-""_-;_-@_-';
  FM_3_COMMA : String = '_-* #,##0.000_-;-* #,##0.000_-;_-* ""-""_-;_-@_-';
type
  TAlignment = (alLeft, alRight, alTop, alBottom);
  TAlignments = Set of TAlignment;

  TLineOption  = (loTop,
                  lobottom,
                  loLeft,
                  loRight,
                  loAll,
                  loTopBottom,
                  loEdge);      //테두리만
  TLineOptions = set of TLineOption;

  TTextAlignment = (taLeft, taCenter, taRight);

  TTextCenterAlignment = (Horizon,Vertical);
  TTextCenterAlignments = set of TTextCenterAlignment;

  TExcelWriter = class
    private
      FXLS : OleVariant;
      FWB : OleVariant;
      FSHEET : OleVariant;
      FRange : OleVariant;
      FSavePath : String;
      FFileName : String;
      FSheetName : String;
      FRowData : Variant;
      function ExcelChar2Number(sChar: String): Integer;
      function ExcelNumber2Char(nNumber: integer): String;
      function getRowData(index: Integer): Variant;
      procedure setRowdata(index: Integer; const Value: Variant);
      procedure ClearData;
    public
      constructor Create;
      destructor Destroy;

      procedure ShowExcel; overload;
      procedure Save(sFileName : String); overload;
      procedure SaveWithDialog;
      procedure Cells(COL,ROW : Integer; Value : string;Align : TTextAlignment=taLeft);
      procedure CellAlign(COL,ROW : Integer; Align : TTextAlignment=taLeft); overload;
      procedure CellAlign(Range : String; Align : TTextAlignment=taLeft); overload;
      procedure CellsLeftIndent(Col,Row : Integer; value : String ; indentLevel : Byte=1);
      procedure CellColor(Col,Row : Integer; Value : TColor); overload;
      procedure CellColor(Range : String; value: TColor); overload;
      procedure CellBorder(sRange : String; Lineoption : TLineOptions; Thick : Integer); overload;
      procedure CellBorder(FromCol,FromRow,ToCol,ToRow: Integer; Lineoption : TLineOptions; Thick : Integer); overload;
      procedure CellFormat(StartRange,EndRange : String; Format : String);
      procedure CellCenterAcross(FromCol,FromRow,ToCol,ToRow : integer);

      procedure FontBold(StartRange,EndRange : String ; Value : Boolean = True);
      procedure FontSize(StartRange,EndRange : String ; Value : integer = 10); overload;
      procedure FontSize(FromCol,FromRow,ToCol,ToRow : integer; Value : Integer=10); overload;
      procedure Formula(StartRange,EndRange : String ; Formula : String);
      procedure MergeCells(StartRange,EndRange : String ; Value : Boolean = True);
      procedure ColumnWidth(Col : String; value : Byte); overload;
      procedure ColumnWidths(Cols : array of String; Value :Byte); overload;
      procedure RowHeight(Row : integer; value : Word);
      procedure RowHeights(Rows : array of Integer; value : Word);
      procedure Autofit;
      procedure AddWorkSheet(SheetName : String ='');
      procedure SelectWorkSheet(idx : Integer);

      procedure NumberFormat(Range : String; FormatStr : String=''); overload;
      procedure NumberFormat(Col,Row : Integer; FormatStr : String=''); overload;

      //행별로 입력하는 함수들--------------------------------------------------
      // 직접 VarArray를 만들어서 AddData(Row,Data)로 넘겨줘도 되고
      // CreateData후에 RowData에 데이터를 할당하고 AddData(Row)를 해도 됨
      procedure CreateData(nSize : Integer);
      procedure AddData(Row : integer); overload;
      procedure AddData(Row : integer;const Data : Variant); overload;
      property RowData[index:Integer] : Variant read getRowData write setRowData;
//      function ExcelNumber2Char(nNumber : integer):String;      
      //------------------------------------------------------------------------

      property Sheet:OleVariant  read FSHEET write FSHEET;
      property SavePath:String  read FSavePath write FSavePath;
      property FileName:String  read FFileName write FFileName;
      property SheetName:String  read FSheetName write FSheetName;
  end;

implementation

{ TExcelWriter }

procedure TExcelWriter.AddWorkSheet(SheetName : String);
begin
//  FSHEET := FWB.Sheets.Add;
  FSHEET := FWB.Sheets.Add(After := FWB.Sheets[FWB.Sheets.Count]);
  IF Trim(SheetName) <> '' Then
    FSHEET.Name := SheetName; 
end;

procedure TExcelWriter.Autofit;
begin
  FSHEET.Columns.AutoFit;
end;

procedure TExcelWriter.Cells(COL,ROW : Integer; Value : string;Align : TTextAlignment=taLeft);
begin
  FSHEET.Cells[ROW,COL].Value := Value ;
  CellAlign(COL,ROW,Align);
//  Case Align of
//    taLeft   : FSHEET.Cells[ROW,COL].HorizontalAlignment := xlLeft;
//    taCenter : FSHEET.Cells[ROW,COL].HorizontalAlignment := xlHAlignCenter;
//    taRight  : FSHEET.Cells[ROW,COL].HorizontalAlignment := xlRight;
//  end;
end;

constructor TExcelWriter.Create;
begin
  //Excel
  Try
    FXLS := CreateOleObject('Excel.Application');
  except
    Raise Exception.Create('Excel프로그램이 없습니다.');
  end;
  FWB := FXLS.Workbooks.add;
  FSHEET := FWB.ActiveSheet;
end;

destructor TExcelWriter.Destroy;
begin
  FSHEET := Unassigned;
  FWB := Unassigned;
  FXLS.Quit;
  FXLS := Unassigned;
end;

procedure TExcelWriter.FontBold(StartRange, EndRange: String;
  Value: Boolean);
begin
  FSHEET.Range[StartRange+':'+EndRange].Font.Bold := Value;
end;

procedure TExcelWriter.FontSize(StartRange, EndRange: String;
  Value: integer);
begin
  FSHEET.Range[StartRange+':'+EndRange].Font.Size := Value;
end;

procedure TExcelWriter.Formula(StartRange, EndRange, Formula: String);
begin
  FXLS.Range[StartRange+':'+EndRange].Formula  := Formula;
end;

procedure TExcelWriter.MergeCells(StartRange, EndRange: String;
  Value: Boolean);
begin
  FXLS.Range[StartRange+':'+EndRange].Mergecells   := Value;
end;

procedure TExcelWriter.ShowExcel;
//var
//  sSaveDialog1 : TsSaveDialog;
begin
//  IF FileName = '' Then
//  begin
//    sSaveDialog1 := TsSaveDialog.Create(nil);

//    sSaveDialog1.DefaultExt := '*.xls';
//    sSaveDialog1.Filter := 'Excel 통합문서'+'(*.xlsx)|*.xlsx|Excel 97 - 2003 통합문서'+'(*.xls)|*.xls';

//    IF sSaveDialog1.Execute Then
//      FileName := sSaveDialog1.FileName;
    IF FSheetName <> '' Then
        FSHEET.Name := FSheetName;

    FXLS.Visible := True;
//  end;

end;

procedure TExcelWriter.Save(sFileName: String);
begin
  FXLS.DisplayAlerts := False;
  If FileExists(PChar(sFileName)) Then DeleteFile(PChar(sFileName));
  FWB.SaveAs(sFileName);
  FWB.Close;
end;

procedure TExcelWriter.SaveWithDialog;
var
  sSaveDialog1 : TsSaveDialog;
  sFileName : string;
begin
    sSaveDialog1 := TsSaveDialog.Create(nil);

    sSaveDialog1.DefaultExt := '*.xlsx';
    sSaveDialog1.Filter := 'Excel 통합문서'+'(*.xlsx)|*.xlsx|Excel 97 - 2003 통합문서'+'(*.xls)|*.xls';

    FXLS.DisplayAlerts := False;
    IF sSaveDialog1.Execute Then
    begin
      sFileName := sSaveDialog1.FileName;
      If FileExists(PChar(sFileName)) Then DeleteFile(PChar(sFileName));
      FWB.SaveAs(sFileName);
      ShellExecute(0, nil, 'explorer.exe', Pchar('/select,'+sFileName), nil, SW_SHOWNORMAL);
    end;
    FWB.Close;
end;

procedure TExcelWriter.SelectWorkSheet(idx: Integer);
begin
  FSHEET := FWB.WorkSheets[idx].select;
  FSHEET := FWB.ActiveSheet;
//  FSHEET := FWB.ActiveSheet;
end;

procedure TExcelWriter.ColumnWidth(Col : String;value: Byte);
begin
  FSHEET.Columns[Col+':'+Col].ColumnWidth := value;
end;

procedure TExcelWriter.ColumnWidths(Cols: array of String; Value: Byte);
var
  i : Integer;
begin
  for i := 0 to High(Cols) do
  begin
    ColumnWidth(Cols[i], Value);
  end;
end;

procedure TExcelWriter.RowHeight(Row: integer; value: Word);
begin
	IF value > 200 Then value := 200;

  FSHEET.Rows[IntToStr(Row)].RowHeight := value;
	//FSHEET.Rows[inttostr(Row)+':'+IntToStr(Row)].RowHeight := value;
end;

procedure TExcelWriter.RowHeights(Rows: array of Integer; value: Word);
var
  i : integer;
begin
  for i:=0 to High(Rows) do
  begin
    RowHeight(Rows[i], value);
  end;
end;

procedure TExcelWriter.AddData(Row : integer; const Data: Variant);
var
   MaxRange : String;
begin
  MaxRange := ExcelNumber2Char( VarArrayHighBound(Data,1) );
  FSHEET.Range['A'+IntToStr(Row),MaxRange+IntToStr(Row)].Value := Data;
  ClearData;
end;

function TExcelWriter.ExcelNumber2Char(nNumber: integer): String;
begin
  Result := '';

  IF nNumber <= 26 Then
    Result := Chr(nNumber+64)
  Else
  IF (nNumber mod 26 = 0) Then
    Result := Chr((nNumber div 26)+63)+'Z'
  Else
    Result := Chr((nNumber div 26)+64)+Chr((nNumber mod 26)+64);
end;

procedure TExcelWriter.CreateData(nSize: Integer);
begin
  FRowData := VarArrayCreate([1,nSize],varVariant);
end;

function TExcelWriter.getRowData(index: Integer): Variant;
begin
  Result := FRowData[index];
end;

procedure TExcelWriter.setRowData(index: Integer; const Value: Variant);
begin
  FRowData[index] := Value;
end;

procedure TExcelWriter.AddData(Row : integer);
begin
  AddData(Row,FRowData);  
end;

procedure TExcelWriter.ClearData;
var
  i : Integer;
begin
  for i := VarArrayLowBound(FRowData,1) to VarArrayHighBound(FRowData,1) do
  begin
    FRowData[i] := Unassigned;
  end;
end;

procedure TExcelWriter.CellFormat(StartRange, EndRange, Format: String);
begin
  //      'NumberFormatLocal = "0.00%"'
  FXLS.Range[StartRange+':'+EndRange].NumberFormatLocal := Format;
end;

procedure TExcelWriter.CellColor(Col, Row: Integer; Value: TColor);
begin
  FSHEET.Cells[ROW,COL].Interior.Color := Value;
end;

procedure TExcelWriter.CellColor(Range: String; value: TColor);
begin
  FSHEET.Range[Range].Select;

  FXLS.Selection.Interior.Color := value;
end;

procedure TExcelWriter.CellBorder(sRange: String; Lineoption: TLineOptions;
  Thick: Integer);
begin
  FSHEET.Range[sRange].Select;

  IF loTop       IN Lineoption then
  begin
    IF Thick = xlNone Then FXLS.Selection.Borders[xlEdgeTop].LineStyle := Thick
    Else
    begin
      FXLS.Selection.Borders[xlEdgeTop].LineStyle := xlContinuous;
      FXLS.Selection.Borders[xlEdgeTop].Weight := Thick;
    end;
  end;
  IF loBottom    IN Lineoption then
  begin
    IF Thick = xlNone Then FXLS.Selection.Borders[xlEdgeBottom].LineStyle := Thick
    Else
    begin
      FXLS.Selection.Borders[xlEdgeBottom].LineStyle := xlContinuous;
      FXLS.Selection.Borders[xlEdgeBottom].Weight := Thick;
    end;
  end;
  IF loLeft      IN Lineoption then
  begin
    IF Thick = xlNone Then FXLS.Selection.Borders[xlEdgeLeft].LineStyle := Thick
    Else
    begin
      FXLS.Selection.Borders[xlEdgeLeft].LineStyle := xlContinuous;
      FXLS.Selection.Borders[xlEdgeLeft].Weight := Thick;
    end;
  end;
  IF loRight     IN Lineoption then
  begin
    IF Thick = xlNone Then FXLS.Selection.Borders[xlEdgeRight].LineStyle := Thick
    Else
    begin
    FXLS.Selection.Borders[xlEdgeRight].LineStyle := xlContinuous;
    FXLS.Selection.Borders[xlEdgeRight].Weight := Thick;
    end;
  end;
  IF loAll       IN Lineoption then
  begin
    IF Thick = xlNone Then FXLS.Selection.Borders.LineStyle := Thick
    Else
    begin
    FXLS.Selection.Borders.LineStyle := xlContinuous;
    FXLS.Selection.Borders.Weight := Thick;
    end;
  end;
  IF loTopBottom IN Lineoption then
  begin
    IF Thick = xlNone Then
    begin
      FXLS.Selection.Borders[xlEdgeTop].LineStyle := Thick;
      FXLS.Selection.Borders[xlEdgeBottom].LineStyle := Thick;
    end
    Else
    begin
      FXLS.Selection.Borders[xlEdgeTop].LineStyle := xlContinuous;
      FXLS.Selection.Borders[xlEdgeTop].Weight := Thick;
      FXLS.Selection.Borders[xlEdgeBottom].LineStyle := xlContinuous;
      FXLS.Selection.Borders[xlEdgeBottom].Weight := Thick;
    end;
  end;
  IF loEdge IN Lineoption Then
  begin
    IF Thick = xlNone Then
    begin
      FXLS.Selection.Borders[xlEdgeTop].LineStyle := Thick;
      FXLS.Selection.Borders[xlEdgeBottom].LineStyle := Thick;
      FXLS.Selection.Borders[xlEdgeLeft].LineStyle := Thick;
      FXLS.Selection.Borders[xlEdgeRight].LineStyle := Thick;
    end
    Else
    begin
      FXLS.Selection.Borders[xlEdgeTop].LineStyle := xlContinuous;
      FXLS.Selection.Borders[xlEdgeTop].Weight := Thick;
      FXLS.Selection.Borders[xlEdgeBottom].LineStyle := xlContinuous;
      FXLS.Selection.Borders[xlEdgeBottom].Weight := Thick;
      FXLS.Selection.Borders[xlEdgeLeft].LineStyle := xlContinuous;
      FXLS.Selection.Borders[xlEdgeLeft].Weight := Thick;
      FXLS.Selection.Borders[xlEdgeRight].LineStyle := xlContinuous;
      FXLS.Selection.Borders[xlEdgeRight].Weight := Thick;
    end;
  end;
end;

procedure TExcelWriter.CellBorder(FromCol, FromRow, ToCol, ToRow: Integer;
  Lineoption: TLineOptions; Thick: Integer);
var
  sRange : String;
begin
  sRange := ExcelNumber2Char(FromCol)+IntToStr(FromRow)+':'+ExcelNumber2Char(ToCol)+IntToStr(ToRow);
  CellBorder(sRange,Lineoption,Thick);
end;

function TExcelWriter.ExcelChar2Number(sChar: String): Integer;
var
  Temp : string;
begin
  Temp := sChar;

  Result := -1;

  IF Length(Temp) = 1 Then
  begin
    Result := Ord(Temp[1])-64;
  end
  else
  IF Length(Temp) = 2 Then
  begin
    If Temp[2] = 'Z' Then
      Result := (Ord(Temp[1])-63)*26
    else
      Result := ((Ord(Temp[1])-64)*26)+(Ord(Temp[2])-64)
  end;
end;

procedure TExcelWriter.CellCenterAcross(FromCol, FromRow, ToCol,
  ToRow: integer);
var
  sRange : String;
begin
  sRange := ExcelNumber2Char(FromCol)+IntToStr(FromRow)+':'+ExcelNumber2Char(ToCol)+IntToStr(ToRow);
  FXLS.Range[sRange].Select;
  FXLS.Selection.HorizontalAlignment := xlCenterAcrossSelection;
  FXLS.Selection.VerticalAlignment := xlCenter;
  FXLS.Selection.WrapText := False;
  FXLS.Selection.Orientation := 0;
  FXLS.Selection.ShrinkToFit := False;
  FXLS.Selection.ReadingOrder := xlContext;
  FXLS.Selection.MergeCells := False;
end;

procedure TExcelWriter.CellsLeftIndent(Col, Row: Integer; value: String;
  indentLevel: Byte);
begin
  FSHEET.Cells[ROW,COL].Value := Value ;
  FSHEET.Cells[ROW,COL].HorizontalAlignment := xlLeft;
  FSHEET.Cells[Row,Col].IndentLevel := indentLevel;
end;

procedure TExcelWriter.FontSize(FromCol, FromRow, ToCol, ToRow,
  Value: Integer);
var
  sFROM, sTO : String;
begin
  sFROM := ExcelNumber2Char(FromCol)+IntToStr(FromRow);
  sTO := ExcelNumber2Char(ToCol)+IntToStr(ToRow);
  FontSize(sFROM,sTO,Value);
end;

procedure TExcelWriter.CellAlign(COL, ROW: Integer; Align: TTextAlignment);
begin
  Case Align of
    taLeft   : FSHEET.Cells[ROW,COL].HorizontalAlignment := xlLeft;
    taCenter : FSHEET.Cells[ROW,COL].HorizontalAlignment := xlHAlignCenter;
    taRight  : FSHEET.Cells[ROW,COL].HorizontalAlignment := xlRight;
  end;
end;

procedure TExcelWriter.CellAlign(Range: String; Align: TTextAlignment);
begin
  FSHEET.Range[Range].Select;

  Case Align of
    taLeft   : FXLS.Selection.HorizontalAlignment := xlLeft;
    taCenter : FXLS.Selection.HorizontalAlignment := xlHAlignCenter;
    taRight  : FXLS.Selection.HorizontalAlignment := xlRight;
  end;

end;

procedure TExcelWriter.NumberFormat(Range, FormatStr: String);
begin
  IF FormatStr = '' Then FormatStr := FM_B_COMMA;
  FSHEET.Range[Range].Select;
  FXLS.Selection.NumberFormatLocal := FormatStr;
end;

procedure TExcelWriter.NumberFormat(Col, Row: Integer; FormatStr: String);
begin
  IF FormatStr = '' Then FormatStr := FM_B_COMMA;
  FSHEET.Cells[ROW,COL].NumberFormatLocal := FormatStr;
end;

end.

