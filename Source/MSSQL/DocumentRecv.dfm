inherited RecvDocuments_frm: TRecvDocuments_frm
  Left = 559
  Top = 153
  BorderWidth = 4
  Caption = #47928#49436#49688#49888
  ClientHeight = 539
  ClientWidth = 752
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object sDBGrid1: TsDBGrid [0]
    Left = 0
    Top = 85
    Width = 752
    Height = 454
    Align = alClient
    Color = clWhite
    Ctl3D = False
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ImeName = 'Microsoft IME 2010'
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    SkinData.CustomColor = True
    SkinData.CustomFont = True
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'SRDATE'
        Title.Alignment = taCenter
        Title.Caption = #49688#49888#51068#51088
        Width = 80
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'SRTIME'
        Title.Alignment = taCenter
        Title.Caption = #49688#49888#49884#44036
        Width = 60
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DOCID'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = #44404#47548#52404
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #47928#49436'ID'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Title.Caption = #47928#49436#47749
        Width = 238
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Caption = #44288#47532#48264#54840
        Width = 203
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'CON'
        Title.Alignment = taCenter
        Title.Caption = #48320#54872
        Width = 33
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DBAPPLY'
        Title.Alignment = taCenter
        Title.Caption = #51200#51109
        Width = 33
        Visible = True
      end>
  end
  object sPanel1: TsPanel [1]
    Left = 0
    Top = 0
    Width = 752
    Height = 53
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 1
    object sSpeedButton3: TsSpeedButton
      Left = 301
      Top = 1
      Width = 2
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSplitter2: TsSplitter
      Left = 1
      Top = 1
      Width = 192
      Height = 51
      Cursor = crHSplit
      AutoSnap = False
      Enabled = False
      SkinData.SkinSection = 'SPLITTER'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 685
      Top = 1
      Width = 11
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Close: TsSpeedButton
      Left = 696
      Top = 1
      Width = 55
      Height = 51
      Cursor = crHandPoint
      Caption = #45803#44592
      Layout = blGlyphTop
      Spacing = 0
      OnClick = Btn_CloseClick
      Align = alRight
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      ImageIndex = 20
      Reflected = True
    end
    object sSpeedButton2: TsSpeedButton
      Left = 193
      Top = 1
      Width = 2
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 7
      Width = 176
      Height = 25
      Caption = 'Receive Documents'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 29
      Width = 52
      Height = 15
      Caption = #47928#49436' '#49688#49888
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton1: TsSpeedButton
      Tag = 2
      Left = 195
      Top = 1
      Width = 53
      Height = 51
      Cursor = crHandPoint
      Caption = #49688#49888
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = sSpeedButton1Click
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      Alignment = taLeftJustify
      ImageIndex = 29
      Reflected = True
    end
    object sSpeedButton4: TsSpeedButton
      Tag = 2
      Left = 248
      Top = 1
      Width = 53
      Height = 51
      Cursor = crHandPoint
      Caption = #48320#54872
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = sSpeedButton4Click
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      Alignment = taLeftJustify
      ImageIndex = 24
      Reflected = True
    end
    object sCheckBox1: TsCheckBox
      Left = 312
      Top = 27
      Width = 270
      Height = 21
      Caption = #49688#49888#49884' '#44592#51316' IN'#54260#45908#50504#50640' '#54028#51068#51012' TEMP'#47196' '#50734#44608
      AutoSize = False
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 0
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sCheckBox2: TsCheckBox
      Left = 312
      Top = 8
      Width = 174
      Height = 21
      Caption = 'WINMATE '#49688#49888#47784#46280' '#48120#49324#50857
      AutoSize = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sCheckBox3: TsCheckBox
      Left = 496
      Top = 8
      Width = 174
      Height = 21
      Caption = #51473#48373#47928#49436' '#47924#49884
      AutoSize = False
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 2
      SkinData.SkinSection = 'CHECKBOX'
    end
  end
  object sPanel7: TsPanel [2]
    Left = 0
    Top = 53
    Width = 752
    Height = 32
    SkinData.CustomColor = True
    SkinData.SkinSection = 'PAGECONTROL'
    Align = alTop
    
    TabOrder = 2
    object sMaskEdit6: TsMaskEdit
      Tag = 1
      Left = 64
      Top = 6
      Width = 73
      Height = 20
      Color = clWhite
      EditMask = '9999-99-99;0;'
      ImeName = 'Microsoft IME 2010'
      MaxLength = 10
      TabOrder = 0
      Text = '20130101'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49688#49888#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 4276545
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      SkinData.CustomColor = True
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object sMaskEdit7: TsMaskEdit
      Tag = 1
      Left = 168
      Top = 6
      Width = 73
      Height = 20
      Color = clWhite
      EditMask = '9999-99-99;0;'
      ImeName = 'Microsoft IME 2010'
      MaxLength = 10
      TabOrder = 1
      Text = '20130101'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #48512#53552
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 4276545
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      SkinData.CustomColor = True
      SkinData.SkinSection = 'PAGECONTROL'
    end
    object sEdit3: TsEdit
      Left = 370
      Top = 6
      Width = 159
      Height = 20
      ImeName = 'Microsoft IME 2010'
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sButton1: TsButton
      Left = 530
      Top = 6
      Width = 55
      Height = 20
      Caption = #51312#54924
      TabOrder = 3
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sComboBox1: TsComboBox
      Left = 640
      Top = 6
      Width = 105
      Height = 20
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#54596#53552
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = 4276545
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'COMBOBOX'
      Style = csDropDownList
      ImeName = 'Microsoft IME 2010'
      ItemHeight = 14
      ItemIndex = -1
      TabOrder = 4
      OnSelect = sComboBox1Select
    end
    object sButton2: TsButton
      Left = 242
      Top = 6
      Width = 55
      Height = 20
      Caption = #50724#45720
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sPanel2: TsPanel [3]
    Left = 139
    Top = 177
    Width = 462
    Height = 248
    SkinData.SkinSection = 'PANEL'
    
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object sLabel1: TsLabel
      Left = 14
      Top = 16
      Width = 211
      Height = 15
      Caption = #45796#51020' '#47928#49436#45716' '#51060#48120' '#51316#51116#54616#45716' '#47928#49436#51077#45768#45796'.'
    end
    object sLabel2: TsLabel
      Left = 14
      Top = 32
      Width = 432
      Height = 15
      Caption = #51316#51116#54616#45716' '#47928#49436#46020' '#49688#49888' '#48155#44592#50948#54644#49436#45716' '#49345#45800' '#50741#49496#51032' ['#51473#48373#47928#49436' '#47924#49884'] '#48260#53948#51012' '#54644#51228' '#54980
    end
    object sLabel3: TsLabel
      Left = 14
      Top = 48
      Width = 143
      Height = 15
      Caption = #51116#49688#49888' '#48155#51004#49884#44600' '#48148#46989#45768#45796'.'
    end
    object sListBox1: TsListBox
      Left = 15
      Top = 72
      Width = 433
      Height = 129
      ItemHeight = 16
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
    end
    object sButton3: TsButton
      Left = 192
      Top = 206
      Width = 75
      Height = 33
      Caption = #54869#51064
      TabOrder = 1
      OnClick = sButton3Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sComboBox2: TsComboBox [4]
    Left = 584
    Top = 144
    Width = 145
    Height = 20
    Style = csOwnerDrawVariable
    ItemHeight = 14
    ItemIndex = -1
    TabOrder = 4
    Items.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5')
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 96
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [SRDATE]'
      '      ,[Sunbun]'
      '      ,[SRTIME]'
      '      ,[DOCID]'
      #9'  ,[NAME]'
      '      ,[MAINT_NO]'
      '      ,[MSEQ]'
      '      ,[CONTROLNO]'
      '      ,[RECEIVEUSER]'
      '      ,[Mig]'
      '      ,[SRVENDER]'
      '      ,[CON]'
      '      ,[DBAPPLY]'
      '      ,[DOCNAME]'
      '  FROM [R_HST] LEFT JOIN (SELECT CODE,NAME'
      #9#9'             FROM CODE2NDD'
      
        #9#9'             WHERE Prefix = '#39'DOCID'#39') DNAME ON R_HST.DOCID = DN' +
        'AME.CODE')
    Left = 48
    Top = 96
    object qryListSRDATE: TStringField
      FieldName = 'SRDATE'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryListSunbun: TAutoIncField
      FieldName = 'Sunbun'
      ReadOnly = True
    end
    object qryListSRTIME: TStringField
      FieldName = 'SRTIME'
      Size = 8
    end
    object qryListDOCID: TStringField
      FieldName = 'DOCID'
      Size = 10
    end
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryListCONTROLNO: TStringField
      FieldName = 'CONTROLNO'
    end
    object qryListRECEIVEUSER: TStringField
      FieldName = 'RECEIVEUSER'
      Size = 10
    end
    object qryListMig: TMemoField
      FieldName = 'Mig'
      BlobType = ftMemo
    end
    object qryListSRVENDER: TStringField
      FieldName = 'SRVENDER'
    end
    object qryListCON: TStringField
      FieldName = 'CON'
      Size = 1
    end
    object qryListDBAPPLY: TStringField
      FieldName = 'DBAPPLY'
      Size = 1
    end
    object qryListDOCNAME: TStringField
      FieldName = 'DOCNAME'
      Size = 6
    end
    object qryListNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 96
  end
  object qryDocList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT DISTINCT [DOCID]'
      '  FROM [R_HST] '
      'ORDER BY DOCID')
    Left = 48
    Top = 128
  end
  object qryIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'MSEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'RECEIVEUSER'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'Mig'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'DOCNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end>
    SQL.Strings = (
      
        'INSERT INTO R_HST(SRDATE, SRTIME, DOCID, MAINT_NO, MSEQ, RECEIVE' +
        'USER, Mig, DOCNAME)'
      
        'VALUES(:SRDATE, :SRTIME, :DOCID, :MAINT_NO, :MSEQ, :RECEIVEUSER,' +
        ' :Mig, :DOCNAME)')
    Left = 48
    Top = 176
  end
  object qryCheckDoc: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      'FROM [R_HST]'
      'WHERE [MAINT_NO] = :MAINT_NO')
    Left = 80
    Top = 176
  end
end
