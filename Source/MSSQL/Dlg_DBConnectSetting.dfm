inherited DialogParent_frm1: TDialogParent_frm1
  Left = 570
  Top = 259
  Caption = #45936#51060#53552#48288#51060#49828' '#49444#51221
  ClientHeight = 125
  ClientWidth = 526
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 526
    Height = 125
    object sBevel1: TsBevel
      Left = 1
      Top = 1
      Width = 524
      Height = 80
      Align = alTop
      Shape = bsBottomLine
    end
    object sEdit1: TsEdit
      Left = 112
      Top = 16
      Width = 121
      Height = 23
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #51064#49828#53556#49828#47749
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sDirectoryEdit1: TsDirectoryEdit
      Left = 112
      Top = 40
      Width = 393
      Height = 23
      AutoSelect = False
      AutoSize = False
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 255
      ParentFont = False
      TabOrder = 1
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.Caption = #45936#51060#53552#48288#51060#49828' '#44221#47196
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
      GlyphMode.Blend = 0
      GlyphMode.Grayed = False
      Root = 'rfDesktop'
    end
    object sButton1: TsButton
      Left = 185
      Top = 88
      Width = 75
      Height = 25
      Caption = #54869#51064
      TabOrder = 2
      SkinData.SkinSection = 'BUTTON'
    end
    object sButton2: TsButton
      Left = 265
      Top = 88
      Width = 75
      Height = 25
      Caption = #52712#49548
      TabOrder = 3
      SkinData.SkinSection = 'BUTTON'
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 448
    Top = 224
  end
end
