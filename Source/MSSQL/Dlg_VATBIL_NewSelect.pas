unit Dlg_VATBIL_NewSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, StdCtrls, sButton, ExtCtrls, sBevel, sLabel,
  sSkinProvider, sPanel;

type
  TDlg_VATBIL_NewSelect_frm = class(TDialogParent_frm)
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sBevel1: TsBevel;
    sLabel1: TsLabel;
    sBevel2: TsBevel;
    sButton2: TsButton;
    sButton3: TsButton;
    sButton1: TsButton;
    procedure sButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Dlg_VATBIL_NewSelect_frm: TDlg_VATBIL_NewSelect_frm;

implementation

{$R *.dfm}



procedure TDlg_VATBIL_NewSelect_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  case TsButton(Sender).Tag of
    0: ModalResult := mrOk;
    1: ModalResult := mrYes;
    2: ModalResult := mrCancel;
  end;
end;

end.
