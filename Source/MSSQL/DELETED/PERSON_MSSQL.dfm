inherited PERSON_MSSQL_frm: TPERSON_MSSQL_frm
  Left = 973
  Top = 293
  Caption = #49324#50857#51088#44288#47532
  ClientHeight = 397
  ClientWidth = 540
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 540
    Height = 49
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object Btn_Close: TsSpeedButton
      Tag = 5
      Left = 487
      Top = 1
      Width = 50
      Height = 47
      Cursor = crHandPoint
      Caption = #45803#44592
      Flat = True
      Layout = blGlyphTop
      Spacing = 0
      OnClick = Btn_CloseClick
      Align = alLeft
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 20
      Reflected = True
    end
    object sSplitter2: TsSplitter
      Left = 1
      Top = 1
      Width = 200
      Height = 47
      Cursor = crHSplit
      AutoSnap = False
      Enabled = False
      SkinData.SkinSection = 'SPLITTER'
    end
    object sLabel1: TsLabel
      Left = 63
      Top = 13
      Width = 80
      Height = 21
      Alignment = taCenter
      Caption = #49324#50857#51088#44288#47532
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object Btn_New: TsSpeedButton
      Left = 212
      Top = 1
      Width = 55
      Height = 47
      Cursor = crHandPoint
      Caption = #52628#44032
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = btn_ClickEvent
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 2
      Reflected = True
    end
    object Btn_Modify: TsSpeedButton
      Tag = 1
      Left = 267
      Top = 1
      Width = 55
      Height = 47
      Cursor = crHandPoint
      Caption = #49688#51221
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = btn_ClickEvent
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 3
      Reflected = True
    end
    object Btn_Del: TsSpeedButton
      Tag = 2
      Left = 322
      Top = 1
      Width = 55
      Height = 47
      Cursor = crHandPoint
      Caption = #49325#51228
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = btn_ClickEvent
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 27
      Reflected = True
    end
    object sSpeedButton1: TsSpeedButton
      Left = 201
      Top = 1
      Width = 11
      Height = 47
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Save: TsSpeedButton
      Tag = 3
      Left = 377
      Top = 1
      Width = 55
      Height = 47
      Cursor = crHandPoint
      Caption = #51200#51109
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = btn_ClickEvent
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 7
      Reflected = True
    end
    object btn_Cancel: TsSpeedButton
      Tag = 4
      Left = 432
      Top = 1
      Width = 55
      Height = 47
      Cursor = crHandPoint
      Caption = #52712#49548
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = btn_ClickEvent
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 18
      Reflected = True
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 49
    Width = 540
    Height = 200
    Align = alTop
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'USERID'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #50500#51060#46356
        Width = 120
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'PASSWORDD'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #54056#49828#50892#46300
        Width = 120
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NAME'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #49324#50857#51088
        Width = 150
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DEPT'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #48512#49436
        Width = 142
        Visible = True
      end>
  end
  object sPanel2: TsPanel [2]
    Left = 0
    Top = 249
    Width = 540
    Height = 148
    Align = alClient
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object sDBEdit1: TsDBEdit
      Left = 160
      Top = 24
      Width = 257
      Height = 20
      Color = clWhite
      DataField = 'USERID'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50500#51060#46356
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit2: TsDBEdit
      Left = 160
      Top = 48
      Width = 257
      Height = 20
      Color = clWhite
      DataField = 'PASSWORDD'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #54056#49828#50892#46300
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit3: TsDBEdit
      Left = 160
      Top = 72
      Width = 257
      Height = 20
      Color = clWhite
      DataField = 'NAME'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit4: TsDBEdit
      Left = 160
      Top = 96
      Width = 257
      Height = 20
      Color = clWhite
      DataField = 'DEPT'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #48512#49436
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 96
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      '  SELECT [USERID],[ID],[NAME],[DEPT],[PASSWORD],[DEPTCODE] '
      '  FROM PERSON')
    Left = 8
    Top = 128
    object qryListUSERID: TStringField
      FieldName = 'USERID'
      Size = 15
    end
    object qryListID: TStringField
      FieldName = 'ID'
      Size = 15
    end
    object qryListNAME: TStringField
      FieldName = 'NAME'
      Size = 10
    end
    object qryListDEPT: TStringField
      FieldName = 'DEPT'
      Size = 40
    end
    object qryListDEPTCODE: TStringField
      FieldName = 'DEPTCODE'
      Size = 8
    end
    object qryListPASSWORD: TStringField
      FieldName = 'PASSWORD'
      Size = 64
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 128
  end
end
