unit PERSON_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, DB, ADODB, StdCtrls, Mask, DBCtrls,
  sDBEdit, Grids, DBGrids, acDBGrid, sLabel, ExtCtrls, sSplitter, Buttons,
  sSpeedButton, sPanel, MSSQL;

type
  TPERSON_MSSQL_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    Btn_Close: TsSpeedButton;
    sSplitter2: TsSplitter;
    sLabel1: TsLabel;
    Btn_New: TsSpeedButton;
    Btn_Modify: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    sSpeedButton1: TsSpeedButton;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListUSERID: TStringField;
    qryListID: TStringField;
    qryListNAME: TStringField;
    qryListDEPT: TStringField;
    qryListDEPTCODE: TStringField;
    sPanel2: TsPanel;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBEdit4: TsDBEdit;
    Btn_Save: TsSpeedButton;
    btn_Cancel: TsSpeedButton;
    qryListPASSWORD: TStringField;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_ClickEvent(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);

  private
    { Private declarations }
    FSQL : String;
    procedure BtnEnabledControls(Val: Boolean);
  public
    { Public declarations }
  end;

var
  PERSON_MSSQL_frm: TPERSON_MSSQL_frm;

implementation

{$R *.dfm}

uses Commonlib , MessageDefine;

procedure TPERSON_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;

end;

procedure TPERSON_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  qryList.Close;
  qryList.Open;

  EnabledControlValue(sPanel2);
  BtnEnabledControls(True);
end;

procedure TPERSON_MSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  PERSON_MSSQL_frm := nil;
end;

procedure TPERSON_MSSQL_frm.btn_ClickEvent(Sender: TObject);
begin
  inherited;
  case (Sender as TsSpeedButton).Tag of

    0 :
      begin
        if not DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.BeginTrans;
        sDBGrid1.Enabled := False;
        EnabledControlValue(sPanel2 , False);
        BtnEnabledControls(False);
        qryList.Append;
      end;

    1 :
      begin
        if not DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.BeginTrans;
        sDBGrid1.Enabled := False;
        EnabledControlValue(sPanel2 , False);
        BtnEnabledControls(False);
        qryList.Edit;
      end;

    2 :
      begin
        if not DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.BeginTrans;
        IF MessageBox(Self.Handle,'사용자를 삭제하시겠습니까?' ,'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          qryList.Delete;
          DMMssql.KISConnect.CommitTrans;
        end
        else
          DMMssql.KISConnect.RollbackTrans;
      end;

    3 :
      begin
        qryList.Post;
        sDBGrid1.Enabled := True;
        EnabledControlValue(sPanel2);
        BtnEnabledControls(True);
        if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.CommitTrans;
      end;

    4 :
      begin
        qryList.Cancel;
        sDBGrid1.Enabled := True;
        EnabledControlValue(sPanel2);
        BtnEnabledControls(True);
        if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
      end;
  end;
end;

procedure TPERSON_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TPERSON_MSSQL_frm.BtnEnabledControls(Val: Boolean);
begin
  Btn_New.Enabled := Val;
  Btn_Modify.Enabled := Val;
  Btn_Del.Enabled := Val;

  Btn_Save.Enabled := not Val;
  btn_Cancel.Enabled := not Val;
end;

end.
