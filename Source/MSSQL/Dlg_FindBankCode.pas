unit Dlg_FindBankCode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, DB, Grids, DBGrids, acDBGrid, StdCtrls, sButton,
  sEdit, sComboBox, sSkinProvider, ExtCtrls, sPanel;

type
  TDlg_FindBankCode_frm = class(TDialogParent_frm)
    sComboBox1: TsComboBox;
    sEdit1: TsEdit;
    sButton1: TsButton;
    sDBGrid1: TsDBGrid;
    sButton2: TsButton;
    sButton3: TsButton;
    DataSource1: TDataSource;
    sButton4: TsButton;
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Run:TModalResult;
    function ExistsValue(Code : string):Boolean; 
  end;

var
  Dlg_FindBankCode_frm: TDlg_FindBankCode_frm;

implementation

uses CodeContents, Dlg_Bank, TypeDefine;

{$R *.dfm}

{ TDlg_FindBankCode_frm }

function TDlg_FindBankCode_frm.ExistsValue(Code: string): Boolean;
begin
  DataSource1.DataSet.Close;
  DataSource1.DataSet.Open;
  DataSource1.DataSet.First;
  Result := DataSource1.DataSet.Locate('CODE',Code,[]);
end;

function TDlg_FindBankCode_frm.Run: TModalResult;
begin
  DataSource1.DataSet.Close;
  DataSource1.DataSet.Open;

  Result := Self.ShowModal;  
end;

procedure TDlg_FindBankCode_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF DataSource1.DataSet.RecordCount > 0 Then
    begin
      ModalResult := mrOk;
    end;
  end;
end;

procedure TDlg_FindBankCode_frm.sDBGrid1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF KEY = VK_RETURN THEN ModalResult := mrOk;
end;

procedure TDlg_FindBankCode_frm.FormShow(Sender: TObject);
begin
  inherited;
  sDBGrid1.SetFocus;
end;

procedure TDlg_FindBankCode_frm.sButton4Click(Sender: TObject);
var
  BANK_CODE : String;
begin
  inherited;

  Dlg_Bank_frm := TDlg_Bank_frm.Create(Self);

  try
    IF Dlg_Bank_frm.Run(ctInsert,nil) = mrOK Then
    begin
      BANK_CODE := Dlg_Bank_frm.edt_BankCode.Text;
      DataSource1.DataSet.Close;
      DataSource1.DataSet.Open;
      DataSource1.DataSet.Locate('CODE',BANK_CODE,[]);
    end;
  finally
     FreeAndNil( Dlg_Bank_frm );
  end;
end;

end.
