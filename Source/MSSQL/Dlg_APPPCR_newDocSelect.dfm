inherited dlg_APPPCR_newDocSelect_frm: Tdlg_APPPCR_newDocSelect_frm
  Left = 392
  Top = 190
  Caption = #49888#44508#47928#49436' '#49373#49457
  ClientHeight = 200
  ClientWidth = 378
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 378
    Height = 200
    object sButton1: TsButton
      Left = 8
      Top = 8
      Width = 361
      Height = 57
      Caption = #49888#44508#47928#49436' '#51089#49457'(&1)'
      ModalResult = 1
      TabOrder = 0
      SkinData.SkinSection = 'BUTTON'
      CommandLinkHint = #49888#44508#47928#49436#47484' '#51649#51217' '#49688#46041#51004#47196' '#51089#49457#54633#45768#45796
      Images = DMICON.System24
      ImageIndex = 5
      Style = bsCommandLink
      Reflected = True
    end
    object sButton3: TsButton
      Left = 8
      Top = 72
      Width = 361
      Height = 57
      Caption = #44592#51316#47928#49436' '#48373#49324'(&2)'
      ModalResult = 6
      TabOrder = 1
      SkinData.SkinSection = 'BUTTON'
      CommandLinkHint = #44592#51316#50640' '#51200#51109#46104#50612#51080#45716' '#47928#49436#47484' '#49440#53469#54616#50668' '#48373#49324#54633#45768#45796
      Images = DMICON.System24
      ImageIndex = 31
      Style = bsCommandLink
      Reflected = True
    end
    object sButton2: TsButton
      Left = 8
      Top = 136
      Width = 361
      Height = 57
      Cancel = True
      Caption = #52712#49548'(ESC)'
      ModalResult = 2
      TabOrder = 2
      SkinData.SkinSection = 'BUTTON'
      CommandLinkHint = #47928#49436#51089#49457#51012' '#52712#49548#54633#45768#45796
      Images = DMICON.System24
      ImageIndex = 18
      Style = bsCommandLink
      Reflected = True
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 344
    Top = 64
  end
end
