unit dlg_CreateDocumentChoice_APP700;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,Dlg_APPPCR_newDocSelect, ChildForm, sSkinProvider, ExtCtrls,
  sBevel, StdCtrls, sLabel, sButton, sPanel;

type
  Tdlg_CreateDocumentChoice_APP700_frm = class(Tdlg_APPPCR_newDocSelect_frm)
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sBevel1: TsBevel;
    sLabel1: TsLabel;
    sButton4: TsButton;
    sBevel2: TsBevel;
    procedure sButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_CreateDocumentChoice_APP700_frm: Tdlg_CreateDocumentChoice_APP700_frm;

implementation

uses MSSQL;

{$R *.dfm}

procedure Tdlg_CreateDocumentChoice_APP700_frm.sButton2Click(
  Sender: TObject);
begin
  inherited;

  DMMssql.KISConnect.RollbackTrans;
  Close;
end;

end.



