unit Dlg_LOCAPP_SelectCopy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Dlg_SelectCopyParent, sSkinProvider, Grids, DBGrids, acDBGrid,
  StdCtrls, sButton, sEdit, sComboBox, Mask, sMaskEdit, ExtCtrls, sPanel,
  DB, ADODB;

type
  TDlg_LOCAPP_SelectCopy_frm = class(TDlg_SelectCopyParent_frm)
    qryListMAINT_NO: TStringField;
    qryListDATEE: TStringField;
    qryListUSER_ID: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUSINESS: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListOPEN_NO: TBCDField;
    qryListAPP_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListGOODDES: TStringField;
    qryListGOODDES1: TMemoField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAPPLIC: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListDOC_ETC: TStringField;
    qryListDOC_ETC1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC_AMT: TBCDField;
    qryListLOC_AMTC: TStringField;
    qryListDOC_DTL: TStringField;
    qryListDOC_NO: TStringField;
    qryListDOC_AMT: TBCDField;
    qryListDOC_AMTC: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListIM_NAME: TStringField;
    qryListIM_NAME1: TStringField;
    qryListIM_NAME2: TStringField;
    qryListIM_NAME3: TStringField;
    qryListDEST: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListEXGOOD: TStringField;
    qryListEXGOOD1: TMemoField;
    qryListCHKNAME1: TStringField;
    qryListCHKNAME2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListLCNO: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    procedure sButton1Click(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ReadList;
  public
    { Public declarations }
  end;

var
  Dlg_LOCAPP_SelectCopy_frm: TDlg_LOCAPP_SelectCopy_frm;

implementation

uses VarDefine;

{$R *.dfm}

{ TDlg_LOCAPP_SelectCopy_frm }

procedure TDlg_LOCAPP_SelectCopy_frm.ReadList;
begin
  inherited;
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE between' + QuotedStr(sMaskEdit6.Text) + 'AND' + QuotedStr(sMaskEdit7.Text));
    SQL.Add('AND MAINT_NO LIKE ' + QuotedStr('%' + sEdit3.Text + '%'));
    SQL.Add('AND USER_ID = ' + QuotedStr(LoginData.sID));
    //CHK2 2 = ����
    SQL.Add('AND CHK2 = 1');
    SQL.Add('ORDER BY DATEE DESC');
    Open;
  end;
end;

procedure TDlg_LOCAPP_SelectCopy_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

end.
