inherited Dlg_ITEM_frm: TDlg_ITEM_frm
  Left = 1021
  Top = 223
  Caption = #54408#47749' '#48143' '#44508#44201#53076#46300
  ClientHeight = 482
  ClientWidth = 429
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 429
    Height = 482
    object btn_Find1: TsSpeedButton
      Left = 138
      Top = 283
      Width = 23
      Height = 23
      OnClick = btn_Find1Click
      SkinData.SkinSection = 'SPEEDBUTTON'
      Images = DMICON.System16
      ImageIndex = 0
    end
    object Btn_Find2: TsSpeedButton
      Tag = 1
      Left = 138
      Top = 307
      Width = 23
      Height = 23
      OnClick = btn_Find1Click
      SkinData.SkinSection = 'SPEEDBUTTON'
      Images = DMICON.System16
      ImageIndex = 0
    end
    object Btn_Find3: TsSpeedButton
      Tag = 2
      Left = 138
      Top = 379
      Width = 23
      Height = 23
      OnClick = btn_Find1Click
      SkinData.SkinSection = 'SPEEDBUTTON'
      Images = DMICON.System16
      ImageIndex = 0
    end
    object sButton2: TsButton
      Left = 137
      Top = 434
      Width = 75
      Height = 29
      Caption = #54869#51064
      TabOrder = 13
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 17
    end
    object sButton3: TsButton
      Left = 217
      Top = 434
      Width = 75
      Height = 29
      Cancel = True
      Caption = #52712#49548
      TabOrder = 14
      TabStop = False
      OnClick = sButton3Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 18
    end
    object edt_CODE: TsEdit
      Left = 96
      Top = 16
      Width = 121
      Height = 23
      MaxLength = 20
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #53076#46300
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_HS: TsEdit
      Left = 96
      Top = 43
      Width = 121
      Height = 23
      MaxLength = 10
      TabOrder = 1
      OnEnter = edt_HSEnter
      OnExit = edt_HSExit
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'HS'#48512#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_ShortName: TsEdit
      Left = 96
      Top = 70
      Width = 121
      Height = 23
      MaxLength = 35
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Short Name'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object Memo_FullName: TsMemo
      Left = 96
      Top = 97
      Width = 301
      Height = 89
      ScrollBars = ssVertical
      TabOrder = 3
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'Full Name'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeftTop
      SkinData.SkinSection = 'EDIT'
    end
    object Memo_Size: TsMemo
      Left = 96
      Top = 190
      Width = 301
      Height = 89
      ScrollBars = ssVertical
      TabOrder = 4
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44508#44201
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeftTop
      SkinData.SkinSection = 'EDIT'
    end
    object edt_detail1: TsEdit
      Left = 162
      Top = 283
      Width = 235
      Height = 23
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 6
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object edt_SuDanwi: TsEdit
      Left = 96
      Top = 283
      Width = 41
      Height = 23
      AutoSelect = False
      CharCase = ecUpperCase
      MaxLength = 3
      TabOrder = 5
      OnDblClick = edt_SuDanwiDblClick
      OnExit = edt_SuDanwiExit
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49688#47049#45800#50948
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_DangaDanwi: TsEdit
      Tag = 1
      Left = 96
      Top = 307
      Width = 41
      Height = 23
      AutoSelect = False
      CharCase = ecUpperCase
      MaxLength = 3
      TabOrder = 7
      OnDblClick = edt_SuDanwiDblClick
      OnExit = edt_SuDanwiExit
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45800#44032#45800#50948
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_Detail2: TsEdit
      Left = 162
      Top = 307
      Width = 235
      Height = 23
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 8
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Caption = 'sEdit7'
    end
    object edt_DanwiSuryangDanwi: TsEdit
      Tag = 2
      Left = 96
      Top = 379
      Width = 41
      Height = 23
      AutoSelect = False
      CharCase = ecUpperCase
      MaxLength = 3
      TabOrder = 11
      OnDblClick = edt_SuDanwiDblClick
      OnExit = edt_SuDanwiExit
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45800#50948#49688#47049#45800#50948
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_Detail3: TsEdit
      Left = 162
      Top = 379
      Width = 235
      Height = 23
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 12
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Caption = 'sEdit7'
    end
    object sCurrencyEdit1: TsCurrencyEdit
      Left = 96
      Top = 331
      Width = 105
      Height = 23
      AutoSize = False
      TabOrder = 9
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45800#44032
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      DecimalPlaces = 3
      DisplayFormat = '###,###,##0.000;-###,###,##0.000;0'
    end
    object sCurrencyEdit2: TsCurrencyEdit
      Left = 96
      Top = 355
      Width = 105
      Height = 23
      AutoSize = False
      TabOrder = 10
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45800#50948#49688#47049
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      DecimalPlaces = 3
      DisplayFormat = '###,###,##0.000;-###,###,##0.000;0'
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 400
    Top = 392
  end
  object qryIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'HSCODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'FNAME'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'SNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'MSIZE'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'QTYC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'PRICEG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'PRICE'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'QTY_U'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'QTY_UG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [ITEM_CODE]('
      '[CODE]'
      ',[HSCODE]'
      ',[FNAME]'
      ',[SNAME]'
      ',[MSIZE]'
      ',[QTYC]'
      ',[PRICEG]'
      ',[PRICE]'
      ',[QTY_U]'
      ',[QTY_UG]'
      ')'
      'VALUES('
      ' :CODE'
      ', :HSCODE'
      ', :FNAME'
      ', :SNAME'
      ', :MSIZE'
      ', :QTYC'
      ', :PRICEG'
      ', :PRICE'
      ', :QTY_U'
      ', :QTY_UG'
      ')')
    Left = 264
    Top = 72
  end
  object qryMod: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'HSCODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'FNAME'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'SNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'MSIZE'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'QTYC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'PRICEG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'PRICE'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'QTY_U'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'QTY_UG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [ITEM_CODE]'
      'SET'#9'[HSCODE]'#9'= :HSCODE'
      #9',[FNAME]'#9'= :FNAME'
      #9',[SNAME]'#9'= :SNAME'
      #9',[MSIZE]'#9'= :MSIZE'
      #9',[QTYC]'#9#9'= :QTYC'
      #9',[PRICEG]'#9'= :PRICEG'
      #9',[PRICE]'#9'= :PRICE'
      #9',[QTY_U]'#9'= :QTY_U'
      #9',[QTY_UG]'#9'= :QTY_UG'
      'WHERE [CODE] = :CODE')
    Left = 296
    Top = 72
  end
  object qryCheck: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'HSCODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'FNAME'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'SNAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'MSIZE'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'QTYC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'PRICEG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'PRICE'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'QTY_U'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 18
        Size = 19
        Value = Null
      end
      item
        Name = 'QTY_UG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [ITEM_CODE]('
      '[CODE]'
      ',[HSCODE]'
      ',[FNAME]'
      ',[SNAME]'
      ',[MSIZE]'
      ',[QTYC]'
      ',[PRICEG]'
      ',[PRICE]'
      ',[QTY_U]'
      ',[QTY_UG]'
      ')'
      'VALUES('
      ' :CODE'
      ', :HSCODE'
      ', :FNAME'
      ', :SNAME'
      ', :MSIZE'
      ', :QTYC'
      ', :PRICEG'
      ', :PRICE'
      ', :QTY_U'
      ', :QTY_UG'
      ')')
    Left = 264
    Top = 104
  end
  object qryDel: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM [ITEM_CODE]'
      'WHERE [CODE] = :CODE')
    Left = 296
    Top = 104
  end
end
