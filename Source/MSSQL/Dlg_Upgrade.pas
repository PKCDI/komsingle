unit Dlg_Upgrade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sLabel, sButton, sMemo,
  sGroupBox, ComCtrls, sPageControl,ADODB,DB;

type
  TDlg_Upgrade_frm = class(TChildForm_frm)
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sGroupBox1: TsGroupBox;
    sMemo1: TsMemo;
    sButton1: TsButton;
    sLabel1: TsLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure sTabSheet1Show(Sender: TObject);
  private
    { Private declarations }
    vSQL : TADOQuery;
    function EXISTS_CODE2NDM_PREFIX(sPrefix : string):Boolean;
  public
    { Public declarations }
  end;

var
  Dlg_Upgrade_frm: TDlg_Upgrade_frm;

implementation

uses ICON, MSSQL;

{$R *.dfm}

{ TDlg_Upgrade_frm }
function TDlg_Upgrade_frm.EXISTS_CODE2NDM_PREFIX(sPrefix: string): Boolean;
begin
  vSQL.Close;
  vSQL.SQL.Text := 'SELECT 1 FROM [CODE2NDM] WHERE Prefix ='+QuotedStr(sPrefix);
  vSQL.Open;

  Result := vSQL.RecordCount > 0;
end;

procedure TDlg_Upgrade_frm.FormCreate(Sender: TObject);
begin
  inherited;
  vSQL := TADOQuery.Create(nil);
  vSQL.Connection := DMMssql.KISConnect
end;

procedure TDlg_Upgrade_frm.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil(vSQL);
end;

procedure TDlg_Upgrade_frm.sTabSheet1Show(Sender: TObject);
begin
  inherited;
  sButton1.Enabled := not EXISTS_CODE2NDM_PREFIX('APPSPC����');
  sLabel1.Enabled := not sButton1.Enabled;
end;

end.
