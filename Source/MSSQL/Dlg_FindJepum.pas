unit Dlg_FindJepum;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, sSkinProvider, ExtCtrls, sPanel, DB, Grids,
  DBGrids, acDBGrid, StdCtrls, sButton, sEdit, sComboBox, DBCtrls, sDBMemo,
  Mask, sDBEdit;

type
  TDlg_FindJepum_frm = class(TDialogParent_frm)
    sComboBox1: TsComboBox;
    sEdit1: TsEdit;
    sButton1: TsButton;
    sDBGrid1: TsDBGrid;
    dsITEM: TDataSource;
    sDBEdit1: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBEdit2: TsDBEdit;
    sDBEdit3: TsDBEdit;
    sDBMemo2: TsDBMemo;
    sDBEdit4: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    sButton2: TsButton;
    sButton3: TsButton;
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure DatasetRefresh;
  public
    { Public declarations }
    function Run(CODE : String):TModalResult;
    function isValidCode(Code : String):boolean;
  end;

var
  Dlg_FindJepum_frm: TDlg_FindJepum_frm;

implementation

uses CodeContents;

{$R *.dfm}

{ TDlg_FindJepum_frm }

procedure TDlg_FindJepum_frm.DatasetRefresh;
begin
  with DMCodeContents do
  begin
    dsITEM.DataSet.Close;
    dsITEM.DataSet.Open;
  end;
end;

function TDlg_FindJepum_frm.isValidCode(Code: String): boolean;
begin
  DatasetRefresh;
  Result := dsITEM.DataSet.Locate('CODE',Code,[]);
end;

function TDlg_FindJepum_frm.Run(CODE: String): TModalResult;
begin
  Result := mrCancel;
  DatasetRefresh;

  isValidCode(CODE);

  Result := Self.ShowModal;
end;

procedure TDlg_FindJepum_frm.sDBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF KEY = VK_RETURN THEN ModalResult := mrOk;  
end;

procedure TDlg_FindJepum_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

end.
