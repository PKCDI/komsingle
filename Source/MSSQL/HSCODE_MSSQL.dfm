inherited HSCODE_MSSQL_frm: THSCODE_MSSQL_frm
  Tag = 103
  Left = 733
  Top = 154
  Caption = 'HS'#53076#46300
  ClientHeight = 631
  ClientWidth = 789
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 789
    Height = 62
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton3: TsSpeedButton
      Left = 377
      Top = 1
      Width = 11
      Height = 60
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Close: TsSpeedButton
      Left = 733
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #45803#44592
      Layout = blGlyphTop
      Spacing = 0
      OnClick = Btn_CloseClick
      Align = alRight
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System32
      ImageIndex = 20
      Reflected = True
    end
    object sSpeedButton4: TsSpeedButton
      Left = 388
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #52636#47141
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Visible = False
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System32
      ImageIndex = 21
      Reflected = True
    end
    object sSpeedButton5: TsSpeedButton
      Left = 722
      Top = 1
      Width = 11
      Height = 60
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSplitter2: TsSplitter
      Left = 1
      Top = 1
      Width = 200
      Height = 60
      Cursor = crHSplit
      AutoSnap = False
      Enabled = False
      SkinData.SkinSection = 'SPLITTER'
    end
    object sLabel1: TsLabel
      Left = 71
      Top = 21
      Width = 71
      Height = 21
      Alignment = taCenter
      Caption = 'H S '#53076' '#46300
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object Btn_New: TsSpeedButton
      Left = 212
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #49888#44508
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = Btn_NewClick
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System32
      ImageIndex = 5
      Reflected = True
    end
    object Btn_Modify: TsSpeedButton
      Tag = 1
      Left = 267
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #49688#51221
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = Btn_NewClick
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System32
      ImageIndex = 8
      Reflected = True
    end
    object Btn_Del: TsSpeedButton
      Tag = 2
      Left = 322
      Top = 1
      Width = 55
      Height = 60
      Cursor = crHandPoint
      Caption = #49325#51228
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = Btn_NewClick
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System32
      ImageIndex = 19
      Reflected = True
    end
    object sSpeedButton1: TsSpeedButton
      Left = 201
      Top = 1
      Width = 11
      Height = 60
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
  end
  object sPanel7: TsPanel [1]
    Left = 0
    Top = 62
    Width = 789
    Height = 32
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object sEdit1: TsEdit
      Left = 94
      Top = 6
      Width = 121
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnKeyUp = sEdit1KeyUp
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
    end
    object sBitBtn1: TsBitBtn
      Left = 216
      Top = 6
      Width = 47
      Height = 21
      Caption = #51312#54924
      TabOrder = 1
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sComboBox1: TsComboBox
      Left = 4
      Top = 6
      Width = 89
      Height = 21
      Alignment = taLeftJustify
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ItemHeight = 15
      ItemIndex = 0
      ParentFont = False
      TabOrder = 2
      Text = #53076#46300
      Items.Strings = (
        #53076#46300
        #45824#54364#54408#47749)
    end
  end
  object sDBGrid1: TsDBGrid [2]
    Left = 0
    Top = 94
    Width = 789
    Height = 139
    Align = alTop
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #53076#46300
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'REPNAME'
        Title.Caption = #45824#54364#54408#47749
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Title.Caption = #54408#47749
        Width = 64
        Visible = True
      end>
  end
  object sPanel2: TsPanel [3]
    Left = 0
    Top = 233
    Width = 789
    Height = 398
    Align = alClient
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton7: TsSpeedButton
      Left = 130
      Top = 376
      Width = 23
      Height = 21
      Enabled = False
      SkinData.SkinSection = 'SPEEDBUTTON'
      Images = DMICON.System16
      ImageIndex = 0
    end
    object sDBEdit1: TsDBEdit
      Left = 100
      Top = 16
      Width = 169
      Height = 21
      Color = clBtnFace
      DataField = 'CODE'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #53076#46300
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit2: TsDBEdit
      Left = 100
      Top = 47
      Width = 317
      Height = 21
      Color = clBtnFace
      DataField = 'REPNAME'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45824#54364#54408#47749
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit3: TsDBEdit
      Left = 100
      Top = 79
      Width = 317
      Height = 21
      Color = clBtnFace
      DataField = 'NAME'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #54408#47749
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [CODE]'
      '            ,[REPNAME]'
      '            ,[NAME]'
      '  FROM [dbo].[HS_CODE]')
    Left = 392
    Top = 232
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 424
    Top = 232
  end
end
