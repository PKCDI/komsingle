unit FINBIL_N_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FINBIL_N, DB, ADODB, sSkinProvider, StdCtrls, sGroupBox,
  DBCtrls, sDBMemo, sDBEdit, Grids, DBGrids, acDBGrid, sButton, sEdit,
  Mask, sMaskEdit, ComCtrls, sPageControl, Buttons, sSpeedButton, sLabel,
  ExtCtrls, sSplitter, sPanel, StrUtils, DateUtils;

type
  TFINBIL_N_MSSQL_frm = class(TFINBIL_frm)
    procedure qryDetailAfterScroll(DataSet: TDataSet);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure Btn_PrintClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_CloseClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    FSQL : String;
    procedure ChangeDisplayFormat;
    procedure ReadList;
    procedure ReadDetail;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FINBIL_N_MSSQL_frm: TFINBIL_N_MSSQL_frm;

implementation

uses Commonlib, FINBIL_PRINT, QuickRpt, Preview;

{$R *.dfm}

procedure TFINBIL_N_MSSQL_frm.ChangeDisplayFormat;
begin
  //Master
  ChangeFormatFORDanwi(qrylistPAYORD1C.AsString,qryListPAYORD1);
  ChangeFormatFORDanwi(qrylistPAYORD2C.AsString,qryListPAYORD2);
  ChangeFormatFORDanwi(qrylistPAYORD3C.AsString,qryListPAYORD3);
  ChangeFormatFORDanwi(qrylistPAYORD4C.AsString,qryListPAYORD4);
  ChangeFormatFORDanwi(qrylistPAYORD5C.AsString,qryListPAYORD5);

  ChangeFormatFORDanwi(qrylistFINAMT1C.AsString,qrylistFINAMT1);
  ChangeFormatFORDanwi(qrylistFINAMT2C.AsString,qrylistFINAMT2);

  //Detail
  ChangeFormatFORDanwi(qryDetailAMT1C.AsString,qryDetailAMT1);
  ChangeFormatFORDanwi(qryDetailAMT2C.AsString,qryDetailAMT2);
end;

procedure TFINBIL_N_MSSQL_frm.ReadDetail;
begin
  with qryDetail do
  begin
    Close;
    Parameters.ParamByName('KEYY').Value := qryListMAINT_NO.AsString;
    Open;
  end;
end;

procedure TFINBIL_N_MSSQL_frm.qryDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ChangeDisplayFormat;
end;

procedure TFINBIL_N_MSSQL_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  sPanel9.Caption := IntToStr(qryList.RecNo) + ' | ' + IntToStr(qryList.RecordCount);
  ReadDetail;
end;

procedure TFINBIL_N_MSSQL_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQl.Text := FSQL;
    SQL.Add('WHERE DATEE between' + QuotedStr(sMaskEdit1.Text) + 'AND' + QuotedStr(sMaskEdit2.Text));
    IF Trim(sEdit1.Text) <> '' Then
      SQL.Add('AND Maint_No LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    SQL.Add('ORDER BY DATEE DESC, MAINT_NO DESC');
    Open;
  end;
end;

procedure TFINBIL_N_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
  sMaskEdit1.Text := '20000101';
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD',EndOfTheMonth(Now));
  ReadList;
end;

procedure TFINBIL_N_MSSQL_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF qryList.RecordCount > 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
    end;
  end;
end;

procedure TFINBIL_N_MSSQL_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TFINBIL_N_MSSQL_frm.Btn_PrintClick(Sender: TObject);
begin
  inherited;

  if qryList.RecordCount = 0 then Exit;

  Preview_frm := TPreview_frm.Create(Self);
  FINBIL_PRINT_frm := TFINBIL_PRINT_frm.Create(Self);
  try
    FINBIL_PRINT_frm.RunOnlyDB(qryListMAINT_NO.AsString);
    Preview_frm.Report := FINBIL_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(FINBIL_PRINT_frm);
  end;
end;

procedure TFINBIL_N_MSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  FINBIL_N_MSSQL_frm := nil;
end;

procedure TFINBIL_N_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TFINBIL_N_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  sPageControl1.ActivePageIndex := 0;  
  ReadOnlyControl(spanel7,false); 
  ReadOnlyControl(spanel4,false);
  ReadOnlyControl(spanel6,false);
end;

procedure TFINBIL_N_MSSQL_frm.FormActivate(Sender: TObject);
var
  BMK : Pointer;
begin
  inherited;
  BMK := qryList.GetBookmark;
  ReadList;
  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);
end;

end.
