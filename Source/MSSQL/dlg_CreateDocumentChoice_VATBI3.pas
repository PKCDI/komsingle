unit dlg_CreateDocumentChoice_VATBI3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, ExtCtrls, sBevel, StdCtrls, sButton, sLabel,
  sSkinProvider, sPanel;

type
  Tdlg_CreateDocumentChoice_VATBI3_frm = class(TDialogParent_frm)
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sLabel1: TsLabel;
    sButton4: TsButton;
    sButton1: TsButton;
    sButton2: TsButton;
    sBevel1: TsBevel;
    procedure sButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_CreateDocumentChoice_VATBI3_frm: Tdlg_CreateDocumentChoice_VATBI3_frm;

implementation

uses MSSQL;

{$R *.dfm}

procedure Tdlg_CreateDocumentChoice_VATBI3_frm.sButton2Click(Sender: TObject);
begin
  inherited;
   if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans ;
  Close;
end;

end.
