inherited Bank_MSSQL_frm: TBank_MSSQL_frm
  Left = 1119
  Top = 78
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel1: TsPanel
    Height = 53
    inherited sSpeedButton3: TsSpeedButton
      Height = 51
    end
    inherited Btn_Close: TsSpeedButton
      Height = 51
      OnClick = Btn_CloseClick
      Images = DMICON.System24
    end
    inherited sSpeedButton4: TsSpeedButton
      Height = 51
      Images = DMICON.System24
    end
    inherited sSpeedButton5: TsSpeedButton
      Height = 51
    end
    inherited sSplitter2: TsSplitter
      Height = 51
    end
    inherited sLabel1: TsLabel
      Left = 10
      Top = 16
    end
    inherited Btn_New: TsSpeedButton
      Height = 51
      OnClick = Btn_NewClick
      Images = DMICON.System24
    end
    inherited Btn_Modify: TsSpeedButton
      Height = 51
      OnClick = Btn_NewClick
      Images = DMICON.System24
    end
    inherited Btn_Del: TsSpeedButton
      Height = 51
      OnClick = Btn_NewClick
      Images = DMICON.System24
    end
    inherited sSpeedButton1: TsSpeedButton
      Height = 51
    end
    object sButton1: TsButton
      Left = 520
      Top = 4
      Width = 72
      Height = 46
      Caption = #44592#51316#51008#54665#13#10#44032#51256#50724#44592
      TabOrder = 0
      OnClick = sButton1Click
    end
  end
  inherited sPanel7: TsPanel
    Top = 53
    inherited sBitBtn1: TsBitBtn
      OnClick = sBitBtn1Click
    end
    inherited sComboBox1: TsComboBox
      ItemIndex = 1
      Text = #51008#54665#47749
    end
  end
  inherited sDBGrid1: TsDBGrid
    Top = 85
    Height = 483
    DataSource = dsList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    Columns = <
      item
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #51008#54665#53076#46300
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BankName1'
        Title.Caption = #51008#54665#47749'1'
        Width = 179
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BankBranch1'
        Title.Caption = #51648#51216#47749
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BankName2'
        Title.Caption = #51008#54665#47749'2'
        Width = 179
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BankBranch2'
        Title.Caption = #51648#51216#47749'2'
        Width = 100
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 0
    Top = 96
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [CODE]'
      '      ,[ENAME1] as BankName1'
      '      ,[ENAME2] as BankName2'
      '      ,[ENAME3] as BankBranch1'
      '      ,[ENAME4] as BankBranch2'
      '      ,[ENAME5] as BankAccount'
      '      ,[ENAME6] as BankPW'
      '  FROM [BANKCODE]')
    Left = 32
    Top = 96
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 64
    Top = 96
  end
  object Query1: TQuery
    Top = 128
  end
end
