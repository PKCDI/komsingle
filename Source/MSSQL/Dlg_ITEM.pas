unit Dlg_ITEM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, StdCtrls, sMemo, sEdit, sButton, sSkinProvider, ExtCtrls,
  sPanel, Buttons, sSpeedButton, TypeDefine, ADODB, DB;

type
  TDlg_ITEM_frm = class(TDialogParent_frm)
    sButton2: TsButton;
    sButton3: TsButton;
    edt_CODE: TsEdit;
    edt_HS: TsEdit;
    edt_ShortName: TsEdit;
    Memo_FullName: TsMemo;
    Memo_Size: TsMemo;
    edt_detail1: TsEdit;
    edt_SuDanwi: TsEdit;
    btn_Find1: TsSpeedButton;
    edt_DangaDanwi: TsEdit;
    Btn_Find2: TsSpeedButton;
    edt_Detail2: TsEdit;
    edt_DanwiSuryangDanwi: TsEdit;
    Btn_Find3: TsSpeedButton;
    edt_Detail3: TsEdit;
    sCurrencyEdit1: TsCurrencyEdit;
    sCurrencyEdit2: TsCurrencyEdit;
    qryIns: TADOQuery;
    qryMod: TADOQuery;
    qryCheck: TADOQuery;
    qryDel: TADOQuery;
    procedure edt_SuDanwiExit(Sender: TObject);
    procedure edt_SuDanwiDblClick(Sender: TObject);
    procedure btn_Find1Click(Sender: TObject);
    procedure edt_HSEnter(Sender: TObject);
    procedure edt_HSExit(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
  private
    { Private declarations }
    FProgramControlType : TProgramControlType;
    FFields : TFields;
    procedure SetDanwiCODE(var objEdit : TsEdit);
    procedure SetData;
    procedure SetParamAndRun(qry : TADOQuery);
    procedure DeleteData(CODE: String);
    function CheckOverlab:Boolean;
  public
    { Public declarations }
    function Run(PType : TProgramControlType;UserFields : TFields):TModalResult;
  end;

var
  Dlg_ITEM_frm: TDlg_ITEM_frm;

implementation

uses dlg_FindNation, Commonlib, MSSQL;

{$R *.dfm}

procedure TDlg_ITEM_frm.SetDanwiCODE(var objEdit : TsEdit);
var
  TempPrefix : String;
begin
  dlg_FindNation_frm := Tdlg_FindNation_frm.Create(Self);

  Case objEdit.Tag of
    0,2: TempPrefix := '단위';
    1: TempPrefix := '통화';
  end;

  try
    IF (not dlg_FindNation_frm.isValidData(TempPrefix,objEdit.Text)) {OR (objEdit.Text = '' )} Then
    begin
      IF dlg_FindNation_frm.RUN(TempPrefix,'') = mrok then
      begin
        objEdit.Text := dlg_FindNation_frm.SelectCODE;
        Case objEdit.Tag of
          0: edt_detail1.Text := dlg_FindNation_frm.SelectNAME;
          1: edt_detail2.Text := dlg_FindNation_frm.SelectNAME;
          2: edt_detail3.Text := dlg_FindNation_frm.SelectNAME;
        end;
      end
      else
      begin
        Case objEdit.Tag of
          0: edt_detail1.Text := 'NOT FOUND DATA!';
          1: edt_detail2.Text := 'NOT FOUND DATA!';
          2: edt_detail3.Text := 'NOT FOUND DATA!';
        end;
      end;

    end
    else
    begin
        Case objEdit.Tag of
          0: edt_detail1.Text := dlg_FindNation_frm.GetContent(TempPrefix,objEdit.Text);
          1: edt_detail2.Text := dlg_FindNation_frm.GetContent(TempPrefix,objEdit.Text);
          2: edt_detail3.Text := dlg_FindNation_frm.GetContent(TempPrefix,objEdit.Text);
        end;
    end;
  finally
    FreeAndNil(dlg_FindNation_frm);
  end;
end;

procedure TDlg_ITEM_frm.edt_SuDanwiExit(Sender: TObject);
begin
  inherited;

  IF Trim((sender as TsEdit).Text) = '' Then
  begin
    Case (Sender as TsEdit).Tag of
      0: edt_detail1.Text := '';
      1: edt_detail2.Text := '';
      2: edt_detail3.Text := '';
    end;
  end
  else
  begin
    Case (Sender as TsEdit).Tag of
      0: SetDanwiCODE(edt_suDanwi);
      1: SetDanwiCODE(edt_DangaDanwi);
      2: SetDanwiCODE(edt_DanwiSuryangDanwi);
    end;
  end;
end;

procedure TDlg_ITEM_frm.edt_SuDanwiDblClick(Sender: TObject);
var
  TempPrefix : String;
begin
  dlg_FindNation_frm := Tdlg_FindNation_frm.Create(Self);

  Case (Sender as TsEdit).Tag of
    0,2: TempPrefix := '단위';
    1: TempPrefix := '통화';
  end;

  try
    IF dlg_FindNation_frm.RUN(TempPrefix,'') = mrok then
    begin
      (Sender as TsEdit).Text := dlg_FindNation_frm.SelectCODE;
      Case (Sender as TsEdit).Tag of
        0: edt_detail1.Text := dlg_FindNation_frm.SelectNAME;
        1: edt_detail2.Text := dlg_FindNation_frm.SelectNAME;
        2: edt_detail3.Text := dlg_FindNation_frm.SelectNAME;
      end;
    end;
  finally
    FreeAndNil(dlg_FindNation_frm);
  end;

end;

procedure TDlg_ITEM_frm.btn_Find1Click(Sender: TObject);
begin
  inherited;
  Case (Sender as TsSpeedButton).Tag of
    0: edt_SuDanwiDblClick(edt_SuDanwi);
    1: edt_SuDanwiDblClick(edt_DangaDanwi);
    2: edt_SuDanwiDblClick(edt_DanwiSuryangDanwi);
  end;
end;

function TDlg_ITEM_frm.Run(PType: TProgramControlType;
  UserFields: TFields): TModalResult;
begin
  FProgramControlType := PType;
  FFields := UserFields;

  edt_CODE.Enabled := FProgramControlType = ctInsert;

  Result := mrCancel;

  CASE FProgramControlType OF
    ctInsert :
    begin
      Self.Caption := '품명 및 규격코드 - [신규]';
    end;

    ctModify :
    begin
      Self.Caption := '품명 및 규격코드 - [수정]';
      SetData;
    end;

    ctDelete :
    begin
      IF ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'[코드] : '+FFields.FieldByName('CODE').AsString+#13#10'삭제한 데이터를 복구가 불가능 합니다') Then
      begin
        DeleteData(FFields.FieldByName('CODE').AsString);
        Result := mrOk;
        Exit;
      end
      else
      begin
        Result := mrCancel;
        Exit;
      end;
    end;
  end;

  iF Self.ShowModal = mrOk then
  begin
    Case FProgramControlType of
      ctInsert : SetParamAndRun(qryIns);
      ctModify : SetParamAndRun(qryMod);
    end;
    Result := mrOk;
  end

end;

procedure TDlg_ITEM_frm.SetData;
begin
  dlg_FindNation_frm := Tdlg_FindNation_frm.Create(Self);
  try
    edt_CODE.Text := FFields.FieldByName('CODE').AsString;
    //세번부호
    edt_HS.Text := HSCodeFormat(FFields.FieldByName('HSCODE').AsString);
    //짧은이름
    edt_ShortName.Text := FFields.FieldByName('SNAME').AsString;
    //긴이름
    Memo_FullName.Lines.Text := FFields.FieldByName('FNAME').AsString;
    //규격
    Memo_Size.Lines.Text := FFields.FieldByName('MSIZE').AsString;
    //수량단위
    edt_SuDanwi.Text := FFields.FieldByName('QTYC').AsString;
    edt_detail1.Text := dlg_FindNation_frm.GetContent('단위',edt_SuDanwi.Text);
    //단가단위
    edt_DangaDanwi.Text := FFields.FieldByName('PRICEG').AsString;
    edt_detail2.Text := dlg_FindNation_frm.GetContent('통화',edt_DangaDanwi.Text);
    //단가
    sCurrencyEdit1.Value := FFields.FieldByName('PRICE').AsCurrency;
    //단위수량
    sCurrencyEdit2.Value := FFields.FieldByName('QTY_U').AsCurrency;
    //단위수량단위
    edt_DanwiSuryangDanwi.Text := FFields.FieldByName('QTY_UG').AsString;
    edt_detail3.Text := dlg_FindNation_frm.GetContent('단위',edt_DanwiSuryangDanwi.Text);
  finally
    FreeAndNil(dlg_FindNation_frm);
  end;
end;

procedure TDlg_ITEM_frm.edt_HSEnter(Sender: TObject);
begin
  inherited;
  edt_HS.Text := HSCodeFormat(edt_HS.Text,False);
end;

procedure TDlg_ITEM_frm.edt_HSExit(Sender: TObject);
begin
  inherited;
  edt_HS.Text := HSCodeFormat(edt_HS.Text);
end;

procedure TDlg_ITEM_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_ITEM_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  IF CheckOverlab AND (FProgramControlType = ctInsert) Then
    raise Exception.Create('코드 ['+edt_CODE.Text+']는 이미 사용중인 코드입니다.');

  ModalResult := mrOk;
end;

procedure TDlg_ITEM_frm.SetParamAndRun(qry: TADOQuery);
begin
  with qry do
  begin
    Parameters.ParamByName('CODE'  ).Value := edt_CODE.Text;
    Parameters.ParamByName('HSCODE').Value := HSCodeFormat(edt_HS.Text,False);
    Parameters.ParamByName('FNAME' ).Value := Memo_FullName.Text;
    Parameters.ParamByName('SNAME' ).Value := edt_ShortName.Text;
    Parameters.ParamByName('MSIZE' ).Value := Memo_Size.Text;
    Parameters.ParamByName('QTYC'  ).Value := edt_SuDanwi.Text;
    Parameters.ParamByName('PRICEG').Value := edt_DangaDanwi.Text;
    Parameters.ParamByName('PRICE' ).Value := sCurrencyEdit1.Value;
    Parameters.ParamByName('QTY_U' ).Value := sCurrencyEdit2.Value;
    Parameters.ParamByName('QTY_UG').Value := edt_DanwiSuryangDanwi.Text;
    ExecSQL;
  end;
end;

function TDlg_ITEM_frm.CheckOverlab: Boolean;
begin
  with qryCheck do
  begin
    Close;
    SQL.Text := 'SELECT 1 FROM [ITEM_CODE] WHERE [CODE] = '+QuotedStr(edt_CODE.Text);
    Open;

    Result := qryCheck.RecordCount > 0;
  end;
end;

procedure TDlg_ITEM_frm.DeleteData(CODE: String);
begin
  with qryDel do
  begin
    Close;
    Parameters.ParamByName('CODE').Value := CODE;
    ExecSQL;
  end;
end;

end.










