inherited Dlg_Bank_frm: TDlg_Bank_frm
  Left = 806
  Top = 362
  Caption = ''
  ClientHeight = 224
  ClientWidth = 334
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 334
    Height = 224
    object Shape1: TShape
      Left = 10
      Top = 160
      Width = 314
      Height = 1
      Brush.Color = 14540252
      Pen.Color = 14540252
    end
    object edt_BankCode: TsEdit
      Left = 72
      Top = 16
      Width = 65
      Height = 23
      CharCase = ecUpperCase
      MaxLength = 4
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #51008#54665#53076#46300
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_BankName1: TsEdit
      Left = 72
      Top = 48
      Width = 221
      Height = 23
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #51008#54665#47749
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_BankBranch1: TsEdit
      Left = 72
      Top = 104
      Width = 221
      Height = 23
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #51648#51216#47749
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_BankName2: TsEdit
      Left = 72
      Top = 72
      Width = 221
      Height = 23
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
    end
    object edt_BankBranch2: TsEdit
      Left = 72
      Top = 128
      Width = 221
      Height = 23
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
    end
    object sButton2: TsButton
      Left = 89
      Top = 172
      Width = 75
      Height = 34
      Caption = #51200#51109
      TabOrder = 5
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 0
    end
    object sButton3: TsButton
      Left = 169
      Top = 172
      Width = 75
      Height = 34
      Cancel = True
      Caption = #52712#49548
      TabOrder = 6
      TabStop = False
      OnClick = sButton3Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 17
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 400
    Top = 16
  end
  object qryIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ENAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [BANKCODE]'
      '(CODE, ENAME1, ENAME2,ENAME3,ENAME4,ENAME5,ENAME6 )'
      'VALUES(:CODE, :ENAME1, :ENAME2, :ENAME3, :ENAME4, NULL, NULL )')
    Left = 224
    Top = 8
  end
  object qryMod: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'ENAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'CODE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [BANKCODE]'
      'SET ENAME1 = :ENAME1'
      '   ,ENAME2 = :ENAME2'
      '   ,ENAME3 = :ENAME3'
      '   ,ENAME4 = :ENAME4'
      '   ,ENAME5 = NULL'
      '   ,ENAME6 = NULL'
      'WHERE [CODE] = :CODE')
    Left = 256
    Top = 8
  end
  object qryDel: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DELETE FROM [BANKCODE]'
      'WHERE [CODE] = :CODE')
    Left = 288
    Top = 8
  end
end
