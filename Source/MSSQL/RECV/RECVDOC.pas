unit RECVDOC;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Dialogs,
  StdCtrls, ExtCtrls, FileCtrl, StrUtils, EDIFACT_RECV, SQLCreator, ADODB, Clipbrd;

Type
  TINF700_NEW = class(TEDIFACT_RECV)
    public
      procedure DefineList; override;
      function ParseData:Boolean; override;
  end;

  TINF707_NEW = class(TEDIFACT_RECV)
    public
      procedure DefineList; override;
      function ParseData:Boolean; override;
  end;

  TCREADV_NEW = class(TEDIFACT_RECV)
    public
      procedure DefineList; override;
      function ParseData:Boolean; override;
  end;

  TATTNTC_NEW = class(TEDIFACT_RECV)
    public
      procedure DefineList; override;
      function ParseData: Boolean; override;
  end;

  TLDANTC_NEW = class(TEDIFACT_RECV)
    public
      procedure DefineList; override;
      function ParseData: Boolean; override;
  end;

  TSPCNTC_NEW = class(TEDIFACT_RECV)
    public
      procedure DefineList; override;
      function ParseData: Boolean; override;
  end;

  //세금계산서
  TVATBIL_NEW = class(TEDIFACT_RECV)
    public
      procedure DefineList; override;
      function ParseData: Boolean; override;
  end;

  //수입화물선취보증서
  TLOGUAR_NEW = class(TEDIFACT_RECV)
    public
      procedure DefineList; override;
      function ParseData: Boolean; override;
  end;

  //출금통지서
  TDEBADV_NEW = class(TEDIFACT_RECV)
  public
    procedure DefineList; override;
    function ParseData: Boolean; override;
  end;

  // 타행경유신용장통지서
  TADV710_NEW = class(TEDIFACT_RECV)
  public
    procedure DefineList; override;
    function ParseData: Boolean; override;
  end;

procedure RunSQL(sSQL : string);
//function INF700_NEW(UNH_FILE_PATH : string;bMigData: Boolean=False):Boolean;
//function INF707_NEW(UNH_FILE_PATH : string;bMigData: Boolean=False):Boolean;

var
  INF700_NEW : TINF700_NEW;
  INF707_NEW : TINF707_NEW;
  CREADV_NEW : TCREADV_NEW;
  ATTNTC_NEW : TATTNTC_NEW;
  LDANTC_NEW : TLDANTC_NEW;
 
implementation

uses
  MSSQL, VarDefine;

procedure RunSQL(sSQL : string);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := sSQL;
//      ShowMessage(sSQL);
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

{ TINF700_NEW }

procedure TINF700_NEW.DefineList;
var
  MAINT_NO, MESSAGE1, MESSAGE2 : String;
  MSEQ : String;
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM','10') THEN
      BEGIN
  //      ShowMessage( A.SEGDATA.Items[0] );
        MAINT_NO := SEGDATA.Items[1];
        IF POS('_', MAINT_NO) > 0 Then
        begin
          MSEQ := MidStr(MAINT_NO, POS('_', MAINT_NO)+1, Length(MAINT_NO)- POS('_', MAINT_NO));
          MAINT_NO := LeftStr(MAINT_NO, POS('_', MAINT_NO)-1);
        end
        else
          MSEQ := '1';
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
      end;

    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'INF700');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', MSEQ, vtInteger);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', '');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'INF700');
      RunSQL(SC.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;

function TINF700_NEW.ParseData: Boolean;
var
  MAINT_NO, MESSAGE1, MESSAGE2, MSEQ : String;
  A  : TEDIFACT_RECV;
  SC1, SC2, SC3, SC4 : TSQLCreate;
  i : integer;
  QUA, SEGVALUE : String;
  TMP_TXT : TStringList;
  TMP_FTX : array [0..11] of string;
  INNER_COUNT : Integer;
  TMP_STR : String;
  PCD_POINT : integer;
begin
  Result := True;

  TMP_TXT := TStringList.Create;
  SC1 := TSQLCreate.Create;
  SC2 := TSQLCreate.Create;
  SC3 := TSQLCreate.Create;
  SC4 := TSQLCreate.Create;

  try
    try
      IF FindSegment('BGM','10') THEN
      BEGIN
  //      ShowMessage( SEGDATA.Items[0] );
        MAINT_NO := SEGDATA.Items[1];
        IF POS('_', MAINT_NO) > 0 Then
        begin
          MSEQ := MidStr(MAINT_NO, POS('_', MAINT_NO)+1, Length(MAINT_NO)- POS('_', MAINT_NO));
          MAINT_NO := LeftStr(MAINT_NO, POS('_', MAINT_NO)-1);
        end
        else
          MSEQ := '1';
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
      end;

    //------------------------------------------------------------------------------
    // 중복확인
    //------------------------------------------------------------------------------
      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('INF700_1');
      SC1.ADDWhere('MAINT_NO', MAINT_NO);
      SC1.ADDWhere('MSEQ', MSEQ, vtInteger);
      RunSQL(SC1.CreateSQL);

      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('INF700_2');
      SC1.ADDWhere('MAINT_NO', MAINT_NO);
      SC1.ADDWhere('MSEQ', MSEQ, vtinteger);
      RunSQL(SC1.CreateSQL);

      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('INF700_3');
      SC1.ADDWhere('MAINT_NO', MAINT_NO);
      SC1.ADDWhere('MSEQ', MSEQ, vtinteger);
      RunSQL(SC1.CreateSQL);

      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('INF700_4');
      SC1.ADDWhere('MAINT_NO', MAINT_NO);
      SC1.ADDWhere('MSEQ', MSEQ, vtinteger);
      RunSQL(SC1.CreateSQL);

    //------------------------------------------------------------------------------
    // 파싱
    //------------------------------------------------------------------------------
      SC1.DMLType := dmlInsert;
      SC2.DMLType := dmlInsert;
      SC3.DMLType := dmlInsert;
      SC4.DMLType := dmlInsert;

      SC1.SQLHeader('INF700_1');
      SC2.SQLHeader('INF700_2');
      SC3.SQLHeader('INF700_3');
      SC4.SQLHeader('INF700_4');

      SC1.ADDValue('MAINT_NO', MAINT_NO);
      SC2.ADDValue('MAINT_NO', MAINT_NO);
      SC3.ADDValue('MAINT_NO', MAINT_NO);
      SC4.ADDValue('MAINT_NO', MAINT_NO);

      SC1.ADDValue('MSEQ', MSEQ, vtInteger);
      SC2.ADDValue('MSEQ', MSEQ, vtInteger);
      SC3.ADDValue('MSEQ', MSEQ, vtInteger);
      SC4.ADDValue('MSEQ', MSEQ, vtInteger);

      SC1.ADDValue('MESSAGE1', MESSAGE1);
      SC1.ADDValue('MESSAGE2', MESSAGE2);
      SC1.ADDValue('USER_ID', LoginData.sID);
      SC1.ADDValue('DATEE', FormatDateTime('YYYYMMDD', Now));
      //신용장 종류
      IF FindSegment('BUS','11', 1) THEN
      BEGIN
        SC1.ADDValue('DOC_CD', SEGDATA.Items[0]);
      end;

      for i := 1 to 2 do
      begin
        IF FindSegment('INP','12', 3) THEN
        BEGIN
          QUA := SEGDATA.Items[1];
          IF QUA = '5' Then
            SC1.ADDValue('IN_MATHOD', SEGDATA.Items[2]);
          IF QUA = '4' Then
            SC3.ADDValue('CONFIRMM', SEGDATA.Items[2]);
        end;
      end;

      TMP_TXT.Clear;
      for i := 1 to 6 do
      begin
        IF FindSegment('ALC','13', 2) Then
        begin
          QUA := SEGDATA.items[0];
          IF SEGDATA.Count = 1 then
            SC3.ADDValue('CHARGE', QUA);
          IF QUA = '2AH' Then
          begin                      
            TMP_TXT.Add(SEGDATA.items[1]);
          end;
        end;
      end;
      SC3.ADDValue('CHARGE_1', TMP_TXT.Text);

      //신용장번호 / 선통지참조사항
      for i := 1 to 2 do
      begin
        IF FindSegment('RFF','14', 2) Then
        begin
          QUA := SEGDATA.items[0];
          SEGVALUE := SEGDATA.items[1];
          IF QUA = 'AAC' Then
            SC1.ADDValue('CD_NO', SEGVALUE);
          IF QUA = '2AF' Then
            SC1.ADDValue('REF_PRE', SEGVALUE);
        end;
      end;

      for i := 1 to 2 do
      begin
        IF FindSegment('DTM', '15', 3) then
        begin
          QUA := SEGDATA.items[0];
          SEGVALUE := SEGDATA.items[1];
          //개설신청일자
          IF QUA = '2AA' Then
            SC1.ADDValue('APP_DATE', '20'+SEGVALUE);
          //개설일자
          IF QUA = '182' Then
            SC1.ADDValue('ISS_DATE', '20'+SEGVALUE);
        end;
      end;

      if FindSegment('TSR', '16' , 2) Then
      begin
        SC3.ADDValue('TSHIP', SEGDATA.items[0]);
        SC3.ADDValue('PSHIP', SEGDATA.items[1]);
      end;

      if FindSegment('PAI', '17' , 1) Then
      begin
        SC1.ADDValue('AD_PAY', SEGDATA.items[0]);
      end;

      //지급조건
      for i := 1 to 4 do
      begin
        IF FindSegment('PAT', '18', 3) then
        begin
          QUA := SEGDATA.items[0]+SEGDATA.items[1];
          IF QUA = '2AB2AO' THen
            SC2.ADDValue('DRAFT'+IntToStr(i), SEGDATA.items[2]);
          IF QUA = '62AM' Then
            SC4.ADDValue('MIX_PAY'+IntToStr(i), SEGDATA.items[2]);
          IF QUA = '42AN' Then
            SC3.ADDValue('DEF_PAY'+IntToStr(i), SEGDATA.items[2]);
        end;
      end;

      TMP_FTX[0] := '';
      TMP_FTX[1] := '';
      INNER_COUNT := 1;

      for i := 1 to 27 do
      begin
        IF FindSegment('FTX', '19', 6) Then
        begin
          QUA := SEGDATA.items[0];
          //기타정보를 표시
          IF QUA = 'ACB' Then
          begin
            SC1.ADDValue('AD_INFO1', SEGDATA.items[1]);
            SC1.ADDValue('AD_INFO2', SEGDATA.items[2]);
            SC1.ADDValue('AD_INFO3', SEGDATA.items[3]);
            SC1.ADDValue('AD_INFO4', SEGDATA.items[4]);
            SC1.ADDValue('AD_INFO5', SEGDATA.items[5]);
          end;
          //신용장 적용규칙
          IF QUA = '3FC' then
          begin
            SC4.ADDValue('APPLICABLE_RULES_1', SEGDATA.items[1]);
            SC4.ADDValue('APPLICABLE_RULES_2', SEGDATA.items[2]);
          end;
          //특별지급조건
          IF QUA = 'PCB' Then
          begin
            TMP_FTX[0] := TMP_FTX[0]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end;
          //지급/인수/매입은행에 대한 지시사항
          IF QUA = '2AB' Then
          begin
            TMP_FTX[1] := TMP_FTX[1]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10;
          end;
          //수신은행 앞 정보
          IF QUA = '2AE' Then
          begin
            SC4.ADDValue('SND_INFO'+IntToStr(INNER_COUNT), SEGDATA.items[1]);
            SC4.ADDValue('SND_INFO'+IntToStr(INNER_COUNT+1), SEGDATA.items[2]);
            SC4.ADDValue('SND_INFO'+IntToStr(INNER_COUNT+2), SEGDATA.items[3]);
            INNER_COUNT := INNER_COUNT+3;
          end;
        end;
      end;
      IF TMP_FTX[0] <> '' Then SC3.ADDValue('SPECIAL_PAY', TMP_FTX[0]);
      IF TMP_FTX[1] <> '' Then SC4.ADDValue('INSTRCT_1', TMP_FTX[1]);

      //------------------------------------------------------------------------------
      // FII그룹
      //------------------------------------------------------------------------------
      for i := 1 to 8 do
      begin
        IF FindSegment('FII', '1A', 8) Then
        begin
          QUA := SEGDATA.items[0];
          //MT700전문발신은행
          IF QUA = 'AW' Then
          begin
            //BIC CODE
            SC1.ADDValue('AP_BANK', SEGDATA.items[2]);
            SC1.ADDValue('AP_BANK1', LeftStr(SEGDATA.items[6], 35));
            SC1.ADDValue('AP_BANK2', Copy(SEGDATA.items[6], 36, 35));
            SC1.ADDValue('AP_BANK3', LeftStr(SEGDATA.items[7],35));
            SC1.ADDValue('AP_BANK4', Copy(SEGDATA.items[7], 36, 35));

            IF NextSegment(8, 'COM', '20') Then
            begin
              SC1.ADDValue('AP_BANK5', SEGDATA.items[0]);
            end;
          end;
          //MT700 전문수신은행
          IF QUA = '2AA' Then
          begin
            SC1.ADDValue('AD_BANK', SEGDATA.items[2]);
            SC1.ADDValue('AD_BANK1', LeftStr(SEGDATA.items[6], 35));
            SC1.ADDValue('AD_BANK2', Copy(SEGDATA.items[6], 36, 35));
            SC1.ADDValue('AD_BANK3', LeftStr(SEGDATA.items[7],35));
            SC1.ADDValue('AD_BANK4', Copy(SEGDATA.items[7], 36, 35));
          end;
          //개설의뢰인 은행
          IF QUA = '2AC' Then
          begin
            SC2.ADDValue('APP_BANK', SEGDATA.items[2]);
            SC2.ADDValue('APP_BANK1', LeftStr(SEGDATA.items[6], 35));
            SC2.ADDValue('APP_BANK2', Copy(SEGDATA.items[6], 36, 35));
            SC2.ADDValue('APP_BANK3', LeftStr(SEGDATA.items[7],35));
            SC2.ADDValue('APP_BANK4', Copy(SEGDATA.items[7], 36, 35));

            IF NextSegment(8, 'COM', '20') Then
            begin
              SC2.ADDValue('APP_BANK5', SEGDATA.items[0]);
            end;
          end;
          //신용장지급방식 및 은행을 표시 (Available with by)
          IF QUA = 'DA' Then
          begin
            SC2.ADDValue('AVAIL', SEGDATA.items[2]);
            SC2.ADDValue('AVAIL_BIC', SEGDATA.items[2]);
            SC2.ADDValue('AVAIL1', LeftStr(SEGDATA.items[6], 35));
            SC2.ADDValue('AVAIL2', Copy(SEGDATA.items[6], 36, 35));
            SC2.ADDValue('AVAIL3', LeftStr(SEGDATA.items[7],35));
            SC2.ADDValue('AVAIL4', Copy(SEGDATA.items[7], 36, 35));
            SC2.ADDValue('AV_PAY', SEGDATA.items[5]);
          end;
          //어음지급인 Drawee
          IF QUA = 'DW' Then
          begin
            SC2.ADDValue('DRAWEE', SEGDATA.items[2]);
            SC2.ADDValue('DRAWEE_BIC', SEGDATA.items[2]);
            SC2.ADDValue('DRAWEE1', LeftStr(SEGDATA.items[6], 35));
            SC2.ADDValue('DRAWEE2', Copy(SEGDATA.items[6], 36, 35));
            SC2.ADDValue('DRAWEE3', LeftStr(SEGDATA.items[7],35));
            SC2.ADDValue('DRAWEE4', Copy(SEGDATA.items[7], 36, 35));
          end;
          //확인은행
          IF QUA = '4AE' Then
          begin
            SC3.ADDValue('CONFIRM_BICCD', SEGDATA.items[2]);
            SC3.ADDValue('CONFIRM_BANKNM', SEGDATA.items[6]);
            SC3.ADDValue('CONFIRM_BANKBR', SEGDATA.items[7]);
          end;
          //상환은행
          IF QUA = 'BD' Then
          begin
            SC4.ADDValue('REI_BANK', SEGDATA.items[2]);
            SC4.ADDValue('REI_BANK1', LeftStr(SEGDATA.items[6], 35));
            SC4.ADDValue('REI_BANK2', Copy(SEGDATA.items[6], 36, 35));
            SC4.ADDValue('REI_BANK3', LeftStr(SEGDATA.items[7],35));
            SC4.ADDValue('REI_BANK4', Copy(SEGDATA.items[7], 36, 35));
          end;
          //최종통지은행
          IF QUA = '2AB' Then
          begin
            SC4.ADDValue('AVT_BANK', SEGDATA.items[2]);
            SC4.ADDValue('AVT_BANK1', LeftStr(SEGDATA.items[6], 35));
            SC4.ADDValue('AVT_BANK2', Copy(SEGDATA.items[6], 36, 35));
            SC4.ADDValue('AVT_BANK3', LeftStr(SEGDATA.items[7],35));
            SC4.ADDValue('AVT_BANK4', Copy(SEGDATA.items[7], 36, 35));
          end;
        end;
      end;

      //------------------------------------------------------------------------------
      // NAD그룹
      //------------------------------------------------------------------------------
      for i := 1 to 4 do
      begin
        IF FindSegment('NAD', '1B', 11) Then
        begin
          QUA := SEGDATA.Items[0];
          //개설의뢰인
          IF QUA = 'DF' Then
          begin
            SC2.ADDValue('APPLIC1', SEGDATA.Items[1]);
            SC2.ADDValue('APPLIC2', SEGDATA.Items[2]);
            SC2.ADDValue('APPLIC3', SEGDATA.Items[3]);
            SC2.ADDValue('APPLIC4', SEGDATA.Items[4]);

            IF NextSegment(11,'COM','21',2) Then
            begin
              SC2.ADDValue('APPLIC5', SEGDATA.Items[0]);
            end;
          end;
          //수익자
          IF QUA = 'DG' then
          begin
            SC2.ADDValue('BENEFC1', SEGDATA.Items[1]);
            SC2.ADDValue('BENEFC2', SEGDATA.Items[2]);
            SC2.ADDValue('BENEFC3', SEGDATA.Items[3]);
            SC2.ADDValue('BENEFC4', SEGDATA.Items[4]);
            SC2.ADDValue('BENEFC5', SEGDATA.Items[5]);          
          end;
          //개설의뢰인 전자서명
          IF QUA = '2AE' Then
          begin
            SC4.ADDValue('EX_NAME1', SEGDATA.items[6]);
            SC4.ADDValue('EX_NAME2', SEGDATA.items[7]);
            SC4.ADDValue('EX_NAME3', SEGDATA.items[8]);
            SC4.ADDValue('EX_ADDR1', SEGDATA.items[9]);
            SC4.ADDValue('EX_ADDR2', SEGDATA.items[10]);
          end;
          //개설은행 전자서명
          IF QUA = '2AF' Then
          begin
            SC4.ADDValue('OP_BANK1', SEGDATA.items[6]);
            SC4.ADDValue('OP_BANK2', SEGDATA.items[7]);
            SC4.ADDValue('OP_BANK3', SEGDATA.items[8]);
            SC4.ADDValue('OP_ADDR1', SEGDATA.items[9]);
            SC4.ADDValue('OP_ADDR2', SEGDATA.items[10]);
          end;
        end;
      end;

      //유효기일
      IF FindSegment('DTM', '1C', 3) Then
      begin
        SC1.ADDValue('EX_DATE', '20'+SEGDATA.items[1]);
      end;
      //유효장소
      IF FindSegment('LOC', '22', 2) Then
      begin
        SC1.ADDValue('EX_PLACE', SEGDATA.items[1]);
      end;

      IF FindSegment('UNS','1D',1) Then
      begin
        //파트구분용
      end;

      //서류제시기간
      IF FindSegment('DTM', '1E', 3) Then
      begin
        SC3.ADDValue('PERIOD', SEGDATA.items[1], vtinteger);
      end;
      //서류제시기간 상세기준 입력
      IF FindSegment('FTX', '23', 2) Then
      begin
        SC3.ADDValue('PERIOD_TXT', SEGDATA.items[1]);
        SC3.ADDValue('PERIOD_IDX', '1', vtINteger);
      end
      else
        SC3.ADDValue('PERIOD_IDX', '0', vtINteger);

      //개설금액
      IF FindSegment('MOA', '1F', 3) Then
      begin
        SC2.ADDValue('CD_AMT', SEGDATA.items[1]);
        SC2.ADDValue('CD_CUR', SEGDATA.items[2]);
      end;

      //부가금액부담
      IF FindSegment('FTX', '24', 5) Then
      begin
        SC2.ADDValue('AA_CV1', SEGDATA.items[1]);
        SC2.ADDValue('AA_CV2', SEGDATA.items[1]);
        SC2.ADDValue('AA_CV3', SEGDATA.items[1]);
        SC2.ADDValue('AA_CV4', SEGDATA.items[1]);
      end;

      //과부족 허용율 표시
      IF FindSegment('PCD', '25', 2) Then
      begin
        TMP_STR := SEGDATA.items[1];
        PCD_POINT := Pos('.', TMP_STR);
        SC2.ADDValue('CD_PERP', LeftStr(TMP_STR, PCD_POINT-1), vtInteger);
        SC2.ADDValue('CD_PERM', MIDStr(TMP_STR, PCD_POINT+1, 2), vtInteger);
      end;

  //------------------------------------------------------------------------------
  // 선적항/도착항/최종목적지
  //------------------------------------------------------------------------------
      for i := 1 to 4 do
      begin
        IF FindSegment('LOC', '1G', 2) Then
        begin
          QUA := SEGDATA.items[0];
          //수탁(발송지)
          IF QUA = '149' Then
          begin
            SC3.ADDValue('LOAD_ON', SEGDATA.items[1]);

            //최종선적일자
            IF NextSegment(2, 'DTM', '26', 3) Then
            begin
              SC3.ADDValue('LST_DATE', '20'+SEGDATA.items[1]);
            end;
            IF NextSegment(2, 'FTX', '27', 4) Then
            begin
              SC3.ADDValue('SHIP_PD1', SEGDATA.items[1]);
              SC3.ADDValue('SHIP_PD2', SEGDATA.items[1]);
              SC3.ADDValue('SHIP_PD3', SEGDATA.items[1]);
              IF NextSegment(4,'FTX', '27', 4) Then
              begin
                SC3.ADDValue('SHIP_PD4', SEGDATA.items[1]);
                SC3.ADDValue('SHIP_PD5', SEGDATA.items[1]);
                SC3.ADDValue('SHIP_PD6', SEGDATA.items[1]);
              end;
            end;
          end;

          //선적항
          IF QUA = '76' Then
          begin
            SC4.ADDValue('SUNJUCK_PORT', SEGDATA.items[1]);

            //최종선적일자
            IF NextSegment(2, 'DTM', '26', 3) Then
            begin
              SC3.ADDValue('LST_DATE', '20'+SEGDATA.items[1]);
            end;
            IF NextSegment(2, 'FTX', '27', 4) Then
            begin
              SC3.ADDValue('SHIP_PD1', SEGDATA.items[1]);
              SC3.ADDValue('SHIP_PD2', SEGDATA.items[1]);
              SC3.ADDValue('SHIP_PD3', SEGDATA.items[1]);
              IF NextSegment(4,'FTX', '27', 4) Then
              begin
                SC3.ADDValue('SHIP_PD4', SEGDATA.items[1]);
                SC3.ADDValue('SHIP_PD5', SEGDATA.items[1]);
                SC3.ADDValue('SHIP_PD6', SEGDATA.items[1]);
              end;
            end;
          end;

          //도착항
          IF QUA = '12' Then
          begin
            SC4.ADDValue('DOCHACK_PORT', SEGDATA.items[1]);
          end;

          //최종목적지
          IF QUA = '148' Then
          begin
            SC3.ADDValue('FOR_TRAN', SEGDATA.items[1]);

            //최종선적일자
            IF NextSegment(2, 'DTM', '26', 3) Then
            begin
              SC3.ADDValue('LST_DATE', '20'+SEGDATA.items[1]);
            end;
            IF NextSegment(2, 'FTX', '27', 4) Then
            begin
              SC3.ADDValue('SHIP_PD1', SEGDATA.items[1]);
              SC3.ADDValue('SHIP_PD2', SEGDATA.items[1]);
              SC3.ADDValue('SHIP_PD3', SEGDATA.items[1]);
              IF NextSegment(4,'FTX', '27', 4) Then
              begin
                SC3.ADDValue('SHIP_PD4', SEGDATA.items[1]);
                SC3.ADDValue('SHIP_PD5', SEGDATA.items[1]);
                SC3.ADDValue('SHIP_PD6', SEGDATA.items[1]);
              end;
            end;
          end;
        end;
      end;

      //가격조건 표시
      IF FindSegment('TOD', '1H', 2) Then
      begin
        SC3.ADDValue('TERM_PR', SEGDATA.items[0]);
        SC3.ADDValue('TERM_PR_M', SEGDATA.items[1]);
      end;

      for i := 1 to 2 do
      begin
        IF FindSegment('LOC', '28', 5) Then
        begin
          QUA := SEGDATA.items[0];
          //가격조건 관련장소(place of terms of price)
          IF QUA = '1' Then
          begin
            SC3.ADDValue('PL_TERM', SEGDATA.items[4]);
          end;
          //원산지
          IF QUA = '27' Then
          begin
            SC3.ADDValue('ORIGIN', SEGDATA.items[1]);
            SC3.ADDValue('ORIGIN_M', SEGDATA.items[4]);
          end;
        end;
      end;

      //상품용역명세
      TMP_TXT.Clear;
      for i := 1 to 60 do
      begin
        IF FindSegment('FTX', '29', 6) Then
        begin
          TMP_TXT.Add(SEGDATA.items[1]);
          TMP_TXT.Add(SEGDATA.items[2]);
          TMP_TXT.Add(SEGDATA.items[3]);
          TMP_TXT.Add(SEGDATA.items[4]);
          TMP_TXT.Add(SEGDATA.items[5]);
        end;
      end;
      SC3.ADDValue('DESGOOD_1', TMP_TXT.Text);

      // 주요부가조건
      SC3.ADDValue('ACD_2AA', '0', vtinteger);
      SC3.ADDValue('ACD_2AB', '0', vtinteger);
      SC3.ADDValue('ACD_2AC', '0', vtinteger);
      SC3.ADDValue('ACD_2AD', '0', vtinteger);
      SC3.ADDValue('ACD_2AE', '0', vtinteger);

      for i := 1 to 5 do
      begin
        IF FindSegment('ALI', '1I', 1) Then
        begin
          QUA := SEGDATA.items[0];
          IF QUA = '2AA' Then
          begin
            SC3.ChangeValue('ACD_2AA', '1', vtinteger);

            IF NextSegment(1, 'NAD', '2A', 2) Then
            begin
              SC3.ADDValue('ACD_2AA_1', SEGDATA.Items[1]);
            end;
          end;

          IF QUA = '2AB' Then
            SC3.ChangeValue('ACD_2AB', '1', vtInteger);

          IF QUA = '2AC' Then
            SC3.ChangeValue('ACD_2AC', '1', vtInteger);

          IF QUA = '2AD' Then
            SC3.ChangeValue('ACD_2AD', '1', vtInteger);

          IF QUA = '2AE' Then
          begin
            SC3.ChangeValue('ACD_2AE', '1', vtInteger);

            TMP_TXT.Clear;
            for INNER_COUNT := 1 to 60 do
            begin
              IF FindSegment('FTX', '2B', 6) Then
              begin
                TMP_TXT.Add(SEGDATA.items[1]);
                TMP_TXT.Add(SEGDATA.items[2]);
                TMP_TXT.Add(SEGDATA.items[3]);
                TMP_TXT.Add(SEGDATA.items[4]);
                TMP_TXT.Add(SEGDATA.items[5]);
              end;
            end;
            SC3.ADDValue('ACD_2AE_1', TMP_TXT.Text);
          end;
        end;
      end;

      // 주요 구비 서류
      SC3.ADDValue('DOC_380', '0', vtInteger);
      SC3.ADDValue('DOC_705', '0', vtInteger);
      SC3.ADDValue('DOC_740', '0', vtInteger);
      SC3.ADDValue('DOC_530', '0', vtInteger);
      SC3.ADDValue('DOC_271', '0', vtInteger);
      SC3.ADDValue('DOC_861', '0', vtInteger);
      SC3.ADDValue('DOC_2AA', '0', vtInteger);
      for i := 1 to 8 do
      begin
        IF FindSegment('DOC', '1J', 2) Then
        begin
          QUA := SEGDATA.items[0];
          IF QUA = '380' then
          begin
            SC3.ChangeValue('DOC_380', '1', vtInteger);
            SC3.ADDValue('DOC_380_1', SEGDATA.items[1], vtInteger);
          end;

          IF AnsiMatchText(QUA, ['705','706','717','718','707']) Then
          begin
            SC3.ChangeValue('DOC_705', '1', vtInteger);
            SC3.ADDValue('DOC_705_GUBUN', QUA);

            IF NextSegment(2, 'ALI', '2D', 1) Then
            begin
              SC3.ADDValue('DOC_705_3', SEGDATA.items[0]);
            end;

            for INNER_COUNT := 1 to 2 do
            begin
              IF FindSegment('NAD', '30', 3) Then
              begin
                QUA := SEGDATA.items[0];
                //수하인
                IF QUA = 'CN' Then
                begin
                  SC3.ADDValue('DOC_705_1', SEGDATA.items[1]);
                  SC3.ADDValue('DOC_705_2', SEGDATA.items[2]);
                end;
                IF QUA = 'NI' Then
                begin
                  SC3.ADDValue('DOC_705_4', SEGDATA.items[1]);
                end;
              end;
            end;
          end;

          //740
          IF QUA = '740' Then
          begin
            SC3.ChangeValue('DOC_740', '1', vtInteger);

            IF NextSegment(2, 'ALI', '2D', 1) Then
            begin
              SC3.ADDValue('DOC_740_3', SEGDATA.items[0]);
            end;

            for INNER_COUNT := 1 to 2 do
            begin
              IF FindSegment('NAD', '30', 3) Then
              begin
                QUA := SEGDATA.items[0];
                //수하인
                IF QUA = 'CN' Then
                begin
                  SC3.ADDValue('DOC_740_1', SEGDATA.items[1]);
                  SC3.ADDValue('DOC_740_2', SEGDATA.items[2]);
                end;
                IF QUA = 'NI' Then
                begin
                  SC3.ADDValue('DOC_740_4', SEGDATA.items[1]);
                end;
              end;
            end;
          end;

          //760
          IF QUA = '760' Then
          begin
            SC3.ChangeValue('DOC_760', '1', vtInteger);

            IF NextSegment(2, 'ALI', '2D', 1) Then
            begin
              SC3.ADDValue('DOC_760_3', SEGDATA.items[0]);
            end;

            for INNER_COUNT := 1 to 2 do
            begin
              IF FindSegment('NAD', '30', 3) Then
              begin
                QUA := SEGDATA.items[0];
                //수하인
                IF QUA = 'CN' Then
                begin
                  SC3.ADDValue('DOC_760_1', SEGDATA.items[1]);
                  SC3.ADDValue('DOC_760_2', SEGDATA.items[2]);
                end;
                IF QUA = 'NI' Then
                begin
                  SC3.ADDValue('DOC_760_4', SEGDATA.items[1]);
                end;
              end;
            end;
          end;

          //530
          IF QUA = '530' Then
          begin
            SC3.ChangeValue('DOC_530', '1', vtInteger);
            IF FindSegment('FTX', '2C', 6) Then
            begin
              SC3.ADDValue('DOC_530_1', SEGDATA.items[1]);
              SC3.ADDValue('DOC_530_2', SEGDATA.items[2]);
            end;
          end;


          //패킹리스트
          IF QUA = '271' Then
          begin
            SC3.ChangeValue('DOC_271', '1', vtInteger);
            SC3.ADDValue('DOC_271_1', SEGDATA.items[1], vtInteger);
          end;

          //Certificate of origin
          IF QUA = '861' Then
            SC3.ChangeValue('DOC_861', '1', vtInteger);

          //OTHER DOCUMENT(S)
          IF QUA = '2AA' Then
          begin
            SC3.ChangeValue('DOC_2AA', '1', vtInteger);
            TMP_TXT.Clear;
            for INNER_COUNT := 1 to 21 do
            begin
              IF FindSegment('FTX', '2C', 6) Then
              begin
                TMP_TXT.Add(SEGDATA.items[1]);
                TMP_TXT.Add(SEGDATA.items[2]);
                TMP_TXT.Add(SEGDATA.items[3]);
                TMP_TXT.Add(SEGDATA.items[4]);
                TMP_TXT.Add(SEGDATA.items[5]);
              end;
            end;
            SC3.ADDValue('DOC_2AA_1', TMP_TXT.Text);
          end;
        end;
      end;

      //수입용도
      INNER_COUNT := 1;
      for i := 1 to 5 do
      begin
        IF FindSegment('RFF', '1K', 2) Then
        begin
          SC1.ADDValue('IMP_CD'+IntToStr(INNER_COUNT), SEGDATA.items[0]);
          SC1.ADDValue('IL_NO'+IntToStr(INNER_COUNT), SEGDATA.items[1]);

          IF FindSegment('MOA', '2E', 3) Then
          begin
            SC1.ADDValue('IL_AMT'+IntToStr(INNER_COUNT), SEGDATA.items[1], vtInteger);
            SC1.ADDValue('IL_CUR'+IntToStr(INNER_COUNT), UpperCase(SEGDATA.items[2]));
          end;
          Inc(INNER_COUNT);
        end;
      end;

  //    Clipboard.AsText := SC3.FieldList;

      RunSQL(SC1.CreateSQL);
      RunSQL(SC2.CreateSQL);
      Clipboard.AsText := SC3.FieldList;
      RunSQL(SC3.CreateSQL);
      RunSQL(SC4.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
        Result := False;
      end;
    end;
  finally
    SC1.Free;
    SC2.Free;
    SC3.Free;
    SC4.Free;
    A.Free;
    TMP_TXT.Free;
  end;
end;

{ TINF707_NEW }

procedure TINF707_NEW.DefineList;
var
  MAINT_NO, MSEQ, AMDNO, MESSAGE1, MESSAGE2 : String;
  i : integer;
  QUA : String;
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM','10') THEN
      BEGIN
        MAINT_NO := SEGDATA.Items[1];
        AMDNO := RightStr(MAINT_NO, Length(MAINT_NO)-(POS('__', MAINT_NO)+1));
        MSEQ  := MidStr(MAINT_NO, Pos('_', MAINT_NO)+1, Pos('__', MAINT_NO)-Pos('_', MAINT_NO)-1);
        MAINT_NO := LeftStr(MAINT_NO, Pos('_', MAINT_NO)-1);
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
      end;

//      for i := 1 to 4 do
//      begin
//        IF FindSegment('RFF', '12', 2) Then
//        begin
//          QUA := SEGDATA.items[0];
//          //조건변경횟수
//          IF QUA = '2AB' Then
//          begin
//            AMD_NO := SEGDATA.items[1];
//          end;
//        end;
//      end;
    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'INF707');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', 0);
      SC.ADDValue('CONTROLNO', AMDNO);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', 'N');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'INF707');
      RunSQL(SC.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;

function TINF707_NEW.ParseData: Boolean;
var
  MAINT_NO, MSEQ, AMDNO, MESSAGE1, MESSAGE2 : String;
  SC1, SC2, SCDEL1: TSQLCreate;
  i : integer;
  QUA, SEGVALUE : String;
  TMP_TXT : TStringList;
  TMP_FTX : array [0..11] of string;
  INNER_COUNT : Integer;
  TMP_STR : String;
  PCD_POINT : integer;

begin
  Result := True;
  TMP_TXT := TStringList.Create;
  SC1 := TSQLCreate.Create;
  SC2 := TSQLCreate.Create;
  SCDEL1 := TSQLCreate.Create;

  try
    try
      IF FindSegment('BGM','10') THEN
      BEGIN
  //      ShowMessage( SEGDATA.Items[0] );
        MAINT_NO := SEGDATA.Items[1];
        AMDNO := RightStr(MAINT_NO, Length(MAINT_NO)-(POS('__', MAINT_NO)+1));
        MSEQ  := MidStr(MAINT_NO, Pos('_', MAINT_NO)+1, Pos('__', MAINT_NO)-Pos('_', MAINT_NO)-1);
        MAINT_NO := LeftStr(MAINT_NO, Pos('_', MAINT_NO)-1);
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
      end;

    //------------------------------------------------------------------------------
    // 중복확인
    //------------------------------------------------------------------------------
      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('INF707_1');
      SC1.ADDWhere('MAINT_NO', MAINT_NO);
      SC1.ADDWhere('MSEQ', MSEQ, vtInteger);
      SC1.ADDWhere('AMD_NO', AMDNO, vtinteger);
      RunSQL(SC1.CreateSQL);

      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('INF707_2');
      SC1.ADDWhere('MAINT_NO', MAINT_NO);
      SC1.ADDWhere('MSEQ', MSEQ, vtinteger);
      SC1.ADDWhere('AMD_NO', AMDNO, vtinteger);
      RunSQL(SC1.CreateSQL);

      //------------------------------------------------------------------------------
      // UNH파일 파싱
      //------------------------------------------------------------------------------
      SC1.DMLType := dmlInsert;
      SC2.DMLType := dmlInsert;

      SC1.SQLHeader('INF707_1');
      SC2.SQLHeader('INF707_2');

      SC1.ADDValue('MAINT_NO', MAINT_NO);
      SC2.ADDValue('MAINT_NO', MAINT_NO);
      SC1.ADDValue('MSEQ', MSEQ, vtInteger);
      SC2.ADDValue('MSEQ', MSEQ, vtInteger);
      SC1.ADDValue('AMD_NO', AMDNO, vtInteger);
      SC2.ADDValue('AMD_NO', AMDNO, vtInteger);

      SC1.ADDValue('MESSAGE1', MESSAGE1);
      SC1.ADDValue('MESSAGE2', MESSAGE2);
      SC1.ADDValue('USER_ID', LoginData.sID);
      SC1.ADDValue('DATEE', FormatDateTime('YYYYMMDD', Now));

  //------------------------------------------------------------------------------
  // 개설방법
  //------------------------------------------------------------------------------
      for i := 1 to 4 do
      begin
        IF FindSegment('INP', '11', 3) then
        begin
           QUA := SEGDATA.items[1];
           //개설방법
           IF QUA = '5' Then
             SC1.ADDValue('IN_MATHOD', SEGDATA.items[2]);
           //문서목적
           IF QUA = '30' Then
           begin
             //해당 필드는 존재하지 않음
           end;
           //신용장 취소 요청
           IF QUA = '29' Then
             SC1.ADDValue('IS_CANCEL', SEGDATA.items[2]);
           //확인지시문언
           IF QUA = '4' then
             SC2.ADDValue('CONFIRM', SEGDATA.items[2]);
        end;
      end;

      for i := 1 to 4 do
      begin
        IF FindSegment('RFF', '12', 2) Then
        begin
          QUA := SEGDATA.items[0];
          // 발신은행 참조사항
          IF QUA = '2AD' then
            SC1.ADDValue('CD_NO', SEGDATA.items[1]);
          // 수신은행 참조사항
          IF QUA = '2AE' then
            SC1.ADDValue('RCV_REF', SEGDATA.items[1]);
          // 개설은행 참조사항
          IF QUA = '2AC' Then
            Sc1.ADDValue('IBANK_REF', SEGDATA.items[1]);
          //조건변경횟수
//          IF QUA = '2AB' Then
//          begin
//            AMD_NO := SEGDATA.items[1];
//            SC1.ADDValue('AMD_NO', AMD_NO);
//            SC2.ADDValue('AMD_NO', AMD_NO);
//          end;
        end;
      end;

      for i := 1 to 5 do
      begin
        IF FindSegment('DTM', '13' ,3) then
        begin
          QUA := SEGDATA.items[0];
          //조건변경 !신청!일자
          IF QUA = '2AA' Then
            SC1.ADDValue('APP_DATE', EdiDateFormat(SEGDATA.items[1]));
          //개설일자
          IF QUA = '182' Then
            SC1.ADDValue('ISS_DATE', EdiDateFormat(SEGDATA.items[1]));
          //조건변경일자
          IF QUA = '2AB' Then
            SC1.ADDValue('AMD_DATE', EdiDateFormat(SEGDATA.items[1]));
          //최종선적일자
          IF QUA = '38' THen
            SC2.ADDValue('LST_DATE', EdiDateFormat(SEGDATA.items[1]));
          //MIG에는 반복 4번이라고하는데 WINMATE의 반복수는 5번으로 되어있음 (???)
        end;
      end;

      //신용장 종류 변경
      IF FindSegment('BUS', '14', 1) Then
        SC1.ADDValue('DOC_CD', SEGDATA.items[0]);

      IF FindSegment('MOA', '15', 3) Then
      begin
        QUA := SEGDATA.items[0];
        //신용장 증액분
        IF QUA = '2AA' Then
        begin
          SC2.ADDValue('INCD_CUR', SEGDATA.items[2]);
          SC2.ADDValue('INCD_AMT', SEGDATA.items[1]);
        end;
        //심용장 감액분
        IF QUA = '2AB' Then
        begin
          SC2.ADDValue('DECD_CUR', SEGDATA.items[2]);
          SC2.ADDValue('DECD_AMT', SEGDATA.items[1]);
        end;
      end;

      //과부족 허용율
      IF FindSegment('PCD', '16', 2) Then
      begin
        TMP_STR := SEGDATA.items[1];
        PCD_POINT := Pos('.', TMP_STR);
        SC2.ADDValue('CD_PERP', LeftStr(TMP_STR, PCD_POINT-1), vtInteger);
        SC2.ADDValue('CD_PERM', MIDStr(TMP_STR, PCD_POINT+1, 2), vtInteger);
      end;

      TMP_FTX[0] := '';
      TMP_FTX[1] := '';
      for i := 1 to 12 do
      begin
        if FindSegment('ALC', '17', 2) Then
        begin
          QUA := SEGDATA.items[0];
          IF AnsiMatchText(QUA, ['2AF','2AG']) Then
          begin
            SC2.ADDValue('CHARGE', QUA);
            Continue;
          end
          else
          IF AnsiMatchText(QUA, ['2AC','2AD']) Then
          begin
            SC2.ADDValue('AMD_CHARGE', QUA);
            Continue;
          end
          else
          IF QUA = '2AH' Then
            TMP_FTX[0] := TMP_FTX[0]+SEGDATA.items[1]+#13#10
          else
          IF QUA = '2AE' Then
            TMP_FTX[1] := TMP_FTX[1]+SEGDATA.items[1]+#13#10

        end;
      end;
      IF Trim(TMP_FTX[0]) <> '' Then
        SC2.ADDValue('CHARGE', '2AH');
      IF Trim(TMP_FTX[1]) <> '' Then
        SC2.ADDValue('AMD_CHARGE', '2AE');
      SC2.ADDValue('CHARGE_1', TMP_FTX[0]);
      SC2.ADDValue('AMD_CHARGE_1', TMP_FTX[1]);

      //선적/환적
      if FindSegment('TSR', '18', 2) Then
      begin
        SC1.ADDValue('PSHIP', SEGDATA.items[1]);
        SC1.ADDValue('TSHIP', SEGDATA.items[0]);
      end;

      for i := 1 to 4 do
      begin
        IF FindSegment('LOC', '19', 2) Then
        begin
          QUA := SEGDATA.items[0];
          //수탁발송지
          IF QUA = '149' Then
            SC2.ADDValue('LOAD_ON', SEGDATA.items[1]);
          //선적항
          IF QUA = '76' Then
            SC2.ADDValue('SUNJUCK_PORT', SEGDATA.items[1]);
          //도착항
          IF QUA = '12' Then
            SC2.ADDValue('DOCHACK_PORT', SEGDATA.items[1]);
          //최종목적지
          IF QUA = '148' Then
            SC2.ADDValue('FOR_TRAN', SEGDATA.items[1]);
        end;
      end;

      //지급조건
      for i := 1 to 4 do
      begin
        IF FindSegment('PAT', '1A', 3) Then
        begin
          QUA := SEGDATA.items[0]+SEGDATA.items[1];
          IF QUA = '2AB2AO' THen
            SC2.ADDValue('DRAFT'+IntToStr(i), SEGDATA.items[2]);
          IF QUA = '62AM' Then
            SC2.ADDValue('MIX'+IntToStr(i), SEGDATA.items[2]);
          IF QUA = '42AN' Then
            SC2.ADDValue('DEFPAY'+IntToStr(i), SEGDATA.items[2]);
        end;
      end;

      //평문처리
      TMP_FTX[0] := '';
      TMP_FTX[1] := '';
      TMP_FTX[2] := '';
      TMP_FTX[3] := '';
      TMP_FTX[4] := '';

      for i := 1 to 170 do
      begin
        IF FindSegment('FTX', '1B', 6) Then
        begin
          QUA := SEGDATA.items[0];
          // 기타정보 개설은행 고유입력사항 1
          IF QUA = 'ACB' Then
          begin
            SC1.ADDValue('AD_INFO1', SEGDATA.items[1]);
            SC1.ADDValue('AD_INFO2', SEGDATA.items[2]);
            SC1.ADDValue('AD_INFO3', SEGDATA.items[3]);
            SC1.ADDValue('AD_INFO4', SEGDATA.items[4]);
            SC1.ADDValue('AD_INFO5', SEGDATA.items[5]);
          end;
        
          // 신용장 적용규칙 1
          IF QUA = '3FC' Then
          begin
            SC1.ADDValue('APP_RULE1', SEGDATA.items[1]);
            SC1.ADDValue('APP_RULE2', SEGDATA.items[2]);
          end;

          // 부가금액부담 1
          IF QUA = 'ABT' Then
          begin
            SC2.ADDValue('AA_CV1', SEGDATA.items[1]);
            SC2.ADDValue('AA_CV2', SEGDATA.items[2]);
            SC2.ADDValue('AA_CV3', SEGDATA.items[3]);
            SC2.ADDValue('AA_CV4', SEGDATA.items[4]);
          end;

          // 선적기간 변경표시 2
          IF QUA = '2AF' Then
          begin
            IF not SC2.ExistsField('SHIP_PD1') Then
            begin
              SC2.ADDValue('SHIP_PD1', SEGDATA.items[1]);
              SC2.ADDValue('SHIP_PD2', SEGDATA.items[2]);
              SC2.ADDValue('SHIP_PD3', SEGDATA.items[3]);
            end
            else
            begin
              SC2.ADDValue('SHIP_PD4', SEGDATA.items[1]);
              SC2.ADDValue('SHIP_PD5', SEGDATA.items[2]);
              SC2.ADDValue('SHIP_PD6', SEGDATA.items[3]);
            end;
          end;

          // 상품(용역)명세 60
          IF QUA = 'AAA' Then
          begin
            TMP_FTX[0] := TMP_FTX[0]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end;

          // 주요구비서류 20
          IF QUA = 'ABX' then
          begin
            TMP_FTX[1] := TMP_FTX[1]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end;

          // 부가조건 변경표시 60
          IF QUA = 'ABS' Then
          begin
            TMP_FTX[2] := TMP_FTX[2]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end;

          // 수익자에 대한 특별지급조건 20
          IF QUA = 'PCB' Then
          begin
            TMP_FTX[3] := TMP_FTX[3]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end;

          // 지급/인수/매입은행에 대한 지시사항 3
          IF QUA = '2AB' Then
          begin
            TMP_FTX[4] := TMP_FTX[4]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end;

          //수신은행 앞 정보 2
          IF QUA = '2AE' then
          begin
            IF not SC2.ExistsField('SR_INFO1') Then
            begin
              SC2.ADDValue('SR_INFO1', SEGDATA.items[1]);
              SC2.ADDValue('SR_INFO2', SEGDATA.items[2]);
              SC2.ADDValue('SR_INFO3', SEGDATA.items[3]);
            end
            else
            begin
              SC2.ADDValue('SR_INFO4', SEGDATA.items[1]);
              SC2.ADDValue('SR_INFO5', SEGDATA.items[2]);
              SC2.ADDValue('SR_INFO6', SEGDATA.items[3]);
            end;
          end;
        end;
      end;
      SC2.ADDValue('GOODS_DESC', TMP_FTX[0]);
      SC2.ADDValue('DOC_REQ', TMP_FTX[1]);
      SC2.ADDValue('ADD_CONDITION', TMP_FTX[2]);
      SC2.ADDValue('SPECIAL_PAY', TMP_FTX[3]);
      SC2.ADDValue('TXT_78', TMP_FTX[4]);

      for i := 1 to 8 do
      begin
        IF FindSegment('FII', '1C', 8) Then
        begin
          QUA := SEGDATA.items[0];
          // SWIFT 전문송신은행
          IF QUA = 'AW' Then
          begin
            SC1.ADDValue('Ap_BANK', SEGDATA.items[2]);
            SC1.ADDValue('Ap_BANK1', LeftStr(SEGDATA.items[6],35));
            SC1.ADDValue('Ap_BANK2', MidStr(SEGDATA.items[6],36,35));
            SC1.ADDValue('Ap_BANK3', LeftStr(SEGDATA.items[7],35));
            SC1.ADDValue('Ap_BANK4', MidStr(SEGDATA.items[7],36,35));
            IF NextSegment(8, 'COM', '20', 2) then
              SC1.ADDValue('AP_BANK5', SEGDATA.items[0]);
          end;
          //SWIFT 전문수신은행
          IF QUA = '2AA' Then
          begin
            SC1.ADDValue('AD_BANK', SEGDATA.items[2]);
            SC1.ADDValue('AD_BANK1', LeftStr(SEGDATA.items[6],35));
            SC1.ADDValue('AD_BANK2', MidStr(SEGDATA.items[6],36,35));
            SC1.ADDValue('AD_BANK3', LeftStr(SEGDATA.items[7],35));
            SC1.ADDValue('AD_BANK4', MidStr(SEGDATA.items[7],36,35));
          end;
          // 개설은행
          IF QUA = 'AZ' Then
          begin
  //          SC1.ADDValue('Ap_BANK', SEGDATA.items[2]);
            SC1.ADDValue('ISS_BANK1', LeftStr(SEGDATA.items[6],35));
            SC1.ADDValue('ISS_BANK2', MidStr(SEGDATA.items[6],36,35));
            SC1.ADDValue('ISS_BANK3', LeftStr(SEGDATA.items[7],35));
            SC1.ADDValue('ISS_BANK4', MidStr(SEGDATA.items[7],36,35));
            IF NextSegment(8, 'COM', '20', 2) then
              SC1.ADDValue('ISS_BANK5', SEGDATA.items[0]);
          end;
          //신용장 지급방식 및 은행 (available with)
          IF QUA = 'DA' Then
          begin
            //해당 항목 사용하는지 확인
          end;
          // 어음지급인(dreawee)
          IF QUA = 'DW' Then
          begin
            //해당 항목 사용하는지 확인
          end;
          //확인은행
          IF QUA = '4AE' Then
          begin
            SC2.ADDValue('CONFIRM_BIC', SEGDATA.items[2]);
            SC2.ADDValue('CONFIRM1', LeftStr(SEGDATA.items[6],35));
            SC2.ADDValue('CONFIRM2', MidStr(SEGDATA.items[6],36,35));
            SC2.ADDValue('CONFIRM3', LeftStr(SEGDATA.items[7],35));
            SC2.ADDValue('CONFIRM4', MidStr(SEGDATA.items[7],36,35));
          end;
          //상환은행
          IF QUA = 'BD' Then
          begin
            SC1.ADDValue('REIM_BANK_BIC', SEGDATA.items[2]);
            SC1.ADDValue('REIM_BANK1', LeftStr(SEGDATA.items[6],35));
            SC1.ADDValue('REIM_BANK2', MidStr(SEGDATA.items[6],36,35));
            SC1.ADDValue('REIM_BANK3', LeftStr(SEGDATA.items[7],35));
            SC1.ADDValue('REIM_BANK4', MidStr(SEGDATA.items[7],36,35));
          end;
          //최종통지은행
          IF QUA = '2AB' Then
          begin
            SC1.ADDValue('AVT_BANK_BIC', SEGDATA.items[2]);
            SC1.ADDValue('AVT_BANK1', LeftStr(SEGDATA.items[6],35));
            SC1.ADDValue('AVT_BANK2', MidStr(SEGDATA.items[6],36,35));
            SC1.ADDValue('AVT_BANK3', LeftStr(SEGDATA.items[7],35));
            SC1.ADDValue('AVT_BANK4', MidStr(SEGDATA.items[7],36,35));
          end;
        end;
      end;

      //유효기일 변경
      IF FindSegment('DTM', '1D', 3) Then
        SC1.ADDValue('EX_DATE', EdiDateFormat(SEGDATA.items[1]));
      //서류제시장소
      IF FindSegment('LOC', '21', 2) Then
        SC1.ADDValue('EX_PLACE', SEGDATA.items[1]);

      for i := 1 to 5 do
      begin
        IF FindSegment('NAD', '1E', 11) Then
        begin
          QUA := SEGDATA.items[0];
          //개설의뢰인
          IF QUA = 'DF' Then
          begin
            SC2.ADDValue('APPLIC1', SEGDATA.items[1]);
            SC2.ADDValue('APPLIC2', SEGDATA.items[2]);
            SC2.ADDValue('APPLIC3', SEGDATA.items[3]);
            SC2.ADDValue('APPLIC4', SEGDATA.items[4]);
            IF NextSegment(11, 'COM', '22', 2) Then
              SC2.ADDValue('APPLIC5', SEGDATA.items[0]);
          end;
          // 개설의뢰인 정보 변경
          IF QUA = 'WX' Then
          begin
            SC2.ADDValue('APPLIC_CHG1', SEGDATA.items[1]);
            SC2.ADDValue('APPLIC_CHG2', SEGDATA.items[2]);
            SC2.ADDValue('APPLIC_CHG3', SEGDATA.items[3]);
            SC2.ADDValue('APPLIC_CHG4', SEGDATA.items[4]);
          end;
          // 수익자 정보 변경
          IF QUA = 'DG' Then
          begin
            SC2.ADDValue('BENEFC1', SEGDATA.items[1]);
            SC2.ADDValue('BENEFC2', SEGDATA.items[2]);
            SC2.ADDValue('BENEFC3', SEGDATA.items[3]);
            SC2.ADDValue('BENEFC4', SEGDATA.items[4]);
            SC2.ADDValue('BENEFC5', SEGDATA.items[5]);
          end;
          // 개설의뢰인 전자서명
          IF QUA = '2AE' Then
          begin
            SC2.ADDValue('EX_NAME1', SEGDATA.items[6]);
            SC2.ADDValue('EX_NAME2', SEGDATA.items[7]);
            SC2.ADDValue('EX_NAME3', SEGDATA.items[8]);
            SC2.ADDValue('EX_ADDR1', SEGDATA.items[9]);
            SC2.ADDValue('EX_ADDR2', SEGDATA.items[10]);
          end;

          // 개설은행 전자서명
          IF QUA = '2AF' Then
          begin
            SC2.ADDValue('OP_BANK1', SEGDATA.items[6]);
            SC2.ADDValue('OP_BANK2', SEGDATA.items[7]);
            SC2.ADDValue('OP_BANK3', SEGDATA.items[8]);
            SC2.ADDValue('OP_ADDR1', SEGDATA.items[9]);
            SC2.ADDValue('OP_ADDR2', SEGDATA.items[10]);
          end;
        end;
      end;

      //서류제시기간 변경
      IF FindSegment('DTM', '1F', 3) Then
        SC2.ADDValue('PERIOD_DAYS', SEGDATA.items[1]);
      IF FindSegment('FTX', '23', 2) Then
      begin
        SC2.ADDValue('PERIOD_DETAIL', SEGDATA.items[1]);
        IF Trim(SEGDATA.items[1]) = '' Then
          SC2.ADDValue('PERIOD_IDX', -1)
        else
        IF Trim(SEGDATA.items[1]) <> '' Then
          SC2.ADDValue('PERIOD_IDX', 1)
        else
          SC2.ADDValue('PERIOD_IDX', 0);
      end;

      //수입승인번호
      INNER_COUNT := 1;
      for i := 1 to 5 do
      begin
        IF FindSegment('RFF', '1G', 2) Then
        begin
          SC1.ADDValue('IMP_CD'+IntToStr(INNER_COUNT), SEGDATA.items[0]);
          SC1.ADDValue('IL_NO'+IntToStr(INNER_COUNT), SEGDATA.items[1]);

          IF FindSegment('MOA', '24', 3) Then
          begin
            SC1.ADDValue('IL_AMT'+IntToStr(INNER_COUNT), SEGDATA.items[1], vtInteger);
            SC1.ADDValue('IL_CUR'+IntToStr(INNER_COUNT), UpperCase(SEGDATA.items[2]));
          end;
          Inc(INNER_COUNT);
        end;
      end;

      RunSQL(SC1.CreateSQL);
      RunSQL(SC2.CreateSQL);
    except
      on E:Exception do
      begin
        Result := False;
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC1.Free;
    SC2.Free;
    SCDEL1.Free;
    TMP_TXT.Free;
  end;
end;

{ TCREADV_NEW }

procedure TCREADV_NEW.DefineList;
var
  MAINT_NO, MESSAGE1, MESSAGE2 : String;
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM','10') THEN
      BEGIN
        MAINT_NO := SEGDATA.Items[1];
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        IF Trim(MESSAGE2) = '' then MESSAGE1 := 'NA';
      end;

    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'CREADV');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', 0);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', '');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'CREADV');
      RunSQL(SC.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;

function TCREADV_NEW.ParseData: Boolean;
var
  MAINT_NO, AMD_NO, MESSAGE1, MESSAGE2 : String;
  SC1, SC2, SC3, SCFC1, SCDOCLIST: TSQLCreate;
  i, DOC_IDX, MOA_IDX, DTM_IDX : integer;
  QUA, SEGVALUE : String;
  TMP_TXT : TStringList;
  INNER_COUNT : Integer;

begin
  Result := True;
  TMP_TXT := TStringList.Create;
  SC1 := TSQLCreate.Create;
  SC2 := TSQLCreate.Create;
  SC3 := TSQLCreate.Create;
  SCFC1 := TSQLCreate.Create;
  SCDOCLIST := TSQLCreate.Create;

  try
    try
      IF FindSegment('BGM','10') THEN
      BEGIN
  //      ShowMessage( SEGDATA.Items[0] );
        MAINT_NO := SEGDATA.Items[1];
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        IF Trim(MESSAGE2) = '' then MESSAGE1 := 'NA';
      end;

    //------------------------------------------------------------------------------
    // 중복확인
    //------------------------------------------------------------------------------
      SC1.DMLType := dmlDelete;//모드설정
      SC1.SQLHeader('CREADV_H1');//테이블명
      SC1.ADDWhere('MAINT_NO', MAINT_NO);
      RunSQL(SC1.CreateSQL);

      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('CREADV_H2');
      SC1.ADDWhere('MAINT_NO', MAINT_NO);
      RunSQL(SC1.CreateSQL);

      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('CREADV_D1');
      SC1.ADDWhere('KEYY', MAINT_NO);
      RunSQL(SC1.CreateSQL);

      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('CREADV_DOCLIST');
      SC1.ADDWhere('KEYY', MAINT_NO);
      RunSQL(SC1.CreateSQL);

      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('CREADV_FC1');
      SC1.ADDWhere('KEYY', MAINT_NO);
      RunSQL(SC1.CreateSQL);
      //------------------------------------------------------------------------------
      // UNH파일 파싱
      //------------------------------------------------------------------------------
      SC1.DMLType := dmlInsert;
      SC2.DMLType := dmlInsert;

      SC1.SQLHeader('CREADV_H1');
      SC2.SQLHeader('CREADV_H2');

      SC1.ADDValue('MAINT_NO', MAINT_NO);
      SC2.ADDValue('MAINT_NO', MAINT_NO);

      SC1.ADDValue('MESSAGE1', MESSAGE1);
      SC1.ADDValue('MESSAGE2', MESSAGE2);
      SC1.ADDValue('USER_ID', LoginData.sID);
      SC1.ADDValue('DATEE', FormatDateTime('YYYYMMDD', Now));

      //========================================================================
      // 통지일자
      //========================================================================      
      for i := 1 to 5 do
      begin
        IF FindSegment('DTM', '11', 3) Then
        begin
          QUA := SEGDATA.items[0];
          //통지일자
          IF QUA = '137' Then
            SC1.ADDValue('ADV_DATE', EdiDateFormat(SEGDATA.items[1]));
          //지급이행일
          IF QUA = '209' Then
            SC1.ADDValue('EXE_DATE', EdiDateFormat(SEGDATA.items[1]));
          //계좌입금일
          IF QUA = '202' Then
            SC1.ADDValue('POS_DATE', EdiDateFormat(SEGDATA.items[1]));
          //자금수취일
          IF QUA = '193' Then
            SC1.ADDValue('VAL_DATE', EdiDateFormat(SEGDATA.items[1]));
        end;
      end;

      //========================================================================
      // 기타참조번호
      //========================================================================
      IF FindSegment('RFF', '12', 2) Then
        SC1.ADDValue('ADREFNO', SEGDATA.items[1]);

      //========================================================================
      // 입금내역
      //========================================================================
      IF FindSegment('FTX', '13', 6) Then
      begin
        TMP_TXT.Clear;
        for i := 1 to 5 do
        begin
          IF Trim(SEGDATA.items[i]) <> '' Then
            TMP_TXT.Add(SEGDATA.items[i]);
        end;

        SC1.ADDValue('PAYDET1', TMP_TXT.Text);
      end;

      //========================================================================
      // 금액
      //========================================================================
      for i := 1 to 4 do
      begin
        IF FindSegment('MOA', '14', 3) then
        begin
          QUA := SEGDATA.items[0];
          //수취금액
          IF QUA = '119' Then
          begin
            SC1.ADDValue('ORD1AMT',  SEGDATA.items[1], vtInteger);
            SC1.ADDValue('ORD1AMTC', SEGDATA.items[2]);
          end;
          //입금금액
          IF QUA = '60' Then
          begin
            SC1.ADDValue('ORD2AMT',  SEGDATA.items[1], vtInteger);
            SC1.ADDValue('ORD2AMTC', SEGDATA.items[2]);
          end;
          //지급금액
          IF QUA = '98' Then
          begin
            SC1.ADDValue('ORD3AMT',  SEGDATA.items[1], vtInteger);
            SC1.ADDValue('ORD3AMTC', SEGDATA.items[2]);
          end;
          //환전금액
          IF QUA = '36' Then
          begin
            SC1.ADDValue('ORD4AMT',  SEGDATA.items[1], vtInteger);
            SC1.ADDValue('ORD4AMTC', SEGDATA.items[2]);
            IF NextSegment(3, 'CUX', '20', 5) then
            begin
              //기준통화 0
              SC1.ADDValue('RATECD41', SEGDATA.items[0]);
              //목적통화 3
              SC1.ADDValue('RATECD42', SEGDATA.items[3]);
              //적용환율 4
              SC1.ADDValue('EXCRATE4', SEGDATA.items[4], vtInteger);
            end;

            for INNER_COUNT := 1 to 2 do
            begin
              IF NextSegment('DTM', '21', 3) then
              begin
                Case INNER_COUNT of
                  1: SC1.ADDValue('EXCDATE41', EdiDateFormat(SEGDATA.items[1]));
                  2: SC1.ADDValue('EXCDATE42', EdiDateFormat(SEGDATA.items[1]));
                end;
              end;
            end;
          end;
        end;
      end;

      //========================================================================
      //금융기관 유형
      //========================================================================
      for i := 1 to 4 do
      begin
        IF FindSegment('FII', '15', 10) Then
        begin
          QUA := SEGDATA.items[0];
          //수익자은행
          IF QUA = 'BF' Then
          begin
            //수익자명
            SC1.ADDValue('BN1NAME1', SEGDATA.items[2]);
            SC1.ADDValue('BN1NAME2', SEGDATA.items[3]);
            //수익자 계좌번호
            SC1.ADDValue('BN1NAME3', SEGDATA.items[1]);
            //수익자 통화코드
            SC1.ADDValue('BN1PCOD4', SEGDATA.items[4]);
            //수익자은행코드
            SC1.ADDValue('BN1BANK', SEGDATA.items[5]);
            //수익자은행명
            SC1.ADDValue('BN1BANK1', SEGDATA.items[8]);
            //수익자지점명
            SC1.ADDValue('BN1BANK2', SEGDATA.items[9]);
            //FTC/BOK
            SC1.ADDValue('BN1FB', SEGDATA.items[7]);
          end;
          //지급의뢰인은행
          IF QUA = 'OR' Then
          begin
            //수익자명
            SC1.ADDValue('OD1NAME1', SEGDATA.items[2]);
            SC1.ADDValue('OD1NAME2', SEGDATA.items[3]);
            //수익자 계좌번호
            SC1.ADDValue('OD1NAME3', SEGDATA.items[1]);
            //수익자 통화코드
            SC1.ADDValue('OD1PCOD4', SEGDATA.items[4]);
            //수익자은행코드
            SC1.ADDValue('OD1BANK', SEGDATA.items[5]);
            //수익자은행명
            SC1.ADDValue('OD1BANK1', SEGDATA.items[8]);
            //수익자지점명
            SC1.ADDValue('OD1BANK2', SEGDATA.items[9]);
            //FTC/BOK
            SC1.ADDValue('OD1FB', SEGDATA.items[7]);
          end;
        end;
      end;

      //========================================================================
      // 수익자/지급의뢰인/전자서명
      //========================================================================
      for i := 1 to 6 do
      begin
        IF FindSegment('NAD', '16', 7) Then
        begin
          QUA := SEGDATA.items[0];
          //수익자
          IF QUA = 'BE' Then
          begin
            SC1.ADDValue('TRN1NAME1', SEGDATA.items[1]);
            SC1.ADDValue('TRN1NAME2', SEGDATA.items[2]);
            SC1.ADDValue('TRN1NAME3', SEGDATA.items[3]);
            SC1.ADDValue('TRN1STR1',  SEGDATA.items[4]);
            SC1.ADDValue('TRN1STR2',  SEGDATA.items[5]);
            SC1.ADDValue('TRN1STR3',  SEGDATA.items[6]);
          end;
          //지급의뢰인
          IF QUA = 'OY' Then
          begin
            SC2.ADDValue('TRN2NAME1', SEGDATA.items[1]);
            SC2.ADDValue('TRN2NAME2', SEGDATA.items[2]);
            SC2.ADDValue('TRN2NAME3', SEGDATA.items[3]);
            SC2.ADDValue('TRN2STR1',  SEGDATA.items[4]);
            SC2.ADDValue('TRN2STR2',  SEGDATA.items[5]);
            SC2.ADDValue('TRN2STR3',  SEGDATA.items[6]);
          end;
          //전자서명
          IF QUA = 'AX' Then
          begin
            SC2.ADDValue('TRN3NAME1', SEGDATA.items[1]);
            SC2.ADDValue('TRN3NAME2', SEGDATA.items[2]);
            SC2.ADDValue('TRN3NAME3', SEGDATA.items[3]);
          end;
        end;
      end;

      //========================================================================
      // 기타정보
      //========================================================================
      for i := 1 to 2 do
      begin
        //INP는 BE BF만 표시되므로 넘어감
//        IF FindSegment('INP', '17', 2) then
        //기타정보
        IF FindSegment('FTX', '22', 6) then
        begin
          TMP_TXT.Clear;
          for INNER_COUNT := 1 to 5 do
          begin
            IF Trim(SEGDATA.Items[INNER_COUNT]) <> '' Then
              TMP_TXT.Add(SEGDATA.Items[INNER_COUNT]);
          end;

          Case i of
            1: SC1.ADDValue('ADINFO11', TMP_TXT.Text);
            2: SC1.ADDValue('ADINFO21', TMP_TXT.Text);
          end;
        end;
      end;
      //========================================================================
      //부가수수료
      //========================================================================      
      DOC_IDX := 0;
      for i := 1 to 5 do
      begin
        IF FindSegment('FCA', '18', 1) Then
        begin
          Inc(DOC_IDX);
          SC3.DMLType := dmlInsert;
          SC3.SQLHeader('CREADV_D1');
          SC3.ADDValue('KEYY', MAINT_NO);
          SC3.ADDValue('SEQ', DOC_IDX);
          SC3.ADDValue('FC1CD', SEGDATA.items[0]);
          for INNER_COUNT := 1 to 2 do
          begin
            IF NextSegment('MOA', '23', 3) Then
            begin
              Case INNER_COUNT of
                1:
                begin
                  //수수료합계1
                  SC3.ADDValue('FC1AMT11',  SEGDATA.items[1], vtinteger);
                  //통화단위
                  SC3.ADDValue('FC1AMT11C', SEGDATA.items[2]);
                end;

                2:
                begin
                  //수수료합계2
                  SC3.ADDValue('FC1AMT12',  SEGDATA.items[1], vtinteger);
                  //통화단위
                  SC3.ADDValue('FC1AMT12C', SEGDATA.items[2]);
                end;
              end;
            end;
          end;
          RunSQL(SC3.CreateSQL);

          //부가수수료 유형
          for INNER_COUNT := 1 to 20 do
          begin
            IF FindSegment('ALC', '24', 2) Then
            begin
              SCFC1.DMLType := dmlInsert;
              SCFC1.SQLHeader('CREADV_FC1');
              SCFC1.ADDValue('KEYY', MAINT_NO);
              SCFC1.ADDValue('SEQ', DOC_IDX);
              SCFC1.ADDValue('FC1SEQ', INNER_COUNT);
              SCFC1.ADDValue('FC1CH', SEGDATA.items[1]);

              for MOA_IDX := 1 to 2 do
              begin
                IF NextSegment('MOA', '30', 3) Then
                begin
                  QUA := SEGDATA.items[0];
                  //수수료
                  IF QUA = '23' Then
                  begin
                    //수수료금액
                    SCFC1.ADDValue('FC1SA1', SEGDATA.items[1], vtinteger);
                    //통화단위
                    SCFC1.ADDValue('FC1SA1C', SEGDATA.items[2]);
                  end;

                  IF QUA = '36' then
                  begin
                    //환전금액
                    SCFC1.ADDValue('FC1SA2', SEGDATA.items[1], vtinteger);
                    //통화단위
                    SCFC1.ADDValue('FC1SA2C', SEGDATA.items[2]);
                    //적용환율
                    IF NextSegment('CUX', '31', 5) THen
                    begin
                      //기준통화
                      SCFC1.ADDValue('FC1RATE1', SEGDATA.items[1]);
                      //목적통화
                      SCFC1.ADDValue('FC1RATE2', SEGDATA.items[3]);
                      //적용환율
                      SCFC1.ADDValue('FC1RATE', SEGDATA.items[4], vtinteger);

                      for DTM_IDX := 1 to 2 do
                      begin
                        IF NextSegment('DTM', '32', 3) Then
                        begin
                          case DTM_IDX of
                            1: SCFC1.ADDValue('FC1RD1', EdiDateFormat(SEGDATA.items[1]));
                            2: SCFC1.ADDValue('FC1RD2', EdiDateFormat(SEGDATA.items[1]));
                          end;
                        end;
                      end;
                    end;
                  end;
                end;
              end;
              RunSQL(SCFC1.CreateSQL);
            end;
          end;
        end;
      end;

      //========================================================================
      //입금관련서류
      //========================================================================
      for i := 1 to 10 do
      begin
        IF FindSegment('DOC', '19', 3) Then
        begin
          SCDOCLIST.DMLType := dmlInsert;
          SCDOCLIST.SQLHeader('CREADV_DOCLIST');
          SCDOCLIST.ADDValue('KEYY', MAINT_NO);
          SCDOCLIST.ADDValue('SEQ', i);
          SCDOCLIST.ADDValue('RELIST_TYPE', SEGDATA.ITEMS[0]);
          SCDOCLIST.ADDValue('RELIST_NO', SEGDATA.ITEMS[1]);
          SCDOCLIST.ADDValue('RELIST_ADDNO', SEGDATA.ITEMS[2]);
//          IF FindSegment('DTM', '25', 3) Then
          IF NextSegment('DTM', '25', 3) Then
            SCDOCLIST.ADDValue('RELIST_APPDT', EdiDateFormat(SEGDATA.ITEMS[1]));
          RunSQL(SCDOCLIST.CreateSQL);
        end;
      end;

      RunSQL(SC1.CreateSQL);
      RunSQL(SC2.CreateSQL);
    except
      on E:Exception do
      begin
        Result := False;
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC1.Free;
    SC2.Free;
    SC3.Free;
    SCFC1.Free;
    SCDOCLIST.Free;
    TMP_TXT.Free;
  end;
end;

{ TATTNTC_NEW }

procedure TATTNTC_NEW.DefineList;
var
  MAINT_NO, MESSAGE1, MESSAGE2 : String;
  SC : TSQLCreate;

begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM', '', 7) THEN
      BEGIN
        MAINT_NO := SEGDATA.Items[4];
        MESSAGE1 := SEGDATA.Items[5];
        MESSAGE2 := SEGDATA.Items[6];
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        IF Trim(MESSAGE2) = '' then MESSAGE1 := 'NA';
      end;

    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'ATTNTC');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', 0);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', '');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'ATTNTC');
      RunSQL(SC.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;

function TATTNTC_NEW.ParseData: Boolean;
var
  SEQ,i : Integer;
  MAINT_NO,QUA,DOC_NO,MESSAGE1,MESSAGE2,MESSAGE3:String;
  SC1,SC2 : TSQLCreate;
begin

  Result := True;

  SEQ := 0;
  SC1 := TSQLCreate.Create;
  SC2 := TSQLCreate.Create;
//===============================
  // BGM 전자문서시작
  try
    try
    if FindSegment('BGM','', 7) then
    begin
      //관리번호
      MAINT_NO := SEGDATA.Items[4];
      //문서기능, 마스터테이블의 MESSAGE1 필드
      MESSAGE1 := SEGDATA.Items[5];
      //문서유형
      MESSAGE2 := SEGDATA.Items[6];

      //문서유형이 빈값이면 'NA'로 간주
      if Trim(MESSAGE2) = '' then MESSAGE2 := 'NA';

    end;

  //===============================
    //중복되는관리번호 있으면 삭제.

    //마스터테이블
    SC1.DMLType := dmlDelete;
    SC1.SQLHeader('ATTNTC_H');
    SC1.ADDWhere('MAINT_NO',MAINT_NO);
    RunSQL(SC1.CreateSQL);//RunSQL사용시 SC1의 버퍼내용이 사라짐.

    //디테일테이블
    SC2.DMLType := dmlDelete;
    SC2.SQLHeader('ATTNTC_D');
    SC2.ADDWhere('KEYY',MAINT_NO);
    RunSQL(SC2.CreateSQL);//RunSQL사용시 SC1의 버퍼내용이 사라짐.

  //===============================
    //BGM 파싱
    //데이터삽입모드(마스터테이블)
    SC1.DMLType := dmlInsert;
    SC1.SQLHeader('ATTNTC_H');
    //관리번호
    SC1.ADDValue('MAINT_NO',MAINT_NO);
    //등록일자
    SC1.ADDValue('DATEE',FormatDateTime('YYYYMMDD', Now));

    //문서기능
    SC1.ADDValue('MESSAGE1', MESSAGE1);
    //문서유형
    SC1.ADDValue('MESSAGE2', MESSAGE2);

    //사용자
    SC1.ADDValue('USER_ID',LoginData.sID);


  //===============================
    // DOC 첨부신청정보 전자문서번호

    if FindSegment('DOC','',11) then
    begin
      //문서명
      SC1.ADDValue('DOC_CD',SEGDATA.Items[0]);
      //전자문서번호
      SC1.ADDValue('DOC_NO',SEGDATA.Items[4]);
    end;


  //===============================
    // NAD 첨부신청정보 수발신인 사업자번호, 상호 ,식별자
    for i:= 1 to 2 do
    begin
      if FindSegment('NAD', '', 19) then
      begin
        QUA := SEGDATA.Items[0];
        // 발신인정보 일때
        if QUA = 'MS' then
        begin
           //발신인 사업자등록번호
           SC1.ADDValue('EX_ELEC',SEGDATA.Items[1]);
           //발신인상호
           SC1.ADDValue('EX_NAME1', SEGDATA.Items[9]);
           //발신인식별자
           SC1.ADDValue('EX_NAME2', SEGDATA.Items[10]);
           //발신인식별자2번째줄.
           SC1.ADDValue('EX_NAME3', SEGDATA.Items[11]);
        end;

        //수신인정보 일때
        if QUA = 'MR' then
        begin
           //수신인상호
           SC1.ADDValue('SR_NAME1', SEGDATA.Items[9]);
           //수신인식별자
           SC1.ADDValue('SR_NAME2', SEGDATA.Items[10]);
           //은행코드
           SC1.ADDValue('SR_NAME3', SEGDATA.Items[11]);
        end;
      end;
    end;

  //===============================
    //FTX, 평문기술 기타정보, 비고 - 선택사항
//    if FindSegment('FTX','',11) then
//    begiN
//      SC1.ADDValue('FTX_DOC1',SEGDATA.Items[5]);
//      SC1.ADDValue('FTX_DOC2',SEGDATA.Items[6]);
//    end;

      IF NextSegment(19,'FTX','',11) then
      begin
        SC1.ADDValue('FTX_DOC1',SEGDATA.Items[5]+#13#10+SEGDATA.Items[5]+#13#10+SEGDATA.Items[5]+#13#10+SEGDATA.Items[5]+#13#10+SEGDATA.Items[5]);
        if NextSegment(11,'FTX','',11) then
          SC1.ADDValue('FTX_DOC2',SEGDATA.Items[5]+#13#10+SEGDATA.Items[5]+#13#10+SEGDATA.Items[5]+#13#10+SEGDATA.Items[5]+#13#10+SEGDATA.Items[5]);
      end;

  //===============================
    //RFF 참조번호 근거서류첨부서 전자문서번호 기재.
    if FindSegment('RFF','',3) then
    begin
      // 한정어가 ACE 일때
       if TRIM(SEGDATA.Items[0]) = 'ACE' then
       begin
         //근거서류첨부서의 전자문서번호(근거서류 첨부서번호)
         SC1.ADDValue('ATT_DOCNO',SEGDATA.Items[1]);
       end;
//       else
//          SC1.ADDValue('ATT_DOCNO','TEST');

    end;

    //마스터테이블 데이터 삽입.
    RunSQL(SC1.CreateSQL);

  //===============================
//    DOC그룹(최대 999행)
    for SEQ := 1 to 999 do
    begin
      SC2.DMLType := dmlInsert;
      SC2.SQLHeader('ATTNTC_D');
      SC2.ADDValue('KEYY',MAINT_NO);

      //DOC 근거서류명, 문서번호, 전자문서번호 (Message Detail)
      if FindSegment('DOC','',11) then
      begin
        SC2.ADDValue('SEQ', SEQ);
        SC2.ADDValue('ATT_NO', SEGDATA.Items[4]);
        SC2.ADDValue('ATT_CD', SEGDATA.Items[0]);
        SC2.ADDValue('ATT_ELEC', SEGDATA.Items[6]);
      end;

      //BUS BUSINESS FUNCTION 근거서류 첨부여부
      if FindSegment('BUS','',11) then
      begin
        if SEGDATA.Items[0] = '1' then
        begin
            SC2.ADDValue('ATT_CK', SEGDATA.Items[4]);
        end;
      end;

      //FTX 평문기술(FreeText) 선택
      for i:=1 to 2 do
      begin
        if FindSegment('FTX','',11) then
        begin
          if SEGDATA.Items[0] = 'AAI' then
          begin
            SC2.ADDValue('MDOC1',SEGDATA.Items[5]);
            SC2.ADDValue('MDOC2',SEGDATA.Items[6]);
            SC2.ADDValue('MDOC3',SEGDATA.Items[7]);
            SC2.ADDValue('MDOC4',SEGDATA.Items[8]);
            SC2.ADDValue('MDOC5',SEGDATA.Items[9]);
          end;
          if SEGDATA.Items[0] = 'ACB' then
          begin
            SC2.ADDValue('DDOC1',SEGDATA.Items[5]);
            SC2.ADDValue('DDOC2',SEGDATA.Items[6]);
            SC2.ADDValue('DDOC3',SEGDATA.Items[7]);
            SC2.ADDValue('DDOC4',SEGDATA.Items[8]);
            SC2.ADDValue('DDOC5',SEGDATA.Items[9]);
          end;
        end;
      end;

      //디테일테이블 저장
      RunSQL(SC2.CreateSQL);

      if not FindSegment('DOC','',11) then
      begin
        Break;
      end;

    end;
    except
      on E:Exception do
      begin
        Result := False;
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC1.Free;
    SC2.Free;
  end;
end;
{ TATTNTC_NEW }

procedure TLDANTC_NEW.DefineList;
var
  MAINT_NO, MESSAGE1, MESSAGE2 : String;
  SC : TSQLCreate;

begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM', '10', 4) THEN
      BEGIN
        MAINT_NO := SEGDATA.Items[1];
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        IF Trim(MESSAGE2) = '' then MESSAGE1 := 'NA';
      end;

    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'LDANTC');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', 0);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', '');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'LDANTC');
      RunSQL(SC.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;

function TLDANTC_NEW.ParseData: Boolean;
var
  i : Integer;
  MAINT_NO, QUA,  MESSAGE1, MESSAGE2:String;
  SC1 : TSQLCreate;
begin

  Result := True;

  SC1 := TSQLCreate.Create;

//===============================
  // BGM 전자문서시작
  try
    try
    if FindSegment('BGM' ,'10', 4) then
    begin
      //관리번호
      MAINT_NO := SEGDATA.Items[1];
      //문서기능
      MESSAGE1 := SEGDATA.Items[2];
      //문서유형
      MESSAGE2 := SEGDATA.Items[3];

      //문서기능이 빈값이면 '9'로 간주
      IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
      //문서유형이 빈값이면 'NA'로 간주
      IF Trim(MESSAGE2) = '' then MESSAGE2 := 'NA';

    end;

  //===============================
    //중복되는관리번호 있으면 삭제.

    // LDANTC 테이블
    SC1.DMLType := dmlDelete;
    SC1.SQLHeader('LDANTC');
    SC1.ADDWhere('MAINT_NO',MAINT_NO);

    RunSQL(SC1.CreateSQL);

  //===============================
    //BGM 전자문서 시작, 관리번호 및 응답유형
    SC1.DMLType := dmlInsert;
    SC1.SQLHeader('LDANTC');
    //관리번호
    SC1.ADDValue('MAINT_NO',MAINT_NO);
    //BGM구분
    SC1.ADDValue('BGM_GUBUN',SEGDATA.Items[0]);
    //등록일자
    SC1.ADDValue('DATEE',FormatDateTime('YYYYMMDD', Now));
    //문서기능
    SC1.ADDValue('MESSAGE1', MESSAGE1);
    //문서유형
    SC1.ADDValue('MESSAGE2', MESSAGE2);
    //사용자
    SC1.ADDValue('USER_ID',LoginData.sID);

  //===============================
    //NAD 수신인, 통지은행
    for i := 1 to 2 do
    begin
      if FindSegment('NAD','11',4) then
      begin
         QUA := SEGDATA.Items[0];
         //수신인 정보
         if QUA = 'MR' then
         begin
            SC1.ADDValue('APP_NAME1', SEGDATA.Items[1]);
            SC1.ADDValue('APP_NAME2', SEGDATA.Items[2]);
            SC1.ADDValue('APP_NAME3', SEGDATA.Items[3]);
         end;
         //통지은행 전자서명

         if QUA = 'AX' then
         begin
            SC1.ADDValue('BK_NAME1', SEGDATA.Items[1]);
            SC1.ADDValue('BK_NAME2', SEGDATA.Items[2]);
            SC1.ADDValue('BK_NAME3', SEGDATA.Items[3]);
         end;
      end;
    end;

  //==============================
    //RFF 내국신용장 번호 , 세금계산서 포함해서 다시짜기.
    for i := 1 to 3 do
    begin
      if FindSegment('RFF','12',2) then
      begin
        QUA := SEGDATA.Items[0];
        // 내국신용장 번호
        if QUA = 'LC' then
          SC1.ADDValue('LC_NO',SEGDATA.Items[1]);
        // 물품수령증명서(인수증) 번호
        if QUA = 'REN' then
          SC1.ADDValue('RC_NO',SEGDATA.Items[1]);
        // 세금계산서번호
        if QUA = '2AI' then
          SC1.ADDValue('FIN_NO',SEGDATA.Items[1]);
      end;
    end;

  //==============================
    //MOA 외화금액
    for i:= 1 to 2 do
    begin
      if FindSegment('MOA','13',3) then
      begin
        QUA := SEGDATA.Items[0];
        //외화금액
        if QUA = '2AD' then
        begin
           //금액
           SC1.ADDValue('AMT1',SEGDATA.Items[1]);
           //통화단위
           SC1.ADDValue('AMT1C',SEGDATA.Items[2]);
        end;
        //원화환산금액
        if QUA = '2AE' then
        begin
           //금액
           SC1.ADDValue('AMT2',SEGDATA.Items[1]);
           //원화표시(KRW)
           SC1.ADDValue('AMT2C',SEGDATA.Items[2]);
        end;
      end;
    end;

  //====================================
    //CUX 적용환율
    if FindSegment('CUX','14',1) then
    begin
      //원화환산금액 산출에 적용된 환율
      SC1.ADDValue('RATE',SEGDATA.Items[0]);
    end;

  //====================================
    //DTM 통지일지 - C인것 제대로 확인
    for i:=1 to 3 do
    begin
      IF FindSegment('DTM','15',3) then
      begin
         QUA := SEGDATA.Items[0];
         //통지일자
         if QUA = '184' then
           SC1.ADDValue('RES_DATE',EdiDateFormat(SEGDATA.Items[1]));
         //최종결제일
         if QUA = '265' then
           SC1.ADDValue('SET_DATE',EdiDateFormat(SEGDATA.Items[1]));
         //NEGO 일자
         if QUA = '2AD' then
           SC1.ADDValue('NG_DATE',EdiDateFormat(SEGDATA.Items[1]));
      end;
    end;

  //======================================
    //FTX 기타사항
    IF FindSegment('FTX','16',6) then
      SC1.ADDValue('REMARK1',SEGDATA.Items[1]+#13#10+SEGDATA.Items[2]+#13#10+SEGDATA.Items[3]+#13#10+SEGDATA.Items[4]+#13#10+SEGDATA.Items[5]);

  //======================================
    //FII 통지은행
    IF FindSegment('FII','17',3) then
    begin
      QUA := SEGDATA.Items[0];
      //한정어 = 통지은행
      if QUA = 'BI' then
      begin
        //통지은행명
        SC1.ADDValue('BANK1',SEGDATA.Items[1]);
        //통지은행 사항(국가/도시/지점명)
        SC1.ADDValue('BANK2',SEGDATA.Items[2]);
      end;
    end;

    RunSQL(SC1.CreateSQL);

    except
    on E:Exception do
    begin
       ShowMessage(E.Message);
       Result := False;
    end;
    end;
  finally
    SC1.Free;
  end;
end;

{ TSPCNTC_NEW }

procedure TSPCNTC_NEW.DefineList;
var
  MAINT_NO, MESSAGE1, MESSAGE2 : String;
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM', '10', 7) THEN
      BEGIN

        MAINT_NO := SEGDATA.Items[4];

        MESSAGE1 := SEGDATA.Items[5];
        MESSAGE2 := SEGDATA.Items[6];
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        IF Trim(MESSAGE2) = '' then MESSAGE1 := 'AB';
      end;

    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'SPCNTC');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', 0);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', '');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'SPCNTC');
      RunSQL(SC.CreateSQL);

    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;

function TSPCNTC_NEW.ParseData: Boolean;
var
  i,SEQ : Integer;
  MAINT_NO, QUA,  MESSAGE1, MESSAGE2:String;
  SC1, SC2 : TSQLCreate;
begin

  Result := True;

  SC1 := TSQLCreate.Create;
  SC2 := TSQLCreate.Create;

//===============================
  // BGM 전자문서시작
  try
    try
    if FindSegment('BGM' ,'10', 7) then
    begin
      //관리번호
      MAINT_NO := SEGDATA.Items[4];
      //문서기능
      MESSAGE1 := SEGDATA.Items[5];
      //문서유형
      MESSAGE2 := SEGDATA.Items[6];

      //문서기능이 빈값이면 '9'로 간주
      IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
      //문서유형이 빈값이면 'AB'로 간주
      IF Trim(MESSAGE2) = '' then MESSAGE2 := 'AB';

    end;

  //===============================
    //중복되는관리번호 있으면 삭제.

    // SPCNTC 마스터 테이블
    SC1.DMLType := dmlDelete;
    SC1.SQLHeader('SPCNTC_H');
    SC1.ADDWhere('MAINT_NO',MAINT_NO);

    // SPCNTC 디테일 테이블
    SC2.DMLType := dmlDelete;
    SC2.SQLHeader('SPCNTC_D');
    SC2.ADDWhere('KEYY',MAINT_NO);

    RunSQL(SC1.CreateSQL);
    RunSQL(SC2.CreateSQL);
  //===============================

    //BGM 전자문서 시작, 관리번호 및 응답유형
    SC1.DMLType := dmlInsert;
    SC1.SQLHeader('SPCNTC_H');

    //관리번호
    SC1.ADDValue('MAINT_NO',MAINT_NO);
    //응답서 구분
    SC1.ADDValue('BGM_GUBUN',SEGDATA.Items[0]);
    //등록일자
    SC1.ADDValue('DATEE',FormatDateTime('YYYYMMDD', Now));
    //문서기능
    SC1.ADDValue('MESSAGE1', MESSAGE1);
    //문서유형
    SC1.ADDValue('MESSAGE2', MESSAGE2);
    //사용자
    SC1.ADDValue('USER_ID',LoginData.sID);

  //===================================
    //DTM 통지일자/매입일자
    for i:=1 to 2 do
    begin
      if FindSegment('DTM','11',3) then
      begin
        QUA := SEGDATA.Items[0];
        //통지일자
        if QUA = '691' then
        begin
          //날짜포맷 CCYYMMDD.
           SC1.ADDValue('NT_DATE',SEGDATA.Items[1]);
        end;
        //매입일자
        if QUA = '2AD' then
        begin
           SC1.ADDValue('CP_DATE',SEGDATA.Items[1]);
        end;

      end;
    end;
  //====================================
    //RFF 신청인이 송신한 추심(매입)의뢰서의 전자문서번호/ 내국신용장번호 /매입번호/ 물품수령증명서번호/세금계산서번호
    for i := 1 to 5 do
    begin
      if FindSegment('RFF','12',3) then
      begin
        QUA := SEGDATA.Items[0];
        // 신청인이 송신한 추심(매입)의뢰서의 전자문서번호
        if QUA = 'DM' then
           SC1.ADDValue('DM_NO',SEGDATA.Items[1]);
        // 내국신용장 번호
        if QUA = 'LC' then
           SC1.ADDValue('LC_NO',SEGDATA.Items[1]);
        // 매입 번호  (추심매입신청번호)
        IF QUA = '2BA' then
           SC1.ADDValue('CP_NO',SEGDATA.Items[1]);
        // 물품수령증명서(인수증) 번호
        IF QUA = 'REN' then
            SC1.ADDValue('RC_NO',SEGDATA.Items[1]);
        //세금계산서 번호
        IF QUA = '2AI' then
            SC1.ADDValue('FIN_NO',SEGDATA.Items[1]);
      end;
    end;


  //=====================================
    //FII 추심(매입) 은행 역;까
    if FindSegment('FII','13',14) then
    begin
       QUA := SEGDATA.Items[0];
       if QUA = 'OM' then
       begin
          //추심(매입)은행코드
          SC1.ADDValue('CP_BANK',SEGDATA.Items[5]);
          //추심(매입)은행명 평문
          SC1.ADDValue('CP_BANKNAME', SEGDATA.Items[11]);
          //추심(매입)은행 지점명 평문
          SC1.ADDValue('CP_BANKBU', SEGDATA.Items[12]);

       end;
    end;

  //======================================
    //NAD 추심(매입) 신청인의 상호, 대표자명 / 추심(매입)은행 전자서명
    for i:=1 to 2 do
    begin
      IF FindSegment('NAD','14',19) then
      begin
        QUA := SEGDATA.Items[0];
        //추심(매입)신청인(Applicant)
        if QUA = '4AA' then
        begin
           //신청인 사업자등록번호
           SC1.ADDValue('APP_NO', SEGDATA.Items[1]);
           //신청인 상호명
           SC1.ADDValue('APP_SNAME', SEGDATA.Items[9]);
           //신청인 대표자명
           SC1.ADDValue('APP_NAME', SEGDATA.Items[10]);
        end;
        //추심(매입)은행 전자서명
        if QUA = 'OM' then
        begin
           //전자서명값
           SC1.ADDValue('CP_BANKELEC', SEGDATA.Items[9]);
        end;
      end;
    end;


  //======================================
    //MOA 외화금액,원화금액,최종지급액,수수료합계
    for i:=1 to 4 do
    begin
      If FindSegment('MOA','15',5) then
      begin
        QUA := SEGDATA.Items[0];
        //추심(매입) 금액(외화)
        if QUA = '2AD' then
        begin
          //외화금액
          SC1.ADDValue('CP_AMTU', SEGDATA.Items[1]);
          //통화단위
          SC1.ADDValue('CP_AMTC', SEGDATA.Items[2]);
        end;
        //추심(매입) 금액(원화)
        if QUA = '2AE' then
        begin
          //원화금액
          SC1.ADDValue('CP_AMT', SEGDATA.Items[1]);
        end;
        // 최종 지급액
        if QUA = '60' then
        begin
          //최종지급액
          SC1.ADDValue('CP_TOTAMT', SEGDATA.Items[1]);
          //최종지급액 통화단위
          SC1.ADDValue('CP_TOTAMTC', SEGDATA.Items[2]);
        end;
        // 수수료(이자)합계
        if QUA = '131' then
        begin
          //수수료합계
          SC1.ADDValue('CP_TOTCHARGE', SEGDATA.Items[1]);
          //수수료합계 통화단위
          SC1.ADDValue('CP_TOTCHARGEC', SEGDATA.Items[2]);
        end;
      end;
    end;

  //==========================================
    //FTX 평문기술 (통보내용)
    if FindSegment('FTX','16',11) then
    begin
      QUA := SEGDATA.Items[0];
      if QUA = 'ZZZ' then
      begin
        SC1.ADDValue('CP_FTX1', SEGDATA.Items[5]+SEGDATA.Items[6]+SEGDATA.Items[7]+SEGDATA.Items[8]+SEGDATA.Items[9]);
      end;
    end;

    //마스터테이블 저장
    RunSQL(SC1.CreateSQL);

    //==========================================
    //ALC 그룹 (최대 99번) ALC~DTM
    for SEQ :=1 to 99 do
    begin
      SC2.DMLType := dmlInsert;
      SC2.SQLHeader('SPCNTC_D');
      SC2.ADDValue('KEYY',MAINT_NO);
      SC2.ADDValue('SEQ', IntToStr(SEQ));
      //ALC 수수료 유형
      if FindSegment('ALC','17',8) then
      begin
        QUA := SEGDATA.Items[0];
        if QUA = 'C' then
        begin
          //계산서번호
          SC2.ADDValue('CHARGE_NO',SEGDATA.Items[1]);
          //수수료(이자) 유형
          SC2.ADDValue('CHARGE_TYPE',SEGDATA.Items[2]);
        end;
      end
      else
        Break;

      //PCD 적용요율 (백분율)
      if FindSegment('PCD','20',5) then
      begin
        QUA := SEGDATA.Items[0];
        if QUA = '2' then
        begin
          SC2.ADDValue('CHARGE_RATE', StringReplace(SEGDATA.Items[1],'0','',[rfReplaceAll, rfIgnoreCase]));
        end;
      end;

      //MOA 대상금액/산출금액
      for i:= 1 to 2 do
      begin
        if FindSegment('MOA','21',5) then
        begin
           QUA := SEGDATA.Items[0];

           //대상금액
           if QUA = '25' then
           begin
             //대상금액 통화단위
             SC2.ADDValue('BAS_AMTC',SEGDATA.Items[2]);
             //대상금액
             SC2.ADDValue('BAS_AMT', SEGDATA.Items[1]);
           end;
           //산출금액
           if QUA = '23' then
           begin                                         
             //산출금액 통화단위
             SC2.ADDValue('CHA_AMTC',SEGDATA.Items[2]);
             //산출금액
             SC2.ADDValue('CHA_AMT', SEGDATA.Items[1]);
           end;
        end;
      end;

      //CUX 적용환율
      if FindSegment('CUX','22',10) then
          SC2.ADDValue('CUX_RATE',SEGDATA.Items[8]);

      //DTM 적용일수, 적용기간
      for i:=1 to 2 do
      begin
        if FindSegment('DTM','23',3) then
        begin
          QUA := SEGDATA.Items[0];
          //적용일수
          if QUA = '221' then
          begin
             SC2.ADDValue('SES_DAY',SEGDATA.Items[1]);
          end;
          //적용기간
          if QUA = '257' then
          begin
             SC2.ADDValue('SES_SDATE', EdiDateFormat(LeftStr(SEGDATA.Items[1],6)));
             SC2.ADDValue('SES_EDATE', EdiDateFormat(RightStr(SEGDATA.Items[1],6)));
          end;

        end;
      end;

      RunSQL(SC2.CreateSQL);

    end;

    except
    on E:Exception do
    begin
       Result := False;
       ShowMessage(E.Message);
    end;
    end;
  finally
    SC1.Free;                 
    SC2.Free;
  end;
end;

{ TVATBIL_NEW }

procedure TVATBIL_NEW.DefineList;
var
  MAINT_NO, MESSAGE1, MESSAGE2 : String;
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM', '10', 5) THEN
      BEGIN
        MAINT_NO := SEGDATA.Items[2];
        MESSAGE1 := SEGDATA.Items[3];
        MESSAGE2 := SEGDATA.Items[4];
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
      end;

    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'VATBIL');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', 0);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', '');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'VATBIL');
      RunSQL(SC.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;
function TVATBIL_NEW.ParseData: Boolean;
var
  i,j,SEQ : Integer;
  MAINT_NO, PAI, QUA,  MESSAGE1, MESSAGE2, IMD1, IMD2 ,FTX:String;
  SC1, SC2 : TSQLCreate;
begin

  Result := True;

  SC1 := TSQLCreate.Create;
  SC2 := TSQLCreate.Create;
//===============================
  // BGM 전자문서시작
  try
    try
    if FindSegment('BGM' ,'10', 5) then
    begin
      //관리번호
      MAINT_NO := SEGDATA.Items[2];
      //문서기능
      MESSAGE1 := SEGDATA.Items[3];
      //문서유형
      MESSAGE2 := SEGDATA.Items[4];

      //문서기능이 빈값이면 '9'로 간주
      IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';

    end;

  //===============================
    //중복되는관리번호 있으면 삭제.

    // VATBI2_H 테이블
    SC1.DMLType := dmlDelete;
    SC1.SQLHeader('VATBI2_H');
    SC1.ADDWhere('MAINT_NO',MAINT_NO);

    // VATBI2_D 테이블
    SC2.DMLType := dmlDelete;
    SC2.SQLHeader('VATBI2_D');
    SC2.ADDWhere('KEYY',MAINT_NO);

    RunSQL(SC1.CreateSQL);
    RunSQL(SC2.CreateSQL);

  //===============================
    //BGM 전자문서 시작, 관리번호 및 응답유형
    SC1.DMLType := dmlInsert;
    SC1.SQLHeader('VATBI2_H');
    //관리번호
    SC1.ADDValue('MAINT_NO',MAINT_NO);
    //세금계산서코드
    SC1.ADDValue('VAT_CODE',SEGDATA.Items[0]);
    //세금계산서종류
    SC1.ADDValue('VAT_TYPE',SEGDATA.Items[1]);
    //등록일자
    SC1.ADDValue('DATEE',FormatDateTime('YYYYMMDD', Now));
    //문서기능
    SC1.ADDValue('MESSAGE1', MESSAGE1);
    //응답유형
    SC1.ADDValue('MESSAGE2', MESSAGE2);
    //사용자
    SC1.ADDValue('USER_ID',LoginData.sID);

  //=================================
    //GIS 일반지시자(C) 세금계산서의 수정 사유 코드를 기재하는 전송항목
    if FindSegment('GIS','11',2)  then
    begin
      //수정사유코드
      SC1.ADDValue('NEW_INDICATOR', SEGDATA.Items[1]);
    end;
  //=================================
    //RFF 참조번호 세금계산서 정보
    for i:=1 to 5 do
    begin
      if FindSegment('RFF','12',2) then
      begin
        QUA := SEGDATA.Items[0];
        //세금계산서 책번호의 권을 기재
        if QUA = 'RE' then
          SC1.ADDValue('RE_NO',SEGDATA.Items[1]);
        //세금계산서 책번호의 호를 기재
        if QUA = 'SE' then
          SC1.ADDValue('SE_NO',SEGDATA.Items[1]);
        //세금계산서 일련번호를 기재
        if QUA = 'FS' then
          SC1.ADDValue('FS_NO',SEGDATA.Items[1]);
        //참조번호 기재
        if QUA = 'ACE' then
          SC1.ADDValue('ACE_NO',SEGDATA.Items[1]);
        // 마이너스 세금계산서 번호
        if QUA = 'DM' then
          SC1.ADDValue('RFF_NO',SEGDATA.Items[1]);
      end;

    end;
  //==================================
    //NAD그룹 NAD-FTX-IMD 최대 3행
     for SEQ := 1 to 3 do
     begin
      //NAD 상호 및 주소 관련사항 기재
      if FindSegment('NAD','13',14) then
      begin
        QUA := SEGDATA.Items[0];
        
        //공급자
        if QUA = 'SE' then
        begin
           //공급자
           SC1.ADDValue('SE_CODE',SEGDATA.Items[0]);
           //사업자등록번호
           SC1.ADDValue('SE_SAUP',SEGDATA.Items[1]);
           //대표자명 3036태그 3행
           SC1.ADDValue('SE_NAME3',SEGDATA.Items[10]);
           //상호 3036태그 1,2행
           SC1.ADDValue('SE_NAME1',SEGDATA.Items[8]);
           SC1.ADDValue('SE_NAME2',SEGDATA.Items[9]);
           //주소 각 에딧당 최대 30byte (5개 토탈해서 150byte)
           SC1.ADDValue('SE_ADDR1',SEGDATA.Items[3]);
           SC1.ADDValue('SE_ADDR2',SEGDATA.Items[4]);
           SC1.ADDValue('SE_ADDR3',SEGDATA.Items[5]);
           SC1.ADDValue('SE_ADDR4',SEGDATA.Items[6]);
           SC1.ADDValue('SE_ADDR5',SEGDATA.Items[7]);
           // 종사업장번호 일련번호 4자리
           SC1.ADDValue('SE_SAUP1',SEGDATA.Items[11]);

           {FTX 담당부서 및 담당자, 담당자 연락처 정보를 기재
            공급자 또는 수탁자의 담당자정보인경우 FTX항목 1번반복.
            공급받는자의 담당자정보인경우 FTX항목 2번반복 가능.}
           if FindSegment('FTX','20',6) then
           begin
             QUA := SEGDATA.Items[0];
             if QUA = '5AN' then
             begin
                //담당부서명
                SC1.ADDValue('SE_FTX1',SEGDATA.Items[1]);
                //담당자명
                SC1.ADDValue('SE_FTX2',SEGDATA.Items[2]);
                //전화번호
                SC1.ADDValue('SE_FTX3',SEGDATA.Items[3]);
                //이메일아이디
                SC1.ADDValue('SE_FTX4',SEGDATA.Items[4]);
                //이메일도메인
                SC1.ADDValue('SE_FTX5',SEGDATA.Items[5]);
             end;
           end;

           IMD1 := '';
           IMD2 := '';

           //IMD 공급자 업태
           if FindSegment('IMD','21',2) then
           begin
                QUA := SEGDATA.Items[0];
                if QUA = 'SG' then
                  IMD1 := IMD1 + SEGDATA.Items[1] + #13#10
                else if QUA = 'HN' then
                  IMD2 := IMD2 + SEGDATA.Items[1] + #13#10;

                for i:=1 to 5 do
                begin
                  if NextSegment(2,'IMD','21',2) then
                  begin
                      QUA := SEGDATA.Items[0];

                      //업태
                      if QUA = 'SG' then
                      begin
                        IMD1 := IMD1 + SEGDATA.Items[1] + #13#10;
                      end;

                      //종목
                      if QUA = 'HN' then
                      begin
                        IMD2 := IMD2 + SEGDATA.Items[1] + #13#10;
                      end;
                  end;
                end;
             //업태
             SC1.ADDValue('SE_UPTA','Y');
             SC1.ADDValue('SE_UPTA1',IMD1);
             //종목
             SC1.ADDValue('SE_ITEM','Y');
             SC1.ADDValue('SE_ITEM1',IMD2);
           end;
        end;
        //공급받는자
        if QUA = 'BY' then
        begin
           //공급받는자
           SC1.ADDValue('BY_CODE',SEGDATA.Items[0]);
           {사업자등록구분 1AB이면 등록번호에 사업자등록번호, 173이면 개인주민등록번호, 253이면
           "9999999999999"를 입력한 후, FTX(비고) 항목에 관련 정보(외국인 등록번호, 여권번호 등)를 기재 }
           SC1.ADDValue('BY_SAUP_CODE',SEGDATA.Items[2]);
           //사업자등록번호
           SC1.ADDValue('BY_SAUP',SEGDATA.Items[1]);
           //대표자명 3036태그 3행
           SC1.ADDValue('BY_NAME3',SEGDATA.Items[10]);
           //상호 3036태그 1,2행
           SC1.ADDValue('BY_NAME1',SEGDATA.Items[8]);
           SC1.ADDValue('BY_NAME2',SEGDATA.Items[9]);
           //주소 각 에딧당 최대 30byte (5개 토탈해서 150byte)
           SC1.ADDValue('BY_ADDR1',SEGDATA.Items[3]);
           SC1.ADDValue('BY_ADDR2',SEGDATA.Items[4]);
           SC1.ADDValue('BY_ADDR3',SEGDATA.Items[5]);
           SC1.ADDValue('BY_ADDR4',SEGDATA.Items[6]);
           SC1.ADDValue('BY_ADDR5',SEGDATA.Items[7]);
           // 종사업장번호 일련번호 4자리
           SC1.ADDValue('BY_SAUP1',SEGDATA.Items[11]);

           {FTX 담당부서 및 담당자, 담당자 연락처 정보를 기재
            공급자 또는 수탁자의 담당자정보인경우 FTX항목 1번반복.
            공급받는자의 담당자정보인경우 FTX항목 2번반복 가능.}

             if FindSegment('FTX','20',6) then
             begin
               QUA := SEGDATA.Items[0];
               if QUA = '5AN' then
               begin
                  //담당부서명
                  SC1.ADDValue('BY_FTX1',SEGDATA.Items[1]);
                  //담당자명
                  SC1.ADDValue('BY_FTX2',SEGDATA.Items[2]);
                  //전화번호
                  SC1.ADDValue('BY_FTX3',SEGDATA.Items[3]);
                  //이메일아이디
                  SC1.ADDValue('BY_FTX4',SEGDATA.Items[4]);
                  //이메일도메인
                  SC1.ADDValue('BY_FTX5',SEGDATA.Items[5]);
               end;
             end;
             //담당자가 2명째 있을경우 한번더 실행.
             if FindSegment('FTX','20',6) then
             begin
               QUA := SEGDATA.Items[0];
               if QUA = '5AN' then
               begin
                  //담당부서명
                  SC1.ADDValue('BY_FTX1_1',SEGDATA.Items[1]);
                  //담당자명
                  SC1.ADDValue('BY_FTX2_1',SEGDATA.Items[2]);
                  //전화번호
                  SC1.ADDValue('BY_FTX3_1',SEGDATA.Items[3]);
                  //이메일아이디
                  SC1.ADDValue('BY_FTX4_1',SEGDATA.Items[4]);
                  //이메일도메인
                  SC1.ADDValue('BY_FTX5_1',SEGDATA.Items[5]);
               end;
             end;


           IMD1 := '';
           IMD2 := '';
           //공급받는자 업태
           if FindSegment('IMD','21',2) then
           begin
                QUA := SEGDATA.Items[0];
                if QUA = 'SG' then
                  IMD1 := IMD1 + SEGDATA.Items[1] + #13#10
                else if QUA = 'HN' then
                  IMD2 := IMD2 + SEGDATA.Items[1] + #13#10;

                for i:=1 to 5 do
                begin
                  if NextSegment(2,'IMD','21',2) then
                  begin
                      QUA := SEGDATA.Items[0];

                      //업태
                      if QUA = 'SG' then
                      begin
                        IMD1 := IMD1 + SEGDATA.Items[1] + #13#10;
                      end;

                      //종목
                      if QUA = 'HN' then
                      begin
                        IMD2 := IMD2 + SEGDATA.Items[1] + #13#10;
                      end;
                  end;
                end;
             //업태
             SC1.ADDValue('BY_UPTA','Y');
             SC1.ADDValue('BY_UPTA1',IMD1);
             //종목
             SC1.ADDValue('BY_ITEM','Y');
             SC1.ADDValue('BY_ITEM1',IMD2);
           end;
        end;

        //수탁자(선택
        if QUA = 'AG' then
        begin
           //수탁자
           SC1.ADDValue('AG_CODE',SEGDATA.Items[0]);
           //사업자등록번호
           SC1.ADDValue('AG_SAUP',SEGDATA.Items[1]);
           //대표자명 3036태그 3행
           SC1.ADDValue('AG_NAME3',SEGDATA.Items[10]);
           //상호 3036태그 1,2행
           SC1.ADDValue('AG_NAME1',SEGDATA.Items[8]);
           SC1.ADDValue('AG_NAME2',SEGDATA.Items[9]);
           //주소 각 에딧당 최대 30byte (5개 토탈해서 150byte)
           SC1.ADDValue('AG_ADDR1',SEGDATA.Items[3]);
           SC1.ADDValue('AG_ADDR2',SEGDATA.Items[4]);
           SC1.ADDValue('AG_ADDR3',SEGDATA.Items[5]);
           SC1.ADDValue('AG_ADDR4',SEGDATA.Items[6]);
           SC1.ADDValue('AG_ADDR5',SEGDATA.Items[7]);
           // 종사업장번호 일련번호 4자리
           SC1.ADDValue('AG_SAUP1',SEGDATA.Items[11]);

           {FTX 담당부서 및 담당자, 담당자 연락처 정보를 기재
            공급자 또는 수탁자의 담당자정보인경우 FTX항목 1번반복.
            공급받는자의 담당자정보인경우 FTX항목 2번반복 가능.}
           if FindSegment('FTX','20',6) then
           begin
             QUA := SEGDATA.Items[0];
             if QUA = '5AN' then
             begin
                //담당부서명
                SC1.ADDValue('AG_FTX1',SEGDATA.Items[1]);
                //담당자명
                SC1.ADDValue('AG_FTX2',SEGDATA.Items[2]);
                //전화번호
                SC1.ADDValue('AG_FTX3',SEGDATA.Items[3]);
                //이메일아이디
                SC1.ADDValue('AG_FTX4',SEGDATA.Items[4]);
                //이메일도메인
                SC1.ADDValue('AG_FTX5',SEGDATA.Items[5]);
             end;
           end;


           IMD1 := '';
           IMD2 := '';

           // 수탁자 업태
           if FindSegment('IMD','21',2) then
           begin
                QUA := SEGDATA.Items[0];
                if QUA = 'SG' then
                  IMD1 := IMD1 + SEGDATA.Items[1] + #13#10
                else if QUA = 'HN' then
                  IMD2 := IMD2 + SEGDATA.Items[1] + #13#10;

                for i:=1 to 5 do
                begin
                  if NextSegment(2,'IMD','21',2) then
                  begin
                      QUA := SEGDATA.Items[0];

                      //업태
                      if QUA = 'SG' then
                      begin
                        IMD1 := IMD1 + SEGDATA.Items[1] + #13#10;
                      end;

                      //종목
                      if QUA = 'HN' then
                      begin
                        IMD2 := IMD2 + SEGDATA.Items[1] + #13#10;
                      end;
                  end;
                end;
             //업태
             SC1.ADDValue('AG_UPTA','Y');
             SC1.ADDValue('AG_UPTA1',IMD1);
             //종목
             SC1.ADDValue('AG_ITEM','Y');
             SC1.ADDValue('AG_ITEM1',IMD2);
           end;
        end;
      end;
     end;
  //=======================================
    //DTM 그룹 DTM-SEQ-MOA
    //DTM 일자 시각 및 기간
    if FindSegment('DTM','14',3) then
    begin
      QUA := SEGDATA.Items[0];
      if QUA = '182' then
      begin        //날짜          , 포맷코드값 -> YYYYMMDD포맷으로 변경
        SC1.ADDValue('DRAW_DAT',EdiDateFormat(SEGDATA.Items[1],SEGDATA.Items[2]));
      end;
    end;

    //SEQ 세금계산서 공급가액란의 공란수 표시.
    if FindSegment('SEQ','22',1) then
       SC1.ADDValue('DETAILNO',SEGDATA.Items[0]);

    //MOA 금액관련
    for i:=1 to 2 do
    begin
        if FindSegment('MOA','23',3) then
        begin
          QUA := SEGDATA.Items[0];
          // 공급가액 단위 KRW, 소숫점이하 삭제.
          if QUA = '79' then
          begin
            SC1.ADDValue('SUP_AMT',SEGDATA.Items[1]);
          end;
          // 세액 단위 KRW, 0세율일경우 0 입력, 소숫점이하 삭제.
          if QUA = '176' then
          begin
            SC1.ADDValue('TAX_AMT',SEGDATA.Items[1]);
          end;
        end;
    end;

  //=======================================
    //FTX 비고사항을 평문형태로 기재하는 복합데이터항목
    IF FindSegment('FTX','15',6) then
    begin
       FTX := '';
       QUA := SEGDATA.Items[0];
       if QUA = 'AAI' then
       begin
//          SC1.ADDValue('REMARK','Y');
            //비고
            FTX := SEGDATA.Items[1]+SEGDATA.Items[2]+SEGDATA.Items[3]+SEGDATA.Items[4]+SEGDATA.Items[5]+#13#10;
       end;
       if NextSegment(6,'FTX','15',5) then
       begin
            FTX := FTX+SEGDATA.Items[1]+SEGDATA.Items[2]+SEGDATA.Items[3]+SEGDATA.Items[4]+SEGDATA.Items[5]+#13#10;
       end;
       SC1.ADDValue('REMARK1',FTX);
    end;

  //=========================================
    //LIN그룹 LIN-DTM-IMD-FTX-QTY-PRI-MOA-CUX 최대 999행
    for SEQ := 1 to 999 do
    begin
      SC2.DMLType := dmlInsert;
      SC2.SQLHeader('VATBI2_D');
      SC2.ADDValue('KEYY',MAINT_NO);
      SC2.ADDValue('SEQ',SEQ);
      // LIN은 공급품목에 대한 세부내역 기재시 일련번호부분. 이지만 출력할 필요없는 항목.
      // DTM 공급물품에 대한 라인별 공급일자를 기재
      IF FindSegment('DTM','24',3) then
      begin
        QUA := SEGDATA.Items[0];
        if QUA = '35' then
        begin
           //공급일자
           SC2.ADDValue('DE_DATE',SEGDATA.Items[1]);
        end;
      end
      else
        //DTM을 더이상 찾을수 없으면 FOR문 종료
        break;
      //IMD 공급물품의 품명 기재
        IMD1 := '';
        if FindSegment('IMD','25',2) then
        begin
          //품명 기재
          IMD1 := IMD1 + SEGDATA.Items[1] + #13#10;
          for i:=1 to 3 do
          begin
            if NextSegment(2,'IMD','25',2) then
            begin
              IMD1 := IMD1 + SEGDATA.Items[1] + #13#10;
            end;
          end;
          //품목의 유형을 식별, 디폴트값은 1AA(품명)
          SC2.ADDValue('NAME_COD',SEGDATA.Items[0]);
          //품명
          SC2.ADDValue('NAME1',IMD1);
        end;


      FTX := '';
      //FTX 공급물품의 규격, 비고(참조사항)
      for i:=1 to 2 do
      begin
        if FindSegment('FTX','26',6) then
        begin
          QUA := SEGDATA.Items[0];
          //규격
          if QUA = 'AAA' then
          begin
            FTX := SEGDATA.Items[1]+SEGDATA.Items[2]+SEGDATA.Items[3]+SEGDATA.Items[4]+SEGDATA.Items[5]+#13#10;
            if NextSegment(6,'FTX','26',6) then
            begin
              FTX := FTX + SEGDATA.Items[1]+SEGDATA.Items[2]+SEGDATA.Items[3]+SEGDATA.Items[4]+SEGDATA.Items[5]+#13#10;
            end;
            SC2.ADDValue('SIZE1',FTX);
          end;
          //비고(참조사항)
          if QUA = 'ACB' then
          begin
            SC2.ADDValue('DE_REM1',SEGDATA.Items[1]+SEGDATA.Items[2]+SEGDATA.Items[3]+SEGDATA.Items[4]+SEGDATA.Items[5]);
          end;
        end;
      end;
      //QTY 공급물품수량, 수량소계
      for i := 1 to 2 do
      begin

         if FindSegment('QTY','27',3) then
         begin
           QUA := SEGDATA.Items[0];
           // 수량
           if QUA = '1' then
           begin
             //수량 기재. 소수점2자리까지, -값을 허용
              SC2.ADDValue('QTY',SEGDATA.Items[1]);
             //수량 단위
              SC2.ADDValue('QTY_G',SEGDATA.Items[2]);
           end;
           //수량소계
           if QUA = '3' then
           begin
             //수량소계 기재.
              SC2.ADDValue('STQTY',SEGDATA.Items[1]);
             //수량 단위
              SC2.ADDValue('STQTY_G',SEGDATA.Items[2]);

           end;
         end;
      end;

      //PRI 가격, 가격유형 단가 단위기준 등의 가격정보
      if FindSegment('PRI','28',6) then
      begin
        QUA := SEGDATA.Items[0];
        if QUA = 'CAL' then
        begin
           //단가
           SC2.ADDValue('PRICE',SEGDATA.Items[1]);
        end;
      end;

      // MOA 금액기재
      for i:=1 to 6 do
      begin
        if FindSegment('MOA','29',3) then
        begin
          QUA := SEGDATA.Items[0];
          // 품목에 대한 공급가액을 기재 (원화)
          if QUA = '203' then
          begin
            //공급가액 원단위 이하 소수점을 허용하지 않음, -값 허용
            SC2.ADDValue('SUPAMT',SEGDATA.Items[1]);
          end;
          // 공급가액에 대한 세액을 기재 (원화)
          if QUA = '124' then
          begin
            //공급가액 원단위 이하 소수점을 허용하지 않음, -값 허용
            SC2.ADDValue('TAXAMT',SEGDATA.Items[1]);
          end;
          // 공급가액을 외화로 기재
          if QUA = '261' then
          begin
            //외화공급가액
            SC2.ADDValue('USAMT',SEGDATA.Items[1]);
            //외화단위
            SC2.ADDValue('USAMT_G',SEGDATA.Items[2]);
          end;
          // 공급가액의 소계 기재 (원화)
          if QUA = '17' then
          begin
            SC2.ADDValue('SUPSTAMT',SEGDATA.Items[1]);
          end;
          // 세액 소계
          if QUA = '168' then
          begin
            SC2.ADDValue('TAXSTAMT',SEGDATA.Items[1]);
          end;
          // 외화공급가액의 소계 기재
          if QUA = '289' then
          begin
            //외화공급가액 소계
            SC2.ADDValue('USSTAMT',SEGDATA.Items[1]);
            //외화단위
            SC2.ADDValue('USSTAMT_G',SEGDATA.Items[2]);
          end;
        end;
      end;

      //CUX 외화의 환율 소숫점이하 2자리
      if FindSegment('CUX','2A',1) then
        SC2.ADDValue('RATE', SEGDATA.Items[0]);

      RunSQL(SC2.CreateSQL);
    end;
  //============ LIN그룹 끝. ===============

  //========================================
    //MOA 세금계산서상의 총금액(공급가액합계+세액합계) 기재 , 공급가액의 총합계, 세액의 총합계
    for i:=1 to 4 do
    begin
      if FindSegment('MOA','17',3) then
      begin
        QUA := SEGDATA.Items[0];
        // 총금액, 원단위 이하 소수점x, -값 허용 (원화)
        if QUA = '128' then
        begin
           SC1.ADDValue('TAMT',SEGDATA.Items[1]);
        end;
        // 공급가액 총액 (원화)
        if QUA = '79' then
        begin
           SC1.ADDValue('SUPTAMT',SEGDATA.Items[1]);
        end;
        // 세액 총합계 (원화) 만약 BGM의 세금계산서종류가 0101~0105,0201~0205이면 필수, 0301~0304,0401~0404 이면 사용안함.
        if QUA = '176' then
        begin
           SC1.ADDValue('TAXTAMT',SEGDATA.Items[1]);
        end;
        // 총외화공급가액
        if QUA = '14' then
        begin
           SC1.ADDValue('USTAMT',SEGDATA.Items[1]);
           SC1.ADDValue('USTAMTC',SEGDATA.Items[2]);
        end;
      end;
    end;

  //====================================
    //CNT 공급물품의 총수량
    IF FindSegment('CNT','18',3) then
    begin
      QUA := SEGDATA.Items[0];
      if QUA = '1AA' then
      begin
           //총수량
           SC1.ADDValue('TQTY',SEGDATA.Items[1]);
           //수량단위
           SC1.ADDValue('TQTYC',SEGDATA.Items[2]);
      end;
    end;

  //=====================================
    //PAI 그룹 PAI-MOA 4행
    for SEQ := 1 to 4 do
    begin
      //PAI 결제방법을 CODE로 기재.
      if FindSegment('PAI','19',1) then
      begin
         PAI := SEGDATA.Items[0];
      end;

      for i:=1 to 2 do
      begin
        if FindSegment('MOA','2B',3) then
        begin
          QUA := SEGDATA.Items[0];
          //결제방법에 따른 해당금액(원화)
          if QUA = '2AE' then
          begin
            //현금
            if PAI = '10' then
            begin
              SC1.ADDValue('AMT12',SEGDATA.Items[1]);
            end;
            //수표
            if PAI = '20' then
            begin
              SC1.ADDValue('AMT22',SEGDATA.Items[1]);
            end;
            //어음
            if PAI = '2AA' then
            begin
              SC1.ADDValue('AMT32',SEGDATA.Items[1]);
            end;
            //외상(매출금/미수금)
            if PAI = '30' then
            begin
              SC1.ADDValue('AMT42',SEGDATA.Items[1]);
            end;
          end;
          //결제방법에 따른 해당금액(외화
          if QUA = '2AD' then
          begin
            //현금
            if PAI = '10' then
            begin
              SC1.ADDValue('AMT11',SEGDATA.Items[1]);
              SC1.ADDValue('AMT11C',SEGDATA.Items[2]);
            end;
            //수표
            if PAI = '20' then
            begin
              SC1.ADDValue('AMT21',SEGDATA.Items[1]);
              SC1.ADDValue('AMT21C',SEGDATA.Items[2]);
            end;
            //어음
            if PAI = '2AA' then
            begin
              SC1.ADDValue('AMT31',SEGDATA.Items[1]);
              SC1.ADDValue('AMT31C',SEGDATA.Items[2]);
            end;
            //외상(매출금/미수금)
            if PAI = '30' then
            begin
              SC1.ADDValue('AMT41',SEGDATA.Items[1]);
              SC1.ADDValue('AMT41C',SEGDATA.Items[2]);
            end;
          end;

        end;
      end;
    end;
  //========= PAI그룹 끝 =================

  //======================================
    //GIS 영수, 청구의 구분을 기재
    // 세금계산서 종류가 0104,0204,0304,0404일때를 제외하고 필수항목으로 기재 할 것을 권고
    if FindSegment('GIS','1A',1) then
    begin
      // 1이면 영수, 18이면 청구
      SC1.ADDValue('INDICATOR',SEGDATA.Items[0]);
    end;

    RunSQL(SC1.CreateSQL);

    except
    on E:Exception do
    begin
       ShowMessage(E.Message);
       Result := False;
    end;
    end;
  finally
    SC1.Free;
    SC2.Free;
  end;
end;

{ TLOGUAR_NEW }

procedure TLOGUAR_NEW.DefineList;
var
  MAINT_NO, MESSAGE1, MESSAGE2 : String;
  SC : TSQLCreate;

begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM', '10', 4) THEN
      BEGIN
        MAINT_NO := SEGDATA.Items[1];
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        if Trim(MESSAGE2) = '' then MESSAGE2 := 'NA';
      end;

    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'LOGUAR');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', 0);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', '');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'LOGUAR');
      RunSQL(SC.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;

function TLOGUAR_NEW.ParseData: Boolean;
var
  i,j,SEQ : Integer;
  MAINT_NO, PAI, QUA,  MESSAGE1, MESSAGE2, MESSAGE3, IMD1, IMD2 ,FTX :String;
  SC1 : TSQLCreate;
begin

  Result := True;

  SC1 := TSQLCreate.Create;
//===============================
  // BGM 전자문서시작
  try
    try
    if FindSegment('BGM' ,'10', 4) then
    begin
      //관리번호
      MAINT_NO := SEGDATA.Items[1];
      //문서코드 2BA,2BC,2BW,2CN,2CO,2CP
      MESSAGE1 := SEGDATA.Items[0];
      //문서기능
      MESSAGE2 := SEGDATA.Items[2];
      //문서유형
      MESSAGE3 := SEGDATA.Items[3];

      //문서기능이 빈값이면 '9'로 간주
      IF Trim(MESSAGE2) = '' then MESSAGE2 := '9';
      //문서유형이 빈값이면 'NA'로 간주
      IF Trim(MESSAGE3) = '' then MESSAGE3 := 'NA';

    end;

  //===============================
    //중복되는관리번호 있으면 삭제.

    // LOGUAR 테이블
    SC1.DMLType := dmlDelete;
    SC1.SQLHeader('LOGUAR');
    SC1.ADDWhere('MAINT_NO',MAINT_NO);

    RunSQL(SC1.CreateSQL);
  //===============================
    //BGM 전자문서 시작, 관리번호 및 응답유형
    SC1.DMLType := dmlInsert;
    SC1.SQLHeader('LOGUAR');
    //관리번호
    SC1.ADDValue('MAINT_NO',MAINT_NO);
    //등록일자
    SC1.ADDValue('DATEE',FormatDateTime('YYYYMMDD', Now));
    //문서코드
    SC1.ADDValue('MESSAGE1', MESSAGE1);
    //응답기능
    SC1.ADDValue('MESSAGE2', MESSAGE2);
    //문서유형
    SC1.ADDValue('MESSAGE3', MESSAGE3);
    //사용자
    SC1.ADDValue('USER_ID',LoginData.sID);


  //================================
    //NAD 선박회사명/운송사명
    for i:=1 to 6 do
    begin
      if FindSegment('NAD','11',4) then
      begin
        QUA := SEGDATA.Items[0];
        //선박회사,운송사명
        if QUA = 'CA' then
        begin
           SC1.ADDValue('CR_NAME1',SEGDATA.Items[1]);
           SC1.ADDValue('CR_NAME2',SEGDATA.Items[2]);
           SC1.ADDValue('CR_NAME3',SEGDATA.Items[3]);
        end;
        //송하인
        if QUA = 'SE' then
        begin
           SC1.ADDValue('SE_NAME1',SEGDATA.Items[1]);
           SC1.ADDValue('SE_NAME2',SEGDATA.Items[2]);
           SC1.ADDValue('SE_NAME3',SEGDATA.Items[3]);
        end;
        //신청인 및 주소
        if QUA = 'MS' then
        begin
           SC1.ADDValue('MS_NAME1',SEGDATA.Items[1]);
           SC1.ADDValue('MS_NAME2',SEGDATA.Items[2]);
           SC1.ADDValue('MS_NAME3',SEGDATA.Items[3]);
        end;
        //발급은행 전자서명
        if QUA = 'AX' then
        begin
           //명의인
           SC1.ADDValue('AX_NAME1',SEGDATA.Items[1]);
           //명의인
           SC1.ADDValue('AX_NAME2',SEGDATA.Items[2]);
           //전자서명값
           SC1.ADDValue('AX_NAME3',SEGDATA.Items[3]);
        end;
        //수하인
        if QUA = 'CN' then
        begin
           SC1.ADDValue('CN_NAME1',SEGDATA.Items[1]);
           SC1.ADDValue('CN_NAME2',SEGDATA.Items[2]);
           SC1.ADDValue('CN_NAME3',SEGDATA.Items[3]);
        end; 
        //인수예정자
        if QUA = 'B5' then
        begin
           SC1.ADDValue('B5_NAME1',SEGDATA.Items[1]);
           SC1.ADDValue('B5_NAME2',SEGDATA.Items[2]);
           SC1.ADDValue('B5_NAME3',SEGDATA.Items[3]);
        end;
      end;
    end;
  //=================================
    //MOA 상업송장금액
    if FindSegment('MOA','12',3) then
    begin
      QUA := SEGDATA.Items[0];
      if QUA = '77' then
      begin
        //상업송장금액
        SC1.ADDValue('INV_AMT',SEGDATA.Items[1]);
        //상업송장 통화코드
        SC1.ADDValue('INV_AMTC',SEGDATA.Items[2]);
      end;
    end;
  //===================================
    //RFF 수입화물선취보증(인도승락)서 번호
    for i:= 1 to 4 do
    begin
      if FindSegment('RFF','13',2) then
      begin
        QUA := SEGDATA.Items[0];
        // 수입화물선취보증(인도승락)서 번호
        if QUA = '2AW' then
        begin
           SC1.ADDValue('LG_NO',SEGDATA.Items[1]);
        end;
        // 신용장 번호
        if QUA = 'AAC' then
        begin
          SC1.ADDValue('LC_G',QUA);
          SC1.ADDValue('LC_NO',SEGDATA.Items[1])
        end
        // 계약서 번호
        else if QUA = 'CT' then
        begin
          SC1.ADDValue('LC_G',QUA);
          SC1.ADDValue('LC_NO',SEGDATA.Items[1]);
        end;

        //선하증권 번호
        if QUA = 'BM' then
        begin
          SC1.ADDValue('BL_G', QUA);
          SC1.ADDValue('BL_NO', SEGDATA.Items[1]);
        end
        //항공화물운송장 번호
        else if QUA = 'AWB' then
        begin
          SC1.ADDValue('BL_G', QUA);
          SC1.ADDValue('BL_NO', SEGDATA.Items[1]);
        end;

        //업체신청번호
        if QUA = 'DM' then
          SC1.ADDValue('USERMAINT_NO',SEGDATA.Items[1]);
      end;
    end;
  //=====================================
    //TDT 운송정보
    if FindSegment('TDT','14',3) then
    begin
      QUA := SEGDATA.Items[0];
      if QUA = '20' then
      begin
        //항차/편명
        SC1.ADDValue('CARRIER1',SEGDATA.Items[1]);
        //선명/기명
        SC1.ADDValue('CARRIER2',SEGDATA.Items[2]);
      end;
    end;

  //=====================================
    //DTM 도착(예정)일
    for i:=1 to 4 do
    begin
      if FindSegment('DTM','15',3) then
      begin
         QUA := SEGDATA.Items[0];
         //도착(예정)일
         if QUA = '132' then
            SC1.ADDValue('AR_DATE', EdiDateFormat(SEGDATA.Items[1],SEGDATA.Items[2]));
         //선하증권(운송장) 발급일자
         if QUA = '95' then
            SC1.ADDValue('BL_DATE', EdiDateFormat(SEGDATA.Items[1],SEGDATA.Items[2]));
         //수입화물선취보증(인도승락)서 발급일자
         if QUA = '55' then
            SC1.ADDValue('LG_DATE', EdiDateFormat(SEGDATA.Items[1],SEGDATA.Items[2]));
         //수입신용장 발급일자
         if QUA = '182' then
            SC1.ADDValue('LC_DATE', EdiDateFormat(SEGDATA.Items[1],SEGDATA.Items[2]));

      end;
    end;

  //======================================
    //LOC
    for i:=1 to 2 do
    begin
      if FindSegment('LOC','16',5) then
      begin
        QUA := SEGDATA.Items[0];
        //선적항, 출발항
        if QUA = '9' then
        begin
          //선적,출발항의 국가코드
          SC1.ADDValue('LOAD_LOC',SEGDATA.Items[1]);
          //선적,출발항의 지명을 text로 기재
          SC1.ADDValue('LOAD_TXT',SEGDATA.Items[4]);
        end;
        //도착항
        if QUA = '8' then
        begin
          //도착항의 국가코드
          SC1.ADDValue('ARR_LOC',SEGDATA.Items[1]);
          //도착항의 지명을 text로 기재
          SC1.ADDValue('ARR_TXT',SEGDATA.Items[4]);
        end;
      end;
    end;

  //=====================================
    //PCI 화물표시 및 번호
    if FindSegment('PCI','17',11) then
    begin
      QUA := SEGDATA.Items[0];
      if QUA = '28' then
      begin
        SC1.ADDValue('SPMARK1',SEGDATA.Items[1]);
        SC1.ADDValue('SPMARK2',SEGDATA.Items[2]);
        SC1.ADDValue('SPMARK3',SEGDATA.Items[3]);
        SC1.ADDValue('SPMARK4',SEGDATA.Items[4]);
        SC1.ADDValue('SPMARK5',SEGDATA.Items[5]);
        SC1.ADDValue('SPMARK6',SEGDATA.Items[6]);
        SC1.ADDValue('SPMARK7',SEGDATA.Items[7]);
        SC1.ADDValue('SPMARK8',SEGDATA.Items[8]);
        SC1.ADDValue('SPMARK9',SEGDATA.Items[9]);
        SC1.ADDValue('SPMARK10',SEGDATA.Items[10]);
      end;
    end;

  //======================================
    //PAC 포장수
    if FindSegment('PAC','18',2) then
    begin
      //포장 및 화물의 갯수
      SC1.ADDValue('PAC_QTY',SEGDATA.Items[0]);
      //포장형태 또는 화물단위 코드
      SC1.ADDValue('PAC_QTYC',SEGDATA.Items[1]);
    end;

  //======================================
    //FTX 상품명세 2번반복
    if FindSegment('FTX','19',6) then
    begin
      QUA := SEGDATA.Items[0];
      if QUA = 'AAA' then
      begin
        SC1.ADDValue('GOODS1',SEGDATA.Items[1]);
        SC1.ADDValue('GOODS2',SEGDATA.Items[2]);
        SC1.ADDValue('GOODS3',SEGDATA.Items[3]);
        SC1.ADDValue('GOODS4',SEGDATA.Items[4]);
        SC1.ADDValue('GOODS5',SEGDATA.Items[5]);

        if FindSegment('FTX','19',6) then
        begin
          QUA := SEGDATA.Items[0];
          if QUA = 'AAA' then
          begin
            SC1.ADDValue('GOODS6',SEGDATA.Items[1]);
            SC1.ADDValue('GOODS7',SEGDATA.Items[2]);
            SC1.ADDValue('GOODS8',SEGDATA.Items[3]);
            SC1.ADDValue('GOODS9',SEGDATA.Items[4]);
            SC1.ADDValue('GOODS10',SEGDATA.Items[5]);
          end;
        end;
      end;
    end;

  //======================================
    //PAT 대금결제조건(결제기간)
    if FindSegment('PAT','1A',3) then
    begin
      QUA := SEGDATA.Items[0];
      if QUA = '1AA' then
      begin
         //결제기간 코드 2AA 2AB 2AC 2AD 2AE 2AF
         SC1.ADDValue('TRM_PAYC',SEGDATA.Items[1]);
         //결제기간 입력
         SC1.ADDValue('TRM_PAY', SEGDATA.Items[2]);
      end;
    end;

  //=========================================
    //FII 발급은행
    if FindSegment('FII','1B',6) then
    begin
      QUA := SEGDATA.Items[0];
      if QUA = 'BI' then
      begin
        //발급은행코드
        SC1.ADDValue('BANK_CD',SEGDATA.Items[1]);
        //발급은행명
        SC1.ADDValue('BANK_TXT',SEGDATA.Items[4]);
        //발급은행사항(국가/도시/지점명/지점전화번호)
        SC1.ADDValue('BANK_BR',SEGDATA.Items[5]);
      end;
    end;

    Clipboard.AsText := SC1.FieldList;
    RunSQL(SC1.CreateSQL);

    except
    on E:Exception do
    begin
       Result := False;
       ShowMessage(E.Message);
    end;
    end;
  finally
    SC1.Free;
  end;
end;
{ TDEBADV_NEW }

procedure TDEBADV_NEW.DefineList;
var
  MAINT_NO, MESSAGE1, MESSAGE2 : String;
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM', '10', 4) THEN
      BEGIN
        MAINT_NO := SEGDATA.Items[1];
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        IF Trim(MESSAGE2) = '' then MESSAGE1 := 'NA';
      end;

    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'DEBADV');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', 0);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', '');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'DEBADV');
      RunSQL(SC.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;

function TDEBADV_NEW.ParseData: Boolean;
var
  i, j, l, INNER_IDX : Integer;
  MAINT_NO, QUA,  MESSAGE1, MESSAGE2 : String;
  TMP_TXT : String;
  SC1, SC2, SCFC1, SCFC2 : TSQLCreate;
  FC_QUA : String;
begin

  Result := True;

  SC1 := TSQLCreate.Create;
  SC2 := TSQLCreate.Create;
  SCFC1 := TSQLCreate.Create;
  SCFC2 := TSQLCreate.Create;

  try
    try
      if FindSegment('BGM' ,'10', 4) then
      begin
        //관리번호
        MAINT_NO := SEGDATA.Items[1];
        //문서기능
        MESSAGE1 := SEGDATA.Items[2];
        //문서유형
        MESSAGE2 := SEGDATA.Items[3];

        //문서기능이 빈값이면 '9'로 간주
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        //문서유형이 빈값이면 'NA'로 간주
        IF Trim(MESSAGE2) = '' then MESSAGE2 := 'NA';
      end;

    //===============================
      //중복되는관리번호 있으면 삭제.

      // DEBADV 테이블
      SC1.DMLType := dmlDelete;
      SC1.SQLHeader('DEBADV_HEADER');
      SC1.ADDWhere('MAINT_NO',MAINT_NO);

      RunSQL(SC1.CreateSQL);

    //===============================
      //BGM 전자문서 시작, 관리번호 및 응답유형
      SC1.DMLType := dmlInsert;
      SC1.SQLHeader('DEBADV_HEADER');
      //관리번호
      SC1.ADDValue('MAINT_NO',MAINT_NO);
      //등록일자
      SC1.ADDValue('DATEE',FormatDateTime('YYYYMMDD', Now));
      //문서기능
      SC1.ADDValue('MESSAGE1', MESSAGE1);
      //문서유형
      SC1.ADDValue('MESSAGE2', MESSAGE2);
      //사용자
      SC1.ADDValue('USER_ID',LoginData.sID);


      // 통지일자, 출금일자, 입금일자, 이행일자
      for i := 1 to 4 do
      begin
        if FindSegment('DTM','11', 3) then
        begin
           QUA := SEGDATA.Items[0];
           Case AnsiIndexText(QUA, ['137', '193', '202', '209']) of
             //통지일자
             0: SC1.ADDValue('ADV_DT', EdiDateFormat(SEGDATA.items[1], SEGDATA.items[2]));
             //계좌출금일
             1: SC1.ADDValue('WIT_DT', EdiDateFormat(SEGDATA.items[1], SEGDATA.items[2]));
             //계좌입금일
             2: SC1.ADDValue('DEP_DT', EdiDateFormat(SEGDATA.items[1], SEGDATA.items[2]));
             //지급이행일
             3: SC1.ADDValue('EXT_DT', EdiDateFormat(SEGDATA.items[1], SEGDATA.items[2]));
           end;
        end;
      end;

      //기타번호
      IF FindSegment('RFF', '12', 3) then
      begin
        SC1.ADDValue('ETC_REFNO', SegItem[1]);
        SC1.ADDValue('ETC_REFCD', SegItem[2]);
      end;

      //출금내역
      IF FindSegment('FTX', '13', 6) then
      begin
        TMP_TXT := '';
        for i := 1 to 5 do
        begin
          IF Trim(SegItem[i]) <> '' Then
            TMP_TXT := TMP_TXT + SegItem[i];
        end;
        SC1.ADDValue('PAY_DTL', TMP_TXT);
      end;
//==============================================================================
      //금액유형별 상세내역
      //중복 값 제거
      SC2.DMLType := dmlDelete;
      SC2.SQLHeader('DEBADV_DTL');
      SC2.ADDWhere('MAINT_NO', MAINT_NO);
      RunSQL(SC2.CreateSQL);

      for i := 1 to 4 do
      begin
        IF FindSegment('MOA', '14', 3) Then
        begin
          SC2.DMLType := dmlInsert;
          SC2.SQLHeader('DEBADV_DTL');

          SC2.ADDValue('MAINT_NO', MAINT_NO);
          SC2.ADDValue('AMT_UNIT', SegItem[2]);
          SC2.ADDValue('AMT', SegItem[1], vtInteger);

          QUA := SegItem[0];
          SC2.ADDValue('CD5025', QUA);
          INNER_IDX := 1;

          //금액유형
          if AnsiMatchText(QUA, ['60','9']) Then
          begin
            //외화계좌 출금금액
            //출금금액
            IF NextSegment('CUX', '20', 5) Then
            begin
              SC2.ADDValue('BASE_UNIT', SegItem[1]);
              SC2.ADDValue('DEST_UNIT', SegItem[3]);
              SC2.ADDValue('RATE', SegItem[4], vtInteger);
            end;
            //환율적용일자
            for j:= 1 to 2 do
            begin
              IF NextSegment('DTM', '21', 3) Then
              begin
                case INNER_IDX of
                  1: SC2.ADDValue('RATE_STDT', EdiDateFormat(SegItem[1], SegItem[2]));
                  2: SC2.ADDValue('RATE_EDDT', EdiDateFormat(SegItem[1], SegItem[2]));
                end;
                Inc(INNER_IDX);
              end;
            end;

            //출금관련 참조번호
            for j:= 1 to 2 do
            begin
              IF NextSegment('RFF', '22', 3) Then
              begin
                IF SegItem[0] = 'ADE' Then
                  SC2.ADDValue('WIT_ACNT', SegItem[1])
                else
                begin
                  SC2.ADDValue('D1_RFF_CD', SegItem[0]);
                  SC2.ADDValue('D1_RFF_NO', SegItem[1])
                end;
              end;
            end;

          end;
          if QUA = '98' Then
          begin
            //지급금액은 상단에서 처리됨

          end;
          if QUA = '36' Then
          begin
            //환전금액
            //출금금액
            IF NextSegment('CUX', '20', 5) Then
            begin
              SC2.ADDValue('BASE_UNIT', SegItem[1]);
              SC2.ADDValue('DEST_UNIT', SegItem[3]);
              SC2.ADDValue('RATE', SegItem[4], vtInteger);
            end;
            //환율적용일자
            for j:= 1 to 2 do
            begin
              IF NextSegment('DTM', '21', 3) Then
              begin
                case INNER_IDX of
                  1: SC2.ADDValue('RATE_STDT', EdiDateFormat(SegItem[1], SegItem[2]));
                  2: SC2.ADDValue('RATE_EDDT', EdiDateFormat(SegItem[1], SegItem[2]));
                end;
                Inc(INNER_IDX);
              end;
            end;
          end;

          RunSQL(SC2.CreateSQL);
        end;
      end;
//==============================================================================
      //금융기관유형
      IF FindSegment('FII', '15', 10) then
      begin
        QUA := SegItem[0];
        //수익자은행
        IF QUA = 'BF' Then
        begin
          SC1.ADDValue('BEN_BKCD', SegItem[5]);
          SC1.ADDValue('BEN_BKNM', SegItem[8]);
          SC1.ADDValue('BEN_BRNM', SegItem[9]);
          SC1.ADDValue('BEN_ACNT', SegItem[1]);
          SC1.ADDValue('BEN_ACNTNM', Trim(SegItem[2]+' '+SegItem[3]));
          SC1.ADDValue('BEN_UNIT', SegItem[4]);
          SC1.ADDValue('BEN_MAN',  SegItem[7]);
        end;
        //지급의뢰인은행
        IF QUA = 'OR' Then
        begin
          SC1.ADDValue('CLT_BKCD', SegItem[5]);
          SC1.ADDValue('CLT_BKNM', SegItem[8]);
          SC1.ADDValue('CLT_BRNM', SegItem[9]);
          SC1.ADDValue('CLT_MAN',  SegItem[7]);
        end;
      end;

      //당사자유형
      for i := 1 to 6 do
      begin
        IF FindSegment('NAD', '16', 7) then
        begin
          QUA := SegItem[0];
          //수익자
          IF QUA = 'BE' Then
          begin
            SC1.ADDValue('BEN_NM1', SegItem[1]);
            SC1.ADDValue('BEN_NM2', SegItem[2]);
            SC1.ADDValue('BEN_NM3', SegItem[3]);
            SC1.ADDValue('BEN_NM4', SegItem[4]);
            SC1.ADDValue('BEN_NM5', SegItem[5]);
            SC1.ADDValue('BEN_NM6', SegItem[6]);
          end;
          //지급의뢰인
          IF QUA = 'OY' Then
          begin
            SC1.ADDValue('CLT_NM1', SegItem[1]);
            SC1.ADDValue('CLT_NM2', SegItem[2]);
            SC1.ADDValue('CLT_NM3', SegItem[3]);
            SC1.ADDValue('CLT_NM4', SegItem[4]);
            SC1.ADDValue('CLT_NM5', SegItem[5]);
            SC1.ADDValue('CLT_NM6', SegItem[6]);
          end;
          //전자서명
          IF QUA = 'AX' Then
          begin
            SC1.ADDValue('SIGN1', SegItem[1]);
            SC1.ADDValue('SIGN2', SegItem[2]);
            SC1.ADDValue('SIGN3', SegItem[3]);
          end;
        end;
      end;

      //지시당사자
      INNER_IDX := 1;
      for i := 1 to 4 do
      begin
        if FindSegment('INP', '17', 2) Then
        begin
          IF NextSegment('FTX', '23', 6) Then
          begin
            TMP_TXT := '';
            for j := 1 to 5 do
            begin
              IF SegItem[j] <> '' Then
                TMP_TXT := TMP_TXT + SegItem[j] + #13#10;
            end;

            Case INNER_IDX of
              1: SC1.ADDValue('ETC_INFO1', TMP_TXT);
              2: SC1.ADDValue('ETC_INFO2', TMP_TXT);
            end;
          end;
        end;
      end;

      //부가수수료부담자(FC1)
      SCFC1.DMLType := dmlDelete;
      SCFC1.SQLHeader('DEBADV_FC1');
      SCFC1.ADDWhere('MAINT_NO', MAINT_NO);
      RunSQL(SCFC1.CreateSQL);
      //수수료내용(FC2)
      SCFC2.DMLType := dmlDelete;
      SCFC2.SQLHeader('DEBADV_FC2');
      SCFC2.ADDWhere('MAINT_NO', MAINT_NO);
      RunSQL(SCFC2.CreateSQL);

      for i := 1 to 5 do
      begin
        IF FindSegment('FCA', '18', 1) Then
        begin
          //수수료부담자
          SCFC1.DMLType := dmlInsert;
          SCFC1.SQLHeader('DEBADV_FC1');
          SCFC1.ADDValue('MAINT_NO', MAINT_NO);
          QUA := SegItem[0];
          SCFC1.ADDValue('FC1CD', QUA);
          for j := 1 to 2 do
          begin
            IF NextSegment('MOA', '24', 3) Then
            begin
              SCFC1.ADDValue('FC1AMT'+IntToStr(j), SegItem[1], vtInteger);
              SCFC1.ADDValue('FC1AMT'+IntToStr(j)+'C', SegItem[2]);
            end;
          end;

          RunSQL(SCFC1.CreateSQL);

          //수수료유형
          for j := 1 to 20 do
          begin
            IF NextSegment('ALC', '25', 2) Then
            begin
              SCFC2.DMLType := dmlInsert;
              SCFC2.SQLHeader('DEBADV_FC2');
              SCFC2.ADDValue('MAINT_NO', MAINT_NO);
              SCFC2.ADDValue('FC1CD', QUA);
              SCFC2.ADDValue('FC1SEQ', IntToStr( j ), vtInteger);
              SCFC2.ADDValue('FC1CH', SegItem[1]);
              //수수료금액
              for l := 1 to 2 do
              begin
                IF NextSegment('MOA', '30', 3) Then
                begin
                  FC_QUA := SegItem[0];
                  IF FC_QUA = '23' Then
                  begin
                    SCFC2.ADDValue('FC1SA1',  SegItem[1], vtInteger);
                    SCFC2.ADDValue('FC1SA1C', SegItem[2]);
                  end
                  else
                  IF FC_QUA = '36' Then
                  begin
                    SCFC2.ADDValue('FC1SA2',  SegItem[1], vtInteger);
                    SCFC2.ADDValue('FC1SA2C', SegItem[2]);
                  end
                end;
              end;
              //기준/목적통화/적용환율
              IF NextSegment('CUX', '31', 5) Then
              begin
                SCFC2.ADDValue('FC1RATE1', SegItem[1]);
                SCFC2.ADDValue('FC1RATE2', SegItem[3]);
                SCFC2.ADDValue('FC1RATE', SegItem[4], vtInteger);
              end;
              //환율적용일자
              for l := 1 to 2 do
              begin
                IF NextSegment('DTM', '32', 3) Then
                begin
                  SCFC2.ADDValue('FC1RD'+IntToStr(l), EdiDateFormat(SegItem[1], SegItem[2]));
                end;
              end;
              RunSQL(SCFC2.CreateSQL);
            end;
          end;
        end;
      end;

      //출금관려서류
      SC2.DMLType := dmlDelete;
      SC2.SQLHeader('DEBADV_DOCLIST');
      SC2.ADDWhere('MAINT_NO', MAINT_NO);
      RunSQL(SC2.CreateSQL);

      for j := 1 to 10 do
      begin
        IF FindSegment('DOC', '19', 3) Then
        begin
          SC2.DMLType := dmlInsert;
          SC2.SQLHeader('DEBADV_DOCLIST');
          SC2.ADDValue('MAINT_NO', MAINT_NO);
          SC2.ADDValue('SEQ', j);
          SC2.ADDValue('RELIST_TYPE', SegItem[0]);
          SC2.ADDValue('RELIST_NO', SegItem[1]);
          SC2.ADDValue('RELIST_ADDNO', SegItem[2]);

          IF NextSegment('DTM', '26', 3) Then
          begin
            SC2.ADDValue('RELIST_APPDT', EdiDateFormat(SegItem[1], SegItem[2]));
          end;

          RunSQL(SC2.CreateSQL);
        end;
      end;
//==============================================================================
      RunSQL(SC1.CreateSQL);
//==============================================================================
    except
      on E:Exception do
      begin
         ShowMessage(E.Message);
         Result := False;
      end;
    end;
  finally
    SC1.Free;
    SC2.Free;
    SCFC1.Free;
    SCFC2.Free;
  end;
end;
{ TADV710_NEW }

procedure TADV710_NEW.DefineList;
var
  MAINT_NO, MESSAGE1, MESSAGE2 : String;
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;
  try
    try
      IF FindSegment('BGM', '10', 4) THEN
      BEGIN
        MAINT_NO := SEGDATA.Items[1];
        MESSAGE1 := SEGDATA.Items[2];
        MESSAGE2 := SEGDATA.Items[3];
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        IF Trim(MESSAGE2) = '' then MESSAGE1 := 'NA';
      end;

    //------------------------------------------------------------------------------
    // 수신목록에 입력
    //------------------------------------------------------------------------------
      SC.DMLType := dmlInsert;
      SC.SQLHeader('R_HST');
      SC.ADDValue('SRDATE', FormatDateTime('YYYYMMDD', Now));
      SC.ADDValue('SRTIME', FormatDateTime('HH:NN:SS', Now));
      SC.ADDValue('DOCID', 'ADV710');
      SC.ADDValue('MAINT_NO', MAINT_NO);
      SC.ADDValue('MSEQ', 0);
      SC.ADDValue('RECEIVEUSER', LoginData.sID);
      SC.ADDValue('Mig', FileName+' '+MigData);
      SC.ADDValue('CON', '');
      SC.ADDValue('SRVENDER', Header[0]);
      SC.ADDValue('DOCNAME', 'ADV710');
      RunSQL(SC.CreateSQL);
    except
      on E:Exception do
      begin
        ShowMessage(E.Message);
      end;
    end;
  finally
    SC.Free;
  end;
end;

function TADV710_NEW.ParseData: Boolean;
var
  i, j, l, INNER_IDX, PCD_POINT : Integer;
  MAINT_NO, QUA,  MESSAGE1, MESSAGE2 : String;
  TMP_STR : String;
  TMP_FTX : array [0..5] of string;
  SC1, SC2, SC3, SC4 : TSQLCreate;
  FC_QUA : String;

begin

  Result := True;

  SC1 := TSQLCreate.Create;
  SC2 := TSQLCreate.Create;
  SC3 := TSQLCreate.Create;
  SC4 := TSQLCreate.Create;

  try
    try
      if FindSegment('BGM' ,'10', 4) then
      begin
        //관리번호
        MAINT_NO := SEGDATA.Items[1];
        //문서기능
        MESSAGE1 := SEGDATA.Items[2];
        //문서유형
        MESSAGE2 := SEGDATA.Items[3];

        //문서기능이 빈값이면 '9'로 간주
        IF Trim(MESSAGE1) = '' then MESSAGE1 := '9';
        //문서유형이 빈값이면 'NA'로 간주
        IF Trim(MESSAGE2) = '' then MESSAGE2 := 'NA';
      end;

    //===============================
      //중복되는관리번호 있으면 삭제.

      // DEBADV 테이블
      SC1.DMLType := dmlDelete; SC1.SQLHeader('ADV7104'); SC1.ADDWhere('MAINT_NO', MAINT_NO); RunSQL(SC1.CreateSQL);
      SC1.DMLType := dmlDelete; SC1.SQLHeader('ADV7103'); SC1.ADDWhere('MAINT_NO', MAINT_NO); RunSQL(SC1.CreateSQL);
      SC1.DMLType := dmlDelete; SC1.SQLHeader('ADV7102'); SC1.ADDWhere('MAINT_NO', MAINT_NO); RunSQL(SC1.CreateSQL);
      SC1.DMLType := dmlDelete; SC1.SQLHeader('ADV710' ); SC1.ADDWhere('MAINT_NO', MAINT_NO); RunSQL(SC1.CreateSQL);

    //===============================
      //BGM 전자문서 시작, 관리번호 및 응답유형
      SC1.DMLType := dmlInsert;
      SC2.DMLType := dmlInsert;
      SC3.DMLType := dmlInsert;
      SC4.DMLType := dmlInsert;
      SC1.SQLHeader('ADV710');
      SC2.SQLHeader('ADV7102');
      SC3.SQLHeader('ADV7103');
      SC4.SQLHeader('ADV7104');
      //관리번호
      SC1.ADDValue('MAINT_NO',MAINT_NO);
      SC2.ADDValue('MAINT_NO',MAINT_NO);
      SC3.ADDValue('MAINT_NO',MAINT_NO);
      SC4.ADDValue('MAINT_NO',MAINT_NO);

      //등록일자
      SC1.ADDValue('DATEE',FormatDateTime('YYYYMMDD', Now));
      //문서기능
      SC1.ADDValue('MESSAGE1', MESSAGE1);
      //문서유형
      SC1.ADDValue('MESSAGE2', MESSAGE2);
      //사용자
      SC1.ADDValue('USER_ID',LoginData.sID);


      // RFF 통지번호, 발신은행 참조사항, 신용장번호, 선통지참조사항
      for i := 1 to 4 do
      begin
        if FindSegment('RFF','11', 2) then
        begin
           QUA := SEGDATA.Items[0];
           Case AnsiIndexText(QUA, ['2AA', '2AD', 'AAC', '2AF']) of
             // 통지번호
             0: SC1.ADDValue('APP_NO', SEGDATA.items[1]);
             // 발신은행참조사항
             1: SC1.ADDValue('ADV_NO', SEGDATA.items[1]);
             // 신용장번호
             2: SC1.ADDValue('LIC_NO', SEGDATA.items[1]);
             // 선통지참조사항
             3: SC1.ADDValue('REF_NO', SEGDATA.items[1]);
           end;
        end;
      end;

      // DTM 통지일자, 개설일자
      for i := 1 to 2 do
      begin
        if FindSegment('DTM','12', 3) then
        begin
          QUA := SEGDATA.items[0];
          Case AnsiIndexText(QUA, ['184', '182']) of
             // 통지일자
             0: SC1.ADDValue('APP_DATE', EdiDateFormat(SEGDATA.items[1]));
             // 개설일자
             1: SC1.ADDValue('ISS_DATE', EdiDateFormat(SEGDATA.items[1]));
          end;
        end;
      end;

      // PAT 최대 11번 3+4+4
      for i := 1 to 11 do
      begin
        if FindSegment('PAT','13',3) Then
        begin
          QUA := SEGDATA.items[0];
          Case AnsiIndexText(QUA, ['2AB', '6', '4']) of
            0:
            begin
              // 화환어음조건(Draft at...)
              IF not SC1.ExistsField('DRAFT') Then
                SC1.ADDValue('DRAFT', SEGDATA.items[2])
              else
              IF not SC1.ExistsField('DRAFT1') Then
                SC1.ADDValue('DRAFT1', SEGDATA.items[2])
              else
                SC1.ADDValue('DRAFT2', SEGDATA.items[2]);
            end;
            1:
            begin
              // 혼합지급조건명세(Mixed Payment Details)
              IF not SC1.ExistsField('MIX_PAY') Then
                SC1.ADDValue('MIX_PAY', SEGDATA.items[2])
              else
              IF not SC1.ExistsField('MIX_PAY1') Then
                SC1.ADDValue('MIX_PAY1', SEGDATA.items[2])
              else
              IF not SC1.ExistsField('MIX_PAY2') Then
                SC1.ADDValue('MIX_PAY2', SEGDATA.items[2])
              else
                SC1.ADDValue('MIX_PAY3', SEGDATA.items[2]);
            end;
            2:
            begin
              // 매입/연지급조건명세(Negotiation/Deferred Payment Details)
              IF not SC1.ExistsField('DEF_PAY') Then
                SC1.ADDValue('DEF_PAY', SEGDATA.items[2])
              else
              IF not SC1.ExistsField('DEF_PAY1') Then
                SC1.ADDValue('DEF_PAY1', SEGDATA.items[2])
              else
              IF not SC1.ExistsField('DEF_PAY2') Then
                SC1.ADDValue('DEF_PAY2', SEGDATA.items[2])
              else
                SC1.ADDValue('DEF_PAY3', SEGDATA.items[2]);
            end;
          end;
        end;
      end;

      // FTX
      for i := 1 to 655 do
      begin
        IF FindSegment('FTX','14',6) Then
        begin
          QUA := SegItem[0];
          // 기타정보
          IF QUA = 'ACB' Then
          begin
            TMP_FTX[0] := TMP_FTX[0]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end
          else
          //신용장 적용규칙
          IF QUA = '3FC' Then
          begin
            SC4.ADDValue('APPLICABLE_RULES_1', SegItem[1]);
            SC4.ADDValue('APPLICABLE_RULES_2', SegItem[2]);
          end
          else
          //신용장종류
          IF QUA = '2AA' Then
          begin
            SC1.ADDValue('DOC_CD1', SegItem[1]);
            SC1.ADDValue('DOC_CD2', SegItem[2]);
          end
          else
          // 부가금액부담
          if QUA = 'ABT' Then
          begin
            SC2.ADDValue('AA_CV1', SegItem[1]);
            SC2.ADDValue('AA_CV2', SegItem[2]);
            SC2.ADDValue('AA_CV3', SegItem[3]);
            SC2.ADDValue('AA_CV4', SegItem[4]);
          end
          else
          // 분할선적
          if QUA = '2AG' then
            SC2.ADDValue('PSHIP', SegItem[1])
          else
          // 환적허용여부
          IF QUA = '2AH' Then
            SC2.ADDValue('TSHIP', SegItem[1])
          else
          // 상품용역명세
          if QUA = 'AAA' Then
          begin
            TMP_FTX[1] := TMP_FTX[1]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end
          // 구비서류
          else
          if QUA = 'ABX' Then
          begin
            TMP_FTX[2] := TMP_FTX[2]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end
          // 부가조건
          else
          if QUA = 'ABS' Then
          begin
            TMP_FTX[3] := TMP_FTX[3]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end
          else
          //수익자에 대한 특별지급조건
          if QUA = 'AEF' Then
          begin
            TMP_FTX[4] := TMP_FTX[4]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10+
                                     SEGDATA.items[5]+#13#10;
          end
          else
          // 수수료부담자
          if QUA = 'ALC' Then
          begin
            IF not SC2.ExistsField('CHARGE1') Then
            begin
              SC2.ADDValue('CHARGE1', SegItem[1]);
              SC2.ADDValue('CHARGE2', SegItem[2]);
              SC2.ADDValue('CHARGE3', SegItem[3]);
            end
            else
            begin
              SC2.ADDValue('CHARGE4', SegItem[1]);
              SC2.ADDValue('CHARGE5', SegItem[2]);
              SC2.ADDValue('CHARGE6', SegItem[3]);
            end;
          end
          else
          // 확인지시문언
          if QUA = 'ABP' Then
            SC2.ADDValue('CONFIRMM', SegItem[1])
          else
          // 지급/인수/매입은행에 대한 지시사항
          if QUA = '2AB' Then
          begin
            TMP_FTX[5] := TMP_FTX[5]+SEGDATA.items[1]+#13#10+
                                     SEGDATA.items[2]+#13#10+
                                     SEGDATA.items[3]+#13#10+
                                     SEGDATA.items[4]+#13#10;
          end
          else
          // 수신은행앞 정보
          if QUA = '2AE' Then
          begin
            IF not SC2.ExistsField('SND_INFO1') Then
            begin
              SC2.ADDValue('SND_INFO1', SegItem[1]);
              SC2.ADDValue('SND_INFO2', SegItem[2]);
              SC2.ADDValue('SND_INFO3', SegItem[3]);
            end
            else
            begin
              SC2.ADDValue('SND_INFO4', SegItem[1]);
              SC2.ADDValue('SND_INFO5', SegItem[2]);
              SC2.ADDValue('SND_INFO6', SegItem[3]);
            end;
          end;
        end;
      end;
      SC1.ADDValue('ADDINFO_1', TMP_FTX[0]);
      SC2.ADDValue('DESGOOD_1', TMP_FTX[1]);
      SC2.ADDValue('DOCREQU_1', TMP_FTX[2]);
      SC2.ADDValue('ADDCOND_1', TMP_FTX[3]);
      SC1.ADDValue('SPECIAL_DESC_1', TMP_FTX[4]);
      SC2.ADDValue('INSTRCT_1', TMP_FTX[5]);

      // FII
      for i := 1 to 9 do
      begin
        if FindSegment('FII', '15', 8) Then
        begin
          QUA := SegItem[0];
          // 전문발신은행
          IF QUA = 'AZ' Then
          begin
            SC2.ADDValue('SE_BANK', SegItem[2]);
            SC2.ADDValue('SE_BANK1', Leftstr(SegItem[6],35));
            SC2.ADDValue('SE_BANK2', MidStr(SegItem[6],36,35));
            SC2.ADDValue('SE_BANK3', Leftstr(SegItem[7],35));
            SC2.ADDValue('SE_BANK4', MidStr(SegItem[7],36,35));
          end
          else
          // 전문수신은행
          IF QUA = '2AA' Then
          begin
            SC2.ADDValue('RE_BANK', SegItem[2]);
            SC2.ADDValue('RE_BANK1', Leftstr(SegItem[6],35));
            SC2.ADDValue('RE_BANK2', MidStr(SegItem[6],36,35));
            SC2.ADDValue('RE_BANK3', Leftstr(SegItem[7],35));
            SC2.ADDValue('RE_BANK4', MidStr(SegItem[7],36,35));
            if NextSegment('COM','20') then
            begin
              SC2.ADDValue('RE_BANK5', SegItem[0]);
            end;
          end
          else
          // 개설은행
          IF QUA = 'AW' Then
          begin
            SC2.ADDValue('ISS_BANK', SegItem[2]);
            SC2.ADDValue('ISS_BANK1', Leftstr(SegItem[6],35));
            SC2.ADDValue('ISS_BANK2', MidStr(SegItem[6],36,35));
            SC2.ADDValue('ISS_BANK3', Leftstr(SegItem[7],35));
            SC2.ADDValue('ISS_BANK4', MidStr(SegItem[7],36,35));
            SC2.ADDValue('ISS_BANK5', SegItem[1]);
          end
          else
          // 비은행 개설자
          IF QUA = 'NB' Then
          begin
            SC4.ADDValue('Non_Bank_Issuer1', Leftstr(SegItem[6],35));
            SC4.ADDValue('Non_Bank_Issuer2', MidStr(SegItem[6],36,35));
            SC4.ADDValue('Non_Bank_Issuer3', Leftstr(SegItem[7],35));
            SC4.ADDValue('Non_Bank_Issuer4', MidStr(SegItem[7],36,35));
          end
          else
          // 개설의뢰인은행
          IF QUA = '2AC' Then
          begin
            SC2.ADDValue('APP_BANK', SegItem[2]);
            SC2.ADDValue('APP_BANK1', Leftstr(SegItem[6],35));
            SC2.ADDValue('APP_BANK2', MidStr(SegItem[6],36,35));
            SC2.ADDValue('APP_BANK3', Leftstr(SegItem[7],35));
            SC2.ADDValue('APP_BANK4', MidStr(SegItem[7],36,35));
            SC2.ADDValue('APP_ACCNT', SegItem[1]);
          end
          // 신용장지급방식 및 은행
          else
          if QUA = 'DA' then
          begin
            SC3.ADDValue('AV_AIL', SegItem[2]);
            SC3.ADDValue('AV_PAY', SegItem[5]);
            SC3.ADDValue('AV_AIL1', Leftstr(SegItem[6],35));
            SC3.ADDValue('AV_AIL2', MidStr(SegItem[6],36,35));
            SC3.ADDValue('AV_AIL3', Leftstr(SegItem[7],35));
            SC3.ADDValue('AV_AIL4', MidStr(SegItem[7],36,35));
            SC3.ADDValue('AV_ACCNT', SegItem[1]);
          end
          // 어음지급인
          else
          if QUA = 'DW' Then
          begin
            SC3.ADDValue('DR_AWEE', SegItem[2]);
            SC3.ADDValue('DR_AWEE1', Leftstr(SegItem[6],35));
            SC3.ADDValue('DR_AWEE2', MidStr(SegItem[6],36,35));
            SC3.ADDValue('DR_AWEE3', Leftstr(SegItem[7],35));
            SC3.ADDValue('DR_AWEE4', MidStr(SegItem[7],36,35));
            SC3.ADDValue('DR_ACCNT', SegItem[1]);
          end
          // 확인은행
          else
          if QUA = '4AE' Then
          begin
            SC1.ADDValue('CO_BANK', SegItem[2]);
            SC1.ADDValue('CO_BANK1', Leftstr(SegItem[6],35));
            SC1.ADDValue('CO_BANK2', MidStr(SegItem[6],36,35));
            SC1.ADDValue('CO_BANK3', Leftstr(SegItem[7],35));
            SC1.ADDValue('CO_BANK4', MidStr(SegItem[7],36,35));
          end
          // 상환은행
          else
          if QUA = 'BD' Then
          begin
            SC3.ADDValue('REI_BANK', SegItem[2]);
            SC3.ADDValue('REI_BANK1', Leftstr(SegItem[6],35));
            SC3.ADDValue('REI_BANK2', MidStr(SegItem[6],36,35));
            SC3.ADDValue('REI_BANK3', Leftstr(SegItem[7],35));
            SC3.ADDValue('REI_BANK4', MidStr(SegItem[7],36,35));
            SC3.ADDValue('REI_ACCNT', SegItem[1]);
          end
          // 최종통지은행
          else
          if QUA = '2AB' Then
          begin
            SC3.ADDValue('AVT_BANK', SegItem[2]);
            SC3.ADDValue('AVT_BANK1', Leftstr(SegItem[6],35));
            SC3.ADDValue('AVT_BANK2', MidStr(SegItem[6],36,35));
            SC3.ADDValue('AVT_BANK3', Leftstr(SegItem[7],35));
            SC3.ADDValue('AVT_BANK4', MidStr(SegItem[7],36,35));
            SC3.ADDValue('AVT_ACCNT', SegItem[1]);
          end
          //
        end;
      end;

      // NAD
      for i := 1 to 3 do
      begin
        if FindSegment('NAD', '16', 11) Then
        begin
          QUA := SegItem[0];
          // 개설의뢰인표시
          if QUA = 'DF' Then
          begin
            SC3.ADDValue('APPLIC1', SegItem[1]);
            SC3.ADDValue('APPLIC2', SegItem[2]);
            SC3.ADDValue('APPLIC3', SegItem[3]);
            SC3.ADDValue('APPLIC4', SegItem[4]);
          end
          // 수익자
          else
          if QUA = 'DG' Then
          begin
            SC3.ADDValue('BENEFC1', SegItem[1]);
            SC3.ADDValue('BENEFC2', SegItem[2]);
            SC3.ADDValue('BENEFC3', SegItem[3]);
            SC3.ADDValue('BENEFC4', SegItem[4]);
            SC3.ADDValue('BENEFC5', SegItem[5]);
          end
          else
          // 통지은행 전자서명
          if QUA = '2AG' then
          begin
            SC3.ADDValue('EX_NAME1', SegItem[6]);
            SC3.ADDValue('EX_NAME2', SegItem[7]);
            SC3.ADDValue('EX_NAME3', SegItem[8]);
            SC3.ADDValue('EX_ADDR1', SegItem[9]);
            SC3.ADDValue('EX_ADDR2', SegItem[10]);
          end;
        end;
      end;

      IF FindSegment('DTM', '17', 3) Then
      begin
        QUA := SegItem[0];
        IF QUA = '123' then
          SC3.ADDValue('EX_DATE', EdiDateFormat(SegItem[1]));
      end;

      IF FindSegment('LOC', '22', 2) Then
      begin
        QUA := SegItem[0];
        IF QUA = '143' Then
          SC3.ADDValue('EX_PLACE', SegItem[1]);
      end;

      IF FindSegment('UNS', '18', 1) then
      begin
        //중간분기
      end;

      // 서류제시기간
      IF FindSegment('DTM', '19', 3) Then
      begin
        QUA := SegItem[0];
        IF QUA = '272' Then
          SC1.ADDValue('PERIOD_DAYS', SegItem[1]);
      end;

      // 서류제시기간관련 선적일자 기준 외 상세기준
      IF FindSegment('FTX', '23', 2) Then
      begin
        QUA := SegItem[0];
        IF QUA = 'DOC' Then
          SC1.ADDValue('PERIOD_TXT', SegItem[1]);
      end;

      // 개설금액
      IF FindSegment('MOA', '1A', 3) Then
      begin
        QUA := SegItem[0];
        IF QUA = '212' Then
        begin
          SC3.ADDValue('CD_AMT', SegItem[1], vtInteger);
          SC3.ADDValue('CD_CUR', SegItem[2]);
        end;
      end;

      // 과부족 허용율
      IF FindSegment('PCD', '24', 2) Then
      begin
        TMP_STR := SEGDATA.items[1];
        PCD_POINT := Pos('.', TMP_STR);
        SC3.ADDValue('CD_PERP', LeftStr(TMP_STR, PCD_POINT-1), vtInteger);
        SC3.ADDValue('CD_PERM', MIDStr(TMP_STR, PCD_POINT+1, 2), vtInteger);
      end;

      for i := 1 to 4 do
      begin
        IF FindSegment('LOC', '1B', 2) Then
        begin
          QUA := SegItem[0];
          // 수탁(발송)지
          IF QUA = '149' Then
            SC4.ADDValue('LOAD_ON', SegItem[1])
          else
          // 선적항
          IF QUA = '76' Then
            SC4.ADDValue('SUNJUCK_PORT', SegItem[1])
          else
          // 도착항
          IF QUA = '12' Then
            SC4.ADDValue('DOCHACK_PORT', SegItem[1])
          else
          // 최종목적지
          IF QUA = '148' Then
            SC4.ADDValue('FOR_TRAN', SegItem[1])
        end;
      end;

      // 최종선적일자
      IF FindSegment('DTM', '25', 3) Then
      begin
        SC4.ADDValue('LST_DATE', EdiDateFormat(SegItem[1]));
      end;

      for i := 1 to 2 do
      begin
        IF FindSegment('FTX', '2AF', 4) Then
        begin
          IF not SC4.ExistsField('SHIP_PD1') then
          begin
            SC4.ADDValue('SHIP_PD1', SegItem[1]);
            SC4.ADDValue('SHIP_PD2', SegItem[2]);
            SC4.ADDValue('SHIP_PD3', SegItem[3]);
          end
          else
          begin
            SC4.ADDValue('SHIP_PD4', SegItem[1]);
            SC4.ADDValue('SHIP_PD5', SegItem[2]);
            SC4.ADDValue('SHIP_PD6', SegItem[3]);
          end;
        end;
      end;
//==============================================================================
      RunSQL(SC1.CreateSQL);
      RunSQL(SC2.CreateSQL);
      RunSQL(SC3.CreateSQL);
      RunSQL(SC4.CreateSQL);
//==============================================================================
    except
      on E:Exception do
      begin
         ShowMessage(E.Message);
         Result := False;
      end;
    end;
  finally
    SC1.Free;
    SC2.Free;
    SC3.Free;
    SC4.Free;
  end;
end;

end.
