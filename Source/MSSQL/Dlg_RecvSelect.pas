unit Dlg_RecvSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sLabel, sSkinProvider, ExtCtrls, sPanel, sButton;

type
  TDlg_RecvSelect_frm = class(TDialogParent_frm)
    sLabel1: TsLabel;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    sButton2: TsButton;
    sButton3: TsButton;
    sHTMLLabel1: TsHTMLLabel;
    sLabel2: TsLabel;
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure sLabel2MouseEnter(Sender: TObject);
    procedure sLabel2MouseLeave(Sender: TObject);
    procedure sLabel2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Dlg_RecvSelect_frm: TDlg_RecvSelect_frm;

implementation

uses MSSQL, dlg_SendCustomer;

{$R *.dfm}

procedure TDlg_RecvSelect_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF qryList.RecordCount > 0 Then
    begin
      ModalResult := mrOk;
    end;
  end;
end;

procedure TDlg_RecvSelect_frm.sDBGrid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF KEY = VK_RETURN THEN ModalResult := mrOk;
end;

procedure TDlg_RecvSelect_frm.FormShow(Sender: TObject);
begin
  inherited;
  qryList.Close;
  qrylist.Open;

  sButton2.Enabled := qryList.RecordCount > 0;
  sHTMLLabel1.Visible := not sButton2.Enabled;
  sLabel2.Visible := not sButton2.Enabled;
end;

procedure TDlg_RecvSelect_frm.sLabel2MouseEnter(Sender: TObject);
begin
  inherited;
  sLabel2.Font.Color := clRed;
end;

procedure TDlg_RecvSelect_frm.sLabel2MouseLeave(Sender: TObject);
begin
  inherited;
  sLabel2.Font.Color := clBlack;
end;

procedure TDlg_RecvSelect_frm.sLabel2Click(Sender: TObject);
begin
  inherited;
  if not Assigned(dlg_SendCustomer_frm) then
    dlg_SendCustomer_frm := Tdlg_SendCustomer_frm.Create(Application)
  else
  begin
    dlg_SendCustomer_frm.Show;
    dlg_SendCustomer_frm.BringToFront;
  end;

  Self.ModalResult := mrCancel;
end;

end.
