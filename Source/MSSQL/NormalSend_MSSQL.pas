unit NormalSend_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, NormalRecv, StdCtrls, sRadioButton, sSkinProvider, DBCtrls,
  sDBMemo, sDBEdit, Grids, DBGrids, acDBGrid, sButton, sEdit, Mask,
  sMaskEdit, ComCtrls, sPageControl, Buttons, sSpeedButton, sLabel,
  ExtCtrls, sSplitter, sPanel, DB, ADODB, VarDefine, TypeDefine, StrUtils, DateUtils;

type
  TNormalSend_MSSQL_frm = class(TNormalRecv_frm)
    sDBEdit2: TsDBEdit;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sButton2: TsButton;
    sButton3: TsButton;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListEX_CODE: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListSR_CODE: TStringField;
    qryListSR_NAME1: TStringField;
    qryListSR_NAME2: TStringField;
    qryListSR_NAME3: TStringField;
    qryListEX_ID: TStringField;
    qryListSR_ID: TStringField;
    qryListXX_ID: TStringField;
    qryListDOC_CODE: TStringField;
    qryListDOC1: TStringField;
    qryListDOC2: TStringField;
    qryListDOC3: TStringField;
    qryListDOC4: TStringField;
    qryListDOC5: TStringField;
    qryListDESC_CODE: TStringField;
    qryListDESC_1: TMemoField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sPanel10: TsPanel;
    sSpeedButton8: TsSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure Btn_NewClick(Sender: TObject);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryListCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sPageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure sPageControl1Change(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure Btn_TempClick(Sender: TObject);
    procedure sDBEdit2DblClick(Sender: TObject);
    procedure qryListAfterInsert(DataSet: TDataSet);
    procedure sButton3Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sSpeedButton8Click(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
  private
    FSQL : STring;
    FBeforeColor : TColor;
    FCurrentWork : TProgramControlType;
    FBeforePageIndex : integer;
    procedure CreateReady;
    procedure SetCurrWork(dsState : TDataSetState);
    procedure ReadList;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NormalSend_MSSQL_frm: TNormalSend_MSSQL_frm;
const
  EnterColor : TColor = clYellow;
implementation

uses ICON, Commonlib, MSSQL, Dlg_Customer, Dlg_FindBankCode;

{$R *.dfm}

procedure TNormalSend_MSSQL_frm.CreateReady;
var
  nLoop , nGLoop ,
  MaxCount,GroupCount : Integer;

  UserEdit : TsEdit;
begin
  SetCurrWork(qryList.State);
  GroupCount := sTabSheet3.ControlCount;

  //관리번호 ReadOnly
  sDBEdit1.ReadOnly := not (FCurrentWork = ctInsert);

  IF sDBEdit1.ReadOnly Then sDBEdit1.Color := clWhite
  else sDBEdit1.Color := clYellow;

  for nGLoop := 0 to GroupCount -1 do
  begin
    IF sTabSheet3.Controls[nGLoop] is TsPanel then
    begin

      MaxCount := (sTabSheet3.Controls[nGLoop] as TsPanel).ControlCount;
      for nLoop := 0 to MaxCount -1 do
      begin
        IF (sTabSheet3.Controls[nGLoop] as TsPanel).Controls[nLoop] is TsDBEdit Then
        begin
          UserEdit := TsEdit((sTabSheet3.Controls[nGLoop] as TsPanel).Controls[nLoop]);
          UserEdit.ReadOnly := FCurrentWork = ctView;
        end;
      end;
    end;
  end;

  //Lock ListGrid
  IF sPageControl1.ActivePageIndex = 0 Then
    sPageControl1.ActivePageIndex := 1;

  sPageControl1.Pages[0].Enabled := FCurrentWork = ctView;
  sDBGrid1.Enabled := FCurrentWork = ctView;

  IF FCurrentWork = ctInsert Then
    sDBEdit1.SetFocus;

  //Button State
  Btn_New.Enabled    := FCurrentWork = ctView;
  Btn_Modify.Enabled := FCurrentWork = ctView;
  Btn_Del.Enabled    := FCurrentWork = ctView;

  Btn_Temp.Enabled   := FCurrentWork IN [ctInsert,ctModify];
  Btn_Save.Enabled   := FCurrentWork IN [ctInsert,ctModify];
  Btn_Cancel.Enabled := FCurrentWork IN [ctInsert,ctModify];

  Btn_Print.Enabled  := FCurrentWork = ctView;
  Btn_Ready.Enabled  := FCurrentWork = ctView;

  //Find Buttons
  sButton3.Enabled := FCurrentWork IN [ctInsert,ctModify];
  sButton2.Enabled := FCurrentWork IN [ctInsert,ctModify];

  //Radio Buttons
  sRadioButton1.Visible := FCurrentWork IN [ctInsert,ctModify];
  sRadioButton2.Visible := FCurrentWork IN [ctInsert,ctModify];
end;

procedure TNormalSend_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
  FCurrentWork := ctView;
  sMaskEdit6.Text := '20000101';
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD',EndOfTheMonth(Now));
end;

procedure TNormalSend_MSSQL_frm.SetCurrWork(dsState: TDataSetState);
begin
  Case dsState of
    dsInsert : FCurrentWork := ctInsert;
    dsEdit : FCurrentWork := ctModify;
    dsBrowse : FCurrentWork := ctView;
  end;
end;

procedure TNormalSend_MSSQL_frm.Btn_NewClick(Sender: TObject);
begin
  inherited;
  Case (Sender as TsSpeedButton).Tag of
    0: qryList.Append;
    1: qryList.Edit;
  end;
  CreateReady;
end;

procedure TNormalSend_MSSQL_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  if qryListCHK2.IsNull then Exit;
    CASE qryListCHK2.AsInteger of
      0: Text := '임시';
      1: Text := '문서';
    end;
end;

procedure TNormalSend_MSSQL_frm.qryListCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  sDate : String;
  sState : String;
begin
  sDate := qryListCHK3.AsString;

  IF StringReplace(sDate,' ','',[rfReplaceAll]) <> '' Then
  begin
    CASE StrToInt(RightStr(sDate,1)) OF
      1: sState := '송신준비';
      2: sState := '준비삭제';
      3: sState := '변환오류';
      4: sState := '통신오류';
      5: sState := '송신완료';
    end;
    Text := FormatDateTime('YYYY.MM.DD',decodeCharDate(LeftStr(sDate,8)))+' '+sState;
  end;
end;

procedure TNormalSend_MSSQL_frm.sPageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  IF FCurrentWork IN [ctInsert, ctModify] Then
    FBeforePageIndex := sPageControl1.ActivePageIndex
end;

procedure TNormalSend_MSSQL_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  IF FCurrentWork IN [ctInsert, ctModify] Then
  begin
    IF sPageControl1.ActivePageIndex = 0 Then
      sPageControl1.ActivePageIndex := FBeforePageIndex;
  end;
end;

procedure TNormalSend_MSSQL_frm.Btn_CancelClick(Sender: TObject);
begin
  inherited;
  IF not ConfirmMessage('작업을 취소하시겠습니까?'#13#10'지금까지 작성했던 데이터들은 삭제됩니다.') Then Exit;

  IF qryList.State IN [dsInsert,dsEdit] Then
  begin
    qryList.Cancel;
    CreateReady;
  end;
end;

procedure TNormalSend_MSSQL_frm.Btn_TempClick(Sender: TObject);
begin
  inherited;
  qryListCHK2.AsString := '0';
  qryList.Post;
  CreateReady;  
end;

procedure TNormalSend_MSSQL_frm.sDBEdit2DblClick(Sender: TObject);
begin
  inherited;
  IF not (FCurrentWork in [ctinsert,ctModify]) Then Exit;

  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try
    with Dlg_Customer_frm do
    begin
      IF Run = mrOk then
      begin
        if (Sender as TsDBEdit).DataField = 'EX_CODE' Then
        begin
            qryListEX_CODE.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
            qryListEX_NAME1.AsString := DataSource1.DataSet.FieldByName('ENAME').AsString;
            qryListEX_NAME2.AsString := DataSource1.DataSet.FieldByName('ADDR1').AsString;
            qryListEX_NAME3.AsString := DataSource1.DataSet.FieldByName('Jenja').AsString;
        end
        else if (Sender as TsDBEdit).DataField = 'SR_CODE' Then
        begin
            qryListSR_CODE.AsString := DataSource1.DataSet.FieldByName('CODE').AsString;
            qryListSR_NAME1.AsString := DataSource1.DataSet.FieldByName('ENAME').AsString;
            qryListSR_NAME2.AsString := DataSource1.DataSet.FieldByName('ADDR1').AsString;
            qryListSR_NAME3.AsString := DataSource1.DataSet.FieldByName('Jenja').AsString;
        end;
      end;
    end;
  finally
    FreeAndNil(Dlg_Customer_frm);
  end;
end;

procedure TNormalSend_MSSQL_frm.qryListAfterInsert(DataSet: TDataSet);
begin
  inherited;
  qryListDATEE.AsString := FormatDateTime('YYYYMMDD',NOW);
  qryListUSER_ID.AsString := LoginData.sID;
end;

procedure TNormalSend_MSSQL_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  sDBEdit2DblClick(sDBEdit2);
end;

procedure TNormalSend_MSSQL_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE MAINT_NO LIKE '+QuotedStr(sEdit3.Text));
    SQL.Add('ORDER BY DATEE DESC');
  end;
end;

procedure TNormalSend_MSSQL_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  sPanel8.Caption := IntToStr(qryList.RecNo) + ' | ' + IntToStr(qryList.RecordCount);
end;

procedure TNormalSend_MSSQL_frm.sSpeedButton4Click(Sender: TObject);
begin
  inherited;
  qryList.Prior;
end;

procedure TNormalSend_MSSQL_frm.sSpeedButton8Click(Sender: TObject);
begin
  inherited;
  qryList.Next;
end;

procedure TNormalSend_MSSQL_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF qryList.RecordCount > 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
//      SelectData;
    end;
  end;
end;

procedure TNormalSend_MSSQL_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  IF sRadioButton1.Checked Then
  begin
    Dlg_FindBankCode_frm := TDlg_FindBankCode_frm.Create(Self);
    try
      with Dlg_FindBankCode_frm do
      begin
        IF Run = mrOK Then
        begin
          qryListSR_CODE.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
          qryListSR_NAME1.Text := DataSource1.DataSet.FieldByName('BANKNAME').AsString;
          qryListSR_NAME3.Text := DataSource1.DataSet.FieldByName('BANKBRANCH').AsString;
        end;
      end;
    finally
      FreeAndNil(Dlg_FindBankCode_frm);
    end;
  end
  else
  IF sRadioButton2.Checked Then sDBEdit2DblClick(sDBEdit40);
end;

end.
