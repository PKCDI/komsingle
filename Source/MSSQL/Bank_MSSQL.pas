unit Bank_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Bank, sSkinProvider, StdCtrls, sComboBox, Buttons, sBitBtn,
  sEdit, dbcgrids, acDBCtrlGrid, sLabel, ExtCtrls, sSplitter, sSpeedButton,
  sPanel, DB, ADODB, Mask, DBCtrls, sDBEdit, Grids, DBGrids, acDBGrid, TypeDefine,
  sButton, DBTables;

type
  TBank_MSSQL_frm = class(TBank_frm)
    qryList: TADOQuery;
    dsList: TDataSource;
    sButton1: TsButton;
    Query1: TQuery;
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure Btn_NewClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sButton1Click(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
    procedure doRefresh;
  public
    { Public declarations }
  end;

var
  Bank_MSSQL_frm: TBank_MSSQL_frm;

implementation

uses MSSQL, Dlg_Bank, ICON;

{$R *.dfm}

{ TBank_MSSQL_frm }

procedure TBank_MSSQL_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Case sComboBox1.ItemIndex of
      0: SQL.Add('WHERE [CODE] LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
      1: SQL.Add('WHERE [ENAME1] LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

procedure TBank_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TBank_MSSQL_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TBank_MSSQL_frm.Btn_NewClick(Sender: TObject);
var
  TempControl : TProgramControlType;
begin
  inherited;
  Dlg_Bank_frm := TDlg_Bank_frm.Create(Self);

  Case (sender as TsSpeedButton).Tag of
    0: TempControl := ctInsert;
    1: TempControl := ctModify;
    2: TempControl := ctDelete;
  end;

  try
    IF Dlg_Bank_frm.Run(TempControl,qryList.Fields) = mrOk Then
    begin
      Case TempControl of
        ctInsert,ctDelete :
        begin
          qryList.Close;
          qryList.Open;
        end;

        ctModify :
        begin
          doRefresh;
        end;

      end;
    end;
  finally
    FreeAndNil(Dlg_Bank_frm);
  end;
end;

procedure TBank_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TBank_MSSQL_frm.doRefresh;
var
  BMK : TBookmark;
begin
  BMK := qryList.GetBookmark;

  qryList.Close;
  qryList.Open;

  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);

  qryList.FreeBookmark(BMK);
end;

procedure TBank_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TBank_MSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  Bank_MSSQL_frm := nil;
end;

procedure TBank_MSSQL_frm.sButton1Click(Sender: TObject);
var
  i : integer;
  sMSG : String;
begin
  inherited;
  IF MessageBox(Self.Handle, '가져오기를 진행하면 현재 저장된 은행는 삭제됩니다. 진행하시겠습니까?', '가져오기 실행', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then Exit;


  with TADOQuery.Create(nil) do
  begin
    try
      try
        Connection := DMMssql.KISConnect;
        DMMssql.BeginTrans;
        SQL.Text := 'DELETE FROM [BANKCODE]';
        ExecSQL;

        Query1.DatabaseName := 'KOM_SINGLE';
        Query1.SQL.Text := 'SELECT CODE, ENAME1, ENAME2, ENAME3, ENAME4, ENAME5, ENAME6 FROM BANKCODE';
        Query1.Open;

        while not Query1.Eof do
        begin
          SQL.Text := 'INSERT INTO BANKCODE(CODE, ENAME1, ENAME2, ENAME3, ENAME4, ENAME5, ENAME6)'#13#10+
                      'VALUES(';
          for i := 0 to Query1.FieldCount-1 do
          begin
            IF i = Query1.FieldCount-1 Then
              SQL.Text := SQL.Text + QuotedStr(Query1.Fields[i].AsString)
            else
              SQL.Text := SQL.Text + QuotedStr(Query1.Fields[i].AsString)+', ';
          end;

          SQL.Text := SQL.Text + ')';
          ExecSQL;
          Query1.Next;
        end;

        DMMssql.CommitTrans;
        qryList.Close;
        qryList.Open;
        ShowMessage('전환이 완료되었습니다');

      except
        on E:Exception do
        begin
          ShowMessage(E.Message);
          DMMssql.RollbackTrans;
        end;
      end;
    finally
      Query1.Close;
      Close;
      Free;
    end;
  end;
end;
end.
