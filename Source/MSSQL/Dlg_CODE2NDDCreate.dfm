inherited Dlg_CODE2NDDCreate_frm: TDlg_CODE2NDDCreate_frm
  Left = 1039
  Top = 354
  Caption = ''
  ClientHeight = 274
  ClientWidth = 330
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poMainFormCenter
  Visible = False
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 330
    Height = 274
    Align = alClient
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sLabel1: TsLabel
      Left = 16
      Top = 24
      Width = 6
      Height = 12
    end
    object sBevel1: TsBevel
      Left = 5
      Top = 205
      Width = 324
      Height = 10
      Shape = bsBottomLine
    end
    object sPanel2: TsPanel
      Left = 16
      Top = 16
      Width = 137
      Height = 25
      Caption = #51068#48152#53076#46300' - '#48516#47448
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
    end
    object Edt_Gubun: TsEdit
      Left = 64
      Top = 49
      Width = 121
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #44396#48516#53076#46300
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sPanel3: TsPanel
      Left = 16
      Top = 88
      Width = 137
      Height = 25
      Caption = #51068#48152#53076#46300' - '#49345#49464#45936#51060#53552
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
    end
    object edt_CODE: TsEdit
      Left = 64
      Top = 121
      Width = 121
      Height = 23
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #53076#46300
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object edt_Contents: TsEdit
      Left = 64
      Top = 148
      Width = 249
      Height = 23
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #54637#47785
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object chk_Use: TsCheckBox
      Left = 62
      Top = 176
      Width = 86
      Height = 20
      Caption = #51312#54924#49884' '#49324#50857
      Checked = True
      State = cbChecked
      TabOrder = 5
      SkinData.SkinSection = 'CHECKBOX'
      ImgChecked = 0
      ImgUnchecked = 0
    end
    object Btn_ok: TsButton
      Left = 90
      Top = 232
      Width = 75
      Height = 25
      Caption = #54869#51064
      ModalResult = 1
      TabOrder = 6
      OnClick = Btn_okClick
      SkinData.SkinSection = 'BUTTON'
    end
    object Btn_Cancel: TsButton
      Left = 169
      Top = 232
      Width = 75
      Height = 25
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 7
      OnClick = Btn_CancelClick
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object qryCodeIns: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'Prefix'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end
      item
        Name = 'Remark'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'Map'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'NAME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [CODE2NDD]([Prefix],[CODE],[Remark],[Map],[NAME])'
      'VALUES ( :Prefix, :CODE, :Remark, :Map, :NAME)')
    Left = 240
    Top = 72
  end
end
