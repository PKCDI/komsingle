unit LOCADV_N_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LOCADV_N, DB, ADODB, sSkinProvider, Buttons, sSpeedButton,
  StdCtrls, DBCtrls, sDBMemo, sLabel, sDBEdit, ExtCtrls, sSplitter, Grids,
  DBGrids, acDBGrid, sComboBox, sButton, sEdit, Mask, sMaskEdit, sPanel,
  ComCtrls, sPageControl, StrUtils, DateUtils;

type
  TLOCADV_N_MSSQL_frm = class(TLOCADV_N_frm)
    qryListMAINT_NO: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListBGM_REF: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUSINESS: TStringField;
    qryListRFF_NO: TStringField;
    qryListLC_NO: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListOPEN_NO: TBCDField;
    qryListADV_DATE: TStringField;
    qryListISS_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListGOODDES: TStringField;
    qryListGOODDES1: TMemoField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListDOC_ETC: TStringField;
    qryListDOC_ETC1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC2AMTC: TStringField;
    qryListEX_RATE: TBCDField;
    qryListDOC_DTL: TStringField;
    qryListDOC_NO: TStringField;
    qryListDOC_AMT: TBCDField;
    qryListDOC_AMTC: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListIM_NAME: TStringField;
    qryListIM_NAME1: TStringField;
    qryListIM_NAME2: TStringField;
    qryListIM_NAME3: TStringField;
    qryListDEST: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListEXGOOD: TStringField;
    qryListEXGOOD1: TMemoField;
    qryListPRNO: TIntegerField;
    qryListBSN_HSCODE: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    sPanel13: TsPanel;
    sPanel14: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sPanel15: TsPanel;
    sSpeedButton8: TsSpeedButton;
    Image1: TImage;
    sDBEdit39: TsDBEdit;
    sDBEdit77: TsDBEdit;
    qryListBUSINESSNAME: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListLOC_TYPENAME: TStringField;
    procedure sButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sSpeedButton8Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Btn_PrintClick(Sender: TObject);
  private
    FSQL : String;
    procedure ReadList;
    procedure ChangeDisplayFormat;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LOCADV_N_MSSQL_frm: TLOCADV_N_MSSQL_frm;

implementation

uses MSSQL, Commonlib, LOCADV_PRINT;

{$R *.dfm}

procedure TLOCADV_N_MSSQL_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQl.Text := FSQL;
    SQL.Add('WHERE ADV_DATE between' + QuotedStr(sMaskEdit6.Text) +  'AND ' + QuotedStr(sMaskEdit7.Text));
    IF Trim(sEdit3.Text) <> '' Then
    begin
      Case sCombobox1.itemindex of
        0:
        begin
          SQL.Add('AND Maint_No LIKE '+QuotedStr('%'+sEdit3.Text+'%'));
          SQL.Add('ORDER BY ADV_DATE DESC');
        end;

        1:
        begin
          SQL.Add('AND LC_NO LIKE '+QuotedStr('%'+sEdit3.Text+'%'));
          SQL.Add('ORDER BY ADV_DATE DESC');
        end;
      end;
    end
    else
      SQL.Add('ORDER BY ADV_DATE DESC');
    Open;
    sql.SaveToFile('C:\LOCADV.sql');
  end;
end;

procedure TLOCADV_N_MSSQL_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TLOCADV_N_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
  sMaskEdit6.Text := '20000101';
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD',EndOfTheMonth(Now));
  ReadList;
end;

procedure TLOCADV_N_MSSQL_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF qryList.RecordCount > 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
    end;
  end;
end;

procedure TLOCADV_N_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TLOCADV_N_MSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  LOCADV_N_MSSQL_frm := nil;
end;

procedure TLOCADV_N_MSSQL_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  sPanel13.Caption := IntToStr(qryList.RecNo) + ' | ' + IntToStr(qryList.RecordCount);
  ChangeDisplayFormat;
end;

procedure TLOCADV_N_MSSQL_frm.sSpeedButton4Click(Sender: TObject);
begin
  inherited;
  qryList.Prior;
end;

procedure TLOCADV_N_MSSQL_frm.sSpeedButton8Click(Sender: TObject);
begin
  inherited;
  qryList.Next;
end;

procedure TLOCADV_N_MSSQL_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
//  inherited;

end;

procedure TLOCADV_N_MSSQL_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Shift = [ssCtrl] Then
  begin
    Case Key of
      VK_LEFT : sSpeedButton4Click(sSpeedButton4);
      VK_RIGHT : sSpeedButton8Click(sSpeedButton8);
    end;
  end;
end;

procedure TLOCADV_N_MSSQL_frm.ChangeDisplayFormat;
begin
  ChangeFormatFORDanwi(qryListLOC1AMTC.AsString,qryListLOC1AMT);
  ChangeFormatFORDanwi(qryListLOC2AMTC.AsString,qryListLOC2AMT);  
end;

procedure TLOCADV_N_MSSQL_frm.Btn_PrintClick(Sender: TObject);
begin
  inherited;
//  LOCADV_PRINT_frm := TLOCADV_PRINT_frm.Create(Self,qryListMAINT_NO.AsString);
//  FreeAndNil(LOCADV_PRINT_frm);
end;

end.
