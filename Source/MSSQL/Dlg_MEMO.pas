unit Dlg_MEMO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, DBCtrls, sDBMemo, Mask, sDBEdit, Buttons,
  sSpeedButton, ExtCtrls, sPanel, sSkinProvider, sBevel, DB, ADODB, sMemo,
  sEdit, sButton, TypeDefine;

type
  TDlg_MEMO_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sButton2: TsButton;
    sButton3: TsButton;
    edt_CODE: TsEdit;
    edt_SUBJ: TsEdit;
    Memo_content: TsMemo;
    qryIns: TADOQuery;
    qryDel: TADOQuery;
    qryMod: TADOQuery;
    sBevel1: TsBevel;
    procedure sButton3Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FProgramControl : TProgramControlType;
    FFields : TFields;
    procedure SetData;
    procedure SetParam(qry : TADOQuery);
    function  CheckCode:Boolean;
    procedure DeleteData(CODE: String);
  public
    { Public declarations }
    function Run(ProgramControl : TProgramControlType; UserFields : TFields):TModalResult;
  end;

var
  Dlg_MEMO_frm: TDlg_MEMO_frm;

implementation

uses MSSQL, Commonlib ;

{$R *.dfm}

{ TDlg_MEMO_frm }

function TDlg_MEMO_frm.CheckCode: Boolean;
var
  qry : TADOQuery;
begin
  Result := False;

  qry := TADOQuery.Create(Self);

  try
    with qry do
    begin
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT 1 FROM BANKCODE WHERE CODE = :CODE';
      Parameters.ParamByName('CODE').Value := edt_CODE.Text;
      Open;

      Result := qry.RecordCount > 0;
    end;
  finally
    qry.Free;
  end;


end;

procedure TDlg_MEMO_frm.DeleteData(CODE: String);
begin
  with qryDel do
  begin
    Close;
    Parameters.ParamByName('CODE').Value := CODE;
    ExecSQL;
  end;
end;

function TDlg_MEMO_frm.Run(ProgramControl: TProgramControlType;
  UserFields: TFields): TModalResult;
begin

  FProgramControl := ProgramControl;
  FFields := UserFields;

  edt_CODE.Enabled := FProgramControl = ctInsert;

  Result := mrCancel;


  Case FProgramControl of
    ctInsert :
    begin
      Self.Caption := '메모코드 - [신규]';
    end;

    ctModify :
    begin
      Self.Caption := '메모코드 - [수정]';
      SetData;
    end;

    ctDelete :
    begin
      IF ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'[코드] : '+FFields.FieldByName('CODE').AsString+#13#10'삭제한 데이터를 복구가 불가능 합니다') Then
      begin
        DeleteData(FFields.FieldByName('CODE').AsString);
        Result := mrOk;
        Exit;
      end
      else
      begin
        Result := mrCancel;
        Exit;
      end;
    end;

  end;

  if Self.ShowModal = mrOk Then
  begin
    Case FProgramControl of
      ctInsert : SetParam(qryIns);
      ctModify : SetParam(qryMod);
    end;
    Result := mrOk;
  end;

end;

procedure TDlg_MEMO_frm.SetData;
begin
  edt_CODE.Text     := FFields.FieldByName('CODE').AsString;
  edt_SUBJ.Text    := FFields.FieldByName('SUBJECT').AsString;
  Memo_content.Text  := FFields.FieldByName('D_MEMO').AsString;
end;

procedure TDlg_MEMO_frm.SetParam(qry: TADOQuery);
begin
  with qry do
  begin
    Close;
    Parameters.ParamByName('PREFIX').Value := '';
    Parameters.ParamByName('CODE'  ).Value := edt_CODE.Text;
    Parameters.ParamByName('SUBJECT').Value := edt_SUBJ.Text;
    Parameters.ParamByName('D_MEMO').Value := Memo_content.Text;
    ExecSQL;
  end;

end;

procedure TDlg_MEMO_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_MEMO_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  IF (FProgramControl = ctInsert) AND (Trim(edt_CODE.Text) = '') Then
  begin
    edt_CODE.SetFocus;
    Raise Exception.Create('메모코드를 입력하세요');
  end;

  IF (FProgramControl = ctInsert) AND (Trim(edt_SUBJ.Text) = '') Then
  begin
    edt_CODE.SetFocus;
    Raise Exception.Create('메모명칭를 입력하세요');
  end;

  IF CheckCode AND (FProgramControl = ctInsert) Then
  begin
    raise Exception.Create('해당 메모코드는 이미 등록되어 있습니다.');
  end;

  ModalResult := mrOk;

end;

procedure TDlg_MEMO_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  //
end;

end.
