unit Dlg_APPPCR_CreateDoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sLabel, ExtCtrls, sSplitter, Buttons,
  sSpeedButton, sPanel, sSkinProvider, sEdit, ComCtrls, sPageControl,
  Grids, DBGrids, acDBGrid, ADODB, DB, StrUtils, TypeDefine;

type
  TDlg_APPPCR_CreateDoc_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton3: TsSpeedButton;
    sSplitter2: TsSplitter;
    Btn_Modify: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sPanel3: TsPanel;
    sEdit1: TsEdit;
    Edt_RegistDate: TsEdit;
    Edt_User: TsEdit;
    Edt_Function: TsEdit;
    Edt_Response: TsEdit;
    sPanel2: TsPanel;
    Edt_ApplicationCode: TsEdit;
    Edt_ApplicationName1: TsEdit;
    Edt_ApplicationName2: TsEdit;
    Edt_ApplicationAddr1: TsEdit;
    Edt_ApplicationAddr2: TsEdit;
    Edt_ApplicationAddr3: TsEdit;
    Edt_ApplicationAXNAME1: TsEdit;
    Edt_ApplicationSAUPNO: TsEdit;
    Edt_ApplicationElectronicSign: TsEdit;
    Edt_ApplicationAPPDATE: TsEdit;
    Edt_DocumentsSection: TsEdit;
    sEdit17: TsEdit;
    Edt_BuyConfirmDocuments: TsEdit;
    Edt_SupplyCode: TsEdit;
    Edt_SupplyName: TsEdit;
    Edt_SupplyCEO: TsEdit;
    Edt_SupplyEID: TsEdit;
    sPanel4: TsPanel;
    Edt_SupplyEID2: TsEdit;
    Edt_SupplyAddr1: TsEdit;
    Edt_SupplyAddr2: TsEdit;
    Edt_SupplySaupNo: TsEdit;
    sLabel1: TsLabel;
    sPanel6: TsPanel;
    sEdit33: TsEdit;
    sEdit34: TsEdit;
    sEdit35: TsEdit;
    sEdit37: TsEdit;
    sEdit39: TsEdit;
    sPanel5: TsPanel;
    sLabel3: TsLabel;
    sEdit27: TsEdit;
    sEdit28: TsEdit;
    sEdit29: TsEdit;
    sEdit30: TsEdit;
    sEdit31: TsEdit;
    sEdit32: TsEdit;
    sEdit36: TsEdit;
    sEdit38: TsEdit;
    sEdit40: TsEdit;
    sEdit41: TsEdit;
    sEdit42: TsEdit;
    sEdit43: TsEdit;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    Pan_Detail: TsPanel;
    Btn_DetailAdd: TsSpeedButton;
    LIne1: TsSpeedButton;
    Line4: TsSpeedButton;
    Btn_DetailDel: TsSpeedButton;
    Line3: TsSpeedButton;
    Btn_DetailMod: TsSpeedButton;
    LIne2: TsSpeedButton;
    sSpeedButton10: TsSpeedButton;
    sSpeedButton11: TsSpeedButton;
    StringGrid1: TStringGrid;
    StringGrid2: TStringGrid;
    StringGrid3: TStringGrid;
    dsDetail: TDataSource;
    dsDocument: TDataSource;
    dsTax: TDataSource;
    procedure Edt_DocumentsSectionDblClick(Sender: TObject);
    procedure Edt_ApplicationCodeDblClick(Sender: TObject);
    procedure sEdit39DblClick(Sender: TObject);
    procedure Edt_DocumentsSectionExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
    FProgramControl : TProgramControlType;
    FCommonFields : TFields;
    FApplyNo : String;

    //DetailData
    FDetailList : TStringList;
    //DocumentsData
    FDocumentsList : TStringList;
    //TaxData
    FTaxList : TStringList;

    procedure InitStringGrid;

    procedure SetData;
    procedure AfterSetData;
    procedure SetDataForInsert;
  public
    { Public declarations }
    function Run(UserControl : TProgramControlType; UserFields : TFields;DetailDataSet : array of TDataSource):TModalResult;
    property ApplyNo : String read FApplyNo;
  end;

var
  Dlg_APPPCR_CreateDoc_frm: TDlg_APPPCR_CreateDoc_frm;

const
  GridTitle_Detail : array [0..7] of String =  ('순번','품명','HS코드','수량','단위','단가','금액','단위');
  GridWidth_Detail : array [0..7] of Integer = (33    ,180   ,80      ,120    ,28    ,60    ,120   ,28);

  GridTitle_Documents : array [0..6] of String  = ('순번','문서코드','문서번호','품명','총금액','단위','선적기일');
  GridWidth_Documents : array [0..6] of Integer = (33    ,60     ,120       ,180    ,100    ,28   ,80);
implementation

uses ICON, Commonlib, CodeContents, VarDefine, Dlg_FindDefineCode,
  Dlg_Customer, Dlg_FindBankCode;

{$R *.dfm}

{ TDlg_APPPCR_CreateDoc_frm }

procedure TDlg_APPPCR_CreateDoc_frm.AfterSetData;
begin
  with DMCodeContents do
  begin
    //PAGE 1 신청/변경구분
    APPPCR_GUBUN.First;
    IF APPPCR_GUBUN.Locate('CODE',Edt_DocumentsSection.Text,[]) Then
      sEdit17.Text := APPPCR_GUBUN.FieldByName('NAME').AsString
    else
      sEdit17.Text := '';

    //PAGE 1 총할인/할증/변동
    BYEONDONG_GUBUN.First;
    IF BYEONDONG_GUBUN.Locate('CODE',sEdit27.Text,[]) Then
      sEdit28.Text := BYEONDONG_GUBUN.FieldByName('NAME').AsString
    else
      sEdit28.Text := '';

    //PAGE 1 공급물품명세구분
    NAEGUKSIN4025.First;
    IF NAEGUKSIN4025.Locate('CODE',sEdit33.Text,[]) Then
      sEdit34.Text := NAEGUKSIN4025.FieldByName('NAME').AsString
    else
      sEdit34.Text := '';

    //구매확인서번호
    Edt_BuyConfirmDocuments.Enabled := Edt_DocumentsSection.Text = '2CH';
  end;
end;

function TDlg_APPPCR_CreateDoc_frm.Run(UserControl : TProgramControlType;
                                       UserFields : TFields;
                                       DetailDataSet : array of TDataSource):TModalResult;
begin
  FProgramControl := UserControl;

  FCommonFields := UserFields;
  dsDetail      := DetailDataSet[0];
  dsDocument    := DetailDataSet[1];
  dsTax         := DetailDataSet[2];

  sEdit1.ReadOnly := FProgramControl = ctModify;

  IF sEdit1.ReadOnly Then sEdit1.Color := clBtnFace
  else sEdit1.Color := clWhite;

  Edt_User.Text := LoginData.sID;

  Case FProgramControl of
    ctInsert :;
    ctModify :
    begin
      SetData;
    end;
  end;

  Self.ShowModal;

end;

procedure TDlg_APPPCR_CreateDoc_frm.SetData;
var
  nLoop ,
  MaxCount : Integer;
begin
  //수정시 필드채우기
  MaxCount := Self.ComponentCount;
  for nLoop := 0 to MaxCount -1 do
  begin
    IF (Self.Components[nLoop] is TsEdit) Then
    begin
//      IF (Self.Components[nLoop] as TsEdit).ReadOnly Then Continue;

      IF (Self.Components[nLoop] as TsEdit).Hint <> '' Then
      begin
        Case (Self.Components[nLoop] as TsEdit).Tag of
          0: (Self.Components[nLoop] as TsEdit).Text := FCommonFields.FieldByName((Self.Components[nLoop] as TsEdit).Hint).AsString;
          1: (Self.Components[nLoop] as TsEdit).Text := FormatFloat('#,0',FCommonFields.FieldByName((Self.Components[nLoop] as TsEdit).Hint).AsCurrency);
          2: (Self.Components[nLoop] as TsEdit).Text := FormatFloat('#,0.000',FCommonFields.FieldByName((Self.Components[nLoop] as TsEdit).Hint).AsCurrency);
          102 : (Self.Components[nLoop] as TsEdit).Text :=
                   FormatDateTime('YYYY.MM.DD',decodeCharDate(FCommonFields.FieldByName((Self.Components[nLoop] as TsEdit).Hint).AsString));
        end;
      end;
    end;
  end;
  AfterSetData;

  //Detail
  dsDetail.DataSet.First;

  IF dsDetail.DataSet.RecordCount = 0 Then
    StringGrid2.RowCount := 2
  else
    StringGrid1.RowCount := dsDetail.DataSet.RecordCount +1;
    
  while not dsDetail.DataSet.Eof do
  begin
//    ('순번','품명','HS코드','수량','단위','단가','금액','단위');
    StringGrid1.Cells[0,dsDetail.DataSet.RecNo] := dsDetail.DataSet.FieldByName('SEQ').AsString;
    StringGrid1.Cells[1,dsDetail.DataSet.RecNo] := dsDetail.DataSet.FieldByName('NAME1').AsString;
    StringGrid1.Cells[2,dsDetail.DataSet.RecNo] := HSCodeFormat(dsDetail.DataSet.FieldByName('HS_NO').AsString);
    StringGrid1.Cells[3,dsDetail.DataSet.RecNo] := FormatFloat('#,0',dsDetail.DataSet.FieldByName('QTY').AsCurrency);
    StringGrid1.Cells[4,dsDetail.DataSet.RecNo] := dsDetail.DataSet.FieldByName('QTYC').AsString;
    StringGrid1.Cells[5,dsDetail.DataSet.RecNo] := FormatFloat('#,0.0000',dsDetail.DataSet.FieldByName('PRI2').AsCurrency);
    StringGrid1.Cells[6,dsDetail.DataSet.RecNo] := FormatFloat('#,0.0000',dsDetail.DataSet.FieldByName('AMT1').AsCurrency);
    StringGrid1.Cells[7,dsDetail.DataSet.RecNo] := dsDetail.DataSet.FieldByName('AMT1C').AsString;
    dsDetail.DataSet.Next;
  end;
  dsDetail.DataSet.First;

  //Documents
  with dsDocument.DataSet do
  begin
    First;
    IF RecordCount = 0 Then
      StringGrid2.RowCount := 2
    else
    begin
      StringGrid2.RowCount := RecordCount + 1;
    end;

    while not eof do
    begin
      //('순번','문서코드','문서번호','품명','총금액','단위','선적기일');
      StringGrid2.Cells[0,RecNo] := FieldByName('SEQ').AsString;
      StringGrid2.Cells[1,RecNo] := FieldByName('DOC_G').AsString;
      StringGrid2.Cells[2,RecNo] := FieldByName('DOC_D').AsString;
      StringGrid2.Cells[3,RecNo] := FieldByName('BNAME1').AsString;
      StringGrid2.Cells[4,RecNo] := FormatFloat('#,0.0000',FieldByName('BAMT').AsCurrency);
      StringGrid2.Cells[5,RecNo] := FieldByName('BAMTC').AsString;
      StringGrid2.Cells[6,RecNo] := FormatDateTime('YYYY.MM.DD',decodeCharDate(FieldByName('SHIP_DATE').AsString));
      dsDocument.DataSet.Next;
    end;
  end;
end;

procedure TDlg_APPPCR_CreateDoc_frm.SetDataForInsert;
begin
  //신규입력시 기본입력 채우기

end;

procedure TDlg_APPPCR_CreateDoc_frm.Edt_DocumentsSectionDblClick(
  Sender: TObject);
begin
  inherited;
  Dlg_FindDefineCode_frm := TDlg_FindDefineCode_frm.Create(Self);
  try
    with Dlg_FindDefineCode_frm do
    begin
      //통화
      IF AnsiMatchText((Sender as TsEdit).Hint,['C1_C','TAMT1C']) Then
      begin
          IF Run('TONGHWA') = mrOk Then
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
      else
      //단위
      IF AnsiMatchText((Sender as TsEdit).Hint,['TQTYC']) Then
      begin
          IF Run('DANWI') = mrOk Then
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
      else
      //문서구분
      IF AnsiMatchText((Sender as TsEdit).Hint,['ChgCd']) Then
      begin
          IF Run('APPPCR_GUBUN') = mrOk Then
          begin
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
            sEdit17.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
            Edt_BuyConfirmDocuments.Enabled := Edt_DocumentsSection.Text = '2CH';
          end;
      end;
      //변동구분
      IF AnsiMatchText((Sender as TsEdit).Hint,['CHG_G']) Then
      begin
          IF Run('BYEONDONG_GUBUN') = mrOk Then
          begin
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
            sEdit28.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
            sEdit31.BoundLabel.Caption := '총' + sEdit28.Text + '금액';
            sEdit29.BoundLabel.Caption := '총' + sEdit28.Text + '금액';
          end;
      end
      else
      //내국신4025 - 공급물품명세구분
      IF AnsiMatchText((Sender as TsEdit).Hint,['ITM_GBN']) Then
      begin
          IF Run('NAEGUKSIN4025') = mrOk Then
          begin
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
            sEdit34.Text := DataSource1.DataSet.FieldByName('NAME').AsString;
          end;
      end;
    end;
  finally
    FreeAndNil(Dlg_FindDefineCode_frm);
  end;
end;

procedure TDlg_APPPCR_CreateDoc_frm.Edt_ApplicationCodeDblClick(
  Sender: TObject);
begin
  inherited;
  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try
    with Dlg_Customer_frm do
    begin
      IF Run = mrOK Then
      begin
        IF (Sender as TsEdit).Hint = 'APP_CODE' then
        begin
          Edt_ApplicationCode.Text  := DataSource1.DataSet.FieldByName('CODE').AsString;
          Edt_ApplicationName1.Text := DataSource1.DataSet.FieldByName('ENAME').AsString;
          Edt_ApplicationName2.Text := '';
          Edt_ApplicationAddr1.Text := DataSource1.DataSet.FieldByName('ADDR1').AsString;
          Edt_ApplicationAddr2.Text := DataSource1.DataSet.FieldByName('ADDR2').AsString;
          Edt_ApplicationAddr3.Text := DataSource1.DataSet.FieldByName('ADDR3').AsString;
          Edt_ApplicationAXNAME1.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
          Edt_ApplicationSAUPNO.Text := DataSource1.DataSet.FieldByName('SAUP_NO').AsString;
          Edt_ApplicationElectronicSign.Text := DataSource1.DataSet.FieldByName('TRAD_NO').AsString;
        end
        else
        if (Sender as TsEdit).Hint = 'SUP_CODE' then
        begin
          Edt_SupplyCode.Text   := DataSource1.DataSet.FieldByName('CODE').AsString;
          Edt_SupplyName.Text   := DataSource1.DataSet.FieldByName('ENAME').AsString;
          Edt_SupplyCEO.Text    := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
          Edt_SupplyEID.Text    := DataSource1.DataSet.FieldByName('EMAIL_ID').AsString;
          Edt_SupplyEID2.Text   := DataSource1.DataSet.FieldByName('EMAIL_DOMAIN').AsString;
          Edt_SupplyAddr1.Text  := DataSource1.DataSet.FieldByName('ADDR1').AsString;
          Edt_SupplyAddr2.Text  := DataSource1.DataSet.FieldByName('ADDR2').AsString;
          Edt_SupplySaupNo.Text := DataSource1.DataSet.FieldByName('SAUP_NO').AsString;
        end;
      end;
    end;
  finally
    FreeAndNil(Dlg_Customer_frm);
  end;
end;

procedure TDlg_APPPCR_CreateDoc_frm.sEdit39DblClick(Sender: TObject);
begin
  inherited;
  Dlg_FindBankCode_frm := TDlg_FindBankCode_frm.Create(Self);
  try
    with Dlg_FindBankCode_frm do
    begin
      IF Run = mrOK Then
      begin
        sEdit39.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
        sEdit35.Text := DataSource1.DataSet.FieldByName('BANKNAME').AsString;
        sEdit37.Text := DataSource1.DataSet.FieldByName('BANKBRANCH').AsString;
      end;
    end;
  finally
    FreeAndNil(Dlg_FindBankCode_frm);
  end;

end;

procedure TDlg_APPPCR_CreateDoc_frm.Edt_DocumentsSectionExit(
  Sender: TObject);
begin
  inherited;
  //통화
  With DMCodeContents do
  begin
    IF AnsiMatchText((Sender as TsEdit).Hint,['C1_C','TAMT1C']) Then
    begin
      TONGHWA.First;
      IF TONGHWA.Locate('CODE',(Sender as TsEdit).Text,[]) Then
        (Sender as TsEdit).Text := TONGHWA.FieldByName('CODE').AsString
      else
        Edt_DocumentsSectionDblClick(Sender);
    end
    else
    //단위
    IF AnsiMatchText((Sender as TsEdit).Hint,['TQTYC']) Then
    begin
      DANWI.First;
      IF DANWI.Locate('CODE',(Sender as TsEdit).Text,[]) Then
        (Sender as TsEdit).Text := DANWI.FieldByName('CODE').AsString
      else
        Edt_DocumentsSectionDblClick(Sender);
    end
    else
    //문서구분
    IF AnsiMatchText((Sender as TsEdit).Hint,['ChgCd']) Then
    begin
      APPPCR_GUBUN.First;
      IF APPPCR_GUBUN.Locate('CODE',(Sender as TsEdit).Text,[]) Then
      begin
        (Sender as TsEdit).Text := APPPCR_GUBUN.FieldByName('CODE').AsString;
        sEdit17.Text := APPPCR_GUBUN.FieldByName('NAME').AsString;
        Edt_BuyConfirmDocuments.Enabled := Edt_DocumentsSection.Text = '2CH';
      end
      else
        Edt_DocumentsSectionDblClick(Sender);
    end
    else
    //변동구분
    IF AnsiMatchText((Sender as TsEdit).Hint,['CHG_G']) Then
    begin
      IF (Sender as TsEdit).Text = '' Then
      begin
        sEdit28.Text := '';
        sEdit31.Text := '';
        sEdit32.Text := '';
        sEdit29.Text := '';
      end
      else
      begin
        BYEONDONG_GUBUN.First;
        IF BYEONDONG_GUBUN.Locate('CODE',(Sender as TsEdit).Text,[]) Then
        begin
          (Sender as TsEdit).Text := BYEONDONG_GUBUN.FieldByName('CODE').AsString;
          sEdit28.Text := BYEONDONG_GUBUN.FieldByName('NAME').AsString;
          sEdit31.BoundLabel.Caption := '총' + sEdit28.Text + '금액';
          sEdit29.BoundLabel.Caption := '총' + sEdit28.Text + '금액';
        end
        else
          Edt_DocumentsSectionDblClick(Sender);
      end;
    end
    else
    //내국신4025 - 공급물품명세구분
    IF AnsiMatchText((Sender as TsEdit).Hint,['ITM_GBN']) Then
    begin
      NAEGUKSIN4025.First;
      IF NAEGUKSIN4025.Locate('CODE',(Sender as TsEdit).Text,[]) Then
      begin
        (Sender as TsEdit).Text := NAEGUKSIN4025.FieldByName('CODE').AsString;
        sEdit34.Text := NAEGUKSIN4025.FieldByName('NAME').AsString;
      end
      else
        Edt_DocumentsSectionDblClick(Sender);
    end;
  end;
end;

procedure TDlg_APPPCR_CreateDoc_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FDetailList := TStringList.Create;
  FDocumentsList := TStringList.Create;
  FTaxList := TStringList.Create;
end;

procedure TDlg_APPPCR_CreateDoc_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  FDetailList.Free;
  FDocumentsList.Free;
  FTaxList.Free;
end;

procedure TDlg_APPPCR_CreateDoc_frm.InitStringGrid;
var
  nLoop : integer;
begin
  //Detail
  with StringGrid1 do
  begin
    ColCount := High(GridTitle_Detail)+1;
    for nLoop := 0 to High(GridTitle_Detail) do
    begin
      Cells[nLoop,0] := GridTitle_Detail[nLoop];
      StringGrid1.ColWidths[nLoop] := Gridwidth_Detail[nLoop];
    end;
  end;

  //Documents
  with StringGrid2 do
  begin
    ColCount := High(GridTitle_Documents)+1;
    for nLoop := 0 to High(GridTitle_Documents) do
    begin
      Cells[nLoop,0] := GridTitle_Documents[nLoop];
      ColWidths[nLoop] := Gridwidth_Documents[nLoop];
    end;
  end;

end;

procedure TDlg_APPPCR_CreateDoc_frm.FormShow(Sender: TObject);
begin
  inherited;
  InitStringGrid;
end;

procedure TDlg_APPPCR_CreateDoc_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_APPPCR_CreateDoc_frm.StringGrid1DrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  S : string;
  SavedAlign : Word;
begin
  inherited;
  IF (ARow = 0) OR (ACol IN [0,4,7])  then
  begin
    S := StringGrid1.Cells[ACol,ARow];
    SavedAlign := SetTextAlign(StringGrid1.Canvas.Handle,TA_CENTER);
    StringGrid1.Canvas.TextRect(Rect,Rect.Left + (Rect.Right - Rect.Left) div 2 , Rect.Top +2 , S);
    SetTextAlign(StringGrid1.Canvas.Handle,SavedAlign);
  end
  else
  IF (ARow > 0) and (ACol IN [3,5,6]) Then
  begin
    S := StringGrid1.Cells[ACol,ARow];
    SavedAlign := SetTextAlign(StringGrid1.Canvas.Handle,TA_RIGHT);
    StringGrid1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top +2 , S);
    SetTextAlign(StringGrid1.Canvas.Handle,SavedAlign);
  end;
end;

end.
