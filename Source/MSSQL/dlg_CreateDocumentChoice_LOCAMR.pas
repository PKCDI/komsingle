unit dlg_CreateDocumentChoice_LOCAMR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Dlg_APPPCR_newDocSelect, sSkinProvider, StdCtrls, sButton,
  ExtCtrls, sPanel, sBevel, sLabel;

type
  Tdlg_CreateDocumentChoice_LOCAMR_frm = class(Tdlg_APPPCR_newDocSelect_frm)
    sButton4: TsButton;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sBevel1: TsBevel;
    sLabel1: TsLabel;
    sBevel2: TsBevel;
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_CreateDocumentChoice_LOCAMR_frm: Tdlg_CreateDocumentChoice_LOCAMR_frm;

implementation

{$R *.dfm}

procedure Tdlg_CreateDocumentChoice_LOCAMR_frm.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_ESCAPE then ModalResult := mrCancel;
end;

end.
