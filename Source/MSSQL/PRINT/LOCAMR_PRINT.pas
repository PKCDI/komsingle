unit LOCAMR_PRINT;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, StrUtils, DateUtils;

type
  TLOCAMR_PRINT_frm = class(TQuickRep)
    BAND_HEADER: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QR_MAINT_NO: TQRLabel;
    QR_APP_DATE: TQRLabel;
    QRImage1: TQRImage;
    QRLabel10: TQRLabel;
    PageFooterBand1: TQRBand;
    QRImage2: TQRImage;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QRLabel11: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC3: TQRLabel;
    QR_APPADDR1: TQRLabel;
    QRLabel26: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    QR_CHKNAME: TQRLabel;
    QR_BNFADDR1: TQRLabel;
    QRLabel24: TQRLabel;
    QR_ISSBANK: TQRLabel;
    QR_ISSBANK1: TQRLabel;
    QR_ISSBANK2: TQRLabel;
    QRLabel6: TQRLabel;
    QR_LOC_TYPENAME: TQRLabel;
    CHILD_OFFERNO: TQRChildBand;
    QRLabel12: TQRLabel;
    QR_LOC_AMT: TQRLabel;
    QRLabel14: TQRLabel;
    QR_CD_PER: TQRLabel;
    QRLabel16: TQRLabel;
    QR_LC_NO: TQRLabel;
    QRLabel18: TQRLabel;
    QR_OFFERNO6: TQRLabel;
    QR_OFFERNO8: TQRLabel;
    QR_OFFERNO9: TQRLabel;
    QR_OFFERNO7: TQRLabel;
    QR_OFFERNO5: TQRLabel;
    QR_OFFERNO2: TQRLabel;
    QR_OFFERNO1: TQRLabel;
    QR_OFFERNO4: TQRLabel;
    QR_OFFERNO3: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel19: TQRLabel;
    QR_ISS_DATE: TQRLabel;
    CHILD_DELIVERY: TQRChildBand;
    QRLabel20: TQRLabel;
    QR_AMD_NO: TQRLabel;
    QRLabel22: TQRLabel;
    QR_DELIVERY: TQRLabel;
    CHILD_EXPIRY: TQRChildBand;
    QRLabel25: TQRLabel;
    QR_EXPIRY: TQRLabel;
    CHILD_CHGINFO: TQRChildBand;
    QRLabel28: TQRLabel;
    QR_CHGINFO: TQRMemo;
    CHILD_REMARK1: TQRChildBand;
    QRLabel29: TQRLabel;
    QR_REMARK1: TQRMemo;
    QR_APPADDR2: TQRLabel;
    QR_BNFADDR2: TQRLabel;
    QR_EMAIL: TQRLabel;
    QRShape2: TQRShape;
    QRLabel80: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel76: TQRLabel;
    procedure PageFooterBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FFields : TFields;
    function getValueFromField(var QRLabel : TQRLabel;Format : String=''):String;
  public
    procedure PrintDocument(Fields : TFields;uPreview : Boolean = True);
  end;

var
  LOCAMR_PRINT_frm: TLOCAMR_PRINT_frm;

implementation

uses Commonlib;

{$R *.DFM}

function TLOCAMR_PRINT_frm.getValueFromField(var QRLabel: TQRLabel;
  Format: String): String;
var
  sFieldName : string;
begin
  sFieldName := AnsiReplaceText(UpperCase(QRLabel.Name),'QR_','');
  Result := FFields.FieldByName(sFieldName).AsString;

  CASE AnsiIndexText(Format,['YYYY-MM-DD','YYYYMMDD','YYYY.MM.DD','#,0.####','#,0']) OF
    0: Result := FormatDateTime('YYYY-MM-DD',ConvertStr2Date(FFields.FieldByName(sFieldName).AsString));
    1: Result := FormatDateTime('YYYYMMDD',ConvertStr2Date(FFields.FieldByName(sFieldName).AsString));
    2: Result := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FFields.FieldByName(sFieldName).AsString));
    3: Result := FormatFloat('#,0.####',FFields.FieldByName(sFieldName).AsCurrency);
    4: Result := FormatFloat('#,0',FFields.FieldByName(sFieldName).AsCurrency);
  end;

  QRLabel.Caption := Result;
end;

Const
  BETWEEN_HEIGHT : integer = 16;
procedure TLOCAMR_PRINT_frm.PrintDocument(Fields: TFields;
  uPreview: Boolean);
var
  i : Integer;
  nIndex : Integer;
  TempStr : String;
  nTop : Integer;
begin
  FFields := Fields;
  with Fields do
  begin
  //------------------------------------------------------------------------------
  // PAGE HEADER SECTION
  //------------------------------------------------------------------------------
    BAND_HEADER.Height := BAND_HEADER.Tag;
    //문서번호
    QR_MAINT_NO.Caption := '전자문서번호 : '+FieldByName('MAINT_NO').AsString;
    //통지일자
    QR_APP_DATE.Caption := '조건변경 신청일자 : '+FormatDateTime('YYYY-MM-DD',ConvertStr2Date(FieldByName('APP_DATE').AsString));
  //------------------------------------------------------------------------------
  // CHILD_HEADER SECTION
  //------------------------------------------------------------------------------
//    CHILD_HEADER.Height := CHILD_HEADER.Tag;
//    개설은행
//    QR_AP_BANK1.Caption := getValueFromField(QR_AP_BANK1);
//    QR_AP_BANK2.Caption := getValueFromField(QR_AP_BANK2);
//    개설일자
//    QR_ISS_DATE.Caption := getValueFromField(QR_ISS_DATE);
//    신용장번호
//    QR_LC_NO.Caption := getValueFromField(QR_LC_NO);
  //------------------------------------------------------------------------------
  // BAND_HEADER
  //------------------------------------------------------------------------------
    //개설자상호
    getValueFromField(QR_APPLIC1);
    //개설자대표자
    getValueFromField(QR_APPLIC2);
    //개설자사업자등록번호
    getValueFromField(QR_APPLIC3);
    //개설자 주소1
    getValueFromField(QR_APPADDR1);
    //개설자 주소2
    getValueFromField(QR_APPADDR2);

    //수혜자 상호
    getValueFromField(QR_BENEFC1);
    //수혜자 대표
    getValueFromField(QR_BENEFC2);
    //사업자등록번호
    getValueFromField(QR_BENEFC3);
    //식별자
    QR_CHKNAME.Caption := '/'+FieldByName('CHKNAME1').AsString+'/'+FieldByName('CHKNAME2').AsString;
    if (Trim(FieldByName('CHKNAME1').AsString) = '') and (Trim(FieldByName('CHKNAME2').AsString) = '') then QR_CHKNAME.Enabled := False;

    //주소1
    QR_BNFADDR1.Caption := getValueFromField(QR_BNFADDR1);
    //주소2
    QR_BNFADDR2.Caption := getValueFromField(QR_BNFADDR2);
    //이메일
    QR_EMAIL.Caption := FieldByName('BNFEMAILID').AsString+'@'+FieldByName('BNFDOMAIN').AsString;
    IF QR_EMAIL.Caption = '@' Then QR_EMAIL.Caption := '';

    //개설은행
    getValueFromField(QR_ISSBANK);
    getValueFromField(QR_ISSBANK1);
    getValueFromField(QR_ISSBANK2);

    nTop := 396;
    //내국신용장 종류
    getValueFromField(QR_LOC_TYPENAME);
    IF Trim(QR_LOC_TYPENAME.Caption) = '' Then
    begin
      QRLabel6.Enabled := False;
      QR_LOC_TYPENAME.Enabled := false;
    end
    else
    begin
      QR_LOC_TYPENAME.Caption := FFields.FieldByName('LOC_TYPE').asString+' '+QR_LOC_TYPENAME.Caption;
      QRLabel6.Enabled := True;
      QR_LOC_TYPENAME.Enabled := True;
      nTop := nTop + BETWEEN_HEIGHT;
    end;

    //변경후 외화금액
    QR_LOC_AMT.Caption := FieldByName('LOC_AMTC').AsString+' '+FormatFloat('#,0.####',FieldByName('LOC_AMT').AsCurrency);
    if FieldByName('LOC_AMT').AsCurrency = 0 then
    begin
      QRLabel12.Enabled := False;
      QR_LOC_AMT.Enabled := False;
    end
    else
    begin
      QRLabel12.Top := nTop;
      QR_LOC_AMT.Top := nTop;

      QRLabel12.Enabled := True;
      QR_LOC_AMT.Enabled := True;
      nTop := nTop + BETWEEN_HEIGHT;
    end;

    //허용오차
    QR_CD_PER.Caption := '(±) '+FieldByName('CD_PERP').AsString+' / '+FieldByName('CD_PERM').AsString+' (%)';
    IF FieldByName('CD_PERP').isnull AND FieldByName('CD_PERM').IsNull Then
    begin
      QRLabel14.Enabled := False;
      QR_CD_PER.Enabled := False;
    end
    else
    begin
      QRLabel14.Top := nTop;
      QR_CD_PER.Top := nTop;

      QRLabel14.Enabled := True;
      QR_CD_PER.Enabled := True;
      nTop := nTop + BETWEEN_HEIGHT;
    end;

    //내국신용장 번호
    getValueFromField(QR_LC_NO);
    QRLabel16.Top := nTop;
    QR_LC_NO.Top := nTop;

    BAND_HEADER.Height := nTop+BETWEEN_HEIGHT;
  //------------------------------------------------------------------------------
  // CHILD_OFFERNO
  //------------------------------------------------------------------------------
    //물품매도확약서
    nIndex := 1;
    for i := 1 to 9 do
    begin
      if Trim(FieldByName('OFFERNO'+IntToStr(i)).AsString) = '' Then Continue;

      (FindComponent('QR_OFFERNO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_OFFERNO'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('OFFERNO'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;
    CHILD_OFFERNO.Height := (15*(nIndex-1))+1;
    IF CHILD_OFFERNO.Height = 1 Then CHILD_OFFERNO.Enabled := False;

    //개설일자
    getValueFromField(QR_ISS_DATE,'YYYY-MM-DD');
    //조건변경횟수
    getValueFromField(QR_AMD_NO);

  //------------------------------------------------------------------------------
  // CHILD_DELIVERY
  //------------------------------------------------------------------------------
    //변경후물품인도기일
    CHILD_DELIVERY.Enabled := (not FieldByName('DELIVERY').IsNull) and (Trim(FieldByName('DELIVERY').AsString) <> '');
    getValueFromField(QR_DELIVERY,'YYYY-MM-DD');
  //------------------------------------------------------------------------------
  // CHILD_EXPIRY
  //------------------------------------------------------------------------------
    //유효기일
    CHILD_EXPIRY.Enabled := (not FieldByName('EXPIRY').IsNull) and (Trim(FieldByName('EXPIRY').AsString) <> '');
    getValueFromField(QR_EXPIRY,'YYYY-MM-DD');

  //------------------------------------------------------------------------------
  // CHILD_CHGINFO
  //------------------------------------------------------------------------------
    //기타조건변경사항
    QR_CHGINFO.Lines.Text := Trim(FieldByName('CHGINFO1').AsString);
    IF QR_CHGINFO.Lines.Count > 0 Then
    begin
      CHILD_CHGINFO.Enabled := True;
      QR_CHGINFO.Height := QR_CHGINFO.Lines.Count*12;
      //------------------------------------------------------------------------------
      // 섹션 높이 변경
      //------------------------------------------------------------------------------
      CHILD_CHGINFO.Height := QR_CHGINFO.Height + 1;
    end
    else
      CHILD_CHGINFO.Enabled := False;

  //------------------------------------------------------------------------------
  // CHILD_REMARK1
  //------------------------------------------------------------------------------
    //기타정보
    QR_REMARK1.Lines.Text := Trim(FieldByName('REMARK1').AsString);
    IF QR_REMARK1.Lines.Count > 0 Then
    begin
      CHILD_REMARK1.Enabled := True;
      QR_REMARK1.Height := QR_REMARK1.Lines.Count*12;
      //------------------------------------------------------------------------------
      // 섹션 높이 변경
      //------------------------------------------------------------------------------
      CHILD_REMARK1.Height := QR_REMARK1.Height + 1;
    end
    else
      CHILD_REMARK1.Enabled := False;

//------------------------------------------------------------------------------
// SIGN SECTION
//------------------------------------------------------------------------------
    QR_EXNAME1.Caption := getValueFromField(QR_EXNAME1);
    QR_EXNAME2.Caption := getValueFromField(QR_EXNAME2);
    QR_EXNAME3.Caption := getValueFromField(QR_EXNAME3);
  end;

//  Self.Prepare;
//  IF uPreview Then Self.Preview
//  else
//  begin
//    Self.PrinterSetup;
////------------------------------------------------------------------------------
//// 프린트 셋업이후 OK면 0 CANCEL이면 1이 리턴됨
////------------------------------------------------------------------------------
//    IF Self.Tag = 0 Then Self.Print;
//  end;

end;
procedure TLOCAMR_PRINT_frm.PageFooterBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  PageFooterBand1.AlignToBottom := Self.PageNumber = 1;
end;

end.
