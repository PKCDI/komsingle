unit PRINT_PARENT;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, StrUtils, DateUtils;

type
  TPRINT_PARENT_QR = class(TQuickRep)
  protected
    FFields : TFields;
    FDetailFields : TFields;
    FPreview : Boolean;
    function getValueFromField(var QRLabel : TQRLabel;Format : String=''):String; overload;
    function getValueFromField(FieldName : String;Format : String=''):String; overload;
    procedure getValueMemo(var QRMemo : TQRMemo; FieldName : String; Format : String='');

  //DOMOFC_D 를 출력하기위해 사용--------------------------------------------------------------
    function getValueFromDetailField(var QRLabel : TQRLabel;Format : String=''):String; overload;
    function getValueFromDetailField(FieldName : String;Format : String=''):String; overload;
    procedure getValueDetailMemo(var QRMemo : TQRMemo; FieldName : String; Format : String='');
  //--------------------------------------------------------------------------------------------

    procedure RunPrint;
  public
    procedure PrintDocument(Fields : TFields;uPreview : Boolean = True); virtual; abstract;

  end;

var
  PRINT_PARENT_QR: TPRINT_PARENT_QR;

implementation

uses Commonlib;

{$R *.DFM}

{ TQuickReport1 }

function TPRINT_PARENT_QR.getValueFromField(var QRLabel: TQRLabel;
  Format: String): String;
var
  sFieldName : string;
begin
  sFieldName := AnsiReplaceText(UpperCase(QRLabel.Name),'QR_','');
  Result := FFields.FieldByName(sFieldName).AsString;

  CASE AnsiIndexText(Format,['YYYY-MM-DD','YYYYMMDD','YYYY.MM.DD','#,0.####','#,0']) OF
    0: Result := FormatDateTime('YYYY-MM-DD',ConvertStr2Date(FFields.FieldByName(sFieldName).AsString));
    1: Result := FormatDateTime('YYYYMMDD',ConvertStr2Date(FFields.FieldByName(sFieldName).AsString));
    2: Result := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FFields.FieldByName(sFieldName).AsString));
    3: Result := FormatFloat('#,0.####',FFields.FieldByName(sFieldName).AsCurrency);
    4: Result := FormatFloat('#,0',FFields.FieldByName(sFieldName).AsCurrency);
  end;

  QRLabel.Caption := Result;
end;

function TPRINT_PARENT_QR.getValueFromField(FieldName, Format: String): String;
begin
  Result := Trim(FFields.FieldByName(FieldName).AsString);

  IF Result <> '' Then
  begin
    CASE AnsiIndexText(Format,['YYYY-MM-DD','YYYYMMDD','YYYY.MM.DD','#,0.####','#,0']) OF
      0: Result := FormatDateTime('YYYY-MM-DD',ConvertStr2Date(FFields.FieldByName(FieldName).AsString));
      1: Result := FormatDateTime('YYYYMMDD',ConvertStr2Date(FFields.FieldByName(FieldName).AsString));
      2: Result := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FFields.FieldByName(FieldName).AsString));
      3: Result := FormatFloat('#,0.####',FFields.FieldByName(FieldName).AsCurrency);
      4: Result := FormatFloat('#,0',FFields.FieldByName(FieldName).AsCurrency);
    end;
  end;
end;

procedure TPRINT_PARENT_QR.getValueMemo(var QRMemo : TQRMemo; FieldName : String;Format : String='');
var
  TempStr : string;
begin
  TempStr := getValueFromField(FieldName,Format);
  IF TempStr <> '' Then
  begin
    IF FFields.FieldByName(FieldName) is TMemoField Then
      QRMemo.Lines.Text := TempStr
    else
      QRMemo.Lines.Add(TempStr);
  end;
end;

function TPRINT_PARENT_QR.getValueFromDetailField(var QRLabel: TQRLabel;
  Format: String): String;
var
  sFieldName : string;
begin
  sFieldName := AnsiReplaceText(UpperCase(QRLabel.Name),'QR_','');
  Result := FDetailFields.FieldByName(sFieldName).AsString;

  CASE AnsiIndexText(Format,['YYYY-MM-DD','YYYYMMDD','YYYY.MM.DD','#,0.####','#,0']) OF
    0: Result := FormatDateTime('YYYY-MM-DD',ConvertStr2Date(FDetailFields.FieldByName(sFieldName).AsString));
    1: Result := FormatDateTime('YYYYMMDD',ConvertStr2Date(FDetailFields.FieldByName(sFieldName).AsString));
    2: Result := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FDetailFields.FieldByName(sFieldName).AsString));
    3: Result := FormatFloat('#,0.####',FDetailFields.FieldByName(sFieldName).AsCurrency);
    4: Result := FormatFloat('#,0',FDetailFields.FieldByName(sFieldName).AsCurrency);

  end;
end;

function TPRINT_PARENT_QR.getValueFromDetailField(FieldName,
  Format: String): String;
begin
  Result := Trim(FDetailFields.FieldByName(FieldName).AsString);

  IF Result <> '' Then
  begin
    CASE AnsiIndexText(Format,['YYYY-MM-DD','YYYYMMDD','YYYY.MM.DD','#,0.####','#,0']) OF
      0: Result := FormatDateTime('YYYY-MM-DD',ConvertStr2Date(FDetailFields.FieldByName(FieldName).AsString));
      1: Result := FormatDateTime('YYYYMMDD',ConvertStr2Date(FDetailFields.FieldByName(FieldName).AsString));
      2: Result := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FDetailFields.FieldByName(FieldName).AsString));
      3: Result := FormatFloat('#,0.####',FDetailFields.FieldByName(FieldName).AsCurrency);
      4: Result := FormatFloat('#,0',FDetailFields.FieldByName(FieldName).AsCurrency);
    end;
  end;
end;

procedure TPRINT_PARENT_QR.getValueDetailMemo(var QRMemo: TQRMemo;
  FieldName, Format: String);
var
  TempStr : string;
begin
  TempStr := getValueFromDetailField(FieldName,Format);
  IF TempStr <> '' Then
  begin
    IF FDetailFields.FieldByName(FieldName) is TMemoField Then
      QRMemo.Lines.Text := TempStr
    else
      QRMemo.Lines.Add(TempStr);
  end;

end;


procedure TPRINT_PARENT_QR.RunPrint;
begin
  Self.Prepare;
  IF FPreview Then
    Self.Preview
  else
  begin
    Self.PrinterSetup;
//------------------------------------------------------------------------------
// 프린트 셋업이후 OK면 0 CANCEL이면 1이 리턴됨
//------------------------------------------------------------------------------
    IF Self.Tag = 0 Then Self.Print;
  end;
end;


end.
 