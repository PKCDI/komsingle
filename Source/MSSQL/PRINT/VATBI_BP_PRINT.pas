unit VATBI_BP_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, DB, ADODB, QRCtrls, QuickRpt, ExtCtrls;

type
  TVATBI_BP_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel4: TQRLabel;
    QR_MAINT_NO: TQRLabel;
    QRLabel2: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel3: TQRLabel;
    QR_RENO: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel6: TQRLabel;
    QR_SENO: TQRLabel;
    ChildBand23: TQRChildBand;
    QRLabel5: TQRLabel;
    QR_FSNO: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel8: TQRLabel;
    QR_ACENO: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel10: TQRLabel;
    QR_RFFNO: TQRLabel;
    ChildBand6: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel12: TQRLabel;
    ChildBand7: TQRChildBand;
    QR_SESAUP: TQRLabel;
    QRLabel13: TQRLabel;
    ChildBand8: TQRChildBand;
    QR_SENAME1: TQRLabel;
    QRLabel16: TQRLabel;
    QR_SENAME2: TQRLabel;
    ChildBand10: TQRChildBand;
    QR_SEADDR3: TQRLabel;
    QR_SEADDR2: TQRLabel;
    QR_SEADDR1: TQRLabel;
    QRLabel19: TQRLabel;
    QR_SEADDR4: TQRLabel;
    QR_SEADDR5: TQRLabel;
    ChildBand11: TQRChildBand;
    QRLabel23: TQRLabel;
    QR_SENAME3: TQRLabel;
    ChildBand12: TQRChildBand;
    QRLabel25: TQRLabel;
    QR_SEUPTA1: TQRMemo;
    ChildBand13: TQRChildBand;
    QRLabel29: TQRLabel;
    QR_SEITEM1: TQRMemo;
    ChildBand14: TQRChildBand;
    QRLabel33: TQRLabel;
    QRImage1: TQRImage;
    ChildBand15: TQRChildBand;
    QRLabel34: TQRLabel;
    QR_BYSAUP: TQRLabel;
    ChildBand18: TQRChildBand;
    QRLabel40: TQRLabel;
    QR_BYADDR1: TQRLabel;
    QR_BYADDR2: TQRLabel;
    QR_BYADDR3: TQRLabel;
    QR_BYADDR4: TQRLabel;
    QR_BYADDR5: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel36: TQRLabel;
    QR_BYNAME1: TQRLabel;
    QR_BYNAME2: TQRLabel;
    ChildBand19: TQRChildBand;
    QRLabel44: TQRLabel;
    QR_BYNAME3: TQRLabel;
    ChildBand20: TQRChildBand;
    QRLabel46: TQRLabel;
    QR_BYUPTA1: TQRMemo;
    ChildBand21: TQRChildBand;
    QRLabel50: TQRLabel;
    QR_BYITEM1: TQRMemo;
    ChildBand9: TQRChildBand;
    QRImage10: TQRImage;
    QRLabel7: TQRLabel;
    ChildBand17: TQRChildBand;
    QRLabel9: TQRLabel;
    QR_AGSAUP: TQRLabel;
    ChildBand29: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_AGNAME1: TQRLabel;
    QR_AGNAME2: TQRLabel;
    ChildBand30: TQRChildBand;
    QRLabel20: TQRLabel;
    QR_AGADDR1: TQRLabel;
    QR_AGADDR2: TQRLabel;
    QR_AGADDR3: TQRLabel;
    QR_AGADDR4: TQRLabel;
    QR_AGADDR5: TQRLabel;
    ChildBand31: TQRChildBand;
    QRLabel28: TQRLabel;
    QR_AGNAME3: TQRLabel;
    ChildBand32: TQRChildBand;
    QR_AGUPTA1: TQRMemo;
    QRLabel31: TQRLabel;
    ChildBand33: TQRChildBand;
    QR_AGITEM1: TQRMemo;
    QRLabel32: TQRLabel;
    ChildBand22: TQRChildBand;
    QRImage2: TQRImage;
    QRImage4: TQRImage;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QR_DRAWDAT: TQRLabel;
    QR_DETAILNO: TQRLabel;
    QR_SUPAMT: TQRLabel;
    QR_TAXAMT: TQRLabel;
    QR_REMARK1: TQRMemo;
    ChildBand24: TQRChildBand;
    QRImage5: TQRImage;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRImage6: TQRImage;
    DetailBand1: TQRBand;
    QR_DEDATED: TQRLabel;
    QR_NAME1D: TQRMemo;
    QR_PRICED: TQRLabel;
    QR_STQTYD: TQRLabel;
    QR_SUPSTAMTD: TQRLabel;
    QR_USSTAMTD: TQRLabel;
    QR_TAXSTAMTD: TQRLabel;
    QR_RATE_D: TQRLabel;
    QR_SIZE1D: TQRMemo;
    QR_DEREM1D: TQRMemo;
    SummaryBand1: TQRBand;
    QRImage7: TQRImage;
    QRLabel78: TQRLabel;
    QR_SUPTAMT: TQRLabel;
    QR_USTAMT: TQRLabel;
    QR_TAXTAMT: TQRLabel;
    ChildBand25: TQRChildBand;
    QRImage8: TQRImage;
    QRLabel82: TQRLabel;
    QRImage9: TQRImage;
    QR_TAMT: TQRLabel;
    ChildBand26: TQRChildBand;
    QRLabel84: TQRLabel;
    label1: TQRLabel;
    QR_AMT12: TQRLabel;
    QR_AMT11: TQRLabel;
    label2: TQRLabel;
    QR_AMT22: TQRLabel;
    QR_AMT21: TQRLabel;
    label3: TQRLabel;
    QR_AMT32: TQRLabel;
    QR_AMT31: TQRLabel;
    label4: TQRLabel;
    QR_AMT42: TQRLabel;
    QR_AMT41: TQRLabel;
    ChildBand27: TQRChildBand;
    QRLabel86: TQRLabel;
    QR_INDICATOR: TQRLabel;
    QRLabel88: TQRLabel;
    QRImage13: TQRImage;
    QRImage14: TQRImage;
    ChildBand28: TQRChildBand;
    QRShape2: TQRShape;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel18: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel14: TQRLabel;
    qryList: TADOQuery;
    qryGoods: TADOQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  VATBI_BP_PRINT_frm: TVATBI_BP_PRINT_frm;

implementation

{$R *.dfm}

end.
