unit APPSPC_PRINT_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TAPPSPC_PRINT_NEW_frm = class(TPRINT_PARENT_QR)
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    qrDOCNO: TQRLabel;
    qrDATEE: TQRLabel;
    QRShape1: TQRShape;
    QRLabel126: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    qrCP_AMTC: TQRLabel;
    qrKRW: TQRLabel;
    qrCP_CUX: TQRLabel;
    qrCP_BANKNAME: TQRLabel;
    qrCP_AMT: TQRLabel;
    QRBand2: TQRBand;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListBGM_GUBUN: TStringField;
    qryListBGM1: TStringField;
    qryListBGM2: TStringField;
    qryListBGM3: TStringField;
    qryListBGM4: TStringField;
    qryListCP_ACCOUNTNO: TStringField;
    qryListCP_NAME1: TStringField;
    qryListCP_NAME2: TStringField;
    qryListCP_CURR: TStringField;
    qryListCP_BANK: TStringField;
    qryListCP_BANKNAME: TStringField;
    qryListCP_BANKBU: TStringField;
    qryListCP_ADD_ACCOUNTNO1: TStringField;
    qryListCP_ADD_NAME1: TStringField;
    qryListCP_ADD_CURR1: TStringField;
    qryListCP_ADD_ACCOUNTNO2: TStringField;
    qryListCP_ADD_NAME2: TStringField;
    qryListCP_ADD_CURR2: TStringField;
    qryListAPP_DATE: TStringField;
    qryListCP_CODE: TStringField;
    qryListCP_NO: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_SNAME: TStringField;
    qryListAPP_SAUPNO: TStringField;
    qryListAPP_NAME: TStringField;
    qryListAPP_ELEC: TStringField;
    qryListAPP_ADDR1: TStringField;
    qryListAPP_ADDR2: TStringField;
    qryListAPP_ADDR3: TStringField;
    qryListCP_AMTU: TBCDField;
    qryListCP_AMTC: TStringField;
    qryListCP_AMT: TBCDField;
    qryListCP_CUX: TBCDField;
    qryListDF_SAUPNO: TStringField;
    qryListDF_EMAIL1: TStringField;
    qryListDF_EMAIL2: TStringField;
    qryListDF_NAME1: TStringField;
    qryListDF_NAME2: TStringField;
    qryListDF_NAME3: TStringField;
    qryListDF_CNT: TStringField;
    qryListVB_CNT: TStringField;
    qryListSS_CNT: TStringField;
    qryDetail: TADOQuery;
    qryDetailKEYY: TStringField;
    qryDetailDOC_GUBUN: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailDOC_NO: TStringField;
    qryDetailLR_GUBUN: TStringField;
    qryDetailLR_NO: TStringField;
    qryDetailLR_NO2: TStringField;
    qryDetailISS_DATE: TStringField;
    qryDetailGET_DATE: TStringField;
    qryDetailEXP_DATE: TStringField;
    qryDetailACC_DATE: TStringField;
    qryDetailVAL_DATE: TStringField;
    qryDetailBSN_HSCODE: TStringField;
    qryDetailISS_EXPAMTU: TBCDField;
    qryDetailISS_EXPAMTC: TStringField;
    qryDetailISS_EXPAMT: TBCDField;
    qryDetailEXCH: TBCDField;
    qryDetailISS_AMTU: TBCDField;
    qryDetailISS_AMTC: TStringField;
    qryDetailISS_AMT: TBCDField;
    qryDetailISS_TOTAMT: TBCDField;
    qryDetailISS_TOTAMTC: TStringField;
    qryDetailTOTCNT: TBCDField;
    qryDetailTOTCNTC: TStringField;
    qryDetailVB_RENO: TStringField;
    qryDetailVB_SENO: TStringField;
    qryDetailVB_FSNO: TStringField;
    qryDetailVB_MAINTNO: TStringField;
    qryDetailVB_DMNO: TStringField;
    qryDetailVB_DETAILNO: TStringField;
    qryDetailVB_GUBUN: TStringField;
    qryDetailSE_GUBUN: TStringField;
    qryDetailSE_SAUPNO: TStringField;
    qryDetailSE_CODE: TStringField;
    qryDetailSE_SNAME: TStringField;
    qryDetailSE_NAME: TStringField;
    qryDetailSE_ELEC: TStringField;
    qryDetailSE_ADDR1: TStringField;
    qryDetailSE_ADDR2: TStringField;
    qryDetailSE_ADDR3: TStringField;
    qryDetailSG_UPTAI1_1: TStringField;
    qryDetailSG_UPTAI1_2: TStringField;
    qryDetailSG_UPTAI1_3: TStringField;
    qryDetailHN_ITEM1_1: TStringField;
    qryDetailHN_ITEM1_2: TStringField;
    qryDetailHN_ITEM1_3: TStringField;
    qryDetailBY_GUBUN: TStringField;
    qryDetailBY_SAUPNO: TStringField;
    qryDetailBY_CODE: TStringField;
    qryDetailBY_SNAME: TStringField;
    qryDetailBY_NAME: TStringField;
    qryDetailBY_ELEC: TStringField;
    qryDetailBY_ADDR1: TStringField;
    qryDetailBY_ADDR2: TStringField;
    qryDetailBY_ADDR3: TStringField;
    qryDetailSG_UPTAI2_1: TStringField;
    qryDetailSG_UPTAI2_2: TStringField;
    qryDetailSG_UPTAI2_3: TStringField;
    qryDetailHN_ITEM2_1: TStringField;
    qryDetailHN_ITEM2_2: TStringField;
    qryDetailHN_ITEM2_3: TStringField;
    qryDetailAG_SAUPNO: TStringField;
    qryDetailAG_CODE: TStringField;
    qryDetailAG_SNAME: TStringField;
    qryDetailAG_NAME: TStringField;
    qryDetailAG_ELEC: TStringField;
    qryDetailAG_ADDR1: TStringField;
    qryDetailAG_ADDR2: TStringField;
    qryDetailAG_ADDR3: TStringField;
    qryDetailLA_BANK: TStringField;
    qryDetailLA_BANKNAME: TStringField;
    qryDetailLA_BANKBU: TStringField;
    qryDetailLA_ELEC: TStringField;
    qryDetailPAI_CODE1: TStringField;
    qryDetailPAI_CODE2: TStringField;
    qryDetailPAI_CODE3: TStringField;
    qryDetailPAI_CODE4: TStringField;
    qryDetailPAI_AMTC1: TStringField;
    qryDetailPAI_AMTC2: TStringField;
    qryDetailPAI_AMTC3: TStringField;
    qryDetailPAI_AMTC4: TStringField;
    qryDetailPAI_AMTU1: TBCDField;
    qryDetailPAI_AMTU2: TBCDField;
    qryDetailPAI_AMTU3: TBCDField;
    qryDetailPAI_AMTU4: TBCDField;
    qryDetailPAI_AMT1: TBCDField;
    qryDetailPAI_AMT2: TBCDField;
    qryDetailPAI_AMT3: TBCDField;
    qryDetailPAI_AMT4: TBCDField;
    qryDetailLA_TYPE: TStringField;
    qryDetailLA_BANKBUCODE: TStringField;
    qryDetailLA_BANKNAMEP: TStringField;
    qryDetailLA_BANKBUP: TStringField;
    qryDetailREMARK1_1: TStringField;
    qryDetailREMARK1_2: TStringField;
    qryDetailREMARK1_3: TStringField;
    qryDetailREMARK1_4: TStringField;
    qryDetailREMARK1_5: TStringField;
    qryDetailREMARK2_1: TStringField;
    qryDetailREMARK2_2: TStringField;
    qryDetailREMARK2_3: TStringField;
    qryDetailREMARK2_4: TStringField;
    qryDetailREMARK2_5: TStringField;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    qrDetail: TQRBand;
    qrDOC_GUBUN: TQRLabel;
    qrDOC_NO: TQRLabel;
    qrLA_BANKNAMEP: TQRLabel;
    qrLR_NO: TQRLabel;
    qrCP_AMTU: TQRLabel;
    QRBand3: TQRBand;
    ChildBand1: TQRChildBand;
    QRChildBand1: TQRChildBand;
    qrISS_DATE: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    qrCP_BANKNAME2: TQRLabel;
    qrCP_ACCOUNTNO: TQRLabel;
    qrCP_AMTCU: TQRLabel;
    qrCP_NAME: TQRLabel;
    qrAPP_SNAME: TQRLabel;
    qrAPP_NAME: TQRLabel;
    qrAPP_SAUPNO: TQRLabel;
    qrAPP_ADDR1: TQRLabel;
    qrAPP_ELEC: TQRLabel;
    QRLabel26: TQRLabel;
    QRImage3: TQRImage;
    QRImage1: TQRImage;
    QRImage2: TQRImage;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
  function StrCase(Selector: string; StrList: array of string): Integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  APPSPC_PRINT_NEW_frm: TAPPSPC_PRINT_NEW_frm;

implementation

{$R *.dfm}
uses Commonlib,UI_APPSPC_NEW;

procedure TAPPSPC_PRINT_NEW_frm.QuickRepBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  qryList.Close;
  qryList.Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
  qryList.Open;
  qryDetail.Close;
  qryDetail.Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
  qryDetail.Open;


end;

procedure TAPPSPC_PRINT_NEW_frm.qrDetailBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  if qryDetail.RecordCount > 0 then
  begin
    case StrCase(Trim(qryDetailDOC_GUBUN.AsString), ['2AP','2AH','2AJ','1BW']) of
      0://내국신용장
      begin
        qrDOC_GUBUN.Caption := '내국신용장';
        //내국신용장의 발급번호는 LR_NO
        qrLR_NO.Caption := qryDetailLR_NO.AsString;
      end;
      1://물품수령증명서
      begin
        qrDOC_GUBUN.Caption := '물품수령증명서';
        //물품수령증명서의 발급번호는 LR_NO2
        qrLR_NO.Caption := qryDetailLR_NO2.AsString;

      end;
      2://세금계산서
      begin
        qrDOC_GUBUN.Caption := '세금계산서';
        qrLR_NO.Caption := ''; //해당항목 사용안함
      end;
      3://상업송장
      begin
        qrDOC_GUBUN.Caption := '상업송장';
        qrLR_NO.Caption := ''; //해당항목 사용안함
      end;
    end;
    //문서번호
    qrDOC_NO.Caption := qryDetailDOC_NO.AsString;
    //은행
    qrLA_BANKNAMEP.Caption := qryDetailLA_BANKNAMEP.AsString+' '+qryDetailLA_BANKBUP.AsString;
    //발급일
    qrISS_DATE.Caption := FormatDateTime('YYYY-MM-DD', ConvertStr2Date(qryDetailISS_DATE.AsString));
  end
  else
  begin
    qrDOC_gubun.Caption := '';
    qrLR_NO.Caption := '';
    qrDOC_NO.Caption := '';
    qrLA_BANKNAMEP.Caption := '';
    qrISS_DATE.Caption := '';
  end;

  qryDetail.Next;

//  if qryDetail.Eof then
//    qrDetail.Frame.DrawBottom := True
//  else
//    qryDetail.Next;
end;

procedure TAPPSPC_PRINT_NEW_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  MoreData := not qryDetail.Eof;
end;

function TAPPSPC_PRINT_NEW_frm.StrCase(Selector: string; StrList: array of string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to High(StrList) do
  begin
    if Selector = StrList[I] then
    begin
      Result := I;
      Break;
    end;
  end;
end;

procedure TAPPSPC_PRINT_NEW_frm.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //전자문서번호
  qrDOCNO.Caption := '전자문서번호 :  '+qryListBGM1.AsString+' - '+qryListBGM2.AsString+qryListBGM3.AsString+qryListBGM4.AsString;
  //신청일자
  qrDATEE.Caption := '신청일자  :  '+FormatDateTime('YYYY-MM-DD', ConvertStr2Date(qryListDATEE.AsString));
  //추심은행명
  qrCP_BANKNAME.Caption := qryListCP_BANKNAME.AsString+'   '+qryListCP_BANKBU.AsString;
  //추심금액외화가 입력되어있는경우만 출력
  if qryListCP_AMTU.AsCurrency > 0 then
  begin
    //추심금액외화
    qrCP_AMTU.Caption := FormatCurr('###,###,###.##;0',qryListCP_AMTU.AsCurrency);
    //추심금액통화단위
    qrCP_AMTC.Caption := qryListCP_AMTC.AsString;
  end
  else
  begin
    QRLabel7.Caption := '';
    qrCP_AMTU.Caption := '';
    qrCP_AMTC.Caption := '';
    QRBand1.Height := QRBand1.Height - 24;
    //추심금액원화 위치조정
    QRLabel8.Top := QRLabel8.Top - 24;
    qrKRW.Top := qrKRW.Top - 24;
    qrCP_AMT.Top := qrCP_AMT.Top - 24;
    //적용환율 위치조정
    QRLabel9.Top := QRLabel9.Top - 24;
    qrCP_CUX.Top := qrCP_CUX.Top - 24;
  end;

  //추심금액원화가 입력되어있는경우만 출력.
  if qryListCP_AMT.AsCurrency > 0 then
  begin
    //추심금액원화
    qrCP_AMT.Caption := FormatCurr('###,###,###.##;0',qryListCP_AMT.AsCurrency);
  end
  else
  begin
    QRLabel8.Caption := '';
    qrKRW.Caption := '';
    qrCP_AMT.Caption := '';
    //적용환율 위치조정
    QRLabel9.Top := QRLabel9.Top - 24;
    qrCP_CUX.Top := qrCP_CUX.Top - 24;
    QRBand1.Height := QRBand1.Height - 24;
  end;

  if qryListCP_CUX.AsCurrency > 0 then
  begin
    //적용환율
    qrCP_CUX.Caption := FormatCurr('###,###,##0.00;0',qryListCP_CUX.AsCurrency);
  end
  else
  begin
    QRLabel9.Caption := '';
    qrCP_CUX.Caption := '';
    QRBand1.Height := QRBand1.Height - 24;
  end;
end;

procedure TAPPSPC_PRINT_NEW_frm.ChildBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  //지급받을 은행명
  qrCP_BANKNAME2.Caption := qryListCP_BANKNAME.AsString+'   '+qryListCP_BANKBU.AsString;

  //계좌번호
  qrCP_ACCOUNTNO.Caption := qryListCP_ACCOUNTNO.AsString;

  //금액
  if qryListCP_AMTU.AsCurrency > 0 then
  begin
    if qryListCP_AMT.AsCurrency >0 then
      qrCP_AMTCU.Caption := qryListCP_AMTC.AsString+'  '+FormatCurr('###,###,###.##;0',qryListCP_AMTU.AsCurrency)+'  KRW '+FormatCurr('###,###,###.##;0',qryListCP_AMT.AsCurrency)
    else
      qrCP_AMTCU.Caption := qryListCP_AMTC.AsString+'  '+FormatCurr('###,###,###.##;0',qryListCP_AMTU.AsCurrency);
  end
  else
    qrCP_AMTCU.Caption := 'KRW '+FormatCurr('###,###,###.##;0',qryListCP_AMT.AsCurrency);

  //예금주
  if Trim(qryListCP_NAME2.AsString) = '' then
  begin
    qrCP_NAME.Caption := qryListCP_NAME1.AsString;
    ChildBand1.Height := ChildBand1.Height - 24;
  end
  else
    qrCP_NAME.Caption := qryListCP_NAME1.AsString+''+#13#10+qryListCP_NAME2.AsString;
end;

procedure TAPPSPC_PRINT_NEW_frm.QRChildBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  //신청인
  qrAPP_SNAME.Caption := qryListAPP_SNAME.AsString;
  //대표자
  qrAPP_NAME.Caption := qryListAPP_NAME.AsString;
  //사업자등록번호
  qrAPP_SAUPNO.Caption := qryListAPP_SAUPNO.AsString;
  //주소
  qrAPP_ADDR1.Caption := qryListAPP_ADDR1.AsString;
  if Trim(qryListAPP_ADDR2.AsString) = '' then
  begin
    QRLabel25.Top := QRLabel25.Top - 15;
    qrAPP_ELEC.Top := qrAPP_ELEC.Top - 15;
    QRChildBand1.Height := QRChildBand1.Height - 15;
  end
  else
    qrAPP_ADDR1.Caption := qrAPP_ADDR1.Caption + #13#10 + qryListAPP_ADDR2.AsString;

  if Trim(qryListAPP_ADDR3.AsString) = '' then
  begin
    QRLabel25.Top := QRLabel25.Top - 15;
    qrAPP_ELEC.Top := qrAPP_ELEC.Top - 15;
    QRChildBand1.Height := QRChildBand1.Height - 15;
  end
  else
    qrAPP_ADDR1.Caption := qrAPP_ADDR1.Caption + #13#10 + qryListAPP_ADDR3.AsString;

  //전자서명 필수아님.
  if Trim(qryListAPP_ELEC.AsString) = '' then
  begin
      QRChildBand1.Height := QRChildBand1.Height - 24;
      QRLabel25.Caption := '';
      qrAPP_ELEC.Caption := '';
  end
  else
    qrAPP_ELEC.Caption := qryListAPP_ELEC.AsString;

end;

procedure TAPPSPC_PRINT_NEW_frm.QRBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  IF qryDetail.Eof then
    PrintBand := False;
end;

end.
