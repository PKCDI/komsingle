unit DOANTC_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TDOANTC_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel4: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QRLabel6: TQRLabel;
    QR_RESDATE: TQRLabel;
    ChildBand2: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel8: TQRLabel;
    QR_BANK1: TQRLabel;
    QR_BANK2: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_APPNAME1: TQRLabel;
    QR_APPNAME2: TQRLabel;
    QR_APPNAME3: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel3: TQRLabel;
    QR_LCNO: TQRLabel;
    QR_BLNO: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QR_AMT: TQRLabel;
    QRLabel12: TQRLabel;
    QR_CHRG: TQRLabel;
    QRLabel14: TQRLabel;
    QR_SETDATE: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel68: TQRLabel;
    QR_REMARK1: TQRMemo;
    ChildBand6: TQRChildBand;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QR_BKNAME1: TQRLabel;
    QR_BKNAME2: TQRLabel;
    QR_BKNAME3: TQRLabel;
    QRImage1: TQRImage;
    ChildBand7: TQRChildBand;
    QRShape1: TQRShape;
    QRLabel94: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel88: TQRLabel;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListBANK_CODE: TStringField;
    qryListBANK1: TStringField;
    qryListBANK2: TStringField;
    qryListLC_G: TStringField;
    qryListLC_NO: TStringField;
    qryListBL_G: TStringField;
    qryListBL_NO: TStringField;
    qryListAMT: TBCDField;
    qryListAMTC: TStringField;
    qryListCHRG: TBCDField;
    qryListCHRGC: TStringField;
    qryListRES_DATE: TStringField;
    qryListSET_DATE: TStringField;
    qryListREMARK1: TMemoField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListBK_NAME3: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    SummaryBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNO: string;
    procedure OPENDB;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
  end;

var
  DOANTC_PRINT_frm: TDOANTC_PRINT_frm;

implementation

{$R *.dfm}

uses MSSQL , Commonlib;

procedure TDOANTC_PRINT_frm.OPENDB;
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryList.Open;
end;

procedure TDOANTC_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TDOANTC_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
 //---------ChildBand1-------------------------------------------------------
  ChildBand1.Height := ChildBand1.Tag;
  //개설신청서전자문서번호
  QR_MAINTNO.Caption := qryListMAINT_NO.AsString;
  //통지일자
  QR_RESDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListRES_DATE.AsString));

end;

procedure TDOANTC_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand2-------------------------------------------------------
  //통지은행
    nIndex := 1;
    for i := 1 to 2 do
    begin
      if Trim(qrylist.FieldByName('BANK'+IntToStr(i)).AsString) = '' then
      begin
        (FindComponent('QR_BANK'+IntToStr(i)) as TQRLabel).Caption := '';
        Continue;
      end;

      (FindComponent('QR_BANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_BANK'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('BANK'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    if qryListBANK2.AsString = '' then
      ChildBand2.Height := 25
    else
      ChildBand2.Height := 38;

end;

procedure TDOANTC_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand3-------------------------------------------------------
  //거래고객
    nIndex := 1;
    for i := 1 to 3 do
    begin
      if Trim(qrylist.FieldByName('APP_NAME'+IntToStr(i)).AsString) = '' then
      begin
        (FindComponent('QR_APPNAME'+IntToStr(i)) as TQRLabel).Caption := '';
        Continue;
      end;

      (FindComponent('QR_APPNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_APPNAME'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('APP_NAME'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    ChildBand3.Height := (15 * (nIndex-1))+ 30;

end;

procedure TDOANTC_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  ChildBand4.Height := ChildBand4.Tag;
  //신용장 계약서번호
  QR_LCNO.Caption := qryListLC_NO.AsString;
  //선하증권번호
  QR_BLNO.Caption := qryListBL_NO.AsString;
  //어음금액
  QR_AMT.Caption := qryListAMTC.AsString + ' ' + FormatFloat('#,##0.####' , qryListAMT.AsFloat);
  if (qryListAMT.AsCurrency = 0) and (Trim(qryListAMTC.AsString) = '') then
    QR_AMT.Enabled := False;
  //기타수수료
  QR_CHRG.Caption := qryListCHRGC.AsString + ' ' + FormatFloat('#,##0.####' , qryListCHRG.AsFloat);
  if (qryListCHRG.AsCurrency = 0) and (Trim(qryListCHRGC.AsString)= '') then
    QR_CHRG.Enabled := False;
  //최종결제일
  QR_SETDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListSET_DATE.AsString));
end;

procedure TDOANTC_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand5-------------------------------------------------------
  //기타구비서류
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListREMARK1.AsString;
    ChildBand5.Height := ( 14 * memoLines.Count) + 4;
    QR_REMARK1.Height := ( 14 * memoLines.Count);

    QR_REMARK1.Lines.Clear;
    QR_REMARK1.Lines.Text := qryListREMARK1.AsString;

  finally
    memoLines.Free;
  end;            
end;

procedure TDOANTC_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand6-------------------------------------------------------
  //발신기관 전자서명
    nIndex := 1;
    for i := 1 to 3 do
    begin
      if Trim(qrylist.FieldByName('BK_NAME'+IntToStr(i)).AsString) = '' then
      begin
        (FindComponent('QR_BKNAME'+IntToStr(i)) as TQRLabel).Caption := '';
        Continue;
      end;

      (FindComponent('QR_BKNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_BKNAME'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('BK_NAME'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    ChildBand6.Height := (15 * (nIndex-1)) + 38;
end;

end.
