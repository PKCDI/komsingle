unit LOCADV_PRINT;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils, DateUtils;

type
  TLOCADV_PRINT_frm = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRImage1: TQRImage;
    QRLabel10: TQRLabel;
    QRImage2: TQRImage;
    QRLabel71: TQRLabel;
    QRShape2: TQRShape;
    QRLabel76: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    CHILD_HEADER: TQRChildBand;
    QRLabel9: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    CHILD_CREATEUSER: TQRChildBand;
    QRLabel5: TQRLabel;
    CHILD_RECVUSER: TQRChildBand;
    QRLabel12: TQRLabel;
    CHILD_ETC1: TQRChildBand;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    CHILD_DOCSELL: TQRChildBand;
    QRLabel3: TQRLabel;
    CHILD_DOCCOPY: TQRChildBand;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QR_DOCCOPY1: TQRLabel;
    QR_DOCCOPY2: TQRLabel;
    QR_DOCCOPY3: TQRLabel;
    QR_DOCCOPY4: TQRLabel;
    QR_DOCCOPY1_QTY: TQRLabel;
    QR_DOCCOPY2_QTY: TQRLabel;
    QR_DOCCOPY3_QTY: TQRLabel;
    QR_DOCCOPY4_QTY: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    CHILD_ETC_DOC: TQRChildBand;
    QRLabel33: TQRLabel;
    QRM_DOC_ETC1: TQRMemo;
    CHILD_REMARK1: TQRChildBand;
    QRLabel34: TQRLabel;
    QRM_REMARK1: TQRMemo;
    ChildBand7: TQRChildBand;
    QRShape1: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    CHILD_GOODS: TQRChildBand;
    QRLabel41: TQRLabel;
    QRM_GOODS: TQRMemo;
    QR_BSN_HSCODE: TQRLabel;
    CHILD_EXPORT1: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel4: TQRLabel;
    QRLabel11: TQRLabel;
    QR_DOC_NO: TQRLabel;
    QR_DOC_AMTC: TQRLabel;
    QR_DOC_AMT: TQRLabel;
    QRLabel18: TQRLabel;
    QR_LOADDATE: TQRLabel;
    QRLabel51: TQRLabel;
    QR_EXPDATE: TQRLabel;
    QRLabel57: TQRLabel;
    QR_ISBANK1: TQRLabel;
    QR_ISBANK2: TQRLabel;
    CHILD_EXPORT2: TQRChildBand;
    QRLabel58: TQRLabel;
    QRM_EXGOOD1: TQRMemo;
    QR_AP_BANK1: TQRLabel;
    QR_AP_BANK2: TQRLabel;
    QR_ISS_DATE: TQRLabel;
    QR_LC_NO: TQRLabel;
    QR_APPADDR1: TQRLabel;
    QR_APPADDR2: TQRLabel;
    QR_APPLIC3: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_BNFADDR1: TQRLabel;
    QR_BNFADDR2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_LOC_TYPENAME: TQRLabel;
    QR_LOC1AMTC: TQRLabel;
    QR_CD_PER: TQRLabel;
    QR_EX_RATE: TQRLabel;
    QR_LOC1AMT: TQRLabel;
    QR_LOC2AMTC: TQRLabel;
    QR_LOC2AMT: TQRLabel;
    QR_OFFERNO7: TQRLabel;
    QR_OFFERNO6: TQRLabel;
    QR_OFFERNO9: TQRLabel;
    QR_OFFERNO8: TQRLabel;
    QR_OFFERNO5: TQRLabel;
    QR_OFFERNO2: TQRLabel;
    QR_OFFERNO1: TQRLabel;
    QR_OFFERNO4: TQRLabel;
    QR_OFFERNO3: TQRLabel;
    QR_DELIVERY: TQRLabel;
    QR_EXPIRY: TQRLabel;
    CHILD_ETC2: TQRChildBand;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QR_TRANSPRTNAME: TQRLabel;
    QR_DOC_PRD: TQRLabel;
    QR_BUSINESSNAME: TQRLabel;
    QR_OPEN_NO: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QR_DOC_DTL: TQRLabel;
    procedure PageFooterBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FFields : TFields;
    function getValueFromField(QRLabel : TQRLabel;Format : String=''):String;
  public
    procedure PrintDocument(Fields : TFields;uPreview : Boolean = True);
    procedure ReadOnlyDocument(Fields : TFields);    
  end;

var
  LOCADV_PRINT_frm: TLOCADV_PRINT_frm;

implementation

uses MSSQL, Commonlib;

{$R *.DFM}

{ TLOCADV_PRINT_frm }

function TLOCADV_PRINT_frm.getValueFromField(QRLabel: TQRLabel;
  Format: String): String;
var
  sFieldName : string;
begin
  sFieldName := AnsiReplaceText(UpperCase(QRLabel.Name),'QR_','');
  Result := FFields.FieldByName(sFieldName).AsString;

  CASE AnsiIndexText(Format,['YYYY-MM-DD','YYYYMMDD','YYYY.MM.DD','#,0.####','#,0']) OF
    0: Result := FormatDateTime('YYYY-MM-DD',ConvertStr2Date(FFields.FieldByName(sFieldName).AsString));
    1: Result := FormatDateTime('YYYYMMDD',ConvertStr2Date(FFields.FieldByName(sFieldName).AsString));
    2: Result := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FFields.FieldByName(sFieldName).AsString));
    3: Result := FormatFloat('#,0.####',FFields.FieldByName(sFieldName).AsCurrency);
    4: Result := FormatFloat('#,0',FFields.FieldByName(sFieldName).AsCurrency);
  end;
end;

procedure TLOCADV_PRINT_frm.PrintDocument(Fields: TFields;
  uPreview: Boolean);
begin

  ReadOnlyDocument(Fields);

  Self.Prepare;
  IF uPreview Then Self.Preview
  else
  begin
    Self.PrinterSetup;
//------------------------------------------------------------------------------
// 프린트 셋업이후 OK면 0 CANCEL이면 1이 리턴됨
//------------------------------------------------------------------------------
    IF Self.Tag = 0 Then Self.Print;
  end;

end;

procedure TLOCADV_PRINT_frm.PageFooterBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  PageFooterBand1.AlignToBottom := Self.PageNumber = 1;
end;

procedure TLOCADV_PRINT_frm.ReadOnlyDocument(Fields: TFields);
var
  i : Integer;
  nIndex : Integer;
  TempStr : String;
begin
  FFields := Fields;
  with Fields do
  begin
  //------------------------------------------------------------------------------
  // PAGE HEADER SECTION
  //------------------------------------------------------------------------------
    //문서번호
    QRLabel8.Caption := FieldByName('MAINT_NO').AsString;
    //통지일자
    QRLabel7.Caption := '통지일자 : '+FormatDateTime('YYYY-MM-DD',ConvertStr2Date(FieldByName('ADV_DATE').AsString));
  //------------------------------------------------------------------------------
  // CHILD_HEADER SECTION
  //------------------------------------------------------------------------------
    CHILD_HEADER.Height := CHILD_HEADER.Tag;
    //개설은행
    QR_AP_BANK1.Caption := getValueFromField(QR_AP_BANK1);
    QR_AP_BANK2.Caption := getValueFromField(QR_AP_BANK2);
    //개설일자
    QR_ISS_DATE.Caption := getValueFromField(QR_ISS_DATE);
    //신용장번호
    QR_LC_NO.Caption := getValueFromField(QR_LC_NO);
  //------------------------------------------------------------------------------
  // CHILD_CREATEUSER SECTION
  //------------------------------------------------------------------------------
    CHILD_CREATEUSER.Height := CHILD_CREATEUSER.Tag;
    //개설자상호
    QR_APPLIC1.Caption := getValueFromField(QR_APPLIC1);
    //개설자대표자
    QR_APPLIC2.Caption := getValueFromField(QR_APPLIC2);
    //개설자사업자등록번호
    QR_APPLIC3.Caption := getValueFromField(QR_APPLIC3);
    //개설자 주소1
    QR_APPADDR1.Caption := getValueFromField(QR_APPADDR1);
    //개설자 주소2
    QR_APPADDR2.Caption := getValueFromField(QR_APPADDR2);
    IF Trim(QR_APPADDR2.Caption) = '' Then
    begin
      QR_APPADDR2.Enabled := False;
      CHILD_CREATEUSER.Height := CHILD_CREATEUSER.Height-15;
    end;
  //------------------------------------------------------------------------------
  // CHILD_RECVUSER SECTION
  //------------------------------------------------------------------------------
    CHILD_RECVUSER.Height := CHILD_RECVUSER.Tag;
    //수혜자 상호
    QR_BENEFC1.Caption := getValueFromField(QR_BENEFC1);
    //수혜자 대표
    QR_BENEFC2.Caption := getValueFromField(QR_BENEFC2);
    //사업자등록번호 식별자
//    QR_BENEFC3.Caption := getValueFromField(QR_BENEFC3);
    QR_BENEFC3.Caption := LeftStr( FieldByName('BENEFC3').AsString , 10 )+' '+MidStr(FieldByName('BENEFC3').AsString,11,100);
    //주소1
    QR_BNFADDR1.Caption := getValueFromField(QR_BNFADDR1);
    //주소2
    QR_BNFADDR2.Caption := getValueFromField(QR_BNFADDR2);
    IF Trim(QR_BNFADDR2.Caption) = '' Then
    begin
      QR_BNFADDR2.Enabled := False;
      CHILD_RECVUSER.Height := CHILD_RECVUSER.Height-15;
    end;
  //------------------------------------------------------------------------------
  // CHILD_ETC1 SECTION
  //------------------------------------------------------------------------------
    CHILD_ETC1.Height := CHILD_ETC1.Tag;
    //내국신용장 종류
    QR_LOC_TYPENAME.Caption := getValueFromField(QR_LOC_TYPENAME);
    //개설외화금액
    QR_LOC1AMTC.Caption := getValueFromField(QR_LOC1AMTC);
    QR_LOC1AMT.Caption := getValueFromField(QR_LOC1AMT,'#,0.####');
    //개설원화금액
    QR_LOC2AMTC.Caption := getValueFromField(QR_LOC2AMTC);
    QR_LOC2AMT.Caption := getValueFromField(QR_LOC2AMT,'#,0.####');
    //허용오차
    QR_CD_PER.Caption := '(±) '+FieldByName('CD_PERP').AsString+' / '+FieldByName('CD_PERM').AsString+' (%)';
    //매매 기준율
    QR_EX_RATE.Caption := getValueFromField(QR_EX_RATE);
  //------------------------------------------------------------------------------
  // CHILD_DOCSELL SECTION
  //------------------------------------------------------------------------------
    CHILD_DOCSELL.Height := CHILD_DOCSELL.Tag;
    //물품매도확약서
    nIndex := 1;
    for i := 1 to 9 do
    begin
      if Trim(FieldByName('OFFERNO'+IntToStr(i)).AsString) = '' Then Continue;

      (FindComponent('QR_OFFERNO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_OFFERNO'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('OFFERNO'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;
    CHILD_DOCSELL.Height := CHILD_DOCSELL.Tag - ((10-(nIndex))*15);
  //------------------------------------------------------------------------------
  // CHILD_DOCCOPY SECTION
  //------------------------------------------------------------------------------
    //물품인도기일
    QR_DELIVERY.Caption := getValueFromField(QR_DELIVERY,'YYYY-MM-DD');
    //유효기일
    QR_EXPIRY.Caption   := getValueFromField(QR_EXPIRY,'YYYY-MM-DD');

    //제출서류
    nIndex := 1;
    for i := 1 to 5 do
    begin
      Case i of
        1: TempStr := '물품수령증명서                     ';
        2: TempStr := '공급자발행 세금계산서 사본         ';
        3: TempStr := '물품명세가 기재된 송장             ';
        4: TempStr := '본 내국신용장의 사본               ';
        5: TempStr := '공급자발행 물품매도확약서 사본     ';
      end;

      IF AnsiMatchText( FieldByName('DOCCOPY'+IntToStr(i)).asString , ['','0'] ) Then Continue;

      (FindComponent('QR_DOCCOPY'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_DOCCOPY'+IntToStr(nIndex)) as TQRLabel).Caption := TempStr;
      (FindComponent('QR_DOCCOPY'+IntToStr(nIndex)+'_QTY') as TQRLabel).Enabled := True;
      (FindComponent('QR_DOCCOPY'+IntToStr(nIndex)+'_QTY') as TQRLabel).Caption := FieldByName('DOCCOPY'+IntToStr(i)).AsString+' 통';
      Inc(nIndex);
    end;
  //------------------------------------------------------------------------------
  // CHILD_ETC_DOC SECTION
  //------------------------------------------------------------------------------
    //기타구비서류
    QRM_DOC_ETC1.Lines.Text := FieldByName('DOC_ETC1').AsString;
    IF QRM_DOC_ETC1.Lines.Count > 0 Then
    begin
      QRM_DOC_ETC1.Enabled := True;
      QRM_DOC_ETC1.Height := 13+((QRM_DOC_ETC1.Lines.Count-1)*12);
      //------------------------------------------------------------------------------
      // 섹션 높이 변경
      //------------------------------------------------------------------------------
      CHILD_ETC_DOC.Height := QRM_DOC_ETC1.Height + 1;
    end;
  //------------------------------------------------------------------------------
  // CHILD_REMARK1 SECTION
  //------------------------------------------------------------------------------
    //기타 정보
    QRM_REMARK1.Lines.Text := FieldByName('REMARK1').AsString;
    IF QRM_REMARK1.Lines.Count > 0 Then
    begin
      CHILD_REMARK1.Enabled := True;
      QRM_REMARK1.Height := 13+((QRM_REMARK1.Lines.Count-1)*12);
      //------------------------------------------------------------------------------
      // 섹션 높이 변경
      //------------------------------------------------------------------------------
      CHILD_REMARK1.Height := QRM_REMARK1.Height + 1;
    end;
  //------------------------------------------------------------------------------
  // ChildBand7 SECTION
  //------------------------------------------------------------------------------
    ChildBand7.Height := ChildBand7.Tag;
  //------------------------------------------------------------------------------
  // CHILD_GOODS
  //------------------------------------------------------------------------------
    //대표공급물품명
    QR_BSN_HSCODE.Caption := '(HS 부호 : '+getValueFromField(QR_BSN_HSCODE)+' )';
    //------------------------------------------------------------------------------
    // 공급물품명
    //------------------------------------------------------------------------------
    QRM_GOODS.Lines.Text := FieldByName('GOODDES1').AsString;
    QRM_GOODS.Lines.Text := TrimMemo(QRM_GOODS.lines);
    QRM_GOODS.Height := 13+((QRM_GOODS.Lines.Count-1)*12);
    //------------------------------------------------------------------------------
    // 섹션 높이 변경
    //------------------------------------------------------------------------------
    CHILD_GOODS.Height := (QR_BSN_HSCODE.Height+1) + (QRM_GOODS.Height+1);
  //------------------------------------------------------------------------------
  // CHILD_ETC2
  //------------------------------------------------------------------------------
    CHILD_ETC2.Height := CHILD_ETC2.Tag;
    //분할인도 허용여부
    QR_TRANSPRTNAME.Caption := getValueFromField(QR_TRANSPRTNAME);
    //서류제시기간
    QR_DOC_PRD.Caption := getValueFromField(QR_DOC_PRD);
    //개설근거별 용도
    QR_BUSINESSNAME.Caption := getValueFromField(QR_BUSINESSNAME);
    //기타
    QR_OPEN_NO.Caption := getValueFromField(QR_OPEN_NO) + ' 차 내국신용장';
  //------------------------------------------------------------------------------
  // CHILD_EXPORT1
  //------------------------------------------------------------------------------
    IF Trim(FieldByName('DOC_NO').AsString) <> '' Then
    begin
      CHILD_EXPORT1.Enabled := True;
      QR_DOC_DTL.Enabled := Trim(getValueFromField(QR_DOC_DTL)) <> '';
      QR_DOC_DTL.Caption := '['+getValueFromField(QR_DOC_DTL)+'] ' + FFields.FieldByName('DOC_DTLNAME').AsString;
      QR_DOC_NO.Caption  := getValueFromField(QR_DOC_NO);

      QR_DOC_AMTC.Caption := getValueFromField(QR_DOC_AMTC);
      QR_DOC_AMT.Caption := getValueFromField(QR_DOC_AMT,'#,0.####');
      QR_DOC_AMTC.Enabled := Trim(QR_DOC_AMTC.Caption) <> '';
      QR_DOC_AMT.Enabled := QR_DOC_AMTC.Enabled;
      QRLabel23.Enabled := QR_DOC_AMTC.Enabled;

      QR_LOADDATE.Caption := getValueFromField(QR_LOADDATE,'YYYY-MM-DD');
      QR_EXPDATE.Caption := getValueFromField(QR_EXPDATE,'YYYY-MM-DD');
      QR_ISBANK1.Caption := getValueFromField(QR_ISBANK1);
      QR_ISBANK2.Caption := getValueFromField(QR_ISBANK2);
    end;
  //------------------------------------------------------------------------------
  // CHILD_EXPORT2
  //------------------------------------------------------------------------------
//    CHILD_EXPORT2.Enabled := CHILD_EXPORT1.Enabled;
    //기타 정보
    QRM_EXGOOD1.Lines.Text := FieldByName('EXGOOD1').AsString;
    CHILD_EXPORT2.Enabled := Trim(QRM_EXGOOD1.Lines.Text) <> '';
    IF QRM_EXGOOD1.Lines.Count > 0 Then
    begin
      CHILD_EXPORT2.Enabled := CHILD_EXPORT1.Enabled AND True;
      QRM_EXGOOD1.Height := 13+((QRM_EXGOOD1.Lines.Count-1)*12);
      //------------------------------------------------------------------------------
      // 섹션 높이 변경
      //------------------------------------------------------------------------------
      CHILD_EXPORT2.Height := QRM_EXGOOD1.Height + 1;
    end;
//------------------------------------------------------------------------------
// SIGN SECTION
//------------------------------------------------------------------------------
    QR_EXNAME1.Caption := getValueFromField(QR_EXNAME1);
    QR_EXNAME2.Caption := getValueFromField(QR_EXNAME2);
    QR_EXNAME3.Caption := getValueFromField(QR_EXNAME3);
  end;
end;

end.
