unit LOCRC2_PRINT2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TLOCRC2_PRINT2_frm = class(TPRINT_PARENT_QR)
    DetailBand1: TQRBand;
    QR_BILLNO: TQRLabel;
    QR_BILL_DATE: TQRLabel;                                   
    QR_TAX_AMOUNT: TQRLabel;
    SummaryBand1: TQRBand;
    ChildBand1: TQRChildBand;
    QRLabel1: TQRLabel;
    QRImage2: TQRImage;
    QRLabel2: TQRLabel;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QR_BILL_AMOUNT: TQRLabel;
    qryTax: TADOQuery;
    qryTaxSEQ: TBCDField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    qryTaxKEYY: TStringField;
    QRBand1: TQRBand;
    QRLabel124: TQRLabel;
    QRImage3: TQRImage;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRImage1: TQRImage;
    PageFooterBand1: TQRBand;
    QRLabel14: TQRLabel;
    ChildBand2: TQRChildBand;
    QRShape2: TQRShape;
    QRLabel13: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel26: TQRLabel;
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);

  private
    FDocNo : String;
    procedure OPENDB;
  public
    { Public declarations }
    property  DocNo:string  read FDocNo write FDocNo;
  end;

var
  LOCRC2_PRINT2_frm: TLOCRC2_PRINT2_frm;

implementation

{$R *.dfm}

uses Commonlib, MSSQL;

procedure TLOCRC2_PRINT2_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
    //승인번호(세금계산서번호)
    QR_BILLNO.Caption := qryTaxBILL_NO.AsString;

    //작성일
    QR_BILL_DATE.Caption := FormatDateTime('YYYY.MM.DD', ConvertStr2Date(qryTaxBILL_DATE.AsString));

    //공급가액
    QR_BILL_AMOUNT.Caption :=  FormatFloat('#,##0.####', qryTaxBILL_AMOUNT.AsFloat);

    //세액
    QR_TAX_AMOUNT.Caption := FormatFloat('#,##0.####', qryTaxTAX_AMOUNT.AsFloat);

    qryTax.Next;

  //  DetailBand1.Height := (qryTax.RecordCount * 15) + 30;
end;

procedure TLOCRC2_PRINT2_frm.OPENDB;
begin
  qryTax.Close;
  qryTax.Parameters.ParamByName('MAINT_NO').Value := FDocNo;
  qryTax.Open;
end;

procedure TLOCRC2_PRINT2_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  //MoreData의 값이 true이면 실행
  //MoreData := not qryTax.Eof;
end;

procedure TLOCRC2_PRINT2_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

end.
