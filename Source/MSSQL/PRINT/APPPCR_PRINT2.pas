unit APPPCR_PRINT2;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils;

type
  TAPPPCR_PRINT2_FRM = class(TQuickRep)
    ColumnHeaderBand1: TQRBand;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel1: TQRLabel;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    qryMst: TADOQuery;
    qryMstKEYY: TStringField;
    qryMstSEQ: TIntegerField;
    qryMstDATEE: TStringField;
    qryMstBILL_NO: TStringField;
    qryMstBILL_DATE: TStringField;
    qryMstITEM_NAME1: TStringField;
    qryMstITEM_DESC: TMemoField;
    qryMstBILL_AMOUNT: TBCDField;
    qryMstBILL_AMOUNT_UNIT: TStringField;
    qryMstTAX_AMOUNT: TBCDField;
    qryMstTAX_AMOUNT_UNIT: TStringField;
    qryMstQUANTITY: TBCDField;
    qryMstQUANTITY_UNIT: TStringField;
    SummaryBand1: TQRBand;
    QRShape3: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRShape4: TQRShape;
    ChildBand1: TQRChildBand;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    qryInfo: TADOQuery;
    qryInfoAPP_NAME2: TStringField;
    qryInfoAX_NAME3: TStringField;
    qryInfoDATEE: TStringField;
    ChildBand2: TQRChildBand;
    PageFooterBand1: TQRBand;
    QRShape5: TQRShape;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    procedure qryMstBILL_AMOUNTGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryMstTAX_AMOUNTGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryMstQUANTITYGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure PageFooterBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FDocNo : String;
    Procedure OPENDB;
  public
    TotalPage :Integer;
    property DocNo:String  read FDocNo write FDocNo;
  end;

var
  APPPCR_PRINT2_FRM: TAPPPCR_PRINT2_FRM;

implementation

uses MSSQL;

{$R *.DFM}

procedure TAPPPCR_PRINT2_FRM.qryMstBILL_AMOUNTGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  IF qryMstBILL_AMOUNT_UNIT.AsString = 'KRW' THEN
    Text := qryMstBILL_AMOUNT_UNIT.AsString + ' ' + FormatFloat('#,0',qryMstBILL_AMOUNT.AsCurrency)
  else
    Text := qryMstBILL_AMOUNT_UNIT.AsString + ' ' + FormatFloat('#,0.000',qryMstBILL_AMOUNT.AsCurrency);
end;

procedure TAPPPCR_PRINT2_FRM.qryMstTAX_AMOUNTGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  IF qryMstTAX_AMOUNT_UNIT.AsString = 'KRW' THEN
    Text := qryMstTAX_AMOUNT_UNIT.AsString + ' ' + FormatFloat('#,0',qryMstTAX_AMOUNT.AsCurrency)
  else
    Text := qryMstTAX_AMOUNT_UNIT.AsString + ' ' + FormatFloat('#,0.000',qryMstTAX_AMOUNT.AsCurrency);
end;

procedure TAPPPCR_PRINT2_FRM.qryMstQUANTITYGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  Text := qryMstQUANTITY_UNIT.AsString + ' ' + FormatFloat('#,0',qryMstQUANTITY.AsCurrency);
end;

procedure TAPPPCR_PRINT2_FRM.OPENDB;
begin
  with qryMst do
  begin
    Close;
    Parameters.ParamByName('KEYY').Value := FDocNo;
    Open;
  end;
end;

procedure TAPPPCR_PRINT2_FRM.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  OPENDB;
end;


procedure TAPPPCR_PRINT2_FRM.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  with qryInfo do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := FDocNo;
    Open;

    QRLabel6.Caption := '신청일자 :  '+ LeftStr(qryInfoDATEE.AsString,4)+'년 '+ MidStr(qryInfoDATEE.AsString,5,2) + '월 '+ RightStr(qryInfoDATEE.AsString,2)+ '일';
    QRLabel7.Caption := '신 청 자 :  '+ qryInfoAPP_NAME2.AsString;
    QRLabel8.Caption := '전자서명 :  '+ qryInfoAX_NAME3.AsString;

    Close;
  end;
end;

procedure TAPPPCR_PRINT2_FRM.PageFooterBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRLabel11.Caption := IntToStr(Self.PageNumber)+ ' / ' + IntToStr(TotalPage); 
end;

end.
