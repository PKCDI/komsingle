unit QR_INF707_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils, DateUtils;

type
  TDOCINFO = record
    MAINT_NO :String;
    MSEQ, AMD_NO : integer;
  end;
  
  TQR_INF707_PRN_frm = class(TQuickRep)
    TitleBand1: TQRBand;
    QRLabel39: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QR_CHILD_TOP: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel6: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC_TEL: TQRLabel;
    QR_AP_BANK: TQRLabel;
    QR_AP_BANK1: TQRLabel;
    QR_AP_BANK_TEL: TQRLabel;
    QR_AD_BANK: TQRLabel;
    QR_AD_BANK1: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QR_APPDATE: TQRLabel;
    QR_IN_MATHOD: TQRLabel;
    M_ADINFO: TQRMemo;
    QRLabel26: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QR_CHILD_SWIFT1: TQRChildBand;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListCD_NO: TStringField;
    qryListRCV_REF: TStringField;
    qryListIBANK_REF: TStringField;
    qryListISS_BANK1: TStringField;
    qryListISS_BANK2: TStringField;
    qryListISS_BANK3: TStringField;
    qryListISS_BANK4: TStringField;
    qryListISS_BANK5: TStringField;
    qryListISS_ACCNT: TStringField;
    qryListISS_DATE: TStringField;
    qryListAMD_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListPrno: TIntegerField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TBooleanField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListSR_INFO1: TStringField;
    qryListSR_INFO2: TStringField;
    qryListSR_INFO3: TStringField;
    qryListSR_INFO4: TStringField;
    qryListSR_INFO5: TStringField;
    qryListSR_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListOP_BANK1: TStringField;
    qryListOP_BANK2: TStringField;
    qryListOP_BANK3: TStringField;
    qryListOP_ADDR1: TStringField;
    qryListOP_ADDR2: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListAPP_RULE1: TStringField;
    qryListAPP_RULE2: TStringField;
    qryListREIM_BANK_BIC: TStringField;
    qryListREIM_BANK1: TStringField;
    qryListREIM_BANK2: TStringField;
    qryListREIM_BANK3: TStringField;
    qryListREIM_BANK4: TStringField;
    qryListAVT_BANK_BIC: TStringField;
    qryListAVT_BANK1: TStringField;
    qryListAVT_BANK2: TStringField;
    qryListAVT_BANK3: TStringField;
    qryListAVT_BANK4: TStringField;
    qryListIS_CANCEL: TStringField;
    qryListDOC_CD: TStringField;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    qryListCHARGE: TStringField;
    qryListCHARGE_1: TMemoField;
    qryListAMD_CHARGE: TStringField;
    qryListAMD_CHARGE_1: TMemoField;
    qryListSPECIAL_PAY: TMemoField;
    qryListGOODS_DESC: TMemoField;
    qryListDOC_REQ: TMemoField;
    qryListADD_CONDITION: TMemoField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListMIX1: TStringField;
    qryListMIX2: TStringField;
    qryListMIX3: TStringField;
    qryListMIX4: TStringField;
    qryListDEFPAY1: TStringField;
    qryListDEFPAY2: TStringField;
    qryListDEFPAY3: TStringField;
    qryListDEFPAY4: TStringField;
    qryListPERIOD_DAYS: TIntegerField;
    qryListPERIOD_IDX: TIntegerField;
    qryListPERIOD_DETAIL: TStringField;
    qryListCONFIRM: TStringField;
    qryListCONFIRM_BIC: TStringField;
    qryListCONFIRM1: TStringField;
    qryListCONFIRM2: TStringField;
    qryListCONFIRM3: TStringField;
    qryListCONFIRM4: TStringField;
    qryListTXT_78: TMemoField;
    qryListAPPLIC_CHG1: TStringField;
    qryListAPPLIC_CHG2: TStringField;
    qryListAPPLIC_CHG3: TStringField;
    qryListAPPLIC_CHG4: TStringField;
    qryListmathod_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListCDMAX_Name: TStringField;
    qryListDOC_CDNM: TStringField;
    QRImage2: TQRImage;
    QRLabel10: TQRLabel;
    QRLabel17: TQRLabel;
    QR_CDNO: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QR_RECVRFF: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QR_ISSREF: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QR_ISSBANK: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QR_ISSDATE: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QR_AMDNO: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QR_AMDDATE: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QR_CANCEL: TQRChildBand;
    QRLabel18: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel31: TQRLabel;
    QR_CHILD_40A: TQRChildBand;
    QRLabel35: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel51: TQRLabel;
    QR_CHILD_40E: TQRChildBand;
    QRLABEL: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QR_CHILD_31D: TQRChildBand;
    QRLabel52: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QR_CHILD_50: TQRChildBand;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QR_CHILD_59: TQRChildBand;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QR_BENE1: TQRLabel;
    QR_BENE2: TQRLabel;
    QR_CHILD_32B: TQRChildBand;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QR_CHILD_33B: TQRChildBand;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QR_CHILD_39A: TQRChildBand;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QR_BENE3: TQRLabel;
    QR_CHILD_39C: TQRChildBand;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel71: TQRLabel;
    QR_CHILD_41a: TQRChildBand;
    QRLabel72: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel91: TQRLabel;
    QR_CHILD_42C: TQRChildBand;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QR_CHILD_42a: TQRChildBand;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QR_CHILD_42M: TQRChildBand;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QR_CHILD_42P: TQRChildBand;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QR_CHILD_43P: TQRChildBand;
    QR_CHILD_43T: TQRChildBand;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QR_CHILD_44A: TQRChildBand;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QR_CHILD_44E: TQRChildBand;
    QRLabel121: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel128: TQRLabel;
    QR_CHILD_44F: TQRChildBand;
    QRLabel127: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QR_CHILD_44B: TQRChildBand;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QR_CHILD_44C: TQRChildBand;
    QRLabel139: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel140: TQRLabel;
    QR_CHILD_44D: TQRChildBand;
    QRLabel143: TQRLabel;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel146: TQRLabel;
    QR_CHILD_45B: TQRChildBand;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    M_45B: TQRMemo;
    QR_CHILD_46B: TQRChildBand;
    QRLabel149: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    M_46B: TQRMemo;
    QR_CHILD_47B: TQRChildBand;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel156: TQRLabel;
    M_47B: TQRMemo;
    QR_CHILD_49M: TQRChildBand;
    QRLabel157: TQRLabel;
    QRLabel158: TQRLabel;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    M_49M: TQRMemo;
    QR_CHILD_71D: TQRChildBand;
    QRLabel161: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel163: TQRLabel;
    M_71D: TQRMemo;
    QR_CHILD_71N: TQRChildBand;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    M_71N: TQRMemo;
    QR_CHILD_48: TQRChildBand;
    QRLabel167: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel169: TQRLabel;
    QR_48: TQRLabel;
    QR_CHILD_49: TQRChildBand;
    QRLabel170: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QR_49: TQRLabel;
    QR_CHILD_58a: TQRChildBand;
    QRLabel173: TQRLabel;
    QRLabel174: TQRLabel;
    QRLabel175: TQRLabel;
    M_58a: TQRMemo;
    QR_CHILD_53a: TQRChildBand;
    QRLabel176: TQRLabel;
    QRLabel177: TQRLabel;
    QRLabel178: TQRLabel;
    M_53a: TQRMemo;
    QR_CHILD_78: TQRChildBand;
    QRLabel179: TQRLabel;
    QRLabel180: TQRLabel;
    QRLabel181: TQRLabel;
    QRLabel182: TQRLabel;
    M_78: TQRMemo;
    QRLabel183: TQRLabel;
    QR_CHILD_57a: TQRChildBand;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    QRLabel186: TQRLabel;
    M_57a: TQRMemo;
    QR_CHILD_72Z: TQRChildBand;
    QRLabel187: TQRLabel;
    QRLabel188: TQRLabel;
    QRLabel190: TQRLabel;
    M_72Z: TQRMemo;
    QR_CHILD_SIGN: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel189: TQRLabel;
    QRShape9: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRLabel191: TQRLabel;
    QRLabel192: TQRLabel;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabel195: TQRLabel;
    QRLabel196: TQRLabel;
    QRLabel197: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel199: TQRLabel;
    QRShape20: TQRShape;
    QRLabel200: TQRLabel;
    QRLabel201: TQRLabel;
    QRLabel202: TQRLabel;
    QRLabel203: TQRLabel;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRLabel205: TQRLabel;
    QRLabel206: TQRLabel;
    QRLabel207: TQRLabel;
    QRLabel208: TQRLabel;
    QRLabel209: TQRLabel;
    QRLabel210: TQRLabel;
    QRLabel211: TQRLabel;
    QRLabel212: TQRLabel;
    QRLabel213: TQRLabel;
    QRShape25: TQRShape;
    QRLabel214: TQRLabel;
    QRLabel215: TQRLabel;
    QRLabel216: TQRLabel;
    QRLabel217: TQRLabel;
    QR_CHILD_BANKSIGN: TQRChildBand;
    QRLabel204: TQRLabel;
    QRLabel218: TQRLabel;
    QRShape26: TQRShape;
    procedure QR_CHILD_TOPBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QR_CHILD_SWIFT1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CANCELBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_40ABeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_40EBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_31DBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_50BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_59BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_32BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_33BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_39ABeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_39CBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_42CBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_42MBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_42PBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_43PBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_43TBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44ABeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44EBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44FBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44CBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44DBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_45BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_46BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_47BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_49MBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_71DBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_71NBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_48BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_49BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_58aBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_53aBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_78BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_57aBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_72ZBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_SIGNBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_BANKSIGNBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FDOCINFO : TDOCINFO;
    function getAMD_NO: Integer;
    function getMAINT_NO: string;
    function getMSEQ: Integer;
    procedure setAMD_NO(const Value: Integer);
    procedure setMAINT_NO(const Value: string);
    procedure setMSEQ(const Value: Integer);
  public
    property MAINT_NO : string read getMAINT_NO write setMAINT_NO;
    property MSEQ : Integer read getMSEQ write setMSEQ;
    property AMD_NO : Integer read getAMD_NO write setAMD_NO;
  end;

var
  QR_INF707_PRN_frm: TQR_INF707_PRN_frm;

implementation

uses
  MSSQL;

{$R *.DFM}

function TQR_INF707_PRN_frm.getAMD_NO: Integer;
begin
  Result := FDOCINFO.AMD_NO;
end;

function TQR_INF707_PRN_frm.getMAINT_NO: string;
begin
  Result := FDOCINFO.MAINT_NO;
end;

function TQR_INF707_PRN_frm.getMSEQ: Integer;
begin
  Result := FDOCINFO.MSEQ;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_TOPBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_CHILD_TOP.Height := 348;
  //신청문서번호
  QR_MAINTNO.Caption := qryListMAINT_NO.AsString;
  //조건변경신청일자
  QR_APPDATE.Caption := qryListAPP_DATE.AsString;
  //개설방법
  QR_IN_MATHOD.Caption := qryListmathod_Name.AsString;
//------------------------------------------------------------------------------
// 개설의뢰인
//------------------------------------------------------------------------------
  //상호
  QR_APPLIC1.Caption := qryListAPPLIC1.AsString;
  //주소
  QR_APPLIC2.Caption := Trim(qryListAPPLIC2.AsString+' '+qryListAPPLIC3.AsString+' '+qryListAPPLIC4.AsString);
  //전화번호
  QR_APPLIC_TEL.Caption := qryListAPPLIC5.AsString;
//------------------------------------------------------------------------------
// SWIFT 전문발신은행
//------------------------------------------------------------------------------
  QR_AP_BANK.Caption  := qryListAP_BANK.AsString;
  QR_AP_BANK1.Caption := Trim(Trim(Trim(qryListAP_BANK1.AsString) +' '+ Trim(qryListAP_BANK2.AsString))+' '+Trim(Trim(qryListAP_BANK3.AsString)+' '+Trim(qryListAP_BANK4.AsString)));
  QR_AP_BANK_TEL.Caption := Trim(qryListAP_BANK5.AsString);
//------------------------------------------------------------------------------
// SWIFT 전문수신은행
//------------------------------------------------------------------------------
  QR_AD_BANK.Caption := qryListAD_BANK.AsString;
  QR_AD_BANK1.Caption := Trim(Trim(Trim(qryListAD_BANK1.AsString)+' '+Trim(qryListAD_BANK2.AsString))+' '+Trim(Trim(qryListAD_BANK3.AsString)+' '+Trim(qryListAD_BANK4.AsString)));
//------------------------------------------------------------------------------
// 기타정보
//------------------------------------------------------------------------------
  M_ADINFO.Lines.Clear;
  IF Trim(qryListAD_INFO1.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO1.AsString));
  IF Trim(qryListAD_INFO2.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO2.AsString));
  IF Trim(qryListAD_INFO3.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO3.AsString));
  IF Trim(qryListAD_INFO4.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO4.AsString));
  IF Trim(qryListAD_INFO5.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO5.AsString));

  M_ADINFO.Height := 12 * M_ADINFO.Lines.Count;

end;

procedure TQR_INF707_PRN_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FDOCINFO.MAINT_NO;
  qryList.Parameters.ParamByName('MSEQ'    ).Value := FDOCINFO.MSEQ;
  qryList.Parameters.ParamByName('AMD_NO'  ).Value := FDOCINFO.AMD_NO;
  qryList.Open;
end;

procedure TQR_INF707_PRN_frm.setAMD_NO(const Value: Integer);
begin
  FDOCINFO.AMD_NO := Value;
end;

procedure TQR_INF707_PRN_frm.setMAINT_NO(const Value: string);
begin
  FDOCINFO.MAINT_NO := Value;
end;

procedure TQR_INF707_PRN_frm.setMSEQ(const Value: Integer);
begin
  FDOCINFO.MSEQ := Value;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_SWIFT1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QR_CHILD_SWIFT1.Height := 207;
  //발신은행 참조사항
  QR_CDNO.Caption := qryListCD_NO.AsString;
  //수신은행 참조사항
  QR_RECVRFF.Caption := qryListRCV_REF.AsString;
  //개설은행 참조사항
  QR_ISSREF.Caption := qryListIBANK_REF.AsString;
  //개설은행
  QR_ISSBANK.Caption := qryListAP_BANK.AsString;
  //개설일자
  QR_ISSDATE.Caption := RightStr(qryListISS_DATE.AsString,6);
  //조건변경차수
  QR_AMDNO.Caption := qryListAMD_NO.AsString;
  //조건변경 신청일
  QR_AMDDATE.Caption := RightStr(qryListAMD_DATE.AsString,6);

end;

procedure TQR_INF707_PRN_frm.QR_CANCELBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled := qryListIS_CANCEL.AsString <> '';
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_40ABeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled    := qryListDOC_CD.AsString <>'';
  QRLabel51.Caption := qryListDOC_CDNM.AsString;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_40EBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled := qryListAPP_RULE1.AsString <> '';
  QRLabel55.Caption := Trim( Trim(qryListAPP_RULE1.AsString)+' '+Trim(qryListAPP_RULE2.AsString) );
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_31DBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled := Trim(qryListEX_DATE.AsString) <> '';
  QRLabel58.Caption := Trim( RightStr( Trim(qryListEX_DATE.AsString) , 6 ) + ' ' + Trim(qryListEX_PLACE.AsString) );
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_50BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled := qryListAPPLIC_CHG1.AsString <> '';
  QRLabel66.Caption := Trim((qryListAPPLIC_CHG1.AsString) + ' ' + Trim(qryListAPPLIC_CHG2.AsString));
  QRLabel67.Caption := Trim((qryListAPPLIC_CHG3.AsString) + ' ' + Trim(qryListAPPLIC_CHG4.AsString));
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_59BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 55;
  Sender.Enabled := qryListBENEFC1.AsString <> '';
  QR_BENE1.Caption := Trim((qryListBENEFC1.AsString) + ' ' + Trim(qryListBENEFC2.AsString));
  QR_BENE2.Caption := Trim((qryListBENEFC3.AsString) + ' ' + Trim(qryListBENEFC4.AsString));
  QR_BENE3.Caption := Trim(qryListBENEFC5.AsString);
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_32BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListINCD_CUR.AsString) <> '';
  QRLabel76.Caption := Trim(qryListINCD_CUR.AsString) + ' ' + FormatFloat('#,0.00', qryListINCD_AMT.AsCurrency);
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_33BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListDECD_CUR.AsString) <> '';
  QRLabel76.Caption := Trim(qryListDECD_CUR.AsString) + ' ' + FormatFloat('#,0.00', qryListDECD_AMT.AsCurrency);
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_39ABeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled := (qryListCD_PERP.AsInteger > 0) OR (qryListCD_PERM.AsInteger > 0);
  QRLabel86.Caption := IntToStr( qryListCD_PERP.AsInteger ) + ' / ' + IntToStr( qryListCD_PERM.AsInteger );
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_39CBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListAA_CV1.AsString) <> '';
  QRLabel62.Caption := Trim(Trim(qryListAA_CV1.AsString) +' '+ Trim(qryListAA_CV2.AsString));
  QRLabel71.Caption := Trim(Trim(qryListAA_CV3.AsString) +' '+ Trim(qryListAA_CV4.AsString));
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_42CBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height     := 22;
  Sender.Enabled    := Trim(qryListDRAFT1.AsString) <> '';
  QRLabel95.Caption := Trim( Trim(qryListDRAFT1.AsString)+' '+Trim(qryListDRAFT2.AsString)+' '+Trim(qryListDRAFT3.AsString) );
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_42MBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListMIX1.AsString) <> '';
  QRLabel103.Caption := Trim(Trim(qryListMIX1.AsString)+' '+Trim(qryListMIX2.AsString));
  QRLabel104.Caption := Trim(Trim(qryListMIX3.AsString)+' '+Trim(qryListMIX4.AsString));
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_42PBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListDEFPAY1.AsString) <> '';
  QRLabel108.Caption := Trim( Trim(qryListDEFPAY1.AsString)+' '+Trim(qryListDEFPAY2.AsString) );
  QRLabel109.Caption := Trim( Trim(qryListDEFPAY3.AsString)+' '+Trim(qryListDEFPAY4.AsString) );
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_43PBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled := Trim(qryListPSHIP.AsString) <> '';
  Case AnsiIndexText( qryListPSHIP.AsString , ['9', '10'] ) of
    0: QRLabel115.Caption := 'ALLOWED';
    1: QRLabel115.Caption := 'NOT ALLOWED';
  else
    QRLabel115.Caption := '';
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_43TBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled := Trim(qryListTSHIP.AsString) <> '';  
  Case AnsiIndexText( qryListTSHIP.AsString , ['7', '8'] ) of
    0: QRLabel118.Caption := 'ALLOWED';
    1: QRLabel118.Caption := 'NOT ALLOWED';
  else
    QRLabel118.Caption := '';
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_44ABeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListLOAD_ON.AsString) <> '';
  QRLabel122.Caption := Trim(qryListLOAD_ON.AsString);
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_44EBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListSUNJUCK_PORT.AsString) <> '';
  QRLabel126.Caption := Trim(qryListSUNJUCK_PORT.AsString);
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_44FBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled     := Trim(qryListDOCHACK_PORT.AsString) <> '';
  QRLabel131.Caption := Trim(qryListDOCHACK_PORT.AsString);
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_44BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 56;
  Sender.Enabled     := Trim(qryListFOR_TRAN.AsString) <> '';
  QRLabel136.Caption := Trim(qryListFOR_TRAN.AsString);
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_44CBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled     := Trim(qryListLST_DATE.AsString) <> '';
  QRLabel142.Caption := RightStr(Trim(qryListLST_DATE.AsString),6);
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_44DBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : String;
begin
  Sender.Height := 22;
  TMP_STR := Trim(qryListSHIP_PD1.AsString)+' '+
             Trim(qryListSHIP_PD2.AsString)+' '+
             Trim(qryListSHIP_PD3.AsString)+' '+
             Trim(qryListSHIP_PD4.AsString)+' '+
             Trim(qryListSHIP_PD5.AsString)+' '+
             Trim(qryListSHIP_PD6.AsString);
  TMP_STR := Trim(TMP_STR);
  Sender.Enabled     := TMP_STR <> '';
  QRLabel145.Caption := TMP_STR;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_45BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListGOODS_DESC.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_45B.Lines.Clear;
    M_45B.Lines.Text := AnsiReplaceText( qryListGOODS_DESC.AsString , #13#10#13#10, '');
    M_45B.Height := (M_45B.Lines.Count * 12);

    IF M_45B.Lines.Count <= 2 Then
      Sender.Height := 40
    else
      Sender.Height := M_45B.Top + M_45B.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_46BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListDOC_REQ.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_46B.Lines.Clear;
    M_46B.Lines.Text := AnsiReplaceText( qryListDOC_REQ.AsString , #13#10#13#10, '');

    M_46B.Height := (M_46B.Lines.Count * 12);    
    IF M_46B.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_46B.Top + M_46B.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_47BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListADD_CONDITION.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_47B.Lines.Clear;
    M_47B.Lines.Text := AnsiReplaceText( qryListADD_CONDITION.AsString , #13#10#13#10, '');

    M_47B.Height := (M_47B.Lines.Count * 12);    
    IF M_47B.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_47B.Top + M_47B.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_49MBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListSPECIAL_PAY.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_49M.Lines.Clear;
    M_49M.Lines.Text := AnsiReplaceText( qryListSPECIAL_PAY.AsString , #13#10#13#10, '');
    M_49M.Height := (M_49M.Lines.Count * 12);

    IF M_49M.Lines.Count <= 2 Then
      Sender.Height := 40
    else
      Sender.Height := M_49M.Top + M_49M.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_71DBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListCHARGE.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_71D.Lines.Add('ALL BANKING COMMISSIONS AND CHARGES');
    M_71D.Lines.Add('INCLUDING REIMBURSEMENT CHARGES');
    M_71D.Lines.Add('OUTSIDE SOUTH KOREA ARE FOR ACCOUNT');
    case AnsiIndexText(UpperCase( Trim(qryListCHARGE.AsString) ), ['2AF', '2AG', '2AH']) of
      0: M_71D.Lines.Add('OF APPLICANT');
      1: M_71D.Lines.Add('OF BENEFICIARY');
      2:
      begin
        M_71D.Lines.Clear;
        M_71D.Lines.Text := AnsiReplaceText( qryListCHARGE_1.AsString , #13#10#13#10, '');
      end;
    end;

    M_71D.Height := (M_71D.Lines.Count * 12);
    IF M_71D.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_71D.Top + M_71D.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_71NBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListAMD_CHARGE.AsString <> '');
  IF Sender.Enabled Then
  begin
    case AnsiIndexText(UpperCase( Trim(qryListAMD_CHARGE.AsString) ), ['2AC', '2AD', '2AE']) of
      0: M_71N.Lines.Text := 'APPL';
      1: M_71N.Lines.Text := 'BENE';
      2:
      begin
        M_71N.Lines.Clear;
        M_71N.Lines.Text := AnsiReplaceText( qryListAMD_CHARGE_1.AsString , #13#10#13#10, '');
      end;
    end;

    M_71N.Height := (M_71N.Lines.Count * 12);
    IF M_71N.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_71N.Top + M_71N.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_48BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height  := 22;
  Sender.Enabled := Trim(qryListPERIOD_DAYS.AsString) <> '';
  QR_48.Caption  := Trim((qryListPERIOD_DAYS.AsString)+' '+Trim(qryListPERIOD_DETAIL.AsString));
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_49BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height  := 22;
  Sender.Enabled := Trim(qryListCONFIRM.AsString) <> '';
  QR_49.Caption := '';
  Case AnsiIndexText(qryListCONFIRM.AsString, ['DA','DB','DC']) of
    0: QR_49.Caption := 'WITHOUT';
    1: QR_49.Caption := 'MAY ADD';
    2: QR_49.Caption := 'CONFIRM';
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_58aBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  //확인은행
  Sender.Enabled := AnsiMatchText(qryListCONFIRM.AsString, ['DB', 'DC']);
  IF Sender.Enabled Then
  begin
    M_58a.Lines.Clear;
    IF Trim(qryListCONFIRM_BIC.AsString) <> '' Then
      M_58a.Lines.Add(qryListCONFIRM_BIC.AsString);
    IF Trim(qryListCONFIRM1.AsString) <> '' Then
      M_58a.Lines.Add(Trim((qryListCONFIRM1.AsString)+' '+Trim(qryListCONFIRM2.AsString)));
    IF Trim(qryListCONFIRM3.AsString) <> '' Then
      M_58a.Lines.Add(Trim((qryListCONFIRM3.AsString)+' '+Trim(qryListCONFIRM4.AsString)));

    M_58a.Height := (M_58a.Lines.Count * 12);
    IF M_58a.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_58a.Top + M_58a.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_53aBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  //상환은행
  Sender.Enabled := (Trim(qryListREIM_BANK_BIC.AsString) <> '') OR (Trim(qryListREIM_BANK1.AsString) <> '');
  IF Sender.Enabled Then
  begin
    M_53a.Lines.Clear;
    IF Trim(qryListREIM_BANK_BIC.AsString) <> '' Then
      M_53a.Lines.Add(qryListREIM_BANK_BIC.AsString);
    IF Trim(qryListREIM_BANK1.AsString) <> '' Then
      M_53a.Lines.Add(Trim((qryListREIM_BANK1.AsString)+' '+Trim(qryListREIM_BANK2.AsString)));
    IF Trim(qryListREIM_BANK3.AsString) <> '' Then
      M_53a.Lines.Add(Trim((qryListREIM_BANK3.AsString)+' '+Trim(qryListREIM_BANK4.AsString)));

    M_53a.Height := (M_53a.Lines.Count * 12);      
    IF M_53a.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_53a.Top + M_53a.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_78BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListTXT_78.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_78.Lines.Clear;
    M_78.Lines.Text := AnsiReplaceText( qryListTXT_78.AsString , #13#10#13#10, '');

    M_78.Height := (M_78.Lines.Count * 12);
    IF M_78.Lines.Count <= 3 Then
      Sender.Height := 56
    else
      Sender.Height := M_78.Top + M_78.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_57aBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  //최종통지은행
  Sender.Enabled := (Trim(qryListAVT_BANK_BIC.AsString) <> '') OR (Trim(qryListAVT_BANK1.AsString) <> '');
  IF Sender.Enabled Then
  begin
    M_57a.Lines.Clear;
    IF Trim(qryListAVT_BANK_BIC.AsString) <> '' Then
      M_57a.Lines.Add(qryListAVT_BANK_BIC.AsString);
    IF Trim(qryListAVT_BANK1.AsString) <> '' Then
      M_57a.Lines.Add(Trim((qryListAVT_BANK1.AsString)+' '+Trim(qryListAVT_BANK2.AsString)));
    IF Trim(qryListAVT_BANK3.AsString) <> '' Then
      M_57a.Lines.Add(Trim((qryListAVT_BANK3.AsString)+' '+Trim(qryListAVT_BANK4.AsString)));

    M_57a.Height := (M_57a.Lines.Count * 12);

    IF M_57a.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_57a.Top + M_57a.Height + 5;
  end;
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_72ZBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : String;
begin
  TMP_STR := Trim(qryListSHIP_PD1.AsString)+#13#10+
             Trim(qryListSHIP_PD2.AsString)+#13#10+
             Trim(qryListSHIP_PD3.AsString)+#13#10+
             Trim(qryListSHIP_PD4.AsString)+#13#10+
             Trim(qryListSHIP_PD5.AsString)+#13#10+
             Trim(qryListSHIP_PD6.AsString);
  TMP_STR := Trim(TMP_STR);
  Sender.Enabled := TMP_STR <> '';
  M_72Z.Lines.Text := TMP_STR;
  M_72Z.Height := (M_72Z.Lines.Count * 12);

  if M_72Z.Lines.Count = 1 Then
    Sender.Height := 22
  else
    Sender.Height := M_72Z.Top + M_72Z.Height + 5;
end;
procedure TQR_INF707_PRN_frm.QR_CHILD_SIGNBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  Sender.Height := 114;
  QRLabel200.Caption := qryListEX_NAME1.AsString;
  QRLabel201.Caption := qryListEX_NAME2.AsString;
  QRLabel202.Caption := qryListEX_NAME3.AsString;
  QRLabel203.Caption := Trim((qryListEX_ADDR1.AsString)+' '+Trim(qryListEX_ADDR2.AsString));
end;

procedure TQR_INF707_PRN_frm.QR_CHILD_BANKSIGNBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  Sender.Height := 114;
  QRLabel214.Caption := qryListOP_BANK1.AsString;
  QRLabel215.Caption := qryListOP_BANK2.AsString;
  QRLabel216.Caption := qryListOP_BANK3.AsString;
  QRLabel217.Caption := Trim((qryListOP_ADDR1.AsString)+' '+Trim(qryListOP_ADDR2.AsString));
end;

end.
