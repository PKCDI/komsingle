unit LOCAD1_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QuickRpt, QRCtrls, ExtCtrls, DB, ADODB;

type
  TLOCAD1_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel4: TQRLabel;
    QRLabel3: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QRLabel6: TQRLabel;
    QR_ADVDATE: TQRLabel;
    ChildBand2: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel124: TQRLabel;
    QRLabel8: TQRLabel;
    QR_APBANK1: TQRLabel;
    QR_APBANK2: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel11: TQRLabel;
    QR_ISSDATE: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel13: TQRLabel;
    QR_LCNO: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    ChildBand6: TQRChildBand;
    ChildBand7: TQRChildBand;
    ChildBand8: TQRChildBand;
    ChildBand9: TQRChildBand;
    ChildBand10: TQRChildBand;
    ChildBand11: TQRChildBand;
    ChildBand12: TQRChildBand;
    ChildBand13: TQRChildBand;
    ChildBand14: TQRChildBand;
    ChildBand15: TQRChildBand;
    ChildBand16: TQRChildBand;
    ChildBand17: TQRChildBand;
    ChildBand18: TQRChildBand;
    ChildBand19: TQRChildBand;
    ChildBand20: TQRChildBand;
    ChildBand21: TQRChildBand;
    ChildBand22: TQRChildBand;
    ChildBand23: TQRChildBand;
    ChildBand24: TQRChildBand;
    ChildBand25: TQRChildBand;
    QR_APPLIC3: TQRLabel;
    qryList: TADOQuery;
    SummaryBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRLabel14: TQRLabel;
    ChildBand28: TQRChildBand;
    ChildBand29: TQRChildBand;
    ChildBand30: TQRChildBand;
    QRShape1: TQRShape;
    QRLabel94: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel84: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QRImage1: TQRImage;
    QRLabel83: TQRLabel;
    QRLabel81: TQRLabel;
    QR_OPENNO: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel79: TQRLabel;
    QR_BUSINESSNAME: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QR_DOCPRD: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel73: TQRLabel;
    QR_BSNHSCODE: TQRLabel;
    QR_GOODDES1: TQRMemo;
    QRShape2: TQRShape;
    QRLabel72: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel68: TQRLabel;
    QR_REMARK1: TQRMemo;
    QRLabel67: TQRLabel;
    QR_DOCETC1: TQRMemo;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QR_DOCCOPYNAME5: TQRLabel;
    QR_DOCCOPY5: TQRLabel;
    QR_DOCCOPY4: TQRLabel;
    QR_DOCCOPY3: TQRLabel;
    QR_DOCCOPYNAME4: TQRLabel;
    QR_DOCCOPYNAME3: TQRLabel;
    QR_DOCCOPYNAME2: TQRLabel;
    QR_DOCCOPYNAME1: TQRLabel;
    QR_DOCCOPY1: TQRLabel;
    QR_DOCCOPY2: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel52: TQRLabel;
    QR_EXPIRY: TQRLabel;
    QRLabel50: TQRLabel;
    QR_DELIVERY: TQRLabel;
    QR_OFFERNO9: TQRLabel;
    QR_OFFERNO8: TQRLabel;
    QR_OFFERNO7: TQRLabel;
    QR_OFFERNO6: TQRLabel;
    QR_OFFERNO5: TQRLabel;
    QR_OFFERNO4: TQRLabel;
    QR_OFFERNO3: TQRLabel;
    QR_OFFERNO2: TQRLabel;
    QR_OFFERNO1: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel38: TQRLabel;
    QR_EXRATE: TQRLabel;
    QRLabel36: TQRLabel;
    QR_CDPERPM: TQRLabel;
    QRLabel34: TQRLabel;
    QR_LOC2AMT: TQRLabel;
    QRLabel32: TQRLabel;
    QR_LOC1AMT: TQRLabel;
    QRLabel30: TQRLabel;
    QR_LOCTYPENAME: TQRLabel;
    QR_BNFEMAILID: TQRLabel;
    QR_BNFADDR3: TQRLabel;
    QR_BNFADDR2: TQRLabel;
    QR_BNFADDR1: TQRLabel;
    QRLabel28: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    QR_APPADDR3: TQRLabel;
    QR_APPADDR2: TQRLabel;
    QR_APPADDR1: TQRLabel;
    qryListMAINT_NO: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListBGM_REF: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUSINESS: TStringField;
    qryListRFF_NO: TStringField;
    qryListLC_NO: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListOPEN_NO: TBCDField;
    qryListADV_DATE: TStringField;
    qryListISS_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListGOODDES: TStringField;
    qryListGOODDES1: TMemoField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListDOC_ETC: TStringField;
    qryListDOC_ETC1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC2AMTC: TStringField;
    qryListEX_RATE: TBCDField;
    qryListDOC_DTL: TStringField;
    qryListDOC_NO: TStringField;
    qryListDOC_AMT: TBCDField;
    qryListDOC_AMTC: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListIM_NAME: TStringField;
    qryListIM_NAME1: TStringField;
    qryListIM_NAME2: TStringField;
    qryListIM_NAME3: TStringField;
    qryListDEST: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListEXGOOD: TStringField;
    qryListEXGOOD1: TMemoField;
    qryListPRNO: TIntegerField;
    qryListNEGODT: TStringField;
    qryListNEGOAMT: TBCDField;
    qryListBSN_HSCODE: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListBUSINESSNAME: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListLOC_TYPENAME: TStringField;
    qryListDOC_DTLNAME: TStringField;
    qryListPAYMENTNAME: TStringField;
    qryListDESTNAME: TStringField;
    QRLabel18: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand14BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand15BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand17BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand18BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand20BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand21BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand23BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand24BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand25BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand28BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand29BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNO: string;
    procedure OPENDB;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
  end;

var
  LOCAD1_PRINT_frm: TLOCAD1_PRINT_frm;

implementation

{$R *.dfm}

uses MSSQL,Commonlib;

{ TLOCAD1_PRINT_frm }

procedure TLOCAD1_PRINT_frm.OPENDB;
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryList.Open;
end;

procedure TLOCAD1_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TLOCAD1_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand1-------------------------------------------------------
  ChildBand1.Height := ChildBand1.Tag;
  //개설신청서전자문서번호
  QR_MAINTNO.Caption := qryListMAINT_NO.AsString;
  //통지일자
  QR_ADVDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListADV_DATE.AsString));

end;

procedure TLOCAD1_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand2-------------------------------------------------------
  //개설은행
    nIndex := 1;
    for i := 1 to 2 do
    begin
      if Trim(qrylist.FieldByName('AP_BANK'+IntToStr(i)).AsString) = '' then
      begin
        (FindComponent('QR_APBANK'+IntToStr(i)) as TQRLabel).Caption := '';
        Continue;
      end;

      (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('AP_BANK'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    if qryListAP_BANK2.AsString = '' then
      ChildBand2.Height := 42
    else
      ChildBand2.Height := 56;
end;

procedure TLOCAD1_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand3-------------------------------------------------------
  ChildBand3.Height := ChildBand3.Tag;
  //개설일자
  QR_ISSDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListISS_DATE.AsString));
end;

procedure TLOCAD1_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand4-------------------------------------------------------
  ChildBand4.Height := ChildBand4.Tag;
  //신용장번호
  QR_LCNO.Caption := qryListLC_NO.AsString;
end;

procedure TLOCAD1_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand5-------------------------------------------------------
   //개설의뢰인
      nIndex := 1;
      for i := 1 to 3 do
      begin
        if Trim(qrylist.FieldByName('APPLIC'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_APPLIC'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('APPLIC'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      ChildBand5.Height := (15 * (nIndex-1));
end;

procedure TLOCAD1_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand6-------------------------------------------------------
   //개설의뢰인 주소
      nIndex := 1;
      for i := 1 to 3 do
      begin
        if Trim(qrylist.FieldByName('APPADDR'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_APPADDR'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_APPADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_APPADDR'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('APPADDR'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if (qryListAPPADDR1.AsString = '') and (qryListAPPADDR2.AsString = '') and (qryListAPPADDR3.AsString = '') then
        ChildBand6.Enabled := False
      else
        ChildBand6.Height := (15 * (nIndex-1));
end;

procedure TLOCAD1_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand7-------------------------------------------------------
   //수혜자
      nIndex := 1;
      for i := 1 to 3 do
      begin
        if Trim(qrylist.FieldByName('BENEFC'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_BNFADDR'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('BENEFC'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      ChildBand7.Height := (15 * (nIndex-1));
end;

procedure TLOCAD1_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
    //---------ChildBand8-------------------------------------------------------
   //수혜자 주소
      nIndex := 1;
      for i := 1 to 3 do
      begin
        if Trim(qrylist.FieldByName('BNFADDR'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_BNFADDR'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_BNFADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_BNFADDR'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('BNFADDR'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if (qryListBNFADDR1.AsString = '') and (qryListBNFADDR2.AsString = '') and (qryListBNFADDR3.AsString = '') then
        ChildBand8.Enabled := False
      else
        ChildBand8.Height := (15 * (nIndex-1));
end;

procedure TLOCAD1_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand9-------------------------------------------------------
  ChildBand9.Height := ChildBand9.Tag;
  //수혜자이메일
  QR_BNFEMAILID.Caption := qryListBNFEMAILID.AsString + '@' + qryListBNFDOMAIN.AsString;

  if (qryListBNFEMAILID.AsString = '') and (qryListBNFDOMAIN.AsString = '') then
    childband9.Enabled := False;
end;

procedure TLOCAD1_PRINT_frm.ChildBand10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand10-------------------------------------------------------
  ChildBand10.Height := ChildBand10.Tag;
  //내국신용장 종류
  QR_LOCTYPENAME.Caption := qryListLOC_TYPENAME.AsString;
end;

procedure TLOCAD1_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand11-------------------------------------------------------
  ChildBand11.Height := ChildBand11.Tag;
  //개설외화금액
  QR_LOC1AMT.Caption := qryListLOC1AMTC.AsString +' ' + FormatFloat('#,##0.####' , qryListLOC1AMT.AsFloat);
end;

procedure TLOCAD1_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand12-------------------------------------------------------
  ChildBand12.Height := ChildBand12.Tag;
  //개설원화금액
  QR_LOC2AMT.Caption := qryListLOC2AMTC.AsString +' ' + FormatFloat('#,##0.####' , qryListLOC2AMT.AsFloat);
end;

procedure TLOCAD1_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand13-------------------------------------------------------
  ChildBand13.Height := ChildBand13.Tag;
  //허용오차
  QR_CDPERPM.Caption := '(±)' + qryListCD_PERP.AsString +'/'+ qryListCD_PERM.AsString + '(%)';
end;

procedure TLOCAD1_PRINT_frm.ChildBand14BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand14-------------------------------------------------------
  ChildBand14.Height := ChildBand14.Tag;
  //매매기준율
  QR_EXRATE.Caption := FormatFloat('#,##0.####' , qryListEX_RATE.AsFloat);
end;

procedure TLOCAD1_PRINT_frm.ChildBand15BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand15-------------------------------------------------------
   //물품매도확약서번호
      nIndex := 1;
      for i := 1 to 9 do
      begin
        if Trim(qrylist.FieldByName('OFFERNO'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_OFFERNO'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_OFFERNO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_OFFERNO'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('OFFERNO'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      ChildBand15.Height := (15 * (nIndex-1));
end;

procedure TLOCAD1_PRINT_frm.ChildBand16BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand16-------------------------------------------------------
  ChildBand16.Height := ChildBand16.Tag;
  //물품인도기일
  QR_DELIVERY.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListDELIVERY.AsString));
end;

procedure TLOCAD1_PRINT_frm.ChildBand17BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand17-------------------------------------------------------
  ChildBand17.Height := ChildBand17.Tag;
  //유효기일
  QR_EXPIRY.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListEXPIRY.AsString));
end;

procedure TLOCAD1_PRINT_frm.ChildBand18BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex  : Integer;
begin
  inherited;
  nIndex := 1;
  for i := 1 to 5 do
  begin
    if (qryList.FieldByName('DOCCOPY'+IntToStr(i)).AsFloat = 0) then
    begin
      (FindComponent('QR_DOCCOPYNAME'+IntToStr(i)) as TQRLabel).Caption := '';
      Continue;
    end;

    (FindComponent('QR_DOCCOPYNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_DOCCOPY'+IntToStr(nIndex)) as TQRLabel).Enabled := True;


    case i of
      1 : (FindComponent('QR_DOCCOPYNAME'+IntToStr(nIndex)) as TQRLabel).Caption := '물품수령증명서';
      2 : (FindComponent('QR_DOCCOPYNAME'+IntToStr(nIndex)) as TQRLabel).Caption := '공급자발행 세금계산서 사본';
      3 : (FindComponent('QR_DOCCOPYNAME'+IntToStr(nIndex)) as TQRLabel).Caption := '물품명세가 기재된 송장';
      4 : (FindComponent('QR_DOCCOPYNAME'+IntToStr(nIndex)) as TQRLabel).Caption := '본내국신용장의 사본';
      5 : (FindComponent('QR_DOCCOPYNAME'+IntToStr(nIndex)) as TQRLabel).Caption := '공급자발행 물품매도확약서 사본';
    end;

    (FindComponent('QR_DOCCOPY'+IntToStr(nIndex)) as TQRLabel).Caption :=  qryList.FieldByName('DOCCOPY'+IntToStr(i)).AsString + ' 통';

    Inc(nIndex); // i 와 nIndex의 값의 변화가 중요
  end;

  if nIndex = 1 then
    ChildBand18.Height := 22
  else
  ChildBand18.Height := (15 * (nIndex-1));  

end;

procedure TLOCAD1_PRINT_frm.ChildBand20BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand20-------------------------------------------------------
  //기타구비서류
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListDOC_ETC1.AsString;
    ChildBand20.Height := ( 14 * memoLines.Count) + 4;
    QR_DOCETC1.Height := ( 14 * memoLines.Count);

    QR_DOCETC1.Lines.Clear;
    QR_DOCETC1.Lines.Text := qryListDOC_ETC1.AsString;

    if QR_DOCETC1.Lines.Count = 0 then
    begin
      ChildBand20.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TLOCAD1_PRINT_frm.ChildBand21BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand21-------------------------------------------------------
  //기타정보
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListREMARK1.AsString;
    ChildBand21.Height := ( 18 * memoLines.Count) + 4;
    QR_REMARK1.Height := ( 18 * memoLines.Count);

    QR_REMARK1.Lines.Clear;
    QR_REMARK1.Lines.Text := qryListREMARK1.AsString;

    if QR_REMARK1.Lines.Count = 0 then
    begin
      ChildBand21.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;

end;

procedure TLOCAD1_PRINT_frm.ChildBand23BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand23-------------------------------------------------------
  //대표공급물품명
   QR_BSNHSCODE.Caption := qryListBSN_HSCODE.AsString;
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListGOODDES1.AsString;
    ChildBand23.Height := ( 16 * memoLines.Count) + 20;
    QR_GOODDES1.Height := ( 16 * memoLines.Count);

    QR_GOODDES1.Lines.Clear;
    QR_GOODDES1.Lines.Text := qryListGOODDES1.AsString;

    //ShowMessage(IntToStr(MemoLines.Count));
    //ShowMessage(IntToStr(QR_GOODDES1.Height));

    if QR_GOODDES1.Lines.Count = 0 then
    begin
      ChildBand23.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TLOCAD1_PRINT_frm.ChildBand24BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand24-------------------------------------------------------
  ChildBand24.Height := ChildBand24.Tag;
  //서류제시기간
   QR_DOCPRD.Caption := qryListDOC_PRD.AsString;
end;

procedure TLOCAD1_PRINT_frm.ChildBand25BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand25-------------------------------------------------------
  ChildBand25.Height := ChildBand25.Tag;
  //개설근거별용도
   QR_BUSINESSNAME.Caption := qryListBUSINESSNAME.AsString;

   if QR_BUSINESSNAME.Caption = '' then ChildBand25.Enabled := False;
end;

procedure TLOCAD1_PRINT_frm.ChildBand28BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand28------------------------------------------------------
  ChildBand28.Height := ChildBand28.Tag;
  //
   QR_OPENNO.Caption := qryListOPEN_NO.AsString;
end;

procedure TLOCAD1_PRINT_frm.ChildBand29BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand29-------------------------------------------------------
  //발신기관 전자서명
    nIndex := 1;
    for i := 1 to 3 do
    begin
      if Trim(qrylist.FieldByName('EXNAME'+IntToStr(i)).AsString) = '' then
      begin
        (FindComponent('QR_EXNAME'+IntToStr(i)) as TQRLabel).Caption := '';
        Continue;
      end;

      (FindComponent('QR_EXNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_EXNAME'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('EXNAME'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    ChildBand29.Height := (15 * (nIndex-1)) + 38;
end;

end.
