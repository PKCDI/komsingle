unit LOCRC1_PRINT2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT ,DB ,ADODB, QRCtrls, QuickRpt, ExtCtrls;

type
  TLOCRC1_PRINT2_frm = class(TPRINT_PARENT_QR)
    QRBand1: TQRBand;
    QRImage3: TQRImage;
    QRLabel124: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRImage1: TQRImage;
    SummaryBand1: TQRBand;
    ChildBand1: TQRChildBand;
    QRShape1: TQRShape;
    QRImage2: TQRImage;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape2: TQRShape;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel26: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel24: TQRLabel;
    qryTax: TADOQuery;
    qryTaxKEYY: TStringField;
    qryTaxSEQ: TBCDField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    DetailBand1: TQRBand;
    QR_BILLNO: TQRLabel;
    QR_BILL_DATE: TQRLabel;
    QR_TAX_AMOUNT: TQRLabel;
    QR_BILL_AMOUNT: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FMaintNO : String;
    procedure OPENDB;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
  end;

var
  LOCRC1_PRINT2_frm: TLOCRC1_PRINT2_frm;

implementation

{$R *.dfm}

uses Commonlib, MSSQL;

procedure TLOCRC1_PRINT2_frm.OPENDB;
begin
  qryTax.Close;
  qryTax.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryTax.Open;
end;

procedure TLOCRC1_PRINT2_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TLOCRC1_PRINT2_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  //MoreData := not qryTax.Eof;
end;

procedure TLOCRC1_PRINT2_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
    //승인번호(세금계산서번호)
    QR_BILLNO.Caption := qryTaxBILL_NO.AsString;

    //작성일
    if qryTaxBILL_DATE.AsString = '' then
     QR_BILL_DATE.Caption := ''
    else
      QR_BILL_DATE.Caption := FormatDateTime('YYYY.MM.DD', ConvertStr2Date(qryTaxBILL_DATE.AsString));

    //공급가액
    if qryTaxBILL_AMOUNT.AsCurrency = 0 then
      QR_BILL_AMOUNT.Caption := ''
    else
      QR_BILL_AMOUNT.Caption :=  FormatFloat('#,##0.###', qryTaxBILL_AMOUNT.AsFloat);

    //세액
    if qryTaxTAX_AMOUNT.AsCurrency = 0 then
      QR_TAX_AMOUNT.Caption := ''
    else
      QR_TAX_AMOUNT.Caption := FormatFloat('#,##0.###', qryTaxTAX_AMOUNT.AsFloat);

    qryTax.Next;

end;

end.
