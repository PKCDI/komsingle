unit APPSPC_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TAPPSPC_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel4: TQRLabel;
    QR_BGM: TQRLabel;
    QRLabel5: TQRLabel;
    QR_DATEE: TQRLabel;
    QRShape1: TQRShape;
    QRLabel126: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel124: TQRLabel;
    QRImage3: TQRImage;
    QRLabel7: TQRLabel;
    QR_CPBANKNAME: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel9: TQRLabel;
    QR_CPAMTU: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel11: TQRLabel;
    QR_CPAMT: TQRLabel;
    QR_CPAMTC: TQRLabel;
    QR_CPAMTKRW: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel10: TQRLabel;
    QR_CPCUX: TQRLabel;
    ChildBand6: TQRChildBand;
    QRImage1: TQRImage;
    QRImage2: TQRImage;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    DetailBand1: TQRBand;
    QR_DOCNAME: TQRLabel;
    QRLabel18: TQRLabel;
    QR_DOCNO: TQRLabel;
    QR_LABANKNAME: TQRLabel;
    QR_ISSDATE: TQRLabel;
    QR_LRNO2: TQRLabel;
    SummaryBand1: TQRBand;
    ChildBand7: TQRChildBand;
    QRImage4: TQRImage;
    ChildBand8: TQRChildBand;
    QRImage5: TQRImage;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QR_CPBANKNAME2: TQRLabel;
    ChildBand9: TQRChildBand;
    QRLabel20: TQRLabel;
    QR_CPACCOUNTNO: TQRLabel;
    ChildBand10: TQRChildBand;
    QRLabel21: TQRLabel;
    QR_CPAMT2: TQRLabel;
    QRLabel22: TQRLabel;
    QR_CPNAME1: TQRLabel;
    ChildBand11: TQRChildBand;
    QRLabel24: TQRLabel;
    QR_APPSNAME: TQRLabel;
    QRLabel26: TQRLabel;
    QRImage6: TQRImage;
    QRLabel27: TQRLabel;
    QR_APPNAME: TQRLabel;
    ChildBand12: TQRChildBand;
    QRLabel29: TQRLabel;
    QR_APPSAUPNO: TQRLabel;
    QRLabel31: TQRLabel;
    QR_APPADDR1: TQRLabel;
    QR_APPADDR2: TQRLabel;
    QR_APPADDR3: TQRLabel;
    ChildBand13: TQRChildBand;
    QRLabel35: TQRLabel;
    QR_APPELEC: TQRLabel;
    qryList: TADOQuery;
    qryDetail: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListBGM_GUBUN: TStringField;
    qryListBGM1: TStringField;
    qryListBGM2: TStringField;
    qryListBGM3: TStringField;
    qryListBGM4: TStringField;
    qryListCP_ACCOUNTNO: TStringField;
    qryListCP_NAME1: TStringField;
    qryListCP_NAME2: TStringField;
    qryListCP_CURR: TStringField;
    qryListCP_BANK: TStringField;
    qryListCP_BANKNAME: TStringField;
    qryListCP_BANKBU: TStringField;
    qryListCP_ADD_ACCOUNTNO1: TStringField;
    qryListCP_ADD_NAME1: TStringField;
    qryListCP_ADD_CURR1: TStringField;
    qryListCP_ADD_ACCOUNTNO2: TStringField;
    qryListCP_ADD_NAME2: TStringField;
    qryListCP_ADD_CURR2: TStringField;
    qryListAPP_DATE: TStringField;
    qryListCP_CODE: TStringField;
    qryListCP_NO: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_SNAME: TStringField;
    qryListAPP_SAUPNO: TStringField;
    qryListAPP_NAME: TStringField;
    qryListAPP_ELEC: TStringField;
    qryListAPP_ADDR1: TStringField;
    qryListAPP_ADDR2: TStringField;
    qryListAPP_ADDR3: TStringField;
    qryListCP_AMTU: TBCDField;
    qryListCP_AMTC: TStringField;
    qryListCP_AMT: TBCDField;
    qryListCP_CUX: TBCDField;
    qryListDF_SAUPNO: TStringField;
    qryListDF_EMAIL1: TStringField;
    qryListDF_EMAIL2: TStringField;
    qryListDF_NAME1: TStringField;
    qryListDF_NAME2: TStringField;
    qryListDF_NAME3: TStringField;
    qryListDF_CNT: TStringField;
    qryListVB_CNT: TStringField;
    qryListSS_CNT: TStringField;
    qryDetailKEYY: TStringField;
    qryDetailDOC_GUBUN: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailDOC_NO: TStringField;
    qryDetailLR_GUBUN: TStringField;
    qryDetailLR_NO: TStringField;
    qryDetailLR_NO2: TStringField;
    qryDetailISS_DATE: TStringField;
    qryDetailGET_DATE: TStringField;
    qryDetailEXP_DATE: TStringField;
    qryDetailACC_DATE: TStringField;
    qryDetailVAL_DATE: TStringField;
    qryDetailBSN_HSCODE: TStringField;
    qryDetailISS_EXPAMTU: TBCDField;
    qryDetailISS_EXPAMTC: TStringField;
    qryDetailISS_EXPAMT: TBCDField;
    qryDetailEXCH: TBCDField;
    qryDetailISS_AMTU: TBCDField;
    qryDetailISS_AMTC: TStringField;
    qryDetailISS_AMT: TBCDField;
    qryDetailISS_TOTAMT: TBCDField;
    qryDetailISS_TOTAMTC: TStringField;
    qryDetailTOTCNT: TBCDField;
    qryDetailTOTCNTC: TStringField;
    qryDetailVB_RENO: TStringField;
    qryDetailVB_SENO: TStringField;
    qryDetailVB_FSNO: TStringField;
    qryDetailVB_MAINTNO: TStringField;
    qryDetailVB_DMNO: TStringField;
    qryDetailVB_DETAILNO: TStringField;
    qryDetailVB_GUBUN: TStringField;
    qryDetailSE_GUBUN: TStringField;
    qryDetailSE_SAUPNO: TStringField;
    qryDetailSE_CODE: TStringField;
    qryDetailSE_SNAME: TStringField;
    qryDetailSE_NAME: TStringField;
    qryDetailSE_ELEC: TStringField;
    qryDetailSE_ADDR1: TStringField;
    qryDetailSE_ADDR2: TStringField;
    qryDetailSE_ADDR3: TStringField;
    qryDetailSG_UPTAI1_1: TStringField;
    qryDetailSG_UPTAI1_2: TStringField;
    qryDetailSG_UPTAI1_3: TStringField;
    qryDetailHN_ITEM1_1: TStringField;
    qryDetailHN_ITEM1_2: TStringField;
    qryDetailHN_ITEM1_3: TStringField;
    qryDetailBY_GUBUN: TStringField;
    qryDetailBY_SAUPNO: TStringField;
    qryDetailBY_CODE: TStringField;
    qryDetailBY_SNAME: TStringField;
    qryDetailBY_NAME: TStringField;
    qryDetailBY_ELEC: TStringField;
    qryDetailBY_ADDR1: TStringField;
    qryDetailBY_ADDR2: TStringField;
    qryDetailBY_ADDR3: TStringField;
    qryDetailSG_UPTAI2_1: TStringField;
    qryDetailSG_UPTAI2_2: TStringField;
    qryDetailSG_UPTAI2_3: TStringField;
    qryDetailHN_ITEM2_1: TStringField;
    qryDetailHN_ITEM2_2: TStringField;
    qryDetailHN_ITEM2_3: TStringField;
    qryDetailAG_SAUPNO: TStringField;
    qryDetailAG_CODE: TStringField;
    qryDetailAG_SNAME: TStringField;
    qryDetailAG_NAME: TStringField;
    qryDetailAG_ELEC: TStringField;
    qryDetailAG_ADDR1: TStringField;
    qryDetailAG_ADDR2: TStringField;
    qryDetailAG_ADDR3: TStringField;
    qryDetailLA_BANK: TStringField;
    qryDetailLA_BANKNAME: TStringField;
    qryDetailLA_BANKBU: TStringField;
    qryDetailLA_ELEC: TStringField;
    qryDetailPAI_CODE1: TStringField;
    qryDetailPAI_CODE2: TStringField;
    qryDetailPAI_CODE3: TStringField;
    qryDetailPAI_CODE4: TStringField;
    qryDetailPAI_AMTC1: TStringField;
    qryDetailPAI_AMTC2: TStringField;
    qryDetailPAI_AMTC3: TStringField;
    qryDetailPAI_AMTC4: TStringField;
    qryDetailPAI_AMTU1: TBCDField;
    qryDetailPAI_AMTU2: TBCDField;
    qryDetailPAI_AMTU3: TBCDField;
    qryDetailPAI_AMTU4: TBCDField;
    qryDetailPAI_AMT1: TBCDField;
    qryDetailPAI_AMT2: TBCDField;
    qryDetailPAI_AMT3: TBCDField;
    qryDetailPAI_AMT4: TBCDField;
    qryDetailLA_TYPE: TStringField;
    qryDetailLA_BANKBUCODE: TStringField;
    qryDetailLA_BANKNAMEP: TStringField;
    qryDetailLA_BANKBUP: TStringField;
    qryDetailREMARK1_1: TStringField;
    qryDetailREMARK1_2: TStringField;
    qryDetailREMARK1_3: TStringField;
    qryDetailREMARK1_4: TStringField;
    qryDetailREMARK1_5: TStringField;
    qryDetailREMARK2_1: TStringField;
    qryDetailREMARK2_2: TStringField;
    qryDetailREMARK2_3: TStringField;
    qryDetailREMARK2_4: TStringField;
    qryDetailREMARK2_5: TStringField;
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
   private
    { Private declarations }
    FMaintNO: string;
    procedure OPENDB;
    function StrCase(Selector: string; StrList: array of string): Integer;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
  end;

var
  APPSPC_PRINT_frm: TAPPSPC_PRINT_frm;

implementation

{$R *.dfm}

uses Commonlib, MSSQL;

function TAPPSPC_PRINT_frm.StrCase(Selector: string; StrList: array of string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to High(StrList) do
  begin
    if Selector = StrList[I] then
    begin
      Result := I;
      Break;
    end;
  end;
end;

procedure TAPPSPC_PRINT_frm.OPENDB;
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryList.Open;

  qryDetail.Close;
  qryDetail.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryDetail.Open;
end;

procedure TAPPSPC_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TAPPSPC_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //전자문서번호
  QR_BGM.Caption := qryListBGM1.AsString + '-' + qryListBGM2.AsString + qryListBGM3.AsString + qryListBGM4.AsString;
  //신청일자
  QR_DATEE.Caption := FormatDateTime('YYYY-MM-DD' , ConvertStr2Date(qryListDATEE.AsString));
end;

procedure TAPPSPC_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //추심은행
  QR_CPBANKNAME.Caption := qryListCP_BANKNAME.AsString + '    ' + qryListCP_BANKBU.AsString;
end;

procedure TAPPSPC_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //추심금액
  ChildBand3.Height := ChildBand3.Tag;
  QR_CPAMTC.Caption := qryListCP_AMTC.AsString;
  QR_CPAMTU.Caption := FormatFloat('#,##0.####' , qryListCP_AMT.AsCurrency);
  if (Trim(QR_CPAMTC.Caption) = '') and (Trim(QR_CPAMTU.Caption) = '') then
  begin
    ChildBand3.Enabled := False;
  end;
end;

procedure TAPPSPC_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //원화환산금액
  ChildBand4.Height := ChildBand4.Tag;
  QR_CPAMTKRW.Caption := 'KRW';
  QR_CPAMT.Caption := FormatFloat('#,##0.####' , qryListCP_AMT.AsCurrency);
  if Trim(QR_CPAMT.Caption) = '' then
  begin
    ChildBand4.Enabled := False;
  end;
end;

procedure TAPPSPC_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //적용환율
  ChildBand5.Height := ChildBand5.Tag;
  QR_CPCUX.Caption := FormatFloat('#,##0.####' , qryListCP_CUX.AsCurrency);
  if Trim(QR_CPCUX.Caption) = '' then
  begin
    ChildBand5.Enabled := False;
  end;
end;

procedure TAPPSPC_PRINT_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;

//  ShowMessage(qryDetail.SQL.Text);
//  ShowMessage(qryDetail.Parameters.paramByname('MAINT_NO').Value);
//  ShowMessage(IntToStr(qrydetail.RecordCount));

  if qryDetail.RecordCount > 0 then
  begin
    case StrCase(Trim(qryDetailDOC_GUBUN.AsString) , ['2AP', '2AH', '2AJ', '1BW']) of
      //내국신용장
      0 :
      begin
        //추심관련 서류
        QR_DOCNAME.Caption := '내국신용장';
        //발급번호 - 내국신용장같은경우 LR_NO에 데이터 값
        QR_LRNO2.Caption := qryDetailLR_NO.AsString;
      end;
      //물품수령증명서
      1 :
      begin
        //추심관련 서류
        QR_DOCNAME.Caption := '물품수령증명서';
        //발급번호 - 물품수령증명서같은경우 LR_NO2에 데이터값
        QR_LRNO2.Caption := qryDetailLR_NO2.AsString;
      end;
      //세금계산서
      2 :
      begin
        //추심관련 서류
        QR_DOCNAME.Caption := '세금계산서';
        //발급번호
        QR_LRNO2.Caption := qryDetailLR_NO2.AsString;
      end;
      //상업송장
      3 :
      begin
        //추심관련 서류
        QR_DOCNAME.Caption := '상업송장';
        //발급번호 - 물품수령증명서같은경우 LR_NO2에 데이터값
        QR_LRNO2.Caption := qryDetailLR_NO2.AsString;
      end;
    end;
  
    //전자문서번호
      QR_DOCNO.Caption := qryDetailDOC_NO.AsString;
    //발급은행
      QR_LABANKNAME.Caption := qryDetailLA_BANKNAMEP.AsString + '  ' + qryDetailLA_BANKBU.AsString;
    //발급일
      QR_ISSDATE.Caption := FormatDateTime('YYYY-MM-DD' , ConvertStr2Date(qryDetailISS_DATE.AsString));
  end
  else
  begin
    QR_DOCNAME.Caption := '';
    QR_LRNO2.Caption := '';
    QR_DOCNO.Caption := '';
    QR_LABANKNAME.Caption := '';
    QR_ISSDATE.Caption := '';
  end;

  qryDetail.Next;
end;

procedure TAPPSPC_PRINT_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  //MoreData의 값이 true이면 실행
  MoreData := not qryDetail.Eof;
end;

procedure TAPPSPC_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //추심대금 지급관련 - 지급받을 은행명
  QR_CPBANKNAME2.Caption := qryListCP_BANKNAME.AsString + '    ' + qryListCP_BANKBU.AsString;
end;

procedure TAPPSPC_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //추심대금 지급관련 - 계좌번호
  ChildBand9.Height := ChildBand9.Tag;
  QR_CPACCOUNTNO.Caption := qryListCP_ACCOUNTNO.AsString; 
  if Trim(QR_CPACCOUNTNO.Caption) = '' then
  begin
    ChildBand9.Enabled := False;
  end;
end;

procedure TAPPSPC_PRINT_frm.ChildBand10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
//추심대금 지급관련
  //금액
  QR_CPAMT2.Caption := qryListCP_AMTC.AsString + ' ' + FormatFloat('#,##0.####' , qryListCP_AMTU.AsCurrency)
                      + '    ' + 'KRW ' + FormatFloat('#,##0.####' , qryListCP_AMT.AsCurrency);
  //예금주
  QR_CPNAME1.Caption := qryListCP_NAME1.AsString;
end;

procedure TAPPSPC_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
//신청인부분
  //신청인
  QR_APPSNAME.Caption := qryListAPP_SNAME.AsString;
  //대표자
  QR_APPNAME.Caption := qryListAPP_NAME.AsString;
  //사업자번호
  QR_APPSAUPNO.Caption := qryListAPP_SAUPNO.AsString;
end;

procedure TAPPSPC_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //신청인주소
    nIndex := 1;
    for i := 1 to 3 do
    begin
      if Trim(qrylist.FieldByName('APP_ADDR'+IntToStr(i)).AsString) = '' then
      begin
        Continue;
      end;

      (FindComponent('QR_APPADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_APPADDR'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('APP_ADDR'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    ChildBand12.Height := (17 * (nIndex-1))  ;
end;

procedure TAPPSPC_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //신청인 - 전자서명
  QR_APPELEC.Caption := qryListAPP_ELEC.AsString;
end;

end.
