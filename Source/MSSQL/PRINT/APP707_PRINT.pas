unit APP707_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, ExtCtrls, QuickRpt, QRCtrls, DB;

type
  TAPP707_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel1: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QR_FUNCTION: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QR_RESPONSE: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QR_INMETHOD_Name: TQRLabel;
    QR_DATEE: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel14: TQRLabel;
    QR_APBANK: TQRLabel;
    QR_APBANK1: TQRLabel;
    QR_APBANK2: TQRLabel;
    QR_APBANK3: TQRLabel;
    QR_APBANK4: TQRLabel;
    QR_APBANK5: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_ADBANK: TQRLabel;
    QR_ADBANK1: TQRLabel;
    QR_ADBANK2: TQRLabel;
    QR_ADBANK3: TQRLabel;
    QR_ADBANK4: TQRLabel;
    QR_ADBANK5: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel10: TQRLabel;
    QR_PayName: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel16: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC3: TQRLabel;
    QR_APPLIC4: TQRLabel;
    QR_APPLIC5: TQRLabel;
    ChildBand6: TQRChildBand;
    QRLabel17: TQRLabel;
    QR_ILNO1: TQRLabel;
    QR_ILNO2: TQRLabel;
    QR_ILNO3: TQRLabel;
    QR_ILNO4: TQRLabel;
    QR_ILNO5: TQRLabel;
    QR_ILCUR5: TQRLabel;
    QR_ILCUR4: TQRLabel;
    QR_ILCUR3: TQRLabel;
    QR_ILCUR2: TQRLabel;
    QR_ILCUR1: TQRLabel;
    QR_ILAMT1: TQRLabel;
    QR_ILAMT2: TQRLabel;
    QR_ILAMT3: TQRLabel;
    QR_ILAMT4: TQRLabel;
    QR_ILAMT5: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel21: TQRLabel;
    QR_ADINFO1: TQRLabel;
    QR_ADINFO2: TQRLabel;
    QR_ADINFO3: TQRLabel;
    QR_ADINFO4: TQRLabel;
    QR_ADINFO5: TQRLabel;
    ChildBand8: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QR_CDNO: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QR_ISSDATE: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel37: TQRLabel;
    QR_AMDNO: TQRLabel;
    ChildBand9: TQRChildBand;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    QR_BENEFC4: TQRLabel;
    QR_BENEFC5: TQRLabel;
    QRLabel25: TQRLabel;
    ChildBand10: TQRChildBand;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QR_EXDATE: TQRLabel;
    ChildBand11: TQRChildBand;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QR_INCDCUR: TQRLabel;
    QR_INCDAMT: TQRLabel;
    ChildBand12: TQRChildBand;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QR_DECDCUR2: TQRLabel;
    QR_DECDAMT2: TQRLabel;
    ChildBand13: TQRChildBand;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QR_NWCDCUR: TQRLabel;
    QR_NWCDAMT: TQRLabel;
    ChildBand14: TQRChildBand;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QR_CDPERP: TQRLabel;
    ChildBand15: TQRChildBand;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QR_CDMAX: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QR_AACV1: TQRLabel;
    QR_AACV2: TQRLabel;
    QR_AACV3: TQRLabel;
    QR_AACV4: TQRLabel;
    ChildBand17: TQRChildBand;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QR_LOADON: TQRLabel;
    ChildBand18: TQRChildBand;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QR_SUNJUCKPORT: TQRLabel;
    ChildBand19: TQRChildBand;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QR_DOCHACKPORT: TQRLabel;
    ChildBand20: TQRChildBand;
    QRLabel58: TQRLabel;
    QRLabel61: TQRLabel;
    QR_FORTRAN: TQRLabel;
    ChildBand21: TQRChildBand;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QR_LSTDATE: TQRLabel;
    ChildBand22: TQRChildBand;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QR_SHIPPD1: TQRLabel;
    QR_SHIPPD2: TQRLabel;
    QR_SHIPPD3: TQRLabel;
    QR_SHIPPD4: TQRLabel;
    QR_SHIPPD5: TQRLabel;
    QR_SHIPPD6: TQRLabel;
    ChildBand23: TQRChildBand;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QR_NARRAT1: TQRMemo;
    SummaryBand1: TQRBand;
    QRImage3: TQRImage;
    QRLabel124: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel129: TQRLabel;
    QR_EXADDR2: TQRLabel;
    QR_EXADDR1: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QRShape3: TQRShape;
    QRLabel126: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel27: TQRLabel;
    QRImage1: TQRImage;
    PageFooterBand1: TQRBand;
    QRLabel13: TQRLabel;
    QRLabel28: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
     procedure PrintDocument(Fields: TFields; uPreview: Boolean = True); override;
  end;

var
  APP707_PRINT_frm: TAPP707_PRINT_frm;

implementation

{$R *.dfm}

{ TAPP707_PRINT_frm }

uses Commonlib;

procedure TAPP707_PRINT_frm.PrintDocument(Fields: TFields;
  uPreview: Boolean);
var
  memoLines : TStringList;
  i,nIndex : Integer;
  messageVal : String;
begin
  inherited;
  FFields := Fields;
  FPreview := uPreview;

  with Fields do
  begin
      TitleBand1.Height := TitleBand1.Tag;
      //문서/전자문서번호
      QR_MAINTNO.Caption := FieldByName('MAINT_NO').AsString;

      //전자문서 기능표시
      messageVal := getValueFromField('MESSAGE1');
      case StrToInt(messageVal) of
       1 : QR_FUNCTION.Caption := 'Cancel';
       2 : QR_FUNCTION.Caption := 'Delete';
       4 : QR_FUNCTION.Caption := 'Change';
       6 : QR_FUNCTION.Caption := 'Confirmation';
       7 : QR_FUNCTION.Caption := 'Duplicate';
       9 : QR_FUNCTION.Caption := 'Original';
      else
        QR_FUNCTION.Caption := '';
      end;


      //전자문서 응답유형
      if getValueFromField('MESSAGE2') = 'AB' then
        QR_RESPONSE.Caption := 'Message Acknowledgment'
      else if getValueFromField('MESSAGE2') = 'AP' then
        QR_RESPONSE.Caption := 'Accepted'
      else if getValueFromField('MESSAGE2') = 'NA' then
        QR_RESPONSE.Caption := 'No acknowledgement needed'
      else if getValueFromField('MESSAGE2') = 'Re' then
        QR_RESPONSE.Caption := 'Rjected'
      else
        QR_RESPONSE.Caption := '';

     //--------------------일반정보---------------------------------------------
      //-------------ChildBand1-------------------------------------------------
        ChildBand1.Height := ChildBand1.Tag;
        //조건변경신청일자
        QR_DATEE.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('DATEE').AsString));

        //개설방법
        QR_INMETHOD_Name.Caption := getValueFromField('IN_MATHOD')+ ' ' + getValueFromField('mathod_Name') ;
      //------------------------------------------------------------------------

      //-------------ChildBand2-------------------------------------------------
        //개설(의뢰)은행
        QR_APBANK.Caption := getValueFromField('AP_BANK');
        nIndex := 1;
        for i := 1 to 5 do
        begin
          if Trim(FieldByName('AP_BANK'+IntToStr(i)).AsString) = '' Then
          begin
            Continue;
          end;
          (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AP_BANK'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;
        ChildBand2.Height := (13 * nIndex)+20;
      //------------------------------------------------------------------------

      //-------------ChildBand3-------------------------------------------------
        //(희망)통지은행
        QR_ADBANK.Caption := getValueFromField('AD_BANK');
        nIndex := 1;
        for i := 1 to 4 do
        begin
          if Trim(FieldByName('AD_BANK'+IntToStr(i)).AsString) = '' Then
          begin
             Continue;
          end;
          (FindComponent('QR_ADBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_ADBANK'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AD_BANK'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if Trim(QR_ADBANK1.Caption) = '' then
        begin
          ChildBand3.Enabled := False;
        end
        else
          ChildBand3.Height := (13 * nIndex)+20;
      //------------------------------------------------------------------------


      //-------------ChildBand4-------------------------------------------------
        //기한부신용장 신용공여주체
        ChildBand4.Height := ChildBand4.Tag;
        QR_PayName.Caption := getValueFromField('pay_Name');

        if Trim(QR_PayName.Caption) = '' then
        begin
          ChildBand4.Enabled := False;
        end;
      //------------------------------------------------------------------------


      //-------------ChildBand5-------------------------------------------------
        //개설 의뢰인
        nIndex := 1;
        for i := 1 to 5 do
        begin
          if Trim(FieldByName('APPLIC'+IntToStr(i)).AsString) = '' then Continue;

          (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('APPLIC'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if Trim(QR_APPLIC1.Caption) = '' then
        begin
          ChildBand2.Enabled := False;
        end
        else
          ChildBand5.Height := (13 * nIndex)+18;
      //------------------------------------------------------------------------


      //---------ChildBand6-----------------------------------------------------
        //수입승인 관리번호
          nIndex := 1;
          for i := 1 to 5 do
          begin
            if Trim(FieldByName('IL_NO'+IntToStr(i)).AsString) = '' then
            begin
              (FindComponent('QR_ILNO'+IntToStr(i)) as TQRLabel).Caption := '';
              Continue;
            end;
            
            (FindComponent('QR_ILNO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
            (FindComponent('QR_ILNO'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('IL_NO'+IntToStr(i)).AsString;

            (FindComponent('QR_ILCUR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
            (FindComponent('QR_ILCUR'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('IL_CUR'+IntToStr(i)).AsString;

            (FindComponent('QR_ILAMT'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
            (FindComponent('QR_ILAMT'+IntToStr(nIndex)) as TQRLabel).Caption :=  FormatFloat('#,##0.###', FieldByName('IL_AMT'+IntToStr(i)).AsFloat);
            Inc(nIndex);
          end;


        if Trim(QR_ILNO1.Caption) = '' then
        begin
          ChildBand6.Enabled := False;
        end
        else
          ChildBand6.Height := (13 * nIndex)+30;
      //------------------------------------------------------------------------


      //---------ChildBand7-----------------------------------------------------
        //기타정보
        nIndex := 1;
        for i := 1 to 5 do
        begin
          if Trim(FieldByName('AD_INFO'+IntToStr(i)).AsString) = '' then
          begin
            (FindComponent('QR_ADINFO'+IntToStr(i)) as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_ADINFO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_ADINFO'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AD_INFO'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if Trim(QR_ADINFO1.Caption) = '' then
        begin
          ChildBand7.Enabled := False;
        end
        else
          ChildBand7.Height := (13 * nIndex) + 10;
      //------------------------------------------------------------------------


      //---------ChildBand8-----------------------------------------------------
        ChildBand8.Height := ChildBand8.Tag;
        //Sender(s) Reference
          QR_CDNO.Caption := getValueFromField('CD_NO');
        //Date of Issue
          QR_ISSDATE.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('ISS_DATE').AsString));
        //Number of Amendment      
          QR_AMDNO.Caption := getValueFromField('AMD_NO');
      //------------------------------------------------------------------------


      //---------ChildBand9-----------------------------------------------------
      //Beneficiary          
        nIndex := 1;
        for i := 1 to 5 do
        begin
          if Trim(FieldByName('BENEFC'+IntToStr(i)).AsString) = '' then
          begin
            (FindComponent('QR_BENEFC'+IntToStr(i)) as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('BENEFC'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if Trim(QR_BENEFC1.Caption) = '' then
        begin
          ChildBand9.Enabled := False;
        end
        else
        begin
          if nIndex < 3 then
             ChildBand9.Height := 40
          else
            ChildBand9.Height := (13 * nIndex) + 4;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand10-----------------------------------------------------
       //New of Date of Expiry
       QR_EXDATE.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('EX_DATE').AsString));
        if Trim(QR_EXDATE.Caption) = '' then
        begin
          ChildBand10.Enabled := False;
        end
        else
          ChildBand10.Height := ChildBand10.Tag;
      //------------------------------------------------------------------------


      //---------ChildBand11-----------------------------------------------------
       //Increase of Documentary Credit Amount
       QR_INCDCUR.Caption := getValueFromField('INCD_CUR');
       QR_INCDAMT.Caption := FormatFloat('#,##0.###', FieldByName('INCD_AMT').AsFloat);
        if (Trim(QR_INCDCUR.Caption) = '') or (Trim(QR_INCDAMT.Caption) = '') then
        begin
          ChildBand11.Enabled := False;
        end
        else
          ChildBand11.Height := ChildBand11.Tag;
      //------------------------------------------------------------------------


      //---------ChildBand12-----------------------------------------------------
       //Decrease of Documentary Credit Amount  :
       QR_DECDCUR2.Caption := getValueFromField('DECD_CUR');
       QR_DECDAMT2.Caption := FormatFloat('#,##0.###', FieldByName('DECD_AMT').AsFloat);
        if (Trim(QR_DECDCUR2.Caption) = '') or (Trim(QR_DECDAMT2.Caption) = '') then
        begin
          ChildBand12.Enabled := False;
        end
        else
          ChildBand12.Height := ChildBand12.Tag;
      //------------------------------------------------------------------------

      //---------ChildBand13-----------------------------------------------------
       //New Documentary CreditAmount after Amentdment :
       QR_NWCDCUR.Caption := getValueFromField('NWCD_CUR');
       QR_NWCDAMT.Caption := FormatFloat('#,##0.###', FieldByName('NWCD_AMT').AsFloat);
        if (Trim(QR_NWCDCUR.Caption) = '') or (Trim(QR_NWCDAMT.Caption) = '') then
        begin
          ChildBand13.Enabled := False;
        end
        else
          ChildBand13.Height := ChildBand13.Tag;
      //------------------------------------------------------------------------


      //---------ChildBand14-----------------------------------------------------
       //Percentage Credit Amount Tolerance
       QR_CDPERP.Caption := '(±) ' + getValueFromField('CD_PERP') + '/' + getValueFromField('CD_PERM')+' %';

        if Trim(QR_CDPERP.Caption) = '' then
        begin
          ChildBand14.Enabled := False;
        end
        else
          ChildBand14.Height := ChildBand14.Tag;
      //------------------------------------------------------------------------


      //---------ChildBand15----------------------------------------------------
       //Maximum Credit Amount
       QR_CDMAX.Caption := getValueFromField('CDMAX_Name');

        if Trim(QR_CDMAX.Caption) = '' then
        begin
          ChildBand15.Enabled := False;
        end
        else
          ChildBand15.Height := ChildBand15.Tag;
      //------------------------------------------------------------------------


      //---------ChildBand16-----------------------------------------------------
      //Additional Amounts Covered           
        nIndex := 1;
        for i := 1 to 4 do
        begin
          if Trim(FieldByName('AA_CV'+IntToStr(i)).AsString) = '' then
          begin
            (FindComponent('QR_AACV'+IntToStr(i)) as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AA_CV'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if Trim(QR_AACV1.Caption) = '' then
        begin
          ChildBand16.Enabled := False;
        end
        else
          ChildBand16.Height := (13 * nIndex) + 4;
      //------------------------------------------------------------------------


      //---------ChildBand17----------------------------------------------------
        //Place of Taking in Charge/Dispatch from ···/ Place of Receipt :
        QR_LOADON.Caption := getValueFromField('LOAD_ON');
        if Trim(QR_LOADON.Caption) = '' then
        begin
          ChildBand17.Enabled := False;
        end;
        ChildBand17.Height := ChildBand17.Tag;
      //------------------------------------------------------------------------


      //---------ChildBand18----------------------------------------------------
        //Port of Loading / Airport of Departure :
        QR_SUNJUCKPORT.Caption := getValueFromField('SUNJUCK_PORT');
        if Trim(QR_SUNJUCKPORT.Caption) = '' then
        begin
          ChildBand18.Enabled := False;
        end;
        ChildBand18.Height := ChildBand18.Tag;
      //------------------------------------------------------------------------


      //---------ChildBand19----------------------------------------------------
        //Port of Discharge / Airport of Departure
        QR_DOCHACKPORT.Caption := getValueFromField('DOCHACK_PORT');
        if Trim(QR_DOCHACKPORT.Caption) = '' then
        begin
          ChildBand19.Enabled := False;
        end;
        ChildBand19.Height := ChildBand19.Tag;
      //------------------------------------------------------------------------

      //---------ChildBand20----------------------------------------------------
        //Place of Final Destination/For Transportation to ···/ Place of Delivery :
        QR_FORTRAN.Caption := getValueFromField('FOR_TRAN');
        if Trim(QR_FORTRAN.Caption) = '' then
        begin
          ChildBand20.Enabled := False;
        end;
        ChildBand20.Height := ChildBand20.Tag;
      //------------------------------------------------------------------------

      //---------ChildBand21----------------------------------------------------
        //Latest Date of Shipment
        QR_LSTDATE.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('LST_DATE').AsString));
        if Trim(QR_LSTDATE.Caption) = '' then
        begin
          ChildBand21.Enabled := False;
        end;
        ChildBand21.Height := ChildBand21.Tag;
      //------------------------------------------------------------------------

      //---------ChildBand22----------------------------------------------------
        //Shipment Period
        nIndex := 1;
        for i := 1 to 6 do
        begin
          if Trim(FieldByName('SHIP_PD'+IntToStr(i)).AsString) = '' then
          begin
            (FindComponent('QR_SHIPPD'+IntToStr(i)) as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('SHIP_PD'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if Trim(QR_SHIPPD1.Caption) = '' then
        begin
          ChildBand22.Enabled := False;
        end
        else
          ChildBand22.Height := (13 * nIndex) + 8;
      //------------------------------------------------------------------------

      //---------ChildBand23----------------------------------------------------
      try
        memoLines := TStringList.Create;
        memoLines.Text := FieldByName('NARRAT_1').AsString;

        ChildBand23.Height := ( 13 * memoLines.Count) + 26;
        QR_NARRAT1.Height := ( 13 * memoLines.Count);

        //Reimbursement Bank
        QR_NARRAT1.Lines.Clear;
        getValueMemo(QR_NARRAT1,'NARRAT_1');

        if (Trim(QR_NARRAT1.Caption) = '') and (FieldByName('NARRAT').AsBoolean = True) then
        begin
          ChildBand23.Enabled := False;
        end;
      finally
        memoLines.Free;
      end;
    //--------------------------------------------------------------------------


    //-------------SummaryBand1-------------------------------------------------
      //신청업체
      QR_EXNAME1.Caption := getValueFromField('EX_NAME1');
      QR_EXNAME2.Caption := getValueFromField('EX_NAME2');
      QR_EXNAME3.Caption := getValueFromField('EX_NAME3');
      QR_EXADDR1.Caption := getValueFromField('EX_ADDR1');
      QR_EXADDR2.Caption := getValueFromField('EX_ADDR2');

  end;

  RunPrint;

end;

end.
