unit PCRLIC_EXCEL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, sDialogs, NewExcelWriter;

type
  TPCRLIC_EXCEL_FRM = class(TForm)
    qryMst: TADOQuery;
    qryTax: TADOQuery;
    sSaveDialog1: TsSaveDialog;
    qryMstMAINT_NO: TStringField;
    qryMstAPP_NAME1: TStringField;
    qryMstAPP_ADDR: TStringField;
    qryMstAPP_NAME2: TStringField;
    qryMstAPP_SAUP_NO: TStringField;
    qryMstConfirmDate: TStringField;
    qryMstSUP_NAME1: TStringField;
    qryMstSUP_NAME2: TStringField;
    qryMstSUP_ADDR: TStringField;
    qryMstSUP_SAUP_NO: TStringField;
    qryMstC1_AMT: TBCDField;
    qryMstC1_C: TStringField;
    qryMstC2_AMT: TBCDField;
    qryMstITM_GBN: TStringField;
    qryMstBANKNAME: TStringField;
    qryMstBANKSIGN: TStringField;
    qryMstLIC_INFO: TMemoField;
    qryMstTQTY: TBCDField;
    qryMstTQTYC: TStringField;
    qryMstTAMT1: TBCDField;
    qryMstTAMT1C: TStringField;
    qryMstTAMT2: TBCDField;
    qryMstLIC_NO: TStringField;
    qryMstSEQ: TIntegerField;
    qryMstHS_NO: TStringField;
    qryMstSupplyDate: TStringField;
    qryMstProductName: TMemoField;
    qryMstSIZE: TStringField;
    qryMstQTY: TBCDField;
    qryMstQTYC: TStringField;
    qryMstPRI2: TBCDField;
    qryMstPRI1: TBCDField;
    qryMstAMT1: TBCDField;
    qryMstAMT1C: TStringField;
    qryMstACHG_G: TStringField;
    qryMstAC2_AMT: TBCDField;
    qryMstAC1_AMT: TBCDField;
    qryMstAC1_C: TStringField;
    qryMstAPP_SIGN: TStringField;
    qryMstAMT2: TBCDField;
    qryTaxKEYY: TStringField;
    qryTaxSEQ: TIntegerField;
    qryTaxDATEE: TStringField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxITEM_NAME1: TStringField;
    qryTaxITEM_NAME2: TStringField;
    qryTaxITEM_NAME3: TStringField;
    qryTaxITEM_NAME4: TStringField;
    qryTaxITEM_NAME5: TStringField;
    qryTaxITEM_DESC: TMemoField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    qryTaxQUANTITY: TBCDField;
    qryTaxQUANTITY_UNIT: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure qryMstAfterOpen(DataSet: TDataSet);
    procedure qryTaxAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FSQL : String;
    FFDate : String;
    FLDate : String;
    FEXCEL : TExcelWriter;
    FDetailCount : integer;
    FTaxCount : Integer;

    FCurrIDX : integer;

    procedure OPENDB;
    procedure OPENTAX(KEYY : String);
    procedure ExcelTitle;
    procedure WriteMStData;
    procedure WriteDetailData;
    procedure WriteTaxData;
    function DelBlank(nText : String):String;
  public
    { Public declarations }
    procedure Run(FDate, LDate : String);
  end;

var
  PCRLIC_EXCEL_FRM: TPCRLIC_EXCEL_FRM;

implementation

uses MSSQL;

{$R *.dfm}

{ TPCRLIC_EXCEL_FRM }

procedure TPCRLIC_EXCEL_FRM.OPENDB;
begin
  with qryMst do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE BETWEEN '+QuotedStr(FFDate)+' AND '+QuotedStr(FLDate));
    SQL.Add('ORDER BY MAINT_NO');
    Open;

    IF qryMst.RecordCount = 0 Then Raise Exception.Create('출력자료가 없습니다');
  end;

end;

procedure TPCRLIC_EXCEL_FRM.OPENTAX(KEYY: String);
begin
  with qryTax do
  begin
    Close;
    Parameters.ParamByName('KEYY').Value := KEYY;
    Open;
  end;
end;

procedure TPCRLIC_EXCEL_FRM.Run(FDate, LDate: String);
var
  LMAINT_NO : String;
begin
  FFDate := FDate;
  FLDate := LDate;
  OPENDB;

  FEXCEL := TExcelWriter.Create;
  FCurrIDX := 4;

  with FEXCEL do
  begin
    ExcelTitle;
    while not qryMst.Eof do
    begin
      IF qryMst.RecNo = 1 Then
      begin
        LMAINT_NO := qryMstMAINT_NO.AsString;
        WriteMStData;
      end;


      IF LMAINT_NO = qryMstMAINT_NO.AsString Then
      begin
        WriteDetailData;
        IF not qrytax.Eof Then WriteTaxData;
        Inc(FCurrIDX);
      end
      else
      begin
        IF not qrytax.Eof Then
        begin
          WriteTaxData;
          Inc(FCurrIDX);
          Continue;
        end
        else
        begin
          WriteMStData;
          WriteDetailData;
          Inc(FCurrIDX);
        end;
      end;

      qryMst.Next;
    end;
//    Autofit('A1:AW');
    Autofit;
    SaveWithDialog;
  end;

end;

procedure TPCRLIC_EXCEL_FRM.FormCreate(Sender: TObject);
begin
  FSQL := qryMst.SQL.Text;
end;

procedure TPCRLIC_EXCEL_FRM.qryMstAfterOpen(DataSet: TDataSet);
begin
  FDetailCount := qryMst.RecordCount;
end;

procedure TPCRLIC_EXCEL_FRM.qryTaxAfterOpen(DataSet: TDataSet);
begin
  FTaxCount := qryTax.RecordCount;
end;

procedure TPCRLIC_EXCEL_FRM.ExcelTitle;
begin
  with FEXCEL do
  begin
    Cells(1,1,'외화획득용 구매확인서(수신) 내역');
    FontSize('A1','A1',16);
    FontBold('A1','A1');
    MergeCells('A1','D1');

    Cells(1,3,'관리번호');

    Cells(2,2,'신청인');
    MergeCells('B2','G2');

    Cells(2,3,'상호');
    Cells(3,3,'주소');
    Cells(4,3,'명의인');
    Cells(5,3,'사업자등록번호');
    Cells(6,3,'전자서명');
    Cells(7,3,'확인일자');

    Cells(8,2,'공급자');
    MergeCells('H2','K2');

    Cells(8,3,'상호');
    Cells(9,3,'대표');
    Cells(10,3,'주소');
    Cells(11,3,'사업자번호');

    Cells(12,2,'공통');
    MergeCells('L2','X2');

    Cells(12,3,'변동금액');
    Cells(13,3,'단위');
    Cells(14,3,'변동금액(USD)');
    Cells(15,3,'공급물품명세구분');
    Cells(16,3,'확인기관');
    Cells(17,3,'전자서명');
    Cells(18,3,'기타사항');
    Cells(19,3,'총수량');
    Cells(20,3,'단위');
    Cells(21,3,'총금액');
    Cells(22,3,'단위');
    Cells(23,3,'총금액(USD)');
    Cells(24,3,'구매확인서번호');

    Cells(25,2,'구매원료·기재의 내용');
    MergeCells('Y2','AN2');

    Cells(25,3,'순번');
    Cells(26,3,'HS부호');
    Cells(27,3,'공급일');
    Cells(28,3,'품명');
    Cells(29,3,'규격');
    Cells(30,3,'수량');
    Cells(31,3,'단위');
    Cells(32,3,'단가');
    Cells(33,3,'단가(USD)');
    Cells(34,3,'금액');
    Cells(35,3,'단위');
    Cells(36,3,'금액(USD)');
    Cells(37,3,'변동구분');
    Cells(38,3,'변동금액(USD)');
    Cells(39,3,'변동금액');
    Cells(40,3,'단위');

    Cells(41,2,'세금계산서');
    MergeCells('AO2','AW2');

    Cells(41,3,'순번');
    Cells(42,3,'작성일자');
    Cells(43,3,'세금계산서번호');
    Cells(44,3,'수량');
    Cells(45,3,'단위');
    Cells(46,3,'공급가액');
    Cells(47,3,'단위');
    Cells(48,3,'세액');
    Cells(49,3,'단위');
  end;
end;

procedure TPCRLIC_EXCEL_FRM.WriteMStData;
begin
  OPENTAX(qryMstMAINT_NO.AsString);
  with FEXCEL do
  begin
    Cells(1,FCurrIDX, qryMstMAINT_NO.AsString   );
    Cells(2,FCurrIDX, qryMstAPP_NAME1.AsString  );
    Cells(3,FCurrIDX, qryMstAPP_ADDR.AsString   );
    Cells(4,FCurrIDX, qryMstAPP_NAME2.AsString   );
    Cells(5,FCurrIDX, qryMstAPP_SAUP_NO.AsString   );
    Cells(6,FCurrIDX, qryMstAPP_SIGN.AsString   );
    Cells(7,FCurrIDX, qryMstConfirmDate.AsString   );
    Cells(8,FCurrIDX, qryMstSUP_NAME1.AsString   );
    Cells(9,FCurrIDX, qryMstSUP_NAME2.AsString   );
    Cells(10,FCurrIDX, qryMstSUP_ADDR.AsString   );
    Cells(11,FCurrIDX, qryMstSUP_SAUP_NO.AsString   );
    Cells(12,FCurrIDX, qryMstC1_AMT.AsString   );
    Cells(13,FCurrIDX, qryMstC1_C.AsString   );
    Cells(14,FCurrIDX, qryMstC2_AMT.AsString   );
    Cells(15,FCurrIDX, qryMstITM_GBN.AsString   );
    Cells(16,FCurrIDX, qryMstBANKNAME.AsString   );
    Cells(17,FCurrIDX, qryMstBANKSIGN.AsString   );
    Cells(18,FCurrIDX, DelBlank(qryMstLIC_INFO.AsString)   );
    Cells(19,FCurrIDX, qryMstTQTY.AsString   );
    Cells(20,FCurrIDX, qryMstTQTYC.AsString   );
    Cells(21,FCurrIDX, qryMstTAMT1.AsString   );
    Cells(22,FCurrIDX, qryMstTAMT1C.AsString   );
    Cells(23,FCurrIDX, qryMstTAMT2.AsString   );
    Cells(24,FCurrIDX, qryMstLIC_NO.AsString   );
  end;
end;

procedure TPCRLIC_EXCEL_FRM.WriteDetailData;
begin
  with FEXCEL do
  begin
    Cells(25,FCurrIDX, qryMstSEQ.AsString );
    Cells(26,FCurrIDX, qryMstHS_NO.AsString );
    Cells(27,FCurrIDX, qryMstSupplyDate.AsString );
    Cells(28,FCurrIDX, DelBlank(qryMstProductName.AsString) );
    Cells(29,FCurrIDX, DelBlank(qryMstSIZE.AsString) );
    IF qryMstQTYC.AsString = 'KRW' Then
      Cells(30,FCurrIDX, FormatFloat('#,0',qryMstQTY.AsCurrency))
    else
      Cells(30,FCurrIDX, FormatFloat('#,0.000',qryMstQTY.AsCurrency));
    Cells(31,FCurrIDX, qryMstQTYC.AsString );
    Cells(32,FCurrIDX, FormatFloat('#,0.000',qryMstPRI2.AsCurrency ));
    Cells(33,FCurrIDX, FormatFloat('#,0.000',qryMstPRI1.AsCurrency ));

    IF qryMstAMT1C.AsString = 'KRW' Then
      Cells(34,FCurrIDX, FormatFloat('#,0',qryMstAMT1.AsCurrency))
    else
      Cells(34,FCurrIDX, FormatFloat('#,0.000',qryMstAMT1.AsCurrency));

    Cells(35,FCurrIDX, qryMstAMT1C.AsString );
    Cells(36,FCurrIDX, FormatFloat('#,0.000',qryMstAMT2.AsCurrency ));
    Cells(37,FCurrIDX, qryMstACHG_G.AsString );
    Cells(38,FCurrIDX, FormatFloat('#,0.000',qryMstAC2_AMT.AsCurrency ));

    IF qryMstAC1_C.AsString = 'KRW' Then
      Cells(39,FCurrIDX, FormatFloat('#,0',qryMstAC1_AMT.AsCurrency))
    else
      Cells(39,FCurrIDX, FormatFloat('#,0.000',qryMstAC1_AMT.AsCurrency));

    Cells(40,FCurrIDX, qryMstAC1_C.AsString );
  end;
end;

function TPCRLIC_EXCEL_FRM.DelBlank(nText: String): String;
var
  nLoop ,
  MaxCount : integer;
  TempStr : TStringList;
  MainStr : String;
begin
  TempStr := TStringList.Create;
  try
    TempStr.Text := nText;
    MainStr := '';

    MaxCount := TempStr.Count -1 ;
    for nLoop := 0 to MaxCount do
    begin
      IF (Trim(TempStr.Strings[nLoop]) <> '') AND (nLoop = 0) Then
        MainStr := TempStr.Strings[nLoop]
      else
      IF Trim(TempStr.Strings[nLoop]) <> '' then
      begin
        MainStr := MainStr +#13#10 + TempStr.Strings[nLoop];
      end;
    end;

    Result := MainStr;
  finally
    TempStr.Free;
  end;
end;

procedure TPCRLIC_EXCEL_FRM.WriteTaxData;
begin
  with FEXCEL do
  begin
    Cells(41,FCurrIDX, qryTaxSEQ.AsString);
    Cells(42,FCurrIDX, qryTaxBILL_DATE.AsString);
    Cells(43,FCurrIDX, qryTaxBILL_NO.AsString);
    Cells(44,FCurrIDX, FormatFloat('#,0',qryTaxQUANTITY.AsCurrency ));
    Cells(45,FCurrIDX, qryTaxQUANTITY_UNIT.AsString);

    IF qryTaxBILL_AMOUNT_UNIT.AsString = 'KRW' Then
      Cells(46,FCurrIDX, FormatFloat('#,0',qryTaxBILL_AMOUNT.AsCurrency ))
    else
      Cells(46,FCurrIDX, FormatFloat('#,0.000',qryTaxBILL_AMOUNT.AsCurrency ));

    Cells(47,FCurrIDX, qryTaxBILL_AMOUNT_UNIT.AsString);

    IF qryTaxTAX_AMOUNT_UNIT.AsString = 'KRW' Then
      Cells(48,FCurrIDX, FormatFloat('#,0',qryTaxTAX_AMOUNT.AsCurrency ))
    else
      Cells(48,FCurrIDX, FormatFloat('#,0.000',qryTaxTAX_AMOUNT.AsCurrency ));

    Cells(49,FCurrIDX, qryTaxTAX_AMOUNT_UNIT.AsString);
    qryTax.Next;
  end;
end;

end.
