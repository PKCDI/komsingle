unit LOCRC2_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TLOCRC2_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel4: TQRLabel;
    QR_MAINT_NO: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel3: TQRLabel;
    QR_RFF_NO: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel6: TQRLabel;
    QR_ISS_DAT: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel8: TQRLabel;
    QR_BENEFC1: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel10: TQRLabel;
    QR_GET_DAT: TQRLabel;
    ChildBand6: TQRChildBand;
    QRLabel12: TQRLabel;
    QR_RCT_AMT1: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel14: TQRLabel;
    QR_EXP_DAT: TQRLabel;
    ChildBand8: TQRChildBand;
    QR_REMARK1: TQRMemo;
    QRLabel24: TQRLabel;
    ChildBand9: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel124: TQRLabel;
    QRImage1: TQRImage;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    DetailBand1: TQRBand;
    QR_HSNO: TQRLabel;
    QR_NAME1: TQRMemo;
    QR_SIZE1: TQRMemo;
    QR_QTY: TQRLabel;
    QR_PRICE: TQRLabel;
    QR_AMT: TQRLabel;
    QRImage2: TQRImage;
    QRImage4: TQRImage;
    QR_SubTotal: TQRLabel;
    QR_STQTY: TQRLabel;
    QR_STAMT: TQRLabel;
    SummaryBand1: TQRBand;
    QR_BSN_HSCODE: TQRLabel;
    QRLabel21: TQRLabel;
    QR_TQTY: TQRLabel;
    QR_TAMT: TQRLabel;
    QRImage5: TQRImage;
    ChildBand10: TQRChildBand;
    QRLabel5: TQRLabel;
    QRImage6: TQRImage;
    ChildBand11: TQRChildBand;
    QR_APNAME: TQRLabel;
    QR_APBANK2: TQRLabel;
    QR_APBANK1: TQRLabel;
    QR_APBANK: TQRLabel;
    QRLabel13: TQRLabel;
    ChildBand12: TQRChildBand;
    QR_LOCNO: TQRLabel;
    QRLabel23: TQRLabel;
    ChildBand13: TQRChildBand;
    QR_LOCAMT: TQRLabel;
    QRLabel26: TQRLabel;
    ChildBand14: TQRChildBand;
    QRLabel27: TQRLabel;
    QR_LOADDATE: TQRLabel;
    ChildBand15: TQRChildBand;
    QR_EXPDATE: TQRLabel;
    QRLabel30: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel31: TQRLabel;
    QR_LOC_REM1: TQRMemo;
    ChildBand17: TQRChildBand;
    QRImage7: TQRImage;
    QRLabel32: TQRLabel;
    ChildBand18: TQRChildBand;
    QR_APPLIC1: TQRLabel;
    QRLabel34: TQRLabel;
    ChildBand19: TQRChildBand;
    QR_APPLIC2: TQRLabel;
    QRLabel36: TQRLabel;
    ChildBand20: TQRChildBand;
    QR_APPLIC3: TQRLabel;
    QRLabel38: TQRLabel;
    qrylist: TADOQuery;
    qrylistMAINT_NO: TStringField;
    qrylistUSER_ID: TStringField;
    qrylistDATEE: TStringField;
    qrylistMESSAGE1: TStringField;
    qrylistMESSAGE2: TStringField;
    qrylistRFF_NO: TStringField;
    qrylistGET_DAT: TStringField;
    qrylistISS_DAT: TStringField;
    qrylistEXP_DAT: TStringField;
    qrylistBENEFC: TStringField;
    qrylistBENEFC1: TStringField;
    qrylistAPPLIC: TStringField;
    qrylistAPPLIC1: TStringField;
    qrylistAPPLIC2: TStringField;
    qrylistAPPLIC3: TStringField;
    qrylistAPPLIC4: TStringField;
    qrylistAPPLIC5: TStringField;
    qrylistAPPLIC6: TStringField;
    qrylistRCT_AMT1: TBCDField;
    qrylistRCT_AMT1C: TStringField;
    qrylistRCT_AMT2: TBCDField;
    qrylistRCT_AMT2C: TStringField;
    qrylistRATE: TBCDField;
    qrylistREMARK: TStringField;
    qrylistREMARK1: TMemoField;
    qrylistTQTY: TBCDField;
    qrylistTQTY_G: TStringField;
    qrylistTAMT: TBCDField;
    qrylistTAMT_G: TStringField;
    qrylistLOC_NO: TStringField;
    qrylistAP_BANK: TStringField;
    qrylistAP_BANK1: TStringField;
    qrylistAP_BANK2: TStringField;
    qrylistAP_BANK3: TStringField;
    qrylistAP_BANK4: TStringField;
    qrylistAP_NAME: TStringField;
    qrylistLOC1AMT: TBCDField;
    qrylistLOC1AMTC: TStringField;
    qrylistLOC2AMT: TBCDField;
    qrylistLOC2AMTC: TStringField;
    qrylistEX_RATE: TBCDField;
    qrylistLOADDATE: TStringField;
    qrylistEXPDATE: TStringField;
    qrylistLOC_REM: TStringField;
    qrylistLOC_REM1: TMemoField;
    qrylistCHK1: TBooleanField;
    qrylistCHK2: TStringField;
    qrylistCHK3: TStringField;
    qrylistPRNO: TIntegerField;
    qrylistAMAINT_NO: TStringField;
    qrylistBSN_HSCODE: TStringField;
    qrylistBENEFC2: TStringField;
    qrylistAPPLIC7: TStringField;
    qryGoods: TADOQuery;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TIntegerField;
    qryGoodsHS_NO: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsAMT: TBCDField;
    qryGoodsAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsSTAMT: TBCDField;
    qryGoodsSTAMT_G: TStringField;
    qryGoodsASEQ: TIntegerField;
    qryGoodsAMAINT_NO: TStringField;
    PageFooterBand1: TQRBand;
    QRLabel7: TQRLabel;
    QR_RCT_AMT2: TQRLabel;
    QR_RATE: TQRLabel;
    QR_LOCAMT2: TQRLabel;
    QR_EXRATE: TQRLabel;
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure SummaryBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand14BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand15BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand18BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand19BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand20BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FDocNo : String;
    procedure OPENDB;
  public
    { Public declarations }
    property  DocNo:string  read FDocNo write FDocNo;
  end;

var
  LOCRC2_PRINT_frm: TLOCRC2_PRINT_frm;

implementation

{$R *.dfm}

uses Commonlib, MSSQL;

{ TLOCRC2_PRINT_frm }

procedure TLOCRC2_PRINT_frm.OPENDB;
begin
  qrylist.Close;
  qrylist.Parameters.ParamByName('MAINT_NO').Value := FDocNo;
  qrylist.Open;

  qryGoods.Close;
  qryGoods.Parameters.ParamByName('MAINT_NO').Value := FDocNo;
  qryGoods.Open;
end;

procedure TLOCRC2_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TLOCRC2_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand1-------------------------------------------------------
  //전자문서번호
  ChildBand1.Height := ChildBand1.Tag;
  QR_MAINT_NO.Caption := qrylistMAINT_NO.AsString;
  if Trim(QR_MAINT_NO.Caption) = '' then
  begin
    ChildBand1.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand2-------------------------------------------------------
  //발급번호
  ChildBand2.Height := ChildBand2.Tag;
  QR_RFF_NO.Caption :=  qrylistRFF_NO.AsString;

  if Trim(QR_RFF_NO.Caption) = '' then
  begin
    ChildBand2.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand3-------------------------------------------------------
  //발급일자
  ChildBand3.Height := ChildBand3.Tag;
  QR_ISS_DAT.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(qrylistISS_DAT.AsString));

  if Trim(QR_ISS_DAT.Caption) = '' then
  begin
    ChildBand3.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand4-------------------------------------------------------
  //공급자
  QR_BENEFC1.Height := ChildBand4.Tag;
  QR_BENEFC1.Caption := qrylistBENEFC1.AsString;

  if Trim(QR_BENEFC1.Caption) = '' then
  begin
    ChildBand4.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand5-------------------------------------------------------
  //인수일자
  ChildBand5.Height := ChildBand5.Tag;
  QR_GET_DAT.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(qrylistGET_DAT.AsString));

  if Trim(QR_ISS_DAT.Caption) = '' then
  begin
    ChildBand5.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //인수금액
  ChildBand6.Height := ChildBand6.Tag;
  if qrylistRCT_AMT1.AsCurrency = 0 then
  begin
    QR_RCT_AMT1.Enabled := False;
    QR_RCT_AMT2.Left := 120;
    QR_RATE.Left := 232;
  end;

  if qryListRCT_AMT2.AsCurrency = 0 then
  begin
    QR_RCT_AMT2.Enabled := False;
    QR_RATE.Left := 120;
  end;

  if qryListRATE.AsCurrency = 0 then
  begin
    QR_RATE.Enabled := False;
  end;

  QR_RCT_AMT1.Caption := qrylistRCT_AMT1C.AsString + ' ' + FormatFloat('#,##0.####',qrylistRCT_AMT1.AsFloat);
  QR_RCT_AMT2.Caption := qrylistRCT_AMT2C.AsString + ' ' + FormatFloat('#,##0.####', qrylistRCT_AMT2.AsFloat);
  QR_RATE.Caption := '(' + FormatFloat('#,##0.####', qrylistRATE.AsFloat) + ')';
end;

procedure TLOCRC2_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand7-------------------------------------------------------
  //문서유효기일
  IF qrylistEXP_DAT.AsString <> '' Then
  begin
    ChildBand7.Height := ChildBand7.Tag;
    QR_EXP_DAT.Caption :=  FormatDateTime('YYYY.MM.DD', ConvertStr2Date(qrylistEXP_DAT.AsString));
    PrintBand := True;
  end
  else
    PrintBand := False;  
end;

procedure TLOCRC2_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand8-------------------------------------------------------
  try
    memoLines := TStringList.Create;
    memoLines.Text := qrylistREMARK1.AsString;

    QR_REMARK1.Height := ( 13 * memoLines.Count);
    QR_REMARK1.Lines.Clear;
    QR_REMARK1.Lines.Text := qrylistREMARK1.AsString;
    ChildBand8.Height := ( 13 * memoLines.Count)+2;

    if QR_REMARK1.Lines.Count = 0 then
    begin
      ChildBand8.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand9-------------------------------------------------------
  //대표공급물품의 HS부호
  QR_BSN_HSCODE.Caption :=  qrylistBSN_HSCODE.AsString;
  if Trim(QR_BSN_HSCODE.Caption) = '' then
  begin
    QRLabel16.Enabled := False;
    QR_BSN_HSCODE.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
  RowCount,nIndex,i : Integer;
begin
  inherited;
  try
    //HS번호
    QR_HSNO.Caption := qryGoodsHS_NO.AsString;

    //수량
    QR_QTY.Caption := FormatFloat('#,##0.####', qryGoodsQTY.AsFloat) + ' ' + qryGoodsQTY_G.AsString;

    //단가
    QR_PRICE.Caption := qryGoodsPRICE_G.AsString + ' ' + FormatFloat('#,##0.####', qryGoodsPRICE.AsFloat)
                      +' / ' +  FormatFloat('#,##0.####' ,qryGoodsQTYG.asFloat) + ' ' + qryGoodsQTYG_G.AsString;

    //금액
    QR_AMT.Caption := qryGoodsAMT_G.AsString + ' ' + FormatFloat('#,##0.####', qryGoodsAMT.AsFloat);



    RowCount := 0;

    //품명
    //TStringList생성
    memoLines := TStringList.Create;
    //memoLines에 NAME1 대입
    memoLines.Text := qryGoodsNAME1.AsString;

    QR_NAME1.Height := ( 13 * memoLines.Count);

    QR_NAME1.Lines.Clear;
    QR_NAME1.Lines.Text := qryGoodsNAME1.AsString;
    //getValueDetailMemo(QR_NAME1,'NAME1');

    RowCount := memoLines.Count;

    //규격
    //TStringList생성
    memoLines := TStringList.Create;
    //memoLines에 SIZE1 대입
    memoLines.Text := qryGoodsSIZE1.AsString;

    QR_SIZE1.Height := (13 * memoLines.Count);

    QR_SIZE1.Lines.Clear;
    QR_SIZE1.Lines.Text := qryGoodsSIZE1.AsString;

    if RowCount < memoLines.Count then RowCount := memoLines.Count;

    QRImage2.Top := 35;
    QRImage2.Top := QRImage2.Top+(RowCount * 13)-13;
    QR_SubTotal.Top := QRImage2.Top + QRImage2.Height + 2;
    QR_STQTY.Top := QR_SubTotal.Top;
    QR_STAMT.Top := QR_SubTotal.Top;
    QRImage4.Top := QR_SubTotal.Top+QR_SubTotal.Height + 2;
    //소량소계
    QR_STQTY.Caption := FormatFloat('#,##0.####', qryGoodsSTQTY.AsFloat) + ' ' + qryGoodsSTQTY_G.AsString;
    //금액소계
    QR_STAMT.Caption := qryGoodsSTAMT_G.AsString + ' ' + FormatFloat('#,##0.####', qryGoodsSTAMT.AsFloat);

    DetailBand1.Height := QRImage4.Top + 3;

    qryGoods.Next;
  finally
     memoLines.Free;
  end;

end;

procedure TLOCRC2_PRINT_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  //MoreData의 값이 true이면 실행
  MoreData := not qryGoods.Eof;
end;

procedure TLOCRC2_PRINT_frm.SummaryBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------SummaryBand1--------------------------------------------------------
  SummaryBand1.Height := SummaryBand1.Tag+5;
  //총수량
  QR_TQTY.Caption := FormatFloat('#,##0.####', qrylistTQTY.AsFloat) + ' ' + qrylistTQTY_G.AsString;
   //총합계
  QR_TAMT.Caption := qrylistTAMT_G.AsString +' '+ FormatFloat('#,##0.####', qrylistTAMT.AsFloat);

  if (Trim(QR_TAMT.Caption) = '') or (Trim(QR_TQTY.Caption) = '')  then
  begin
    SummaryBand1.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand11-------------------------------------------------------
  QR_APBANK.Caption  := qrylistAP_BANK.AsString;
  QR_APBANK1.Caption := qrylistAP_BANK1.AsString;
  QR_APBANK2.Caption := qrylistAP_BANK3.AsString;
  QR_APNAME.Enabled := Trim(qrylistAP_NAME.asString) <> '';
  QR_APNAME.Caption  := qrylistAP_NAME.AsString;

  IF QR_APNAME.Enabled Then
    ChildBand11.Height := 64
  else
    ChildBand11.Height := 48;
end;

procedure TLOCRC2_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand12-------------------------------------------------------
  //신용장번호
  ChildBand12.Height := ChildBand12.Tag;
  QR_LOCNO.Caption := qrylistLOC_NO.AsString;

  if Trim(QR_LOCNO.Caption) = '' then
  begin
    ChildBand12.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand13-------------------------------------------------------
  //내국신용장금액
    ChildBand13.Height := ChildBand13.Tag;
  if qrylistLOC1AMT.AsCurrency = 0 then
  begin
    QR_LOCAMT.Enabled := False;
    QR_LOCAMT2.Left := 120;
    QR_EXRATE.Left := 232;
  end;

  if qrylistLOC2AMT.AsCurrency = 0 then
  begin
    QR_LOCAMT2.Enabled := False;
    QR_EXRATE.Left := 120;
  end;

  if qrylistEX_RATE.AsCurrency = 0 then
  begin
    QR_EXRATE.Enabled := False;
  end;

  QR_LOCAMT.Caption := qrylistLOC1AMTC.AsString + ' ' + FormatFloat('#,##0.####',qrylistLOC1AMT.AsFloat);
  QR_LOCAMT2.Caption := qrylistLOC2AMTC.AsString + ' ' + FormatFloat('#,##0.####', qrylistLOC2AMT.AsFloat);
  QR_EXRATE.Caption := '('+ FormatFloat('#,##0.####', qryListEX_RATE.AsFloat) + ')';     

  if (QR_LOCAMT.Enabled = False) and (QR_LOCAMT2.Enabled = False) then
  begin
    ChildBand13.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand14BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand14-------------------------------------------------------
  //인도기일
  ChildBand14.Height := ChildBand14.Tag;
  QR_LOADDATE.Caption := FormatDateTime('YYYY.MM.DD', ConvertStr2Date(qrylistLOADDATE.AsString));

  if Trim(QR_LOADDATE.Caption) = '' then
  begin
    ChildBand14.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand15BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand15-------------------------------------------------------
  //유효기일
  ChildBand15.Height := ChildBand15.Tag;
  QR_EXPDATE.Caption := FormatDateTime('YYYY.MM.DD', ConvertStr2Date(qrylistEXPDATE.AsString));

  if Trim(QR_EXPDATE.Caption) = '' then
  begin
    ChildBand15.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand16BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand16-------------------------------------------------------
  //참조사항
  try
    memoLines := TStringList.Create;
    memoLines.Text := qrylistLOC_REM1.AsString;

    QR_LOC_REM1.Height := ( 13 * memoLines.Count);
    QR_LOC_REM1.Lines.Clear;
    QR_LOC_REM1.Lines.Text := qrylistLOC_REM1.AsString;
    ChildBand16.Height := ( 13 * memoLines.Count)+2;

    if QR_LOC_REM1.Lines.Count = 0 then
    begin
      ChildBand16.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand18BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand18-------------------------------------------------------
  //기관명(개설업체)
  ChildBand18.Height := ChildBand18.Tag;
  QR_APPLIC1.Caption := qrylistAPPLIC1.AsString;

  if Trim(QR_APPLIC1.Caption) = '' then
  begin
    ChildBand18.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand19BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand18-------------------------------------------------------
  //대표자명(개설업체)
  ChildBand19.Height := ChildBand19.Tag;
  QR_APPLIC2.Caption := qrylistAPPLIC2.AsString;

  if Trim(QR_APPLIC2.Caption) = '' then
  begin
    ChildBand19.Enabled := False;
  end;
end;

procedure TLOCRC2_PRINT_frm.ChildBand20BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand20-------------------------------------------------------
  //전자서명(개설업체)
  ChildBand20.Height := ChildBand20.Tag;
  QR_APPLIC3.Caption := qrylistAPPLIC3.AsString;

  if Trim(QR_APPLIC3.Caption) = '' then
  begin
    ChildBand20.Enabled := False;
  end;
end;

end.
