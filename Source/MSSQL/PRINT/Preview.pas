unit Preview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, acImage, sPanel, QRPrntr, QuickRpt, QRCtrls, StdCtrls,
  sButton, Buttons, sSpeedButton, sEdit, sSpinEdit;

type
  TPreview_frm = class(TForm)
    QRPreview1: TQRPreview;
    sPanel1: TsPanel;
    sImage1: TsImage;
    sImage2: TsImage;
    sImage3: TsImage;
    sEdit1: TsEdit;
    sSpeedButton1: TsSpeedButton;
    sEdit2: TsEdit;
    sButton1: TsButton;
    sButton2: TsButton;
    sSpinEdit1: TsSpinEdit;
    sPanel2: TsPanel;
    sImage4: TsImage;
    sImage5: TsImage;
    sSpeedButton2: TsSpeedButton;
    procedure sImage1Click(Sender: TObject);
    procedure sImage3Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sSpinEdit1Change(Sender: TObject);
    procedure QRPreview1Click(Sender: TObject);
    procedure sImage4Click(Sender: TObject);
  private
    { Private declarations }
    FQR_REp :TQuickRep;
    FQRComposite_REP :TQRCompositeReport;
    procedure ButtonControl;
  public
    { Public declarations }
    procedure Preview;
    property Report:TQuickRep read FQR_REP write FQR_REP;
    property CompositeRepost : TQRCompositeReport read FQRComposite_REP write FQRComposite_REP;
  end;

var
  Preview_frm: TPreview_frm;

implementation

uses
  ICON;

{$R *.dfm}

{ TPRN_IMPORTCARGO_frm }

procedure TPreview_frm.Preview;
begin
  IF (FQR_REP = nil) AND (FQRComposite_REP = nil) Then
    raise Exception.Create('출력물을 연결하세요');

  IF FQR_REP <> nil Then
  begin
    FQR_REP.Prepare;
    sEdit1.Text := '1';
    sEdit2.Text := IntToStr(FQR_REP.QRPrinter.PageCount);

    ButtonControl;
  //  edt_CurrenctPage.Text := '1';
  //  edt_TotalPage.Text := IntToStr(FQR_REP.QRPrinter.PageCount);

    QRPreview1.QRPrinter := FQR_REP.QRPrinter;
    QRPreview1.ZoomToWidth;
    sSpinEdit1.Value := QRPreview1.Zoom;
  end
  else
  IF FQRComposite_REP <> nil Then
  begin
    FQRComposite_REP.Prepare;
    sEdit1.Text := '1';
    sEdit2.Text := IntToStr(TQuickRep(FQRComposite_REP.Reports[0]).QRPrinter.PageCount);

    ButtonControl;

    QRPreview1.QRPrinter := TQuickRep(FQRComposite_REP.Reports[0]).QRPrinter;
    QRPreview1.ZoomToWidth;
    sSpinEdit1.Value := QRPreview1.Zoom;
  end;
  Self.ShowModal;
end;

procedure TPreview_frm.sImage1Click(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TPreview_frm.sImage3Click(Sender: TObject);
begin
  IF FQR_REp <> nil Then
  begin
    FQR_REP.PrinterSetup;

    IF FQR_REP.Tag = 0 Then
      FQR_REP.Print;
  end
  else
  IF FQRComposite_REP <> nil Then
  begin
    FQRComposite_REP.PrinterSetup;
    IF FQRComposite_REP.Tag = 0 Then
      FQRComposite_REP.Print;
  end;
end;

procedure TPreview_frm.sButton2Click(Sender: TObject);
begin
  QRPreview1.PageNumber := QRPreview1.PageNumber+1;
  sEdit1.Text := IntToStr( QRPreview1.PageNumber );
  ButtonControl;
end;

procedure TPreview_frm.ButtonControl;
begin
  sButton1.Enabled := QRPreview1.PageNumber > 1;
  IF FQR_REp <> nil Then
    sButton2.Enabled := QRPreview1.PageNumber < FQR_REP.QRPrinter.PageCount
  else
  IF FQRComposite_REP <> nil Then
    sButton2.Enabled := QRPreview1.PageNumber < TQuickRep(FQRComposite_REP.Reports[0]).QRPrinter.PageCount;
end;

procedure TPreview_frm.sButton1Click(Sender: TObject);
begin
  QRPreview1.PageNumber := QRPreview1.PageNumber-1;
  sEdit1.Text := IntToStr( QRPreview1.PageNumber );
  ButtonControl;
end;

procedure TPreview_frm.sSpinEdit1Change(Sender: TObject);
begin
  QRPreview1.Zoom := sSpinEdit1.Value;
end;

procedure TPreview_frm.QRPreview1Click(Sender: TObject);
begin
  sSpinEdit1.SetFocus;
end;

procedure TPreview_frm.sImage4Click(Sender: TObject);
begin
  Case (Sender as TsImage).Tag of
    0: QRPreview1.ZoomToWidth;
    1: QRPreview1.ZoomTofit;
  end;

  sSpinEdit1.Value := QRPreview1.Zoom;
end;

end.
