unit QR_INF700_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, Dialogs, DB, ADODB, StrUtils;

type
  TQR_INF700_PRN_frm = class(TQuickRep)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel39: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRChildBand1: TQRChildBand;
    QRChildBand2: TQRChildBand;
    QRChildBand3: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel2: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC_TEL: TQRLabel;
    QR_AP_BANK: TQRLabel;
    QR_AP_BANK1: TQRLabel;
    QR_AP_BANK_TEL: TQRLabel;
    QR_AD_BANK: TQRLabel;
    QR_AD_BANK1: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QR_APPDATE: TQRLabel;
    QR_IN_MATHOD: TQRLabel;
    QR_ADPAY: TQRLabel;
    M_ADINFO: TQRMemo;
    QRLabel26: TQRLabel;
    QRLabel14: TQRLabel;
    qryINF700: TADOQuery;
    qryINF700MAINT_NO: TStringField;
    qryINF700MESSAGE1: TStringField;
    qryINF700MESSAGE2: TStringField;
    qryINF700USER_ID: TStringField;
    qryINF700DATEE: TStringField;
    qryINF700APP_DATE: TStringField;
    qryINF700IN_MATHOD: TStringField;
    qryINF700AP_BANK: TStringField;
    qryINF700AP_BANK1: TStringField;
    qryINF700AP_BANK2: TStringField;
    qryINF700AP_BANK3: TStringField;
    qryINF700AP_BANK4: TStringField;
    qryINF700AP_BANK5: TStringField;
    qryINF700AD_BANK: TStringField;
    qryINF700AD_BANK1: TStringField;
    qryINF700AD_BANK2: TStringField;
    qryINF700AD_BANK3: TStringField;
    qryINF700AD_BANK4: TStringField;
    qryINF700AD_PAY: TStringField;
    qryINF700IL_NO1: TStringField;
    qryINF700IL_NO2: TStringField;
    qryINF700IL_NO3: TStringField;
    qryINF700IL_NO4: TStringField;
    qryINF700IL_NO5: TStringField;
    qryINF700IL_AMT1: TBCDField;
    qryINF700IL_AMT2: TBCDField;
    qryINF700IL_AMT3: TBCDField;
    qryINF700IL_AMT4: TBCDField;
    qryINF700IL_AMT5: TBCDField;
    qryINF700IL_CUR1: TStringField;
    qryINF700IL_CUR2: TStringField;
    qryINF700IL_CUR3: TStringField;
    qryINF700IL_CUR4: TStringField;
    qryINF700IL_CUR5: TStringField;
    qryINF700AD_INFO1: TStringField;
    qryINF700AD_INFO2: TStringField;
    qryINF700AD_INFO3: TStringField;
    qryINF700AD_INFO4: TStringField;
    qryINF700AD_INFO5: TStringField;
    qryINF700DOC_CD: TStringField;
    qryINF700CD_NO: TStringField;
    qryINF700REF_PRE: TStringField;
    qryINF700ISS_DATE: TStringField;
    qryINF700EX_DATE: TStringField;
    qryINF700EX_PLACE: TStringField;
    qryINF700CHK1: TStringField;
    qryINF700CHK2: TStringField;
    qryINF700CHK3: TStringField;
    qryINF700prno: TIntegerField;
    qryINF700F_INTERFACE: TStringField;
    qryINF700IMP_CD1: TStringField;
    qryINF700IMP_CD2: TStringField;
    qryINF700IMP_CD3: TStringField;
    qryINF700IMP_CD4: TStringField;
    qryINF700IMP_CD5: TStringField;
    qryINF700MAINT_NO_1: TStringField;
    qryINF700APP_BANK: TStringField;
    qryINF700APP_BANK1: TStringField;
    qryINF700APP_BANK2: TStringField;
    qryINF700APP_BANK3: TStringField;
    qryINF700APP_BANK4: TStringField;
    qryINF700APP_BANK5: TStringField;
    qryINF700APP_ACCNT: TStringField;
    qryINF700APPLIC1: TStringField;
    qryINF700APPLIC2: TStringField;
    qryINF700APPLIC3: TStringField;
    qryINF700APPLIC4: TStringField;
    qryINF700APPLIC5: TStringField;
    qryINF700BENEFC1: TStringField;
    qryINF700BENEFC2: TStringField;
    qryINF700BENEFC3: TStringField;
    qryINF700BENEFC4: TStringField;
    qryINF700BENEFC5: TStringField;
    qryINF700CD_AMT: TBCDField;
    qryINF700CD_CUR: TStringField;
    qryINF700CD_PERP: TBCDField;
    qryINF700CD_PERM: TBCDField;
    qryINF700CD_MAX: TStringField;
    qryINF700AA_CV1: TStringField;
    qryINF700AA_CV2: TStringField;
    qryINF700AA_CV3: TStringField;
    qryINF700AA_CV4: TStringField;
    qryINF700AVAIL: TStringField;
    qryINF700AVAIL1: TStringField;
    qryINF700AVAIL2: TStringField;
    qryINF700AVAIL3: TStringField;
    qryINF700AVAIL4: TStringField;
    qryINF700AV_ACCNT: TStringField;
    qryINF700AV_PAY: TStringField;
    qryINF700DRAFT1: TStringField;
    qryINF700DRAFT2: TStringField;
    qryINF700DRAFT3: TStringField;
    qryINF700DRAWEE: TStringField;
    qryINF700DRAWEE1: TStringField;
    qryINF700DRAWEE2: TStringField;
    qryINF700DRAWEE3: TStringField;
    qryINF700DRAWEE4: TStringField;
    qryINF700DR_ACCNT: TStringField;
    qryINF700MAINT_NO_2: TStringField;
    qryINF700PSHIP: TStringField;
    qryINF700TSHIP: TStringField;
    qryINF700LOAD_ON: TStringField;
    qryINF700FOR_TRAN: TStringField;
    qryINF700LST_DATE: TStringField;
    qryINF700SHIP_PD: TBooleanField;
    qryINF700SHIP_PD1: TStringField;
    qryINF700SHIP_PD2: TStringField;
    qryINF700SHIP_PD3: TStringField;
    qryINF700SHIP_PD4: TStringField;
    qryINF700SHIP_PD5: TStringField;
    qryINF700SHIP_PD6: TStringField;
    qryINF700DESGOOD: TBooleanField;
    qryINF700DESGOOD_1: TMemoField;
    qryINF700TERM_PR: TStringField;
    qryINF700TERM_PR_M: TStringField;
    qryINF700PL_TERM: TStringField;
    qryINF700ORIGIN: TStringField;
    qryINF700ORIGIN_M: TStringField;
    qryINF700DOC_380: TBooleanField;
    qryINF700DOC_380_1: TBCDField;
    qryINF700DOC_705: TBooleanField;
    qryINF700DOC_705_1: TStringField;
    qryINF700DOC_705_2: TStringField;
    qryINF700DOC_705_3: TStringField;
    qryINF700DOC_705_4: TStringField;
    qryINF700DOC_740: TBooleanField;
    qryINF700DOC_740_1: TStringField;
    qryINF700DOC_740_2: TStringField;
    qryINF700DOC_740_3: TStringField;
    qryINF700DOC_740_4: TStringField;
    qryINF700DOC_530: TBooleanField;
    qryINF700DOC_530_1: TStringField;
    qryINF700DOC_530_2: TStringField;
    qryINF700DOC_271: TBooleanField;
    qryINF700DOC_271_1: TBCDField;
    qryINF700DOC_861: TBooleanField;
    qryINF700DOC_2AA: TBooleanField;
    qryINF700DOC_2AA_1: TMemoField;
    qryINF700ACD_2AA: TBooleanField;
    qryINF700ACD_2AA_1: TStringField;
    qryINF700ACD_2AB: TBooleanField;
    qryINF700ACD_2AC: TBooleanField;
    qryINF700ACD_2AD: TBooleanField;
    qryINF700ACD_2AE: TBooleanField;
    qryINF700ACD_2AE_1: TMemoField;
    qryINF700CHARGE: TStringField;
    qryINF700PERIOD: TBCDField;
    qryINF700CONFIRMM: TStringField;
    qryINF700DEF_PAY1: TStringField;
    qryINF700DEF_PAY2: TStringField;
    qryINF700DEF_PAY3: TStringField;
    qryINF700DEF_PAY4: TStringField;
    qryINF700DOC_705_GUBUN: TStringField;
    qryINF700MAINT_NO_3: TStringField;
    qryINF700REI_BANK: TStringField;
    qryINF700REI_BANK1: TStringField;
    qryINF700REI_BANK2: TStringField;
    qryINF700REI_BANK3: TStringField;
    qryINF700REI_BANK4: TStringField;
    qryINF700REI_BANK5: TStringField;
    qryINF700REI_ACNNT: TStringField;
    qryINF700INSTRCT: TBooleanField;
    qryINF700INSTRCT_1: TMemoField;
    qryINF700AVT_BANK: TStringField;
    qryINF700AVT_BANK1: TStringField;
    qryINF700AVT_BANK2: TStringField;
    qryINF700AVT_BANK3: TStringField;
    qryINF700AVT_BANK4: TStringField;
    qryINF700AVT_BANK5: TStringField;
    qryINF700AVT_ACCNT: TStringField;
    qryINF700SND_INFO1: TStringField;
    qryINF700SND_INFO2: TStringField;
    qryINF700SND_INFO3: TStringField;
    qryINF700SND_INFO4: TStringField;
    qryINF700SND_INFO5: TStringField;
    qryINF700SND_INFO6: TStringField;
    qryINF700EX_NAME1: TStringField;
    qryINF700EX_NAME2: TStringField;
    qryINF700EX_NAME3: TStringField;
    qryINF700EX_ADDR1: TStringField;
    qryINF700EX_ADDR2: TStringField;
    qryINF700EX_ADDR3: TStringField;
    qryINF700OP_BANK1: TStringField;
    qryINF700OP_BANK2: TStringField;
    qryINF700OP_BANK3: TStringField;
    qryINF700OP_ADDR1: TStringField;
    qryINF700OP_ADDR2: TStringField;
    qryINF700OP_ADDR3: TStringField;
    qryINF700MIX_PAY1: TStringField;
    qryINF700MIX_PAY2: TStringField;
    qryINF700MIX_PAY3: TStringField;
    qryINF700MIX_PAY4: TStringField;
    qryINF700APPLICABLE_RULES_1: TStringField;
    qryINF700APPLICABLE_RULES_2: TStringField;
    qryINF700DOC_760: TBooleanField;
    qryINF700DOC_760_1: TStringField;
    qryINF700DOC_760_2: TStringField;
    qryINF700DOC_760_3: TStringField;
    qryINF700DOC_760_4: TStringField;
    qryINF700SUNJUCK_PORT: TStringField;
    qryINF700DOCHACK_PORT: TStringField;
    qryINF700mathod_Name: TStringField;
    qryINF700pay_Name: TStringField;
    qryINF700Imp_Name_1: TStringField;
    qryINF700Imp_Name_2: TStringField;
    qryINF700Imp_Name_3: TStringField;
    qryINF700Imp_Name_4: TStringField;
    qryINF700Imp_Name_5: TStringField;
    qryINF700DOC_Name: TStringField;
    qryINF700CDMAX_Name: TStringField;
    qryINF700pship_Name: TStringField;
    qryINF700tship_Name: TStringField;
    qryINF700doc705_Name: TStringField;
    qryINF700doc740_Name: TStringField;
    qryINF700doc760_Name: TStringField;
    qryINF700CHARGE_Name: TStringField;
    qryINF700CONFIRM_Name: TStringField;
    qryINF700CHARGE_1: TMemoField;
    qryINF700CONFIRM_BICCD: TStringField;
    qryINF700CONFIRM_BANKNM: TStringField;
    qryINF700CONFIRM_BANKBR: TStringField;
    qryINF700PERIOD_IDX: TIntegerField;
    qryINF700PERIOD_TXT: TStringField;
    qryINF700SPECIAL_PAY: TMemoField;
    qryINF700AVAIL_BIC: TStringField;
    qryINF700DRAWEE_BIC: TStringField;
    qryINF700MSEQ: TIntegerField;
    QRImage2: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRChildBand4: TQRChildBand;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRChildBand5: TQRChildBand;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRChildBand6: TQRChildBand;
    QRLabel29: TQRLabel;
    QRChildBand7: TQRChildBand;
    QRLabel30: TQRLabel;
    QRChildBand8: TQRChildBand;
    QRLabel31: TQRLabel;
    QRChildBand9: TQRChildBand;
    QRChildBand10: TQRChildBand;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRChildBand11: TQRChildBand;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel40: TQRLabel;
    QRChildBand12: TQRChildBand;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRChildBand13: TQRChildBand;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRChildBand14: TQRChildBand;
    QRLabel47: TQRLabel;
    QRChildBand15: TQRChildBand;
    QRLabel43: TQRLabel;
    DetailBand1: TQRBand;
    QRChildBand16: TQRChildBand;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    SummaryBand1: TQRBand;
    QRChildBand17: TQRChildBand;
    QRLabel48: TQRLabel;
    QRChildBand18: TQRChildBand;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRChildBand19: TQRChildBand;
    QRLabel56: TQRLabel;
    QRChildBand20: TQRChildBand;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRChildBand21: TQRChildBand;
    QRLabel59: TQRLabel;
    QRChildBand22: TQRChildBand;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRChildBand23: TQRChildBand;
    QRLabel63: TQRLabel;
    QRChildBand24: TQRChildBand;
    QRLabel64: TQRLabel;
    QR_DOCCD: TQRLabel;
    QR_LCNO: TQRLabel;
    QR_REFPRE: TQRLabel;
    QR_ISSDATE: TQRLabel;
    QR_APPRULES: TQRLabel;
    QR_EXDATE: TQRLabel;
    M_APBANK: TQRMemo;
    M_APPLIC: TQRMemo;
    M_BENEFIC: TQRMemo;
    QR_CDCUR: TQRLabel;
    QR_PCD: TQRLabel;
    M_AACV: TQRMemo;
    M_AVAIL: TQRMemo;
    M_DRAFT: TQRMemo;
    M_DRAWEE: TQRMemo;
    M_MIX: TQRMemo;
    M_DEFPAY: TQRMemo;
    QR_PSHIP: TQRLabel;
    QR_TSHIP: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QR_FORTRAN: TQRMemo;
    QR_SUNJUK: TQRLabel;
    QR_DOCHAK: TQRLabel;
    QR_LOADON: TQRLabel;
    QR_LATEST: TQRLabel;
    QR_SHIPPERIOD: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    M_ADD: TQRMemo;
    M_SPECIAL: TQRMemo;
    QR_PERIOD: TQRLabel;
    QRLabel53: TQRLabel;
    M_CHARGE: TQRMemo;
    QR_CONFIRM: TQRLabel;
    M_CONFIRMBK: TQRMemo;
    M_REIBK: TQRMemo;
    M_INSTBK: TQRMemo;
    M_ADVBK: TQRMemo;
    M_SNDINFO: TQRMemo;
    QRChildBand25: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel69: TQRLabel;
    QRChildBand26: TQRChildBand;
    QRChildBand27: TQRChildBand;
    QRChildBand28: TQRChildBand;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QR_EXADDR: TQRLabel;
    QR_OPBANK1: TQRLabel;
    QR_OPBANK2: TQRLabel;
    QR_OPBANK3: TQRLabel;
    QR_OPADDR: TQRLabel;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand14BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand15BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRepAfterPrint(Sender: TObject);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand18BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand19BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand20BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand21BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand22BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand23BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand24BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand26BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBand27BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FDETAIL_COUNT : integer;
    FGOODS : TStringList;
    FSUM_HEIGHT : integer;
    FMAINT_NO : String;
    FMSEQ: String;
    procedure RecordHeight(nValue : integer);
    procedure SetMSEQ(const Value: String);
  public
     property MAINT_NO:string read FMAINT_NO write FMAINT_NO;
     property MSEQ:String read FMSEQ write SetMSEQ;
  end;

var
  QR_INF700_PRN_frm: TQR_INF700_PRN_frm;
Const
  RowHeight : integer = 13;
  IndentHeight : integer = 4;
  DOC_FLAG : String = 'DOC_FLAG';
  ADD_FLAG : String = 'ADD_FLAG';
implementation

{$R *.DFM}

{ TQR_INF700_PRN_frm }

procedure TQR_INF700_PRN_frm.RecordHeight(nValue: integer);
begin
  IF nValue > 0 Then
    FSUM_HEIGHT := FSUM_HEIGHT+nValue-13
  else
    FSUM_HEIGHT := FSUM_HEIGHT-13-IndentHeight;
end;

procedure TQR_INF700_PRN_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
var
  TMP_STR : String;
begin
  FSUM_HEIGHT := 0;
  FDETAIL_COUNT := 0;
  FGOODS := TStringList.Create;

  qryINF700.Close;
  qryINF700.Parameters[0].Value := FMAINT_NO;
  qryINF700.Parameters[1].Value := FMSEQ;
  qryINF700.Open;

  FGOODS.Clear;
  IF Trim(qryINF700TERM_PR.AsString) <> '' Then
  begin
    IF Trim(qryINF700PL_TERM.AsString) <> '' Then
      FGOODS.Add('+TERMS OF PRICE : '+qryINF700TERM_PR.AsString+' '+qryINF700PL_TERM.AsString)
    else
      FGOODS.Add('+TERMS OF PRICE : '+qryINF700TERM_PR.AsString);
  end
  else
  IF Trim(qryINF700TERM_PR_M.AsString) <> '' Then
  begin
    IF Trim(qryINF700PL_TERM.AsString) <> '' Then
      FGOODS.Add('+TERMS OF PRICE : '+qryINF700TERM_PR_M.AsString+' '+qryINF700PL_TERM.AsString)
    else
      FGOODS.Add('+TERMS OF PRICE : '+qryINF700TERM_PR_M.AsString);
  end;

  if Trim(qryINF700ORIGIN.AsString) <> '' Then
  begin
    IF Trim(qryINF700ORIGIN_M.AsString) <> '' Then
      FGOODS.Add('+COUNTRY OF ORIGIN : '+qryINF700ORIGIN.AsString+' '+qryINF700ORIGIN_M.AsString)
    else
      FGOODS.Add('+COUNTRY OF ORIGIN : '+qryINF700ORIGIN.AsString);
  end;
  FGOODS.Text := FGOODS.Text+qryINF700DESGOOD_1.AsString;
  
  //----------------------------------------------------------------------------
  IF qryINF700DOC_380.AsBoolean OR qryINF700DOC_705.AsBoolean OR qryINF700DOC_740.AsBoolean OR
     qryINF700DOC_760.AsBoolean OR qryINF700DOC_530.AsBoolean OR qryINF700DOC_271.AsBoolean OR
     qryINF700DOC_861.AsBoolean OR qryINF700DOC_2AA.AsBoolean Then
  begin
    FGOODS.Add(DOC_FLAG);
  end;

  IF qryINF700DOC_380.AsBoolean Then
  begin
    TMP_STR := '+SIGNED COMMERCIAL INVOICE IN '+IntToStr(qryINF700DOC_380_1.AsInteger)+' COPIES';
    FGOODS.Add(TMP_STR);
  end;
  //----------------------------------------------------------------------------
  IF qryINF700DOC_705.AsBoolean Then
  begin
    Case StrToIntDef(qryINF700DOC_705_GUBUN.AsString, -1) of
      705,706 : TMP_STR := 'FULL SET';
      707 : TMP_STR := '1/3 SET';
      717 : TMP_STR := '2/3 SET';
      718 : TMP_STR := 'COPY';
    end;
    FGOODS.Add( '+'+TMP_STR+' OF CLEAN ON BOARD OCEAN BILLS OF LADING MADE OUT TO THE' );
    FGOODS.Add( 'ORDER OF '+Trim((qryINF700DOC_705_1.AsString)+' '+Trim(qryINF700DOC_705_2.AsString)) );
    Case AnsiIndexText(qryINF700DOC_705_3.AsString, ['31', '32']) of
      0: TMP_STR := 'PREPAID';
      1: TMP_STR := 'COLLECT';
    end;
    FGOODS.Add( 'MARKED FREIGHT '+TMP_STR+' AND NOTIFY '+qryINF700DOC_705_4.AsString);
  end;
  //----------------------------------------------------------------------------
  IF qryINF700DOC_740.AsBoolean Then
  begin
    FGOODS.Add('+AIR WAYBILL CONSIGNED TO '+Trim((qryINF700DOC_740_1.AsString)+' '+Trim(qryINF700DOC_740_2.AsString)));
    Case AnsiIndexText(qryINF700DOC_740_3.AsString, ['31', '32']) of
      0: TMP_STR := 'PREPAID';
      1: TMP_STR := 'COLLECT';
    end;
    FGOODS.Add( 'MARKED FREIGHT '+TMP_STR+' AND NOTIFY '+qryINF700DOC_740_4.AsString);
  end;
  //----------------------------------------------------------------------------
  IF qryINF700DOC_760.AsBoolean Then
  begin
    FGOODS.Add( '+FULL SET OF CLEAN MULTIMODAL TRANSPORT DOCUMENT MADE OUT TO THE' );
    FGOODS.Add( 'ORDER OF '+Trim((qryINF700DOC_760_1.AsString)+' '+Trim(qryINF700DOC_760_2.AsString)) );
    Case AnsiIndexText(qryINF700DOC_760_3.AsString, ['31', '32']) of
      0: TMP_STR := 'PREPAID';
      1: TMP_STR := 'COLLECT';
    end;
    FGOODS.Add( 'MARKED FREIGHT '+TMP_STR+' AND NOTIFY '+qryINF700DOC_760_4.AsString);
  end;
  //----------------------------------------------------------------------------
  IF qryINF700DOC_530.AsBoolean Then
  begin
    FGOODS.Add( '+FULL SET OF INSURANCE POLICES OF CERTIFICATES, ENDORSED IN BLANK' );
    FGOODS.Add( 'FOR 110% OF THE INVOICE VALUE, EXPRESSLY STIPULATING THAT CLAIMS' );
    FGOODS.Add( 'ARE PAYABLE IN KOREA AND IT MUST INCLUDE : INSTITUTE CARGO CLAUSE' );
    if Trim(qryINF700DOC_530_1.AsString) <> '' Then
      FGOODS.Add( Trim(qryINF700DOC_530_1.AsString) );
    if Trim(qryINF700DOC_530_2.AsString) <> '' Then
      FGOODS.Add( Trim(qryINF700DOC_530_2.AsString) );
  end;
  //----------------------------------------------------------------------------
  IF qryINF700DOC_271.AsBoolean Then
    FGOODS.Add( '+PACKING LIST IN '+IntToStr(qryINF700DOC_271_1.AsInteger)+' COPIES' );
  //----------------------------------------------------------------------------
  IF qryINF700DOC_861.AsBoolean Then
    FGOODS.Add( '+CERTIFICATE OF ORIGIN' );
  //----------------------------------------------------------------------------
  if qryINF700DOC_2AA.AsBoolean Then
    FGOODS.Text := FGOODS.Text+'+'+qryINF700DOC_2AA_1.AsString;

  //============================================================================
  IF qryINF700ACD_2AA.AsBoolean OR qryINF700ACD_2AB.AsBoolean OR qryINF700ACD_2AC.AsBoolean OR
     qryINF700ACD_2AD.AsBoolean OR qryINF700ACD_2AE.AsBoolean Then
  begin
    FGOODS.Add(ADD_FLAG);
  end;

  IF qryINF700ACD_2AA.AsBoolean Then
    FGOODS.Add( '+SHIPMENT BY '+Trim(qryINF700ACD_2AA_1.AsString) );
  //----------------------------------------------------------------------------
  IF qryINF700ACD_2AB.AsBoolean Then
    FGOODS.Add('+ACCEPTANCE COMMISSION& DISCOUNT CHARGES ARE FOR BUYER`S ACCOUNT');
  //----------------------------------------------------------------------------
  IF qryINF700ACD_2AC.AsBoolean Then
    FGOODS.Add('+ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER');
  //----------------------------------------------------------------------------
  IF qryINF700ACD_2AD.AsBoolean Then
    FGOODS.Add('+LATE PRESENTATION B/L ACCEPTABLE');
  //----------------------------------------------------------------------------
  if qryINF700ACD_2AE.AsBoolean Then
  begin
    FGOODS.Add('+OTHER ADDITIONAL CONDITIONS');
    FGOODS.Text := FGOODS.Text + qryINF700ACD_2AE_1.AsString;
  end;
end;

procedure TQR_INF700_PRN_frm.QRChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i : Integer;
  TMP_STR : String;
begin
  QRChildBand1.Height := QRChildBand1.Tag;

  QR_MAINTNO.Caption := qryINF700MAINT_NO.AsString;
  QR_APPDATE.Caption := LeftStr( qryINF700APP_DATE.AsString , 4)+'-'+MidStr( qryINF700APP_DATE.AsString , 5, 2)+'-'+RightStr( qryINF700APP_DATE.AsString , 2);
  QR_IN_MATHOD.Caption := qryINF700mathod_Name.AsString;
  QR_ADPAY.Caption := qryINF700pay_Name.AsString;

  QR_APPLIC1.Caption := Trim(Trim(qryINF700APPLIC1.AsString)+' '+Trim(qryINF700APPLIC2.AsString));
  QR_APPLIC2.Caption := Trim(Trim(qryINF700APPLIC3.AsString)+' '+Trim(qryINF700APPLIC4.AsString));
  QR_APPLIC2.Enabled := QR_APPLIC2.Caption <> '';
  QR_APPLIC_TEL.Caption := Trim( qryINF700APPLIC5.AsString );
  QR_APPLIC_TEL.Enabled := QR_APPLIC_TEL.Caption <> '';

  QR_AP_BANK.Caption  := qryINF700AP_BANK.AsString;
  QR_AP_BANK1.Caption := Trim((qryINF700AP_BANK1.AsString)+' '+Trim(qryINF700AP_BANK2.AsString)+' '+Trim(qryINF700AP_BANK3.AsString)+' '+Trim(qryINF700AP_BANK4.AsString));
  QR_AP_BANK_TEL.Caption := Trim(qryINF700AP_BANK5.AsString);
  QR_AP_BANK_TEL.Enabled := Trim(qryINF700AP_BANK5.AsString) <> '';

  QR_AD_BANK.Caption := qryINF700AD_BANK.AsString;
  QR_AD_BANK1.Caption := Trim((qryINF700AD_BANK1.AsString)+' '+Trim(qryINF700AD_BANK2.AsString)+' '+Trim(qryINF700AD_BANK3.AsString)+' '+Trim(qryINF700AD_BANK4.AsString));

  QRShape4.Top := 297;
  QRShape11.Height  := 274;
  QRShape13.Height := 274;
  QRShape12.Height := 274;
  QRLabel13.Top := 279;

  M_ADINFO.Lines.Clear;
  for i := 1 to 5 do
  begin
    TMP_STR := Trim(qryINF700.FieldByName('AD_INFO'+IntToStr(i)).AsString);
    IF TMP_STR <> '' Then
      M_ADINFO.Lines.Add(TMP_STR);
  end;
  M_ADINFO.Height := M_ADINFO.Lines.Count * RowHeight;
  IF M_ADINFO.Height > 13 Then
  begin
    QRShape4.Top := QRShape4.Top + M_ADINFO.Height - 13;
    QRShape11.Height  := QRShape4.Top-23;
    QRShape13.Height := QRShape11.Height;
    QRShape12.Height := QRShape11.Height;
    QRChildBand1.Height := QRChildBand1.Height + M_ADINFO.Height;
    QRLabel13.Top := QRLabel13.Top + ((QRShape4.Top - QRShape2.Top) div 2) - QRLabel13.Height;    
//    QRLabel13.Top := QRLabel13.Top + ((QRShape4.Top - QRShape2.Top) div 2) - (QRLabel13.Height div 2);

  end;

end;

procedure TQR_INF700_PRN_frm.QRChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  FSUM_HEIGHT := 0;

  QRLabel16.Top := 24;
  QRLabel17.Top := QRLabel16.Top + (1 * 16);
  QRLabel18.Top := QRLabel16.Top + (2 * 16);
  QRLabel19.Top := QRLabel16.Top + (3 * 16);
  QRLabel20.Top := QRLabel16.Top + (4 * 16);
  QRLabel21.Top := QRLabel16.Top + (5 * 16);

  QRChildBand2.Height := QRChildBand2.Tag;

  QR_DOCCD.Caption := qryINF700DOC_Name.AsString;

  QR_LCNO.Caption := qryINF700CD_NO.AsString;

  QR_REFPRE.Caption := Trim(qryINF700REF_PRE.AsString);
  QR_REFPRE.Enabled := QR_REFPRE.Caption <> '';
  QRLabel18.Enabled := QR_REFPRE.Enabled;
  IF not QR_REFPRE.Enabled then
    RecordHeight(0);

  QRLabel19.Top := QRLabel19.Top + FSUM_HEIGHT;
  QR_ISSDATE.Top := QRLabel19.Top;
  QR_ISSDATE.Caption := RightStr( qryINF700ISS_DATE.AsString ,6 );

  QRLabel20.Top := QRLabel20.Top + FSUM_HEIGHT;
  QR_APPRULES.Top := QRLabel20.Top;
  IF Trim(qryINF700APPLICABLE_RULES_1.AsString) = 'OTHR' Then
    QR_APPRULES.Caption := qryINF700APPLICABLE_RULES_2.AsString
  else
    QR_APPRULES.Caption := qryINF700APPLICABLE_RULES_1.AsString;

  QRLabel21.Top := QRLabel21.Top + FSUM_HEIGHT;
  QR_EXDATE.Top := QRLabel21.Top;
  QR_EXDATE.Caption := RightStr(qryINF700EX_DATE.AsString,6)+' '+qryINF700EX_PLACE.AsString;

  QRChildBand2.Height := QRChildBand2.Height + FSUM_HEIGHT;
end;

procedure TQR_INF700_PRN_frm.QRChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : string;
begin
  QRChildBand3.Height := QRChildBand3.Tag;
  M_APBANK.lines.clear;
  M_APBANK.Top := 1;

  TMP_STR := Trim(Trim(qryINF700AP_BANK1.AsString) +' '+ Trim(qryINF700AP_BANK2.AsString));
  IF TMP_STR <> '' Then
    M_APBANK.Lines.Add(TMP_STR);
  TMP_STR := Trim(Trim(qryINF700AP_BANK3.AsString) +' '+ Trim(qryINF700AP_BANK4.AsString));
  IF TMP_STR <> '' Then
    M_APBANK.Lines.Add(TMP_STR);
  TMP_STR := Trim(qryINF700AP_BANK5.AsString);
  IF TMP_STR <> '' Then
    M_APBANK.Lines.Add(TMP_STR);

  M_APBANK.Height := M_APBANK.Lines.Count * RowHeight;
  QRChildBand3.Height := M_APBANK.Height + 4;
end;

procedure TQR_INF700_PRN_frm.QRChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i : Integer;
  TMP_STR : String;
begin
  QRChildBand4.Height := QRChildBand4.Tag;
  QRLabel23.Top := 4;
  QRLabel24.Top := QRLabel23.Top + (1 * 16);
  QRLabel25.Top := QRLabel23.Top + (2 * 16);

  M_APPLIC.lines.Clear;
  M_APPLIC.Top := 4;
  for i := 1 to 5 do
  begin
    TMP_STR := Trim(qryINF700.FieldByName('APPLIC'+IntToStr(i)).AsString);
    IF TMP_STR <> '' Then
      M_APPLIC.Lines.Add(TMP_STR);
  end;
  M_APPLIC.Height := M_APPLIC.Lines.Count * RowHeight;
  RecordHeight(M_APPLIC.Height);

  FSUM_HEIGHT := FSUM_HEIGHT + 10;

  QRLabel24.Top := QRLabel24.Top + FSUM_HEIGHT;
  M_BENEFIC.Top := QRLabel24.Top;
  M_BENEFIC.Lines.Clear;
  for i := 1 to 5 do
  begin
    TMP_STR := Trim(qryINF700.FieldByName('BENEFC'+IntToStr(i)).AsString);
    IF TMP_STR <> '' Then
      M_BENEFIC.Lines.Add(TMP_STR);
  end;
  M_BENEFIC.Height := M_BENEFIC.Lines.Count * RowHeight;
  RecordHeight(M_BENEFIC.Height);

  QRLabel25.Top := QRLabel25.Top + FSUM_HEIGHT;
  QR_CDCUR.Top := QRLabel25.Top;
  QR_CDCUR.Caption := qryINF700CD_CUR.AsString + ' ' +  FormatFloat('#,0.###', qryINF700CD_AMT.AsCurrency);

  QRChildBand4.Height := QRChildBand4.Height + FSUM_HEIGHT;
end;

procedure TQR_INF700_PRN_frm.QRChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRChildBand5.Enabled := not ((qryINF700CD_PERP.AsInteger = 0) and (qryINF700CD_PERM.AsInteger = 0));
  IF QRChildBand5.Enabled then
    QR_PCD.Caption := FormatFloat('0', qryINF700CD_PERP.AsInteger) + '/'+ FormatFloat('0', qryINF700CD_PERM.AsInteger);
end;

procedure TQR_INF700_PRN_frm.QRChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i : integer;
  TMP_STR : String;
begin
  QRChildBand6.Height := QRChildBand6.Tag;
  M_AACV.Lines.Clear;
  for i := 1 to 4 do
  begin
    TMP_STR := Trim(qryINF700.FieldByName('AA_CV'+IntToStr(i)).AsString);
    IF TMP_STR <> '' Then
      M_AACV.Lines.Add(TMP_STR);
  end;
  M_AACV.Height := M_AACV.Lines.Count * RowHeight;

  QRChildBand6.Enabled := M_AACV.Height > 0;
  QRChildBand6.Height := M_AACV.Height + 4;
end;

procedure TQR_INF700_PRN_frm.QRChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i : integer;
  TMP_STR : String;
begin
  QRChildBand7.Height := QRChildBand6.Tag;
  M_AVAIL.Lines.Clear;

  IF Trim(qryINF700AVAIL_BIC.AsString) <> '' Then
    M_AVAIL.Lines.Add(Trim(qryINF700AVAIL_BIC.AsString));

  TMP_STR := Trim((qryINF700AVAIL1.AsString)+' '+Trim(qryINF700AVAIL2.AsString));
  IF TMP_STR <> '' Then M_AVAIL.Lines.Add(TMP_STR);
  TMP_STR := Trim((qryINF700AVAIL3.AsString)+' '+Trim(qryINF700AVAIL4.AsString));
  IF TMP_STR <> '' Then M_AVAIL.Lines.Add(TMP_STR);
  IF Trim(qryINF700AV_ACCNT.AsString) <> '' Then
    M_AVAIL.Lines.Add(Trim(qryINF700AV_ACCNT.AsString));
  IF Trim(qryINF700AV_PAY.AsString) <> '' Then
    M_AVAIL.Lines.Add(Trim(qryINF700AV_PAY.AsString));

  M_AVAIL.Height := M_AVAIL.Lines.Count * RowHeight;

  QRChildBand7.Enabled := M_AVAIL.Height > 0;
  QRChildBand7.Height := M_AVAIL.Height + 4;
end;

procedure TQR_INF700_PRN_frm.QRChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i : integer;
  TMP_STR : String;
begin
  QRChildBand8.Height := QRChildBand6.Tag;
  M_DRAFT.Lines.Clear;
  for i := 1 to 3 do
  begin
    TMP_STR := Trim(qryINF700.FieldByName('DRAFT'+IntToStr(i)).AsString);
    IF TMP_STR <> '' Then
      M_DRAFT.Lines.Add(TMP_STR);
  end;
  M_DRAFT.Height := M_DRAFT.Lines.Count * RowHeight;

  QRChildBand8.Enabled := M_DRAFT.Height > 0;
  QRChildBand8.Height := M_DRAFT.Height + 4;
end;

procedure TQR_INF700_PRN_frm.QRChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i : integer;
  TMP_STR : String;
begin
  FSUM_HEIGHT := 0;
  
  QRChildBand9.Height := QRChildBand9.Tag;
  QRLabel32.Top := 1;
  QRLabel33.Top := QRLabel32.Top + (1 * 16);
  QRLabel34.Top := QRLabel32.Top + (2 * 16);

  M_DRAWEE.Lines.Clear;
  IF Trim( qryINF700DRAWEE_BIC.AsString ) <> '' Then
    M_DRAWEE.Lines.Add(Trim( qryINF700DRAWEE_BIC.AsString ));
  for i := 1 to 4 do
  begin
    TMP_STR := Trim(qryINF700.FieldByName('DRAWEE'+IntToStr(i)).AsString);
    IF TMP_STR <> '' Then
      M_DRAWEE.Lines.Add(TMP_STR);
  end;
  IF Trim( qryINF700DR_ACCNT.AsString ) <> '' Then
    M_DRAWEE.Lines.Add(Trim( qryINF700DR_ACCNT.AsString ));

  M_DRAWEE.Height := M_DRAWEE.Lines.Count * RowHeight;
  M_DRAWEE.Enabled := M_DRAWEE.Height > 0;
  RecordHeight(M_DRAWEE.Height);


  QRLabel33.Top := QRLabel33.Top + FSUM_HEIGHT;
  M_MIX.Top := QRLabel33.Top;
  for i := 1 to 4 do
  begin
    TMP_STR := Trim(qryINF700.FieldByName('MIX_PAY'+IntToStr(i)).AsString);
    IF TMP_STR <> '' Then
      M_MIX.Lines.Add(TMP_STR);
  end;
  M_MIX.Height := M_MIX.Lines.Count * RowHeight;
  M_MIX.Enabled := M_MIX.Height > 0;
  RecordHeight(M_MIX.Height);


  QRLabel34.Top := QRLabel34.Top + FSUM_HEIGHT;
  M_DEFPAY.Top := QRLabel34.Top;
  for i := 1 to 4 do
  begin
    TMP_STR := Trim(qryINF700.FieldByName('DEF_PAY'+IntToStr(i)).AsString);
    IF TMP_STR <> '' Then
      M_DEFPAY.Lines.Add(TMP_STR);
  end;
  M_DEFPAY.Height := M_DEFPAY.Lines.Count * RowHeight;
  M_DEFPAY.Enabled := M_DEFPAY.Height > 0;
  RecordHeight(M_DEFPAY.Height);

//  IF QRChildBand9.Tag > M_DRAWEE.Height + M_MIX.Height + M_DEFPAY.Height Then
//    QRChildBand9.Height := (M_DRAWEE.Height + M_MIX.Height + M_DEFPAY.Height)+4
//  else
//    QRChildBand9.Height := QRChildBand9.Height + FSUM_HEIGHT;
//  QRChildBand9.Enabled := M_DRAWEE.Enabled OR M_MIX.Enabled OR M_DEFPAY.Enabled;

    QRChildBand9.Height := QRChildBand9.Height + FSUM_HEIGHT;
end;
procedure TQR_INF700_PRN_frm.QRChildBand10BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QR_PSHIP.Caption := qryINF700pship_Name.AsString;
  QR_TSHIP.Caption := qryINF700tship_Name.AsString;
end;

procedure TQR_INF700_PRN_frm.QRChildBand11BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRChildBand11.Enabled := Trim(qryINF700LOAD_ON.AsString) <> '';
  QR_LOADON.Caption := Trim(qryINF700LOAD_ON.AsString);
end;

procedure TQR_INF700_PRN_frm.QRChildBand13BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRChildBand13.Enabled := Trim(qryINF700FOR_TRAN.AsString) <> '';
  QR_FORTRAN.Caption := Trim(qryINF700FOR_TRAN.AsString);
end;

procedure TQR_INF700_PRN_frm.QRChildBand12BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  POS_Y : Integer;  
begin
  QRChildBand12.Height := QRChildBand12.Tag;

  POS_Y := 0;
  QRLabel41.Top := 1;
  QRLabel42.Top := 16;
  QRLabel65.Top := 32;
  QRLabel66.Top := 47;

  QR_SUNJUK.Top := QRLabel41.Top;  
  QR_SUNJUK.Enabled := Trim(qryINF700SUNJUCK_PORT.AsString) <> '';
  QRLabel41.Enabled := QR_SUNJUK.Enabled;
  QRLabel42.Enabled := QR_SUNJUK.Enabled;
  QR_SUNJUK.Caption := Trim(qryINF700SUNJUCK_PORT.AsString);

  IF not QR_SUNJUK.Enabled Then POS_Y := -31;

  QR_DOCHAK.Top := QRLabel65.Top + POS_Y;
  QRLabel65.Top := QR_DOCHAK.Top + POS_Y;
  QRLabel66.Top := QR_DOCHAK.Top + POS_Y + 15;
  QR_DOCHAK.Enabled := Trim(qryINF700DOCHACK_PORT.AsString) <> '';
  QRLabel65.Enabled := QR_SUNJUK.Enabled;
  QRLabel66.Enabled := QR_SUNJUK.Enabled;
  QR_DOCHAK.Caption := Trim(qryINF700DOCHACK_PORT.AsString);

  QRChildBand12.Enabled := QR_SUNJUK.Enabled or QR_DOCHAK.Enabled;
  QRChildBand12.Height := QRChildBand12.Height + POS_Y;  
end;

procedure TQR_INF700_PRN_frm.QRChildBand14BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRChildBand14.Enabled := Trim(qryINF700LST_DATE.AsString) <> '';
  QR_LATEST.Caption := RightStr( Trim(qryINF700LST_DATE.AsString) , 6);
end;

procedure TQR_INF700_PRN_frm.QRChildBand15BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TMP_STR : string;
begin
  TMP_STR := Trim(qryINF700SHIP_PD1.AsString)+' '+Trim(qryINF700SHIP_PD2.AsString)+' '+Trim(qryINF700SHIP_PD3.AsString)+
             Trim(qryINF700SHIP_PD4.AsString)+' '+Trim(qryINF700SHIP_PD5.AsString);
  QRChildBand15.Enabled := Trim(TMP_STR) <> '';
  QR_LATEST.Caption := TMP_STR;
end;

procedure TQR_INF700_PRN_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := FDETAIL_COUNT <= FGOODS.Count-1;
end;

procedure TQR_INF700_PRN_frm.QuickRepAfterPrint(Sender: TObject);
begin
  FGOODS.Free;
end;

procedure TQR_INF700_PRN_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand := FGOODS.Strings[FDETAIL_COUNT] <> '';
  QRLabel68.Enabled := False;

  IF FGOODS.Strings[FDETAIL_COUNT] = DOC_FLAG Then
  begin
    QRLabel68.Caption := '46A  Documents Required               :';
    QRLabel68.Enabled := True;
    Inc(FDETAIL_COUNT);
    QRLabel67.Caption := FGOODS.Strings[FDETAIL_COUNT];
    Inc(FDETAIL_COUNT);
  end
  else
  IF FGOODS.Strings[FDETAIL_COUNT] = ADD_FLAG Then
  begin
    QRLabel68.Caption := '47A  Additional Conditions            :';
    QRLabel68.Enabled := True;
    Inc(FDETAIL_COUNT);
    QRLabel67.Caption := FGOODS.Strings[FDETAIL_COUNT];
    Inc(FDETAIL_COUNT);
  end
  else
  begin
    QRLabel67.Caption := FGOODS.Strings[FDETAIL_COUNT];
    Inc(FDETAIL_COUNT);
  end;
end;

procedure TQR_INF700_PRN_frm.QRChildBand16BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRLabel51.Caption := FGOODS.Strings[FDETAIL_COUNT];
  Inc(FDETAIL_COUNT);
  QRLabel52.Caption := FGOODS.Strings[FDETAIL_COUNT];
  Inc(FDETAIL_COUNT);
end;

procedure TQR_INF700_PRN_frm.QRChildBand18BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i : integer;
begin
  QRChildBand18.Height := QRChildBand18.Tag;
  M_SPECIAL.Lines.Clear;
  M_SPECIAL.Lines.Text := qryINF700SPECIAL_PAY.AsString;
  M_SPECIAL.Enabled := Trim(M_SPECIAL.Lines.Text) <> '';
  PrintBand := M_SPECIAL.Enabled;
  IF M_SPECIAL.Enabled Then
  begin
    M_SPECIAL.Height := M_SPECIAL.Lines.Count * RowHeight;
    IF M_SPECIAL.Height > QRChildBand18.Tag Then
      QRChildBand18.Height := M_SPECIAL.Height+5;
  end;

end;


procedure TQR_INF700_PRN_frm.QRChildBand19BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TMP_STR : String;
begin
  FSUM_HEIGHT := 0;
  QRLabel56.Top := 18;
  QRChildBand19.Height := QRChildBand19.Tag;

  M_CHARGE.Lines.Clear;
  //수수료 부담자
  case AnsiIndexText(UpperCase(Trim(qryINF700CHARGE.AsString)), ['2AF','2AG','2AH','16','19']) of
    0,3: TMP_STR := 'ALL BANKING COMMISSIONS AND CHARGES'#13#10+
                  'INCLUDING REIMBURSEMENT CHARGE'#13#10+
                  'OUTSIDE SOUTH KOREA ARE FOR ACCOUNT'#13#10+
                  'OF APPLICANT';
    1,4: TMP_STR := 'ALL BANKING COMMISSIONS AND CHARGES'#13#10+
                  'INCLUDING REIMBURSEMENT CHARGE'#13#10+
                  'OUTSIDE SOUTH KOREA ARE FOR ACCOUNT'#13#10+
                  'OF BENEFICIARY';
    2: TMP_STR := qryINF700CHARGE_1.AsString;
  else
     TMP_STR := '';
  end;

  M_CHARGE.Lines.Text := TMP_STR;
  M_CHARGE.Height := M_CHARGE.Lines.Count * RowHeight;
  RecordHeight(M_CHARGE.Height);

  QRLabel56.Top := QRLabel56.Top + FSUM_HEIGHT;
  QR_PERIOD.Top := QRLabel56.Top;
  QR_PERIOD.Caption := Trim(qryINF700PERIOD.AsString+' '+qryINF700PERIOD_TXT.AsString);

  QRChildBand19.Height := QRChildBand19.Height + FSUM_HEIGHT;

end;

procedure TQR_INF700_PRN_frm.QRChildBand20BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  FSUM_HEIGHT := 0;
  QRChildBand20.Height := QRChildBand20.Tag;
  QR_CONFIRM.Caption := qryINF700CONFIRM_Name.AsString;
  IF AnsiMatchText(qryINF700CONFIRMM.AsString, ['DB','DC']) Then
  begin
    M_CONFIRMBK.Lines.Clear;  
    M_CONFIRMBK.Enabled := True;
    QRLabel56.Enabled := True;
    IF Trim(qryINF700CONFIRM_BICCD.AsString) <> '' Then
      M_CONFIRMBK.Lines.Add(Trim(qryINF700CONFIRM_BICCD.AsString));
    IF Trim(qryINF700CONFIRM_BANKNM.AsString) <> '' Then
      M_CONFIRMBK.Lines.Add(Trim(qryINF700CONFIRM_BANKNM.AsString));
    IF Trim(qryINF700CONFIRM_BANKBR.AsString) <> '' Then
      M_CONFIRMBK.Lines.Add(Trim(qryINF700CONFIRM_BANKBR.AsString));
    M_CONFIRMBK.Height := M_CONFIRMBK.Lines.Count * RowHeight;
    RecordHeight(M_CONFIRMBK.Height);

    QRChildBand20.Height :=   QRChildBand20.Height + FSUM_HEIGHT;    
  end
  else
  begin
    M_CONFIRMBK.Enabled := False;
    QRLabel56.Enabled := False;
    QRChildBand20.Height := 17;
  end;


//  Case AnsiIndexText(qryListCONFIRMM.AsString, ['DA','DB','DC'] ) of
//    0: QR_CONFIRM.Caption := 'WITHOUT';
//    1: QR_CONFIRM.Caption := 'MAY ADD';
//    2: QR_CONFIRM.Caption := 'CONFIRM';
//  end;
end;

procedure TQR_INF700_PRN_frm.QRChildBand21BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TMP_STR : String;
begin
  FSUM_HEIGHT := 0;
  QRChildBand21.Height := QRChildBand21.Tag;
  M_REIBK.Lines.Clear;
  IF Trim(qryINF700REI_BANK.AsString) <> '' Then
    M_REIBK.Lines.Add(Trim(qryINF700REI_BANK.AsString));

  TMP_STR := Trim((qryINF700REI_BANK1.AsString)+' '+Trim(qryINF700REI_BANK2.AsString));
  IF TMP_STR <> '' Then
    M_REIBK.Lines.Add(TMP_STR);
  TMP_STR := Trim((qryINF700REI_BANK3.AsString)+' '+Trim(qryINF700REI_BANK4.AsString));
  IF TMP_STR <> '' Then
    M_REIBK.Lines.Add(TMP_STR);
  IF Trim(qryINF700REI_BANK5.AsString) <> '' Then
    M_REIBK.Lines.Add(Trim(qryINF700REI_BANK5.AsString));
  M_REIBK.Height := M_REIBK.Lines.Count * RowHeight;
  RecordHeight(M_REIBK.Height);

  QRChildBand21.Enabled := Trim(M_REIBK.lines.Text) <> '';
  QRChildBand21.Height := QRChildBand21.Height + FSUM_HEIGHT;
end;

procedure TQR_INF700_PRN_frm.QRChildBand22BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRChildBand22.Height := QRChildBand22.Tag;
  M_INSTBK.Lines.Clear;
  M_INSTBK.Lines.Text := qryINF700INSTRCT_1.AsString;
  M_INSTBK.Height := M_INSTBK.Lines.Count * RowHeight;

  IF QRChildBand22.Tag < M_INSTBK.Height Then
    QRChildBand22.Height := M_INSTBK.Height + 4;

end;

procedure TQR_INF700_PRN_frm.QRChildBand23BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TMP_STR : String;
begin
  FSUM_HEIGHT := 0;
  QRChildBand23.Height := QRChildBand23.Tag;
  M_ADVBK.Lines.Clear;
  IF Trim(qryINF700AVT_BANK.AsString) <> '' Then
    M_ADVBK.Lines.Add(Trim(qryINF700AVT_BANK.AsString));

  TMP_STR := Trim((qryINF700AVT_BANK1.AsString)+' '+Trim(qryINF700AVT_BANK2.AsString));
  IF TMP_STR <> '' Then
    M_ADVBK.Lines.Add(TMP_STR);
  TMP_STR := Trim((qryINF700AVT_BANK3.AsString)+' '+Trim(qryINF700AVT_BANK4.AsString));
  IF TMP_STR <> '' Then
    M_ADVBK.Lines.Add(TMP_STR);
  IF Trim(qryINF700AVT_ACCNT.AsString) <> '' Then
    M_ADVBK.Lines.Add(Trim(qryINF700AVT_ACCNT.AsString));
  M_ADVBK.Height := M_ADVBK.Lines.Count * RowHeight;
  RecordHeight(M_ADVBK.Height);

  QRChildBand23.Enabled := Trim(M_ADVBK.lines.Text) <> '';
  QRChildBand23.Height := QRChildBand23.Height + FSUM_HEIGHT;
end;

procedure TQR_INF700_PRN_frm.QRChildBand24BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  FSUM_HEIGHT := 0;
  QRChildBand24.Height := QRChildBand24.Tag;
  M_SNDINFO.Lines.Clear;
  IF Trim(qryINF700SND_INFO1.AsString) <> '' Then M_SNDINFO.Lines.Add(Trim(qryINF700SND_INFO1.AsString));
  IF Trim(qryINF700SND_INFO2.AsString) <> '' Then M_SNDINFO.Lines.Add(Trim(qryINF700SND_INFO3.AsString));
  IF Trim(qryINF700SND_INFO3.AsString) <> '' Then M_SNDINFO.Lines.Add(Trim(qryINF700SND_INFO3.AsString));

  M_SNDINFO.Height := M_SNDINFO.Lines.Count * RowHeight;
  RecordHeight(M_SNDINFO.Height);

  QRChildBand24.Enabled := Trim(M_SNDINFO.lines.Text) <> '';
  QRChildBand24.Height := QRChildBand24.Height + FSUM_HEIGHT;
end;

procedure TQR_INF700_PRN_frm.QRChildBand26BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRChildBand26.Height := QRChildBand26.Tag;
  QR_EXNAME1.Caption := qryINF700EX_NAME1.AsString;
  QR_EXNAME2.Caption := qryINF700EX_NAME2.AsString;
  QR_EXNAME3.Caption := qryINF700EX_NAME3.AsString;
  QR_EXADDR.Caption := Trim((qryINF700EX_ADDR1.AsString)+' '+Trim(qryINF700EX_ADDR2.AsString)+' '+Trim(qryINF700EX_ADDR3.AsString));
end;

procedure TQR_INF700_PRN_frm.QRChildBand27BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRChildBand27.Height := QRChildBand27.Tag;
  QR_OPBANK1.Caption := qryINF700OP_BANK1.AsString;
  QR_OPBANK2.Caption := qryINF700OP_BANK2.AsString;
  QR_OPBANK3.Caption := qryINF700OP_BANK3.AsString;
  QR_OPADDR.Caption := Trim((qryINF700OP_ADDR1.AsString)+' '+Trim(qryINF700OP_ADDR2.AsString)+' '+Trim(qryINF700OP_ADDR3.AsString));  
end;

procedure TQR_INF700_PRN_frm.SetMSEQ(const Value: String);
begin
  FMSEQ := Value;
end;

end.
