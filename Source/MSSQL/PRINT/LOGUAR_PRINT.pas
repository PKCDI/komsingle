unit LOGUAR_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TLOGUAR_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRImage3: TQRImage;
    QR_BANKTXT: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QR_LGNO: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel31: TQRLabel;
    QR_CRNAME1: TQRLabel;
    QR_CRNAME2: TQRLabel;
    QR_CRNAME3: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel3: TQRLabel;
    QR_SENAME1: TQRLabel;
    QR_SENAME2: TQRLabel;
    QR_SENAME3: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel8: TQRLabel;
    QR_LCNO: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel6: TQRLabel;
    QR_BLNO: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel9: TQRLabel;
    QR_BLDATE: TQRLabel;
    ChildBand6: TQRChildBand;
    QRLabel11: TQRLabel;
    QR_CARRIER1: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel13: TQRLabel;
    QR_CARRIER2: TQRLabel;
    ChildBand8: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_ARDATE: TQRLabel;
    ChildBand9: TQRChildBand;
    QRLabel17: TQRLabel;
    QR_LOADLOC: TQRLabel;
    ChildBand10: TQRChildBand;
    QRLabel19: TQRLabel;
    QR_ARRLOC: TQRLabel;
    QRImage1: TQRImage;
    ChildBand11: TQRChildBand;
    QRLabel24: TQRLabel;
    QR_INVAMT: TQRLabel;
    ChildBand12: TQRChildBand;
    QRLabel26: TQRLabel;
    QR_SPMARK1: TQRLabel;
    QR_SPMARK2: TQRLabel;
    QR_SPMARK3: TQRLabel;
    QR_SPMARK6: TQRLabel;
    QR_SPMARK5: TQRLabel;
    QR_SPMARK4: TQRLabel;
    QR_SPMARK9: TQRLabel;
    QR_SPMARK8: TQRLabel;
    QR_SPMARK7: TQRLabel;
    QR_SPMARK10: TQRLabel;
    ChildBand13: TQRChildBand;
    QRLabel38: TQRLabel;
    QR_PACQTY: TQRLabel;
    ChildBand14: TQRChildBand;
    QRLabel40: TQRLabel;
    QR_GOODS1: TQRLabel;
    QR_GOODS2: TQRLabel;
    QR_GOODS3: TQRLabel;
    QR_GOODS4: TQRLabel;
    QR_GOODS5: TQRLabel;
    QR_GOODS6: TQRLabel;
    QR_GOODS7: TQRLabel;
    QR_GOODS8: TQRLabel;
    QR_GOODS9: TQRLabel;
    QR_GOODS10: TQRLabel;
    ChildBand15: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel51: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel21: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel63: TQRLabel;
    QR_APPDATE: TQRLabel;
    QR_MSNAME1: TQRLabel;
    QRLabel67: TQRLabel;
    QR_MSNAME2: TQRLabel;
    QR_MSNAME3: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel25: TQRLabel;
    QR_LGDATE: TQRLabel;
    QR_AXNAME1: TQRLabel;
    QR_AXNAME2: TQRLabel;
    QR_AXNAME3: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape2: TQRShape;
    QRLabel126: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel130: TQRLabel;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListMESSAGE3: TStringField;
    qryListCR_NAME1: TStringField;
    qryListCR_NAME2: TStringField;
    qryListCR_NAME3: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_NAME3: TStringField;
    qryListMS_NAME1: TStringField;
    qryListMS_NAME2: TStringField;
    qryListMS_NAME3: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListINV_AMT: TBCDField;
    qryListINV_AMTC: TStringField;
    qryListLG_NO: TStringField;
    qryListLC_G: TStringField;
    qryListLC_NO: TStringField;
    qryListBL_G: TStringField;
    qryListBL_NO: TStringField;
    qryListCARRIER1: TStringField;
    qryListCARRIER2: TStringField;
    qryListAR_DATE: TStringField;
    qryListBL_DATE: TStringField;
    qryListLG_DATE: TStringField;
    qryListLOAD_LOC: TStringField;
    qryListLOAD_TXT: TStringField;
    qryListARR_LOC: TStringField;
    qryListARR_TXT: TStringField;
    qryListSPMARK1: TStringField;
    qryListSPMARK2: TStringField;
    qryListSPMARK3: TStringField;
    qryListSPMARK4: TStringField;
    qryListSPMARK5: TStringField;
    qryListSPMARK6: TStringField;
    qryListSPMARK7: TStringField;
    qryListSPMARK8: TStringField;
    qryListSPMARK9: TStringField;
    qryListSPMARK10: TStringField;
    qryListPAC_QTY: TBCDField;
    qryListPAC_QTYC: TStringField;
    qryListGOODS1: TStringField;
    qryListGOODS2: TStringField;
    qryListGOODS3: TStringField;
    qryListGOODS4: TStringField;
    qryListGOODS5: TStringField;
    qryListGOODS6: TStringField;
    qryListGOODS7: TStringField;
    qryListGOODS8: TStringField;
    qryListGOODS9: TStringField;
    qryListGOODS10: TStringField;
    qryListTRM_PAYC: TStringField;
    qryListTRM_PAY: TStringField;
    qryListBANK_CD: TStringField;
    qryListBANK_TXT: TStringField;
    qryListBANK_BR: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListUSERMAINT_NO: TStringField;
    qryListPRNO: TIntegerField;
    qryListCN_NAME1: TStringField;
    qryListCN_NAME2: TStringField;
    qryListCN_NAME3: TStringField;
    qryListB5_NAME1: TStringField;
    qryListB5_NAME2: TStringField;
    qryListB5_NAME3: TStringField;
    qryListLC_DATE: TStringField;
    qryListLOADnationName: TStringField;
    qryListARRnationName: TStringField;
    SummaryBand1: TQRBand;
    QRLabel5: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand14BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNo : String;
    procedure OPENDB;
  public
    { Public declarations }
    property MaintNo:String read FMaintNo write FMaintNo;
  end;
var
  LOGUAR_PRINT_frm: TLOGUAR_PRINT_frm;

implementation

uses MSSQL, Commonlib, DateUtils, StrUtils;

{$R *.dfm}

{ TLOGUAR_PRINT_frm }

procedure TLOGUAR_PRINT_frm.OPENDB;
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FMaintNo;
  qryList.Open;
end;

procedure TLOGUAR_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
  ChildBand15.Height := ChildBand15.Tag;
end;

procedure TLOGUAR_PRINT_frm.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  TitleBand1.Height := TitleBand1.Tag;
  QR_BANKTXT.Caption := qryListBANK_TXT.AsString + ' 앞'; // 은행명
  QR_LGNO.Caption := qryListLG_NO.AsString; //인도승락서 번호
end;

procedure TLOGUAR_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
    //운송회사명
    nIndex := 1;
    for i := 1 to 3 do
    begin
      if Trim(qrylist.FieldByName('CR_NAME'+IntToStr(i)).AsString) = '' then
      begin
        Continue;
      end;

      (FindComponent('QR_CRNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_CRNAME'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('CR_NAME'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    ChildBand1.Height := 15 * nIndex;
end;

procedure TLOGUAR_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
    //송하인
    nIndex := 1;
    for i := 1 to 3 do
    begin
      if Trim(qrylist.FieldByName('SE_NAME'+IntToStr(i)).AsString) = '' then
      begin
        Continue;
      end;

      (FindComponent('QR_SENAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_SENAME'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('SE_NAME'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    ChildBand2.Height := 15 * nIndex;

end;

procedure TLOGUAR_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //신용장번호
  QR_LCNO.Caption := qryListLC_NO.AsString;
  if Trim(qryListLC_NO.AsString) = '' then
    ChildBand3.Enabled := False
  else
    ChildBand3.Height := ChildBand3.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //항공화물운송장번호
  QR_BLNO.Caption := qryListBL_NO.AsString;
  if Trim(qryListBL_NO.AsString) = '' then
    ChildBand4.Enabled := False
  else
    ChildBand4.Height := ChildBand4.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //항공화물운송장 발급일자
  QR_BLDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListBL_DATE.AsString));
  if Trim(qryListBL_DATE.AsString) = '' then
    ChildBand5.Enabled := False
  else
    ChildBand5.Height := ChildBand5.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //기명
  QR_CARRIER1.Caption := qryListCARRIER1.AsString;
  if Trim(qryListCARRIER2.AsString) = '' then
    ChildBand6.Enabled := False
  else
    ChildBand6.Height := Childband6.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //비행편번호
  QR_CARRIER2.Caption := qryListCARRIER2.AsString;
  if Trim(qryListCARRIER2.AsString) = '' then
    ChildBand7.Enabled := False
  else
    ChildBand7.Height := ChildBand7.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //도착(예정)일
  QR_ARDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListAR_DATE.AsString));
  if Trim(qryListAR_DATE.AsString) = '' then
    ChildBand8.Enabled := False
  else
    ChildBand8.Height := ChildBand8.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //출발항공
  QR_LOADLOC.Caption := qryListLOADnationName.AsString + ' ' + qryListLOAD_TXT.AsString;
  if (Trim(qryListLOAD_LOC.AsString) = '') and (Trim(qryListLOAD_TXT.AsString) = '') then
    ChildBand9.Enabled := False
  else
    ChildBand9.Height := ChildBand9.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //도착항공
  QR_ARRLOC.Caption := qryListARRnationName.AsString + ' ' + qryListARR_TXT.AsString;
  if (Trim(qryListARR_LOC.AsString) = '') and (Trim(qryListARR_TXT.AsString) = '') then
    ChildBand10.Enabled := False
  else
    ChildBand10.Height := ChildBand10.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //상업송장금액
  QR_INVAMT.Caption := qryListINV_AMTC.AsString + '  ' + FormatCurr('#,###.###' , qryListINV_AMT.AsCurrency);
  if (Trim(qryListINV_AMTC.AsString) = '') and (qryListINV_AMT.AsCurrency = 0) then
    ChildBand11.Enabled := False
  else
    ChildBand11.Height := ChildBand11.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
    //화물표시 및 번호
    nIndex := 1;
    for i := 1 to 10 do
    begin
      if Trim(qrylist.FieldByName('SPMARK'+IntToStr(i)).AsString) = '' then
      begin
        Continue;
      end;

      (FindComponent('QR_SPMARK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_SPMARK'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('SPMARK'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    ChildBand12.Height := 15 * (nIndex-1);
end;

procedure TLOGUAR_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  QR_PACQTY.Caption := qryListPAC_QTYC.AsString + '  ' + FormatCurr('#,###.###' , qryListPAC_QTY.AsCurrency);
  if (Trim(qryListPAC_QTYC.AsString) = '') and (qryListPAC_QTY.AsCurrency = 0) then
    ChildBand13.Enabled := False
  else
    ChildBand13.Height := ChildBand13.Tag;
end;

procedure TLOGUAR_PRINT_frm.ChildBand14BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
    //화물표시 및 번호
    nIndex := 1;
    for i := 1 to 10 do
    begin
      if Trim(qrylist.FieldByName('GOODS'+IntToStr(i)).AsString) = '' then
      begin
        Continue;
      end;

      (FindComponent('QR_GOODS'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_GOODS'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('GOODS'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    ChildBand14.Height := 15 * (nIndex-1);
end;

procedure TLOGUAR_PRINT_frm.ChildBand16BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //신청일자
  QR_APPDATE.Caption := ''; //APP_DATE 필드가 없음.
  //신청인
  QR_MSNAME1.Caption := qryListMS_NAME1.AsString;
  QR_MSNAME2.Caption := qryListMS_NAME2.AsString;
  QR_MSNAME3.Caption := qryListMS_NAME3.AsString;
  //인도승락서 발급일자
  QR_LGDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListLG_DATE.AsString));
  //승락권자
  QR_AXNAME1.Caption := qryListAX_NAME1.AsString;
  QR_AXNAME2.Caption := qryListAX_NAME2.AsString;
  QR_AXNAME3.Caption := qryListAX_NAME3.AsString;

  ChildBand16.Height := ChildBand16.Tag;
end;

end.
