unit INF700_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls , DB ;

type
  TINF700_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QR_FUNCTION: TQRLabel;
    QR_RESPONSE: TQRLabel;
    DetailBand1: TQRBand;
    QRLabel12: TQRLabel;
    QR_INMATHOD: TQRLabel;
    QR_DATEE: TQRLabel;
    QRLabel11: TQRLabel;
    QRImage1: TQRImage;
    QRLabel10: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel13: TQRLabel;
    QR_ApBank: TQRLabel;
    QR_ApBank1: TQRLabel;
    QR_ApBank2: TQRLabel;
    QR_ApBank3: TQRLabel;
    QR_ApBank4: TQRLabel;
    QR_ApBank5: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel14: TQRLabel;
    QR_AdBank: TQRLabel;
    QR_AdBank1: TQRLabel;
    QR_AdBank2: TQRLabel;
    QR_AdBank3: TQRLabel;
    QR_AdBank4: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_ADPAY1: TQRLabel;
    ChildBand4: TQRChildBand;
    ChildBand5: TQRChildBand;
    QR_appRules2: TQRLabel;
    QR_appRules1: TQRLabel;
    QR_dateOfIssue: TQRLabel;
    QR_docCredutNo: TQRLabel;
    QR_Doccd1: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel18: TQRLabel;
    QRImage2: TQRImage;
    QRLabel17: TQRLabel;
    QRLabel16: TQRLabel;
    QR_ADINFO1: TQRLabel;
    QR_ADINFO2: TQRLabel;
    QR_ADINFO3: TQRLabel;
    QR_ADINFO4: TQRLabel;
    QR_ADINFO5: TQRLabel;
    ChildBand6: TQRChildBand;
    QRLabel30: TQRLabel;
    QR_refPreAdvice: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel27: TQRLabel;
    QRLabel26: TQRLabel;
    QR_exDate: TQRLabel;
    QR_exPlace: TQRLabel;
    ChildBand8: TQRChildBand;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QR_APPBANK1: TQRLabel;
    QR_APPBANK2: TQRLabel;
    QR_APPBANK3: TQRLabel;
    QR_APPBANK4: TQRLabel;
    QR_APPBANK5: TQRLabel;
    QR_APPBANK6: TQRLabel;
    ChildBand9: TQRChildBand;
    QRLabel32: TQRLabel;
    QRLabel31: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC3: TQRLabel;
    QR_APPLIC4: TQRLabel;
    QR_APPLIC5: TQRLabel;
    ChildBand10: TQRChildBand;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    QR_BENEFC4: TQRLabel;
    QR_BENEFC5: TQRLabel;
    ChildBand11: TQRChildBand;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QR_CDCUR: TQRLabel;
    ChildBand12: TQRChildBand;
    QR_CDAMT: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel37: TQRLabel;
    QR_CDPERP: TQRLabel;
    ChildBand13: TQRChildBand;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QR_CDMAXNAME: TQRLabel;
    ChildBand14: TQRChildBand;
    QRLabel42: TQRLabel;
    QRLabel41: TQRLabel;
    QR_AACV1: TQRLabel;
    QR_AACV2: TQRLabel;
    QR_AACV3: TQRLabel;
    QR_AACV4: TQRLabel;
    ChildBand15: TQRChildBand;
    QRLabel44: TQRLabel;
    QR_AVAIL: TQRLabel;
    QRLabel43: TQRLabel;
    QR_AVAIL2: TQRLabel;
    QR_AVAIL3: TQRLabel;
    QR_AVAIL1: TQRLabel;
    QR_AVAIL4: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QR_DRAFT1: TQRLabel;
    QR_DRAFT2: TQRLabel;
    QR_DRAFT3: TQRLabel;
    ChildBand17: TQRChildBand;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QR_DRAWEE: TQRLabel;
    QR_DRAWEE1: TQRLabel;
    QR_DRAWEE2: TQRLabel;
    QR_DRAWEE3: TQRLabel;
    QR_DRAWEE4: TQRLabel;
    ChildBand18: TQRChildBand;
    QRLabel50: TQRLabel;
    QRLabel49: TQRLabel;
    QR_MIXPAY1: TQRLabel;
    QR_MIXPAY2: TQRLabel;
    QR_MIXPAY3: TQRLabel;
    QR_MIXPAY4: TQRLabel;
    ChildBand19: TQRChildBand;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel51: TQRLabel;
    QR_PSHIP: TQRLabel;
    QR_TSHIP: TQRLabel;
    ChildBand20: TQRChildBand;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QR_SUNJUCKPORT: TQRLabel;
    ChildBand21: TQRChildBand;
    QRLabel58: TQRLabel;
    QRLabel57: TQRLabel;
    QR_DOCHACKPORT: TQRLabel;
    ChildBand22: TQRChildBand;
    QRLabel59: TQRLabel;
    QRLabel65: TQRLabel;
    QR_LOADON: TQRLabel;
    ChildBand23: TQRChildBand;
    ChildBand24: TQRChildBand;
    QRLabel60: TQRLabel;
    QRLabel64: TQRLabel;
    QR_FORTRAN: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel66: TQRLabel;
    QR_LSTDATE: TQRLabel;
    ChildBand25: TQRChildBand;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    ChildBand26: TQRChildBand;
    QRLabel68: TQRLabel;
    QRLabel67: TQRLabel;
    QR_SHIPPD1: TQRLabel;
    QR_SHIPPD2: TQRLabel;
    QR_SHIPPD3: TQRLabel;
    QR_SHIPPD4: TQRLabel;
    QR_SHIPPD5: TQRLabel;
    QR_SHIPPD6: TQRLabel;
    ChildBand27: TQRChildBand;
    QRLabel71: TQRLabel;
    QR_TERMPR: TQRLabel;
    QR_TERMPR_M: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel74: TQRLabel;
    QR_PLTERM: TQRLabel;
    QR_ORIGIN: TQRLabel;
    QR_ORIGIN_M: TQRLabel;
    ChildBand28: TQRChildBand;
    QRLabel73: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QR_DOC380_1: TQRLabel;
    QRLabel79: TQRLabel;
    ChildBand29: TQRChildBand;
    QR_GUBUN: TQRLabel;
    QR_GUBUN1: TQRLabel;
    QRLabel81: TQRLabel;
    QR_GUBUN2: TQRLabel;
    QRLabel82: TQRLabel;
    QR_DOC705_1: TQRLabel;
    QR_DOC705_3_1: TQRLabel;
    QRLabel83: TQRLabel;
    QR_DOC705_2: TQRLabel;
    ChildBand30: TQRChildBand;
    ChildBand31: TQRChildBand;
    ChildBand32: TQRChildBand;
    ChildBand33: TQRChildBand;
    QR_DOC861: TQRLabel;
    ChildBand34: TQRChildBand;
    ChildBand35: TQRChildBand;
    QRLabel78: TQRLabel;
    QRLabel80: TQRLabel;
    ChildBand36: TQRChildBand;
    QRLabel93: TQRLabel;
    QRLabel98: TQRLabel;
    ChildBand37: TQRChildBand;
    ChildBand38: TQRChildBand;
    ChildBand39: TQRChildBand;
    ChildBand40: TQRChildBand;
    ChildBand41: TQRChildBand;
    ChildBand42: TQRChildBand;
    ChildBand43: TQRChildBand;
    SummaryBand1: TQRBand;
    QRShape2: TQRShape;
    QRLabel123: TQRLabel;
    QRImage3: TQRImage;
    QRLabel124: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel129: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QR_EXADDR1: TQRLabel;
    QR_EXADDR2: TQRLabel;
    QR_EXADDR3: TQRLabel;
    QR_OPBANK1: TQRLabel;
    QR_OPBANK2: TQRLabel;
    QR_OPBANK3: TQRLabel;
    QR_OPADDR1: TQRLabel;
    QR_OPADDR2: TQRLabel;
    QR_OPADDR3: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel133: TQRLabel;
    QR_DOC705_4: TQRLabel;
    ChildBand44: TQRChildBand;
    ChildBand45: TQRChildBand;
    ChildBand46: TQRChildBand;
    ChildBand47: TQRChildBand;
    ChildBand48: TQRChildBand;
    QRLabel118: TQRLabel;
    QRLabel121: TQRLabel;
    QR_SndInfo1: TQRLabel;
    QR_SndInfo2: TQRLabel;
    QR_SndInfo3: TQRLabel;
    QR_SndInfo4: TQRLabel;
    QR_SndInfo5: TQRLabel;
    QR_SndInfo6: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel117: TQRLabel;
    QR_AVTBANK: TQRLabel;
    QR_AVTBANK1: TQRLabel;
    QR_AVTBANK2: TQRLabel;
    QR_AVTBANK3: TQRLabel;
    QR_AVTBANK4: TQRLabel;
    QR_AVTBANK5: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QR_REIBANK: TQRLabel;
    QR_REIBANK1: TQRLabel;
    QR_REIBANK2: TQRLabel;
    QR_REIBANK3: TQRLabel;
    QR_REIBANK4: TQRLabel;
    QR_REIBANK5: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QR_Confirm: TQRLabel;
    QR_Confirm1: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QR_acd2AD: TQRLabel;
    QRLabel102: TQRLabel;
    QR_acd2AC: TQRLabel;
    QRLabel101: TQRLabel;
    QR_acd2AB: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QR_acd2AA1: TQRLabel;
    QR_DESGOOD1: TQRMemo;
    QR_Doc2AA_1: TQRMemo;
    QR_acd2AE1: TQRMemo;
    QR_Instrct1: TQRMemo;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QR_DOC740_1: TQRLabel;
    QR_DOC740_2: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel69: TQRLabel;
    QR_DOC740_3_1: TQRLabel;
    QR_DOC740_4: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel88: TQRLabel;
    QR_DOC760_1: TQRLabel;
    QR_DOC760_2: TQRLabel;
    QRLabel89: TQRLabel;
    QR_DOC760_3_1: TQRLabel;
    QRLabel91: TQRLabel;
    QR_DOC760_4: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel94: TQRLabel;
    QR_DOC530_1: TQRLabel;
    QR_DOC530_2: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QR_DOC271_1: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel122: TQRLabel;
    QR_ChargeName: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel109: TQRLabel;
    QR_Period: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
     procedure PrintDocument(Fields: TFields; uPreview: Boolean = True); override;

  end;

var
  INF700_PRINT_frm: TINF700_PRINT_frm;

implementation

uses Commonlib;

{$R *.dfm}



{ TINF700_PRINT_frm }

procedure TINF700_PRINT_frm.PrintDocument(Fields: TFields;
  uPreview: Boolean);
var
  memoLines : TStringList;
  nIndex,i : Integer;
  messageVal : String;
begin
  inherited;
  FFields := Fields;
  FPreview := uPreview;

  with Fields do
  begin
    //문서/전자문서번호
    QR_MAINTNO.Caption := FieldByName('MAINT_NO').AsString;


    //전자문서 기능표시
    messageVal := getValueFromField('MESSAGE1');
    case StrToInt(messageVal) of
     1 : QR_FUNCTION.Caption := 'Cancel';
     2 : QR_FUNCTION.Caption := 'Delete';
     4 : QR_FUNCTION.Caption := 'Change';
     6 : QR_FUNCTION.Caption := 'Confirmation';
     7 : QR_FUNCTION.Caption := 'Duplicate';
     9 : QR_FUNCTION.Caption := 'Original';
    else
      QR_FUNCTION.Caption := '';
    end;


    //전자문서 응답유형
    if getValueFromField('MESSAGE2') = 'AB' then
      QR_RESPONSE.Caption := 'Message Acknowledgment'
    else if getValueFromField('MESSAGE2') = 'AP' then
      QR_RESPONSE.Caption := 'Accepted'
    else if getValueFromField('MESSAGE2') = 'NA' then
      QR_RESPONSE.Caption := 'No acknowledgement needed'
    else if getValueFromField('MESSAGE2') = 'Re' then
      QR_RESPONSE.Caption := 'Rjected'
    else
      QR_RESPONSE.Caption := '';


  //----------------------------------------------------------------------------
  //일반정보
  //----------------------------------------------------------------------------

    //---------DetailBand1------------------------------------------------------
      DetailBand1.Height := DetailBand1.Tag;
      //개설신청일자
      QR_DATEE.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('DATEE').AsString));

      //개설방법
      QR_INMATHOD.Caption := getValueFromField('IN_MATHOD')+ ' ' + getValueFromField('mathod_Name');
    //--------------------------------------------------------------------------

    //---------ChildBand1-------------------------------------------------------
      //개설(전문발신)은행
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('AP_BANK'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

        (FindComponent('QR_ApBank'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_ApBank'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AP_BANK'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      QR_ApBank.Caption := getValueFromField('AP_BANK');
      if Trim(QR_ApBank.Caption) <> '' then
      begin
        QR_ApBank.Enabled := True;
        ChildBand1.Height := 15 * (nIndex+1);
      end
      else
      begin
        QR_ApBank5.Top := QR_ApBank4.Top;
        QR_ApBank4.Top := QR_ApBank3.Top;
        QR_ApBank3.Top := QR_ApBank2.Top;
        QR_ApBank2.Top := QR_ApBank1.Top;
        QR_ApBank1.Top := QR_ApBank.Top;
        ChildBand1.Height := 15 * nIndex;

        if (Trim(FieldByName('AP_BANK1').AsString) = '') and (Trim(FieldByName('AP_BANK2').AsString) = '') and (Trim(FieldByName('AP_BANK3').AsString) = '') and
           (Trim(FieldByName('AP_BANK4').AsString) = '') and (Trim(FieldByName('AP_BANK5').AsString) = '') then
        begin
          ChildBand1.Enabled := False;
        end;  
      end;

    //--------------------------------------------------------------------------

    //---------ChildBand2-------------------------------------------------------
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if Trim(FieldByName('AD_BANK'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

        (FindComponent('QR_AdBank'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_AdBank'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AD_BANK'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      QR_AdBank.Caption := getValueFromField('AD_BANK');
      if Trim(QR_AdBank.Caption) <> '' then
      begin
        QR_AdBank.Enabled := True;
        ChildBand2.Height := 15 * (nIndex+1);
      end
      else
      begin
        QR_AdBank4.Top := QR_AdBank3.Top;
        QR_AdBank3.Top := QR_AdBank2.Top;
        QR_AdBank2.Top := QR_AdBank1.Top;
        QR_AdBank1.Top := QR_AdBank.Top;
        ChildBand2.Height := 15 * nIndex;

        if (Trim(FieldByName('AD_BANK1').AsString) = '') and (Trim(FieldByName('AD_BANK2').AsString) = '') and
           (Trim(FieldByName('AD_BANK3').AsString) = '') and (Trim(FieldByName('AD_BANK4').AsString) = '') then
        begin
          ChildBand2.Enabled := False;
        end

      end;
   //---------------------------------------------------------------------------

   //---------ChildBand3--------------------------------------------------------
    ChildBand3.Height := ChildBand3.Tag;
    QR_ADPAY1.Caption := getValueFromField('pay_Name');
    //기한부신용장 신용공여주체
      if Trim(FieldByName('pay_Name').AsString)  = '' then
      begin
        ChildBand3.Enabled := False;
      end;
   //---------------------------------------------------------------------------

   //---------ChildBand4--------------------------------------------------------
    //기타정보
    nIndex := 1;
    for i := 1 to 5 do
    begin
      if Trim(FieldByName('AD_INFO'+IntToStr(i)).AsString) = '' then
      begin
        Continue;
      end;

      (FindComponent('QR_ADINFO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_ADINFO'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AD_INFO'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    if (Trim(FieldByName('AD_INFO1').AsString) = '') and (Trim(FieldByName('AD_INFO2').AsString) = '') and (Trim(FieldByName('AD_INFO3').AsString) = '')
        and (Trim(FieldByName('AD_INFO4').AsString) = '') and (Trim(FieldByName('AD_INFO5').AsString) = '') then
    begin
      ChildBand4.Enabled := False;
    end
    else
      ChildBand4.Height := (13 * nIndex)+5;
   //----------------------------------------------------------------------------

  //----------------------------------------------------------------------------
  //스위프트
  //----------------------------------------------------------------------------
    //---------ChildBand5-------------------------------------------------------
      ChildBand5.Height := ChildBand5.Tag;
      //Form of Documentary Credit
      QR_Doccd1.Caption := getValueFromField('DOC_NAME');
      //Documentary Credit Number
      QR_docCredutNo.Caption := getValueFromField('CD_NO');
      //Date of Issue
      QR_dateOfIssue.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('ISS_DATE').AsString));
      //Applicable Rules
      QR_appRules1.Caption := getValueFromField('APPLICABLE_RULES_1');
      QR_appRules2.Caption := getValueFromField('APPLICABLE_RULES_2');
    //--------------------------------------------------------------------------

    //---------ChildBand6-------------------------------------------------------
      ChildBand6.Height := ChildBand6.Tag;
      //Reference to Pre-Advice
      QR_refPreAdvice.Caption := getValueFromField('REF_PRE');
      if Trim(FieldByName('REF_PRE').AsString)  = '' then
      begin
        ChildBand6.Enabled := False;
//        QR_refPreAdvice.Height := QR_refPreAdvice.Height - 10;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand7-------------------------------------------------------
    //Date and Place of Expiry
    ChildBand7.Height := ChildBand7.Tag;
    QR_exDate.Caption := '(date) ' +  FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('EX_DATE').AsString));
    QR_exPlace.Caption := '(place)' + getValueFromField('EX_PLACE');
    //--------------------------------------------------------------------------


    //---------ChildBand8-------------------------------------------------------
      nIndex := 1;
      for i := 1 to 5 do
      begin

        if Trim(FieldByName('APP_BANK'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;
         (FindComponent('QR_APPBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_APPBANK'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('APP_BANK'+IntToStr(i)).AsString;
         Inc(nIndex);

      end;

      QR_APPBANK6.Caption := getValueFromField('APP_ACCNT');
      //Caption이 아닌 asString 식으로 변경해야함
      if (Trim(FieldByName('APP_BANK1').AsString)  = '') and (Trim(FieldByName('APP_BANK2').AsString)  = '') and (Trim(FieldByName('APP_BANK3').AsString)  = '')
          and (Trim(FieldByName('APP_BANK4').AsString)  = '') and (Trim(FieldByName('APP_BANK5').AsString)  = '') then
      begin
        ChildBand8.Enabled := False;
      end
      else
      begin
        if Trim(QR_APPBANK6.Caption) <> '' then
        begin
          QR_APPBANK6.Top := (nIndex * 14);
          ChildBand8.Height := (13 * nIndex) + 20;
        end
        else
        begin
           QR_APPBANK6.Enabled := False;
           ChildBand8.Height := (13 * nIndex);
        end;
      end;

    //--------------------------------------------------------------------------

    //---------ChildBand9-------------------------------------------------------
      //Applicant   
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('APPLIC'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('APPLIC'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if (Trim(FieldByName('APPLIC1').AsString)  = '') and (Trim(FieldByName('APPLIC2').AsString)  = '') and (Trim(FieldByName('APPLIC3').AsString)  = '') and
         (Trim(FieldByName('APPLIC4').AsString)  = '') and (Trim(FieldByName('APPLIC5').AsString)  = '') then
      begin
        ChildBand9.Enabled := False;
      end
      else
        ChildBand9.Height := (13 * nIndex)+5;
    //--------------------------------------------------------------------------

    //---------ChildBand10------------------------------------------------------
      //Beneficiary
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('BENEFC'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('BENEFC'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if (Trim(FieldByName('BENEFC1').AsString)  = '') and (Trim(FieldByName('BENEFC2').AsString)  = '') and (Trim(FieldByName('BENEFC3').AsString)  = '')
         and (Trim(FieldByName('BENEFC4').AsString)  = '') and (Trim(FieldByName('BENEFC5').AsString)  = '') then
      begin
        ChildBand10.Enabled := False;
      end
      else
        ChildBand10.Height := (14 * nIndex);
    //--------------------------------------------------------------------------


    //---------ChildBand11------------------------------------------------------
      ChildBand11.Height := ChildBand11.Tag;
      //Currency Code,Amount
      QR_CDCUR.Caption := getValueFromField('CD_CUR');
      QR_CDAMT.Caption := FormatFloat('#,##0.###' , FieldByName('CD_AMT').AsFloat) ;
      if Trim(QR_CDAMT.Caption) = '0' then QR_CDAMT.Enabled := False;
    //--------------------------------------------------------------------------


    //---------ChildBand12------------------------------------------------------
      ChildBand12.Height := ChildBand12.Tag;
      //Percentage Credit Amount
      QR_CDPERP.Caption := '(±) '+ getValueFromField('CD_PERP') + '/' + getValueFromField('CD_PERM') + ' %';


      if (Trim(getValueFromField('CD_PERP')) = '') and (Trim(getValueFromField('CD_PERM')) = '') or
          (Trim(getValueFromField('CD_PERP')) = '0') and (Trim(getValueFromField('CD_PERM')) = '0') then
      begin
        ChildBand12.Enabled := False;
        ChildBand12.Height := ChildBand12.Height -10;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand13------------------------------------------------------
      ChildBand13.Height := ChildBand13.Tag;
      //Maximum Credit Amount
      QR_CDMAXNAME.Caption := getValueFromField('CDMAX_Name');

      if Trim(FieldByName('CDMAX_NAME').AsString)  = '' then
      begin
        ChildBand13.Enabled := False;
        ChildBand13.Height := ChildBand13.Height - 10;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand14------------------------------------------------------
      //Additional Amounts Covered
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if Trim(FieldByName('AA_CV'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AA_CV'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if (Trim(FieldByName('AA_CV1').AsString)  = '') and (Trim(FieldByName('AA_CV2').AsString)  = '') and
          (Trim(FieldByName('AA_CV3').AsString)  = '') and (Trim(FieldByName('AA_CV4').AsString)  = '') then
      begin
        ChildBand14.Enabled := False;
      end
      else
        ChildBand14.Height := (13 * nIndex)+5;
    //--------------------------------------------------------------------------


    //---------ChildBand15------------------------------------------------------
      //Available With ... By ...  :
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if Trim(FieldByName('AVAIL'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_AVAIL'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_AVAIL'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AVAIL'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;


      QR_AVAIL.Caption := getValueFromField('AVAIL');
      if Trim(QR_AVAIL.Caption) = '' then
      begin
        QR_AVAIL4.Top := QR_AVAIL3.Top;
        QR_AVAIL3.Top := QR_AVAIL2.Top;
        QR_AVAIL2.Top := QR_AVAIL1.Top;
        QR_AVAIL1.Top := QR_AVAIL.Top;
        ChildBand15.Height := 15 * nIndex;

        if (Trim(FieldByName('AVAIL1').AsString)  = '') and (Trim(FieldByName('AVAIL2').AsString)  = '') and (Trim(FieldByName('AVAIL3').AsString)  = '') and
           (Trim(FieldByName('AVAIL4').AsString)  = '') then
        begin
         ChildBand15.Enabled := False;
        end;
      end
      else
      begin
        QR_AVAIL.Enabled := True;
        ChildBand15.Height := 15 * (nIndex+1)
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand16------------------------------------------------------
      //Drafts at ....
      nIndex := 1;
      for i := 1 to 3 do
      begin
        if Trim(FieldByName('DRAFT'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_DRAFT'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_DRAFT'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('DRAFT'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if (Trim(FieldByName('DRAFT1').AsString)  = '') and (Trim(FieldByName('DRAFT2').AsString)  = '') and (Trim(FieldByName('DRAFT3').AsString)  = '') then
      begin
        ChildBand16.Enabled := False;
      end
      else
        ChildBand16.Height := (13 * nIndex)+5;
    //--------------------------------------------------------------------------

    //---------ChildBand17------------------------------------------------------
      //Drawee
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if Trim(FieldByName('DRAWEE'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_DRAWEE'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_DRAWEE'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('DRAWEE'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      QR_DRAWEE.Caption := getValueFromField('DRAWEE');
      if Trim(QR_DRAWEE.Caption) <> '' then
      begin
        QR_DRAWEE.Enabled := True;
        ChildBand17.Height := 15 * (nIndex+1);
      end
      else
      begin
        QR_DRAWEE4.Top := QR_DRAWEE3.Top;
        QR_DRAWEE3.Top := QR_DRAWEE2.Top;
        QR_DRAWEE2.Top := QR_DRAWEE1.Top;
        QR_DRAWEE1.Top := QR_DRAWEE.Top;
        ChildBand17.Height := 15 * nIndex;

        if (Trim(FieldByName('DRAWEE1').AsString)  = '') and (Trim(FieldByName('DRAWEE2').AsString)  = '') and (Trim(FieldByName('DRAWEE3').AsString)  = '') and (Trim(FieldByName('DRAWEE4').AsString)  = '') then
        begin
          ChildBand17.Enabled := False;
        end
      end;
    //--------------------------------------------------------------------------

    //---------ChildBand18------------------------------------------------------
      //Mixed PayMent Details
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if Trim(FieldByName('MIX_PAY'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_MIXPAY'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_MIXPAY'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('MIX_PAY'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if (Trim(FieldByName('MIX_PAY1').AsString)  = '') and (Trim(FieldByName('MIX_PAY2').AsString)  = '') and (Trim(FieldByName('MIX_PAY3').AsString)  = '') and (Trim(FieldByName('MIX_PAY4').AsString)  = '') then
      begin
        ChildBand18.Enabled := False;
      end
      else
        ChildBand18.Height := (13 * nIndex)+5;
    //--------------------------------------------------------------------------


    //---------ChildBand19------------------------------------------------------
      ChildBand19.Height := ChildBand19.Tag;
      //Partal Shipment
      QR_PSHIP.Caption := getValueFromField('PSHIP');
      //Transhipment
      QR_TSHIP.Caption := getValueFromField('TSHIP');
    //--------------------------------------------------------------------------


    //---------ChildBand20------------------------------------------------------
      ChildBand20.Height := ChildBand20.Tag;
      //Port of Loading / Airport of Departure
      QR_SUNJUCKPORT.Caption := getValueFromField('SUNJUCK_PORT');
      if Trim(QR_SUNJUCKPORT.Caption) = '' then
      begin
        ChildBand20.Enabled := False;
        ChildBand20.Height := ChildBand20.Height - 10;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand21------------------------------------------------------
      ChildBand21.Height := ChildBand21.Tag;
      //Port of Discharge/Airport of Destination
      QR_DOCHACKPORT.Caption := getValueFromField('DOCHACK_PORT');
      if Trim(QR_DOCHACKPORT.Caption) = '' then
      begin
        ChildBand21.Enabled := False;
        ChildBand21.Height := ChildBand21.Height - 10;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand22------------------------------------------------------
      ChildBand22.Height := ChildBand22.Tag;
      //Place of Taking in Charge/Dispatch from ···/Place of Receipt
      QR_LOADON.Caption := getValueFromField('LOAD_ON');
      if Trim(QR_LOADON.Caption) = '' then
      begin
        ChildBand22.Enabled := False;
        ChildBand22.Height := ChildBand22.Height - 10;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand23------------------------------------------------------
      ChildBand23.Height := ChildBand23.Tag;
      //Place of Final Destination/For Transportation to ···/Place of Delivery
      QR_FORTRAN.Caption := getValueFromField('FOR_TRAN');
      if Trim(QR_FORTRAN.Caption) = '' then
      begin
        ChildBand23.Enabled := False;
        ChildBand23.Height := ChildBand23.Height - 10;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand24------------------------------------------------------
      ChildBand24.Height := ChildBand24.Tag;
      //Latest Date of Shipment
      QR_LSTDATE.Caption := getValueFromField('LST_DATE');
      if Trim(QR_LSTDATE.Caption) = '' then
      begin
        ChildBand24.Enabled :=  False;
        ChildBand24.Height := ChildBand24.Height - 10;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand25------------------------------------------------------
      try
        //TStringList생성
        memoLines := TStringList.Create;
        //memoLines에 DESGOOD_1 대입
        memoLines.Text := FieldByName('DESGOOD_1').AsString;

        ChildBand25.Height := ( 13 * memoLines.Count) + 26;
        QR_DESGOOD1.Height := ( 13 * memoLines.Count);

        //Description of Goods and/or Services :
        QR_DESGOOD1.Lines.Clear;
        getValueMemo(QR_DESGOOD1,'DESGOOD_1');
        
        if Trim(QR_DESGOOD1.Caption) = '' then
        begin
          ChildBand25.Enabled := False;
          ChildBand25.Height := ChildBand25.Height - 10;
        end;
      finally
        memoLines.Free;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand26------------------------------------------------------
      //Shipment Period
      nIndex := 1;
      for i := 1 to 6 do
      begin
        if Trim(FieldByName('SHIP_PD'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('SHIP_PD'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if (Trim(FieldByName('SHIP_PD1').AsString)  = '') and (Trim(FieldByName('SHIP_PD2').AsString)  = '') and (Trim(FieldByName('SHIP_PD3').AsString)  = '') and
          (Trim(FieldByName('SHIP_PD4').AsString)  = '') and (Trim(FieldByName('SHIP_PD5').AsString)  = '') and (Trim(FieldByName('SHIP_PD6').AsString)  = '') then
      begin
        ChildBand26.Enabled := False;
      end
      else
        ChildBand26.Height := (13 * nIndex)+5;
    //--------------------------------------------------------------------------


    //---------ChildBand27------------------------------------------------------
      ChildBand27.Height := ChildBand27.Tag;
      //TERMS OF PRICE
      QR_TERMPR.Caption := getValueFromField('TERM_PR');
      QR_TERMPR_M.Caption := getValueFromField('TERM_PR_M');

      //PLACE OF TERMS OF PRICE
      QR_PLTERM.Caption := getValueFromField('PL_TERM');

      //COUNTRY OF ORIGIN
      QR_ORIGIN.Caption := getValueFromField('ORIGIN');
      QR_ORIGIN_M.Caption := getValueFromField('ORIGIN_M');
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    //Documents Required
    //--------------------------------------------------------------------------
      //---------ChildBand28----------------------------------------------------
        ChildBand28.Height := ChildBand28.Tag;
        // 380
        QR_DOC380_1.Caption := getValueFromField('DOC_380_1');
        if (FieldByName('DOC_380').AsBoolean = False) or (Trim(FieldByName('DOC_380').AsString) = '')then
        begin
          ChildBand28.Enabled := False;
          ChildBand28.Height := ChildBand28.Height - 10;
        end;
      //------------------------------------------------------------------------

      //---------ChildBand29----------------------------------------------------
        ChildBand29.Height := ChildBand29.Tag;
        //705
        QR_GUBUN.Caption := getValueFromField('DOC_705_GUBUN');
          if QR_GUBUN.Caption = '705' then
          begin
            QR_GUBUN1.Caption := 'FULL SET';
            QR_GUBUN2.Caption := 'THE ORDER OF';
          end
          else if QR_GUBUN.Caption = '706' then
          begin
            QR_GUBUN1.Caption := 'FULL SET';
            QR_GUBUN2.Caption := ''
          end
          else if QR_GUBUN.Caption = '707' then
          begin
            QR_GUBUN1.Caption := '  COPY';
            QR_GUBUN2.Caption := ''
          end
          else if QR_GUBUN.Caption = '717' then
          begin
            QR_GUBUN1.Caption := ' 1/3 SET';
            QR_GUBUN2.Caption := ''
          end
          else if QR_GUBUN.Caption = '718' then
          begin
            QR_GUBUN1.Caption := ' 2/3 SET';
            QR_GUBUN2.Caption := ''
          end
          else
          begin
            QR_GUBUN1.Caption := '';
            QR_GUBUN2.Caption := '';
          end;

          QR_DOC705_1.Caption := getValueFromField('DOC_705_1');
          QR_DOC705_2.Caption := getValueFromField('DOC_705_2');
          QR_DOC705_3_1.Caption := getValueFromField('doc705_Name');
          QR_DOC705_4.Caption := getValueFromField('doc_705_4');

        if (FieldByName('DOC_705').AsBoolean = False) or (Trim(FieldByName('DOC_705').AsString) = '')then
        begin
          ChildBand29.Enabled := False;
          ChildBand29.Height := ChildBand29.Height - 10;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand30----------------------------------------------------
        ChildBand30.Height := ChildBand30.Tag;
        //740
        QR_DOC740_1.Caption := getValueFromField('DOC_740_1');
        QR_DOC740_2.Caption := getValueFromField('DOC_740_2');
        QR_DOC740_3_1.Caption := getValueFromField('doc740_Name');
        QR_DOC740_4.Caption := getValueFromField('DOC_740_4');
        if (FieldByName('DOC_740').AsBoolean = False) or (Trim(FieldByName('DOC_740').AsString) = '') then
        begin
          ChildBand30.Enabled := False;
          ChildBand30.Height := ChildBand30.Height - 10;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand31----------------------------------------------------
        ChildBand31.Height := ChildBand31.Tag;
        //760
        QR_DOC760_1.Caption := getValueFromField('DOC_760_1');
        QR_DOC760_2.Caption := getValueFromField('DOC_760_2');
        QR_DOC760_3_1.Caption := getValueFromField('doc760_Name');
        QR_DOC760_4.Caption := getValueFromField('DOC_760_4');
        if (FieldByName('DOC_760').AsBoolean = False) or (Trim(FieldByName('DOC_760').AsString) = '') then
        begin
          ChildBand31.Enabled := False;
          ChildBand31.Height := ChildBand31.Height - 10;
        end;
        //----------------------------------------------------------------------


      //---------ChildBand32----------------------------------------------------
        ChildBand32.Height := ChildBand32.Tag;
        //530
        QR_DOC530_1.Caption := getValueFromField('DOC_530_1');
        QR_DOC530_2.Caption := getValueFromField('DOC_530_2');
        if (FieldByName('DOC_530').AsBoolean = False) or (Trim(FieldByName('DOC_530').AsString) = '') then
        begin
          ChildBand32.Enabled := False;
          ChildBand32.Height := ChildBand32.Height - 10;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand33----------------------------------------------------
        ChildBand33.Height := ChildBand33.Tag;
        //861
        QR_DOC861.Caption := 'CRETIFICATE OF ORIGIN';
        if (FieldByName('DOC_861').AsBoolean = False) or (Trim(FieldByName('DOC_861').AsString) = '') then
        begin
          ChildBand33.Enabled := False;
          ChildBand33.Height := ChildBand33.Height - 10;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand34----------------------------------------------------
        ChildBand34.Height := ChildBand34.Tag;
        //PACKING LIST IN
        QR_DOC271_1.Caption := getValueFromField('DOC_271_1');
        if (FieldByName('DOC_271').AsBoolean = False) or (Trim(FieldByName('DOC_271').AsString) = '') then
        begin
          ChildBand34.Enabled := False;
          ChildBand34.Height := ChildBand34.Height - 10;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand35----------------------------------------------------
        try
          //TStringList생성
          memoLines := TStringList.Create;
          //memoLines에 DOC_2AA_1 대입
          memoLines.Text := FieldByName('DOC_2AA_1').AsString;

          ChildBand35.Height := ( 13 * memoLines.Count) + 26;
          QR_Doc2AA_1.Height := ( 13 * memoLines.Count);

          //OTHER DOCUMENT(S)(if any)
          QR_Doc2AA_1.Lines.Clear;
          getValueMemo(QR_Doc2AA_1,'DOC_2AA_1');

          if (FieldByName('DOC_2AA').AsBoolean = False) or (Trim(FieldByName('DOC_2AA').AsString) = '') then
          begin
            ChildBand35.Enabled := False;
            ChildBand35.Height := ChildBand35.Height - 10;
          end;
        finally
          memoLines.Free;
        end;
      //------------------------------------------------------------------------
      


      //------------------------------------------------------------------------
      //Additional conditions..
      //------------------------------------------------------------------------
      if (Trim(QR_acd2AA1.Caption) <> '') or (Trim(QR_acd2AB.Caption) <> '') or (Trim(QR_acd2AC.Caption) <> '')
          or (Trim(QR_acd2AD.Caption) <> '') or (Trim(QR_acd2AE1.Caption) <> '') then
      begin
        ChildBand36.Height := ChildBand36.Tag;
      end
      else
      begin
        ChildBand36.Enabled := False;
        ChildBand36.Height := ChildBand36.Height - 10;
      end;


      //---------ChildBand37----------------------------------------------------
        ChildBand37.Height := ChildBand37.Tag;
        //Shipment By
        QR_acd2AA1.Caption := getValueFromField('ACD_2AA_1');
        if Trim(QR_acd2AA1.Caption) = '' then
        begin
          ChildBand37.Enabled := False;
          ChildBand37.Height := ChildBand37.Height - 10;
        end;
      //------------------------------------------------------------------------

      //---------ChildBand38----------------------------------------------------
        ChildBand38.Height := ChildBand38.Tag;
        //ACCEPTANCE COMMISSION DISCOUNT
        // check_acd2AB의 값이 True 이면 아래 문자열 출력
         QR_acd2AB.Caption := ' ACCEPTANCE COMMISSION DISCOUNT CHARGES ARE FOR BUYER`S ACCOUNT ';
         if FieldByName('ACD_2AB').AsBoolean = False then
         begin
           ChildBand38.Enabled := False;
           ChildBand38.Height := ChildBand38.Height - 10;
         end;
      //------------------------------------------------------------------------


      //---------ChildBand39----------------------------------------------------
        ChildBand39.Height := ChildBand39.Tag;
          //ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER
          // check_acd2AC의 값이 True 이면 아래 문자열 출력
          QR_acd2AC.Caption := ' ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER ';
          if FieldByName('ACD_2AC').AsBoolean = False then
          begin
            ChildBand39.Enabled := False;
            ChildBand39.Height := ChildBand39.Height - 10;
          end;
      //------------------------------------------------------------------------

      //---------ChildBand40----------------------------------------------------
        ChildBand40.Height := ChildBand40.Tag;
        //LATE PRESENTATION B/L ACCEPTABLE
        // check_acd2AD의 값이 True 이면 아래 문자열 출력
        QR_acd2AD.Caption := ' LATE PRESENTATION B/L ACCEPTABLE ';
        if FieldByName('ACD_2AD').AsBoolean = False then
        begin
          ChildBand40.Enabled := False;
          ChildBand40.Height := ChildBand40.Height - 10;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand41----------------------------------------------------
      try
        memoLines := TStringList.Create;
        memoLines.Text := FieldByName('ACD_2AE_1').AsString;

         ChildBand41.Height := ( 13 * memoLines.Count) + 26;
         QR_acd2AE1.Height := ( 13 * memoLines.Count);

        //OTHER CONDITION(S)    ( if any )
        // check_acd2AE의 값이 True 이면 아래 문자열 출력, 메모 출력
          QRLabel103.Caption := ' OTHER CONDITION(S)    ( if any ) ';
          QR_acd2AE1.Lines.Clear;
          getValueMemo(QR_acd2AE1,'ACD_2AE_1');

          if (FieldByName('ACD_2AE').AsBoolean = False) or (Trim(FieldByName('ACD_2AE').AsString) = '') then
          begin
            ChildBand41.Enabled := False;
            ChildBand41.Height := ChildBand41.Height - 10;
          end;
      finally
        memoLines.Free
      end;
      //------------------------------------------------------------------------


    //---------ChildBand42------------------------------------------------------
      ChildBand42.Height := ChildBand42.Tag;
      //Charges
      QR_ChargeName.Caption := getValueFromField('CHARGE_Name');
      if Trim(QR_ChargeName.Caption) = '' then
      begin
        ChildBand42.Enabled := False;
        ChildBand42.Height := ChildBand42.Height - 10;
      end;
    //--------------------------------------------------------------------------

    //---------ChildBand43------------------------------------------------------
      ChildBand43.Height := ChildBand43.Tag;
      //Period for Pressentation
      QR_Period.Caption := getValueFromField('PERIOD');
      if Trim(QR_Period.Caption) = '' then
      begin
        ChildBand43.Enabled := False;
        ChildBand43.Height := ChildBand43.Height - 10;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand44------------------------------------------------------
      ChildBand44.Height := ChildBand44.Tag;
      //Confirmation Instructions
      QR_Confirm.Caption := getValueFromField('CONFIRMM');
      QR_Confirm1.Caption := getValueFromField('CONFIRM_Name');
      if Trim(QR_Confirm.Caption) = '' then
      begin
        ChildBand44.Enabled := False;
        ChildBand44.Height := ChildBand44.Height - 10;
      end;
    //--------------------------------------------------------------------------

    //---------ChildBand45------------------------------------------------------
      //Reimbursement Bank
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('REI_BANK'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_REIBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_REIBANK'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('REI_BANK'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      QR_REIBANK.Caption  := getValueFromField('REI_BANK');
      if Trim(QR_REIBANK.Caption) = '' then
      begin
        QR_REIBANK.Enabled := True;
        ChildBand45.Height := 15 * (nIndex+1);
      end
      else
      begin
        QR_REIBANK5.Top := QR_REIBANK4.Top;
        QR_REIBANK4.Top := QR_REIBANK3.Top;
        QR_REIBANK3.Top := QR_REIBANK2.Top;
        QR_REIBANK2.Top := QR_REIBANK1.Top;
        QR_REIBANK1.Top := QR_REIBANK.Top;
        ChildBand45.Height := 15 * nIndex;
         if (Trim(FieldByName('REI_BANK1').AsString)  = '') and (Trim(FieldByName('REI_BANK2').AsString)  = '') and (Trim(FieldByName('REI_BANK3').AsString)  = '') and
            (Trim(FieldByName('REI_BANK4').AsString)  = '') and (Trim(FieldByName('REI_BANK5').AsString)  = '') then
        begin
          ChildBand45.Enabled := False;
        end 
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand46------------------------------------------------------
      try
        memoLines := TStringList.Create;
        memoLines.Text := FieldByName('INSTRCT_1').AsString;

        ChildBand46.Height := ( 13 * memoLines.Count) + 26;
        QR_Instrct1.Height := ( 13 * memoLines.Count);

        //Reimbursement Bank
        QR_Instrct1.Lines.Clear;
        getValueMemo(QR_Instrct1,'INSTRCT_1');

        if Trim(QR_Instrct1.Caption) = '' then
        begin
          ChildBand46.Enabled := False;                                                     
          ChildBand46.Height := ChildBand46.Height - 10;
        end;
      finally
        memoLines.Free;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand47------------------------------------------------------
      //Advise Through Bank
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('AVT_BANK'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_AVTBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_AVTBANK'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AVT_BANK'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      QR_AVTBANK.Caption := getValueFromField('AVT_BANK');
      if Trim(QR_AVTBANK.Caption) = '' then
      begin
        QR_AVTBANK.Enabled := True;
        ChildBand47.Height := 15 * (nIndex+1);
      end
      else
      begin
        QR_AVTBANK5.Top := QR_AVTBANK4.Top;
        QR_AVTBANK4.Top := QR_AVTBANK3.Top;
        QR_AVTBANK3.Top := QR_AVTBANK2.Top;
        QR_AVTBANK2.Top := QR_AVTBANK1.Top;
        QR_AVTBANK1.Top := QR_AVTBANK.Top;
        ChildBand47.Height := 15 * nIndex;

        if (Trim(FieldByName('AVT_BANK1').AsString)  = '') and (Trim(FieldByName('AVT_BANK2').AsString)  = '') and (Trim(FieldByName('AVT_BANK3').AsString)  = '') and
           (Trim(FieldByName('AVT_BANK4').AsString)  = '') and (Trim(FieldByName('AVT_BANK5').AsString)  = '') then
        begin
          ChildBand47.Enabled := False;
        end;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand48------------------------------------------------------
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('SND_INFO'+IntToStr(i)).AsString) = '' then
        begin
          Continue;
        end;

         (FindComponent('QR_SndInfo'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_SndInfo'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('SND_INFO'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if (Trim(FieldByName('SND_INFO1').AsString)  = '') and (Trim(FieldByName('SND_INFO2').AsString)  = '') and (Trim(FieldByName('SND_INFO3').AsString)  = '') and
         (Trim(FieldByName('SND_INFO4').AsString)  = '') and (Trim(FieldByName('SND_INFO5').AsString)  = '') then
      begin
        ChildBand48.Enabled := False;
      end
      else
        ChildBand48.Height := (13 * nIndex)+5;

    //-------------SummaryBand1-------------------------------------------------
      SummaryBand1.Height := SummaryBand1.Tag;
      //신청업체
      QR_EXNAME1.Caption := getValueFromField('EX_NAME1');
      QR_EXNAME2.Caption := getValueFromField('EX_NAME2');
      QR_EXNAME3.Caption := getValueFromField('EX_NAME3');
      QR_EXADDR1.Caption := getValueFromField('EX_ADDR1');
      QR_EXADDR2.Caption := getValueFromField('EX_ADDR2');
      QR_EXADDR3.Caption := getValueFromField('EX_ADDR3');

      //개설은행
      QR_OPBANK1.Caption := getValueFromField('OP_BANK1');
      QR_OPBANK2.Caption := getValueFromField('OP_BANK2');
      QR_OPBANK3.Caption := getValueFromField('OP_BANK3');
      QR_OPADDR1.Caption := getValueFromField('OP_ADDR1');
      QR_OPADDR2.Caption := getValueFromField('OP_ADDR2');
      QR_OPADDR3.Caption := getValueFromField('OP_ADDR3');
    //--------------------------------------------------------------------------

  end;

//  RunPrint;

end;


end.
