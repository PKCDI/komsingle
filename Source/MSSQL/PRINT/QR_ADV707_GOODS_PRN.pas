unit QR_ADV707_GOODS_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB;

type
  TQR_ADV707_GOODS_PRN_frm = class(TQuickRep)
    DetailBand1: TQRBand;
    qryList: TADOQuery;
    qryListGOODS_DESC_1: TMemoField;
    QRContent: TQRLabel;
    QRSwift: TQRLabel;
    QRTitle: TQRLabel;
    QRLabel1: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRepAfterPrint(Sender: TObject);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FGoodsList : TStringList;
    nCurrIndex : Integer;
  public
    FMAINT_NO : String;
    FAMD_NO : Integer;
  end;

var
  QR_ADV707_GOODS_PRN_frm: TQR_ADV707_GOODS_PRN_frm;

implementation

{$R *.DFM}

procedure TQR_ADV707_GOODS_PRN_frm.QuickRepBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  qryList.Close;
  qryList.Parameters[0].Value := FMAINT_NO;
  qryList.Parameters[1].Value := FAMD_NO;
  qryList.Open;  

  nCurrIndex := 0;

  FGoodsList := TStringList.Create;
  FGoodsList.Text := qryListGOODS_DESC_1.AsString;
end;

procedure TQR_ADV707_GOODS_PRN_frm.QuickRepAfterPrint(Sender: TObject);
begin
  FGoodsList.Free;
end;

procedure TQR_ADV707_GOODS_PRN_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := nCurrIndex < FGoodsList.Count;
end;

procedure TQR_ADV707_GOODS_PRN_frm.DetailBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  if nCurrIndex = 0 Then
  begin
    Sender.Height := 25;
    QRSwift.Top := 11;
    QRTitle.Top := 11;
    QRContent.Top := 11;

    QRSwift.Enabled := true;
    QRTitle.Enabled := true;
    QRtitle.Caption := 'Description of Goods and/';
    QRLabel1.Enabled := True;
  end
  else
  begin
    QRSwift.Enabled := False;
    QRTitle.Enabled := False;
    QRLabel1.Enabled := False;

    if nCurrIndex = 1 Then
    begin
      QRTitle.Enabled := True;
      QRTitle.Caption := 'or Services';    
    end;
    Sender.Height := 13;
    QRSwift.Top := 1;
    QRTitle.Top := 1;
    QRContent.Top := 1;
  end;

  QRContent.Caption := FGoodsList.Strings[nCurrIndex];
  Inc(nCurrIndex);
end;

end.
