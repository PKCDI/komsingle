 
 TQR_APP707_PRN_FRM 0{h TPF0TQR_APP707_PRN_frmQR_APP707_PRN_frmLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightBeforePrintQuickRepBeforePrintFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       ╚@      а╣
@       ╚@      @Г
@       ╚@       ╚@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	
SnapToGrid	UnitsMMZoomd TQRBand
TitleBand1Left&Top&Width╬HeightА Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUUUй@йкккккvэ	@ BandTyperbTitle TQRLabel	QRLabel39LeftTopWidth9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesкккккк*Ф@UUUUUUUй@       ■@      ╨Ц@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption[APP707]ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel1Left╠ TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesккккккъ│@      ЁЖ@UUUUUUUй@      Ф┤@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption   ш═М┴И╜е▓T╓X╓р┬й╞е╟p╚tм└╝╜мр┬н╠┴ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЁ	Font.Name   tн╝╣┤╠
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel3LeftШ Top Width{HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesкккккк*Ф@UUUUUU╔@UUUUUUUй@UUUUUU▒·@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption6(Irrevocable Documentary Credit Amendment Application)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRShapeQRShape1LeftTop8Width╛HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй@UUUUUUUй@кккккк*Ф@      ,ш	@ Brush.ColorclBlackShapeqrsRectangle  TQRLabelQRLabel2LeftTop>WidthоHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesкккккк*Ф@UUUUUUUй@кккккк
д@TUUUUUст	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption`Except so far as otherwise expressly stated, this documentary credit is Subject to the "Uniform ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel4LeftTopPWidthоHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesкккккк*Ф@UUUUUUUй@ккккккк╙@TUUUUUст	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionaCustoms and Practice for Documentary Cresdits"(2007 Revision) International Chamber of Commerce (ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel5LeftTopbWidthоHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesкккккк*Ф@UUUUUUUй@UUUUUUеБ@TUUUUUст	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionPublication No.600)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize
  TQRShapeQRShape2Left	ToptWidth╜HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй@      А╛@UUUUUUuЩ@UUUUUU╫ч	@ Brush.ColorclBlackShapeqrsRectangle  TQRShapeQRShape3LeftTop8WidthHeight=Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUeб@UUUUUUUй@кккккк*Ф@UUUUUUUй@ Brush.ColorclBlackShapeqrsRectangle  TQRShapeQRShape4Left┼Top8WidthHeight=Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUeб@кккккк|ъ	@кккккк*Ф@UUUUUUUй@ Brush.ColorclBlackShapeqrsRectangle   TQRChildBandQR_CHILD_TOPLeft&Topж Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_TOPBeforePrintColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUU╔@йкккккvэ	@ 
ParentBand
TitleBand1 TQRImageQRImage1Left
TopWidth╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesккккккк╙@ккккккк╙@       ■@ккккккДц	@ Picture.Data
┌:  TBitmap╬:  BM╬:      6   (   ш           Ш:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  TQRLabelQRLabel6LeftATopWidthMHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesкккккк*Ф@      T╘@UUUUUUUй@кккккк║╦@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption   < |╟╝╚Ї╝> ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.Name   tн╝╣┤╠
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRShapeQRShape5Left
TopWidth╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй @ккккккк╙@ккккккю▒@ккккккДц	@ Brush.ColorclBlackShapeqrsRectangle  TQRShapeQRShape6Left
Topб Width╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй @ккккккк╙@TUUUUU¤╘@ккккккДц	@ Brush.ColorclBlackShapeqrsRectangle  TQRShapeQRShape7Left
TopRWidth╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй @ккккккк╙@йкккккТ▀@ккккккДц	@ Brush.ColorclBlackShapeqrsRectangle  TQRShapeQRShape8Left
Top╫ Width╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй @ккккккк╙@кккккк6О@ккккккДц	@ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape10Left
Top.Width╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй @ккккккк╙@лкккккjє@ккккккДц	@ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape11Left
TopWidth╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй @ккккккк╙@       ■@ккккккДц	@ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape12Left
TopiWidth╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй @ккккккк╙@      шК@ккккккДц	@ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape13Left
TopSWidth╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй @ккккккк╙@ккккккЪ█@ккккккДц	@ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape14Left
TopWidthHeight;Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     \╨@ккккккк╙@       ■@UUUUUUUй @ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape15Left┬TopWidthHeight;Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     \╨@кккккк~щ	@       ■@UUUUUUUй @ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape16Leftс TopWidthHeight;Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values     \╨@      ╘Ф@       ■@UUUUUUUй @ Brush.ColorclBlackShapeqrsRectangle  TQRLabelQRLabel7LeftTopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU╔@      └Ю@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ьаДьЮРым╕ьДЬы▓ИэШ╕(Doc No.)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQRLabel8LeftTop4WidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU╔@UUUUUUХЙ@ккккккRА@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   p╚tм└╝╜мр┬н╠|╟Р╟ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQRLabel9LeftTopYWidthЛ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU╔@ккккккzы@ккккккт╖@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДды░йы▓Х(Type of cable)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel11LeftTopБ WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU╔@      ик@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel12LeftTop╢ Widthй HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU╔@TUUUUU┼Ё@йкккккТ▀@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption"   ъ░ЬьДд(ьЭШыв░)ьЭАэЦЙ(Issuing bank)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel13LeftTopь Widthп HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU╔@ккккккЬ@ккккккВч@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption#   (эЭмызЭ)эЖ╡ьзАьЭАэЦЙ(Advising bank)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel14LeftTopWidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU╔@UUUUUU=╡@ккккккRА@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ╕░эГАьаХы│┤(Others)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QR_APPLIC1Leftы ToppWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@кккккк*Ф@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QR_APPLIC2Leftы TopБ WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@      ик@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightї	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQR_APPLIC_TELLeftы TopС WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@кккккк╥┐@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QR_AP_BANKLeftы Topж WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@ккккккЪ█@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQR_AP_BANK1Leftы Top╖ WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@      Є@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightї	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQR_AP_BANK_TELLeftы Top╟ WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@UUUUUUбГ@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QR_AD_BANKLeftы Top▄ WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@UUUUUUЕС@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQR_AD_BANK1Leftы Topы WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@UUUUUUqЫ@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightї	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel
QR_MAINTNOLeftы TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@      └Ю@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QR_APPDATELeftы Top:WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@UUUUUUuЩ@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQR_IN_MATHODLeftы TopYWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@ккккккzы@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRMemoM_ADINFOLeftы TopWidth╤Height<Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      └Ю@UUUUUUqЫ@UUUUUU=╡@      ╩Щ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style Lines.Strings12345 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel16LeftTopDWidth╗ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU╔@ккккккъ│@ккккккbў@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption(Date fo amendment application)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQR_AD_BANK2Leftы Top√ WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUqЫ@ккккккж@кккккки@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   ъ░ЬьДдьЭШыв░ьЭ╕(Applicant)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightї	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRChildBandQR_SWIFTTITLELeft&Top╣ Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Valuesлкккккjє@йкккккvэ	@ 
ParentBandQR_CHILD_TOP TQRImageQRImage2Left
TopWidth╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesккккккк╙@ккккккк╙@       ■@ккккккДц	@ Picture.Data
┌:  TBitmap╬:  BM╬:      6   (   ш           Ш:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  TQRLabel	QRLabel10Left+TopWidthyHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesкккккк*Ф@кккккк╞┼@UUUUUUUй@кккккка@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption<SWIFT MESSAGE>ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.Name   tн╝╣┤╠
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
   TQRChildBandQR_CHILD_COMMONLeft&Top╨ Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_COMMONBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_SWIFTTITLE TQRLabel	QRLabel35LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption20ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel40Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionSender's ReferenceColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel44Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQR_20LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel15LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@VUUUUU╒ш@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption31CColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel17Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@VUUUUU╒ш@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionDate of Issue(YYMMDD)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel18Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@VUUUUU╒ш@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQR_31CLeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@VUUUUU╒ш@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel20LeftTop&Width-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@UUUUUU╔@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption26EColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel21Left@Top&Width╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@UUUUUU╔@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionNumber of AmndmentColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel22Left
Top&Width
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@UUUUUU╔@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQR_26ELeftTop&WidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@UUUUUU╔@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBand	QR_CANCELLeft&Top▀ Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CANCELBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_COMMON TQRLabel	QRLabel19LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption23SColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel23Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionCancellation RequestColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel27Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel31LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionCANCELColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_40ALeft&Topю Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_40ABeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBand	QR_CANCEL TQRLabel	QRLabel24LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption40AColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel25Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionForm of Documentary CreditColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel26Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel28LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_31DLeft&Top¤ Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_31DBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_40A TQRLabel	QRLabel29LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption31DColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel30Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionDate(YYMMDD) and Place ofColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel32Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel33LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel34Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@ккккккк╙@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionExpiryColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_50Left&TopWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_50BeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_31D TQRLabel	QRLabel36LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption50ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel37Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionChanged Applicant DetailsColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel38Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel41LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel42LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@ккккккк╙@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_59Left&TopWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_59BeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_50 TQRLabel	QRLabel43LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption59ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel45Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionBeneficiaryColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel46Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel47LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	QRLabel47ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel48LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@ккккккк╙@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	QRLabel48ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel49LeftTop"WidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@ккккккъ│@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	QRLabel48ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_32BLeft&Top*Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_32BBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_59 TQRLabel	QRLabel50LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption32BColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel51Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIncrease of Documentary CreditColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel52Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel53LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	QRLabel53ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel54Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@ккккккк╙@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionAmountColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_39ALeft&Top9Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_39ABeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_32B TQRLabel	QRLabel55LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption39AColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel56Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionPercentage Credit AmountColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel57Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel58LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	QRLabel53ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel59Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@ккккккк╙@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionTolerance(+/-)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_39CLeft&TopHWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_39CBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_39A TQRLabel	QRLabel60LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption39CColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel61Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionAdditional Amounts CoveredColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel62Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel63LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	QRLabel53ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel64LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@ккккккк╙@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	QRLabel53ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_42MLeft&TopfWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_42MBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_42C TQRLabel
QRLabel100LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption42MColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel101Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionMixed payment DetailsColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel102Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel103LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel104LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@VUUUUU╒ш@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_42CLeft&TopWWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_42CBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_39C TQRLabel	QRLabel92LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption42CColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel93Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionDrafts at ...ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel94Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel95LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_42PLeft&TopuWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_42PBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Values      └Ю@йкккккvэ	@ 
ParentBandQR_CHILD_42M TQRLabel
QRLabel105LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption42PColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel106Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionNegotiation/Deferred PaymentColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel107Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel108LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel109LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@VUUUUU╒ш@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel110Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@VUUUUU╒ш@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionDetailsColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_43PLeft&TopДWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_43PBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_42P TQRLabel
QRLabel111LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption43PColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel113Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionPartial ShipmentsColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel114Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel115LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_43TLeft&TopЙWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_43TBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_43P TQRLabel
QRLabel112LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption43TColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel116Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionTranshipmentColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel117Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel118LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_44ALeft&TopОWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_44ABeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_43T TQRLabel
QRLabel119LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption44AColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel120Left@TopWidth╘ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@кккккк:М@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption#Place of Taking in Charge/Dispatch:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel122LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel123Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@VUUUUU╒ш@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionfrom.../Place of ReceiptColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_44ELeft&TopУWidth╬Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_44EBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_44A TQRLabel
QRLabel121LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption44EColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel124Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionPort of Loading/Airport ofColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel125Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel126LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel128Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@VUUUUU╒ш@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption	DepartureColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_44FLeft&TopЭWidth╬Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_44FBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_44E TQRLabel
QRLabel127LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption44FColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel129Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionPort of Discharge/Airport ofColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel130Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel131LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel132Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@VUUUUU╒ш@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionDestinationColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_44BLeft&TopзWidth╬Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_44BBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_44F TQRLabel
QRLabel133LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption44BColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel134Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionPlace of Final Destination/ForColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel135Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel136LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel137Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@VUUUUU╒ш@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionTransportation to.../Place ofColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel138Left@Top&Width╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@UUUUUU╔@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionDeliveryColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_44CLeft&Top▒Width╬Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_44CBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_44B TQRLabel
QRLabel139LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption44CColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel141Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel142LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel140Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionLatest Date of Shipment(YYMMDD)ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_44DLeft&Top╗Width╬Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_44DBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_44C TQRLabel
QRLabel143LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption44DColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel144Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel145LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel146Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionShipment PeriodColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_45BLeft&Top┼Width╬Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_45BBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_44D TQRLabel
QRLabel147LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption45BColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel148Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel150Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionDescription of Goods and/orColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel151Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@VUUUUU╒ш@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionServicesColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRMemoM_45BLeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_46BLeft&Top╧Width╬Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_46BBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_45B TQRLabel
QRLabel149LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption46BColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel152Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel153Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionDocuments RequiredColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRMemoM_46BLeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_47BLeft&Top┘Width╬Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_47BBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_46B TQRLabel
QRLabel154LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption47BColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel155Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel156Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionAdditional ConditionsColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRMemoM_47BLeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_49MLeft&TopуWidth╬Height
Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_49MBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesккккккк╙@йкккккvэ	@ 
ParentBandQR_CHILD_47B TQRLabel
QRLabel157LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption49MColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel158Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel159Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionSpecial Payment Conditions forColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel160Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@VUUUUU╒ш@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionBeneficiaryColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRMemoM_49MLeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_71DLeft&TopэWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_71DBeforePrintColorclWhiteForceNewColumnForceNewPageSize.ValuesVUUUUU╒ш@йкккккvэ	@ 
ParentBandQR_CHILD_49M TQRLabel
QRLabel161LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption71DColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel162Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel163Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionChargesColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRMemoM_71DLeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_71NLeft&TopWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_71NBeforePrintColorclWhiteForceNewColumnForceNewPageSize.ValuesVUUUUU╒ш@йкккккvэ	@ 
ParentBandQR_CHILD_71D TQRLabel
QRLabel164LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption71NColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel165Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel166Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionAmendment Charge Payable ByColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRMemoM_71NLeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_48Left&TopWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_48BeforePrintColorclWhiteForceNewColumnForceNewPageSize.ValuesVUUUUU╒ш@йкккккvэ	@ 
ParentBandQR_CHILD_71N TQRLabel
QRLabel167LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption48ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel168Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel169Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionPeriod for Presentation in DaysColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQR_48LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_49Left&Top/Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_49BeforePrintColorclWhiteForceNewColumnForceNewPageSize.ValuesVUUUUU╒ш@йкккккvэ	@ 
ParentBandQR_CHILD_48 TQRLabel
QRLabel170LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption49ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel171Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel172Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionConfirmation InstructionsColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabelQR_49LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionIRRColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_58aLeft&TopEWidth╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_58aBeforePrintColorclWhiteForceNewColumnForceNewPageSize.ValuesVUUUUU╒ш@йкккккvэ	@ 
ParentBandQR_CHILD_49 TQRLabel
QRLabel173LeftTopWidth-HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ю@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption58aColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel174Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel175Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionRequested Confrimation PartyColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRMemoM_58aLeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   TQRChildBandQR_CHILD_SIGNLeft&TopqWidth╬HeightЧ Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_SIGNBeforePrintColorclWhiteForceNewColumnForceNewPageSize.Valuesкккккк┬╟@йкккккvэ	@ 
ParentBandQR_CHILD_RFF TQRImageQRImage3Left
TopWidth╣HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesккккккк╙@ккккккк╙@       ■@ккккккДц	@ Picture.Data
┌:  TBitmap╬:  BM╬:      6   (   ш           Ш:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  TQRLabel
QRLabel189Left@TopWidthMHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesкккккк*Ф@ккккккк╙@UUUUUUUй@кккккк║╦@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchCaption   < ╚Р╟┴Е║> ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightє	Font.Name   tн╝╣┤╠
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRShapeQRShape9LeftTopWidth╛HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй@UUUUUUUй@UUUUUU╔@      ,ш	@ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape17Left	ToplWidth╜HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй@      А╛@      рО@UUUUUU╫ч	@ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape18LeftTopWidthHeightГ Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUMн@UUUUUUUй@UUUUUU╔@UUUUUUUй@ Brush.ColorclBlackShapeqrsRectangle  TQRShape	QRShape19Left┼TopWidthHeightГ Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUMн@кккккк|ъ	@UUUUUU╔@UUUUUUUй@ Brush.ColorclBlackShapeqrsRectangle  TQRLabel
QRLabel191LeftTop:WidthyHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@       ■@UUUUUUuЩ@кккккка@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaption	   р┬н╠┼┼┤╠  ╚Р╟┴Е║ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel192Left░ TopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@VUUUUU╒ш@UUUUUUХЙ@UUUUUU%┴@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption   ┴└8╓ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel193Left░ Top/WidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@VUUUUU╒ш@TUUUUU╡°@UUUUUU%┴@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption    │\╘Р╟Е║ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel194Left░ TopDWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@VUUUUU╒ш@ккккккъ│@UUUUUU%┴@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption   ╚Р╟┴Е║ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel195Left░ TopYWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@VUUUUU╒ш@ккккккzы@UUUUUU%┴@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption   №╚М┴ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel196Left· TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU]е@UUUUUUХЙ@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel197Left· Top/Width
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU]е@TUUUUU╡°@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel198Left· TopDWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU]е@ккккккъ│@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel199Left· TopYWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU]е@ккккккzы@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRShape	QRShape20Leftа TopWidthHeightZFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ю@ккккккк╙@UUUUUU╔@UUUUUUUй@ Brush.ColorclBlackShapeqrsRectangle  TQRLabel
QRLabel200LeftTopWidth╗HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@      дм@UUUUUUХЙ@UUUUUUГТ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
QRLabel200ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel201LeftTop/Width╗HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@      дм@TUUUUU╡°@UUUUUUГТ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
QRLabel201ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel202LeftTopDWidth╗HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@      дм@ккккккъ│@UUUUUUГТ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
QRLabel202ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel
QRLabel203LeftTopYWidth╗HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@      дм@ккккккzы@UUUUUUГТ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption
QRLabel203ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel65LeftTopsWidth╖HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@       ■@кккккк"Ш@VUUUUU█х	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption=   t╟  ╚Р╟4╗э┼8╗┴Ф▓  0╚Р╟4╗э┼  	═─╔╨┼   н\╒  Х╝`╣0╨┼  0╡|╖  ╚Р╟8╗┴PнX╓)╝▌┬<╟\╕  ╝Й╒┤  Гм<╟\╕┴  Ь═%╕X╒ь┼  8┴ н  ╢Ф▓ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel66LeftTopГ Width╖HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@       ■@UUUUUUMн@VUUUUU█х	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionB   4╗э┼ ╟ н0о н  ё┤  ╚3 Р╟╨┼Мм  ╚Ь═X╒$╕Ф▓  ╜м░╞  ┼┼┤╠Ф▓  ┘│  Х╝`╣  ▄┬Й╒▄н╚  ╚1 2 p╚╚3 m╒╨┼  0╡|╖  ╚╔└рм4╗x╟D╟  а░x╟X╒ь┼|┼i╒╚▓ф▓. ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRShape	QRShape21Left	TopФ Width╜HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUUй@      А╛@кккккк╩├@UUUUUU╫ч	@ Brush.ColorclBlackShapeqrsRectangle   TQRChildBandQR_CHILD_RFFLeft&Top[Width╬HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQR_CHILD_RFFBeforePrintColorclWhiteForceNewColumnForceNewPageSize.ValuesVUUUUU╒ш@йкккккvэ	@ 
ParentBandQR_CHILD_58a TQRLabel	QRLabel68Left
TopWidth
HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@ккккккЄп@       ■@ккккккк╙@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaption:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRLabel	QRLabel69Left@TopWidth╔ HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUUUй@       ■@      ЇД@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionTo specify a referenceColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	  TQRMemoQRMemo1LeftTopWidthЗHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUUХЙ@UUUUUU9╖@       ■@ккккккPБ	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.HeightЇ	Font.Name   tн╝╣┤╠
Font.Style 
ParentFontTransparentWordWrap	FontSize	   	TADOQueryqryList
ConnectionDMMssql.KISConnect
ParametersNameMAINT_NO
Attributes
paNullable DataTypeftStringNumericScale  	Precision  Size#ValueL2018VIDMK051 NameMSEQ
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue NameAMD_NO
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.StringsРSELECT A707_1.MAINT_NO,A707_1.MSeq,A707_1.AMD_NO,A707_1.MESSAGE1,A707_1.MESSAGE2,A707_1.[USER_ID],A707_1.DATEE,A707_1.APP_DATE,A707_1.IN_MATHOD,c	   A707_1.AP_BANK,A707_1.AP_BANK1,A707_1.AP_BANK2,A707_1.AP_BANK3,A707_1.AP_BANK4,A707_1.AP_BANK5,a	   A707_1.AD_BANK,A707_1.AD_BANK1,A707_1.AD_BANK2,A707_1.AD_BANK3,A707_1.AD_BANK4,A707_1.AD_PAY,J	   A707_1.IL_NO1,A707_1.IL_NO2,A707_1.IL_NO3,A707_1.IL_NO4,A707_1.IL_NO5,O	   A707_1.IL_AMT1,A707_1.IL_AMT2,A707_1.IL_AMT3,A707_1.IL_AMT4,A707_1.IL_AMT5,O	   A707_1.IL_CUR1,A707_1.IL_CUR2,A707_1.IL_CUR3,A707_1.IL_CUR4,A707_1.IL_CUR5,T	   A707_1.AD_INFO1,A707_1.AD_INFO2,A707_1.AD_INFO3,A707_1.AD_INFO4,A707_1.AD_INFO5,Г	   A707_1.CD_NO,A707_1.ISS_DATE,A707_1.EX_DATE,A707_1.EX_PLACE,A707_1.CHK1,A707_1.CHK2,A707_1.CHK3,A707_1.prno,A707_1.F_INTERFACE,^                   A707_1.IMP_CD1,A707_1.IMP_CD2,A707_1.IMP_CD3,A707_1.IMP_CD4,A707_1.IMP_CD5, 2                  IS_CANCEL, DOC_CD, PSHIP, TSHIP, O	   A707_2.APPLIC1,A707_2.APPLIC2,A707_2.APPLIC3,A707_2.APPLIC4,A707_2.APPLIC5,}	   A707_2.BENEFC,A707_2.BENEFC1,A707_2.BENEFC2,A707_2.BENEFC3,A707_2.BENEFC4,A707_2.BENEFC5,A707_2.INCD_CUR,A707_2.INCD_AMT,c	   A707_2.DECD_CUR,A707_2.DECD_AMT,A707_2.NWCD_CUR,A707_2.NWCD_AMT,A707_2.CD_PERP,A707_2.CD_PERM, k	   A707_2.AA_CV1,A707_2.AA_CV2,A707_2.AA_CV3,A707_2.AA_CV4,A707_2.LOAD_ON,A707_2.FOR_TRAN,A707_2.LST_DATE,s	   A707_2.SHIP_PD,A707_2.SHIP_PD1,A707_2.SHIP_PD2,A707_2.SHIP_PD3,A707_2.SHIP_PD4,A707_2.SHIP_PD5,A707_2.SHIP_PD6,r	   A707_2.NARRAT,A707_2.NARRAT_1,A707_2.EX_NAME1,A707_2.EX_NAME2,A707_2.EX_NAME3,A707_2.EX_ADDR1,A707_2.EX_ADDR2,j	   A707_2.SRBUHO,A707_2.BFCD_AMT,A707_2.BFCD_CUR,A707_2.CARRIAGE,A707_2.SUNJUCK_PORT,A707_2.DOCHACK_PORT, (                    CHARGE, CHARGE_1, AMD_CHARGE, AMD_CHARGE_1, SPECIAL_PAY, GOODS_DESC, DOC_REQ, ADD_CONDITION, DRAFT1, DRAFT2, DRAFT3, MIX1, MIX2, MIX3, MIX4, DEFPAY1, DEFPAY2, DEFPAY3, DEFPAY4, PERIOD_DAYS, PERIOD_IDX, PERIOD_DETAIL, CONFIRM, CONFIRM_BIC, CONFIRM1, CONFIRM2, CONFIRM3, CONFIRM4,\                  APPLIC_CHG, APPLIC_CHG1, APPLIC_CHG2, APPLIC_CHG3, APPLIC_CHG4, BENEFC_CHG %	  ,Mathod707.DOC_NAME as mathod_Name	  ,Pay707.DOC_NAME as pay_Name%	  ,IMPCD707_1.DOC_NAME as Imp_Name_1%	  ,IMPCD707_2.DOC_NAME as Imp_Name_2%	  ,IMPCD707_3.DOC_NAME as Imp_Name_3%	  ,IMPCD707_4.DOC_NAME as Imp_Name_4%	  ,IMPCD707_5.DOC_NAME as Imp_Name_5(	  ,cMethod707.DOC_NAME as Carriage_Name.                  ,DOC_CD.DOC_NAME as DOC_CDNM ,FROM [dbo].[APP707_1] AS A707_1 with(nolock)ЧINNER JOIN [dbo].[APP707_2] AS A707_2 with(nolock) ON A707_1.MAINT_NO = A707_2.MAINT_NO AND A707_1.MSeq = A707_2.MSeq AND A707_1.AMD_NO = A707_2.AMD_NO Р   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(nolock) WHERE Prefix = 'ъ░ЬьДды░йы▓Х') Mathod707 ON A707_1.IN_MATHOD = Mathod707.CODEЗ   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(nolock) WHERE Prefix = 'ьЛаьЪйъ│╡ьЧм') Pay707 ON A707_1.AD_PAY = Pay707.CODEКLEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(nolock) WHERE Prefix = 'IMP_CD') IMPCD707_1 ON A707_1.IMP_CD1 = IMPCD707_1.CODEКLEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(nolock) WHERE Prefix = 'IMP_CD') IMPCD707_2 ON A707_1.IMP_CD2 = IMPCD707_2.CODEКLEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(nolock) WHERE Prefix = 'IMP_CD') IMPCD707_3 ON A707_1.IMP_CD3 = IMPCD707_3.CODEКLEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(nolock) WHERE Prefix = 'IMP_CD') IMPCD707_4 ON A707_1.IMP_CD4 = IMPCD707_4.CODEКLEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(nolock) WHERE Prefix = 'IMP_CD') IMPCD707_5 ON A707_1.IMP_CD5 = IMPCD707_5.CODEНLEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(nolock) WHERE Prefix = 'C_METHOD') cMethod707 ON A707_2.CARRIAGE = cMethod707.CODEТLEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(nolock) WHERE Prefix = 'DOC_CD' AND CODE != '9' ) DOC_CD ON A707_1.DOC_CD = DOC_CD.CODE!WHERE A707_1.MAINT_NO = :MAINT_NOAND A707_1.MSeq = :MSEQAND A707_1.AMD_NO = :AMD_NO LeftTop TStringFieldqryListMAINT_NO	FieldNameMAINT_NOSize#  TIntegerFieldqryListMSeq	FieldNameMSeq  TIntegerFieldqryListAMD_NO	FieldNameAMD_NO  TStringFieldqryListMESSAGE1	FieldNameMESSAGE1Size  TStringFieldqryListMESSAGE2	FieldNameMESSAGE2Size  TStringFieldqryListUSER_ID	FieldNameUSER_IDSize
  TStringFieldqryListDATEE	FieldNameDATEESize  TStringFieldqryListAPP_DATE	FieldNameAPP_DATESize  TStringFieldqryListIN_MATHOD	FieldName	IN_MATHODSize  TStringFieldqryListAP_BANK	FieldNameAP_BANKSize  TStringFieldqryListAP_BANK1	FieldNameAP_BANK1Size#  TStringFieldqryListAP_BANK2	FieldNameAP_BANK2Size#  TStringFieldqryListAP_BANK3	FieldNameAP_BANK3Size#  TStringFieldqryListAP_BANK4	FieldNameAP_BANK4Size#  TStringFieldqryListAP_BANK5	FieldNameAP_BANK5Size#  TStringFieldqryListAD_BANK	FieldNameAD_BANKSize  TStringFieldqryListAD_BANK1	FieldNameAD_BANK1Size#  TStringFieldqryListAD_BANK2	FieldNameAD_BANK2Size#  TStringFieldqryListAD_BANK3	FieldNameAD_BANK3Size#  TStringFieldqryListAD_BANK4	FieldNameAD_BANK4Size#  TStringFieldqryListAD_PAY	FieldNameAD_PAYSize  TStringFieldqryListIL_NO1	FieldNameIL_NO1Size#  TStringFieldqryListIL_NO2	FieldNameIL_NO2Size#  TStringFieldqryListIL_NO3	FieldNameIL_NO3Size#  TStringFieldqryListIL_NO4	FieldNameIL_NO4Size#  TStringFieldqryListIL_NO5	FieldNameIL_NO5Size#  	TBCDFieldqryListIL_AMT1	FieldNameIL_AMT1	Precision  	TBCDFieldqryListIL_AMT2	FieldNameIL_AMT2	Precision  	TBCDFieldqryListIL_AMT3	FieldNameIL_AMT3	Precision  	TBCDFieldqryListIL_AMT4	FieldNameIL_AMT4	Precision  	TBCDFieldqryListIL_AMT5	FieldNameIL_AMT5	Precision  TStringFieldqryListIL_CUR1	FieldNameIL_CUR1Size  TStringFieldqryListIL_CUR2	FieldNameIL_CUR2Size  TStringFieldqryListIL_CUR3	FieldNameIL_CUR3Size  TStringFieldqryListIL_CUR4	FieldNameIL_CUR4Size  TStringFieldqryListIL_CUR5	FieldNameIL_CUR5Size  TStringFieldqryListAD_INFO1	FieldNameAD_INFO1SizeF  TStringFieldqryListAD_INFO2	FieldNameAD_INFO2SizeF  TStringFieldqryListAD_INFO3	FieldNameAD_INFO3SizeF  TStringFieldqryListAD_INFO4	FieldNameAD_INFO4SizeF  TStringFieldqryListAD_INFO5	FieldNameAD_INFO5SizeF  TStringFieldqryListCD_NO	FieldNameCD_NOSize#  TStringFieldqryListISS_DATE	FieldNameISS_DATESize  TStringFieldqryListEX_DATE	FieldNameEX_DATESize  TStringFieldqryListEX_PLACE	FieldNameEX_PLACESize#  TBooleanFieldqryListCHK1	FieldNameCHK1  TStringFieldqryListCHK2	FieldNameCHK2Size  TStringFieldqryListCHK3	FieldNameCHK3Size
  TIntegerFieldqryListprno	FieldNameprno  TStringFieldqryListF_INTERFACE	FieldNameF_INTERFACESize  TStringFieldqryListIMP_CD1	FieldNameIMP_CD1Size  TStringFieldqryListIMP_CD2	FieldNameIMP_CD2Size  TStringFieldqryListIMP_CD3	FieldNameIMP_CD3Size  TStringFieldqryListIMP_CD4	FieldNameIMP_CD4Size  TStringFieldqryListIMP_CD5	FieldNameIMP_CD5Size  TStringFieldqryListIS_CANCEL	FieldName	IS_CANCELSize
  TStringFieldqryListDOC_CD	FieldNameDOC_CDSize  TStringFieldqryListPSHIP	FieldNamePSHIPSize  TStringFieldqryListTSHIP	FieldNameTSHIPSize  TStringFieldqryListAPPLIC1	FieldNameAPPLIC1Size#  TStringFieldqryListAPPLIC2	FieldNameAPPLIC2Size#  TStringFieldqryListAPPLIC3	FieldNameAPPLIC3Size#  TStringFieldqryListAPPLIC4	FieldNameAPPLIC4Size#  TStringFieldqryListAPPLIC5	FieldNameAPPLIC5Size#  TStringFieldqryListBENEFC	FieldNameBENEFCSize
  TStringFieldqryListBENEFC1	FieldNameBENEFC1Size#  TStringFieldqryListBENEFC2	FieldNameBENEFC2Size#  TStringFieldqryListBENEFC3	FieldNameBENEFC3Size#  TStringFieldqryListBENEFC4	FieldNameBENEFC4Size#  TStringFieldqryListBENEFC5	FieldNameBENEFC5Size#  TStringFieldqryListINCD_CUR	FieldNameINCD_CURSize  	TBCDFieldqryListINCD_AMT	FieldNameINCD_AMT	Precision  TStringFieldqryListDECD_CUR	FieldNameDECD_CURSize  	TBCDFieldqryListDECD_AMT	FieldNameDECD_AMT	Precision  TStringFieldqryListNWCD_CUR	FieldNameNWCD_CURSize  	TBCDFieldqryListNWCD_AMT	FieldNameNWCD_AMT	Precision  TIntegerFieldqryListCD_PERP	FieldNameCD_PERP  TIntegerFieldqryListCD_PERM	FieldNameCD_PERM  TStringFieldqryListAA_CV1	FieldNameAA_CV1Size#  TStringFieldqryListAA_CV2	FieldNameAA_CV2Size#  TStringFieldqryListAA_CV3	FieldNameAA_CV3Size#  TStringFieldqryListAA_CV4	FieldNameAA_CV4Size#  TStringFieldqryListLOAD_ON	FieldNameLOAD_ONSizeF  TStringFieldqryListFOR_TRAN	FieldNameFOR_TRANSizeF  TStringFieldqryListLST_DATE	FieldNameLST_DATESize  TBooleanFieldqryListSHIP_PD	FieldNameSHIP_PD  TStringFieldqryListSHIP_PD1	FieldNameSHIP_PD1SizeF  TStringFieldqryListSHIP_PD2	FieldNameSHIP_PD2SizeF  TStringFieldqryListSHIP_PD3	FieldNameSHIP_PD3SizeF  TStringFieldqryListSHIP_PD4	FieldNameSHIP_PD4SizeF  TStringFieldqryListSHIP_PD5	FieldNameSHIP_PD5SizeF  TStringFieldqryListSHIP_PD6	FieldNameSHIP_PD6SizeF  TBooleanFieldqryListNARRAT	FieldNameNARRAT  
TMemoFieldqryListNARRAT_1	FieldNameNARRAT_1BlobTypeftMemo  TStringFieldqryListEX_NAME1	FieldNameEX_NAME1Size#  TStringFieldqryListEX_NAME2	FieldNameEX_NAME2Size#  TStringFieldqryListEX_NAME3	FieldNameEX_NAME3Size#  TStringFieldqryListEX_ADDR1	FieldNameEX_ADDR1Size#  TStringFieldqryListEX_ADDR2	FieldNameEX_ADDR2Size#  TStringFieldqryListSRBUHO	FieldNameSRBUHOSize#  	TBCDFieldqryListBFCD_AMT	FieldNameBFCD_AMT	Precision  TStringFieldqryListBFCD_CUR	FieldNameBFCD_CURSize  TStringFieldqryListCARRIAGE	FieldNameCARRIAGESize  TStringFieldqryListSUNJUCK_PORT	FieldNameSUNJUCK_PORTSizeA  TStringFieldqryListDOCHACK_PORT	FieldNameDOCHACK_PORTSizeA  TStringFieldqryListCHARGE	FieldNameCHARGESize  
TMemoFieldqryListCHARGE_1	FieldNameCHARGE_1BlobTypeftMemo  TStringFieldqryListAMD_CHARGE	FieldName
AMD_CHARGESize  
TMemoFieldqryListAMD_CHARGE_1	FieldNameAMD_CHARGE_1BlobTypeftMemo  
TMemoFieldqryListSPECIAL_PAY	FieldNameSPECIAL_PAYBlobTypeftMemo  
TMemoFieldqryListGOODS_DESC	FieldName
GOODS_DESCBlobTypeftMemo  
TMemoFieldqryListDOC_REQ	FieldNameDOC_REQBlobTypeftMemo  
TMemoFieldqryListADD_CONDITION	FieldNameADD_CONDITIONBlobTypeftMemo  TStringFieldqryListDRAFT1	FieldNameDRAFT1Size#  TStringFieldqryListDRAFT2	FieldNameDRAFT2Size#  TStringFieldqryListDRAFT3	FieldNameDRAFT3Size#  TStringFieldqryListMIX1	FieldNameMIX1Size#  TStringFieldqryListMIX2	FieldNameMIX2Size#  TStringFieldqryListMIX3	FieldNameMIX3Size#  TStringFieldqryListMIX4	FieldNameMIX4Size#  TStringFieldqryListDEFPAY1	FieldNameDEFPAY1Size#  TStringFieldqryListDEFPAY2	FieldNameDEFPAY2Size#  TStringFieldqryListDEFPAY3	FieldNameDEFPAY3Size#  TStringFieldqryListDEFPAY4	FieldNameDEFPAY4Size#  TIntegerFieldqryListPERIOD_DAYS	FieldNamePERIOD_DAYS  TIntegerFieldqryListPERIOD_IDX	FieldName
PERIOD_IDX  TStringFieldqryListPERIOD_DETAIL	FieldNamePERIOD_DETAILSize#  TStringFieldqryListCONFIRM	FieldNameCONFIRMSize  TStringFieldqryListCONFIRM_BIC	FieldNameCONFIRM_BICSize  TStringFieldqryListCONFIRM1	FieldNameCONFIRM1Size#  TStringFieldqryListCONFIRM2	FieldNameCONFIRM2Size#  TStringFieldqryListCONFIRM3	FieldNameCONFIRM3Size#  TStringFieldqryListCONFIRM4	FieldNameCONFIRM4Size#  TBooleanFieldqryListAPPLIC_CHG	FieldName
APPLIC_CHG  TStringFieldqryListAPPLIC_CHG1	FieldNameAPPLIC_CHG1Size#  TStringFieldqryListAPPLIC_CHG2	FieldNameAPPLIC_CHG2Size#  TStringFieldqryListAPPLIC_CHG3	FieldNameAPPLIC_CHG3Size#  TStringFieldqryListAPPLIC_CHG4	FieldNameAPPLIC_CHG4Size#  TBooleanFieldqryListBENEFC_CHG	FieldName
BENEFC_CHG  TStringFieldqryListmathod_Name	FieldNamemathod_NameSized  TStringFieldqryListpay_Name	FieldNamepay_NameSized  TStringFieldqryListImp_Name_1	FieldName
Imp_Name_1Sized  TStringFieldqryListImp_Name_2	FieldName
Imp_Name_2Sized  TStringFieldqryListImp_Name_3	FieldName
Imp_Name_3Sized  TStringFieldqryListImp_Name_4	FieldName
Imp_Name_4Sized  TStringFieldqryListImp_Name_5	FieldName
Imp_Name_5Sized  TStringFieldqryListCarriage_Name	FieldNameCarriage_NameSized  TStringFieldqryListDOC_CDNM	FieldNameDOC_CDNMSized    