unit APPPCR_PRINT;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils;

type
  TAPPPCR_PRINT_FRM = class(TQuickRep)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel20: TQRLabel;
    ColumnHeaderBand1: TQRBand;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    DetailBand1: TQRBand;
    QRLabel33: TQRLabel;
    QRMemo1: TQRMemo;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    qryMst: TADOQuery;
    qryMstAPP_NAME1: TStringField;
    qryMstAPP_ADDR1: TStringField;
    qryMstAPP_ADDR2: TStringField;
    qryMstAPP_NAME2: TStringField;
    qryMstAX_NAME2: TStringField;
    qryMstSUP_NAME1: TStringField;
    qryMstSUP_ADDR1: TStringField;
    qryMstSUP_ADDR2: TStringField;
    qryMstSUP_NAME2: TStringField;
    qryMstSUP_ADDR3: TStringField;
    qryMstEMAIL_ID: TStringField;
    qryMstEMAIL_DOMAIN: TStringField;
    qryMstPCrLic_No: TStringField;
    qryMstTQTY: TBCDField;
    qryMstTQTYC: TStringField;
    qryMstTAMT1: TBCDField;
    qryMstTAMT1C: TStringField;
    qryMstTAMT2: TBCDField;
    qryDetail: TADOQuery;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailREMARK: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTYC: TStringField;
    qryDetailAMT1: TBCDField;
    qryDetailAMT1C: TStringField;
    qryDetailAMT2: TBCDField;
    qryDetailPRI1: TBCDField;
    qryDetailPRI2: TBCDField;
    qryDetailPRI_BASE: TBCDField;
    qryDetailPRI_BASEC: TStringField;
    qryDetailACHG_G: TStringField;
    qryDetailAC1_AMT: TBCDField;
    qryDetailAC1_C: TStringField;
    qryDetailAC2_AMT: TBCDField;
    qryDetailLOC_TYPE: TStringField;
    qryDetailNAMESS: TStringField;
    qryDetailSIZESS: TStringField;
    qryDetailAPP_DATE: TStringField;
    qryDetailITM_NUM: TStringField;
    QRImage1: TQRImage;
    QRShape3: TQRShape;
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure SummaryBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FDocNo : String;
    procedure OPENDB;
    procedure DelEmptyLine(var StrList : TStringList);
  public
    property  DocNo:string  read FDocNo write FDocNo;

  end;

var
  APPPCR_PRINT_FRM: TAPPPCR_PRINT_FRM;

implementation

uses MSSQL, Commonlib;

{$R *.DFM}

procedure TAPPPCR_PRINT_FRM.OPENDB;
begin
  qryMst.Close;
  qryMst.Parameters.ParamByName('MAINT_NO').Value := FDocNo;
  qryMst.Open;

  qryDetail.Close;
  qryDetail.Parameters.ParamByName('KEYY').Value := FDocNo;
  qryDetail.Open;
end;

procedure TAPPPCR_PRINT_FRM.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
//  QRLabel4.Caption := qryMstLIC_NO.AsString;

  QRLabel16.Caption := qryMstAPP_NAME1.AsString;
  QRLabel17.Caption := qryMstAPP_ADDR1.AsString+' '+qryMstAPP_ADDR2.AsString;
  QRLabel18.Caption := qryMstAPP_NAME2.AsString;
  QRLabel19.Caption := qryMstAX_NAME2.AsString;

  QRLabel21.Caption := qryMstSUP_NAME1.AsString;
  QRLabel22.Caption := qryMstSUP_ADDR1.AsString + ' ' + qryMstSUP_ADDR2.AsString;
  QRLabel23.Caption := qryMstSUP_NAME2.AsString;
  QRLabel24.Caption := qryMstSUP_ADDR3.AsString;

  IF (qryMstEMAIL_ID.AsString <> '') and (qryMstEMAIL_DOMAIN.AsString <> '') Then
    QRLabel20.Caption := qryMstEMAIL_ID.AsString + '@' + qryMstEMAIL_DOMAIN.AsString
  else
    QRLabel20.Caption := '';
end;

procedure TAPPPCR_PRINT_FRM.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TempLines : TStringList;
  nTop : Integer; 
begin
  QRLabel33.Caption := HSCodeFormat(qryDetailHS_NO.AsString);

  TempLines := TStringList.Create;
  try
    TempLines.Text := qryDetailNAME1.AsString +#13#10+ qryDetailSIZE1.AsString;
    DelEmptyLine(TempLines);
    QRMemo1.Top := 4;
    QRMemo1.Height := TempLines.Count * 14;
    DetailBand1.Height := QRMemo1.Top + (TempLines.Count * 14)+8+18;
    QRMemo1.Lines := TempLines;

    nTop := QRMemo1.Top + (TempLines.Count * 14)+8;

    //단위및 수량
    QRLabel34.Top := nTop;
    QRLabel34.Caption := FormatFloat('#,0.000',qryDetailQTY.AsCurrency)+' '+qryDetailQTYC.AsString;

    //구매일
    QRLabel35.Top := nTop;
    QRLabel35.Caption := LeftStr(qryDetailAPP_DATE.AsString,4) + '.' +
                         MidStr(qryDetailAPP_DATE.AsString,5,2)+ '.' +
                         RightStr(qryDetailAPP_DATE.AsString,2);

    //단가
    QRLabel36.Top := nTop;
    QRLabel36.Caption := '@'+FormatFloat('#,0.000',qryDetailPRI2.AsCurrency);

    QRLabel37.Enabled := qryDetailPRI1.AsCurrency > 0;
    IF qryDetailPRI1.AsCurrency > 0 Then
    begin
      DetailBand1.Height := QRMemo1.Top+(TempLines.Count * 14)+8+18+14;
      QRLabel37.Top := nTop+4;
      QRLabel37.Caption := '(@USD '+FormatFloat('#,0.000',qryDetailPRI1.AsCurrency)+')';
    end
    else
    begin
      QRLabel37.Top := 0;
    end;

    //금액
    QRLabel38.Top := nTop;
    IF qryDetailAMT1C.AsString = 'KRW' Then
      QRLabel38.Caption := qryDetailAMT1C.AsString+' '+FormatFloat('#,0',qryDetailAMT1.AsCurrency)
    else
      QRLabel38.Caption := qryDetailAMT1C.AsString+' '+FormatFloat('#,0.000',qryDetailAMT1.AsCurrency);

    QRLabel39.Enabled := qryDetailAMT2.AsCurrency > 0;
    IF qryDetailAMT2.AsCurrency > 0 Then
    begin
      DetailBand1.Height := QRMemo1.Top+(TempLines.Count * 14)+8+18+14;
      QRLabel39.Top := nTop+4;
      QRLabel39.Caption := '(USD '+FormatFloat('#,0.000',qryDetailAMT2.AsCurrency)+')';
    end
    else
    begin
      QRLabel39.Top := 0;
    end;

//    QRImage1.top := DetailBand1.top+1;

    qryDetail.Next;
  finally
    TempLines.Free;
  end;
end;

procedure TAPPPCR_PRINT_FRM.DelEmptyLine(var StrList: TStringList);
var
  nLoop : integer;
begin
  for nLoop := 0 to StrList.Count -1 do
  begin
    IF nLoop > Strlist.count Then Break;
    IF StrList.Strings[nLoop] = '' Then
    begin
      StrList.Delete(nLoop);
      DelEmptyLine(StrList);
    end;
  end;
end;

procedure TAPPPCR_PRINT_FRM.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  OPENDB;
end;

procedure TAPPPCR_PRINT_FRM.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := not qryDetail.Eof;
end;

procedure TAPPPCR_PRINT_FRM.SummaryBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  //총 수량
  QRLabel41.Caption :=  FormatFloat('#,0.000',qryMstTQTY.AsCurrency)+' '+qryMstTQTYC.AsString;

  //총금액
  IF qryMstTAMT1C.AsString = 'KRW' Then
    QRLabel42.Caption := qryMstTAMT1C.AsString +' '+ FormatFloat('#,0',qryMstTAMT1.AsCurrency)
  else
    QRLabel42.Caption := qryMstTAMT1C.AsString +' '+ FormatFloat('#,0.000',qryMstTAMT1.AsCurrency);

  QRLabel43.Enabled := qryMstTAMT2.AsCurrency > 0;  
  IF qryMstTAMT2.AsCurrency > 0 Then
    QRLabel43.Caption := 'USD ' + FormatFloat('#,0.000',qryMstTAMT2.AsCurrency);
end;

end.
