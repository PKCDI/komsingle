unit LDANTC_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QuickRpt, QRCtrls, ExtCtrls, DB, ADODB;

type
  TLDANTC_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    ChildBand1: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QR_MAINTNO: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel3: TQRLabel;
    QR_BANK1: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel5: TQRLabel;
    QR_APPNAME1: TQRLabel;
    QR_APPNAME2: TQRLabel;
    QR_APPNAME3: TQRLabel;
    ChildBand4: TQRChildBand;
    QR_BANK2: TQRLabel;
    QRLabel10: TQRLabel;
    QR_LCNO: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel12: TQRLabel;
    QR_RCNO: TQRLabel;
    ChildBand6: TQRChildBand;
    QRLabel14: TQRLabel;
    QR_AMT1: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel17: TQRLabel;
    QR_AMT2: TQRLabel;
    ChildBand8: TQRChildBand;
    QR_NGDATE: TQRLabel;
    QRLabel20: TQRLabel;
    ChildBand9: TQRChildBand;
    QRLabel21: TQRLabel;
    QR_SETDATE: TQRLabel;
    ChildBand10: TQRChildBand;
    QRLabel23: TQRLabel;
    QR_REMARK1: TQRMemo;
    ChildBand11: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel105: TQRLabel;
    QRLabel24: TQRLabel;
    QR_BKNAME1: TQRLabel;
    QR_BKNAME2: TQRLabel;
    QR_BKNAME3: TQRLabel;
    QRShape2: TQRShape;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel115: TQRLabel;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListBK_CODE: TStringField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListBK_NAME3: TStringField;
    qryListLC_NO: TStringField;
    qryListRC_NO: TStringField;
    qryListAMT1: TBCDField;
    qryListAMT1C: TStringField;
    qryListAMT2: TBCDField;
    qryListAMT2C: TStringField;
    qryListRATE: TBCDField;
    qryListRES_DATE: TStringField;
    qryListSET_DATE: TStringField;
    qryListNG_DATE: TStringField;
    qryListREMAKR1: TMemoField;
    qryListBANK1: TStringField;
    qryListBANK2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListBGM_GUBUN: TStringField;
    qryListRC_NO2: TStringField;
    qryListRC_NO3: TStringField;
    qryListRC_NO4: TStringField;
    qryListRC_NO5: TStringField;
    qryListRC_NO6: TStringField;
    qryListRC_NO7: TStringField;
    qryListRC_NO8: TStringField;
    qryListFIN_NO: TStringField;
    qryListFIN_NO2: TStringField;
    qryListFIN_NO3: TStringField;
    qryListFIN_NO4: TStringField;
    qryListFIN_NO5: TStringField;
    qryListFIN_NO6: TStringField;
    qryListFIN_NO7: TStringField;
    QRLabel4: TQRLabel;
    QR_RESDATE: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNO: string;
    procedure OPENDB;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
  end;

var
  LDANTC_PRINT_frm: TLDANTC_PRINT_frm;

implementation

{$R *.dfm}

uses MSSQL, Commonlib, strUtils, DateUtils;
{ TLDANTC_PRINT_frm }

procedure TLDANTC_PRINT_frm.OPENDB;
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryList.Open;
end;

procedure TLDANTC_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TLDANTC_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  ChildBand1.Height := ChildBand1.Tag;
  //전자문서번호
  QR_MAINTNO.Caption := Trim(qryListMAINT_NO.AsString);
  //통지일자
  QR_RESDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListRES_DATE.AsString));
end;

procedure TLDANTC_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex : Integer;
begin
  inherited;
  //통지은행
  nIndex := 1;
  for i := 1 to 2 do
  begin
    if Trim(qryList.FieldByName('BANK' + IntToStr(i)).AsString) = '' then
      Continue;

    (FindComponent('QR_BANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_BANK'+IntToStr(nIndex)) as TQRLabel).Caption := qryList.FieldByName('BANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(qryListBANK1.AsString) = '') and (Trim(qryListBANK2.AsString) = '') then
    ChildBand2.Enabled := False
  else
    ChildBand2.Height := 16 * (nIndex-1);
end;

procedure TLDANTC_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex : Integer;
begin
  inherited;
  //거래고객
  nIndex := 1;
  for i := 1 to 3 do
  begin
    if Trim(qryList.FieldByName('APP_NAME' + IntToStr(i)).AsString) = '' then
      Continue;
    (FindComponent('QR_APPNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_APPNAME'+IntToStr(nIndex)) as TQRLabel).Caption := qryList.FieldByName('APP_NAME'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(qryListAPP_NAME1.AsString) = '') and (Trim(qryListAPP_NAME2.AsString) = '') and (Trim(qryListAPP_NAME3.AsString) = '') then
    ChildBand3.Enabled := False
  else
    ChildBand3.Height := 15 * nIndex;
end;

procedure TLDANTC_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //내국신용장번호
  QR_LCNO.Caption := Trim(qryListLC_NO.AsString);
  if Trim(qryListLC_NO.AsString) = '' then
    ChildBand4.Enabled := False
  else
    ChildBand4.Height := ChildBand4.Tag;
end;

procedure TLDANTC_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //물품수령증명서번호
  QR_RCNO.Caption := Trim(qryListRC_NO.AsString);
  if Trim(qryListRC_NO.AsString) = '' then
    ChildBand5.Enabled := False
  else
    ChildBand5.Height := ChildBand5.Tag;
end;

procedure TLDANTC_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //외화어음금액
  QR_AMT1.Caption := Trim(qryListAMT1C.AsString) + ' ' + FormatCurr('#,###.####' , qryListAMT1.AsCurrency);
  if qryListAMT1.AsCurrency = 0 then
    ChildBand6.Enabled := False
  else
    ChildBand6.Height := ChildBand6.Tag;
end;

procedure TLDANTC_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //원화환산금액
  QR_AMT2.Caption := Trim(qryListAMT2C.AsString) + ' ' + FormatCurr('#,###.####', qryListAMT2.AsCurrency);
  if qryListAMT2.AsCurrency = 0 then
    ChildBand7.Enabled := False
  else
    ChildBand7.Height := ChildBand7.Tag;
end;

procedure TLDANTC_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //NEGO일자
  QR_NGDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListNG_DATE.AsString));
  if Trim(qryListNG_DATE.AsString) = '' then
    ChildBand8.Enabled := False
  else
    ChildBand8.Height := ChildBand8.Tag;
end;

procedure TLDANTC_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //최종결제일자
  QR_SETDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListSET_DATE.AsString));
  if Trim(qryListSET_DATE.AsString) = '' then
    ChildBand9.Enabled := False
  else
    ChildBand9.Height := ChildBand9.Tag;
end;

procedure TLDANTC_PRINT_frm.ChildBand10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //기타사항
  try
    memoLines := TStringList.Create;
    memoLines.Text := Trim(qryListREMAKR1.AsString);
    ChildBand10.Height := ( 15 * memoLines.Count) + 10;
    QR_REMARK1.Height := 15 * memoLines.Count;
    QR_REMARK1.Lines.Clear;
    QR_REMARK1.Lines.Text := memoLines.Text;
  finally
    memoLines.Free;
  end;
  if Trim(QR_REMARK1.Lines.Text) = '' then ChildBand10.Enabled := False;
end;

procedure TLDANTC_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //발신기관전자서명
  ChildBand11.Height := ChildBand11.Tag;
  QR_BKNAME1.Caption := Trim(qryListBK_NAME1.AsString);
  QR_BKNAME2.Caption := Trim(qryListBK_NAME2.AsString);
  QR_BKNAME3.Caption := Trim(qryListBK_NAME3.AsString);
end;

end.
