object PCRLIC_EXCEL_FRM: TPCRLIC_EXCEL_FRM
  Left = 631
  Top = 280
  Width = 367
  Height = 250
  Caption = #50808#54868#54925#46301#50857' '#44396#47588#54869#51064#49436'('#49688#49888')'#45236#50669
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object qryMst: TADOQuery
    Connection = DMMssql.KISConnect
    AfterOpen = qryMstAfterOpen
    Parameters = <>
    SQL.Strings = (
      '  SELECT MAINT_NO'
      #9#9',APP_NAME1'
      #9#9',APP_ADDR1 + '#39' '#39' + APP_ADDR2 as APP_ADDR'
      #9#9',APP_NAME2'
      #9#9',AX_NAME2 as APP_SAUP_NO'
      '                                ,AX_NAME3 as APP_SIGN'
      #9#9',[PCRLIC_H].APP_DATE as ConfirmDate'
      #9#9',SUP_NAME1'
      #9#9',SUP_NAME2'
      #9#9',RTRIM(SUP_ADDR1 + '#39' '#39' + SUP_ADDR2) as SUP_ADDR'
      #9#9',SUP_ADDR3 as SUP_SAUP_NO'#9#9
      #9#9',C1_AMT'
      #9#9',C1_C'
      #9#9',C2_AMT'
      #9#9',ITM_GBN'
      #9#9',BK_NAME1 + '#39' '#39' + BK_NAME2 as BANKNAME'
      #9#9',BK_NAME5 as BANKSIGN'
      #9#9',LIC_INFO'
      #9#9',TQTY'
      #9#9',TQTYC'
      #9#9',TAMT1'
      #9#9',TAMT1C'
      #9#9',TAMT2'
      #9#9',LIC_NO'
      #9#9',SEQ'
      #9#9',HS_NO'
      #9#9',PCRLICD1.APP_DATE as SupplyDate'
      #9#9',PCRLICD1.NAME1 as ProductName'
      #9#9',SIZE'
      #9#9',QTY'
      #9#9',QTYC'
      #9#9',PRI2'
      #9#9',PRI1'
      #9#9',AMT1'
      #9#9',AMT1C'
      '                                ,AMT2'
      #9#9',ACHG_G'
      #9#9',PCRLICD1.AC2_AMT'
      #9#9',PCRLICD1.AC1_AMT'
      #9#9',PCRLICD1.AC1_C'
      
        '  FROM [PCRLIC_H] LEFT JOIN PCRLICD1 ON PCRLIC_H.MAINT_NO = PCRL' +
        'ICD1.KEYY')
    Left = 8
    Top = 8
    object qryMstMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryMstAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryMstAPP_ADDR: TStringField
      FieldName = 'APP_ADDR'
      ReadOnly = True
      Size = 71
    end
    object qryMstAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryMstAPP_SAUP_NO: TStringField
      FieldName = 'APP_SAUP_NO'
      Size = 35
    end
    object qryMstConfirmDate: TStringField
      FieldName = 'ConfirmDate'
      Size = 8
    end
    object qryMstSUP_NAME1: TStringField
      FieldName = 'SUP_NAME1'
      Size = 35
    end
    object qryMstSUP_NAME2: TStringField
      FieldName = 'SUP_NAME2'
      Size = 35
    end
    object qryMstSUP_ADDR: TStringField
      FieldName = 'SUP_ADDR'
      ReadOnly = True
      Size = 71
    end
    object qryMstSUP_SAUP_NO: TStringField
      FieldName = 'SUP_SAUP_NO'
      Size = 35
    end
    object qryMstC1_AMT: TBCDField
      FieldName = 'C1_AMT'
      Precision = 18
    end
    object qryMstC1_C: TStringField
      FieldName = 'C1_C'
      Size = 3
    end
    object qryMstC2_AMT: TBCDField
      FieldName = 'C2_AMT'
      Precision = 18
    end
    object qryMstITM_GBN: TStringField
      FieldName = 'ITM_GBN'
      Size = 3
    end
    object qryMstBANKNAME: TStringField
      FieldName = 'BANKNAME'
      ReadOnly = True
      Size = 141
    end
    object qryMstBANKSIGN: TStringField
      FieldName = 'BANKSIGN'
      Size = 10
    end
    object qryMstLIC_INFO: TMemoField
      FieldName = 'LIC_INFO'
      BlobType = ftMemo
    end
    object qryMstTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qryMstTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryMstTAMT1: TBCDField
      FieldName = 'TAMT1'
      Precision = 18
    end
    object qryMstTAMT1C: TStringField
      FieldName = 'TAMT1C'
      Size = 3
    end
    object qryMstTAMT2: TBCDField
      FieldName = 'TAMT2'
      Precision = 18
    end
    object qryMstLIC_NO: TStringField
      FieldName = 'LIC_NO'
      Size = 35
    end
    object qryMstSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryMstHS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 10
    end
    object qryMstSupplyDate: TStringField
      FieldName = 'SupplyDate'
      Size = 8
    end
    object qryMstProductName: TMemoField
      FieldName = 'ProductName'
      BlobType = ftMemo
    end
    object qryMstSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryMstQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryMstQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryMstPRI2: TBCDField
      FieldName = 'PRI2'
      Precision = 18
    end
    object qryMstPRI1: TBCDField
      FieldName = 'PRI1'
      Precision = 18
    end
    object qryMstAMT1: TBCDField
      FieldName = 'AMT1'
      Precision = 18
    end
    object qryMstAMT1C: TStringField
      FieldName = 'AMT1C'
      Size = 3
    end
    object qryMstACHG_G: TStringField
      FieldName = 'ACHG_G'
      Size = 3
    end
    object qryMstAC2_AMT: TBCDField
      FieldName = 'AC2_AMT'
      Precision = 18
    end
    object qryMstAC1_AMT: TBCDField
      FieldName = 'AC1_AMT'
      Precision = 18
    end
    object qryMstAC1_C: TStringField
      FieldName = 'AC1_C'
      Size = 3
    end
    object qryMstAPP_SIGN: TStringField
      FieldName = 'APP_SIGN'
      Size = 10
    end
    object qryMstAMT2: TBCDField
      FieldName = 'AMT2'
      Precision = 18
    end
  end
  object qryTax: TADOQuery
    Connection = DMMssql.KISConnect
    AfterOpen = qryTaxAfterOpen
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  [KEYY]'
      '      ,[SEQ]'
      '      ,[DATEE]'
      '      ,[BILL_NO]'
      '      ,[BILL_DATE]'
      '      ,[ITEM_NAME1]'
      '      ,[ITEM_NAME2]'
      '      ,[ITEM_NAME3]'
      '      ,[ITEM_NAME4]'
      '      ,[ITEM_NAME5]'
      '      ,[ITEM_DESC]'
      '      ,[BILL_AMOUNT]'
      '      ,[BILL_AMOUNT_UNIT]'
      '      ,[TAX_AMOUNT]'
      '      ,[TAX_AMOUNT_UNIT]'
      '      ,[QUANTITY]'
      '      ,[QUANTITY_UNIT]'
      '  FROM [PCRLIC_TAX]'
      '  WHERE KEYY = :KEYY')
    Left = 40
    Top = 8
    object qryTaxKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryTaxDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryTaxBILL_NO: TStringField
      FieldName = 'BILL_NO'
      Size = 35
    end
    object qryTaxBILL_DATE: TStringField
      FieldName = 'BILL_DATE'
      Size = 8
    end
    object qryTaxITEM_NAME1: TStringField
      FieldName = 'ITEM_NAME1'
      Size = 35
    end
    object qryTaxITEM_NAME2: TStringField
      FieldName = 'ITEM_NAME2'
      Size = 35
    end
    object qryTaxITEM_NAME3: TStringField
      FieldName = 'ITEM_NAME3'
      Size = 35
    end
    object qryTaxITEM_NAME4: TStringField
      FieldName = 'ITEM_NAME4'
      Size = 35
    end
    object qryTaxITEM_NAME5: TStringField
      FieldName = 'ITEM_NAME5'
      Size = 35
    end
    object qryTaxITEM_DESC: TMemoField
      FieldName = 'ITEM_DESC'
      BlobType = ftMemo
    end
    object qryTaxBILL_AMOUNT: TBCDField
      FieldName = 'BILL_AMOUNT'
      Precision = 18
    end
    object qryTaxBILL_AMOUNT_UNIT: TStringField
      FieldName = 'BILL_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxTAX_AMOUNT: TBCDField
      FieldName = 'TAX_AMOUNT'
      Precision = 18
    end
    object qryTaxTAX_AMOUNT_UNIT: TStringField
      FieldName = 'TAX_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxQUANTITY: TBCDField
      FieldName = 'QUANTITY'
      Precision = 18
    end
    object qryTaxQUANTITY_UNIT: TStringField
      FieldName = 'QUANTITY_UNIT'
      Size = 3
    end
  end
  object sSaveDialog1: TsSaveDialog
    DefaultExt = '*.xls'
    Filter = 'Excel '#53685#54633#47928#49436' (*.xlsx)|*.xlsx|Excel 97 - 2003 '#53685#54633' '#47928#49436'(*.xls)|*.xls'
    Left = 168
    Top = 112
  end
end
