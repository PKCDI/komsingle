unit QR_ADV707_DOCS_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB;

type
  TQR_ADV707_DOCS_PRN_frm = class(TQuickRep)
    qryList: TADOQuery;
    DetailBand1: TQRBand;
    QRContent: TQRLabel;
    QRSwift: TQRLabel;
    QRTitle: TQRLabel;
    QRLabel1: TQRLabel;
    qryListDOC_DESC_1: TMemoField;
    procedure QuickRepAfterPrint(Sender: TObject);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FDocsList : TStringList;
    nCurrIndex : Integer;
  public
    FMAINT_NO : String;
    FAMD_NO : Integer;
  end;

var
  QR_ADV707_DOCS_PRN_frm: TQR_ADV707_DOCS_PRN_frm;

implementation

{$R *.DFM}

procedure TQR_ADV707_DOCS_PRN_frm.QuickRepAfterPrint(Sender: TObject);
begin
  FDocsList.Free;
end;

procedure TQR_ADV707_DOCS_PRN_frm.QuickRepBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  qryList.Close;
  qryList.Parameters[0].Value := FMAINT_NO;
  qryList.Parameters[1].Value := FAMD_NO;
  qryList.Open;

  nCurrIndex := 0;

  FDocsList := TStringList.Create;
  FDocsList.Text := qryListDOC_DESC_1.AsString;
end;

procedure TQR_ADV707_DOCS_PRN_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := nCurrIndex < FDocsList.Count;
end;

procedure TQR_ADV707_DOCS_PRN_frm.DetailBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  if nCurrIndex = 0 Then
  begin
    Sender.Height := 25;
    QRSwift.Top := 11;
    QRTitle.Top := 11;
    QRContent.Top := 11;

    QRSwift.Enabled := true;
    QRTitle.Enabled := true;
    QRtitle.Caption := 'Documents Required';
    QRLabel1.Enabled := True;
  end
  else
  begin
    QRSwift.Enabled := False;
    QRTitle.Enabled := False;
    QRLabel1.Enabled := False;

    Sender.Height := 13;
    QRSwift.Top := 1;
    QRTitle.Top := 1;
    QRContent.Top := 1;
  end;

  QRContent.Caption := FDocsList.Strings[nCurrIndex];
  Inc(nCurrIndex);
end;

end.
