unit VATBI3_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VATBI_BP_PRINT, DB, ADODB, QRCtrls, QuickRpt, ExtCtrls;

type
  TVATBI3_PRINT_frm = class(TVATBI_BP_PRINT_frm)
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListRE_NO: TStringField;
    qryListSE_NO: TStringField;
    qryListFS_NO: TStringField;
    qryListACE_NO: TStringField;
    qryListRFF_NO: TStringField;
    qryListSE_CODE: TStringField;
    qryListSE_SAUP: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_ADDR1: TStringField;
    qryListSE_ADDR2: TStringField;
    qryListSE_ADDR3: TStringField;
    qryListSE_UPTA: TStringField;
    qryListSE_UPTA1: TMemoField;
    qryListSE_ITEM: TStringField;
    qryListSE_ITEM1: TMemoField;
    qryListBY_CODE: TStringField;
    qryListBY_SAUP: TStringField;
    qryListBY_NAME1: TStringField;
    qryListBY_NAME2: TStringField;
    qryListBY_ADDR1: TStringField;
    qryListBY_ADDR2: TStringField;
    qryListBY_ADDR3: TStringField;
    qryListBY_UPTA: TStringField;
    qryListBY_UPTA1: TMemoField;
    qryListBY_ITEM: TStringField;
    qryListBY_ITEM1: TMemoField;
    qryListAG_CODE: TStringField;
    qryListAG_SAUP: TStringField;
    qryListAG_NAME1: TStringField;
    qryListAG_NAME2: TStringField;
    qryListAG_NAME3: TStringField;
    qryListAG_ADDR1: TStringField;
    qryListAG_ADDR2: TStringField;
    qryListAG_ADDR3: TStringField;
    qryListAG_UPTA: TStringField;
    qryListAG_UPTA1: TMemoField;
    qryListAG_ITEM: TStringField;
    qryListAG_ITEM1: TMemoField;
    qryListDRAW_DAT: TStringField;
    qryListDETAILNO: TBCDField;
    qryListSUP_AMT: TBCDField;
    qryListTAX_AMT: TBCDField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAMT11: TBCDField;
    qryListAMT11C: TStringField;
    qryListAMT12: TBCDField;
    qryListAMT21: TBCDField;
    qryListAMT21C: TStringField;
    qryListAMT22: TBCDField;
    qryListAMT31: TBCDField;
    qryListAMT31C: TStringField;
    qryListAMT32: TBCDField;
    qryListAMT41: TBCDField;
    qryListAMT41C: TStringField;
    qryListAMT42: TBCDField;
    qryListINDICATOR: TStringField;
    qryListTAMT: TBCDField;
    qryListSUPTAMT: TBCDField;
    qryListTAXTAMT: TBCDField;
    qryListUSTAMT: TBCDField;
    qryListUSTAMTC: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYC: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListVAT_CODE: TStringField;
    qryListVAT_TYPE: TStringField;
    qryListNEW_INDICATOR: TStringField;
    qryListSE_ADDR4: TStringField;
    qryListSE_ADDR5: TStringField;
    qryListSE_SAUP1: TStringField;
    qryListSE_SAUP2: TStringField;
    qryListSE_SAUP3: TStringField;
    qryListSE_FTX1: TStringField;
    qryListSE_FTX2: TStringField;
    qryListSE_FTX3: TStringField;
    qryListSE_FTX4: TStringField;
    qryListSE_FTX5: TStringField;
    qryListBY_SAUP_CODE: TStringField;
    qryListBY_ADDR4: TStringField;
    qryListBY_ADDR5: TStringField;
    qryListBY_SAUP1: TStringField;
    qryListBY_SAUP2: TStringField;
    qryListBY_SAUP3: TStringField;
    qryListBY_FTX1: TStringField;
    qryListBY_FTX2: TStringField;
    qryListBY_FTX3: TStringField;
    qryListBY_FTX4: TStringField;
    qryListBY_FTX5: TStringField;
    qryListBY_FTX1_1: TStringField;
    qryListBY_FTX2_1: TStringField;
    qryListBY_FTX3_1: TStringField;
    qryListBY_FTX4_1: TStringField;
    qryListBY_FTX5_1: TStringField;
    qryListAG_ADDR4: TStringField;
    qryListAG_ADDR5: TStringField;
    qryListAG_SAUP1: TStringField;
    qryListAG_SAUP2: TStringField;
    qryListAG_SAUP3: TStringField;
    qryListAG_FTX1: TStringField;
    qryListAG_FTX2: TStringField;
    qryListAG_FTX3: TStringField;
    qryListAG_FTX4: TStringField;
    qryListAG_FTX5: TStringField;
    qryListSE_NAME3: TStringField;
    qryListBY_NAME3: TStringField;
    qryListINDICATOR_NAME: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TBCDField;
    qryGoodsDE_DATE: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsDE_REM1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsSUPAMT: TBCDField;
    qryGoodsTAXAMT: TBCDField;
    qryGoodsUSAMT: TBCDField;
    qryGoodsUSAMT_G: TStringField;
    qryGoodsSUPSTAMT: TBCDField;
    qryGoodsTAXSTAMT: TBCDField;
    qryGoodsUSSTAMT: TBCDField;
    qryGoodsUSSTAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsRATE: TBCDField;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand23BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand15BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand18BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand19BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand20BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand21BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand17BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand29BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand30BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand31BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand32BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand33BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand22BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure SummaryBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand25BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand26BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand27BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNO: string;
    procedure OPENDB;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
  end;

var
  VATBI3_PRINT_frm: TVATBI3_PRINT_frm;

implementation

{$R *.dfm}

uses Commonlib, MSSQL;

{ TVATBI3_PRINT_frm }

procedure TVATBI3_PRINT_frm.OPENDB;
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryList.Open;

  qryGoods.Close;
  qryGoods.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryGoods.Open;
end;

procedure TVATBI3_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TVATBI3_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand1-------------------------------------------------------
  //전자문서번호
  ChildBand1.Height := ChildBand1.Tag;
  QR_MAINT_NO.Caption := qryListMAINT_NO.AsString;
  if Trim(QR_MAINT_NO.Caption) = '' then
  begin
    ChildBand1.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand2-------------------------------------------------------
  //책번호
  ChildBand2.Height := ChildBand2.Tag;
  QR_RENO.Caption := qryListRE_NO.AsString;
  if Trim(QR_RENO.Caption) = '' then
  begin
    ChildBand2.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand3-------------------------------------------------------
  //호번호
  ChildBand3.Height := ChildBand3.Tag;
  QR_SENO.Caption := qryListSE_NO.AsString;
  if Trim(QR_SENO.Caption) = '' then
  begin
    ChildBand3.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand23BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand23-------------------------------------------------------
  //일련번호
  ChildBand23.Height := ChildBand23.Tag;
  QR_FSNO.Caption := qryListFS_NO.AsString;
  if Trim(QR_FSNO.Caption) = '' then
  begin
    ChildBand23.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand4-------------------------------------------------------
  //관련참조번호
  ChildBand4.Height := ChildBand4.Tag;
  QR_ACENO.Caption := qryListACE_NO.AsString;
  if Trim(QR_ACENO.Caption) = '' then
  begin
    ChildBand4.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand5-------------------------------------------------------
  //세금계산서번호
  ChildBand5.Height := ChildBand5.Tag;
  QR_RFFNO.Caption := qryListRFF_NO.AsString;
  if Trim(QR_RFFNO.Caption) = '' then
  begin
    ChildBand5.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand7-------------------------------------------------------
  //공급자등록번호
  ChildBand7.Height := ChildBand7.Tag;
  QR_SESAUP.Caption := qryListSE_SAUP.AsString;
  if Trim(QR_SESAUP.Caption) = '' then
  begin
    ChildBand7.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex,i : Integer;
begin
  inherited;
  //---------ChildBand8-------------------------------------------------------
   //공급자 상호
      nIndex := 1;
      for i := 1 to 2 do
      begin
        if Trim(qrylist.FieldByName('SE_NAME'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_SENAME'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_SENAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_SENAME'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('SE_NAME'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if (Trim(QR_SENAME1.Caption) = '') and (Trim(QR_SENAME2.Caption) = '') then
      begin
        ChildBand8.Enabled := False;
      end
      else
        ChildBand8.Height := (15 * (nIndex-1));
end;

procedure TVATBI3_PRINT_frm.ChildBand10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand10-------------------------------------------------------
   //공급자주소
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(qrylist.FieldByName('SE_ADDR'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_SEADDR'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_SEADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_SEADDR'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('SE_ADDR'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if (Trim(QR_SEADDR1.Caption) = '') and (Trim(QR_SEADDR2.Caption) = '') then
      begin
        ChildBand10.Enabled := False;
      end
      else
        ChildBand10.Height := (15 * (nIndex-1));
end;

procedure TVATBI3_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand11-------------------------------------------------------
  //공급자 대표자명
  ChildBand11.Height := ChildBand11.Tag;
  QR_SENAME3.Caption := qryListSE_NAME3.AsString;
  if Trim(QR_SENAME3.Caption) = '' then
  begin
    ChildBand11.Enabled := False;
  end
end;

procedure TVATBI3_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand12-------------------------------------------------------
  //공급자 업태
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListSE_UPTA1.AsString;
    ChildBand12.Height := ( 13 * memoLines.Count) + 4;
    QR_SEUPTA1.Height := ( 13 * memoLines.Count);

    QR_SEUPTA1.Lines.Clear;
    QR_SEUPTA1.Lines.Text := qryListSE_UPTA1.AsString;

    if QR_SEUPTA1.Lines.Count = 0 then
    begin
      ChildBand12.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand13-------------------------------------------------------
  //공급자 종목
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListSE_ITEM1.AsString;
    ChildBand13.Height := ( 13 * memoLines.Count) + 4;
    QR_SEITEM1.Height := ( 13 * memoLines.Count);

    QR_SEITEM1.Lines.Clear;
    QR_SEITEM1.Lines.Text := qryListSE_ITEM1.AsString;

    if QR_SEITEM1.Lines.Count = 0 then
    begin
      ChildBand13.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand15BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand15-------------------------------------------------------
  //공급받는자 등록번호
  ChildBand15.Height := ChildBand15.Tag;
  QR_BYSAUP.Caption := qryListBY_SAUP.AsString;
  if Trim(QR_BYSAUP.Caption) = '' then
  begin
    ChildBand15.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand16BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand16-------------------------------------------------------
   //공급받는자 상호
      nIndex := 1;
      for i := 1 to 2 do
      begin
        if Trim(qrylist.FieldByName('BY_NAME'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_BYNAME'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_BYNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_BYNAME'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('BY_NAME'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if (Trim(QR_BYNAME1.Caption) = '') and (Trim(QR_BYNAME2.Caption) = '') then
      begin
        ChildBand16.Enabled := False;
      end
      else
        ChildBand16.Height := (15 * (nIndex-1));
end;

procedure TVATBI3_PRINT_frm.ChildBand18BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand18-------------------------------------------------------
  //공급받는자주소
    nIndex := 1;
    for i := 1 to 5 do
    begin
      if Trim(qrylist.FieldByName('BY_ADDR'+IntToStr(i)).AsString) = '' then
      begin
        (FindComponent('QR_BYADDR'+IntToStr(i)) as TQRLabel).Caption := '';
        Continue;
      end;

      (FindComponent('QR_BYADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_BYADDR'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('BY_ADDR'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    if (Trim(QR_BYADDR1.Caption) = '') and (Trim(QR_BYADDR2.Caption) = '') then
    begin
      ChildBand18.Enabled := False;
    end
    else
      ChildBand18.Height := (15 * (nIndex-1));
end;

procedure TVATBI3_PRINT_frm.ChildBand19BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand19-------------------------------------------------------
  //공급받는자 대표자명
  ChildBand19.Height := ChildBand19.Tag;
  QR_BYNAME3.Caption := qryListBY_NAME3.AsString;
  if Trim(QR_BYNAME3.Caption) = '' then
  begin
    ChildBand19.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand20BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand20-------------------------------------------------------
  //공급받는자 업태
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListBY_UPTA1.AsString;
    ChildBand20.Height := ( 13 * memoLines.Count) + 4;
    QR_BYUPTA1.Height := ( 13 * memoLines.Count);

    QR_BYUPTA1.Lines.Clear;
    QR_BYUPTA1.Lines.Text := qryListBY_UPTA1.AsString;

    if QR_BYUPTA1.Lines.Count = 0 then
    begin
      ChildBand20.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand21BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand21-------------------------------------------------------
  //공급받는자 종목
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListBY_ITEM1.AsString;
    ChildBand21.Height := ( 13 * memoLines.Count) + 10;
    QR_BYITEM1.Height := ( 13 * memoLines.Count);

    QR_BYITEM1.Lines.Clear;
    QR_BYITEM1.Lines.Text := qryListBY_ITEM1.AsString;

    if QR_BYITEM1.Lines.Count = 0 then
    begin
      ChildBand21.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand17BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand17-------------------------------------------------------
  //수탁자 등록번호
  ChildBand17.Height := ChildBand17.Tag;
  QR_AGSAUP.Caption := qryListAG_SAUP.AsString;
  if Trim(QR_AGSAUP.Caption) = '' then
  begin
    ChildBand17.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand29BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand29-------------------------------------------------------
 //수탁자 상호
    nIndex := 1;
    for i := 1 to 2 do
    begin
      if Trim(qrylist.FieldByName('AG_NAME'+IntToStr(i)).AsString) = '' then
      begin
        (FindComponent('QR_AGNAME'+IntToStr(i)) as TQRLabel).Caption := '';
        Continue;
      end;

      (FindComponent('QR_AGNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_AGNAME'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('AG_NAME'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    if (Trim(QR_AGNAME1.Caption) = '') and (Trim(QR_AGNAME2.Caption) = '') then
    begin
      ChildBand29.Enabled := False;
    end
    else
      ChildBand29.Height := (15 * (nIndex-1));
end;

procedure TVATBI3_PRINT_frm.ChildBand30BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand30-------------------------------------------------------
  //수탁자주소
    nIndex := 1;
    for i := 1 to 5 do
    begin
      if Trim(qrylist.FieldByName('AG_ADDR'+IntToStr(i)).AsString) = '' then
      begin
        (FindComponent('QR_AGADDR'+IntToStr(i)) as TQRLabel).Caption := '';
        Continue;
      end;

      (FindComponent('QR_AGADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_AGADDR'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('AG_ADDR'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    if (Trim(QR_AGADDR1.Caption) = '') and (Trim(QR_AGADDR2.Caption) = '') then
    begin
      ChildBand30.Enabled := False;
    end
    else
      ChildBand30.Height := (15 * (nIndex-1))  ;
end;

procedure TVATBI3_PRINT_frm.ChildBand31BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand31-------------------------------------------------------
  //수탁자 대표자명
  ChildBand31.Height := ChildBand31.Tag;
  QR_AGNAME3.Caption := qryListAG_NAME3.AsString;
  if Trim(QR_AGNAME3.Caption) = '' then
  begin
    ChildBand31.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand32BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand32-------------------------------------------------------
  //수탁자 업태
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListAG_UPTA1.AsString;
    ChildBand32.Height := ( 13 * memoLines.Count) + 4;
    QR_AGUPTA1.Height := ( 13 * memoLines.Count);

    QR_AGUPTA1.Lines.Clear;
    QR_AGUPTA1.Lines.Text := qryListAG_UPTA1.AsString;

    if QR_AGUPTA1.Lines.Count = 0 then
    begin
      ChildBand32.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;

end;

procedure TVATBI3_PRINT_frm.ChildBand33BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //---------ChildBand33-------------------------------------------------------
  //수탁자 종목
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryListAG_ITEM1.AsString;
    ChildBand33.Height := ( 13 * memoLines.Count) + 4;
    QR_AGITEM1.Height := ( 13 * memoLines.Count);

    QR_AGITEM1.Lines.Clear;
    QR_AGITEM1.Lines.Text := qryListAG_ITEM1.AsString;

    if QR_AGITEM1.Lines.Count = 0 then
    begin
      ChildBand33.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //---------ChildBand9-------------------------------------------------------
  //수탁자 타이틀 -> 상호가 없을경우 미출력
  ChildBand9.Height := ChildBand9.Tag;
  if (Trim(QR_AGNAME1.Caption) = '') and (Trim(QR_AGNAME2.Caption) = '') then
  begin
    ChildBand9.Enabled := False;
  end;
end;

procedure TVATBI3_PRINT_frm.ChildBand22BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //작성일
  QR_DRAWDAT.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryListDRAW_DAT.AsString));
  //공란수
  QR_DETAILNO.Caption := FormatFloat('#,##0.####' , qryListDETAILNO.AsFloat);
  //공급가액
  QR_SUPAMT.Caption := FormatFloat('#,##0.####' , qryListSUP_AMT.AsFloat);
  //세액
  QR_TAXAMT.Caption := FormatFloat('#,##0.####' , qryListTAX_AMT.AsFloat);
  //비고
  try
    //TStringList생성
    memoLines := TStringList.Create;
    memoLines.Text := qryListREMARK1.AsString;
    ChildBand22.Height := ( 13 * memoLines.Count) + 40;
    QR_REMARK1.Height := ( 13 * memoLines.Count);

    QR_REMARK1.Lines.Clear;
    QR_REMARK1.Lines.Text := qryListREMARK1.AsString;

    if QR_REMARK1.Lines.Count = 0 then
    begin
      ChildBand22.Height := 59;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TVATBI3_PRINT_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
  RowCount,nIndex,i : Integer;
begin
  inherited;
  //공급일--------------------------------------------------------------------------------------
  QR_DEDATED.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryGoodsDE_DATE.AsString));

  //단가----------------------------------------------------------------------------------------
  QR_PRICED.Caption := '@' + FormatFloat('#,##0.####' , qryGoodsPRICE.AsFloat) + '/'
                        + FormatFloat('#,##0.####' , qryGoodsQTYG.AsFloat) + qryGoodsQTYG_G.AsString;

  //수량----------------------------------------------------------------------------------------
  QR_STQTYD.Caption := FormatFloat('#,##0.####', qryGoodsSTQTY.AsFloat) + ' ' + qryGoodsSTQTY_G.AsString;

  //공급가액------------------------------------------------------------------------------------
  QR_SUPSTAMTD.Caption := '\' + FormatFloat('#,##0.####', qryGoodsSUPSTAMT.AsFloat);

  //외화공급가액--------------------------------------------------------------------------------
  QR_USSTAMTD.Caption := qryGoodsUSSTAMT_G.AsString + ' ' + FormatFloat('#,##0.####' , qryGoodsUSSTAMT.AsFloat);

  //세액----------------------------------------------------------------------------------------
  QR_TAXSTAMTD.Caption := '\' + FormatFloat('#,##0.####' , qryGoodsTAXSTAMT.AsFloat);

  //환율----------------------------------------------------------------------------------------
  QR_RATE_D.Caption := FormatFloat('#,##0.####' , qryGoodsRATE.AsFloat);

  try
    //품명----------------------------------------------------------------------
    //TStringList생성
    memoLines := TStringList.Create;
    //memoLines에 NAME1 대입
    memoLines.Text := qryGoodsNAME1.AsString;

    if memoLines.Count > 0 then
    begin
      QR_NAME1D.Height := ( 13 * memoLines.Count);

      QR_NAME1D.Lines.Clear;
      QR_NAME1D.Lines.Text := qryGoodsNAME1.AsString;
      QR_NAME1D.Enabled := True;
    end
    else
    begin
      QR_NAME1D.Enabled := False;
      QR_NAME1D.Height := 0;
    end;
    //--------------------------------------------------------------------------


    //규격----------------------------------------------------------------------
    //TStringList생성
    memoLines := TStringList.Create;
    //memoLines에 SIZE1 대입
    memoLines.Text := qryGoodsSIZE1.AsString;

    if memoLines.Count > 0 then
    begin
      QR_SIZE1D.Height := ( 13 * (memoLines.Count+1));

      QR_SIZE1D.Lines.Clear;

      if memoLines.Count > 0 then
        QR_SIZE1D.Lines.Add('** 규격 **');

      QR_SIZE1D.Lines.Add(qryGoodsSIZE1.AsString);
      QR_SIZE1D.Top := QR_NAME1D.Height + 4;
      QR_SIZE1D.Enabled := True;
    end
    else
    begin
      QR_SIZE1D.Enabled := False;
      QR_SIZE1D.Height := 0;
    end;
    //--------------------------------------------------------------------------

    //비고----------------------------------------------------------------------
    //TStringList생성
    memoLines := TStringList.Create;
    //memoLines에 DE_REM1 대입
    memoLines.Text := qryGoodsDE_REM1.AsString;

    if memoLines.Count > 0 then
    begin
      QR_DEREM1D.Height := ( 13 * (memoLines.Count+1));

      QR_DEREM1D.Lines.Clear;

      if memoLines.Count > 0 then
        QR_DEREM1D.Lines.Add('** 비고 **');

      QR_DEREM1D.Lines.Add(qryGoodsDE_REM1.AsString);
      QR_DEREM1D.Top := QR_NAME1D.Height + QR_SIZE1D.Height + 4;
      QR_DEREM1D.Enabled := True;
    end
    else
    begin
      QR_DEREM1D.Enabled := False;
      QR_DEREM1D.Height := 0;
    end;
    //--------------------------------------------------------------------------

    //품명,규격,비고의  값이 하나도 없는경우 기본높이 설정
    if (QR_NAME1D.Enabled = False) and (QR_SIZE1D.Enabled = False) and (QR_DEREM1D.Enabled = False) then
      DetailBand1.Height := 42
    else
      DetailBand1.Height := QR_NAME1D.Height + QR_SIZE1D.Height + QR_DEREM1D.Height + 10;

    qryGoods.Next;

  finally
      memoLines.Free;
  end;
end;

procedure TVATBI3_PRINT_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  //MoreData의 값이 true이면 실행
  MoreData := not qryGoods.Eof;
end;

procedure TVATBI3_PRINT_frm.SummaryBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //TOTAL-----------------------------------------------------------------------
  //총공급가액
  QR_SUPTAMT.Caption := '\'+FormatFloat('#,##0.####' , qryListSUPTAMT.AsFloat);
  //총외화공급가액
  QR_USTAMT.Caption := qryListUSTAMTC.AsString + ' ' + FormatFloat('#,##0.####' , qryListUSTAMT.AsFloat);
  //총세액
  QR_TAXTAMT.Caption := FormatFloat('#,##0.####' , qryListTAXTAMT.AsFloat);
end;

procedure TVATBI3_PRINT_frm.ChildBand25BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //합계금액
  QR_TAMT.Caption := '\' + FormatFloat('#,##0.####' , qryListTAMT.AsFloat);
end;

procedure TVATBI3_PRINT_frm.ChildBand26BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex  : Integer;
begin
  inherited;
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if (qryList.FieldByName('AMT'+IntToStr(i)+'1').AsFloat = 0) and (qryList.FieldByName('AMT'+IntToStr(i)+'2').AsFloat = 0) then
    begin
      (FindComponent('label'+IntToStr(i)) as TQRLabel).Caption := '';
      Continue;
    end;

    (FindComponent('label'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_AMT'+IntToStr(nIndex)+'1') as TQRLabel).Enabled := True;
    (FindComponent('QR_AMT'+IntToStr(nIndex)+'2') as TQRLabel).Enabled := True;

    case i of
      1 : (FindComponent('label'+IntToStr(nIndex)) as TQRLabel).Caption := '[현 금] : ';
      2 : (FindComponent('label'+IntToStr(nIndex)) as TQRLabel).Caption := '[수 표] : ';
      3 : (FindComponent('label'+IntToStr(nIndex)) as TQRLabel).Caption := '[어 음] : ';
      4 : (FindComponent('label'+IntToStr(nIndex)) as TQRLabel).Caption := '[외 상] : ';
    end;

    (FindComponent('QR_AMT'+IntToStr(nIndex)+'1') as TQRLabel).Caption :=  FormatFloat('#,##0.####' , qryList.FieldByName('AMT'+IntToStr(i)+'1').AsFloat);
    (FindComponent('QR_AMT'+IntToStr(nIndex)+'2') as TQRLabel).Caption :=  FormatFloat('#,##0.####' ,  qryList.FieldByName('AMT'+IntToStr(i)+'2').AsFloat);

    Inc(nIndex); // i 와 nIndex의 값의 변화가 중요

  end;

  if (Trim(label1.Caption) = '') and (Trim(label2.Caption) = '') and (Trim(label3.Caption) = '')  and (Trim(label4.Caption) = '') then
  begin
    ChildBand26.Enabled := False;
  end
  else
     ChildBand26.Height := (35 * (nIndex-1))  ;
end;

procedure TVATBI3_PRINT_frm.ChildBand27BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  QR_INDICATOR.Caption := qryListINDICATOR_NAME.AsString;

  if ChildBand26.Enabled = False then QRImage13.Enabled := False;
end;

end.
