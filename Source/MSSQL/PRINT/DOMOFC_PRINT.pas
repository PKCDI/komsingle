unit DOMOFC_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QuickRpt, QRCtrls, ExtCtrls, DB, ADODB;

type
  TDOMOFC_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel7: TQRLabel;
    QR_SRNAME1: TQRLabel;
    QR_SRNAME2: TQRLabel;
    QR_SRNAME3: TQRLabel;                                                                    
    ChildBand2: TQRChildBand;
    ChildBand4: TQRChildBand;
    ChildBand5: TQRChildBand;
    ChildBand6: TQRChildBand;
    ChildBand7: TQRChildBand;
    ChildBand8: TQRChildBand;
    ChildBand9: TQRChildBand;
    ChildBand10: TQRChildBand;
    QRLabel8: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel10: TQRLabel;
    QRImage3: TQRImage;
    QRImage1: TQRImage;
    QRLabel9: TQRLabel;
    QR_OFRDATE: TQRLabel;
    QRLabel5: TQRLabel;
    QR_OFRNO: TQRLabel;
    QRLabel6: TQRLabel;
    QR_HSCODE: TQRLabel;
    QRLabel4: TQRLabel;
    QR_UDRENO: TQRLabel;
    QRLabel3: TQRLabel;
    QR_UDNAME1: TQRLabel;
    QR_UDNAME2: TQRLabel;
    QR_UDNAME3: TQRLabel;
    QR_UDADDR1: TQRLabel;
    QR_UDADDR2: TQRLabel;
    QR_UDADDR3: TQRLabel;
    QR_SRADDR1: TQRLabel;
    QR_SRADDR2: TQRLabel;
    QR_SRADDR3: TQRLabel;
    DetailBand1: TQRBand;
    qryDetail: TADOQuery;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_CHK: TStringField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME_CHK: TStringField;
    qryDetailNAME_COD: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTY_G: TStringField;
    qryDetailQTYG: TBCDField;
    qryDetailQTYG_G: TStringField;
    qryDetailPRICE: TBCDField;
    qryDetailPRICE_G: TStringField;
    qryDetailAMT: TBCDField;
    qryDetailAMT_G: TStringField;
    qryDetailSTQTY: TBCDField;
    qryDetailSTQTY_G: TStringField;
    qryDetailSTAMT: TBCDField;
    qryDetailSTAMT_G: TStringField;
    QR_HSNO: TQRLabel;
    QR_NAME1: TQRMemo;
    QR_SIZE1: TQRMemo;
    QR_QTY: TQRLabel;
    QR_PRICE: TQRLabel;
    QR_AMT: TQRLabel;
    QRImage2: TQRImage;
    QRImage4: TQRImage;
    QR_SubTotal: TQRLabel;
    QR_STQTY: TQRLabel;
    QR_STAMT: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel17: TQRLabel;
    QR_TQTYCUR: TQRLabel;
    QR_TQTY: TQRLabel;
    QR_TAMTCUR: TQRLabel;
    QR_TAMT: TQRLabel;
    QRImage5: TQRImage;
    ChildBand11: TQRChildBand;
    QR_ORGN1LOC: TQRLabel;
    QR_ORGN1N: TQRLabel;
    QR_ORGN2LOC: TQRLabel;
    QR_ORGN2N: TQRLabel;
    QR_ORGN3LOC: TQRLabel;
    QR_ORGN3N: TQRLabel;
    QR_ORGN4LOC: TQRLabel;
    QR_ORGN4N: TQRLabel;
    QR_ORGN5LOC: TQRLabel;
    QR_ORGN5N: TQRLabel;
    QRLabel20: TQRLabel;
    ChildBand12: TQRChildBand;
    QRLabel31: TQRLabel;
    QR_PAYETC5: TQRLabel;
    QR_PAYETC4: TQRLabel;
    QR_PAYETC3: TQRLabel;
    QR_PAYETC2: TQRLabel;
    QR_PAYETC1: TQRLabel;
    ChildBand13: TQRChildBand;
    QRLabel37: TQRLabel;
    QR_OFRSQ5: TQRLabel;
    QR_OFRSQ4: TQRLabel;
    QR_OFRSQ3: TQRLabel;
    QR_OFRSQ2: TQRLabel;
    QR_OFRSQ1: TQRLabel;
    ChildBand14: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_TSTINST5: TQRLabel;
    QR_TSTINST4: TQRLabel;
    QR_TSTINST3: TQRLabel;
    QR_TSTINST2: TQRLabel;
    QR_TSTINST1: TQRLabel;
    ChildBand15: TQRChildBand;
    QRLabel16: TQRLabel;
    QR_PCKINST5: TQRLabel;
    QR_PCKINST4: TQRLabel;
    QR_PCKINST3: TQRLabel;
    QR_PCKINST2: TQRLabel;
    QR_PCKINST1: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel18: TQRLabel;
    QR_TRNSIDNAME: TQRLabel;
    ChildBand17: TQRChildBand;
    QRLabel19: TQRLabel;
    QR_LOADLOC: TQRLabel;
    ChildBand18: TQRChildBand;
    QRLabel21: TQRLabel;
    QR_DESTLOC: TQRLabel;
    ChildBand19: TQRChildBand;
    QRLabel22: TQRLabel;
    QR_LOADTXT5: TQRLabel;
    QR_LOADTXT4: TQRLabel;
    QR_LOADTXT3: TQRLabel;
    QR_LOADTXT2: TQRLabel;
    QR_LOADTXT1: TQRLabel;
    ChildBand20: TQRChildBand;
    QRLabel23: TQRLabel;
    QR_DESTTXT5: TQRLabel;
    QR_DESTTXT4: TQRLabel;
    QR_DESTTXT3: TQRLabel;
    QR_DESTTXT2: TQRLabel;
    QR_DESTTXT1: TQRLabel;
    ChildBand21: TQRChildBand;
    QRLabel24: TQRLabel;
    QR_REMARK1: TQRMemo;
    SummaryBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRLabel25: TQRLabel;
    ChildBand22: TQRChildBand;
    QRShape1: TQRShape;
    QRLabel126: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel26: TQRLabel;
    QR_SRNO: TQRLabel;
    QR_UDNO: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
     FKEY : String; 
     procedure PrintDocument(Fields: TFields; DetailFields: TFields; uPreview: Boolean = True);
end;

var
  DOMOFC_PRINT_frm: TDOMOFC_PRINT_frm;

implementation

uses Commonlib, MSSQL;

{$R *.dfm}

{ TDOMOFC_PRINT_frm }

procedure TDOMOFC_PRINT_frm.PrintDocument(Fields: TFields; DetailFields : TFields; uPreview: Boolean);
var
  i,j,nIndex : Integer;
  memoLines : TStringList;

begin
  inherited;

  FFields := Fields;
  FDetailFields := DetailFields;
  FPreview := uPreview;
  
  with Fields do
  begin
  //---------ChildBand1-------------------------------------------------------
      //발행자
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if i = 4 then
        begin
          if Trim(FieldByName('SR_NO').AsString) = '' then
          begin
            (FindComponent('QR_SRNO') as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_SRNO') as TQRLabel).Enabled := True;
          (FindComponent('QR_SRNO') as TQRLabel).Caption  := '등록번호 : ' + FieldByName('SR_NO').AsString;

          if Trim(FieldByName('SR_NAME3').AsString)= '' then
            QR_SRNO.Top := QR_SRNAME3.Top;  //전자서명에 값이없고 발행번호에 값이있을경우 짤리는 현상 방지

          Inc(nIndex);
        end
        else
        begin
          if Trim(FieldByName('SR_NAME'+IntToStr(i)).AsString) = '' then
          begin
            (FindComponent('QR_SRNAME'+IntToStr(i)) as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_SRNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_SRNAME'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('SR_NAME'+IntToStr(i)).AsString;

           Inc(nIndex);
        end;
      end;

      if Trim(QR_SRNAME1.Caption) = '' then
      begin
        ChildBand1.Enabled := False;
      end
      else
        ChildBand1.Height := (11 * (nIndex)) ;
  //----------------------------------------------------------------------------

  //---------ChildBand2-------------------------------------------------------
      //발행자 주소
      nIndex := 1;
      for i := 1 to 3 do
      begin
        if Trim(FieldByName('SR_ADDR'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_SRADDR'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_SRADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_SRADDR'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('SR_ADDR'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if (Trim(QR_SRADDR1.Caption) = '') and (Trim(QR_SRADDR2.Caption) = '') then
      begin
        ChildBand2.Enabled := False;
      end
      else
        ChildBand2.Height := (10 * (nIndex)) ;
  //----------------------------------------------------------------------------

  //---------ChildBand4-------------------------------------------------------
      //수요자
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if i = 4 then
        begin
          if Trim(FieldByName('UD_NO').AsString) = '' then
          begin
            (FindComponent('QR_UDNO') as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_UDNO') as TQRLabel).Enabled := True;
          (FindComponent('QR_UDNO') as TQRLabel).Caption  := '사업자등록번호 : ' + FieldByName('UD_NO').AsString;

          Inc(nIndex);
        end
        else
        begin
          if Trim(FieldByName('UD_NAME'+IntToStr(i)).AsString) = '' then
          begin
            (FindComponent('QR_UDNAME'+IntToStr(i)) as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_UDNAME'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_UDNAME'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('UD_NAME'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;
      end;

      if (Trim(QR_UDNAME1.Caption) = '') then
      begin
        ChildBand4.Enabled := False;
      end
      else
        ChildBand4.Height := (12 * (nIndex)) ;
  //----------------------------------------------------------------------------

  //---------ChildBand5-------------------------------------------------------
   //수요자 주소
      nIndex := 1;
      for i := 1 to 3 do
      begin
        if Trim(FieldByName('UD_ADDR'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_UDADDR'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_UDADDR'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('UD_ADDR'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if (Trim(QR_UDADDR1.Caption) = '') and (Trim(QR_UDADDR2.Caption) = '') then
      begin
        ChildBand5.Enabled := False;
        (FindComponent('QR_UDADDR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      end
      else
        ChildBand5.Height := (10 * (nIndex)) ;
  //----------------------------------------------------------------------------

  //---------ChildBand6--------------------------------------------------------
    //수요자참조번호
    ChildBand6.Height := ChildBand6.Tag;
    QR_UDRENO.Caption := getValueFromField('UD_RENO');
      if Trim(QR_UDRENO.Caption) = '' then
      begin
        ChildBand6.Enabled := False;
      end;
  //---------------------------------------------------------------------------

  //---------ChildBand7--------------------------------------------------------
     //대표공급HS코드
    ChildBand7.Height := ChildBand7.Tag;
    QR_HSCODE.Caption := getValueFromField('HS_CODE');

      if Trim(QR_HSCODE.Caption) = '' then
      begin
        ChildBand7.Enabled := False;
      end;
  //---------------------------------------------------------------------------

  //---------ChildBand8--------------------------------------------------------
     //발행번호
    ChildBand8.Height := ChildBand8.Tag;
    QR_OFRNO.Caption := getValueFromField('OFR_NO');

      if Trim(QR_OFRNO.Caption) = '' then
      begin
        ChildBand8.Enabled := False;
      end;
  //---------------------------------------------------------------------------

  //---------ChildBand9--------------------------------------------------------
     //발행일자
    ChildBand9.Height := ChildBand9.Tag;
    QR_OFRDATE.Caption :=  FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('OFR_DATE').AsString));

      if Trim(QR_OFRDATE.Caption) = '' then
      begin
        ChildBand9.Enabled := False;
      end;
  //---------------------------------------------------------------------------

  //---------ChildBand3--------------------------------------------------------
    ChildBand3.Height := ChildBand3.Tag+10;
    //총수량
    QR_TQTYCUR.Caption :=  getValueFromField('TQTYCUR');
    QR_TQTY.Caption := FormatFloat('#,##0', FieldByName('TQTY').AsFloat);
     //총합계
    QR_TAMTCUR.Caption :=  getValueFromField('TAMTCUR');
    QR_TAMT.Caption := FormatFloat('#,##0', FieldByName('TAMT').AsFloat);

      if (Trim(QR_TAMT.Caption) = '') or (Trim(QR_TQTY.Caption) = '')  then
      begin
        ChildBand3.Enabled := False;
      end;

  //---------ChildBand11-------------------------------------------------------
   //원산지
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('ORGN'+IntToStr(i)+'N').AsString) = '' then
        begin
          (FindComponent('QR_ORGN'+IntToStr(i)+'N') as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_ORGN'+IntToStr(nIndex)+'N') as TQRLabel).Enabled := True;
        (FindComponent('QR_ORGN'+IntToStr(nIndex)+'N') as TQRLabel).Caption := FieldByName('ORGN'+IntToStr(i)+'N').AsString;

        (FindComponent('QR_ORGN'+IntToStr(nIndex)+'LOC') as TQRLabel).Enabled := True;
        (FindComponent('QR_ORGN'+IntToStr(nIndex)+'LOC') as TQRLabel).Caption := FieldByName('ORGN'+IntToStr(i)+'LOC').AsString;
        Inc(nIndex);
      end;

      if (Trim(QR_ORGN1N.Caption) = '') and (Trim(QR_ORGN2N.Caption) = '') then
      begin
        ChildBand11.Enabled := False;
      end
      else
        ChildBand11.Height := (13 * (nIndex)) ;
  //----------------------------------------------------------------------------

  //---------ChildBand12-------------------------------------------------------
   //결제조건
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('PAY_ETC'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_PAYETC'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_PAYETC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_PAYETC'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('PAY_ETC'+IntToStr(i)).AsString;

        Inc(nIndex);
      end;

      if (Trim(QR_PAYETC1.Caption) = '') and (Trim(QR_PAYETC2.Caption) = '') then
      begin
        ChildBand12.Enabled := False;
      end
      else
        ChildBand12.Height := (13 * (nIndex)) ;
  //----------------------------------------------------------------------------

  //---------ChildBand13-------------------------------------------------------
   //유효기간
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('OFR_SQ'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_OFRSQ'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_OFRSQ'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_OFRSQ'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('OFR_SQ'+IntToStr(i)).AsString;

        Inc(nIndex);
      end;

      if (Trim(QR_OFRSQ1.Caption) = '') and (Trim(QR_OFRSQ2.Caption) = '') then
      begin
        ChildBand13.Enabled := False;
      end
      else
        ChildBand13.Height := (13 * (nIndex)) ;
  //----------------------------------------------------------------------------
  //---------ChildBand14-------------------------------------------------------
   //검사방법
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('TSTINST'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_TSTINST'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_TSTINST'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_TSTINST'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('TSTINST'+IntToStr(i)).AsString;

        Inc(nIndex);
      end;

      if (Trim(QR_TSTINST1.Caption) = '') and (Trim(QR_TSTINST2.Caption) = '') then
      begin
        ChildBand14.Enabled := False;
      end
      else
        ChildBand14.Height := (13 * (nIndex)) ;
  //----------------------------------------------------------------------------

  //---------ChildBand15-------------------------------------------------------
   //포장방법
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('PCKINST'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_PCKINST'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_PCKINST'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_PCKINST'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('PCKINST'+IntToStr(i)).AsString;

        Inc(nIndex);
      end;

      if (Trim(QR_PCKINST1.Caption) = '') and (Trim(QR_PCKINST2.Caption) = '') then
      begin
        ChildBand15.Enabled := False;
      end
      else
        ChildBand15.Height := (13 * (nIndex)) ;
  //----------------------------------------------------------------------------
   //---------ChildBand16--------------------------------------------------------
     //운송방법
    ChildBand16.Height := ChildBand16.Tag;
    QR_TRNSIDNAME.Caption :=  (FieldByName('TRANSNME').AsString);

      if Trim(QR_TRNSIDNAME.Caption) = '' then
      begin
        ChildBand16.Enabled := False;
      end;
  //---------------------------------------------------------------------------

  //---------ChildBand17--------------------------------------------------------
     //선적지
    ChildBand17.Height := ChildBand17.Tag;
    QR_LOADLOC.Caption :=  (FieldByName('LOADDNAME').AsString) + '   ' + (FieldByName('LOADLOC').AsString);

      if Trim(QR_LOADLOC.Caption) = '' then
      begin
        ChildBand17.Enabled := False;
      end;
  //---------------------------------------------------------------------------

  //---------ChildBand18--------------------------------------------------------
     //도착지
    ChildBand18.Height := ChildBand18.Tag;
    QR_DESTLOC.Caption :=  (FieldByName('DESTNAME').AsString) + '   ' + (FieldByName('DESTLOC').AsString);

      if Trim(QR_DESTLOC.Caption) = '' then
      begin
        ChildBand18.Enabled := False;
      end;
  //---------------------------------------------------------------------------

  //---------ChildBand19-------------------------------------------------------
   //선적시기
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('LOADTXT'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_LOADTXT'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_LOADTXT'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_LOADTXT'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('LOADTXT'+IntToStr(i)).AsString;

        Inc(nIndex);
      end;

      if (Trim(QR_LOADTXT1.Caption) = '') and (Trim(QR_LOADTXT2.Caption) = '') then
      begin
        ChildBand19.Enabled := False;
      end
      else
        ChildBand19.Height := (13 * (nIndex)) ;
  //----------------------------------------------------------------------------

  //---------ChildBand20-------------------------------------------------------
  //도착시기
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('DESTTXT'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_DESTTXT'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_DESTTXT'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_DESTTXT'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('DESTTXT'+IntToStr(i)).AsString;

        Inc(nIndex);
      end;

      if (Trim(QR_DESTTXT1.Caption) = '') and (Trim(QR_DESTTXT2.Caption) = '') then
      begin
        ChildBand20.Enabled := False;
      end
      else
        ChildBand20.Height := (13 * (nIndex)) ;
  //----------------------------------------------------------------------------


  //---------ChildBand21------------------------------------------------------
      //기타참조사항
      try
        //TStringList생성
        memoLines := TStringList.Create;
        //memoLines에 REMARK_1 대입
        memoLines.Text := FieldByName('REMARK_1').AsString;

        ChildBand21.Height := ( 13 * memoLines.Count) + 26;
        QR_REMARK1.Height := ( 13 * memoLines.Count);

        QR_REMARK1.Lines.Clear;
        getValueMemo(QR_REMARK1,'REMARK_1');

        //if Trim(QR_REMARK1.Caption) = '' then
        if QR_REMARK1.Lines.Count = 0 then
        begin
          ChildBand21.Enabled := False;
        end;
      finally
        memoLines.Free;
      end;

  end;

//  RunPrint;

end;

procedure TDOMOFC_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  qryDetail.Close;
//  qryDetail.Parameters.ParamByName('KEYY').Value := UI_DOMOFC_frm.qryDetail.FieldByName('KEYY').AsString;
  qryDetail.Parameters.ParamByName('KEYY').Value := FKEY;
  qryDetail.Open;
end;

procedure TDOMOFC_PRINT_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  //MoreData의 값이 true이면 실행
  MoreData := not qryDetail.Eof;
end;

procedure TDOMOFC_PRINT_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
  RowCount,nIndex,i : Integer;
begin
  inherited;
  try
    //HS번호
    QR_HSNO.Caption := qryDetail.FieldByName('HS_NO').AsString;

    //수량
    QR_QTY.Caption := FormatFloat('#,##0', qryDetail.FieldByName('QTY').AsFloat) + ' ' + qryDetail.FieldByName('QTY_G').AsString;

    //단가
    QR_PRICE.Caption := qryDetail.FieldByName('PRICE_G').AsString + ' ' + FormatFloat('#,##0', qryDetail.FieldByName('PRICE').AsFloat)
                      +' / ' +  qryDetail.FieldByName('QTYG_G').AsString + ' ' + FormatFloat('#,##0', qryDetail.FieldByName('QTYG').AsFloat);

    //금액
    QR_AMT.Caption := qryDetail.FieldByName('AMT_G').AsString + ' ' + FormatFloat('#,##0', qryDetail.FieldByName('AMT').AsFloat);

    RowCount := 0;
    
      //품명
      //TStringList생성
      memoLines := TStringList.Create;
      //memoLines에 NAME1 대입
      memoLines.Text := qryDetail.FieldByName('NAME1').AsString;

      QR_NAME1.Height := ( 13 * memoLines.Count);

      QR_NAME1.Lines.Clear;
      QR_NAME1.Lines.Text := qryDetail.FieldByName('NAME1').AsString;
      //getValueDetailMemo(QR_NAME1,'NAME1');

      RowCount := memoLines.Count;


      //규격
      //TStringList생성
      memoLines := TStringList.Create;
      //memoLines에 SIZE1 대입
      memoLines.Text := qryDetail.FieldByName('SIZE1').AsString;

      QR_SIZE1.Height := ( 13 * memoLines.Count);

      QR_SIZE1.Lines.Clear;
      QR_SIZE1.Lines.Text := qryDetail.FieldByName('SIZE1').AsString;

      if RowCount < memoLines.Count then RowCount := memoLines.Count;



    QRImage2.Top := 40 + (rowCount*14) + 5;
    QR_SubTotal.Top := QRImage2.Top + 8;

    //소량소계
    QR_STQTY.Top := QRImage2.Top + 8;
    QR_STQTY.Caption := FormatFloat('#,##0', qryDetail.FieldByName('STQTY').AsFloat) + ' ' + qryDetail.FieldByName('STQTY_G').AsString;

    //금액소계
    QR_STAMT.Top := QRImage2.Top + 8;
    QR_STAMT.Caption := qryDetail.FieldByName('STAMT_G').AsString + ' ' + FormatFloat('#,##0', qryDetail.FieldByName('STAMT').AsFloat);

    QRImage4.Top := QR_STAMT.Top + 15;

    DetailBand1.Height := QRImage4.Top + 10;

    qryDetail.Next;

  finally
      memoLines.Free;
  end;
end;

end.



