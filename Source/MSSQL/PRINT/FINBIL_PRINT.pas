unit FINBIL_PRINT;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB,StrUtils;

type
  TFINBIL_PRINT_frm = class(TQuickRep)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRMemo1: TQRMemo;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRShape5: TQRShape;
    ColumnHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    SummaryBand1: TQRBand;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRShape10: TQRShape;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    fin: TStringField;
    qryListBUS: TStringField;
    qryListBUS_DESC: TStringField;
    qryListDOC_NO1: TStringField;
    qryListDOC_NO2: TStringField;
    qryListDOC_NO3: TStringField;
    qryListDOC_NO4: TStringField;
    qryListTRN_DATE: TStringField;
    qryListADV_DATE: TStringField;
    qryListTERM: TStringField;
    qryListPAYMENT: TStringField;
    qryListSPECIAL1: TStringField;
    qryListSPECIAL2: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListBANK: TStringField;
    qryListBNKNAME1: TStringField;
    qryListBNKNAME2: TStringField;
    qryListCUSTNAME1: TStringField;
    qryListCUSTNAME2: TStringField;
    qryListCUSTNAME3: TStringField;
    qryListCUSTSIGN1: TStringField;
    qryListCUSTSIGN2: TStringField;
    qryListCUSTSIGN3: TStringField;
    qryListPAYORD1: TBCDField;
    qryListPAYORD11: TBCDField;
    qryListPAYORD1C: TStringField;
    qryListRATE1: TBCDField;
    qryListPAYORD2: TBCDField;
    qryListPAYORD21: TBCDField;
    qryListPAYORD2C: TStringField;
    qryListRATE2: TBCDField;
    qryListPAYORD3: TBCDField;
    qryListPAYORD31: TBCDField;
    qryListPAYORD3C: TStringField;
    qryListRATE3: TBCDField;
    qryListPAYORD4: TBCDField;
    qryListPAYORD41: TBCDField;
    qryListPAYORD4C: TStringField;
    qryListRATE4: TBCDField;
    qryListPAYORD5: TBCDField;
    qryListPAYORD51: TBCDField;
    qryListPAYORD5C: TStringField;
    qryListRATE5: TBCDField;
    qryListFOBRATE: TBCDField;
    qryListRATEIN: TBCDField;
    qryListFINAMT1: TBCDField;
    qryListFINAMT1C: TStringField;
    qryListFINAMT2: TBCDField;
    qryListFINAMT2C: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListBUS_NAME: TStringField;
    qryListTERM_NAME: TStringField;
    qryListPAYMENT_NAME: TStringField;
    qryListSPECIAL1_NAME: TStringField;
    qryListSPECIAL2_NAME: TStringField;
    qryDetail: TADOQuery;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TStringField;
    qryDetailCHCODE: TStringField;
    qryDetailCHCODE_NAME: TStringField;
    qryDetailBILLNO: TStringField;
    qryDetailCHRATE: TBCDField;
    qryDetailAMT1: TBCDField;
    qryDetailAMT1C: TStringField;
    qryDetailAMT2: TBCDField;
    qryDetailAMT2C: TStringField;
    qryDetailKRWRATE: TBCDField;
    qryDetailCHDAY: TSmallintField;
    qryDetailCHDATE1: TStringField;
    qryDetailCHDATE2: TStringField;
    qryDetailBETWEENDATE: TStringField;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel70: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel71: TQRLabel;
    QRDBText12: TQRDBText;
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qryDetailAMT1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryDetailAMT2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryListFINAMT1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryListFINAMT2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private

  public
    procedure Run(Keyy : String; isPreview :Boolean=True);
    procedure RunOnlyDB(Keyy : String);
  end;

var
  FINBIL_PRINT_frm: TFINBIL_PRINT_frm;

implementation

uses MSSQL, Commonlib;

{$R *.DFM}

Const
  FORMAT_KRW = '#,0';
  FORMAT_USD = '#,0.000';
procedure TFINBIL_PRINT_frm.Run(Keyy : String; isPreview :Boolean=True);
begin
  qryList.Parameters.ParamByName('MAINT_NO').Value := Keyy;
  qryList.Open;
  qryDetail.Parameters.ParamByName('KEYY').Value := Keyy;
  qryDetail.Open;

  IF isPreview Then Self.Preview
  else
  begin
    Self.PrinterSetup;
    If Self.QRPrinter.Master.Tag = 1 Then
      Self.Print;
  end;

end;

procedure TFINBIL_PRINT_frm.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nLoop, i : integer;
  nNo : integer;
  FORMAT_MONEY : String;
begin
  //전자문서번호
  QRLabel4.Caption := '전자문서 번호: '+qryListMAINT_NO.AsString;

  //통지일자
  QRLabel5.Caption := '통지일자: '+LeftStr(qryListADV_DATE.AsString,4)+'.'+MidStr(qryListADV_DATE.AsString,5,2)+'.'+RightStr(qryListADV_DATE.AsString,2);

  //발급은행
  QRLabel15.Caption := qryListBNKNAME1.AsString;
  QRLabel16.Caption := qryListBNKNAME2.AsString;

  //거래고객
  QRLabel17.Caption := qryListCUSTNAME1.AsString;

  //계산서용도
  QRLabel18.Caption := qryListBUS_NAME.AsString;

  //세부거래명
  QRLabel19.Caption := qryListBUS_DESC.AsString;

  //신용장(계약서)번호
  QRLabel20.Caption := qryListDOC_NO1.AsString;

  //신청번호
  QRLabel21.Caption := qryListDOC_NO2.AsString;
  //계산서번호
  QRLabel22.Caption := qryListDOC_NO3.AsString;
  //거래일자
  QRLabel23.Caption := decodeDateStr(qryListTRN_DATE.AsString);
  //기타정보
  QRMemo1.Lines.Text := qryListREMARK1.AsString;

  //외화총액
  QRLabel37.Caption := qryList.FieldByName('PAYORD1C').AsString +' '+ CTNULL(FORMAT_MONEY,qryList.FieldByName('PAYORD1').AsCurrency);
  QRLabel38.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATE1').AsCurrency);
  QRLabel39.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('FOBRATE').AsCurrency);
  QRLabel40.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATEIN').AsCurrency);
  QRLabel41.Caption := CTNULL(FORMAT_KRW,qryList.FieldByName('PAYORD11').AsCurrency);
  //외화대체
  QRLabel42.Caption := qryList.FieldByName('PAYORD2C').AsString +' '+ CTNULL(FORMAT_MONEY,qryList.FieldByName('PAYORD2').AsCurrency);
  QRLabel43.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATE2').AsCurrency);
  QRLabel44.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('FOBRATE').AsCurrency);
  QRLabel45.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATEIN').AsCurrency);
  QRLabel46.Caption := CTNULL(FORMAT_KRW,qryList.FieldByName('PAYORD21').AsCurrency);
  //현물환
  QRLabel47.Caption := qryList.FieldByName('PAYORD3C').AsString +' '+ CTNULL(FORMAT_MONEY,qryList.FieldByName('PAYORD3').AsCurrency);
  QRLabel48.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATE3').AsCurrency);
  QRLabel49.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('FOBRATE').AsCurrency);
  QRLabel50.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATEIN').AsCurrency);
  QRLabel51.Caption := CTNULL(FORMAT_KRW,qryList.FieldByName('PAYORD31').AsCurrency);
  //선물환
  QRLabel52.Caption := qryList.FieldByName('PAYORD4C').AsString +' '+ CTNULL(FORMAT_MONEY,qryList.FieldByName('PAYORD4').AsCurrency);
  QRLabel53.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATE4').AsCurrency);
  QRLabel54.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('FOBRATE').AsCurrency);
  QRLabel55.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATEIN').AsCurrency);
  QRLabel56.Caption := CTNULL(FORMAT_KRW,qryList.FieldByName('PAYORD41').AsCurrency);
  //수입보증금
  QRLabel57.Caption := qryList.FieldByName('PAYORD5C').AsString +' '+ CTNULL(FORMAT_MONEY,qryList.FieldByName('PAYORD5').AsCurrency);
  QRLabel58.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATE5').AsCurrency);
  QRLabel59.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('FOBRATE').AsCurrency);
  QRLabel60.Caption := CTNULL(FORMAT_USD,qryList.FieldByName('RATEIN').AsCurrency);
  QRLabel61.Caption := CTNULL(FORMAT_KRW,qryList.FieldByName('PAYORD51').AsCurrency);

end;

procedure TFINBIL_PRINT_frm.qryDetailAMT1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  IF qryDetailAMT1.AsCurrency > 0 Then
  begin
    IF qryDetailAMT1C.AsString = 'KRW' Then
      Text := qryDetailAMT1C.AsString +' '+ FormatFloat('#,0',qryDetailAMT1.AsCurrency)
    else
      Text := qryDetailAMT1C.AsString +' '+ FormatFloat('#,0.000',qryDetailAMT1.AsCurrency);
  end
  else
    Text := '';
end;

procedure TFINBIL_PRINT_frm.qryDetailAMT2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  IF qryDetailAMT2.AsCurrency > 0 Then
  begin
    IF qryDetailAMT2C.AsString = 'KRW' Then
      Text := qryDetailAMT2C.AsString +' '+ FormatFloat('#,0',qryDetailAMT2.AsCurrency)
    else
      Text := qryDetailAMT2C.AsString +' '+ FormatFloat('#,0.000',qryDetailAMT2.AsCurrency);
  end
  else
    Text := '';
end;

procedure TFINBIL_PRINT_frm.qryListFINAMT1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  IF qryListFINAMT1.AsCurrency > 0 Then
  begin
    IF qryListFINAMT1C.AsString = 'KRW' Then
      Text := qryListFINAMT1C.AsString +' '+ FormatFloat('#,0',qryListFINAMT1.AsCurrency)
    else
      Text := qryListFINAMT1C.AsString +' '+ FormatFloat('#,0.000',qryListFINAMT1.AsCurrency);
  end
  else
    Text := '';
end;

procedure TFINBIL_PRINT_frm.qryListFINAMT2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  IF qryListFINAMT2.AsCurrency > 0 Then
  begin
    IF qryListFINAMT2C.AsString = 'KRW' Then
      Text := qryListFINAMT2C.AsString +' '+ FormatFloat('#,0',qryListFINAMT2.AsCurrency)
    else
      Text := qryListFINAMT2C.AsString +' '+ FormatFloat('#,0.000',qryListFINAMT2.AsCurrency);
  end
  else
    Text := '';
end;

procedure TFINBIL_PRINT_frm.RunOnlyDB(Keyy: String);
begin
  qryList.Parameters.ParamByName('MAINT_NO').Value := Keyy;
  qryList.Open;
  qryDetail.Parameters.ParamByName('KEYY').Value := Keyy;
  qryDetail.Open;
end;

procedure TFINBIL_PRINT_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
      QRDBText12.Enabled := (qryDetail.FieldByName('CHDAY').AsInteger > 0); 
      QRDBText11.Enabled := (qryDetail.FieldByName('KRWRATE').AsInteger > 0);
      QRDBText4.Enabled := (qryDetail.FieldByName('CHRATE').AsInteger > 0);

end;

end.
