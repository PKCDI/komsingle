unit APPPCR_PRINT1;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils;

type
  TAPPPCR_PRINT1_FRM = class(TQuickRep)
    ColumnHeaderBand1: TQRBand;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRBand1: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRMemo1: TQRMemo;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    qryList: TADOQuery;
    qryListKEYY: TStringField;
    qryListSEQ: TIntegerField;
    qryListDOC_G: TStringField;
    qryListDOC_NAME: TStringField;
    qryListDOC_D: TStringField;
    qryListBHS_NO: TStringField;
    qryListBNAME: TStringField;
    qryListBNAME1: TMemoField;
    qryListBAMT: TBCDField;
    qryListBAMTC: TStringField;
    qryListEXP_DATE: TStringField;
    qryListSHIP_DATE: TStringField;
    qryListBSIZE1: TMemoField;
    qryListBAL_NAME1: TStringField;
    qryListBAL_NAME2: TStringField;
    qryListBAL_CD: TStringField;
    qryListNAT: TStringField;
    QRImage1: TQRImage;
    procedure qryListBAMTGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qryListSHIP_DATEGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
  private
    FDocNo : String;
    Procedure OPENDB;
  public
    property DocNo:String  read FDocNo write FDocNo;
  end;

var
  APPPCR_PRINT1_FRM: TAPPPCR_PRINT1_FRM;

implementation

uses MSSQL;

{$R *.DFM}

procedure TAPPPCR_PRINT1_FRM.OPENDB;
begin
  with qryList do
  begin
    Close;
    Parameters.ParamByName('KEYY').Value := FDocNo;
    Open;
  end;
end;

procedure TAPPPCR_PRINT1_FRM.qryListBAMTGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  IF qryListBAMTC.AsString = 'KRW' then
    Text := qryListBAMTC.AsString + ' ' + FormatFloat('#,0',qryListBAMT.AsCurrency)
  else
    Text := qryListBAMTC.AsString + ' ' + FormatFloat('#,0.000',qryListBAMT.AsCurrency);
end;

procedure TAPPPCR_PRINT1_FRM.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  OPENDB;
end;

Const
  BANDDEFAULT = 59;
  MEMODEFAULT = 28;
procedure TAPPPCR_PRINT1_FRM.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TempStr : TStringList;
begin
  TempStr := TStringList.Create;
  try
    TempStr.Text := qryListBNAME1.AsString;

    IF TempStr.Count > 2 Then
    begin
      QRMemo1.Height := TempStr.Count * 14;
      QRMemo1.Lines.Text := TempStr.Text;
      QRBand1.Height := 4+(TempStr.Count * 14) + 4 + 13 + 4;
      QRDBText6.Top := 4 + (TempStr.Count * 14) + 4;
      QRDBText7.Top := 4 + (TempStr.Count * 14) + 4;
      QRDBText8.Top := 4 + (TempStr.Count * 14) + 4;
      QRImage1.Top := QRBand1.Height - 5;
    end
    else
    begin
      QRMemo1.Height := MEMODEFAULT;
      QRBand1.Height := BANDDEFAULT;
      QRMemo1.Lines.Text := TempStr.Text;
      QRDBText6.Top := 38;
      QRDBText7.Top := 38;
      QRDBText8.Top := 38;
      QRImage1.Top := 54;
    end;

    QRImage1.Enabled := not (qryList.RecordCount = qryList.RecNo);

  finally
    TempStr.Free;
  end;
end;

procedure TAPPPCR_PRINT1_FRM.qryListSHIP_DATEGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  IF TRIM(qryListSHIP_DATE.AsString) <> '' Then
  begin
    Text := LeftStr(qryListSHIP_DATE.AsString,4)+'.'+
            MidStr(qryListSHIP_DATE.AsString,5,2)+'.'+
            RightStr(qryListSHIP_DATE.AsString,2);
  end;

end;

end.
