unit QR_APP707_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils, Clipbrd;

type
  TDOCINFO = record
    MAINT_NO :String;
    MSEQ, AMD_NO : integer;
  end;

  TQR_APP707_PRN_frm = class(TQuickRep)
    TitleBand1: TQRBand;
    QRLabel39: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QR_CHILD_TOP: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel6: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC_TEL: TQRLabel;
    QR_AP_BANK: TQRLabel;
    QR_AP_BANK1: TQRLabel;
    QR_AP_BANK_TEL: TQRLabel;
    QR_AD_BANK: TQRLabel;
    QR_AD_BANK1: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QR_APPDATE: TQRLabel;
    QR_IN_MATHOD: TQRLabel;
    M_ADINFO: TQRMemo;
    QRLabel16: TQRLabel;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListMSeq: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_PAY: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListCD_NO: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListprno: TIntegerField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListIS_CANCEL: TStringField;
    qryListDOC_CD: TStringField;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TBooleanField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListSRBUHO: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListCARRIAGE: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListCHARGE: TStringField;
    qryListCHARGE_1: TMemoField;
    qryListAMD_CHARGE: TStringField;
    qryListAMD_CHARGE_1: TMemoField;
    qryListSPECIAL_PAY: TMemoField;
    qryListGOODS_DESC: TMemoField;
    qryListDOC_REQ: TMemoField;
    qryListADD_CONDITION: TMemoField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListMIX1: TStringField;
    qryListMIX2: TStringField;
    qryListMIX3: TStringField;
    qryListMIX4: TStringField;
    qryListDEFPAY1: TStringField;
    qryListDEFPAY2: TStringField;
    qryListDEFPAY3: TStringField;
    qryListDEFPAY4: TStringField;
    qryListPERIOD_DAYS: TIntegerField;
    qryListPERIOD_IDX: TIntegerField;
    qryListPERIOD_DETAIL: TStringField;
    qryListCONFIRM: TStringField;
    qryListCONFIRM_BIC: TStringField;
    qryListCONFIRM1: TStringField;
    qryListCONFIRM2: TStringField;
    qryListCONFIRM3: TStringField;
    qryListCONFIRM4: TStringField;
    qryListAPPLIC_CHG: TBooleanField;
    qryListAPPLIC_CHG1: TStringField;
    qryListAPPLIC_CHG2: TStringField;
    qryListAPPLIC_CHG3: TStringField;
    qryListAPPLIC_CHG4: TStringField;
    qryListBENEFC_CHG: TBooleanField;
    qryListmathod_Name: TStringField;
    qryListpay_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListCarriage_Name: TStringField;
    qryListDOC_CDNM: TStringField;
    QR_SWIFTTITLE: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel10: TQRLabel;
    QR_CHILD_COMMON: TQRChildBand;
    QRLabel35: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel44: TQRLabel;
    QR_20: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QR_31C: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QR_26E: TQRLabel;
    QR_AD_BANK2: TQRLabel;
    QR_CANCEL: TQRChildBand;
    QRLabel19: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel31: TQRLabel;
    QR_CHILD_40A: TQRChildBand;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel28: TQRLabel;
    QR_CHILD_31D: TQRChildBand;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QR_CHILD_50: TQRChildBand;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QR_CHILD_59: TQRChildBand;
    QRLabel43: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QR_CHILD_32B: TQRChildBand;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QR_CHILD_39A: TQRChildBand;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QR_CHILD_39C: TQRChildBand;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QR_CHILD_42M: TQRChildBand;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QR_CHILD_42C: TQRChildBand;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QR_CHILD_42P: TQRChildBand;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QR_CHILD_43P: TQRChildBand;
    QRLabel111: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QR_CHILD_43T: TQRChildBand;
    QRLabel112: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QR_CHILD_44A: TQRChildBand;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QR_CHILD_44E: TQRChildBand;
    QRLabel121: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel128: TQRLabel;
    QR_CHILD_44F: TQRChildBand;
    QRLabel127: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QR_CHILD_44B: TQRChildBand;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QR_CHILD_44C: TQRChildBand;
    QRLabel139: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel140: TQRLabel;
    QR_CHILD_44D: TQRChildBand;
    QRLabel143: TQRLabel;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel146: TQRLabel;
    QR_CHILD_45B: TQRChildBand;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    M_45B: TQRMemo;
    QR_CHILD_46B: TQRChildBand;
    QRLabel149: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    M_46B: TQRMemo;
    QR_CHILD_47B: TQRChildBand;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel156: TQRLabel;
    M_47B: TQRMemo;
    QR_CHILD_49M: TQRChildBand;
    QRLabel157: TQRLabel;
    QRLabel158: TQRLabel;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    M_49M: TQRMemo;
    QR_CHILD_71D: TQRChildBand;
    QRLabel161: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel163: TQRLabel;
    M_71D: TQRMemo;
    QR_CHILD_71N: TQRChildBand;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    M_71N: TQRMemo;
    QR_CHILD_48: TQRChildBand;
    QRLabel167: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel169: TQRLabel;
    QR_48: TQRLabel;
    QR_CHILD_49: TQRChildBand;
    QRLabel170: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QR_49: TQRLabel;
    QR_CHILD_58a: TQRChildBand;
    QRLabel173: TQRLabel;
    QRLabel174: TQRLabel;
    QRLabel175: TQRLabel;
    M_58a: TQRMemo;
    QR_CHILD_SIGN: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel189: TQRLabel;
    QRShape9: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRLabel191: TQRLabel;
    QRLabel192: TQRLabel;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabel195: TQRLabel;
    QRLabel196: TQRLabel;
    QRLabel197: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel199: TQRLabel;
    QRShape20: TQRShape;
    QRLabel200: TQRLabel;
    QRLabel201: TQRLabel;
    QRLabel202: TQRLabel;
    QRLabel203: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRShape21: TQRShape;
    QR_CHILD_RFF: TQRChildBand;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRMemo1: TQRMemo;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QR_CHILD_TOPBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_COMMONBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CANCELBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_40ABeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_31DBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_50BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_59BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_32BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_39ABeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_39CBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_42CBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_42MBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_42PBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_43PBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_43TBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44ABeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44EBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44FBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44CBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_44DBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_45BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_46BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_47BBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_49MBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_71DBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_71NBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_48BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_49BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_58aBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_SIGNBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QR_CHILD_RFFBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FIDX : integer;
    FDOCINFO : TDOCINFO;
    FDetailData : TStringList;
    function getMAINT_NO: string;
    function getMSEQ: Integer;
    procedure setMAINT_NO(const Value: string);
    procedure setMSEQ(const Value: Integer);
    procedure SetAMDNO(const Value: Integer);
    function GetAMDNO: Integer;
    procedure DataInit;
  public
    property MAINT_NO : string read getMAINT_NO write setMAINT_NO;
    property MSEQ : Integer read getMSEQ write setMSEQ;
    property AMDNO : Integer read GetAMDNO write SetAMDNO;
    function SumText(StrList : array of string):String;
  end;

var
  QR_APP707_PRN_frm: TQR_APP707_PRN_frm;

implementation

{$R *.DFM}

function TQR_APP707_PRN_frm.getMAINT_NO: string;
begin
  Result := FDOCINFO.MAINT_NO;
end;

function TQR_APP707_PRN_frm.getMSEQ: Integer;
begin
  Result := FDOCINFO.MSEQ;
end;

procedure TQR_APP707_PRN_frm.setMAINT_NO(const Value: string);
begin
  FDOCINFO.MAINT_NO := Value;
end;

procedure TQR_APP707_PRN_frm.setMSEQ(const Value: Integer);
begin
  FDOCINFO.MSEQ := Value;
end;

function TQR_APP707_PRN_frm.SumText(StrList: array of string): String;
var
  i : Integer;
begin
  Result := '';
  i := 0;
  while i <= High(StrList) do
  begin
    IF i+1 <= High(StrList) Then
    begin
      Result := Trim( Result+' '+(StrList[i])+' '+Trim(StrList[i+1]) );
      Inc(i); Inc(i);
    end
    else
    begin
      Result := Trim( Result+' '+(StrList[i]));
      Inc(i);
    end;

  end;
end;

procedure TQR_APP707_PRN_frm.SetAMDNO(const Value: Integer);
begin
  FDOCINFO.AMD_NO := Value;
end;

function TQR_APP707_PRN_frm.GetAMDNO: Integer;
begin
  Result := FDOCINFO.MSEQ;
end;

procedure TQR_APP707_PRN_frm.DataInit;
var
  TempStr : string;
begin
  FDetailData := TStringList.Create;

  with FDetailData do
  begin
    // 20 Sender's Reference
    Add('!20');
    Add('|Sender''s Reference');
    Add(qryListCD_NO.AsString);
    // 31C date of Issue(YYMMDD)
    Add('!31C');
    Add('|Date of Issue(YYMMDD)');    
    Add( RightStr(qryListISS_DATE.AsString,6));
    // 26E Number of Amendment
    Add('!26E');
    Add('|Number of Amendment');
    Add(qryListAMD_NO.AsString);
    // 23S Cancellation Request
    IF qryListIS_CANCEL.AsString <> '' Then
    begin
      Add('!23S');
      Add('|Cancellation Request');
    end;
    // 40A Form of Documentary Credit 신용장종류(C)
    IF qryListDOC_CD.AsString <> '' Then
    begin
      Add('!40A');
      Add('|Form of Documentary Credit');
      Add(qryListDOC_CDNM.AsString);
    end;
    // 31D Date(YYMMDD) and Place of expiry (Group C)
    IF (qryListEX_DATE.AsString <> '') AND (qryListEX_PLACE.AsString <> '') then
    begin
      Add('!31D');
      Add('|Date(YYMMDD) and Place of');
      Add('|Expiry');
      Add(RightStr(qryListEX_DATE.AsString,6)+qryListEX_PLACE.AsString);
    end;
    // 50 Changed Applicant Details
    IF qryListAPPLIC_CHG.AsBoolean Then
    begin
      Add('!50');
      Add('|Changed Applicant Details');
      Add(SumText([qryListAPPLIC_CHG1.AsString, qryListAPPLIC_CHG2.AsString]));
      Add(SumText([qryListAPPLIC_CHG3.AsString, qryListAPPLIC_CHG4.AsString]));
    end;
    // 59 Beneficiary
    IF qryListBENEFC_CHG.AsBoolean Then
    begin
      Add('!59');
      Add('|Beneficiary');
      Add(SumText([qryListBENEFC1.AsString, qryListBENEFC2.AsString]));
      Add(SumText([qryListBENEFC3.AsString, qryListBENEFC4.AsString]));
      Add(Trim(qryListBENEFC5.AsString));
    end;

    // 32B, 33B Increase/Decrease of Documentary Credit Amount
    IF Trim(qryListINCD_CUR.AsString) <> '' then
    begin
      Add('!32B');
      Add('|Increase of Documentary Credit');
      Add('|Amount');
      Add(UpperCase(qryListINCD_CUR.AsString)+FormatFloat('#,0.##',qryListINCD_AMT.AsCurrency));
    end
    else
    IF Trim(qryListDECD_CUR.AsString) <> '' then
    begin
      Add('!33B');
      Add('|Decrease of Documentary Credit');
      Add('|Amount');
      Add(UpperCase(qryListDECD_CUR.AsString)+FormatFloat('#,0.##',qryListDECD_AMT.AsCurrency));
    end;

    // 39A Percentage Credit Amount Tolerance(+/-)
    IF (qryListCD_PERP.AsInteger > 0) OR (qryListCD_PERM.AsInteger > 0) then
    begin
      Add('!39A');
      Add('|Percentage Credit Amount');
      Add('|Tolerance(+/-)');
      Add(qryListCD_PERP.AsString +'/'+ qryListCD_PERM.AsString);
    end;

    //39C Additional Amounts Covered
    IF Trim(SumText([qryListAA_CV1.AsString, qryListAA_CV2.AsString, qryListAA_CV3.AsString, qryListAA_CV4.AsString])) <> '' then
    begin
      Add('!39C');
      Add('|Additional Amounts Covered');
      Add(SumText([qryListAA_CV1.AsString, qryListAA_CV2.AsString]));
      Add(SumText([qryListAA_CV3.AsString, qryListAA_CV4.AsString]));
    end;

    // 42C Drafts at...
    IF Trim(SumText([qryListDRAFT1.AsString, qryListDRAFT2.AsString, qryListDRAFT3.AsString])) <> '' Then
    begin
      Add('!42C');
      Add('|Drafts at...');
      Add(SumText([qryListDRAFT1.AsString, qryListDRAFT2.AsString, qryListDRAFT3.AsString]));
    end
    else
    // 42M Mixed payment Details
    IF Trim(SumText([qryListMIX1.AsString, qryListMIX2.AsString, qryListMIX3.AsString, qryListMIX4.AsString])) <> '' Then
    begin
      Add('!42M');
      Add('|Mixed Payment Details');
      Add(SumText([qryListMIX1.AsString, qryListMIX2.AsString]));
      Add(SumText([qryListMIX3.AsString, qryListMIX4.AsString]));
    end
    else
    // 42P negotiation/Deferred payment Details
    IF Trim(SumText([qryListDEFPAY1.AsString, qryListDEFPAY2.AsString, qryListDEFPAY3.AsString, qryListDEFPAY4.AsString])) <> '' Then
    begin
      Add('!42P');
      Add('|Negotiation/Deferred Payment');
      Add('|Details');
      Add(SumText([qryListDEFPAY1.AsString, qryListDEFPAY2.AsString]));
      Add(SumText([qryListDEFPAY3.AsString, qryListDEFPAY4.AsString]));
    end;

    // 43P Partial Shipments
    IF Trim(qryListPSHIP.AsString) <> '' Then
    begin
      Add('!43P');
      Add('|Partial Shipments');
      Case AnsiIndexText(qryListPSHIP.AsString,['9','10']) of
        0: TempStr := 'ALLOWED';
        1: TempStr := 'NOT ALLOWED';
      end;
      Add(TempStr);
    end;

    // 43T Transhipment
    IF Trim(qryListTSHIP.AsString) <> '' Then
    begin
      Add('!43T');
      Add('|Transhipment');
      Case AnsiIndexText(qryListPSHIP.AsString,['7','8']) of
        0: TempStr := 'ALLOWED';
        1: TempStr := 'NOT ALLOWED';
      end;
      Add(TempStr);
    end;

    // 44A Place of taking in Charge/Dispatch from.../Place of Receipt
    IF Trim(qryListLOAD_ON.AsString) <> '' Then
    begin
      Add('!44A');
      Add('|Place of Taking in Charge/Dispatch');
      Add('|from.../Place of Receipt');
      Add(Trim(qryListLOAD_ON.AsString));
    end;
    // 44E Port of Loading/Airport of Departure
    IF Trim(qryListSUNJUCK_PORT.AsString) <> '' Then
    begin
      Add('!44E');
      Add('|Port of Loading/Aiport of');
      Add('|Departure');
      Add(Trim(qryListSUNJUCK_PORT.AsString));
    end;
    // 44F Port of Discharge/Airport of Destination
    IF Trim(qryListDOCHACK_PORT.AsString) <> '' Then
    begin
      Add('!44F');
      Add('|Port of Discharge/Airport of');
      Add('|Destination');
      Add(Trim(qryListDOCHACK_PORT.AsString));
    end;
    // 44B Place of Final Destination/For Transportation to .../ Place of Delivery
    IF qryListFOR_TRAN.AsString <> '' Then
    begin
      Add('!44B');
      Add('|Place of Final Destination/For');
      Add('|Transportation to .../Place of');
      Add('|Delivery');
      Add(Trim(qryListFOR_TRAN.AsString));
    end;
    // 44C latest date of Shipment(YYMMDD)
    IF qryListLST_DATE.AsString <> '' Then
    begin
      Add('!44C');
      Add('|Latest date of');
      Add('Shipment(YYMMDD)');
      Add(RightStr(Trim(qryListLST_DATE.AsString),6));
    end;
    // 44D Shipment Period
    TempStr := SumText([qryListSHIP_PD1.AsString, qryListSHIP_PD2.AsString, qryListSHIP_PD3.AsString, qryListSHIP_PD4.AsString, qryListSHIP_PD5.AsString, qryListSHIP_PD6.AsString]);
    IF TempStr <> '' Then
    begin
      Add('!44D');
      Add('|Shipment Period');
      Add(SumText([qryListSHIP_PD1.AsString, qryListSHIP_PD2.AsString]));
      Add(SumText([qryListSHIP_PD3.AsString, qryListSHIP_PD4.AsString]));
      Add(SumText([qryListSHIP_PD5.AsString, qryListSHIP_PD6.AsString]));
    end;
    // 45B Description of Goods and/or Services
    IF Trim(qryListGOODS_DESC.AsString) <> '' Then
    begin
      Add('!45B');
      Add('|Description of Goods and/or');
      Add('|Services');
      Add(qryListGOODS_DESC.AsString);
    end;
    // 46B Documents Required
    IF Trim(qryListDOC_REQ.AsString) <> '' Then
    begin
      Add('!46B');
      Add('|Documents Required');
      Add(qryListDOC_REQ.AsString);
    end;
    // 47B Additional Confitions
    IF Trim(qryListADD_CONDITION.AsString) <> '' then
    begin
      Add('!47B');
      Add('|Additional Conditions');
      Add(qryListADD_CONDITION.AsString);
    end;

    // 49M Special Payment Conditions for beneficiary
    IF Trim(qryListSPECIAL_PAY.AsString) <> '' then
    begin
      Add('!49M');
      Add('|Special Payment Conditions for');
      Add('|Beneficiary');
      Add(qryListSPECIAL_PAY.AsString);
    end;

    // 71D Charges
    IF Trim(qryListCHARGE.AsString) <> '' Then
    begin
      Add('!71D');
      Add('|Charges');
      Case AnsiIndexText( Trim(qryListCHARGE.AsString),['2AF', '2AG', '2AH'] ) of
        0:begin
            Add('ALL BANKING COMMISSIONS AND CHARGES');
            Add('INCLUDING REIMBURSEMENT CHARGES');
            Add('OUTSIDE SOUTH KOREA ARE FOR ACCOUNT');
            Add('OF APPLICANT');
          end;
        1:begin
            Add('ALL BANKING COMMISSIONS AND CHARGES');
            Add('INCLUDING REIMBURSEMENT CHARGES');
            Add('OUTSIDE SOUTH KOREA ARE FOR ACCOUNT');
            Add('OF BENEFICIARY');
          end;
        2: Add(qryListCHARGE_1.AsString);
      end;
    end;

    // 71N Amedment Charge Payable By
    IF Trim(qryListAMD_CHARGE.AsString) <> '' Then
    begin
      Add('!71N');
      Add('|Amendment Charge payable By');
      Case AnsiIndexText(qryListAMD_CHARGE.AsString, ['2AC','2AD','2AE']) of
        0: Add('APPL');
        1: Add('BENE');
        2: Add(qryListAMD_CHARGE_1.AsString);
      end;
    end;

    // 48 Period for Presesntation in Days
    IF qryListPERIOD_DAYS.AsInteger > 0 then
    begin
      Add('!48');
      Add('|Period for Presentation in Days');
      IF qryListPERIOD_IDX.AsString = '1' Then
        Add(qryListPERIOD_DAYS.AsString+qryListPERIOD_DETAIL.AsString)
      else
        Add(qryListPERIOD_DAYS.AsString);
    end;

    // 49 Confirmation Instructions
    IF Trim(qryListCONFIRM.AsString) <> '' Then
    begin
      Add('!49');
      Add('|Confirmation Instructions');
      Case AnsiIndexText(qryListCONFIRM.AsString, ['DA', 'DB', 'DC']) of
        0: Add('WITHOUT');
        1: Add('MAY ADD');
        2: Add('CONFIRM');
      end;
    end;

    // 58a Requested Confirmation Party
    IF Trim(qryListCONFIRM.AsString) = 'DC' Then
    begin
      Add('!58a');
      Add('|Requested Confirmation Party');
      IF Trim(qryListCONFIRM_BIC.AsString) <> '' then
        Add(qryListCONFIRM_BIC.AsString);

      IF SumText([qryListCONFIRM1.AsString, qryListCONFIRM2.AsString, qryListCONFIRM3.AsString, qryListCONFIRM4.AsString ]) <> '' Then
      begin
        Add(SumText([qryListCONFIRM1.AsString, qryListCONFIRM2.AsString]));
        Add(SumText([qryListCONFIRM3.AsString, qryListCONFIRM4.AsString]));
      end;
    end;

    Clipboard.AsText := FDetailData.Text;
  end;
end;
procedure TQR_APP707_PRN_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  FIDX := 0;
//  DataInit;



end;

procedure TQR_APP707_PRN_frm.QR_CHILD_TOPBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_CHILD_TOP.Height := 348;

  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FDOCINFO.MAINT_NO;
  qryList.Parameters.ParamByName('MSEQ'    ).Value := FDOCINFO.MSEQ;
  qryList.Parameters.ParamByName('AMD_NO'  ).Value := FDOCINFO.AMD_NO;
  qryList.Open;

//------------------------------------------------------------------------------
  QR_MAINTNO.Caption := qryListMAINT_NO.AsString;
  //조건변경신청일자
  QR_APPDATE.Caption := LeftStr(qryListAPP_DATE.AsString,4)+'-'+MidStr(qryListAPP_DATE.AsString, 5, 2)+RightStr(qryListAPP_DATE.AsString, 2);
  //개설방법
  QR_IN_MATHOD.Caption := qryListmathod_Name.AsString;
//------------------------------------------------------------------------------
// 개설의뢰인
//------------------------------------------------------------------------------
  //상호
  QR_APPLIC1.Caption := qryListAPPLIC1.AsString;
  //주소
  QR_APPLIC2.Caption := Trim(qryListAPPLIC2.AsString+' '+qryListAPPLIC3.AsString+' '+qryListAPPLIC4.AsString);
  //전화번호
  QR_APPLIC_TEL.Caption := qryListAPPLIC5.AsString;
//------------------------------------------------------------------------------
// 개설의뢰은행
//------------------------------------------------------------------------------
  QR_AP_BANK.Caption  := qryListAP_BANK.AsString;
  QR_AP_BANK1.Caption := Trim(Trim(Trim(qryListAP_BANK1.AsString) +' '+ Trim(qryListAP_BANK2.AsString))+' '+Trim(Trim(qryListAP_BANK3.AsString)+' '+Trim(qryListAP_BANK4.AsString)));
  QR_AP_BANK_TEL.Caption := Trim(qryListAP_BANK5.AsString);

//------------------------------------------------------------------------------
// 통지은행
//------------------------------------------------------------------------------
  QR_AD_BANK.Caption := qryListAD_BANK.AsString;
  QR_AD_BANK1.Caption := Trim(Trim(Trim(qryListAD_BANK1.AsString)+' '+Trim(qryListAD_BANK2.AsString)));
  QR_AD_BANK2.Caption := Trim(Trim(Trim(qryListAD_BANK3.AsString)+' '+Trim(qryListAD_BANK4.AsString)));
//------------------------------------------------------------------------------
// 기타정보
//------------------------------------------------------------------------------
  M_ADINFO.Lines.Clear;
  IF Trim(qryListAD_INFO1.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO1.AsString));
  IF Trim(qryListAD_INFO2.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO2.AsString));
  IF Trim(qryListAD_INFO3.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO3.AsString));
  IF Trim(qryListAD_INFO4.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO4.AsString));
  IF Trim(qryListAD_INFO5.AsString) <> '' Then
    M_ADINFO.Lines.Add(Trim(qryListAD_INFO5.AsString));

  M_ADINFO.Height := 12 * M_ADINFO.Lines.Count;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_COMMONBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  Sender.Height := 53;
  QR_20.Caption := qryListCD_NO.AsString;
  QR_31C.Caption := RightStr(qryListISS_DATE.AsString, 6);
  QR_26E.Caption := qryListAMD_NO.AsString;
end;

procedure TQR_APP707_PRN_frm.QR_CANCELBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled := qryListIS_CANCEL.AsString <> '';
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_40ABeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled    := qryListDOC_CD.AsString <>'';
  QRLabel28.Caption := qryListDOC_CDNM.AsString;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_31DBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 34;
  Sender.Enabled    := (qryListEX_DATE.AsString <> '') AND (qryListEX_PLACE.AsString <> '');
  QRLabel33.Caption := RightStr(qryListEX_DATE.AsString,6)+qryListEX_PLACE.AsString;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_50BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 34;
  Sender.Enabled    := qryListAPPLIC_CHG.AsBoolean;
  QRLabel41.Caption := SumText([qryListAPPLIC_CHG1.AsString, qryListAPPLIC_CHG2.AsString]);
  QRLabel42.Caption := SumText([qryListAPPLIC_CHG3.AsString, qryListAPPLIC_CHG4.AsString]);
  IF QRLabel42.Caption = '' Then
  begin
    QRLabel42.Enabled := False;
    Sender.Height := 22;
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_59BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height   := 49;
  Sender.Enabled  := qryListBENEFC_CHG.AsBoolean;
  QRLabel47.Caption := SumText([qryListBENEFC1.AsString, qryListBENEFC2.AsString]);
  QRLabel48.Caption := SumText([qryListBENEFC3.AsString, qryListBENEFC4.AsString]);
  QRLabel49.Caption := Trim(qryListBENEFC5.AsString);
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_32BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 34;
  Sender.Enabled := (Trim(qryListINCD_CUR.AsString) <> '') OR (Trim(qryListDECD_CUR.AsString) <> '');
    IF Trim(qryListINCD_CUR.AsString) <> '' then
    begin
      QRLabel50.Caption := '32B';
      QRLabel41.Caption := 'Increase of Documentary Credit';
      QRLabel53.Caption := UpperCase(qryListINCD_CUR.AsString)+FormatFloat('#,0.##',qryListINCD_AMT.AsCurrency);
    end
    else
    IF Trim(qryListDECD_CUR.AsString) <> '' then
    begin
      QRLabel50.Caption := '33B';
      QRLabel41.Caption := 'Decrease of Documentary Credit';
      QRLabel53.Caption := UpperCase(qryListDECD_CUR.AsString)+FormatFloat('#,0.##',qryListDECD_AMT.AsCurrency);
    end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_39ABeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 34;
  Sender.Enabled := (qryListCD_PERP.AsInteger > 0) OR (qryListCD_PERM.AsInteger > 0);
  QRLabel58.Caption := qryListCD_PERP.AsString +'/'+ qryListCD_PERM.AsString;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_39CBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 34;
  Sender.Enabled := Trim(SumText([qryListAA_CV1.AsString, qryListAA_CV2.AsString, qryListAA_CV3.AsString, qryListAA_CV4.AsString])) <> '';
  QRLabel63.Caption := SumText([qryListAA_CV1.AsString, qryListAA_CV2.AsString]);
  QRLabel64.Caption := SumText([qryListAA_CV3.AsString, qryListAA_CV4.AsString]);
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_42CBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height     := 22;
  Sender.Enabled    := Trim(qryListDRAFT1.AsString) <> '';
  QRLabel95.Caption := Trim( Trim(qryListDRAFT1.AsString)+' '+Trim(qryListDRAFT2.AsString)+' '+Trim(qryListDRAFT3.AsString) );
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_42MBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListMIX1.AsString) <> '';
  QRLabel103.Caption := Trim(Trim(qryListMIX1.AsString)+' '+Trim(qryListMIX2.AsString));
  QRLabel104.Caption := Trim(Trim(qryListMIX3.AsString)+' '+Trim(qryListMIX4.AsString));
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_42PBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListDEFPAY1.AsString) <> '';
  QRLabel108.Caption := Trim( Trim(qryListDEFPAY1.AsString)+' '+Trim(qryListDEFPAY2.AsString) );
  QRLabel109.Caption := Trim( Trim(qryListDEFPAY3.AsString)+' '+Trim(qryListDEFPAY4.AsString) );
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_43PBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled := Trim(qryListPSHIP.AsString) <> '';
  Case AnsiIndexText( qryListPSHIP.AsString , ['9', '10'] ) of
    0: QRLabel115.Caption := 'ALLOWED';
    1: QRLabel115.Caption := 'NOT ALLOWED';
  else
    QRLabel115.Caption := '';
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_43TBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled := Trim(qryListTSHIP.AsString) <> '';  
  Case AnsiIndexText( qryListTSHIP.AsString , ['7', '8'] ) of
    0: QRLabel118.Caption := 'ALLOWED';
    1: QRLabel118.Caption := 'NOT ALLOWED';
  else
    QRLabel118.Caption := '';
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_44ABeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListLOAD_ON.AsString) <> '';
  QRLabel122.Caption := Trim(qryListLOAD_ON.AsString);
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_44EBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled    := Trim(qryListSUNJUCK_PORT.AsString) <> '';
  QRLabel126.Caption := Trim(qryListSUNJUCK_PORT.AsString);
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_44FBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 40;
  Sender.Enabled     := Trim(qryListDOCHACK_PORT.AsString) <> '';
  QRLabel131.Caption := Trim(qryListDOCHACK_PORT.AsString);
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_44BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 56;
  Sender.Enabled     := Trim(qryListFOR_TRAN.AsString) <> '';
  QRLabel136.Caption := Trim(qryListFOR_TRAN.AsString);
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_44CBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height := 22;
  Sender.Enabled     := Trim(qryListLST_DATE.AsString) <> '';
  QRLabel142.Caption := RightStr(Trim(qryListLST_DATE.AsString),6);
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_44DBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TMP_STR : String;
begin
  Sender.Height := 22;
  TMP_STR := Trim(qryListSHIP_PD1.AsString)+' '+
             Trim(qryListSHIP_PD2.AsString)+' '+
             Trim(qryListSHIP_PD3.AsString)+' '+
             Trim(qryListSHIP_PD4.AsString)+' '+
             Trim(qryListSHIP_PD5.AsString)+' '+
             Trim(qryListSHIP_PD6.AsString);
  TMP_STR := Trim(TMP_STR);
  Sender.Enabled     := TMP_STR <> '';
  QRLabel145.Caption := TMP_STR;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_45BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListGOODS_DESC.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_45B.Lines.Clear;
    M_45B.Lines.Text := AnsiReplaceText( qryListGOODS_DESC.AsString , #13#10#13#10, '');
    M_45B.Height := (M_45B.Lines.Count * 12);

    IF M_45B.Lines.Count <= 2 Then
      Sender.Height := 40
    else
      Sender.Height := M_45B.Top + M_45B.Height + 5;
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_46BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListDOC_REQ.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_46B.Lines.Clear;
    M_46B.Lines.Text := AnsiReplaceText( qryListDOC_REQ.AsString , #13#10#13#10, '');

    M_46B.Height := (M_46B.Lines.Count * 12);    
    IF M_46B.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_46B.Top + M_46B.Height + 5;
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_47BBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListADD_CONDITION.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_47B.Lines.Clear;
    M_47B.Lines.Text := AnsiReplaceText( qryListADD_CONDITION.AsString , #13#10#13#10, '');

    M_47B.Height := (M_47B.Lines.Count * 12);    
    IF M_47B.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_47B.Top + M_47B.Height + 5;
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_49MBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListSPECIAL_PAY.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_49M.Lines.Clear;
    M_49M.Lines.Text := AnsiReplaceText( qryListSPECIAL_PAY.AsString , #13#10#13#10, '');
    M_49M.Height := (M_49M.Lines.Count * 12);

    IF M_49M.Lines.Count <= 2 Then
      Sender.Height := 40
    else
      Sender.Height := M_49M.Top + M_49M.Height + 5;
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_71DBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListCHARGE.AsString <> '');
  IF Sender.Enabled Then
  begin
    M_71D.Lines.Add('ALL BANKING COMMISSIONS AND CHARGES');
    M_71D.Lines.Add('INCLUDING REIMBURSEMENT CHARGES');
    M_71D.Lines.Add('OUTSIDE SOUTH KOREA ARE FOR ACCOUNT');
    case AnsiIndexText(UpperCase( Trim(qryListCHARGE.AsString) ), ['2AF', '2AG', '2AH']) of
      0: M_71D.Lines.Add('OF APPLICANT');
      1: M_71D.Lines.Add('OF BENEFICIARY');
      2:
      begin
        M_71D.Lines.Clear;
        M_71D.Lines.Text := AnsiReplaceText( qryListCHARGE_1.AsString , #13#10#13#10, '');
      end;
    end;

    M_71D.Height := (M_71D.Lines.Count * 12);
    IF M_71D.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_71D.Top + M_71D.Height + 5;
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_71NBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Enabled := (qryListAMD_CHARGE.AsString <> '');
  IF Sender.Enabled Then
  begin
    case AnsiIndexText(UpperCase( Trim(qryListAMD_CHARGE.AsString) ), ['2AC', '2AD', '2AE']) of
      0: M_71N.Lines.Text := 'APPL';
      1: M_71N.Lines.Text := 'BENE';
      2:
      begin
        M_71N.Lines.Clear;
        M_71N.Lines.Text := AnsiReplaceText( qryListAMD_CHARGE_1.AsString , #13#10#13#10, '');
      end;
    end;

    M_71N.Height := (M_71N.Lines.Count * 12);
    IF M_71N.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_71N.Top + M_71N.Height + 5;
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_48BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height  := 22;
  Sender.Enabled := Trim(qryListPERIOD_DAYS.AsString) > '0';
  QR_48.Caption  := Trim((qryListPERIOD_DAYS.AsString)+' '+Trim(qryListPERIOD_DETAIL.AsString));
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_49BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  Sender.Height  := 22;
  Sender.Enabled := Trim(qryListCONFIRM.AsString) <> '';
  QR_49.Caption := '';
  Case AnsiIndexText(qryListCONFIRM.AsString, ['DA','DB','DC']) of
    0: QR_49.Caption := 'WITHOUT';
    1: QR_49.Caption := 'MAY ADD';
    2: QR_49.Caption := 'CONFIRM';
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_58aBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  //확인은행
  Sender.Enabled := AnsiMatchText(qryListCONFIRM.AsString, ['DB', 'DC']);
  IF Sender.Enabled Then
  begin
    M_58a.Lines.Clear;
    IF Trim(qryListCONFIRM_BIC.AsString) <> '' Then
      M_58a.Lines.Add(qryListCONFIRM_BIC.AsString);
    IF Trim(qryListCONFIRM1.AsString) <> '' Then
      M_58a.Lines.Add(Trim((qryListCONFIRM1.AsString)+' '+Trim(qryListCONFIRM2.AsString)));
    IF Trim(qryListCONFIRM3.AsString) <> '' Then
      M_58a.Lines.Add(Trim((qryListCONFIRM3.AsString)+' '+Trim(qryListCONFIRM4.AsString)));

    M_58a.Height := (M_58a.Lines.Count * 12);
    IF M_58a.Lines.Count = 1 Then
      Sender.Height := 22
    else
      Sender.Height := M_58a.Top + M_58a.Height + 5;
  end;
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_SIGNBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  Sender.Height := 151;
  QRLabel200.Caption := qryListEX_NAME1.AsString;
  QRLabel201.Caption := qryListEX_NAME2.AsString;
  QRLabel202.Caption := qryListEX_NAME3.AsString;
  QRLabel203.Caption := Trim((qryListEX_ADDR1.AsString)+' '+Trim(qryListEX_ADDR2.AsString));
end;

procedure TQR_APP707_PRN_frm.QR_CHILD_RFFBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRMemo1.Lines.Clear;
  if Trim(qryListIMP_CD1.AsString) <> '' Then
    QRMemo1.Lines.Add(Trim(qryListIMP_CD1.AsString)+'/'+qryListIL_NO1.AsString+'/'+qryListIL_CUR1.AsString+' '+FormatFloat('#,0.###',qryListIL_AMT1.AsCurrency));
  if Trim(qryListIMP_CD2.AsString) <> '' Then
    QRMemo1.Lines.Add(Trim(qryListIMP_CD2.AsString)+'/'+qryListIL_NO2.AsString+'/'+qryListIL_CUR2.AsString+' '+FormatFloat('#,0.###',qryListIL_AMT2.AsCurrency));
  if Trim(qryListIMP_CD3.AsString) <> '' Then
    QRMemo1.Lines.Add(Trim(qryListIMP_CD3.AsString)+'/'+qryListIL_NO3.AsString+'/'+qryListIL_CUR3.AsString+' '+FormatFloat('#,0.###',qryListIL_AMT3.AsCurrency));
  if Trim(qryListIMP_CD4.AsString) <> '' Then
    QRMemo1.Lines.Add(Trim(qryListIMP_CD4.AsString)+'/'+qryListIL_NO4.AsString+'/'+qryListIL_CUR4.AsString+' '+FormatFloat('#,0.###',qryListIL_AMT4.AsCurrency));
  if Trim(qryListIMP_CD5.AsString) <> '' Then
    QRMemo1.Lines.Add(Trim(qryListIMP_CD5.AsString)+'/'+qryListIL_NO5.AsString+'/'+qryListIL_CUR5.AsString+' '+FormatFloat('#,0.###',qryListIL_AMT5.AsCurrency));

  QRMemo1.Height := (QRMemo1.Lines.Count * 12);

  Sender.Enabled := QRMemo1.Lines.Count > 0; 

  IF QRMemo1.Lines.Count = 1 Then
    Sender.Height := 22
  else
    Sender.Height := QRMemo1.Top + QRMemo1.Height + 5;
end;

end.

