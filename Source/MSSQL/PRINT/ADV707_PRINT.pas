unit ADV707_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TADV707_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRShape1: TQRShape;
    QRLabel88: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QR_MSG2: TQRLabel;
    QR_MSG1: TQRLabel;
    QR_MAINTNO: TQRLabel;
    qryADV707: TADOQuery;
    qryADV7072: TADOQuery;
    qryADV707MAINT_NO: TStringField;
    qryADV707AMD_NO: TIntegerField;
    qryADV707AMD_NO1: TIntegerField;
    qryADV707CHK1: TBooleanField;
    qryADV707CHK2: TStringField;
    qryADV707CHK3: TStringField;
    qryADV707MESSAGE1: TStringField;
    qryADV707MESSAGE2: TStringField;
    qryADV707USER_ID: TStringField;
    qryADV707DATEE: TStringField;
    qryADV707AMD_DATE: TStringField;
    qryADV707APP_DATE: TStringField;
    qryADV707APP_NO: TStringField;
    qryADV707AP_BANK: TStringField;
    qryADV707AP_BANK1: TStringField;
    qryADV707AP_BANK2: TStringField;
    qryADV707AP_BANK3: TStringField;
    qryADV707AP_BANK4: TStringField;
    qryADV707AP_BANK5: TStringField;
    qryADV707AD_BANK: TStringField;
    qryADV707AD_BANK1: TStringField;
    qryADV707AD_BANK2: TStringField;
    qryADV707AD_BANK3: TStringField;
    qryADV707AD_BANK4: TStringField;
    qryADV707AD_BANK5: TStringField;
    qryADV707ADDINFO: TStringField;
    qryADV707ADDINFO_1: TMemoField;
    qryADV707CD_NO: TStringField;
    qryADV707RCV_REF: TStringField;
    qryADV707ISS_BANK: TStringField;
    qryADV707ISS_BANK1: TStringField;
    qryADV707ISS_BANK2: TStringField;
    qryADV707ISS_BANK3: TStringField;
    qryADV707ISS_BANK4: TStringField;
    qryADV707ISS_BANK5: TStringField;
    qryADV707ISS_ACCNT: TStringField;
    qryADV707ISS_DATE: TStringField;
    qryADV707EX_DATE: TStringField;
    qryADV707EX_PLACE: TStringField;
    qryADV707BENEFC1: TStringField;
    qryADV707BENEFC2: TStringField;
    qryADV707BENEFC3: TStringField;
    qryADV707BENEFC4: TStringField;
    qryADV707BENEFC5: TStringField;
    qryADV707INCD_CUR: TStringField;
    qryADV707INCD_AMT: TBCDField;
    qryADV707DECD_CUR: TStringField;
    qryADV707DECD_AMT: TBCDField;
    qryADV707NWCD_CUR: TStringField;
    qryADV707NWCD_AMT: TBCDField;
    qryADV707CD_PERP: TIntegerField;
    qryADV707CD_PERM: TIntegerField;
    qryADV707CD_MAX: TStringField;
    qryADV707AA_CV1: TStringField;
    qryADV707AA_CV2: TStringField;
    qryADV707AA_CV3: TStringField;
    qryADV707AA_CV4: TStringField;
    qryADV707BENEFC6: TStringField;
    qryADV707IBANK_REF: TStringField;
    qryADV707PRNO: TIntegerField;
    qryADV7072MAINT_NO: TStringField;
    qryADV7072AMD_NO: TIntegerField;
    qryADV7072LOAD_ON: TStringField;
    qryADV7072FOR_TRAN: TStringField;
    qryADV7072LST_DATE: TStringField;
    qryADV7072SHIP_PD: TStringField;
    qryADV7072SHIP_PD1: TStringField;
    qryADV7072SHIP_PD2: TStringField;
    qryADV7072SHIP_PD3: TStringField;
    qryADV7072SHIP_PD4: TStringField;
    qryADV7072SHIP_PD5: TStringField;
    qryADV7072SHIP_PD6: TStringField;
    qryADV7072NARRAT: TBooleanField;
    qryADV7072NARRAT_1: TMemoField;
    qryADV7072SR_INFO1: TStringField;
    qryADV7072SR_INFO2: TStringField;
    qryADV7072SR_INFO3: TStringField;
    qryADV7072SR_INFO4: TStringField;
    qryADV7072SR_INFO5: TStringField;
    qryADV7072SR_INFO6: TStringField;
    qryADV7072EX_NAME1: TStringField;
    qryADV7072EX_NAME2: TStringField;
    qryADV7072EX_NAME3: TStringField;
    qryADV7072EX_ADDR1: TStringField;
    qryADV7072EX_ADDR2: TStringField;
    qryADV7072BFCD_AMT: TBCDField;
    qryADV7072BFCD_CUR: TStringField;
    qryADV7072SUNJUCK_PORT: TStringField;
    qryADV7072DOCHACK_PORT: TStringField;
    ChildBand1: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel83: TQRLabel;
    QRLabel9: TQRLabel;
    QR_APPDATE: TQRLabel;
    QRLabel11: TQRLabel;
    QR_APPNO: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_APBANK: TQRLabel;
    QR_APBANK1: TQRLabel;
    QR_APBANK2: TQRLabel;
    QR_APBANK3: TQRLabel;
    QR_APBANK4: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel16: TQRLabel;
    QR_ADBANK: TQRLabel;
    QR_ADBANK1: TQRLabel;
    QR_ADBANK2: TQRLabel;
    QR_ADBANK3: TQRLabel;
    QR_ADBANK4: TQRLabel;
    QR_ADBANK5: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel22: TQRLabel;
    QR_ADDINFO1: TQRMemo;
    ChildBand5: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel23: TQRLabel;
    QRLabel6: TQRLabel;
    QR_CDNO: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QR_RCVREF: TQRLabel;
    ChildBand6: TQRChildBand;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QR_IBANKREF: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QR_ISSBANK: TQRLabel;
    QR_ISSBANK1: TQRLabel;
    QR_ISSBANK2: TQRLabel;
    QR_ISSBANK3: TQRLabel;
    QR_ISSBANK4: TQRLabel;
    ChildBand8: TQRChildBand;
    ChildBand9: TQRChildBand;
    ChildBand10: TQRChildBand;
    ChildBand11: TQRChildBand;
    ChildBand12: TQRChildBand;
    ChildBand13: TQRChildBand;
    ChildBand14: TQRChildBand;
    ChildBand15: TQRChildBand;
    ChildBand16: TQRChildBand;
    ChildBand17: TQRChildBand;
    ChildBand18: TQRChildBand;
    ChildBand19: TQRChildBand;
    ChildBand20: TQRChildBand;
    ChildBand21: TQRChildBand;
    ChildBand22: TQRChildBand;
    ChildBand23: TQRChildBand;
    ChildBand24: TQRChildBand;
    ChildBand25: TQRChildBand;
    ChildBand26: TQRChildBand;
    ChildBand27: TQRChildBand;
    SummaryBand1: TQRBand;
    QRLabel115: TQRLabel;
    ChildBand28: TQRChildBand;
    QRShape2: TQRShape;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QR_EXADDR2: TQRLabel;
    QR_EXADDR1: TQRLabel;
    QRLabel109: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QRLabel108: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel106: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QRLabel105: TQRLabel;
    QRImage3: TQRImage;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QR_SRINFO1: TQRLabel;
    QR_SRINFO2: TQRLabel;
    QR_SRINFO3: TQRLabel;
    QR_SRINFO4: TQRLabel;
    QR_SRINFO5: TQRLabel;
    QR_SRINFO6: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel95: TQRLabel;
    QR_NARRAT1: TQRMemo;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QR_SHIPPD1: TQRLabel;
    QR_SHIPPD2: TQRLabel;
    QR_SHIPPD3: TQRLabel;
    QR_SHIPPD4: TQRLabel;
    QR_SHIPPD5: TQRLabel;
    QR_SHIPPD6: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QR_LSTDATE: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QR_FORTRAN: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QR_DOCHACKPORT: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QR_SUNJUCKPORT: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QR_LOADON: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QR_AACV1: TQRLabel;
    QR_AACV2: TQRLabel;
    QR_AACV3: TQRLabel;
    QR_AACV4: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QR_CDMAX: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QR_PERPM: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QR_NWCDAMT: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QR_DECDAMT: TQRLabel;
    QR_INCDAMT: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QR_EXDATE: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel47: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    QR_BENEFC4: TQRLabel;
    QR_BENEFC5: TQRLabel;
    QR_BENEFC6: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QR_AMDNO1: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel34: TQRLabel;
    QR_AMDDATE: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QR_ISSDATE: TQRLabel;
    QR_ISSACCNT: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand14BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand15BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand17BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand18BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand19BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand20BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand21BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand22BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand23BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand24BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand25BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand26BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand27BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand28BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNO: string;
    FAmdNo : String;
    procedure OPENDB;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
    property AmdNo:string read FAmdNo write FAmdNo;
  end;

var
  ADV707_PRINT_frm: TADV707_PRINT_frm;

implementation

uses MSSQL, Commonlib, strUtils;

{$R *.dfm}

{ TADV707_PRINT_frm }


procedure TADV707_PRINT_frm.OPENDB;
begin
  qryADV707.Close;
  qryADV707.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryADV707.Parameters.ParamByName('AMD_NO').Value := FAmdNo;
  qryADV707.Open;

  qryADV7072.Close;
  qryADV7072.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryADV7072.Parameters.ParamByName('AMD_NO').Value := FAmdNo;
  qryADV7072.Open;
end;

procedure TADV707_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TADV707_PRINT_frm.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  TitleBand1.Height := TitleBand1.Tag;
  //문서/전자문서번호
  QR_MAINTNO.Caption := qryADV707MAINT_NO.AsString;
  //전자문서 기능표시
  case StrToInt(Trim(qryADV707MESSAGE1.AsString)) of
    1 : QR_MSG1.Caption := 'Cancel';
    3 : QR_MSG1.Caption := 'Delete';
    4 : QR_MSG1.Caption := 'Change';
    6 : QR_MSG1.Caption := 'Confirmation';
    7 : QR_MSG1.Caption := 'Duplicate';
    9 : QR_MSG1.Caption := 'Original';
  end;

  case AnsiIndexText( Trim(qryADV707MESSAGE2.AsString) , ['AB', 'AP', 'NA', 'RE']) of
    0 : QR_MSG2.Caption := 'Message Acknowledgement';
    1 : QR_MSG2.Caption := 'Accepted';
    2 : QR_MSG2.Caption := 'No acknowledgement needed';
    3 : QR_MSG2.Caption := 'Rejected';
  end;
end;

procedure TADV707_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  ChildBand1.Height := ChildBand1.Tag;
  //조건변경통지일자
  QR_APPDATE.Caption := FormatDateTime('YYYY.MM.DD' ,  ConvertStr2Date(qryADV707APP_DATE.AsString) );
  //통지번호
  QR_APPNO.Caption := Trim(qryADV707APP_NO.AsString);
end;

procedure TADV707_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //개설(전문발신)은행
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV707.FieldByName('AP_BANK'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV707.FieldByName('AP_BANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if Trim(qryADV707AP_BANK.AsString) <> '' then //코드값이 있을경우
  begin
    QR_APBANK.Enabled := True;
    QR_APBANK.Caption := qryADV707AP_BANK.AsString;
    ChildBand2.Height := (15 * nIndex);
  end
  else//코드값이 없을경우
  begin
    QR_APBANK4.Top := QR_APBANK3.Top;
    QR_APBANK3.Top := QR_APBANK2.Top;
    QR_APBANK2.Top := QR_APBANK1.Top;
    QR_APBANK1.Top := QR_APBANK.Top;
    ChildBand2.Height := (15 * (nIndex-1));
  end;

  //값이 하나도 없을경우 ChaildBand 전체 숨김
  if (Trim(qryADV707AP_BANK.AsString) = '') and (Trim(qryADV707AP_BANK1.AsString) = '') and
     (Trim(qryADV707AP_BANK2.AsString) = '') and (Trim(qryADV707AP_BANK4.AsString) = '') then
  begin
    ChildBand2.Enabled := False;
  end;  
end;

procedure TADV707_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //통지(전문수신)은행
  nIndex := 1;
  for i := 1 to 5 do
  begin
    if Trim(qryADV707.FieldByName('AD_BANK'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_ADBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_ADBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV707.FieldByName('AD_BANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if Trim(qryADV707AD_BANK.AsString) <> '' then // 코드값이 있을경우
  begin
    QR_ADBANK.Enabled := True;
    QR_ADBANK.Caption := qryADV707AD_BANK.AsString;
    ChildBand3.Height := (15 * nIndex);
  end
  else // 코드값이 없을경우
  begin
    QR_ADBANK5.Top := QR_ADBANK4.Top;
    QR_ADBANK4.Top := QR_ADBANK3.Top;
    QR_ADBANK3.Top := QR_ADBANK2.Top;
    QR_ADBANK2.Top := QR_ADBANK1.Top;
    QR_ADBANK1.Top := QR_ADBANK.Top;
    ChildBand3.Height := (15 * (nIndex-1));
  end;

  //값이 하나도 없을경우 ChaildBand 전체 숨김
  if (Trim(qryADV707AD_BANK.AsString) = '') and (Trim(qryADV707AD_BANK1.AsString) = '') and
     (Trim(qryADV707AD_BANK2.AsString) = '') and (Trim(qryADV707AD_BANK4.AsString) = '') then
  begin
    ChildBand3.Enabled := False;
  end;  
end;

procedure TADV707_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //기타정보
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := Trim(qryADV707ADDINFO_1.AsString);
    ChildBand4.Height := ( 15 * memoLines.Count) + 10;
    QR_ADDINFO1.Height := ( 15 * memoLines.Count);

    QR_ADDINFO1.Lines.Clear;
    QR_ADDINFO1.Lines.Text := memoLines.Text;
  finally
    memoLines.Free;
  end;
  if Trim(QR_ADDINFO1.Lines.Text) = '' then ChildBand4.Enabled := False;
end;

procedure TADV707_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  ChildBand5.Height := ChildBand5.Tag;
  //Sender(s) Reference
  QR_CDNO.Caption := Trim(qryADV707CD_NO.AsString);
  //Receiver(s) Reference
  QR_RCVREF.Caption := Trim(qryADV707RCV_REF.AsString);
end;

procedure TADV707_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Issuing Bank's Reference
  ChildBand6.Height := ChildBand6.Tag;
  QR_IBANKREF.Caption := Trim(qryADV707IBANK_REF.AsString);
  if QR_IBANKREF.Caption = '' then ChildBand6.Enabled := False;     
end;

procedure TADV707_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Issuing Bank
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV707.FieldByName('ISS_BANK'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_ISSBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_ISSBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV707.FieldByName('ISS_BANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if Trim(qryADV707ISS_BANK.AsString) <> '' then //코드값이 있을경우
  begin
    QR_ISSBANK.Enabled := True;
    QR_ISSBANK.Caption := qryADV707ISS_BANK.AsString;
    ChildBand7.Height := (15 * nIndex);
  end
  else//코드값이 없을경우
  begin
    QR_ISSBANK4.Top := QR_ISSBANK3.Top;
    QR_ISSBANK3.Top := QR_ISSBANK2.Top;
    QR_ISSBANK2.Top := QR_ISSBANK1.Top;
    QR_ISSBANK1.Top := QR_ISSBANK.Top;
    ChildBand17.Height := (15 * (nIndex-1));
  end;

  //값이 하나도 없을경우 ChaildBand 전체 숨김
  if (Trim(qryADV707ISS_BANK1.AsString) = '') and (Trim(qryADV707ISS_BANK2.AsString) = '') and
     (Trim(qryADV707ISS_BANK3.AsString) = '') and (Trim(qryADV707ISS_BANK4.AsString) = '') then
  begin
    ChildBand7.Enabled := False;
  end;

end;

procedure TADV707_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  ChildBand8.Height := ChildBand8.Tag;
  QR_ISSACCNT.Caption := Trim(qryADV707ISS_ACCNT.AsString);
  if QR_ISSACCNT.Caption = '' then
    ChildBand8.Enabled := False
  else
    QR_ISSACCNT.Enabled := True;
end;

procedure TADV707_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Date of Issue
  QR_ISSDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryADV707ISS_DATE.AsString));
  if Trim(QR_ISSDATE.Caption) = '' then
    ChildBand9.Enabled := False
  else
    ChildBand9.Height := ChildBand9.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Date of Amendment
  QR_AMDDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryADV707AMD_DATE.AsString));
  if Trim(QR_AMDDATE.Caption) = '' then
    ChildBand10.Enabled := False
  else
    ChildBand10.Height := ChildBand10.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Number of Amendment
  QR_AMDNO1.Caption := Trim(qryADV707AMD_NO1.AsString);
  if QR_AMDNO1.Caption = '' then
    ChildBand11.Enabled := False
  else
    ChildBand11.Height := ChildBand11.Tag;

end;

procedure TADV707_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex : Integer;
begin
  inherited;
  //Beneficiary
  nIndex := 1;
  for i := 1 to 6 do
  begin
    if Trim(qryADV707.FieldByName('BENEFC'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV707.FieldByName('BENEFC'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (trim(qryADV707BENEFC1.AsString) = '') and (trim(qryADV707BENEFC2.AsString) = '') and (trim(qryADV707BENEFC3.AsString) = '') and
     (trim(qryADV707BENEFC4.AsString) = '') and (trim(qryADV707BENEFC5.AsString) = '') and (trim(qryADV707BENEFC6.AsString) = '') then
    ChildBand12.Enabled := False
  else
    ChildBand12.Height := 15 * (nIndex-1);
end;

procedure TADV707_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //New Date of Expiry
  QR_EXDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryADV707EX_DATE.AsString));
  if Trim(QR_EXDATE.Caption) = '' then
    ChildBand13.Enabled := False
  else
    ChildBand13.Height := ChildBand13.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand14BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Increase of Documentary Credit Amount (신용장 증액분)
  QR_INCDAMT.Caption := qryADV707INCD_CUR.AsString + ' ' + FormatCurr('#,###.####' , qryADV707INCD_AMT.AsCurrency);
  if qryADV707INCD_AMT.AsCurrency = 0 then
    ChildBand14.Enabled := False
  else
    ChildBand14.Height := ChildBand14.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand15BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Decrease of Documentary Credit Amount(신용장 감액분)
  QR_DECDAMT.Caption := qryADV707DECD_CUR.AsString + ' ' + FormatCurr('#,###.####' , qryADV707DECD_AMT.AsCurrency);
  if qryADV707DECD_AMT.AsCurrency = 0 then
    ChildBand15.Enabled := False
  else
    ChildBand15.Height := ChildBand15.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand16BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //New Documentary Credit Amount After Amendment (변경후 최종금액)
  QR_NWCDAMT.Caption := qryADV707NWCD_CUR.AsString + ' ' + FormatCurr('#,###.####' , qryADV707NWCD_AMT.AsCurrency);
  if qryADV707NWCD_AMT.AsCurrency = 0 then
    ChildBand16.Enabled := False
  else
    ChildBand16.Height := ChildBand16.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand17BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Percentage Credit Amount Tolerance (과부족허용율)
  QR_PERPM.Caption := '(±) ' +  CurrToStr(qryADV707CD_PERP.AsCurrency) + '/' + CurrToStr(qryADV707CD_PERM.AsCurrency)+' (%)';
  if (qryADV707CD_PERP.AsCurrency = 0) and (qryADV707CD_PERM.AsCurrency = 0) then
    ChildBand17.Enabled := False
  else
    ChildBand17.Height := ChildBand17.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand18BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Maximum Credit Amount(최대신용장금액)
  QR_CDMAX.Caption := Trim(qryADV707CD_MAX.AsString);
  if (Trim(QR_CDMAX.Caption)= '') or (Trim(QR_CDMAX.Caption)= '0') then
    ChildBand18.Enabled := False
  else
    ChildBand18.Height := ChildBand18.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand19BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i,nIndex : Integer;
begin
  inherited;
  //Additional Amounts Covered
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV707.FieldByName('AA_CV'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV707.FieldByName('AA_CV'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (trim(qryADV707AA_CV1.AsString) = '') and (trim(qryADV707AA_CV2.AsString) = '') and (trim(qryADV707AA_CV3.AsString) = '') and
     (trim(qryADV707AA_CV4.AsString) = '') then
    ChildBand19.Enabled := False
  else
    ChildBand19.Height := 15 * (nIndex-1);
end;

procedure TADV707_PRINT_frm.ChildBand20BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Place of Taking in Charge/Dispatch from ···/Place of Receipt :  (수탁발송지)
  QR_LOADON.Caption := Trim(qryADV7072LOAD_ON.AsString);
  if QR_LOADON.Caption = '' then
    ChildBand20.Enabled := False
  else
    ChildBand20.Height := ChildBand20.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand21BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Port of Loading/Airport of Departure : (선적항)
  QR_SUNJUCKPORT.Caption := Trim(qryADV7072SUNJUCK_PORT.AsString);
  if QR_SUNJUCKPORT.Caption = '' then
    ChildBand21.Enabled := False
  else
    ChildBand21.Height := ChildBand21.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand22BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Port of Discharge/Airport of Destination :  (도착항)
  QR_DOCHACKPORT.Caption := Trim(qryADV7072DOCHACK_PORT.AsString);
  if QR_DOCHACKPORT.Caption = '' then
    ChildBand22.Enabled := False
  else
    ChildBand22.Height := ChildBand22.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand23BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Place of Final Destination/For Transportation to ···/Place of Delivery (최종도착지)
  QR_FORTRAN.Caption := Trim(qryADV7072FOR_TRAN.AsString);
  if QR_FORTRAN.Caption = '' then
    ChildBand23.Enabled := False
  else
    ChildBand23.Height := ChildBand23.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand24BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //최종선적일자
  QR_LSTDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date( qryADV7072LST_DATE.AsString ));
  if Trim(QR_LSTDATE.Caption) = '' then
    ChildBand24.Enabled := False
  else
    ChildBand24.Height := ChildBand24.Tag;
end;

procedure TADV707_PRINT_frm.ChildBand25BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Shipment Period
  nIndex := 1;
  for i := 1 to 6 do
  begin
    if Trim(qryADV7072.FieldByName('SHIP_PD'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7072.FieldByName('SHIP_PD'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_SHIPPD1.Caption) = '') and (Trim(QR_SHIPPD2.Caption) = '') and (Trim(QR_SHIPPD3.Caption) = '') and
     (Trim(QR_SHIPPD4.Caption) = '') and (Trim(QR_SHIPPD5.Caption) = '') and (Trim(QR_SHIPPD6.Caption) = '') then
  begin
    ChildBand25.Enabled := False;
  end
  else
    ChildBand25.Height := 15 * nIndex;

end;

procedure TADV707_PRINT_frm.ChildBand26BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //NARRAT
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := Trim(qryADV7072NARRAT_1.AsString);
    ChildBand26.Height := ( 15 * memoLines.Count) + 10;
    QR_NARRAT1.Height := ( 15 * memoLines.Count);

    QR_NARRAT1.Lines.Clear;
    QR_NARRAT1.Lines.Text := memoLines.Text;
  finally
    memoLines.Free;
  end;
  if Trim(QR_NARRAT1.Lines.Text) = '' then ChildBand4.Enabled := False;

end;

procedure TADV707_PRINT_frm.ChildBand27BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Sender to Receive
  nIndex := 1;
  for i := 1 to 6 do
  begin
    if Trim(qryADV7072.FieldByName('SR_INFO'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_SRINFO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_SRINFO'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7072.FieldByName('SR_INFO'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_SRINFO1.Caption) = '') and (Trim(QR_SRINFO2.Caption) = '') and (Trim(QR_SRINFO3.Caption) = '') and
     (Trim(QR_SRINFO4.Caption) = '') and (Trim(QR_SRINFO5.Caption) = '') and (Trim(QR_SRINFO6.Caption) = '') then
  begin
    ChildBand27.Enabled := False;
  end
  else
    ChildBand27.Height := 15 * (nIndex-1);

end;

procedure TADV707_PRINT_frm.ChildBand28BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  ChildBand28.Height := ChildBand28.Tag;
  QR_EXNAME1.Caption := Trim(qryADV7072EX_NAME1.AsString);
  QR_EXNAME2.Caption := Trim(qryADV7072EX_NAME2.AsString);
  QR_EXNAME3.Caption := Trim(qryADV7072EX_NAME3.AsString);
  QR_EXADDR1.Caption := Trim(qryADV7072EX_ADDR1.AsString);
  QR_EXADDR2.Caption := Trim(qryADV7072EX_ADDR2.AsString);
end;

end.


