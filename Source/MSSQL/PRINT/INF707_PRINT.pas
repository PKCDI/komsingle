unit INF707_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB ;

type
  TINF707_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRShape1: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QR_RESPONSE: TQRLabel;
    QR_FUNCTION: TQRLabel;
    QR_MAINTNO: TQRLabel;
    ChildBand1: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QR_DATEE: TQRLabel;
    QRLabel12: TQRLabel;
    QR_INMETHOD_Name: TQRLabel;
    QRLabel14: TQRLabel;
    QR_APBANK: TQRLabel;
    QR_APBANK1: TQRLabel;
    QR_APBANK2: TQRLabel;
    QR_APBANK3: TQRLabel;
    QR_APBANK4: TQRLabel;
    QR_APBANK5: TQRLabel;
    QRLabel15: TQRLabel;
    QR_ADBANK: TQRLabel;
    QR_ADBANK1: TQRLabel;
    QR_ADBANK2: TQRLabel;
    QR_ADBANK3: TQRLabel;
    QR_ADBANK4: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel13: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC3: TQRLabel;
    QR_APPLIC4: TQRLabel;
    QR_APPLIC5: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QR_ILNO1: TQRLabel;
    QR_ILNO2: TQRLabel;
    QR_ILNO4: TQRLabel;
    QR_ILNO3: TQRLabel;
    QR_ILCUR1: TQRLabel;
    QR_ILCUR2: TQRLabel;
    QR_ILCUR3: TQRLabel;
    QR_ILCUR4: TQRLabel;
    QR_ILCUR5: TQRLabel;
    QR_ILAMT1: TQRLabel;
    QR_ILAMT2: TQRLabel;
    QR_ILAMT3: TQRLabel;
    QR_ILAMT4: TQRLabel;
    QR_ILAMT5: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel20: TQRLabel;
    QR_ADINFO1: TQRLabel;
    QR_ADINFO2: TQRLabel;
    QR_ADINFO3: TQRLabel;
    QR_ADINFO4: TQRLabel;
    QR_ADINFO5: TQRLabel;
    ChildBand5: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QR_CDNO: TQRLabel;
    ChildBand6: TQRChildBand;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QR_RCVREF: TQRLabel;
    QRLabel26: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QR_IBANKREF: TQRLabel;
    ChildBand8: TQRChildBand;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QR_ISSBANK1: TQRLabel;
    QR_ISSBANK2: TQRLabel;
    QR_ISSBANK3: TQRLabel;
    QR_ISSBANK4: TQRLabel;
    QR_ISSBANK5: TQRLabel;
    QRLabel36: TQRLabel;
    QR_ISSACCNT: TQRLabel;
    ChildBand9: TQRChildBand;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QR_ISSDATE: TQRLabel;
    ChildBand10: TQRChildBand;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QR_AMDDATE: TQRLabel;
    ChildBand11: TQRChildBand;
    QRLabel33: TQRLabel;
    QRLabel37: TQRLabel;
    QR_AMDNO: TQRLabel;
    ChildBand12: TQRChildBand;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    QR_BENEFC4: TQRLabel;
    QR_BENEFC5: TQRLabel;
    ChildBand13: TQRChildBand;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QR_DECDCUR: TQRLabel;
    QR_DECDAMT: TQRLabel;
    ChildBand14: TQRChildBand;
    ChildBand15: TQRChildBand;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QR_INCDCUR: TQRLabel;
    QR_INCDAMT: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QR_DECDCUR2: TQRLabel;
    QR_DECDAMT2: TQRLabel;
    ChildBand17: TQRChildBand;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QR_NWCDCUR: TQRLabel;
    QR_NWCDAMT: TQRLabel;
    ChildBand18: TQRChildBand;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QR_CDPERP: TQRLabel;
    ChildBand19: TQRChildBand;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QR_CDMAX: TQRLabel;
    ChildBand20: TQRChildBand;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QR_AACV1: TQRLabel;
    QR_AACV2: TQRLabel;
    QR_AACV3: TQRLabel;
    QR_AACV4: TQRLabel;
    ChildBand21: TQRChildBand;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QR_LOADON: TQRLabel;
    ChildBand22: TQRChildBand;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QR_SUNJUCKPORT: TQRLabel;
    ChildBand23: TQRChildBand;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QR_DOCHACKPORT: TQRLabel;
    ChildBand24: TQRChildBand;
    QRLabel58: TQRLabel;
    QRLabel61: TQRLabel;
    QR_FORTRAN: TQRLabel;
    ChildBand25: TQRChildBand;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QR_LSTDATE: TQRLabel;
    ChildBand26: TQRChildBand;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QR_SHIPPD1: TQRLabel;
    QR_SHIPPD2: TQRLabel;
    QR_SHIPPD3: TQRLabel;
    QR_SHIPPD6: TQRLabel;
    QR_SHIPPD5: TQRLabel;
    QR_SHIPPD4: TQRLabel;
    ChildBand27: TQRChildBand;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QR_NARRAT1: TQRMemo;
    ChildBand28: TQRChildBand;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QR_SRINFO1: TQRLabel;
    QR_SRINFO2: TQRLabel;
    QR_SRINFO3: TQRLabel;
    QR_SRINFO6: TQRLabel;
    QR_SRINFO5: TQRLabel;
    QR_SRINFO4: TQRLabel;
    ChildBand29: TQRChildBand;
    QRShape2: TQRShape;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    SummaryBand1: TQRBand;
    QRImage3: TQRImage;
    QRLabel124: TQRLabel;
    QRLabel123: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel129: TQRLabel;
    QR_EXADDR1: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QR_EXADDR2: TQRLabel;
    QR_OPADDR1: TQRLabel;
    QR_OPBANK3: TQRLabel;
    QR_OPBANK2: TQRLabel;
    QR_OPADDR2: TQRLabel;
    QR_OPBANK1: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel130: TQRLabel;
    QRShape3: TQRShape;
    QRLabel131: TQRLabel;
    QRLabel133: TQRLabel;
    QR_ILNO5: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QR_EXDATE: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure PrintDocument(Fields: TFields; uPreview: Boolean = True); override;
  end;

var
  INF707_PRINT_frm: TINF707_PRINT_frm;

implementation

uses Commonlib;

{$R *.dfm}

procedure TINF707_PRINT_frm.PrintDocument(Fields: TFields; uPreview: Boolean);
var
  memoLines : TStringList;
  i,nIndex : Integer;
  messageVal : String;
begin
  inherited;
  FFields := Fields;
  FPreview := uPreview;

  with Fields do
  begin
    //문서/전자문서번호
    QR_MAINTNO.Caption := FieldByName('MAINT_NO').AsString;

    //전자문서 기능표시
    messageVal := getValueFromField('MESSAGE1');
    case StrToInt(messageVal) of
     1 : QR_FUNCTION.Caption := 'Cancel';
     2 : QR_FUNCTION.Caption := 'Delete';
     4 : QR_FUNCTION.Caption := 'Change';
     6 : QR_FUNCTION.Caption := 'Confirmation';
     7 : QR_FUNCTION.Caption := 'Duplicate';
     9 : QR_FUNCTION.Caption := 'Original';
    else
      QR_FUNCTION.Caption := '';
    end;


    //전자문서 응답유형
    if getValueFromField('MESSAGE2') = 'AB' then
      QR_RESPONSE.Caption := 'Message Acknowledgment'
    else if getValueFromField('MESSAGE2') = 'AP' then
      QR_RESPONSE.Caption := 'Accepted'
    else if getValueFromField('MESSAGE2') = 'NA' then
      QR_RESPONSE.Caption := 'No acknowledgement needed'
    else if getValueFromField('MESSAGE2') = 'Re' then
      QR_RESPONSE.Caption := 'Rjected'
    else
      QR_RESPONSE.Caption := '';


    //--------------------일반정보----------------------------------------------
      //조건변경신청일자
     QR_DATEE.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('DATEE').AsString));

     //개설방법
     QR_INMETHOD_Name.Caption := getValueFromField('IN_MATHOD') + ' ' + getValueFromField('mathod_Name');

     //개설(전문발신)은행
     QR_APBANK.Caption := getValueFromField('AP_BANK');
     QR_APBANK1.Caption := getValueFromField('AP_BANK1');
     QR_APBANK2.Caption := getValueFromField('AP_BANK2');
     QR_APBANK3.Caption := getValueFromField('AP_BANK3');
     QR_APBANK4.Caption := getValueFromField('AP_BANK4');
     QR_APBANK5.Caption := getValueFromField('AP_BANK5');

     //통지(전문수신)은행
     nIndex := 1;
      for i := 1 to 4 do
      begin
        if Trim(FieldByName('AD_BANK'+IntToStr(i)).AsString) = '' Then
        begin
           Continue;
        end;
        (FindComponent('QR_ADBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_ADBANK'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AD_BANK'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;
      QR_ADBANK.Caption := getValueFromField('AD_BANK');
      if Trim(QR_ADBANK.Caption) <> '' then
      begin
        QR_ADBANK.Enabled := True;
        ChildBand1.Height := 184 + ( 12 * nIndex);
      end
      else
      begin
        QR_ADBANK4.Top := QR_ADBANK3.Top;
        QR_ADBANK3.Top := QR_ADBANK2.Top;
        QR_ADBANK2.Top := QR_ADBANK1.Top;
        QR_ADBANK1.Top := QR_ADBANK.Top;
        ChildBand1.Height := 184 + ( 12 * (nIndex-1));
      end;

      //---------ChildBand2-----------------------------------------------------
        //신용장개설 의뢰인
        nIndex := 1;
        for i := 1 to 5 do
        begin
          if Trim(FieldByName('APPLIC'+IntToStr(i)).AsString) = '' then Continue;

          (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('APPLIC'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        //값이 하나도 없을경우 nIndex는 초기값 1을 유지하기때문
        //if (Trim(FieldByName('APPLIC1').AsString) = '') and (Trim(FieldByName('APPLIC2').AsString) = '') and (Trim(FieldByName('APPLIC3').AsString) = '') and
        //    (Trim(FieldByName('APPLIC4').AsString) = '') and (Trim(FieldByName('APPLIC5').AsString) = '') then
        if nIndex = 1 then
        begin
          ChildBand2.Enabled := False;
        end
        else
          ChildBand2.Height := (15 * (nIndex+1));
      //------------------------------------------------------------------------

      //---------ChildBand3-----------------------------------------------------
        //수입승인 관리번호
          nIndex := 1;
          for i := 1 to 5 do
          begin
            if (Trim(FieldByName('IL_NO'+IntToStr(i)).AsString) = '') and (Trim(FieldByName('IL_CUR'+IntToStr(i)).AsString) = '') and
               ((FieldByName('IL_AMT'+IntToStr(i)).AsCurrency = 0) or (Trim(FieldByName('IL_AMT'+IntToStr(i)).AsString) = ''))  then
            begin
              Continue;
            end;

            (FindComponent('QR_ILNO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
            (FindComponent('QR_ILNO'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('IL_NO'+IntToStr(i)).AsString;

            (FindComponent('QR_ILCUR'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
            (FindComponent('QR_ILCUR'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('IL_CUR'+IntToStr(i)).AsString;

            (FindComponent('QR_ILAMT'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
            (FindComponent('QR_ILAMT'+IntToStr(nIndex)) as TQRLabel).Caption := FormatFloat('#,##0.###', FieldByName('IL_AMT'+IntToStr(i)).AsFloat);
            Inc(nIndex);
          end;
        //값이 하나도 없을경우 nIndex는 초기값 1을 유지하기때문
        if nIndex = 1 then
        begin
          ChildBand3.Enabled := False;
        end
        else
          ChildBand3.Height := (13 * nIndex)+30;
      //------------------------------------------------------------------------


      //---------ChildBand4-----------------------------------------------------
        //기타정보
        nIndex := 1;
        for i := 1 to 5 do
        begin
          if Trim(FieldByName('AD_INFO'+IntToStr(i)).AsString) = '' then
          begin
            Continue;
          end;

          (FindComponent('QR_ADINFO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_ADINFO'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AD_INFO'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if nIndex = 1 then
        begin
          ChildBand4.Enabled := False;
        end
        else
          ChildBand4.Height := (13 * nIndex) + 10;
      //------------------------------------------------------------------------


      //---------ChildBand5-----------------------------------------------------
        ChildBand5.Height := ChildBand5.Tag;
        //Sender(s) Reference
        QR_CDNO.Caption := getValueFromField('CD_NO');
      //------------------------------------------------------------------------


      //---------ChildBand6-----------------------------------------------------
        //Receiver(s) Reference
        QR_RCVREF.Caption := getValueFromField('RCV_REF');
        if Trim(QR_RCVREF.Caption) = '' then
        begin
          ChildBand6.Enabled := False;
        end
        else
          ChildBand6.Height := ChildBand6.Tag;
      //------------------------------------------------------------------------


       //---------ChildBand7-----------------------------------------------------
        //Issuing Bank(s) Reference
        QR_IBANKREF.Caption := getValueFromField('IBANK_REF');
        if Trim(QR_IBANKREF.Caption) = '' then
        begin
          ChildBand7.Enabled := False;
        end
        else
          ChildBand7.Height := ChildBand7.Tag;
      //------------------------------------------------------------------------


      //---------ChildBand8-----------------------------------------------------
        //Issuing Bank
        nIndex := 1;
        for i := 1 to 5 do
        begin
          if Trim(FieldByName('ISS_BANK'+IntToStr(i)).AsString) = '' then
          begin
            (FindComponent('QR_ISSBANK'+IntToStr(i)) as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_ISSBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_ISSBANK'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('ISS_BANK'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        QR_ISSACCNT.Caption := getValueFromField('ISS_ACCNT');

        if (nIndex = 1) and (Trim(QR_ISSACCNT.Caption) = '') then
        begin
          ChildBand8.Enabled := False;
        end
        else
        begin
          if Trim(QR_ISSACCNT.Caption) <> '' then
          begin
            QRLabel36.Enabled := True;
            QR_ISSACCNT.Enabled := True;
            QRLabel36.Top := (nIndex * 13)+4;
            QR_ISSACCNT.Top := (nIndex * 13)+4;
            ChildBand8.Height := (13 * nIndex) + 20;
          end
          else
          begin
             QRLabel36.Enabled := False;
             QR_ISSACCNT.Enabled := False;
             ChildBand8.Height := (13 * nIndex);
          end;

        end;
      //------------------------------------------------------------------------

      //---------ChildBand9-----------------------------------------------------
      //Date of Issue
        QR_ISSDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(FieldByName('ISS_DATE').AsString));
        if Trim(QR_ISSDATE.Caption) = '' then
        begin
          ChildBand9.Enabled := False;
        end
        else
          ChildBand9.Height := ChildBand9.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan10-----------------------------------------------------
      //Date of Amendment         :
        QR_AMDDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(FieldByName('AMD_DATE').AsString));
        if Trim(QR_AMDDATE.Caption) = '' then
        begin
          ChildBand10.Enabled := False;
        end
        else
          ChildBand10.Height := ChildBand10.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan11-----------------------------------------------------
      //Number of Amendment       :
        QR_AMDNO.Caption := getValueFromField('AMD_NO');
        if Trim(QR_AMDNO.Caption) = '' then
        begin
          ChildBand11.Enabled := False;
        end
        else
          ChildBand11.Height := ChildBand11.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan12-----------------------------------------------------
      //Beneficiary          
        nIndex := 1;
        for i := 1 to 5 do
        begin
          if Trim(FieldByName('BENEFC'+IntToStr(i)).AsString) = '' then
          begin
            Continue;
          end;

          (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('BENEFC'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if nIndex = 1 then
        begin
          ChildBand12.Enabled := False;
        end
        else
          ChildBand12.Height := (13 * nIndex) + 4;
      //------------------------------------------------------------------------


      //---------ChildBan13-----------------------------------------------------
       //Decrease of Documentary Credit Amount
       QR_DECDCUR.Caption := getValueFromField('DECD_CUR');
         QR_DECDAMT.Caption := FormatFloat('#,##0.###' , FieldByName('DECD_AMT').AsFloat);
        if (Trim(QR_DECDAMT.Caption) = '') or (Trim(QR_DECDCUR.Caption) = '') then
        begin
          ChildBand13.Enabled := False;
        end
        else
          ChildBand13.Height := ChildBand13.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan14-----------------------------------------------------
       //New of Date of Expiry    
       QR_EXDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(FieldByName('EX_DATE').AsString));

        if Trim(QR_EXDATE.Caption) = '' then
        begin
          ChildBand14.Enabled := False;
        end
        else
          ChildBand14.Height := ChildBand14.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan15-----------------------------------------------------
       //Increase of Documentary Credit Amount
       QR_INCDCUR.Caption := getValueFromField('INCD_CUR');
       QR_INCDAMT.Caption := FormatFloat('#,##0.###' , FieldByName('INCD_AMT').AsFloat);

        if (Trim(QR_INCDCUR.Caption) = '') or (Trim(QR_INCDAMT.Caption) = '') then
        begin
          ChildBand15.Enabled := False;
        end
        else
          ChildBand15.Height := ChildBand15.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan16-----------------------------------------------------
       //Decrease of Documentary Credit Amount  :
       QR_DECDCUR2.Caption := getValueFromField('DECD_CUR');
       QR_DECDAMT2.Caption := FormatFloat('#,##0.###' , FieldByName('DECD_AMT').AsFloat);

        if (Trim(QR_DECDCUR2.Caption) = '') or (Trim(QR_DECDAMT2.Caption) = '') then
        begin
          ChildBand16.Enabled := False;
        end
        else
          ChildBand16.Height := ChildBand16.Tag;
      //------------------------------------------------------------------------
      

      //---------ChildBan17-----------------------------------------------------
       //New Documentary CreditAmount after Amentdment :
       QR_NWCDCUR.Caption := getValueFromField('NWCD_CUR');
       QR_NWCDAMT.Caption := FormatFloat('#,##0.###' , FieldByName('NWCD_AMT').AsFloat);
        if (Trim(QR_NWCDCUR.Caption) = '') or (Trim(QR_NWCDAMT.Caption) = '') then
        begin
          ChildBand17.Enabled := False;
        end
        else
          ChildBand17.Height := ChildBand17.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan18-----------------------------------------------------
       //Percentage Credit Amount Tolerance
       QR_CDPERP.Caption := '(±) ' + getValueFromField('CD_PERP') + '/' + getValueFromField('CD_PERM')+' %';

        if ( (FieldByName('CD_PERP').AsCurrency = 0) or (Trim(FieldByName('CD_PERP').AsString) = '') ) and
           ( (FieldByName('CD_PERM').AsCurrency = 0) or (Trim(FieldByName('CD_PERM').AsString) = '') ) then
        begin
          ChildBand18.Enabled := False;
        end
        else
          ChildBand18.Height := ChildBand18.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan19-----------------------------------------------------
       //Maximum Credit Amount
       QR_CDMAX.Caption := getValueFromField('CDMAX_Name');

        if Trim(QR_CDMAX.Caption) = '' then
        begin
          ChildBand19.Enabled := False;
        end
        else
          ChildBand19.Height := ChildBand19.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan20-----------------------------------------------------
      //Additional Amounts Covered           
        nIndex := 1;
        for i := 1 to 4 do
        begin
          if Trim(FieldByName('AA_CV'+IntToStr(i)).AsString) = '' then
          begin
            Continue;
          end;

          (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AA_CV'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if nIndex = 1 then
        begin
          ChildBand20.Enabled := False;
        end
        else
          ChildBand20.Height := (13 * nIndex) + 4;
      //------------------------------------------------------------------------


      //---------ChildBan21-----------------------------------------------------
        //Place of Taking in Charge/Dispatch from ···/ Place of Receipt :
        QR_LOADON.Caption := getValueFromField('LOAD_ON');
        if Trim(QR_LOADON.Caption) = '' then
        begin
          ChildBand21.Enabled := False;
        end;
        ChildBand21.Height := ChildBand21.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan22-----------------------------------------------------
        //Port of Loading / Airport of Departure :
        QR_SUNJUCKPORT.Caption := getValueFromField('SUNJUCK_PORT');
        if Trim(QR_SUNJUCKPORT.Caption) = '' then
        begin
          ChildBand22.Enabled := False;
        end;
        ChildBand22.Height := ChildBand22.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan23-----------------------------------------------------
        //Port of Discharge / Airport of Departure
        QR_DOCHACKPORT.Caption := getValueFromField('DOCHACK_PORT');
        if Trim(QR_DOCHACKPORT.Caption) = '' then
        begin
          ChildBand23.Enabled := False;
        end;
        ChildBand23.Height := ChildBand23.Tag;
      //------------------------------------------------------------------------

      //---------ChildBan24-----------------------------------------------------
        //Place of Final Destination/For Transportation to ···/ Place of Delivery : 
        QR_FORTRAN.Caption := getValueFromField('FOR_TRAN');
        if Trim(QR_FORTRAN.Caption) = '' then
        begin
          ChildBand24.Enabled := False;
        end;
        ChildBand24.Height := ChildBand24.Tag;
      //------------------------------------------------------------------------


      //---------ChildBan25-----------------------------------------------------
        //Latest Date of Shipment 
        QR_LSTDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(FieldByName('LST_DATE').AsString));

        if Trim(QR_LSTDATE.Caption) = '' then
        begin
          ChildBand25.Enabled := False;
        end;
        ChildBand25.Height := ChildBand25.Tag;
      //------------------------------------------------------------------------

      //---------ChildBan26-----------------------------------------------------
        //Shipment Period
        nIndex := 1;
        for i := 1 to 6 do
        begin
          if Trim(FieldByName('SHIP_PD'+IntToStr(i)).AsString) = '' then
          begin
            (FindComponent('QR_SHIPPD'+IntToStr(i)) as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('SHIP_PD'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if nIndex = 1  then
        begin
          ChildBand26.Enabled := False;
        end
        else
          ChildBand26.Height := (13 * nIndex) + 8;
      //------------------------------------------------------------------------

      //---------ChildBand27----------------------------------------------------
      try
        memoLines := TStringList.Create;
        memoLines.Text := FieldByName('NARRAT_1').AsString;

        ChildBand27.Height := ( 13 * memoLines.Count) + 26;
        QR_NARRAT1.Height := ( 13 * memoLines.Count);

        //Reimbursement Bank
        QR_NARRAT1.Lines.Clear;
        getValueMemo(QR_NARRAT1,'NARRAT_1');

        if (Trim(QR_NARRAT1.Caption) = '') and (FieldByName('NARRAT').AsBoolean = True) then
        begin
          ChildBand27.Enabled := False;
        end;
      finally
        memoLines.Free;
      end;
    //--------------------------------------------------------------------------

    //---------ChildBan28-----------------------------------------------------
        //Shipment Period
        nIndex := 1;
        for i := 1 to 6 do
        begin
          if Trim(FieldByName('SR_INFO'+IntToStr(i)).AsString) = '' then
          begin
            (FindComponent('QR_SRINFO'+IntToStr(i)) as TQRLabel).Caption := '';
            Continue;
          end;

          (FindComponent('QR_SRINFO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
          (FindComponent('QR_SRINFO'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('SR_INFO'+IntToStr(i)).AsString;
          Inc(nIndex);
        end;

        if nIndex = 1 then
        begin
          ChildBand28.Enabled := False;
        end
        else
          ChildBand28.Height := (13 * nIndex) + 8;
      //------------------------------------------------------------------------


      //-------------SummaryBand1-------------------------------------------------
      //신청업체
      QR_EXNAME1.Caption := getValueFromField('EX_NAME1');
      QR_EXNAME2.Caption := getValueFromField('EX_NAME2');
      QR_EXNAME3.Caption := getValueFromField('EX_NAME3');
      QR_EXADDR1.Caption := getValueFromField('EX_ADDR1');
      QR_EXADDR2.Caption := getValueFromField('EX_ADDR2');


      //개설은행
      QR_OPBANK1.Caption := getValueFromField('OP_BANK1');
      QR_OPBANK2.Caption := getValueFromField('OP_BANK2');
      QR_OPBANK3.Caption := getValueFromField('OP_BANK3');
      QR_OPADDR1.Caption := getValueFromField('OP_ADDR1');
      QR_OPADDR2.Caption := getValueFromField('OP_ADDR2');
    //--------------------------------------------------------------------------

  end;

  RunPrint;

end;

end.
