unit PCRLIC_PRINT2;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, StrUtils,Dialogs;

type
  TPCRLIC_PRINT2_FRM = class(TQuickRep)
    ColumnHeaderBand1: TQRBand;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel1: TQRLabel;
    DetailBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRShape5: TQRShape;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    qryMst: TADOQuery;
    PageFooterBand2: TQRBand;
    QRLabel16: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    qryTax: TADOQuery;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    MemoField1: TMemoField;
    BCDField1: TBCDField;
    StringField10: TStringField;
    BCDField2: TBCDField;
    StringField11: TStringField;
    BCDField3: TBCDField;
    StringField12: TStringField;
    qryMstAPP_DATE: TStringField;
    qryMstBK_NAME1: TStringField;
    qryMstBK_NAME2: TStringField;
    qryMstBK_NAME5: TStringField;
    QRLabel20: TQRLabel;
    lbl_Product1: TQRLabel;
    lbl_Product2: TQRLabel;
    lbl_Product3: TQRLabel;
    lbl_Product4: TQRLabel;
    lbl_Product5: TQRLabel;
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure PageFooterBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure PageFooterBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FDocNo : String;
    FTotalPage : integer;
    FPageCount : integer;
    Findex : integer;
    procedure OPENDB;
  public
    property  DocNo:string  read FDocNo write FDocNo;
    property Totalpage:integer  read FTotalPage write FTotalPage;
  end;

var
  PCRLIC_PRINT2_FRM: TPCRLIC_PRINT2_FRM;

const
  MULTI_LINE_HEIGHT : array [0..5] of Integer = (23,44,61,79,97,116);
implementation

uses MSSQL;

{$R *.DFM}

{ TPCRLIC_PRINT2_FRM }

procedure TPCRLIC_PRINT2_FRM.OPENDB;
begin
  qryMst.Close;
  qryMst.Parameters.ParamByName('MAINT_NO').Value := FDocNo;
  qryMst.Open;

  qryTax.Close;
  qryTax.Parameters.ParamByName('KEYY').Value := FDocNo;
  qryTax.Open;
end;

procedure TPCRLIC_PRINT2_FRM.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  MoreData := not qryTax.Eof
end;

procedure TPCRLIC_PRINT2_FRM.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  OPENDB;
  qryTax.First;
  FPageCount := 0;
end;

procedure TPCRLIC_PRINT2_FRM.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  TempDate : String;
  Line_Cnt : Integer;
begin
  QRLabel33.Caption := qryTax.fieldByName('BILL_NO').AsString;
  TempDate := qryTax.fieldByName('BILL_DATE').AsString;
  QRLabel17.Caption := LeftStr(TempDate,4)+'.'+MidStr(TempDate,5,2)+'.'+RightStr(TempDate,2);
  QRLabel18.Caption := qryTax.FieldByName('BILL_AMOUNT_UNIT').AsString+' '+FormatFloat('#,0.000',qryTax.FieldByName('BILL_AMOUNT').AsCurrency);

  IF qryTax.FieldByName('TAX_AMOUNT').AsCurrency > 0 Then
    QRLabel19.Caption := qryTax.FieldByName('TAX_AMOUNT_UNIT').AsString+' '+FormatFloat('#,0.000',qryTax.FieldByName('TAX_AMOUNT').AsCurrency)
  else
    QRLabel19.Caption := qryTax.FieldByName('TAX_AMOUNT_UNIT').AsString+' '+'0';

  IF qryTax.FieldByName('QUANTITY').AsCurrency > 0 Then
    QRLabel20.Caption := qryTax.FieldByName('QUANTITY_UNIT').AsString+' '+FormatFloat('#,0.000',qryTax.FieldByName('QUANTITY').AsCurrency)
  else
    QRLabel20.Caption := '';

  Line_Cnt := 0;
  //품목이 있으면
  IF Trim(qryTax.FieldByName('ITEM_NAME1').AsString) <> '' THEN
  begin
    Line_Cnt := 1;
    lbl_Product1.Caption := Trim(qryTax.FieldByName('ITEM_NAME1').AsString);
  end;
  IF Trim(qryTax.FieldByName('ITEM_NAME2').AsString) <> '' THEN
  begin
    Line_Cnt := 2;
    lbl_Product2.Caption := Trim(qryTax.FieldByName('ITEM_NAME2').AsString);
  end;
  IF Trim(qryTax.FieldByName('ITEM_NAME3').AsString) <> '' THEN
  begin
    Line_Cnt := 3;
    lbl_Product3.Caption := Trim(qryTax.FieldByName('ITEM_NAME3').AsString);
  end;
  IF Trim(qryTax.FieldByName('ITEM_NAME4').AsString) <> '' THEN
  begin
    Line_Cnt := 4;
    lbl_Product4.Caption := Trim(qryTax.FieldByName('ITEM_NAME4').AsString);
  end;
  IF Trim(qryTax.FieldByName('ITEM_NAME5').AsString) <> '' THEN
  begin
    Line_Cnt := 5;
    lbl_Product5.Caption := Trim(qryTax.FieldByName('ITEM_NAME5').AsString);
  end;

  DetailBand1.Height := MULTI_LINE_HEIGHT[Line_Cnt];

  qryTax.Next;
end;

procedure TPCRLIC_PRINT2_FRM.PageFooterBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

  QRLabel9.Caption := LeftStr(qryMstAPP_DATE.AsString,4)+' 년 '+MIDStr(qryMstAPP_DATE.AsString,5,2)+' 월 '+RightStr(qryMstAPP_DATE.AsString,2)+' 일';

  QRLabel10.Caption := qryMstBK_NAME1.AsString;
  QRLabel11.Caption := qryMstBK_NAME2.AsString;

  QRLabel12.Caption := qryMstBK_NAME5.AsString;
end;

procedure TPCRLIC_PRINT2_FRM.PageFooterBand2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRLabel16.Caption := IntToStr(Self.PageNumber)+' / '+IntToStr(FTotalPage) ;
end;

end.
