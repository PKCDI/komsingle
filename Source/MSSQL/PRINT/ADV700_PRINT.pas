unit ADV700_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TADV700_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRLabel88: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel4: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QRLabel5: TQRLabel;
    QR_MSG1: TQRLabel;
    QRLabel7: TQRLabel;
    QR_MSG2: TQRLabel;
    ChildBand1: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel83: TQRLabel;
    QRLabel9: TQRLabel;
    QR_APPDATE: TQRLabel;
    QRLabel11: TQRLabel;
    QR_APPNO: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_APBANK: TQRLabel;
    QR_APBANK1: TQRLabel;
    QR_APBANK2: TQRLabel;
    QR_APBANK3: TQRLabel;
    QR_APBANK4: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel16: TQRLabel;
    QR_ADBANK: TQRLabel;
    QR_ADBANK1: TQRLabel;
    QR_ADBANK2: TQRLabel;
    QR_ADBANK3: TQRLabel;
    QR_ADBANK4: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel22: TQRLabel;
    QR_ADDINFO1: TQRMemo;
    ChildBand5: TQRChildBand;
    QRLabel23: TQRLabel;
    QRImage2: TQRImage;
    ChildBand6: TQRChildBand;
    QRLabel24: TQRLabel;
    QR_DOCCD1: TQRLabel;
    QR_DOCCD2: TQRLabel;
    QR_DOCCD3: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel28: TQRLabel;
    QR_CDNO: TQRLabel;
    ChildBand8: TQRChildBand;
    QRLabel30: TQRLabel;
    QR_REEPRE: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    ChildBand9: TQRChildBand;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QR_ISSDATE: TQRLabel;
    ChildBand10: TQRChildBand;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QR_APPLICABLERULES1: TQRLabel;
    QR_APPLICABLERULES2: TQRLabel;
    ChildBand11: TQRChildBand;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QR_EXDATE: TQRLabel;
    QR_EXPLACE: TQRLabel;
    ChildBand12: TQRChildBand;
    QRLabel46: TQRLabel;
    QR_APPBANK: TQRLabel;
    QR_APPBANK1: TQRLabel;
    QR_APPBANK2: TQRLabel;
    QR_APPBANK3: TQRLabel;
    QR_APPBANK4: TQRLabel;
    QRLabel52: TQRLabel;
    QR_APPBANK5: TQRLabel;
    ChildBand13: TQRChildBand;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC3: TQRLabel;
    QR_APPLIC4: TQRLabel;
    QR_APPLIC5: TQRLabel;
    ChildBand14: TQRChildBand;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    QR_BENEFC4: TQRLabel;
    QR_BENEFC5: TQRLabel;
    ChildBand15: TQRChildBand;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    QR_CDAMT: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QR_CDPERPM: TQRLabel;
    ChildBand17: TQRChildBand;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QR_CDMAX: TQRLabel;
    ChildBand18: TQRChildBand;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QR_AACV1: TQRLabel;
    QR_AACV2: TQRLabel;
    QR_AACV3: TQRLabel;
    QR_AACV4: TQRLabel;
    ChildBand19: TQRChildBand;
    QRLabel10: TQRLabel;
    QRLabel14: TQRLabel;
    QR_AVPAY: TQRLabel;
    ChildBand20: TQRChildBand;
    ChildBand21: TQRChildBand;
    ChildBand22: TQRChildBand;
    ChildBand23: TQRChildBand;
    ChildBand24: TQRChildBand;
    ChildBand25: TQRChildBand;
    ChildBand26: TQRChildBand;
    ChildBand27: TQRChildBand;
    ChildBand28: TQRChildBand;
    ChildBand29: TQRChildBand;
    ChildBand30: TQRChildBand;
    ChildBand31: TQRChildBand;
    ChildBand32: TQRChildBand;
    ChildBand33: TQRChildBand;
    ChildBand34: TQRChildBand;
    ChildBand35: TQRChildBand;
    ChildBand36: TQRChildBand;
    ChildBand37: TQRChildBand;
    ChildBand38: TQRChildBand;
    ChildBand40: TQRChildBand;
    ChildBand41: TQRChildBand;
    ChildBand42: TQRChildBand;
    SummaryBand1: TQRBand;
    PageFooterBand1: TQRBand;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    qryADV700: TADOQuery;
    qryADV7001: TADOQuery;
    qryADV7002: TADOQuery;
    qryADV7003: TADOQuery;
    qryADV700MAINT_NO: TStringField;
    qryADV700Chk1: TBooleanField;
    qryADV700Chk2: TStringField;
    qryADV700Chk3: TStringField;
    qryADV700MESSAGE1: TStringField;
    qryADV700MESSAGE2: TStringField;
    qryADV700USER_ID: TStringField;
    qryADV700DATEE: TStringField;
    qryADV700APP_DATE: TStringField;
    qryADV700APP_NO: TStringField;
    qryADV700AP_BANK: TStringField;
    qryADV700AP_BANK1: TStringField;
    qryADV700AP_BANK2: TStringField;
    qryADV700AP_BANK3: TStringField;
    qryADV700AP_BANK4: TStringField;
    qryADV700AP_BANK5: TStringField;
    qryADV700AD_BANK: TStringField;
    qryADV700AD_BANK1: TStringField;
    qryADV700AD_BANK2: TStringField;
    qryADV700AD_BANK3: TStringField;
    qryADV700AD_BANK4: TStringField;
    qryADV700AD_BANK5: TStringField;
    qryADV700ADDINFO: TStringField;
    qryADV700ADDINFO_1: TMemoField;
    qryADV700DOC_CD1: TStringField;
    qryADV700DOC_CD2: TStringField;
    qryADV700DOC_CD3: TStringField;
    qryADV700CD_NO: TStringField;
    qryADV700REE_PRE: TStringField;
    qryADV700ISS_DATE: TStringField;
    qryADV700EX_DATE: TStringField;
    qryADV700EX_PLACE: TStringField;
    qryADV700APP_BANK: TStringField;
    qryADV700APP_BANK1: TStringField;
    qryADV700APP_BANK2: TStringField;
    qryADV700APP_BANK3: TStringField;
    qryADV700APP_BANK4: TStringField;
    qryADV700APP_BANK5: TStringField;
    qryADV700APP_ACCNT: TStringField;
    qryADV700APPLIC1: TStringField;
    qryADV700APPLIC2: TStringField;
    qryADV700APPLIC3: TStringField;
    qryADV700APPLIC4: TStringField;
    qryADV700APPLIC5: TStringField;
    qryADV700BENEFC1: TStringField;
    qryADV700BENEFC2: TStringField;
    qryADV700BENEFC3: TStringField;
    qryADV700BENEFC4: TStringField;
    qryADV700BENEFC5: TStringField;
    qryADV700CD_AMT: TBCDField;
    qryADV700CD_CUR: TStringField;
    qryADV700CD_PERP: TBCDField;
    qryADV700CD_PERM: TBCDField;
    qryADV700CD_MAX: TStringField;
    qryADV700AA_CV1: TStringField;
    qryADV700AA_CV2: TStringField;
    qryADV700AA_CV3: TStringField;
    qryADV700AA_CV4: TStringField;
    qryADV700BENEFC6: TStringField;
    qryADV700PRNO: TIntegerField;
    qryADV7001MAINT_NO: TStringField;
    qryADV7001AVAIL: TStringField;
    qryADV7001AVAIL1: TStringField;
    qryADV7001AVAIL2: TStringField;
    qryADV7001AVAIL3: TStringField;
    qryADV7001AVAIL4: TStringField;
    qryADV7001AVAIL5: TStringField;
    qryADV7001AV_ACCNT: TStringField;
    qryADV7001AV_PAY: TStringField;
    qryADV7001DRAFT1: TStringField;
    qryADV7001DRAFT2: TStringField;
    qryADV7001DRAFT3: TStringField;
    qryADV7001DRAFT4: TStringField;
    qryADV7001DRAWEE: TStringField;
    qryADV7001DRAWEE1: TStringField;
    qryADV7001DRAWEE2: TStringField;
    qryADV7001DRAWEE3: TStringField;
    qryADV7001DRAWEE4: TStringField;
    qryADV7001DRAWEE5: TStringField;
    qryADV7001DR_ACCNT: TStringField;
    qryADV7001MIX_PAY1: TStringField;
    qryADV7001MIX_PAY2: TStringField;
    qryADV7001MIX_PAY3: TStringField;
    qryADV7001MIX_PAY4: TStringField;
    qryADV7001DEF_PAY1: TStringField;
    qryADV7001DEF_PAY2: TStringField;
    qryADV7001DEF_PAY3: TStringField;
    qryADV7001DEF_PAY4: TStringField;
    qryADV7001LST_DATE: TStringField;
    qryADV7001SHIP_PD: TStringField;
    qryADV7001SHIP_PD1: TStringField;
    qryADV7001SHIP_PD2: TStringField;
    qryADV7001SHIP_PD3: TStringField;
    qryADV7001SHIP_PD4: TStringField;
    qryADV7001SHIP_PD5: TStringField;
    qryADV7001SHIP_PD6: TStringField;
    qryADV7002MAINT_NO: TStringField;
    qryADV7002DESGOOD: TStringField;
    qryADV7002DESGOOD_1: TMemoField;
    qryADV7002DOCREQU: TStringField;
    qryADV7002DOCREQU_1: TMemoField;
    qryADV7002ADDCOND: TStringField;
    qryADV7002ADDCOND_1: TMemoField;
    qryADV7002CHARGE1: TStringField;
    qryADV7002CHARGE2: TStringField;
    qryADV7002CHARGE3: TStringField;
    qryADV7002CHARGE4: TStringField;
    qryADV7002CHARGE5: TStringField;
    qryADV7002CHARGE6: TStringField;
    qryADV7002PD_PRSNT1: TStringField;
    qryADV7002PD_PRSNT2: TStringField;
    qryADV7002PD_PRSNT3: TStringField;
    qryADV7002PD_PRSNT4: TStringField;
    qryADV7002CONFIRMM: TStringField;
    qryADV7002REI_BANK: TStringField;
    qryADV7002REI_BANK1: TStringField;
    qryADV7002REI_BANK2: TStringField;
    qryADV7002REI_BANK3: TStringField;
    qryADV7002REI_BANK4: TStringField;
    qryADV7002REI_BANK5: TStringField;
    qryADV7002REI_ACCNT: TStringField;
    qryADV7002INSTRCT: TStringField;
    qryADV7002INSTRCT_1: TMemoField;
    qryADV7002AVT_BANK: TStringField;
    qryADV7002AVT_BANK1: TStringField;
    qryADV7002AVT_BANK2: TStringField;
    qryADV7002AVT_BANK3: TStringField;
    qryADV7002AVT_BANK4: TStringField;
    qryADV7002AVT_BANK5: TStringField;
    qryADV7002AVT_ACCNT: TStringField;
    qryADV7002SND_INFO1: TStringField;
    qryADV7002SND_INFO2: TStringField;
    qryADV7002SND_INFO3: TStringField;
    qryADV7002SND_INFO4: TStringField;
    qryADV7002SND_INFO5: TStringField;
    qryADV7002SND_INFO6: TStringField;
    qryADV7002EX_NAME1: TStringField;
    qryADV7002EX_NAME2: TStringField;
    qryADV7002EX_NAME3: TStringField;
    qryADV7002EX_ADDR1: TStringField;
    qryADV7002EX_ADDR2: TStringField;
    qryADV7002CHK1: TStringField;
    qryADV7002CHK2: TStringField;
    qryADV7002CHK3: TStringField;
    qryADV7002PRNO: TIntegerField;
    qryADV7003MAINT_NO: TStringField;
    qryADV7003FOR_TRAN: TStringField;
    qryADV7003LOAD_ON: TStringField;
    qryADV7003TSHIP: TStringField;
    qryADV7003PSHIP: TStringField;
    qryADV7003APPLICABLE_RULES_1: TStringField;
    qryADV7003APPLICABLE_RULES_2: TStringField;
    qryADV7003SUNJUCK_PORT: TStringField;
    qryADV7003DOCHACK_PORT: TStringField;
    ChildBand43: TQRChildBand;
    ChildBand44: TQRChildBand;
    ChildBand45: TQRChildBand;
    ChildBand46: TQRChildBand;
    QR_REIBANK5: TQRLabel;
    QR_REIBANK4: TQRLabel;
    QR_REIBANK3: TQRLabel;
    QR_REIBANK1: TQRLabel;
    QR_REIBANK: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel76: TQRLabel;
    ChildBand39: TQRChildBand;
    QRLabel78: TQRLabel;
    QRLabel77: TQRLabel;
    QR_CONFIRMM: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QR_PDPRSNT1: TQRLabel;
    QR_PDPRSNT2: TQRLabel;
    QR_PDPRSNT3: TQRLabel;
    QR_PDPRSNT4: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel72: TQRLabel;
    QR_CHARGE1: TQRLabel;
    QR_CHARGE2: TQRLabel;
    QR_CHARGE3: TQRLabel;
    QR_CHARGE4: TQRLabel;
    QR_CHARGE5: TQRLabel;
    QR_CHARGE6: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QR_ADDCOND1: TQRMemo;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QR_DOCREQU1: TQRMemo;
    QRLabel67: TQRLabel;
    QRLabel66: TQRLabel;
    QR_DESGOOD1: TQRMemo;
    QRLabel51: TQRLabel;
    QRLabel65: TQRLabel;
    QR_SHIPPD1: TQRLabel;
    QR_SHIPPD2: TQRLabel;
    QR_SHIPPD3: TQRLabel;
    QR_SHIPPD4: TQRLabel;
    QR_SHIPPD5: TQRLabel;
    QR_SHIPPD6: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel57: TQRLabel;
    QR_LSTDATE: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QR_FORTRAN: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel58: TQRLabel;
    QR_DOCHACKPORT: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel56: TQRLabel;
    QR_SUNJUCKPORT: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QR_LOADON: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel47: TQRLabel;
    QR_TSHIP: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel44: TQRLabel;
    QR_PSHIP: TQRLabel;
    QR_DRACCNT: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel40: TQRLabel;
    QR_DRAWEE: TQRLabel;
    QR_DRAWEE1: TQRLabel;
    QR_DRAWEE2: TQRLabel;
    QR_DRAWEE3: TQRLabel;
    QR_DRAWEE4: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel31: TQRLabel;
    QR_DEFPAY1: TQRLabel;
    QR_DEFPAY2: TQRLabel;
    QR_DEFPAY3: TQRLabel;
    QR_DEFPAY4: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QR_MIXPAY1: TQRLabel;
    QR_MIXPAY2: TQRLabel;
    QR_MIXPAY3: TQRLabel;
    QR_MIXPAY4: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel19: TQRLabel;
    QR_DRAFT1: TQRLabel;
    QR_DRAFT2: TQRLabel;
    QR_DRAFT3: TQRLabel;
    QR_AVACCNT: TQRLabel;
    QR_AVAIL: TQRLabel;
    QR_AVAIL1: TQRLabel;
    QR_AVAIL2: TQRLabel;
    QR_AVAIL3: TQRLabel;
    QR_AVAIL4: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QR_REIBANK2: TQRLabel;
    ChildBand47: TQRChildBand;
    QRShape2: TQRShape;
    QRLabel99: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabel87: TQRLabel;
    QR_EXADDR2: TQRLabel;
    QR_EXADDR1: TQRLabel;
    QRLabel97: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QRLabel93: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QRLabel89: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel85: TQRLabel;
    QRImage3: TQRImage;
    QR_SNDINFO6: TQRLabel;
    QR_SNDINFO5: TQRLabel;
    QR_SNDINFO4: TQRLabel;
    QR_SNDINFO3: TQRLabel;
    QR_SNDINFO2: TQRLabel;
    QR_SNDINFO1: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel95: TQRLabel;
    QR_AVTACCNT: TQRLabel;
    QR_AVTBANK5: TQRLabel;
    QR_AVTBANK4: TQRLabel;
    QR_AVTBANK3: TQRLabel;
    QR_AVTBANK2: TQRLabel;
    QR_AVTBANK1: TQRLabel;
    QR_AVTBANK: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel80: TQRLabel;
    QR_INSTRCT1: TQRMemo;
    QRLabel81: TQRLabel;
    QR_REIACCNT: TQRLabel;
    QR_ADBANK5: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure TitleBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand14BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand15BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand17BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand18BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand19BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand20BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand21BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand22BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand23BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand24BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand25BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand26BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand27BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand28BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand29BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand30BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand31BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand32BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand33BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand34BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand35BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand36BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand37BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand38BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand39BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand40BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand41BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand42BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand43BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand44BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand45BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand46BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand47BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNO: string;
    procedure OPENDB;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
  end;

var
  ADV700_PRINT_frm: TADV700_PRINT_frm;

implementation

{$R *.dfm}

uses MSSQL , Commonlib, StrUtils;

{ TADV700_PRINT_frm }

procedure TADV700_PRINT_frm.OPENDB;
begin
  qryADV700.Close;
  qryADV700.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryADV700.Open;

  qryADV7001.Close;
  qryADV7001.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryADV7001.Open;

  qryADV7002.Close;
  qryADV7002.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryADV7002.Open;

  qryADV7003.Close;
  qryADV7003.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryADV7003.Open;
end;

procedure TADV700_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
end;

procedure TADV700_PRINT_frm.TitleBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  TitleBand1.Height := TitleBand1.Tag;
  //문서/전자문서번호
  QR_MAINTNO.Caption := qryADV700MAINT_NO.AsString;
  //전자문서 기능표시
  case StrToInt(Trim(qryADV700MESSAGE1.AsString)) of
    1 : QR_MSG1.Caption := 'Cancel';
    3 : QR_MSG1.Caption := 'Delete';
    4 : QR_MSG1.Caption := 'Change';
    6 : QR_MSG1.Caption := 'Confirmation';
    7 : QR_MSG1.Caption := 'Duplicate';
    9 : QR_MSG1.Caption := 'Original';
  end;

  case AnsiIndexText( Trim(qryADV700MESSAGE2.AsString) , ['AB', 'AP', 'NA', 'RE']) of
    0 : QR_MSG2.Caption := 'Message Acknowledgement';
    1 : QR_MSG2.Caption := 'Accepted';
    2 : QR_MSG2.Caption := 'No acknowledgement needed';
    3 : QR_MSG2.Caption := 'Rejected';
  end;

end;

procedure TADV700_PRINT_frm.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  ChildBand1.Height := ChildBand1.Tag;
  //통지일자
  QR_APPDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryADV700APP_DATE.AsString));
  //통지번호
  QR_APPNO.Caption := qryADV700APP_NO.AsString;
end;

procedure TADV700_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //개설(전문발신)은행
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV700.FieldByName('AP_BANK'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV700.FieldByName('AP_BANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if Trim(qryADV700AP_BANK.AsString) <> '' then //코드값이 있을경우
  begin
    QR_APBANK.Enabled := True;
    QR_APBANK.Caption := qryADV700AP_BANK.AsString;
    ChildBand2.Height := (15 * nIndex);
  end
  else//코드값이 없을경우
  begin
    QR_APBANK4.Top := QR_APBANK3.Top;
    QR_APBANK3.Top := QR_APBANK2.Top;
    QR_APBANK2.Top := QR_APBANK1.Top;
    QR_APBANK1.Top := QR_APBANK.Top;
    ChildBand2.Height := (15 * (nIndex-1));
  end;

  //값이 하나도 없을경우 ChaildBand 전체 숨김
  if (Trim(qryADV700AP_BANK.AsString) = '') and (Trim(qryADV700AP_BANK1.AsString) = '') and
     (Trim(qryADV700AP_BANK2.AsString) = '') and (Trim(qryADV700AP_BANK4.AsString) = '') then
  begin
    ChildBand2.Enabled := False;
  end;
end;

procedure TADV700_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //통지(전문수신)은행
  nIndex := 1;
  for i := 1 to 5 do
  begin
    if Trim(qryADV700.FieldByName('AD_BANK'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_ADBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_ADBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV700.FieldByName('AD_BANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if Trim(qryADV700AD_BANK.AsString) <> '' then // 코드값이 있을경우
  begin
    QR_ADBANK.Enabled := True;
    QR_ADBANK.Caption := qryADV700AD_BANK.AsString;
    ChildBand3.Height := (15 * nIndex);
  end
  else // 코드값이 없을경우
  begin
    QR_ADBANK5.Top := QR_ADBANK4.Top;
    QR_ADBANK4.Top := QR_ADBANK3.Top;
    QR_ADBANK3.Top := QR_ADBANK2.Top;
    QR_ADBANK2.Top := QR_ADBANK1.Top;
    QR_ADBANK1.Top := QR_ADBANK.Top;
    ChildBand3.Height := (15 * (nIndex-1));
  end;

  //값이 하나도 없을경우 ChaildBand 전체 숨김
  if (Trim(qryADV700AD_BANK.AsString) = '') and (Trim(qryADV700AD_BANK1.AsString) = '') and
     (Trim(qryADV700AD_BANK2.AsString) = '') and (Trim(qryADV700AD_BANK4.AsString) = '') then
  begin
    ChildBand3.Enabled := False;
  end;

end;

procedure TADV700_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //기타정보
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryADV700ADDINFO_1.AsString;
    ChildBand4.Height := ( 15 * memoLines.Count) + 20;
    QR_ADDINFO1.Height := ( 15 * memoLines.Count);

    QR_ADDINFO1.Lines.Clear;
    QR_ADDINFO1.Lines.Text := qryADV700ADDINFO_1.AsString;
  finally
    memoLines.Free;
  end;

  if Trim(QR_ADDINFO1.Lines.Text) = '' then ChildBand4.Enabled := False;

end;

procedure TADV700_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex : Integer;
begin
  inherited;
  //Form of Documentary Credit
  nIndex := 1;
  for i := 1 to 3 do
  begin
    if Trim(qryADV700.FieldByName('DOC_CD'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_DOCCD'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_DOCCD'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV700.FieldByName('DOC_CD'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (trim(qryADV700DOC_CD1.AsString) = '') and (trim(qryADV700DOC_CD2.AsString) = '') and (trim(qryADV700DOC_CD1.AsString) = '') then
    ChildBand6.Enabled := False
  else
    ChildBand6.Height := 15 * (nIndex-1);

end;

procedure TADV700_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Documentary Credit Number
  QR_CDNO.Caption := qryADV700CD_NO.AsString;
  if Trim(QR_CDNO.Caption) = '' then
    ChildBand7.Enabled := False
  else
    ChildBand7.Height := ChildBand7.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Reference to Pre-Advice    
  QR_REEPRE.Caption := qryADV700REE_PRE.AsString;
  if Trim(QR_REEPRE.Caption) = '' then
    ChildBand8.Enabled := False
  else
    ChildBand8.Height := ChildBand8.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Date of Issue               
  QR_ISSDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryADV700ISS_DATE.AsString));
  if Trim(QR_ISSDATE.Caption) = '' then
    ChildBand9.Enabled := False
  else
    ChildBand9.Height := ChildBand9.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand10BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex : Integer;
begin
  inherited;
  //Applicable Rulles
  nIndex := 1;
  for i := 1 to 2 do
  begin
    if Trim(qryADV7003.FieldByName('APPLICABLE_RULES_'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_APPLICABLERULES'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_APPLICABLERULES'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7003.FieldByName('APPLICABLE_RULES_'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (trim(qryADV7003APPLICABLE_RULES_1.AsString) = '') and (trim(qryADV7003APPLICABLE_RULES_2.AsString) = '') then
    ChildBand10.Enabled := False
  else
    ChildBand10.Height := 15 * (nIndex-1);

end;

procedure TADV700_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Date and Place of Expiry
  QR_EXDATE.Caption := '(date) ' + FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryADV700EX_DATE.AsString));
  QR_EXPLACE.Caption :='(place) ' + qryADV700EX_PLACE.AsString;

  if (Trim(qryADV700EX_DATE.AsString) = '') and (Trim(qryADV700EX_PLACE.AsString) = '') then
    ChildBand11.Enabled := False
  else
    ChildBand11.Height := ChildBand11.Tag;

end;

procedure TADV700_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Applicant Bank
  nIndex := 1;
  for i := 1 to 5 do
  begin
    if Trim(qryADV700.FieldByName('APP_BANK'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_APPBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_APPBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV700.FieldByName('APP_BANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if Trim(qryADV700APP_BANK.AsString) <> '' then //코드값이 있을경우
  begin
    QR_APPBANK.Enabled := True;
    QR_APPBANK.Caption := qryADV700APP_BANK.AsString;
    ChildBand12.Height := (15 * nIndex);
  end
  else//코드값이 없을경우
  begin
    QR_APPBANK5.Top := QR_APPBANK4.Top;
    QR_APPBANK4.Top := QR_APPBANK3.Top;
    QR_APPBANK3.Top := QR_APPBANK2.Top;
    QR_APPBANK2.Top := QR_APPBANK1.Top;
    QR_APPBANK1.Top := QR_APPBANK.Top;
    ChildBand12.Height := (15 * (nIndex-1));
  end;

  //값이 하나도 없을경우 ChaildBand 전체 숨김
  if (Trim(qryADV700APP_BANK.AsString) = '') and (Trim(qryADV700APP_BANK1.AsString) = '') and
     (Trim(qryADV700APP_BANK2.AsString) = '') and (Trim(qryADV700APP_BANK4.AsString) = '') then
  begin
    ChildBand12.Enabled := False;
  end;
end;

procedure TADV700_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex : Integer;
begin
  inherited;
  //Applicant
  nIndex := 1;
  for i := 1 to 5 do
  begin
    if Trim(qryADV700.FieldByName('APPLIC'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV700.FieldByName('APPLIC'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (trim(qryADV700APPLIC1.AsString) = '') and (trim(qryADV700APPLIC2.AsString) = '') and (trim(qryADV700APPLIC3.AsString) = '') and
     (trim(qryADV700APPLIC4.AsString) = '') and (trim(qryADV700APPLIC5.AsString) = '')then
    ChildBand13.Enabled := False
  else
    ChildBand13.Height := 15 * (nIndex-1);
end;

procedure TADV700_PRINT_frm.ChildBand14BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex : Integer;
begin
  inherited;
  //Beneficiary
  nIndex := 1;
  for i := 1 to 5 do
  begin
    if Trim(qryADV700.FieldByName('BENEFC'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;

    (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV700.FieldByName('BENEFC'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (trim(qryADV700BENEFC1.AsString) = '') and (trim(qryADV700BENEFC2.AsString) = '') and (trim(qryADV700BENEFC3.AsString) = '') and
     (trim(qryADV700BENEFC4.AsString) = '') and (trim(qryADV700BENEFC5.AsString) = '')then
    ChildBand14.Enabled := False
  else
    ChildBand14.Height := 15 * (nIndex-1);
end;

procedure TADV700_PRINT_frm.ChildBand15BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Currency Code,Amount
  QR_CDAMT.Caption := qryADV700CD_CUR.AsString + ' ' + FormatFloat('#,##0.000' , qryADV700CD_AMT.AsFloat);
  if Trim(QR_CDAMT.Caption) = '0' then
    ChildBand15.Enabled := False
  else
    ChildBand15.Height := ChildBand15.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand16BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Percentage Credit Amount Tolerance
  QR_CDPERPM.Caption := '(±) ' + CurrToStr(qryADV700CD_PERP.AsCurrency) + '/' + CurrToStr(qryADV700CD_PERM.AsCurrency) + ' (%)';
  if (qryADV700CD_PERP.AsCurrency = 0) and (qryADV700CD_PERM.AsCurrency = 0) then
    ChildBand16.Enabled := False
  else
    ChildBand16.Height := ChildBand16.Tag
end;

procedure TADV700_PRINT_frm.ChildBand17BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Maximum Credit Amount
  QR_CDMAX.Caption := qryADV700CD_MAX.AsString;
  if QR_CDMAX.Caption = '' then
    ChildBand17.Enabled := False
  else
    ChildBand17.Height := ChildBand17.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand18BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i , nIndex : Integer;
begin
  inherited;
  //Additional Amounts Covered
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV700.FieldByName('AA_CV'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV700.FieldByName('AA_CV'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (trim(qryADV700AA_CV1.AsString) = '') and (trim(qryADV700AA_CV2.AsString) = '') and (trim(qryADV700AA_CV3.AsString) = '') and
     (trim(qryADV700AA_CV4.AsString) = '') then
    ChildBand18.Enabled := False
  else
    ChildBand18.Height := 15 * (nIndex-1);
end;

procedure TADV700_PRINT_frm.ChildBand19BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_AVPAY.Caption := qryADV7001AV_PAY.AsString;
  if Trim(QR_AVPAY.Caption) = '' then
    ChildBand19.Enabled := False
  else
    ChildBand19.Height := ChildBand19.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand20BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Available With...... By
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV7001.FieldByName('AVAIL'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_AVAIL'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_AVAIL'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7001.FieldByName('AVAIL'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  //AV_PAY의 값이 없을경우 Label활성화시킴
  if Trim(QR_AVPAY.Caption) = '' then
  begin
    QRLabel102.Enabled := True;
    QRLabel103.Enabled := True;
  end
  else
  begin
    QRLabel102.Enabled := False;
    QRLabel103.Enabled := False;
  end;

  if Trim(qryADV7001AVAIL.AsString) <> '' then //코드값이 있을경우
  begin
    QR_AVAIL.Enabled := True;
    QR_AVAIL.Caption := qryADV7001AVAIL.AsString;
    ChildBand20.Height := (15 * nIndex);
  end
  else//코드값이 없을경우
  begin
    QR_AVAIL4.Top := QR_AVAIL3.Top;
    QR_AVAIL3.Top := QR_AVAIL2.Top;
    QR_AVAIL2.Top := QR_AVAIL1.Top;
    QR_AVAIL1.Top := QR_AVAIL.Top;
    ChildBand20.Height := (15 * (nIndex-1));
  end;

  //값이 하나도 없을경우 ChaildBand 전체 숨김
  if (Trim(qryADV7001AVAIL.AsString) = '') and (Trim(qryADV7001AVAIL1.AsString) = '') and
     (Trim(qryADV7001AVAIL2.AsString) = '') and (Trim(qryADV7001AVAIL3.AsString) = '') and (Trim(qryADV7001AVAIL4.AsString) = '') then
  begin
    ChildBand20.Enabled := False;
  end;


end;

procedure TADV700_PRINT_frm.ChildBand21BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
   QR_AVACCNT.Caption := qryADV7001AV_ACCNT.AsString;
   if Trim(QR_AVACCNT.Caption) = '' then
    ChildBand21.Enabled := False
   else
    ChildBand21.Height := ChildBand21.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand22BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Drafts at ...
  nIndex := 1;
  for i := 1 to 3 do
  begin
    if Trim(qryADV7001.FieldByName('DRAFT'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_DRAFT'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_DRAFT'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7001.FieldByName('DRAFT'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_DRAFT1.Caption) = '') and (Trim(QR_DRAFT2.Caption) = '') and (Trim(QR_DRAFT3.Caption) = '') then
    ChildBand22.Enabled := False
  else
    ChildBand22.Height := 15 * (nIndex-1);

end;

procedure TADV700_PRINT_frm.ChildBand23BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Mixed Payment Details
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV7001.FieldByName('MIX_PAY'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_MIXPAY'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_MIXPAY'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7001.FieldByName('MIX_PAY'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_MIXPAY1.Caption) = '') and (Trim(QR_MIXPAY2.Caption) = '') and (Trim(QR_MIXPAY3.Caption) = '') and (Trim(QR_MIXPAY4.Caption) = '') then
    ChildBand23.Enabled := False
  else
    ChildBand23.Height := 15 * (nIndex-1);

end;

procedure TADV700_PRINT_frm.ChildBand24BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Terms of Payment
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV7001.FieldByName('DEF_PAY'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_DEFPAY'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_DEFPAY'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7001.FieldByName('DEF_PAY'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_DEFPAY1.Caption) = '') and (Trim(QR_DEFPAY2.Caption) = '') and (Trim(QR_DEFPAY3.Caption) = '') and (Trim(QR_DEFPAY4.Caption) = '') then
    ChildBand24.Enabled := False
  else
    ChildBand24.Height := 15 * (nIndex-1);
end;

procedure TADV700_PRINT_frm.ChildBand25BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Drawee
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV7001.FieldByName('DRAWEE'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_DRAWEE'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_DRAWEE'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7001.FieldByName('DRAWEE'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  QR_DRAWEE.Caption := qryADV7001DRAWEE.AsString;
  if Trim(QR_DRAWEE.Caption) <> '' then
  begin
    QR_DRAWEE.Enabled := True;
    ChildBand25.Height := 15 * nIndex;
  end
  else
  begin
    QR_DRAWEE4.Top := QR_DRAWEE3.Top;
    QR_DRAWEE3.Top := QR_DRAWEE2.Top;
    QR_DRAWEE2.Top := QR_DRAWEE1.Top;
    QR_DRAWEE1.Top := QR_DRAWEE.Top;
    ChildBand25.Height := 15 * (nIndex-1);
  end;

  if (Trim(QR_DRAWEE.Caption) = '') and (Trim(QR_DRAWEE1.Caption) = '') and (Trim(QR_DRAWEE2.Caption) = '') and
     (Trim(QR_DRAWEE3.Caption) = '') and (Trim(QR_DRAWEE4.Caption) = '') then
  begin
    ChildBand25.Enabled := False;
  end;        
end;

procedure TADV700_PRINT_frm.ChildBand26BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  QR_DRACCNT.Caption := qryADV7001DR_ACCNT.AsString;
  if Trim(QR_DRACCNT.Caption) = '' then
    ChildBand26.Enabled := False
  else
    ChildBand26.Height := ChildBand26.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand27BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Partial Shipments
  QR_PSHIP.Caption := qryADV7003PSHIP.AsString;
  if Trim(QR_PSHIP.Caption) = '' then
    ChildBand27.Enabled := False
  else
    ChildBand27.Height := ChildBand27.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand28BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Transhipments
  QR_TSHIP.Caption := qryADV7003TSHIP.AsString;
  if Trim(QR_TSHIP.Caption) = '' then
    ChildBand28.Enabled := False
  else
    ChildBand28.Height := ChildBand28.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand29BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Place of Taking in Charge/Dispatch from ···/Place of Receipt
  QR_LOADON.Caption := qryADV7003LOAD_ON.AsString;
  if Trim(QR_LOADON.Caption) = '' then
    ChildBand29.Enabled := False
  else
    ChildBand29.Height := ChildBand29.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand30BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Port of Loading/Airport of Departure :
  QR_SUNJUCKPORT.Caption := qryADV7003SUNJUCK_PORT.AsString;
  if Trim(QR_SUNJUCKPORT.Caption) = '' then
    ChildBand30.Enabled := False
  else
    ChildBand30.Height := ChildBand30.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand31BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Port of Discharge/Airport of Destination
  QR_DOCHACKPORT.Caption := qryADV7003DOCHACK_PORT.AsString;
  if Trim(QR_DOCHACKPORT.Caption) = '' then
    ChildBand31.Enabled := False
  else
    childBand31.Height := ChildBand31.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand32BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Place of Final Destination/For Transportation to ···/Place of Delivery :
  QR_FORTRAN.Caption := qryADV7003FOR_TRAN.AsString;
  if Trim(QR_FORTRAN.Caption) = '' then
    ChildBand32.Enabled := False
  else
    ChildBand32.Height := ChildBand32.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand33BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //Latest Date Of Shipment
  QR_LSTDATE.Caption := FormatDateTime('YYYY.MM.DD' , ConvertStr2Date(qryADV7001LST_DATE.AsString));
  if Trim(QR_LSTDATE.Caption) = '' then
    ChildBand33.Enabled := False
  else
    ChildBand33.Height := ChildBand33.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand34BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Shipment Period
  nIndex := 1;
  for i := 1 to 6 do
  begin
    if Trim(qryADV7001.FieldByName('SHIP_PD'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7001.FieldByName('SHIP_PD'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_SHIPPD1.Caption) = '') and (Trim(QR_SHIPPD2.Caption) = '') and (Trim(QR_SHIPPD3.Caption) = '') and
     (Trim(QR_SHIPPD4.Caption) = '') and (Trim(QR_SHIPPD5.Caption) = '') and (Trim(QR_SHIPPD6.Caption) = '') then
  begin
    ChildBand34.Enabled := False;
  end
  else
    ChildBand34.Height := 15 * nIndex;

end;

procedure TADV700_PRINT_frm.ChildBand35BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //Descript of Goods and /or Services
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryADV7002DESGOOD_1.AsString;
    ChildBand35.Height := ( 15 * memoLines.Count) + 20;
    QR_DESGOOD1.Height := ( 15 * memoLines.Count);

    QR_DESGOOD1.Lines.Clear;
    QR_DESGOOD1.Lines.Text := qryADV7002DESGOOD_1.AsString;
  finally
    memoLines.Free;
  end;

  if Trim(QR_DESGOOD1.Lines.Text) = '' then ChildBand35.Enabled := False;
end;

procedure TADV700_PRINT_frm.ChildBand36BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //Documents Required
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryADV7002DOCREQU_1.AsString;
    ChildBand36.Height := ( 15 * memoLines.Count) + 20;
    QR_DOCREQU1.Height := ( 15 * memoLines.Count);

    QR_DOCREQU1.Lines.Clear;
    QR_DOCREQU1.Lines.Text := qryADV7002DOCREQU_1.AsString;
  finally
    memoLines.Free;
  end;

  if Trim(QR_DOCREQU1.Lines.Text) = '' then ChildBand36.Enabled := False;
end;

procedure TADV700_PRINT_frm.ChildBand37BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //Additional Conditions
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryADV7002ADDCOND_1.AsString;
    ChildBand37.Height := ( 15 * memoLines.Count) + 20;
    QR_ADDCOND1.Height := ( 15 * memoLines.Count);

    QR_ADDCOND1.Lines.Clear;
    QR_ADDCOND1.Lines.Text := qryADV7002ADDCOND_1.AsString;
  finally
    memoLines.Free;
  end;

  if Trim(QR_ADDCOND1.Lines.Text) = '' then ChildBand37.Enabled := False;
end;

procedure TADV700_PRINT_frm.ChildBand38BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Charges
  nIndex := 1;
  for i := 1 to 6 do
  begin
    if Trim(qryADV7002.FieldByName('CHARGE'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_CHARGE'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_CHARGE'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7002.FieldByName('CHARGE'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_CHARGE1.Caption) = '') and (Trim(QR_CHARGE2.Caption) = '') and (Trim(QR_CHARGE3.Caption) = '') and
     (Trim(QR_CHARGE4.Caption) = '') and (Trim(QR_CHARGE5.Caption) = '') and  (Trim(QR_CHARGE6.Caption) = '') then
  begin
    ChildBand38.Enabled := False;
  end
  else
    ChildBand38.Height := 15 * (nIndex-1);
end;

procedure TADV700_PRINT_frm.ChildBand39BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Period for Presentation
  nIndex := 1;
  for i := 1 to 4 do
  begin
    if Trim(qryADV7002.FieldByName('PD_PRSNT'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_PDPRSNT'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_PDPRSNT'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7002.FieldByName('PD_PRSNT'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;
  if (Trim(QR_PDPRSNT1.Caption) = '') and (Trim(QR_PDPRSNT2.Caption) = '') and (Trim(QR_PDPRSNT3.Caption) = '') and (Trim(QR_PDPRSNT4.Caption) = '') then
    ChildBand39.Height := 18
  else
    ChildBand39.Height := 15 * (nIndex-1);
end;

procedure TADV700_PRINT_frm.ChildBand40BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  QR_CONFIRMM.Caption := qryADV7002CONFIRMM.AsString;
  if Trim(QR_CONFIRMM.Caption) = '' then
    ChildBand40.Enabled := False
  else
    ChildBand40.Height := ChildBand40.Tag
end;

procedure TADV700_PRINT_frm.ChildBand41BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Reimbursement Bank
  nIndex := 1;
  for i := 1 to 5 do
  begin
    if Trim(qryADV7002.FieldByName('REI_BANK'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_REIBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_REIBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7002.FieldByName('REI_BANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  QR_REIBANK.Caption := qryADV7002REI_BANK.AsString;
  if Trim(QR_REIBANK.Caption) <> '' then
  begin
    QR_REIBANK.Enabled := True;
    ChildBand41.Height := 15 * nIndex;
  end
  else
  begin
    QR_REIBANK5.Top := QR_REIBANK4.Top;
    QR_REIBANK4.Top := QR_REIBANK3.Top;
    QR_REIBANK3.Top := QR_REIBANK2.Top;
    QR_REIBANK2.top := QR_REIBANK1.Top;
    QR_REIBANK1.Top := QR_REIBANK.Top;
    ChildBand41.Height := 15 * (nIndex-1);
  end;

  if (Trim(QR_REIBANK.Caption) = '') and (Trim(QR_REIBANK1.Caption) = '') and (Trim(QR_REIBANK2.Caption) = '') and
     (Trim(QR_REIBANK3.Caption) = '') and (Trim(QR_REIBANK4.Caption) = '') and (Trim(QR_REIBANK5.Caption) = '') then
  begin
    ChildBand41.Enabled := False;
  end;
end;

procedure TADV700_PRINT_frm.ChildBand42BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QR_REIACCNT.Caption := qryADV7002REI_ACCNT.AsString;
  if Trim(QR_REIACCNT.Caption) = '' then
    ChildBand42.Enabled := False
  else
    ChildBand42.Height := ChildBand42.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand43BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //Instructions to the Paying/Accepting/Negotiating Bank
  try
    //TStringList생성
    memoLines := TStringList.Create;

    memoLines.Text := qryADV7002INSTRCT_1.AsString;
    ChildBand43.Height := ( 15 * memoLines.Count) + 20;
    QR_INSTRCT1.Height := ( 15 * memoLines.Count);

    QR_INSTRCT1.Lines.Clear;
    QR_INSTRCT1.Lines.Text := qryADV7002INSTRCT_1.AsString;
  finally
    memoLines.Free;
  end;

  if Trim(QR_INSTRCT1.Lines.Text) = '' then ChildBand43.Enabled := False;

end;

procedure TADV700_PRINT_frm.ChildBand44BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  ////"Advise Through" Bank        :
  nIndex := 1;
  for i := 1 to 5 do
  begin
    if Trim(qryADV7002.FieldByName('AVT_BANK'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_AVTBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_AVTBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7002.FieldByName('AVT_BANK'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  QR_AVTBANK.Caption := qryADV7002AVT_BANK.AsString;
  if Trim(QR_AVTBANK.Caption) <> '' then
  begin
    QR_AVTBANK.Enabled := True;
    ChildBand44.Height := 15 * nIndex;
  end
  else
  begin
    QR_AVTBANK5.Top := QR_AVTBANK4.Top;
    QR_AVTBANK4.Top := QR_AVTBANK3.Top;
    QR_AVTBANK3.Top := QR_AVTBANK2.Top;
    QR_AVTBANK2.top := QR_AVTBANK1.Top;
    QR_AVTBANK1.Top := QR_AVTBANK.Top;
    ChildBand44.Height := 15 * (nIndex-1);
  end;

  if (Trim(QR_AVTBANK.Caption) = '') and (Trim(QR_AVTBANK1.Caption) = '') and (Trim(QR_AVTBANK2.Caption) = '') and
     (Trim(QR_AVTBANK3.Caption) = '') and (Trim(QR_AVTBANK4.Caption) = '') and (Trim(QR_AVTBANK5.Caption) = '') then
  begin
    ChildBand44.Enabled := False;
  end;

end;

procedure TADV700_PRINT_frm.ChildBand45BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);

begin
  inherited;
    QR_AVTACCNT.Caption := qryADV7002AVT_ACCNT.AsString;
  if Trim(QR_AVTACCNT.Caption) = '' then
    ChildBand45.Enabled := False
  else
    ChildBand45.Height := ChildBand45.Tag;
end;

procedure TADV700_PRINT_frm.ChildBand46BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i  : Integer;
begin
  inherited;
  //Sender to Receive
  nIndex := 1;
  for i := 1 to 6 do
  begin
    if Trim(qryADV7002.FieldByName('SND_INFO'+IntToStr(i)).AsString) = '' then
    begin
      Continue;
    end;
    (FindComponent('QR_SNDINFO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
    (FindComponent('QR_SNDINFO'+IntToStr(nIndex)) as TQRLabel).Caption := qryADV7002.FieldByName('SND_INFO'+IntToStr(i)).AsString;
    Inc(nIndex);
  end;

  if (Trim(QR_SNDINFO1.Caption) = '') and (Trim(QR_SNDINFO2.Caption) = '') and (Trim(QR_SNDINFO3.Caption) = '') and
     (Trim(QR_SNDINFO4.Caption) = '') and (Trim(QR_SNDINFO5.Caption) = '') and (Trim(QR_SNDINFO6.Caption) = '') then
  begin
    ChildBand46.Enabled := False;
  end
  else
    ChildBand46.Height := 15 * nIndex;
end;

procedure TADV700_PRINT_frm.ChildBand47BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  ChildBand47.Height := ChildBand47.Tag;
  QR_EXNAME1.Caption := qryADV7002EX_NAME1.AsString;
  QR_EXNAME2.Caption := qryADV7002EX_NAME2.AsString;
  QR_EXNAME3.Caption := qryADV7002EX_NAME3.AsString;
  QR_EXADDR1.Caption := qryADV7002EX_ADDR1.AsString;
  QR_EXADDR2.Caption := qryADV7002EX_ADDR2.AsString;
end;

end.


