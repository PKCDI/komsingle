unit APP700_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QRCtrls, QuickRpt, ExtCtrls, DB;

type
  TAPP700_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QR_MAINTNO: TQRLabel;
    QR_FUNCTION: TQRLabel;
    QR_RESPONSE: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel8: TQRLabel;
    ChildBand1: TQRChildBand;
    QRImage1: TQRImage;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QR_DATEE: TQRLabel;
    QR_INMATHOD: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel13: TQRLabel;
    QR_ApBank: TQRLabel;
    QR_ApBank1: TQRLabel;
    QR_ApBank2: TQRLabel;
    QR_ApBank3: TQRLabel;
    QR_ApBank4: TQRLabel;
    QR_ApBank5: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel14: TQRLabel;
    QR_AdBank: TQRLabel;
    QR_AdBank1: TQRLabel;
    QR_AdBank2: TQRLabel;
    QR_AdBank3: TQRLabel;
    QR_AdBank4: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel15: TQRLabel;
    QR_ADPAY1: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel16: TQRLabel;
    QR_ADINFO1: TQRLabel;
    QR_ADINFO2: TQRLabel;
    QR_ADINFO3: TQRLabel;
    QR_ADINFO4: TQRLabel;
    QR_ADINFO5: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    ChildBand6: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel21: TQRLabel;
    QR_Doccd1: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel26: TQRLabel;
    QR_exDate: TQRLabel;
    QR_exPlace: TQRLabel;
    ChildBand7: TQRChildBand;
    ChildBand8: TQRChildBand;
    ChildBand9: TQRChildBand;
    ChildBand10: TQRChildBand;
    ChildBand11: TQRChildBand;
    ChildBand12: TQRChildBand;
    ChildBand13: TQRChildBand;
    ChildBand14: TQRChildBand;
    ChildBand15: TQRChildBand;
    ChildBand16: TQRChildBand;
    ChildBand17: TQRChildBand;
    ChildBand18: TQRChildBand;
    ChildBand19: TQRChildBand;
    ChildBand20: TQRChildBand;
    ChildBand21: TQRChildBand;
    ChildBand22: TQRChildBand;
    ChildBand23: TQRChildBand;
    ChildBand24: TQRChildBand;
    ChildBand25: TQRChildBand;
    ChildBand26: TQRChildBand;
    ChildBand27: TQRChildBand;
    ChildBand28: TQRChildBand;
    ChildBand29: TQRChildBand;
    ChildBand30: TQRChildBand;
    ChildBand31: TQRChildBand;
    ChildBand32: TQRChildBand;
    ChildBand33: TQRChildBand;
    ChildBand34: TQRChildBand;
    ChildBand35: TQRChildBand;
    ChildBand36: TQRChildBand;
    ChildBand37: TQRChildBand;
    ChildBand38: TQRChildBand;
    ChildBand39: TQRChildBand;
    ChildBand40: TQRChildBand;
    SummaryBand1: TQRBand;
    QRImage3: TQRImage;
    QRLabel124: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel129: TQRLabel;
    QR_EXADDR1: TQRLabel;
    QR_EXADDR2: TQRLabel;
    QR_EXNAME3: TQRLabel;
    QR_EXNAME2: TQRLabel;
    QR_EXNAME1: TQRLabel;
    QRShape2: TQRShape;
    QRLabel126: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel133: TQRLabel;
    ChildBand41: TQRChildBand;
    ChildBand42: TQRChildBand;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QR_Confirm: TQRLabel;
    QR_Confirm1: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel109: TQRLabel;
    QR_Period: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel122: TQRLabel;
    QR_ChargeName: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel106: TQRLabel;
    QR_acd2AE1: TQRMemo;
    QRLabel103: TQRLabel;
    QRLabel105: TQRLabel;
    QR_acd2AD: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel102: TQRLabel;
    QR_acd2AC: TQRLabel;
    QRLabel101: TQRLabel;
    QR_acd2AB: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QR_acd2AA1: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel98: TQRLabel;
    QR_Doc2AA_1: TQRMemo;
    QRLabel80: TQRLabel;
    QRLabel78: TQRLabel;
    QR_DOC861: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QR_DOC271_1: TQRLabel;
    QRLabel97: TQRLabel;
    QR_DOC530_2: TQRLabel;
    QR_DOC530_1: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel90: TQRLabel;
    QR_DOC760_4: TQRLabel;
    QRLabel91: TQRLabel;
    QR_DOC760_3_1: TQRLabel;
    QRLabel89: TQRLabel;
    QR_DOC760_2: TQRLabel;
    QR_DOC760_1: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel70: TQRLabel;
    QR_DOC740_3_1: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel86: TQRLabel;
    QR_DOC740_2: TQRLabel;
    QR_DOC740_1: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel84: TQRLabel;
    QR_DOC740_4: TQRLabel;
    QRLabel83: TQRLabel;
    QR_DOC705_4: TQRLabel;
    QR_DOC705_2: TQRLabel;
    QRLabel81: TQRLabel;
    QR_DOC705_1: TQRLabel;
    QRLabel82: TQRLabel;
    QR_DOC705_3_1: TQRLabel;
    QR_GUBUN2: TQRLabel;
    QR_GUBUN1: TQRLabel;
    QR_GUBUN: TQRLabel;
    QRLabel79: TQRLabel;
    QR_DOC380_1: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel73: TQRLabel;
    QR_ORIGIN_M: TQRLabel;
    QR_ORIGIN: TQRLabel;
    QRLabel74: TQRLabel;
    QR_PLTERM: TQRLabel;
    QRLabel72: TQRLabel;
    QR_TERMPR_M: TQRLabel;
    QR_TERMPR: TQRLabel;
    QRLabel71: TQRLabel;
    QR_DESGOOD1: TQRMemo;
    QRLabel63: TQRLabel;
    QRLabel62: TQRLabel;
    QR_SHIPPD6: TQRLabel;
    QR_SHIPPD5: TQRLabel;
    QR_SHIPPD4: TQRLabel;
    QR_SHIPPD3: TQRLabel;
    QR_SHIPPD2: TQRLabel;
    QR_SHIPPD1: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QR_LSTDATE: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel64: TQRLabel;
    QR_FORTRAN: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel65: TQRLabel;
    QR_LOADON: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel57: TQRLabel;
    QR_DOCHACKPORT: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QR_SUNJUCKPORT: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QR_TSHIP: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel51: TQRLabel;
    QR_PSHIP: TQRLabel;
    QR_DRAFT3: TQRLabel;
    QR_DRAFT2: TQRLabel;
    QR_DRAFT1: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel45: TQRLabel;
    QR_AACV4: TQRLabel;
    QR_AACV3: TQRLabel;
    QR_AACV2: TQRLabel;
    QR_AACV1: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QR_CDMAXNAME: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel39: TQRLabel;
    QR_CDPERP: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QR_CDCUR: TQRLabel;
    QR_CDAMT: TQRLabel;
    QR_BENEFC5: TQRLabel;
    QR_BENEFC4: TQRLabel;
    QR_BENEFC2: TQRLabel;
    QR_BENEFC3: TQRLabel;
    QR_BENEFC1: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QR_APPLIC5: TQRLabel;
    QR_APPLIC4: TQRLabel;
    QR_APPLIC2: TQRLabel;
    QR_APPLIC3: TQRLabel;
    QR_APPLIC1: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
     procedure PrintDocument(Fields: TFields; uPreview: Boolean = True); override;
  end;

var
  APP700_PRINT_frm: TAPP700_PRINT_frm;

implementation

uses Commonlib;

{$R *.dfm}

{ TAPP700_PRINT_frm }

procedure TAPP700_PRINT_frm.PrintDocument(Fields: TFields;
  uPreview: Boolean);
var
  memoLines : TStringList;
  nIndex,i : Integer;
  messageVal : String;
begin
  inherited;
  FFields := Fields;
  FPreview := uPreview;

  with Fields do
  begin
    //문서/전자문서번호
    QR_MAINTNO.Caption := FieldByName('MAINT_NO').AsString;


    //전자문서 기능표시
    messageVal := getValueFromField('MESSAGE1');
    case StrToInt(messageVal) of
     1 : QR_FUNCTION.Caption := 'Cancel';
     2 : QR_FUNCTION.Caption := 'Delete';
     4 : QR_FUNCTION.Caption := 'Change';
     6 : QR_FUNCTION.Caption := 'Confirmation';
     7 : QR_FUNCTION.Caption := 'Duplicate';
     9 : QR_FUNCTION.Caption := 'Original';
    else
      QR_FUNCTION.Caption := '';
    end;


    //전자문서 응답유형
    if getValueFromField('MESSAGE2') = 'AB' then
      QR_RESPONSE.Caption := 'Message Acknowledgment'
    else if getValueFromField('MESSAGE2') = 'AP' then
      QR_RESPONSE.Caption := 'Accepted'
    else if getValueFromField('MESSAGE2') = 'NA' then
      QR_RESPONSE.Caption := 'No acknowledgement needed'
    else if getValueFromField('MESSAGE2') = 'RE' then
      QR_RESPONSE.Caption := 'Rjected'
    else
      QR_RESPONSE.Caption := '';


  //----------------------------------------------------------------------------
  //일반정보
  //----------------------------------------------------------------------------

    //---------ChildBand1------------------------------------------------------
      ChildBand1.Height := ChildBand1.Tag;
      //개설신청일자
      QR_DATEE.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('DATEE').AsString));

      //개설방법
      QR_INMATHOD.Caption := getValueFromField('IN_MATHOD')+ ' ' + getValueFromField('mathod_Name');
    //--------------------------------------------------------------------------

    //---------ChildBand2-------------------------------------------------------
      //개설(전문발신)은행
      QR_ApBank.Caption := getValueFromField('AP_BANK');
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('AP_BANK'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_ApBank'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_ApBank'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_ApBank'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AP_BANK'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if Trim(QR_ApBank1.Caption) = '' then
      begin
        ChildBand2.Enabled := False;
      end
      else
        ChildBand2.Height := (13 * nIndex)+ 20;
    //--------------------------------------------------------------------------

    //---------ChildBand3-------------------------------------------------------
      QR_AdBank.Caption := getValueFromField('AD_BANK');
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if Trim(FieldByName('AD_BANK'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_AdBank'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_AdBank'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_AdBank'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AD_BANK'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if Trim(QR_AdBank1.Caption) = '' then
      begin
        ChildBand3.Enabled := False;
      end
      else
        ChildBand3.Height := (13 * nIndex)+ 20;
   //---------------------------------------------------------------------------

   //---------ChildBand4--------------------------------------------------------
    ChildBand4.Height := ChildBand4.Tag;
    QR_ADPAY1.Caption := getValueFromField('pay_Name');
    //기한부신용장 신용공여주체
      if Trim(QR_ADPAY1.Caption) = '' then
      begin
        ChildBand4.Enabled := False;
      end;
   //---------------------------------------------------------------------------

    //---------ChildBand5--------------------------------------------------------
    //기타정보
    nIndex := 1;
    for i := 1 to 5 do
    begin
      if Trim(FieldByName('AD_INFO'+IntToStr(i)).AsString) = '' then
      begin
        (FindComponent('QR_ADINFO'+IntToStr(i)) as TQRLabel).Caption := '';
        Continue;
      end;

      (FindComponent('QR_ADINFO'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
      (FindComponent('QR_ADINFO'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AD_INFO'+IntToStr(i)).AsString;
      Inc(nIndex);
    end;

    if Trim(QR_ADINFO1.Caption) = '' then
    begin
      ChildBand5.Enabled := False;
    end
    else
      ChildBand5.Height := (13 * nIndex)+82;
   //----------------------------------------------------------------------------

   //---------ChildBand6-------------------------------------------------------
      ChildBand6.Height := ChildBand6.Tag;
      //Form of Documentary Credit
      QR_Doccd1.Caption := getValueFromField('DOC_NAME');
      //Date and Place of Expiry
      QR_exDate.Caption := '(date) ' +  FormatDateTime('YYYY.MM.DD',ConvertStr2Date(FieldByName('EX_DATE').AsString));
      QR_exPlace.Caption := '(place)' + getValueFromField('EX_PLACE');

      if (Trim(QR_exDate.Caption) = '') or (Trim(QR_exPlace.Caption) = '') then
      begin
        ChildBand6.Enabled := False;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand7-------------------------------------------------------
      //Applicant   
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('APPLIC'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_APPLIC'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

         (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_APPLIC'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('APPLIC'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if Trim(QR_APPLIC1.Caption) = '' then
      begin
        ChildBand7.Enabled := False;
      end
      else
        ChildBand7.Height := (13 * nIndex)+5;
    //--------------------------------------------------------------------------

    //---------ChildBand8------------------------------------------------------
      //Beneficiary
      nIndex := 1;
      for i := 1 to 5 do
      begin
        if Trim(FieldByName('BENEFC'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_BENEFC'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

         (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_BENEFC'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('BENEFC'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if Trim(QR_BENEFC1.Caption) = '' then
      begin
        ChildBand8.Enabled := False;
      end
      else
        ChildBand8.Height := (14 * nIndex);
    //--------------------------------------------------------------------------

    //---------ChildBand9------------------------------------------------------
      ChildBand9.Height := ChildBand9.Tag;
      //Currency Code,Amount
      QR_CDCUR.Caption := getValueFromField('CD_CUR');
      QR_CDAMT.Caption := FormatFloat('#,##0.###', FieldByName('CD_AMT').AsFloat);

    //--------------------------------------------------------------------------

    //---------ChildBand10------------------------------------------------------
      ChildBand10.Height := ChildBand10.Tag;
      //Percentage Credit Amount
      QR_CDPERP.Caption := '(±) '+ getValueFromField('CD_PERP') +'/' + getValueFromField('CD_PERM') + ' %';

      if (FieldByName('CD_PERP').AsInteger = 0) and (FieldByName('CD_PERM').AsInteger = 0) then
      begin
        ChildBand10.Enabled := False;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand11------------------------------------------------------
      ChildBand11.Height := ChildBand11.Tag;
      //Maximum Credit Amount
      QR_CDMAXNAME.Caption := getValueFromField('CDMAX_Name');

      if Trim(QR_CDMAXNAME.Caption) = '' then
      begin
        ChildBand11.Enabled := False;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand12------------------------------------------------------
      //Additional Amounts Covered
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if Trim(FieldByName('AA_CV'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_AACV'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

         (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_AACV'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('AA_CV'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if Trim(QR_AACV1.Caption) = '' then
      begin
        ChildBand12.Enabled := False;
      end
      else
        ChildBand12.Height := (13 * nIndex)+5;
    //--------------------------------------------------------------------------


    //---------ChildBand13------------------------------------------------------
      //Drafts at ....
      nIndex := 1;
      for i := 1 to 3 do
      begin
        if Trim(FieldByName('DRAFT'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_DRAFT'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

         (FindComponent('QR_DRAFT'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_DRAFT'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('DRAFT'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if Trim(QR_DRAFT1.Caption) = '' then
      begin
        ChildBand13.Enabled := False;
      end
      else
        ChildBand13.Height := (13 * nIndex)+5;
    //--------------------------------------------------------------------------

    //---------ChildBand14------------------------------------------------------
      ChildBand14.Height := ChildBand14.Tag;
      //Partal Shipment
      QR_PSHIP.Caption := getValueFromField('pship_Name');
      //Transhipment
      QR_TSHIP.Caption := getValueFromField('tship_Name');
    //--------------------------------------------------------------------------


    //---------ChildBand15------------------------------------------------------
      ChildBand15.Height := ChildBand15.Tag;
      //Port of Loading / Airport of Departure
      QR_SUNJUCKPORT.Caption := getValueFromField('SUNJUCK_PORT');
      if Trim(QR_SUNJUCKPORT.Caption) = '' then
      begin
        ChildBand15.Enabled := False;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand16------------------------------------------------------
      ChildBand16.Height := ChildBand16.Tag;
      //Port of Discharge/Airport of Destination
      QR_DOCHACKPORT.Caption := getValueFromField('DOCHACK_PORT');
      if Trim(QR_DOCHACKPORT.Caption) = '' then
      begin
        ChildBand16.Enabled := False;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand17------------------------------------------------------
      ChildBand17.Height := ChildBand17.Tag;
      //Place of Taking in Charge/Dispatch from ···/Place of Receipt
      QR_LOADON.Caption := getValueFromField('LOAD_ON');
      if Trim(QR_LOADON.Caption) = '' then
      begin
        ChildBand17.Enabled := False;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand18------------------------------------------------------
      ChildBand18.Height := ChildBand18.Tag;
      //Place of Final Destination/For Transportation to ···/Place of Delivery
      QR_FORTRAN.Caption := getValueFromField('FOR_TRAN');
      if Trim(QR_FORTRAN.Caption) = '' then
      begin
        ChildBand18.Enabled := False;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand19------------------------------------------------------
      ChildBand19.Height := ChildBand19.Tag;
      //Latest Date of Shipment
      QR_LSTDATE.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(getValueFromField('LST_DATE')));
      if Trim(QR_LSTDATE.Caption) = '' then
      begin
        ChildBand19.Enabled :=  False;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand20------------------------------------------------------
      //Shipment Period
      nIndex := 1;
      for i := 1 to 6 do
      begin
        if Trim(FieldByName('SHIP_PD'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_SHIPPD'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

         (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
         (FindComponent('QR_SHIPPD'+IntToStr(nIndex)) as TQRLabel).Caption := FieldByName('SHIP_PD'+IntToStr(i)).AsString;
         Inc(nIndex);
      end;

      if Trim(QR_SHIPPD1.Caption) = '' then
      begin
        ChildBand20.Enabled := False;
      end
      else
        ChildBand20.Height := (13 * nIndex)+5;
    //--------------------------------------------------------------------------

    //---------ChildBand21------------------------------------------------------
      try
        //TStringList생성
        memoLines := TStringList.Create;
        //memoLines에 DESGOOD_1 대입
        memoLines.Text := FieldByName('DESGOOD_1').AsString;

        ChildBand21.Height := ( 13 * memoLines.Count) + 26;
        QR_DESGOOD1.Height := ( 13 * memoLines.Count);

        //Description of Goods and/or Services :
        QR_DESGOOD1.Lines.Clear;
        getValueMemo(QR_DESGOOD1,'DESGOOD_1');
        
        if Trim(QR_DESGOOD1.Caption) = '' then
        begin
          ChildBand21.Enabled := False;
        end;
      finally
        memoLines.Free;
      end;
    //--------------------------------------------------------------------------

    //---------ChildBand22------------------------------------------------------
      ChildBand22.Height := ChildBand22.Tag;
      //TERMS OF PRICE
      QR_TERMPR.Caption := getValueFromField('TERM_PR');
      QR_TERMPR_M.Caption := getValueFromField('TERM_PR_M');

      if (Trim(QR_TERMPR.Caption) = '') or (Trim(QR_TERMPR_M.Caption) = '') then
      begin
        ChildBand22.Enabled :=  False;
      end;
    //--------------------------------------------------------------------------

    //---------ChildBand23------------------------------------------------------
      ChildBand23.Height := ChildBand23.Tag;
      //PLACE OF TERMS OF PRICE
      QR_PLTERM.Caption := getValueFromField('PL_TERM');
      if Trim(QR_PLTERM.Caption) = '' then
      begin
        ChildBand23.Enabled :=  False;
      end;
    //--------------------------------------------------------------------------


    //---------ChildBand24------------------------------------------------------
      ChildBand24.Height := ChildBand24.Tag;
      //COUNTRY OF ORIGIN
      QR_ORIGIN.Caption := getValueFromField('ORIGIN');
      QR_ORIGIN_M.Caption := getValueFromField('ORIGIN_M');
      if(Trim(QR_ORIGIN.Caption) = '') or (Trim(QR_ORIGIN_M.Caption) = '') then
      begin
        ChildBand24.Enabled :=  False;
      end;
    //--------------------------------------------------------------------------

    //Documents Required--------------------------------------------------------
      //---------ChildBand25--------------------------------------
        ChildBand25.Height := ChildBand25.Tag;
      //----------------------------------------------------------

      //---------ChildBand26--------------------------------------
        ChildBand26.Height := ChildBand26.Tag;
        // 380
        QR_DOC380_1.Caption := getValueFromField('DOC_380_1');
        if (FieldByName('DOC_380').AsBoolean = False) or (Trim(FieldByName('DOC_380').AsString) = '')then
        begin
          ChildBand26.Enabled := False;
        end;
      //-----------------------------------------------------------

      //---------ChildBand27---------------------------------------
        ChildBand27.Height := ChildBand27.Tag;
        //705
        QR_GUBUN.Caption := getValueFromField('DOC_705_GUBUN');
          if QR_GUBUN.Caption = '705' then
          begin
            QR_GUBUN1.Caption := 'FULL SET';
            QR_GUBUN2.Caption := 'THE ORDER OF';
          end
          else if QR_GUBUN.Caption = '706' then
          begin
            QR_GUBUN1.Caption := 'FULL SET';
            QR_GUBUN2.Caption := ''
          end
          else if QR_GUBUN.Caption = '707' then
          begin
            QR_GUBUN1.Caption := '  COPY';
            QR_GUBUN2.Caption := ''
          end
          else if QR_GUBUN.Caption = '717' then
          begin
            QR_GUBUN1.Caption := ' 1/3 SET';
            QR_GUBUN2.Caption := ''
          end
          else if QR_GUBUN.Caption = '718' then
          begin
            QR_GUBUN1.Caption := ' 2/3 SET';
            QR_GUBUN2.Caption := ''
          end
          else
          begin
            QR_GUBUN1.Caption := '';
            QR_GUBUN2.Caption := '';
          end;

          QR_DOC705_1.Caption := getValueFromField('DOC_705_1');
          QR_DOC705_2.Caption := getValueFromField('DOC_705_2');
          QR_DOC705_3_1.Caption := getValueFromField('doc705_Name');
          QR_DOC705_4.Caption := getValueFromField('doc_705_4');

        if (FieldByName('DOC_705').AsBoolean = False) or (Trim(FieldByName('DOC_705').AsString) = '')then
        begin
          ChildBand27.Enabled := False;
        end;
      //------------------------------------------------------------------------

      //---------ChildBand28----------------------------------------------------
        ChildBand28.Height := ChildBand28.Tag;
        //740
        QR_DOC740_1.Caption := getValueFromField('DOC_740_1');
        QR_DOC740_2.Caption := getValueFromField('DOC_740_2');
        QR_DOC740_3_1.Caption := getValueFromField('doc740_Name');
        QR_DOC740_4.Caption := getValueFromField('DOC_740_4');
        if (FieldByName('DOC_740').AsBoolean = False) or (Trim(FieldByName('DOC_740').AsString) = '') then
        begin
          ChildBand28.Enabled := False;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand29----------------------------------------------------
        ChildBand29.Height := ChildBand29.Tag;
        //760
        QR_DOC760_1.Caption := getValueFromField('DOC_760_1');
        QR_DOC760_2.Caption := getValueFromField('DOC_760_2');
        QR_DOC760_3_1.Caption := getValueFromField('doc760_Name');
        QR_DOC760_4.Caption := getValueFromField('DOC_760_4');
        if (FieldByName('DOC_760').AsBoolean = False) or (Trim(FieldByName('DOC_760').AsString) = '') then
        begin
          ChildBand29.Enabled := False;
        end;
        //----------------------------------------------------------------------

      //---------ChildBand30----------------------------------------------------
        ChildBand30.Height := ChildBand30.Tag;
        //530
        QR_DOC530_1.Caption := getValueFromField('DOC_530_1');
        QR_DOC530_2.Caption := getValueFromField('DOC_530_2');
        if (FieldByName('DOC_530').AsBoolean = False) or (Trim(FieldByName('DOC_530').AsString) = '') then
        begin
          ChildBand30.Enabled := False;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand31----------------------------------------------------
        ChildBand31.Height := ChildBand31.Tag;
        //PACKING LIST IN
        QR_DOC271_1.Caption := getValueFromField('DOC_271_1');
        if Trim(QR_DOC271_1.Caption) = '' then
        begin
          ChildBand31.Enabled :=  False;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand32----------------------------------------------------
        ChildBand32.Height := ChildBand32.Tag;
        //861
        QR_DOC861.Caption := 'CRETIFICATE OF ORIGIN';
        if (FieldByName('DOC_861').AsBoolean = False) or (Trim(FieldByName('DOC_861').AsString) = '') then
        begin
          ChildBand32.Enabled := False;
        end;
      //------------------------------------------------------------------------


      //---------ChildBand33----------------------------------------------------
        try
          //TStringList생성
          memoLines := TStringList.Create;
          //memoLines에 DOC_2AA_1 대입
          memoLines.Text := FieldByName('DOC_2AA_1').AsString;

          ChildBand33.Height := ( 13 * memoLines.Count) + 26;
          QR_Doc2AA_1.Height := ( 13 * memoLines.Count);

          //OTHER DOCUMENT(S)(if any)
          QR_Doc2AA_1.Lines.Clear;
          getValueMemo(QR_Doc2AA_1,'DOC_2AA_1');

          if (FieldByName('DOC_2AA').AsBoolean = False) or (Trim(FieldByName('DOC_2AA').AsString) = '') then
          begin
            ChildBand33.Enabled := False;
          end;
        finally
          memoLines.Free;
        end;
      //------------------------------------------------------------------------

     //Additional conditions..--------------------------------------------------
        //---------ChildBand34--------------------------------------------------
          if (FieldByName('ACD_2AA').AsBoolean = True) or (FieldByName('ACD_2AB').AsBoolean = True) or
              (FieldByName('ACD_2AC').AsBoolean = True) or (FieldByName('ACD_2AD').AsBoolean = True) or
              (FieldByName('ACD_2AE').AsBoolean = True )
          then  
          begin
            ChildBand34.Height := ChildBand34.Tag;
          end
          else
          begin
            ChildBand34.Enabled := False;
          end;
        //----------------------------------------------------------------------


        //---------ChildBand35--------------------------------------------------
          ChildBand35.Height := ChildBand35.Tag;
          //Shipment By
          QR_acd2AA1.Caption := getValueFromField('ACD_2AA_1');
          if Trim(QR_acd2AA1.Caption) = '' then
          begin
            ChildBand35.Enabled := False;
          end;
        //------------------------------------------------------------------------

        //---------ChildBand36----------------------------------------------------
          ChildBand36.Height := ChildBand36.Tag;
          //ACCEPTANCE COMMISSION DISCOUNT
          // check_acd2AB의 값이 True 이면 아래 문자열 출력
           QR_acd2AB.Caption := ' ACCEPTANCE COMMISSION DISCOUNT CHARGES ARE FOR BUYER`S ACCOUNT ';
           if FieldByName('ACD_2AB').AsBoolean = False then
           begin
             ChildBand36.Enabled := False;
           end;
        //------------------------------------------------------------------------


        //---------ChildBand37----------------------------------------------------
          ChildBand37.Height := ChildBand37.Tag;
            //ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER
            // check_acd2AC의 값이 True 이면 아래 문자열 출력
            QR_acd2AC.Caption := ' ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER ';
            if FieldByName('ACD_2AC').AsBoolean = False then
            begin
              ChildBand37.Enabled := False;
            end;
        //------------------------------------------------------------------------

        //---------ChildBand38----------------------------------------------------
          ChildBand38.Height := ChildBand38.Tag;
          //LATE PRESENTATION B/L ACCEPTABLE
          // check_acd2AD의 값이 True 이면 아래 문자열 출력
          QR_acd2AD.Caption := ' LATE PRESENTATION B/L ACCEPTABLE ';
          if FieldByName('ACD_2AD').AsBoolean = False then
          begin
            ChildBand38.Enabled := False;
          end;
        //------------------------------------------------------------------------


        //---------ChildBand39----------------------------------------------------
          try
            memoLines := TStringList.Create;
            memoLines.Text := FieldByName('ACD_2AE_1').AsString;

             ChildBand39.Height := ( 13 * memoLines.Count) + 26;
             QR_acd2AE1.Height := ( 13 * memoLines.Count);

            //OTHER CONDITION(S)    ( if any )
            // check_acd2AE의 값이 True 이면 아래 문자열 출력, 메모 출력
              QRLabel103.Caption := ' OTHER CONDITION(S)    ( if any ) ';
              QR_acd2AE1.Lines.Clear;
              getValueMemo(QR_acd2AE1,'ACD_2AE_1');

              if (FieldByName('ACD_2AE').AsBoolean = False) or (Trim(FieldByName('ACD_2AE').AsString) = '') then
              begin
                ChildBand39.Enabled := False;
              end;
          finally
            memoLines.Free
          end;
        //----------------------------------------------------------------------

        //---------ChildBand40--------------------------------------------------
          //Charges
          QR_ChargeName.Caption := getValueFromField('CHARGE_Name');

          if Trim(QR_ChargeName.Caption) = '' then ChildBand40.Enabled := False;
        //----------------------------------------------------------------------

        //---------ChildBand41--------------------------------------------------
          ChildBand41.Height := ChildBand41.Tag;
          //Period for Pressentation
          QR_Period.Caption := getValueFromField('PERIOD');

          if Trim(QR_Period.Caption) = '' then ChildBand41.Enabled := False;
        //----------------------------------------------------------------------


        //---------ChildBand42--------------------------------------------------
          ChildBand42.Height := ChildBand42.Tag;
          //Confirmation Instructions
          QR_Confirm.Caption := getValueFromField('CONFIRMM');
          QR_Confirm1.Caption := getValueFromField('CONFIRM_Name');

          if Trim(QR_Confirm.Caption) = '' then  ChildBand42.Enabled := False;
        //----------------------------------------------------------------------


     //-------------SummaryBand1-------------------------------------------------
      //신청업체
      QR_EXNAME1.Caption := getValueFromField('EX_NAME1');
      QR_EXNAME2.Caption := getValueFromField('EX_NAME2');
      QR_EXNAME3.Caption := getValueFromField('EX_NAME3');
      QR_EXADDR1.Caption := getValueFromField('EX_ADDR1');
      QR_EXADDR2.Caption := getValueFromField('EX_ADDR2');
    
  end;

  RunPrint;

end;

end.
