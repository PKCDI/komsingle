unit QR_ADV707_DATA2_PRN;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB;

type
  TQR_ADV707_DATA2_PRN_frm = class(TQuickRep)
    TitleBand1: TQRBand;
    CHILD_19: TQRChildBand;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QR_71D_4: TQRLabel;
    QR_71D_5: TQRLabel;
    QR_71D_6: TQRLabel;
    QR_71D_1: TQRLabel;
    QR_71D_2: TQRLabel;
    QR_71D_3: TQRLabel;
    qryList: TADOQuery;
    qryListMAINT_NO_TXT: TStringField;
    qryListMAINT_NO: TStringField;
    qryListAMD_NO: TIntegerField;
    qryListAMD_NO1: TIntegerField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAMD_DATE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListAPP_NO: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_BANK5: TStringField;
    qryListADDINFO: TStringField;
    qryListADDINFO_1: TMemoField;
    qryListCD_NO: TStringField;
    qryListRCV_REF: TStringField;
    qryListISS_BANK: TStringField;
    qryListISS_BANK1: TStringField;
    qryListISS_BANK2: TStringField;
    qryListISS_BANK3: TStringField;
    qryListISS_BANK4: TStringField;
    qryListISS_BANK5: TStringField;
    qryListISS_ACCNT: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListBENEFC6: TStringField;
    qryListIBANK_REF: TStringField;
    qryListPRNO: TIntegerField;
    qryListGOODS_DESC: TStringField;
    qryListGOODS_DESC_1: TMemoField;
    qryListDOC_DESC: TStringField;
    qryListDOC_DESC_1: TMemoField;
    qryListADD_DESC: TStringField;
    qryListADD_DESC_1: TMemoField;
    qryListSPECIAL_DESC: TStringField;
    qryListSPECIAL_DESC_1: TMemoField;
    qryListINST_DESC: TStringField;
    qryListINST_DESC_1: TMemoField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListSR_INFO1: TStringField;
    qryListSR_INFO2: TStringField;
    qryListSR_INFO3: TStringField;
    qryListSR_INFO4: TStringField;
    qryListSR_INFO5: TStringField;
    qryListSR_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListPURP_MSG: TStringField;
    qryListPURP_MSG_NM: TStringField;
    qryListCAN_REQ: TStringField;
    qryListCON_INST: TStringField;
    qryListDOC_TYPE: TStringField;
    qryListCHARGE: TStringField;
    qryListCHARGE_NUM1: TStringField;
    qryListCHARGE_NUM2: TStringField;
    qryListCHARGE_NUM3: TStringField;
    qryListCHARGE_NUM4: TStringField;
    qryListCHARGE_NUM5: TStringField;
    qryListCHARGE_NUM6: TStringField;
    qryListAMD_CHARGE: TStringField;
    qryListAMD_CHARGE_NUM1: TStringField;
    qryListAMD_CHARGE_NUM2: TStringField;
    qryListAMD_CHARGE_NUM3: TStringField;
    qryListAMD_CHARGE_NUM4: TStringField;
    qryListAMD_CHARGE_NUM5: TStringField;
    qryListAMD_CHARGE_NUM6: TStringField;
    qryListTSHIP: TStringField;
    qryListPSHIP: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListAPPLICABLE_RULES_1: TStringField;
    qryListAPPLICABLE_RULES_2: TStringField;
    qryListAVAIL: TStringField;
    qryListAVAIL1: TStringField;
    qryListAVAIL2: TStringField;
    qryListAVAIL3: TStringField;
    qryListAVAIL4: TStringField;
    qryListAV_ACCNT: TStringField;
    qryListDRAWEE: TStringField;
    qryListDRAWEE1: TStringField;
    qryListDRAWEE2: TStringField;
    qryListDRAWEE3: TStringField;
    qryListDRAWEE4: TStringField;
    qryListCO_BANK: TStringField;
    qryListCO_BANK1: TStringField;
    qryListCO_BANK2: TStringField;
    qryListCO_BANK3: TStringField;
    qryListCO_BANK4: TStringField;
    qryListREI_BANK: TStringField;
    qryListREI_BANK1: TStringField;
    qryListREI_BANK2: TStringField;
    qryListREI_BANK3: TStringField;
    qryListREI_BANK4: TStringField;
    qryListAVT_BANK: TStringField;
    qryListAVT_BANK1: TStringField;
    qryListAVT_BANK2: TStringField;
    qryListAVT_BANK3: TStringField;
    qryListAVT_BANK4: TStringField;
    qryListAMD_APPLIC1: TStringField;
    qryListAMD_APPLIC2: TStringField;
    qryListAMD_APPLIC3: TStringField;
    qryListAMD_APPLIC4: TStringField;
    qryListPERIOD: TIntegerField;
    qryListPERIOD_TXT: TStringField;
    qryListNBANK_ISS: TStringField;
    qryListNBANK_ISS1: TStringField;
    qryListNBANK_ISS2: TStringField;
    qryListNBANK_ISS3: TStringField;
    qryListNBANK_ISS4: TStringField;
    CHILD_20: TQRChildBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    CHILD_21: TQRChildBand;
    QRLabel3: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    CHILD_22: TQRChildBand;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel15: TQRLabel;
    CHILD_23: TQRChildBand;
    QRLabel16: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    CHILD_24: TQRChildBand;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    CHILD_25: TQRChildBand;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRMemo1: TQRMemo;
    CHILD_26: TQRChildBand;
    QRLabel26: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    CHILD_27: TQRChildBand;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QR_RECV4: TQRLabel;
    QR_RECV5: TQRLabel;
    QR_RECV3: TQRLabel;
    QR_RECV1: TQRLabel;
    QR_RECV2: TQRLabel;
    QR_RECV6: TQRLabel;
    CHILD_28: TQRChildBand;
    QRImage2: TQRImage;
    QRLabel36: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRShape6: TQRShape;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    procedure CHILD_19BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure CHILD_28BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private

  public
    FMAINT_NO : String;
    FAMD_NO : Integer;
  end;

var
  QR_ADV707_DATA2_PRN_frm: TQR_ADV707_DATA2_PRN_frm;

implementation

{$R *.DFM}

procedure TQR_ADV707_DATA2_PRN_frm.CHILD_19BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  sSwiftCode, sContent, sValue : string;
begin
  //Applicable Rules
  Sender.Height := Sender.Tag;
  sSwiftCode := Sender.Hint;

  if sSwiftCode = '71D' then
  begin
    PrintBand := not qryListCHARGE_NUM1.IsNull;
    QR_71D_1.Caption := qryListCHARGE_NUM1.AsString;
    QR_71D_2.Caption := qryListCHARGE_NUM2.AsString;
    QR_71D_3.Caption := qryListCHARGE_NUM3.AsString;
    QR_71D_4.Caption := qryListCHARGE_NUM4.AsString;
    QR_71D_5.Caption := qryListCHARGE_NUM5.AsString;
    QR_71D_6.Caption := qryListCHARGE_NUM6.AsString;
  end
  else
  if sSwiftCode = '71N' then
  begin
    PrintBand := not qryListAMD_CHARGE_NUM1.IsNull;
    QRLabel7.Caption := qryListAMD_CHARGE_NUM1.AsString;
    QRLabel8.Caption := qryListAMD_CHARGE_NUM2.AsString;
    QRLabel9.Caption := qryListAMD_CHARGE_NUM3.AsString;
    QRLabel4.Caption := qryListAMD_CHARGE_NUM4.AsString;
    QRLabel5.Caption := qryListAMD_CHARGE_NUM5.AsString;
    QRLabel6.Caption := qryListAMD_CHARGE_NUM6.AsString;
  end
  else
  if sSwiftCode = '48' Then
  begin
    PrintBand := not qryListPERIOD.IsNull;
    QRLabel14.Caption := qryListPERIOD.AsString+'/'+qryListPERIOD_TXT.AsString;
  end
  else
  if sSwiftCode = '49' then
  begin
    PrintBand := not qryListCON_INST.IsNull;
    QRLabel13.Caption := qryListCON_INST.AsString;
  end
  else
  if sSwiftCode = '58a' then
  begin
    PrintBand := not qryListCO_BANK.IsNull;
    QRLabel20.Caption := qryListCO_BANK.AsString;
  end
  else
  if sSwiftCode = '53a' then
  begin
    PrintBand := not qryListREI_BANK.IsNull;
    QRLabel20.Caption := qryListREI_BANK.AsString;
  end
  else
  if sSwiftCode = '78' then
  begin
    PrintBand := not qryListINST_DESC_1.IsNull;
    QRMemo1.Caption := qryListINST_DESC_1.AsString;
  end
  else
  if sSwiftCode = '57a' then
  begin
    PrintBand := not qryListAVT_BANK.IsNull;
    QRMemo1.Caption := qryListAVT_BANK.AsString;
  end
  else
  if sSwiftCode = '72Z' then
  begin
    PrintBand := not qryListSR_INFO1.IsNull;
    QR_RECV1.Caption := qryListSR_INFO1.AsString;
    QR_RECV2.Caption := qryListSR_INFO2.AsString;
    QR_RECV3.Caption := qryListSR_INFO3.AsString;
    QR_RECV4.Caption := qryListSR_INFO4.AsString;
    QR_RECV5.Caption := qryListSR_INFO5.AsString;
    QR_RECV6.Caption := qryListSR_INFO6.AsString;
  end;

end;

procedure TQR_ADV707_DATA2_PRN_frm.QuickRepBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  with qryList do
  begin
    Close;
    // MAINT_NO
    Parameters[0].Value := FMAINT_NO;
    // AMD_NO
    Parameters[1].Value := FAMD_NO;
    Open;    
  end;
end;

procedure TQR_ADV707_DATA2_PRN_frm.CHILD_28BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRLabel48.Caption := qryListEX_NAME1.AsString;
  QRLabel49.Caption := qryListEX_NAME2.AsString;
  QRLabel50.Caption := qryListEX_NAME3.AsString;
  QRLabel51.Caption := Trim(qryListEX_ADDR1.AsString+' '+qryListEX_ADDR2.AsString);
end;

end.
