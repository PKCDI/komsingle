unit LOCRC1_PRINT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PRINT_PARENT, QuickRpt, QRCtrls, ExtCtrls, DB,
  ADODB, LOCRC2_PRINT;

type
  TLOCRC1_PRINT_frm = class(TPRINT_PARENT_QR)
    TitleBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel4: TQRLabel;
    QR_MAINT_NO: TQRLabel;
    ChildBand2: TQRChildBand;
    QRLabel3: TQRLabel;
    QR_RFF_NO: TQRLabel;
    ChildBand3: TQRChildBand;
    QRLabel6: TQRLabel;
    QR_ISS_DAT: TQRLabel;
    ChildBand4: TQRChildBand;
    QRLabel8: TQRLabel;
    QR_BENEFC1: TQRLabel;
    ChildBand5: TQRChildBand;
    QRLabel10: TQRLabel;
    QR_GET_DAT: TQRLabel;
    ChildBand7: TQRChildBand;
    QRLabel14: TQRLabel;
    QR_EXP_DAT: TQRLabel;
    ChildBand8: TQRChildBand;
    QR_REMARK1: TQRMemo;
    QRLabel24: TQRLabel;
    ChildBand9: TQRChildBand;
    QRImage3: TQRImage;
    QRLabel124: TQRLabel;
    QRImage1: TQRImage;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QR_BSN_HSCODE: TQRLabel;
    DetailBand1: TQRBand;
    QR_HSNO: TQRLabel;
    QR_NAME1: TQRMemo;
    QR_SIZE1: TQRMemo;
    QR_QTY: TQRLabel;
    QR_PRICE: TQRLabel;
    QR_AMT: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel21: TQRLabel;
    QR_TQTY: TQRLabel;
    QR_TAMT: TQRLabel;
    QRImage5: TQRImage;
    ChildBand10: TQRChildBand;
    QRImage6: TQRImage;
    QRLabel5: TQRLabel;
    ChildBand11: TQRChildBand;
    QR_APBANK4: TQRLabel;
    QR_APBANK3: TQRLabel;
    QR_APBANK2: TQRLabel;
    QR_APBANK1: TQRLabel;
    QRLabel13: TQRLabel;
    ChildBand12: TQRChildBand;
    QR_LOCNO: TQRLabel;
    QRLabel23: TQRLabel;
    ChildBand13: TQRChildBand;
    QR_LOCAMT: TQRLabel;
    QRLabel26: TQRLabel;
    ChildBand14: TQRChildBand;
    QRLabel27: TQRLabel;
    QR_LOADDATE: TQRLabel;
    ChildBand15: TQRChildBand;
    QR_EXPDATE: TQRLabel;
    QRLabel30: TQRLabel;
    ChildBand16: TQRChildBand;
    QRLabel31: TQRLabel;
    QR_LOC_REM1: TQRMemo;
    ChildBand17: TQRChildBand;
    QRImage7: TQRImage;
    QRLabel32: TQRLabel;
    ChildBand18: TQRChildBand;
    QR_APPLIC1: TQRLabel;
    QRLabel34: TQRLabel;
    ChildBand19: TQRChildBand;
    QR_APPLIC2: TQRLabel;
    QRLabel36: TQRLabel;
    ChildBand20: TQRChildBand;
    QR_APPLIC3: TQRLabel;
    QRLabel38: TQRLabel;
    PageFooterBand1: TQRBand;
    QRLabel7: TQRLabel;
    qryList: TADOQuery;
    qryGoods: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListRFF_NO: TStringField;
    qryListGET_DAT: TStringField;
    qryListISS_DAT: TStringField;
    qryListEXP_DAT: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListAPPLIC: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListAPPLIC6: TStringField;
    qryListRCT_AMT1: TBCDField;
    qryListRCT_AMT1C: TStringField;
    qryListRCT_AMT2: TBCDField;
    qryListRCT_AMT2C: TStringField;
    qryListRATE: TBCDField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListTQTY: TBCDField;
    qryListTQTY_G: TStringField;
    qryListTAMT: TBCDField;
    qryListTAMT_G: TStringField;
    qryListLOC_NO: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_NAME: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC2AMTC: TStringField;
    qryListEX_RATE: TBCDField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListLOC_REM: TStringField;
    qryListLOC_REM1: TMemoField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListNEGODT: TStringField;
    qryListNEGOAMT: TBCDField;
    qryListBSN_HSCODE: TStringField;
    qryListPRNO: TIntegerField;
    qryListAPPLIC7: TStringField;
    qryListBENEFC2: TStringField;
    qryListCK_S: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TBCDField;
    qryGoodsHS_CHK: TStringField;
    qryGoodsHS_NO: TStringField;
    qryGoodsNAME_CHK: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE: TStringField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsAMT: TBCDField;
    qryGoodsAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsSTAMT: TBCDField;
    qryGoodsSTAMT_G: TStringField;
    QRImage2: TQRImage;
    ChildBand6: TQRChildBand;
    QRLabel12: TQRLabel;
    QR_RCT_AMT1: TQRLabel;
    QR_RCT_AMT2: TQRLabel;
    QR_RATE: TQRLabel;
    QR_LOCAMT2: TQRLabel;
    QR_EXRATE: TQRLabel;
    procedure QuickRepBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand5BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRepNeedData(Sender: TObject; var MoreData: Boolean);
    procedure SummaryBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand13BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand14BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand15BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand16BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand18BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand19BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand20BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FMaintNO: string;
    procedure OPENDB;
  public
    { Public declarations }
    property  MaintNo:string  read FMaintNO write FMaintNO;
  end;

var
  LOCRC1_PRINT_frm: TLOCRC1_PRINT_frm;

implementation

{$R *.dfm}
uses Commonlib,  MSSQL;

{ TLOCRC1_PRINT_frm }

procedure TLOCRC1_PRINT_frm.OPENDB;
begin
  qryList.Close;
  qryList.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryList.Open;

  qryGoods.Close;
  qryGoods.Parameters.ParamByName('MAINT_NO').Value := FMaintNO;
  qryGoods.Open;
end;

procedure TLOCRC1_PRINT_frm.QuickRepBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  OPENDB;
  //---------ChildBand1-------------------------------------------------------
  //관리번호
  QR_MAINT_NO.Caption := qrylistMAINT_NO.AsString;       
end;

procedure TLOCRC1_PRINT_frm.ChildBand2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //발급번호
  ChildBand2.Height := ChildBand2.Tag;
  QR_RFF_NO.Caption :=  qryListRFF_NO.AsString;

  if Trim(QR_RFF_NO.Caption) = '' then
  begin
    ChildBand2.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand3BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //발급일자
  ChildBand3.Height := ChildBand3.Tag;
  if qryListISS_DAT.AsString = '' then
    QR_ISS_DAT.Caption := ''
  else
    QR_ISS_DAT.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(qryListISS_DAT.AsString));

  if Trim(QR_ISS_DAT.Caption) = '' then
  begin
    ChildBand3.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //공급자
  QR_BENEFC1.Height := ChildBand4.Tag;
  QR_BENEFC1.Caption := qryListBENEFC1.AsString;

  if Trim(QR_BENEFC1.Caption) = '' then
  begin
    ChildBand4.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand5BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //인수일자
  ChildBand5.Height := ChildBand5.Tag;
  if qryListGET_DAT.AsString = '' then
    QR_GET_DAT.Caption := ''
  else
    QR_GET_DAT.Caption := FormatDateTime('YYYY.MM.DD',ConvertStr2Date(qryListGET_DAT.AsString));

  if Trim(QR_ISS_DAT.Caption) = '' then
  begin
    ChildBand5.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand6BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //인수금액
  ChildBand6.Height := ChildBand6.Tag;
  if qryListRCT_AMT1.AsCurrency = 0 then
  begin
    QR_RCT_AMT1.Enabled := False;
    QR_RCT_AMT2.Left := 120;
    QR_RATE.Left := 232;
  end;

  if qryListRCT_AMT2.AsCurrency = 0 then
  begin
    QR_RCT_AMT2.Enabled := False;
    QR_RATE.Left := 120;
  end;

  if qryListRATE.AsCurrency = 0 then
  begin
    QR_RATE.Enabled := False;
  end;

  QR_RCT_AMT1.Caption := qrylistRCT_AMT1C.AsString + ' ' + FormatFloat('#,##0.####',qrylistRCT_AMT1.AsFloat);
  if qrylistRCT_AMT1.AsFloat = 0 then QR_RCT_AMT1.Enabled := False;

  QR_RCT_AMT2.Caption := qrylistRCT_AMT2C.AsString + ' ' + FormatFloat('#,##0.####', qrylistRCT_AMT2.AsFloat);
  if qrylistRCT_AMT2.AsFloat = 0 then QR_RCT_AMT2.Enabled := False;

  QR_RATE.Caption := '(' + FormatFloat('#,##0.####', qrylistRATE.AsFloat) + ')';
  if qrylistRATE.AsFloat = 0 then QR_RATE.Enabled := False;

end;

procedure TLOCRC1_PRINT_frm.ChildBand7BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //문서유효기일
  ChildBand7.Height := ChildBand7.Tag;
  if qryListEXP_DAT.AsString = '' then
    QR_EXP_DAT.Caption := ''
  else
    QR_EXP_DAT.Caption :=  FormatDateTime('YYYY.MM.DD', ConvertStr2Date(qrylistEXP_DAT.AsString));

  if Trim(QR_EXP_DAT.Caption) = '' then
  begin
    ChildBand7.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand8BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //기타조건
  try
    //TStringList생성
    memoLines := TStringList.Create;
    //memoLines에 REMARK_1 대입
    memoLines.Text := qrylistREMARK1.AsString;

    ChildBand8.Height := ( 13 * memoLines.Count) + 26;
    QR_REMARK1.Height := ( 13 * memoLines.Count);

    QR_REMARK1.Lines.Clear;
    QR_REMARK1.Lines.Text := qrylistREMARK1.AsString;

    if QR_REMARK1.Lines.Count = 0 then
    begin
      ChildBand8.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand9BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //대표공급물품의 HS부호
  QR_BSN_HSCODE.Caption :=  qrylistBSN_HSCODE.AsString;
  if Trim(QR_BSN_HSCODE.Caption) = '' then
  begin
    QR_BSN_HSCODE.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
  RowCount,nIndex,i : Integer;
begin
  inherited;
  try
    //HS번호
    QR_HSNO.Caption := qryGoodsHS_NO.AsString;

    //수량
    if qryGoodsQTY.AsCurrency = 0 then
     QR_QTY.Caption := ''
    else
      QR_QTY.Caption := FormatFloat('#,##0.####', qryGoodsQTY.AsFloat) + ' ' + qryGoodsQTY_G.AsString;

    //단가
    if (qryGoodsPRICE.AsCurrency = 0) and (qryGoodsQTYG.AsCurrency = 0) then
      QR_PRICE.Caption := ''
    else
      QR_PRICE.Caption := qryGoodsPRICE_G.AsString + ' ' + FormatFloat('#,##0.####', qryGoodsPRICE.AsFloat)
                        +' / ' +  FormatFloat('#,##0.####' ,qryGoodsQTYG.asFloat) + ' ' + qryGoodsQTYG_G.AsString;
    //금액
    if qryGoodsAMT.AsCurrency = 0 then
      QR_AMT.Caption := ''
    else
      QR_AMT.Caption := qryGoodsAMT_G.AsString + ' ' + FormatFloat('#,##0.####', qryGoodsAMT.AsFloat);

    RowCount := 0;
   
    //품명
    //TStringList생성
    memoLines := TStringList.Create;
    //memoLines에 NAME1 대입
    memoLines.Text := Trim(qryGoodsNAME1.AsString);

    QR_NAME1.Height := ( 13 * memoLines.Count);

    QR_NAME1.Lines.Clear;
    QR_NAME1.Lines.Text := memoLines.Text;
    //getValueDetailMemo(QR_NAME1,'NAME1');

    RowCount := memoLines.Count;

    //규격
    //TStringList생성
    memoLines := TStringList.Create;
    //memoLines에 SIZE1 대입
    memoLines.Text := Trim(qryGoodsSIZE1.AsString);

    QR_SIZE1.Height := ( 13 * memoLines.Count);

    QR_SIZE1.Lines.Clear;
    QR_SIZE1.Lines.Text := memoLines.Text;

    if RowCount < memoLines.Count then RowCount := memoLines.Count;

    DetailBand1.Height := 56 + (RowCount*15);

    qryGoods.Next;

  finally
      memoLines.Free;
  end;

end;

procedure TLOCRC1_PRINT_frm.QuickRepNeedData(Sender: TObject;
  var MoreData: Boolean);
begin
  inherited;
  //MoreData의 값이 true이면 실행
  MoreData := not qryGoods.Eof;
end;

procedure TLOCRC1_PRINT_frm.SummaryBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //총수량
  if qryListTQTY.AsCurrency = 0 then
    QR_TQTY.Caption := ''
  else
    QR_TQTY.Caption := FormatFloat('#,##0.####', qrylistTQTY.AsFloat) + ' ' + qrylistTQTY_G.AsString;

   //총합계
  if qryListTAMT.AsCurrency = 0 then
    QR_TAMT.Caption := ''
  else
    QR_TAMT.Caption := qrylistTAMT_G.AsString +' '+ FormatFloat('#,##0.####', qrylistTAMT.AsFloat);
    
end;

procedure TLOCRC1_PRINT_frm.ChildBand11BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  nIndex , i : Integer;
begin
  inherited;
  //---------ChildBand11-------------------------------------------------------
   //개설은행
      nIndex := 1;
      for i := 1 to 4 do
      begin
        if Trim(qrylist.FieldByName('AP_BANK'+IntToStr(i)).AsString) = '' then
        begin
          (FindComponent('QR_APBANK'+IntToStr(i)) as TQRLabel).Caption := '';
          Continue;
        end;

        (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Enabled := True;
        (FindComponent('QR_APBANK'+IntToStr(nIndex)) as TQRLabel).Caption := qrylist.FieldByName('AP_BANK'+IntToStr(i)).AsString;
        Inc(nIndex);
      end;

      if (Trim(QR_APBANK1.Caption) = '') and (Trim(QR_APBANK2.Caption) = '') then
      begin
        ChildBand11.Enabled := False;
      end
      else
        ChildBand11.Height := (13 * (nIndex)) ;
end;

procedure TLOCRC1_PRINT_frm.ChildBand12BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //신용장번호
  ChildBand12.Height := ChildBand12.Tag;
  QR_LOCNO.Caption := qrylistLOC_NO.AsString;

  if Trim(QR_LOCNO.Caption) = '' then
  begin
    ChildBand12.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand13BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //내국신용장금액
  ChildBand13.Height := ChildBand13.Tag;
  if qrylistLOC1AMT.AsCurrency = 0 then
  begin
    QR_LOCAMT.Enabled := False;
    QR_LOCAMT2.Left := 120;
    QR_EXRATE.Left := 232;
  end;

  if qrylistLOC2AMT.AsCurrency = 0 then
  begin
    QR_LOCAMT2.Enabled := False;
    QR_EXRATE.Left := 120;
  end;

  if qrylistEX_RATE.AsCurrency = 0 then
  begin
    QR_EXRATE.Enabled := False;
  end;

  QR_LOCAMT.Caption := qrylistLOC1AMTC.AsString + ' ' + FormatFloat('#,##0.####',qrylistLOC1AMT.AsFloat);
  QR_LOCAMT2.Caption := qrylistLOC2AMTC.AsString + ' ' + FormatFloat('#,##0.####', qrylistLOC2AMT.AsFloat);
  QR_EXRATE.Caption := '('+ FormatFloat('#,##0.####', qryListEX_RATE.AsFloat) + ')';     

  if (QR_LOCAMT.Enabled = False) and (QR_LOCAMT2.Enabled = False) then
  begin
    ChildBand13.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand14BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //인도기일
  ChildBand14.Height := ChildBand14.Tag;
  if qryListLOADDATE.AsString = '' then
    QR_LOADDATE.Caption := ''
  else
    QR_LOADDATE.Caption := FormatDateTime('YYYY.MM.DD', ConvertStr2Date(qrylistLOADDATE.AsString));

  if Trim(QR_LOADDATE.Caption) = '' then
  begin
    ChildBand14.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand15BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //유효기일
  ChildBand15.Height := ChildBand15.Tag;
  if qryListEXPDATE.AsString = '' then
    QR_EXPDATE.Caption := ''
  else
    QR_EXPDATE.Caption := FormatDateTime('YYYY.MM.DD', ConvertStr2Date(qrylistEXPDATE.AsString));

  if Trim(QR_EXPDATE.Caption) = '' then
  begin
    ChildBand15.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand16BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  memoLines : TStringList;
begin
  inherited;
  //참조사항
  try
    //TStringList생성
    memoLines := TStringList.Create;
    //memoLines에 LOC_REM1 대입
    memoLines.Text := qrylistLOC_REM1.AsString;

    ChildBand16.Height := ( 13 * memoLines.Count) + 26;
    QR_LOC_REM1.Height := ( 13 * memoLines.Count);

    QR_LOC_REM1.Lines.Clear;
    QR_LOC_REM1.Lines.Text := qrylistLOC_REM1.AsString;

    if QR_LOC_REM1.Lines.Count = 0 then
    begin
      ChildBand16.Enabled := False;
    end;
  finally
    memoLines.Free;
  end;

end;

procedure TLOCRC1_PRINT_frm.ChildBand18BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //기관명(개설업체)
  ChildBand18.Height := ChildBand18.Tag;
  QR_APPLIC1.Caption := qrylistAPPLIC1.AsString;

  if Trim(QR_APPLIC1.Caption) = '' then
  begin
    ChildBand18.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand19BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //대표자명(개설업체)
  ChildBand19.Height := ChildBand19.Tag;
  QR_APPLIC2.Caption := qrylistAPPLIC2.AsString;

  if Trim(QR_APPLIC2.Caption) = '' then
  begin
    ChildBand19.Enabled := False;
  end;
end;

procedure TLOCRC1_PRINT_frm.ChildBand20BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  //전자서명(개설업체)
  ChildBand20.Height := ChildBand20.Tag;
  QR_APPLIC3.Caption := qrylistAPPLIC3.AsString;

  if Trim(QR_APPLIC3.Caption) = '' then
  begin
    ChildBand20.Enabled := False;
  end;
end;

end.
