inherited CODE_MSSQL_frm: TCODE_MSSQL_frm
  Left = 943
  Top = 221
  Caption = #54364#51456'/'#51068#48152#53076#46300' '#44288#47532' - MSSQL'
  ClientHeight = 620
  FormStyle = fsNormal
  Position = poOwnerFormCenter
  Visible = False
  OnActivate = nil
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel1: TsPanel
    Height = 43
    SkinData.SkinSection = 'GROUPBOX'
    inherited sSpeedButton3: TsSpeedButton
      Height = 41
    end
    inherited Btn_Close: TsSpeedButton
      Height = 41
      OnClick = Btn_CloseClick
      SkinData.CustomFont = True
      Images = DMICON.System18
      ImageIndex = 34
    end
    inherited sSpeedButton4: TsSpeedButton
      Height = 41
      Visible = False
      Images = DMICON.System18
      ImageIndex = 20
    end
    inherited sSpeedButton5: TsSpeedButton
      Height = 41
    end
    inherited sSplitter2: TsSplitter
      Height = 41
    end
    inherited sLabel1: TsLabel
      Top = 11
    end
  end
  inherited sPanel2: TsPanel
    Top = 43
    inherited sSpeedButton2: TsSpeedButton
      OnClick = sSpeedButton2Click
      Images = DMICON.System16
      ImageIndex = 1
    end
    inherited sSpeedButton6: TsSpeedButton
      Tag = 1
      OnClick = sSpeedButton2Click
      Images = DMICON.System16
      ImageIndex = 2
    end
  end
  inherited sPanel3: TsPanel
    Top = 84
    Height = 536
    inherited sDBGrid1: TsDBGrid
      Top = 76
      Height = 459
      DataSource = dsMst
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      Columns = <
        item
          Expanded = False
          FieldName = 'Prefix'
          Title.Caption = #44396#48516#53076#46300
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Name'
          Title.Caption = #47749#52845
          Width = 196
          Visible = True
        end>
    end
    inherited sPanel6: TsPanel
      Height = 43
      inherited Btn_New: TsSpeedButton
        Left = 99
        Height = 41
        OnClick = Btn_NewClick
        Images = DMICON.System18
      end
      inherited Btn_Modify: TsSpeedButton
        Left = 154
        Height = 41
        OnClick = Btn_NewClick
        Images = DMICON.System18
        ImageIndex = 3
      end
      inherited Btn_Del: TsSpeedButton
        Left = 209
        Height = 41
        OnClick = Btn_DelClick
        Images = DMICON.System18
        ImageIndex = 26
      end
      inherited sSplitter3: TsSplitter
        Height = 41
      end
      inherited sLabel4: TsLabel
        Left = 29
        Top = 13
        Width = 31
        Height = 17
        Font.Height = -13
      end
      object sSpeedButton13: TsSpeedButton
        Left = 89
        Top = 1
        Width = 10
        Height = 41
        Cursor = crHandPoint
        Layout = blGlyphTop
        Spacing = 0
        Align = alLeft
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
      end
    end
    inherited sPanel7: TsPanel
      Top = 44
      inherited sBitBtn1: TsBitBtn
        OnClick = sBitBtn1Click
      end
    end
  end
  inherited sPanel4: TsPanel
    Top = 84
    Height = 536
    inherited sPanel5: TsPanel
      Height = 43
      inherited Btn_DetailNew: TsSpeedButton
        Tag = 3
        Height = 41
        OnClick = Btn_NewClick
        Images = DMICON.System18
      end
      inherited Btn_DetailMod: TsSpeedButton
        Tag = 4
        Height = 41
        OnClick = Btn_NewClick
        Images = DMICON.System18
        ImageIndex = 3
      end
      inherited Btn_DetailDel: TsSpeedButton
        Tag = 5
        Height = 41
        OnClick = Btn_DetailDelClick
        Images = DMICON.System18
        ImageIndex = 26
      end
      inherited sSplitter1: TsSplitter
        Height = 41
      end
      inherited sLabel3: TsLabel
        Left = 12
        Top = 13
        Width = 65
        Height = 17
        Font.Height = -13
      end
      inherited sSpeedButton9: TsSpeedButton
        Height = 41
      end
      inherited sSpeedButton10: TsSpeedButton
        Height = 41
      end
      inherited Btn_DetailCheck: TsSpeedButton
        Tag = 3
        Height = 41
        OnClick = Btn_DetailCheckClick
        Images = DMICON.System18
        ImageIndex = 16
      end
      inherited Btn_DetailNoCheck: TsSpeedButton
        Tag = 4
        Height = 41
        OnClick = Btn_DetailCheckClick
        Images = DMICON.System18
        ImageIndex = 17
      end
    end
    inherited sDBGrid2: TsDBGrid
      Top = 76
      Height = 459
      DataSource = dsDetail
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      OnDblClick = sDBGrid2DblClick
      OnKeyUp = sDBGrid2KeyUp
      Columns = <
        item
          Expanded = False
          FieldName = 'CODE'
          Title.Caption = #53076#46300
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #54637#47785
          Width = 324
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Remark'
          Title.Alignment = taCenter
          Title.Caption = #49324#50857
          Width = 39
          Visible = True
        end>
    end
    object sPanel8: TsPanel
      Left = 1
      Top = 44
      Width = 457
      Height = 32
      SkinData.SkinSection = 'GROUPBOX'
      Align = alTop
      
      TabOrder = 2
      object sEdit2: TsEdit
        Left = 64
        Top = 6
        Width = 121
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = #44404#47548#52404
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = sEdit2Change
        SkinData.CustomColor = True
        SkinData.SkinSection = 'GROUPBOX'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #53076#46300#52286#44592
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #44404#47548#52404
        BoundLabel.Font.Style = []
      end
      object sBitBtn2: TsBitBtn
        Left = 186
        Top = 6
        Width = 47
        Height = 21
        Caption = #51312#54924
        TabOrder = 1
        SkinData.SkinSection = 'BUTTON'
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    DrawClientArea = True
    DrawNonClientArea = True
    Left = 24
    Top = 240
  end
  object qryMst: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryMstAfterOpen
    AfterScroll = qryMstAfterScroll
    Parameters = <
      item
        Name = 'GUBUN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Prefix,[Name],Gubun'
      'FROM CODE2NDM'
      'WHERE Gubun = :GUBUN')
    Left = 24
    Top = 200
    object qryMstPrefix: TStringField
      FieldName = 'Prefix'
      Size = 10
    end
    object qryMstName: TStringField
      FieldName = 'Name'
      Size = 50
    end
    object qryMstGubun: TStringField
      FieldName = 'Gubun'
      Size = 10
    end
  end
  object dsMst: TDataSource
    DataSet = qryMst
    Left = 56
    Top = 200
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryDetailAfterOpen
    AfterScroll = qryDetailAfterScroll
    Parameters = <
      item
        Name = 'prefix'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = #53685#54868
      end>
    SQL.Strings = (
      'SELECT Prefix,[CODE],[MAP],LTRIM([NAME]) as [NAME],[Remark]'
      'FROM CODE2NDD'
      'WHERE Prefix = :prefix')
    Left = 320
    Top = 232
    object qryDetailCODE: TStringField
      FieldName = 'CODE'
      Size = 12
    end
    object qryDetailMAP: TStringField
      FieldName = 'MAP'
      Size = 10
    end
    object qryDetailNAME: TStringField
      FieldName = 'NAME'
      ReadOnly = True
      Size = 100
    end
    object qryDetailRemark: TStringField
      FieldName = 'Remark'
      OnGetText = qryDetailRemarkGetText
      Size = 1
    end
    object qryDetailPrefix: TStringField
      FieldName = 'Prefix'
      Size = 10
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 352
    Top = 232
  end
  object qryCheck: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryDetailAfterOpen
    AfterScroll = qryDetailAfterScroll
    Parameters = <
      item
        Name = 'Remark'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'prefix'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'CODE'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 12
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE CODE2NDD'
      'SET Remark = :Remark'
      'Where Prefix = :Prefix'
      'AND CODE = :CODE')
    Left = 320
    Top = 264
    object StringField1: TStringField
      FieldName = 'CODE'
      Size = 12
    end
    object StringField2: TStringField
      FieldName = 'MAP'
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'NAME'
      ReadOnly = True
      Size = 100
    end
    object StringField4: TStringField
      FieldName = 'Remark'
      OnGetText = qryDetailRemarkGetText
      Size = 1
    end
  end
end
