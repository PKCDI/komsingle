unit PCRLIC_N_MSSQL;

interface    

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PCRLIC_N, Buttons, sSpeedButton, StdCtrls, sLabel,
  sSkinProvider, DBCtrls, sDBMemo, sDBEdit, sGroupBox, ExtCtrls, sSplitter,
  Grids, DBGrids, acDBGrid, sEdit, sComboBox, Mask, sMaskEdit, ComCtrls,
  sPageControl, sPanel, DB, ADODB, StrUtils, sButton, DateUtils, QuickRpt,
  sCustomComboEdit, sToolEdit, sBevel, sBitBtn;

type
  TPCRLIC_N_MSSQL_frm = class(TPCRLIC_N_frm)
    sSpeedButton3: TsSpeedButton;
    Btn_Del: TsSpeedButton;
    Btn_Close: TsSpeedButton;
    Btn_Print: TsSpeedButton;
    sSpeedButton27: TsSpeedButton;
    sDBEdit49: TsDBEdit;
    sDBEdit50: TsDBEdit;
    sDBEdit63: TsDBEdit;
    sDBEdit64: TsDBEdit;
    sDBEdit65: TsDBEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListAPP_ADDR1: TStringField;
    qryListAPP_ADDR2: TStringField;
    qryListAPP_ADDR3: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListSUP_CODE: TStringField;
    qryListSUP_NAME1: TStringField;
    qryListSUP_NAME2: TStringField;
    qryListSUP_NAME3: TStringField;
    qryListSUP_ADDR1: TStringField;
    qryListSUP_ADDR2: TStringField;
    qryListSUP_ADDR3: TStringField;
    qryListITM_G1: TStringField;
    qryListITM_G2: TStringField;
    qryListITM_G3: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYC: TStringField;
    qryListTAMT1: TBCDField;
    qryListTAMT1C: TStringField;
    qryListTAMT2: TBCDField;
    qryListTAMT2C: TStringField;
    qryListCHG_G: TStringField;
    qryListC1_AMT: TBCDField;
    qryListC1_C: TStringField;
    qryListC2_AMT: TBCDField;
    qryListLIC_INFO: TMemoField;
    qryListLIC_NO: TStringField;
    qryListBK_CD: TStringField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListBK_NAME3: TStringField;
    qryListBK_NAME4: TStringField;
    qryListBK_NAME5: TStringField;
    qryListAC1_AMT: TBCDField;
    qryListAC1_C: TStringField;
    qryListAC2_AMT: TBCDField;
    qryListComf_dat: TStringField;
    qryListPRNO: TIntegerField;
    qryListPCrLic_No: TStringField;
    qryListChgCd: TStringField;
    qryListBankSend_No: TStringField;
    qryListAPP_DATE: TStringField;
    qryListDOC_G: TStringField;
    qryListDOC_D: TStringField;
    qryListBHS_NO: TStringField;
    qryListBNAME: TStringField;
    qryListBNAME1: TMemoField;
    qryListBAMT: TBCDField;
    qryListBAMTC: TStringField;
    qryListEXP_DATE: TStringField;
    qryListSHIP_DATE: TStringField;
    qryListBSIZE1: TMemoField;
    qryListBAL_CD: TStringField;
    qryListBAL_NAME1: TStringField;
    qryListBAL_NAME2: TStringField;
    qryListF_INTERFACE: TStringField;
    qryListITM_GBN: TStringField;
    qryListITM_GNAME: TStringField;
    qryListISSUE_GBN: TStringField;
    qryListDOC_GBN: TStringField;
    qryListDOC_NO: TStringField;
    qryListBAMT2: TBCDField;
    qryListBAMTC2: TStringField;
    qryListBAMT3: TBCDField;
    qryListBAMTC3: TStringField;
    qryListPCRLIC_ISS_NO: TStringField;
    qryListBK_CD2: TStringField;
    qryListBAL_CD2: TStringField;
    qryListEMAIL_ID: TStringField;
    qryListEMAIL_DOMAIN: TStringField;
    sButton1: TsButton;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailREMARK: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTYC: TStringField;
    qryDetailAMT1: TBCDField;
    qryDetailAMT1C: TStringField;
    qryDetailAMT2: TBCDField;
    qryDetailPRI1: TBCDField;
    qryDetailPRI2: TBCDField;
    qryDetailPRI_BASE: TBCDField;
    qryDetailPRI_BASEC: TStringField;
    qryDetailACHG_G: TStringField;
    qryDetailAC1_AMT: TBCDField;
    qryDetailAC1_C: TStringField;
    qryDetailAC2_AMT: TBCDField;
    qryDetailLOC_TYPE: TStringField;
    qryDetailNAMESS: TStringField;
    qryDetailSIZESS: TStringField;
    qryDetailAPP_DATE: TStringField;
    qryDetailITM_NUM: TStringField;
    qryTax: TADOQuery;
    dsTax: TDataSource;
    qryTaxKEYY: TStringField;
    qryTaxSEQ: TIntegerField;
    qryTaxDATEE: TStringField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxITEM_NAME1: TStringField;
    qryTaxITEM_NAME2: TStringField;
    qryTaxITEM_NAME3: TStringField;
    qryTaxITEM_NAME4: TStringField;
    qryTaxITEM_NAME5: TStringField;
    qryTaxITEM_DESC: TMemoField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    qryTaxQUANTITY: TBCDField;
    qryTaxQUANTITY_UNIT: TStringField;
    QRCompositeReport1: TQRCompositeReport;
    sSpeedButton2: TsSpeedButton;
    sPanel2: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sDateEdit1: TsDateEdit;
    sDateEdit2: TsDateEdit;
    sButton2: TsButton;
    sLabel3: TsLabel;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sLabel60: TsLabel;
    sLabel59: TsLabel;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton6: TsSpeedButton;
    sButton3: TsButton;
    sSplitter2: TsSplitter;
    sSplitter1: TsSplitter;
    sPanel14: TsPanel;
    sPanel15: TsPanel;
    Mask_fromDate: TsMaskEdit;
    Mask_toDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    sDBGrid3: TsDBGrid;
    sPanel23: TsPanel;
    sPanel16: TsPanel;
    sPanel17: TsPanel;
    sPanel18: TsPanel;
    sPanel22: TsPanel;
    sSpeedButton19: TsSpeedButton;
    sPanel20: TsPanel;
    sPanel32: TsPanel;
    sPanel19: TsPanel;
    sPanel25: TsPanel;
    sBitBtn24: TsBitBtn;
    sBitBtn25: TsBitBtn;
    sPanel26: TsPanel;
    procedure qryListDATEEGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sSpeedButton8Click(Sender: TObject);
    procedure qryDetailNAME1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryTaxITEM_DESCGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure Btn_CloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_PrintClick(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure Btn_DelClick(Sender: TObject);
    procedure sDBGrid3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure sComboBox5Select(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
//    procedure ReadList; overload;
    procedure ReadDetail;
    procedure SetAfterData;
    procedure ReadTax;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;
    procedure Readlist(OrderSyntax : string = ''); overload;
    procedure DeleteDocumnet;
  public
    { Public declarations }
  end;

var
  PCRLIC_N_MSSQL_frm: TPCRLIC_N_MSSQL_frm;

implementation

uses MSSQL, Commonlib, VarDefine, CodeContents, PCRLIC_PRINT, PCRLIC_PRINT2,
  NewExcelWriter, PCRLIC_EXCEL, MessageDefine;

{$R *.dfm}

procedure TPCRLIC_N_MSSQL_frm.qryListDATEEGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  sDate : String;
begin
  inherited;
  sDate := qryListDATEE.AsString;
  IF sDate = '' Then Exit;
  Text := FormatDateTime('YYYY-MM-DD',decodeCharDate(LeftStr(sDate,8)))
end;

procedure TPCRLIC_N_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

//procedure TPCRLIC_N_MSSQL_frm.ReadList;
//begin
//  with qryList do
//  begin
//    Close;
//    SQL.Text := FSQL;
//    SQL.Add('WHERE DATEE between' + QuotedStr(sMaskEdit6.Text) + 'AND' + QuotedStr(sMaskEdit7.Text));
//    SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+sEdit3.Text+'%'));
//    SQL.Add('AND USER_ID = '+QuotedStr(LoginData.sID));
//
//    SQL.Add('ORDER BY DATEE ASC, MAINT_NO ASC');
//    Open;
//  end;
//end;

procedure TPCRLIC_N_MSSQL_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList();
  Mask_fromDate.Text := sMaskEdit6.Text;
  Mask_toDate.Text := sMaskEdit7.Text;
end;

procedure TPCRLIC_N_MSSQL_frm.SetAfterData;
begin
  with DMCodeContents do
  begin
    CODEREFRESH;
    //PAGE 1 신청/변경구분
    PCRLIC_GUBUN.First;
    IF PCRLIC_GUBUN.Locate('CODE',qryListChgCd.AsString,[]) Then
      sEdit17.Text := PCRLIC_GUBUN.FieldByName('NAME').AsString
    else
      sEdit17.Text := '';

    //PAGE 1 총할인/할증/변동
    BYEONDONG_GUBUN.First;
    IF BYEONDONG_GUBUN.Locate('CODE',qrylistCHG_G.AsString,[]) Then
      sEdit28.Text := BYEONDONG_GUBUN.FieldByName('NAME').AsString
    else
      sEdit28.Text := '';

    //PAGE 1 공급물품명세구분
    NAEGUKSIN4025.First;
    IF NAEGUKSIN4025.Locate('CODE',qryListITM_GBN.AsString,[]) Then
      sEdit34.Text := NAEGUKSIN4025.FieldByName('NAME').AsString
    else
      sEdit34.Text := '';
  end;
end;

procedure TPCRLIC_N_MSSQL_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
 // sPanel4.Caption := IntToStr(qryList.RecNo) + ' | ' + IntToStr(qryList.RecordCount);
  
  SetAfterData;
  
  ReadDetail;
  ReadTax;  
end;

procedure TPCRLIC_N_MSSQL_frm.sSpeedButton4Click(Sender: TObject);
begin
  inherited;
  qryList.Prior;
end;

procedure TPCRLIC_N_MSSQL_frm.sSpeedButton8Click(Sender: TObject);
begin
  inherited;
  qrylist.Next;
end;

procedure TPCRLIC_N_MSSQL_frm.ReadDetail;
begin
  qryDetail.DisableControls;
  with qryDetail do
  begin
    Close;
    Parameters.ParamByName('KEYY').Value := qryListMAINT_NO.AsString;
    Open;
  end;
  qryDetail.EnableControls;
end;

procedure TPCRLIC_N_MSSQL_frm.ReadTax;
begin
  qryTax.DisableControls;
  with qryTax do
  begin
    Close;
    Parameters.ParamByName('KEYY').Value := qryListMAINT_NO.AsString;
    Open;
  end;
  qryTax.EnableControls;
end;

procedure TPCRLIC_N_MSSQL_frm.qryDetailNAME1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  Text := AnsiReplaceText(Sender.AsString,#13#10,' ');
end;

procedure TPCRLIC_N_MSSQL_frm.qryTaxITEM_DESCGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  Text := AnsiReplaceText(Sender.AsString,#13#10,' ');
end;

procedure TPCRLIC_N_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TPCRLIC_N_MSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  PCRLIC_N_MSSQL_frm := nil;
end;

procedure TPCRLIC_N_MSSQL_frm.Btn_PrintClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
//  PCRLIC_PRINT_FRM.DocNo := 'YAPPPCR2015000042389';
  PCRLIC_PRINT_FRM := TPCRLIC_PRINT_FRM.Create(Self);
  PCRLIC_PRINT2_FRM := TPCRLIC_PRINT2_FRM.Create(Self);
  try
    PCRLIC_PRINT_FRM.DocNo := qryListMAINT_NO.AsString;
    PCRLIC_PRINT2_FRM.DocNo := qryListMAINT_NO.AsString;
    QRCompositeReport1.Prepare;
    PCRLIC_PRINT2_FRM.Totalpage := PCRLIC_PRINT2_FRM.QRPrinter.PageCount;

    if (Sender as TsSpeedButton).Tag = 0 then
    begin
      QRCompositeReport1.PrinterSetup;
      //------------------------------------------------------------------------------
      // 프린트 셋업이후 OK면 0 CANCEL이면 1이 리턴됨
      //------------------------------------------------------------------------------
      IF QRCompositeReport1.Tag = 0 Then QRCompositeReport1.Print;
    end
    else
       QRCompositeReport1.Preview;
  finally
    FreeAndNil(PCRLIC_PRINT_FRM);
    FreeAndNil(PCRLIC_PRINT2_FRM);    
  end;
end;

procedure TPCRLIC_N_MSSQL_frm.QRCompositeReport1AddReports(
  Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(PCRLIC_PRINT_FRM);
  QRCompositeReport1.Reports.Add(PCRLIC_PRINT2_FRM);
end;

procedure TPCRLIC_N_MSSQL_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  PCRLIC_EXCEL_FRM := TPCRLIC_EXCEL_FRM.Create(Self);
  PCRLIC_EXCEL_FRM.Run(AnsiReplaceText(sDateEdit1.Text,'-','') ,
                       AnsiReplaceText(sDateEdit2.Text,'-',''));
  sPanel2.Visible := False;
  PCRLIC_EXCEL_FRM.Free;
end;

procedure TPCRLIC_N_MSSQL_frm.sSpeedButton2Click(Sender: TObject);
begin
  inherited;
  sDateEdit1.Date := StartOfTheMonth(Now);
  sDateEdit2.Date := Now;  
  sPanel2.Visible := not sPanel2.Visible;
end;

procedure TPCRLIC_N_MSSQL_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  sPanel2.Visible := False;
end;

procedure TPCRLIC_N_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  EnabledControlValue(sPanel5);
  EnabledControlValue(sGroupBox5);
  EnabledControlValue(sPanel11);
  EnabledControlValue(sGroupBox7);
  EnabledControlValue(sPanel3);
  sPageControl1.ActivePageIndex := 0;

  sMaskEdit6.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD', Now);

  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD', Now);

  ReadList(Mask_fromDate.Text,Mask_toDate.Text,'');
end;

procedure TPCRLIC_N_MSSQL_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  //외화획득용 구매확인신청서 왼쪽목 록 DBGRID 패널을 숨김
  sPanel14.Visible := not (sPageControl1.ActivePageIndex = 4);
end;

function TPCRLIC_N_MSSQL_frm.ReadList(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
  Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
   //SQL.Add(' ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TPCRLIC_N_MSSQL_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  ReadList(mask_fromdate.text,Mask_toDate.text,'');
  sMaskEdit6.Text := Mask_fromDate.Text;
  sMaskEdit7.Text := Mask_toDate.Text;
end;

procedure TPCRLIC_N_MSSQL_frm.Btn_DelClick(Sender: TObject);
begin
  if qryList.RecordCount = 0 then Exit;
  if not qryList.Active then Exit;
  DeleteDocumnet;
end;

procedure TPCRLIC_N_MSSQL_frm.DeleteDocumnet;
var
  maint_no : String;
  nCursor : integer;
begin
  if qryList.RecordCount = 0 then Exit;
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := qryListMAINT_NO.AsString;
    ShowMessage(maint_no);
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM PCRLIC_H WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM PCRLIC_TAX WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM PCRLICD1 WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM PCRLICD2 WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;
end;

procedure TPCRLIC_N_MSSQL_frm.sDBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TPCRLIC_N_MSSQL_frm.sComboBox5Select(Sender: TObject);
begin
  inherited;
  sEdit3.Visible  := sComboBox5.ItemIndex <> 0;
  sMaskEdit6.Visible := sComboBox5.ItemIndex = 0;
  sMaskEdit7.Visible := sComboBox5.ItemIndex = 0;
  sPanel26.Visible := sComboBox5.ItemIndex = 0;
  sBitBtn24.Visible := sComboBox5.ItemIndex = 0;
  sBitBtn25.Visible := sComboBox5.ItemIndex = 0;

  IF sEdit3.Visible Then sEdit3.Clear;
end;

procedure TPCRLIC_N_MSSQL_frm.ReadList(OrderSyntax: string);
begin
 if sComboBox5.ItemIndex in [1,2] then
  begin
    IF Trim(sEdit3.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 4 Then sEdit3.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := FSQL;
    case sComboBox5.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(sMaskEdit6.Text)+' AND '+QuotedStr(sMaskEdit7.Text));
        Mask_fromDate.Text := sMaskEdit6.Text;
        Mask_toDate.Text := sMaskEdit7.Text;
      end;
      1 : SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+sEdit3.Text+'%'));
      2 : SQL.Add(' WHERE SUP_NAME1 LIKE ' + QuotedStr('%'+sEdit3.Text+'%'));
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ');
    end;

    Open;

  end;
end;

end.
