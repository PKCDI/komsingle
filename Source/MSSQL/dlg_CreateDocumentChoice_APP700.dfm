inherited dlg_CreateDocumentChoice_APP700_frm: Tdlg_CreateDocumentChoice_APP700_frm
  Left = 1189
  Top = 246
  Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#44060#49444' '#49888#52397#49436' '#51089#49457
  ClientHeight = 349
  ClientWidth = 437
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 437
    Height = 349
    object sLabel6: TsLabel [0]
      Left = 8
      Top = 7
      Width = 70
      Height = 25
      Caption = 'APP700'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel [1]
      Left = 8
      Top = 29
      Width = 176
      Height = 15
      Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#44060#49444' '#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sBevel1: TsBevel [2]
      Left = 8
      Top = 49
      Width = 423
      Height = 2
      Shape = bsFrame
    end
    object sLabel1: TsLabel [3]
      Left = 8
      Top = 55
      Width = 124
      Height = 15
      Caption = #51089#49457#48169#49885#51012' '#49440#53469#54616#49464#50836
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object sBevel2: TsBevel [4]
      Left = 8
      Top = 273
      Width = 423
      Height = 2
      Shape = bsFrame
    end
    inherited sButton1: TsButton
      Top = 202
      Width = 425
      Caption = #49888#44508#47928#49436' '#51089#49457'(&3)'
      ModalResult = 4
    end
    inherited sButton3: TsButton
      Top = 141
      Width = 425
      Caption = #50808#54868#54925#46301#50857#49688#51077#49849#51064' '#49888#52397#49436' '#47785#47197#50640#49436' '#49440#53469'(&2)'
    end
    inherited sButton2: TsButton
      Top = 280
      Width = 425
      OnClick = sButton2Click
    end
    object sButton4: TsButton
      Left = 8
      Top = 80
      Width = 425
      Height = 57
      Caption = #49688#51077#49849#51064' '#49888#52397#49436' '#47785#47197#50640#49436' '#49440#53469'(&1)'
      ModalResult = 1
      TabOrder = 3
      SkinData.SkinSection = 'BUTTON'
      CommandLinkHint = #44592#51316#50640' '#51200#51109#46104#50612#51080#45716' '#47928#49436#47484' '#49440#53469#54616#50668' '#48373#49324#54633#45768#45796
      Images = DMICON.System24
      ImageIndex = 31
      Style = bsCommandLink
      Reflected = True
    end
  end
end
