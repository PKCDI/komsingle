object MIGVIEW_frm: TMIGVIEW_frm
  Left = 654
  Top = 189
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = #49688#49888#45936#51060#53552' '#54869#51064
  ClientHeight = 462
  ClientWidth = 545
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 545
    Height = 462
    Align = alClient
    BorderWidth = 4
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sMemo1: TsMemo
      Left = 5
      Top = 5
      Width = 535
      Height = 412
      Align = alTop
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 213
      Top = 424
      Width = 118
      Height = 25
      Caption = #54869#51064
      TabOrder = 1
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
end
