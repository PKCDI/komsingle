inherited Dlg_FindBankCode_frm: TDlg_FindBankCode_frm
  Left = 487
  Top = 309
  Caption = #51008#54665#53076#46300' '#51312#54924
  ClientHeight = 394
  ClientWidth = 363
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 363
    Height = 394
    object sComboBox1: TsComboBox
      Left = 8
      Top = 8
      Width = 57
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = #53076#46300
      Items.Strings = (
        #53076#46300
        #54637#47785)
    end
    object sEdit1: TsEdit
      Left = 67
      Top = 8
      Width = 110
      Height = 23
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sButton1: TsButton
      Left = 178
      Top = 8
      Width = 75
      Height = 23
      Caption = #44160#49353
      TabOrder = 2
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System16
      ImageIndex = 0
    end
    object sDBGrid1: TsDBGrid
      Left = 8
      Top = 36
      Width = 347
      Height = 309
      Anchors = [akLeft, akTop, akRight]
      Color = clWhite
      Ctl3D = False
      DataSource = DataSource1
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDblClick = sDBGrid1DblClick
      OnKeyUp = sDBGrid1KeyUp
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Expanded = False
          FieldName = 'CODE'
          Title.Caption = #53076#46300
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BANKNAME'
          Title.Caption = #51008#54665#47749
          Width = 135
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BANKBRANCH'
          Title.Caption = #51648#51216#47749
          Width = 104
          Visible = True
        end>
    end
    object sButton2: TsButton
      Left = 104
      Top = 356
      Width = 75
      Height = 29
      Caption = #54869#51064
      ModalResult = 1
      TabOrder = 4
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 17
    end
    object sButton3: TsButton
      Left = 184
      Top = 356
      Width = 75
      Height = 29
      Cancel = True
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 5
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 18
    end
    object sButton4: TsButton
      Left = 254
      Top = 8
      Width = 75
      Height = 23
      Caption = #52628#44032
      TabOrder = 6
      OnClick = sButton4Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System16
      ImageIndex = 4
    end
  end
  object DataSource1: TDataSource
    DataSet = DMCodeContents.BANKCODE
    Left = 264
    Top = 80
  end
end
