unit NormalRecv_MSSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, NormalRecv, sSkinProvider, StdCtrls, DBCtrls, sDBMemo, sDBEdit,
  sGroupBox, Grids, DBGrids, acDBGrid, sButton, sEdit, sComboBox, Mask,
  sMaskEdit, ComCtrls, sPageControl, Buttons, sSpeedButton, sLabel,
  ExtCtrls, sSplitter, sPanel, DB, ADODB, StrUtils, DateUtils;

type
  TNormalRecv_MSSQL_frm = class(TNormalRecv_frm)
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMaint_No: TStringField;
    qryListUser_Id: TStringField;
    qryListDatee: TStringField;
    qryListEX_Name1: TStringField;
    qryListEX_Name2: TStringField;
    qryListEX_Name3: TStringField;
    qryListSR_Name1: TStringField;
    qryListSR_Name2: TStringField;
    qryListSr_Name3: TStringField;
    qryListEX_Id: TStringField;
    qryListSR_Id: TStringField;
    qryListXX_Id: TStringField;
    qryListDoc1: TStringField;
    qryListDoc2: TStringField;
    qryListDoc3: TStringField;
    qryListDoc4: TStringField;
    qryListDoc5: TStringField;
    qryListDescc: TBooleanField;
    qryListDesc_1: TMemoField;
    qryListChk1: TBooleanField;
    qryListChk2: TStringField;
    qryListChk3: TStringField;
    qryListPRNO: TIntegerField;
    qryListBGM_CODE: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    sComboBox1: TsComboBox;
    qryDocList: TADOQuery;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sPanel10: TsPanel;
    sSpeedButton8: TsSpeedButton;
    qryListBGM_NAME: TStringField;
    qryDel: TADOQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    StringField17: TStringField;
    BooleanField1: TBooleanField;
    MemoField1: TMemoField;
    BooleanField2: TBooleanField;
    StringField18: TStringField;
    StringField19: TStringField;
    IntegerField1: TIntegerField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sComboBox1Select(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sSpeedButton8Click(Sender: TObject);
    procedure Btn_CloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn_DelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
    procedure SetDocList;
  public
    { Public declarations }
  end;

var
  NormalRecv_MSSQL_frm: TNormalRecv_MSSQL_frm;

implementation

uses MSSQL, Commonlib;

{$R *.dfm}

{ TNormalRecv_MSSQL_frm }

procedure TNormalRecv_MSSQL_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQl.Text := FSQL;
    SQL.Add('WHERE DATEE between' + QuotedStr(sMaskEdit6.Text) + 'AND' + QuotedStr(sMaskEdit7.Text));
    IF Trim(sEdit3.Text) <> '' Then
      SQL.Add('AND Maint_No LIKE '+QuotedStr('%'+sEdit3.Text+'%'));
    IF sComboBox1.ItemIndex > 0 Then
      SQL.Add('AND DOC1 = '+QuotedStr(sComboBox1.Text));

    SQL.Add('ORDER BY DATEE DESC');
    Open;
  end;
end;

procedure TNormalRecv_MSSQL_frm.SetDocList;
begin
  sCombobox1.Items.Clear;

  qryDocList.Close;
  qryDocList.Open;
  try
    sComboBox1.items.Add('전체');
    while not qryDocList.Eof do
    begin
      sComboBox1.Items.Add(qryDocList.FieldByName('Doc1').AsString);
      qryDocList.Next;
    end;
  finally
    qryDocList.Close;
  end;
  sComboBox1.ItemIndex := 0;
end;

procedure TNormalRecv_MSSQL_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
//  sMaskEdit6.Text := '20000101';
//  sMaskEdit7.Text := FormatDateTime('YYYYMMDD',EndOfTheMonth(Now));
  SetDocList;
  ReadList;
end;

procedure TNormalRecv_MSSQL_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TNormalRecv_MSSQL_frm.sComboBox1Select(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TNormalRecv_MSSQL_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF qryList.RecordCount > 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
//      SelectData;
    end;
  end;
end;

procedure TNormalRecv_MSSQL_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  sPanel8.Caption := IntToStr(qryList.RecNo) + ' | ' + IntToStr(qryList.RecordCount);
end;

procedure TNormalRecv_MSSQL_frm.sSpeedButton4Click(Sender: TObject);
begin
  inherited;
  qryList.Prior;
end;

procedure TNormalRecv_MSSQL_frm.sSpeedButton8Click(Sender: TObject);
begin
  inherited;
  qryList.Next;
end;

procedure TNormalRecv_MSSQL_frm.Btn_CloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TNormalRecv_MSSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  NormalRecv_MSSQL_frm := nil;
end;

procedure TNormalRecv_MSSQL_frm.Btn_DelClick(Sender: TObject);
begin
  inherited;
  IF ConfirmMessage('해당 일반응답서('+qryListMaint_No.AsString+')를 삭제하시겠습니까?') Then
  begin
    qryDel.Close;
    qryDel.Parameters.ParamByName('MAINT_NO').Value := qryListMaint_No.AsString;
    qryDel.ExecSQL;

    ReadList;
  end;
end;

procedure TNormalRecv_MSSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  sPageControl1.ActivePageIndex := 0;           
  ReadOnlyControl(spanel3,false);
end;

procedure TNormalRecv_MSSQL_frm.FormActivate(Sender: TObject);
var
  BMK : Pointer;
begin
  inherited;
  BMK := qryList.GetBookmark;
  ReadList;
  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);
end;

end.
