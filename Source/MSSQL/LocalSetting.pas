unit LocalSetting;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sDialogs, StdCtrls, sEdit, sSpinEdit, sButton, Grids, sGroupBox, Registry,
  sLabel, ExtCtrls, sPanel, DB, ADODB, DBGrids, acDBGrid, sComboBox;


type
    TLocalSetting_frm = class(TForm)
    sPanel3: TsPanel;
    sGroupBox3: TsGroupBox;
    StringGrid2: TStringGrid;
    sButton7: TsButton;
    sButton8: TsButton;
    sSpinEdit1: TsSpinEdit;
    sOpenDialog1: TsOpenDialog;
    sPanel1: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sEdit1: TsEdit;
    sPanel2: TsPanel;

    procedure sSpinEdit1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton8Click(Sender: TObject);
    procedure sButton7Click(Sender: TObject);
    procedure StringGrid2DblClick(Sender: TObject);

  private
    //레지스트리에 등록한 사융자수를 저장하는 변수. 저장시 기존자료 초기화할때 사용.
    LoadDBCount:Integer;
    FREG : TRegistry;

    //레지스트리에 저장된 기존 설정값 불러옴
    procedure LoadLocalSetting;

    //레지스트리에 설정값저장
    procedure SetLocalSetting;

    //스트링그리드 필드명 설정
    procedure InitGrid;
    { Private declarations }
  public
    //파라미터로 입력된 인덱스값에 맞는 ConnectionString 입력
    function GetLocalConnectionString(RegIdx:Integer):String;
    procedure getServerList(var ListCombobox: TsComboBox);
    { Public declarations }
  end;

var
  LocalSetting_frm: TLocalSetting_frm;
Const
  ROOT_KEY : string = 'SOFTWARE\KIS\COMM';
implementation

uses
  Encryption;


{$R *.dfm}

procedure TLocalSetting_frm.sSpinEdit1Change(Sender: TObject);
var
  iCount:Integer;
begin
  if sSpinEdit1.Value < 1 then    //스핀값이 1이하로 떨어지면 FixedRows가 제기능을 못해서 스핀값 1이하로 안떨어지게 설정
    sSpinEdit1.Value := 1;

  StringGrid2.RowCount := sSpinEdit1.Value+1;

  StringGrid2.FixedRows := 1;

end;

procedure TLocalSetting_frm.FormShow(Sender: TObject);
begin
  LoadLocalSetting;
end;

procedure TLocalSetting_frm.LoadLocalSetting;
var
  DBCount : integer;
  nLoop : integer;
begin
  InitGrid;

  FREG := TRegistry.Create;
  try
    with FREG do
    begin
      RootKey := HKEY_LOCAL_MACHINE;
      OpenKey(ROOT_KEY, True);

      IF ValueExists('DBCOUNT') Then  //DBCOUNT가 존재하지않으면 COUNT값 1로 설정
      begin
        DBCount := ReadInteger('DBCOUNT');
        LoadDBCount := ReadInteger('DBCOUNT');
      end
      else
      begin
        DBCount := 1;
        LoadDBCount := 1;
      end;
      IF ValueExists('INSTANCE') Then
      begin
        sEdit1.Text := ReadString('INSTANCE');
      end;

      sSpinEdit1.Value := DBCount;
      StringGrid2.RowCount := DBCount+1;

      for nLoop := 1 to DBCount do
      begin
        with StringGrid2 do
        begin
          IF nLoop = 1 Then
          begin

            IF ValueExists('DBName') Then
              Cells[0,nLoop] := ServerDecrypt(ReadString('DBName'))  //DBName이 존재하면 DBName값 입력
            else
              Cells[0,nLoop] := '이름없음'+inttostr(nLoop) ; //존재하지않으면 이름없음값으로 입력

            IF Trim(ReadString('DBPath')) <> '' Then
              Cells[1,nLoop] := ServerDecrypt(ReadString('DBPath'))
            else
              Cells[1,nLoop] := '';
          end
          else
          begin
            IF ValueExists('DBName'+IntToStr(nLoop)) Then
              Cells[0,nLoop] := ServerDecrypt(ReadString('DBName'+IntToStr(nLoop)))
            else
              Cells[0,nLoop] := '이름없음'+inttostr(nLoop);

            IF Trim(ReadString('DBPATH'+IntToStr(nLoop))) <> '' Then
              Cells[1,nLoop] := ServerDecrypt(ReadString('DBPATH'+IntToStr(nLoop)))
            else
              Cells[1,nLoop] := '';
          end;
        end;
      end;

    end;
  finally
    FREG.Free;
  end;
end;

procedure TLocalSetting_frm.InitGrid;
begin
  with StringGrid2 do
  begin
    Cells[0,0] := 'DB Name';
    Cells[1,0] := 'DB Path';
  end;
end;

procedure TLocalSetting_frm.sButton8Click(Sender: TObject);
begin
  ModalResult := mrCancel;
//    Close;
end;

procedure TLocalSetting_frm.SetLocalSetting;
var
  Reg : TRegistry;
  nLoop :integer;
begin
  //------Registry--------------------------------------------------------------
//  CheckBlank;

  FREG := TRegistry.Create;
  try
    with FREG do
    begin
      RootKey := HKEY_LOCAL_MACHINE;
      OpenKey(ROOT_KEY,True);

        if Length(sEdit1.Text) < 1 then
        begin
          ShowMessage('인스턴스를 입력하세요');
          exit;
        end
        else
          WriteString('INSTANCE',sEdit1.Text);

        for nLoop := 1 to sSpinEdit1.Value do
        begin
          if StringGrid2.Cells[0,nLoop] = '' then
          begin
            ShowMessage('DBNAME 중에 빈값이 포함되어 있습니다.');
            exit;
          end;
          if StringGrid2.Cells[1,nLoop] = '' then
          begin
            ShowMessage('DBPATH 중에 빈값이 포함되어 있습니다.');
            exit;
          end;
        end;

        if LoadDBCount > 1 then
        begin
          for nLoop := 2 to LoadDBCount do                 //레지스트리에 기존저장된 DB카운트가 2개이상이면
          begin                                            // DBNAME2~ DBPATH2~ 전부삭제 (초기화)
            DeleteValue('DBNAME'+IntToStr(nLoop));
            DeleteValue('DBPATH'+IntToStr(nLoop));
          end
        end;


        for nLoop := 1 to sSpinEdit1.Value do          //입력한 사용자수만큼 반복수행
        begin
          IF StringGrid2.Cells[0,nLoop] = '' Then      //DBNAME에 아무값도 들어가지않았으면
          begin
            WriteInteger('DBCOUNT',nLoop-1 );          // DBBCOUNT는 비어있는값 전까지로 체크하고
            Break;                                     // 반복구간 나가기
          end
          ELSE
          if nLoop = sSpinEdit1.Value Then
          begin
            WriteInteger('DBCOUNT',nLoop);
          end;

          IF nLoop = 1 Then
          begin
            WriteString('DBName', ServerEncrypt(StringGrid2.Cells[0,nLoop]));
            WriteString('DBPath', ServerEncrypt(StringGrid2.Cells[1,nLoop]));
          end
          else
          begin
            WriteString('DBName'+IntToStr(nLoop), ServerEncrypt(StringGrid2.Cells[0,nLoop]));
            WriteString('DBPath'+IntToStr(nLoop), ServerEncrypt(StringGrid2.Cells[1,nLoop]));
          end;

        end;

      CloseKey;

      ShowMessage('저장되었습니다');
      ModalResult := mrOk;
    end;

  finally
    FREG.Free;

  end;

end;

procedure TLocalSetting_frm.sButton7Click(Sender: TObject);
begin
  SetLocalSetting;
end;

procedure TLocalSetting_frm.StringGrid2DblClick(Sender: TObject);
VAR
  iCol,iRow:Integer;
begin

  //현재 더블클릭한 셀의 행-열 인덱스 가져오기
  iCol := (Sender as TStringGrid).Col;
  iRow := (Sender as TStringGrid).Row;

  IF (Sender as TStringGrid).Col = 1 THEN //DBPATH쪽을 더블클릭하면 opendialog 실
  begin

    sOpenDialog1.InitialDir := ExtractFilePath(Application.ExeName)+'\';

    IF not sOpenDialog1.Execute then
      Exit;

    StringGrid2.Cells[iCol,iRow] :=  sOpenDialog1.Files[0];

  end;
end;

function TLocalSetting_frm.GetLocalConnectionString(RegIdx:Integer):String;
var
  sDBName, sDBPath, sInstance, sConnectionString : String;
begin

  FREG := TRegistry.Create;

  try
    with FREG do
    begin

      RootKey := HKEY_LOCAL_MACHINE;
      OpenKey(ROOT_KEY,True);

      if RegIdx = 1 then
      begin
        if ValueExists('DBName') then
           sDBName := ReadString('DBName')
        else
        begin
          ShowMessage('현재 등록된 DBName 설정이 존재하지않습니다.');
          exit;
        end;
        if ValueExists('DBPath') then
          sDBPath := ReadString('DBPath')
        else
        begin
          ShowMessage('현재 등록된 DBPath 설정이 존재하지않습니다.');
          exit;
        end;
        if ValueExists('INSTANCE') then
          sInstance := ReadString('INSTANCE')
        else
        begin
          ShowMessage('현재 등록된 인스턴스설정이 존재하지않습니다.');
          exit;
        end;
      end
      else
      begin
        if ValueExists('DBName'+IntToStr(RegIdx)) then
           sDBName := ReadString('DBName'+IntToStr(RegIdx))
        else
        begin
          ShowMessage('현재 등록된 DBName'+IntToStr(RegIdx)+' 설정이 존재하지않습니다.');
          exit;
        end;
        if ValueExists('DBPath'+IntToStr(RegIdx)) then
          sDBPath := ReadString('DBPath'+IntToStr(RegIdx))
        else
        begin
          ShowMessage('현재 등록된 DBPath'+IntToStr(RegIdx)+' 설정이 존재하지않습니다.');
          exit;
        end;
        if ValueExists('INSTANCE') then
          sInstance := ReadString('INSTANCE')
        else
        begin
          ShowMessage('현재 등록된 인스턴스설정이 존재하지않습니다.');
          exit;
        end;
      end;

    end;
  finally
    FREG.Free

  end;

  sConnectionString := 'Provider=SQLNCLI11.1;Integrated Security=SSPI;Persist Security Info=False;';
  //데이터베이스명
  sConnectionString := sConnectionString+'Initial Catalog='+ServerDecrypt(sDBName)+';';
  //인스턴스명
  sConnectionString := sConnectionString+'Data Source=(localdb)\'+sInstance+';';
  //MDF파일 경로
  sConnectionString := sConnectionString+'Initial File Name='+ServerDecrypt(sDBPath)+';';
  sConnectionString := sConnectionString+'Server SPN=""';

  Result := sConnectionString;

end;

procedure TLocalSetting_frm.getServerList(var ListCombobox: TsComboBox);
var
  DBCount, nLoop :integer;
begin
  ListCombobox.Clear;
  ListCombobox.Items.Add('SELECT SERVER');

  FREG := TRegistry.Create;
  try
    with FREG do
    begin
      RootKey := HKEY_LOCAL_MACHINE;
      OpenKey(ROOT_KEY, True);

      IF ValueExists('DBCOUNT') Then  //DBCOUNT가 존재하지않으면 COUNT값 1로 설정
        DBCount := ReadInteger('DBCOUNT')
      else
        DBCount := 0;

      IF ValueExists('INSTANCE') Then
      begin
        sEdit1.Text := ReadString('INSTANCE');
      end;

      for nLoop := 1 to DBCount do
      begin
        IF nLoop = 1 Then
        begin
          IF ValueExists('DBName') Then
            ListCombobox.Items.Add(ServerDecrypt(ReadString('DBName')))
          else
            ListCombobox.Items.Add('이름없음'+inttostr(nLoop))
        end
        else
        begin
          IF ValueExists('DBName'+IntToStr(nLoop)) Then
            ListCombobox.Items.Add(ServerDecrypt(ReadString('DBName'+IntToStr(nLoop))))
          else
            ListCombobox.Items.Add('이름없음'+inttostr(nLoop))
        end;
      end;
    end;
  finally
    FREG.Free;
  end;

end;

end.
