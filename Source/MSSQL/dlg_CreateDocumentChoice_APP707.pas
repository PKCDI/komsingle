unit dlg_CreateDocumentChoice_APP707;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dlg_CreateDocumentChoice_APP700, sSkinProvider, StdCtrls,
  sButton, ExtCtrls, sBevel, sLabel, sPanel,MSSQL;

type
  Tdlg_CreateDocumentChoice_APP707_frm = class(Tdlg_CreateDocumentChoice_APP700_frm)
    procedure sButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_CreateDocumentChoice_APP707_frm: Tdlg_CreateDocumentChoice_APP707_frm;

implementation

{$R *.dfm}

procedure Tdlg_CreateDocumentChoice_APP707_frm.sButton2Click(
  Sender: TObject);
begin
  inherited;

  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans ;
  Close;
  
end;

end.
