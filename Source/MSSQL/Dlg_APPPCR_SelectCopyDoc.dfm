inherited Dlg_APPPCR_SelectCopyDoc_frm: TDlg_APPPCR_SelectCopyDoc_frm
  Left = 768
  Top = 240
  BorderIcons = [biSystemMenu]
  Caption = #48373#49324#54624' '#50896#48376#47928#49436' '#49440#53469
  ClientHeight = 476
  ClientWidth = 740
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 740
    Height = 476
    object sPanel7: TsPanel
      Left = 1
      Top = 1
      Width = 738
      Height = 32
      Align = alTop
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'PAGECONTROL'
      DesignSize = (
        738
        32)
      object sMaskEdit6: TsMaskEdit
        Tag = 1
        Left = 64
        Top = 6
        Width = 73
        Height = 20
        Color = clWhite
        EditMask = '9999-99-99;0;'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = []
        ImeName = 'Microsoft IME 2010'
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '20130101'
        OnKeyUp = sMaskEdit6KeyUp
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 4276545
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #44404#47548#52404
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.CustomColor = True
        SkinData.SkinSection = 'PAGECONTROL'
      end
      object sComboBox5: TsComboBox
        Left = 272
        Top = 6
        Width = 105
        Height = 20
        Alignment = taLeftJustify
        BoundLabel.Active = True
        BoundLabel.Caption = #44620#51648
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 4276545
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #44404#47548#52404
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = []
        ImeName = 'Microsoft IME 2010'
        ItemHeight = 14
        ItemIndex = 0
        ParentFont = False
        TabOrder = 2
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840
          #44277#44553#51088)
      end
      object sMaskEdit7: TsMaskEdit
        Tag = 1
        Left = 168
        Top = 6
        Width = 73
        Height = 20
        Color = clWhite
        EditMask = '9999-99-99;0;'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = []
        ImeName = 'Microsoft IME 2010'
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20130101'
        OnKeyUp = sMaskEdit7KeyUp
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #48512#53552
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = 4276545
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #44404#47548#52404
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.CustomColor = True
        SkinData.SkinSection = 'PAGECONTROL'
      end
      object sEdit3: TsEdit
        Left = 378
        Top = 6
        Width = 183
        Height = 20
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #44404#47548#52404
        Font.Style = []
        ImeName = 'Microsoft IME 2010'
        ParentFont = False
        TabOrder = 3
        OnKeyUp = sMaskEdit7KeyUp
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
      object sButton1: TsButton
        Left = 562
        Top = 6
        Width = 75
        Height = 20
        Caption = #51312#54924
        TabOrder = 4
        OnClick = sButton1Click
        SkinData.SkinSection = 'BUTTON'
      end
      object sButton2: TsButton
        Left = 654
        Top = 6
        Width = 75
        Height = 20
        Anchors = [akTop, akRight]
        Caption = #45803#44592
        TabOrder = 5
        OnClick = sButton2Click
        SkinData.SkinSection = 'BUTTON'
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 33
      Width = 738
      Height = 442
      Align = alClient
      Color = clWhite
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ImeName = 'Microsoft IME 2010'
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid1DrawColumnCell
      OnDblClick = sDBGrid1DblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Expanded = False
          Title.Alignment = taCenter
          Title.Caption = #49440#53469
          Width = -1
          Visible = False
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #49345#54889
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Caption = #44288#47532#48264#54840
          Width = 129
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SUP_NAME1'
          Title.Caption = #44277#44553#51088
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TQTY'
          Title.Caption = #52509#49688#47049
          Width = 99
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'TQTYC'
          Title.Alignment = taCenter
          Title.Caption = #45800#50948
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TAMT1'
          Title.Caption = #52509#44552#50529
          Width = 99
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'TAMT1C'
          Title.Alignment = taCenter
          Title.Caption = #45800#50948
          Width = 28
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'USER_ID'
          Title.Caption = #49324#50857#51088
          Width = 42
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #51089#49457#51068#51088
          Width = 90
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 48
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[USER_ID]'
      '      ,[DATEE]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[APP_CODE]'
      '      ,[APP_NAME1]'
      '      ,[APP_NAME2]'
      '      ,[APP_NAME3]'
      '      ,[APP_ADDR1]'
      '      ,[APP_ADDR2]'
      '      ,[APP_ADDR3]'
      '      ,[AX_NAME1]'
      '      ,[AX_NAME2]'
      '      ,[AX_NAME3]'
      '      ,[SUP_CODE]'
      '      ,[SUP_NAME1]'
      '      ,[SUP_NAME2]'
      '      ,[SUP_NAME3]'
      '      ,[SUP_ADDR1]'
      '      ,[SUP_ADDR2]'
      '      ,[SUP_ADDR3]'
      '      ,[ITM_G1]'
      '      ,[ITM_G2]'
      '      ,[ITM_G3]'
      '      ,[TQTY]'
      '      ,[TQTYC]'
      '      ,[TAMT1]'
      '      ,[TAMT1C]'
      '      ,[TAMT2]'
      '      ,[TAMT2C]'
      '      ,[CHG_G]'
      '      ,[C1_AMT]'
      '      ,[C1_C]'
      '      ,[C2_AMT]'
      '      ,[BK_NAME1]'
      '      ,[BK_NAME2]'
      '      ,[PRNO]'
      '      ,[PCrLic_No]'
      '      ,[ChgCd]'
      '      ,[APP_DATE]'
      '      ,[DOC_G]'
      '      ,[DOC_D]'
      '      ,[BHS_NO]'
      '      ,[BNAME]'
      '      ,[BNAME1]'
      '      ,[BAMT]'
      '      ,[BAMTC]'
      '      ,[EXP_DATE]'
      '      ,[SHIP_DATE]'
      '      ,[BSIZE1]'
      '      ,[BAL_CD]'
      '      ,[BAL_NAME1]'
      '      ,[BAL_NAME2]'
      '      ,[F_INTERFACE]'
      '      ,[SRBUHO]'
      '      ,[ITM_GBN]'
      '      ,[ITM_GNAME]'
      '      ,[ISSUE_GBN]'
      '      ,[DOC_GBN]'
      '      ,[DOC_NO]'
      '      ,[BAMT2]'
      '      ,[BAMTC2]'
      '      ,[BAMT3]'
      '      ,[BAMTC3]'
      '      ,[PCRLIC_ISS_NO]'
      '      ,[FIN_CODE]'
      '      ,[FIN_NAME]'
      '      ,[FIN_NAME2]'
      '      ,[NEGO_APPDT]'
      '      ,[EMAIL_ID]'
      '      ,[EMAIL_DOMAIN]'
      '      ,[BK_CD]'
      'FROM APPPCR_H with(nolock)')
    Left = 8
    Top = 80
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryListAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryListAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryListAPP_ADDR1: TStringField
      FieldName = 'APP_ADDR1'
      Size = 35
    end
    object qryListAPP_ADDR2: TStringField
      FieldName = 'APP_ADDR2'
      Size = 35
    end
    object qryListAPP_ADDR3: TStringField
      FieldName = 'APP_ADDR3'
      Size = 35
    end
    object qryListAX_NAME1: TStringField
      FieldName = 'AX_NAME1'
      Size = 35
    end
    object qryListAX_NAME2: TStringField
      FieldName = 'AX_NAME2'
      Size = 35
    end
    object qryListAX_NAME3: TStringField
      FieldName = 'AX_NAME3'
      Size = 10
    end
    object qryListSUP_CODE: TStringField
      FieldName = 'SUP_CODE'
      Size = 10
    end
    object qryListSUP_NAME1: TStringField
      FieldName = 'SUP_NAME1'
      Size = 35
    end
    object qryListSUP_NAME2: TStringField
      FieldName = 'SUP_NAME2'
      Size = 35
    end
    object qryListSUP_NAME3: TStringField
      FieldName = 'SUP_NAME3'
      Size = 35
    end
    object qryListSUP_ADDR1: TStringField
      FieldName = 'SUP_ADDR1'
      Size = 35
    end
    object qryListSUP_ADDR2: TStringField
      FieldName = 'SUP_ADDR2'
      Size = 35
    end
    object qryListSUP_ADDR3: TStringField
      FieldName = 'SUP_ADDR3'
      Size = 35
    end
    object qryListITM_G1: TStringField
      FieldName = 'ITM_G1'
      Size = 3
    end
    object qryListITM_G2: TStringField
      FieldName = 'ITM_G2'
      Size = 3
    end
    object qryListITM_G3: TStringField
      FieldName = 'ITM_G3'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListTAMT1: TBCDField
      FieldName = 'TAMT1'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListTAMT1C: TStringField
      FieldName = 'TAMT1C'
      Size = 3
    end
    object qryListTAMT2: TBCDField
      FieldName = 'TAMT2'
      Precision = 18
    end
    object qryListTAMT2C: TStringField
      FieldName = 'TAMT2C'
      Size = 3
    end
    object qryListCHG_G: TStringField
      FieldName = 'CHG_G'
      Size = 3
    end
    object qryListC1_AMT: TBCDField
      FieldName = 'C1_AMT'
      Precision = 18
    end
    object qryListC1_C: TStringField
      FieldName = 'C1_C'
      Size = 3
    end
    object qryListC2_AMT: TBCDField
      FieldName = 'C2_AMT'
      Precision = 18
    end
    object qryListBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 70
    end
    object qryListBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 70
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListPCrLic_No: TStringField
      FieldName = 'PCrLic_No'
      Size = 35
    end
    object qryListChgCd: TStringField
      FieldName = 'ChgCd'
      Size = 3
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListDOC_G: TStringField
      FieldName = 'DOC_G'
      Size = 3
    end
    object qryListDOC_D: TStringField
      FieldName = 'DOC_D'
      Size = 35
    end
    object qryListBHS_NO: TStringField
      FieldName = 'BHS_NO'
      Size = 10
    end
    object qryListBNAME: TStringField
      FieldName = 'BNAME'
    end
    object qryListBNAME1: TMemoField
      FieldName = 'BNAME1'
      BlobType = ftMemo
    end
    object qryListBAMT: TBCDField
      FieldName = 'BAMT'
      Precision = 18
    end
    object qryListBAMTC: TStringField
      FieldName = 'BAMTC'
      Size = 3
    end
    object qryListEXP_DATE: TStringField
      FieldName = 'EXP_DATE'
      Size = 8
    end
    object qryListSHIP_DATE: TStringField
      FieldName = 'SHIP_DATE'
      Size = 8
    end
    object qryListBSIZE1: TMemoField
      FieldName = 'BSIZE1'
      BlobType = ftMemo
    end
    object qryListBAL_CD: TStringField
      FieldName = 'BAL_CD'
      Size = 4
    end
    object qryListBAL_NAME1: TStringField
      FieldName = 'BAL_NAME1'
      Size = 70
    end
    object qryListBAL_NAME2: TStringField
      FieldName = 'BAL_NAME2'
      Size = 70
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListSRBUHO: TStringField
      FieldName = 'SRBUHO'
      Size = 35
    end
    object qryListITM_GBN: TStringField
      FieldName = 'ITM_GBN'
      Size = 3
    end
    object qryListITM_GNAME: TStringField
      FieldName = 'ITM_GNAME'
      Size = 70
    end
    object qryListISSUE_GBN: TStringField
      FieldName = 'ISSUE_GBN'
      Size = 3
    end
    object qryListDOC_GBN: TStringField
      FieldName = 'DOC_GBN'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListBAMT2: TBCDField
      FieldName = 'BAMT2'
      Precision = 18
    end
    object qryListBAMTC2: TStringField
      FieldName = 'BAMTC2'
      Size = 3
    end
    object qryListBAMT3: TBCDField
      FieldName = 'BAMT3'
      Precision = 18
    end
    object qryListBAMTC3: TStringField
      FieldName = 'BAMTC3'
      Size = 3
    end
    object qryListPCRLIC_ISS_NO: TStringField
      FieldName = 'PCRLIC_ISS_NO'
      Size = 35
    end
    object qryListFIN_CODE: TStringField
      FieldName = 'FIN_CODE'
      Size = 11
    end
    object qryListFIN_NAME: TStringField
      FieldName = 'FIN_NAME'
      Size = 70
    end
    object qryListFIN_NAME2: TStringField
      FieldName = 'FIN_NAME2'
      Size = 70
    end
    object qryListNEGO_APPDT: TStringField
      FieldName = 'NEGO_APPDT'
      Size = 8
    end
    object qryListEMAIL_ID: TStringField
      FieldName = 'EMAIL_ID'
      Size = 35
    end
    object qryListEMAIL_DOMAIN: TStringField
      FieldName = 'EMAIL_DOMAIN'
      Size = 35
    end
    object qryListBK_CD: TStringField
      FieldName = 'BK_CD'
      Size = 11
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 80
  end
end
