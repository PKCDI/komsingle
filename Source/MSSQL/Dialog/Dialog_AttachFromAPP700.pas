unit Dialog_AttachFromAPP700;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, DB, ADODB, Grids, DBGrids, acDBGrid,
  StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit, ExtCtrls,
  sPanel;

type
  TDialog_AttachFromAPP700_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_PAY: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListDOC_CD: TStringField;
    qryListCD_NO: TStringField;
    qryListREF_PRE: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListprno: TIntegerField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListMAINT_NO_1: TStringField;
    qryListAPP_BANK: TStringField;
    qryListAPP_BANK1: TStringField;
    qryListAPP_BANK2: TStringField;
    qryListAPP_BANK3: TStringField;
    qryListAPP_BANK4: TStringField;
    qryListAPP_BANK5: TStringField;
    qryListAPP_ACCNT: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListCD_AMT: TBCDField;
    qryListCD_CUR: TStringField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListAVAIL: TStringField;
    qryListAVAIL1: TStringField;
    qryListAVAIL2: TStringField;
    qryListAVAIL3: TStringField;
    qryListAVAIL4: TStringField;
    qryListAV_ACCNT: TStringField;
    qryListAV_PAY: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListDRAWEE: TStringField;
    qryListDRAWEE1: TStringField;
    qryListDRAWEE2: TStringField;
    qryListDRAWEE3: TStringField;
    qryListDRAWEE4: TStringField;
    qryListDR_ACCNT: TStringField;
    qryListMAINT_NO_2: TStringField;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TBooleanField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListDESGOOD: TBooleanField;
    qryListDESGOOD_1: TMemoField;
    qryListTERM_PR: TStringField;
    qryListTERM_PR_M: TStringField;
    qryListPL_TERM: TStringField;
    qryListORIGIN: TStringField;
    qryListORIGIN_M: TStringField;
    qryListDOC_380: TBooleanField;
    qryListDOC_380_1: TBCDField;
    qryListDOC_705: TBooleanField;
    qryListDOC_705_1: TStringField;
    qryListDOC_705_2: TStringField;
    qryListDOC_705_3: TStringField;
    qryListDOC_705_4: TStringField;
    qryListDOC_740: TBooleanField;
    qryListDOC_740_1: TStringField;
    qryListDOC_740_2: TStringField;
    qryListDOC_740_3: TStringField;
    qryListDOC_740_4: TStringField;
    qryListDOC_530: TBooleanField;
    qryListDOC_530_1: TStringField;
    qryListDOC_530_2: TStringField;
    qryListDOC_271: TBooleanField;
    qryListDOC_271_1: TBCDField;
    qryListDOC_861: TBooleanField;
    qryListDOC_2AA: TBooleanField;
    qryListDOC_2AA_1: TMemoField;
    qryListACD_2AA: TBooleanField;
    qryListACD_2AA_1: TStringField;
    qryListACD_2AB: TBooleanField;
    qryListACD_2AC: TBooleanField;
    qryListACD_2AD: TBooleanField;
    qryListACD_2AE: TBooleanField;
    qryListACD_2AE_1: TMemoField;
    qryListCHARGE: TStringField;
    qryListPERIOD: TBCDField;
    qryListCONFIRMM: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListDOC_705_GUBUN: TStringField;
    qryListMAINT_NO_3: TStringField;
    qryListREI_BANK: TStringField;
    qryListREI_BANK1: TStringField;
    qryListREI_BANK2: TStringField;
    qryListREI_BANK3: TStringField;
    qryListREI_BANK4: TStringField;
    qryListREI_BANK5: TStringField;
    qryListREI_ACNNT: TStringField;
    qryListINSTRCT: TBooleanField;
    qryListINSTRCT_1: TMemoField;
    qryListAVT_BANK: TStringField;
    qryListAVT_BANK1: TStringField;
    qryListAVT_BANK2: TStringField;
    qryListAVT_BANK3: TStringField;
    qryListAVT_BANK4: TStringField;
    qryListAVT_BANK5: TStringField;
    qryListAVT_ACCNT: TStringField;
    qryListSND_INFO1: TStringField;
    qryListSND_INFO2: TStringField;
    qryListSND_INFO3: TStringField;
    qryListSND_INFO4: TStringField;
    qryListSND_INFO5: TStringField;
    qryListSND_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListEX_ADDR3: TStringField;
    qryListOP_BANK1: TStringField;
    qryListOP_BANK2: TStringField;
    qryListOP_BANK3: TStringField;
    qryListOP_ADDR1: TStringField;
    qryListOP_ADDR2: TStringField;
    qryListOP_ADDR3: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListAPPLICABLE_RULES_1: TStringField;
    qryListAPPLICABLE_RULES_2: TStringField;
    qryListDOC_760: TBooleanField;
    qryListDOC_760_1: TStringField;
    qryListDOC_760_2: TStringField;
    qryListDOC_760_3: TStringField;
    qryListDOC_760_4: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListmathod_Name: TStringField;
    qryListpay_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListDOC_Name: TStringField;
    qryListCDMAX_Name: TStringField;
    qryListpship_Name: TStringField;
    qryListtship_Name: TStringField;
    qryListdoc705_Name: TStringField;
    qryListdoc740_Name: TStringField;
    qryListdoc760_Name: TStringField;
    qryListCHARGE_Name: TStringField;
    qryListCONFIRM_Name: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sBitBtn2Click(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure btnCalendar(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
  function openDialog : String;
  end;

var
  Dialog_AttachFromAPP700_frm: TDialog_AttachFromAPP700_frm;

implementation

uses DateUtils, KISCalendar, Commonlib, MSSQL;

{$R *.dfm}

procedure TDialog_AttachFromAPP700_frm.FormCreate(Sender: TObject);
begin
  inherited;
   FSQL := qryList.SQL.Text;
end;

procedure TDialog_AttachFromAPP700_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Case com_SearchKeyword.ItemIndex of
      0: SQL.Add('WHERE APP_DATE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
      1: SQL.Add('WHERE  I700_1.MAINT_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE I700_2.BENEFC1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    SQL.Add('ORDER BY APP_DATE');

    Open;
  end;
end;

procedure TDialog_AttachFromAPP700_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_AttachFromAPP700_frm.com_SearchKeywordSelect(
  Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

function TDialog_AttachFromAPP700_frm.openDialog: String;
begin
  Result := '';

  com_SearchKeyword.ItemIndex := 0;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',now);

  ReadList;

  if ShowModal = mrOk then
    Result := qryListMAINT_NO.AsString;
end;

procedure TDialog_AttachFromAPP700_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  Dialog_AttachFromAPP700_frm := nil;
end;

procedure TDialog_AttachFromAPP700_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
  if DMMssql.KISConnect.InTransaction then
    DMMssql.KISConnect.RollbackTrans;
  Close;
end;

procedure TDialog_AttachFromAPP700_frm.Mask_SearchDate1DblClick(
  Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
  //if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TDialog_AttachFromAPP700_frm.btnCalendar(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    900 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    901 : Mask_SearchDate1DblClick(Mask_SearchDate2);
  end;

end;

procedure TDialog_AttachFromAPP700_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsDBgrid).ScreenToClient(Mouse.CursorPos).Y > 17 Then
    ModalResult := mrOk;  
end;

end.
