object dlgPAYORDCopy_frm: TdlgPAYORDCopy_frm
  Left = 894
  Top = 365
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  BorderWidth = 4
  Caption = #51060#51204' '#45936#51060#53552' '#44032#51256#50724#44592
  ClientHeight = 147
  ClientWidth = 326
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 326
    Height = 147
    Align = alClient
    
    TabOrder = 0
    object sComboBox1: TsComboBox
      Left = 112
      Top = 24
      Width = 89
      Height = 23
      BoundLabel.Active = True
      BoundLabel.Caption = #45380#46020
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      Style = csOwnerDrawFixed
      ItemHeight = 17
      ItemIndex = 0
      TabOrder = 0
      Text = '2019'
      OnSelect = sComboBox1Select
      Items.Strings = (
        '2019'
        '2018'
        '2017'
        '2016'
        '2015'
        '2014'
        '2013'
        '2012'
        '2011'
        '2010'
        '2009')
    end
    object sSpinEdit1: TsSpinEdit
      Left = 112
      Top = 48
      Width = 89
      Height = 23
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = #44160#49353#45936#51060#53552' '#44079#49688
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      ShowSpinButtons = False
      Increment = 1
    end
    object sButton1: TsButton
      Left = 112
      Top = 104
      Width = 153
      Height = 25
      Caption = #45936#51060#53552' '#44032#51256#50724#44592
      TabOrder = 2
      OnClick = sButton1Click
    end
    object sCheckBox1: TsCheckBox
      Left = 112
      Top = 80
      Width = 149
      Height = 19
      Caption = #44288#47532#48264#54840' '#51473#48373#49884' PASS'
      Checked = True
      State = cbChecked
      TabOrder = 3
      Visible = False
    end
  end
  object Query1: TQuery
    DatabaseName = 'KOM_SINGLE'
    SQL.Strings = (
      'SELECT * FROM PAYORD'
      'WHERE SUBSTRING(DATEE FROM 1 FOR 4) = :Year')
    Left = 24
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Year'
        ParamType = ptUnknown
      end>
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 264
    Top = 48
  end
end
