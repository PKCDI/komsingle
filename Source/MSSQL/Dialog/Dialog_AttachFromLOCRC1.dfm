inherited Dialog_AttachFromLOCRC1_frm: TDialog_AttachFromLOCRC1_frm
  Left = 665
  Top = 253
  Caption = '['#49688#54812#50629#51088'] '#45236#44397#49888#50857#51109' '#47932#54408#49688#47161#51613#47749#49436' '#49440#53469
  ClientHeight = 584
  ClientWidth = 815
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poMainFormCenter
  Visible = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 815
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      815
      41)
    object edt_SearchText: TsEdit
      Left = 102
      Top = 13
      Width = 225
      Height = 23
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object com_SearchKeyword: TsComboBox
      Left = 8
      Top = 13
      Width = 93
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = #48156#44553#51068#51088
      OnSelect = com_SearchKeywordSelect
      Items.Strings = (
        #48156#44553#51068#51088
        'L/C'#48264#54840
        #44288#47532#48264#54840
        '')
    end
    object Mask_SearchDate1: TsMaskEdit
      Left = 102
      Top = 13
      Width = 78
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 328
      Top = 13
      Width = 66
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object sBitBtn21: TsBitBtn
      Tag = 900
      Left = 179
      Top = 13
      Width = 20
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = sBitBtn21Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object Mask_SearchDate2: TsMaskEdit
      Left = 222
      Top = 13
      Width = 82
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn23: TsBitBtn
      Tag = 901
      Left = 303
      Top = 13
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      OnClick = sBitBtn21Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object sPanel25: TsPanel
      Left = 198
      Top = 13
      Width = 25
      Height = 23
      Caption = '~'
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
    end
    object sBitBtn2: TsBitBtn
      Left = 743
      Top = 13
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 8
      OnClick = sBitBtn2Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 673
      Top = 13
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 9
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 41
    Width = 815
    Height = 543
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Caption = #44288#47532#48264#54840
        Width = 213
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOC_NO'
        Title.Caption = 'L/C '#48264#54840' '
        Width = 122
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'APPLIC1'
        Title.Caption = #44060#49444#50629#52404#49345#54840
        Width = 213
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ISS_DAT'
        Title.Caption = #48156#44553#51068#51088
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GET_DAT'
        Title.Caption = #51064#49688#51068#51088
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RFF_NO'
        Title.Caption = #48156#44553#48264#54840
        Width = 213
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOC1AMT'
        Title.Caption = #44060#49444#44552#50529'('#50808#54868')'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOC1AMTC'
        Title.Caption = #53685#54868
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOC2AMT'
        Title.Caption = #44060#49444#44552#50529'('#50896#54868')'
        Width = 100
        Visible = True
      end>
  end
  object sDBGrid2: TsDBGrid [2]
    Left = 8
    Top = 488
    Width = 745
    Height = 73
    Color = clWhite
    DataSource = dsGoods
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    Visible = False
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'KEYY'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SEQ'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HS_CHK'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HS_NO'
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME_CHK'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME_COD'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Width = 124
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME1'
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SIZE'
        Width = 124
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SIZE1'
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTY'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTY_G'
        Width = 34
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTYG'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTYG_G'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE_G'
        Width = 46
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AMT'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AMT_G'
        Width = 34
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STQTY'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STQTY_G'
        Width = 46
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STAMT'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STAMT_G'
        Width = 46
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 64
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO,USER_ID,DATEE,MESSAGE1,MESSAGE2,RFF_NO,GET_DAT,I' +
        'SS_DAT,EXP_DAT,BENEFC,BENEFC1,APPLIC,APPLIC1,APPLIC2,APPLIC3,APP' +
        'LIC4,APPLIC5,APPLIC6 '
      
        '            ,RCT_AMT1,RCT_AMT1C,RCT_AMT2,RCT_AMT2C,RATE,REMARK,R' +
        'EMARK1,TQTY,TQTY_G,TAMT,TAMT_G,LOC_NO,AP_BANK,AP_BANK1,AP_BANK2,' +
        'AP_BANK3,AP_BANK4,AP_NAME'
      
        '            ,LOC1AMT,LOC1AMTC,LOC2AMT,LOC2AMTC,EX_RATE,LOADDATE,' +
        'EXPDATE,LOC_REM,LOC_REM1,CHK1,CHK2,CHK3,NEGODT,NEGOAMT,BSN_HSCOD' +
        'E,PRNO,APPLIC7,BENEFC2,CK_S'
      'FROM LOCRC1_H')
    Left = 8
    Top = 104
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListGET_DAT: TStringField
      FieldName = 'GET_DAT'
      Size = 8
    end
    object qryListISS_DAT: TStringField
      FieldName = 'ISS_DAT'
      Size = 8
    end
    object qryListEXP_DAT: TStringField
      FieldName = 'EXP_DAT'
      Size = 8
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListAPPLIC6: TStringField
      FieldName = 'APPLIC6'
      Size = 35
    end
    object qryListRCT_AMT1: TBCDField
      FieldName = 'RCT_AMT1'
      Precision = 18
    end
    object qryListRCT_AMT1C: TStringField
      FieldName = 'RCT_AMT1C'
      Size = 3
    end
    object qryListRCT_AMT2: TBCDField
      FieldName = 'RCT_AMT2'
      Precision = 18
    end
    object qryListRCT_AMT2C: TStringField
      FieldName = 'RCT_AMT2C'
      Size = 3
    end
    object qryListRATE: TBCDField
      FieldName = 'RATE'
      Precision = 18
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qryListTQTY_G: TStringField
      FieldName = 'TQTY_G'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      Precision = 18
    end
    object qryListTAMT_G: TStringField
      FieldName = 'TAMT_G'
      Size = 3
    end
    object qryListLOC_NO: TStringField
      FieldName = 'LOC_NO'
      Size = 35
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_NAME: TStringField
      FieldName = 'AP_NAME'
      Size = 17
    end
    object qryListLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qryListLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 3
    end
    object qryListLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qryListLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 3
    end
    object qryListEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListLOC_REM: TStringField
      FieldName = 'LOC_REM'
      Size = 1
    end
    object qryListLOC_REM1: TMemoField
      FieldName = 'LOC_REM1'
      BlobType = ftMemo
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListNEGODT: TStringField
      FieldName = 'NEGODT'
      Size = 8
    end
    object qryListNEGOAMT: TBCDField
      FieldName = 'NEGOAMT'
      Precision = 18
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListAPPLIC7: TStringField
      FieldName = 'APPLIC7'
      Size = 10
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 10
    end
    object qryListCK_S: TStringField
      FieldName = 'CK_S'
      Size = 1
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 104
  end
  object qryGoods: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT KEYY,SEQ,HS_CHK,HS_NO,NAME_CHK,NAME_COD,NAME,NAME1,SIZE,S' +
        'IZE1,QTY,QTY_G,QTYG,QTYG_G,PRICE,PRICE_G,AMT,AMT_G,STQTY,STQTY_G' +
        ',STAMT,STAMT_G'
      'FROM LOCRC1_D')
    Left = 8
    Top = 136
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryGoodsHS_CHK: TStringField
      FieldName = 'HS_CHK'
      Size = 1
    end
    object qryGoodsHS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 10
    end
    object qryGoodsNAME_CHK: TStringField
      FieldName = 'NAME_CHK'
      Size = 1
    end
    object qryGoodsNAME_COD: TStringField
      FieldName = 'NAME_COD'
      Size = 35
    end
    object qryGoodsNAME: TStringField
      FieldName = 'NAME'
    end
    object qryGoodsNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryGoodsSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryGoodsSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryGoodsQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryGoodsQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryGoodsQTYG: TBCDField
      FieldName = 'QTYG'
      Precision = 18
    end
    object qryGoodsQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryGoodsPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryGoodsPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryGoodsAMT: TBCDField
      FieldName = 'AMT'
      Precision = 18
    end
    object qryGoodsAMT_G: TStringField
      FieldName = 'AMT_G'
      Size = 3
    end
    object qryGoodsSTQTY: TBCDField
      FieldName = 'STQTY'
      Precision = 18
    end
    object qryGoodsSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryGoodsSTAMT: TBCDField
      FieldName = 'STAMT'
      Precision = 18
    end
    object qryGoodsSTAMT_G: TStringField
      FieldName = 'STAMT_G'
      Size = 3
    end
  end
  object dsGoods: TDataSource
    DataSet = qryGoods
    Left = 40
    Top = 136
  end
end
