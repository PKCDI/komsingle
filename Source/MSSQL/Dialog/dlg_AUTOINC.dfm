inherited dlg_AUTOINC_frm: Tdlg_AUTOINC_frm
  Left = 882
  Top = 214
  BorderIcons = [biSystemMenu]
  Caption = #51228#52636#48264#54840' '#44288#47532
  ClientHeight = 484
  ClientWidth = 332
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 332
    Height = 484
    SkinData.SkinSection = 'TRANSPARENT'
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 1
      Width = 330
      Height = 482
      Align = alClient
      Color = clGray
      Ctl3D = True
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomFont = True
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'DOCID'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #47928#49436#53076#46300
          Width = 110
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'DEPTCODE'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = 'ID'
          Width = 44
          Visible = True
        end
        item
          Color = clBtnFace
          Expanded = False
          FieldName = 'REGDATE'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #51228#52636#44396#48516
          Width = 64
          Visible = True
        end
        item
          Alignment = taRightJustify
          Color = clWhite
          Expanded = False
          FieldName = 'LASTSEQ'
          Title.Alignment = taCenter
          Title.Caption = #48264#54840
          Width = 64
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 48
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'sID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = '1'
      end>
    SQL.Strings = (
      
        'SELECT DOCID, DEPTCODE, REGDATE, LASTSEQ FROM [AUTOINCR] WHERE D' +
        'EPTCODE = :sID')
    Left = 64
    Top = 56
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 96
    Top = 56
  end
end
