inherited Dialog_AttachFromLOCAD1_frm: TDialog_AttachFromLOCAD1_frm
  Left = 348
  Top = 313
  Caption = #45236#44397#49888#50857#51109' '#53685#51648#49436' '#49440#53469
  ClientHeight = 577
  ClientWidth = 815
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poMainFormCenter
  Visible = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 815
    Height = 73
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      815
      73)
    object edt_SearchText: TsEdit
      Left = 102
      Top = 45
      Width = 225
      Height = 23
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object com_SearchKeyword: TsComboBox
      Left = 8
      Top = 45
      Width = 93
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = #44060#49444#51068#51088
      OnSelect = com_SearchKeywordSelect
      Items.Strings = (
        #44060#49444#51068#51088
        'L/C'#48264#54840
        #49688#54812#51088)
    end
    object Mask_SearchDate1: TsMaskEdit
      Left = 102
      Top = 45
      Width = 78
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 328
      Top = 45
      Width = 66
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object sBitBtn21: TsBitBtn
      Tag = 900
      Left = 179
      Top = 45
      Width = 20
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = sBitBtn21Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object Mask_SearchDate2: TsMaskEdit
      Left = 222
      Top = 45
      Width = 82
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn23: TsBitBtn
      Tag = 901
      Left = 303
      Top = 45
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      OnClick = sBitBtn21Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object sPanel25: TsPanel
      Left = 198
      Top = 45
      Width = 25
      Height = 23
      Caption = '~'
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
    end
    object sBitBtn2: TsBitBtn
      Left = 743
      Top = 45
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 8
      OnClick = sBitBtn2Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 673
      Top = 45
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 9
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
    object sRadioGroup1: TsRadioGroup
      Left = 8
      Top = 0
      Width = 449
      Height = 38
      TabOrder = 10
      SkinData.SkinSection = 'GROUPBOX'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        '['#49688#54812#50629#51088'] '#45236#44397#49888#50857#51109' '#53685#51648#49436
        '['#44060#49444#50629#51088'] '#45236#44397#49888#50857#51109' '#44060#49444#51025#45813#49436)
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 73
    Width = 815
    Height = 504
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Caption = #44288#47532#48264#54840
        Width = 213
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LC_NO'
        Title.Caption = 'L/C'#48264#54840
        Width = 122
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'APPLIC1'
        Title.Caption = #44060#49444#51088
        Width = 176
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ISS_DATE'
        Title.Caption = #44060#49444#51068#51088
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ISS_DATE'
        Title.Caption = #44060#49444#51068#51088
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOC1AMT'
        Title.Caption = #44060#49444#44552#50529'('#50808#54868')'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOC1AMTC'
        Title.Caption = #53685#54868
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOC2AMT'
        Title.Caption = #44060#49444#44552#50529'('#50896#54868')'
        Width = 100
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 120
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 152
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ''
      
        '    SELECT MAINT_NO,[USER_ID],DATEE,MESSAGE1,MESSAGE2,APPLIC1,AP' +
        'PLIC2,APPLIC3,APPADDR1,APPADDR2,APPADDR3,BENEFC1,BENEFC2,BENEFC3' +
        ',BNFADDR1,BNFADDR2,BNFADDR3,AP_BANK,AP_BANK1,AP_BANK2'
      
        #9'  ,LOC_TYPE,LOC2AMTC,LOC2AMT,LOC1AMTC,LOC1AMT,CD_PERP,CD_PERM,E' +
        'X_RATE,BUSINESS,OFFERNO1,OFFERNO2,OFFERNO3,OFFERNO4,OFFERNO5,OFF' +
        'ERNO6,OFFERNO7,OFFERNO8,OFFERNO9'
      
        #9'  ,DOCCOPY1,DOCCOPY2,DOCCOPY3,DOCCOPY4,DOCCOPY5,OPEN_NO,ISS_DAT' +
        'E,ADV_DATE,DOC_PRD,LC_NO,DELIVERY,EXPIRY,TRANSPRT,BSN_HSCODE,GOO' +
        'DDES,GOODDES1,DOC_ETC,DOC_ETC'
      
        #9'  ,REMARK,REMARK1,DOC_DTL,DOC_NO,LOADDATE,EXPDATE,DEST,ISBANK1,' +
        'ISBANK2,PAYMENT,EXGOOD,EXGOOD1,EXNAME1,EXNAME2,EXNAME3,BNFEMAILI' +
        'D,BNFDOMAIN,CHK1,CHK2,CHK3'
      '                  ,N4025.DOC_NAME as BUSINESSNAME'
      '                 ,PSHIP.DOC_NAME as TRANSPRTNAME'
      '                 ,N4487.DOC_NAME as LOC_TYPENAME'
      '                 ,N1001.DOC_NAME as DOC_DTLNAME'
      '                 ,N4277.DOC_NAME as PAYMENTNAME'
      '                 ,NAT.DOC_NAME as DESTNAME'
      
        'FROM LOCAD1 LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2ND' +
        'D with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON LOCAD1.LOC_TYP' +
        'E = N4487.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON L' +
        'OCAD1.BUSINESS = N4025.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOC' +
        'AD1.TRANSPRT = PSHIP.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON L' +
        'OCAD1.PAYMENT = N4277.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON L' +
        'OCAD1.DOC_DTL = N1001.CODE'
      
        #9'         LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD ' +
        'with(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCAD1.DEST = NAT.CODE')
    Left = 8
    Top = 152
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object qryListLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qryListLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object qryListLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListGOODDES1: TMemoField
      FieldName = 'GOODDES1'
      BlobType = ftMemo
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListDOC_ETC_1: TStringField
      FieldName = 'DOC_ETC_1'
      Size = 1
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListEXGOOD1: TMemoField
      FieldName = 'EXGOOD1'
      BlobType = ftMemo
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryListTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object qryListDOC_DTLNAME: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object qryListPAYMENTNAME: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
  end
  object qryList2: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ''
      
        '    SELECT MAINT_NO,[USER_ID],DATEE,MESSAGE1,MESSAGE2,APPLIC1,AP' +
        'PLIC2,APPLIC3,APPADDR1,APPADDR2,APPADDR3,BENEFC1,BENEFC2,BENEFC3' +
        ',BNFADDR1,BNFADDR2,BNFADDR3,AP_BANK,AP_BANK1,AP_BANK2'
      
        #9'  ,LOC_TYPE,LOC2AMTC,LOC2AMT,LOC1AMTC,LOC1AMT,CD_PERP,CD_PERM,E' +
        'X_RATE,BUSINESS,OFFERNO1,OFFERNO2,OFFERNO3,OFFERNO4,OFFERNO5,OFF' +
        'ERNO6,OFFERNO7,OFFERNO8,OFFERNO9'
      
        #9'  ,DOCCOPY1,DOCCOPY2,DOCCOPY3,DOCCOPY4,DOCCOPY5,OPEN_NO,ISS_DAT' +
        'E,ADV_DATE,DOC_PRD,LC_NO,DELIVERY,EXPIRY,TRANSPRT,BSN_HSCODE,GOO' +
        'DDES,GOODDES1,DOC_ETC,DOC_ETC'
      
        #9'  ,REMARK,REMARK1,DOC_DTL,DOC_NO,LOADDATE,EXPDATE,DEST,ISBANK1,' +
        'ISBANK2,PAYMENT,EXGOOD,EXGOOD1,EXNAME1,EXNAME2,EXNAME3,BNFEMAILI' +
        'D,BNFDOMAIN,CHK1,CHK2,CHK3'
      '                  ,N4025.DOC_NAME as BUSINESSNAME'
      '                 ,PSHIP.DOC_NAME as TRANSPRTNAME'
      '                 ,N4487.DOC_NAME as LOC_TYPENAME'
      '                 ,N1001.DOC_NAME as DOC_DTLNAME'
      '                 ,N4277.DOC_NAME as PAYMENTNAME'
      '                 ,NAT.DOC_NAME as DESTNAME'
      
        'FROM LOCADV LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2ND' +
        'D with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON LOCADV.LOC_TYP' +
        'E = N4487.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON L' +
        'OCADV.BUSINESS = N4025.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOC' +
        'ADV.TRANSPRT = PSHIP.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON L' +
        'OCADV.PAYMENT = N4277.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON L' +
        'OCADV.DOC_DTL = N1001.CODE'
      
        #9'         LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD ' +
        'with(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCADV.DEST = NAT.CODE')
    Left = 8
    Top = 192
    object StringField1: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object StringField2: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object StringField3: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object StringField4: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object StringField5: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object StringField6: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object StringField7: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object StringField8: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object StringField9: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object StringField10: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object StringField11: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object StringField12: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object StringField13: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object StringField14: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object StringField15: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object StringField16: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object StringField17: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object StringField18: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object StringField19: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object StringField20: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object StringField21: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object BCDField1: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object StringField22: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object BCDField2: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object IntegerField1: TIntegerField
      FieldName = 'CD_PERP'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CD_PERM'
    end
    object BCDField3: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object StringField23: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object StringField24: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object StringField25: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object StringField26: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object StringField27: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object StringField28: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object StringField29: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object StringField30: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object StringField31: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object StringField32: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object BCDField4: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object BCDField5: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object BCDField6: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object BCDField7: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object BCDField8: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object BCDField9: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object StringField33: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object StringField34: TStringField
      FieldName = 'ADV_DATE'
      Size = 8
    end
    object BCDField10: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object StringField35: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object StringField36: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object StringField37: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object StringField38: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object StringField39: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object StringField40: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object MemoField1: TMemoField
      FieldName = 'GOODDES1'
      BlobType = ftMemo
    end
    object StringField41: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object StringField42: TStringField
      FieldName = 'DOC_ETC_1'
      Size = 1
    end
    object StringField43: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object MemoField2: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object StringField44: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object StringField45: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object StringField46: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object StringField47: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object StringField48: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object StringField49: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object StringField50: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object StringField51: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object StringField52: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object MemoField3: TMemoField
      FieldName = 'EXGOOD1'
      BlobType = ftMemo
    end
    object StringField53: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object StringField54: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object StringField55: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object StringField56: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object StringField57: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object StringField58: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object StringField59: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object StringField60: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object StringField61: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object StringField62: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object StringField63: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object StringField64: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object StringField65: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object StringField66: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
    object StringField67: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
  end
end
