object dlg_Memoviewer_frm: Tdlg_Memoviewer_frm
  Left = 427
  Top = 95
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = 'dlg_Memoviewer_frm'
  ClientHeight = 685
  ClientWidth = 814
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 814
    Height = 41
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alTop
    
    TabOrder = 0
    object sButton1: TsButton
      Left = 528
      Top = 0
      Width = 286
      Height = 40
      Cursor = crHandPoint
      Caption = 'Close Description of Goods and/or Services'
      TabOrder = 0
      OnClick = sButton1Click
      Reflected = True
      Images = DMICON.System24
      ImageIndex = 18
    end
  end
  object memo_desc: TsMemo
    Left = 0
    Top = 41
    Width = 814
    Height = 644
    Align = alClient
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
end
