object Dialog_SearchCustom_frm: TDialog_SearchCustom_frm
  Left = 929
  Top = 258
  BorderIcons = []
  BorderStyle = bsDialog
  BorderWidth = 4
  ClientHeight = 593
  ClientWidth = 475
  Color = clBtnFace
  Font.Charset = HANGEUL_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 15
  object sSplitter3: TsSplitter
    Left = 0
    Top = 33
    Width = 475
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter1: TsSplitter
    Left = 0
    Top = 69
    Width = 475
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 36
    Width = 475
    Height = 33
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object com_SearchKeyword: TsComboBox
      Left = 16
      Top = 5
      Width = 81
      Height = 23
      Alignment = taLeftJustify
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 1
      ParentFont = False
      TabOrder = 0
      Text = #49324#50629#51088#47749
      Items.Strings = (
        #53076#46300
        #49324#50629#51088#47749)
    end
    object btn_Search: TsBitBtn
      Left = 382
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 1
      OnClick = btn_SearchClick
      ImageIndex = 6
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_Search: TsEdit
      Left = 98
      Top = 5
      Width = 283
      Height = 23
      Color = clWhite
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnKeyUp = edt_SearchKeyUp
      SkinData.SkinSection = 'EDIT'
    end
  end
  object sDBGrid1: TsDBGrid
    Left = 0
    Top = 72
    Width = 475
    Height = 521
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = HANGEUL_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = HANGEUL_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    OnKeyUp = sDBGrid1KeyUp
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #53076#46300
        Width = 50
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'SAUP_NO'
        Title.Alignment = taCenter
        Title.Caption = #49324#50629#51088#46321#47197#48264#54840
        Width = 92
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENAME'
        Title.Caption = #49324#50629#51088#47749
        Width = 200
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'REP_NAME'
        Title.Alignment = taCenter
        Title.Caption = #45824#54364#51088
        Width = 100
        Visible = True
      end>
  end
  object sPanel2: TsPanel
    Left = 0
    Top = 0
    Width = 475
    Height = 33
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      475
      33)
    object sSpeedButton5: TsSpeedButton
      Left = 266
      Top = -2
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sBitBtn3: TsBitBtn
      Left = 17
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52628#44032
      TabOrder = 0
      OnClick = sBitBtn3Click
      ImageIndex = 2
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn6: TsBitBtn
      Tag = 1
      Left = 88
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49688#51221
      TabOrder = 1
      OnClick = sBitBtn3Click
      ImageIndex = 3
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn4: TsBitBtn
      Tag = 2
      Left = 159
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49325#51228
      TabOrder = 2
      OnClick = sBitBtn3Click
      ImageIndex = 27
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn2: TsBitBtn
      Left = 311
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #54869#51064
      ModalResult = 2
      TabOrder = 3
      OnClick = sBitBtn2Click
      ImageIndex = 17
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn1: TsBitBtn
      Left = 382
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Caption = #45803#44592
      ModalResult = 2
      TabOrder = 4
      ImageIndex = 18
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 128
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      
        '           CODE, TRAD_NO, SAUP_NO, ENAME, REP_NAME, REP_TEL, REP' +
        '_FAX, DDAN_DEPT, DDAN_NAME, DDAN_TEL, DDAN_FAX, ZIPCD, ADDR1, AD' +
        'DR2, ADDR3, CITY, NAT, ESU1, ESU2, ESU3, Jenja, ADDR4, ADDR5, EM' +
        'AIL_ID, EMAIL_DOMAIN'
      'FROM CUSTOM')
    Left = 24
    Top = 128
  end
end
