object dlg_UserControl_frm: Tdlg_UserControl_frm
  Left = 939
  Top = 336
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = #45824#49345#44144#47000#52376
  ClientHeight = 177
  ClientWidth = 378
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel3: TsPanel
    Left = 0
    Top = 0
    Width = 378
    Height = 177
    Align = alClient
    
    TabOrder = 0
    object sImage1: TsImage
      Left = 8
      Top = 8
      Width = 24
      Height = 24
      AutoSize = True
      Picture.Data = {07544269746D617000000000}
      ImageIndex = 37
      Images = DMICON.System26
      SkinData.SkinSection = 'CHECKBOX'
    end
    object sLabel1: TsLabel
      Left = 36
      Top = 13
      Width = 88
      Height = 15
      Caption = #45824#49345#44144#47000#52376' '#52628#44032
    end
    object edt_CODE: TsEdit
      Left = 48
      Top = 40
      Width = 57
      Height = 23
      CharCase = ecUpperCase
      MaxLength = 12
      TabOrder = 0
      BoundLabel.Active = True
      BoundLabel.Caption = #53076#46300
    end
    object edt_Name: TsEdit
      Left = 48
      Top = 64
      Width = 313
      Height = 23
      CharCase = ecUpperCase
      TabOrder = 1
      BoundLabel.Active = True
      BoundLabel.Caption = #50629#52404#47749
    end
    object chk_Used: TsCheckBox
      Left = 46
      Top = 96
      Width = 90
      Height = 19
      Caption = #49569#49688#49888' '#49324#50857
      TabOrder = 2
    end
    object QRShape1: TQRShape
      Left = 9
      Top = 122
      Width = 360
      Height = 2
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        5.291666666666670000
        23.812500000000000000
        322.791666666667000000
        952.500000000000000000)
      Brush.Color = 13882332
      Pen.Style = psClear
      Shape = qrsRectangle
    end
    object btnOK: TsButton
      Left = 111
      Top = 129
      Width = 75
      Height = 39
      Caption = #54869#51064
      ModalResult = 1
      TabOrder = 4
      Reflected = True
      Images = DMICON.System24
      ImageIndex = 17
    end
    object btnCancel: TsButton
      Left = 191
      Top = 129
      Width = 75
      Height = 39
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 5
      Reflected = True
      Images = DMICON.System24
      ImageIndex = 18
    end
  end
end
