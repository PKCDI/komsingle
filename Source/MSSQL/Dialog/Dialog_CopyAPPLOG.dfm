inherited Dialog_CopyAPPLOG_frm: TDialog_CopyAPPLOG_frm
  Left = 721
  Top = 268
  Caption = #48373#49324#54624' '#50896#48376' '#47928#49436' '#49440#53469
  ClientHeight = 558
  ClientWidth = 900
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poOwnerFormCenter
  Visible = False
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel2: TsPanel [0]
    Left = 0
    Top = 0
    Width = 900
    Height = 35
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      900
      35)
    object edt_SearchText: TsEdit
      Left = 98
      Top = 5
      Width = 229
      Height = 23
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object com_SearchKeyword: TsComboBox
      Left = 4
      Top = 5
      Width = 93
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = #46321#47197#51068#51088
      OnSelect = com_SearchKeywordSelect
      Items.Strings = (
        #46321#47197#51068#51088
        #44288#47532#48264#54840
        #49888#50857#51109#48264#54840)
    end
    object Mask_SearchDate1: TsMaskEdit
      Left = 98
      Top = 5
      Width = 80
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 331
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object sBitBtn21: TsBitBtn
      Left = 179
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = sBitBtn21Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object Mask_SearchDate2: TsMaskEdit
      Left = 224
      Top = 5
      Width = 82
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn23: TsBitBtn
      Tag = 1
      Left = 305
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object sPanel25: TsPanel
      Left = 200
      Top = 5
      Width = 25
      Height = 23
      Caption = '~'
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
    end
    object sBitBtn2: TsBitBtn
      Left = 827
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 8
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 757
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 9
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 35
    Width = 900
    Height = 523
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    OnDrawColumnCell = sDBGrid1DrawColumnCell
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Alignment = taCenter
        Title.Caption = #44288#47532#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 160
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DATEE'
        Title.Alignment = taCenter
        Title.Caption = #46321#47197#51068#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LC_NO'
        Title.Alignment = taCenter
        Title.Caption = #49888#50857#51109#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CR_NAME1'
        Title.Alignment = taCenter
        Title.Caption = #49440#48149#54924#49324
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'INV_AMT'
        Title.Alignment = taCenter
        Title.Caption = #49345#50629#49569#51109#44552#50529
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 90
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'INV_AMTC'
        Title.Alignment = taCenter
        Title.Caption = #45800#50948
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PAC_QTY'
        Title.Alignment = taCenter
        Title.Caption = #54252#51109#49688
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 100
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'PAC_QTYC'
        Title.Alignment = taCenter
        Title.Caption = #54252#51109#45800#50948
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MS_NAME1'
        Title.Alignment = taCenter
        Title.Caption = #49888#52397#51064
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AX_NAME1'
        Title.Alignment = taCenter
        Title.Caption = #47749#51032#51064
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SE_NAME1'
        Title.Alignment = taCenter
        Title.Caption = #49569#54616#51064
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CARRIER1'
        Title.Alignment = taCenter
        Title.Caption = #49440#44592#47749
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CARRIER2'
        Title.Alignment = taCenter
        Title.Caption = #54637#54644#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 160
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'AR_DATE'
        Title.Alignment = taCenter
        Title.Caption = #46020#52265'('#50696#51221')'#51068
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 80
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'BL_DATE'
        Title.Alignment = taCenter
        Title.Caption = #49440#54616#51613#44428' '#48156#44553#51068#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 80
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'APP_DATE'
        Title.Alignment = taCenter
        Title.Caption = #49888#52397#51068#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LOAD_LOC'
        Title.Alignment = taCenter
        Title.Caption = #49440#51201#54637
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ARR_LOC'
        Title.Alignment = taCenter
        Title.Caption = #46020#52265#54637
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 80
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 64
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 96
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      
        #9'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, MESSAGE3, CR_NAME' +
        '1, CR_NAME2, CR_NAME3, SE_NAME1, SE_NAME2, SE_NAME3, MS_NAME1, M' +
        'S_NAME2, MS_NAME3, AX_NAME1, AX_NAME2, AX_NAME3, INV_AMT, INV_AM' +
        'TC, LC_G, LC_NO, BL_G, BL_NO, CARRIER1, CARRIER2, AR_DATE, BL_DA' +
        'TE, APP_DATE, LOAD_LOC, LOAD_TXT, ARR_LOC, ARR_TXT, SPMARK1, SPM' +
        'ARK2, SPMARK3, SPMARK4, SPMARK5, SPMARK6, SPMARK7, SPMARK8, SPMA' +
        'RK9, SPMARK10, PAC_QTY, PAC_QTYC, GOODS1, GOODS2, GOODS3, GOODS4' +
        ', GOODS5, GOODS6, GOODS7, GOODS8, GOODS9, GOODS10, TRM_PAYC, TRM' +
        '_PAY, BANK_CD, BANK_TXT, BANK_BR, CHK1, CHK2, CHK3, PRNO, CN_NAM' +
        'E1, CN_NAME2, CN_NAME3, B5_NAME1, B5_NAME2, B5_NAME3'
      #9',msg1CODE.MSG1NAME'
      #9',LCGCODE.LCGNAME'
      #9',BLGCODE.BLGNAME'
      #9',LOADCODE.LOADNAME'
      #9',ARRCODE.ARRNAME'
      #9',TRMPAYCCODE.TRMPAYCNAME'
      #9',SUNSACODE.CRNAEM3NAME'
      'FROM APPLOG'
      
        'LEFT JOIN (SELECT CODE,NAME as MSG1NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'APPLOG'#44396#48516#39') msg1CODE ON MESSAGE1 = msg1CODE.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as LCGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39'LC'#44396#48516#39') LCGCODE ON LC_G = LCGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as BLGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#49440#54616#51613#44428#39') BLGCODE ON BL_G = BLGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as LOADNAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') LOADCODE ON LOAD_LOC = LOADCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as ARRNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#44397#44032#39') ARRCODE ON ARR_LOC = ARRCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as TRMPAYCNAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39#44208#51228#44592#44036#39') TRMPAYCCODE ON TRM_PAYC = TRMPAYCCO' +
        'DE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as CRNAEM3NAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39'SUNSA'#39') SUNSACODE ON CR_NAME3 = SUNSACODE.' +
        'CODE')
    Left = 16
    Top = 96
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListMESSAGE3: TStringField
      FieldName = 'MESSAGE3'
      Size = 3
    end
    object qryListCR_NAME1: TStringField
      FieldName = 'CR_NAME1'
      Size = 35
    end
    object qryListCR_NAME2: TStringField
      FieldName = 'CR_NAME2'
      Size = 35
    end
    object qryListCR_NAME3: TStringField
      FieldName = 'CR_NAME3'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListMS_NAME1: TStringField
      FieldName = 'MS_NAME1'
      Size = 35
    end
    object qryListMS_NAME2: TStringField
      FieldName = 'MS_NAME2'
      Size = 35
    end
    object qryListMS_NAME3: TStringField
      FieldName = 'MS_NAME3'
      Size = 35
    end
    object qryListAX_NAME1: TStringField
      FieldName = 'AX_NAME1'
      Size = 35
    end
    object qryListAX_NAME2: TStringField
      FieldName = 'AX_NAME2'
      Size = 35
    end
    object qryListAX_NAME3: TStringField
      FieldName = 'AX_NAME3'
      Size = 35
    end
    object qryListINV_AMT: TBCDField
      FieldName = 'INV_AMT'
      Precision = 18
    end
    object qryListINV_AMTC: TStringField
      FieldName = 'INV_AMTC'
      Size = 3
    end
    object qryListLC_G: TStringField
      FieldName = 'LC_G'
      Size = 3
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListBL_G: TStringField
      FieldName = 'BL_G'
      Size = 3
    end
    object qryListBL_NO: TStringField
      FieldName = 'BL_NO'
      Size = 35
    end
    object qryListCARRIER1: TStringField
      FieldName = 'CARRIER1'
      Size = 17
    end
    object qryListCARRIER2: TStringField
      FieldName = 'CARRIER2'
      Size = 35
    end
    object qryListAR_DATE: TStringField
      FieldName = 'AR_DATE'
      Size = 8
    end
    object qryListBL_DATE: TStringField
      FieldName = 'BL_DATE'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListLOAD_LOC: TStringField
      FieldName = 'LOAD_LOC'
      Size = 3
    end
    object qryListLOAD_TXT: TStringField
      FieldName = 'LOAD_TXT'
      Size = 70
    end
    object qryListARR_LOC: TStringField
      FieldName = 'ARR_LOC'
      Size = 3
    end
    object qryListARR_TXT: TStringField
      FieldName = 'ARR_TXT'
      Size = 70
    end
    object qryListSPMARK1: TStringField
      FieldName = 'SPMARK1'
      Size = 35
    end
    object qryListSPMARK2: TStringField
      FieldName = 'SPMARK2'
      Size = 35
    end
    object qryListSPMARK3: TStringField
      FieldName = 'SPMARK3'
      Size = 35
    end
    object qryListSPMARK4: TStringField
      FieldName = 'SPMARK4'
      Size = 35
    end
    object qryListSPMARK5: TStringField
      FieldName = 'SPMARK5'
      Size = 35
    end
    object qryListSPMARK6: TStringField
      FieldName = 'SPMARK6'
      Size = 35
    end
    object qryListSPMARK7: TStringField
      FieldName = 'SPMARK7'
      Size = 35
    end
    object qryListSPMARK8: TStringField
      FieldName = 'SPMARK8'
      Size = 35
    end
    object qryListSPMARK9: TStringField
      FieldName = 'SPMARK9'
      Size = 35
    end
    object qryListSPMARK10: TStringField
      FieldName = 'SPMARK10'
      Size = 35
    end
    object qryListPAC_QTY: TBCDField
      FieldName = 'PAC_QTY'
      Precision = 18
    end
    object qryListPAC_QTYC: TStringField
      FieldName = 'PAC_QTYC'
      Size = 3
    end
    object qryListGOODS1: TStringField
      FieldName = 'GOODS1'
      Size = 70
    end
    object qryListGOODS2: TStringField
      FieldName = 'GOODS2'
      Size = 70
    end
    object qryListGOODS3: TStringField
      FieldName = 'GOODS3'
      Size = 70
    end
    object qryListGOODS4: TStringField
      FieldName = 'GOODS4'
      Size = 70
    end
    object qryListGOODS5: TStringField
      FieldName = 'GOODS5'
      Size = 70
    end
    object qryListGOODS6: TStringField
      FieldName = 'GOODS6'
      Size = 70
    end
    object qryListGOODS7: TStringField
      FieldName = 'GOODS7'
      Size = 70
    end
    object qryListGOODS8: TStringField
      FieldName = 'GOODS8'
      Size = 70
    end
    object qryListGOODS9: TStringField
      FieldName = 'GOODS9'
      Size = 70
    end
    object qryListGOODS10: TStringField
      FieldName = 'GOODS10'
      Size = 70
    end
    object qryListTRM_PAYC: TStringField
      FieldName = 'TRM_PAYC'
      Size = 3
    end
    object qryListTRM_PAY: TStringField
      FieldName = 'TRM_PAY'
      Size = 35
    end
    object qryListBANK_CD: TStringField
      FieldName = 'BANK_CD'
      Size = 4
    end
    object qryListBANK_TXT: TStringField
      FieldName = 'BANK_TXT'
      Size = 70
    end
    object qryListBANK_BR: TStringField
      FieldName = 'BANK_BR'
      Size = 70
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListCN_NAME1: TStringField
      FieldName = 'CN_NAME1'
      Size = 35
    end
    object qryListCN_NAME2: TStringField
      FieldName = 'CN_NAME2'
      Size = 35
    end
    object qryListCN_NAME3: TStringField
      FieldName = 'CN_NAME3'
      Size = 35
    end
    object qryListB5_NAME1: TStringField
      FieldName = 'B5_NAME1'
      Size = 35
    end
    object qryListB5_NAME2: TStringField
      FieldName = 'B5_NAME2'
      Size = 35
    end
    object qryListB5_NAME3: TStringField
      FieldName = 'B5_NAME3'
      Size = 35
    end
    object qryListMSG1NAME: TStringField
      FieldName = 'MSG1NAME'
      Size = 100
    end
    object qryListLCGNAME: TStringField
      FieldName = 'LCGNAME'
      Size = 100
    end
    object qryListBLGNAME: TStringField
      FieldName = 'BLGNAME'
      Size = 100
    end
    object qryListLOADNAME: TStringField
      FieldName = 'LOADNAME'
      Size = 100
    end
    object qryListARRNAME: TStringField
      FieldName = 'ARRNAME'
      Size = 100
    end
    object qryListTRMPAYCNAME: TStringField
      FieldName = 'TRMPAYCNAME'
      Size = 100
    end
    object qryListCRNAEM3NAME: TStringField
      FieldName = 'CRNAEM3NAME'
      Size = 100
    end
  end
end
