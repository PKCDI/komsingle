unit Dialog_MemoList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, StdCtrls, sEdit, Buttons, sBitBtn, sComboBox,
  sSkinProvider, ExtCtrls, sPanel, DBCtrls, sDBMemo, DB, ADODB, Grids,
  DBGrids, acDBGrid, sRadioButton, sGroupBox, sMemo, sSplitter,
  sSpeedButton;

type
  TDialog_MemoList_frm = class(TDialogParent_frm)
    sPanel2: TsPanel;
    com_SearchKeyword: TsComboBox;
    btn_Search: TsBitBtn;
    edt_Search: TsEdit;
    sDBGrid1: TsDBGrid;
    dsList: TDataSource;
    qryList: TADOQuery;
    sMemo1: TsMemo;
    qryListD_MEMO: TMemoField;
    qryListCODE: TStringField;
    qryListSUBJECT: TStringField;
    sSplitter3: TsSplitter;
    sPanel4: TsPanel;
    codeRadioGroup: TsRadioGroup;
    writeRadioGroup: TsRadioGroup;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sBitBtn3: TsBitBtn;
    sBitBtn1: TsBitBtn;
    sBitBtn4: TsBitBtn;
    sBitBtn6: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sSpeedButton2: TsSpeedButton;
    sSplitter1: TsSplitter;
    qryItem: TADOQuery;
    qryItemCODE: TStringField;
    qryItemHSCODE: TStringField;
    qryItemFNAME: TMemoField;
    qryItemSNAME: TStringField;
    qryItemMSIZE: TMemoField;
    qryItemQTYC: TStringField;
    qryItemPRICEG: TStringField;
    qryItemPRICE: TBCDField;
    qryItemQTY_U: TBCDField;
    qryItemQTY_UG: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btn_SearchClick(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure RadioGroupClick(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sBitBtn2Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);

  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog(): TModalResult;
  end;

var
  Dialog_MemoList_frm: TDialog_MemoList_frm;

implementation

uses UI_APP700, Dlg_MEMO, TypeDefine, Dlg_ITEM;
{$R *.dfm}

{ TDialog_MemoList_frm }

function TDialog_MemoList_frm.openDialog(): TModalResult;
begin

  ReadList;
  
  Result := ShowModal;

end;

procedure TDialog_MemoList_frm.ReadList;
begin

  with qryList do
  begin

    Close;
    sql.Clear;
    SQL.Text := FSQL;

    if com_SearchKeyword.ItemIndex = 0 then
    begin
      SQL.Add(' WHERE [CODE] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
    end
    else if com_SearchKeyword.ItemIndex = 1 then
    begin
      SQL.Add(' WHERE [SUBJECT] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
    end;

    Open;
  end;
end;

procedure TDialog_MemoList_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_MemoList_frm.btn_SearchClick(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_MemoList_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;

end;

procedure TDialog_MemoList_frm.RadioGroupClick(Sender: TObject);
begin
  inherited;

    if codeRadioGroup.ItemIndex = 0 then
    begin

      dsList.DataSet := qryList;
      with qryList do
      begin

        Close;
        sDBGrid1.Columns[1].FieldName := 'SUBJECT';
        Open;

        Dialog_MemoList_frm.Caption := '皋 葛 内 靛 格 废';
      end;
    end
    else if codeRadioGroup.ItemIndex = 1 then
    begin
      dsList.DataSet := qryItem;
      with qryItem do
      begin
        Close;
        sDBGrid1.Columns[1].FieldName := 'SNAME';
        Open;
        Dialog_MemoList_frm.Caption := '力 前 内 靛 格 废';
      end;
    end;
end;

procedure TDialog_MemoList_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  if codeRadioGroup.ItemIndex = 0 then
    sMemo1.Text := qryListD_MEMO.AsString
  else if codeRadioGroup.ItemIndex = 1 then
    sMemo1.Text := qryItemFNAME.AsString;


end;

procedure TDialog_MemoList_frm.sBitBtn2Click(Sender: TObject);
var
  memo_Code : String;
  item_Code : String;
begin
  inherited;
  if codeRadioGroup.ItemIndex = 0 then
  begin

    Dlg_MEMO_frm := TDlg_MEMO_frm.Create(Self);

    try
      case (Sender as TsBitBtn).Tag of
        0 :
        begin
          if Dlg_MEMO_frm.Run(ctInsert,nil) = mrOK then
          begin
            memo_Code :=  Dlg_MEMO_frm.edt_CODE.Text;
            qryList.Close;
            qryList.Open;
            qryList.Locate('CODE',memo_Code,[]);
          end;
        end;
        1 :
        begin
          if Dlg_MEMO_frm.Run(ctModify,dsList.DataSet.Fields) = mrOK then
          begin
            memo_Code :=  Dlg_MEMO_frm.edt_CODE.Text;
            qryList.Close;
            qryList.Open;
            qryList.Locate('CODE',memo_Code,[]);
          end;
        end;
        2:
        begin
          if Dlg_MEMO_frm.Run(ctDelete,dsList.DataSet.Fields) = mrOK then
          begin
            qryList.Close;
            qryList.Open;
          end;
        end;
      end;
    finally
      FreeAndNil(Dlg_MEMO_frm);
    end;
  end
  else if codeRadioGroup.ItemIndex = 1 then
  begin
    Dlg_ITEM_frm := TDlg_ITEM_frm.Create(Self);

    try
      case (Sender as TsBitBtn).Tag of
        0 :
        begin
          if Dlg_ITEM_frm.Run(ctInsert,nil) = mrOK then
          begin
            item_Code :=  Dlg_ITEM_frm.edt_CODE.Text;
            qryItem.Close;
            qryItem.Open;
            qryList.Locate('CODE',item_Code,[]);
          end;
        end;
        1 :
        begin
          if Dlg_ITEM_frm.Run(ctModify,dsList.DataSet.Fields) = mrOK then
          begin
            item_Code :=  Dlg_ITEM_frm.edt_CODE.Text;
            qryItem.Close;
            qryItem.Open;
            qryList.Locate('CODE',item_Code,[]);
          end;
        end;
        2:
        begin
          if Dlg_ITEM_frm.Run(ctDelete,dsList.DataSet.Fields) = mrOK then
          begin
            qryItem.Close;
            qryItem.Open;
          end;
        end;
      end;
    finally
      FreeAndNil(Dlg_ITEM_frm);
    end;
  end;

end;

procedure TDialog_MemoList_frm.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  //
end;

end.

