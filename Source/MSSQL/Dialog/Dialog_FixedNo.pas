unit Dialog_FixedNo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, StdCtrls, Buttons, sBitBtn, sEdit, sGroupBox,
  ExtCtrls, sSplitter, ComCtrls, sTreeView, sSkinProvider, sPanel, sButton, ADODB, Clipbrd;

type
  TDialog_FixedNo_frm = class(TDialogParent_frm)
    sTreeView1: TsTreeView;
    sSplitter1: TsSplitter;
    sGroupBox1: TsGroupBox;
    sEdit55: TsEdit;
    sEdit56: TsEdit;
    sEdit57: TsEdit;
    sBitBtn21: TsBitBtn;
    sPanel16: TsPanel;
    sPanel18: TsPanel;
    sEdit58: TsEdit;
    sEdit59: TsEdit;
    sButton1: TsButton;
    sButton2: TsButton;
    procedure sButton2Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReadData;
  public
    { Public declarations }
  end;

var
  Dialog_FixedNo_frm: TDialog_FixedNo_frm;

implementation

uses
  ICON, MSSQL, DB, MessageDefine, CD_APPSPCBANK;

{$R *.dfm}

procedure TDialog_FixedNo_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT ESU1 FROM CUSTOM WHERE CODE = ''00000''';
      Open;
      IF RecordCount > 0 Then
      begin
        sEdit56.Text := FieldByName('ESU1').AsString;
        sEdit59.Text := FieldByName('ESU1').AsString;
      end
      else
        ShowMessage('거래처관리의 코드 "00000"의 거래처가 없습니다');
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TDialog_FixedNo_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  IF MessageBox(Self.Handle, MSG_QUESTION_SAVE, '데이터저장 확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL THEN Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      try
        Connection := DMMssql.KISConnect;
        //식별자저장 LOCAPP용
        SQL.Text := 'SELECT 1 FROM CODE2NDD WHERE CODE = ''LA_BGM1''';
        Open;
        IF RecordCount = 0 then
        begin
          Close;
          SQL.Text := 'INSERT INTO CODE2NDD VALUES(''LOCAPP문서'',''LA_BGM1'',1,'''','+QuotedStr(sEdit56.Text)+')'
        end
        else
        begin
          Close;
          SQL.Text := 'UPDATE CODE2NDD SET NAME = '+QuotedStr(sEdit56.Text)+' WHERE CODE = ''LA_BGM1''';
        end;
        ExecSQL;
        Close;

        //은행저장 LOCAPP용
        SQL.Text := 'SELECT 1 FROM CODE2NDD WHERE CODE = ''LA_BGM2''';
        Open;
        IF RecordCount = 0 then
        begin
          Close;
          SQL.Text := 'INSERT INTO CODE2NDD VALUES(''LOCAPP문서'',''LA_BGM2'',1,'''','+QuotedStr(sEdit57.Text)+')'
        end
        else
        begin
          Close;
          SQL.Text := 'UPDATE CODE2NDD SET NAME = '+QuotedStr(sEdit57.Text)+' WHERE CODE = ''LA_BGM2''';
        end;
        ExecSQL;
        Close;

        //식별자저장 LOCRCT용
        SQL.Text := 'SELECT 1 FROM CODE2NDD WHERE CODE = ''LC_BGM1''';
        Open;
        IF RecordCount = 0 then
        begin
          Close;
          SQL.Text := 'INSERT INTO CODE2NDD VALUES(''LOCRCT문서'',''LC_BGM1'',1,'''','+QuotedStr(sEdit59.Text)+')'
        end
        else
        begin
          Close;
          SQL.Text := 'UPDATE CODE2NDD SET NAME = '+QuotedStr(sEdit59.Text)+' WHERE CODE = ''LC_BGM1''';
        end;
        ExecSQL;
        Close;

        ShowMessage('저장완료');
      except
        on E:Exception do
        begin
          Clipboard.AsText := SQL.Text;
          ShowMessage(E.Message);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TDialog_FixedNo_frm.ReadData;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT NAME FROM CODE2NDD WHERE CODE = ''LA_BGM1''';
      Open;
      sEdit56.Text := FieldByName('NAME').AsString;

      Close;
      SQL.Text := 'SELECT NAME FROM CODE2NDD WHERE CODE = ''LA_BGM2''';
      Open;

      sEdit57.Text := FieldByName('NAME').AsString;

      Close;
      SQL.Text := 'SELECT NAME FROM CODE2NDD WHERE CODE = ''LC_BGM1''';
      Open;
      sEdit59.Text := FieldByName('NAME').AsString;

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TDialog_FixedNo_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadData;
end;

procedure TDialog_FixedNo_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  CD_APPSPCBANK_frm := TCD_APPSPCBANK_frm.Create(Self);
  try
    IF CD_APPSPCBANK_frm.OpenDialog(0, sEdit57.Text) Then
      sEdit57.Text := CD_APPSPCBANK_frm.Values[0];
  finally
    FreeAndNil(CD_APPSPCBANK_frm);
  end;
end;

end.
