object dlg_ErrView_frm: Tdlg_ErrView_frm
  Left = 689
  Top = 68
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = 'dlg_ErrView_frm'
  ClientHeight = 677
  ClientWidth = 775
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sMemo1: TsMemo
    Left = 0
    Top = 31
    Width = 775
    Height = 646
    Align = alClient
    Font.Charset = HANGEUL_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    WordWrap = False
  end
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 29
    Align = alTop
    
    TabOrder = 1
    object sComboBox1: TsComboBox
      Left = 5
      Top = 3
      Width = 81
      Height = 23
      VerticalAlignment = taVerticalCenter
      Style = csDropDownList
      ItemHeight = 17
      ItemIndex = 0
      TabOrder = 0
      Text = #49569#49888#48320#54872
      OnSelect = sComboBox1Select
      Items.Strings = (
        #49569#49888#48320#54872
        #49688#49888#48320#54872)
    end
  end
  object sPanel2: TsPanel
    Left = 0
    Top = 29
    Width = 775
    Height = 2
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alTop
    
    TabOrder = 2
  end
end
