unit Dialog_HsCodeList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls, sEdit,
  Buttons, sBitBtn, sComboBox, ExtCtrls, sPanel, sSkinProvider;

type
  TDialog_HsCodeList_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    com_SearchKeyword: TsComboBox;
    btn_Search: TsBitBtn;
    edt_Search: TsEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sDBGrid1: TsDBGrid;
    dsList: TDataSource;
    qryList: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btn_SearchClick(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
  function openDialog(): TModalResult;
  end;

var
  Dialog_HsCodeList_frm: TDialog_HsCodeList_frm;

implementation

{$R *.dfm}

{ TDialog_HsCodeList_frm }
uses Dlg_HSCODE, TypeDefine;

function TDialog_HsCodeList_frm.openDialog: TModalResult;
begin
  ReadList;
  Result := ShowModal;
end;

procedure TDialog_HsCodeList_frm.ReadList;
begin
    with qryList do
  begin

    Close;
    sql.Clear;
    SQL.Text := FSQL;

    if com_SearchKeyword.ItemIndex = 0 then
    begin
      SQL.Add(' WHERE [CODE] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
    end
    else if com_SearchKeyword.ItemIndex = 1 then
    begin
      SQL.Add(' WHERE [REPNAME] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
    end;

    Open;
  end;
end;

procedure TDialog_HsCodeList_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_HsCodeList_frm.btn_SearchClick(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_HsCodeList_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure TDialog_HsCodeList_frm.sBitBtn3Click(Sender: TObject);
var
  Hs_code : String;
begin
  inherited;
  Dlg_HSCODE_frm := TDlg_HSCODE_frm.Create(Self);

  try
    if Dlg_HSCODE_frm.Run(ctInsert,nil) = mrOK then
    begin

      Hs_code :=  Dlg_HSCODE_frm.edt_CODE.Text;
      qryList.Close;
      qryList.Open;
      qryList.Locate('CODE',Hs_code,[]);

    end;
  finally
    FreeAndNil(Dlg_HSCODE_frm);
  end;

end;

end.

