unit Dialog_SearchCustom;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sEdit, Buttons, sBitBtn, sComboBox, Grids, DBGrids,
  acDBGrid, ExtCtrls, sSplitter, sPanel, DB, ADODB, sSpeedButton;

type
  TDialog_SearchCustom_frm = class(TForm)
    sPanel1: TsPanel;
    sSplitter3: TsSplitter;
    sDBGrid1: TsDBGrid;
    com_SearchKeyword: TsComboBox;
    btn_Search: TsBitBtn;
    edt_Search: TsEdit;
    dsList: TDataSource;
    qryList: TADOQuery;
    sPanel2: TsPanel;
    sSplitter1: TsSplitter;
    sBitBtn3: TsBitBtn;
    sBitBtn6: TsBitBtn;
    sBitBtn4: TsBitBtn;
    sSpeedButton5: TsSpeedButton;
    sBitBtn2: TsBitBtn;
    sBitBtn1: TsBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure edt_SearchKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure btn_SearchClick(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    procedure Readlist;
  public
    { Public declarations }
    function openDialog(KeyWord : string):TModalResult; overload;
    function openDialog():TModalResult; overload;
    
  end;

var
  Dialog_SearchCustom_frm: TDialog_SearchCustom_frm;

implementation

uses MSSQL, CodeContents, dlg_CustomerReg, TypeDefine ;

{$R *.dfm}

{ TDialog_SearchCustom_frm }

procedure TDialog_SearchCustom_frm.Readlist;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Case com_SearchKeyword.ItemIndex of
      0: SQL.Add('WHERE [CODE] LIKE '+QuotedStr('%'+edt_Search.Text+'%'));
      1: SQL.Add('WHERE [ENAME] LIKE '+QuotedStr('%'+edt_Search.Text+'%'));
    end;
    SQL.Add('ORDER BY ENAME');
    Open;
  end;
end;

procedure TDialog_SearchCustom_frm.FormCreate(Sender: TObject);
begin
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_SearchCustom_frm.edt_SearchKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  IF KEY = VK_RETURN THEN Readlist;
end;

function TDialog_SearchCustom_frm.openDialog: TModalResult;
begin
  Readlist;
  Result := ShowModal;
end;

function TDialog_SearchCustom_frm.openDialog(
  KeyWord: string): TModalResult;
begin
  Readlist;
  IF Trim(KeyWord) <> '' Then
  begin
    com_SearchKeyword.ItemIndex := 0;
    edt_Search.Text := KeyWord;
    qryList.Locate('CODE',KeyWord,[]);
  end;

  Result := ShowModal;
end;

procedure TDialog_SearchCustom_frm.sDBGrid1DblClick(Sender: TObject);
begin
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF qryList.RecordCount > 0 Then
    begin
      ModalResult := mrOk;
    end;
  end;
end;

procedure TDialog_SearchCustom_frm.sDBGrid1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  Case Key of
    VK_RETURN : ModalResult := mrOk;
    VK_ESCAPE : ModalResult := mrCancel;
  end;
end;

procedure TDialog_SearchCustom_frm.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  Case Key of
    VK_ESCAPE : ModalResult := mrCancel;
  end;
end;

procedure TDialog_SearchCustom_frm.sBitBtn2Click(Sender: TObject);
begin

  IF qryList.RecordCount > 0 Then
  begin
      ModalResult := mrOk;
  end;
end;

procedure TDialog_SearchCustom_frm.sBitBtn3Click(Sender: TObject);
var
  Custom_Code : String;
begin
  dlg_CustomerReg_frm := Tdlg_CustomerReg_frm.Create(Self);
  try
    case (Sender as TsBitBtn).Tag of
      0 :
      begin
        if dlg_CustomerReg_frm.Run(ctInsert,nil) = mrOk then
        begin
            Custom_Code := dlg_CustomerReg_frm.edt_CODE.Text;
            qryList.Close;
            qrylist.Open;
            qryList.Locate('CODE',Custom_Code,[]);
        end;
      end;
      1 :
      begin
        if dlg_CustomerReg_frm.Run(ctModify,dsList.DataSet.Fields) = mrOk then
        begin
            Custom_Code := dlg_CustomerReg_frm.edt_CODE.Text;
            qryList.Close;
            qrylist.Open;
            qryList.Locate('CODE',Custom_Code,[]);
        end;
      end;
      2 :
      begin
        if dlg_CustomerReg_frm.Run(ctDelete,dsList.DataSet.Fields) = mrOk then
        begin
            qryList.Close;
            qrylist.Open;
        end;
      end; 
    end;
  finally
    FreeAndNil(dlg_CustomerReg_frm);
  end;
end;

procedure TDialog_SearchCustom_frm.btn_SearchClick(Sender: TObject);
begin
  Readlist;
end;



end.

