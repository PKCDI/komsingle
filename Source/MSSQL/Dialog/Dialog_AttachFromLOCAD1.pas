unit Dialog_AttachFromLOCAD1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit, ExtCtrls, sPanel,
  sSkinProvider, sGroupBox;

type
  TDialog_AttachFromLOCAD1_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sDBGrid1: TsDBGrid;
    dsList: TDataSource;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListLOC_TYPE: TStringField;
    qryListLOC2AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListEX_RATE: TBCDField;
    qryListBUSINESS: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListOPEN_NO: TBCDField;
    qryListISS_DATE: TStringField;
    qryListADV_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListLC_NO: TStringField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListGOODDES: TStringField;
    qryListGOODDES1: TMemoField;
    qryListDOC_ETC: TStringField;
    qryListDOC_ETC_1: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListDOC_DTL: TStringField;
    qryListDOC_NO: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListDEST: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListEXGOOD: TStringField;
    qryListEXGOOD1: TMemoField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListBUSINESSNAME: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListLOC_TYPENAME: TStringField;
    qryListDOC_DTLNAME: TStringField;
    qryListPAYMENTNAME: TStringField;
    qryListDESTNAME: TStringField;
    qryListAP_BANK2: TStringField;
    sRadioGroup1: TsRadioGroup;
    qryList2: TADOQuery;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    StringField17: TStringField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    BCDField1: TBCDField;
    StringField22: TStringField;
    BCDField2: TBCDField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    BCDField3: TBCDField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    StringField27: TStringField;
    StringField28: TStringField;
    StringField29: TStringField;
    StringField30: TStringField;
    StringField31: TStringField;
    StringField32: TStringField;
    BCDField4: TBCDField;
    BCDField5: TBCDField;
    BCDField6: TBCDField;
    BCDField7: TBCDField;
    BCDField8: TBCDField;
    BCDField9: TBCDField;
    StringField33: TStringField;
    StringField34: TStringField;
    BCDField10: TBCDField;
    StringField35: TStringField;
    StringField36: TStringField;
    StringField37: TStringField;
    StringField38: TStringField;
    StringField39: TStringField;
    StringField40: TStringField;
    MemoField1: TMemoField;
    StringField41: TStringField;
    StringField42: TStringField;
    StringField43: TStringField;
    MemoField2: TMemoField;
    StringField44: TStringField;
    StringField45: TStringField;
    StringField46: TStringField;
    StringField47: TStringField;
    StringField48: TStringField;
    StringField49: TStringField;
    StringField50: TStringField;
    StringField51: TStringField;
    StringField52: TStringField;
    MemoField3: TMemoField;
    StringField53: TStringField;
    StringField54: TStringField;
    StringField55: TStringField;
    StringField56: TStringField;
    StringField57: TStringField;
    StringField58: TStringField;
    StringField59: TStringField;
    StringField60: TStringField;
    StringField61: TStringField;
    StringField62: TStringField;
    StringField63: TStringField;
    StringField64: TStringField;
    StringField65: TStringField;
    StringField66: TStringField;
    StringField67: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sBitBtn2Click(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    FSQL : string;
    FSQL2 : string;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog():TModalResult;
  end;

var
  Dialog_AttachFromLOCAD1_frm: TDialog_AttachFromLOCAD1_frm;

implementation

uses DateUtils, KISCalendar, Commonlib, MSSQL, MessageDefine;

{$R *.dfm}

procedure TDialog_AttachFromLOCAD1_frm.FormCreate(Sender: TObject);
begin
  inherited;
  //수혜업자 내국신용장 통지서 쿼리
  FSQL := qryList.SQL.Text;
  //개설업자 내국신용장 개설응답서 쿼리
  FSQL2 := qryList2.SQL.Text;
end;

procedure TDialog_AttachFromLOCAD1_frm.ReadList;
begin

  if sRadioGroup1.ItemIndex = 0 then
  begin
    with qryList do
    begin
      Close;

      SQL.Text := FSQL;

      Case com_SearchKeyword.ItemIndex of
        0: SQL.Add(' WHERE ISS_DATE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        1: SQL.Add(' WHERE  LC_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
        2: SQL.Add(' WHERE  BENEFC1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      end;

      SQL.Add('ORDER BY ISS_DATE');

      Open;
    end;

    dsList.DataSet := qryList;

  end
  else if sRadioGroup1.ItemIndex = 1 then
  begin

    with qryList2 do
    begin
      Close;

      SQL.Text := FSQL2;

      Case com_SearchKeyword.ItemIndex of
        0: SQL.Add(' WHERE ISS_DATE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        1: SQL.Add(' WHERE  LC_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
        2: SQL.Add(' WHERE  BENEFC1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      end;

      SQL.Add('ORDER BY ISS_DATE');

      Open;
    end;

    dsList.DataSet := qryList2;

  end;


end;

procedure TDialog_AttachFromLOCAD1_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_AttachFromLOCAD1_frm.com_SearchKeywordSelect(
  Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TDialog_AttachFromLOCAD1_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  inherited;
end;

procedure TDialog_AttachFromLOCAD1_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
    if DMMssql.KISConnect.InTransaction then
    DMMssql.KISConnect.RollbackTrans;
  Close;
end;

procedure TDialog_AttachFromLOCAD1_frm.Mask_SearchDate1DblClick(
  Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
  //if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TDialog_AttachFromLOCAD1_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of

    900 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    901 : Mask_SearchDate1DblClick(Mask_SearchDate2);

  end;
end;

procedure TDialog_AttachFromLOCAD1_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF (qryList.RecordCount > 0) or (qryList2.RecordCount > 0) Then
    begin
      ModalResult := mrOk;
    end;
  end;
end;

procedure TDialog_AttachFromLOCAD1_frm.sBitBtn3Click(Sender: TObject);
begin
  inherited;

  IF (qryList.RecordCount > 0) and (sRadioGroup1.ItemIndex = 0) Then
  begin
    ModalResult := mrOk;
  end;

  if sRadioGroup1.ItemIndex = 1 then
  begin
    qryList2.Open;
    IF (qryList2.RecordCount > 0) Then
    begin
      ModalResult := mrOk;
    end;
  end;

end;

function TDialog_AttachFromLOCAD1_frm.openDialog: TModalResult;
begin
  Readlist;

  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Result := ShowModal;
end;

end.
