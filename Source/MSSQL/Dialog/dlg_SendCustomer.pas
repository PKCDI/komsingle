unit dlg_SendCustomer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, sSkinProvider, ExtCtrls, sPanel, Buttons,
  sSpeedButton, StdCtrls, sButton, DB, ADODB, Grids, DBGrids, acDBGrid;

type
  Tdlg_SendCustomer_frm = class(TDialogParent_frm)
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListPrefix: TStringField;
    qryListCODE: TStringField;
    qryListRemark: TStringField;
    qryListMap: TStringField;
    qryListNAME: TStringField;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    btnExit: TsButton;
    sButton1: TsButton;
    sButton2: TsButton;
    sButton3: TsButton;
    sSpeedButton1: TsSpeedButton;
    procedure sButton1Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_SendCustomer_frm: Tdlg_SendCustomer_frm;

implementation

uses
  MSSQL, dlg_UserControl, MessageDefine;

{$R *.dfm}

procedure Tdlg_SendCustomer_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  dlg_UserControl_frm := Tdlg_UserControl_frm.Create(Self);
  try
    IF dlg_UserControl_frm.WriteUser(nil) = mrOK then
    begin
      qryList.Close;
      qryList.Open;
      qryList.Locate('CODE', dlg_UserControl_frm.CODE, []);
    end;
  finally
    FreeAndNil(dlg_UserControl_frm);
  end;
end;

procedure Tdlg_SendCustomer_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  dlg_UserControl_frm := Tdlg_UserControl_frm.Create(Self);
  try
    IF dlg_UserControl_frm.EditUser(qryList.Fields) = mrOK Then
    begin
      qryList.Close;
      qryList.Open;
      qryList.Locate('CODE', dlg_UserControl_frm.CODE, []);    
    end;
  finally
    FreeAndNil(dlg_UserControl_frm);
  end;
end;

procedure Tdlg_SendCustomer_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure Tdlg_SendCustomer_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  dlg_SendCustomer_frm := nil;
end;

procedure Tdlg_SendCustomer_frm.FormShow(Sender: TObject);
begin
  inherited;
  qryList.Close;
  qryList.Open;  
end;

procedure Tdlg_SendCustomer_frm.sButton2Click(Sender: TObject);
var
  nIDX : integer;
begin
  inherited;
  IF MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '송수신거래처 삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      nIDX := qryList.RecNo;
      Connection := DMMssql.KISConnect;
      SQL.Text := 'DELETE FROM CODE2NDD WHERE Prefix = ''EDI_SEND'' AND CODE = '+QuotedStr(qryListCODE.AsString);
      ExecSQL;

      qryList.Close;
      qryList.Open;
      IF qryList.RecordCount > 2 Then
        qryList.MoveBy(nIDX-1);
    finally
      Close;
      Free;
    end;
  end;
end;

procedure Tdlg_SendCustomer_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  sButton3.Enabled := qryList.RecordCount > 0;
  sButton2.Enabled := sButton3.Enabled;
end;

end.
