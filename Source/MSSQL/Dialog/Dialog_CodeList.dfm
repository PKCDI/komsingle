inherited Dialog_CodeList_frm: TDialog_CodeList_frm
  Left = 1000
  Top = 234
  Caption = #51228#47785
  ClientHeight = 552
  ClientWidth = 524
  KeyPreview = True
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 33
    Width = 524
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  inherited sPanel1: TsPanel
    Width = 524
    Height = 33
    Align = alTop
    DesignSize = (
      524
      33)
    object com_SearchKeyword: TsComboBox
      Left = 4
      Top = 5
      Width = 81
      Height = 23
      Alignment = taLeftJustify
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = #53076#46300
      Items.Strings = (
        #53076#46300
        #49444#47749)
    end
    object btn_Search: TsBitBtn
      Left = 208
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 1
      OnClick = btn_SearchClick
      ImageIndex = 6
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_Search: TsEdit
      Left = 86
      Top = 5
      Width = 121
      Height = 23
      Color = clWhite
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnKeyUp = edt_SearchKeyUp
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 448
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      ModalResult = 2
      TabOrder = 3
      ImageIndex = 18
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn2: TsBitBtn
      Left = 377
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #54869#51064
      ModalResult = 2
      TabOrder = 4
      OnClick = sDBGrid1DblClick
      ImageIndex = 17
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sBitBtn3: TsBitBtn
      Left = 279
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52628#44032
      TabOrder = 5
      OnClick = sBitBtn3Click
      ImageIndex = 2
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sDBGrid1: TsDBGrid [2]
    Left = 0
    Top = 36
    Width = 524
    Height = 516
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    OnKeyUp = sDBGrid1KeyUp
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'CODE'
        Title.Alignment = taCenter
        Title.Caption = #53076#46300
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Title.Caption = #49444#47749
        Width = 414
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 128
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Prefix'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CODE2NDD'
      'WHERE Prefix = :Prefix'
      'AND Remark = 1')
    Left = 16
    Top = 160
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 160
  end
end
