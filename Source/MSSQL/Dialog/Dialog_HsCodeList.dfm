inherited Dialog_HsCodeList_frm: TDialog_HsCodeList_frm
  Left = 1118
  Top = 372
  Caption = 'H S '#53076' '#46300' '#47785' '#47197
  ClientHeight = 411
  ClientWidth = 536
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poOwnerFormCenter
  Visible = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 536
    Height = 34
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      536
      34)
    object com_SearchKeyword: TsComboBox
      Left = 4
      Top = 5
      Width = 81
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = #53076#46300
      Items.Strings = (
        #53076#46300
        #45824#54364#54408#47749)
    end
    object btn_Search: TsBitBtn
      Left = 208
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 1
      OnClick = btn_SearchClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object edt_Search: TsEdit
      Left = 86
      Top = 5
      Width = 121
      Height = 23
      Color = clWhite
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sBitBtn1: TsBitBtn
      Left = 460
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      ModalResult = 2
      TabOrder = 3
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn2: TsBitBtn
      Left = 389
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #54869#51064
      ModalResult = 2
      TabOrder = 4
      OnClick = sDBGrid1DblClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 279
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52628#44032
      TabOrder = 5
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 2
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 34
    Width = 536
    Height = 377
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #53076#46300
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'REPNAME'
        Title.Caption = #45824#54364#54408#47749
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Title.Caption = #54408#47749
        Width = 230
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 104
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 72
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE,REPNAME,NAME'
      'FROM HS_CODE ')
    Left = 16
    Top = 72
  end
end
