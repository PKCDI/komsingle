unit dlg_UserControl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, StdCtrls, sCheckBox, sEdit, sButton, DB, 
  ExtCtrls, sPanel, TypeDefine, sLabel, acImage, ADODB;

type
  Tdlg_UserControl_frm = class(TForm)
    sPanel3: TsPanel;
    edt_CODE: TsEdit;
    edt_Name: TsEdit;
    chk_Used: TsCheckBox;
    QRShape1: TQRShape;
    btnOK: TsButton;
    btnCancel: TsButton;
    sImage1: TsImage;
    sLabel1: TsLabel;
  private
    { Private declarations }
    FCODE : String;
    FWork : TProgramControlType;
    function RunDialog(FFields : TFields):TModalResult;    
  public
    { Public declarations }
    property CODE:String read FCODE;
    function WriteUser(FFields : TFields):TModalResult;
    function EditUser(FFields : TFields):TModalResult;
  end;

var
  dlg_UserControl_frm: Tdlg_UserControl_frm;

implementation

uses
  MSSQL, ICON, SQLCreator;

{$R *.dfm}

{ Tdlg_UserControl_frm }

function Tdlg_UserControl_frm.EditUser(FFields: TFields): TModalResult;
begin
  FWork := ctModify;
  sLabel1.Caption := '대상거래처 수정';
  Result := RunDialog(FFields);
end;

function Tdlg_UserControl_frm.RunDialog(FFields: TFields): TModalResult;
var
  SC : TSQLCreate;
begin
  FCODE := '';
  edt_CODE.Enabled := (FWork = ctInsert);

  IF FWork = ctModify Then
  begin
    edt_CODE.Text := FFields.FieldByName('CODE').AsString;
    edt_Name.Text := FFields.FieldByName('Name').AsString;
    chk_Used.Checked := (FFields.FieldByName('Remark').AsString = '1');
  end
  else
    chk_Used.Checked := FWork = ctInsert;

  IF Self.ShowModal = mrOK Then
  begin
    SC := TSQLCreate.Create;
    try
      if FWork = ctInsert Then
        SC.DMLType := dmlInsert
      else
      if FWork = ctModify Then
      begin
        SC.DMLType := dmlUpdate;
        SC.ADDWhere('Prefix', 'EDI_SEND');
        SC.ADDWhere('CODE', edt_CODE.Text);
      end;

      SC.SQLHeader('CODE2NDD');
      IF Fwork = ctInsert Then
      begin
        SC.ADDValue('Prefix', 'EDI_SEND');
        SC.ADDValue('CODE', edt_CODE.Text);
      end;

      SC.ADDValue('MAP', edt_CODE.Text);
      IF chk_Used.Checked Then
        SC.ADDValue('Remark', '1')
      else
        SC.ADDValue('Remark', '0');
        
      SC.ADDValue('NAME', edt_Name.Text);

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SC.CreateSQL;
          ExecSQL;
          FCODE := edt_CODE.Text;
        finally
          Close;
          Free;
        end;
      end;

    finally
      SC.Free;
    end;

    Result := mrOk;
  end
  else
    Result := mrCancel;
end;

function Tdlg_UserControl_frm.WriteUser(FFields: TFields): TModalResult;
begin
  FWork := ctInsert;
  sLabel1.Caption := '대상거래처 추가';
  Result := RunDialog(FFields);
end;

end.
