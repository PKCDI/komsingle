inherited Dialog_BANK_frm: TDialog_BANK_frm
  Left = 1252
  Top = 274
  Caption = #51008#54665#49440#53469
  ClientHeight = 567
  ClientWidth = 419
  KeyPreview = True
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sSplitter3: TsSplitter [0]
    Left = 0
    Top = 69
    Width = 419
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter1: TsSplitter [1]
    Left = 0
    Top = 33
    Width = 419
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  inherited sPanel1: TsPanel
    Top = 36
    Width = 419
    Height = 33
    Align = alTop
    object com_SearchKeyword: TsComboBox
      Left = 16
      Top = 5
      Width = 81
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = #53076#46300
      Items.Strings = (
        #53076#46300
        #51008#54665#47749
        #51648#51216#47749)
    end
    object btn_Search: TsBitBtn
      Left = 334
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 1
      OnClick = btn_SearchClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object edt_Search: TsEdit
      Left = 98
      Top = 5
      Width = 235
      Height = 23
      Color = clWhite
      Font.Charset = HANGEUL_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnKeyUp = edt_SearchKeyUp
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object sDBGrid1: TsDBGrid [3]
    Left = 0
    Top = 72
    Width = 419
    Height = 495
    Align = alClient
    Color = clWhite
    DataSource = dsBank
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    OnKeyUp = sDBGrid1KeyUp
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Alignment = taCenter
        Color = clBtnFace
        Expanded = False
        FieldName = 'CODE'
        Title.Alignment = taCenter
        Title.Caption = #53076#46300
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BankName1'
        Title.Caption = #51008#54665#47749
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BankBranch1'
        Title.Caption = #51648#51216#47749
        Width = 258
        Visible = True
      end>
  end
  object sPanel2: TsPanel [4]
    Left = 0
    Top = 0
    Width = 419
    Height = 33
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      419
      33)
    object sSpeedButton5: TsSpeedButton
      Left = 240
      Top = -2
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sBitBtn3: TsBitBtn
      Left = 17
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52628#44032
      TabOrder = 0
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 2
      Images = DMICON.System18
    end
    object sBitBtn6: TsBitBtn
      Tag = 1
      Left = 88
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49688#51221
      TabOrder = 1
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 3
      Images = DMICON.System18
    end
    object sBitBtn4: TsBitBtn
      Tag = 2
      Left = 159
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49325#51228
      TabOrder = 2
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 27
      Images = DMICON.System18
    end
    object sBitBtn2: TsBitBtn
      Left = 263
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #54869#51064
      ModalResult = 2
      TabOrder = 3
      OnClick = sDBGrid1DblClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
    object sBitBtn1: TsBitBtn
      Left = 334
      Top = 5
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Caption = #45803#44592
      ModalResult = 2
      TabOrder = 4
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 160
  end
  object dsBank: TDataSource
    DataSet = qryBank
    Left = 72
    Top = 160
  end
  object qryBank: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [CODE]'
      '      ,[ENAME1] as BankName1'
      '      ,[ENAME2] as BankName2'
      '      ,[ENAME3] as BankBranch1'
      '      ,[ENAME4] as BankBranch2'
      '      ,[ENAME5] as BankAccount'
      '      ,[ENAME6] as BankPW'
      'FROM BANKCODE')
    Left = 40
    Top = 160
  end
end
