unit Dialog_AttachFromVATBI2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, Grids, DBGrids, acDBGrid, DB, ADODB,
  StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit, ExtCtrls,
  sPanel;

type
  TDialog_AttachFromVATBI2_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryGoods: TADOQuery;
    dsGoods: TDataSource;
    sDBGrid1: TsDBGrid;
    sDBGrid2: TsDBGrid;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListRE_NO: TStringField;
    qryListSE_NO: TStringField;
    qryListFS_NO: TStringField;
    qryListACE_NO: TStringField;
    qryListRFF_NO: TStringField;
    qryListSE_CODE: TStringField;
    qryListSE_SAUP: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_ADDR1: TStringField;
    qryListSE_ADDR2: TStringField;
    qryListSE_ADDR3: TStringField;
    qryListSE_UPTA: TStringField;
    qryListSE_UPTA1: TMemoField;
    qryListSE_ITEM: TStringField;
    qryListSE_ITEM1: TMemoField;
    qryListBY_CODE: TStringField;
    qryListBY_SAUP: TStringField;
    qryListBY_NAME1: TStringField;
    qryListBY_NAME2: TStringField;
    qryListBY_ADDR1: TStringField;
    qryListBY_ADDR2: TStringField;
    qryListBY_ADDR3: TStringField;
    qryListBY_UPTA: TStringField;
    qryListBY_UPTA1: TMemoField;
    qryListBY_ITEM: TStringField;
    qryListBY_ITEM1: TMemoField;
    qryListAG_CODE: TStringField;
    qryListAG_SAUP: TStringField;
    qryListAG_NAME1: TStringField;
    qryListAG_NAME2: TStringField;
    qryListAG_NAME3: TStringField;
    qryListAG_ADDR1: TStringField;
    qryListAG_ADDR2: TStringField;
    qryListAG_ADDR3: TStringField;
    qryListAG_UPTA: TStringField;
    qryListAG_UPTA1: TMemoField;
    qryListAG_ITEM: TStringField;
    qryListAG_ITEM1: TMemoField;
    qryListDRAW_DAT: TStringField;
    qryListDETAILNO: TBCDField;
    qryListSUP_AMT: TBCDField;
    qryListTAX_AMT: TBCDField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAMT11: TBCDField;
    qryListAMT11C: TStringField;
    qryListAMT12: TBCDField;
    qryListAMT21: TBCDField;
    qryListAMT21C: TStringField;
    qryListAMT22: TBCDField;
    qryListAMT31: TBCDField;
    qryListAMT31C: TStringField;
    qryListAMT32: TBCDField;
    qryListAMT41: TBCDField;
    qryListAMT41C: TStringField;
    qryListAMT42: TBCDField;
    qryListINDICATOR: TStringField;
    qryListTAMT: TBCDField;
    qryListSUPTAMT: TBCDField;
    qryListTAXTAMT: TBCDField;
    qryListUSTAMT: TBCDField;
    qryListUSTAMTC: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYC: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListVAT_CODE: TStringField;
    qryListVAT_TYPE: TStringField;
    qryListNEW_INDICATOR: TStringField;
    qryListSE_ADDR4: TStringField;
    qryListSE_ADDR5: TStringField;
    qryListSE_SAUP1: TStringField;
    qryListSE_SAUP2: TStringField;
    qryListSE_SAUP3: TStringField;
    qryListSE_FTX1: TStringField;
    qryListSE_FTX2: TStringField;
    qryListSE_FTX3: TStringField;
    qryListSE_FTX4: TStringField;
    qryListSE_FTX5: TStringField;
    qryListBY_SAUP_CODE: TStringField;
    qryListBY_ADDR4: TStringField;
    qryListBY_ADDR5: TStringField;
    qryListBY_SAUP1: TStringField;
    qryListBY_SAUP2: TStringField;
    qryListBY_SAUP3: TStringField;
    qryListBY_FTX1: TStringField;
    qryListBY_FTX2: TStringField;
    qryListBY_FTX3: TStringField;
    qryListBY_FTX4: TStringField;
    qryListBY_FTX5: TStringField;
    qryListBY_FTX1_1: TStringField;
    qryListBY_FTX2_1: TStringField;
    qryListBY_FTX3_1: TStringField;
    qryListBY_FTX4_1: TStringField;
    qryListBY_FTX5_1: TStringField;
    qryListAG_ADDR4: TStringField;
    qryListAG_ADDR5: TStringField;
    qryListAG_SAUP1: TStringField;
    qryListAG_SAUP2: TStringField;
    qryListAG_SAUP3: TStringField;
    qryListAG_FTX1: TStringField;
    qryListAG_FTX2: TStringField;
    qryListAG_FTX3: TStringField;
    qryListAG_FTX4: TStringField;
    qryListAG_FTX5: TStringField;
    qryListSE_NAME3: TStringField;
    qryListBY_NAME3: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TBCDField;
    qryGoodsDE_DATE: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsDE_REM1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsSUPAMT: TBCDField;
    qryGoodsTAXAMT: TBCDField;
    qryGoodsUSAMT: TBCDField;
    qryGoodsUSAMT_G: TStringField;
    qryGoodsSUPSTAMT: TBCDField;
    qryGoodsTAXSTAMT: TBCDField;
    qryGoodsUSSTAMT: TBCDField;
    qryGoodsUSSTAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsRATE: TBCDField;
    procedure sBitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    VATBI2_SQL : string;
    VATBI2GOODS_SQL : string;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog():TModalResult; 
    function CopyOpenDialog : String;
  end;

var
  Dialog_AttachFromVATBI2_frm: TDialog_AttachFromVATBI2_frm;

implementation

{$R *.dfm}
uses DateUtils, KISCalendar, Commonlib, MSSQL, MessageDefine;

procedure TDialog_AttachFromVATBI2_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
  Close;
end;

procedure TDialog_AttachFromVATBI2_frm.FormCreate(Sender: TObject);
begin
  inherited;
  VATBI2_SQL := qryList.SQL.Text;
  VATBI2GOODS_SQL := qryGoods.SQL.Text;
end;

procedure TDialog_AttachFromVATBI2_frm.ReadList;
begin
    with qryList do
    begin
      Close;

      SQL.Text := VATBI2_SQL;

      Case com_SearchKeyword.ItemIndex of
        0: SQL.Add(' WHERE DATEE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        1: SQL.Add(' WHERE  MAINT_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
        2: SQL.Add(' WHERE  BY_NAME1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      end;

      SQL.Add('ORDER BY DRAW_DAT');

      Open;
    end;

    with qryGoods do
    begin
      Close;
      SQL.Text := VATBI2GOODS_SQL;
      SQL.Add(' WHERE KEYY = ' + QuotedStr(qryListMAINT_NO.AsString) );
      Open;
    end;
end;

procedure TDialog_AttachFromVATBI2_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_AttachFromVATBI2_frm.com_SearchKeywordSelect(
  Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TDialog_AttachFromVATBI2_frm.Mask_SearchDate1DblClick(
  Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
  //if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TDialog_AttachFromVATBI2_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of

    900 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    901 : Mask_SearchDate1DblClick(Mask_SearchDate2);

  end;
end;

procedure TDialog_AttachFromVATBI2_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF qryList.RecordCount > 0 Then
    begin
      ModalResult := mrOk;
    end;
  end;
end;

procedure TDialog_AttachFromVATBI2_frm.sBitBtn3Click(Sender: TObject);
begin
  inherited;
  IF qryList.RecordCount > 0 Then
  begin
    ModalResult := mrOk;
  end;
end;

function TDialog_AttachFromVATBI2_frm.openDialog: TModalResult;
begin

  Readlist;

  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Result := ShowModal;
end;

procedure TDialog_AttachFromVATBI2_frm.qryListAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  with qryGoods do
  begin
    Close;
    SQL.Text := VATBI2GOODS_SQL;
    SQL.Add(' WHERE KEYY = ' + QuotedStr(qryListMAINT_NO.AsString) );
    Open;
  end;
end;

function TDialog_AttachFromVATBI2_frm.CopyOpenDialog: String;
begin
  Result := '';

  com_SearchKeyword.ItemIndex := 0;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',now);

  ReadList;

  if ShowModal = mrOk then
    Result := qryListMAINT_NO.AsString;
end;

end.
