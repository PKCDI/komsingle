inherited Dialog_MemoList_frm: TDialog_MemoList_frm
  Left = 714
  Top = 277
  Caption = #47700' '#47784' '#53076' '#46300' '#47785' '#47197
  ClientHeight = 384
  ClientWidth = 784
  OldCreateOrder = True
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 15
  object sSplitter3: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 784
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  inherited sPanel1: TsPanel
    Top = 44
    Width = 784
    Height = 340
    object sSplitter1: TsSplitter
      Left = 401
      Top = 33
      Width = 4
      Height = 306
      Cursor = crHSplit
      Enabled = False
      SkinData.SkinSection = 'SPLITTER'
    end
    object sPanel2: TsPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 32
      Align = alTop
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      object com_SearchKeyword: TsComboBox
        Left = 4
        Top = 5
        Width = 81
        Height = 23
        Alignment = taLeftJustify
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'COMBOBOX'
        VerticalAlignment = taAlignTop
        Style = csDropDownList
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ItemHeight = 17
        ItemIndex = 0
        ParentFont = False
        TabOrder = 0
        Text = 'CODE'
        Items.Strings = (
          'CODE'
          'SUBJECT')
      end
      object btn_Search: TsBitBtn
        Left = 379
        Top = 5
        Width = 70
        Height = 23
        Cursor = crHandPoint
        Caption = #44160#49353
        TabOrder = 1
        OnClick = btn_SearchClick
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 6
        Images = DMICON.System18
      end
      object edt_Search: TsEdit
        Left = 86
        Top = 5
        Width = 292
        Height = 23
        Color = clWhite
        Font.Charset = HANGEUL_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 33
      Width = 400
      Height = 306
      Align = alLeft
      Color = clWhite
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDblClick = sDBGrid1DblClick
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Expanded = False
          FieldName = 'CODE'
          Width = 94
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SUBJECT'
          Width = 309
          Visible = True
        end>
    end
    object sMemo1: TsMemo
      Left = 405
      Top = 33
      Width = 378
      Height = 306
      Align = alClient
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Lines.Strings = (
        'sMemo1')
      ParentFont = False
      TabOrder = 2
      Text = 'sMemo1'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
  end
  object sPanel4: TsPanel [2]
    Left = 0
    Top = 0
    Width = 784
    Height = 41
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      784
      41)
    object sSpeedButton1: TsSpeedButton
      Left = 173
      Top = 1
      Width = 8
      Height = 40
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 601
      Top = 0
      Width = 8
      Height = 40
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton2: TsSpeedButton
      Left = 361
      Top = 1
      Width = 8
      Height = 40
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object codeRadioGroup: TsRadioGroup
      Left = 2
      Top = -1
      Width = 161
      Height = 34
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = RadioGroupClick
      SkinData.SkinSection = 'GROUPBOX'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        #47700#47784#53076#46300
        #51228#54408#53076#46300)
    end
    object writeRadioGroup: TsRadioGroup
      Left = 191
      Top = -1
      Width = 160
      Height = 34
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'GROUPBOX'
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        #45934#50612#50416#44592
        #52628#44032#54616#44592)
    end
    object sBitBtn3: TsBitBtn
      Left = 627
      Top = 8
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 2
      OnClick = sDBGrid1DblClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
    object sBitBtn1: TsBitBtn
      Left = 698
      Top = 8
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      ModalResult = 2
      TabOrder = 3
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn4: TsBitBtn
      Tag = 2
      Left = 529
      Top = 8
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49325#51228
      TabOrder = 4
      OnClick = sBitBtn2Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 27
      Images = DMICON.System18
    end
    object sBitBtn6: TsBitBtn
      Tag = 1
      Left = 458
      Top = 8
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49688#51221
      TabOrder = 5
      OnClick = sBitBtn2Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 3
      Images = DMICON.System18
    end
    object sBitBtn2: TsBitBtn
      Left = 387
      Top = 8
      Width = 70
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52628#44032
      TabOrder = 6
      OnClick = sBitBtn2Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 2
      Images = DMICON.System18
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 120
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 152
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT PREFIX,CODE,SUBJECT,D_MEMO FROM MEMOD ')
    Left = 16
    Top = 152
    object qryListCODE: TStringField
      FieldName = 'CODE'
    end
    object qryListSUBJECT: TStringField
      FieldName = 'SUBJECT'
      Size = 40
    end
    object qryListD_MEMO: TMemoField
      FieldName = 'D_MEMO'
      BlobType = ftMemo
    end
  end
  object qryItem: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT CODE,HSCODE,FNAME,SNAME,MSIZE,QTYC,PRICEG,PRICE,QTY_U,QTY' +
        '_UG  FROM ITEM_CODE')
    Left = 16
    Top = 184
    object qryItemCODE: TStringField
      FieldName = 'CODE'
    end
    object qryItemHSCODE: TStringField
      FieldName = 'HSCODE'
      Size = 10
    end
    object qryItemFNAME: TMemoField
      FieldName = 'FNAME'
      BlobType = ftMemo
    end
    object qryItemSNAME: TStringField
      FieldName = 'SNAME'
      Size = 35
    end
    object qryItemMSIZE: TMemoField
      FieldName = 'MSIZE'
      BlobType = ftMemo
    end
    object qryItemQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryItemPRICEG: TStringField
      FieldName = 'PRICEG'
      Size = 3
    end
    object qryItemPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryItemQTY_U: TBCDField
      FieldName = 'QTY_U'
      Precision = 18
    end
    object qryItemQTY_UG: TStringField
      FieldName = 'QTY_UG'
      Size = 3
    end
  end
end
