object dlgUserAdmin_frm: TdlgUserAdmin_frm
  Left = 1106
  Top = 307
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = #50976#51200#44288#47532
  ClientHeight = 399
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 337
    Top = 0
    Width = 287
    Height = 399
    Align = alClient
    
    TabOrder = 0
    object sLabel1: TsLabel
      Left = 16
      Top = 16
      Width = 64
      Height = 21
      Caption = #50976#51200#51221#48372
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object Shape1: TShape
      Left = 8
      Top = 176
      Width = 272
      Height = 1
      Brush.Color = clGray
      Pen.Color = clGray
    end
    object sLabel2: TsLabel
      Left = 8
      Top = 256
      Width = 255
      Height = 15
      Caption = #50976#51200#52628#44032' : ['#50976#51200#52628#44032']'#48260#53948#51012' '#45572#47476#44256' '#51221#48372#51077#47141#54980
    end
    object sLabel3: TsLabel
      Left = 67
      Top = 272
      Width = 84
      Height = 15
      Caption = '['#54869#51064']'#48260#53948' '#53364#47533
    end
    object sLabel4: TsLabel
      Left = 8
      Top = 296
      Width = 251
      Height = 15
      Caption = #50976#51200#49688#51221' : '#50812#51901#50640#49436' '#49440#53469#54980' '#51221#48372' '#49688#51221#54980' ['#54869#51064']'
    end
    object sLabel5: TsLabel
      Left = 8
      Top = 320
      Width = 167
      Height = 15
      Caption = #50976#51200#49325#51228' : ['#50976#51200#49325#51228']'#48260#53948' '#53364#47533
    end
    object sEdit1: TsEdit
      Left = 64
      Top = 48
      Width = 81
      Height = 23
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = #50500#51060#46356
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object sEdit2: TsEdit
      Left = 64
      Top = 96
      Width = 121
      Height = 23
      TabOrder = 2
      BoundLabel.Active = True
      BoundLabel.Caption = #51060#47492
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object sEdit3: TsEdit
      Left = 64
      Top = 120
      Width = 121
      Height = 23
      TabOrder = 3
      BoundLabel.Active = True
      BoundLabel.Caption = #48512#49436
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object sCheckBox1: TsCheckBox
      Left = 62
      Top = 149
      Width = 64
      Height = 19
      Caption = #54876#49457#54868
      TabOrder = 4
    end
    object sEdit4: TsEdit
      Left = 64
      Top = 72
      Width = 201
      Height = 23
      PasswordChar = '*'
      TabOrder = 1
      BoundLabel.Active = True
      BoundLabel.Caption = #48708#48128#48264#54840
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object sButton1: TsButton
      Left = 144
      Top = 185
      Width = 65
      Height = 25
      Caption = #54869#51064
      TabOrder = 5
      OnClick = sButton1Click
    end
    object sButton2: TsButton
      Left = 215
      Top = 185
      Width = 65
      Height = 25
      Caption = #52712#49548
      TabOrder = 6
      OnClick = sButton2Click
    end
    object sButton3: TsButton
      Left = 8
      Top = 185
      Width = 97
      Height = 25
      Caption = #50976#51200#52628#44032
      TabOrder = 7
      OnClick = sButton3Click
    end
    object sButton4: TsButton
      Left = 8
      Top = 214
      Width = 97
      Height = 25
      Caption = #50976#51200#49325#51228
      TabOrder = 8
      OnClick = sButton4Click
    end
  end
  object sDBGrid1: TsDBGrid
    Left = 0
    Top = 0
    Width = 337
    Height = 399
    Align = alLeft
    Color = clWhite
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'USERID'
        Title.Alignment = taCenter
        Title.Caption = #50500#51060#46356
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Title.Alignment = taCenter
        Title.Caption = #51060#47492
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DEPT'
        Title.Alignment = taCenter
        Title.Caption = #48512#49436
        Width = 67
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'ACTIVATE'
        Title.Alignment = taCenter
        Title.Caption = #54876#49457#54868
        Width = 63
        Visible = True
      end>
  end
  object ADOQuery1: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = ADOQuery1AfterOpen
    AfterScroll = ADOQuery1AfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM PERSON')
    Left = 232
    Top = 352
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 264
    Top = 352
  end
end
