 unit Dialog_CodeList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, StdCtrls, sEdit, Buttons, sBitBtn, sComboBox,
  sSkinProvider, ExtCtrls, sPanel, DB, ADODB, Grids, DBGrids, acDBGrid,
  sSplitter;

type
//  TMasterType = (mtStandard,mtGeneral);
  TDialog_CodeList_frm = class(TDialogParent_frm)
    com_SearchKeyword: TsComboBox;
    btn_Search: TsBitBtn;
    edt_Search: TsEdit;
    sBitBtn1: TsBitBtn;
    sSplitter1: TsSplitter;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure btn_SearchClick(Sender: TObject);
    procedure edt_SearchKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sDBGrid1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    FSQL: string;
    procedure ReadList;
    procedure SetTitle;
  public
    { Public declarations }
    FGroup: string;
//    FMasterType : TMasterType; //추가시 CODE_MSSQL 새로고침을 위해서 사용
    function openDialog(sGroup: string; DefaultKeyWord: string = ''): TModalResult;
    
  end;

var
  Dialog_CodeList_frm: TDialog_CodeList_frm;

implementation

uses
  MSSQL, DLG_CODE2NDDCreate, TypeDefine, CODE_MSSQL;
//  MSSQL, ,CodeContents, dlg_CODE2NDDCreate;

{$R *.dfm}

{ TDialogParent_frm1 }

function TDialog_CodeList_frm.openDialog(sGroup, DefaultKeyWord: string): TModalResult;
begin
  FGroup := sGroup;
  SetTitle;
  ReadList;
  if Trim(DefaultKeyWord) <> '' then
  begin
    qryList.Locate('CODE', DefaultKeyWord, []);
  end;

  Result := ShowModal;
end;

procedure TDialog_CodeList_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Parameters.ParamByName('Prefix').Value := FGroup;
    case com_SearchKeyword.ItemIndex of
      0:
        SQL.Add('AND [CODE] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
      1:
        SQL.Add('AND [NAME] LIKE ' + QuotedStr('%' + edt_Search.Text + '%'));
    end;
    Open;
  end;
end;

procedure TDialog_CodeList_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_CodeList_frm.btn_SearchClick(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_CodeList_frm.edt_SearchKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_RETURN:
      ReadList;
    VK_DOWN:
      sDBGrid1.SetFocus;
  end;
end;

procedure TDialog_CodeList_frm.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_ESCAPE then
    ModalResult := mrCancel;
end;

procedure TDialog_CodeList_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure TDialog_CodeList_frm.sDBGrid1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sDBGrid1DblClick(sDBGrid1);
end;

procedure TDialog_CodeList_frm.FormShow(Sender: TObject);
begin
  inherited;
  sDBGrid1.SetFocus;
end;

procedure TDialog_CodeList_frm.SetTitle;
begin
  Self.Caption := '[' + FGroup + '] ';
  if FGroup = '내국신1001' then
    Self.Caption := Self.Caption + '개설근거서류 종류'
  else if FGroup = '내국신4487' then
    Self.Caption := Self.Caption + '신용장 종류'
  else if FGroup = '내국신4025' then
    Self.Caption := Self.Caption + '개설근거별 용도'
  else if FGroup = '내국신4277' then
    Self.Caption := Self.Caption + '대금결제조건'
  else if FGroup = '통화' then
    Self.Caption := Self.Caption + '통화단위'
  else if FGroup = 'PSHIP' then
    Self.Caption := Self.Caption + '분할허용여부'
  else if FGroup = '국가' then
    Self.Caption := Self.Caption + '국가'
  else if FGroup = '단위' then
    Self.Caption := Self.Caption + '단위'

  //APP700 취소불능화환신용장 개설 신청서
  else if FGroup = '개설방법' then
    Self.Caption := Self.Caption + '개설방법'
  else if FGroup = '신용공여' then
    Self.Caption := Self.Caption + '신용공여'
  else if FGroup = 'IMP_CD' then
    Self.Caption := Self.Caption + '수입용도'
  else if FGroup = 'DOC_CD' then
    Self.Caption := Self.Caption + 'Form of Documentary Credit'
  else if FGroup = 'CD_MAX' then
    Self.Caption := Self.Caption + 'Maximum Credit Amount'
  else if FGroup = '가격조건' then
    Self.Caption := Self.Caption + 'Terms of Price'
  else if FGroup = 'CHARGE' then
    Self.Caption := Self.Caption + 'CHARGE'
  else if FGroup = 'CONFIRM' then
    Self.Caption := Self.Caption + 'CONFIRM'
  else if FGroup = 'TSHIP' then
    Self.Caption := Self.Caption + '환적여부'
  else if FGroup = 'C_METHOD' then
    Self.Caption := Self.Caption + '운송수단'
  else if FGroup = 'DOC_705CD' then
    Self.Caption := Self.Caption + 'Full SET 종류'
  else if FGroup = 'DOC_705' then
    Self.Caption := Self.Caption + 'MARKED FREIGHT'
  else if FGroup = 'DOC_740' then
    Self.Caption := Self.Caption + 'MARKED FREIGHT'

  //APP707 취소불능화환신용장 변경  신청서
  else if FGroup = 'CD_MAX' then
    Self.Caption := Self.Caption + 'Maximum Credit Amount'
  else if FGroup = '기능표시' then
    Self.Caption := Self.Caption + '기능표시'
  else if FGroup = '응답유형' then
    Self.Caption := Self.Caption + '응답유형'

  //판매대금추심(매입)의뢰서)
  else if FGroup = 'APPSPC은행' then
   Self.Caption := Self.Caption + '은행'

  //수입화물선취보증(인도승락)신청서
  else if FGroup = 'APPLOG구분' then
   Self.Caption := Self.Caption + '운송유형'
  else if FGroup = 'LC구분' then
    Self.Caption := Self.Caption + '신용장(계약서)'
  else if FGroup = '선하증권' then
    self.Caption := Self.Caption + '선하증권'
  else if FGroup = 'SUNSA' then
    Self.Caption := Self.Caption + '선박회사'
  else if FGroup = '결제기간' then
    Self.Caption := Self.Caption + '결제기간'


end;

procedure TDialog_CodeList_frm.sBitBtn3Click(Sender: TObject);
begin
  inherited;

  CODE_MSSQL_frm := TCODE_MSSQL_frm.Create(Self);

  try
    if CODE_MSSQL_frm.Run(FGroup) = mrOk then
    begin
      qryList.Close;
      qryList.Open;
    end;
  finally
    FreeAndNil(CODE_MSSQL_frm);
  end;


end;

end.

