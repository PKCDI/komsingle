unit dlg_MemoViewer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sMemo, sButton;

type
  Tdlg_Memoviewer_frm = class(TForm)
    sPanel1: TsPanel;
    memo_desc: TsMemo;
    sButton1: TsButton;
    procedure sButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_Memoviewer_frm: Tdlg_Memoviewer_frm;

implementation

uses
  ICON;

{$R *.dfm}

procedure Tdlg_Memoviewer_frm.sButton1Click(Sender: TObject);
begin
  Close;
end;

procedure Tdlg_Memoviewer_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
