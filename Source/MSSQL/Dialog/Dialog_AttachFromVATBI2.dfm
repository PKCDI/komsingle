inherited Dialog_AttachFromVATBI2_frm: TDialog_AttachFromVATBI2_frm
  Left = 757
  Top = 206
  Caption = '['#49688#54812#50629#51088'] '#49464#44552#44228#49328#49436'('#49688#49888') '#49440#53469
  ClientHeight = 585
  ClientWidth = 1058
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poOwnerFormCenter
  Visible = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1058
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      1058
      41)
    object edt_SearchText: TsEdit
      Left = 102
      Top = 13
      Width = 225
      Height = 23
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object com_SearchKeyword: TsComboBox
      Left = 8
      Top = 13
      Width = 93
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = #46321#47197#51068#51088
      OnSelect = com_SearchKeywordSelect
      Items.Strings = (
        #46321#47197#51068#51088
        #44288#47532#48264#54840
        #44277#44553#48155#45716#51088)
    end
    object Mask_SearchDate1: TsMaskEdit
      Left = 102
      Top = 13
      Width = 78
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 328
      Top = 13
      Width = 66
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object sBitBtn21: TsBitBtn
      Tag = 900
      Left = 179
      Top = 13
      Width = 20
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = sBitBtn21Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object Mask_SearchDate2: TsMaskEdit
      Left = 222
      Top = 13
      Width = 82
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn23: TsBitBtn
      Tag = 901
      Left = 303
      Top = 13
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object sPanel25: TsPanel
      Left = 198
      Top = 13
      Width = 25
      Height = 23
      Caption = '~'
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
    end
    object sBitBtn2: TsBitBtn
      Left = 986
      Top = 13
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 8
      OnClick = sBitBtn2Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 916
      Top = 13
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 9
      OnClick = sBitBtn3Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 41
    Width = 1058
    Height = 544
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Alignment = taCenter
        Title.Caption = #44288#47532#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 150
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DATEE'
        Title.Alignment = taCenter
        Title.Caption = #46321#47197#51068#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 85
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DRAW_DAT'
        Title.Alignment = taCenter
        Title.Caption = #51089#49457#51068#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RE_NO'
        Title.Alignment = taCenter
        Title.Caption = #44428#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SE_NO'
        Title.Alignment = taCenter
        Title.Caption = #54840#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FS_NO'
        Title.Alignment = taCenter
        Title.Caption = #51068#47144#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RFF_NO'
        Title.Alignment = taCenter
        Title.Caption = #49464#44552#44228#49328#49436#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BY_NAME1'
        Title.Alignment = taCenter
        Title.Caption = #44277#44553#48155#45716#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SE_NAME1'
        Title.Alignment = taCenter
        Title.Caption = #44277#44553#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUPTAMT'
        Title.Alignment = taCenter
        Title.Caption = #52509#44277#44553#44032#50529
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 90
        Visible = True
      end>
  end
  object sDBGrid2: TsDBGrid [2]
    Left = 24
    Top = 472
    Width = 329
    Height = 97
    Color = clWhite
    DataSource = dsGoods
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    Visible = False
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'KEYY'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SEQ'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DE_DATE'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME_COD'
        Width = 124
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME1'
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SIZE1'
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DE_REM1'
        Width = 64
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTY'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTY_G'
        Width = 34
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTYG'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QTYG_G'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE_G'
        Width = 46
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUPAMT'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TAXAMT'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USAMT'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USAMT_G'
        Width = 46
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUPSTAMT'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TAXSTAMT'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USSTAMT'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USSTAMT_G'
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STQTY'
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STQTY_G'
        Width = 46
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RATE'
        Width = 118
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 96
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RE_NO, SE_N' +
        'O, FS_NO, ACE_NO, RFF_NO, SE_CODE, SE_SAUP, SE_NAME1, SE_NAME2, ' +
        'SE_ADDR1, SE_ADDR2, SE_ADDR3, SE_UPTA, SE_UPTA1, SE_ITEM, SE_ITE' +
        'M1, BY_CODE, BY_SAUP, BY_NAME1, BY_NAME2, BY_ADDR1, BY_ADDR2, BY' +
        '_ADDR3, BY_UPTA, BY_UPTA1, BY_ITEM, BY_ITEM1, AG_CODE, AG_SAUP, ' +
        'AG_NAME1, AG_NAME2, AG_NAME3, AG_ADDR1, AG_ADDR2, AG_ADDR3, AG_U' +
        'PTA, AG_UPTA1, AG_ITEM, AG_ITEM1, DRAW_DAT, DETAILNO, SUP_AMT, T' +
        'AX_AMT, REMARK, REMARK1, AMT11, AMT11C, AMT12, AMT21, AMT21C, AM' +
        'T22, AMT31, AMT31C, AMT32, AMT41, AMT41C, AMT42, INDICATOR, TAMT' +
        ', SUPTAMT, TAXTAMT, USTAMT, USTAMTC, TQTY, TQTYC, CHK1, CHK2, CH' +
        'K3, PRNO, VAT_CODE, VAT_TYPE, NEW_INDICATOR, SE_ADDR4, SE_ADDR5,' +
        ' SE_SAUP1, SE_SAUP2, SE_SAUP3, SE_FTX1, SE_FTX2, SE_FTX3, SE_FTX' +
        '4, SE_FTX5, BY_SAUP_CODE, BY_ADDR4, BY_ADDR5, BY_SAUP1, BY_SAUP2' +
        ', BY_SAUP3, BY_FTX1, BY_FTX2, BY_FTX3, BY_FTX4, BY_FTX5, BY_FTX1' +
        '_1, BY_FTX2_1, BY_FTX3_1, BY_FTX4_1, BY_FTX5_1, AG_ADDR4, AG_ADD' +
        'R5, AG_SAUP1, AG_SAUP2, AG_SAUP3, '
      
        '       AG_FTX1, AG_FTX2, AG_FTX3, AG_FTX4, AG_FTX5, SE_NAME3, BY' +
        '_NAME3'
      'FROM VATBI2_H')
    Left = 16
    Top = 136
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRE_NO: TStringField
      FieldName = 'RE_NO'
      Size = 35
    end
    object qryListSE_NO: TStringField
      FieldName = 'SE_NO'
      Size = 35
    end
    object qryListFS_NO: TStringField
      FieldName = 'FS_NO'
      Size = 35
    end
    object qryListACE_NO: TStringField
      FieldName = 'ACE_NO'
      Size = 35
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListSE_CODE: TStringField
      FieldName = 'SE_CODE'
      Size = 10
    end
    object qryListSE_SAUP: TStringField
      FieldName = 'SE_SAUP'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_ADDR1: TStringField
      FieldName = 'SE_ADDR1'
      Size = 35
    end
    object qryListSE_ADDR2: TStringField
      FieldName = 'SE_ADDR2'
      Size = 35
    end
    object qryListSE_ADDR3: TStringField
      FieldName = 'SE_ADDR3'
      Size = 35
    end
    object qryListSE_UPTA: TStringField
      FieldName = 'SE_UPTA'
      Size = 35
    end
    object qryListSE_UPTA1: TMemoField
      FieldName = 'SE_UPTA1'
      BlobType = ftMemo
    end
    object qryListSE_ITEM: TStringField
      FieldName = 'SE_ITEM'
      Size = 1
    end
    object qryListSE_ITEM1: TMemoField
      FieldName = 'SE_ITEM1'
      BlobType = ftMemo
    end
    object qryListBY_CODE: TStringField
      FieldName = 'BY_CODE'
      Size = 10
    end
    object qryListBY_SAUP: TStringField
      FieldName = 'BY_SAUP'
      Size = 35
    end
    object qryListBY_NAME1: TStringField
      FieldName = 'BY_NAME1'
      Size = 35
    end
    object qryListBY_NAME2: TStringField
      FieldName = 'BY_NAME2'
      Size = 35
    end
    object qryListBY_ADDR1: TStringField
      FieldName = 'BY_ADDR1'
      Size = 35
    end
    object qryListBY_ADDR2: TStringField
      FieldName = 'BY_ADDR2'
      Size = 35
    end
    object qryListBY_ADDR3: TStringField
      FieldName = 'BY_ADDR3'
      Size = 35
    end
    object qryListBY_UPTA: TStringField
      FieldName = 'BY_UPTA'
      Size = 35
    end
    object qryListBY_UPTA1: TMemoField
      FieldName = 'BY_UPTA1'
      BlobType = ftMemo
    end
    object qryListBY_ITEM: TStringField
      FieldName = 'BY_ITEM'
      Size = 1
    end
    object qryListBY_ITEM1: TMemoField
      FieldName = 'BY_ITEM1'
      BlobType = ftMemo
    end
    object qryListAG_CODE: TStringField
      FieldName = 'AG_CODE'
      Size = 10
    end
    object qryListAG_SAUP: TStringField
      FieldName = 'AG_SAUP'
      Size = 35
    end
    object qryListAG_NAME1: TStringField
      FieldName = 'AG_NAME1'
      Size = 35
    end
    object qryListAG_NAME2: TStringField
      FieldName = 'AG_NAME2'
      Size = 35
    end
    object qryListAG_NAME3: TStringField
      FieldName = 'AG_NAME3'
      Size = 35
    end
    object qryListAG_ADDR1: TStringField
      FieldName = 'AG_ADDR1'
      Size = 35
    end
    object qryListAG_ADDR2: TStringField
      FieldName = 'AG_ADDR2'
      Size = 35
    end
    object qryListAG_ADDR3: TStringField
      FieldName = 'AG_ADDR3'
      Size = 35
    end
    object qryListAG_UPTA: TStringField
      FieldName = 'AG_UPTA'
      Size = 35
    end
    object qryListAG_UPTA1: TMemoField
      FieldName = 'AG_UPTA1'
      BlobType = ftMemo
    end
    object qryListAG_ITEM: TStringField
      FieldName = 'AG_ITEM'
      Size = 1
    end
    object qryListAG_ITEM1: TMemoField
      FieldName = 'AG_ITEM1'
      BlobType = ftMemo
    end
    object qryListDRAW_DAT: TStringField
      FieldName = 'DRAW_DAT'
      Size = 8
    end
    object qryListDETAILNO: TBCDField
      FieldName = 'DETAILNO'
      Precision = 18
    end
    object qryListSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      Precision = 18
    end
    object qryListTAX_AMT: TBCDField
      FieldName = 'TAX_AMT'
      Precision = 18
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAMT11: TBCDField
      FieldName = 'AMT11'
      Precision = 18
    end
    object qryListAMT11C: TStringField
      FieldName = 'AMT11C'
      Size = 3
    end
    object qryListAMT12: TBCDField
      FieldName = 'AMT12'
      Precision = 18
    end
    object qryListAMT21: TBCDField
      FieldName = 'AMT21'
      Precision = 18
    end
    object qryListAMT21C: TStringField
      FieldName = 'AMT21C'
      Size = 3
    end
    object qryListAMT22: TBCDField
      FieldName = 'AMT22'
      Precision = 18
    end
    object qryListAMT31: TBCDField
      FieldName = 'AMT31'
      Precision = 18
    end
    object qryListAMT31C: TStringField
      FieldName = 'AMT31C'
      Size = 3
    end
    object qryListAMT32: TBCDField
      FieldName = 'AMT32'
      Precision = 18
    end
    object qryListAMT41: TBCDField
      FieldName = 'AMT41'
      Precision = 18
    end
    object qryListAMT41C: TStringField
      FieldName = 'AMT41C'
      Size = 3
    end
    object qryListAMT42: TBCDField
      FieldName = 'AMT42'
      Precision = 18
    end
    object qryListINDICATOR: TStringField
      FieldName = 'INDICATOR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      Precision = 18
    end
    object qryListSUPTAMT: TBCDField
      FieldName = 'SUPTAMT'
      Precision = 18
    end
    object qryListTAXTAMT: TBCDField
      FieldName = 'TAXTAMT'
      Precision = 18
    end
    object qryListUSTAMT: TBCDField
      FieldName = 'USTAMT'
      Precision = 18
    end
    object qryListUSTAMTC: TStringField
      FieldName = 'USTAMTC'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListVAT_CODE: TStringField
      FieldName = 'VAT_CODE'
      Size = 4
    end
    object qryListVAT_TYPE: TStringField
      FieldName = 'VAT_TYPE'
      Size = 4
    end
    object qryListNEW_INDICATOR: TStringField
      FieldName = 'NEW_INDICATOR'
      Size = 4
    end
    object qryListSE_ADDR4: TStringField
      FieldName = 'SE_ADDR4'
      Size = 35
    end
    object qryListSE_ADDR5: TStringField
      FieldName = 'SE_ADDR5'
      Size = 35
    end
    object qryListSE_SAUP1: TStringField
      FieldName = 'SE_SAUP1'
      Size = 4
    end
    object qryListSE_SAUP2: TStringField
      FieldName = 'SE_SAUP2'
      Size = 4
    end
    object qryListSE_SAUP3: TStringField
      FieldName = 'SE_SAUP3'
      Size = 4
    end
    object qryListSE_FTX1: TStringField
      FieldName = 'SE_FTX1'
      Size = 40
    end
    object qryListSE_FTX2: TStringField
      FieldName = 'SE_FTX2'
      Size = 30
    end
    object qryListSE_FTX3: TStringField
      FieldName = 'SE_FTX3'
    end
    object qryListSE_FTX4: TStringField
      FieldName = 'SE_FTX4'
    end
    object qryListSE_FTX5: TStringField
      FieldName = 'SE_FTX5'
    end
    object qryListBY_SAUP_CODE: TStringField
      FieldName = 'BY_SAUP_CODE'
      Size = 4
    end
    object qryListBY_ADDR4: TStringField
      FieldName = 'BY_ADDR4'
      Size = 35
    end
    object qryListBY_ADDR5: TStringField
      FieldName = 'BY_ADDR5'
      Size = 35
    end
    object qryListBY_SAUP1: TStringField
      FieldName = 'BY_SAUP1'
      Size = 4
    end
    object qryListBY_SAUP2: TStringField
      FieldName = 'BY_SAUP2'
      Size = 4
    end
    object qryListBY_SAUP3: TStringField
      FieldName = 'BY_SAUP3'
      Size = 4
    end
    object qryListBY_FTX1: TStringField
      FieldName = 'BY_FTX1'
      Size = 40
    end
    object qryListBY_FTX2: TStringField
      FieldName = 'BY_FTX2'
      Size = 30
    end
    object qryListBY_FTX3: TStringField
      FieldName = 'BY_FTX3'
    end
    object qryListBY_FTX4: TStringField
      FieldName = 'BY_FTX4'
    end
    object qryListBY_FTX5: TStringField
      FieldName = 'BY_FTX5'
    end
    object qryListBY_FTX1_1: TStringField
      FieldName = 'BY_FTX1_1'
      Size = 40
    end
    object qryListBY_FTX2_1: TStringField
      FieldName = 'BY_FTX2_1'
      Size = 30
    end
    object qryListBY_FTX3_1: TStringField
      FieldName = 'BY_FTX3_1'
    end
    object qryListBY_FTX4_1: TStringField
      FieldName = 'BY_FTX4_1'
    end
    object qryListBY_FTX5_1: TStringField
      FieldName = 'BY_FTX5_1'
    end
    object qryListAG_ADDR4: TStringField
      FieldName = 'AG_ADDR4'
      Size = 35
    end
    object qryListAG_ADDR5: TStringField
      FieldName = 'AG_ADDR5'
      Size = 35
    end
    object qryListAG_SAUP1: TStringField
      FieldName = 'AG_SAUP1'
      Size = 4
    end
    object qryListAG_SAUP2: TStringField
      FieldName = 'AG_SAUP2'
      Size = 4
    end
    object qryListAG_SAUP3: TStringField
      FieldName = 'AG_SAUP3'
      Size = 4
    end
    object qryListAG_FTX1: TStringField
      FieldName = 'AG_FTX1'
      Size = 40
    end
    object qryListAG_FTX2: TStringField
      FieldName = 'AG_FTX2'
      Size = 30
    end
    object qryListAG_FTX3: TStringField
      FieldName = 'AG_FTX3'
    end
    object qryListAG_FTX4: TStringField
      FieldName = 'AG_FTX4'
    end
    object qryListAG_FTX5: TStringField
      FieldName = 'AG_FTX5'
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListBY_NAME3: TStringField
      FieldName = 'BY_NAME3'
      Size = 35
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 136
  end
  object qryGoods: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, DE_DATE, NAME_COD, NAME1, SIZE1, DE_REM1, QTY,' +
        ' QTY_G, QTYG, QTYG_G, PRICE, PRICE_G, SUPAMT, TAXAMT, USAMT, USA' +
        'MT_G, SUPSTAMT, TAXSTAMT, USSTAMT, USSTAMT_G, STQTY, STQTY_G, RA' +
        'TE'
      'FROM VATBI2_D')
    Left = 16
    Top = 168
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryGoodsDE_DATE: TStringField
      FieldName = 'DE_DATE'
      Size = 8
    end
    object qryGoodsNAME_COD: TStringField
      FieldName = 'NAME_COD'
    end
    object qryGoodsNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryGoodsSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryGoodsDE_REM1: TMemoField
      FieldName = 'DE_REM1'
      BlobType = ftMemo
    end
    object qryGoodsQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryGoodsQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryGoodsQTYG: TBCDField
      FieldName = 'QTYG'
      Precision = 18
    end
    object qryGoodsQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryGoodsPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryGoodsPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryGoodsSUPAMT: TBCDField
      FieldName = 'SUPAMT'
      Precision = 18
    end
    object qryGoodsTAXAMT: TBCDField
      FieldName = 'TAXAMT'
      Precision = 18
    end
    object qryGoodsUSAMT: TBCDField
      FieldName = 'USAMT'
      Precision = 18
    end
    object qryGoodsUSAMT_G: TStringField
      FieldName = 'USAMT_G'
      Size = 3
    end
    object qryGoodsSUPSTAMT: TBCDField
      FieldName = 'SUPSTAMT'
      Precision = 18
    end
    object qryGoodsTAXSTAMT: TBCDField
      FieldName = 'TAXSTAMT'
      Precision = 18
    end
    object qryGoodsUSSTAMT: TBCDField
      FieldName = 'USSTAMT'
      Precision = 18
    end
    object qryGoodsUSSTAMT_G: TStringField
      FieldName = 'USSTAMT_G'
      Size = 3
    end
    object qryGoodsSTQTY: TBCDField
      FieldName = 'STQTY'
      Precision = 18
    end
    object qryGoodsSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryGoodsRATE: TBCDField
      FieldName = 'RATE'
      Precision = 18
    end
  end
  object dsGoods: TDataSource
    DataSet = qryGoods
    Left = 48
    Top = 168
  end
end
