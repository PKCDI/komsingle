unit Dialog_AttachFromINF707;
                                                 
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, Grids, DBGrids, acDBGrid, StdCtrls, Buttons, sBitBtn,
  Mask, sMaskEdit, sComboBox, sEdit, ExtCtrls, sPanel, DB, ADODB,
  sSkinProvider;

type
  TDialog_AttachFromINF707_frm = class(TChildForm_frm)
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListCD_NO: TStringField;
    qryListRCV_REF: TStringField;
    qryListIBANK_REF: TStringField;
    qryListISS_BANK1: TStringField;
    qryListISS_BANK2: TStringField;
    qryListISS_BANK3: TStringField;
    qryListISS_BANK4: TStringField;
    qryListISS_BANK5: TStringField;
    qryListISS_ACCNT: TStringField;
    qryListISS_DATE: TStringField;
    qryListAMD_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListPrno: TIntegerField;
    qryListMAINT_NO_1: TStringField;
    qryListMSEQ_1: TIntegerField;
    qryListAMD_NO_1: TIntegerField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TBooleanField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListSR_INFO1: TStringField;
    qryListSR_INFO2: TStringField;
    qryListSR_INFO3: TStringField;
    qryListSR_INFO4: TStringField;
    qryListSR_INFO5: TStringField;
    qryListSR_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListOP_BANK1: TStringField;
    qryListOP_BANK2: TStringField;
    qryListOP_BANK3: TStringField;
    qryListOP_ADDR1: TStringField;
    qryListOP_ADDR2: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListmathod_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListCDMAX_Name: TStringField;
    dsList: TDataSource;
    sPanel1: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sDBGrid1: TsDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sBitBtn2Click(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure btnCalendar(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog : String;
  end;

var
  Dialog_AttachFromINF707_frm: TDialog_AttachFromINF707_frm;

implementation

uses  MSSQL, KISCalendar,Commonlib,DateUtils;

{$R *.dfm}

procedure TDialog_AttachFromINF707_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_AttachFromINF707_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Case com_SearchKeyword.ItemIndex of
      0: SQL.Add('WHERE APP_DATE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
      1: SQL.Add('WHERE  I707_1.MAINT_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE I707_2.BENEFC1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    SQL.Add('ORDER BY APP_DATE');

    Open;
  end;

end;

procedure TDialog_AttachFromINF707_frm.com_SearchKeywordSelect(
  Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;

end;

procedure TDialog_AttachFromINF707_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_AttachFromINF707_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  Dialog_AttachFromINF707_frm := nil;
end;

procedure TDialog_AttachFromINF707_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;

  Close;
end;

procedure TDialog_AttachFromINF707_frm.Mask_SearchDate1DblClick(
  Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
  //if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));


end;

procedure TDialog_AttachFromINF707_frm.btnCalendar(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    900 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    901 : Mask_SearchDate1DblClick(Mask_SearchDate2);
  end;
end;

procedure TDialog_AttachFromINF707_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsDBgrid).ScreenToClient(Mouse.CursorPos).Y > 17 Then
    ModalResult := mrYes;
end;

function TDialog_AttachFromINF707_frm.openDialog: String;
begin

  Result := '';

  com_SearchKeyword.ItemIndex := 0;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',now);

  ReadList;

  if ShowModal = mrYes then
    Result := qryListMAINT_NO.AsString;

end;

end.
