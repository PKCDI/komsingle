inherited Dialog_FixedNo_frm: TDialog_FixedNo_frm
  Left = 583
  Top = 181
  BorderIcons = [biSystemMenu]
  Caption = #51204#51088#47928#49436' '#48264#54840#48512#50668
  ClientHeight = 221
  ClientWidth = 518
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sSplitter1: TsSplitter [0]
    Left = 193
    Top = 0
    Width = 4
    Height = 221
    Cursor = crHSplit
    SkinData.SkinSection = 'SPLITTER'
  end
  inherited sPanel1: TsPanel
    Left = 197
    Width = 321
    Height = 221
    object sGroupBox1: TsGroupBox
      Left = 11
      Top = 9
      Width = 302
      Height = 288
      Caption = #51204#51088#47928#49436#48264#54840
      TabOrder = 0
      SkinData.SkinSection = 'GROUPBOX'
      object sEdit55: TsEdit
        Left = 127
        Top = 48
        Width = 57
        Height = 23
        TabStop = False
        Color = clBtnFace
        Ctl3D = True
        ParentCtl3D = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP'
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #47928#49436#48264#54840
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object sEdit56: TsEdit
        Left = 185
        Top = 48
        Width = 73
        Height = 23
        TabStop = False
        Color = clWhite
        Ctl3D = True
        MaxLength = 8
        ParentCtl3D = False
        TabOrder = 1
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object sEdit57: TsEdit
        Left = 127
        Top = 72
        Width = 25
        Height = 23
        TabStop = False
        Color = clWhite
        Ctl3D = True
        MaxLength = 2
        ParentCtl3D = False
        TabOrder = 2
        Text = '12'
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51008#54665#53076#46300
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object sBitBtn21: TsBitBtn
        Tag = 1
        Left = 153
        Top = 72
        Width = 24
        Height = 23
        Cursor = crHandPoint
        TabOrder = 3
        OnClick = sBitBtn21Click
        ImageIndex = 25
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object sPanel16: TsPanel
        Left = 44
        Top = 24
        Width = 214
        Height = 22
        SkinData.CustomColor = True
        SkinData.SkinSection = 'PANEL'
        Caption = #45236#44397#49888#50857#51109' '#44060#49444#49888#52397#49436'[LOCAPP]'
        Color = 16042877
        
        TabOrder = 4
      end
      object sPanel18: TsPanel
        Left = 44
        Top = 112
        Width = 214
        Height = 22
        SkinData.CustomColor = True
        SkinData.SkinSection = 'PANEL'
        Caption = #47932#54408#49688#47161#51613#47749#49436' '#49569#49888'[LOCRCT]'
        Color = 16042877
        
        TabOrder = 5
      end
      object sEdit58: TsEdit
        Left = 127
        Top = 136
        Width = 57
        Height = 23
        TabStop = False
        Color = clBtnFace
        Ctl3D = True
        ParentCtl3D = False
        ReadOnly = True
        TabOrder = 6
        Text = 'LOCRCT'
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #47928#49436#48264#54840
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object sEdit59: TsEdit
        Left = 185
        Top = 136
        Width = 73
        Height = 23
        TabStop = False
        Color = clWhite
        Ctl3D = True
        MaxLength = 8
        ParentCtl3D = False
        TabOrder = 7
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object sButton1: TsButton
        Left = 176
        Top = 168
        Width = 121
        Height = 33
        Caption = #48320#44221#49324#54637#51200#51109
        TabOrder = 8
        OnClick = sButton1Click
        Images = DMICON.System24
        ImageIndex = 0
      end
      object sButton2: TsButton
        Left = 8
        Top = 168
        Width = 161
        Height = 33
        Caption = #49885#48324#51088#44032#51256#50724#44592
        TabOrder = 9
        OnClick = sButton2Click
        Images = DMICON.System24
        ImageIndex = 33
      end
    end
  end
  object sTreeView1: TsTreeView [2]
    Left = 0
    Top = 0
    Width = 193
    Height = 221
    Align = alLeft
    Indent = 19
    TabOrder = 1
    Items.Data = {
      010000002A0000000000000000000000FFFFFFFFFFFFFFFF0000000001000000
      11C0FCC0DAB9AEBCAD20B9F8C8A3BACEBFA92B0000000000000000000000FFFF
      FFFFFFFFFFFF000000000000000012B3BBB1B9BDC5BFEBC0E55B4C4F43415050
      5D}
    SkinData.SkinSection = 'EDIT'
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 200
  end
end
