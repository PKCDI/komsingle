unit dlgPAYORDCopy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, sEdit, sSpinEdit, sComboBox, ExtCtrls, sPanel,
  sCheckBox, DB, DBTables, ADODB, clipbrd;

type
  TdlgPAYORDCopy_frm = class(TForm)
    sPanel1: TsPanel;
    sComboBox1: TsComboBox;
    sSpinEdit1: TsSpinEdit;
    sButton1: TsButton;
    Query1: TQuery;
    sCheckBox1: TsCheckBox;
    ADOQuery1: TADOQuery;
    procedure sButton1Click(Sender: TObject);
    procedure sComboBox1Select(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    function ReadList:integer;
    procedure CopyData;
  public
    { Public declarations }
  end;

var
  dlgPAYORDCopy_frm: TdlgPAYORDCopy_frm;

implementation

uses
  SQLCreator, MSSQL;

{$R *.dfm}

procedure TdlgPAYORDCopy_frm.CopyData;
var
  SC : TSQLCreate;
  i, j : integer;
  FD_NM : String;
  TMP_LIST : TStringList;

begin
  SC := TSQLCreate.Create;
  try
    SC.DMLType := dmlDelete;
    SC.SQLHeader('PAYORD');
    SC.ADDWhere('SUBSTRING(DATEE,1,4)', sComboBox1.Text);
    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SC.CreateSQL;
//        Clipboard.AsText := SQL.Text;
        ExecSQL;
      finally
        Close;
        Free;
      end;
    end;

    Query1.First;
    while not Query1.Eof do
    begin
      SC.DMLType := dmlInsert;
      SC.SQLHeader('PAYORD');    
      for i := 0 to Query1.FieldCount-1 do
      begin
        FD_NM := UpperCase(Query1.Fields[i].FieldName);
        if FD_NM = 'REMARK' Then
        begin
          if Query1.Fields[i].AsBoolean Then
            SC.ADDValue(FD_NM, 1)
          else
            SC.ADDValue(FD_NM, 0);
        end
        else
        if FD_NM = 'REMARK1' Then
        begin
          TMP_LIST := TStringList.Create;
          try
            TMP_LIST.Text := Query1.Fields[i].AsString;
            for j := 0 to TMP_LIST.Count-1 do
            begin
              if j > 4 Then Break;
              SC.ADDValue('REMARK'+IntToStr(j+1), TMP_LIST.Strings[j]);
            end;
          finally
            TMP_LIST.Free;
          end;
        end
        else
          SC.ADDValue(Query1.Fields[i].FieldName, Query1.Fields[i].AsString);
      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          ParamCheck := False;
          SQL.Text := SC.CreateSQL;
//        Clipboard.AsText := SC.FieldList;
//        Clipboard.AsText := SQL.Text;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;
      Query1.Next;
    end;
  finally
    SC.Free;
    Query1.Close;
  end;
end;

function TdlgPAYORDCopy_frm.ReadList: integer;
begin
  Result := -1;
  with Query1 do
  begin
    Close;
    ParamByName('Year').AsString := sComboBox1.Text;
    Open;

    Result := RecordCount;
  end;
end;

procedure TdlgPAYORDCopy_frm.sButton1Click(Sender: TObject);
var
  sMSG : String;
begin
  ReadList;

  sMSG := '※주의'#13#10'현재 입력되어있는 데이터는 전부 삭제됩니다.'#13#10'계속하시겠습니까?';
  IF MessageBox(self.Handle, PChar(sMSG), '기존데이터 삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEl then Exit;

  CopyData;

end;

procedure TdlgPAYORDCopy_frm.sComboBox1Select(Sender: TObject);
begin
  sSpinEdit1.Value := ReadList;
end;

procedure TdlgPAYORDCopy_frm.FormDestroy(Sender: TObject);
begin
  IF Query1.Active Then Query1.Close;
end;

end.
