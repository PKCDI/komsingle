inherited dlg_SendCustomer_frm: Tdlg_SendCustomer_frm
  Left = 916
  Top = 167
  Caption = #49569'/'#49688#49888' '#45824#49345' '#44144#47000#52376
  ClientHeight = 476
  ClientWidth = 414
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 414
    Height = 476
    SkinData.SkinSection = 'TRANSPARENT'
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 44
      Width = 412
      Height = 431
      Align = alClient
      Color = clGray
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      Columns = <
        item
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 88
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Alignment = taCenter
          Title.Caption = #50629#52404#47749
          Width = 260
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'Remark'
          Title.Alignment = taCenter
          Title.Caption = #49324#50857
          Width = 41
          Visible = True
        end>
    end
    object sPanel2: TsPanel
      Left = 1
      Top = 42
      Width = 412
      Height = 2
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alTop
      
      TabOrder = 1
    end
    object sPanel3: TsPanel
      Left = 1
      Top = 1
      Width = 412
      Height = 41
      Align = alTop
      
      TabOrder = 2
      object sSpeedButton1: TsSpeedButton
        Left = 256
        Top = 6
        Width = 9
        Height = 29
        ButtonStyle = tbsDivider
      end
      object btnExit: TsButton
        Left = 336
        Top = 6
        Width = 72
        Height = 29
        Cursor = crHandPoint
        Caption = #45803#44592
        TabOrder = 0
        TabStop = False
        OnClick = btnExitClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 20
        ContentMargin = 8
      end
      object sButton1: TsButton
        Left = 4
        Top = 6
        Width = 119
        Height = 29
        Cursor = crHandPoint
        Caption = #45824#49345#44144#47000#52376' '#52628#44032
        TabOrder = 1
        TabStop = False
        OnClick = sButton1Click
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 37
        ContentMargin = 2
      end
      object sButton2: TsButton
        Left = 189
        Top = 6
        Width = 64
        Height = 29
        Cursor = crHandPoint
        Caption = #49325#51228
        TabOrder = 2
        TabStop = False
        OnClick = sButton2Click
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 27
        ContentMargin = 4
      end
      object sButton3: TsButton
        Left = 124
        Top = 6
        Width = 64
        Height = 29
        Cursor = crHandPoint
        Caption = #49688#51221
        TabOrder = 3
        TabStop = False
        OnClick = sButton3Click
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 3
        ContentMargin = 4
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 392
    Top = 216
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM CODE2NDD WHERE Prefix = '#39'EDI_SEND'#39)
    Left = 16
    Top = 296
    object qryListPrefix: TStringField
      FieldName = 'Prefix'
      Size = 10
    end
    object qryListCODE: TStringField
      FieldName = 'CODE'
      Size = 12
    end
    object qryListRemark: TStringField
      FieldName = 'Remark'
      Size = 1
    end
    object qryListMap: TStringField
      FieldName = 'Map'
      Size = 10
    end
    object qryListNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 296
  end
end
