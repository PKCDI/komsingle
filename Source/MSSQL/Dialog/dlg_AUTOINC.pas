unit dlg_AUTOINC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, sSkinProvider, ExtCtrls, sPanel, Grids, DBGrids,
  acDBGrid, DB, ADODB;

type
  Tdlg_AUTOINC_frm = class(TDialogParent_frm)
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_AUTOINC_frm: Tdlg_AUTOINC_frm;

implementation

uses
  MSSQL, VarDefine;

{$R *.dfm}

procedure Tdlg_AUTOINC_frm.FormShow(Sender: TObject);
begin
  inherited;
  qryList.Close;
  qryList.Parameters[0].Value := LoginData.sID;
  qrylist.Open;
end;

end.
