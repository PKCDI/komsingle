unit Dialog_CopyLocapp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, Grids, DBGrids, acDBGrid, ExtCtrls, sSplitter,
  StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit,
  sSkinProvider, sPanel, DB, ADODB, StrUtils, DateUtils;

type
  TDialog_CopyLocapp_frm = class(TDialogParent_frm)
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sSplitter1: TsSplitter;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListMAINT_NO2: TStringField;
    qryListDATEE: TStringField;
    qryListUSER_ID: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListBUSINESS: TStringField;
    qryListBUSINESSNAME: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListOPEN_NO: TBCDField;
    qryListAPP_DATE: TStringField;
    qryListDOC_PRD: TBCDField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListTRANSPRT: TStringField;
    qryListTRANSPRTNAME: TStringField;
    qryListGOODDES: TStringField;
    qryListREMARK: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAPPLIC: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDOCCOPY1: TBCDField;
    qryListDOCCOPY2: TBCDField;
    qryListDOCCOPY3: TBCDField;
    qryListDOCCOPY4: TBCDField;
    qryListDOCCOPY5: TBCDField;
    qryListDOC_ETC: TStringField;
    qryListLOC_TYPE: TStringField;
    qryListLOC_TYPENAME: TStringField;
    qryListLOC_AMT: TBCDField;
    qryListLOC_AMTC: TStringField;
    qryListDOC_DTL: TStringField;
    qryListDOC_DTLNAME: TStringField;
    qryListDOC_NO: TStringField;
    qryListDOC_AMT: TBCDField;
    qryListDOC_AMTC: TStringField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListIM_NAME: TStringField;
    qryListIM_NAME1: TStringField;
    qryListIM_NAME2: TStringField;
    qryListIM_NAME3: TStringField;
    qryListDEST: TStringField;
    qryListDESTNAME: TStringField;
    qryListISBANK1: TStringField;
    qryListISBANK2: TStringField;
    qryListPAYMENT: TStringField;
    qryListPAYMENTNAME: TStringField;
    qryListEXGOOD: TStringField;
    qryListCHKNAME1: TStringField;
    qryListCHKNAME2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListLCNO: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListGOODDES1: TStringField;
    qryListREMARK1: TStringField;
    qryListDOC_ETC1: TStringField;
    qryListEXGOOD1: TStringField;
    dsList: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sBitBtn23Click(Sender: TObject);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog:String;

  end;

var
  Dialog_CopyLocapp_frm: TDialog_CopyLocapp_frm;

implementation

uses MSSQL, ICON, KISCalendar, Commonlib;

{$R *.dfm}

{ TDialog_CopyLocapp_frm }

function TDialog_CopyLocapp_frm.openDialog: string;
begin
  Result := '';
//------------------------------------------------------------------------------
//개설일자
//관리번호
//수혜자
//------------------------------------------------------------------------------
  com_SearchKeyword.ItemIndex := 0;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ReadList;

  IF ShowModal = mrOk then
    Result := qryListMAINT_NO.AsString;

end;

procedure TDialog_CopyLocapp_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_CopyLocapp_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Case com_SearchKeyword.ItemIndex of
      0: SQL.Add('WHERE APP_DATE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
      1: SQL.Add('WHERE MAINT_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE BENEFC1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    SQL.Add('ORDER BY APP_DATE');

    Open;
  end;
end;

procedure TDialog_CopyLocapp_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TDialog_CopyLocapp_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_CopyLocapp_frm.FormShow(Sender: TObject);
begin
  inherited;
  sDBGrid1.SetFocus;
end;

procedure TDialog_CopyLocapp_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  Case Key of
    VK_RETURN : ModalResult := mrOk;
    VK_ESCAPE : ModalResult := mrCancel;
  end;
end;

procedure TDialog_CopyLocapp_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsDBgrid).ScreenToClient(Mouse.CursorPos).Y > 17 Then
    ModalResult := mrOk;
end;

procedure TDialog_CopyLocapp_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
//  IF not (ProgramControlType in [ctInsert,ctModify]) Then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TDialog_CopyLocapp_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate1);
end;

procedure TDialog_CopyLocapp_frm.sBitBtn23Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate2);
end;

procedure TDialog_CopyLocapp_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

end.
