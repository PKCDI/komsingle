unit Dialog_AttachFromLOCRC1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit, ExtCtrls, sPanel,
  sSkinProvider;

type
  TDialog_AttachFromLOCRC1_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListRFF_NO: TStringField;
    qryListGET_DAT: TStringField;
    qryListISS_DAT: TStringField;
    qryListEXP_DAT: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListAPPLIC: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListAPPLIC6: TStringField;
    qryListRCT_AMT1: TBCDField;
    qryListRCT_AMT1C: TStringField;
    qryListRCT_AMT2: TBCDField;
    qryListRCT_AMT2C: TStringField;
    qryListRATE: TBCDField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListTQTY: TBCDField;
    qryListTQTY_G: TStringField;
    qryListTAMT: TBCDField;
    qryListTAMT_G: TStringField;
    qryListLOC_NO: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_NAME: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC2AMTC: TStringField;
    qryListEX_RATE: TBCDField;
    qryListLOADDATE: TStringField;
    qryListEXPDATE: TStringField;
    qryListLOC_REM: TStringField;
    qryListLOC_REM1: TMemoField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListNEGODT: TStringField;
    qryListNEGOAMT: TBCDField;
    qryListBSN_HSCODE: TStringField;
    qryListPRNO: TIntegerField;
    qryListAPPLIC7: TStringField;
    qryListBENEFC2: TStringField;
    qryListCK_S: TStringField;
    qryGoods: TADOQuery;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TBCDField;
    qryGoodsHS_CHK: TStringField;
    qryGoodsHS_NO: TStringField;
    qryGoodsNAME_CHK: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE: TStringField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsAMT: TBCDField;
    qryGoodsAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsSTAMT: TBCDField;
    qryGoodsSTAMT_G: TStringField;
    sDBGrid2: TsDBGrid;
    dsGoods: TDataSource;
    procedure sBitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
  private
    LOCRC1_SQL : String;
    LOCRC1_GOODS_SQL : string;
    procedure ReadList;
    { Private declarations }
  public
    { Public declarations }
    function openDialog():TModalResult;
  end;

var
  Dialog_AttachFromLOCRC1_frm: TDialog_AttachFromLOCRC1_frm;

implementation

uses DateUtils, KISCalendar, Commonlib, MSSQL, MessageDefine;

{$R *.dfm}

procedure TDialog_AttachFromLOCRC1_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
    if DMMssql.KISConnect.InTransaction then
    DMMssql.KISConnect.RollbackTrans;
  Close;
end;

procedure TDialog_AttachFromLOCRC1_frm.FormCreate(Sender: TObject);
begin
  inherited;
  //LOCRC1_H 테이블 데이터
  LOCRC1_SQL := qryList.SQL.Text;
  //LOCRC1_D1 테이블 데이터
  LOCRC1_GOODS_SQL := qryGoods.SQL.Text;
end;

procedure TDialog_AttachFromLOCRC1_frm.ReadList;
begin
    with qryList do
    begin
      Close;

      SQL.Text := LOCRC1_SQL;

      Case com_SearchKeyword.ItemIndex of
        0: SQL.Add(' WHERE ISS_DAT between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        1: SQL.Add(' WHERE  LOC_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
        2: SQL.Add(' WHERE  MAINT_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      end;

      SQL.Add('ORDER BY ISS_DAT');

      Open;
    end;

    with qryGoods do
    begin
      Close;
      SQL.Text := LOCRC1_GOODS_SQL;
      SQL.Add(' WHERE KEYY = ' + QuotedStr(qryListMAINT_NO.AsString) );
      Open;
    end;

end;

procedure TDialog_AttachFromLOCRC1_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_AttachFromLOCRC1_frm.com_SearchKeywordSelect(
  Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TDialog_AttachFromLOCRC1_frm.Mask_SearchDate1DblClick(
  Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
  //if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TDialog_AttachFromLOCRC1_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of

    900 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    901 : Mask_SearchDate1DblClick(Mask_SearchDate2);

  end;
end;

procedure TDialog_AttachFromLOCRC1_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid1.ScreenToClient(Mouse.CursorPos).Y>17 then
  begin
    IF qryList.RecordCount > 0 Then
    begin
      ModalResult := mrOk;
    end;
  end;
end;

procedure TDialog_AttachFromLOCRC1_frm.sBitBtn3Click(Sender: TObject);
begin
  inherited;
  IF qryList.RecordCount > 0 Then
  begin
    ModalResult := mrOk;
  end;
end;

function TDialog_AttachFromLOCRC1_frm.openDialog: TModalResult;
begin
  Readlist;

  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Result := ShowModal;
end;

procedure TDialog_AttachFromLOCRC1_frm.qryListAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  with qryGoods do
    begin
      Close;
      SQL.Text := LOCRC1_GOODS_SQL;
      SQL.Add(' WHERE KEYY = ' + QuotedStr(qryListMAINT_NO.AsString) );
      Open;
    end;

end;

end.
