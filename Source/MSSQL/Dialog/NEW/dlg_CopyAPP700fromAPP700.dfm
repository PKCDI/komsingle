inherited dlg_CopyAPP700fromAPP700_frm: Tdlg_CopyAPP700fromAPP700_frm
  Left = 518
  Top = 192
  Caption = '[APP700]'#52712#49548#48520#45733#54868#54872#49888#50857#51109' '#48373#49324' - '#50896#48376#45936#51060#53552' '#49440#53469
  ClientHeight = 581
  ClientWidth = 828
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 828
    Height = 581
    inherited sPanel2: TsPanel
      Width = 826
      DesignSize = (
        826
        56)
      inherited btnExit: TsButton
        Left = 754
      end
      inherited sButton1: TsButton
        Left = 681
      end
      inherited com_find: TsComboBox
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840)
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 826
      Height = 523
      Columns = <
        item
          Color = clBtnFace
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 156
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 82
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC1'
          Title.Alignment = taCenter
          Title.Caption = #49688#54812#51088
          Width = 248
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AP_BANK1'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#51008#54665
          Width = 209
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'APP_DATE'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#51068#51088
          Width = 92
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Active = True
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180101'
      end>
    SQL.Strings = (
      
        'SELECT A1.MAINT_NO, MESSAGE1, MESSAGE2, USER_ID, DATEE, APP_DATE' +
        ', IN_MATHOD, AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, AP_BANK4, AP' +
        '_BANK5, AD_BANK, AD_BANK1, AD_BANK2, AD_BANK3, AD_BANK4, AD_BANK' +
        '_BIC, AD_PAY, IL_NO1, IL_NO2, IL_NO3, IL_NO4, IL_NO5, IL_AMT1, I' +
        'L_AMT2, IL_AMT3, IL_AMT4, IL_AMT5, IL_CUR1, IL_CUR2, IL_CUR3, IL' +
        '_CUR4, IL_CUR5, AD_INFO1, AD_INFO2, AD_INFO3, AD_INFO4, AD_INFO5' +
        ', DOC_CD, EX_DATE, EX_PLACE, CHK1, CHK2, CHK3, prno, F_INTERFACE' +
        ', IMP_CD1, IMP_CD2, IMP_CD3, IMP_CD4, IMP_CD5,'
      
        'APPLIC1, APPLIC2, APPLIC3, APPLIC4, APPLIC5, BENEFC, BENEFC1, BE' +
        'NEFC2, BENEFC3, BENEFC4, BENEFC5, CD_AMT, CD_CUR, CD_PERP, CD_PE' +
        'RM, CD_MAX, AA_CV1, AA_CV2, AA_CV3, AA_CV4, DRAFT1, DRAFT2, DRAF' +
        'T3, MIX_PAY1, MIX_PAY2, MIX_PAY3, MIX_PAY4, DEF_PAY1, DEF_PAY2, ' +
        'DEF_PAY3, DEF_PAY4, PSHIP, TSHIP, LOAD_ON, FOR_TRAN, LST_DATE, S' +
        'HIP_PD1, SHIP_PD2, SHIP_PD3, SHIP_PD4, SHIP_PD5, SHIP_PD6, DESGO' +
        'OD_1,'
      
        'DOC_380, DOC_380_1, DOC_705, DOC_705_1, DOC_705_2, DOC_705_3, DO' +
        'C_705_4, DOC_740, DOC_740_1, DOC_740_2, DOC_740_3, DOC_740_4, DO' +
        'C_530, DOC_530_1, DOC_530_2, DOC_271, DOC_271_1, DOC_861, DOC_2A' +
        'A, DOC_2AA_1, ACD_2AA, ACD_2AA_1, ACD_2AB, ACD_2AC, ACD_2AD, ACD' +
        '_2AE, ACD_2AE_1, CHARGE, CHARGE_1, PERIOD, CONFIRMM, INSTRCT, IN' +
        'STRCT_1, EX_NAME1, EX_NAME2, EX_NAME3, EX_ADDR1, EX_ADDR2, ORIGI' +
        'N, ORIGIN_M, PL_TERM, TERM_PR, TERM_PR_M, SRBUHO, DOC_705_GUBUN,' +
        ' CARRIAGE, DOC_760, DOC_760_1, DOC_760_2, DOC_760_3, DOC_760_4, ' +
        'SUNJUCK_PORT, DOCHACK_PORT, CONFIRM_BICCD, CONFIRM_BANKNM, CONFI' +
        'RM_BANKBR, PERIOD_IDX,  PERIOD_TXT, SPECIAL_PAY,'
      'Mathod700.DOC_NAME as mathod_Name'
      ',Pay700.DOC_NAME as pay_Name'
      ',IMPCD700_1.DOC_NAME as Imp_Name_1'
      ',IMPCD700_2.DOC_NAME as Imp_Name_2'
      ',IMPCD700_3.DOC_NAME as Imp_Name_3'
      ',IMPCD700_4.DOC_NAME as Imp_Name_4'
      ',IMPCD700_5.DOC_NAME as Imp_Name_5'
      ',DocCD700.DOC_NAME as DOC_Name'
      ',doc705_700.DOC_NAME as doc705_Name'
      ',doc740_700.DOC_NAME as doc740_Name'
      ',doc760_700.DOC_NAME as doc760_Name'
      ',CHARGE700.DOC_NAME as CHARGE_Name'
      ',CARRIAGE700.DOC_NAME as CARRIAGE_NAME'
      'FROM [APP700_1] AS A1'
      'INNER JOIN [APP700_2] AS A2 ON A1.MAINT_NO = A2.MAINT_NO'
      'INNER JOIN [APP700_3] AS A3 ON A1.MAINT_NO = A3.MAINT_NO'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod700 ON A1.IN_MATHOD = Mathod700.' +
        'CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay700 ON A1.AD_PAY = Pay700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_1 ON A1.IMP_CD1 = IMPCD700_' +
        '1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_2 ON A1.IMP_CD2 = IMPCD700_' +
        '2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_3 ON A1.IMP_CD3 = IMPCD700_' +
        '3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_4 ON A1.IMP_CD4 = IMPCD700_' +
        '4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_5 ON A1.IMP_CD5 = IMPCD700_' +
        '5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39') DocCD700 ON A1.DOC_CD = DocCD700.COD' +
        'E'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_705'#39') doc705_700 ON A3.DOC_705_3 = doc705' +
        '_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc740_700 ON A3.DOC_740_3 = doc740' +
        '_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc760_700 ON A3.DOC_760_3 = doc760' +
        '_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CHARGE'#39') CHARGE700 ON A3.CHARGE = CHARGE700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'C_METHOD'#39') CARRIAGE700 ON A3.CARRIAGE = CARRI' +
        'AGE700.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 4
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_BANK_BIC: TStringField
      FieldName = 'AD_BANK_BIC'
      Size = 11
    end
    object qryListAD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListDOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListprno: TIntegerField
      FieldName = 'prno'
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListCD_AMT: TBCDField
      FieldName = 'CD_AMT'
      Precision = 18
    end
    object qryListCD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryListMIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryListMIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryListMIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryListMIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryListDEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryListDEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryListDEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryListDEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListDESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryListDOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object qryListDOC_380_1: TBCDField
      FieldName = 'DOC_380_1'
      Precision = 18
    end
    object qryListDOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object qryListDOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object qryListDOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object qryListDOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object qryListDOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object qryListDOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object qryListDOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object qryListDOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object qryListDOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object qryListDOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object qryListDOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object qryListDOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object qryListDOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object qryListDOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object qryListDOC_271_1: TBCDField
      FieldName = 'DOC_271_1'
      Precision = 18
    end
    object qryListDOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object qryListDOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object qryListDOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
    end
    object qryListACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object qryListACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object qryListACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object qryListACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object qryListACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object qryListACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object qryListACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
    end
    object qryListCHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryListCHARGE_1: TMemoField
      FieldName = 'CHARGE_1'
      BlobType = ftMemo
    end
    object qryListPERIOD: TBCDField
      FieldName = 'PERIOD'
      Precision = 18
    end
    object qryListCONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object qryListINSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object qryListINSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object qryListORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object qryListPL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object qryListTERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object qryListTERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object qryListSRBUHO: TStringField
      FieldName = 'SRBUHO'
      Size = 35
    end
    object qryListDOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object qryListCARRIAGE: TStringField
      FieldName = 'CARRIAGE'
      Size = 3
    end
    object qryListDOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object qryListDOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object qryListDOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object qryListDOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object qryListDOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListCONFIRM_BICCD: TStringField
      FieldName = 'CONFIRM_BICCD'
      Size = 11
    end
    object qryListCONFIRM_BANKNM: TStringField
      FieldName = 'CONFIRM_BANKNM'
      Size = 70
    end
    object qryListCONFIRM_BANKBR: TStringField
      FieldName = 'CONFIRM_BANKBR'
      Size = 70
    end
    object qryListPERIOD_IDX: TIntegerField
      FieldName = 'PERIOD_IDX'
    end
    object qryListPERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 35
    end
    object qryListSPECIAL_PAY: TMemoField
      FieldName = 'SPECIAL_PAY'
      BlobType = ftMemo
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListpay_Name: TStringField
      FieldName = 'pay_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListDOC_Name: TStringField
      FieldName = 'DOC_Name'
      Size = 100
    end
    object qryListdoc705_Name: TStringField
      FieldName = 'doc705_Name'
      Size = 100
    end
    object qryListdoc740_Name: TStringField
      FieldName = 'doc740_Name'
      Size = 100
    end
    object qryListdoc760_Name: TStringField
      FieldName = 'doc760_Name'
      Size = 100
    end
    object qryListCHARGE_Name: TStringField
      FieldName = 'CHARGE_Name'
      Size = 100
    end
    object qryListCARRIAGE_NAME: TStringField
      FieldName = 'CARRIAGE_NAME'
      Size = 100
    end
  end
  inherited qryCopy: TADOQuery
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT A1.MAINT_NO, MESSAGE1, MESSAGE2, USER_ID, DATEE, APP_DATE' +
        ', IN_MATHOD, AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, AP_BANK4, AP' +
        '_BANK5, AD_BANK, AD_BANK1, AD_BANK2, AD_BANK3, AD_BANK4, AD_BANK' +
        '_BIC, AD_PAY, IL_NO1, IL_NO2, IL_NO3, IL_NO4, IL_NO5, IL_AMT1, I' +
        'L_AMT2, IL_AMT3, IL_AMT4, IL_AMT5, IL_CUR1, IL_CUR2, IL_CUR3, IL' +
        '_CUR4, IL_CUR5, AD_INFO1, AD_INFO2, AD_INFO3, AD_INFO4, AD_INFO5' +
        ', DOC_CD, EX_DATE, EX_PLACE, CHK1, CHK2, CHK3, prno, F_INTERFACE' +
        ', IMP_CD1, IMP_CD2, IMP_CD3, IMP_CD4, IMP_CD5,'
      
        'APPLIC1, APPLIC2, APPLIC3, APPLIC4, APPLIC5, BENEFC, BENEFC1, BE' +
        'NEFC2, BENEFC3, BENEFC4, BENEFC5, CD_AMT, CD_CUR, CD_PERP, CD_PE' +
        'RM, CD_MAX, AA_CV1, AA_CV2, AA_CV3, AA_CV4, DRAFT1, DRAFT2, DRAF' +
        'T3, MIX_PAY1, MIX_PAY2, MIX_PAY3, MIX_PAY4, DEF_PAY1, DEF_PAY2, ' +
        'DEF_PAY3, DEF_PAY4, PSHIP, TSHIP, LOAD_ON, FOR_TRAN, LST_DATE, S' +
        'HIP_PD1, SHIP_PD2, SHIP_PD3, SHIP_PD4, SHIP_PD5, SHIP_PD6, DESGO' +
        'OD_1,'
      
        'DOC_380, DOC_380_1, DOC_705, DOC_705_1, DOC_705_2, DOC_705_3, DO' +
        'C_705_4, DOC_740, DOC_740_1, DOC_740_2, DOC_740_3, DOC_740_4, DO' +
        'C_530, DOC_530_1, DOC_530_2, DOC_271, DOC_271_1, DOC_861, DOC_2A' +
        'A, DOC_2AA_1, ACD_2AA, ACD_2AA_1, ACD_2AB, ACD_2AC, ACD_2AD, ACD' +
        '_2AE, ACD_2AE_1, CHARGE, CHARGE_1, PERIOD, CONFIRMM, INSTRCT, IN' +
        'STRCT_1, EX_NAME1, EX_NAME2, EX_NAME3, EX_ADDR1, EX_ADDR2, ORIGI' +
        'N, ORIGIN_M, PL_TERM, TERM_PR, TERM_PR_M, SRBUHO, DOC_705_GUBUN,' +
        ' CARRIAGE, DOC_760, DOC_760_1, DOC_760_2, DOC_760_3, DOC_760_4, ' +
        'SUNJUCK_PORT, DOCHACK_PORT, CONFIRM_BICCD, CONFIRM_BANKNM, CONFI' +
        'RM_BANKBR, PERIOD_IDX,  PERIOD_TXT, SPECIAL_PAY,'
      'Mathod700.DOC_NAME as mathod_Name'
      ',Pay700.DOC_NAME as pay_Name'
      ',IMPCD700_1.DOC_NAME as Imp_Name_1'
      ',IMPCD700_2.DOC_NAME as Imp_Name_2'
      ',IMPCD700_3.DOC_NAME as Imp_Name_3'
      ',IMPCD700_4.DOC_NAME as Imp_Name_4'
      ',IMPCD700_5.DOC_NAME as Imp_Name_5'
      ',DocCD700.DOC_NAME as DOC_Name'
      ',doc705_700.DOC_NAME as doc705_Name'
      ',doc740_700.DOC_NAME as doc740_Name'
      ',doc760_700.DOC_NAME as doc760_Name'
      ',CHARGE700.DOC_NAME as CHARGE_Name'
      ',CARRIAGE700.DOC_NAME as CARRIAGE_NAME'
      'FROM [APP700_1] AS A1'
      'INNER JOIN [APP700_2] AS A2 ON A1.MAINT_NO = A2.MAINT_NO'
      'INNER JOIN [APP700_3] AS A3 ON A1.MAINT_NO = A3.MAINT_NO'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod700 ON A1.IN_MATHOD = Mathod700.' +
        'CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay700 ON A1.AD_PAY = Pay700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_1 ON A1.IMP_CD1 = IMPCD700_' +
        '1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_2 ON A1.IMP_CD2 = IMPCD700_' +
        '2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_3 ON A1.IMP_CD3 = IMPCD700_' +
        '3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_4 ON A1.IMP_CD4 = IMPCD700_' +
        '4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_5 ON A1.IMP_CD5 = IMPCD700_' +
        '5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39') DocCD700 ON A1.DOC_CD = DocCD700.COD' +
        'E'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_705'#39') doc705_700 ON A3.DOC_705_3 = doc705' +
        '_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc740_700 ON A3.DOC_740_3 = doc740' +
        '_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc760_700 ON A3.DOC_760_3 = doc760' +
        '_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CHARGE'#39') CHARGE700 ON A3.CHARGE = CHARGE700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'C_METHOD'#39') CARRIAGE700 ON A3.CARRIAGE = CARRI' +
        'AGE700.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)')
  end
end
