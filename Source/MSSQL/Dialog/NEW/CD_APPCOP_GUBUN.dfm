inherited CD_APPCOP_GUBUN_frm: TCD_APPCOP_GUBUN_frm
  Left = 811
  Top = 264
  Caption = 'CD_APPCOP_GUBUN_frm'
  ClientHeight = 204
  ClientWidth = 453
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 453
    inherited sImage1: TsImage
      Left = 430
    end
  end
  inherited sPanel3: TsPanel
    Width = 453
    Height = 179
    inherited sPanel2: TsPanel
      Width = 443
      inherited sImage2: TsImage
        Left = 419
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 443
      Height = 135
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 55
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Alignment = taCenter
          Title.Caption = #47928#49436#44396#48516
          Width = 363
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME FROM CODE2NDD where Prefix = '#39'APPCOP'#44396#48516#39)
  end
end
