unit dlg_CopyAPPSPCfromVATBI2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dlg_SelectCopyDocument, DB, ADODB, Grids, DBGrids, acDBGrid,
  Mask, sMaskEdit, StdCtrls, sEdit, sComboBox, sButton, Buttons,
  sSpeedButton, ExtCtrls, sPanel;

type
  Tdlg_CopyAPPSPCfromVATBI2_frm = class(Tdlg_SelectCopyDocument_frm)
  private
    { Private declarations }

  protected
    procedure ReadList; override;

  public
    { Public declarations }
  end;

var
  dlg_CopyAPPSPCfromVATBI2_frm: Tdlg_CopyAPPSPCfromVATBI2_frm;

implementation

{$R *.dfm}

procedure Tdlg_CopyAPPSPCfromVATBI2_frm.ReadList;
var
  TempField : String;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := qryCopy.SQL.Text;
    Parameters[0].Value := sMaskEdit1.Text;
    Parameters[1].Value := sMaskEdit2.Text;

    if edt_find.Text <> '' then
    begin
      Case com_find.ItemIndex of
        0: SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+edt_find.Text+'%'));
        1: SQL.Add('AND RFF_NO LIKE '+QuotedStr('%'+edt_find.Text+'%'));
      end;
    end;

    SQL.Add('ORDER BY DATEE DESC');

    Open;
  end;
end;

end.
