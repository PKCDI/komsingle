// 지급지시서용도 코드
unit CD_4487;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_4487_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ReadList; override;
  public
    function GetCodeText(value: String): String; override;
    { Public declarations }
  end;

var
  CD_4487_frm: TCD_4487_frm;

implementation

{$R *.dfm}

{ TCodeDialogParent_frm1 }

function TCD_4487_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ''4487''';
    SQL.Add('AND CODE = '+QuotedStr(value));
    try
      Open;
      IF RecordCount > 0 Then
        Result := FieldByName('NAME').AsString;
    finally
      Close;
    end;
  end;
end;

procedure TCD_4487_frm.ReadList;
begin
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ''4487'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

procedure TCD_4487_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '지급지시서용도';
end;

end.
