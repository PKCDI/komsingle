inherited CD_PAYORDBFCD_frm: TCD_PAYORDBFCD_frm
  Left = 929
  Top = 321
  Caption = 'CD_PAYORDBFCD_frm'
  ClientHeight = 551
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    Height = 526
    inherited sDBGrid1: TsDBGrid
      Height = 482
      Color = clWhite
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 88
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NAME'
          Title.Alignment = taCenter
          Title.Caption = #44144#47000#54805#53468
          Width = 374
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT * FROM CODE2NDD Where Prefix = '#39'PAYORDBFCD'#39)
  end
end
