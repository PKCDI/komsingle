//------------------------------------------------------------------------------
// 수입용도
//------------------------------------------------------------------------------

unit CD_IMP_CD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_IMP_CD_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    function GetCodeText(value: String): String; override;
    { Public declarations }
  end;

var
  CD_IMP_CD_frm: TCD_IMP_CD_frm;

implementation

{$R *.dfm}

procedure TCD_IMP_CD_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '수입용도';
end;

function TCD_IMP_CD_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qryList do
  begin
    Close;
    SQL.Text := 'SELECT NAME FROM CODE2NDD with(nolock) WHERE prefix = ''IMP_CD'' AND CODE = '+QuotedStr(value);
    Open;

    IF RecordCount > 0 Then
      Result := FieldByName('NAME').AsString;
  end;
end;

procedure TCD_IMP_CD_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''IMP_CD'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

end.
