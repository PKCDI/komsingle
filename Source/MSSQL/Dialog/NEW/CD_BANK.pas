unit CD_BANK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TBANKINFO = record
    sName : string;
    sBrunch : string;
  end;

  TCD_BANK_frm = class(TCodeDialogParent_frm)
    qryListCODE: TStringField;
    qryListBankName1: TStringField;
    qryListBankBranch1: TStringField;
    qryListENAME2: TStringField;
    qryListENAME4: TStringField;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    function GetCodeText(value: String): TBANKINFO;
    { Public declarations }
  end;

var
  CD_BANK_frm: TCD_BANK_frm;

implementation

{$R *.dfm}

{ TCD_BANK_frm }

procedure TCD_BANK_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT [CODE] ,[ENAME1] as BankName1 ,[ENAME3] as BankBranch1, [ENAME2], [ENAME4] FROM BANKCODE';
    IF (sEdit1.Text <> '') Then
    begin
      Case sComboBox1.ItemIndex of
        0: SQL.Add( 'WHERE CODE LIKE '+QuotedStr('%'+sEdit1.Text+'%') );
        1: SQL.Add( 'WHERE ENAME1 LIKE '+QuotedStr('%'+sEdit1.Text+'%') );
        2: SQL.Add( 'WHERE ENAME3 LIKE '+QuotedStr('%'+sEdit1.Text+'%') );
      end;
    end;
    Open;
  end;
end;

procedure TCD_BANK_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '���༱��';
end;

function TCD_BANK_frm.GetCodeText(value: String): TBANKINFO;
begin
  Result.sName := 'NONE';
  Result.sBrunch := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT [CODE] ,[ENAME1] as BankName1 ,[ENAME3] as BankBranch1, [ENAME2], [ENAME4] FROM BANKCODE';
    SQL.Add('WHERE CODE = '+QuotedStr(value));
    Open;
    IF RecordCount > 0 Then
    begin
      Result.sName   := FieldByName('BankName1').AsString;
      Result.sBrunch := FieldByName('BankBranch1').AsString;
    end;
  end;
end;

end.
