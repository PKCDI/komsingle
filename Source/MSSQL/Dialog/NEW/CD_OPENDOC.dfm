inherited CD_OPENDOC_frm: TCD_OPENDOC_frm
  Left = 815
  Top = 270
  Caption = 'CD_OPENDOC_frm'
  ClientHeight = 310
  ClientWidth = 400
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 400
    inherited sImage1: TsImage
      Left = 377
    end
  end
  inherited sPanel3: TsPanel
    Width = 400
    Height = 285
    inherited sPanel2: TsPanel
      Width = 390
      inherited sImage2: TsImage
        Left = 366
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 390
      Height = 241
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 68
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #44060#49444#48169#48277
          Width = 297
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT * FROM CODE2NDD WHERE Prefix = '#39#44060#49444#48169#48277#39' AND Remark = 1')
  end
end
