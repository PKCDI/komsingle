inherited CD_4487_frm: TCD_4487_frm
  Caption = 'CD_4487_frm'
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    inherited sDBGrid1: TsDBGrid
      Color = clWhite
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NAME'
          Title.Alignment = taCenter
          Title.Caption = #51648#44553#51648#49884#49436
          Width = 375
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      
        'SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD Where Prefix = '#39'4' +
        '487'#39)
  end
end
