unit dlg_CopyLOCRCTfromLOCAPP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dlg_SelectCopyDocument, DB, ADODB, Grids, DBGrids, acDBGrid,
  Mask, sMaskEdit, StdCtrls, sEdit, sComboBox, sButton, Buttons,
  sSpeedButton, ExtCtrls, sPanel;

type
  Tdlg_CopyLOCRCTfromLOCAPP_frm = class(Tdlg_SelectCopyDocument_frm)
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_CopyLOCRCTfromLOCAPP_frm: Tdlg_CopyLOCRCTfromLOCAPP_frm;

implementation

{$R *.dfm}

procedure Tdlg_CopyLOCRCTfromLOCAPP_frm.FormShow(Sender: TObject);
begin
  FCaption := '내국신용장 개설신청서 가져오기';
  inherited;
end;

procedure Tdlg_CopyLOCRCTfromLOCAPP_frm.ReadList;
var
  TempField : String;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := qryCopy.SQL.Text;
    Parameters[0].Value := sMaskEdit1.Text;
    Parameters[1].Value := sMaskEdit2.Text;

    IF (edt_find.Text <> '') Then
    begin
      Case com_find.ItemIndex of
        0: SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+edt_find.Text+'%'));
        1: SQL.Add('AND LOC_NO LIKE '+QuotedStr('%'+edt_find.Text+'%'));
      end;
    end;

    SQL.Add('ORDER BY DATEE DESC');

    Open;
  end;
end;

end.
