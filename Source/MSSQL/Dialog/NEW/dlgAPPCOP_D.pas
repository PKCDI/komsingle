unit dlgAPPCOP_D;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit, sCurrencyEdit,
  StdCtrls, sEdit, ExtCtrls, sPanel, sButton, DB, ADODB, CodeDialogParent;

type
  TdlgAPPCOP_D_frm = class(TForm)
    sPanel1: TsPanel;
    edtMessage1: TsEdit;
    sEdit3: TsEdit;
    edtIMP_NO: TsEdit;
    edtBANK_CD: TsEdit;
    edtD_AMTC: TsEdit;
    currD_AMT: TsCurrencyEdit;
    edtTERMS: TsEdit;
    sEdit4: TsEdit;
    edtIMP_CD: TsEdit;
    sEdit6: TsEdit;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    btnNew: TsButton;
    btnEdit: TsButton;
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtD_AMTCDblClick(Sender: TObject);
  private
    FMaintNo : String;
    FaffSeq: Integer;
    CDDialog : TCodeDialogParent_frm;
    { Private declarations }
    procedure DataProcess;
    function newSEQ:integer;
  public
    { Public declarations }
    function NewItem(Maint_no : String):TModalResult;
    function EditItem(uFields : TFields):TModalResult;
    property affSeq:integer read FaffSeq;
  end;

var
  dlgAPPCOP_D_frm: TdlgAPPCOP_D_frm;

implementation

uses
  ICON, SQLCreator, MSSQL, CD_AMT_UNIT, CD_TERM, CD_IMP_CD;

{$R *.dfm}

{ TdlgAPPCOP_D_frm }

procedure TdlgAPPCOP_D_frm.DataProcess;
var
  SC : TSQLCreate;
begin
  SC := TSQLCreate.Create;

  if Self.Tag = 1 Then
  begin
    SC.DMLType := dmlInsert;
    SC.ADDValue('KEYY', FMaintNo);
    FaffSeq := newSEQ;
    SC.ADDValue('SEQ', FaffSeq);
  end
  else
  begin
    SC.DMLType := dmlUpdate;
    SC.ADDWhere('KEYY', FMaintNo);
    SC.ADDWhere('SEQ', FaffSeq);
  end;

  SC.SQLHeader('APPCOP_D');
  SC.ADDValue('IMP_NO', edtIMP_NO.Text);
  SC.ADDValue('BANK_CD', edtBANK_CD.Text);
  SC.ADDValue('D_AMTC', edtD_AMTC.Text);
  SC.ADDValue('D_AMT', currD_AMT.Value);
  SC.ADDValue('TERMS', edtTERMS.Text);
  SC.ADDValue('IMP_CD', edtIMP_CD.Text);

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := SC.CreateSQL;
      ExecSQL; 
    finally
      Close;
      Free;
    end;
  end;
end;

function TdlgAPPCOP_D_frm.EditItem(uFields: TFields): TModalResult;
begin
  Self.Tag := 2;
  FaffSeq := uFields.FieldByName('SEQ').AsInteger;
  FMaintNo := uFields.FieldByName('KEYY').AsString;

  edtIMP_NO.Text  := uFields.FieldByName('IMP_NO').AsString;
  edtBANK_CD.Text := uFields.FieldByName('BANK_CD').AsString;
  edtD_AMTC.Text := uFields.FieldByName('D_AMTC').AsString;
  currD_AMT.Value := uFields.FieldByName('D_AMT').AsFloat;
  edtTERMS.Text   := uFields.FieldByName('TERMS').AsString;
  edtIMP_CD.Text  := uFields.FieldByName('IMP_CD').AsString;

  IF self.ShowModal = mrOK Then
  begin
    DataProcess;
    Result := mrOK;
  end
  else
  begin
    Result := mrCancel;
  end;

end;

function TdlgAPPCOP_D_frm.NewItem(Maint_no: String): TModalResult;
begin
  Self.Tag := 1;
  FMaintNo := Maint_no;

  IF self.ShowModal = mrOK Then
  begin
    DataProcess;
    Result := mrOK;
  end
  else
  begin
    Result := mrCancel;
  end;  
end;

function TdlgAPPCOP_D_frm.newSEQ: integer;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT ISNULL(MAX(SEQ),0) as N_SEQ FROM [APPCOP_D] WHERE KEYY = '+QuotedStr(FMaintNo);
      Open;

      Result := FieldByName('N_SEQ').AsInteger + 1;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TdlgAPPCOP_D_frm.btnNewClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TdlgAPPCOP_D_frm.btnEditClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TdlgAPPCOP_D_frm.FormShow(Sender: TObject);
var
  FORM_POS : TPoint;
begin
  inherited;
  FORM_POS.X := TForm(Owner).Left;
  FORM_POS.Y := TForm(Owner).Top;
  FORM_POS := Application.MainForm.ClientToScreen(FORM_POS);
  Self.Left := FORM_POS.X+(TForm(Owner).Width div 2)-(Self.Width div 2);
  Self.Top  := FORM_POS.Y+(TForm(Owner).Height div 2)-(Self.Height div 2);
end;

procedure TdlgAPPCOP_D_frm.edtD_AMTCDblClick(Sender: TObject);
begin
  Case (Sender as TsEdit).Tag of
    100 : CDDialog := TCD_AMT_UNIT_frm.Create(Self);
    101 : CDDialog := TCD_TERM_frm.Create(Self);
    102 : CDDialog := TCD_IMP_CD_frm.Create(Self);
  end;

  try
    if CDDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CDDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        101 : sEdit4.Text := CDDialog.Values[1];
        102 : sEdit6.Text := CDDialog.Values[1];
      end;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

end.
