unit OKBottomDlg;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, sLabel, sButton, sCheckBox, sEdit, DB, ADODB,
  sComboBox, sRadioButton, DateUtils, SQLCreator, Clipbrd;

type
  TOKBottomDlg_frm = class(TForm)
    Bevel1: TBevel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sEdit1: TsEdit;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sCheckBox1: TsCheckBox;
    sButton1: TsButton;
    sButton2: TsButton;
    qryExec: TADOQuery;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sRadioButton3: TsRadioButton;
    sLabel5: TsLabel;
    sComboBox1: TsComboBox;
    sLabel6: TsLabel;
    procedure sButton1Click(Sender: TObject);
    procedure sCheckBox1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    FSQL : String;
    function RunSQL(objCreator : TSQLCreate):Boolean; overload;
    function RunSQL(sSQL : string):Boolean; overload;
    function RunSQLCount(sSQL : String):Integer;
  public
    { Public declarations }
    function ConvertData:Boolean; virtual; abstract;
  end;

var
  OKBottomDlg_frm: TOKBottomDlg_frm;

implementation

uses
  MSSQL;

{$R *.dfm}

function TOKBottomDlg_frm.RunSQL(sSQL: string): Boolean;
begin
  with qryExec do
  begin
    Close;
    SQL.Text := sSQL;
    ExecSQL;
  end;
end;

procedure TOKBottomDlg_frm.sButton1Click(Sender: TObject);
begin
//  IF MessageBox(TForm(Owner).Handle, '정말 진행하시겠습니까?', '가져오기 확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;
end;

procedure TOKBottomDlg_frm.sCheckBox1Click(Sender: TObject);
begin
  sButton1.Enabled := sCheckBox1.Checked;
end;

procedure TOKBottomDlg_frm.FormShow(Sender: TObject);
var
  i : Integer;
begin
  sComboBox1.Clear;
  for i := YearOf(Now) downto 2000 do
  begin
    sComboBox1.Items.Add(IntToStr(i));
  end;
  sComboBox1.ItemIndex := 0;
end;

function TOKBottomDlg_frm.RunSQLCount(sSQL: String): Integer;
begin
  Result := -1;
  with qryExec do
  begin
    Close;
    SQL.Text := sSQL;
    try
      Open;
      Result := qryExec.RecordCount;
    finally
      qryExec.Close;
    end;
  end;
end;

function TOKBottomDlg_frm.RunSQL(objCreator: TSQLCreate): Boolean;
var
  sLOG : String;
begin
  try
    sLOG := objCreator.FieldList;
    RunSQL(objCreator.CreateSQL);
  except
    on E:Exception do
    begin
      Clipboard.AsText := sLOG;
      raise Exception.Create(E.Message);
    end;
  end;
end;

end.
