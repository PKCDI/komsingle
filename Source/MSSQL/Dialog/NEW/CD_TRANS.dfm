inherited CD_TRANS_frm: TCD_TRANS_frm
  Caption = 'CD_TRANS_frm'
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 88
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #50868#49569#48169#48277
          Width = 375
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Active = True
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT * FROM CODE2NDD WHERE Prefix = '#39#50868#49569#49688#45800#39' AND Remark = 1')
    Top = 168
  end
  inherited dsList: TDataSource
    Top = 168
  end
end
