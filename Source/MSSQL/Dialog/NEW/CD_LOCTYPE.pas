unit CD_LOCTYPE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_LOCTYPE_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ReadList; override;
  public
    function GetCodeText(value: String): String; override;
    { Public declarations }
  end;

var
  CD_LOCTYPE_frm: TCD_LOCTYPE_frm;

implementation

{$R *.dfm}

{ TCodeDialogParent_frm1 }

procedure TCD_LOCTYPE_frm.ReadList;
begin
//  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''내국신4487'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

procedure TCD_LOCTYPE_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '신용장 종류';
end;

function TCD_LOCTYPE_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''내국신4487''';
    SQL.Add('AND CODE = '+QuotedStr(value));
    Open;
    IF RecordCount > 0 Then
      Result := FieldByName('NAME').AsString;
  end;
end;

end.
