inherited CD_C_METHOD_frm: TCD_C_METHOD_frm
  Caption = 'CD_C_METHOD_frm'
  ClientHeight = 337
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    Height = 312
    inherited sDBGrid1: TsDBGrid
      Height = 268
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 88
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #50868#49569#49688#45800
          Width = 374
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Active = True
    CursorType = ctStatic
    SQL.Strings = (
      
        'SELECT CODE,NAME FROM CODE2NDD with(nolock) WHERE prefix = '#39'C_ME' +
        'THOD'#39)
  end
end
