object dlg_SelectCopyDocument_frm: Tdlg_SelectCopyDocument_frm
  Left = 615
  Top = 187
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = 'dlg_SelectCopyDocument_frm'
  ClientHeight = 577
  ClientWidth = 871
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 871
    Height = 577
    Align = alClient
    
    TabOrder = 0
    object sPanel2: TsPanel
      Left = 1
      Top = 1
      Width = 869
      Height = 56
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alTop
      
      TabOrder = 0
      DesignSize = (
        869
        56)
      object sSpeedButton4: TsSpeedButton
        Left = 343
        Top = 5
        Width = 8
        Height = 45
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'SPEEDBUTTON'
      end
      object btnExit: TsButton
        Left = 798
        Top = 9
        Width = 69
        Height = 37
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #45803#44592
        ModalResult = 2
        TabOrder = 0
        TabStop = False
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 20
        ContentMargin = 8
      end
      object sButton1: TsButton
        Left = 725
        Top = 9
        Width = 69
        Height = 37
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #54869#51064
        ModalResult = 1
        TabOrder = 1
        TabStop = False
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 17
        ContentMargin = 8
      end
      object com_find: TsComboBox
        Left = 7
        Top = 28
        Width = 97
        Height = 23
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 2
        Text = #44160#49353'1'
        Items.Strings = (
          #44160#49353'1'
          #44160#49353'2'
          #44160#49353'3')
      end
      object edt_find: TsEdit
        Left = 105
        Top = 28
        Width = 171
        Height = 23
        TabOrder = 3
        OnKeyUp = edt_findKeyUp
      end
      object sButton2: TsButton
        Left = 277
        Top = 4
        Width = 63
        Height = 47
        Cursor = crHandPoint
        Caption = #51312#54924
        TabOrder = 4
        OnClick = sButton2Click
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 105
        Top = 4
        Width = 76
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        MaxLength = 10
        TabOrder = 5
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 198
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        MaxLength = 10
        TabOrder = 6
        Text = '20180621'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 869
      Height = 519
      Align = alClient
      Color = clWhite
      Ctl3D = True
      DataSource = dsList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid1DrawColumnCell
      OnDblClick = sDBGrid1DblClick
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    Left = 176
    Top = 160
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 208
    Top = 160
  end
  object qryCopy: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    Left = 240
    Top = 160
  end
end
