unit dlg_CopyLOCAPPfromDOMOFB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dlg_SelectCopyDocument, DB, ADODB, Grids, DBGrids, acDBGrid,
  StdCtrls, sEdit, sComboBox, sButton, Buttons, sSpeedButton, ExtCtrls,
  sPanel, Mask, sMaskEdit;

type
  Tdlg_CopyLOCAPPfromDOMOFB_frm = class(Tdlg_SelectCopyDocument_frm)
    qryListMAINT_NO: TStringField;
    qryListMaint_Rff: TStringField;
    qryListChk1: TStringField;
    qryListChk2: TStringField;
    qryListChk3: TStringField;
    qryListUSER_ID: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListDATEE: TStringField;
    qryListSR_CODE: TStringField;
    qryListSR_NO: TStringField;
    qryListSR_NAME1: TStringField;
    qryListSR_NAME2: TStringField;
    qryListSR_NAME3: TStringField;
    qryListSR_ADDR1: TStringField;
    qryListSR_ADDR2: TStringField;
    qryListSR_ADDR3: TStringField;
    qryListUD_CODE: TStringField;
    qryListUD_RENO: TStringField;
    qryListUD_NO: TStringField;
    qryListUD_NAME1: TStringField;
    qryListUD_NAME2: TStringField;
    qryListUD_NAME3: TStringField;
    qryListUD_ADDR1: TStringField;
    qryListUD_ADDR2: TStringField;
    qryListUD_ADDR3: TStringField;
    qryListOFR_NO: TStringField;
    qryListOFR_DATE: TStringField;
    qryListOFR_SQ: TStringField;
    qryListOFR_SQ1: TStringField;
    qryListOFR_SQ2: TStringField;
    qryListOFR_SQ3: TStringField;
    qryListOFR_SQ4: TStringField;
    qryListOFR_SQ5: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK_1: TMemoField;
    qryListPRNO: TIntegerField;
    qryListHS_CODE: TStringField;
    qryListMAINT_NO_1: TStringField;
    qryListTSTINST: TStringField;
    qryListTSTINST1: TStringField;
    qryListTSTINST2: TStringField;
    qryListTSTINST3: TStringField;
    qryListTSTINST4: TStringField;
    qryListTSTINST5: TStringField;
    qryListPCKINST: TStringField;
    qryListPCKINST1: TStringField;
    qryListPCKINST2: TStringField;
    qryListPCKINST3: TStringField;
    qryListPCKINST4: TStringField;
    qryListPCKINST5: TStringField;
    qryListPAY: TStringField;
    qryListPAY_ETC: TStringField;
    qryListPAY_ETC1: TStringField;
    qryListPAY_ETC2: TStringField;
    qryListPAY_ETC3: TStringField;
    qryListPAY_ETC4: TStringField;
    qryListPAY_ETC5: TStringField;
    qryListMAINT_NO_2: TStringField;
    qryListTERM_DEL: TStringField;
    qryListTERM_NAT: TStringField;
    qryListTERM_LOC: TStringField;
    qryListORGN1N: TStringField;
    qryListORGN1LOC: TStringField;
    qryListORGN2N: TStringField;
    qryListORGN2LOC: TStringField;
    qryListORGN3N: TStringField;
    qryListORGN3LOC: TStringField;
    qryListORGN4N: TStringField;
    qryListORGN4LOC: TStringField;
    qryListORGN5N: TStringField;
    qryListORGN5LOC: TStringField;
    qryListTRNS_ID: TStringField;
    qryListLOADD: TStringField;
    qryListLOADLOC: TStringField;
    qryListLOADTXT: TStringField;
    qryListLOADTXT1: TStringField;
    qryListLOADTXT2: TStringField;
    qryListLOADTXT3: TStringField;
    qryListLOADTXT4: TStringField;
    qryListLOADTXT5: TStringField;
    qryListDEST: TStringField;
    qryListDESTLOC: TStringField;
    qryListDESTTXT: TStringField;
    qryListDESTTXT1: TStringField;
    qryListDESTTXT2: TStringField;
    qryListDESTTXT3: TStringField;
    qryListDESTTXT4: TStringField;
    qryListDESTTXT5: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYCUR: TStringField;
    qryListTAMT: TBCDField;
    qryListTAMTCUR: TStringField;
    qryListCODE: TStringField;
    qryListDOC_NAME: TStringField;
    qryListCODE_1: TStringField;
    qryListDOC_NAME_1: TStringField;
    qryListCODE_2: TStringField;
    qryListDOC_NAME_2: TStringField;
    qryListTRANSNME: TStringField;
    qryListLOADDNAME: TStringField;
    qryListDESTNAME: TStringField;
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_CopyLOCAPPfromDOMOFB_frm: Tdlg_CopyLOCAPPfromDOMOFB_frm;

implementation

{$R *.dfm}

{ Tdlg_SelectCopyDocument_frm1 }

procedure Tdlg_CopyLOCAPPfromDOMOFB_frm.ReadList;
var
  TempField : String;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := qryCopy.SQL.Text;
    Parameters[0].Value := sMaskEdit1.Text;
    Parameters[1].Value := sMaskEdit2.Text;

    IF (edt_find.Text <> '') Then
    begin
      TempField := sDBGrid1.Columns[com_find.ItemIndex].FieldName;
      IF TempField = 'MAINT_NO' Then TempField := 'H1.MAINT_NO';

      SQL.Add('AND '+TempField+' LIKE '+QuotedStr('%'+edt_find.Text+'%'));
    end;

    SQL.Add('ORDER BY OFR_DATE DESC');

    Open;
  end;
end;

procedure Tdlg_CopyLOCAPPfromDOMOFB_frm.FormShow(Sender: TObject);
begin
  FCaption := '물품매도확약서(개설자 - 수신) 선택';
  inherited;  
end;

end.
