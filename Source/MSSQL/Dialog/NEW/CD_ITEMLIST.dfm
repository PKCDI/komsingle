inherited CD_ITEMLIST_frm: TCD_ITEMLIST_frm
  Left = 627
  Top = 164
  Caption = 'CD_ITEMLIST_frm'
  ClientWidth = 678
  OldCreateOrder = True
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 678
    inherited sImage1: TsImage
      Left = 655
    end
  end
  inherited sPanel3: TsPanel
    Width = 678
    inherited sPanel2: TsPanel
      Width = 668
      inherited sImage2: TsImage
        Left = 644
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 668
      Columns = <
        item
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #51228#54408#53076#46300
          Width = 121
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'HSCODE'
          Title.Alignment = taCenter
          Title.Caption = 'HS'#53076#46300
          Width = 92
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'FNAME'
          Title.Alignment = taCenter
          Title.Caption = #54408#47749
          Width = 74
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'MSIZE'
          Title.Alignment = taCenter
          Title.Caption = #44508#44201
          Width = 74
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'QTY_U'
          Title.Alignment = taCenter
          Title.Caption = #49688#47049
          Width = 50
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'QTYC'
          Title.Alignment = taCenter
          Title.Caption = #45800#50948
          Width = 50
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'PRICE'
          Title.Alignment = taCenter
          Title.Caption = #45800#44032
          Width = 50
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'PRICEG'
          Title.Alignment = taCenter
          Title.Caption = #45800#50948
          Width = 50
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'QTY_UG'
          Title.Alignment = taCenter
          Title.Caption = #44592#51456#49688#47049#45800#50948
          Width = 75
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT * FROM [ITEM_CODE]')
    object qryListCODE: TStringField
      FieldName = 'CODE'
    end
    object qryListHSCODE: TStringField
      FieldName = 'HSCODE'
      Size = 10
    end
    object qryListFNAME: TMemoField
      FieldName = 'FNAME'
      OnGetText = qryListFNAMEGetText
      BlobType = ftMemo
    end
    object qryListSNAME: TStringField
      FieldName = 'SNAME'
      Size = 35
    end
    object qryListMSIZE: TMemoField
      FieldName = 'MSIZE'
      OnGetText = qryListFNAMEGetText
      BlobType = ftMemo
    end
    object qryListQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryListPRICEG: TStringField
      FieldName = 'PRICEG'
      Size = 3
    end
    object qryListPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,0.###'
      Precision = 18
    end
    object qryListQTY_U: TBCDField
      FieldName = 'QTY_U'
      Precision = 18
    end
    object qryListQTY_UG: TStringField
      FieldName = 'QTY_UG'
      Size = 3
    end
  end
end
