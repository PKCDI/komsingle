unit CD_PAYORDBFCD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_PAYORDBFCD_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ReadList; override;
  public
    function GetCodeText(value: String): String; override;
    { Public declarations }
  end;

var
  CD_PAYORDBFCD_frm: TCD_PAYORDBFCD_frm;

implementation

{$R *.dfm}

{ TCodeDialogParent_frm1 }

function TCD_PAYORDBFCD_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''PAYORDBFCD''';
    SQL.Add('AND CODE = '+QuotedStr(value));
    try
      Open;
      IF RecordCount > 0 Then
        Result := FieldByName('NAME').AsString;
    finally
      Close;
    end;
  end;
end;

procedure TCD_PAYORDBFCD_frm.ReadList;
begin
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = ''PAYORDBFCD'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

procedure TCD_PAYORDBFCD_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '거래형태구분';
end;

end.
