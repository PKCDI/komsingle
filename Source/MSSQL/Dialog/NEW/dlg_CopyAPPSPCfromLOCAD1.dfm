inherited dlg_CopyAPPSPCfromLOCAD1_frm: Tdlg_CopyAPPSPCfromLOCAD1_frm
  Left = 806
  Top = 269
  Caption = #45236#44397#49888#50857#51109#51025#45813#49436' '#49440#53469
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    inherited sPanel2: TsPanel
      inherited com_find: TsComboBox
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840
          #45236#44397#49888#50857#51109#48264#54840)
      end
      inherited sMaskEdit1: TsMaskEdit
        Width = 75
        Text = ''
      end
      inherited sMaskEdit2: TsMaskEdit
        Width = 77
        Text = ''
      end
    end
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LC_NO'
          Title.Alignment = taCenter
          Title.Caption = #45236#44397#49888#50857#51109#48264#54840
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPLIC1'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#51088
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ADV_DATE'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC1AMT'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#44552#50529'('#50808#54868')'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC1AMTC'
          Title.Alignment = taCenter
          Title.Caption = #53685#54868#45800#50948
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC2AMT'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#44552#50529'('#50896#54868')'
          Width = 100
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Parameters = <
      item
        Name = 'DATE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'DATE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, CHK1, CHK2, CHK3, USER_ID, DATEE, BGM_REF, MESS' +
        'AGE1, MESSAGE2, BUSINESS, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFF' +
        'ERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO' +
        '9, OPEN_NO, ADV_DATE, ISS_DATE, DOC_PRD, DELIVERY, EXPIRY, TRANS' +
        'PRT, GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_B' +
        'ANK2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNA' +
        'ME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, DOCCOPY4, D' +
        'OCCOPY5, DOC_ETC, DOC_ETC1, LOC_TYPE, LOC1AMT, LOC1AMTC, LOC2AMT' +
        ', LOC2AMTC, EX_RATE, DOC_DTL, DOC_NO, DOC_AMT, DOC_AMTC, LOADDAT' +
        'E, EXPDATE, IM_NAME, IM_NAME1, IM_NAME2, IM_NAME3, DEST, ISBANK1' +
        ', ISBANK2, PAYMENT, EXGOOD, EXGOOD1, PRNO, NEGODT, NEGOAMT, BSN_' +
        'HSCODE, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADD' +
        'R3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      'FROM LOCAD1'
      'WHERE DATEE BETWEEN :DATE1 AND :DATE2')
  end
  inherited qryCopy: TADOQuery
    Parameters = <
      item
        Name = 'DATE1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'DATE2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, CHK1, CHK2, CHK3, USER_ID, DATEE, BGM_REF, MESS' +
        'AGE1, MESSAGE2, BUSINESS, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFF' +
        'ERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO' +
        '9, OPEN_NO, ADV_DATE, ISS_DATE, DOC_PRD, DELIVERY, EXPIRY, TRANS' +
        'PRT, GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_B' +
        'ANK2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNA' +
        'ME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, DOCCOPY4, D' +
        'OCCOPY5, DOC_ETC, DOC_ETC1, LOC_TYPE, LOC1AMT, LOC1AMTC, LOC2AMT' +
        ', LOC2AMTC, EX_RATE, DOC_DTL, DOC_NO, DOC_AMT, DOC_AMTC, LOADDAT' +
        'E, EXPDATE, IM_NAME, IM_NAME1, IM_NAME2, IM_NAME3, DEST, ISBANK1' +
        ', ISBANK2, PAYMENT, EXGOOD, EXGOOD1, PRNO, NEGODT, NEGOAMT, BSN_' +
        'HSCODE, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADD' +
        'R3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      'FROM LOCAD1'
      'WHERE DATEE BETWEEN :DATE1 AND :DATE2')
  end
end
