object dlgAPPCOP_D_frm: TdlgAPPCOP_D_frm
  Left = 931
  Top = 170
  BorderIcons = []
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = #44592#53440' '#51648#44553#49324#50976' '#46321#47197'/'#49688#51221
  ClientHeight = 221
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 384
    Height = 175
    Align = alClient
    
    TabOrder = 0
    OnDblClick = edtD_AMTCDblClick
    object edtMessage1: TsEdit
      Tag = 100
      Left = 122
      Top = 18
      Width = 33
      Height = 23
      TabStop = False
      CharCase = ecUpperCase
      Color = 12775866
      MaxLength = 3
      ReadOnly = True
      TabOrder = 0
      Text = 'ACB'
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44592#53440#51648#44553#49324#50976#53076#46300
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = [fsBold]
    end
    object sEdit3: TsEdit
      Tag = -1
      Left = 156
      Top = 18
      Width = 186
      Height = 23
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 7
      Text = #44592#53440#51648#44553#49324#50976'('#49688#51077#45824#44552' '#44208#51228')'
      SkinData.CustomColor = True
    end
    object edtIMP_NO: TsEdit
      Left = 122
      Top = 42
      Width = 220
      Height = 23
      Color = clWhite
      MaxLength = 35
      TabOrder = 1
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49688#51077#49888#44256#48264#54840
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edtBANK_CD: TsEdit
      Left = 122
      Top = 66
      Width = 167
      Height = 23
      Color = clWhite
      MaxLength = 35
      TabOrder = 2
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #51648#44553#51032#47280#51008#54665#53076#46300
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edtD_AMTC: TsEdit
      Tag = 100
      Left = 122
      Top = 90
      Width = 33
      Height = 23
      CharCase = ecUpperCase
      Color = 12775866
      MaxLength = 3
      TabOrder = 3
      OnDblClick = edtD_AMTCDblClick
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49888#52397#44552#50529
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = [fsBold]
    end
    object currD_AMT: TsCurrencyEdit
      Left = 156
      Top = 90
      Width = 133
      Height = 23
      Color = 12582911
      TabOrder = 4
      SkinData.CustomColor = True
      DecimalPlaces = 3
      DisplayFormat = '#,0.####'
    end
    object edtTERMS: TsEdit
      Tag = 101
      Left = 122
      Top = 114
      Width = 33
      Height = 23
      CharCase = ecUpperCase
      Color = 12775866
      MaxLength = 3
      TabOrder = 5
      OnDblClick = edtD_AMTCDblClick
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44032#44201#51312#44148
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sEdit4: TsEdit
      Tag = -1
      Left = 156
      Top = 114
      Width = 186
      Height = 23
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 8
      SkinData.CustomColor = True
    end
    object edtIMP_CD: TsEdit
      Tag = 102
      Left = 122
      Top = 138
      Width = 33
      Height = 23
      CharCase = ecUpperCase
      Color = 12775866
      MaxLength = 3
      TabOrder = 6
      OnDblClick = edtD_AMTCDblClick
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49688#51077#50857#46020
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sEdit6: TsEdit
      Tag = -1
      Left = 156
      Top = 138
      Width = 186
      Height = 23
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 9
      SkinData.CustomColor = True
    end
  end
  object sPanel2: TsPanel
    Left = 0
    Top = 175
    Width = 384
    Height = 2
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alBottom
    
    TabOrder = 1
  end
  object sPanel3: TsPanel
    Left = 0
    Top = 177
    Width = 384
    Height = 44
    Align = alBottom
    
    TabOrder = 2
    object btnNew: TsButton
      Left = 246
      Top = 4
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #51200#51109
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 0
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Tag = 1
      Left = 311
      Top = 4
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #52712#49548
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnEditClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
  end
end
