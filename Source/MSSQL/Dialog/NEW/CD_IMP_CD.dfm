inherited CD_IMP_CD_frm: TCD_IMP_CD_frm
  Caption = 'CD_IMP_CD_frm'
  ClientHeight = 420
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    Height = 395
    inherited sDBGrid1: TsDBGrid
      Height = 351
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 60
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #54637#47785
          Width = 403
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Active = True
    CursorType = ctStatic
    SQL.Strings = (
      
        'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'IMP_CD'#39' AND Rema' +
        'rk = 1')
  end
end
