unit CD_PAYORD1001;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_PAYORD1001_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    { Private declarations }
    procedure ReadList; override;
  public
    { Public declarations }  
    function GetCodeText(value: String): String; override;
  end;

var
  CD_PAYORD1001_frm: TCD_PAYORD1001_frm;

implementation

{$R *.dfm}

procedure TCD_PAYORD1001_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '입금관련서류';
end;

function TCD_PAYORD1001_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ''PAYORD1001''';
    SQL.Add('AND CODE = '+QuotedStr(value));
    try
      Open;
      IF RecordCount > 0 Then
        Result := FieldByName('NAME').AsString;
    finally
      Close;
    end;
  end;
end;

procedure TCD_PAYORD1001_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ''PAYORD1001'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

end.
