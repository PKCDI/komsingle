inherited CD_APPPCR_DOC_frm: TCD_APPPCR_DOC_frm
  Caption = 'CD_APPPCR_DOC_frm'
  ClientHeight = 251
  ClientWidth = 391
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 391
    inherited sImage1: TsImage
      Left = 368
    end
  end
  inherited sPanel3: TsPanel
    Width = 391
    Height = 226
    inherited sPanel2: TsPanel
      Width = 381
      inherited sImage2: TsImage
        Left = 357
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 381
      Height = 182
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = #47569#51008' '#44256#46357
          Title.Font.Style = [fsBold]
          Width = 85
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Alignment = taCenter
          Title.Caption = #44540#44144#49436#47448#47749
          Width = 271
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Active = True
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39'APPPCR'#49436#47448#39
      'AND Remark = 1')
  end
end
