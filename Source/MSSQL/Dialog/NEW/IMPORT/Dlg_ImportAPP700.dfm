inherited Dlg_ImportAPP700_frm: TDlg_ImportAPP700_frm
  Left = 1065
  Top = 360
  Caption = #49688#51077'L/C '#44032#51256#50724#44592
  ClientHeight = 364
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  inherited sButton1: TsButton
    Caption = 'Import'
  end
  inherited sButton2: TsButton
    Caption = 'Close'
  end
  object sProgressBar1: TsProgressBar [11]
    Left = 0
    Top = 346
    Width = 314
    Height = 18
    Align = alBottom
    BorderWidth = 1
    Min = 0
    Max = 100
    TabOrder = 4
  end
  inherited sRadioButton1: TsRadioButton
    TabOrder = 5
  end
  inherited sRadioButton2: TsRadioButton
    TabOrder = 6
  end
  inherited sRadioButton3: TsRadioButton
    TabOrder = 7
  end
  inherited sComboBox1: TsComboBox
    Width = 105
    TabOrder = 8
  end
  object Query1: TQuery
    DatabaseName = 'KOM_SINGLE'
    ParamCheck = False
    SQL.Strings = (
      
        'SELECT A1.MAINT_NO, A1.MESSAGE1, A1.MESSAGE2, A1.USER_ID, A1.DAT' +
        'EE, A1.APP_DATE, A1.IN_MATHOD, A1.AP_BANK, A1.AP_BANK1, A1.AP_BA' +
        'NK2,'
      
        'A1.AP_BANK3, A1.AP_BANK4, A1.AP_BANK5, A1.AD_BANK, A1.AD_BANK1, ' +
        'A1.AD_BANK2, A1.AD_BANK3, A1.AD_BANK4, A1.AD_PAY, A1.IL_NO1, A1.' +
        'IL_NO2,'
      
        'A1.IL_NO3, A1.IL_NO4, A1.IL_NO5, A1.IL_AMT1, A1.IL_AMT2, A1.IL_A' +
        'MT3, A1.IL_AMT4, A1.IL_AMT5, A1.IL_CUR1, A1.IL_CUR2, A1.IL_CUR3,' +
        ' A1.IL_CUR4,'
      
        'A1.IL_CUR5, A1.AD_INFO1, A1.AD_INFO2, A1.AD_INFO3, A1.AD_INFO4, ' +
        'A1.AD_INFO5, A1.DOC_CD, A1.EX_DATE, A1.EX_PLACE, A1.CHK1, A1.CHK' +
        '2, A1.CHK3,'
      
        'A1.IMP_CD1, A1.IMP_CD2, A1.IMP_CD3, A1.IMP_CD4, A1.IMP_CD5, A1.F' +
        '_INTERFACE, A1.prno, A1.AD_BANK_BIC, A1.CO_BANK, A1.CO_BANK1, A1' +
        '.CO_BANK2, A1.CO_BANK3, A1.CO_BANK4,'
      
        'A2.APPLIC1, A2.APPLIC2, A2.APPLIC3, A2.APPLIC4, A2.APPLIC5, A2.B' +
        'ENEFC, A2.BENEFC1, A2.BENEFC2, A2.BENEFC3, A2.BENEFC4, A2.BENEFC' +
        '5, A2.CD_AMT, A2.CD_CUR, A2.CD_PERP,'
      
        'A2.CD_PERM, A2.CD_MAX, A2.AA_CV1, A2.AA_CV2, A2.AA_CV3, A2.AA_CV' +
        '4, A2.DRAFT1, A2.DRAFT2, A2.DRAFT3, A2.MIX_PAY1, A2.MIX_PAY2, A2' +
        '.MIX_PAY3, A2.MIX_PAY4, A2.DEF_PAY1,'
      
        'A2.DEF_PAY2, A2.DEF_PAY3, A2.DEF_PAY4, A2.PSHIP, A2.TSHIP, A2.LO' +
        'AD_ON, A2.FOR_TRAN, A2.LST_DATE, A2.SHIP_PD1, A2.SHIP_PD2, A2.SH' +
        'IP_PD3, A2.SHIP_PD4, A2.SHIP_PD5, A2.SHIP_PD6,'
      'A2.DESGOOD_1, A2.SPECIAL_DESC, A2.SPECIAL_DESC_1,'
      
        'A3.DOC_380, A3.DOC_380_1, A3.DOC_705, A3.DOC_705_1, A3.DOC_705_2' +
        ', A3.DOC_705_3, A3.DOC_705_4, A3.DOC_740, A3.DOC_740_1, A3.DOC_7' +
        '40_2, A3.DOC_740_3, A3.DOC_740_4,'
      
        'A3.DOC_530, A3.DOC_530_1, A3.DOC_530_2, A3.DOC_271, A3.DOC_271_1' +
        ', A3.DOC_861, A3.DOC_2AA, A3.DOC_2AA_1, A3.ACD_2AA, A3.ACD_2AA_1' +
        ', A3.ACD_2AB, A3.ACD_2AC, A3.ACD_2AD,'
      
        'A3.ACD_2AE, A3.ACD_2AE_1, A3.CHARGE, A3.PERIOD, A3.CONFIRMM, A3.' +
        'INSTRCT, A3.INSTRCT_1, A3.EX_NAME1, A3.EX_NAME2, A3.EX_NAME3, A3' +
        '.EX_ADDR1, A3.EX_ADDR2, A3.ORIGIN,'
      
        'A3.ORIGIN_M, A3.PL_TERM, A3.TERM_PR, A3.TERM_PR_M, A3.SRBUHO, A3' +
        '.DOC_705_GUBUN, A3.CARRIAGE, A3.DOC_760, A3.DOC_760_1, A3.DOC_76' +
        '0_2, A3.DOC_760_3, A3.DOC_760_4,'
      
        'A3.SUNJUCK_PORT, A3.DOCHACK_PORT, A3.CHARGE_NUM1, A3.CHARGE_NUM2' +
        ', A3.CHARGE_NUM3, A3.CHARGE_NUM4, A3.CHARGE_NUM5, A3.CHARGE_NUM6' +
        ', A3.PERIOD_TXT'
      'FROM APP700_1 A1'
      'INNER JOIN APP700_2 A2 ON A1.MAINT_NO = A2.MAINT_NO'
      'INNER JOIN APP700_3 A3 ON A1.MAINT_NO = A3.MAINT_NO')
    Left = 232
    Top = 104
    object Query1MAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object Query1MESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object Query1MESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object Query1USER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object Query1DATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object Query1APP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object Query1IN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object Query1AP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object Query1AP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object Query1AP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object Query1AP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object Query1AP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object Query1AP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object Query1AD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 4
    end
    object Query1AD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object Query1AD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object Query1AD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object Query1AD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object Query1AD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object Query1IL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object Query1IL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object Query1IL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object Query1IL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object Query1IL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object Query1IL_AMT1: TFloatField
      FieldName = 'IL_AMT1'
    end
    object Query1IL_AMT2: TFloatField
      FieldName = 'IL_AMT2'
    end
    object Query1IL_AMT3: TFloatField
      FieldName = 'IL_AMT3'
    end
    object Query1IL_AMT4: TFloatField
      FieldName = 'IL_AMT4'
    end
    object Query1IL_AMT5: TFloatField
      FieldName = 'IL_AMT5'
    end
    object Query1IL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object Query1IL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object Query1IL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object Query1IL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object Query1IL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object Query1AD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object Query1AD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object Query1AD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object Query1AD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object Query1AD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object Query1DOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object Query1EX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object Query1EX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object Query1CHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object Query1CHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object Query1CHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object Query1IMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object Query1IMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object Query1IMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object Query1IMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object Query1IMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object Query1F_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object Query1prno: TIntegerField
      FieldName = 'prno'
    end
    object Query1AD_BANK_BIC: TStringField
      FieldName = 'AD_BANK_BIC'
      Size = 11
    end
    object Query1CO_BANK: TStringField
      FieldName = 'CO_BANK'
      Size = 11
    end
    object Query1CO_BANK1: TStringField
      FieldName = 'CO_BANK1'
      Size = 35
    end
    object Query1CO_BANK2: TStringField
      FieldName = 'CO_BANK2'
      Size = 35
    end
    object Query1CO_BANK3: TStringField
      FieldName = 'CO_BANK3'
      Size = 35
    end
    object Query1CO_BANK4: TStringField
      FieldName = 'CO_BANK4'
      Size = 35
    end
    object Query1APPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object Query1APPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object Query1APPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object Query1APPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object Query1APPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object Query1BENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object Query1BENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object Query1BENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object Query1BENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object Query1BENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object Query1BENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object Query1CD_AMT: TFloatField
      FieldName = 'CD_AMT'
    end
    object Query1CD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object Query1CD_PERP: TFloatField
      FieldName = 'CD_PERP'
    end
    object Query1CD_PERM: TFloatField
      FieldName = 'CD_PERM'
    end
    object Query1CD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object Query1AA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object Query1AA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object Query1AA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object Query1AA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object Query1DRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object Query1DRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object Query1DRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object Query1MIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object Query1MIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object Query1MIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object Query1MIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object Query1DEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object Query1DEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object Query1DEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object Query1DEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object Query1PSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object Query1TSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object Query1LOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object Query1FOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object Query1LST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object Query1SHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object Query1SHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object Query1SHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object Query1SHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object Query1SHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object Query1SHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object Query1DESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query1SPECIAL_DESC: TStringField
      FieldName = 'SPECIAL_DESC'
      Size = 1
    end
    object Query1SPECIAL_DESC_1: TMemoField
      FieldName = 'SPECIAL_DESC_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query1DOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object Query1DOC_380_1: TFloatField
      FieldName = 'DOC_380_1'
    end
    object Query1DOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object Query1DOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object Query1DOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object Query1DOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object Query1DOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object Query1DOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object Query1DOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object Query1DOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object Query1DOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object Query1DOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object Query1DOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object Query1DOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object Query1DOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object Query1DOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object Query1DOC_271_1: TFloatField
      FieldName = 'DOC_271_1'
    end
    object Query1DOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object Query1DOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object Query1DOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query1ACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object Query1ACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object Query1ACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object Query1ACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object Query1ACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object Query1ACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object Query1ACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query1CHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object Query1PERIOD: TFloatField
      FieldName = 'PERIOD'
    end
    object Query1CONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object Query1INSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object Query1INSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query1EX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object Query1EX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object Query1EX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object Query1EX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object Query1EX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object Query1ORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object Query1ORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object Query1PL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object Query1TERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object Query1TERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object Query1SRBUHO: TStringField
      FieldName = 'SRBUHO'
      Size = 35
    end
    object Query1DOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object Query1CARRIAGE: TStringField
      FieldName = 'CARRIAGE'
      Size = 3
    end
    object Query1DOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object Query1DOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object Query1DOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object Query1DOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object Query1DOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object Query1SUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object Query1DOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object Query1CHARGE_NUM1: TStringField
      FieldName = 'CHARGE_NUM1'
      Size = 35
    end
    object Query1CHARGE_NUM2: TStringField
      FieldName = 'CHARGE_NUM2'
      Size = 35
    end
    object Query1CHARGE_NUM3: TStringField
      FieldName = 'CHARGE_NUM3'
      Size = 35
    end
    object Query1CHARGE_NUM4: TStringField
      FieldName = 'CHARGE_NUM4'
      Size = 35
    end
    object Query1CHARGE_NUM5: TStringField
      FieldName = 'CHARGE_NUM5'
      Size = 35
    end
    object Query1CHARGE_NUM6: TStringField
      FieldName = 'CHARGE_NUM6'
      Size = 35
    end
    object Query1PERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 35
    end
  end
end
