inherited Dlg_ImportINF707_frm: TDlg_ImportINF707_frm
  Left = 1450
  Top = 237
  Caption = #51312#44148#48320#44221#54869#51064#49436' '#44032#51256#50724#44592
  ClientHeight = 366
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  inherited sEdit1: TsEdit
    Enabled = False
  end
  inherited sButton1: TsButton
    Caption = 'Import'
  end
  inherited sButton2: TsButton
    Caption = 'Close'
  end
  inherited sRadioButton1: TsRadioButton
    Enabled = False
  end
  inherited sRadioButton2: TsRadioButton
    Enabled = False
  end
  inherited sComboBox1: TsComboBox
    Enabled = False
  end
  object sProgressBar1: TsProgressBar [15]
    Left = 0
    Top = 348
    Width = 314
    Height = 18
    Align = alBottom
    BorderWidth = 1
    Min = 0
    Max = 100
    TabOrder = 8
  end
  object Query3: TQuery
    DatabaseName = 'KOM_SINGLE'
    ParamCheck = False
    SQL.Strings = (
      'SELECT'
      
        'A1.MAINT_NO, A1.MSEQ, A1.AMD_NO, A1.MESSAGE1, A1.MESSAGE2, A1.US' +
        'ER_ID, A1.DATEE, A1.APP_DATE, A1.IN_MATHOD, A1.AP_BANK,'
      
        'A1.AP_BANK1, A1.AP_BANK2, A1.AP_BANK3, A1.AP_BANK4, A1.AP_BANK5,' +
        ' A1.AD_BANK, A1.AD_BANK1, A1.AD_BANK2, A1.AD_BANK3,'
      
        'A1.AD_BANK4, A1.IL_NO1, A1.IL_NO2, A1.IL_NO3, A1.IL_NO4, A1.IL_N' +
        'O5, A1.IL_AMT1, A1.IL_AMT2, A1.IL_AMT3, A1.IL_AMT4,'
      
        'A1.IL_AMT5, A1.IL_CUR1, A1.IL_CUR2, A1.IL_CUR3, A1.IL_CUR4, A1.I' +
        'L_CUR5, A1.AD_INFO1, A1.AD_INFO2, A1.AD_INFO3,'
      
        'A1.AD_INFO4, A1.AD_INFO5, A1.CD_NO, A1.RCV_REF, A1.IBANK_REF, A1' +
        '.ISS_BANK1, A1.ISS_BANK2, A1.ISS_BANK3, A1.ISS_BANK4,'
      
        'A1.ISS_BANK5, A1.ISS_ACCNT, A1.ISS_DATE, A1.AMD_DATE, A1.EX_DATE' +
        ', A1.EX_PLACE, A1.CHK1, A1.CHK2, A1.CHK3, A1.IMP_CD1,'
      
        'A1.IMP_CD2, A1.IMP_CD3, A1.IMP_CD4, A1.IMP_CD5, A1.F_INTERFACE, ' +
        'A1.prno, A1.PURP_MSG, A1.CAN_REQ, A1.CON_INST, A1.DOC_TYPE,'
      
        'A1.CHARGE, A1.CHARGE_NUM1, A1.CHARGE_NUM2, A1.CHARGE_NUM3, A1.CH' +
        'ARGE_NUM4, A1.CHARGE_NUM5, A1.CHARGE_NUM6, A1.AMD_CHARGE,'
      
        'A1.AMD_CHARGE_NUM1, A1.AMD_CHARGE_NUM2, A1.AMD_CHARGE_NUM3, A1.A' +
        'MD_CHARGE_NUM4, A1.AMD_CHARGE_NUM5, A1.AMD_CHARGE_NUM6,'
      
        'A1.TSHIP, A1.PSHIP, A1.DRAFT1, A1.DRAFT2, A1.DRAFT3, A1.MIX_PAY1' +
        ', A1.MIX_PAY2, A1.MIX_PAY3, A1.MIX_PAY4, A1.DEF_PAY1,'
      
        'A1.DEF_PAY2, A1.DEF_PAY3, A1.DEF_PAY4, A1.APPLICABLE_RULES_1, A1' +
        '.APPLICABLE_RULES_2, A1.AVAIL, A1.AVAIL1, A1.AVAIL2,'
      
        'A1.AVAIL3, A1.AVAIL4, A1.AV_ACCNT, A1.DRAWEE, A1.DRAWEE1, A1.DRA' +
        'WEE2, A1.DRAWEE3, A1.DRAWEE4, A1.CO_BANK, A1.CO_BANK1,'
      
        'A1.CO_BANK2, A1.CO_BANK3, A1.CO_BANK4, A1.REI_BANK, A1.REI_BANK1' +
        ', A1.REI_BANK2, A1.REI_BANK3, A1.REI_BANK4, A1.AVT_BANK,'
      
        'A1.AVT_BANK1, A1.AVT_BANK2, A1.AVT_BANK3, A1.AVT_BANK4, A1.AMD_A' +
        'PPLIC1, A1.AMD_APPLIC2, A1.AMD_APPLIC3, A1.AMD_APPLIC4,'
      'A1.PERIOD, A1.PERIOD_TXT,'
      
        'A2.MAINT_NO, A2.MSEQ, A2.AMD_NO, A2.APPLIC1, A2.APPLIC2, A2.APPL' +
        'IC3, A2.APPLIC4, A2.APPLIC5, A2.BENEFC1, A2.BENEFC2,'
      
        'A2.BENEFC3, A2.BENEFC4, A2.BENEFC5, A2.INCD_CUR, A2.INCD_AMT, A2' +
        '.DECD_CUR, A2.DECD_AMT, A2.NWCD_CUR, A2.NWCD_AMT,'
      
        'A2.CD_PERP, A2.CD_PERM, A2.CD_MAX, A2.AA_CV1, A2.AA_CV2, A2.AA_C' +
        'V3, A2.AA_CV4, A2.LOAD_ON, A2.FOR_TRAN, A2.LST_DATE,'
      
        'A2.SHIP_PD, A2.SHIP_PD1, A2.SHIP_PD2, A2.SHIP_PD3, A2.SHIP_PD4, ' +
        'A2.SHIP_PD5, A2.SHIP_PD6, A2.NARRAT, A2.NARRAT_1,'
      
        'A2.SR_INFO1, A2.SR_INFO2, A2.SR_INFO3, A2.SR_INFO4, A2.SR_INFO5,' +
        ' A2.SR_INFO6, A2.EX_NAME1, A2.EX_NAME2, A2.EX_NAME3,'
      
        'A2.EX_ADDR1, A2.EX_ADDR2, A2.OP_BANK1, A2.OP_BANK2, A2.OP_BANK3,' +
        ' A2.OP_ADDR1, A2.OP_ADDR2, A2.BFCD_AMT, A2.BFCD_CUR,'
      
        'A2.SUNJUCK_PORT, A2.DOCHACK_PORT, A2.GOODS_DESC, A2.GOODS_DESC_1' +
        ', A2.DOC_DESC, A2.DOC_DESC_1, A2.ADD_DESC, A2.ADD_DESC_1,'
      'A2.SPECIAL_DESC, A2.SPECIAL_DESC_1, A2.INST_DESC, A2.INST_DESC_1'
      
        'FROM INF707_1 A1 INNER JOIN INF707_2 A2 ON A1.MAINT_NO = A2.MAIN' +
        'T_NO AND A1.MSEQ = A2.MSEQ')
    Left = 232
    Top = 104
    object Query3MAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Origin = 'KOM_SINGLE."INF707_1.DB".MAINT_NO'
      Size = 35
    end
    object Query3MSEQ: TIntegerField
      FieldName = 'MSEQ'
      Origin = 'KOM_SINGLE."INF707_1.DB".MSEQ'
    end
    object Query3AMD_NO: TIntegerField
      FieldName = 'AMD_NO'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_NO'
    end
    object Query3MESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Origin = 'KOM_SINGLE."INF707_1.DB".MESSAGE1'
      Size = 3
    end
    object Query3MESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Origin = 'KOM_SINGLE."INF707_1.DB".MESSAGE2'
      Size = 3
    end
    object Query3USER_ID: TStringField
      FieldName = 'USER_ID'
      Origin = 'KOM_SINGLE."INF707_1.DB".USER_ID'
      Size = 10
    end
    object Query3DATEE: TStringField
      FieldName = 'DATEE'
      Origin = 'KOM_SINGLE."INF707_1.DB".DATEE'
      Size = 8
    end
    object Query3APP_DATE: TStringField
      FieldName = 'APP_DATE'
      Origin = 'KOM_SINGLE."INF707_1.DB".APP_DATE'
      Size = 8
    end
    object Query3IN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Origin = 'KOM_SINGLE."INF707_1.DB".IN_MATHOD'
      Size = 3
    end
    object Query3AP_BANK: TStringField
      FieldName = 'AP_BANK'
      Origin = 'KOM_SINGLE."INF707_1.DB".AP_BANK'
      Size = 11
    end
    object Query3AP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Origin = 'KOM_SINGLE."INF707_1.DB".AP_BANK1'
      Size = 35
    end
    object Query3AP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Origin = 'KOM_SINGLE."INF707_1.DB".AP_BANK2'
      Size = 35
    end
    object Query3AP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Origin = 'KOM_SINGLE."INF707_1.DB".AP_BANK3'
      Size = 35
    end
    object Query3AP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Origin = 'KOM_SINGLE."INF707_1.DB".AP_BANK4'
      Size = 35
    end
    object Query3AP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Origin = 'KOM_SINGLE."INF707_1.DB".AP_BANK5'
      Size = 35
    end
    object Query3AD_BANK: TStringField
      FieldName = 'AD_BANK'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_BANK'
      Size = 11
    end
    object Query3AD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_BANK1'
      Size = 35
    end
    object Query3AD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_BANK2'
      Size = 35
    end
    object Query3AD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_BANK3'
      Size = 35
    end
    object Query3AD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_BANK4'
      Size = 35
    end
    object Query3IL_NO1: TStringField
      FieldName = 'IL_NO1'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_NO1'
      Size = 35
    end
    object Query3IL_NO2: TStringField
      FieldName = 'IL_NO2'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_NO2'
      Size = 35
    end
    object Query3IL_NO3: TStringField
      FieldName = 'IL_NO3'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_NO3'
      Size = 35
    end
    object Query3IL_NO4: TStringField
      FieldName = 'IL_NO4'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_NO4'
      Size = 35
    end
    object Query3IL_NO5: TStringField
      FieldName = 'IL_NO5'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_NO5'
      Size = 35
    end
    object Query3IL_AMT1: TFloatField
      FieldName = 'IL_AMT1'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_AMT1'
    end
    object Query3IL_AMT2: TFloatField
      FieldName = 'IL_AMT2'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_AMT2'
    end
    object Query3IL_AMT3: TFloatField
      FieldName = 'IL_AMT3'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_AMT3'
    end
    object Query3IL_AMT4: TFloatField
      FieldName = 'IL_AMT4'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_AMT4'
    end
    object Query3IL_AMT5: TFloatField
      FieldName = 'IL_AMT5'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_AMT5'
    end
    object Query3IL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_CUR1'
      Size = 3
    end
    object Query3IL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_CUR2'
      Size = 3
    end
    object Query3IL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_CUR3'
      Size = 3
    end
    object Query3IL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_CUR4'
      Size = 3
    end
    object Query3IL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Origin = 'KOM_SINGLE."INF707_1.DB".IL_CUR5'
      Size = 3
    end
    object Query3AD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_INFO1'
      Size = 70
    end
    object Query3AD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_INFO2'
      Size = 70
    end
    object Query3AD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_INFO3'
      Size = 70
    end
    object Query3AD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_INFO4'
      Size = 70
    end
    object Query3AD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Origin = 'KOM_SINGLE."INF707_1.DB".AD_INFO5'
      Size = 70
    end
    object Query3CD_NO: TStringField
      FieldName = 'CD_NO'
      Origin = 'KOM_SINGLE."INF707_1.DB".CD_NO'
      Size = 35
    end
    object Query3RCV_REF: TStringField
      FieldName = 'RCV_REF'
      Origin = 'KOM_SINGLE."INF707_1.DB".RCV_REF'
      Size = 35
    end
    object Query3IBANK_REF: TStringField
      FieldName = 'IBANK_REF'
      Origin = 'KOM_SINGLE."INF707_1.DB".IBANK_REF'
      Size = 35
    end
    object Query3ISS_BANK1: TStringField
      FieldName = 'ISS_BANK1'
      Origin = 'KOM_SINGLE."INF707_1.DB".ISS_BANK1'
      Size = 35
    end
    object Query3ISS_BANK2: TStringField
      FieldName = 'ISS_BANK2'
      Origin = 'KOM_SINGLE."INF707_1.DB".ISS_BANK2'
      Size = 35
    end
    object Query3ISS_BANK3: TStringField
      FieldName = 'ISS_BANK3'
      Origin = 'KOM_SINGLE."INF707_1.DB".ISS_BANK3'
      Size = 35
    end
    object Query3ISS_BANK4: TStringField
      FieldName = 'ISS_BANK4'
      Origin = 'KOM_SINGLE."INF707_1.DB".ISS_BANK4'
      Size = 35
    end
    object Query3ISS_BANK5: TStringField
      FieldName = 'ISS_BANK5'
      Origin = 'KOM_SINGLE."INF707_1.DB".ISS_BANK5'
      Size = 35
    end
    object Query3ISS_ACCNT: TStringField
      FieldName = 'ISS_ACCNT'
      Origin = 'KOM_SINGLE."INF707_1.DB".ISS_ACCNT'
      Size = 35
    end
    object Query3ISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Origin = 'KOM_SINGLE."INF707_1.DB".ISS_DATE'
      Size = 8
    end
    object Query3AMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_DATE'
      Size = 8
    end
    object Query3EX_DATE: TStringField
      FieldName = 'EX_DATE'
      Origin = 'KOM_SINGLE."INF707_1.DB".EX_DATE'
      Size = 8
    end
    object Query3EX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Origin = 'KOM_SINGLE."INF707_1.DB".EX_PLACE'
      Size = 35
    end
    object Query3CHK1: TStringField
      FieldName = 'CHK1'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHK1'
      Size = 1
    end
    object Query3CHK2: TStringField
      FieldName = 'CHK2'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHK2'
      Size = 1
    end
    object Query3CHK3: TStringField
      FieldName = 'CHK3'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHK3'
      Size = 10
    end
    object Query3IMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Origin = 'KOM_SINGLE."INF707_1.DB".IMP_CD1'
      Size = 3
    end
    object Query3IMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Origin = 'KOM_SINGLE."INF707_1.DB".IMP_CD2'
      Size = 3
    end
    object Query3IMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Origin = 'KOM_SINGLE."INF707_1.DB".IMP_CD3'
      Size = 3
    end
    object Query3IMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Origin = 'KOM_SINGLE."INF707_1.DB".IMP_CD4'
      Size = 3
    end
    object Query3IMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Origin = 'KOM_SINGLE."INF707_1.DB".IMP_CD5'
      Size = 3
    end
    object Query3F_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Origin = 'KOM_SINGLE."INF707_1.DB".F_INTERFACE'
      Size = 1
    end
    object Query3prno: TIntegerField
      FieldName = 'prno'
      Origin = 'KOM_SINGLE."INF707_1.DB".Prno'
    end
    object Query3PURP_MSG: TStringField
      FieldName = 'PURP_MSG'
      Origin = 'KOM_SINGLE."INF707_1.DB".PURP_MSG'
      Size = 3
    end
    object Query3CAN_REQ: TStringField
      FieldName = 'CAN_REQ'
      Origin = 'KOM_SINGLE."INF707_1.DB".CAN_REQ'
      Size = 3
    end
    object Query3CON_INST: TStringField
      FieldName = 'CON_INST'
      Origin = 'KOM_SINGLE."INF707_1.DB".CON_INST'
      Size = 3
    end
    object Query3DOC_TYPE: TStringField
      FieldName = 'DOC_TYPE'
      Origin = 'KOM_SINGLE."INF707_1.DB".DOC_TYPE'
      Size = 3
    end
    object Query3CHARGE: TStringField
      FieldName = 'CHARGE'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHARGE'
      Size = 3
    end
    object Query3CHARGE_NUM1: TStringField
      FieldName = 'CHARGE_NUM1'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHARGE_NUM1'
      Size = 35
    end
    object Query3CHARGE_NUM2: TStringField
      FieldName = 'CHARGE_NUM2'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHARGE_NUM2'
      Size = 35
    end
    object Query3CHARGE_NUM3: TStringField
      FieldName = 'CHARGE_NUM3'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHARGE_NUM3'
      Size = 35
    end
    object Query3CHARGE_NUM4: TStringField
      FieldName = 'CHARGE_NUM4'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHARGE_NUM4'
      Size = 35
    end
    object Query3CHARGE_NUM5: TStringField
      FieldName = 'CHARGE_NUM5'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHARGE_NUM5'
      Size = 35
    end
    object Query3CHARGE_NUM6: TStringField
      FieldName = 'CHARGE_NUM6'
      Origin = 'KOM_SINGLE."INF707_1.DB".CHARGE_NUM6'
      Size = 35
    end
    object Query3AMD_CHARGE: TStringField
      FieldName = 'AMD_CHARGE'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_CHARGE'
      Size = 3
    end
    object Query3AMD_CHARGE_NUM1: TStringField
      FieldName = 'AMD_CHARGE_NUM1'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_CHARGE_NUM1'
      Size = 35
    end
    object Query3AMD_CHARGE_NUM2: TStringField
      FieldName = 'AMD_CHARGE_NUM2'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_CHARGE_NUM2'
      Size = 35
    end
    object Query3AMD_CHARGE_NUM3: TStringField
      FieldName = 'AMD_CHARGE_NUM3'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_CHARGE_NUM3'
      Size = 35
    end
    object Query3AMD_CHARGE_NUM4: TStringField
      FieldName = 'AMD_CHARGE_NUM4'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_CHARGE_NUM4'
      Size = 35
    end
    object Query3AMD_CHARGE_NUM5: TStringField
      FieldName = 'AMD_CHARGE_NUM5'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_CHARGE_NUM5'
      Size = 35
    end
    object Query3AMD_CHARGE_NUM6: TStringField
      FieldName = 'AMD_CHARGE_NUM6'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_CHARGE_NUM6'
      Size = 35
    end
    object Query3TSHIP: TStringField
      FieldName = 'TSHIP'
      Origin = 'KOM_SINGLE."INF707_1.DB".TSHIP'
      Size = 3
    end
    object Query3PSHIP: TStringField
      FieldName = 'PSHIP'
      Origin = 'KOM_SINGLE."INF707_1.DB".PSHIP'
      Size = 3
    end
    object Query3DRAFT1: TStringField
      FieldName = 'DRAFT1'
      Origin = 'KOM_SINGLE."INF707_1.DB".DRAFT1'
      Size = 35
    end
    object Query3DRAFT2: TStringField
      FieldName = 'DRAFT2'
      Origin = 'KOM_SINGLE."INF707_1.DB".DRAFT2'
      Size = 35
    end
    object Query3DRAFT3: TStringField
      FieldName = 'DRAFT3'
      Origin = 'KOM_SINGLE."INF707_1.DB".DRAFT3'
      Size = 35
    end
    object Query3MIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Origin = 'KOM_SINGLE."INF707_1.DB".MIX_PAY1'
      Size = 35
    end
    object Query3MIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Origin = 'KOM_SINGLE."INF707_1.DB".MIX_PAY2'
      Size = 35
    end
    object Query3MIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Origin = 'KOM_SINGLE."INF707_1.DB".MIX_PAY3'
      Size = 35
    end
    object Query3MIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Origin = 'KOM_SINGLE."INF707_1.DB".MIX_PAY4'
      Size = 35
    end
    object Query3DEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Origin = 'KOM_SINGLE."INF707_1.DB".DEF_PAY1'
      Size = 35
    end
    object Query3DEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Origin = 'KOM_SINGLE."INF707_1.DB".DEF_PAY2'
      Size = 35
    end
    object Query3DEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Origin = 'KOM_SINGLE."INF707_1.DB".DEF_PAY3'
      Size = 35
    end
    object Query3DEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Origin = 'KOM_SINGLE."INF707_1.DB".DEF_PAY4'
      Size = 35
    end
    object Query3APPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Origin = 'KOM_SINGLE."INF707_1.DB".APPLICABLE_RULES_1'
      Size = 30
    end
    object Query3APPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Origin = 'KOM_SINGLE."INF707_1.DB".APPLICABLE_RULES_2'
      Size = 35
    end
    object Query3AVAIL: TStringField
      FieldName = 'AVAIL'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVAIL'
      Size = 11
    end
    object Query3AVAIL1: TStringField
      FieldName = 'AVAIL1'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVAIL1'
      Size = 35
    end
    object Query3AVAIL2: TStringField
      FieldName = 'AVAIL2'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVAIL2'
      Size = 35
    end
    object Query3AVAIL3: TStringField
      FieldName = 'AVAIL3'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVAIL3'
      Size = 35
    end
    object Query3AVAIL4: TStringField
      FieldName = 'AVAIL4'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVAIL4'
      Size = 35
    end
    object Query3AV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Origin = 'KOM_SINGLE."INF707_1.DB".AV_ACCNT'
      Size = 17
    end
    object Query3DRAWEE: TStringField
      FieldName = 'DRAWEE'
      Origin = 'KOM_SINGLE."INF707_1.DB".DRAWEE'
      Size = 11
    end
    object Query3DRAWEE1: TStringField
      FieldName = 'DRAWEE1'
      Origin = 'KOM_SINGLE."INF707_1.DB".DRAWEE1'
      Size = 35
    end
    object Query3DRAWEE2: TStringField
      FieldName = 'DRAWEE2'
      Origin = 'KOM_SINGLE."INF707_1.DB".DRAWEE2'
      Size = 35
    end
    object Query3DRAWEE3: TStringField
      FieldName = 'DRAWEE3'
      Origin = 'KOM_SINGLE."INF707_1.DB".DRAWEE3'
      Size = 35
    end
    object Query3DRAWEE4: TStringField
      FieldName = 'DRAWEE4'
      Origin = 'KOM_SINGLE."INF707_1.DB".DRAWEE4'
      Size = 35
    end
    object Query3CO_BANK: TStringField
      FieldName = 'CO_BANK'
      Origin = 'KOM_SINGLE."INF707_1.DB".CO_BANK'
      Size = 11
    end
    object Query3CO_BANK1: TStringField
      FieldName = 'CO_BANK1'
      Origin = 'KOM_SINGLE."INF707_1.DB".CO_BANK1'
      Size = 35
    end
    object Query3CO_BANK2: TStringField
      FieldName = 'CO_BANK2'
      Origin = 'KOM_SINGLE."INF707_1.DB".CO_BANK2'
      Size = 35
    end
    object Query3CO_BANK3: TStringField
      FieldName = 'CO_BANK3'
      Origin = 'KOM_SINGLE."INF707_1.DB".CO_BANK3'
      Size = 35
    end
    object Query3CO_BANK4: TStringField
      FieldName = 'CO_BANK4'
      Origin = 'KOM_SINGLE."INF707_1.DB".CO_BANK4'
      Size = 35
    end
    object Query3REI_BANK: TStringField
      FieldName = 'REI_BANK'
      Origin = 'KOM_SINGLE."INF707_1.DB".REI_BANK'
      Size = 11
    end
    object Query3REI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Origin = 'KOM_SINGLE."INF707_1.DB".REI_BANK1'
      Size = 35
    end
    object Query3REI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Origin = 'KOM_SINGLE."INF707_1.DB".REI_BANK2'
      Size = 35
    end
    object Query3REI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Origin = 'KOM_SINGLE."INF707_1.DB".REI_BANK3'
      Size = 35
    end
    object Query3REI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Origin = 'KOM_SINGLE."INF707_1.DB".REI_BANK4'
      Size = 35
    end
    object Query3AVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVT_BANK'
      Size = 11
    end
    object Query3AVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVT_BANK1'
      Size = 35
    end
    object Query3AVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVT_BANK2'
      Size = 35
    end
    object Query3AVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVT_BANK3'
      Size = 35
    end
    object Query3AVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Origin = 'KOM_SINGLE."INF707_1.DB".AVT_BANK4'
      Size = 35
    end
    object Query3AMD_APPLIC1: TStringField
      FieldName = 'AMD_APPLIC1'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_APPLIC1'
      Size = 35
    end
    object Query3AMD_APPLIC2: TStringField
      FieldName = 'AMD_APPLIC2'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_APPLIC2'
      Size = 35
    end
    object Query3AMD_APPLIC3: TStringField
      FieldName = 'AMD_APPLIC3'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_APPLIC3'
      Size = 35
    end
    object Query3AMD_APPLIC4: TStringField
      FieldName = 'AMD_APPLIC4'
      Origin = 'KOM_SINGLE."INF707_1.DB".AMD_APPLIC4'
      Size = 35
    end
    object Query3PERIOD: TFloatField
      FieldName = 'PERIOD'
      Origin = 'KOM_SINGLE."INF707_1.DB".PERIOD'
    end
    object Query3PERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Origin = 'KOM_SINGLE."INF707_1.DB".PERIOD_TXT'
      Size = 35
    end
    object Query3MAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Origin = 'KOM_SINGLE."INF707_2.DB".MAINT_NO'
      Size = 35
    end
    object Query3MSEQ_1: TIntegerField
      FieldName = 'MSEQ_1'
      Origin = 'KOM_SINGLE."INF707_2.DB".MSEQ'
    end
    object Query3AMD_NO_1: TIntegerField
      FieldName = 'AMD_NO_1'
      Origin = 'KOM_SINGLE."INF707_2.DB".AMD_NO'
    end
    object Query3APPLIC1: TStringField
      FieldName = 'APPLIC1'
      Origin = 'KOM_SINGLE."INF707_2.DB".APPLIC1'
      Size = 35
    end
    object Query3APPLIC2: TStringField
      FieldName = 'APPLIC2'
      Origin = 'KOM_SINGLE."INF707_2.DB".APPLIC2'
      Size = 35
    end
    object Query3APPLIC3: TStringField
      FieldName = 'APPLIC3'
      Origin = 'KOM_SINGLE."INF707_2.DB".APPLIC3'
      Size = 35
    end
    object Query3APPLIC4: TStringField
      FieldName = 'APPLIC4'
      Origin = 'KOM_SINGLE."INF707_2.DB".APPLIC4'
      Size = 35
    end
    object Query3APPLIC5: TStringField
      FieldName = 'APPLIC5'
      Origin = 'KOM_SINGLE."INF707_2.DB".APPLIC5'
      Size = 35
    end
    object Query3BENEFC1: TStringField
      FieldName = 'BENEFC1'
      Origin = 'KOM_SINGLE."INF707_2.DB".BENEFC1'
      Size = 35
    end
    object Query3BENEFC2: TStringField
      FieldName = 'BENEFC2'
      Origin = 'KOM_SINGLE."INF707_2.DB".BENEFC2'
      Size = 35
    end
    object Query3BENEFC3: TStringField
      FieldName = 'BENEFC3'
      Origin = 'KOM_SINGLE."INF707_2.DB".BENEFC3'
      Size = 35
    end
    object Query3BENEFC4: TStringField
      FieldName = 'BENEFC4'
      Origin = 'KOM_SINGLE."INF707_2.DB".BENEFC4'
      Size = 35
    end
    object Query3BENEFC5: TStringField
      FieldName = 'BENEFC5'
      Origin = 'KOM_SINGLE."INF707_2.DB".BENEFC5'
      Size = 35
    end
    object Query3INCD_CUR: TStringField
      FieldName = 'INCD_CUR'
      Origin = 'KOM_SINGLE."INF707_2.DB".INCD_CUR'
      Size = 3
    end
    object Query3INCD_AMT: TFloatField
      FieldName = 'INCD_AMT'
      Origin = 'KOM_SINGLE."INF707_2.DB".INCD_AMT'
    end
    object Query3DECD_CUR: TStringField
      FieldName = 'DECD_CUR'
      Origin = 'KOM_SINGLE."INF707_2.DB".DECD_CUR'
      Size = 3
    end
    object Query3DECD_AMT: TFloatField
      FieldName = 'DECD_AMT'
      Origin = 'KOM_SINGLE."INF707_2.DB".DECD_AMT'
    end
    object Query3NWCD_CUR: TStringField
      FieldName = 'NWCD_CUR'
      Origin = 'KOM_SINGLE."INF707_2.DB".NWCD_CUR'
      Size = 3
    end
    object Query3NWCD_AMT: TFloatField
      FieldName = 'NWCD_AMT'
      Origin = 'KOM_SINGLE."INF707_2.DB".NWCD_AMT'
    end
    object Query3CD_PERP: TFloatField
      FieldName = 'CD_PERP'
      Origin = 'KOM_SINGLE."INF707_2.DB".CD_PERP'
    end
    object Query3CD_PERM: TFloatField
      FieldName = 'CD_PERM'
      Origin = 'KOM_SINGLE."INF707_2.DB".CD_PERM'
    end
    object Query3CD_MAX: TStringField
      FieldName = 'CD_MAX'
      Origin = 'KOM_SINGLE."INF707_2.DB".CD_MAX'
      Size = 3
    end
    object Query3AA_CV1: TStringField
      FieldName = 'AA_CV1'
      Origin = 'KOM_SINGLE."INF707_2.DB".AA_CV1'
      Size = 35
    end
    object Query3AA_CV2: TStringField
      FieldName = 'AA_CV2'
      Origin = 'KOM_SINGLE."INF707_2.DB".AA_CV2'
      Size = 35
    end
    object Query3AA_CV3: TStringField
      FieldName = 'AA_CV3'
      Origin = 'KOM_SINGLE."INF707_2.DB".AA_CV3'
      Size = 35
    end
    object Query3AA_CV4: TStringField
      FieldName = 'AA_CV4'
      Origin = 'KOM_SINGLE."INF707_2.DB".AA_CV4'
      Size = 35
    end
    object Query3LOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Origin = 'KOM_SINGLE."INF707_2.DB".LOAD_ON'
      Size = 65
    end
    object Query3FOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Origin = 'KOM_SINGLE."INF707_2.DB".FOR_TRAN'
      Size = 65
    end
    object Query3LST_DATE: TStringField
      FieldName = 'LST_DATE'
      Origin = 'KOM_SINGLE."INF707_2.DB".LST_DATE'
      Size = 8
    end
    object Query3SHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
      Origin = 'KOM_SINGLE."INF707_2.DB".SHIP_PD'
    end
    object Query3SHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Origin = 'KOM_SINGLE."INF707_2.DB".SHIP_PD1'
      Size = 65
    end
    object Query3SHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Origin = 'KOM_SINGLE."INF707_2.DB".SHIP_PD2'
      Size = 65
    end
    object Query3SHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Origin = 'KOM_SINGLE."INF707_2.DB".SHIP_PD3'
      Size = 65
    end
    object Query3SHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Origin = 'KOM_SINGLE."INF707_2.DB".SHIP_PD4'
      Size = 65
    end
    object Query3SHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Origin = 'KOM_SINGLE."INF707_2.DB".SHIP_PD5'
      Size = 65
    end
    object Query3SHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Origin = 'KOM_SINGLE."INF707_2.DB".SHIP_PD6'
      Size = 65
    end
    object Query3NARRAT: TBooleanField
      FieldName = 'NARRAT'
      Origin = 'KOM_SINGLE."INF707_2.DB".NARRAT'
    end
    object Query3NARRAT_1: TMemoField
      FieldName = 'NARRAT_1'
      Origin = 'KOM_SINGLE."INF707_2.DB".NARRAT_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query3SR_INFO1: TStringField
      FieldName = 'SR_INFO1'
      Origin = 'KOM_SINGLE."INF707_2.DB".SR_INFO1'
      Size = 35
    end
    object Query3SR_INFO2: TStringField
      FieldName = 'SR_INFO2'
      Origin = 'KOM_SINGLE."INF707_2.DB".SR_INFO2'
      Size = 35
    end
    object Query3SR_INFO3: TStringField
      FieldName = 'SR_INFO3'
      Origin = 'KOM_SINGLE."INF707_2.DB".SR_INFO3'
      Size = 35
    end
    object Query3SR_INFO4: TStringField
      FieldName = 'SR_INFO4'
      Origin = 'KOM_SINGLE."INF707_2.DB".SR_INFO4'
      Size = 35
    end
    object Query3SR_INFO5: TStringField
      FieldName = 'SR_INFO5'
      Origin = 'KOM_SINGLE."INF707_2.DB".SR_INFO5'
      Size = 35
    end
    object Query3SR_INFO6: TStringField
      FieldName = 'SR_INFO6'
      Origin = 'KOM_SINGLE."INF707_2.DB".SR_INFO6'
      Size = 35
    end
    object Query3EX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Origin = 'KOM_SINGLE."INF707_2.DB".EX_NAME1'
      Size = 35
    end
    object Query3EX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Origin = 'KOM_SINGLE."INF707_2.DB".EX_NAME2'
      Size = 35
    end
    object Query3EX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Origin = 'KOM_SINGLE."INF707_2.DB".EX_NAME3'
      Size = 35
    end
    object Query3EX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Origin = 'KOM_SINGLE."INF707_2.DB".EX_ADDR1'
      Size = 35
    end
    object Query3EX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Origin = 'KOM_SINGLE."INF707_2.DB".EX_ADDR2'
      Size = 35
    end
    object Query3OP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Origin = 'KOM_SINGLE."INF707_2.DB".OP_BANK1'
      Size = 35
    end
    object Query3OP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Origin = 'KOM_SINGLE."INF707_2.DB".OP_BANK2'
      Size = 35
    end
    object Query3OP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Origin = 'KOM_SINGLE."INF707_2.DB".OP_BANK3'
      Size = 35
    end
    object Query3OP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Origin = 'KOM_SINGLE."INF707_2.DB".OP_ADDR1'
      Size = 35
    end
    object Query3OP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Origin = 'KOM_SINGLE."INF707_2.DB".OP_ADDR2'
      Size = 35
    end
    object Query3BFCD_AMT: TFloatField
      FieldName = 'BFCD_AMT'
      Origin = 'KOM_SINGLE."INF707_2.DB".BFCD_AMT'
    end
    object Query3BFCD_CUR: TStringField
      FieldName = 'BFCD_CUR'
      Origin = 'KOM_SINGLE."INF707_2.DB".BFCD_CUR'
      Size = 3
    end
    object Query3SUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Origin = 'KOM_SINGLE."INF707_2.DB".SUNJUCK_PORT'
      Size = 65
    end
    object Query3DOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Origin = 'KOM_SINGLE."INF707_2.DB".DOCHACK_PORT'
      Size = 65
    end
    object Query3GOODS_DESC: TStringField
      FieldName = 'GOODS_DESC'
      Origin = 'KOM_SINGLE."INF707_2.DB".GOODS_DESC'
      Size = 1
    end
    object Query3GOODS_DESC_1: TMemoField
      FieldName = 'GOODS_DESC_1'
      Origin = 'KOM_SINGLE."INF707_2.DB".GOODS_DESC_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query3DOC_DESC: TStringField
      FieldName = 'DOC_DESC'
      Origin = 'KOM_SINGLE."INF707_2.DB".DOC_DESC'
      Size = 1
    end
    object Query3DOC_DESC_1: TMemoField
      FieldName = 'DOC_DESC_1'
      Origin = 'KOM_SINGLE."INF707_2.DB".DOC_DESC_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query3ADD_DESC: TStringField
      FieldName = 'ADD_DESC'
      Origin = 'KOM_SINGLE."INF707_2.DB".ADD_DESC'
      Size = 1
    end
    object Query3ADD_DESC_1: TMemoField
      FieldName = 'ADD_DESC_1'
      Origin = 'KOM_SINGLE."INF707_2.DB".ADD_DESC_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query3SPECIAL_DESC: TStringField
      FieldName = 'SPECIAL_DESC'
      Origin = 'KOM_SINGLE."INF707_2.DB".SPECIAL_DESC'
      Size = 1
    end
    object Query3SPECIAL_DESC_1: TMemoField
      FieldName = 'SPECIAL_DESC_1'
      Origin = 'KOM_SINGLE."INF707_2.DB".SPECIAL_DESC_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query3INST_DESC: TStringField
      FieldName = 'INST_DESC'
      Origin = 'KOM_SINGLE."INF707_2.DB".INST_DESC'
      Size = 1
    end
    object Query3INST_DESC_1: TMemoField
      FieldName = 'INST_DESC_1'
      Origin = 'KOM_SINGLE."INF707_2.DB".INST_DESC_1'
      BlobType = ftMemo
      Size = 1
    end
  end
end
