unit Dlg_ImportINF700;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OKBottomDlg, DB, ADODB, StdCtrls, sComboBox, sRadioButton,
  sButton, sCheckBox, sEdit, sLabel, ExtCtrls, DBTables, ComCtrls,
  acProgressBar;

type
  TDlg_ImportINF700_frm = class(TOKBottomDlg_frm)
    Query2: TQuery;
    Query2MAINT_NO: TStringField;
    Query2MESSAGE1: TStringField;
    Query2MESSAGE2: TStringField;
    Query2USER_ID: TStringField;
    Query2DATEE: TStringField;
    Query2APP_DATE: TStringField;
    Query2IN_MATHOD: TStringField;
    Query2AP_BANK: TStringField;
    Query2AP_BANK1: TStringField;
    Query2AP_BANK2: TStringField;
    Query2AP_BANK3: TStringField;
    Query2AP_BANK4: TStringField;
    Query2AP_BANK5: TStringField;
    Query2AD_BANK: TStringField;
    Query2AD_BANK1: TStringField;
    Query2AD_BANK2: TStringField;
    Query2AD_BANK3: TStringField;
    Query2AD_BANK4: TStringField;
    Query2AD_PAY: TStringField;
    Query2IL_NO1: TStringField;
    Query2IL_NO2: TStringField;
    Query2IL_NO3: TStringField;
    Query2IL_NO4: TStringField;
    Query2IL_NO5: TStringField;
    Query2IL_AMT1: TFloatField;
    Query2IL_AMT2: TFloatField;
    Query2IL_AMT3: TFloatField;
    Query2IL_AMT4: TFloatField;
    Query2IL_AMT5: TFloatField;
    Query2IL_CUR1: TStringField;
    Query2IL_CUR2: TStringField;
    Query2IL_CUR3: TStringField;
    Query2IL_CUR4: TStringField;
    Query2IL_CUR5: TStringField;
    Query2AD_INFO1: TStringField;
    Query2AD_INFO2: TStringField;
    Query2AD_INFO3: TStringField;
    Query2AD_INFO4: TStringField;
    Query2AD_INFO5: TStringField;
    Query2DOC_CD: TStringField;
    Query2CD_NO: TStringField;
    Query2REF_PRE: TStringField;
    Query2ISS_DATE: TStringField;
    Query2EX_DATE: TStringField;
    Query2EX_PLACE: TStringField;
    Query2CHK1: TStringField;
    Query2CHK2: TStringField;
    Query2CHK3: TStringField;
    Query2prno: TIntegerField;
    Query2F_INTERFACE: TStringField;
    Query2IMP_CD1: TStringField;
    Query2IMP_CD2: TStringField;
    Query2IMP_CD3: TStringField;
    Query2IMP_CD4: TStringField;
    Query2IMP_CD5: TStringField;
    Query2IMP_CD5_1: TStringField;
    Query2MAINT_NO_1: TStringField;
    Query2APP_BANK: TStringField;
    Query2APP_BANK1: TStringField;
    Query2APP_BANK2: TStringField;
    Query2APP_BANK3: TStringField;
    Query2APP_BANK4: TStringField;
    Query2APP_BANK5: TStringField;
    Query2APP_ACCNT: TStringField;
    Query2APPLIC1: TStringField;
    Query2APPLIC2: TStringField;
    Query2APPLIC3: TStringField;
    Query2APPLIC4: TStringField;
    Query2APPLIC5: TStringField;
    Query2BENEFC1: TStringField;
    Query2BENEFC2: TStringField;
    Query2BENEFC3: TStringField;
    Query2BENEFC4: TStringField;
    Query2BENEFC5: TStringField;
    Query2CD_AMT: TFloatField;
    Query2CD_CUR: TStringField;
    Query2CD_PERP: TFloatField;
    Query2CD_PERM: TFloatField;
    Query2CD_MAX: TStringField;
    Query2AA_CV1: TStringField;
    Query2AA_CV2: TStringField;
    Query2AA_CV3: TStringField;
    Query2AA_CV4: TStringField;
    Query2AVAIL: TStringField;
    Query2AVAIL1: TStringField;
    Query2AVAIL2: TStringField;
    Query2AVAIL3: TStringField;
    Query2AVAIL4: TStringField;
    Query2AV_ACCNT: TStringField;
    Query2AV_PAY: TStringField;
    Query2DRAFT1: TStringField;
    Query2DRAFT2: TStringField;
    Query2DRAFT3: TStringField;
    Query2DRAWEE: TStringField;
    Query2DRAWEE1: TStringField;
    Query2DRAWEE2: TStringField;
    Query2DRAWEE3: TStringField;
    Query2DRAWEE4: TStringField;
    Query2DR_ACCNT: TStringField;
    Query2MAINT_NO_2: TStringField;
    Query2PSHIP: TStringField;
    Query2TSHIP: TStringField;
    Query2LOAD_ON: TStringField;
    Query2FOR_TRAN: TStringField;
    Query2LST_DATE: TStringField;
    Query2SHIP_PD: TBooleanField;
    Query2SHIP_PD1: TStringField;
    Query2SHIP_PD2: TStringField;
    Query2SHIP_PD3: TStringField;
    Query2SHIP_PD4: TStringField;
    Query2SHIP_PD5: TStringField;
    Query2SHIP_PD6: TStringField;
    Query2DESGOOD: TBooleanField;
    Query2DESGOOD_1: TMemoField;
    Query2TERM_PR: TStringField;
    Query2TERM_PR_M: TStringField;
    Query2PL_TERM: TStringField;
    Query2ORIGIN: TStringField;
    Query2ORIGIN_M: TStringField;
    Query2DOC_380: TBooleanField;
    Query2DOC_380_1: TFloatField;
    Query2DOC_705: TBooleanField;
    Query2DOC_705_1: TStringField;
    Query2DOC_705_2: TStringField;
    Query2DOC_705_3: TStringField;
    Query2DOC_705_4: TStringField;
    Query2DOC_740: TBooleanField;
    Query2DOC_740_1: TStringField;
    Query2DOC_740_2: TStringField;
    Query2DOC_740_3: TStringField;
    Query2DOC_740_4: TStringField;
    Query2DOC_530: TBooleanField;
    Query2DOC_530_1: TStringField;
    Query2DOC_530_2: TStringField;
    Query2DOC_271: TBooleanField;
    Query2DOC_271_1: TFloatField;
    Query2DOC_861: TBooleanField;
    Query2DOC_2AA: TBooleanField;
    Query2DOC_2AA_1: TMemoField;
    Query2ACD_2AA: TBooleanField;
    Query2ACD_2AA_1: TStringField;
    Query2ACD_2AB: TBooleanField;
    Query2ACD_2AC: TBooleanField;
    Query2ACD_2AD: TBooleanField;
    Query2ACD_2AE: TBooleanField;
    Query2ACD_2AE_1: TMemoField;
    Query2CHARGE: TStringField;
    Query2PERIOD: TFloatField;
    Query2CONFIRMM: TStringField;
    Query2DEF_PAY1: TStringField;
    Query2DEF_PAY2: TStringField;
    Query2DEF_PAY3: TStringField;
    Query2DEF_PAY4: TStringField;
    Query2DOC_705_GUBUN: TStringField;
    Query2CHARGE_NUM1: TStringField;
    Query2CHARGE_NUM2: TStringField;
    Query2CHARGE_NUM3: TStringField;
    Query2CHARGE_NUM4: TStringField;
    Query2CHARGE_NUM5: TStringField;
    Query2CHARGE_NUM6: TStringField;
    Query2PERIOD_TXT: TStringField;
    Query2MAINT_NO_3: TStringField;
    Query2REI_BANK: TStringField;
    Query2REI_BANK1: TStringField;
    Query2REI_BANK2: TStringField;
    Query2REI_BANK3: TStringField;
    Query2REI_BANK4: TStringField;
    Query2REI_BANK5: TStringField;
    Query2REI_ACNNT: TStringField;
    Query2INSTRCT: TBooleanField;
    Query2INSTRCT_1: TMemoField;
    Query2AVT_BANK: TStringField;
    Query2AVT_BANK1: TStringField;
    Query2AVT_BANK2: TStringField;
    Query2AVT_BANK3: TStringField;
    Query2AVT_BANK4: TStringField;
    Query2AVT_BANK5: TStringField;
    Query2AVT_ACCNT: TStringField;
    Query2SND_INFO1: TStringField;
    Query2SND_INFO2: TStringField;
    Query2SND_INFO3: TStringField;
    Query2SND_INFO4: TStringField;
    Query2SND_INFO5: TStringField;
    Query2SND_INFO6: TStringField;
    Query2EX_NAME1: TStringField;
    Query2EX_NAME2: TStringField;
    Query2EX_NAME3: TStringField;
    Query2EX_ADDR1: TStringField;
    Query2EX_ADDR2: TStringField;
    Query2EX_ADDR3: TStringField;
    Query2OP_BANK1: TStringField;
    Query2OP_BANK2: TStringField;
    Query2OP_BANK3: TStringField;
    Query2OP_ADDR1: TStringField;
    Query2OP_ADDR2: TStringField;
    Query2OP_ADDR3: TStringField;
    Query2MIX_PAY1: TStringField;
    Query2MIX_PAY2: TStringField;
    Query2MIX_PAY3: TStringField;
    Query2MIX_PAY4: TStringField;
    Query2APPLICABLE_RULES_1: TStringField;
    Query2APPLICABLE_RULES_2: TStringField;
    Query2DOC_760: TBooleanField;
    Query2DOC_760_1: TStringField;
    Query2DOC_760_2: TStringField;
    Query2DOC_760_3: TStringField;
    Query2DOC_760_4: TStringField;
    Query2SUNJUCK_PORT: TStringField;
    Query2DOCHACK_PORT: TStringField;
    Query2CO_BANK: TStringField;
    Query2CO_BANK1: TStringField;
    Query2CO_BANK2: TStringField;
    Query2CO_BANK3: TStringField;
    Query2CO_BANK4: TStringField;
    Query2SPECIAL_DESC: TStringField;
    Query2SPECIAL_DESC_1: TMemoField;
    sProgressBar1: TsProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    function ConvertData: Boolean; override;
    { Public declarations }
  end;

var
  Dlg_ImportINF700_frm: TDlg_ImportINF700_frm;

implementation

uses
  SQLCreator, VarDefine;

{$R *.dfm}

function TDlg_ImportINF700_frm.ConvertData: Boolean;
var
  SC : array [0..3] of TSQLCreate;
  nCount, nMax : Integer;
  TEMP_STR : string;
begin
  try
    Query2.Close;
    Query2.SQL.Text := FSQL;
    Query2.Open;

    if Query2.RecordCount = 0 Then
      raise Exception.Create('데이터가 없습니다');

    SC[0] := TSQLCreate.Create;
    SC[1] := TSQLCreate.Create;
    SC[2] := TSQLCreate.Create;
    SC[3] := TSQLCreate.Create;

    //------------------------------------------------------------------------------
    // 초기화 섹션
    //------------------------------------------------------------------------------
    with SC[0] do begin DMLType := dmlDelete; SQLHeader('INF700_1'); end;
    with SC[1] do begin DMLType := dmlDelete; SQLHeader('INF700_2'); end;
    with SC[2] do begin DMLType := dmlDelete; SQLHeader('INF700_3'); end;
    with SC[3] do begin DMLType := dmlDelete; SQLHeader('INF700_4'); end;
    RunSQL(SC[0].CreateSQL);
    RunSQL(SC[1].CreateSQL);
    RunSQL(SC[2].CreateSQL);
    RunSQL(SC[3].CreateSQL);

    nCount := 0;
    sProgressBar1.Max := Query2.RecordCount;
    sProgressBar1.Position := 0;

    while not Query2.Eof do
    begin
      Application.ProcessMessages;
      with SC[0] do
      begin
        DMLType := dmlInsert;
        SQLHeader('INF700_1');
        ADDValue('MAINT_NO', Query2.Fields[0].AsString);
        ADDValue('MSEQ', 1);
        ADDValue('MESSAGE1', Query2MESSAGE1.AsString);
        ADDValue('MESSAGE2', Query2MESSAGE2.AsString);
        ADDValue('USER_ID',  LoginData.sID);
        ADDValue('DATEE',    Query2DATEE.AsString);
        ADDValue('APP_DATE', Query2APP_DATE.AsString);
        ADDValue('IN_MATHOD',  Query2IN_MATHOD.AsString);
        ADDValue('AP_BANK',    Query2AP_BANK.AsString);
        ADDValue('AP_BANK1',   Query2AP_BANK1.AsString);
        ADDValue('AP_BANK2',   Query2AP_BANK2.AsString);
        ADDValue('AP_BANK3',   Query2AP_BANK3.AsString);
        ADDValue('AP_BANK4',   Query2AP_BANK4.AsString);
        ADDValue('AP_BANK5',   Query2AP_BANK5.AsString);
        ADDValue('AD_BANK',    Query2AD_BANK.AsString);
        ADDValue('AD_BANK1',   Query2AD_BANK1.AsString);
        ADDValue('AD_BANK2',   Query2AD_BANK2.AsString);
        ADDValue('AD_BANK3',   Query2AD_BANK3.AsString);
        ADDValue('AD_BANK4',   Query2AD_BANK4.AsString);
        ADDValue('AD_PAY',     Query2AD_PAY.AsString);
        ADDValue('IL_NO1',     Query2IL_NO1.AsString);
        ADDValue('IL_NO2',     Query2IL_NO2.AsString);
        ADDValue('IL_NO3',     Query2IL_NO3.AsString);
        ADDValue('IL_NO4',     Query2IL_NO4.AsString);
        ADDValue('IL_NO5',     Query2IL_NO5.AsString);
        ADDValue('IL_AMT1',    Query2IL_AMT1.asFloat);
        ADDValue('IL_AMT2',    Query2IL_AMT2.asFloat);
        ADDValue('IL_AMT3',    Query2IL_AMT3.asFloat);
        ADDValue('IL_AMT4',    Query2IL_AMT4.asFloat);
        ADDValue('IL_AMT5',    Query2IL_AMT5.asFloat);
        ADDValue('IL_CUR1',    Query2IL_CUR1.AsString);
        ADDValue('IL_CUR2',    Query2IL_CUR2.AsString);
        ADDValue('IL_CUR3',    Query2IL_CUR3.AsString);
        ADDValue('IL_CUR4',    Query2IL_CUR4.AsString);
        ADDValue('IL_CUR5',    Query2IL_CUR5.AsString);
        ADDValue('AD_INFO1',   Query2AD_INFO1.AsString);
        ADDValue('AD_INFO2',   Query2AD_INFO2.AsString);
        ADDValue('AD_INFO3',   Query2AD_INFO3.AsString);
        ADDValue('AD_INFO4',   Query2AD_INFO4.AsString);
        ADDValue('AD_INFO5',   Query2AD_INFO5.AsString);
        ADDValue('DOC_CD',     Query2DOC_CD.AsString);
        ADDValue('CD_NO',     Query2CD_NO.AsString);
        ADDValue('REF_PRE',   Query2REF_PRE.AsString);
        ADDValue('ISS_DATE',  Query2ISS_DATE.AsString);
        ADDValue('EX_DATE',    Query2EX_DATE.AsString);
        ADDValue('EX_PLACE',   Query2EX_PLACE.AsString);
        ADDValue('CHK1',       Query2CHK1.asBoolean);
        ADDValue('CHK2',       Query2CHK2.AsString);
        ADDValue('CHK3',       Query2CHK3.AsString);
        ADDValue('F_INTERFACE', Query2F_INTERFACE.AsString);
        ADDValue('IMP_CD1', Query2IMP_CD1.AsString);
        ADDValue('IMP_CD2', Query2IMP_CD2.AsString);
        ADDValue('IMP_CD3', Query2IMP_CD3.AsString);
        ADDValue('IMP_CD4', Query2IMP_CD4.AsString);
        ADDValue('IMP_CD5', Query2IMP_CD5.AsString);
      end;

      with SC[1] do
      begin
        DMLType := dmlInsert;
        SQLHeader('INF700_2');
        ADDValue('MAINT_NO', Query2.Fields[0].AsString);
        ADDValue('MSEQ', 1);
        ADDValue('APP_BANK',  Query2APP_BANK.AsString);
        ADDValue('APP_BANK1', Query2APP_BANK1.AsString);
        ADDValue('APP_BANK2', Query2APP_BANK2.AsString);
        ADDValue('APP_BANK3', Query2APP_BANK3.AsString);
        ADDValue('APP_BANK4', Query2APP_BANK4.AsString);
        ADDValue('APP_BANK5', Query2APP_BANK5.AsString);
        ADDValue('APP_ACCNT', Query2APP_ACCNT.AsString);
        ADDValue('APPLIC1',   Query2APPLIC1.AsString);
        ADDValue('APPLIC2',   Query2APPLIC2.AsString);
        ADDValue('APPLIC3',   Query2APPLIC3.AsString);
        ADDValue('APPLIC4',   Query2APPLIC4.AsString);
        ADDValue('APPLIC5',   Query2APPLIC5.AsString);
        ADDValue('BENEFC1',   Query2BENEFC1.AsString);
        ADDValue('BENEFC2',   Query2BENEFC2.AsString);
        ADDValue('BENEFC3',   Query2BENEFC3.AsString);
        ADDValue('BENEFC4',   Query2BENEFC4.AsString);
        ADDValue('BENEFC5',   Query2BENEFC5.AsString);
        ADDValue('CD_AMT',    Query2CD_AMT.asfloat);
        ADDValue('CD_CUR',    Query2CD_CUR.AsString);
        ADDValue('CD_PERP',   Query2CD_PERP.asfloat);
        ADDValue('CD_PERM',   Query2CD_PERM.asfloat);
        ADDValue('CD_MAX',    Query2CD_MAX.AsString);
        ADDValue('AA_CV1',    Query2AA_CV1.AsString);
        ADDValue('AA_CV2',    Query2AA_CV2.AsString);
        ADDValue('AA_CV3',    Query2AA_CV3.AsString);
        ADDValue('AA_CV4',    Query2AA_CV4.AsString);
        ADDValue('AVAIL',     Query2AVAIL.AsString);
        ADDValue('AVAIL1',    Query2AVAIL1.AsString);
        ADDValue('AVAIL2',    Query2AVAIL2.AsString);
        ADDValue('AVAIL3',    Query2AVAIL3.AsString);
        ADDValue('AVAIL4',    Query2AVAIL4.AsString);
        ADDValue('AV_ACCNT',  Query2AV_ACCNT.AsString);
        ADDValue('AV_PAY',    Query2AV_PAY.AsString);
        ADDValue('DRAFT1',    Query2DRAFT1.AsString);
        ADDValue('DRAFT2',    Query2DRAFT2.AsString);
        ADDValue('DRAFT3',    Query2DRAFT3.AsString);
        ADDValue('DRAWEE',    Query2DRAWEE.AsString);
        ADDValue('DRAWEE1',   Query2DRAWEE1.AsString);
        ADDValue('DRAWEE2',   Query2DRAWEE2.AsString);
        ADDValue('DRAWEE3',   Query2DRAWEE3.AsString);
        ADDValue('DRAWEE4',   Query2DRAWEE4.AsString);
        ADDValue('DR_ACCNT',  Query2DR_ACCNT.AsString);
//        ADDValue('AVAIL_BIC',
//        ADDValue('DRAWEE_BIC',
      end;

      with SC[2] do
      begin
        DMLType := dmlInsert;
        SQLHeader('INF700_3');
        ADDValue('MAINT_NO', Query2.Fields[0].AsString);
        ADDValue('MSEQ', 1);
        ADDValue('PSHIP',    Query2PSHIP.AsString);
        ADDValue('TSHIP',    Query2TSHIP.asString);
        ADDValue('LOAD_ON',  Query2LOAD_ON.AsString);
        ADDValue('FOR_TRAN', Query2FOR_TRAN.AsString);
        ADDValue('LST_DATE', Query2LST_DATE.AsString);
        ADDValue('SHIP_PD',  Query2SHIP_PD.asBoolean);
        ADDValue('SHIP_PD1', Query2SHIP_PD1.AsString);
        ADDValue('SHIP_PD2', Query2SHIP_PD2.AsString);
        ADDValue('SHIP_PD3', Query2SHIP_PD3.AsString);
        ADDValue('SHIP_PD4', Query2SHIP_PD4.AsString);
        ADDValue('SHIP_PD5', Query2SHIP_PD5.AsString);
        ADDValue('SHIP_PD6', Query2SHIP_PD6.AsString);
        ADDValue('DESGOOD',  Query2DESGOOD.asBoolean);
        ADDValue('DESGOOD_1', Query2DESGOOD_1.AsString);
        ADDValue('TERM_PR',   Query2TERM_PR.AsString);
        ADDValue('TERM_PR_M', Query2TERM_PR_M.AsString);
        ADDValue('PL_TERM', Query2PL_TERM.AsString);
        ADDValue('ORIGIN',  Query2ORIGIN.AsString);
        ADDValue('ORIGIN_M',  Query2ORIGIN_M.AsString);
        ADDValue('DOC_380',   Query2DOC_380.asBoolean);
        ADDValue('DOC_380_1', Query2DOC_380_1.asFloat);
        ADDValue('DOC_705',   Query2DOC_705.asBoolean);
        ADDValue('DOC_705_1', Query2DOC_705_1.AsString);
        ADDValue('DOC_705_2', Query2DOC_705_2.AsString);
        ADDValue('DOC_705_3', Query2DOC_705_3.AsString);
        ADDValue('DOC_705_4', Query2DOC_705_4.AsString);
        ADDValue('DOC_740',   Query2DOC_740.asBoolean);
        ADDValue('DOC_740_1', Query2DOC_740_1.AsString);
        ADDValue('DOC_740_2', Query2DOC_740_2.AsString);
        ADDValue('DOC_740_3', Query2DOC_740_3.AsString);
        ADDValue('DOC_740_4', Query2DOC_740_4.AsString);
        ADDValue('DOC_530',   Query2DOC_530.asBoolean);
        ADDValue('DOC_530_1', Query2DOC_530_1.AsString);
        ADDValue('DOC_530_2', Query2DOC_530_2.AsString);
        ADDValue('DOC_271',   Query2DOC_271.asBoolean);
        ADDValue('DOC_271_1', Query2DOC_271_1.asFloat);
        ADDValue('DOC_861',   Query2DOC_861.asBoolean);
        ADDValue('DOC_2AA',   Query2DOC_2AA.asBoolean);
        ADDValue('DOC_2AA_1', Query2DOC_2AA_1.AsString);
        ADDValue('ACD_2AA',   Query2ACD_2AA.asBoolean);
        ADDValue('ACD_2AA_1', Query2ACD_2AA_1.AsString);
        ADDValue('ACD_2AB',   Query2ACD_2AB.asBoolean);
        ADDValue('ACD_2AC',   Query2ACD_2AC.asBoolean);
        ADDValue('ACD_2AD',   Query2ACD_2AD.asBoolean);
        ADDValue('ACD_2AE',   Query2ACD_2AE.asBoolean);
        ADDValue('ACD_2AE_1', Query2ACD_2AE_1.AsString);
        ADDValue('CHARGE',    Query2CHARGE.AsString);
        ADDValue('PERIOD',    Query2PERIOD.asFloat);
        ADDValue('CONFIRMM',  Query2CONFIRMM.AsString);
        ADDValue('DEF_PAY1',  Query2DEF_PAY1.AsString);
        ADDValue('DEF_PAY2',  Query2DEF_PAY2.AsString);
        ADDValue('DEF_PAY3',  Query2DEF_PAY3.AsString);
        ADDValue('DEF_PAY4',  Query2DEF_PAY4.AsString);
        ADDValue('DOC_705_GUBUN', Query2DOC_705_GUBUN.AsString);
        TEMP_STR := Query2CHARGE_NUM1.AsString;
        IF Length(Trim(Query2CHARGE_NUM2.AsString)) > 0 Then
          TEMP_STR := TEMP_STR +#13#10+ Query2CHARGE_NUM2.AsString;
        IF Length(Trim(Query2CHARGE_NUM3.AsString)) > 0 Then
          TEMP_STR := TEMP_STR +#13#10+ Query2CHARGE_NUM3.AsString;
        IF Length(Trim(Query2CHARGE_NUM4.AsString)) > 0 Then
          TEMP_STR := TEMP_STR +#13#10+ Query2CHARGE_NUM4.AsString;
        IF Length(Trim(Query2CHARGE_NUM5.AsString)) > 0 Then
          TEMP_STR := TEMP_STR +#13#10+ Query2CHARGE_NUM5.AsString;
        IF Length(Trim(Query2CHARGE_NUM6.AsString)) > 0 Then
          TEMP_STR := TEMP_STR +#13#10+ Query2CHARGE_NUM6.AsString;
        ADDValue('CHARGE_1', TEMP_STR );
        ADDValue('CONFIRM_BICCD', Query2CO_BANK.AsString);
        ADDValue('CONFIRM_BANKNM', Trim(Query2CO_BANK1.AsString+' '+Query2CO_BANK2.AsString));
        ADDValue('CONFIRM_BANKBR', Trim(Query2CO_BANK3.AsString+' '+Query2CO_BANK4.AsString));
        if Length(Trim(Query2PERIOD_TXT.AsString)) > 0 Then
          ADDValue('PERIOD_IDX', 0)
        else
          ADDValue('PERIOD_IDX', 1);        
        ADDValue('PERIOD_TXT', Query2PERIOD_TXT.AsString);
        ADDValue('SPECIAL_PAY', Query2SPECIAL_DESC_1.AsString);
      end;

      with SC[3] do
      begin
        DMLType := dmlInsert;
        SQLHeader('INF700_4');
        ADDValue('MAINT_NO', Query2.Fields[0].AsString);
        ADDValue('MSEQ', 1);
        ADDValue('REI_BANK', Query2REI_BANK.AsString);
        ADDValue('REI_BANK1', Query2REI_BANK.AsString);
        ADDValue('REI_BANK2', Query2REI_BANK1.AsString);
        ADDValue('REI_BANK3', Query2REI_BANK2.AsString);
        ADDValue('REI_BANK4', Query2REI_BANK3.AsString);
        ADDValue('REI_BANK5', Query2REI_BANK4.AsString);
        ADDValue('REI_ACNNT', Query2REI_ACNNT.AsString);
        ADDValue('INSTRCT',   Query2INSTRCT.asBoolean);
        ADDValue('INSTRCT_1', Query2INSTRCT_1.AsString);
        ADDValue('AVT_BANK',  Query2AVT_BANK.AsString);
        ADDValue('AVT_BANK1', Query2AVT_BANK1.AsString);
        ADDValue('AVT_BANK2', Query2AVT_BANK2.AsString);
        ADDValue('AVT_BANK3', Query2AVT_BANK3.AsString);
        ADDValue('AVT_BANK4', Query2AVT_BANK4.AsString);
        ADDValue('AVT_BANK5', Query2AVT_BANK5.AsString);
        ADDValue('AVT_ACCNT', Query2AVT_ACCNT.AsString);
        ADDValue('SND_INFO1', Query2SND_INFO1.AsString);
        ADDValue('SND_INFO2', Query2SND_INFO2.AsString);
        ADDValue('SND_INFO3', Query2SND_INFO3.AsString);
        ADDValue('SND_INFO4', Query2SND_INFO4.AsString);
        ADDValue('SND_INFO5', Query2SND_INFO5.AsString);
        ADDValue('SND_INFO6', Query2SND_INFO6.AsString);
        ADDValue('EX_NAME1',  Query2EX_NAME1.AsString);
        ADDValue('EX_NAME2',  Query2EX_NAME2.AsString);
        ADDValue('EX_NAME3',  Query2EX_NAME3.AsString);
        ADDValue('EX_ADDR1',  Query2EX_ADDR1.AsString);
        ADDValue('EX_ADDR2',  Query2EX_ADDR2.AsString);
        ADDValue('EX_ADDR3',  Query2EX_ADDR3.AsString);
        ADDValue('OP_BANK1',  Query2OP_BANK1.AsString);
        ADDValue('OP_BANK2',  Query2OP_BANK2.AsString);
        ADDValue('OP_BANK3',  Query2OP_BANK3.AsString);
        ADDValue('OP_ADDR1',  Query2OP_ADDR1.AsString);
        ADDValue('OP_ADDR2',  Query2OP_ADDR2.AsString);
        ADDValue('OP_ADDR3',  Query2OP_ADDR3.AsString);
        ADDValue('MIX_PAY1',  Query2MIX_PAY1.AsString);
        ADDValue('MIX_PAY2',  Query2MIX_PAY2.AsString);
        ADDValue('MIX_PAY3',  Query2MIX_PAY3.AsString);
        ADDValue('MIX_PAY4',  Query2MIX_PAY4.AsString);
        ADDValue('APPLICABLE_RULES_1', Query2APPLICABLE_RULES_1.AsString);
        ADDValue('APPLICABLE_RULES_2', Query2APPLICABLE_RULES_2.AsString);
        ADDValue('DOC_760',   Query2DOC_760.asBoolean);
        ADDValue('DOC_760_1', Query2DOC_760_1.AsString);
        ADDValue('DOC_760_2', Query2DOC_760_2.AsString);
        ADDValue('DOC_760_3', Query2DOC_760_3.AsString);
        ADDValue('DOC_760_4', Query2DOC_760_4.AsString);
        ADDValue('SUNJUCK_PORT', Query2SUNJUCK_PORT.AsString);
        ADDValue('DOCHACK_PORT', Query2DOCHACK_PORT.AsString);
      end;

      RunSQL(SC[0].CreateSQL);
      RunSQL(SC[1].CreateSQL);
      RunSQL(SC[2].CreateSQL);
      RunSQL(SC[3].CreateSQL);

      sProgressBar1.Position := Query2.RecNo;
      Query2.Next;
    end;
  finally
    SC[0].Free;
    SC[1].Free;
    SC[2].Free;
    SC[3].Free;
    Query2.Close;
  end;
end;

procedure TDlg_ImportINF700_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := Query2.SQL.Text;
end;

procedure TDlg_ImportINF700_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  try
    ConvertData;
    ShowMessage('전환이 완료되었습니다');
  except
    on E:Exception do
    begin
      ShowMessage(E.Message);
    end;
  end;
end;

end.
