unit Dlg_ImportAPP700;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OKBottomDlg, StdCtrls, sButton, sCheckBox, sEdit, sLabel,
  ExtCtrls, DB, ADODB, DBTables, ComCtrls, acProgressBar, sComboBox,
  sRadioButton;

type
  TDlg_ImportAPP700_frm = class(TOKBottomDlg_frm)
    Query1: TQuery;
    sProgressBar1: TsProgressBar;
    Query1MAINT_NO: TStringField;
    Query1MESSAGE1: TStringField;
    Query1MESSAGE2: TStringField;
    Query1USER_ID: TStringField;
    Query1DATEE: TStringField;
    Query1APP_DATE: TStringField;
    Query1IN_MATHOD: TStringField;
    Query1AP_BANK: TStringField;
    Query1AP_BANK1: TStringField;
    Query1AP_BANK2: TStringField;
    Query1AP_BANK3: TStringField;
    Query1AP_BANK4: TStringField;
    Query1AP_BANK5: TStringField;
    Query1AD_BANK: TStringField;
    Query1AD_BANK1: TStringField;
    Query1AD_BANK2: TStringField;
    Query1AD_BANK3: TStringField;
    Query1AD_BANK4: TStringField;
    Query1AD_PAY: TStringField;
    Query1IL_NO1: TStringField;
    Query1IL_NO2: TStringField;
    Query1IL_NO3: TStringField;
    Query1IL_NO4: TStringField;
    Query1IL_NO5: TStringField;
    Query1IL_AMT1: TFloatField;
    Query1IL_AMT2: TFloatField;
    Query1IL_AMT3: TFloatField;
    Query1IL_AMT4: TFloatField;
    Query1IL_AMT5: TFloatField;
    Query1IL_CUR1: TStringField;
    Query1IL_CUR2: TStringField;
    Query1IL_CUR3: TStringField;
    Query1IL_CUR4: TStringField;
    Query1IL_CUR5: TStringField;
    Query1AD_INFO1: TStringField;
    Query1AD_INFO2: TStringField;
    Query1AD_INFO3: TStringField;
    Query1AD_INFO4: TStringField;
    Query1AD_INFO5: TStringField;
    Query1DOC_CD: TStringField;
    Query1EX_DATE: TStringField;
    Query1EX_PLACE: TStringField;
    Query1CHK1: TBooleanField;
    Query1CHK2: TStringField;
    Query1CHK3: TStringField;
    Query1IMP_CD1: TStringField;
    Query1IMP_CD2: TStringField;
    Query1IMP_CD3: TStringField;
    Query1IMP_CD4: TStringField;
    Query1IMP_CD5: TStringField;
    Query1F_INTERFACE: TStringField;
    Query1prno: TIntegerField;
    Query1AD_BANK_BIC: TStringField;
    Query1CO_BANK: TStringField;
    Query1CO_BANK1: TStringField;
    Query1CO_BANK2: TStringField;
    Query1CO_BANK3: TStringField;
    Query1CO_BANK4: TStringField;
    Query1APPLIC1: TStringField;
    Query1APPLIC2: TStringField;
    Query1APPLIC3: TStringField;
    Query1APPLIC4: TStringField;
    Query1APPLIC5: TStringField;
    Query1BENEFC: TStringField;
    Query1BENEFC1: TStringField;
    Query1BENEFC2: TStringField;
    Query1BENEFC3: TStringField;
    Query1BENEFC4: TStringField;
    Query1BENEFC5: TStringField;
    Query1CD_AMT: TFloatField;
    Query1CD_CUR: TStringField;
    Query1CD_PERP: TFloatField;
    Query1CD_PERM: TFloatField;
    Query1CD_MAX: TStringField;
    Query1AA_CV1: TStringField;
    Query1AA_CV2: TStringField;
    Query1AA_CV3: TStringField;
    Query1AA_CV4: TStringField;
    Query1DRAFT1: TStringField;
    Query1DRAFT2: TStringField;
    Query1DRAFT3: TStringField;
    Query1MIX_PAY1: TStringField;
    Query1MIX_PAY2: TStringField;
    Query1MIX_PAY3: TStringField;
    Query1MIX_PAY4: TStringField;
    Query1DEF_PAY1: TStringField;
    Query1DEF_PAY2: TStringField;
    Query1DEF_PAY3: TStringField;
    Query1DEF_PAY4: TStringField;
    Query1PSHIP: TStringField;
    Query1TSHIP: TStringField;
    Query1LOAD_ON: TStringField;
    Query1FOR_TRAN: TStringField;
    Query1LST_DATE: TStringField;
    Query1SHIP_PD1: TStringField;
    Query1SHIP_PD2: TStringField;
    Query1SHIP_PD3: TStringField;
    Query1SHIP_PD4: TStringField;
    Query1SHIP_PD5: TStringField;
    Query1SHIP_PD6: TStringField;
    Query1DESGOOD_1: TMemoField;
    Query1SPECIAL_DESC: TStringField;
    Query1SPECIAL_DESC_1: TMemoField;
    Query1DOC_380: TBooleanField;
    Query1DOC_380_1: TFloatField;
    Query1DOC_705: TBooleanField;
    Query1DOC_705_1: TStringField;
    Query1DOC_705_2: TStringField;
    Query1DOC_705_3: TStringField;
    Query1DOC_705_4: TStringField;
    Query1DOC_740: TBooleanField;
    Query1DOC_740_1: TStringField;
    Query1DOC_740_2: TStringField;
    Query1DOC_740_3: TStringField;
    Query1DOC_740_4: TStringField;
    Query1DOC_530: TBooleanField;
    Query1DOC_530_1: TStringField;
    Query1DOC_530_2: TStringField;
    Query1DOC_271: TBooleanField;
    Query1DOC_271_1: TFloatField;
    Query1DOC_861: TBooleanField;
    Query1DOC_2AA: TBooleanField;
    Query1DOC_2AA_1: TMemoField;
    Query1ACD_2AA: TBooleanField;
    Query1ACD_2AA_1: TStringField;
    Query1ACD_2AB: TBooleanField;
    Query1ACD_2AC: TBooleanField;
    Query1ACD_2AD: TBooleanField;
    Query1ACD_2AE: TBooleanField;
    Query1ACD_2AE_1: TMemoField;
    Query1CHARGE: TStringField;
    Query1PERIOD: TFloatField;
    Query1CONFIRMM: TStringField;
    Query1INSTRCT: TBooleanField;
    Query1INSTRCT_1: TMemoField;
    Query1EX_NAME1: TStringField;
    Query1EX_NAME2: TStringField;
    Query1EX_NAME3: TStringField;
    Query1EX_ADDR1: TStringField;
    Query1EX_ADDR2: TStringField;
    Query1ORIGIN: TStringField;
    Query1ORIGIN_M: TStringField;
    Query1PL_TERM: TStringField;
    Query1TERM_PR: TStringField;
    Query1TERM_PR_M: TStringField;
    Query1SRBUHO: TStringField;
    Query1DOC_705_GUBUN: TStringField;
    Query1CARRIAGE: TStringField;
    Query1DOC_760: TBooleanField;
    Query1DOC_760_1: TStringField;
    Query1DOC_760_2: TStringField;
    Query1DOC_760_3: TStringField;
    Query1DOC_760_4: TStringField;
    Query1SUNJUCK_PORT: TStringField;
    Query1DOCHACK_PORT: TStringField;
    Query1CHARGE_NUM1: TStringField;
    Query1CHARGE_NUM2: TStringField;
    Query1CHARGE_NUM3: TStringField;
    Query1CHARGE_NUM4: TStringField;
    Query1CHARGE_NUM5: TStringField;
    Query1CHARGE_NUM6: TStringField;
    Query1PERIOD_TXT: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    function ConvertData: Boolean; override;
    { Public declarations }
  end;

var
  Dlg_ImportAPP700_frm: TDlg_ImportAPP700_frm;

implementation

uses
  SQLCreator, VarDefine;

{$R *.dfm}

{ TDlg_ImportAPP700_frm }

function TDlg_ImportAPP700_frm.ConvertData: Boolean;
var
  SC : array [0..2] of TSQLCreate;
  nCount, nMax : Integer;
begin
  Result := False;
  try
    if sRadioButton1.Checked then
    begin
      Query1.Close;
      Query1.SQL.Text := FSQL;
      Query1.SQL.Add('WHERE A1.MAINT_NO = '+QuotedStr(sEdit1.Text));
      Query1.Open;
    end
    else
    if sRadioButton2.Checked then
    begin
      Query1.Close;
      Query1.SQL.Text := FSQL;
      Query1.SQL.Add('WHERE SUBSTRING(A1.DATEE FROM 1 FOR 4) = '+QuotedStr(sComboBox1.Text));
      Query1.Open;
    end
    else
    if sRadioButton3.Checked Then
    begin
      Query1.Close;
      Query1.SQL.Text := FSQL;
      Query1.Open;
    end;

    if Query1.RecordCount = 0 Then
      raise Exception.Create('데이터가 없습니다');

    SC[0] := TSQLCreate.Create;
    SC[1] := TSQLCreate.Create;
    SC[2] := TSQLCreate.Create;

//------------------------------------------------------------------------------
// 중복삭제/초기화 섹션
//------------------------------------------------------------------------------
    if sRadioButton1.Checked then
    begin
      if RunSQLCount('SELECT 1 FROM APP700_1 WHERE MAINT_NO = '+QuotedStr(sEdit1.Text)) > 0 Then
      begin
        if MessageBox(Self.Handle, '해당 문서는 이미 존재합니다. 덮어씌우시겠습니까?', '문서확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then
          raise Exception.Create('취소하였습니다')
        else
        begin
          with SC[0] do begin DMLType := dmlDelete; SQLHeader('APP700_1'); ADDWhere('MAINT_NO', sEdit1.Text); end;
          with SC[1] do begin DMLType := dmlDelete; SQLHeader('APP700_2'); ADDWhere('MAINT_NO', sEdit1.Text); end;
          with SC[2] do begin DMLType := dmlDelete; SQLHeader('APP700_3'); ADDWhere('MAINT_NO', sEdit1.Text); end;
          RunSQL(SC[0].CreateSQL);
          RunSQL(SC[1].CreateSQL);
          RunSQL(SC[2].CreateSQL);
        end;
      end;
    end
    else
    if sRadioButton2.Checked then
    begin
      if MessageBox(Self.Handle, '현재프로그램의 해당년도 데이터는 초기화 됩니다. 계속하시겠습니까?', '초기화 확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then
        raise Exception.Create('취소하였습니다')
      else
      begin
        RunSQL('DELETE FROM APP700_3 WHERE MAINT_NO IN (SELECT MAINT_NO FROM APP700_1 WHERE SUBSTRING(DATEE, 1, 4) = '+QuotedStr(sComboBox1.Text)+')');
        RunSQL('DELETE FROM APP700_2 WHERE MAINT_NO IN (SELECT MAINT_NO FROM APP700_1 WHERE SUBSTRING(DATEE, 1, 4) = '+QuotedStr(sComboBox1.Text)+')');
        RunSQL('DELETE FROM APP700_1 WHERE SUBSTRING(DATEE, 1, 4) = '+QuotedStr(sCombobox1.Text));
      end;
    end
    else
    if sRadioButton3.Checked Then
    begin
      with SC[0] do begin DMLType := dmlDelete; SQLHeader('APP700_1'); end;
      with SC[1] do begin DMLType := dmlDelete; SQLHeader('APP700_2'); end;
      with SC[2] do begin DMLType := dmlDelete; SQLHeader('APP700_3'); end;
      RunSQL(SC[0].CreateSQL);
      RunSQL(SC[1].CreateSQL);
      RunSQL(SC[2].CreateSQL);
    end;

    nCount := 0;
    sProgressBar1.Max := Query1.RecordCount;
    sProgressBar1.Position := 0;

    while not Query1.Eof do
    begin
      Application.ProcessMessages;

      with SC[0] do
      begin
        // APP700_1
        DMLType := dmlInsert;
        SQLHeader('APP700_1');
        ADDValue('MAINT_NO', Query1.Fields[0].AsString);
        ADDValue('MSEQ', 1);
        ADDValue('MESSAGE1', Query1MESSAGE1.AsString);
        ADDValue('MESSAGE2', Query1MESSAGE2.AsString);
        ADDValue('USER_ID',  LoginData.sID);
        ADDValue('DATEE',    Query1DATEE.AsString);
        ADDValue('APP_DATE', Query1APP_DATE.AsString);
        ADDValue('IN_MATHOD',  Query1IN_MATHOD.AsString);
        ADDValue('AP_BANK',    Query1AP_BANK.AsString);
        ADDValue('AP_BANK1',   Query1AP_BANK1.AsString);
        ADDValue('AP_BANK2',   Query1AP_BANK2.AsString);
        ADDValue('AP_BANK3',   Query1AP_BANK3.AsString);
        ADDValue('AP_BANK4',   Query1AP_BANK4.AsString);
        ADDValue('AP_BANK5',   Query1AP_BANK5.AsString);
        ADDValue('AD_BANK',    Query1AD_BANK.AsString);
        ADDValue('AD_BANK1',   Query1AD_BANK1.AsString);
        ADDValue('AD_BANK2',   Query1AD_BANK2.AsString);
        ADDValue('AD_BANK3',   Query1AD_BANK3.AsString);
        ADDValue('AD_BANK4',   Query1AD_BANK4.AsString);
        ADDValue('AD_BANK_BIC', Query1AD_BANK_BIC.AsString);
        ADDValue('AD_PAY',     Query1AD_PAY.AsString);
        ADDValue('IL_NO1',     Query1IL_NO1.AsString);
        ADDValue('IL_NO2',     Query1IL_NO2.AsString);
        ADDValue('IL_NO3',     Query1IL_NO3.AsString);
        ADDValue('IL_NO4',     Query1IL_NO4.AsString);
        ADDValue('IL_NO5',     Query1IL_NO5.AsString);
        ADDValue('IL_AMT1',    Query1IL_AMT1.asFloat);
        ADDValue('IL_AMT2',    Query1IL_AMT2.asFloat);
        ADDValue('IL_AMT3',    Query1IL_AMT3.asFloat);
        ADDValue('IL_AMT4',    Query1IL_AMT4.asFloat);
        ADDValue('IL_AMT5',    Query1IL_AMT5.asFloat);
        ADDValue('IL_CUR1',    Query1IL_CUR1.AsString);
        ADDValue('IL_CUR2',    Query1IL_CUR2.AsString);
        ADDValue('IL_CUR3',    Query1IL_CUR3.AsString);
        ADDValue('IL_CUR4',    Query1IL_CUR4.AsString);
        ADDValue('IL_CUR5',    Query1IL_CUR5.AsString);
        ADDValue('AD_INFO1',   Query1AD_INFO1.AsString);
        ADDValue('AD_INFO2',   Query1AD_INFO2.AsString);
        ADDValue('AD_INFO3',   Query1AD_INFO3.AsString);
        ADDValue('AD_INFO4',   Query1AD_INFO4.AsString);
        ADDValue('AD_INFO5',   Query1AD_INFO5.AsString);
        ADDValue('DOC_CD',     Query1DOC_CD.AsString);
        ADDValue('EX_DATE',    Query1EX_DATE.AsString);
        ADDValue('EX_PLACE',   Query1EX_PLACE.AsString);
        ADDValue('CHK1',       Query1CHK1.asBoolean);
        ADDValue('CHK2',       Query1CHK2.AsString);
        ADDValue('CHK3',       Query1CHK3.AsString);
        ADDValue('F_INTERFACE', Query1F_INTERFACE.AsString);
        ADDValue('IMP_CD1', Query1IMP_CD1.AsString);
        ADDValue('IMP_CD2', Query1IMP_CD2.AsString);
        ADDValue('IMP_CD3', Query1IMP_CD3.AsString);
        ADDValue('IMP_CD4', Query1IMP_CD4.AsString);
        ADDValue('IMP_CD5', Query1IMP_CD5.AsString);
      end;

      with SC[1] do
      begin
        DMLType := dmlInsert;
        SQLHeader('APP700_2');
        ADDValue('MAINT_NO', Query1.Fields[0].AsString);
        ADDValue('MSEQ', 1);
        ADDValue('APPLIC1',  Query1APPLIC1.AsString);
        ADDValue('APPLIC2',  Query1APPLIC2.AsString);
        ADDValue('APPLIC3',  Query1APPLIC3.AsString);
        ADDValue('APPLIC4',  Query1APPLIC4.AsString);
        ADDValue('APPLIC5',  Query1APPLIC5.AsString);
        ADDValue('BENEFC',   Query1BENEFC.AsString);
        ADDValue('BENEFC1',  Query1BENEFC1.AsString);
        ADDValue('BENEFC2',  Query1BENEFC2.AsString);
        ADDValue('BENEFC3',  Query1BENEFC3.AsString);
        ADDValue('BENEFC4',  Query1BENEFC4.AsString);
        ADDValue('BENEFC5',  Query1BENEFC5.AsString);
        ADDValue('CD_AMT',   Query1CD_AMT.asFloat);
        ADDValue('CD_CUR',   Query1CD_CUR.AsString);
        ADDValue('CD_PERP',  Query1CD_PERP.asfloat);
        ADDValue('CD_PERM',  Query1CD_PERM.asfloat);
        ADDValue('CD_MAX',   Query1CD_MAX.AsString);
        ADDValue('AA_CV1',   Query1AA_CV1.AsString);
        ADDValue('AA_CV2',   Query1AA_CV2.AsString);
        ADDValue('AA_CV3',   Query1AA_CV3.AsString);
        ADDValue('AA_CV4',   Query1AA_CV4.AsString);
        ADDValue('DRAFT1',   Query1DRAFT1.AsString);
        ADDValue('DRAFT2',   Query1DRAFT2.AsString);
        ADDValue('DRAFT3',   Query1DRAFT3.AsString);
        ADDValue('MIX_PAY1', Query1MIX_PAY1.asString);
        ADDValue('MIX_PAY2', Query1MIX_PAY2.asString);
        ADDValue('MIX_PAY3', Query1MIX_PAY3.asString);
        ADDValue('MIX_PAY4', Query1MIX_PAY4.asString);
        ADDValue('DEF_PAY1', Query1DEF_PAY1.AsString);
        ADDValue('DEF_PAY2', Query1DEF_PAY2.AsString);
        ADDValue('DEF_PAY3', Query1DEF_PAY3.AsString);
        ADDValue('DEF_PAY4', Query1DEF_PAY4.AsString);
        ADDValue('PSHIP',    Query1PSHIP.AsString);
        ADDValue('TSHIP',    Query1TSHIP.AsString);
        ADDValue('LOAD_ON',  Query1LOAD_ON.AsString);
        ADDValue('FOR_TRAN', Query1FOR_TRAN.AsString);
        ADDValue('LST_DATE', Query1LST_DATE.AsString);
        ADDValue('SHIP_PD1', Query1SHIP_PD1.AsString);
        ADDValue('SHIP_PD2', Query1SHIP_PD2.AsString);
        ADDValue('SHIP_PD3', Query1SHIP_PD3.AsString);
        ADDValue('SHIP_PD4', Query1SHIP_PD4.AsString);
        ADDValue('SHIP_PD5', Query1SHIP_PD5.AsString);
        ADDValue('SHIP_PD6', Query1SHIP_PD6.AsString);
        ADDValue('DESGOOD_1', Query1DESGOOD_1.AsString);
      end;

      with SC[2] do
      begin
        DMLType := dmlInsert;
        SQLHeader('APP700_3');
        ADDValue('MAINT_NO', Query1.Fields[0].AsString);
        ADDValue('MSEQ', 1);
        ADDValue('DOC_380',    Query1DOC_380.asBoolean);
        ADDValue('DOC_380_1',  Query1DOC_380_1.asfloat);
        ADDValue('DOC_705',    Query1DOC_705.asBoolean);
        ADDValue('DOC_705_1',  Query1DOC_705_1.AsString);
        ADDValue('DOC_705_2',  Query1DOC_705_2.AsString);
        ADDValue('DOC_705_3',  Query1DOC_705_3.AsString);
        ADDValue('DOC_705_4',  Query1DOC_705_4.AsString);
        ADDValue('DOC_740',    Query1DOC_740.asBoolean);
        ADDValue('DOC_740_1',  Query1DOC_740_1.AsString);
        ADDValue('DOC_740_2',  Query1DOC_740_2.AsString);
        ADDValue('DOC_740_3',  Query1DOC_740_3.AsString);
        ADDValue('DOC_740_4',  Query1DOC_740_4.AsString);
        ADDValue('DOC_530',    Query1DOC_530.asBoolean);
        ADDValue('DOC_530_1',  Query1DOC_530_1.AsString);
        ADDValue('DOC_530_2',  Query1DOC_530_2.AsString);
        ADDValue('DOC_271',    Query1DOC_271.asBoolean);
        ADDValue('DOC_271_1',  Query1DOC_271_1.AsString);
        ADDValue('DOC_861',    Query1DOC_861.asBoolean);
        ADDValue('DOC_2AA',    Query1DOC_2AA.asBoolean);
        ADDValue('DOC_2AA_1',  Query1DOC_2AA_1.AsString);
        ADDValue('ACD_2AA',    Query1ACD_2AA.asBoolean);
        ADDValue('ACD_2AA_1',  Query1ACD_2AA_1.AsString);
        ADDValue('ACD_2AB',    Query1ACD_2AB.asBoolean);
        ADDValue('ACD_2AC',    Query1ACD_2AC.asBoolean);
        ADDValue('ACD_2AD',    Query1ACD_2AD.asBoolean);
        ADDValue('ACD_2AE',    Query1ACD_2AE.asBoolean);
        ADDValue('ACD_2AE_1',  Query1ACD_2AE_1.AsString);
        ADDValue('CHARGE',     Query1CHARGE.AsString);
        ADDValue('CHARGE_1',   Trim(Query1CHARGE_NUM1.AsString+Query1CHARGE_NUM2.AsString+Query1CHARGE_NUM3.AsString+Query1CHARGE_NUM4.AsString+Query1CHARGE_NUM5.AsString+Query1CHARGE_NUM6.AsString));
        ADDValue('PERIOD',     Query1PERIOD.asfloat);
        ADDValue('CONFIRMM',   Query1CONFIRMM.AsString);
        ADDValue('INSTRCT',    Query1INSTRCT.asBoolean);
        ADDValue('INSTRCT_1',  Query1INSTRCT_1.AsString);
        ADDValue('EX_NAME1',   Query1EX_NAME1.AsString);
        ADDValue('EX_NAME2',   Query1EX_NAME2.AsString);
        ADDValue('EX_NAME3',   Query1EX_NAME3.AsString);
        ADDValue('EX_ADDR1',   Query1EX_ADDR1.AsString);
        ADDValue('EX_ADDR2',   Query1EX_ADDR2.AsString);
        ADDValue('ORIGIN',     Query1ORIGIN.AsString);
        ADDValue('ORIGIN_M',   Query1ORIGIN_M.AsString);
        ADDValue('PL_TERM',    Query1PL_TERM.AsString);
        ADDValue('TERM_PR',    Query1TERM_PR.AsString);
        ADDValue('TERM_PR_M',  Query1TERM_PR_M.AsString);
        ADDValue('SRBUHO',     Query1SRBUHO.AsString);
        ADDValue('DOC_705_GUBUN',  Query1DOC_705_GUBUN.AsString);
        ADDValue('CARRIAGE',  Query1CARRIAGE.AsString);
        ADDValue('DOC_760',   Query1DOC_760.asboolean);
        ADDValue('DOC_760_1', Query1DOC_760_1.AsString);
        ADDValue('DOC_760_2', Query1DOC_760_2.AsString);
        ADDValue('DOC_760_3', Query1DOC_760_3.AsString);
        ADDValue('DOC_760_4', Query1DOC_760_4.AsString);
        ADDValue('SUNJUCK_PORT', Query1SUNJUCK_PORT.AsString);
        ADDValue('DOCHACK_PORT', Query1DOCHACK_PORT.AsString);
        ADDValue('CONFIRM_BICCD', Query1CO_BANK.AsString);
        ADDValue('CONFIRM_BANKNM', Trim(Query1CO_BANK1.AsString+Query1CO_BANK2.AsString));
        ADDValue('CONFIRM_BANKBR', Trim(Query1CO_BANK3.AsString+Query1CO_BANK4.AsString));
        ADDValue('PERIOD_TXT', Query1PERIOD_TXT.AsString);
        IF Length(Trim(Query1PERIOD_TXT.AsString)) = 0 Then
          ADDValue('PERIOD_IDX', 0)
        else
          ADDValue('PERIOD_IDX', 1);

        ADDValue('SPECIAL_PAY', Query1SPECIAL_DESC_1.AsString);
      end;

      RunSQL(SC[0].CreateSQL);
      RunSQL(SC[1].CreateSQL);
      RunSQL(SC[2].CreateSQL);

      sProgressBar1.Position := Query1.RecNo;
      Query1.Next;
    end;
  finally
    SC[0].Free;
    SC[1].Free;
    SC[2].Free;
    Query1.Close;
  end;
end;

procedure TDlg_ImportAPP700_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := Query1.SQL.Text;
end;

procedure TDlg_ImportAPP700_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  try
    ConvertData;
    ShowMessage('전환이 완료되었습니다');
  except
    on E:Exception do
    begin
      ShowMessage(E.Message);
    end;
  end;
end;

end.
