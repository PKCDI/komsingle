inherited Dlg_ImportINF700_frm: TDlg_ImportINF700_frm
  Caption = #49688#51077'L/C'#51025#45813#49436' '#44032#51256#50724#44592
  ClientHeight = 366
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  inherited sEdit1: TsEdit
    Enabled = False
  end
  inherited sButton1: TsButton
    Caption = 'Import'
  end
  inherited sButton2: TsButton
    Caption = 'Close'
  end
  inherited sRadioButton1: TsRadioButton
    Enabled = False
  end
  inherited sRadioButton2: TsRadioButton
    Enabled = False
  end
  inherited sComboBox1: TsComboBox
    Enabled = False
  end
  object sProgressBar1: TsProgressBar [15]
    Left = 0
    Top = 348
    Width = 314
    Height = 18
    Align = alBottom
    BorderWidth = 1
    Min = 0
    Max = 100
    TabOrder = 8
  end
  object Query2: TQuery
    DatabaseName = 'KOM_SINGLE'
    ParamCheck = False
    SQL.Strings = (
      
        'SELECT A1.MAINT_NO, A1.MESSAGE1, A1.MESSAGE2, A1.USER_ID, A1.DAT' +
        'EE, A1.APP_DATE, A1.IN_MATHOD, A1.AP_BANK,'
      
        'A1.AP_BANK1, A1.AP_BANK2, A1.AP_BANK3, A1.AP_BANK4, A1.AP_BANK5,' +
        ' A1.AD_BANK, A1.AD_BANK1, A1.AD_BANK2,'
      
        'A1.AD_BANK3, A1.AD_BANK4, A1.AD_PAY, A1.IL_NO1, A1.IL_NO2, A1.IL' +
        '_NO3, A1.IL_NO4, A1.IL_NO5, A1.IL_AMT1,'
      
        'A1.IL_AMT2, A1.IL_AMT3, A1.IL_AMT4, A1.IL_AMT5, A1.IL_CUR1, A1.I' +
        'L_CUR2, A1.IL_CUR3, A1.IL_CUR4,'
      
        'A1.IL_CUR5, A1.AD_INFO1, A1.AD_INFO2, A1.AD_INFO3, A1.AD_INFO4, ' +
        'A1.AD_INFO5, A1.DOC_CD, A1.CD_NO,'
      
        'A1.REF_PRE, A1.ISS_DATE, A1.EX_DATE, A1.EX_PLACE, A1.CHK1, A1.CH' +
        'K2, A1.CHK3, A1.prno, A1.F_INTERFACE,'
      
        'A1.IMP_CD1, A1.IMP_CD2, A1.IMP_CD3, A1.IMP_CD4, A1.IMP_CD5, A1.I' +
        'MP_CD5,'
      
        'A2.MAINT_NO, A2.APP_BANK, A2.APP_BANK1, A2.APP_BANK2, A2.APP_BAN' +
        'K3, A2.APP_BANK4, A2.APP_BANK5, A2.APP_ACCNT,'
      
        'A2.APPLIC1, A2.APPLIC2, A2.APPLIC3, A2.APPLIC4, A2.APPLIC5, A2.B' +
        'ENEFC1, A2.BENEFC2, A2.BENEFC3, A2.BENEFC4, A2.BENEFC5,'
      
        'A2.CD_AMT, A2.CD_CUR, A2.CD_PERP, A2.CD_PERM, A2.CD_MAX, A2.AA_C' +
        'V1, A2.AA_CV2, A2.AA_CV3, A2.AA_CV4, A2.AVAIL, A2.AVAIL1,'
      
        'A2.AVAIL2, A2.AVAIL3, A2.AVAIL4, A2.AV_ACCNT, A2.AV_PAY, A2.DRAF' +
        'T1, A2.DRAFT2, A2.DRAFT3, A2.DRAWEE, A2.DRAWEE1,'
      'A2.DRAWEE2, A2.DRAWEE3, A2.DRAWEE4, A2.DR_ACCNT,'
      
        'A3.MAINT_NO, A3.PSHIP, A3.TSHIP, A3.LOAD_ON, A3.FOR_TRAN, A3.LST' +
        '_DATE, A3.SHIP_PD, A3.SHIP_PD1, A3.SHIP_PD2, A3.SHIP_PD3,'
      
        'A3.SHIP_PD4, A3.SHIP_PD5, A3.SHIP_PD6, A3.DESGOOD, A3.DESGOOD_1,' +
        ' A3.TERM_PR, A3.TERM_PR_M, A3.PL_TERM, A3.ORIGIN, A3.ORIGIN_M,'
      
        'A3.DOC_380, A3.DOC_380_1, A3.DOC_705, A3.DOC_705_1, A3.DOC_705_2' +
        ', A3.DOC_705_3, A3.DOC_705_4, A3.DOC_740, A3.DOC_740_1,'
      
        'A3.DOC_740_2, A3.DOC_740_3, A3.DOC_740_4, A3.DOC_530, A3.DOC_530' +
        '_1, A3.DOC_530_2, A3.DOC_271, A3.DOC_271_1, A3.DOC_861,'
      
        'A3.DOC_2AA, A3.DOC_2AA_1, A3.ACD_2AA, A3.ACD_2AA_1, A3.ACD_2AB, ' +
        'A3.ACD_2AC, A3.ACD_2AD, A3.ACD_2AE, A3.ACD_2AE_1, A3.CHARGE,'
      
        'A3.PERIOD, A3.CONFIRMM, A3.DEF_PAY1, A3.DEF_PAY2, A3.DEF_PAY3, A' +
        '3.DEF_PAY4, A3.DOC_705_GUBUN, A3.CHARGE_NUM1, A3.CHARGE_NUM2,'
      
        'A3.CHARGE_NUM3, A3.CHARGE_NUM4, A3.CHARGE_NUM5, A3.CHARGE_NUM6, ' +
        'A3.PERIOD_TXT,'
      
        'A4.MAINT_NO, A4.REI_BANK, A4.REI_BANK1, A4.REI_BANK2, A4.REI_BAN' +
        'K3, A4.REI_BANK4, A4.REI_BANK5, A4.REI_ACNNT, A4.INSTRCT,'
      
        'A4.INSTRCT_1, A4.AVT_BANK, A4.AVT_BANK1, A4.AVT_BANK2, A4.AVT_BA' +
        'NK3, A4.AVT_BANK4, A4.AVT_BANK5, A4.AVT_ACCNT, A4.SND_INFO1,'
      
        'A4.SND_INFO2, A4.SND_INFO3, A4.SND_INFO4, A4.SND_INFO5, A4.SND_I' +
        'NFO6, A4.EX_NAME1, A4.EX_NAME2, A4.EX_NAME3, A4.EX_ADDR1,'
      
        'A4.EX_ADDR2, A4.EX_ADDR3, A4.OP_BANK1, A4.OP_BANK2, A4.OP_BANK3,' +
        ' A4.OP_ADDR1, A4.OP_ADDR2, A4.OP_ADDR3, A4.MIX_PAY1,'
      
        'A4.MIX_PAY2, A4.MIX_PAY3, A4.MIX_PAY4, A4.APPLICABLE_RULES_1, A4' +
        '.APPLICABLE_RULES_2, A4.DOC_760, A4.DOC_760_1, A4.DOC_760_2,'
      
        'A4.DOC_760_3, A4.DOC_760_4, A4.SUNJUCK_PORT, A4.DOCHACK_PORT, A4' +
        '.CO_BANK, A4.CO_BANK1, A4.CO_BANK2, A4.CO_BANK3, A4.CO_BANK4,'
      'A4.SPECIAL_DESC, A4.SPECIAL_DESC_1'
      'FROM INF700_1 A1'
      'INNER JOIN INF700_2 A2 ON A1.MAINT_NO = A2.MAINT_NO'
      'INNER JOIN INF700_3 A3 ON A1.MAINT_NO = A3.MAINT_NO'
      'INNER JOIN INF700_4 A4 ON A1.MAINT_NO = A4.MAINT_NO')
    Left = 240
    Top = 104
    object Query2MAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object Query2MESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object Query2MESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object Query2USER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 15
    end
    object Query2DATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object Query2APP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object Query2IN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object Query2AP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object Query2AP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object Query2AP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object Query2AP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object Query2AP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object Query2AP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object Query2AD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object Query2AD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object Query2AD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object Query2AD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object Query2AD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object Query2AD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object Query2IL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object Query2IL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object Query2IL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object Query2IL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object Query2IL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object Query2IL_AMT1: TFloatField
      FieldName = 'IL_AMT1'
    end
    object Query2IL_AMT2: TFloatField
      FieldName = 'IL_AMT2'
    end
    object Query2IL_AMT3: TFloatField
      FieldName = 'IL_AMT3'
    end
    object Query2IL_AMT4: TFloatField
      FieldName = 'IL_AMT4'
    end
    object Query2IL_AMT5: TFloatField
      FieldName = 'IL_AMT5'
    end
    object Query2IL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object Query2IL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object Query2IL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object Query2IL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object Query2IL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object Query2AD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object Query2AD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object Query2AD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object Query2AD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object Query2AD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object Query2DOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object Query2CD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object Query2REF_PRE: TStringField
      FieldName = 'REF_PRE'
      Size = 35
    end
    object Query2ISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object Query2EX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object Query2EX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object Query2CHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object Query2CHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object Query2CHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object Query2prno: TIntegerField
      FieldName = 'prno'
    end
    object Query2F_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object Query2IMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object Query2IMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object Query2IMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object Query2IMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object Query2IMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object Query2IMP_CD5_1: TStringField
      FieldName = 'IMP_CD5_1'
      Size = 3
    end
    object Query2MAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object Query2APP_BANK: TStringField
      FieldName = 'APP_BANK'
      Size = 11
    end
    object Query2APP_BANK1: TStringField
      FieldName = 'APP_BANK1'
      Size = 35
    end
    object Query2APP_BANK2: TStringField
      FieldName = 'APP_BANK2'
      Size = 35
    end
    object Query2APP_BANK3: TStringField
      FieldName = 'APP_BANK3'
      Size = 35
    end
    object Query2APP_BANK4: TStringField
      FieldName = 'APP_BANK4'
      Size = 35
    end
    object Query2APP_BANK5: TStringField
      FieldName = 'APP_BANK5'
      Size = 35
    end
    object Query2APP_ACCNT: TStringField
      FieldName = 'APP_ACCNT'
      Size = 35
    end
    object Query2APPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object Query2APPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object Query2APPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object Query2APPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object Query2APPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object Query2BENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object Query2BENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object Query2BENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object Query2BENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object Query2BENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object Query2CD_AMT: TFloatField
      FieldName = 'CD_AMT'
    end
    object Query2CD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object Query2CD_PERP: TFloatField
      FieldName = 'CD_PERP'
    end
    object Query2CD_PERM: TFloatField
      FieldName = 'CD_PERM'
    end
    object Query2CD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object Query2AA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object Query2AA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object Query2AA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object Query2AA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object Query2AVAIL: TStringField
      FieldName = 'AVAIL'
      Size = 11
    end
    object Query2AVAIL1: TStringField
      FieldName = 'AVAIL1'
      Size = 35
    end
    object Query2AVAIL2: TStringField
      FieldName = 'AVAIL2'
      Size = 35
    end
    object Query2AVAIL3: TStringField
      FieldName = 'AVAIL3'
      Size = 35
    end
    object Query2AVAIL4: TStringField
      FieldName = 'AVAIL4'
      Size = 35
    end
    object Query2AV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Size = 35
    end
    object Query2AV_PAY: TStringField
      FieldName = 'AV_PAY'
      Size = 35
    end
    object Query2DRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object Query2DRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object Query2DRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object Query2DRAWEE: TStringField
      FieldName = 'DRAWEE'
      Size = 11
    end
    object Query2DRAWEE1: TStringField
      FieldName = 'DRAWEE1'
      Size = 35
    end
    object Query2DRAWEE2: TStringField
      FieldName = 'DRAWEE2'
      Size = 35
    end
    object Query2DRAWEE3: TStringField
      FieldName = 'DRAWEE3'
      Size = 35
    end
    object Query2DRAWEE4: TStringField
      FieldName = 'DRAWEE4'
      Size = 35
    end
    object Query2DR_ACCNT: TStringField
      FieldName = 'DR_ACCNT'
      Size = 35
    end
    object Query2MAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object Query2PSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object Query2TSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object Query2LOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object Query2FOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object Query2LST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object Query2SHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object Query2SHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object Query2SHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object Query2SHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object Query2SHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object Query2SHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object Query2SHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object Query2DESGOOD: TBooleanField
      FieldName = 'DESGOOD'
    end
    object Query2DESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query2TERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object Query2TERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object Query2PL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object Query2ORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object Query2ORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object Query2DOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object Query2DOC_380_1: TFloatField
      FieldName = 'DOC_380_1'
    end
    object Query2DOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object Query2DOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object Query2DOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object Query2DOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object Query2DOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object Query2DOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object Query2DOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object Query2DOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object Query2DOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object Query2DOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object Query2DOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object Query2DOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object Query2DOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object Query2DOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object Query2DOC_271_1: TFloatField
      FieldName = 'DOC_271_1'
    end
    object Query2DOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object Query2DOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object Query2DOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query2ACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object Query2ACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object Query2ACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object Query2ACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object Query2ACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object Query2ACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object Query2ACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query2CHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object Query2PERIOD: TFloatField
      FieldName = 'PERIOD'
    end
    object Query2CONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object Query2DEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object Query2DEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object Query2DEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object Query2DEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object Query2DOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object Query2CHARGE_NUM1: TStringField
      FieldName = 'CHARGE_NUM1'
      Size = 35
    end
    object Query2CHARGE_NUM2: TStringField
      FieldName = 'CHARGE_NUM2'
      Size = 35
    end
    object Query2CHARGE_NUM3: TStringField
      FieldName = 'CHARGE_NUM3'
      Size = 35
    end
    object Query2CHARGE_NUM4: TStringField
      FieldName = 'CHARGE_NUM4'
      Size = 35
    end
    object Query2CHARGE_NUM5: TStringField
      FieldName = 'CHARGE_NUM5'
      Size = 35
    end
    object Query2CHARGE_NUM6: TStringField
      FieldName = 'CHARGE_NUM6'
      Size = 35
    end
    object Query2PERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 35
    end
    object Query2MAINT_NO_3: TStringField
      FieldName = 'MAINT_NO_3'
      Size = 35
    end
    object Query2REI_BANK: TStringField
      FieldName = 'REI_BANK'
      Size = 11
    end
    object Query2REI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Size = 35
    end
    object Query2REI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Size = 35
    end
    object Query2REI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Size = 35
    end
    object Query2REI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Size = 35
    end
    object Query2REI_BANK5: TStringField
      FieldName = 'REI_BANK5'
      Size = 35
    end
    object Query2REI_ACNNT: TStringField
      FieldName = 'REI_ACNNT'
      Size = 35
    end
    object Query2INSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object Query2INSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
      Size = 1
    end
    object Query2AVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Size = 11
    end
    object Query2AVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Size = 35
    end
    object Query2AVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Size = 35
    end
    object Query2AVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Size = 35
    end
    object Query2AVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Size = 35
    end
    object Query2AVT_BANK5: TStringField
      FieldName = 'AVT_BANK5'
      Size = 35
    end
    object Query2AVT_ACCNT: TStringField
      FieldName = 'AVT_ACCNT'
      Size = 35
    end
    object Query2SND_INFO1: TStringField
      FieldName = 'SND_INFO1'
      Size = 35
    end
    object Query2SND_INFO2: TStringField
      FieldName = 'SND_INFO2'
      Size = 35
    end
    object Query2SND_INFO3: TStringField
      FieldName = 'SND_INFO3'
      Size = 35
    end
    object Query2SND_INFO4: TStringField
      FieldName = 'SND_INFO4'
      Size = 35
    end
    object Query2SND_INFO5: TStringField
      FieldName = 'SND_INFO5'
      Size = 35
    end
    object Query2SND_INFO6: TStringField
      FieldName = 'SND_INFO6'
      Size = 35
    end
    object Query2EX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object Query2EX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object Query2EX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object Query2EX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object Query2EX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object Query2EX_ADDR3: TStringField
      FieldName = 'EX_ADDR3'
      Size = 35
    end
    object Query2OP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Size = 35
    end
    object Query2OP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Size = 35
    end
    object Query2OP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Size = 35
    end
    object Query2OP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Size = 35
    end
    object Query2OP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Size = 35
    end
    object Query2OP_ADDR3: TStringField
      FieldName = 'OP_ADDR3'
      Size = 35
    end
    object Query2MIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object Query2MIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object Query2MIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object Query2MIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object Query2APPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Size = 30
    end
    object Query2APPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Size = 35
    end
    object Query2DOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object Query2DOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object Query2DOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object Query2DOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object Query2DOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object Query2SUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object Query2DOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object Query2CO_BANK: TStringField
      FieldName = 'CO_BANK'
      Size = 11
    end
    object Query2CO_BANK1: TStringField
      FieldName = 'CO_BANK1'
      Size = 35
    end
    object Query2CO_BANK2: TStringField
      FieldName = 'CO_BANK2'
      Size = 35
    end
    object Query2CO_BANK3: TStringField
      FieldName = 'CO_BANK3'
      Size = 35
    end
    object Query2CO_BANK4: TStringField
      FieldName = 'CO_BANK4'
      Size = 35
    end
    object Query2SPECIAL_DESC: TStringField
      FieldName = 'SPECIAL_DESC'
      Size = 1
    end
    object Query2SPECIAL_DESC_1: TMemoField
      FieldName = 'SPECIAL_DESC_1'
      BlobType = ftMemo
      Size = 1
    end
  end
end
