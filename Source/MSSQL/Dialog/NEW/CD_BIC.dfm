inherited CD_BIC_frm: TCD_BIC_frm
  Left = 1067
  Top = 157
  Caption = ''
  ClientWidth = 636
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 636
    inherited sImage1: TsImage
      Left = 611
    end
  end
  inherited sPanel3: TsPanel
    Width = 636
    inherited sPanel2: TsPanel
      Width = 626
      inherited sImage2: TsImage
        Left = 600
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 626
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'BIC_CD'
          Title.Alignment = taCenter
          Title.Caption = 'BIC CODE'
          Width = 64
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'BIC_NMKR'
          Title.Caption = #51008#54665#47749'('#54620')'
          Width = 164
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'BIC_NMEN'
          Title.Caption = #51008#54665#47749'('#50689')'
          Width = 327
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT * FROM SWIFT_BIC WHERE MAP = 1')
  end
end
