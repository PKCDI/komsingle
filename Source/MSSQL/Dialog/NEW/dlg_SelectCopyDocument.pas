unit dlg_SelectCopyDocument;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, sPanel, StdCtrls, sEdit, sComboBox, sButton, Grids,
  DBGrids, acDBGrid, Buttons, sSpeedButton, DB, ADODB, Mask, sMaskEdit, StrUtils, DateUtils;

type
  Tdlg_SelectCopyDocument_frm = class(TForm)
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    sDBGrid1: TsDBGrid;
    btnExit: TsButton;
    sButton1: TsButton;
    com_find: TsComboBox;
    edt_find: TsEdit;
    sSpeedButton4: TsSpeedButton;
    sButton2: TsButton;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryCopy: TADOQuery;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormDestroy(Sender: TObject);
    procedure edt_findKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
  private
    function getValues(index: integer): Variant;
    { Private declarations }
  protected
    FCaption : String;
    procedure ReadList; virtual; abstract;
  public
    { Public declarations }
    property Values[index : integer] : Variant read getValues;
    function FieldValues(FieldName : String): Variant;
    function FieldString(FieldName : String): string;
    function FieldInt(FieldName : string):Integer;
    function FieldCurr(FieldName : String):Currency;
    function FieldBool(FieldName : String):Boolean;
    function OpenDialog(KeyIndex : Integer;KeyValue : String=''):Boolean; virtual;
  end;

var
  dlg_SelectCopyDocument_frm: Tdlg_SelectCopyDocument_frm;

implementation

uses
  MSSQL, ICON;

{$R *.dfm}

procedure Tdlg_SelectCopyDocument_frm.FormShow(Sender: TObject);
begin
  {$IFDEF DEBUG}
  sMaskEdit1.Text := '20000101';
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD',Now);
  {$ELSE}
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD',Now);
  {$ENDIF}

  ReadList;

  IF FCaption <> '' Then                 
    Self.Caption := FCaption;
end;

function Tdlg_SelectCopyDocument_frm.OpenDialog(KeyIndex: Integer;
  KeyValue: String): Boolean;
begin
  com_find.ItemIndex := KeyIndex;

  IF Trim(KeyValue) <> '' Then
  begin
    edt_find.Clear;
    ReadList;
    qryList.Locate(sDBGrid1.Columns[KeyIndex].FieldName, KeyValue,[]);
  end
  else
    ReadList;

  Result := Self.ShowModal = mrOK;
end;

procedure Tdlg_SelectCopyDocument_frm.sDBGrid1DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $0054CEFC;
      Canvas.Font.Color := clBlack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure Tdlg_SelectCopyDocument_frm.FormDestroy(Sender: TObject);
begin
  IF qryList.Active Then qryList.Close;
end;

procedure Tdlg_SelectCopyDocument_frm.edt_findKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  IF Key = VK_RETURN Then ReadList;
end;

procedure Tdlg_SelectCopyDocument_frm.sDBGrid1DblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure Tdlg_SelectCopyDocument_frm.sButton2Click(Sender: TObject);
begin
  ReadList;
end;

function Tdlg_SelectCopyDocument_frm.getValues(index: integer): Variant;
begin
  IF qryList.Fields[index].IsNull Then
    Result := ''
  else
    Result := qryList.Fields[index].AsVariant;
end;

function Tdlg_SelectCopyDocument_frm.FieldValues(
  FieldName: String): Variant;
begin
  if qryList.FieldByName(FieldName).IsNull Then
    Result := ''
  else
    Result := qryList.FieldByName(FieldName).AsVariant;
end;

function Tdlg_SelectCopyDocument_frm.FieldString(
  FieldName: String): string;
begin
  Result := qryList.FieldByName(FieldName).AsString;
end;

function Tdlg_SelectCopyDocument_frm.FieldInt(FieldName: string): Integer;
begin
  Result := qryList.FieldByName(FieldName).AsInteger;
end;

function Tdlg_SelectCopyDocument_frm.FieldCurr(
  FieldName: String): Currency;
begin
  Result := qryList.FieldByName(FieldName).AsCurrency;
end;


function Tdlg_SelectCopyDocument_frm.FieldBool(FieldName: String): Boolean;
begin
  Result := qryList.FieldByName(FieldName).AsBoolean;
end;

end.
