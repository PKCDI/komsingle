unit dlg_ADDTAX;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, ExtCtrls, sPanel, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, Mask, sMaskEdit, sEdit, StrUtils, DateUtils, DB, ADODB, TypeDefine, Clipbrd;

type
  Tdlg_ADDTAX_frm = class(TForm)
    sPanel1: TsPanel;
    Shape3: TShape;
    sButton1: TsButton;
    sButton2: TsButton;
    edt_SEQ: TsEdit;
    msk_BILL_DATE: TsMaskEdit;
    edt_AMT_UNIT: TsEdit;
    curr_AMT: TsCurrencyEdit;
    edt_TAX_UNIT: TsEdit;
    curr_TAX: TsCurrencyEdit;
    edt_BILL_NO: TsEdit;
  private
    { Private declarations }
    FWorkType : TProgramControlType;
    FKEYY : String;
    FKeyValues: Variant;
    function RunDialog(uFields:TFields):TModalResult;
    function getMAXSEQ:Integer;
    procedure WriteData;
    procedure EditData;
  public
    { Public declarations }
    function WriteTax(sKEY : String):TModalResult;
    function EditTax(uFields : TFields):TModalResult;
    function DelTax(sKEY : string; nSeq : Integer):Boolean;
    property ApplyKeyValues:Variant read FKeyValues;
  end;

var
  dlg_ADDTAX_frm: Tdlg_ADDTAX_frm;

implementation

uses
  MSSQL, ICON, MessageDefine, SQLCreator;

{$R *.dfm}

{ Tdlg_ADDTAX_frm }

function Tdlg_ADDTAX_frm.DelTax(sKEY: string; nSeq: Integer): Boolean;
begin
  Result := False;
  IF MessageBox(Self.Handle, MSG_LOCRCT_DELTAX_QUESTION, '세금계산서 삭제',MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := 'DELETE FROM LOCRC2_TAX WHERE KEYY = '+QuotedStr(sKEY)+' AND SEQ = '+IntToStr(nSeq);
        ExecSQL;
        Result := True;
      Except
        on E:Exception do
        begin
          Result := False;
          ShowMessage(MSG_SYSTEM_DEL_ERR+':'#13#10+E.Message);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;

end;

procedure Tdlg_ADDTAX_frm.EditData;
var
  SC : TSQLCreate;
begin
  with TADOQuery.Create(nil) do
  begin
    SC := TSQLCreate.Create;
    try
      SC.DMLType := dmlUpdate;
      SC.SQLHeader('LOCRC2_TAX');
      SC.ADDWhere('KEYY', FKEYY);
      SC.ADDWhere('SEQ', StrtoInt(edt_SEQ.Text) );
      SC.ADDValue('BILL_NO', edt_BILL_NO.Text);
      SC.ADDValue('BILL_DATE', msk_BILL_DATE.Text);
      SC.ADDValue('BILL_AMOUNT', curr_AMT.Value);
      SC.ADDValue('BILL_AMOUNT_UNIT', 'KRW');
      SC.ADDValue('TAX_AMOUNT', curr_TAX.Value);
      SC.ADDValue('TAX_AMOUNT_UNIT', 'KRW');

      FKeyValues := VarArrayOf([FKEYY, StrtoInt(edt_SEQ.Text)]);

      Connection := DMMssql.KISConnect;
      SQL.Text := SC.CreateSQL;
      Clipboard.AsText := SQl.Text;
      ExecSQL;
    finally
      Close;
      Free;
      SC.Free;
    end;
  end;
end;

procedure Tdlg_ADDTAX_frm.WriteData;
var
  SC : TSQLCreate;
begin
  with TADOQuery.Create(nil) do
  begin
    SC := TSQLCreate.Create;
    try
      SC.DMLType := dmlInsert;
      SC.SQLHeader('LOCRC2_TAX');
      SC.ADDValue('KEYY', FKEYY);
      SC.ADDValue('SEQ', StrtoInt(edt_SEQ.Text) );
      SC.ADDValue('BILL_NO', edt_BILL_NO.Text);
      SC.ADDValue('BILL_DATE', msk_BILL_DATE.Text);
      SC.ADDValue('BILL_AMOUNT', curr_AMT.Value);
      SC.ADDValue('BILL_AMOUNT_UNIT', 'KRW');
      SC.ADDValue('TAX_AMOUNT', curr_TAX.Value);
      SC.ADDValue('TAX_AMOUNT_UNIT', 'KRW');

      FKeyValues := VarArrayOf([FKEYY, StrtoInt(edt_SEQ.Text)]);

      Connection := DMMssql.KISConnect;
      SQL.Text := SC.CreateSQL;
      Clipboard.AsText := SQl.Text;
      ExecSQL;
    finally
      Close;
      Free;
      SC.Free;
    end;
  end;
end;

function Tdlg_ADDTAX_frm.EditTax(uFields: TFields): TModalResult;
begin
  FWorkType := ctModify;
  FKEYY := uFields.FieldByName('KEYY').AsString;
  Self.Caption := '세금계산서 수정 - '+uFields.FieldByName('KEYY').AsString;

  edt_SEQ.Text := uFields.FieldByName('SEQ').AsString;
  msk_BILL_DATE.Text := uFields.FieldByName('BILL_DATE').AsString;
  edt_BILL_NO.Text := uFields.FieldByName('BILL_NO').AsString;
  edt_AMT_UNIT.Text := 'KRW';
  edt_TAX_UNIT.Text := 'KRW';
  curr_AMT.Value := uFields.FieldByName('BILL_AMOUNT').AsCurrency;
  curr_TAX.Value := uFields.FieldByName('TAX_AMOUNT').AsCurrency;

  Result := RunDialog(uFields);
end;

function Tdlg_ADDTAX_frm.getMAXSEQ: Integer;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT ISNULL(MAX(SEQ),0) as MAX_SEQ FROM LOCRC2_TAX WHERE KEYY = '+QuotedStr(FKEYY);
      Open;

      Result := FieldByName('MAX_SEQ').AsInteger+1;

    finally
      Close;
      Free;
    end;
  end;
end;

function Tdlg_ADDTAX_frm.RunDialog(uFields: TFields): TModalResult;
begin
  If Self.ShowModal = mrOK Then
  begin
    Case FWorkType of
      ctInsert : WriteData;
      ctModify : EditData;
    end;
    Result := mrOk;
  end
  else
    Result := mrCancel;
end;

function Tdlg_ADDTAX_frm.WriteTax(sKEY : String): TModalResult;
begin
  FWorkType := ctInsert;
  FKEYY := sKEY;
  Self.Caption := '세금계산서 등록 - '+FKEYY;

  edt_SEQ.Text := IntToStr(getMAXSEQ);
  msk_BILL_DATE.Text := FormatDateTime('YYYYMMDD', Now);
  edt_BILL_NO.Clear;
  edt_AMT_UNIT.Text := 'KRW';
  edt_TAX_UNIT.Text := 'KRW';
  curr_AMT.Value := 0;
  curr_TAX.Value := 0;

  Result := RunDialog(nil);  
end;

end.
