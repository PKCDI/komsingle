inherited CD_BANK_frm: TCD_BANK_frm
  Left = 951
  Top = 153
  Caption = #51008#54665#49440#53469
  ClientHeight = 555
  ClientWidth = 610
  OldCreateOrder = True
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 610
    inherited sImage1: TsImage
      Left = 585
    end
  end
  inherited sPanel3: TsPanel
    Width = 610
    Height = 530
    inherited sPanel2: TsPanel
      Width = 600
      inherited sImage2: TsImage
        Left = 574
      end
      inherited sComboBox1: TsComboBox
        Text = #51008#54665#53076#46300
        Items.Strings = (
          #51008#54665#53076#46300
          #51008#54665#47749
          #51648#51216#47749)
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 600
      Height = 486
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #51008#54665#53076#46300
          Width = 74
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'BankName1'
          Title.Alignment = taCenter
          Title.Caption = #51008#54665#47749
          Width = 250
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'BankBranch1'
          Title.Alignment = taCenter
          Title.Caption = #51648#51216#47749
          Width = 250
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      
        'SELECT [CODE]  ,[ENAME1] as BankName1 ,[ENAME3] as BankBranch1, ' +
        '[ENAME2], [ENAME4] FROM BANKCODE')
    object qryListCODE: TStringField
      FieldName = 'CODE'
      Size = 10
    end
    object qryListBankName1: TStringField
      FieldName = 'BankName1'
      Size = 35
    end
    object qryListBankBranch1: TStringField
      FieldName = 'BankBranch1'
      Size = 35
    end
    object qryListENAME2: TStringField
      FieldName = 'ENAME2'
      Size = 35
    end
    object qryListENAME4: TStringField
      FieldName = 'ENAME4'
      Size = 35
    end
  end
end
