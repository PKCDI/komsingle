inherited CD_BYEONDONG_frm: TCD_BYEONDONG_frm
  Caption = 'CD_BYEONDONG_frm'
  ClientHeight = 294
  ClientWidth = 384
  OldCreateOrder = True
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 384
    inherited sImage1: TsImage
      Left = 361
    end
  end
  inherited sPanel3: TsPanel
    Width = 384
    Height = 269
    inherited sPanel2: TsPanel
      Width = 374
      inherited sImage2: TsImage
        Left = 350
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 374
      Height = 225
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = #47569#51008' '#44256#46357
          Title.Font.Style = [fsBold]
          Width = 64
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Alignment = taCenter
          Title.Caption = #48320#46041#44396#48516
          Width = 262
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39#48320#46041#44396#48516#39)
  end
end
