inherited CD_CUST_frm: TCD_CUST_frm
  Left = 894
  Top = 164
  Caption = 'CD_CUST_frm'
  OldCreateOrder = True
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    inherited sLabel1: TsLabel
      Width = 6
      Caption = ''
    end
  end
  inherited sPanel3: TsPanel
    inherited sPanel2: TsPanel
      inherited sSpeedButton1: TsSpeedButton
        Left = 336
      end
      inherited sImage2: TsImage
        Reflected = False
      end
      inherited sEdit1: TsEdit
        Left = 114
      end
      inherited sComboBox1: TsComboBox
        Width = 113
        ItemIndex = 2
        Text = #49324#50629#51088#47749
        Items.Strings = (
          #53076#46300
          #49324#50629#51088#46321#47197#48264#54840
          #49324#50629#51088#47749
          #45824#54364#51088)
      end
      inherited sButton1: TsButton
        Left = 260
      end
    end
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 50
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'SAUP_NO'
          Title.Alignment = taCenter
          Title.Caption = #49324#50629#51088#46321#47197#48264#54840
          Width = 92
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'ENAME'
          Title.Caption = #49324#50629#51088#47749
          Width = 219
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'REP_NAME'
          Title.Alignment = taCenter
          Title.Caption = #45824#54364#51088
          Width = 100
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      
        'SELECT  CODE, TRAD_NO, SAUP_NO, ENAME, REP_NAME, REP_TEL, REP_FA' +
        'X, DDAN_DEPT, DDAN_NAME, DDAN_TEL, DDAN_FAX, ZIPCD, ADDR1, ADDR2' +
        ', ADDR3, CITY, NAT, ESU1, ESU2, ESU3, Jenja, ADDR4, ADDR5, EMAIL' +
        '_ID, EMAIL_DOMAIN'
      'FROM CUSTOM')
  end
end
