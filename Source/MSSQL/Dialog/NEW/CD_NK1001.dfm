inherited CD_NK1001_frm: TCD_NK1001_frm
  Left = 1213
  Top = 284
  Caption = 'CD_NK1001_frm'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 64
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #44060#49444#44540#44144#49436#47448' '#51333#47448#47749
          Width = 397
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME,Prefix'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39#45236#44397#49888'1001'#39
      'AND Remark = 1')
  end
end
