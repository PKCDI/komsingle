object dlg_ADDTAX_frm: Tdlg_ADDTAX_frm
  Left = 1026
  Top = 341
  BorderIcons = []
  BorderStyle = bsSingle
  BorderWidth = 4
  Caption = #49464#44552#44228#49328#49436' '#51077#47141'/'#49688#51221
  ClientHeight = 252
  ClientWidth = 389
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 389
    Height = 252
    Align = alClient
    
    TabOrder = 0
    object Shape3: TShape
      Left = 9
      Top = 195
      Width = 371
      Height = 1
      Brush.Color = 14540252
      Pen.Color = 14540252
    end
    object sButton1: TsButton
      Left = 107
      Top = 205
      Width = 109
      Height = 40
      Caption = #51200#51109
      ModalResult = 1
      TabOrder = 5
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 0
    end
    object sButton2: TsButton
      Left = 219
      Top = 205
      Width = 64
      Height = 40
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 6
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
    end
    object edt_SEQ: TsEdit
      Left = 104
      Top = 16
      Width = 41
      Height = 23
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      Text = '1'
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = #49692#48264
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
    object msk_BILL_DATE: TsMaskEdit
      Left = 104
      Top = 43
      Width = 76
      Height = 23
      AutoSize = False
      Color = clWhite
      Ctl3D = True
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #51089#49457#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object edt_AMT_UNIT: TsEdit
      Left = 104
      Top = 97
      Width = 37
      Height = 23
      HelpContext = 1
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 7
      Text = 'KRW'
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44277#44553#44032#50529
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object curr_AMT: TsCurrencyEdit
      Left = 142
      Top = 97
      Width = 173
      Height = 23
      AutoSelect = False
      AutoSize = False
      CharCase = ecUpperCase
      TabOrder = 3
      SkinData.SkinSection = 'EDIT'
      DisplayFormat = '#,0.##;0;'
    end
    object edt_TAX_UNIT: TsEdit
      Left = 104
      Top = 124
      Width = 37
      Height = 23
      HelpContext = 1
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 8
      Text = 'KRW'
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49464#50529
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object curr_TAX: TsCurrencyEdit
      Left = 142
      Top = 124
      Width = 173
      Height = 23
      AutoSelect = False
      AutoSize = False
      CharCase = ecUpperCase
      TabOrder = 4
      SkinData.SkinSection = 'EDIT'
      DisplayFormat = '#,0.##;0;'
    end
    object edt_BILL_NO: TsEdit
      Left = 104
      Top = 70
      Width = 211
      Height = 23
      Color = 10813439
      TabOrder = 2
      Text = '20170306-10000000-00078002'
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = #49464#44552#44228#49328#49436#48264#54840
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
    end
  end
end
