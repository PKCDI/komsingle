inherited CD_LC_GB_frm: TCD_LC_GB_frm
  Left = 781
  Top = 310
  Caption = 'CD_LC_GB_frm'
  ClientHeight = 227
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    Height = 202
    inherited sDBGrid1: TsDBGrid
      Height = 158
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 64
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #47928#49436#44396#48516
          Width = 372
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      
        'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'LC'#44396#48516#39' AND Remark' +
        ' = 1 ORDER BY CODE')
  end
end
