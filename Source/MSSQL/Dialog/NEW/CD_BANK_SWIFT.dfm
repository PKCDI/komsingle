inherited CD_BANK_SWIFT_frm: TCD_BANK_SWIFT_frm
  Left = 1002
  Caption = #54644#50808#51008#54665#53076#46300' '#49440#53469
  ClientHeight = 447
  ClientWidth = 609
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 609
    inherited sImage1: TsImage
      Left = 586
    end
  end
  inherited sPanel3: TsPanel
    Width = 609
    Height = 422
    inherited sPanel2: TsPanel
      Width = 599
      inherited sImage2: TsImage
        Left = 575
      end
      inherited sComboBox1: TsComboBox
        Text = 'SWIFT CODE'
        Items.Strings = (
          'SWIFT CODE'
          #51008#54665#47749)
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 599
      Height = 378
      DefaultRowHeight = 19
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'BANK_CD'
          Title.Alignment = taCenter
          Title.Caption = 'SWIFT CODE'
          Width = 95
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'BANK_NM1'
          Title.Caption = #51008#54665#47749
          Width = 160
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'ACCT_NO'
          Title.Caption = #44228#51340#48264#54840
          Width = 170
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'ACCT_NM'
          Title.Caption = #49688#52712#51064
          Width = 145
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT [BANK_CD]'
      '      ,[BANK_NM1]'
      '      ,[BANK_NM2]'
      '      ,[BANK_BNCH_NM1]'
      '      ,[BANK_BNCH_NM2]'
      '      ,[ACCT_NO]'
      '      ,[ACCT_NM]'
      '      ,[BANK_NAT_CD]'
      '      ,[IS_USE]'
      '      ,[BANK_UNIT]'
      '  FROM BANKCODE_SWIFT')
    Top = 112
    object qryListBANK_CD: TStringField
      FieldName = 'BANK_CD'
      Size = 11
    end
    object qryListBANK_NM1: TStringField
      FieldName = 'BANK_NM1'
      Size = 35
    end
    object qryListBANK_NM2: TStringField
      FieldName = 'BANK_NM2'
      Size = 35
    end
    object qryListBANK_BNCH_NM1: TStringField
      FieldName = 'BANK_BNCH_NM1'
      Size = 35
    end
    object qryListBANK_BNCH_NM2: TStringField
      FieldName = 'BANK_BNCH_NM2'
      Size = 35
    end
    object qryListACCT_NM: TStringField
      FieldName = 'ACCT_NM'
      Size = 35
    end
    object qryListBANK_NAT_CD: TStringField
      FieldName = 'BANK_NAT_CD'
      Size = 2
    end
    object qryListIS_USE: TBooleanField
      FieldName = 'IS_USE'
    end
    object qryListBANK_UNIT: TStringField
      FieldName = 'BANK_UNIT'
      Size = 3
    end
  end
  inherited dsList: TDataSource
    Top = 112
  end
end
