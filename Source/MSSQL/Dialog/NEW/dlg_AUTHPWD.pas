unit dlg_AUTHPWD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, sButton, sEdit, sLabel, sPanel;

type
  Tdlg_AUTHPWD_frm = class(TForm)
    sPanel1: TsPanel;
    sLabel1: TsLabel;
    sEdit1: TsEdit;
    sButton1: TsButton;
    sButton2: TsButton;
    Shape1: TShape;
    procedure sButton1Click(Sender: TObject);
    procedure sEdit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlg_AUTHPWD_frm: Tdlg_AUTHPWD_frm;

implementation

{$R *.dfm}

procedure Tdlg_AUTHPWD_frm.sButton1Click(Sender: TObject);
begin
  IF UpperCase( sEdit1.Text ) = 'KIS2000' Then
    ModalResult := mrOk
  else
    MessageBox(Self.Handle,'Invalid authentication code','Error',MB_OK+MB_ICONERROR);
end;

procedure Tdlg_AUTHPWD_frm.sEdit1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF Key = VK_RETURN THEN sButton1Click(sButton1);
end;

procedure Tdlg_AUTHPWD_frm.FormShow(Sender: TObject);
begin
  sEdit1.Clear;
end;

end.
