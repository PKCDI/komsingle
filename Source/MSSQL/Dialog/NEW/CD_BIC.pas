unit CD_BIC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_BIC_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
  public
    function GetCodeText(value: String): String; override;
    { Public declarations }
  end;

var
  CD_BIC_frm: TCD_BIC_frm;

implementation

{$R *.dfm}
Const
  sSQL : String = 'SELECT BIC_CD, BIC_NMKR, BIC_NMEN FROM SWIFT_BIC WHERE MAP = 1';
procedure TCD_BIC_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '���� BIC�ڵ�';
end;

function TCD_BIC_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := sSQL+' AND BIC_CD = '+QuotedStr(value);
    Open;
    IF RecordCount > 0 Then
      Result := FieldByName('BIC_NMKR').AsString;
  end;
end;

procedure TCD_BIC_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := sSQL;
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

end.
