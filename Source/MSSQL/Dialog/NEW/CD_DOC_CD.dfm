inherited CD_DOC_CD_frm: TCD_DOC_CD_frm
  Caption = 'CD_DOC_CD_frm'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 60
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #54637#47785
          Width = 403
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT * FROM CODE2NDD WHERE Prefix = '#39'DOC_CD'#39' AND Remark = 1')
    Left = 16
    Top = 176
  end
  inherited dsList: TDataSource
    Left = 48
    Top = 176
  end
end
