unit CD_4383;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_4383_frm = class(TCodeDialogParent_frm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ReadList; override;

  public
    { Public declarations }
    function GetCodeText(value: String): String; override;
  end;

var
  CD_4383_frm: TCD_4383_frm;

implementation

{$R *.dfm}

{ TCodeDialogParent_frm1 }

function TCD_4383_frm.GetCodeText(value: String): String;
begin
  Result := '';
  with qrylist do
  begin
    Close;
    SQL.Text := 'SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ''4383''';
    SQL.Add('AND CODE = '+QuotedStr(value));
    try
      Open;
      IF RecordCount > 0 Then
        Result := FieldByName('NAME').AsString;
    finally
      Close;
    end;
  end;
end;

procedure TCD_4383_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT CODE, LTRIM(NAME) as NAME FROM CODE2NDD WHERE Prefix = ''4383'' AND Remark = 1';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('AND '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

procedure TCD_4383_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '납부수수료 유형';
end;

end.
 