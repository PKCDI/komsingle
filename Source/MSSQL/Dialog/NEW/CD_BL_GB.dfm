inherited CD_BL_GB_frm: TCD_BL_GB_frm
  Caption = 'CD_BL_GB_frm'
  ClientHeight = 201
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    Height = 176
    inherited sDBGrid1: TsDBGrid
      Height = 132
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 64
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #47928#49436#44396#48516
          Width = 372
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      
        'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39#49440#54616#51613#44428#39' AND Remark' +
        ' = 1 ORDER BY CODE')
  end
end
