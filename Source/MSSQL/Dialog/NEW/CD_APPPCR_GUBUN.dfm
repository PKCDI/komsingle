inherited CD_APPPCR_GUBUN_frm: TCD_APPPCR_GUBUN_frm
  Caption = 'CD_APPPCR_GUBUN_frm'
  ClientHeight = 203
  ClientWidth = 476
  OldCreateOrder = True
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 476
    inherited sImage1: TsImage
      Left = 453
    end
  end
  inherited sPanel3: TsPanel
    Width = 476
    Height = 178
    inherited sPanel2: TsPanel
      Width = 466
      inherited sImage2: TsImage
        Left = 442
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 466
      Height = 134
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clWindowText
          Title.Font.Height = -12
          Title.Font.Name = #47569#51008' '#44256#46357
          Title.Font.Style = [fsBold]
          Width = 88
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Alignment = taCenter
          Title.Caption = #47928#49436#44396#48516
          Width = 354
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'APPPCR'#44396#48516#39)
  end
end
