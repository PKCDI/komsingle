unit CD_ITEMLIST;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CodeDialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sComboBox, sEdit, Buttons, sSpeedButton, sLabel, ExtCtrls,
  acImage, sPanel;

type
  TCD_ITEMLIST_frm = class(TCodeDialogParent_frm)
    qryListCODE: TStringField;
    qryListHSCODE: TStringField;
    qryListFNAME: TMemoField;
    qryListSNAME: TStringField;
    qryListMSIZE: TMemoField;
    qryListQTYC: TStringField;
    qryListPRICEG: TStringField;
    qryListPRICE: TBCDField;
    qryListQTY_U: TBCDField;
    qryListQTY_UG: TStringField;
    procedure qryListFNAMEGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure ReadList; override;
    { Private declarations }
    
  public
    { Public declarations }
  end;

var
  CD_ITEMLIST_frm: TCD_ITEMLIST_frm;

implementation

{$R *.dfm}

procedure TCD_ITEMLIST_frm.qryListFNAMEGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
end;

procedure TCD_ITEMLIST_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FCaption := '��ǰ����';
end;

procedure TCD_ITEMLIST_frm.ReadList;
begin
  inherited;
  with qryList do
  Begin
    Close;
    SQL.Text := 'SELECT * FROM ITEM_CODE';
    IF (sEdit1.Text <> '') Then
    begin
      SQL.Add('WHERE '+sDBGrid1.Columns[sCombobox1.ItemIndex].FieldName+' LIKE '+QuotedStr('%'+sEdit1.Text+'%'));
    end;
    Open;
  end;
end;

end.
