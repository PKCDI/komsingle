inherited dlg_CopyLOCAMRfromLOCAMA_frm: Tdlg_CopyLOCAMRfromLOCAMA_frm
  Caption = '[LOCAMA]'#45236#44397#49888#50857#51109' '#51312#44148#48320#44221' '#53685#51648#49436
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    inherited sPanel2: TsPanel
      inherited com_find: TsComboBox
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840
          #45236#44397#49888#50857#51109#48264#54840)
      end
    end
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 200
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MSEQ'
          Title.Alignment = taCenter
          Title.Caption = #49692#48264
          Width = 40
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'USER_ID'
          Title.Alignment = taCenter
          Title.Caption = #49324#50857#51088
          Width = 60
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 82
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LC_NO'
          Title.Alignment = taCenter
          Title.Caption = #45236#44397#49888#50857#51109#48264#54840
          Width = 210
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'APPLIC1'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#51032#47280#51064
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC1'
          Title.Alignment = taCenter
          Title.Caption = #49688#54812#51088
          Width = 200
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'ISS_DATE'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#51068#51088
          Width = 82
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'AMD_DATE'
          Title.Alignment = taCenter
          Title.Caption = #48320#44221#49888#52397#51068#51088
          Width = 82
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC2AMT'
          Title.Alignment = taCenter
          Title.Caption = #44552#50529'('#50896#54868')'
          Width = 100
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'LOC2AMTC'
          Title.Alignment = taCenter
          Title.Caption = #45800#50948
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOC1AMT'
          Title.Alignment = taCenter
          Title.Caption = #44552#50529'('#50808#54868')'
          Width = 100
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'LOC1AMTC'
          Title.Alignment = taCenter
          Title.Caption = #45800#50948
          Width = 60
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180731'
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, MSEQ, AMD_NO, [USER_ID], DATEE, BGM_REF, MESSAG' +
        'E1, MESSAGE2, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFFERNO3, OFFER' +
        'NO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO9, AMD_DATE,' +
        ' ADV_DATE, ISS_DATE, REMARK, REMARK1, ISSBANK, ISSBANK1, ISSBANK' +
        '2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNAME1' +
        ', EXNAME2, EXNAME3, DELIVERY, EXPIRY, CHGINFO, CHGINFO1, LOC_TYP' +
        'E, LOC1AMT, LOC1AMTC, LOC2AMT, LOC2AMTC, EX_RATE, CHK1, CHK2, CH' +
        'K3, PRNO, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFA' +
        'DDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM,'
      '        ISNULL(N4487.DOC_NAME,'#39#39') as LOC_TYPENAME'
      
        'FROM [LOCAMA] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAMA.LOC_TYPE = N4487.CODE'
      'WHERE DATEE BETWEEN :FDATE AND :TDATE')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryListAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListBGM_REF: TStringField
      FieldName = 'BGM_REF'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListAMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      Size = 8
    end
    object qryListADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      Size = 8
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListISSBANK: TStringField
      FieldName = 'ISSBANK'
      Size = 4
    end
    object qryListISSBANK1: TStringField
      FieldName = 'ISSBANK1'
      Size = 35
    end
    object qryListISSBANK2: TStringField
      FieldName = 'ISSBANK2'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListCHGINFO: TStringField
      FieldName = 'CHGINFO'
      Size = 1
    end
    object qryListCHGINFO1: TMemoField
      FieldName = 'CHGINFO1'
      BlobType = ftMemo
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qryListLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object qryListLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qryListLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object qryListEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      ReadOnly = True
      Size = 100
    end
  end
  inherited qryCopy: TADOQuery
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, MSEQ, AMD_NO, [USER_ID], DATEE, BGM_REF, MESSAG' +
        'E1, MESSAGE2, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFFERNO3, OFFER' +
        'NO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO9, AMD_DATE,' +
        ' ADV_DATE, ISS_DATE, REMARK, REMARK1, ISSBANK, ISSBANK1, ISSBANK' +
        '2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNAME1' +
        ', EXNAME2, EXNAME3, DELIVERY, EXPIRY, CHGINFO, CHGINFO1, LOC_TYP' +
        'E, LOC1AMT, LOC1AMTC, LOC2AMT, LOC2AMTC, EX_RATE, CHK1, CHK2, CH' +
        'K3, PRNO, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFA' +
        'DDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM,'
      '        ISNULL(N4487.DOC_NAME,'#39#39') as LOC_TYPENAME'
      
        'FROM [LOCAMA] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAMA.LOC_TYPE = N4487.CODE'
      'WHERE ADV_DATE BETWEEN :FDATE AND :TDATE')
  end
end
