inherited CD_APPLOG_GB_frm: TCD_APPLOG_GB_frm
  Caption = 'CD_APPLOG_GB_frm'
  ClientHeight = 240
  ClientWidth = 522
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 522
    inherited sImage1: TsImage
      Left = 499
    end
  end
  inherited sPanel3: TsPanel
    Width = 522
    Height = 215
    inherited sPanel2: TsPanel
      Width = 512
      inherited sImage2: TsImage
        Left = 488
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 512
      Height = 171
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 75
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #54637#47785
          Width = 413
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      
        'SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'APPLOG'#44396#48516#39' AND CO' +
        'DE <> '#39#39' AND Remark = 1 ORDER BY CODE')
  end
end
