inherited CD_APPCOP_JIGEUP_frm: TCD_APPCOP_JIGEUP_frm
  Left = 1277
  Top = 175
  Caption = 'CD_APPCOP_JIGEUP_frm'
  ClientHeight = 259
  ClientWidth = 426
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 426
    inherited sImage1: TsImage
      Left = 403
    end
  end
  inherited sPanel3: TsPanel
    Width = 426
    Height = 234
    inherited sPanel2: TsPanel
      Width = 416
      inherited sImage2: TsImage
        Left = 392
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 416
      Height = 190
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 50
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Alignment = taCenter
          Title.Caption = #51648#44553#44396#48516
          Width = 342
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME FROM CODE2NDD where Prefix = '#39'APPCOP'#51648#44553#39)
  end
end
