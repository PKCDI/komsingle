inherited dlg_CopyLOCAPPfromLOCAPP_frm: Tdlg_CopyLOCAPPfromLOCAPP_frm
  Left = 575
  Top = 279
  Caption = '[LOCAPP] '#45236#44397#49888#50857#51109' '#48373#49324' - '#50896#48376#45936#51060#53552' '#49440#53469
  ClientWidth = 1038
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 1038
    inherited sPanel2: TsPanel
      Width = 1036
      inherited btnExit: TsButton
        Left = 965
        Top = 9
        Anchors = [akTop, akRight]
      end
      inherited sButton1: TsButton
        Left = 892
        Top = 9
        Anchors = [akTop, akRight]
      end
      inherited com_find: TsComboBox
        Text = #44288#47532#48264#54840
        Items.Strings = (
          #44288#47532#48264#54840)
      end
      inherited sMaskEdit1: TsMaskEdit
        BoundLabel.Caption = #44060#49444#49888#52397#51068#51088
      end
    end
    inherited sDBGrid1: TsDBGrid
      Width = 1036
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 220
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'APP_DATE'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#49888#52397#51068#51088
          Width = 79
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'AP_BANK'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#51008#54665
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AP_BANK1'
          Title.Caption = #51008#54665#47749
          Width = 170
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AP_BANK2'
          Title.Caption = #51648#51216#47749
          Width = 170
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BENEFC1'
          Title.Caption = #49688#54812#51088
          Width = 213
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'USER_ID'
          Title.Alignment = taCenter
          Title.Caption = #50976#51200
          Width = 38
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'OPEN_NO'
          Title.Alignment = taCenter
          Title.Caption = #44060#49444#54924#52264
          Width = 52
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20181231'
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO,SUBSTRING(MAINT_NO,16,100) as MAINT_NO2, DATEE, ' +
        'USER_ID, MESSAGE1, MESSAGE2, '
      'BUSINESS, '
      'N4025.DOC_NAME as BUSINESSNAME,'
      
        'OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFE' +
        'RNO7, OFFERNO8, OFFERNO9, OPEN_NO, APP_DATE, DOC_PRD, DELIVERY, ' +
        'EXPIRY,'
      'TRANSPRT, '
      'PSHIP.DOC_NAME as TRANSPRTNAME,'
      
        'GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_BANK2,' +
        ' APPLIC, APPLIC1, APPLIC2, APPLIC3, BENEFC, BENEFC1, BENEFC2, BE' +
        'NEFC3, EXNAME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, ' +
        'DOCCOPY4, DOCCOPY5, DOC_ETC, DOC_ETC1,'
      'LOC_TYPE, '
      'N4487.DOC_NAME as LOC_TYPENAME,'
      'LOC_AMT, LOC_AMTC, '
      'DOC_DTL,'
      'N1001.DOC_NAME as DOC_DTLNAME,'
      
        'DOC_NO, DOC_AMT, DOC_AMTC, LOADDATE, EXPDATE, IM_NAME, IM_NAME1,' +
        ' IM_NAME2, IM_NAME3, DEST,NAT.DOC_NAME as DESTNAME, ISBANK1, ISB' +
        'ANK2,'
      'PAYMENT,'
      'N4277.DOC_NAME as PAYMENTNAME,'
      
        'EXGOOD, EXGOOD1, CHKNAME1, CHKNAME2, CHK1, CHK2, CHK3, PRNO, LCN' +
        'O, BSN_HSCODE, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2,' +
        ' BNFADDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      
        'FROM [LOCAPP] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAPP.LOC_TYPE = N4487.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON LOCAPP.BUSINESS =' +
        ' N4025.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOCAPP.TRANSPRT = P' +
        'SHIP.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON LOCAPP.DOC_DTL = ' +
        'N1001.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON LOCAPP.PAYMENT = ' +
        'N4277.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCAPP.DEST = NAT.CODE'
      'WHERE (APP_DATE BETWEEN :FDATE AND :TDATE)')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMAINT_NO2: TStringField
      FieldName = 'MAINT_NO2'
      ReadOnly = True
      Size = 35
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListGOODDES1: TStringField
      FieldName = 'GOODDES1'
      Size = 350
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TStringField
      FieldName = 'REMARK1'
      Size = 350
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListDOC_ETC1: TStringField
      FieldName = 'DOC_ETC1'
      Size = 350
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object qryListLOC_AMT: TBCDField
      FieldName = 'LOC_AMT'
      Precision = 18
    end
    object qryListLOC_AMTC: TStringField
      FieldName = 'LOC_AMTC'
      Size = 3
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_DTLNAME: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryListDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryListIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryListIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryListIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListPAYMENTNAME: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListEXGOOD1: TStringField
      FieldName = 'EXGOOD1'
      Size = 350
    end
    object qryListCHKNAME1: TStringField
      FieldName = 'CHKNAME1'
      Size = 35
    end
    object qryListCHKNAME2: TStringField
      FieldName = 'CHKNAME2'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListLCNO: TStringField
      FieldName = 'LCNO'
      Size = 35
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
  end
  inherited qryCopy: TADOQuery
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO,SUBSTRING(MAINT_NO,16,100) as MAINT_NO2, DATEE, ' +
        'USER_ID, MESSAGE1, MESSAGE2, '
      'BUSINESS, '
      'N4025.DOC_NAME as BUSINESSNAME,'
      
        'OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFE' +
        'RNO7, OFFERNO8, OFFERNO9, OPEN_NO, APP_DATE, DOC_PRD, DELIVERY, ' +
        'EXPIRY,'
      'TRANSPRT, '
      'PSHIP.DOC_NAME as TRANSPRTNAME,'
      
        'GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_BANK2,' +
        ' APPLIC, APPLIC1, APPLIC2, APPLIC3, BENEFC, BENEFC1, BENEFC2, BE' +
        'NEFC3, EXNAME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, ' +
        'DOCCOPY4, DOCCOPY5, DOC_ETC, DOC_ETC1,'
      'LOC_TYPE, '
      'N4487.DOC_NAME as LOC_TYPENAME,'
      'LOC_AMT, LOC_AMTC, '
      'DOC_DTL,'
      'N1001.DOC_NAME as DOC_DTLNAME,'
      
        'DOC_NO, DOC_AMT, DOC_AMTC, LOADDATE, EXPDATE, IM_NAME, IM_NAME1,' +
        ' IM_NAME2, IM_NAME3, DEST,NAT.DOC_NAME as DESTNAME, ISBANK1, ISB' +
        'ANK2,'
      'PAYMENT,'
      'N4277.DOC_NAME as PAYMENTNAME,'
      
        'EXGOOD, EXGOOD1, CHKNAME1, CHKNAME2, CHK1, CHK2, CHK3, PRNO, LCN' +
        'O, BSN_HSCODE, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2,' +
        ' BNFADDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      
        'FROM [LOCAPP] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAPP.LOC_TYPE = N4487.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON LOCAPP.BUSINESS =' +
        ' N4025.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOCAPP.TRANSPRT = P' +
        'SHIP.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON LOCAPP.DOC_DTL = ' +
        'N1001.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON LOCAPP.PAYMENT = ' +
        'N4277.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCAPP.DEST = NAT.CODE'
      'WHERE (APP_DATE BETWEEN :FDATE AND :TDATE)')
  end
end
