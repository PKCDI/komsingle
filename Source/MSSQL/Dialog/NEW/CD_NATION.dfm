inherited CD_NATION_frm: TCD_NATION_frm
  Caption = 'CD_NATION_frm'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel3: TsPanel
    inherited sPanel2: TsPanel
      inherited sComboBox1: TsComboBox
        Text = #44397#44032#53076#46300
        Items.Strings = (
          #44397#44032#53076#46300
          #44397#44032#47749)
      end
    end
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'CODE'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44397#44032#53076#46300
          Width = 64
          Visible = True
        end
        item
          Color = clWhite
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #44397#44032#47749
          Width = 375
          Visible = True
        end>
    end
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    SQL.Strings = (
      'SELECT CODE, NAME FROM CODE2NDD'
      'WHERE Prefix = '#39#44397#44032#39' AND Remark = 1')
  end
end
