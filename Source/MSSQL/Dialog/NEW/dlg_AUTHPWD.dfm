object dlg_AUTHPWD_frm: Tdlg_AUTHPWD_frm
  Left = 1344
  Top = 105
  BorderIcons = []
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = 'Authentication'
  ClientHeight = 132
  ClientWidth = 378
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 378
    Height = 132
    Align = alClient
    
    TabOrder = 0
    object sLabel1: TsLabel
      Left = 16
      Top = 24
      Width = 164
      Height = 15
      Caption = 'Enter Authentication Password'
    end
    object Shape1: TShape
      Left = 16
      Top = 71
      Width = 345
      Height = 1
      Brush.Color = 14540252
      Pen.Color = 14540252
    end
    object sEdit1: TsEdit
      Left = 16
      Top = 40
      Width = 345
      Height = 23
      PasswordChar = '9'
      TabOrder = 0
      OnKeyUp = sEdit1KeyUp
    end
    object sButton1: TsButton
      Left = 112
      Top = 80
      Width = 75
      Height = 32
      Caption = 'OK'
      TabOrder = 1
      OnClick = sButton1Click
    end
    object sButton2: TsButton
      Left = 191
      Top = 80
      Width = 75
      Height = 32
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 2
    end
  end
end
