unit dlgUserAdmin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, ExtCtrls, sLabel, sCheckBox, sEdit, DB,
  ADODB, Grids, DBGrids, acDBGrid, sPanel;

type
  TdlgUserAdmin_frm = class(TForm)
    sPanel1: TsPanel;
    sDBGrid1: TsDBGrid;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    sEdit1: TsEdit;
    sEdit2: TsEdit;
    sEdit3: TsEdit;
    sCheckBox1: TsCheckBox;
    sEdit4: TsEdit;
    sLabel1: TsLabel;
    Shape1: TShape;
    sButton1: TsButton;
    sButton2: TsButton;
    sButton3: TsButton;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sButton4: TsButton;
    sLabel5: TsLabel;
    procedure sButton3Click(Sender: TObject);
    procedure ADOQuery1AfterScroll(DataSet: TDataSet);
    procedure ADOQuery1AfterOpen(DataSet: TDataSet);
    procedure sButton1Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
  private
    { Private declarations }
    procedure readData;
    procedure setData;
  public
    { Public declarations }
  end;

var
  dlgUserAdmin_frm: TdlgUserAdmin_frm;

implementation

uses
  MSSQL, ICON, KISEncryption, SQLCreator;

{$R *.dfm}

{ TdlgUserAdmin_frm }

procedure TdlgUserAdmin_frm.readData;
begin
  with ADOQuery1 do
  begin
    Close;
    Open;
  end;
end;

procedure TdlgUserAdmin_frm.setData;
begin
  with ADOQuery1 do
  begin
    sEdit1.Text := FieldByName('USERID').AsString;
    sEdit4.Clear;
    sEdit2.Text := FieldByName('NAME').AsString;
    sEdit3.Text := FieldByName('DEPT').AsString;
//    sCheckBox1.Checked := FieldByName('ACTIVATE').AsBoolean;
  end;
end;

procedure TdlgUserAdmin_frm.sButton3Click(Sender: TObject);
begin
  sEdit1.ReadOnly := false;
  sEdit1.Color := clWhite;
  sEdit1.Clear;

  sEdit4.Clear;
  sEdit2.Clear;
  sEdit3.Clear;

  sCheckBox1.Checked := True;

  sButton4.Enabled := False;

  sDBGrid1.Enabled := False;
end;

procedure TdlgUserAdmin_frm.ADOQuery1AfterScroll(DataSet: TDataSet);
begin
  setData;
end;

procedure TdlgUserAdmin_frm.ADOQuery1AfterOpen(DataSet: TDataSet);
begin
  if DataSet.RecordCount = 0 Then ADOQuery1AfterScroll(DataSet);
end;

procedure TdlgUserAdmin_frm.sButton1Click(Sender: TObject);
var
  sc : TSQLCreate;
  KISEN : TKISEncryption;
begin
  IF not sDBGrid1.Enabled Then
  begin
    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := 'SELECT * FROM PERSON WHERE USERID = '+QuotedStr(sEdit1.Text);
        Open;

        if recordCount > 0 Then
        begin
          sEdit1.SetFocus;
          MessageBox(self.Handle, PChar('유저아이디['+sEdit1.Text+']는 이미 존재합니다'), '아이디중복', MB_OK+MB_ICONERROR);
          Exit;
        end;
      finally
        Close;
        Free;
      end;
    end;
  end;

  // 수정모드
  try
    sc := TSQLCreate.Create;
    KISEN := TKISEncryption.Create;
    IF sDBGrid1.Enabled Then
    begin
      sc.DMLType := dmlUpdate;
      sc.SQLHeader('PERSON');
      sc.ADDWhere('USERID', sEdit1.Text);
      sc.ADDValue('NAME', sEdit2.Text);
      sc.ADDValue('DEPT', sEdit3.Text);
//      sc.ADDValue('ACTIVATE', sCheckBox1.Checked);
    end
    else
    // 추가모드
    begin
      sc.DMLType := dmlInsert;
      sc.SQLHeader('PERSON');
      sc.ADDValue('PASSWORD', KISEN.SHA256_EN(sEdit4.Text));
      sc.ADDValue('USERID', sEdit1.Text);
      sc.ADDValue('NAME', sEdit2.Text);
      sc.ADDValue('DEPT', sEdit3.Text);
//      sc.ADDValue('ACTIVATE', sCheckBox1.Checked);      
    end;

    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := sc.CreateSQL;
        ShowMessage(SQL.Text);
        ExecSQL;

        readData;

        if sc.DMLType = dmlInsert Then
          ShowMessage('유저추가를 완료하였습니다')
        else
        if sc.DMLType = dmlUpdate Then
          ShowMessage('유저수정을 완료하였습니다');

      finally
        Close;
        Free;
      end;
    end;
  finally
    sc.Free;
    KISEN.Free;
  end;


  sButton4.Enabled := True;
  sDBGrid1.Enabled := True;

  sEdit1.ReadOnly := True;
  sEdit1.Color := clBtnFace;
end;

procedure TdlgUserAdmin_frm.sButton2Click(Sender: TObject);
begin
  sButton4.Enabled := True;
  sDBGrid1.Enabled := True;

  sEdit1.ReadOnly := True;
  sEdit1.Color := clBtnFace;

  setData;

end;

procedure TdlgUserAdmin_frm.FormShow(Sender: TObject);
begin
  readData;
end;

procedure TdlgUserAdmin_frm.sButton4Click(Sender: TObject);
var
  sc : TSQLCreate;
  nIdx : Integer;
begin
  if MessageBox(self.Handle, '선택하신 유저를 삭제하시겠습니까?', '삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  nIdx := ADOQuery1.RecNo;

  sc := TSQLCreate.Create;
  try
    sc.DMLType := dmlDelete;
    sc.SQLHeader('PERSON');
    sc.ADDWhere('USERID', ADOQuery1.FieldByName('USERID').AsString);

    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := sc.CreateSQL;
        try
          ExecSQL;
          MessageBox(self.Handle, '삭제가 완료되었습니다', '완료', MB_OK+MB_ICONINFORMATION);

          readData;
          if nIdx > 1 Then
          ADOQuery1.MoveBy(nIDx-1);
        except
          on E:Exception do
          begin
            MessageBox(self.Handle, PChar('ERROR'#13#10+E.Message), '완료', MB_OK+MB_ICONERROR);
          end;
        end;
      finally
        Close;
        Free;
      end;
    end;
  finally
   sc.Free;
  end;
end;

end.
