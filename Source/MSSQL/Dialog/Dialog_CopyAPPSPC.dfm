inherited Dialog_CopyAPPSPC_frm: TDialog_CopyAPPSPC_frm
  Left = 462
  Top = 293
  Caption = #48373#49324#54624' '#50896#48376' '#47928#49436' '#49440#53469
  ClientHeight = 558
  ClientWidth = 904
  FormStyle = fsNormal
  OldCreateOrder = True
  Position = poOwnerFormCenter
  Visible = False
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel2: TsPanel [0]
    Left = 0
    Top = 0
    Width = 904
    Height = 35
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      904
      35)
    object edt_SearchText: TsEdit
      Left = 82
      Top = 5
      Width = 229
      Height = 23
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object com_SearchKeyword: TsComboBox
      Left = 4
      Top = 5
      Width = 77
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = #46321#47197#51068#51088
      OnSelect = com_SearchKeywordSelect
      Items.Strings = (
        #46321#47197#51068#51088
        #47928#49436#48264#54840
        #44396#47588#51088)
    end
    object Mask_SearchDate1: TsMaskEdit
      Left = 82
      Top = 5
      Width = 80
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 315
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object sBitBtn21: TsBitBtn
      Tag = 900
      Left = 163
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = sBitBtn21Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object Mask_SearchDate2: TsMaskEdit
      Left = 208
      Top = 5
      Width = 82
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn23: TsBitBtn
      Tag = 907
      Left = 289
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      OnClick = sBitBtn23Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object sPanel25: TsPanel
      Left = 184
      Top = 5
      Width = 25
      Height = 23
      Caption = '~'
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
    end
    object sBitBtn2: TsBitBtn
      Left = 831
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 8
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 761
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 9
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 35
    Width = 904
    Height = 523
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    OnDrawColumnCell = sDBGrid1DrawColumnCell
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'BGM_GUBUN'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #49888#52397#44396#48516
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTDOC_NO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #47928#49436#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 230
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CP_NO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #49888#52397#48264#54840
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 160
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'DATEE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #46321#47197#51068#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'APP_SNAME'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #49888#52397#51064
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DF_NAME1'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #44396#47588#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CP_AMT'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #52628#49900#44552#50529'('#50896#54868')'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 118
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CP_AMTU'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #52628#49900#44552#50529'('#50808#54868')'
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 118
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'CP_AMTC'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #45800#50948
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 82
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'USER_ID'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #51089#49457#51088
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -12
        Title.Font.Name = #47569#51008' '#44256#46357
        Title.Font.Style = []
        Width = 74
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 32
    Top = 88
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO,[USER_ID],DATEE,MESSAGE1,MESSAGE2,CHK1,CHK2,CHK3' +
        ',BGM_GUBUN,BGM1,BGM2,BGM3,BGM4,CP_NO'
      
        #9'   ,APP_CODE,APP_SNAME,APP_SAUPNO,[APP_NAME],APP_ELEC,APP_ADDR1' +
        ',APP_ADDR2,APP_ADDR3'
      
        #9'   ,CP_BANK,CP_BANKNAME,CP_BANKBU,CP_ACCOUNTNO,CP_NAME1,CP_NAME' +
        '2,CP_CURR'
      #9'   ,DF_SAUPNO,DF_NAME1,DF_NAME2,DF_NAME3,DF_EMAIL1,DF_EMAIL2'
      
        #9'   ,CP_ADD_ACCOUNTNO1,CP_ADD_NAME1,CP_ADD_CURR1,CP_ADD_ACCOUNTN' +
        'O2,CP_ADD_NAME2,CP_ADD_CURR2'
      #9'   ,CP_AMTC,CP_AMT,CP_AMTU,CP_CUX'
      #9'   ,BGM1+'#39'-'#39'+BGM2+BGM3+BGM4 TOTDOC_NO'
      #9'  '
      ''
      'FROM APPSPC_H AS SPC_H'
      '')
    Left = 32
    Top = 120
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListBGM_GUBUN: TStringField
      FieldName = 'BGM_GUBUN'
      OnGetText = qryListBGM_GUBUNGetText
      Size = 3
    end
    object qryListBGM1: TStringField
      FieldName = 'BGM1'
      Size = 12
    end
    object qryListBGM2: TStringField
      FieldName = 'BGM2'
      Size = 2
    end
    object qryListBGM3: TStringField
      FieldName = 'BGM3'
      Size = 2
    end
    object qryListBGM4: TStringField
      FieldName = 'BGM4'
      Size = 12
    end
    object qryListCP_NO: TStringField
      FieldName = 'CP_NO'
      Size = 35
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_SNAME: TStringField
      FieldName = 'APP_SNAME'
      Size = 35
    end
    object qryListAPP_SAUPNO: TStringField
      FieldName = 'APP_SAUPNO'
      Size = 10
    end
    object qryListAPP_NAME: TStringField
      FieldName = 'APP_NAME'
      Size = 35
    end
    object qryListAPP_ELEC: TStringField
      FieldName = 'APP_ELEC'
      Size = 35
    end
    object qryListAPP_ADDR1: TStringField
      FieldName = 'APP_ADDR1'
      Size = 35
    end
    object qryListAPP_ADDR2: TStringField
      FieldName = 'APP_ADDR2'
      Size = 35
    end
    object qryListAPP_ADDR3: TStringField
      FieldName = 'APP_ADDR3'
      Size = 35
    end
    object qryListCP_BANK: TStringField
      FieldName = 'CP_BANK'
      Size = 11
    end
    object qryListCP_BANKNAME: TStringField
      FieldName = 'CP_BANKNAME'
      Size = 70
    end
    object qryListCP_BANKBU: TStringField
      FieldName = 'CP_BANKBU'
      Size = 70
    end
    object qryListCP_ACCOUNTNO: TStringField
      FieldName = 'CP_ACCOUNTNO'
      Size = 17
    end
    object qryListCP_NAME1: TStringField
      FieldName = 'CP_NAME1'
      Size = 35
    end
    object qryListCP_NAME2: TStringField
      FieldName = 'CP_NAME2'
      Size = 35
    end
    object qryListCP_CURR: TStringField
      FieldName = 'CP_CURR'
      Size = 3
    end
    object qryListDF_SAUPNO: TStringField
      FieldName = 'DF_SAUPNO'
      Size = 10
    end
    object qryListDF_NAME1: TStringField
      FieldName = 'DF_NAME1'
      Size = 35
    end
    object qryListDF_NAME2: TStringField
      FieldName = 'DF_NAME2'
      Size = 35
    end
    object qryListDF_NAME3: TStringField
      FieldName = 'DF_NAME3'
      Size = 35
    end
    object qryListDF_EMAIL1: TStringField
      FieldName = 'DF_EMAIL1'
      Size = 35
    end
    object qryListDF_EMAIL2: TStringField
      FieldName = 'DF_EMAIL2'
      Size = 35
    end
    object qryListCP_ADD_ACCOUNTNO1: TStringField
      FieldName = 'CP_ADD_ACCOUNTNO1'
      Size = 17
    end
    object qryListCP_ADD_NAME1: TStringField
      FieldName = 'CP_ADD_NAME1'
      Size = 35
    end
    object qryListCP_ADD_CURR1: TStringField
      FieldName = 'CP_ADD_CURR1'
      Size = 3
    end
    object qryListCP_ADD_ACCOUNTNO2: TStringField
      FieldName = 'CP_ADD_ACCOUNTNO2'
      Size = 17
    end
    object qryListCP_ADD_NAME2: TStringField
      FieldName = 'CP_ADD_NAME2'
      Size = 35
    end
    object qryListCP_ADD_CURR2: TStringField
      FieldName = 'CP_ADD_CURR2'
      Size = 3
    end
    object qryListCP_AMTC: TStringField
      FieldName = 'CP_AMTC'
      Size = 3
    end
    object qryListCP_AMT: TBCDField
      FieldName = 'CP_AMT'
      DisplayFormat = '#,###.####;0'
      Precision = 18
    end
    object qryListCP_AMTU: TBCDField
      FieldName = 'CP_AMTU'
      DisplayFormat = '#,###.####;0'
      Precision = 18
    end
    object qryListCP_CUX: TBCDField
      FieldName = 'CP_CUX'
      Precision = 18
    end
    object qryListTOTDOC_NO: TStringField
      FieldName = 'TOTDOC_NO'
      ReadOnly = True
      Size = 29
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 64
    Top = 120
  end
end
