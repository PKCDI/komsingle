unit Dialog_CopyAPPSPC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, MSSQL, DB, ADODB, Grids, DBGrids,
  acDBGrid, StdCtrls, Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit,
  ExtCtrls, sPanel;

type
  TDialog_CopyAPPSPC_frm = class(TChildForm_frm)
    sPanel2: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListBGM_GUBUN: TStringField;
    qryListBGM1: TStringField;
    qryListBGM2: TStringField;
    qryListBGM3: TStringField;
    qryListBGM4: TStringField;
    qryListCP_NO: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_SNAME: TStringField;
    qryListAPP_SAUPNO: TStringField;
    qryListAPP_NAME: TStringField;
    qryListAPP_ELEC: TStringField;
    qryListAPP_ADDR1: TStringField;
    qryListAPP_ADDR2: TStringField;
    qryListAPP_ADDR3: TStringField;
    qryListCP_BANK: TStringField;
    qryListCP_BANKNAME: TStringField;
    qryListCP_BANKBU: TStringField;
    qryListCP_ACCOUNTNO: TStringField;
    qryListCP_NAME1: TStringField;
    qryListCP_NAME2: TStringField;
    qryListCP_CURR: TStringField;
    qryListDF_SAUPNO: TStringField;
    qryListDF_NAME1: TStringField;
    qryListDF_NAME2: TStringField;
    qryListDF_NAME3: TStringField;
    qryListDF_EMAIL1: TStringField;
    qryListDF_EMAIL2: TStringField;
    qryListCP_ADD_ACCOUNTNO1: TStringField;
    qryListCP_ADD_NAME1: TStringField;
    qryListCP_ADD_CURR1: TStringField;
    qryListCP_ADD_ACCOUNTNO2: TStringField;
    qryListCP_ADD_NAME2: TStringField;
    qryListCP_ADD_CURR2: TStringField;
    qryListCP_AMTC: TStringField;
    qryListCP_AMT: TBCDField;
    qryListCP_AMTU: TBCDField;
    qryListCP_CUX: TBCDField;
    qryListTOTDOC_NO: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sBitBtn23Click(Sender: TObject);
    procedure qryListBGM_GUBUNGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    FSQL : String;
    procedure ReadList;
  public
    { Public declarations }
    function openDialog:String;
  end;

var
  Dialog_CopyAPPSPC_frm: TDialog_CopyAPPSPC_frm;

implementation

uses KISCalendar, Commonlib, DateUtils;

{$R *.dfm}

{ TDialog_CopyAPPSPC_frm }

function TDialog_CopyAPPSPC_frm.openDialog: String;
begin
    Result := '';

  com_SearchKeyword.ItemIndex := 0;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ReadList;

  IF ShowModal = mrOk then
    Result := qryListMAINT_NO.AsString;
end;

procedure TDialog_CopyAPPSPC_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    Case com_SearchKeyword.ItemIndex of
      0: SQL.Add('WHERE DATEE between '+QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
      1: SQL.Add('WHERE TOTDOC_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE DFNAME1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    SQL.Add('ORDER BY DATEE');

    Open;
  end;
end;

procedure TDialog_CopyAPPSPC_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TDialog_CopyAPPSPC_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TDialog_CopyAPPSPC_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TDialog_CopyAPPSPC_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  Case Key of
    VK_RETURN : ModalResult := mrOk;
    VK_ESCAPE : ModalResult := mrCancel;
  end;
end;

procedure TDialog_CopyAPPSPC_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsDBgrid).ScreenToClient(Mouse.CursorPos).Y > 17 Then
    ModalResult := mrOk;
end;

procedure TDialog_CopyAPPSPC_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
//  IF not (ProgramControlType in [ctInsert,ctModify]) Then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TDialog_CopyAPPSPC_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate1);
end;

procedure TDialog_CopyAPPSPC_frm.sBitBtn23Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate2);
end;

procedure TDialog_CopyAPPSPC_frm.qryListBGM_GUBUNGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  if Trim(qryListBGM_GUBUN.AsString) = '2BK' then
    Text := '�߽�'
  else if Trim(qryListBGM_GUBUN.AsString) = '2BJ' then
    Text := '����';
end;

procedure TDialog_CopyAPPSPC_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

end.
