inherited Dialog_AttachFromINF707_frm: TDialog_AttachFromINF707_frm
  Left = 967
  Top = 279
  Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#51312#44148' '#48320#44221' '#51025#45813#49436' '#47785#47197#50640#49436' '#49440#53469
  ClientHeight = 583
  ClientWidth = 853
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 853
    Height = 35
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      853
      35)
    object edt_SearchText: TsEdit
      Left = 78
      Top = 5
      Width = 225
      Height = 23
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Visible = False
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object com_SearchKeyword: TsComboBox
      Left = 4
      Top = 5
      Width = 73
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = #53685#51648#51068#51088
      OnSelect = com_SearchKeywordSelect
      Items.Strings = (
        #53685#51648#51068#51088
        #44288#47532#48264#54840
        #49688#54812#51088)
    end
    object Mask_SearchDate1: TsMaskEdit
      Left = 78
      Top = 5
      Width = 78
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161125'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn1: TsBitBtn
      Left = 304
      Top = 5
      Width = 66
      Height = 23
      Cursor = crHandPoint
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 6
      Images = DMICON.System18
    end
    object sBitBtn21: TsBitBtn
      Tag = 900
      Left = 155
      Top = 5
      Width = 20
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      OnClick = btnCalendar
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object Mask_SearchDate2: TsMaskEdit
      Left = 198
      Top = 5
      Width = 82
      Height = 23
      AutoSize = False
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 5
      Text = '20161125'
      CheckOnExit = True
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn23: TsBitBtn
      Tag = 901
      Left = 279
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object sPanel25: TsPanel
      Left = 174
      Top = 5
      Width = 25
      Height = 23
      Caption = '~'
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
    end
    object sBitBtn2: TsBitBtn
      Left = 781
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 8
      OnClick = sBitBtn2Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 18
      Images = DMICON.System18
    end
    object sBitBtn3: TsBitBtn
      Left = 711
      Top = 5
      Width = 68
      Height = 23
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #49440#53469
      ModalResult = 1
      TabOrder = 9
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 17
      Images = DMICON.System18
    end
  end
  object sDBGrid1: TsDBGrid [1]
    Left = 0
    Top = 35
    Width = 853
    Height = 548
    Align = alClient
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #47569#51008' '#44256#46357
    TitleFont.Style = []
    OnDblClick = sDBGrid1DblClick
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'MAINT_NO'
        Title.Caption = #44288#47532#48264#54840
        Width = 213
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USER_ID'
        Title.Caption = #49324#50857#51088
        Width = 109
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DATEE'
        Title.Caption = #46321#47197'('#49688#49888')'#51068#51088
        Width = 170
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = #47928#49436#51221#48372
        Width = 170
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = #48708#44256
        Width = 170
        Visible = True
      end>
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 56
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT I707_1.MAINT_NO,I707_1.MSEQ,I707_1.AMD_NO,I707_1.MESSAGE1' +
        ',I707_1.MESSAGE2,I707_1.[USER_ID],I707_1.DATEE,I707_1.APP_DATE,I' +
        '707_1.IN_MATHOD,'
      
        #9'   I707_1.AP_BANK,I707_1.AP_BANK1,I707_1.AP_BANK2,I707_1.AP_BAN' +
        'K3,I707_1.AP_BANK4,I707_1.AP_BANK5,'
      
        #9'   I707_1.AD_BANK,I707_1.AD_BANK1,I707_1.AD_BANK2,I707_1.AD_BAN' +
        'K3,I707_1.AD_BANK4,'
      
        #9'   I707_1.IL_NO1,I707_1.IL_NO2,I707_1.IL_NO3,I707_1.IL_NO4,I707' +
        '_1.IL_NO5,'
      
        #9'   I707_1.IL_AMT1,I707_1.IL_AMT2,I707_1.IL_AMT3,I707_1.IL_AMT4,' +
        'I707_1.IL_AMT5,'
      
        #9'   I707_1.IL_CUR1,I707_1.IL_CUR2,I707_1.IL_CUR3,I707_1.IL_CUR4,' +
        'I707_1.IL_CUR5,'
      
        #9'   I707_1.AD_INFO1,I707_1.AD_INFO2,I707_1.AD_INFO3,I707_1.AD_IN' +
        'FO4,I707_1.AD_INFO5,'
      
        #9'   I707_1.CD_NO,I707_1.RCV_REF,I707_1.IBANK_REF,I707_1.ISS_BANK' +
        '1,I707_1.ISS_BANK2,I707_1.ISS_BANK3,I707_1.ISS_BANK4,I707_1.ISS_' +
        'BANK5,I707_1.ISS_ACCNT,'
      
        '                   I707_1.ISS_DATE,I707_1.AMD_DATE,I707_1.EX_DAT' +
        'E,I707_1.EX_PLACE,I707_1.CHK1,I707_1.CHK2,I707_1.CHK3,'
      
        #9'   I707_1.F_INTERFACE,I707_1.IMP_CD1,I707_1.IMP_CD2,I707_1.IMP_' +
        'CD3,I707_1.IMP_CD4,I707_1.IMP_CD5,I707_1.Prno,'
      ''
      
        #9'   I707_2.MAINT_NO,I707_2.MSEQ,I707_2.AMD_NO,I707_2.APPLIC1,I70' +
        '7_2.APPLIC2,I707_2.APPLIC3,I707_2.APPLIC4,I707_2.APPLIC5,'
      
        #9'   I707_2.BENEFC1,I707_2.BENEFC2,I707_2.BENEFC3,I707_2.BENEFC4,' +
        'I707_2.BENEFC5,I707_2.INCD_CUR,I707_2.INCD_AMT,I707_2.DECD_CUR,I' +
        '707_2.DECD_AMT,'
      
        #9'   I707_2.NWCD_CUR,I707_2.NWCD_AMT,I707_2.CD_PERP,I707_2.CD_PER' +
        'M,I707_2.CD_MAX,I707_2.AA_CV1,I707_2.AA_CV2,I707_2.AA_CV3,I707_2' +
        '.AA_CV4,'
      
        #9'   I707_2.LOAD_ON,I707_2.FOR_TRAN,I707_2.LST_DATE,I707_2.SHIP_P' +
        'D,'
      
        #9'   I707_2.SHIP_PD1,I707_2.SHIP_PD2,I707_2.SHIP_PD3,I707_2.SHIP_' +
        'PD4,I707_2.SHIP_PD5,I707_2.SHIP_PD6,'
      
        '                   I707_2.NARRAT,I707_2.NARRAT_1,I707_2.SR_INFO1' +
        ',I707_2.SR_INFO2,I707_2.SR_INFO3,I707_2.SR_INFO4,I707_2.SR_INFO5' +
        ',I707_2.SR_INFO6,'
      
        #9'   I707_2.EX_NAME1,I707_2.EX_NAME2,I707_2.EX_NAME3,I707_2.EX_AD' +
        'DR1,I707_2.EX_ADDR2,'
      
        #9'   I707_2.OP_BANK1,I707_2.OP_BANK2,I707_2.OP_BANK3,I707_2.OP_AD' +
        'DR1,I707_2.OP_ADDR2,I707_2.BFCD_AMT,I707_2.BFCD_CUR,I707_2.SUNJU' +
        'CK_PORT,I707_2.DOCHACK_PORT'
      #9'   '
      #9
      #9'  ,Mathod707.DOC_NAME as mathod_Name'
      #9'  ,IMPCD707_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD707_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD707_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD707_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD707_5.DOC_NAME as Imp_Name_5'
      #9'  ,CDMAX707.DOC_NAME as CDMAX_Name'
      #9'       '
      '  '
      'FROM [dbo].[INF707_1] AS I707_1'
      
        'INNER JOIN [dbo].[INF707_2] AS I707_2 ON I707_1.MAINT_NO = I707_' +
        '2.MAINT_NO'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod707 ON I707_1.IN_MATHOD = Mathod' +
        '707.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_1 ON I707_1.IMP_CD1 = IMPCD' +
        '707_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_2 ON I707_1.IMP_CD2 = IMPCD' +
        '707_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_3 ON I707_1.IMP_CD3 = IMPCD' +
        '707_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_4 ON I707_1.IMP_CD4 = IMPCD' +
        '707_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_5 ON I707_1.IMP_CD5 = IMPCD' +
        '707_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX707 ON I707_2.CD_MAX = CDMAX707' +
        '.CODE')
    Left = 8
    Top = 96
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryListAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListRCV_REF: TStringField
      FieldName = 'RCV_REF'
      Size = 35
    end
    object qryListIBANK_REF: TStringField
      FieldName = 'IBANK_REF'
      Size = 35
    end
    object qryListISS_BANK1: TStringField
      FieldName = 'ISS_BANK1'
      Size = 35
    end
    object qryListISS_BANK2: TStringField
      FieldName = 'ISS_BANK2'
      Size = 35
    end
    object qryListISS_BANK3: TStringField
      FieldName = 'ISS_BANK3'
      Size = 35
    end
    object qryListISS_BANK4: TStringField
      FieldName = 'ISS_BANK4'
      Size = 35
    end
    object qryListISS_BANK5: TStringField
      FieldName = 'ISS_BANK5'
      Size = 35
    end
    object qryListISS_ACCNT: TStringField
      FieldName = 'ISS_ACCNT'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListAMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListPrno: TIntegerField
      FieldName = 'Prno'
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListMSEQ_1: TIntegerField
      FieldName = 'MSEQ_1'
    end
    object qryListAMD_NO_1: TIntegerField
      FieldName = 'AMD_NO_1'
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListINCD_CUR: TStringField
      FieldName = 'INCD_CUR'
      Size = 3
    end
    object qryListINCD_AMT: TBCDField
      FieldName = 'INCD_AMT'
      Precision = 18
    end
    object qryListDECD_CUR: TStringField
      FieldName = 'DECD_CUR'
      Size = 3
    end
    object qryListDECD_AMT: TBCDField
      FieldName = 'DECD_AMT'
      Precision = 18
    end
    object qryListNWCD_CUR: TStringField
      FieldName = 'NWCD_CUR'
      Size = 3
    end
    object qryListNWCD_AMT: TBCDField
      FieldName = 'NWCD_AMT'
      Precision = 18
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListNARRAT: TBooleanField
      FieldName = 'NARRAT'
    end
    object qryListNARRAT_1: TMemoField
      FieldName = 'NARRAT_1'
      BlobType = ftMemo
    end
    object qryListSR_INFO1: TStringField
      FieldName = 'SR_INFO1'
      Size = 35
    end
    object qryListSR_INFO2: TStringField
      FieldName = 'SR_INFO2'
      Size = 35
    end
    object qryListSR_INFO3: TStringField
      FieldName = 'SR_INFO3'
      Size = 35
    end
    object qryListSR_INFO4: TStringField
      FieldName = 'SR_INFO4'
      Size = 35
    end
    object qryListSR_INFO5: TStringField
      FieldName = 'SR_INFO5'
      Size = 35
    end
    object qryListSR_INFO6: TStringField
      FieldName = 'SR_INFO6'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListOP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Size = 35
    end
    object qryListOP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Size = 35
    end
    object qryListOP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Size = 35
    end
    object qryListOP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Size = 35
    end
    object qryListOP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Size = 35
    end
    object qryListBFCD_AMT: TBCDField
      FieldName = 'BFCD_AMT'
      Precision = 18
    end
    object qryListBFCD_CUR: TStringField
      FieldName = 'BFCD_CUR'
      Size = 3
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListCDMAX_Name: TStringField
      FieldName = 'CDMAX_Name'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 96
  end
end
