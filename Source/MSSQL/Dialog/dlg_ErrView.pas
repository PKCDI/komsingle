unit dlg_ErrView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sMemo, sComboBox, ExtCtrls, sPanel, StrUtils;

type
  Tdlg_ErrView_frm = class(TForm)
    sMemo1: TsMemo;
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    sComboBox1: TsComboBox;
    procedure FormShow(Sender: TObject);
    procedure sComboBox1Select(Sender: TObject);
  private
    { Private declarations }
    procedure ReadLogFile;
  public
    { Public declarations }
  end;

var
  dlg_ErrView_frm: Tdlg_ErrView_frm;

implementation

uses
  VarDefine;

{$R *.dfm}

procedure Tdlg_ErrView_frm.FormShow(Sender: TObject);
begin
  ReadLogFile;
end;

procedure Tdlg_ErrView_frm.ReadLogFile;
var
  TMP_LIST : TStringList;
  i : Integer;
begin
  sMemo1.Clear;
  TMP_LIST := TStringList.Create;
  try
    case sCombobox1.ItemIndex of
      0: TMP_LIST.LoadFromFile(WINMATE_LOG+WINMATE_LOG_OUT);
      1: TMP_LIST.LoadFromFile(WINMATE_LOG+WINMATE_LOG_IN);
    end;

    for i:= 0 to TMP_LIST.Count-1 do
    begin
      IF (LeftStr(TMP_LIST.Strings[i], 1) <> '+') OR (RightStr(TMP_LIST.Strings[i], 1) = 'N') Then
        sMemo1.Lines.Add(TMP_LIST.Strings[i]);
    end;

  finally
    TMP_LIST.Free;
  end;
end;

procedure Tdlg_ErrView_frm.sComboBox1Select(Sender: TObject);
begin
  ReadLogFile;
end;

end.
