inherited dlg_CreateDocumentChoice_VATBI3_frm: Tdlg_CreateDocumentChoice_VATBI3_frm
  Left = 1153
  Top = 359
  Caption = #49464#44552#44228#49328#49436' '#51089#49457
  ClientHeight = 271
  ClientWidth = 440
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 440
    Height = 271
    object sLabel6: TsLabel
      Left = 8
      Top = 7
      Width = 66
      Height = 25
      Caption = 'VATBI3'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 29
      Width = 112
      Height = 15
      Caption = #49688#54812#50629#51088' '#49464#44552#44228#49328#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel1: TsLabel
      Left = 8
      Top = 55
      Width = 124
      Height = 15
      Caption = #51089#49457#48169#49885#51012' '#49440#53469#54616#49464#50836
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object sBevel1: TsBevel
      Left = 8
      Top = 49
      Width = 423
      Height = 2
      Shape = bsFrame
    end
    object sButton4: TsButton
      Left = 8
      Top = 80
      Width = 425
      Height = 57
      Caption = #49688#49888#46108' '#49464#44552#44228#49328#49436#50640#49436' '#49440#53469'(&1)'
      ModalResult = 1
      TabOrder = 0
      SkinData.SkinSection = 'BUTTON'
      CommandLinkHint = #49688#49888#46108' '#47928#49436#47484' '#49440#53469#54616#50668' '#48373#49324#54633#45768#45796
      Images = DMICON.System24
      ImageIndex = 31
      Style = bsCommandLink
      Reflected = True
    end
    object sButton1: TsButton
      Left = 8
      Top = 142
      Width = 425
      Height = 57
      Caption = #49888#44508#47928#49436' '#51089#49457'(&3)'
      ModalResult = 4
      TabOrder = 1
      SkinData.SkinSection = 'BUTTON'
      CommandLinkHint = #49888#44508#47928#49436#47484' '#51649#51217' '#49688#46041#51004#47196' '#51089#49457#54633#45768#45796
      Images = DMICON.System24
      ImageIndex = 5
      Style = bsCommandLink
      Reflected = True
    end
    object sButton2: TsButton
      Left = 8
      Top = 204
      Width = 425
      Height = 57
      Cancel = True
      Caption = #52712#49548'(ESC)'
      ModalResult = 2
      TabOrder = 2
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
      CommandLinkHint = #47928#49436#51089#49457#51012' '#52712#49548#54633#45768#45796
      Images = DMICON.System24
      ImageIndex = 18
      Style = bsCommandLink
      Reflected = True
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 392
  end
end
