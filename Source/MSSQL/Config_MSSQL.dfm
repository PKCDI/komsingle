inherited Config_MSSQL_frm: TConfig_MSSQL_frm
  Left = 856
  Top = 151
  ClientWidth = 388
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel1: TsPanel
    Width = 388
    inherited sGroupBox1: TsGroupBox
      Height = 137
      inherited Rad_0: TsRadioButton
        Checked = True
        TabStop = True
      end
      inherited Rad_1: TsRadioButton
        Width = 160
        Caption = #45380#50900'+'#50976#51200#48264#54840'+'#51068#47144#48264#54840
      end
      inherited Rad_2: TsRadioButton
        Left = 264
        Top = 80
        Width = 160
        Caption = #50500#51060#46356'+'#45380#50900#51068'+'#51068#47144#48264#54840
        Visible = False
      end
      inherited Rad_3: TsRadioButton
        Left = 264
        Top = 104
        Width = 148
        Caption = #50500#51060#46356'+'#45380#50900'+'#51068#47144#48264#54840
        Visible = False
      end
      inherited Rad_4: TsRadioButton
        Top = 72
        Width = 172
        Caption = #45380#50900#51068'+'#50976#51200#48264#54840'+'#51068#47144#48264#54840
      end
      inherited Chk_Added: TsCheckBox
        Top = 104
      end
    end
    inherited sButton1: TsButton
      Top = 600
      OnClick = sButton1Click
    end
    inherited sButton2: TsButton
      Top = 600
    end
  end
  object qryConfig: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [NAME1]'
      '      ,[NAME2]'
      '      ,[TRAD_NO]'
      '      ,[SAUP_NO]'
      '      ,[ADDR1]'
      '      ,[ADDR2]'
      '      ,[ADDR3]'
      '      ,[ENAME1]'
      '      ,[ENAME2]'
      '      ,[ENAME3]'
      '      ,[EADDR1]'
      '      ,[EADDR2]'
      '      ,[EADDR3]'
      '      ,[SIGN_C]'
      '      ,[SIGN]'
      '      ,[NMLEN]'
      '      ,[SZLEN]'
      '      ,[HDMRG]'
      '      ,[BDLEN]'
      '      ,[LFMRG]'
      '      ,[OP_NO]'
      '      ,[APP_SAUP]'
      '      ,[APP_NAME1]'
      '      ,[APP_NAME2]'
      '      ,[APP_NAME3]'
      '      ,[APP_NAME4]'
      '      ,[APP_NO]'
      '      ,[APP_CST]'
      '      ,[MNF_SAUP]'
      '      ,[MNF_NAME1]'
      '      ,[MNF_NAME2]'
      '      ,[MNF_NAME3]'
      '      ,[MNF_NAME4]'
      '      ,[BANK_CD]'
      '      ,[BANK]'
      '      ,[BANK_ACC]'
      '      ,[CST1]'
      '      ,[CST2]'
      '      ,[PrtNm]'
      '      ,[PrtNo]'
      '      ,[AutoGubun]'
      '      ,[Jecw]'
      '      ,[CST3]'
      '  FROM [CONFIG]')
    Left = 328
    Top = 280
    object qryConfigNAME1: TStringField
      FieldName = 'NAME1'
      Size = 35
    end
    object qryConfigNAME2: TStringField
      FieldName = 'NAME2'
      Size = 35
    end
    object qryConfigTRAD_NO: TStringField
      FieldName = 'TRAD_NO'
      Size = 10
    end
    object qryConfigSAUP_NO: TStringField
      FieldName = 'SAUP_NO'
      Size = 10
    end
    object qryConfigADDR1: TStringField
      FieldName = 'ADDR1'
      Size = 35
    end
    object qryConfigADDR2: TStringField
      FieldName = 'ADDR2'
      Size = 35
    end
    object qryConfigADDR3: TStringField
      FieldName = 'ADDR3'
      Size = 35
    end
    object qryConfigENAME1: TStringField
      FieldName = 'ENAME1'
      Size = 35
    end
    object qryConfigENAME2: TStringField
      FieldName = 'ENAME2'
      Size = 35
    end
    object qryConfigENAME3: TStringField
      FieldName = 'ENAME3'
      Size = 35
    end
    object qryConfigEADDR1: TStringField
      FieldName = 'EADDR1'
      Size = 35
    end
    object qryConfigEADDR2: TStringField
      FieldName = 'EADDR2'
      Size = 35
    end
    object qryConfigEADDR3: TStringField
      FieldName = 'EADDR3'
      Size = 35
    end
    object qryConfigSIGN_C: TStringField
      FieldName = 'SIGN_C'
      Size = 1
    end
    object qryConfigSIGN: TStringField
      FieldName = 'SIGN'
      Size = 10
    end
    object qryConfigNMLEN: TStringField
      FieldName = 'NMLEN'
      Size = 2
    end
    object qryConfigSZLEN: TStringField
      FieldName = 'SZLEN'
      Size = 2
    end
    object qryConfigHDMRG: TStringField
      FieldName = 'HDMRG'
      Size = 2
    end
    object qryConfigBDLEN: TStringField
      FieldName = 'BDLEN'
      Size = 2
    end
    object qryConfigLFMRG: TStringField
      FieldName = 'LFMRG'
      Size = 2
    end
    object qryConfigOP_NO: TStringField
      FieldName = 'OP_NO'
      Size = 35
    end
    object qryConfigAPP_SAUP: TStringField
      FieldName = 'APP_SAUP'
      Size = 10
    end
    object qryConfigAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 40
    end
    object qryConfigAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 40
    end
    object qryConfigAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 40
    end
    object qryConfigAPP_NAME4: TStringField
      FieldName = 'APP_NAME4'
      Size = 40
    end
    object qryConfigAPP_NO: TStringField
      FieldName = 'APP_NO'
      Size = 40
    end
    object qryConfigAPP_CST: TStringField
      FieldName = 'APP_CST'
      Size = 40
    end
    object qryConfigMNF_SAUP: TStringField
      FieldName = 'MNF_SAUP'
      Size = 10
    end
    object qryConfigMNF_NAME1: TStringField
      FieldName = 'MNF_NAME1'
      Size = 40
    end
    object qryConfigMNF_NAME2: TStringField
      FieldName = 'MNF_NAME2'
      Size = 40
    end
    object qryConfigMNF_NAME3: TStringField
      FieldName = 'MNF_NAME3'
      Size = 40
    end
    object qryConfigMNF_NAME4: TStringField
      FieldName = 'MNF_NAME4'
      Size = 40
    end
    object qryConfigBANK_CD: TStringField
      FieldName = 'BANK_CD'
      Size = 6
    end
    object qryConfigBANK: TStringField
      FieldName = 'BANK'
      Size = 30
    end
    object qryConfigBANK_ACC: TStringField
      FieldName = 'BANK_ACC'
    end
    object qryConfigCST1: TStringField
      FieldName = 'CST1'
      Size = 3
    end
    object qryConfigCST2: TStringField
      FieldName = 'CST2'
      Size = 10
    end
    object qryConfigPrtNm: TStringField
      FieldName = 'PrtNm'
      Size = 1
    end
    object qryConfigPrtNo: TIntegerField
      FieldName = 'PrtNo'
    end
    object qryConfigAutoGubun: TStringField
      FieldName = 'AutoGubun'
      Size = 1
    end
    object qryConfigJecw: TStringField
      FieldName = 'Jecw'
      Size = 1
    end
    object qryConfigCST3: TStringField
      FieldName = 'CST3'
      Size = 3
    end
  end
  object qryIns: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'TRAD_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SAUP_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SIGN_C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'SIGN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'NMLEN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'SZLEN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'HDMRG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'BDLEN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'LFMRG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'OP_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_SAUP'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'APP_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_NAME4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_CST'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'MNF_SAUP'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MNF_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'MNF_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'MNF_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'MNF_NAME4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'BANK_CD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'BANK'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'BANK_ACC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CST1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'CST2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'PrtNm'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'PrtNo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'AutoGubun'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'Jecw'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'CST3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'INSERT INTO [CONFIG]'
      'VALUES( :NAME1'
      '      , :NAME2'
      '      , :TRAD_NO'
      '      , :SAUP_NO'
      '      , :ADDR1'
      '      , :ADDR2'
      '      , :ADDR3'
      '      , :ENAME1'
      '      , :ENAME2'
      '      , :ENAME3'
      '      , :EADDR1'
      '      , :EADDR2'
      '      , :EADDR3'
      '      , :SIGN_C'
      '      , :SIGN'
      '      , :NMLEN'
      '      , :SZLEN'
      '      , :HDMRG'
      '      , :BDLEN'
      '      , :LFMRG'
      '      , :OP_NO'
      '      , :APP_SAUP'
      '      , :APP_NAME1'
      '      , :APP_NAME2'
      '      , :APP_NAME3'
      '      , :APP_NAME4'
      '      , :APP_NO'
      '      , :APP_CST'
      '      , :MNF_SAUP'
      '      , :MNF_NAME1'
      '      , :MNF_NAME2'
      '      , :MNF_NAME3'
      '      , :MNF_NAME4'
      '      , :BANK_CD'
      '      , :BANK'
      '      , :BANK_ACC'
      '      , :CST1'
      '      , :CST2'
      '      , :PrtNm'
      '      , :PrtNo'
      '      , :AutoGubun'
      '      , :Jecw'
      '      , :CST3)')
    Left = 328
    Top = 312
    object StringField1: TStringField
      FieldName = 'NAME1'
      Size = 35
    end
    object StringField2: TStringField
      FieldName = 'NAME2'
      Size = 35
    end
    object StringField3: TStringField
      FieldName = 'TRAD_NO'
      Size = 10
    end
    object StringField4: TStringField
      FieldName = 'SAUP_NO'
      Size = 10
    end
    object StringField5: TStringField
      FieldName = 'ADDR1'
      Size = 35
    end
    object StringField6: TStringField
      FieldName = 'ADDR2'
      Size = 35
    end
    object StringField7: TStringField
      FieldName = 'ADDR3'
      Size = 35
    end
    object StringField8: TStringField
      FieldName = 'ENAME1'
      Size = 35
    end
    object StringField9: TStringField
      FieldName = 'ENAME2'
      Size = 35
    end
    object StringField10: TStringField
      FieldName = 'ENAME3'
      Size = 35
    end
    object StringField11: TStringField
      FieldName = 'EADDR1'
      Size = 35
    end
    object StringField12: TStringField
      FieldName = 'EADDR2'
      Size = 35
    end
    object StringField13: TStringField
      FieldName = 'EADDR3'
      Size = 35
    end
    object StringField14: TStringField
      FieldName = 'SIGN_C'
      Size = 1
    end
    object StringField15: TStringField
      FieldName = 'SIGN'
      Size = 10
    end
    object StringField16: TStringField
      FieldName = 'NMLEN'
      Size = 2
    end
    object StringField17: TStringField
      FieldName = 'SZLEN'
      Size = 2
    end
    object StringField18: TStringField
      FieldName = 'HDMRG'
      Size = 2
    end
    object StringField19: TStringField
      FieldName = 'BDLEN'
      Size = 2
    end
    object StringField20: TStringField
      FieldName = 'LFMRG'
      Size = 2
    end
    object StringField21: TStringField
      FieldName = 'OP_NO'
      Size = 35
    end
    object StringField22: TStringField
      FieldName = 'APP_SAUP'
      Size = 10
    end
    object StringField23: TStringField
      FieldName = 'APP_NAME1'
      Size = 40
    end
    object StringField24: TStringField
      FieldName = 'APP_NAME2'
      Size = 40
    end
    object StringField25: TStringField
      FieldName = 'APP_NAME3'
      Size = 40
    end
    object StringField26: TStringField
      FieldName = 'APP_NAME4'
      Size = 40
    end
    object StringField27: TStringField
      FieldName = 'APP_NO'
      Size = 40
    end
    object StringField28: TStringField
      FieldName = 'APP_CST'
      Size = 40
    end
    object StringField29: TStringField
      FieldName = 'MNF_SAUP'
      Size = 10
    end
    object StringField30: TStringField
      FieldName = 'MNF_NAME1'
      Size = 40
    end
    object StringField31: TStringField
      FieldName = 'MNF_NAME2'
      Size = 40
    end
    object StringField32: TStringField
      FieldName = 'MNF_NAME3'
      Size = 40
    end
    object StringField33: TStringField
      FieldName = 'MNF_NAME4'
      Size = 40
    end
    object StringField34: TStringField
      FieldName = 'BANK_CD'
      Size = 6
    end
    object StringField35: TStringField
      FieldName = 'BANK'
      Size = 30
    end
    object StringField36: TStringField
      FieldName = 'BANK_ACC'
    end
    object StringField37: TStringField
      FieldName = 'CST1'
      Size = 3
    end
    object StringField38: TStringField
      FieldName = 'CST2'
      Size = 10
    end
    object StringField39: TStringField
      FieldName = 'PrtNm'
      Size = 1
    end
    object IntegerField1: TIntegerField
      FieldName = 'PrtNo'
    end
    object StringField40: TStringField
      FieldName = 'AutoGubun'
      Size = 1
    end
    object StringField41: TStringField
      FieldName = 'Jecw'
      Size = 1
    end
    object StringField42: TStringField
      FieldName = 'CST3'
      Size = 3
    end
  end
  object qryMod: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'TRAD_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SAUP_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'ENAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EADDR1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EADDR2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'EADDR3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SIGN_C'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'SIGN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'NMLEN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'SZLEN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'HDMRG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'BDLEN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'LFMRG'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'OP_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'APP_SAUP'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'APP_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_NAME4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'APP_CST'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'MNF_SAUP'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MNF_NAME1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'MNF_NAME2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'MNF_NAME3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'MNF_NAME4'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 40
        Value = Null
      end
      item
        Name = 'BANK_CD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'BANK'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end
      item
        Name = 'BANK_ACC'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CST1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'CST2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'PrtNm'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'PrtNo'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'AutoGubun'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'Jecw'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'CST3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE [CONFIG]'
      'SET NAME1'#9#9#9'= :NAME1 '
      '    ,NAME2'#9#9#9'= :NAME2'
      '    ,TRAD_NO'#9#9#9'= :TRAD_NO'
      '    ,SAUP_NO'#9#9#9'= :SAUP_NO'
      '    ,ADDR1'#9#9#9'= :ADDR1'
      '    ,ADDR2'#9#9#9'= :ADDR2'
      '    ,ADDR3'#9#9#9'= :ADDR3'
      '    ,ENAME1'#9#9#9'= :ENAME1'
      '    ,ENAME2'#9#9#9'= :ENAME2'
      '    ,ENAME3'#9#9#9'= :ENAME3'
      '    ,EADDR1'#9#9#9'= :EADDR1'
      '    ,EADDR2'#9#9#9'= :EADDR2'
      '    ,EADDR3'#9#9#9'= :EADDR3'
      '    ,SIGN_C'#9#9#9'= :SIGN_C'
      '    ,SIGN'#9#9#9'= :SIGN'
      '    ,NMLEN'#9#9#9'= :NMLEN'
      '    ,SZLEN'#9#9#9'= :SZLEN'
      '    ,HDMRG'#9#9#9'= :HDMRG'
      '    ,BDLEN'#9#9#9'= :BDLEN'
      '    ,LFMRG'#9#9#9'= :LFMRG'
      '    ,OP_NO'#9#9#9'= :OP_NO'
      '    ,APP_SAUP'#9#9'= :APP_SAUP'
      '    ,APP_NAME1'#9#9'= :APP_NAME1'
      '    ,APP_NAME2'#9#9'= :APP_NAME2'
      '    ,APP_NAME3'#9#9'= :APP_NAME3'
      '    ,APP_NAME4'#9#9'= :APP_NAME4'
      '    ,APP_NO'#9#9#9'= :APP_NO'
      '    ,APP_CST'#9#9#9'= :APP_CST'
      '    ,MNF_SAUP'#9#9'= :MNF_SAUP'
      '    ,MNF_NAME1'#9#9'= :MNF_NAME1'
      '    ,MNF_NAME2'#9#9'= :MNF_NAME2'
      '    ,MNF_NAME3'#9#9'= :MNF_NAME3'
      '    ,MNF_NAME4'#9#9'= :MNF_NAME4'
      '    ,BANK_CD'#9#9#9'= :BANK_CD'
      '    ,BANK'#9#9#9'= :BANK'
      '    ,BANK_ACC'#9#9'= :BANK_ACC'
      '    ,CST1'#9#9#9'= :CST1'
      '    ,CST2'#9#9#9'= :CST2'
      '    ,PrtNm'#9#9#9'= :PrtNm'
      '    ,PrtNo'#9#9#9'= :PrtNo'
      '    ,AutoGubun'#9#9'= :AutoGubun'
      '    ,Jecw'#9#9#9'= :Jecw'
      '    ,CST3'#9#9#9'= :CST3')
    Left = 328
    Top = 344
    object StringField43: TStringField
      FieldName = 'NAME1'
      Size = 35
    end
    object StringField44: TStringField
      FieldName = 'NAME2'
      Size = 35
    end
    object StringField45: TStringField
      FieldName = 'TRAD_NO'
      Size = 10
    end
    object StringField46: TStringField
      FieldName = 'SAUP_NO'
      Size = 10
    end
    object StringField47: TStringField
      FieldName = 'ADDR1'
      Size = 35
    end
    object StringField48: TStringField
      FieldName = 'ADDR2'
      Size = 35
    end
    object StringField49: TStringField
      FieldName = 'ADDR3'
      Size = 35
    end
    object StringField50: TStringField
      FieldName = 'ENAME1'
      Size = 35
    end
    object StringField51: TStringField
      FieldName = 'ENAME2'
      Size = 35
    end
    object StringField52: TStringField
      FieldName = 'ENAME3'
      Size = 35
    end
    object StringField53: TStringField
      FieldName = 'EADDR1'
      Size = 35
    end
    object StringField54: TStringField
      FieldName = 'EADDR2'
      Size = 35
    end
    object StringField55: TStringField
      FieldName = 'EADDR3'
      Size = 35
    end
    object StringField56: TStringField
      FieldName = 'SIGN_C'
      Size = 1
    end
    object StringField57: TStringField
      FieldName = 'SIGN'
      Size = 10
    end
    object StringField58: TStringField
      FieldName = 'NMLEN'
      Size = 2
    end
    object StringField59: TStringField
      FieldName = 'SZLEN'
      Size = 2
    end
    object StringField60: TStringField
      FieldName = 'HDMRG'
      Size = 2
    end
    object StringField61: TStringField
      FieldName = 'BDLEN'
      Size = 2
    end
    object StringField62: TStringField
      FieldName = 'LFMRG'
      Size = 2
    end
    object StringField63: TStringField
      FieldName = 'OP_NO'
      Size = 35
    end
    object StringField64: TStringField
      FieldName = 'APP_SAUP'
      Size = 10
    end
    object StringField65: TStringField
      FieldName = 'APP_NAME1'
      Size = 40
    end
    object StringField66: TStringField
      FieldName = 'APP_NAME2'
      Size = 40
    end
    object StringField67: TStringField
      FieldName = 'APP_NAME3'
      Size = 40
    end
    object StringField68: TStringField
      FieldName = 'APP_NAME4'
      Size = 40
    end
    object StringField69: TStringField
      FieldName = 'APP_NO'
      Size = 40
    end
    object StringField70: TStringField
      FieldName = 'APP_CST'
      Size = 40
    end
    object StringField71: TStringField
      FieldName = 'MNF_SAUP'
      Size = 10
    end
    object StringField72: TStringField
      FieldName = 'MNF_NAME1'
      Size = 40
    end
    object StringField73: TStringField
      FieldName = 'MNF_NAME2'
      Size = 40
    end
    object StringField74: TStringField
      FieldName = 'MNF_NAME3'
      Size = 40
    end
    object StringField75: TStringField
      FieldName = 'MNF_NAME4'
      Size = 40
    end
    object StringField76: TStringField
      FieldName = 'BANK_CD'
      Size = 6
    end
    object StringField77: TStringField
      FieldName = 'BANK'
      Size = 30
    end
    object StringField78: TStringField
      FieldName = 'BANK_ACC'
    end
    object StringField79: TStringField
      FieldName = 'CST1'
      Size = 3
    end
    object StringField80: TStringField
      FieldName = 'CST2'
      Size = 10
    end
    object StringField81: TStringField
      FieldName = 'PrtNm'
      Size = 1
    end
    object IntegerField2: TIntegerField
      FieldName = 'PrtNo'
    end
    object StringField82: TStringField
      FieldName = 'AutoGubun'
      Size = 1
    end
    object StringField83: TStringField
      FieldName = 'Jecw'
      Size = 1
    end
    object StringField84: TStringField
      FieldName = 'CST3'
      Size = 3
    end
  end
end
