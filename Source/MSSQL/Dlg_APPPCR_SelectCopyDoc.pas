unit Dlg_APPPCR_SelectCopyDoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sButton, sEdit, sComboBox, Mask, sMaskEdit, sSkinProvider, ExtCtrls,
  sPanel,DateUtils;

type
  TDlg_APPPCR_SelectCopyDoc_frm = class(TDialogParent_frm)
    sPanel7: TsPanel;
    sMaskEdit6: TsMaskEdit;
    sComboBox5: TsComboBox;
    sMaskEdit7: TsMaskEdit;
    sEdit3: TsEdit;
    sButton1: TsButton;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListAPP_ADDR1: TStringField;
    qryListAPP_ADDR2: TStringField;
    qryListAPP_ADDR3: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListSUP_CODE: TStringField;
    qryListSUP_NAME1: TStringField;
    qryListSUP_NAME2: TStringField;
    qryListSUP_NAME3: TStringField;
    qryListSUP_ADDR1: TStringField;
    qryListSUP_ADDR2: TStringField;
    qryListSUP_ADDR3: TStringField;
    qryListITM_G1: TStringField;
    qryListITM_G2: TStringField;
    qryListITM_G3: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYC: TStringField;
    qryListTAMT1: TBCDField;
    qryListTAMT1C: TStringField;
    qryListTAMT2: TBCDField;
    qryListTAMT2C: TStringField;
    qryListCHG_G: TStringField;
    qryListC1_AMT: TBCDField;
    qryListC1_C: TStringField;
    qryListC2_AMT: TBCDField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListPRNO: TIntegerField;
    qryListPCrLic_No: TStringField;
    qryListChgCd: TStringField;
    qryListAPP_DATE: TStringField;
    qryListDOC_G: TStringField;
    qryListDOC_D: TStringField;
    qryListBHS_NO: TStringField;
    qryListBNAME: TStringField;
    qryListBNAME1: TMemoField;
    qryListBAMT: TBCDField;
    qryListBAMTC: TStringField;
    qryListEXP_DATE: TStringField;
    qryListSHIP_DATE: TStringField;
    qryListBSIZE1: TMemoField;
    qryListBAL_CD: TStringField;
    qryListBAL_NAME1: TStringField;
    qryListBAL_NAME2: TStringField;
    qryListF_INTERFACE: TStringField;
    qryListSRBUHO: TStringField;
    qryListITM_GBN: TStringField;
    qryListITM_GNAME: TStringField;
    qryListISSUE_GBN: TStringField;
    qryListDOC_GBN: TStringField;
    qryListDOC_NO: TStringField;
    qryListBAMT2: TBCDField;
    qryListBAMTC2: TStringField;
    qryListBAMT3: TBCDField;
    qryListBAMTC3: TStringField;
    qryListPCRLIC_ISS_NO: TStringField;
    qryListFIN_CODE: TStringField;
    qryListFIN_NAME: TStringField;
    qryListFIN_NAME2: TStringField;
    qryListNEGO_APPDT: TStringField;
    qryListEMAIL_ID: TStringField;
    qryListEMAIL_DOMAIN: TStringField;
    qryListBK_CD: TStringField;
    dsList: TDataSource;
    sButton2: TsButton;
    procedure FormCreate(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sMaskEdit6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sMaskEdit7KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    FSQL :String;
    FDocNo : String;
    procedure ReadMstList;
    { Private declarations }
  public
    { Public declarations }
    property DocNo:String  read FDocNo;
  end;

var
  Dlg_APPPCR_SelectCopyDoc_frm: TDlg_APPPCR_SelectCopyDoc_frm;

implementation

uses MSSQL, VarDefine;

{$R *.dfm}

procedure TDlg_APPPCR_SelectCopyDoc_frm.ReadMstList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE between' + QuotedStr(sMaskEdit6.Text) + 'AND' + QuotedStr(sMaskEdit7.Text));
//    ParamByName('FROMDATE').Value := sMaskEdit6.Text;
//    ParamByName('TODATE'  ).Value := sMaskEdit7.Text;
//    IF sEdit3.Text <> '' Then
    SQL.Add('AND MAINT_NO LIKE ' + QuotedStr('%' + sEdit3.Text + '%'));
//    SQL.Add('AND USER_ID = ' + QuotedStr(LoginData.sID));
    //CHK2 2 = 삭제
    SQL.Add('AND CHK2 = 1');
    SQL.Add('ORDER BY DATEE ');
    Open;
  end;
end;

procedure TDlg_APPPCR_SelectCopyDoc_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
  sMaskEdit6.Text := FormatDateTime('YYYYMMDD',StartOfTheMonth(Now));
  sMaskEdit7.Text := FormatDateTime('YYYYMMDD',Now);
end;

procedure TDlg_APPPCR_SelectCopyDoc_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadMstList;
end;

procedure TDlg_APPPCR_SelectCopyDoc_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  IF qryList.RecordCount = 0 Then Exit;
  
  ModalResult := mrOk;
  FDocNo := qryListMAINT_NO.AsString;
end;

procedure TDlg_APPPCR_SelectCopyDoc_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadMstList;
end;

procedure TDlg_APPPCR_SelectCopyDoc_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_APPPCR_SelectCopyDoc_frm.sMaskEdit6KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN THEN sMaskEdit7.SetFocus;
end;

procedure TDlg_APPPCR_SelectCopyDoc_frm.sMaskEdit7KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN THEN ReadMstList;
end;

procedure TDlg_APPPCR_SelectCopyDoc_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  if qryListCHK2.IsNull then
    Exit;
  case qryListCHK2.AsInteger of
    0:
      Text := '임시';
    1:
      Text := '문서';
  end;
end;

procedure TDlg_APPPCR_SelectCopyDoc_frm.sDBGrid1DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

end.
