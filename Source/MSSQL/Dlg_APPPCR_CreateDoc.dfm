inherited Dlg_APPPCR_CreateDoc_frm: TDlg_APPPCR_CreateDoc_frm
  Left = 772
  Top = 24
  BorderWidth = 4
  Caption = ''
  ClientHeight = 763
  ClientWidth = 696
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  Visible = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 696
    Height = 53
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton3: TsSpeedButton
      Left = 233
      Top = 1
      Width = 2
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSplitter2: TsSplitter
      Left = 1
      Top = 1
      Width = 232
      Height = 51
      Cursor = crHSplit
      AutoSnap = False
      Enabled = False
      SkinData.SkinSection = 'SPLITTER'
    end
    object Btn_Modify: TsSpeedButton
      Tag = 1
      Left = 576
      Top = 1
      Width = 53
      Height = 51
      Cursor = crHandPoint
      Caption = #51200#51109
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Align = alRight
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 7
      Images = DMICON.System24
      Reflected = True
    end
    object sSpeedButton5: TsSpeedButton
      Left = 629
      Top = 1
      Width = 11
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Close: TsSpeedButton
      Left = 640
      Top = 1
      Width = 55
      Height = 51
      Cursor = crHandPoint
      Caption = #52712#49548
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = Btn_CloseClick
      Align = alRight
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 18
      Images = DMICON.System24
      Reflected = True
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 7
      Width = 73
      Height = 25
      Caption = 'APPPCR'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 29
      Width = 220
      Height = 15
      Caption = #50808#54868#54925#46301#50857' '#44396#47588#54869#51064#49888#52397#49436' '#51089#49457' '#48143' '#49688#51221
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton10: TsSpeedButton
      Tag = 1
      Left = 512
      Top = 1
      Width = 53
      Height = 51
      Cursor = crHandPoint
      Caption = #51076#49884
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Align = alRight
      Alignment = taLeftJustify
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 12
      Images = DMICON.System24
      Reflected = True
    end
    object sSpeedButton11: TsSpeedButton
      Left = 565
      Top = 1
      Width = 11
      Height = 51
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
  end
  object sPanel3: TsPanel [1]
    Left = 0
    Top = 53
    Width = 696
    Height = 32
    Align = alTop
    TabOrder = 1
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    DesignSize = (
      696
      32)
    object sEdit1: TsEdit
      Left = 88
      Top = 6
      Width = 153
      Height = 21
      Hint = 'MAINT_NO'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_RegistDate: TsEdit
      Left = 304
      Top = 6
      Width = 81
      Height = 21
      Hint = 'DATEE'
      TabStop = False
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_User: TsEdit
      Left = 432
      Top = 6
      Width = 41
      Height = 21
      TabStop = False
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_Function: TsEdit
      Left = 625
      Top = 6
      Width = 33
      Height = 21
      Hint = 'MESSAGE1'
      TabStop = False
      Anchors = [akTop, akRight]
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #47928#49436#44592#45733'/'#51025#45813#50976#54805
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_Response: TsEdit
      Left = 657
      Top = 6
      Width = 33
      Height = 21
      Hint = 'MESSAGE2'
      TabStop = False
      Anchors = [akTop, akRight]
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object sPanel2: TsPanel [2]
    Left = 0
    Top = 85
    Width = 696
    Height = 214
    Align = alTop
    TabOrder = 2
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    object sLabel1: TsLabel
      Left = 424
      Top = 49
      Width = 216
      Height = 12
      Caption = #44396#47588#54869#51064#49436#48264#54840#45716' '#48320#44221#49888#52397#51068#46412#47564' '#51077#47141
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      UseSkinColor = False
    end
    object Edt_ApplicationCode: TsEdit
      Left = 120
      Top = 6
      Width = 57
      Height = 21
      Hint = 'APP_CODE'
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnDblClick = Edt_ApplicationCodeDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #49888#52397#51064#53076#46300
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = [fsBold]
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_ApplicationName1: TsEdit
      Left = 120
      Top = 26
      Width = 211
      Height = 21
      Hint = 'APP_NAME1'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #49345#54840
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_ApplicationName2: TsEdit
      Left = 120
      Top = 46
      Width = 211
      Height = 21
      Hint = 'APP_NAME2'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_ApplicationAddr1: TsEdit
      Left = 120
      Top = 66
      Width = 211
      Height = 21
      Hint = 'APP_ADDR1'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #51452#49548
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_ApplicationAddr2: TsEdit
      Left = 120
      Top = 86
      Width = 211
      Height = 21
      Hint = 'APP_ADDR2'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_ApplicationAddr3: TsEdit
      Left = 120
      Top = 106
      Width = 211
      Height = 21
      Hint = 'APP_ADDR3'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_ApplicationAXNAME1: TsEdit
      Left = 120
      Top = 126
      Width = 211
      Height = 21
      Hint = 'AX_NAME1'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 6
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #47749#51032#51064
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_ApplicationSAUPNO: TsEdit
      Left = 120
      Top = 146
      Width = 97
      Height = 21
      Hint = 'AX_NAME2'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #49324#50629#51088#48264#54840
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_ApplicationElectronicSign: TsEdit
      Left = 120
      Top = 166
      Width = 97
      Height = 21
      Hint = 'AX_NAME3'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #51204#51088#49436#47749
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_ApplicationAPPDATE: TsEdit
      Tag = 102
      Left = 120
      Top = 186
      Width = 97
      Height = 21
      Hint = 'APP_DATE'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 9
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #44396#47588'('#44277#44553')'#51068
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_DocumentsSection: TsEdit
      Left = 424
      Top = 6
      Width = 33
      Height = 21
      Hint = 'ChgCd'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      OnDblClick = Edt_DocumentsSectionDblClick
      OnExit = Edt_DocumentsSectionExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #47928#49436#44396#48516
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = [fsBold]
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit17: TsEdit
      Left = 456
      Top = 6
      Width = 201
      Height = 21
      TabStop = False
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 11
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_BuyConfirmDocuments: TsEdit
      Left = 424
      Top = 26
      Width = 233
      Height = 21
      Hint = 'PCrLic_No'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 12
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #44396#47588#54869#51064#49436#48264#54840
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_SupplyCode: TsEdit
      Left = 424
      Top = 66
      Width = 57
      Height = 21
      Hint = 'SUP_CODE'
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 13
      OnDblClick = Edt_ApplicationCodeDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #44277#44553#51088#53076#46300
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = [fsBold]
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_SupplyName: TsEdit
      Left = 424
      Top = 86
      Width = 233
      Height = 21
      Hint = 'SUP_NAME1'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 14
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #49345#54840
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_SupplyCEO: TsEdit
      Left = 424
      Top = 106
      Width = 233
      Height = 21
      Hint = 'SUP_NAME2'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 15
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #45824#54364#51060#49324
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_SupplyEID: TsEdit
      Left = 424
      Top = 126
      Width = 105
      Height = 21
      Hint = 'EMAIL_ID'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 16
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = 'E-MAIL'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sPanel4: TsPanel
      Left = 528
      Top = 126
      Width = 25
      Height = 21
      Caption = '@'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 17
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
    end
    object Edt_SupplyEID2: TsEdit
      Left = 552
      Top = 126
      Width = 105
      Height = 21
      Hint = 'EMAIL_DOMAIN'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 18
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_SupplyAddr1: TsEdit
      Left = 424
      Top = 146
      Width = 233
      Height = 21
      Hint = 'SUP_ADDR1'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 19
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #51452#49548
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_SupplyAddr2: TsEdit
      Left = 424
      Top = 166
      Width = 233
      Height = 21
      Hint = 'SUP_ADDR2'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 20
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object Edt_SupplySaupNo: TsEdit
      Left = 424
      Top = 186
      Width = 233
      Height = 21
      Hint = 'SUP_ADDR3'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 21
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #49324#50629#51088#48264#54840
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object sPanel6: TsPanel [3]
    Left = 0
    Top = 377
    Width = 696
    Height = 104
    Align = alTop
    TabOrder = 3
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    object sEdit33: TsEdit
      Left = 134
      Top = 6
      Width = 33
      Height = 21
      Hint = 'ITM_GBN'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnDblClick = Edt_DocumentsSectionDblClick
      OnExit = Edt_DocumentsSectionExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #44277#44553#47932#54408#47749#49464#44396#48516
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit34: TsEdit
      Left = 166
      Top = 6
      Width = 153
      Height = 21
      TabStop = False
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit35: TsEdit
      Left = 134
      Top = 54
      Width = 185
      Height = 21
      Hint = 'BK_NAME1'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit37: TsEdit
      Left = 134
      Top = 74
      Width = 185
      Height = 21
      Hint = 'BK_NAME2'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit39: TsEdit
      Left = 134
      Top = 34
      Width = 57
      Height = 21
      Hint = 'BK_CD'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      OnDblClick = sEdit39DblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #54869#51064#44592#44288
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit36: TsEdit
      Left = 528
      Top = 33
      Width = 33
      Height = 21
      Hint = 'TQTYC'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      OnDblClick = Edt_DocumentsSectionDblClick
      OnExit = Edt_DocumentsSectionExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit38: TsEdit
      Left = 528
      Top = 73
      Width = 33
      Height = 21
      TabStop = False
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 6
      Text = 'USD'
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit40: TsEdit
      Left = 528
      Top = 53
      Width = 33
      Height = 21
      Hint = 'TAMT1C'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      OnDblClick = Edt_DocumentsSectionDblClick
      OnExit = Edt_DocumentsSectionExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit41: TsEdit
      Tag = 1
      Left = 392
      Top = 33
      Width = 137
      Height = 21
      Hint = 'TQTY'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #52509#49688#47049
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit42: TsEdit
      Tag = 1
      Left = 392
      Top = 53
      Width = 137
      Height = 21
      Hint = 'TAMT1'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 9
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #52509#44552#50529
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit43: TsEdit
      Tag = 2
      Left = 392
      Top = 73
      Width = 137
      Height = 21
      Hint = 'TAMT2'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #52509#44552#50529
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object sPanel5: TsPanel [4]
    Left = 0
    Top = 299
    Width = 696
    Height = 78
    Align = alTop
    TabOrder = 4
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    object sLabel3: TsLabel
      Left = 291
      Top = 12
      Width = 366
      Height = 12
      Caption = #48320#46041#44552#50529' : '#49692#49688#50896#54868' '#46608#45716' '#50808#54868'(USD'#47484' '#51228#50808#54620')'#51068' '#44221#50864' '#49324#50857' '#50504' '#54632
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      UseSkinColor = False
    end
    object sEdit27: TsEdit
      Left = 135
      Top = 8
      Width = 33
      Height = 21
      Hint = 'CHG_G'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnDblClick = Edt_DocumentsSectionDblClick
      OnExit = Edt_DocumentsSectionExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #48320#46041#44396#48516
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit28: TsEdit
      Left = 167
      Top = 8
      Width = 113
      Height = 21
      TabStop = False
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit29: TsEdit
      Tag = 2
      Left = 135
      Top = 48
      Width = 113
      Height = 21
      Hint = 'C2_AMT'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #52509#44552#50529
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit30: TsEdit
      Left = 247
      Top = 48
      Width = 33
      Height = 21
      TabStop = False
      Color = clBtnFace
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      Text = 'USD'
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit31: TsEdit
      Left = 135
      Top = 28
      Width = 113
      Height = 21
      Hint = 'C1_AMT'
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Active = True
      BoundLabel.Caption = #52509#44552#50529
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sEdit32: TsEdit
      Left = 247
      Top = 28
      Width = 33
      Height = 21
      Hint = 'C1_C'
      CharCase = ecUpperCase
      Color = 11923648
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      OnDblClick = Edt_DocumentsSectionDblClick
      OnExit = Edt_DocumentsSectionExit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object sPageControl1: TsPageControl [5]
    Left = 0
    Top = 481
    Width = 696
    Height = 282
    ActivePage = sTabSheet1
    Align = alClient
    TabHeight = 25
    TabIndex = 0
    TabOrder = 5
    TabWidth = 95
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #49464#48512#49324#54637
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object StringGrid1: TStringGrid
        Left = 0
        Top = 0
        Width = 688
        Height = 247
        Align = alClient
        ColCount = 7
        Ctl3D = False
        DefaultRowHeight = 20
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
        ParentCtl3D = False
        TabOrder = 0
        OnDrawCell = StringGrid1DrawCell
        ColWidths = (
          38
          64
          64
          64
          64
          64
          64)
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #44540#44144#49436#47448
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object StringGrid2: TStringGrid
        Left = 0
        Top = 0
        Width = 688
        Height = 247
        Align = alClient
        ColCount = 7
        Ctl3D = False
        DefaultRowHeight = 20
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
        ParentCtl3D = False
        TabOrder = 0
        ColWidths = (
          38
          64
          64
          64
          64
          64
          64)
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #49464#44552#44228#49328#49436
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object StringGrid3: TStringGrid
        Left = 0
        Top = 0
        Width = 688
        Height = 247
        Align = alClient
        ColCount = 7
        Ctl3D = False
        DefaultRowHeight = 20
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
        ParentCtl3D = False
        TabOrder = 0
        ColWidths = (
          38
          64
          64
          64
          64
          64
          64)
      end
    end
  end
  object Pan_Detail: TsPanel [6]
    Left = 301
    Top = 483
    Width = 398
    Height = 25
    TabOrder = 6
    SkinData.SkinSection = 'TRANSPARENT'
    object Btn_DetailAdd: TsSpeedButton
      Left = 173
      Top = 1
      Width = 91
      Height = 23
      Caption = #45936#51060#53552#52628#44032
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      ParentFont = False
      Align = alRight
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 2
      Images = DMICON.System18
    end
    object LIne1: TsSpeedButton
      Left = 168
      Top = 1
      Width = 5
      Height = 23
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Line4: TsSpeedButton
      Left = 392
      Top = 1
      Width = 5
      Height = 23
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_DetailDel: TsSpeedButton
      Left = 333
      Top = 1
      Width = 59
      Height = 23
      Caption = #49325#51228
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      ParentFont = False
      Align = alRight
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 1
      Images = DMICON.System18
    end
    object Line3: TsSpeedButton
      Left = 328
      Top = 1
      Width = 5
      Height = 23
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_DetailMod: TsSpeedButton
      Left = 269
      Top = 1
      Width = 59
      Height = 23
      Caption = #49688#51221
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      ParentFont = False
      Align = alRight
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      ImageIndex = 3
      Images = DMICON.System18
    end
    object LIne2: TsSpeedButton
      Left = 264
      Top = 1
      Width = 5
      Height = 23
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 280
    Top = 16
  end
  object dsDetail: TDataSource
    Left = 24
    Top = 264
  end
  object dsDocument: TDataSource
    Left = 24
    Top = 296
  end
  object dsTax: TDataSource
    Left = 24
    Top = 328
  end
end
