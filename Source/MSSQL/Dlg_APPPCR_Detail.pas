unit Dlg_APPPCR_Detail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, Buttons, sSpeedButton, StdCtrls, sMemo, sEdit,
  sLabel, sSkinProvider, ExtCtrls, sPanel, ADODB, DB, StrUtils, TypeDefine;

type
  TDlg_APPPCR_Detail_frm = class(TDialogParent_frm)
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    Edt_Name: TsEdit;
    Edt_HSCode: TsEdit;
    Edt_APPDATE: TsEdit;
    sPanel2: TsPanel;
    sMemo1: TsMemo;
    sMemo2: TsMemo;
    sMemo3: TsMemo;
    Edt_QTYC: TsEdit;
    Edt_QTY: TsEdit;
    Edt_PRI_BASEC: TsEdit;
    Edt_PRI_BASE: TsEdit;
    Edt_PRI2: TsEdit;
    Edt_PRI1: TsEdit;
    EDT_AMT1C: TsEdit;
    EDT_AMT1: TsEdit;
    Edt_AMT2: TsEdit;
    Edt_ACHG_G: TsEdit;
    Edt_AC1_AMT: TsEdit;
    Edt_AC1_C: TsEdit;
    Edt_AC2_AMT: TsEdit;
    Btn_Save: TsSpeedButton;
    Btn_Cancel: TsSpeedButton;
    qryIns: TADOQuery;
    qryDel: TADOQuery;
    qryMod: TADOQuery;
    Edt_Seq: TsEdit;
    procedure Btn_CancelClick(Sender: TObject);
    procedure Btn_SaveClick(Sender: TObject);
    procedure Edt_QTYCDblClick(Sender: TObject);
    procedure Edt_NameDblClick(Sender: TObject);
  private
    { Private declarations }
    FFields : TFields;
    FProgramControls : TProgramControlType;
    FTempDoc : String;
    procedure SetData;
    procedure SetData_Insert;
    procedure SetParamAndExec(qry : TADOQuery);
  public
    { Public declarations }
    function Run(UserWork : TProgramControlType; UserFields : TFields; Const TempDoc : String=''):TModalResult;
  end;

var
  Dlg_APPPCR_Detail_frm: TDlg_APPPCR_Detail_frm;

implementation

uses MSSQL, Commonlib, Dlg_FindDefineCode, Dlg_FindJepum;

{$R *.dfm}

{ TDlg_APPPCR_Detail_frm }

{ TDlg_APPPCR_Detail_frm }

function TDlg_APPPCR_Detail_frm.Run(UserWork : TProgramControlType; UserFields : TFields; Const TempDoc : String=''): TModalResult;
begin
  FProgramControls := UserWork;
  FFields := UserFields;

  //문서번호
  Case FProgramControls of
    ctInsert : FTempDoc := TempDoc;
    ctModify : FTempDoc := FFields.FieldByName('KEYY').AsString;
  end;

  Case FProgramControls of
    ctInsert : SetData_Insert;
    ctModify : SetData;
  end;

  IF Self.ShowModal = mrOk Then
  begin
    Case FProgramControls of
      ctInsert : SetParamAndExec(qryIns);
      ctModify : SetParamAndExec(qryMod);
    end;
    Result := mrOk;
  end
  else
    Result := mrCancel;
end;

procedure TDlg_APPPCR_Detail_frm.SetData;
var
  nLoop, MaxCount : integer;
  TempEdit : TsEdit;
  TempMemo : TsMemo;
begin
  MaxCount := Self.ComponentCount;

  for nLoop := 0 to MaxCount - 1 do
  begin
    IF Components[nLoop] is TsEdit Then
    begin
      TempEdit := TsEdit(Components[nLoop]);

      IF TempEdit.Hint <> '' Then
      begin
        Case TempEdit.Tag of
          0: TempEdit.Text := FFields.FieldByName(TempEdit.Hint).AsString;
          1: TempEdit.Text := FormatFloat('#,0',FFields.FieldByName(TempEdit.Hint).AsCurrency);
          2: TempEdit.Text := FormatFloat('#,0.000',FFields.FieldByName(TempEdit.Hint).AsCurrency);
          102 :
          begin
            IF FFields.FieldByName(TempEdit.Hint).AsString <> '' Then
              TempEdit.Text := FormatDateTime('YYYY.MM.DD',decodeCharDate(FFields.FieldByName(TempEdit.Hint).AsString))
            else
              TempEdit.Text := '';
          end;
          103 : TempEdit.Text := HSCodeFormat(FFields.FieldByName(TempEdit.Hint).AsString);
        end;
      end;
    end
    else
    IF Components[nLoop] is TsMemo Then
    begin
      TempMemo := TsMemo(Components[nLoop]);

      IF TempMemo.Hint <> '' Then
      begin
        TempMemo.Text := FFields.FieldByName(TempMemo.Hint).AsString;
      end;
    end;
  end
end;

procedure TDlg_APPPCR_Detail_frm.Btn_CancelClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TDlg_APPPCR_Detail_frm.Btn_SaveClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure TDlg_APPPCR_Detail_frm.SetParamAndExec(qry: TADOQuery);
begin
  with qry do
  begin
    Close;
    Parameters.ParamByName('KEYY'     ).Value := FTempDoc;
    Parameters.ParamByName('SEQ'      ).Value := Edt_Seq.Text;
    Parameters.ParamByName('HS_NO'    ).Value := HSCodeFormat(Edt_HSCode.Text,false);
    Parameters.ParamByName('NAME'     ).Value := '';
    Parameters.ParamByName('NAME1'    ).Value := sMemo1.Text;
    Parameters.ParamByName('SIZE'     ).Value := sMemo2.Text;
    Parameters.ParamByName('SIZE1'    ).Value := '';
    Parameters.ParamByName('REMARK'   ).Value := sMemo3.Text;
    Parameters.ParamByName('QTY'      ).Value := STC(Edt_QTY.Text);
    Parameters.ParamByName('QTYC'     ).Value := Edt_QTYC.Text;
    Parameters.ParamByName('AMT1'     ).Value := STC(EDT_AMT1.Text);
    Parameters.ParamByName('AMT1C'    ).Value := EDT_AMT1C.Text;
    Parameters.ParamByName('AMT2'     ).Value := STC(Edt_AMT2.Text);
    Parameters.ParamByName('PRI1'     ).Value := STC(Edt_PRI1.Text);
    Parameters.ParamByName('PRI2'     ).Value := STC(Edt_PRI2.Text);
    Parameters.ParamByName('PRI_BASE' ).Value := STC(Edt_PRI_BASE.Text);
    Parameters.ParamByName('PRI_BASEC').Value := Edt_PRI_BASEC.Text;
    Parameters.ParamByName('ACHG_G'   ).Value := Edt_ACHG_G.Text;
    Parameters.ParamByName('AC1_AMT'  ).Value := STC(Edt_AC1_AMT.Text);
    Parameters.ParamByName('AC1_C'    ).Value := Edt_AC1_C.Text;
    Parameters.ParamByName('AC2_AMT'  ).Value := STC(Edt_AC2_AMT.Text);
    Parameters.ParamByName('LOC_TYPE' ).Value := '';
    Parameters.ParamByName('NAMESS'   ).Value := Edt_Name.Text;
    Parameters.ParamByName('SIZESS'   ).Value := '';
    Parameters.ParamByName('APP_DATE' ).Value := AnsiReplaceText(Edt_APPDATE.Text,'.','');
    Parameters.ParamByName('ITM_NUM'  ).Value := '';
    ExecSQL;
  end;
end;

procedure TDlg_APPPCR_Detail_frm.SetData_Insert;
begin
  Edt_Seq.Text := IntToStr(FFields.DataSet.RecordCount+1);
end;

procedure TDlg_APPPCR_Detail_frm.Edt_QTYCDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  Dlg_FindDefineCode_frm := TDlg_FindDefineCode_frm.Create(Self);
  try
    with Dlg_FindDefineCode_frm do
    begin
      //통화
      IF AnsiMatchText((Sender as TsEdit).Hint,['AMT1C','ACHG_G','AC1_C']) Then
      begin
          IF Run('TONGHWA') = mrOk Then
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
      else
      //단위
      IF AnsiMatchText((Sender as TsEdit).Hint,['QTYC','PRI_BASEC']) Then
      begin
          IF Run('DANWI') = mrOk Then
            (Sender as TsEdit).Text := DataSource1.DataSet.FieldByName('CODE').AsString;
      end
    end;
  finally
    FreeAndNil(Dlg_FindDefineCode_frm);
  end;
end;

procedure TDlg_APPPCR_Detail_frm.Edt_NameDblClick(Sender: TObject);
var
  TempDataSet : TDataSet;
begin
  inherited;
  Dlg_FindJepum_frm := TDlg_FindJepum_frm.Create(Self);
  try
    IF Dlg_FindJepum_frm.Run(Edt_Name.Text) = mrOk then
    begin
      TempDataSet := Dlg_FindJepum_frm.dsITEM.DataSet;
      Edt_Name.Text := TempDataset.FieldByName('CODE').AsString;
      sMemo1.Text := TempDataset.FieldByName('FNAME').AsString;
      sMemo2.Text := TempDataset.FieldByName('MSIZE').AsString;
    end;
  finally
    FreeAndNil(Dlg_FindJepum_frm);
  end;
end;

end.
