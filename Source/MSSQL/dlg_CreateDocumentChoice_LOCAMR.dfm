inherited dlg_CreateDocumentChoice_LOCAMR_frm: Tdlg_CreateDocumentChoice_LOCAMR_frm
  Left = 2380
  Top = 202
  Caption = #45236#44397#49888#50857#51109' '#51312#44148#48320#44221' '#49888#52397#49436' '#51089#49457
  ClientHeight = 349
  ClientWidth = 437
  KeyPreview = True
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 437
    Height = 349
    object sLabel6: TsLabel [0]
      Left = 8
      Top = 7
      Width = 80
      Height = 25
      Caption = 'LOCAMR'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel [1]
      Left = 8
      Top = 29
      Width = 152
      Height = 15
      Caption = #45236#44397#49888#50857#51109' '#51312#44148#48320#44221' '#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sBevel1: TsBevel [2]
      Left = 8
      Top = 49
      Width = 423
      Height = 2
      Shape = bsFrame
    end
    object sLabel1: TsLabel [3]
      Left = 8
      Top = 55
      Width = 124
      Height = 15
      Caption = #51089#49457#48169#49885#51012' '#49440#53469#54616#49464#50836
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object sBevel2: TsBevel [4]
      Left = 8
      Top = 273
      Width = 423
      Height = 2
      Shape = bsFrame
    end
    inherited sButton1: TsButton
      Top = 80
      Width = 425
      Caption = #45236#44397#49888#50857#51109' '#44060#49444' '#51025#45813#49436' '#47785#47197#50640#49436' '#49440#53469' (&1)'
      ImageIndex = 31
    end
    inherited sButton3: TsButton
      Top = 141
      Width = 423
      Caption = #45236#44397#49888#50857#51109' '#51312#44148#48320#44221' '#53685#51648#49436' '#47785#47197#50640#49436' '#49440#53469' (&2)'
    end
    object sButton4: TsButton [7]
      Left = 8
      Top = 202
      Width = 423
      Height = 57
      Caption = #49888#44508#46321#47197' (&3)'
      ModalResult = 4
      TabOrder = 2
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      CommandLinkHint = #49888#44508#47928#49436#47484' '#51649#51217' '#49688#46041#51004#47196' '#51089#49457#54633#45768#45796
      Images = DMICON.System24
      ImageIndex = 5
      Style = bsCommandLink
    end
    inherited sButton2: TsButton
      Top = 288
      Width = 423
      TabOrder = 3
    end
  end
end
