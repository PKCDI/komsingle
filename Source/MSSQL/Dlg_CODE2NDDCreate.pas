unit Dlg_CODE2NDDCreate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, StdCtrls, sButton, sCheckBox, sEdit,
  ExtCtrls, sBevel, sLabel, sPanel, sSkinProvider, TypeDefine ;

type
  TGroupType = (gtCode);
  TDlg_CODE2NDDCreate_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sLabel1: TsLabel;
    sBevel1: TsBevel;
    sPanel2: TsPanel;
    Edt_Gubun: TsEdit;
    sPanel3: TsPanel;
    edt_CODE: TsEdit;
    edt_Contents: TsEdit;
    chk_Use: TsCheckBox;
    Btn_ok: TsButton;
    Btn_Cancel: TsButton;
    qryCodeIns: TADOQuery;
    procedure Btn_CancelClick(Sender: TObject);
    procedure Btn_okClick(Sender: TObject);
  private
    { Private declarations }
    FGroupType : TGroupType;
    FProgramControlType : TProgramControlType;
    FFieldsDetail : TFields;
    procedure SetParam_Code(qry : TADOQuery);
  public
    { Public declarations }
     function Run(GroupType : TGroupType; ProgramControlType : TProgramControlType):TModalResult;
  end;

var
  Dlg_CODE2NDDCreate_frm: TDlg_CODE2NDDCreate_frm;

implementation

uses Dialog_CodeList, MSSQL, strUtils ;

{$R *.dfm}

{ TChildForm_frm1 }

function TDlg_CODE2NDDCreate_frm.Run(GroupType: TGroupType;
  ProgramControlType: TProgramControlType): TModalResult;
begin
  Result := mrCancel;
  FGroupType := GroupType;
  FProgramControlType := ProgramControlType;
//  FFieldsDetail := DetailFields;


  if FProgramControlType = ctInsert then
  begin
      IF FGroupType = gtCODE Then
      begin
        Edt_Gubun.Text := Dialog_CodeList_frm.FGroup;
      end;
  end;


  IF Self.ShowModal = mrOK Then
  begin
    if FGroupType = gtCode then
    begin
      if FProgramControlType = ctInsert then
      begin
        SetParam_Code(qryCodeIns);
      end;
    end;

    Result := mrOk;
  end

end;

procedure TDlg_CODE2NDDCreate_frm.SetParam_Code(qry: TADOQuery);
begin
  with qry do
  begin
    Close;
    Parameters.ParamByName('Prefix').Value := Edt_Gubun.Text;
    Parameters.ParamByName('CODE').Value := edt_CODE.Text;
    IF chk_Use.Checked Then
      Parameters.ParamByName('Remark').Value := '1'
    else
      Parameters.ParamByName('Remark').Value := '0';
    Parameters.ParamByName('Map').Value := edt_CODE.Text;
    Parameters.ParamByName('Name').Value := edt_Contents.Text;
    ExecSQL;
  end;
end;

procedure TDlg_CODE2NDDCreate_frm.Btn_CancelClick(Sender: TObject);
begin
  inherited;
   ModalResult := mrCancel;
end;

procedure TDlg_CODE2NDDCreate_frm.Btn_okClick(Sender: TObject);
var
  qryCheck : TADOQuery;
  TempStr : String;
  bOverlab : Boolean;
begin
  inherited;
  qryCheck := TADOQuery.Create(Self);
  qryCheck.Connection := DMMssql.KISConnect;
  bOverlab := False;

  try
    IF FProgramControlType = ctInsert then
    begin
      Case FGroupType of
        gtCode :
        begin
          //CODE중복확인
          qryCheck.SQL.Text := 'SELECT CODE2NDM.Prefix,CODE2NDM.Name,LTRIM(CODE2NDD.Name) as Name2,Gubun,CODE'#13#10+
                               'FROM CODE2NDM LEFT JOIN CODE2NDD ON CODE2NDM.Prefix = CODE2NDD.Prefix'#13#10+
                               'WHERE CODE2NDM.[Prefix] = :Prefix AND [CODE] = :CODE';
          qryCheck.Parameters.ParamByName('Prefix').Value := Dialog_CodeList_frm.FGroup;
          qryCheck.Parameters.ParamByName('CODE').Value := edt_CODE.Text;
          qryCheck.Open;

          bOverlab := qryCheck.RecordCount > 0;

          if bOverlab Then
          begin
            TempStr := '중복되는 구분코드 : ['+qryCheck.FieldByName('Prefix').AsString+']['+qryCheck.FieldByName('Name').AsString+']'#13#10+
                       '코드 : ['+qryCheck.FieldByName('CODE').AsString+']['+qryCheck.FieldByName('Name2').AsString+']';
          end;
        end;
      end;
    end;

    CASE AnsiIndexText('',[Trim(edt_contents.Text),Trim(edt_CODE.Text)]) OF
      0 : begin
           IF FGroupType = gtCODE then
           begin
             Tempstr := '[항목]을 입력하세요';
             edt_Contents.SetFocus;
             bOverlab := True;
           end
           else
             bOverlab := False;
         end;

      1: begin
           IF FGroupType = gtCODE then
           begin
             TempStr := '[코드]를 입력하세요';
             edt_CODE.SetFocus;
             bOverlab := True;
           end
           else
            bOverlab := False;
         end;
    end;

  finally
    qryCheck.Close;
    qryChecK.Free;
  end;
end;

end.
