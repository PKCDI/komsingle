unit Encryption;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Registry, StrUtils,
  DCPrc4, DCPcrypt2, DCPsha1, Dialogs;

//SHA-1을 이용한 해쉬 암호화 (복호화불가능)
function GetHashString(Value: String): String;

//RC4를 이용한 암호화/복호화 (KEY Setting : KISKEYSTRING)
Const
  RC4KEYSET = 'KISKEYSTRING';
function ServerEncrypt(Value : string):String;   //암호화
function ServerDecrypt(Value : string):String;   //복호화

//레지스트리 함수모음
//서버정보 내보내기
procedure SetServerConfig(IP,Port,ID,Pass,DBName : string; APPNO : Integer);
//서버정보 가져오기
Type
  TName = (nIP,nPort,nID,nPass,nDBName,nAppno);
function GetServerConfig(Name : TName) : Variant;
//레지스트리 정보를 ConnectionString으로 가져오기
function GetConnectString:string;


implementation

function GetHashString(Value: String): String;
var
  SHA1 : TDCP_sha1;
  Digest: array[0..19] of byte;  //20*8 = 60bit
  i : Integer;
begin
  SHA1 := TDCP_sha1.Create(nil);
  SHA1.Init;
  SHA1.UpdateStr(Value);
  SHA1.Final(Digest);
  for i:= 0 to 19 do
    Result := Result + IntToHex(Digest[i],2);
  SHA1.Free;
end;

function ServerEncrypt(Value : string):String;
var
  Cipher : TDCP_rc4;
begin
  Cipher := TDCP_rc4.Create(nil);
  Cipher.InitStr(RC4KEYSET,TDCP_sha1);
  Result := Cipher.EncryptString(Value);
  Cipher.Burn;
  Cipher.Free;
end;

function ServerDecrypt(Value : string):String;
var
  Cipher : TDCP_rc4;
begin
  Cipher := TDCP_rc4.Create(nil);
  Cipher.InitStr(RC4KEYSET,TDCP_sha1);
  Result := Cipher.DecryptString(Value);
  Cipher.Burn;
  Cipher.Free;
end;

procedure SetServerConfig(IP,Port,ID,Pass,DBName : string; APPNO : Integer);
var
  Reg : TRegistry;
begin
  //------Registry--------------------------------------------------------------
  Reg := TRegistry.Create;
  with Reg do
  begin
    RootKey := HKEY_LOCAL_MACHINE;                           //-----------Root
    OpenKey('SOFTWARE\KIS\INFOMATION',True);                //--Registry OPEN

    WriteString('ID',ServerEncrypt(ID));
    WriteString('PASSWORD',ServerEncrypt(Pass));
    WriteString('IPADDRESS',ServerEncrypt(IP));
    IF Port = '' Then Port := '0';
    WriteString('PORT',ServerEncrypt(Port));
    WriteString('DBNAME',ServerEncrypt(DBName));
    WriteInteger('APPNO',APPNO);
    CloseKey;
  end;
  Reg.Free;

end;

function GetServerConfig(Name : TName) : Variant;
var
  Reg : TRegistry;
begin
  //------Registry--------------------------------------------------------------
  Reg := TRegistry.Create;
  with Reg do
  begin
    RootKey := HKEY_LOCAL_MACHINE;                           //-----------Root
    OpenKey('SOFTWARE\KIS\INFOMATION',True);                //--Registry OPEN

    Result := '';

    CASE Name of
      nIP:
      begin
        IF ValueExists('IPADDRESS') Then Result := ServerDecrypt(ReadString('IPADDRESS'))
      end;

      nPort:
      begin
        IF ValueExists('PORT') Then
        begin
          if (ServerDecrypt(ReadString('PORT')) = '0') Then Result := ''
          else Result := ServerDecrypt(ReadString('PORT'));
        end;
      end;

      nID:
      begin
        IF ValueExists('ID') Then Result := ServerDecrypt(ReadString('ID'));
      end;

      nPass:
      begin
        IF ValueExists('PASSWORD') Then Result := ServerDecrypt(ReadString('PASSWORD'));
      end;

      nDBName:
      begin
        IF ValueExists('DBNAME') Then Result := ServerDecrypt(ReadString('DBNAME'));
      end;

      nAppno:
      begin
        IF ValueExists('APPNO') Then Result := ReadInteger('APPNO')
        else Result := 0;
      end;
    end;

    CloseKey;
  end;
  Reg.Free;
end;

function GetConnectString:string;
var
  IP, Port, ID, Pass, DBName : string;
begin
  try
    IP     := GetServerConfig(nIP);
    Port   := GetServerConfig(nPort);
    ID     := GetServerConfig(nID);
    Pass   := GetServerConfig(nPass);
    DBName := GetServerConfig(nDBName);
    Result := 'Provider=SQLOLEDB.1;Password='+Pass;
    Result := Result + ';Persist Security Info=True;User ID='+ID;
    Result := Result + ';Initial Catalog='+DBName;
    if Port = '' Then
      Result := Result + ';Data Source='+IP
    else
      Result := Result + ';Data Source='+IP+','+Port;
  except
    on E:Exception do
    begin
      ShowMessage('SQL설정이 필요합니다.');
    end;
  end;
end;

end.
