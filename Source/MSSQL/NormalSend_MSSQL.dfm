inherited NormalSend_MSSQL_frm: TNormalSend_MSSQL_frm
  Left = 607
  Top = 96
  Caption = #51068#48152#51025#45813#49436' - '#49569#49888
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel1: TsPanel
    inherited sLabel7: TsLabel
      Caption = #51068#48152#51025#45813#49436' ('#49569#49888')'
    end
    inherited sLabel6: TsLabel
      Width = 73
      Caption = 'GENRER'
    end
    inherited Btn_New: TsSpeedButton
      OnClick = Btn_NewClick
    end
    inherited Btn_Modify: TsSpeedButton
      OnClick = Btn_NewClick
    end
    inherited Btn_Cancel: TsSpeedButton
      OnClick = Btn_CancelClick
    end
    inherited Btn_Temp: TsSpeedButton
      OnClick = Btn_TempClick
    end
  end
  inherited sPageControl1: TsPageControl
    ActivePage = sTabSheet3
    TabIndex = 1
    OnChange = sPageControl1Change
    OnChanging = sPageControl1Changing
    inherited sTabSheet1: TsTabSheet
      inherited sDBGrid1: TsDBGrid
        DataSource = dsList
        OnDblClick = sDBGrid1DblClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            Title.Alignment = taCenter
            Title.Caption = #49440#53469
            Width = 33
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #47928#49436
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 249
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51088
            Width = 47
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51068#51088
            Width = 110
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #51204#49569
            Width = 115
            Visible = True
          end>
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sPanel2: TsPanel
        inherited sDBEdit1: TsDBEdit
          DataField = 'MAINT_NO'
          DataSource = dsList
        end
        inherited sDBEdit23: TsDBEdit
          DataField = 'USER_ID'
          DataSource = dsList
        end
      end
      inherited sPanel3: TsPanel
        inherited sDBEdit33: TsDBEdit
          Top = 61
          DataField = 'EX_NAME1'
          DataSource = dsList
        end
        inherited sDBEdit34: TsDBEdit
          Top = 85
          DataField = 'EX_NAME2'
          DataSource = dsList
        end
        inherited sDBEdit35: TsDBEdit
          Top = 109
          DataField = 'EX_NAME3'
          DataSource = dsList
        end
        inherited sDBEdit36: TsDBEdit
          Top = 149
          DataField = 'DATEE'
          DataSource = dsList
          MaxLength = 10
        end
        inherited sDBEdit37: TsDBEdit
          Top = 173
          DataField = 'EX_ID'
          DataSource = dsList
        end
        inherited sDBEdit38: TsDBEdit
          Top = 197
          DataField = 'SR_ID'
          DataSource = dsList
        end
        inherited sDBEdit39: TsDBEdit
          Top = 221
          DataField = 'XX_ID'
          DataSource = dsList
        end
        inherited sDBEdit40: TsDBEdit
          Width = 71
          Color = 11923648
          DataField = 'SR_CODE'
          DataSource = dsList
          OnDblClick = sButton2Click
          BoundLabel.Caption = ''
        end
        inherited sDBEdit41: TsDBEdit
          DataField = 'SR_NAME1'
          DataSource = dsList
          BoundLabel.Caption = '[1]'
        end
        inherited sDBEdit42: TsDBEdit
          DataField = 'SR_NAME2'
          DataSource = dsList
          BoundLabel.Caption = '[2]'
        end
        inherited sDBEdit43: TsDBEdit
          Width = 279
          DataField = 'SR_NAME3'
          DataSource = dsList
          BoundLabel.Caption = '[3]'
        end
        inherited sDBEdit44: TsDBEdit
          Top = 133
          DataField = ''
          Visible = False
        end
        inherited sPanel5: TsPanel
          Top = 133
          Visible = False
        end
        inherited sDBEdit45: TsDBEdit
          DataField = 'DOC1'
          DataSource = dsList
        end
        inherited sDBEdit46: TsDBEdit
          DataField = 'DOC2'
          DataSource = dsList
        end
        inherited sDBEdit47: TsDBEdit
          DataField = 'DOC3'
          DataSource = dsList
        end
        inherited sDBEdit48: TsDBEdit
          DataField = 'DOC4'
          DataSource = dsList
        end
        inherited sDBEdit49: TsDBEdit
          DataField = 'DOC5'
          DataSource = dsList
        end
        object sDBEdit2: TsDBEdit
          Left = 90
          Top = 37
          Width = 79
          Height = 23
          Color = 11923648
          DataField = 'EX_CODE'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 19
          OnDblClick = sDBEdit2DblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.Caption = #53076#46300
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlue
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object sRadioButton1: TsRadioButton
          Left = 466
          Top = 38
          Width = 44
          Height = 20
          Caption = #51008#54665
          Checked = True
          TabOrder = 20
          TabStop = True
          Visible = False
          SkinData.SkinSection = 'CHECKBOX'
        end
        object sRadioButton2: TsRadioButton
          Left = 519
          Top = 38
          Width = 56
          Height = 20
          Caption = #44144#47000#52376
          TabOrder = 21
          Visible = False
          SkinData.SkinSection = 'CHECKBOX'
        end
        object sButton2: TsButton
          Left = 434
          Top = 37
          Width = 31
          Height = 23
          Enabled = False
          TabOrder = 22
          OnClick = sButton2Click
          SkinData.SkinSection = 'BUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
        object sButton3: TsButton
          Left = 170
          Top = 37
          Width = 31
          Height = 23
          Enabled = False
          TabOrder = 23
          OnClick = sButton3Click
          SkinData.SkinSection = 'BUTTON'
          Images = DMICON.System16
          ImageIndex = 0
        end
      end
      inherited sPanel6: TsPanel
        inherited sDBMemo1: TsDBMemo
          DataField = 'DESC_1'
          DataSource = dsList
        end
      end
    end
  end
  object sPanel8: TsPanel [2]
    Left = 638
    Top = 59
    Width = 130
    Height = 24
    Anchors = [akRight]
    TabOrder = 2
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
  end
  object sPanel9: TsPanel [3]
    Left = 612
    Top = 59
    Width = 25
    Height = 24
    Anchors = [akRight]
    TabOrder = 3
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    object sSpeedButton4: TsSpeedButton
      Left = 1
      Top = 1
      Width = 23
      Height = 22
      Caption = #9664
      OnClick = sSpeedButton4Click
      Align = alClient
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
  end
  object sPanel10: TsPanel [4]
    Left = 769
    Top = 59
    Width = 25
    Height = 24
    Anchors = [akRight]
    TabOrder = 4
    SkinData.CustomColor = True
    SkinData.SkinSection = 'GROUPBOX'
    object sSpeedButton8: TsSpeedButton
      Left = 1
      Top = 1
      Width = 23
      Height = 22
      Caption = #9654
      OnClick = sSpeedButton8Click
      Align = alClient
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 56
  end
  object qryList: TADOQuery
    Active = True
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterInsert = qryListAfterInsert
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[USER_ID]'
      '      ,[DATEE]'
      '      ,[EX_CODE]'
      '      ,[EX_NAME1]'
      '      ,[EX_NAME2]'
      '      ,[EX_NAME3]'
      '      ,[SR_CODE]'
      '      ,[SR_NAME1]'
      '      ,[SR_NAME2]'
      '      ,[SR_NAME3]'
      '      ,[EX_ID]'
      '      ,[SR_ID]'
      '      ,[XX_ID]'
      '      ,[DOC_CODE]'
      '      ,[DOC1]'
      '      ,[DOC2]'
      '      ,[DOC3]'
      '      ,[DOC4]'
      '      ,[DOC5]'
      '      ,[DESC_CODE]'
      '      ,[DESC_1]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[PRNO]'
      '  FROM [GENRES]')
    Left = 32
    Top = 168
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryListEX_CODE: TStringField
      FieldName = 'EX_CODE'
      Size = 10
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 10
    end
    object qryListSR_CODE: TStringField
      FieldName = 'SR_CODE'
      Size = 10
    end
    object qryListSR_NAME1: TStringField
      FieldName = 'SR_NAME1'
      Size = 35
    end
    object qryListSR_NAME2: TStringField
      FieldName = 'SR_NAME2'
      Size = 35
    end
    object qryListSR_NAME3: TStringField
      FieldName = 'SR_NAME3'
      Size = 35
    end
    object qryListEX_ID: TStringField
      FieldName = 'EX_ID'
      Size = 25
    end
    object qryListSR_ID: TStringField
      FieldName = 'SR_ID'
      Size = 25
    end
    object qryListXX_ID: TStringField
      FieldName = 'XX_ID'
      Size = 25
    end
    object qryListDOC_CODE: TStringField
      FieldName = 'DOC_CODE'
      Size = 10
    end
    object qryListDOC1: TStringField
      FieldName = 'DOC1'
      Size = 70
    end
    object qryListDOC2: TStringField
      FieldName = 'DOC2'
      Size = 70
    end
    object qryListDOC3: TStringField
      FieldName = 'DOC3'
      Size = 70
    end
    object qryListDOC4: TStringField
      FieldName = 'DOC4'
      Size = 70
    end
    object qryListDOC5: TStringField
      FieldName = 'DOC5'
      Size = 70
    end
    object qryListDESC_CODE: TStringField
      FieldName = 'DESC_CODE'
      Size = 10
    end
    object qryListDESC_1: TMemoField
      FieldName = 'DESC_1'
      BlobType = ftMemo
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 64
    Top = 168
  end
end
