object LocalSetting_frm: TLocalSetting_frm
  Left = 811
  Top = 243
  BorderStyle = bsDialog
  Caption = #47196#52972#44221#47196#49444#51221
  ClientHeight = 374
  ClientWidth = 596
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #47569#51008' '#44256#46357
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel
    Left = 0
    Top = 0
    Width = 596
    Height = 51
    SkinData.CustomColor = True
    Align = alTop
    Color = clWhite
    
    TabOrder = 0
    object sLabel1: TsLabel
      Left = 8
      Top = 8
      Width = 183
      Height = 15
      Caption = #51217#49549#50640' '#54596#50836#54620' '#44221#47196#47484' '#49444#51221#54633#45768#45796'.'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel2: TsLabel
      Left = 8
      Top = 24
      Width = 243
      Height = 15
      Caption = #51076#51032#47196' '#48320#44221#49884' '#51217#49549#51060' '#48520#44032#45733' '#54624' '#49688' '#51080#49845#45768#45796'.'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
  end
  object sPanel3: TsPanel
    Left = 0
    Top = 51
    Width = 596
    Height = 323
    Align = alClient
    BorderWidth = 4
    
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object sGroupBox3: TsGroupBox
      Left = 4
      Top = 28
      Width = 587
      Height = 228
      Caption = #47196#52972#51217#49549' '#47785#47197
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object StringGrid2: TStringGrid
        Left = 2
        Top = 21
        Width = 583
        Height = 205
        Align = alClient
        ColCount = 2
        FixedCols = 0
        RowCount = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
        ParentFont = False
        TabOrder = 0
        OnDblClick = StringGrid2DblClick
        ColWidths = (
          73
          482)
      end
      object sPanel2: TsPanel
        Left = 2
        Top = 17
        Width = 583
        Height = 4
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 1
      end
    end
    object sButton7: TsButton
      Left = 208
      Top = 277
      Width = 88
      Height = 33
      Caption = #54869#51064
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = sButton7Click
    end
    object sButton8: TsButton
      Left = 300
      Top = 277
      Width = 88
      Height = 33
      Caption = #52712#49548
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = sButton8Click
    end
    object sSpinEdit1: TsSpinEdit
      Left = 326
      Top = 6
      Width = 41
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Text = '1'
      OnChange = sSpinEdit1Change
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47196#52972' '#51217#49549' '#44079#49688
      Increment = 1
      MaxValue = 999
      MinValue = 1
      Value = 1
    end
    object sEdit1: TsEdit
      Left = 107
      Top = 6
      Width = 126
      Height = 23
      TabOrder = 4
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = 'INSTANCE NAME'
    end
  end
  object sOpenDialog1: TsOpenDialog
    Filter = 'DataBase File(*.mdf)|*.mdf'
    Options = [ofHideReadOnly, ofNoValidate, ofEnableSizing]
    Left = 312
    Top = 16
  end
end
