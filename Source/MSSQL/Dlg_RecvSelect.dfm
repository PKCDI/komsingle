inherited Dlg_RecvSelect_frm: TDlg_RecvSelect_frm
  Left = 1216
  Top = 191
  Caption = #49688#49888' '#49345#45824#48169' '#49440#53469
  ClientHeight = 387
  ClientWidth = 356
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 356
    Height = 387
    object sLabel1: TsLabel
      Left = 16
      Top = 16
      Width = 168
      Height = 15
      Caption = #49688#49888' '#48155#51012' '#49345#45824#48169#51012' '#49440#53469#54633#45768#45796
    end
    object sHTMLLabel1: TsHTMLLabel
      Left = 16
      Top = 316
      Width = 203
      Height = 15
      Caption = '<font color=red><b>'#49688#49888#44144#47000#52376'</b></font>'#44032' '#50630#49845#45768#45796'. '#46321#47197#54644#51452#49464#50836
    end
    object sLabel2: TsLabel
      Left = 232
      Top = 316
      Width = 48
      Height = 15
      Cursor = crHandPoint
      Alignment = taCenter
      Caption = #48148#47196#44032#44592
      ParentFont = False
      OnClick = sLabel2Click
      OnMouseEnter = sLabel2MouseEnter
      OnMouseLeave = sLabel2MouseLeave
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsUnderline]
      UseSkinColor = False
    end
    object sDBGrid1: TsDBGrid
      Left = 16
      Top = 40
      Width = 320
      Height = 273
      Color = clWhite
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDblClick = sDBGrid1DblClick
      OnKeyUp = sDBGrid1KeyUp
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Expanded = False
          FieldName = 'CODE'
          Title.Caption = #53076#46300
          Width = 81
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #44144#47000#52376#47749
          Width = 200
          Visible = True
        end>
    end
    object sButton2: TsButton
      Left = 101
      Top = 343
      Width = 75
      Height = 29
      Caption = #54869#51064
      ModalResult = 1
      TabOrder = 1
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 16
    end
    object sButton3: TsButton
      Left = 181
      Top = 343
      Width = 75
      Height = 29
      Cancel = True
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 2
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 17
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 256
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT CODE,NAME'
      'FROM CODE2NDD'
      'WHERE Prefix = '#39'EDI_SEND'#39
      'AND Remark = 1')
    Left = 192
    Top = 8
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 224
    Top = 8
  end
end
