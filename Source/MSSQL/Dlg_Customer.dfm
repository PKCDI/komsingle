inherited Dlg_Customer_frm: TDlg_Customer_frm
  Left = 156
  Top = 299
  Caption = #49888#52397#51064#53076#46300' / '#44277#44553#51088#53076#46300' '#49440#53469
  ClientHeight = 435
  ClientWidth = 465
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 465
    Height = 435
    object sComboBox1: TsComboBox
      Left = 8
      Top = 8
      Width = 97
      Height = 23
      Alignment = taLeftJustify
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ItemHeight = 17
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = #53076#46300
      Items.Strings = (
        #53076#46300
        #49345#54840)
    end
    object sEdit1: TsEdit
      Left = 107
      Top = 8
      Width = 126
      Height = 23
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = DEFAULT_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -11
      BoundLabel.Font.Name = 'MS Sans Serif'
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sDBGrid1: TsDBGrid
      Left = 8
      Top = 36
      Width = 450
      Height = 349
      Anchors = [akLeft, akTop, akRight, akBottom]
      Color = clWhite
      Ctl3D = True
      DataSource = DataSource1
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDblClick = sDBGrid1DblClick
      OnKeyUp = sDBGrid1KeyUp
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Expanded = False
          FieldName = 'CODE'
          Title.Caption = #53076#46300
          Width = 64
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ENAME'
          Title.Caption = #49345#54840
          Width = 185
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'REP_NAME'
          Title.Caption = #45824#54364#51088
          Width = 147
          Visible = True
        end>
    end
    object sButton1: TsButton
      Left = 234
      Top = 8
      Width = 75
      Height = 23
      Caption = #44160#49353
      TabOrder = 3
      OnClick = sButton1Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System16
      ImageIndex = 0
    end
    object sButton2: TsButton
      Left = 151
      Top = 388
      Width = 75
      Height = 29
      Caption = #54869#51064
      ModalResult = 1
      TabOrder = 4
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 17
    end
    object sButton3: TsButton
      Left = 231
      Top = 388
      Width = 75
      Height = 29
      Cancel = True
      Caption = #52712#49548
      ModalResult = 2
      TabOrder = 5
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System18
      ImageIndex = 18
    end
    object sButton4: TsButton
      Left = 310
      Top = 8
      Width = 75
      Height = 23
      Caption = #52628#44032
      TabOrder = 6
      OnClick = sButton4Click
      SkinData.SkinSection = 'BUTTON'
      Images = DMICON.System16
      ImageIndex = 4
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 72
  end
  object DataSource1: TDataSource
    DataSet = DMCodeContents.GEOLAECHEO
    Left = 312
    Top = 40
  end
end
