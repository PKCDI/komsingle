inherited Dlg_APPPCR_NewSelect_frm: TDlg_APPPCR_NewSelect_frm
  Left = 1421
  Top = 534
  Caption = #50808#54868#54925#46301#50857' '#44396#47588#54869#51064#49888#52397#49436' '#51089#49457#50741#49496
  ClientHeight = 266
  ClientWidth = 425
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 15
  inherited sPanel1: TsPanel
    Width = 425
    Height = 266
    object sLabel6: TsLabel
      Left = 8
      Top = 7
      Width = 73
      Height = 25
      Caption = 'APPPCR'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 29
      Width = 148
      Height = 15
      Caption = #50808#54868#54925#46301#50857' '#44396#47588#54869#51064#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sBevel1: TsBevel
      Left = 8
      Top = 49
      Width = 401
      Height = 2
      Shape = bsFrame
    end
    object sLabel1: TsLabel
      Left = 8
      Top = 55
      Width = 124
      Height = 15
      Caption = #51089#49457#48169#49885#51012' '#49440#53469#54616#49464#50836
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object sBevel2: TsBevel
      Left = 9
      Top = 191
      Width = 401
      Height = 2
      Shape = bsFrame
    end
    object sButton2: TsButton
      Left = 7
      Top = 128
      Width = 403
      Height = 52
      Caption = #49888#44508#46321#47197
      TabOrder = 0
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      CommandLinkHint = #49324#50857#51088#44032' '#51649#51217' '#51077#47141#54633#45768#45796'.'
      Images = DMICON.System24
      ImageIndex = 26
      Style = bsCommandLink
      ContentMargin = 5
    end
    object sButton3: TsButton
      Left = 7
      Top = 204
      Width = 403
      Height = 52
      Caption = #51089#49457#52712#49548
      TabOrder = 1
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      CommandLinkHint = #51089#49457#51012' '#52712#49548#54633#45768#45796'.'
      Images = DMICON.System24
      ImageIndex = 23
      Style = bsCommandLink
      ContentMargin = 5
    end
    object sButton1: TsButton
      Left = 8
      Top = 72
      Width = 401
      Height = 52
      Caption = #50808#54868#54925#46301#50857#50896#47308'('#47932#54408' '#46321') '#44396#47588'('#44277#44553')'#54869#51064#49436#50640#49436' '#49440#53469
      TabOrder = 2
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      CommandLinkHint = #51060#48120' '#51077#47141#46104#50612#51080#45716' '#54869#51064#49436#47484' '#48520#47084#50752' '#51089#49457#54633#45768#45796'.'
      Images = DMICON.System24
      ImageIndex = 25
      Style = bsCommandLink
      ContentMargin = 5
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 304
  end
end
