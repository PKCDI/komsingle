unit Dlg_ErrorMessage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DialogParent, sSkinProvider, ExtCtrls, sPanel, StdCtrls,
  sButton, sMemo, sLabel,Clipbrd, StrUtils;

type
  TDlg_ErrorMessage_frm = class(TDialogParent_frm)
    sMemo1: TsMemo;
    sButton1: TsButton;
    sPanel2: TsPanel;
    sLabel6: TsLabel;
    sButton2: TsButton;
    procedure sButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Run_ErrorMessage(ProgramTitle : string;Msg : string=''):Boolean;    
  end;

var
  Dlg_ErrorMessage_frm: TDlg_ErrorMessage_frm;

implementation

uses ICON;

{$R *.dfm}

{ TDialogParent_frm1 }

function TDlg_ErrorMessage_frm.Run_ErrorMessage(ProgramTitle : string;Msg: string):Boolean;
var
  i : Integer;
  TempStr : String;
begin
  Result := False;
  sLabel6.Caption := '['+ProgramTitle+' 검증]';
  sMemo1.Lines.Text := Msg;

  for i := 0 to sMemo1.Lines.Count-1 do
  begin
    IF i = 0 then begin TempStr := LeftStr( sMemo1.Lines.Strings[i] , 6 ); Continue; end;

    IF TempStr <> LeftStr( sMemo1.Lines.Strings[i] , 6 ) Then
    begin
      TempStr := LeftStr( sMemo1.Lines.Strings[i] , 6 );

      sMemo1.Lines.Insert(i,'---------------------------------------------------------------');
    end;
  end;

  IF Trim(Msg) <> '' Then
  begin
    Result := True;
    Self.ShowModal;
  end;
end;

procedure TDlg_ErrorMessage_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  Clipboard.AsText := sMemo1.Text;
  ShowMessage('복사가 완료되었습니다'#13#10'메모장이나 기타 텍스트편집프로그램에 붙여넣기(Ctrl+v)해주세요');
end;

end.
