inherited UI_INF700_frm: TUI_INF700_frm
  Left = 701
  Top = 158
  Caption = 'UI_INF700_frm'
  ClientWidth = 1114
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sSplitter1: TsSplitter
    Width = 1114
  end
  inherited sSplitter3: TsSplitter
    Width = 1114
  end
  inherited sPageControl1: TsPageControl
    Width = 1114
    OnChange = sPageControl1Change
    inherited sTabSheet1: TsTabSheet
      inherited sSplitter4: TsSplitter
        Width = 1106
      end
      inherited sPanel51: TsPanel
        Width = 806
        inherited sSpeedButton6: TsSpeedButton
          Left = 398
          Top = 0
          Height = 558
        end
        inherited btn_LCNumber1: TsBitBtn [1]
          Left = 602
          Visible = False
        end
        inherited sPanel5: TsPanel [2]
          Left = 15
          Top = 2
          Width = 370
        end
        inherited sPanel6: TsPanel [3]
          Left = 421
          Top = 2
          Width = 370
        end
        inherited sPanel9: TsPanel [4]
          Left = 421
          Top = 384
          Width = 370
        end
        inherited edt_ImpCd2_1: TsEdit [5]
          Left = 156
          Top = 253
          Width = 229
          Color = clWhite
        end
        inherited edt_ImpCd3_1: TsEdit [6]
          Left = 156
          Top = 275
          Width = 229
          Color = clWhite
        end
        inherited btn_LCNumber2: TsBitBtn [7]
          Left = 610
          Top = 249
          Width = 23
          Visible = False
        end
        inherited edt_AdPay1: TsEdit [8]
          Left = 562
          Top = 114
          Width = 229
          Color = clWhite
        end
        inherited edt_ImpCd5_1: TsEdit [9]
          Left = 156
          Top = 319
          Width = 229
          Color = clWhite
        end
        inherited sPanel2: TsPanel [10]
          Left = 15
          Top = 207
          Width = 370
        end
        inherited btn_ImpCd4: TsBitBtn [11]
          Left = 194
          Top = 208
          Visible = False
        end
        inherited btn_ImpCd3: TsBitBtn [12]
          Left = 194
          Top = 208
          Visible = False
        end
        inherited btn_ImpCd2: TsBitBtn [13]
          Left = 186
          Top = 208
          Height = 19
          Visible = False
        end
        inherited btn_OpenMethod: TsBitBtn [14]
          Left = 154
          Top = 24
          Visible = False
        end
        inherited btn_NoticeBank: TsBitBtn [15]
          Top = 26
          Visible = False
        end
        inherited btn_ImpCd1: TsBitBtn [16]
          Left = 130
          Top = 208
          Visible = False
        end
        inherited edt_In_Mathod1: TsEdit [17]
          Left = 156
          Top = 49
          Width = 229
          TabStop = False
          Color = clWhite
        end
        inherited btn_LCNumber4: TsBitBtn [18]
          Left = 602
          Top = 293
          Visible = False
        end
        inherited edt_ImpCd4_1: TsEdit [19]
          Left = 156
          Top = 297
          Width = 229
          Color = clWhite
        end
        inherited btn_ApBank: TsBitBtn [20]
          Left = 114
          Top = 26
          Visible = False
        end
        inherited btn_GrantingCredit: TsBitBtn [21]
          Left = 538
          Top = 114
          Visible = False
        end
        inherited sPanel8: TsPanel [22]
          Left = 15
          Top = 384
          Width = 370
        end
        inherited edt_ImpCd1_1: TsEdit [23]
          Left = 156
          Top = 231
          Width = 229
          Color = clWhite
        end
        inherited btn_LCNumber5: TsBitBtn [24]
          Left = 611
          Top = 315
          Visible = False
        end
        inherited btn_LCNumber3: TsBitBtn [25]
          Left = 602
          Top = 271
          Visible = False
        end
        inherited sPanel7: TsPanel [26]
          Left = 421
          Top = 207
          Width = 370
        end
        inherited btn_ImpCd5: TsBitBtn [27]
          Left = 242
          Top = 208
          Visible = False
        end
        inherited edt_Datee: TsMaskEdit
          Left = 106
          Top = 26
          Color = clWhite
          BoundLabel.Font.Style = []
        end
        inherited edt_In_Mathod: TsEdit
          Left = 106
          Top = 48
          Color = clWhite
          BoundLabel.Font.Style = []
        end
        inherited edt_ApBank: TsEdit
          Left = 106
          Top = 70
          Color = clWhite
          BoundLabel.Font.Style = []
        end
        inherited edt_ApBank1: TsEdit
          Left = 156
          Top = 71
          Width = 229
        end
        inherited edt_ApBank2: TsEdit
          Left = 156
          Top = 93
          Width = 229
        end
        inherited edt_ApBank3: TsEdit
          Left = 156
          Top = 115
          Width = 229
        end
        inherited edt_ApBank4: TsEdit
          Left = 156
          Top = 137
          Width = 229
        end
        inherited edt_ApBank5: TsEdit
          Left = 156
          Top = 159
          Width = 229
          Height = 19
        end
        inherited edt_AdBank: TsEdit
          Left = 560
          Top = 48
          Visible = False
        end
        inherited edt_AdBank1: TsEdit
          Left = 512
          Top = 26
          Width = 279
        end
        inherited edt_AdBank2: TsEdit
          Left = 512
          Top = 48
          Width = 279
        end
        inherited edt_AdBank3: TsEdit
          Left = 512
          Top = 70
          Width = 279
        end
        inherited edt_AdBank4: TsEdit
          Left = 512
          Top = 92
          Width = 279
        end
        inherited edt_AdPay: TsEdit
          Left = 512
          Top = 114
        end
        inherited edt_ImpCd1: TsEdit
          Left = 106
          Top = 231
        end
        inherited edt_ImpCd2: TsEdit
          Left = 106
          Top = 253
        end
        inherited edt_ImpCd3: TsEdit
          Left = 106
          Top = 275
        end
        inherited edt_ImpCd4: TsEdit
          Left = 106
          Top = 297
        end
        inherited edt_ImpCd5: TsEdit
          Left = 106
          Top = 319
        end
        inherited edt_ILno1: TsEdit
          Left = 471
          Top = 231
        end
        inherited edt_ILCur1: TsEdit
          Left = 655
          Top = 231
        end
        inherited edt_ILAMT1: TsCurrencyEdit
          Left = 706
          Top = 231
          DisplayFormat = '#,0.###;0'
        end
        inherited edt_ILno2: TsEdit
          Left = 471
          Top = 253
        end
        inherited edt_ILCur2: TsEdit
          Left = 655
          Top = 253
        end
        inherited edt_ILAMT2: TsCurrencyEdit
          Left = 706
          Top = 253
          DisplayFormat = '#,0.###;0'
        end
        inherited edt_ILno3: TsEdit
          Left = 471
          Top = 275
        end
        inherited edt_ILCur3: TsEdit
          Left = 655
          Top = 275
        end
        inherited edt_ILAMT3: TsCurrencyEdit
          Left = 706
          Top = 275
          DisplayFormat = '#,0.###;0'
        end
        inherited edt_ILno4: TsEdit
          Left = 471
          Top = 297
        end
        inherited edt_ILCur4: TsEdit
          Left = 655
          Top = 297
        end
        inherited edt_ILAMT4: TsCurrencyEdit
          Left = 706
          Top = 297
          DisplayFormat = '#,0.###;0'
        end
        inherited edt_ILno5: TsEdit
          Left = 471
          Top = 319
        end
        inherited edt_ILCur5: TsEdit
          Left = 655
          Top = 319
        end
        inherited edt_ILAMT5: TsCurrencyEdit
          Left = 706
          Top = 319
          DisplayFormat = '#,0.###;0'
        end
        inherited edt_AdInfo1: TsEdit
          Left = 106
          Top = 410
          Width = 279
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#51221#48372
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited edt_AdInfo2: TsEdit
          Left = 106
          Top = 432
          Width = 279
        end
        inherited edt_AdInfo3: TsEdit
          Left = 106
          Top = 454
          Width = 279
        end
        inherited edt_AdInfo4: TsEdit
          Left = 106
          Top = 476
          Width = 279
        end
        inherited edt_AdInfo5: TsEdit
          Left = 106
          Top = 498
          Width = 279
        end
        inherited edt_EXName1: TsEdit
          Left = 552
          Top = 410
          Width = 239
          Color = clWhite
          BoundLabel.Font.Style = []
        end
        inherited edt_EXName2: TsEdit
          Left = 552
          Top = 432
          Width = 239
        end
        inherited edt_EXName3: TsEdit
          Left = 552
          Top = 454
        end
        inherited edt_EXAddr1: TsEdit
          Left = 552
          Top = 476
          Width = 239
        end
        inherited edt_EXAddr2: TsEdit
          Left = 552
          Top = 498
          Width = 239
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sSplitter7: TsSplitter
        Width = 1106
      end
      inherited sPanel21: TsPanel
        Width = 806
        inherited sLabel9: TsLabel
          Left = 582
          Top = 215
        end
        object sSpeedButton11: TsSpeedButton [1]
          Left = 398
          Top = 0
          Width = 10
          Height = 558
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        inherited sPanel22: TsPanel [2]
          Left = 421
          Top = 244
          Width = 370
          TabOrder = 36
          inherited sLabel3: TsLabel
            Left = 421
          end
        end
        inherited sPanel24: TsPanel [3]
          Left = 421
          Top = 191
          Width = 370
          TabOrder = 34
          inherited sLabel5: TsLabel
            Left = 421
          end
        end
        inherited edt_Cdamt: TsCurrencyEdit [4]
          Left = 520
          Top = 160
          Width = 271
          TabOrder = 20
        end
        inherited edt_Benefc: TsEdit [5]
          Left = 597
          Top = 52
          TabOrder = 33
          Visible = False
          BoundLabel.Font.Height = -12
        end
        inherited edt_exPlace: TsEdit [6]
          Left = 145
          Top = 296
          Width = 240
          Color = clWhite
          TabOrder = 6
          BoundLabel.Active = False
          BoundLabel.Caption = ''
        end
        inherited btn_Doccd: TsBitBtn [7]
          Left = 132
          Top = 85
          Height = 15
          TabOrder = 43
          Visible = False
        end
        inherited edt_TermPR_M: TsEdit [8]
          Left = 520
          Top = 319
          Width = 271
          TabOrder = 25
        end
        inherited edt_CdMax: TsEdit [9]
          Left = 478
          Top = 266
          Width = 41
          TabOrder = 23
        end
        inherited edt_Benefc3: TsEdit [10]
          Left = 478
          Top = 68
          Width = 313
          TabOrder = 16
        end
        inherited sPanel20: TsPanel [11]
          Left = 421
          Top = 138
          Width = 370
          TabOrder = 37
        end
        inherited sPanel15: TsPanel [12]
          Left = 15
          Top = 2
          Width = 370
          SkinData.CustomFont = True
          Caption = 'Date of Issue('#44060#49444#51068')'
          TabOrder = 42
        end
        inherited edt_Doccd: TsEdit [13]
          Left = 72
          Top = 92
          Width = 41
          TabOrder = 32
          SkinData.CustomColor = False
        end
        inherited edt_CdMax1: TsEdit [14]
          Left = 520
          Top = 266
          Width = 271
          Color = clWhite
          TabOrder = 35
        end
        inherited edt_Doccd1: TsEdit [15]
          Left = 114
          Top = 92
          Width = 271
          Color = clWhite
          TabOrder = 2
        end
        inherited btn_MaximumCreditCode: TsBitBtn [16]
          Left = 542
          Top = 264
          Height = 16
          TabOrder = 44
          Visible = False
        end
        inherited btn_TermsOfPrice: TsBitBtn [17]
          Left = 726
          Top = 321
          Height = 17
          TabOrder = 45
          Visible = False
        end
        inherited edt_Benefc1: TsEdit [18]
          Left = 478
          Top = 24
          Width = 313
          TabOrder = 14
        end
        inherited edt_Cdcur: TsEdit [19]
          Left = 478
          Top = 160
          Width = 41
          Color = clWhite
          TabOrder = 19
        end
        inherited edt_Applic4: TsEdit [20]
          Left = 72
          Top = 506
          Width = 313
          TabOrder = 12
        end
        inherited edt_exDate: TsMaskEdit [21]
          Left = 72
          Top = 296
          Width = 72
          Color = clWhite
          TabOrder = 5
          BoundLabel.Active = False
          BoundLabel.Caption = ''
        end
        inherited edt_cdPerP: TsCurrencyEdit [22]
          Left = 478
          Top = 213
          Width = 39
          TabOrder = 21
          BoundLabel.Active = False
          BoundLabel.Caption = ''
        end
        inherited edt_Applic3: TsEdit [23]
          Left = 72
          Top = 484
          Width = 313
          TabOrder = 11
        end
        inherited edt_Benefc4: TsEdit [24]
          Left = 478
          Top = 90
          Width = 313
          TabOrder = 17
        end
        inherited edt_Benefc5: TsEdit [25]
          Left = 478
          Top = 112
          Width = 313
          TabOrder = 18
          BoundLabel.Font.Color = clBlack
        end
        inherited btn_Beneficiary: TsBitBtn [26]
          Left = 563
          Top = 28
          Height = 15
          TabOrder = 48
          Visible = False
        end
        inherited edt_Benefc2: TsEdit [27]
          Left = 478
          Top = 46
          Width = 313
          TabOrder = 15
        end
        inherited sPanel26: TsPanel [28]
          Left = 421
          Top = 297
          Width = 370
          inherited sLabel7: TsLabel
            Left = 421
          end
        end
        inherited btn_CurrencyCode: TsBitBtn [29]
          Left = 622
          Top = 158
          TabOrder = 38
          Visible = False
        end
        inherited edt_Applic2: TsEdit [30]
          Left = 72
          Top = 462
          Width = 313
          TabOrder = 10
        end
        inherited sPanel17: TsPanel [31]
          Left = 15
          Top = 418
          Width = 370
          TabOrder = 39
        end
        inherited edt_Applic5: TsEdit [32]
          Left = 72
          Top = 528
          Width = 313
          TabOrder = 13
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
        end
        inherited sPanel12: TsPanel [33]
          Left = 421
          Top = 403
          Width = 370
          Caption = 'Applicant Bank'
          TabOrder = 47
        end
        inherited sPanel11: TsPanel [34]
          Left = 421
          Top = 350
          Width = 370
          TabOrder = 46
        end
        inherited edt_Applic1: TsEdit [35]
          Left = 72
          Top = 440
          Width = 313
          Color = clWhite
          TabOrder = 9
          BoundLabel.Active = False
          BoundLabel.Caption = ''
        end
        inherited edt_cdPerM: TsCurrencyEdit [36]
          Left = 541
          Top = 213
          Width = 39
          TabOrder = 22
          BoundLabel.Caption = '/ '
          BoundLabel.Font.Height = -15
          BoundLabel.Font.Style = []
        end
        inherited sPanel16: TsPanel [37]
          Left = 15
          Top = 274
          Width = 370
          SkinData.CustomFont = True
          TabOrder = 41
        end
        inherited sPanel19: TsPanel [38]
          Left = 421
          Top = 2
          Width = 370
          TabOrder = 40
        end
        inherited edt_TermPR: TsEdit [39]
          Left = 478
          Top = 319
          Width = 41
          Color = clWhite
          TabOrder = 24
        end
        inherited edt_plTerm: TsEdit [40]
          Left = 478
          Top = 372
          Width = 313
          TabOrder = 26
        end
        inherited edt_aaCcv1: TsEdit
          Left = 478
          Top = 425
          Width = 313
          TabOrder = 27
        end
        inherited edt_aaCcv2: TsEdit
          Left = 478
          Top = 447
          Width = 313
          TabOrder = 28
        end
        inherited edt_aaCcv3: TsEdit
          Left = 478
          Top = 469
          Width = 313
          TabOrder = 29
        end
        inherited edt_aaCcv4: TsEdit
          Left = 478
          Top = 491
          Width = 313
          TabOrder = 30
        end
        object edt_dateOfIssue: TsMaskEdit
          Left = 72
          Top = 24
          Width = 78
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 1
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object edt_docCredutNo: TsEdit
          Left = 72
          Top = 160
          Width = 313
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object edt_refPreAdvice: TsEdit
          Left = 72
          Top = 228
          Width = 313
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel54: TsPanel
          Left = 15
          Top = 342
          Width = 370
          Height = 21
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Applicable Rules'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 49
        end
        object edt_appRules1: TsEdit
          Left = 72
          Top = 364
          Width = 313
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
        end
        object edt_appRules2: TsEdit
          Left = 72
          Top = 386
          Width = 313
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AppAccnt: TsEdit
          Left = 478
          Top = 535
          Width = 223
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 31
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel59: TsPanel
          Left = 15
          Top = 138
          Width = 370
          Height = 21
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Documentary Credit Number'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 50
        end
        object sPanel60: TsPanel
          Left = 15
          Top = 70
          Width = 370
          Height = 21
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'FormofDocumentary Credit'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 51
        end
        object sPanel61: TsPanel
          Left = 15
          Top = 206
          Width = 370
          Height = 21
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Reference to Pre-Advice'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 52
        end
        object edt_APPBANK5: TsEdit
          Left = 478
          Top = 513
          Width = 223
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 53
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sSplitter5: TsSplitter
        Width = 1106
      end
      inherited sPanel14: TsPanel
        Width = 806
        inherited sSpeedButton1: TsSpeedButton
          Left = 398
          Top = 0
          Height = 558
        end
        inherited sLabel16: TsLabel
          Left = 576
          Top = 475
          Visible = False
        end
        inherited sPanel28: TsPanel
          Left = 421
          Top = 2
          Width = 370
          Caption = 'Drafts at ... '
          Ctl3D = False
          ParentCtl3D = False
        end
        inherited edt_Draft1: TsEdit
          Left = 421
          Top = 26
          Width = 370
          TabOrder = 14
        end
        inherited edt_Draft3: TsEdit
          Left = 421
          Top = 70
          Width = 370
          TabOrder = 16
        end
        inherited edt_Draft2: TsEdit
          Left = 421
          Top = 48
          Width = 370
          TabOrder = 15
        end
        inherited sPanel29: TsPanel
          Left = 15
          Top = 183
          Width = 370
          Caption = 'Mixed Payment Detail'
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 18
        end
        inherited edt_mixPay1: TsEdit
          Left = 15
          Top = 207
          Width = 370
          TabOrder = 5
        end
        inherited edt_mixPay2: TsEdit
          Left = 15
          Top = 229
          Width = 370
          TabOrder = 6
        end
        inherited edt_mixPay3: TsEdit
          Left = 15
          Top = 251
          Width = 370
          TabOrder = 7
        end
        inherited edt_mixPay4: TsEdit
          Left = 15
          Top = 273
          Width = 370
          TabOrder = 8
        end
        inherited sPanel30: TsPanel
          Left = 421
          Top = 183
          Width = 370
          Caption = 'Deferred Payment Details'
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 28
        end
        inherited edt_defPay1: TsEdit
          Left = 421
          Top = 207
          Width = 370
          TabOrder = 17
        end
        inherited edt_defPay2: TsEdit
          Left = 421
          Top = 229
          Width = 370
          TabOrder = 19
        end
        inherited edt_defPay3: TsEdit
          Left = 421
          Top = 251
          Width = 370
          TabOrder = 20
        end
        inherited edt_defPay4: TsEdit
          Left = 421
          Top = 273
          Width = 370
          TabOrder = 21
        end
        inherited sPanel10: TsPanel
          Left = 421
          Top = 364
          Width = 370
          Caption = 'Sender to Receive Information'
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 29
        end
        inherited btn_DescriptionServices: TsBitBtn
          Left = 506
          Top = 476
          TabOrder = 30
          Visible = False
        end
        inherited memo_Desgood1: TsMemo
          Left = 504
          Top = 480
          Width = 209
          Height = 17
          TabOrder = 27
        end
        object sPanel55: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Additional Amount Covered'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 31
        end
        object edt_Aacv1: TsEdit
          Left = 15
          Top = 26
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Aacv2: TsEdit
          Left = 15
          Top = 48
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Aacv3: TsEdit
          Left = 15
          Top = 70
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Aacv4: TsEdit
          Left = 15
          Top = 92
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object panel_advise: TsPanel
          Left = 15
          Top = 364
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = '"Advise Through" Bank'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
        end
        object edt_Avtbank1: TsEdit
          Left = 80
          Top = 388
          Width = 305
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Avtbank2: TsEdit
          Left = 80
          Top = 410
          Width = 305
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Avtbank3: TsEdit
          Left = 80
          Top = 432
          Width = 305
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Avtbank4: TsEdit
          Left = 80
          Top = 454
          Width = 305
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AvtAccnt: TsEdit
          Left = 80
          Top = 476
          Width = 305
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_sndInfo1: TsEdit
          Left = 421
          Top = 388
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 22
          SkinData.SkinSection = 'EDIT'
        end
        object edt_sndInfo2: TsEdit
          Left = 421
          Top = 410
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 23
          SkinData.SkinSection = 'EDIT'
        end
        object edt_sndInfo3: TsEdit
          Left = 421
          Top = 432
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 24
          SkinData.SkinSection = 'EDIT'
        end
        object edt_sndInfo4: TsEdit
          Left = 421
          Top = 454
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 25
          SkinData.SkinSection = 'EDIT'
        end
        object edt_sndInfo5: TsEdit
          Left = 421
          Top = 476
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 26
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet4: TsTabSheet
      inherited sSplitter8: TsSplitter
        Width = 1106
      end
      inherited sPanel31: TsPanel
        Width = 806
        DesignSize = (
          806
          559)
        object sSpeedButton7: TsSpeedButton
          Left = 398
          Top = 0
          Width = 10
          Height = 558
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel32: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Available With ... By ...'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
        end
        object edt_Avail1: TsEdit
          Left = 96
          Top = 26
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Avail2: TsEdit
          Left = 96
          Top = 48
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Avail3: TsEdit
          Left = 96
          Top = 70
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Avail4: TsEdit
          Left = 96
          Top = 92
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AvAccnt: TsEdit
          Left = 96
          Top = 136
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel33: TsPanel
          Left = 421
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Drawee'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
        end
        object edt_Drawee1: TsEdit
          Left = 502
          Top = 26
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Drawee2: TsEdit
          Left = 502
          Top = 48
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Drawee3: TsEdit
          Left = 502
          Top = 70
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Drawee4: TsEdit
          Left = 502
          Top = 92
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object edt_DrAccnt: TsEdit
          Left = 502
          Top = 114
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_Avail5: TsEdit
          Left = 96
          Top = 114
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AvPay: TsEdit
          Left = 96
          Top = 158
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109#51648#44553#48169#49885
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel34: TsPanel
          Left = 15
          Top = 238
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Reimbursement Bank'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
        end
        object edt_ReiBank1: TsEdit
          Left = 96
          Top = 262
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ReiBank2: TsEdit
          Left = 96
          Top = 284
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ReiBank3: TsEdit
          Left = 96
          Top = 306
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 15
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ReiBank4: TsEdit
          Left = 96
          Top = 328
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ReiAccnt: TsEdit
          Left = 96
          Top = 372
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 18
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_ReiBank5: TsEdit
          Left = 96
          Top = 350
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 17
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel35: TsPanel
          Left = 421
          Top = 238
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #44060#49444#51008#54665
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 27
        end
        object edt_OpBank1: TsEdit
          Left = 502
          Top = 262
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 19
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OpBank2: TsEdit
          Left = 502
          Top = 284
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 20
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OpBank3: TsEdit
          Left = 502
          Top = 306
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 21
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OpAddr1: TsEdit
          Left = 502
          Top = 328
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 22
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_OpAddr2: TsEdit
          Left = 502
          Top = 350
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 23
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OpAddr3: TsEdit
          Left = 502
          Top = 372
          Width = 289
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 24
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet5: TsTabSheet
      inherited sSplitter6: TsSplitter
        Width = 1106
      end
      inherited sPanel43: TsPanel
        Tag = 300
        Width = 806
        object sSpeedButton9: TsSpeedButton
          Left = 398
          Top = -1
          Width = 10
          Height = 558
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel57: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Partial Shipment'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
        end
        object edt_pShip: TsEdit
          Left = 88
          Top = 26
          Width = 49
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel58: TsPanel
          Left = 15
          Top = 88
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Transhipment'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
        end
        object edt_tShip: TsEdit
          Left = 88
          Top = 112
          Width = 49
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel39: TsPanel
          Left = 15
          Top = 174
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Port of Loading/Airport of Departure'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
        end
        object edt_SunjuckPort: TsEdit
          Left = 88
          Top = 198
          Width = 297
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sPanel40: TsPanel
          Tag = 300
          Left = 15
          Top = 260
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Port of Discharge/Airport of Destination'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
        end
        object edt_DochakPort: TsEdit
          Tag = 300
          Left = 88
          Top = 284
          Width = 297
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #46020#52265#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sPanel41: TsPanel
          Left = 15
          Top = 346
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Place of Taking in Charge/Dispatch from '#183#183#183'/Place of Receipt'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
        end
        object edt_LoadOn: TsEdit
          Left = 15
          Top = 370
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel47: TsPanel
          Left = 15
          Top = 432
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 
            'Place of Final Destination/For Transportation to '#183#183#183'/Place of De' +
            'livery'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
        end
        object edt_ForTran: TsEdit
          Left = 15
          Top = 456
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Origin: TsEdit
          Left = 496
          Top = 306
          Width = 47
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OriginM: TsEdit
          Left = 544
          Top = 306
          Width = 247
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel62: TsPanel
          Left = 421
          Top = 282
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Country of Origin'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
        end
        object edt_ShipPd6: TsEdit
          Left = 421
          Top = 222
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ShipPd5: TsEdit
          Left = 421
          Top = 200
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ShipPd4: TsEdit
          Left = 421
          Top = 178
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ShipPd3: TsEdit
          Left = 421
          Top = 156
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ShipPd2: TsEdit
          Left = 421
          Top = 134
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ShipPd1: TsEdit
          Left = 421
          Top = 112
          Width = 370
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel38: TsPanel
          Left = 421
          Top = 88
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Shipment Period'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
        end
        object edt_LstDate: TsMaskEdit
          Left = 502
          Top = 26
          Width = 77
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#51333#49440#51201#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel37: TsPanel
          Left = 421
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Latest Date of Shipment'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
        end
        object edt_pShipName: TsEdit
          Left = 138
          Top = 26
          Width = 247
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
          SkinData.SkinSection = 'EDIT'
        end
        object edt_tShipName: TsEdit
          Left = 138
          Top = 112
          Width = 247
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object sTabSheet8: TsTabSheet [5]
      Caption = 'Page6'
      object sSplitter10: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel36: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alLeft
        BevelOuter = bvNone
        
        TabOrder = 0
      end
      object sPanel63: TsPanel
        Left = 300
        Top = 3
        Width = 806
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 1
        object sLabel33: TsLabel
          Left = 167
          Top = 62
          Width = 352
          Height = 15
          Caption = 'OF CLEAN ON BOARD OCEAN BILLIS OF LADING MADE OUT TO'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel37: TsLabel
          Left = 448
          Top = 110
          Width = 70
          Height = 15
          Caption = 'AND NOTIFY'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel36: TsLabel
          Left = 64
          Top = 110
          Width = 97
          Height = 15
          Caption = 'MARKED FREIGHT'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel40: TsLabel
          Left = 80
          Top = 219
          Width = 82
          Height = 15
          Caption = 'THE ORDER OF'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel43: TsLabel
          Left = 448
          Top = 243
          Width = 70
          Height = 15
          Caption = 'AND NOTIFY'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel41: TsLabel
          Left = 64
          Top = 243
          Width = 97
          Height = 15
          Caption = 'MARKED FREIGHT'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel31: TsLabel
          Left = 328
          Top = 34
          Width = 40
          Height = 15
          Caption = 'COPIES'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel45: TsLabel
          Left = 256
          Top = 391
          Width = 146
          Height = 17
          Caption = #52572#45824' 400'#54665' 64'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel44: TsLabel
          Left = 216
          Top = 343
          Width = 40
          Height = 15
          Caption = 'COPIES'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel38: TsLabel
          Left = 78
          Top = 295
          Width = 638
          Height = 15
          Caption = 
            'EXPRESSLY STIPULATING THAT CLAIMS ARE PAYABLE IN KOREA AND IT MU' +
            'ST INCLUDE : INSTITUTE CARGO CLAUSE'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel39: TsLabel
          Left = 80
          Top = 166
          Width = 82
          Height = 15
          Caption = 'THE ORDER OF'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel42: TsLabel
          Left = 448
          Top = 167
          Width = 70
          Height = 15
          Caption = 'AND NOTIFY'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel32: TsLabel
          Left = 111
          Top = 62
          Width = 4
          Height = 15
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel35: TsLabel
          Left = 77
          Top = 86
          Width = 4
          Height = 15
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sPanel56: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Documents Required ...'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object check_Doc380: TsCheckBox
          Left = 56
          Top = 34
          Width = 232
          Height = 19
          Caption = 'ASSIGNED COMMERCIAL INVOICE IN'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          SkinData.SkinSection = 'CHECKBOX'
          ReadOnly = True
        end
        object edt_Doc380_1: TsEdit
          Left = 288
          Top = 33
          Width = 33
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc705Gubun: TsEdit
          Left = 77
          Top = 60
          Width = 33
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc705_1: TsEdit
          Left = 165
          Top = 84
          Width = 303
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_Doc705: TsCheckBox
          Left = 56
          Top = 61
          Width = 20
          Height = 16
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'CHECKBOX'
          ReadOnly = True
        end
        object edt_Doc705_3: TsEdit
          Left = 165
          Top = 108
          Width = 33
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc705_3_1: TsEdit
          Left = 199
          Top = 108
          Width = 245
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc705_2: TsEdit
          Left = 469
          Top = 84
          Width = 303
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc705_4: TsEdit
          Left = 524
          Top = 108
          Width = 248
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc740_1: TsEdit
          Left = 245
          Top = 140
          Width = 260
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc740_2: TsEdit
          Left = 506
          Top = 140
          Width = 266
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc740_4: TsEdit
          Left = 524
          Top = 164
          Width = 248
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc740_3_1: TsEdit
          Left = 199
          Top = 164
          Width = 245
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc740_3: TsEdit
          Left = 165
          Top = 164
          Width = 33
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_Doc740: TsCheckBox
          Left = 56
          Top = 141
          Width = 189
          Height = 19
          Caption = 'AIRWAY BILL CONSIGNED TO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 15
          SkinData.SkinSection = 'CHECKBOX'
          ReadOnly = True
        end
        object check_Doc760: TsCheckBox
          Left = 56
          Top = 194
          Width = 442
          Height = 19
          Caption = 'FULL SET OF CLEAN MULTIMODAL TRANSPORT DOCUMENT MADE OUT TO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          SkinData.SkinSection = 'CHECKBOX'
          ReadOnly = True
        end
        object edt_Doc760_1: TsEdit
          Left = 165
          Top = 217
          Width = 303
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc760_2: TsEdit
          Left = 469
          Top = 217
          Width = 303
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc760_4: TsEdit
          Left = 524
          Top = 241
          Width = 248
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc760_3_1: TsEdit
          Left = 199
          Top = 241
          Width = 245
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc760_3: TsEdit
          Left = 165
          Top = 241
          Width = 33
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_doc530: TsCheckBox
          Left = 55
          Top = 274
          Width = 637
          Height = 19
          Caption = 
            'FULL SET OF INSURANCE POLICIES OR CERTIFICATES, ENDORSED'#13#10'IN BLA' +
            'NK FOR 110% OF THE INVOICE VALUE,'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 22
          SkinData.SkinSection = 'CHECKBOX'
          ReadOnly = True
        end
        object edt_doc530_1: TsEdit
          Left = 77
          Top = 312
          Width = 316
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_doc530_2: TsEdit
          Left = 394
          Top = 312
          Width = 316
          Height = 21
          TabStop = False
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_doc271_1: TsEdit
          Left = 176
          Top = 339
          Width = 33
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_doc271: TsCheckBox
          Left = 55
          Top = 340
          Width = 120
          Height = 19
          Caption = 'PACKING LIST IN'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 26
          SkinData.SkinSection = 'CHECKBOX'
          ReadOnly = True
        end
        object check_doc861: TsCheckBox
          Left = 55
          Top = 364
          Width = 157
          Height = 19
          Caption = 'CRETIFICATE OF ORIGIN'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
          SkinData.SkinSection = 'CHECKBOX'
          ReadOnly = True
        end
        object check_doc2AA: TsCheckBox
          Left = 55
          Top = 388
          Width = 200
          Height = 19
          Caption = 'OTHER DOCUMENT(S) ( if any )'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 28
          SkinData.SkinSection = 'CHECKBOX'
          ReadOnly = True
        end
        object memo_Doc2AA_1: TsMemo
          Left = 110
          Top = 410
          Width = 510
          Height = 143
          TabStop = False
          MaxLength = 25600
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 29
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet6: TsTabSheet
      Caption = 'Page7'
      inherited sSplitter9: TsSplitter
        Width = 1106
      end
      inherited sPanel45: TsPanel
        Width = 806
        inherited sLabel46: TsLabel
          Left = 152
          Top = 173
          Visible = False
        end
        inherited sLabel48: TsLabel
          Left = 32
          Top = 302
        end
        inherited sLabel50: TsLabel
          Left = 32
          Top = 321
          Width = 250
          Caption = 'OUTSIDE KOREA ARE FOR ACCOUNT OF '
        end
        inherited sLabel52: TsLabel
          Top = 368
        end
        inherited sLabel53: TsLabel
          Left = 327
          Top = 368
        end
        inherited sLabel54: TsLabel
          Top = 387
        end
        inherited sBitBtn34: TsBitBtn [6]
          Left = 151
          TabOrder = 13
          Visible = False
        end
        inherited sBitBtn36: TsBitBtn [7]
          Left = 161
          Top = 410
          TabOrder = 18
          Visible = False
        end
        inherited sBitBtn35: TsBitBtn [8]
          Left = 335
          Top = 317
          TabOrder = 15
          Visible = False
        end
        inherited check_acd2AA: TsCheckBox [9]
          Top = 31
        end
        inherited sPanel46: TsPanel [10]
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          Ctl3D = False
          ParentCtl3D = False
        end
        inherited edt_acd2AA1: TsEdit [11]
          Top = 31
          Color = clWindow
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
        end
        inherited check_acd2AB: TsCheckBox [12]
          Top = 52
        end
        inherited check_acd2AC: TsCheckBox [13]
          Top = 72
        end
        inherited check_acd2AD: TsCheckBox [14]
          Top = 93
        end
        inherited check_acd2AE: TsCheckBox [15]
          Top = 114
        end
        inherited memo_acd2AE1: TsMemo [16]
          Top = 134
          Width = 510
          Height = 58
          TabOrder = 12
        end
        inherited sPanel48: TsPanel [17]
          Left = 15
          Top = 279
          Width = 370
          Height = 23
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 14
        end
        inherited edt_Charge: TsEdit [18]
          Left = 285
          Top = 319
          Color = clWhite
          TabOrder = 7
        end
        inherited sPanel49: TsPanel [19]
          Left = 15
          Top = 342
          Width = 370
          Height = 23
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 16
        end
        inherited edt_period: TsEdit [20]
          Left = 290
          Top = 366
          TabOrder = 8
        end
        inherited edt_Confirm: TsEdit
          Left = 110
          Top = 430
          TabOrder = 9
        end
        inherited sPanel50: TsPanel [22]
          Left = 15
          Top = 406
          Width = 370
          Height = 23
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 17
        end
        inherited edt_Charge1: TsEdit
          Left = 319
          Top = 319
          Color = clWhite
          TabOrder = 19
        end
        inherited edt_Confirm1: TsEdit
          Left = 144
          Top = 430
          Width = 241
          Color = clWhite
          TabOrder = 20
        end
        object sPanel64: TsPanel
          Left = 15
          Top = 195
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Instructions to the ...'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
        end
        object memo_Instrct1: TsMemo
          Left = 147
          Top = 219
          Width = 510
          Height = 58
          Ctl3D = True
          MaxLength = 25600
          ParentCtl3D = False
          ScrollBars = ssVertical
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel65: TsPanel
          Left = 15
          Top = 459
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Description of Goods and/or Services'
          Color = 16042877
          Ctl3D = False
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
        end
        object memo_INFDesGood1: TsMemo
          Left = 147
          Top = 483
          Width = 510
          Height = 74
          MaxLength = 25600
          ScrollBars = ssVertical
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet7: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      inherited sSplitter2: TsSplitter
        Width = 1106
      end
      inherited sDBGrid1: TsDBGrid
        Width = 1106
        DataSource = dsList
        OnDrawColumnCell = sDBGrid2DrawColumnCell
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 180
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#49888#52397#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CD_NO'
            Title.Alignment = taCenter
            Title.Caption = 'L/C'#48264#54840
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51032#47280#51008#54665
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CD_AMT'
            Title.Alignment = taCenter
            Title.Caption = #44552#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CD_CUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 52
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #49688#49888#51088
            Width = 52
            Visible = True
          end>
      end
      inherited sPanel3: TsPanel
        Width = 1106
        inherited edt_SearchText: TsEdit
          Height = 23
          TabOrder = 4
        end
        inherited com_SearchKeyword: TsComboBox
          TabOrder = 2
          TabStop = False
          OnSelect = com_SearchKeywordSelect
        end
        inherited Mask_SearchDate1: TsMaskEdit
          Height = 23
          TabOrder = 0
          OnDblClick = Mask_SearchDate1DblClick
        end
        inherited sBitBtn1: TsBitBtn
          TabOrder = 3
          OnClick = sBitBtn1Click
        end
        inherited sBitBtn21: TsBitBtn
          Tag = 902
          TabOrder = 5
          TabStop = False
          OnClick = btn_CalClick
        end
        inherited Mask_SearchDate2: TsMaskEdit
          Height = 23
          TabOrder = 1
          OnDblClick = Mask_SearchDate1DblClick
        end
        inherited sBitBtn23: TsBitBtn
          Tag = 903
          TabOrder = 6
          TabStop = False
          OnClick = btn_CalClick
        end
      end
    end
  end
  inherited sPanel1: TsPanel
    Width = 1114
    inherited sSpeedButton2: TsSpeedButton
      Left = 226
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 640
      Visible = False
    end
    inherited sSpeedButton4: TsSpeedButton
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 628
      Visible = False
    end
    object sSpeedButton8: TsSpeedButton [4]
      Left = 486
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel [5]
      Left = 8
      Top = 5
      Width = 197
      Height = 17
      Caption = #52712#49548#48520#45733#54868#54872' '#49888#50857#51109' '#44060#49444' '#51025#45813#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel [6]
      Left = 8
      Top = 20
      Width = 36
      Height = 13
      Caption = 'INF700'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton10: TsSpeedButton [7]
      Left = 209
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    inherited btnExit: TsButton
      Left = 1039
      OnClick = btnExitClick
    end
    inherited btnNew: TsButton
      Left = 899
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 839
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 219
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      Left = 710
      Visible = False
    end
    inherited btnPrint: TsButton
      Left = 774
      Visible = False
    end
    inherited btnTemp: TsButton
      Left = 705
      Visible = False
    end
    inherited btnSave: TsButton
      Left = 701
      Visible = False
    end
    inherited btnCancel: TsButton
      Left = 705
      Visible = False
    end
    inherited sButton1: TsButton
      Visible = False
    end
    inherited sButton3: TsButton
      Visible = False
    end
    object sButton2: TsButton
      Tag = 1
      Left = 385
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 11
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
    object sButton4: TsButton
      Left = 286
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 12
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
  end
  inherited sPanel4: TsPanel
    Width = 1114
    inherited sCheckBox1: TsCheckBox [0]
    end
    inherited edt_UserNo: TsEdit [1]
      Color = clWhite
    end
    inherited edt_msg1: TsEdit [2]
      Color = clWhite
    end
    inherited sBitBtn2: TsBitBtn [3]
      Left = 674
      Visible = False
    end
    inherited edt_MaintNo: TsEdit [4]
      Color = clWhite
      BoundLabel.Font.Color = clBlack
    end
    inherited btn_Cal: TsBitBtn [5]
      OnClick = btn_CalClick
    end
    inherited sMaskEdit1: TsMaskEdit [6]
      OnDblClick = Mask_SearchDate1DblClick
    end
    inherited edt_msg2: TsEdit
      Left = 601
      Color = clWhite
    end
    inherited sBitBtn3: TsBitBtn
      Left = 698
      Visible = False
    end
  end
  inherited sPanel52: TsPanel
    Height = 652
    inherited sDBGrid2: TsDBGrid
      Height = 617
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OnDrawColumnCell = sDBGrid2DrawColumnCell
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 204
          Visible = True
        end>
    end
    inherited sPanel53: TsPanel
      inherited Mask_fromDate: TsMaskEdit
        TabStop = True
      end
      inherited sBitBtn22: TsBitBtn
        TabOrder = 2
        TabStop = True
        OnClick = sBitBtn22Click
      end
      inherited Mask_toDate: TsMaskEdit
        TabStop = True
        TabOrder = 1
      end
    end
  end
  inherited qryList: TADOQuery
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      ''
      
        'SELECT I700_1.MAINT_NO,I700_1.MESSAGE1,I700_1.MESSAGE2,I700_1.[U' +
        'SER_ID],I700_1.DATEE,I700_1.APP_DATE,I700_1.IN_MATHOD,I700_1.AP_' +
        'BANK,I700_1.AP_BANK1,I700_1.AP_BANK2,I700_1.AP_BANK3,I700_1.AP_B' +
        'ANK4,I700_1.AP_BANK5,I700_1.AD_BANK,I700_1.AD_BANK1'
      
        '      ,I700_1.AD_BANK2,I700_1.AD_BANK3,I700_1.AD_BANK4,I700_1.AD' +
        '_PAY,I700_1.IL_NO1,I700_1.IL_NO2,I700_1.IL_NO3,I700_1.IL_NO4,I70' +
        '0_1.IL_NO5,I700_1.IL_AMT1,I700_1.IL_AMT2,I700_1.IL_AMT3,I700_1.I' +
        'L_AMT4,I700_1.IL_AMT5,I700_1.IL_CUR1,I700_1.IL_CUR2,I700_1.IL_CU' +
        'R3'
      
        '      ,I700_1.IL_CUR4,I700_1.IL_CUR5,I700_1.AD_INFO1,I700_1.AD_I' +
        'NFO2,I700_1.AD_INFO3,I700_1.AD_INFO4,I700_1.AD_INFO5,I700_1.DOC_' +
        'CD,I700_1.CD_NO,I700_1.REF_PRE,I700_1.ISS_DATE,I700_1.EX_DATE,I7' +
        '00_1.EX_PLACE,I700_1.CHK1,I700_1.CHK2,I700_1.CHK3,I700_1.prno'
      
        '      ,I700_1.F_INTERFACE,I700_1.IMP_CD1,I700_1.IMP_CD2,I700_1.I' +
        'MP_CD3,I700_1.IMP_CD4,I700_1.IMP_CD5,'
      ''
      
        '       I700_2.MAINT_NO,I700_2.APP_BANK,I700_2.APP_BANK1,I700_2.A' +
        'PP_BANK2,I700_2.APP_BANK3,I700_2.APP_BANK4,I700_2.APP_BANK5,I700' +
        '_2.APP_ACCNT,I700_2.APPLIC1,I700_2.APPLIC2,I700_2.APPLIC3,I700_2' +
        '.APPLIC4,I700_2.APPLIC5,I700_2.BENEFC1'
      
        '      ,I700_2.BENEFC2,I700_2.BENEFC3,I700_2.BENEFC4,I700_2.BENEF' +
        'C5,I700_2.CD_AMT,I700_2.CD_CUR,I700_2.CD_PERP,I700_2.CD_PERM,I70' +
        '0_2.CD_MAX,I700_2.AA_CV1,I700_2.AA_CV2,I700_2.AA_CV3,I700_2.AA_C' +
        'V4,I700_2.AVAIL,I700_2.AVAIL1,I700_2.AVAIL2,I700_2.AVAIL3'
      
        '      ,I700_2.AVAIL4,I700_2.AV_ACCNT,I700_2.AV_PAY,I700_2.DRAFT1' +
        ',I700_2.DRAFT2,I700_2.DRAFT3,I700_2.DRAWEE,I700_2.DRAWEE1,I700_2' +
        '.DRAWEE2,I700_2.DRAWEE3,I700_2.DRAWEE4,I700_2.DR_ACCNT,'
      ''
      
        '       I700_3.MAINT_NO,I700_3.PSHIP,I700_3.TSHIP,I700_3.LOAD_ON,' +
        'I700_3.FOR_TRAN,I700_3.LST_DATE,I700_3.SHIP_PD,I700_3.SHIP_PD1,I' +
        '700_3.SHIP_PD2,I700_3.SHIP_PD3,I700_3.SHIP_PD4,I700_3.SHIP_PD5,I' +
        '700_3.SHIP_PD6,I700_3.DESGOOD,I700_3.DESGOOD_1,I700_3.TERM_PR'
      
        '      ,I700_3.TERM_PR_M,I700_3.PL_TERM,I700_3.ORIGIN,I700_3.ORIG' +
        'IN_M,I700_3.DOC_380,I700_3.DOC_380_1,I700_3.DOC_705,I700_3.DOC_7' +
        '05_1,I700_3.DOC_705_2,I700_3.DOC_705_3,I700_3.DOC_705_4,I700_3.D' +
        'OC_740,I700_3.DOC_740_1,I700_3.DOC_740_2,I700_3.DOC_740_3'
      
        '      ,I700_3.DOC_740_4,I700_3.DOC_530,I700_3.DOC_530_1,I700_3.D' +
        'OC_530_2,I700_3.DOC_271,I700_3.DOC_271_1,I700_3.DOC_861,I700_3.D' +
        'OC_2AA,I700_3.DOC_2AA_1,I700_3.ACD_2AA,I700_3.ACD_2AA_1,I700_3.A' +
        'CD_2AB,I700_3.ACD_2AC,I700_3.ACD_2AD,I700_3.ACD_2AE'
      
        '      ,I700_3.ACD_2AE_1,I700_3.CHARGE,I700_3.PERIOD,I700_3.CONFI' +
        'RMM,I700_3.DEF_PAY1,I700_3.DEF_PAY2,I700_3.DEF_PAY3,I700_3.DEF_P' +
        'AY4,I700_3.DOC_705_GUBUN,'
      ''
      
        '       I700_4.MAINT_NO,I700_4.REI_BANK,I700_4.REI_BANK1,I700_4.R' +
        'EI_BANK2,I700_4.REI_BANK3,I700_4.REI_BANK4,I700_4.REI_BANK5,I700' +
        '_4.REI_ACNNT,I700_4.INSTRCT,I700_4.INSTRCT_1,I700_4.AVT_BANK,I70' +
        '0_4.AVT_BANK1,I700_4.AVT_BANK2,I700_4.AVT_BANK3'
      
        '      ,I700_4.AVT_BANK4,I700_4.AVT_BANK5,I700_4.AVT_ACCNT,I700_4' +
        '.SND_INFO1,I700_4.SND_INFO2,I700_4.SND_INFO3,I700_4.SND_INFO4,I7' +
        '00_4.SND_INFO5,I700_4.SND_INFO6,I700_4.EX_NAME1,I700_4.EX_NAME2,' +
        'I700_4.EX_NAME3,I700_4.EX_ADDR1,I700_4.EX_ADDR2'
      
        '      ,I700_4.EX_ADDR3,I700_4.OP_BANK1,I700_4.OP_BANK2,I700_4.OP' +
        '_BANK3,I700_4.OP_ADDR1,I700_4.OP_ADDR2,I700_4.OP_ADDR3,I700_4.MI' +
        'X_PAY1,I700_4.MIX_PAY2,I700_4.MIX_PAY3,I700_4.MIX_PAY4'
      
        '      ,I700_4.APPLICABLE_RULES_1,I700_4.APPLICABLE_RULES_2,I700_' +
        '4.DOC_760,I700_4.DOC_760_1,I700_4.DOC_760_2,I700_4.DOC_760_3,I70' +
        '0_4.DOC_760_4,I700_4.SUNJUCK_PORT,I700_4.DOCHACK_PORT'
      ''
      #9'  ,Mathod700.DOC_NAME as mathod_Name'
      #9'  ,Pay700.DOC_NAME as pay_Name'
      #9'  ,IMPCD700_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD700_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD700_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD700_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD700_5.DOC_NAME as Imp_Name_5'
      #9'  ,DocCD700.DOC_NAME as DOC_Name'
      #9'  ,CDMAX700.DOC_NAME as CDMAX_Name'
      #9'  ,Pship700.DOC_NAME as pship_Name'
      #9'  ,Tship700.DOC_NAME as tship_Name'
      #9'  ,doc705_700.DOC_NAME as doc705_Name'
      #9'  ,doc740_700.DOC_NAME as doc740_Name'
      #9'  ,doc760_700.DOC_NAME as doc760_Name'
      #9'  ,CHARGE700.DOC_NAME as CHARGE_Name'
      #9'  ,CONFIRM700.DOC_NAME as CONFIRM_Name'
      ''
      ''
      'FROM [dbo].[INF700_1] AS I700_1'
      
        'INNER JOIN [dbo].[INF700_2] AS I700_2 ON I700_1.MAINT_NO = I700_' +
        '2.MAINT_NO'
      
        'INNER JOIN [dbo].[INF700_3] AS I700_3 ON I700_1.MAINT_NO = I700_' +
        '3.MAINT_NO'
      
        'INNER JOIN [dbo].[INF700_4] AS I700_4 ON I700_1.MAINT_NO = I700_' +
        '4.MAINT_NO'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod700 ON I700_1.IN_MATHOD = Mathod' +
        '700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay700 ON I700_1.AD_PAY = Pay700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_1 ON I700_1.IMP_CD1 = IMPCD' +
        '700_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_2 ON I700_1.IMP_CD2 = IMPCD' +
        '700_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_3 ON I700_1.IMP_CD3 = IMPCD' +
        '700_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_4 ON I700_1.IMP_CD4 = IMPCD' +
        '700_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_5 ON I700_1.IMP_CD5 = IMPCD' +
        '700_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39') DocCD700 ON I700_1.DOC_CD = DocCD700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX700 ON I700_2.CD_MAX = CDMAX700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'PSHIP'#39') Pship700 ON I700_3.PSHIP = Pship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'TSHIP'#39') Tship700 ON I700_3.TSHIP = Tship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_705'#39') doc705_700 ON I700_3.DOC_705_3 = do' +
        'c705_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc740_700 ON I700_3.DOC_740_3 = do' +
        'c740_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc760_700 ON I700_4.DOC_760_3 = do' +
        'c760_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CHARGE'#39') CHARGE700 ON I700_3.CHARGE = CHARGE7' +
        '00.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CONFIRM'#39') CONFIRM700 ON I700_3.CONFIRMM = CON' +
        'FIRM700.CODE')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 15
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListDOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListREF_PRE: TStringField
      FieldName = 'REF_PRE'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListprno: TIntegerField
      FieldName = 'prno'
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListAPP_BANK: TStringField
      FieldName = 'APP_BANK'
      Size = 11
    end
    object qryListAPP_BANK1: TStringField
      FieldName = 'APP_BANK1'
      Size = 35
    end
    object qryListAPP_BANK2: TStringField
      FieldName = 'APP_BANK2'
      Size = 35
    end
    object qryListAPP_BANK3: TStringField
      FieldName = 'APP_BANK3'
      Size = 35
    end
    object qryListAPP_BANK4: TStringField
      FieldName = 'APP_BANK4'
      Size = 35
    end
    object qryListAPP_BANK5: TStringField
      FieldName = 'APP_BANK5'
      Size = 35
    end
    object qryListAPP_ACCNT: TStringField
      FieldName = 'APP_ACCNT'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListCD_AMT: TBCDField
      FieldName = 'CD_AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListCD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListAVAIL: TStringField
      FieldName = 'AVAIL'
      Size = 11
    end
    object qryListAVAIL1: TStringField
      FieldName = 'AVAIL1'
      Size = 35
    end
    object qryListAVAIL2: TStringField
      FieldName = 'AVAIL2'
      Size = 35
    end
    object qryListAVAIL3: TStringField
      FieldName = 'AVAIL3'
      Size = 35
    end
    object qryListAVAIL4: TStringField
      FieldName = 'AVAIL4'
      Size = 35
    end
    object qryListAV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Size = 35
    end
    object qryListAV_PAY: TStringField
      FieldName = 'AV_PAY'
      Size = 35
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryListDRAWEE: TStringField
      FieldName = 'DRAWEE'
      Size = 11
    end
    object qryListDRAWEE1: TStringField
      FieldName = 'DRAWEE1'
      Size = 35
    end
    object qryListDRAWEE2: TStringField
      FieldName = 'DRAWEE2'
      Size = 35
    end
    object qryListDRAWEE3: TStringField
      FieldName = 'DRAWEE3'
      Size = 35
    end
    object qryListDRAWEE4: TStringField
      FieldName = 'DRAWEE4'
      Size = 35
    end
    object qryListDR_ACCNT: TStringField
      FieldName = 'DR_ACCNT'
      Size = 35
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListDESGOOD: TBooleanField
      FieldName = 'DESGOOD'
    end
    object qryListDESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryListTERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object qryListTERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object qryListPL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object qryListORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object qryListORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object qryListDOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object qryListDOC_380_1: TBCDField
      FieldName = 'DOC_380_1'
      Precision = 18
    end
    object qryListDOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object qryListDOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object qryListDOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object qryListDOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object qryListDOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object qryListDOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object qryListDOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object qryListDOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object qryListDOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object qryListDOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object qryListDOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object qryListDOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object qryListDOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object qryListDOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object qryListDOC_271_1: TBCDField
      FieldName = 'DOC_271_1'
      Precision = 18
    end
    object qryListDOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object qryListDOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object qryListDOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
    end
    object qryListACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object qryListACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object qryListACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object qryListACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object qryListACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object qryListACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object qryListACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
    end
    object qryListCHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryListPERIOD: TBCDField
      FieldName = 'PERIOD'
      Precision = 18
    end
    object qryListCONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object qryListDEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryListDEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryListDEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryListDEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryListDOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object qryListMAINT_NO_3: TStringField
      FieldName = 'MAINT_NO_3'
      Size = 35
    end
    object qryListREI_BANK: TStringField
      FieldName = 'REI_BANK'
      Size = 11
    end
    object qryListREI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Size = 35
    end
    object qryListREI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Size = 35
    end
    object qryListREI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Size = 35
    end
    object qryListREI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Size = 35
    end
    object qryListREI_BANK5: TStringField
      FieldName = 'REI_BANK5'
      Size = 35
    end
    object qryListREI_ACNNT: TStringField
      FieldName = 'REI_ACNNT'
      Size = 35
    end
    object qryListINSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object qryListINSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryListAVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Size = 11
    end
    object qryListAVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Size = 35
    end
    object qryListAVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Size = 35
    end
    object qryListAVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Size = 35
    end
    object qryListAVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Size = 35
    end
    object qryListAVT_BANK5: TStringField
      FieldName = 'AVT_BANK5'
      Size = 35
    end
    object qryListAVT_ACCNT: TStringField
      FieldName = 'AVT_ACCNT'
      Size = 35
    end
    object qryListSND_INFO1: TStringField
      FieldName = 'SND_INFO1'
      Size = 35
    end
    object qryListSND_INFO2: TStringField
      FieldName = 'SND_INFO2'
      Size = 35
    end
    object qryListSND_INFO3: TStringField
      FieldName = 'SND_INFO3'
      Size = 35
    end
    object qryListSND_INFO4: TStringField
      FieldName = 'SND_INFO4'
      Size = 35
    end
    object qryListSND_INFO5: TStringField
      FieldName = 'SND_INFO5'
      Size = 35
    end
    object qryListSND_INFO6: TStringField
      FieldName = 'SND_INFO6'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListEX_ADDR3: TStringField
      FieldName = 'EX_ADDR3'
      Size = 35
    end
    object qryListOP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Size = 35
    end
    object qryListOP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Size = 35
    end
    object qryListOP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Size = 35
    end
    object qryListOP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Size = 35
    end
    object qryListOP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Size = 35
    end
    object qryListOP_ADDR3: TStringField
      FieldName = 'OP_ADDR3'
      Size = 35
    end
    object qryListMIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryListMIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryListMIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryListMIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryListAPPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Size = 30
    end
    object qryListAPPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Size = 35
    end
    object qryListDOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object qryListDOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object qryListDOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object qryListDOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object qryListDOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListpay_Name: TStringField
      FieldName = 'pay_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListDOC_Name: TStringField
      FieldName = 'DOC_Name'
      Size = 100
    end
    object qryListCDMAX_Name: TStringField
      FieldName = 'CDMAX_Name'
      Size = 100
    end
    object qryListpship_Name: TStringField
      FieldName = 'pship_Name'
      Size = 100
    end
    object qryListtship_Name: TStringField
      FieldName = 'tship_Name'
      Size = 100
    end
    object qryListdoc705_Name: TStringField
      FieldName = 'doc705_Name'
      Size = 100
    end
    object qryListdoc740_Name: TStringField
      FieldName = 'doc740_Name'
      Size = 100
    end
    object qryListdoc760_Name: TStringField
      FieldName = 'doc760_Name'
      Size = 100
    end
    object qryListCHARGE_Name: TStringField
      FieldName = 'CHARGE_Name'
      Size = 100
    end
    object qryListCONFIRM_Name: TStringField
      FieldName = 'CONFIRM_Name'
      Size = 100
    end
  end
end
