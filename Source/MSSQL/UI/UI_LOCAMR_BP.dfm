inherited UI_LOCAMR_BP_frm: TUI_LOCAMR_BP_frm
  Left = 393
  Top = 32
  BorderWidth = 4
  Caption = #52712#49548#48520#45733' '#45236#44397#49888#50857#51109' '#48320#44221' '#49888#52397#49436' - [LOCAMR]'
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter4: TsSplitter [1]
    Left = 0
    Top = 77
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel1: TsPanel [2]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton2: TsSpeedButton
      Left = 210
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 512
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton4: TsSpeedButton
      Left = 595
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 293
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1105
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 3
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Left = 71
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Left = 139
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 222
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 524
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 305
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 373
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Left = 441
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
    object sButton1: TsButton
      Left = 607
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
    end
    object sButton3: TsButton
      Left = 703
      Top = 2
      Width = 74
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
  end
  object sPageControl1: TsPageControl [3]
    Left = 0
    Top = 80
    Width = 1114
    Height = 593
    ActivePage = sTabSheet1
    Align = alClient
    TabHeight = 30
    TabIndex = 0
    TabOrder = 1
    TabWidth = 110
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #48320#44221#49888#52397#45236#50857
      object sSplitter3: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel2: TsPanel
        Left = 0
        Top = 2
        Width = 1106
        Height = 551
        Align = alClient
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          1106
          551)
        object sSpeedButton1: TsSpeedButton
          Left = 594
          Top = 7
          Width = 10
          Height = 537
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sSpeedButton6: TsSpeedButton
          Left = 221
          Top = 7
          Width = 10
          Height = 537
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sLabel1: TsLabel
          Left = 394
          Top = 35
          Width = 12
          Height = 12
          AutoSize = False
          Caption = #54924
        end
        object sPanel5: TsPanel
          Left = 239
          Top = 108
          Width = 195
          Height = 20
          Caption = #44060#49444#51032#47280#51064
          Color = 16042877
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_APPLIC: TsEdit
          Left = 360
          Top = 129
          Width = 65
          Height = 21
          HelpContext = 1
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 5
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51032#47280#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn4: TsBitBtn
          Left = 426
          Top = 129
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 2
          TabStop = False
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_APPLIC1: TsEdit
          Left = 360
          Top = 151
          Width = 223
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_APPLIC2: TsEdit
          Left = 360
          Top = 173
          Width = 223
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#51088#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_APPADDR1: TsEdit
          Left = 360
          Top = 195
          Width = 223
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51032#47280#51064' '#51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_APPADDR2: TsEdit
          Left = 360
          Top = 217
          Width = 223
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPADDR3: TsEdit
          Left = 360
          Top = 239
          Width = 223
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel6: TsPanel
          Left = 239
          Top = 292
          Width = 195
          Height = 20
          Caption = #49688#54812#51088
          Color = 16042877
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_BENEFC: TsEdit
          Tag = 1
          Left = 360
          Top = 313
          Width = 65
          Height = 21
          HelpContext = 1
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 5
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#54812#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn5: TsBitBtn
          Tag = 1
          Left = 426
          Top = 313
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 10
          TabStop = False
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_BENEFC1: TsEdit
          Left = 360
          Top = 335
          Width = 223
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_BENEFC2: TsEdit
          Left = 360
          Top = 357
          Width = 223
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#51088#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_BNFADDR1: TsEdit
          Left = 360
          Top = 379
          Width = 223
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#54812#51088' '#51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_BNFADDR2: TsEdit
          Left = 360
          Top = 401
          Width = 223
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BNFADDR3: TsEdit
          Left = 360
          Top = 423
          Width = 223
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BNFMAILID: TsEdit
          Left = 360
          Top = 467
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51060#47700#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel8: TsPanel
          Left = 442
          Top = 467
          Width = 20
          Height = 21
          Caption = '@'
          TabOrder = 17
          SkinData.SkinSection = 'PANEL'
        end
        object edt_BNFDOMAIN: TsEdit
          Left = 463
          Top = 467
          Width = 120
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CHKNAME1: TsEdit
          Left = 360
          Top = 489
          Width = 103
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#48156#49888#51064' '#49885#48324#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CHKNAME2: TsEdit
          Left = 360
          Top = 511
          Width = 103
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#49464#49688#48156#49888#51064' '#49885#48324#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_LOC_TYPE: TsEdit
          Tag = 101
          Left = 359
          Top = 629
          Width = 33
          Height = 21
          Hint = #45236#44397#49888'4487'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          OnChange = edt_LOC_TYPEChange
          OnDblClick = edt_LOC_TYPEDblClick
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109#51333#47448
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn7: TsBitBtn
          Tag = 101
          Left = 393
          Top = 629
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 22
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit21: TsEdit
          Left = 418
          Top = 629
          Width = 164
          Height = 21
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LOC_AMTC: TsEdit
          Tag = 102
          Left = 359
          Top = 651
          Width = 33
          Height = 21
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
          OnChange = edt_LOC_TYPEChange
          OnDblClick = edt_LOC_TYPEDblClick
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#44221' '#54980' '#44060#49444#44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn8: TsBitBtn
          Tag = 102
          Left = 393
          Top = 651
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 25
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_LOC_AMT: TsCurrencyEdit
          Left = 418
          Top = 651
          Width = 164
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object edt_CD_PERP: TsEdit
          Left = 359
          Top = 673
          Width = 33
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 27
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54728#50857#50724#52264'(+)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_CD_PERM: TsEdit
          Left = 418
          Top = 673
          Width = 33
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 28
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '(-)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_LC_NO: TsEdit
          Left = 359
          Top = 695
          Width = 223
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45236#44397#49888#50857#51109#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_ISSBANK: TsEdit
          Left = 359
          Top = 563
          Width = 65
          Height = 21
          HelpContext = 1
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 5
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_ISSBANK1: TsEdit
          Left = 359
          Top = 585
          Width = 223
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 31
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ISSBANK2: TsEdit
          Left = 359
          Top = 607
          Width = 223
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn6: TsBitBtn
          Left = 425
          Top = 563
          Width = 24
          Height = 21
          Cursor = crHandPoint
          TabOrder = 33
          TabStop = False
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel7: TsPanel
          Left = 239
          Top = 542
          Width = 195
          Height = 20
          Caption = #44060#49444#51008#54665' / '#49888#50857#51109
          Color = 16042877
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sPanel9: TsPanel
          Left = 613
          Top = 108
          Width = 195
          Height = 20
          Caption = #52572#51333' '#47932#54408#47588#46020#54869#50557#49436' '#48264#54840
          Color = 16042877
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_OFFERNO1: TsEdit
          Left = 726
          Top = 129
          Width = 179
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 36
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#47588#46020#54869#50557#49436#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_OFFERNO2: TsEdit
          Left = 726
          Top = 151
          Width = 179
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 37
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OFFERNO3: TsEdit
          Left = 726
          Top = 173
          Width = 179
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 38
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit30'
        end
        object edt_OFFERNO4: TsEdit
          Left = 726
          Top = 195
          Width = 179
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 39
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit31'
        end
        object edt_OFFERNO5: TsEdit
          Left = 726
          Top = 217
          Width = 179
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 40
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit32'
        end
        object edt_OFFERNO6: TsEdit
          Left = 726
          Top = 239
          Width = 179
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 41
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit33'
        end
        object edt_OFFERNO7: TsEdit
          Left = 726
          Top = 261
          Width = 179
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 42
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit34'
        end
        object edt_OFFERNO8: TsEdit
          Left = 726
          Top = 283
          Width = 179
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 43
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit35'
        end
        object edt_OFFERNO9: TsEdit
          Left = 726
          Top = 305
          Width = 179
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 44
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit36'
        end
        object edt_AMD_NO: TsEdit
          Left = 360
          Top = 29
          Width = 33
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 45
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51312#44148#48320#44221#54924#52264
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object mask_ISS_DATE: TsMaskEdit
          Left = 360
          Top = 51
          Width = 83
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 46
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object mask_APP_DATE: TsMaskEdit
          Left = 360
          Top = 75
          Width = 83
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 47
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#44221#49888#52397#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object mask_DELIVERY: TsMaskEdit
          Left = 726
          Top = 29
          Width = 83
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 48
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#44221#54980' '#47932#54408#51064#46020#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object mask_EXPIRY: TsMaskEdit
          Left = 726
          Top = 51
          Width = 83
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 49
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#44221#54980' '#50976#54952#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel10: TsPanel
          Left = 239
          Top = 8
          Width = 195
          Height = 20
          Caption = #44592#48376#51221#48372
          Color = 16042877
          TabOrder = 50
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object memo_REMARK1: TsMemo
          Left = 725
          Top = 327
          Width = 439
          Height = 103
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #44404#47548
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 51
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#51221#48372
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object memo_CHGINFO: TsMemo
          Left = 725
          Top = 431
          Width = 439
          Height = 103
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 52
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#51312#44148' '#48320#44221#49324#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel17: TsPanel
          Left = 616
          Top = 544
          Width = 210
          Height = 20
          Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
          Color = 16042877
          TabOrder = 53
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_EXNAME1: TsEdit
          Left = 729
          Top = 565
          Width = 189
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 54
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_EXNAME2: TsEdit
          Left = 729
          Top = 587
          Width = 189
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 55
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
        end
        object edt_EXNAME3: TsEdit
          Left = 729
          Top = 609
          Width = 101
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 56
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_APPLIC3: TsEdit
          Left = 360
          Top = 261
          Width = 223
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 57
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_BENEFC3: TsEdit
          Left = 360
          Top = 445
          Width = 223
          Height = 21
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 58
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sPanel12: TsPanel
          Left = 613
          Top = 8
          Width = 195
          Height = 20
          Caption = #44060#49444#44288#47144
          Color = 16042877
          TabOrder = 59
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter2: TsSplitter
        Left = 0
        Top = 32
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 0
        Width = 1106
        Height = 32
        Align = alTop
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object edt_SearchText: TsEdit
          Left = 86
          Top = 5
          Width = 233
          Height = 23
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 81
          Height = 23
          Alignment = taLeftJustify
          SkinData.SkinSection = 'COMBOBOX'
          VerticalAlignment = taAlignTop
          Style = csDropDownList
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ItemHeight = 17
          ItemIndex = 0
          ParentFont = False
          TabOrder = 0
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 86
          Top = 5
          Width = 82
          Height = 18
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn1: TsBitBtn
          Left = 321
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn21: TsBitBtn
          Tag = 900
          Left = 167
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 4
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 214
          Top = 5
          Width = 82
          Height = 18
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn23: TsBitBtn
          Tag = 907
          Left = 295
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          OnClick = sBitBtn23Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel25: TsPanel
          Left = 190
          Top = 5
          Width = 25
          Height = 23
          Caption = '~'
          TabOrder = 7
          SkinData.SkinSection = 'PANEL'
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 34
        Width = 1106
        Height = 519
        Align = alClient
        Color = clWhite
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid1DrawColumnCell
        OnDblClick = sDBGrid1DblClick
        SkinData.SkinSection = 'EDIT'
      end
    end
  end
  object sPanel4: TsPanel [4]
    Left = 0
    Top = 44
    Width = 1114
    Height = 33
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object edt_DocNo1: TsEdit
      Left = 64
      Top = 5
      Width = 241
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sMaskEdit1: TsMaskEdit
      Left = 398
      Top = 5
      Width = 83
      Height = 23
      AutoSize = False
      Color = clWhite
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = '20161122'
      OnDblClick = Mask_SearchDate1DblClick
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_UserNo: TsEdit
      Left = 560
      Top = 5
      Width = 57
      Height = 23
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 480
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 3
      OnClick = btn_CalClick
      ImageIndex = 9
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_DocNo2: TsEdit
      Left = 306
      Top = 5
      Width = 27
      Height = 23
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 2
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object edt_msg1: TsEdit
      Tag = 109
      Left = 686
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 2
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      OnDblClick = edt_LOC_TYPEDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn25: TsBitBtn
      Tag = 109
      Left = 719
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      TabStop = False
      OnClick = sBitBtn7Click
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg2: TsEdit
      Tag = 110
      Left = 786
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 2
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      OnDblClick = edt_LOC_TYPEDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn26: TsBitBtn
      Tag = 110
      Left = 819
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 8
      TabStop = False
      OnClick = sBitBtn7Click
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 24
    Top = 128
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 128
  end
end
