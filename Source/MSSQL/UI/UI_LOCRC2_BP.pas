unit UI_LOCRC2_BP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sButton, Buttons, sSpeedButton, ExtCtrls,
  sPanel, sSplitter, sSkinProvider, Grids, DBGrids, acDBGrid, sBitBtn,
  Mask, sMaskEdit, sComboBox, sEdit, ComCtrls, sPageControl, StrUtils, DateUtils, TypeDefine,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, sMemo, sLabel, QuickRpt,
  QRCtrls;

type
  TUI_LOCRC2_BP_frm = class(TChildForm_frm)
    sSplitter1: TsSplitter;
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPageControl1: TsPageControl;
    sTabSheet3: TsTabSheet;
    sSplitter2: TsSplitter;
    sDBGrid1: TsDBGrid;
    sTabSheet1: TsTabSheet;
    sSplitter3: TsSplitter;
    sPanel2: TsPanel;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sSplitter4: TsSplitter;
    sPanel9: TsPanel;
    mask_GET_DAT: TsMaskEdit;
    edt_BSN_HSCODE: TsEdit;
    edt_RFF_NO: TsEdit;
    mask_ISS_DAT: TsMaskEdit;
    mask_EXP_DAT: TsMaskEdit;
    edt_BENEFC: TsEdit;
    sBitBtn5: TsBitBtn;
    edt_BENEFC1: TsEdit;
    edt_APPLIC: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_APPLIC1: TsEdit;
    edt_APPLIC2: TsEdit;
    edt_APPLIC3: TsEdit;
    edt_APPLIC4: TsEdit;
    sSpeedButton1: TsSpeedButton;
    edt_AP_BANK: TsEdit;
    sBitBtn3: TsBitBtn;
    edt_AP_BANK1: TsEdit;
    edt_AP_BANK2: TsEdit;
    edt_AP_BANK4: TsEdit;
    edt_AP_NAME: TsEdit;
    edt_AP_BANK3: TsEdit;
    sPanel12: TsPanel;
    edt_LOC_NO: TsEdit;
    edt_LOC1AMTC: TsEdit;
    sBitBtn8: TsBitBtn;
    curr_LOC1AMT: TsCurrencyEdit;
    edt_EX_RATE: TsEdit;
    edt_LOC2AMTC: TsEdit;
    mask_LOADDATE: TsMaskEdit;
    mask_EXPDATE: TsMaskEdit;
    edt_AMAINT_NO: TsEdit;
    sPanel15: TsPanel;
    memo_LOC_REM1: TsMemo;
    memo_REMARK1: TsMemo;
    sTabSheet2: TsTabSheet;
    sSplitter5: TsSplitter;
    sPanel20: TsPanel;
    sDBGrid3: TsDBGrid;
    sSplitter6: TsSplitter;
    sSplitter7: TsSplitter;
    sPanel21: TsPanel;
    edt_NAME_COD: TsEdit;
    edt_HS_NO: TsMaskEdit;
    memo_NAME1: TsMemo;
    memo_SIZE1: TsMemo;
    sPanel22: TsPanel;
    sPanel23: TsPanel;
    sSpeedButton6: TsSpeedButton;
    edt_QTY_G: TsEdit;
    sBitBtn9: TsBitBtn;
    curr_QTY: TsCurrencyEdit;
    edt_PRICE_G: TsEdit;
    sBitBtn10: TsBitBtn;
    curr_PRICE: TsCurrencyEdit;
    edt_QTYG_G: TsEdit;
    sBitBtn11: TsBitBtn;
    curr_QTYG: TsCurrencyEdit;
    edt_AMT_G: TsEdit;
    sBitBtn12: TsBitBtn;
    curr_AMT: TsCurrencyEdit;
    edt_STQTY_G: TsEdit;
    sBitBtn13: TsBitBtn;
    curr_STQTY: TsCurrencyEdit;
    edt_STAMT_G: TsEdit;
    sBitBtn14: TsBitBtn;
    curr_STAMT: TsCurrencyEdit;
    btn_GoodsCalc: TsButton;
    sPanel24: TsPanel;
    sBitBtn15: TsBitBtn;
    edt_RCT_AMT1C: TsEdit;
    sBitBtn4: TsBitBtn;
    curr_RCT_AMT1: TsCurrencyEdit;
    edt_RATE: TsEdit;
    edt_RCT_AMT2C: TsEdit;
    curr_RCT_AMT2: TsCurrencyEdit;
    edt_APPLIC5: TsEdit;
    edt_APPLIC6: TsEdit;
    sPanel33: TsPanel;
    sDBGrid5: TsDBGrid;
    sSplitter11: TsSplitter;
    curr_LOC2AMT: TsCurrencyEdit;
    Pan_Detail: TsPanel;
    LIne1: TsSpeedButton;
    Line4: TsSpeedButton;
    Btn_GoodsDel: TsSpeedButton;
    Line3: TsSpeedButton;
    Btn_GoodsEdit: TsSpeedButton;
    LIne2: TsSpeedButton;
    Btn_GoodsOK: TsSpeedButton;
    Btn_GoodsCancel: TsSpeedButton;
    sSpeedButton14: TsSpeedButton;
    Btn_GoodsNew: TsSpeedButton;
    sSpeedButton18: TsSpeedButton;
    sPanel30: TsPanel;
    edt_BENEFC2: TsMaskEdit;
    edt_APPLIC7: TsMaskEdit;
    sTabSheet4: TsTabSheet;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sPanel4: TsPanel;
    edt_MAINT_NO: TsEdit;
    mask_DATEE: TsMaskEdit;
    edt_USER_ID: TsEdit;
    btn_Cal: TsBitBtn;
    sPanel19: TsPanel;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btn_TaxDel: TsSpeedButton;
    sSpeedButton10: TsSpeedButton;
    btn_TaxEdit: TsSpeedButton;
    sSpeedButton12: TsSpeedButton;
    btn_TaxOK: TsSpeedButton;
    btn_TaxCancel: TsSpeedButton;
    sSpeedButton16: TsSpeedButton;
    btn_TaxNew: TsSpeedButton;
    taxExcelBtn: TsSpeedButton;
    sDBGrid4: TsDBGrid;
    sPanel28: TsPanel;
    edt_BILL_NO: TsEdit;
    mask_BILL_DATE: TsMaskEdit;
    edt_BILL_AMOUNT_UNIT: TsEdit;
    curr_BILL_AMOUNT: TsCurrencyEdit;
    edt_TAX_AMOUNT_UNIT: TsEdit;
    curr_TAX_AMOUNT: TsCurrencyEdit;
    sSplitter9: TsSplitter;
    sPanel18: TsPanel;
    panel1: TsPanel;
    edt_msg1: TsEdit;
    sBitBtn19: TsBitBtn;
    edt_msg2: TsEdit;
    sBitBtn20: TsBitBtn;
    sPanel14: TsPanel;
    sLabel4: TsLabel;
    btn_TOTAL_CALC: TsButton;
    edt_TQTY_G: TsEdit;
    sBitBtn6: TsBitBtn;
    curr_TQTY: TsCurrencyEdit;
    edt_TAMT_G: TsEdit;
    sBitBtn7: TsBitBtn;
    curr_TAMT: TsCurrencyEdit;
    sPanel32: TsPanel;
    sPanel7: TsPanel;
    sSplitter13: TsSplitter;
    sSplitter14: TsSplitter;
    sPanel26: TsPanel;
    sPanel53: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    sDBGrid2: TsDBGrid;
    left_panel: TsPanel;
    procedure FormShow(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
  private
    { Private declarations }
  protected
    ProgramControlType : TProgramControlType;
    FSQL : String;
    procedure getGoodsList(MAINT_NO : String); virtual; abstract;
    procedure getTaxList(MAINT_NO : String); virtual; abstract;
    procedure ReadDocument; virtual; abstract;
    procedure ReadGoods; virtual; abstract;
    procedure ReadTax; virtual; abstract;
    procedure ButtonEnable(Val : Boolean);
  public
    { Public declarations }
  end;

var
  UI_LOCRC2_BP_frm: TUI_LOCRC2_BP_frm;

implementation

uses Commonlib, KISCalendar, ICON;

{$R *.dfm}

procedure TUI_LOCRC2_BP_frm.FormShow(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',EndOfTheYear(Now));
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',EndOfTheYear(Now));
  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_LOCRC2_BP_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
//  IF not (ProgramControlType in [ctInsert,ctModify]) Then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_LOCRC2_BP_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;

  IF edt_SearchText.Visible Then
    edt_SearchText.Clear;
end;

procedure TUI_LOCRC2_BP_frm.ButtonEnable(Val: Boolean);
begin
  btnNew.Enabled := Val;
  btnEdit.Enabled := Val;
  btnDel.Enabled := Val;

  btnTemp.Enabled := not Val;
  btnSave.Enabled := not Val;
  btnCancel.Enabled := not Val;

  btnCopy.Enabled := Val;
  btnPrint.Enabled := Val;

  btnReady.Enabled := Val;
  btnSend.Enabled := Val;
end;

end.

