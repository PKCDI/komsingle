inherited UI_LDANTC_frm: TUI_LDANTC_frm
  Left = 755
  Top = 184
  ClientWidth = 1114
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter3: TsSplitter [1]
    Left = 0
    Top = 76
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel1: TsPanel [2]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    object sSpeedButton2: TsSpeedButton
      Left = 504
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 231
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel
      Left = 8
      Top = 5
      Width = 218
      Height = 17
      Caption = #45236#44397#49888#50857#51109' '#54032#47588#45824#44552#52628#49900#46020#52265' '#53685#48372#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel
      Left = 8
      Top = 20
      Width = 44
      Height = 13
      Caption = 'LDANTC'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object btnExit: TsButton
      Left = 1034
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Left = 239
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object sButton4: TsButton
      Left = 314
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton2: TsButton
      Tag = 1
      Left = 405
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 3
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [3]
    Left = 0
    Top = 44
    Width = 1114
    Height = 32
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 1
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 778
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 5
      TabStop = False
      Visible = False
      ImageIndex = 9
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object mask_DATEE: TsMaskEdit
      Left = 302
      Top = 5
      Width = 83
      Height = 23
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_MaintNo: TsEdit
      Left = 64
      Top = 5
      Width = 177
      Height = 21
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clBlack
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn2: TsBitBtn
      Tag = 1
      Left = 730
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 7
      TabStop = False
      Visible = False
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 569
      Top = 5
      Width = 32
      Height = 21
      Hint = #44592#45733#54364#49884
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_UserNo: TsEdit
      Left = 441
      Top = 5
      Width = 57
      Height = 21
      TabStop = False
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sCheckBox1: TsCheckBox
      Left = 880
      Top = 6
      Width = 64
      Height = 16
      TabStop = False
      Caption = #46356#48260#44536
      TabOrder = 6
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 649
      Top = 5
      Width = 32
      Height = 21
      Hint = #51025#45813#50976#54805
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn3: TsBitBtn
      Tag = 2
      Left = 754
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 8
      TabStop = False
      Visible = False
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sPageControl1: TsPageControl [4]
    Left = 0
    Top = 79
    Width = 1114
    Height = 602
    ActivePage = sTabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 0
    TabOrder = 2
    TabStop = False
    TabWidth = 125
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter4: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel5: TsPanel
        Left = 0
        Top = 3
        Width = 270
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        
        TabOrder = 0
        object sPanel57: TsPanel
          Left = 1
          Top = 1
          Width = 268
          Height = 33
          SkinData.SkinSection = 'PANEL'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 0
          object Mask_fromDate: TsMaskEdit
            Left = 94
            Top = 5
            Width = 72
            Height = 23
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            Text = '20160101'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #46321#47197#51068#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
          end
          object sBitBtn60: TsBitBtn
            Left = 240
            Top = 5
            Width = 25
            Height = 23
            Cursor = crHandPoint
            TabOrder = 2
            OnClick = sBitBtn60Click
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object mask_toDate: TsMaskEdit
            Left = 167
            Top = 5
            Width = 73
            Height = 23
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Text = '20171231'
            CheckOnExit = True
            SkinData.SkinSection = 'EDIT'
          end
        end
        object sDBGrid8: TsDBGrid
          Left = 1
          Top = 34
          Width = 268
          Height = 524
          TabStop = False
          Align = alClient
          Color = clWhite
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #44404#47548#52404
          TitleFont.Style = []
          OnDrawColumnCell = sDBGrid8DrawColumnCell
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 71
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 172
              Visible = True
            end>
        end
      end
      object sPanel2: TsPanel
        Left = 270
        Top = 3
        Width = 836
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 1
        DesignSize = (
          836
          559)
        object sSpeedButton1: TsSpeedButton
          Left = 417
          Top = 1
          Width = 9
          Height = 456
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel18: TsPanel
          Left = 12
          Top = 2
          Width = 400
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44592#48376#51221#48372
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object mask_SETDATE: TsMaskEdit
          Left = 149
          Top = 177
          Width = 83
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 1
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#51333#44208#51228#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel24: TsPanel
          Left = 429
          Top = 89
          Width = 400
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44552#50529#51221#48372
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object edt_BKNAME1: TsEdit
          Left = 149
          Top = 45
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#49888#44592#44288
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_BKNAME2: TsEdit
          Left = 149
          Top = 67
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BKNAME3: TsEdit
          Left = 149
          Top = 89
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_LCNO: TsEdit
          Left = 149
          Top = 111
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45236#44397#49888#50857#51109#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_BGMGUBUN: TsEdit
          Left = 149
          Top = 23
          Width = 39
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 11
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 7
          Text = '2CS'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44396#48516
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_RCNO2: TsEdit
          Left = 149
          Top = 245
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RCNO3: TsEdit
          Left = 149
          Top = 267
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object mask_NGDATE: TsMaskEdit
          Left = 149
          Top = 155
          Width = 83
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 10
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'NEGO'#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object mask_RESDATE: TsMaskEdit
          Left = 149
          Top = 133
          Width = 83
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 11
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #53685#51648#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object curr_AMT1: TsCurrencyEdit
          Left = 614
          Top = 110
          Width = 167
          Height = 21
          AutoSize = False
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AMT1C: TsEdit
          Left = 566
          Top = 110
          Width = 47
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 3
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50808#54868#44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel13: TsPanel
          Left = 429
          Top = 2
          Width = 400
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #49688#49888#51064
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 14
        end
        object edt_AMT2C: TsEdit
          Left = 566
          Top = 132
          Width = 47
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 3
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50896#54868#54872#49328#44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object curr_AMT2: TsCurrencyEdit
          Left = 614
          Top = 132
          Width = 167
          Height = 21
          AutoSize = False
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
        end
        object curr_RATE: TsCurrencyEdit
          Left = 566
          Top = 154
          Width = 135
          Height = 21
          AutoSize = False
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 17
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51201#50857#54872#50984
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPNAME2: TsEdit
          Left = 566
          Top = 45
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPNAME1: TsEdit
          Left = 566
          Top = 23
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#49888#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APPNAME3: TsEdit
          Left = 566
          Top = 67
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RCNO: TsEdit
          Left = 149
          Top = 223
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#49688#47161#51613#47749#49436#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel14: TsPanel
          Left = 12
          Top = 202
          Width = 400
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #47932#54408#49688#47161#51613#47749#49436'('#51064#49688#51613')'#48264#54840
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 22
        end
        object sPanel12: TsPanel
          Left = 12
          Top = 448
          Width = 817
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44592#53440#49324#54637
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 23
        end
        object memo_REMARK1: TsMemo
          Left = 77
          Top = 469
          Width = 700
          Height = 87
          ScrollBars = ssVertical
          TabOrder = 24
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BGMGUBUNName: TsEdit
          Left = 189
          Top = 23
          Width = 223
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RCNO6: TsEdit
          Left = 149
          Top = 333
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RCNO5: TsEdit
          Left = 149
          Top = 311
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RCNO4: TsEdit
          Left = 149
          Top = 289
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RCNO8: TsEdit
          Left = 149
          Top = 377
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RCNO7: TsEdit
          Left = 149
          Top = 355
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel6: TsPanel
          Left = 429
          Top = 202
          Width = 400
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #49464#44552#44228#49328#49436#48264#54840
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 31
        end
        object edt_FINNO: TsEdit
          Left = 566
          Top = 223
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49464#44552#44228#49328#49436#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_FINNO2: TsEdit
          Left = 566
          Top = 245
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 33
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_FINNO4: TsEdit
          Left = 566
          Top = 289
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_FINNO3: TsEdit
          Left = 566
          Top = 267
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_FINNO5: TsEdit
          Left = 566
          Top = 311
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_FINNO6: TsEdit
          Left = 566
          Top = 333
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_FINNO7: TsEdit
          Left = 566
          Top = 355
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 38
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel7: TsPanel
          Left = 429
          Top = 377
          Width = 400
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #53685#51648#51008#54665
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 39
        end
        object edt_BANK1: TsEdit
          Left = 566
          Top = 398
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #53685#51648#51008#54665#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_BANK2: TsEdit
          Left = 566
          Top = 420
          Width = 263
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 41
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51648#51216#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
      end
    end
    object sTabSheet9: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sPanel3: TsPanel
        Left = 0
        Top = 0
        Width = 1106
        Height = 33
        SkinData.SkinSection = 'PANEL'
        Align = alTop
        
        TabOrder = 0
        object edt_SearchText: TsEdit
          Left = 103
          Top = 5
          Width = 233
          Height = 23
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 101
          Height = 23
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 17
          ItemIndex = 0
          ParentCtl3D = False
          TabOrder = 4
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #45236#44397#49888#50857#51109)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 102
          Top = 5
          Width = 82
          Height = 23
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn1: TsBitBtn
          Left = 336
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          OnClick = sBitBtn1Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn21: TsBitBtn
          Left = 183
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 230
          Top = 5
          Width = 82
          Height = 23
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn23: TsBitBtn
          Tag = 1
          Left = 312
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel25: TsPanel
          Left = 206
          Top = 5
          Width = 25
          Height = 23
          SkinData.SkinSection = 'PANEL'
          Caption = '~'
          
          TabOrder = 7
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 33
        Width = 1106
        Height = 529
        TabStop = False
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid8DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LC_NO'
            Title.Alignment = taCenter
            Title.Caption = #45236#44397#49888#50857#51109#48264#54840
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BANK1'
            Title.Alignment = taCenter
            Title.Caption = #53685#51648#51008#54665
            Width = 160
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NG_DATE'
            Title.Alignment = taCenter
            Title.Caption = 'NEGO'#51068#51088
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SET_DATE'
            Title.Alignment = taCenter
            Title.Caption = #52572#51333#44208#51228#51068
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT1'
            Title.Alignment = taCenter
            Title.Caption = #50808#54868#44552#50529
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT1C'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT2'
            Title.Alignment = taCenter
            Title.Caption = #50896#54868#54872#49328#44552#50529
            Width = 100
            Visible = True
          end>
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 200
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    AfterOpen = qryListAfterScroll
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      
        '  MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, APP_CODE, APP_NA' +
        'ME1, APP_NAME2, APP_NAME3, BK_CODE, BK_NAME1, BK_NAME2, BK_NAME3' +
        ', LC_NO, RC_NO, AMT1, AMT1C, AMT2, AMT2C, RATE, RES_DATE, SET_DA' +
        'TE, NG_DATE, REMAKR1, BANK1, BANK2, CHK1, CHK2, CHK3, PRNO, BGM_' +
        'GUBUN, RC_NO2, RC_NO3, RC_NO4, RC_NO5, RC_NO6, RC_NO7, RC_NO8, F' +
        'IN_NO, FIN_NO2, FIN_NO3, FIN_NO4, FIN_NO5, FIN_NO6, FIN_NO7'
      'FROM LDANTC')
    Left = 24
    Top = 232
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryListAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryListAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryListBK_CODE: TStringField
      FieldName = 'BK_CODE'
      Size = 10
    end
    object qryListBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 35
    end
    object qryListBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 35
    end
    object qryListBK_NAME3: TStringField
      FieldName = 'BK_NAME3'
      Size = 35
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListRC_NO: TStringField
      FieldName = 'RC_NO'
      Size = 35
    end
    object qryListAMT1: TBCDField
      FieldName = 'AMT1'
      Precision = 18
    end
    object qryListAMT1C: TStringField
      FieldName = 'AMT1C'
      Size = 3
    end
    object qryListAMT2: TBCDField
      FieldName = 'AMT2'
      Precision = 18
    end
    object qryListAMT2C: TStringField
      FieldName = 'AMT2C'
      Size = 3
    end
    object qryListRATE: TBCDField
      FieldName = 'RATE'
      Precision = 18
    end
    object qryListRES_DATE: TStringField
      FieldName = 'RES_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListSET_DATE: TStringField
      FieldName = 'SET_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListNG_DATE: TStringField
      FieldName = 'NG_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListREMAKR1: TMemoField
      FieldName = 'REMAKR1'
      BlobType = ftMemo
    end
    object qryListBANK1: TStringField
      FieldName = 'BANK1'
      Size = 70
    end
    object qryListBANK2: TStringField
      FieldName = 'BANK2'
      Size = 70
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListBGM_GUBUN: TStringField
      FieldName = 'BGM_GUBUN'
      Size = 3
    end
    object qryListRC_NO2: TStringField
      FieldName = 'RC_NO2'
      Size = 35
    end
    object qryListRC_NO3: TStringField
      FieldName = 'RC_NO3'
      Size = 35
    end
    object qryListRC_NO4: TStringField
      FieldName = 'RC_NO4'
      Size = 35
    end
    object qryListRC_NO5: TStringField
      FieldName = 'RC_NO5'
      Size = 35
    end
    object qryListRC_NO6: TStringField
      FieldName = 'RC_NO6'
      Size = 35
    end
    object qryListRC_NO7: TStringField
      FieldName = 'RC_NO7'
      Size = 35
    end
    object qryListRC_NO8: TStringField
      FieldName = 'RC_NO8'
      Size = 35
    end
    object qryListFIN_NO: TStringField
      FieldName = 'FIN_NO'
      Size = 35
    end
    object qryListFIN_NO2: TStringField
      FieldName = 'FIN_NO2'
      Size = 35
    end
    object qryListFIN_NO3: TStringField
      FieldName = 'FIN_NO3'
      Size = 35
    end
    object qryListFIN_NO4: TStringField
      FieldName = 'FIN_NO4'
      Size = 35
    end
    object qryListFIN_NO5: TStringField
      FieldName = 'FIN_NO5'
      Size = 35
    end
    object qryListFIN_NO6: TStringField
      FieldName = 'FIN_NO6'
      Size = 35
    end
    object qryListFIN_NO7: TStringField
      FieldName = 'FIN_NO7'
      Size = 35
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 24
    Top = 264
  end
end
