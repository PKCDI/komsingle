inherited UI_LOCAPP_frm: TUI_LOCAPP_frm
  Left = 2073
  Top = 40
  BorderWidth = 4
  Caption = #45236#44397#49888#50857#51109' '#44060#49444#49888#52397#49436'['#44060#49444#50629#52404']'
  ClientHeight = 683
  ClientWidth = 1111
  Constraints.MaxHeight = 730
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 76
    Width = 1111
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter5: TsSplitter [1]
    Left = 0
    Top = 41
    Width = 1111
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPageControl1: TsPageControl [2]
    Left = 0
    Top = 79
    Width = 1111
    Height = 604
    ActivePage = sTabSheet2
    Align = alClient
    TabHeight = 30
    TabIndex = 0
    TabOrder = 2
    TabWidth = 110
    OnChange = sPageControl1Change
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet2: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter3: TsSplitter
        Left = 0
        Top = 0
        Width = 1103
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sScrollBox1: TsScrollBox
        Left = 280
        Top = 3
        Width = 823
        Height = 561
        Align = alClient
        BorderStyle = bsNone
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          823
          561)
        object sSpeedButton1: TsSpeedButton
          Left = 426
          Top = 0
          Width = 10
          Height = 560
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sLabel1: TsLabel
          Left = 584
          Top = 363
          Width = 76
          Height = 15
          AutoSize = False
          Caption = #52264' '#45236#44397#49888#50857#51109
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel3: TsLabel
          Left = 499
          Top = 489
          Width = 160
          Height = 15
          AutoSize = False
          Caption = #47932#54408#49688#47161#51613#47749#49436' '#48156#44553#51068#47196#48512#53552
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object sLabel4: TsLabel
          Left = 707
          Top = 489
          Width = 24
          Height = 15
          AutoSize = False
          Caption = #51060#45236
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object edt_CreateUserSangho: TsEdit
          Left = 160
          Top = 42
          Width = 225
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CreateUserCode: TsEdit
          Left = 160
          Top = 23
          Width = 65
          Height = 19
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 5
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnDblClick = edt_CreateUserCodeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51032#47280#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CreateUserDaepyo: TsEdit
          Left = 160
          Top = 61
          Width = 225
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#51088#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CreateUserAddr1: TsEdit
          Left = 160
          Top = 80
          Width = 225
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51032#47280#51064' '#51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_CreateUserAddr2: TsEdit
          Left = 160
          Top = 99
          Width = 225
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CreateUserAddr3: TsEdit
          Left = 160
          Top = 118
          Width = 225
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CreateUserSaupNo: TsMaskEdit
          Left = 160
          Top = 137
          Width = 97
          Height = 19
          HelpContext = 1
          Ctl3D = False
          EditMask = '999-99-99999;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 12
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          Text = '2200222960'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel5: TsPanel
          Left = 40
          Top = 2
          Width = 345
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44060#49444#51032#47280#51064
          Color = 16042877
          
          TabOrder = 49
        end
        object sPanel7: TsPanel
          Left = 40
          Top = 160
          Width = 345
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #49688#54812#51088
          Color = 16042877
          
          TabOrder = 50
        end
        object edt_RecvCode: TsEdit
          Tag = 1
          Left = 160
          Top = 181
          Width = 65
          Height = 19
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 5
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          OnDblClick = edt_CreateUserCodeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#54812#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn5: TsBitBtn
          Tag = 1
          Left = 226
          Top = 181
          Width = 24
          Height = 19
          Cursor = crHandPoint
          TabOrder = 51
          TabStop = False
          OnClick = sBitBtn4Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_RecvSangho: TsEdit
          Left = 160
          Top = 200
          Width = 223
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_RecvDaepyo: TsEdit
          Left = 160
          Top = 219
          Width = 223
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#51088#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_RecvAddr1: TsEdit
          Left = 160
          Top = 238
          Width = 223
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#54812#51088' '#51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_RecvAddr2: TsEdit
          Left = 160
          Top = 257
          Width = 223
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RecvAddr3: TsEdit
          Left = 160
          Top = 276
          Width = 223
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RecvSaup: TsMaskEdit
          Left = 160
          Top = 295
          Width = 103
          Height = 19
          HelpContext = 1
          Ctl3D = False
          EditMask = '999-99-99999;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 12
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          Text = '2200222960'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RecvEmail1: TsEdit
          Left = 160
          Top = 314
          Width = 81
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51060#47700#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel8: TsPanel
          Left = 242
          Top = 314
          Width = 20
          Height = 19
          SkinData.SkinSection = 'PANEL'
          Caption = '@'
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 52
        end
        object edt_RecvEmail2: TsEdit
          Left = 263
          Top = 314
          Width = 120
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          SkinData.SkinSection = 'EDIT'
        end
        object edt_RecvID: TsEdit
          Left = 160
          Top = 333
          Width = 103
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#48156#49888#51064' '#49885#48324#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_RecvID_Detail: TsEdit
          Left = 160
          Top = 352
          Width = 103
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#49464#49688#48156#49888#51064' '#49885#48324#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sPanel9: TsPanel
          Left = 40
          Top = 375
          Width = 345
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44060#49444#51008#54665'/'#49888#50857#51109
          Color = 16042877
          
          TabOrder = 53
        end
        object edt_CreateBankCode: TsEdit
          Left = 160
          Top = 396
          Width = 65
          Height = 19
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 5
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          OnDblClick = edt_CreateBankCodeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn6: TsBitBtn
          Left = 226
          Top = 396
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 54
          TabStop = False
          OnClick = sBitBtn6Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_CreateBankName: TsEdit
          Left = 160
          Top = 415
          Width = 223
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CreateBankBrunch: TsEdit
          Left = 160
          Top = 434
          Width = 223
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.SkinSection = 'EDIT'
        end
        object sEdit20: TsEdit
          Tag = 101
          Left = 160
          Top = 453
          Width = 33
          Height = 19
          Hint = #45236#44397#49888'4487'
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          OnChange = sEdit20Change
          OnDblClick = sEdit20DblClick
          OnKeyUp = sEdit20KeyUp
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109#51333#47448
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn7: TsBitBtn
          Tag = 101
          Left = 194
          Top = 453
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 55
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit21: TsEdit
          Left = 219
          Top = 453
          Width = 164
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 56
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sEdit22: TsEdit
          Tag = 102
          Left = 160
          Top = 472
          Width = 33
          Height = 19
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          OnChange = sEdit20Change
          OnDblClick = sEdit20DblClick
          OnKeyUp = sEdit20KeyUp
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn8: TsBitBtn
          Tag = 102
          Left = 194
          Top = 472
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 57
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sCurrencyEdit1: TsCurrencyEdit
          Left = 219
          Top = 471
          Width = 164
          Height = 21
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
          SkinData.SkinSection = 'EDIT'
          OnButtonClick = sCurrencyEdit9ButtonClick
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object sEdit23: TsEdit
          Left = 160
          Top = 491
          Width = 33
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54728#50857#50724#52264'(+)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sEdit24: TsEdit
          Left = 219
          Top = 491
          Width = 33
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '(-)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sEdit25: TsEdit
          Tag = 103
          Left = 160
          Top = 510
          Width = 33
          Height = 19
          Hint = #45236#44397#49888'4025'
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          OnChange = sEdit20Change
          OnDblClick = sEdit20DblClick
          OnKeyUp = sEdit20KeyUp
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#44540#44144#48324' '#50857#46020
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn9: TsBitBtn
          Tag = 103
          Left = 194
          Top = 510
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 58
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit26: TsEdit
          Left = 219
          Top = 510
          Width = 164
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 59
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sEdit27: TsEdit
          Left = 160
          Top = 529
          Width = 223
          Height = 19
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'L/C '#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_Sell1: TsEdit
          Left = 600
          Top = 22
          Width = 161
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 28
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#47588#46020#54869#50557#49436#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_Sell2: TsEdit
          Left = 600
          Top = 41
          Width = 161
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 29
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Sell3: TsEdit
          Left = 600
          Top = 60
          Width = 161
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 30
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit30'
        end
        object edt_Sell4: TsEdit
          Left = 600
          Top = 79
          Width = 161
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 31
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit31'
        end
        object edt_Sell5: TsEdit
          Left = 600
          Top = 98
          Width = 161
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit32'
        end
        object edt_Sell6: TsEdit
          Left = 600
          Top = 117
          Width = 161
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 33
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit33'
        end
        object edt_Sell7: TsEdit
          Left = 600
          Top = 136
          Width = 161
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 34
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit34'
        end
        object edt_Sell8: TsEdit
          Left = 600
          Top = 155
          Width = 161
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 35
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit35'
        end
        object edt_Sell9: TsEdit
          Left = 600
          Top = 174
          Width = 161
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 36
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'sEdit36'
        end
        object sPanel10: TsPanel
          Left = 478
          Top = 1
          Width = 283
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #47932#54408#47588#46020#54869#50557#49436
          Color = 16042877
          
          TabOrder = 60
        end
        object sPanel11: TsPanel
          Left = 478
          Top = 197
          Width = 283
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #51452#50836#44396#48708#49436#47448
          Color = 16042877
          
          TabOrder = 61
        end
        object Curr_AttachDoc1: TsCurrencyEdit
          Left = 738
          Top = 218
          Width = 23
          Height = 23
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 37
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#49688#47161#51613#47749#49436
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
          OnButtonClick = sCurrencyEdit9ButtonClick
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 9
          Value = 1
        end
        object Curr_AttachDoc2: TsCurrencyEdit
          Left = 738
          Top = 241
          Width = 23
          Height = 23
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 38
          OnChange = Curr_AttachDoc2Change
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44277#44553#51088#48156#54665' '#49464#44552#44228#49328#49436' '#49324#48376
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          OnButtonClick = sCurrencyEdit9ButtonClick
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 1
          Value = 1
        end
        object Curr_AttachDoc3: TsCurrencyEdit
          Tag = 1
          Left = 738
          Top = 264
          Width = 23
          Height = 23
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 39
          OnChange = Curr_AttachDoc2Change
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#47749#49464#44032' '#44592#51116#46108' '#49569#51109
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          OnButtonClick = sCurrencyEdit9ButtonClick
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 1
        end
        object Curr_AttachDoc4: TsCurrencyEdit
          Left = 738
          Top = 287
          Width = 23
          Height = 23
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 40
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48376' '#45236#44397#49888#50857#51109#51032' '#49324#48376
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
          OnButtonClick = sCurrencyEdit9ButtonClick
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 9
          Value = 1
        end
        object Curr_AttachDoc5: TsCurrencyEdit
          Left = 738
          Top = 310
          Width = 23
          Height = 23
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 1
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 41
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44277#44553#51088#48156#54665' '#47932#54408#47588#46020#54869#50557#49436' '#49324#48376
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
          OnButtonClick = sCurrencyEdit9ButtonClick
          DecimalPlaces = 0
          DisplayFormat = '0'
          MaxValue = 9
          Value = 1
        end
        object sPanel12: TsPanel
          Left = 478
          Top = 337
          Width = 283
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44060#49444#44288#47144
          Color = 16042877
          
          TabOrder = 62
        end
        object Curr_CreateSeq: TsCurrencyEdit
          Left = 559
          Top = 358
          Width = 23
          Height = 23
          HelpContext = 1
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 42
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#54924#52264
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          OnButtonClick = sCurrencyEdit9ButtonClick
          DecimalPlaces = 0
          DisplayFormat = '0'
        end
        object Mask_CreateRequestDate: TsMaskEdit
          Left = 559
          Top = 381
          Width = 83
          Height = 19
          HelpContext = 1
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 43
          Text = '20161122'
          OnDblClick = sMaskEdit1DblClick
          OnKeyUp = Mask_CreateRequestDateKeyUp
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#49888#52397#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn10: TsBitBtn
          Tag = 902
          Left = 643
          Top = 381
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 63
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sComboBox1: TsComboBox
          Left = 654
          Top = 483
          Width = 49
          Height = 23
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 47
          Text = '5'#51068
          Items.Strings = (
            '5'#51068
            '7'#51068)
        end
        object sPanel13: TsPanel
          Left = 478
          Top = 461
          Width = 283
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #49436#47448#51228#49884#44592#44036
          Color = 16042877
          
          TabOrder = 64
        end
        object Mask_IndoDate: TsMaskEdit
          Left = 559
          Top = 400
          Width = 83
          Height = 19
          HelpContext = 1
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 44
          Text = '20161122'
          OnDblClick = sMaskEdit1DblClick
          OnKeyUp = Mask_CreateRequestDateKeyUp
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47932#54408#51064#46020#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn11: TsBitBtn
          Tag = 903
          Left = 643
          Top = 400
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 65
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_ExpiryDate: TsMaskEdit
          Left = 559
          Top = 419
          Width = 83
          Height = 19
          HelpContext = 1
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 45
          Text = '20161122'
          OnDblClick = sMaskEdit1DblClick
          OnKeyUp = Mask_CreateRequestDateKeyUp
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50976#54952#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn12: TsBitBtn
          Tag = 904
          Left = 643
          Top = 419
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 66
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit37: TsEdit
          Tag = 104
          Left = 559
          Top = 438
          Width = 34
          Height = 19
          Hint = 'PSHIP'
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 2
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 46
          OnChange = sEdit20Change
          OnDblClick = sEdit20DblClick
          OnKeyUp = sEdit20KeyUp
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48516#54624#54728#50857#50668#48512
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sBitBtn13: TsBitBtn
          Tag = 104
          Left = 594
          Top = 438
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 67
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit38: TsEdit
          Left = 619
          Top = 438
          Width = 142
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 68
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn4: TsBitBtn
          Left = 226
          Top = 23
          Width = 24
          Height = 19
          Cursor = crHandPoint
          TabOrder = 48
          TabStop = False
          OnClick = sBitBtn4Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
      end
      object sPanel18: TsPanel
        Left = 0
        Top = 3
        Width = 280
        Height = 561
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'sPanel18'
        
        TabOrder = 1
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #45824#54364#44277#44553#47932#54408
      object sSplitter4: TsSplitter
        Left = 0
        Top = 0
        Width = 1103
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel23: TsPanel
        Left = 0
        Top = 3
        Width = 280
        Height = 561
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'sPanel18'
        
        TabOrder = 1
      end
      object sPanel21: TsPanel
        Left = 280
        Top = 3
        Width = 823
        Height = 561
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 0
        object sLabel16: TsLabel
          Left = 650
          Top = 92
          Width = 111
          Height = 13
          Caption = #52572#45824' 5'#54665' 70'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel2: TsLabel
          Left = 650
          Top = 156
          Width = 111
          Height = 13
          Caption = #52572#45824' 5'#54665' 70'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel5: TsLabel
          Left = 650
          Top = 218
          Width = 111
          Height = 13
          Caption = #52572#45824' 5'#54665' 70'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel56: TsLabel
          Left = 193
          Top = 89
          Width = 4
          Height = 13
          Alignment = taRightJustify
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel6: TsLabel
          Left = 194
          Top = 153
          Width = 4
          Height = 13
          Alignment = taRightJustify
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel7: TsLabel
          Left = 193
          Top = 215
          Width = 4
          Height = 13
          Alignment = taRightJustify
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel8: TsLabel
          Left = 610
          Top = 457
          Width = 111
          Height = 13
          Caption = #52572#45824' 5'#54665' 70'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel9: TsLabel
          Left = 154
          Top = 453
          Width = 4
          Height = 13
          Alignment = taRightJustify
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sPanel14: TsPanel
          Left = 80
          Top = 2
          Width = 281
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #45824#54364#44277#44553#47932#54408
          Color = 16042877
          
          TabOrder = 22
        end
        object sMemo1: TsMemo
          Left = 201
          Top = 42
          Width = 446
          Height = 63
          HelpContext = 1
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 1
          OnChange = sMemo1Change
          OnKeyPress = memoLimit
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#44277#44553#47932#54408#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Hs: TsMaskEdit
          Tag = 201
          Left = 201
          Top = 23
          Width = 89
          Height = 19
          HelpContext = 1
          Color = clWhite
          Ctl3D = False
          EditMask = '9999.99-9999;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 12
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '1234567890'
          OnDblClick = edt_HsDblClick
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#44277#44553#47932#54408' HS'#48512#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sMemo2: TsMemo
          Left = 201
          Top = 105
          Width = 446
          Height = 63
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 2
          OnChange = sMemo1Change
          OnKeyPress = memoLimit
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#44396#48708#49436#47448
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sMemo3: TsMemo
          Left = 201
          Top = 168
          Width = 446
          Height = 63
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 3
          OnChange = sMemo1Change
          OnKeyPress = memoLimit
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#51221#48372
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel15: TsPanel
          Left = 80
          Top = 243
          Width = 281
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #50896#49688#52636#49888#50857#51109
          Color = 16042877
          
          TabOrder = 23
        end
        object edt_SetupDocumentType: TsEdit
          Tag = 105
          Left = 189
          Top = 264
          Width = 33
          Height = 19
          Hint = #45236#44397#49888'1001'
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          OnChange = sEdit20Change
          OnDblClick = sEdit20DblClick
          OnKeyUp = sEdit20KeyUp
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#44540#44144#49436#47448#51333#47448
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn14: TsBitBtn
          Tag = 105
          Left = 223
          Top = 264
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit40: TsEdit
          Left = 248
          Top = 264
          Width = 138
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LetterOfCreditNo: TsEdit
          Left = 189
          Top = 283
          Width = 197
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109'('#44228#50557#49436')'#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sEdit42: TsEdit
          Tag = 106
          Left = 189
          Top = 302
          Width = 33
          Height = 19
          Hint = #53685#54868
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          OnChange = sEdit20Change
          OnDblClick = sEdit20DblClick
          OnKeyUp = sEdit20KeyUp
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44208#51228#44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn15: TsBitBtn
          Tag = 106
          Left = 223
          Top = 302
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 26
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sCurrencyEdit8: TsCurrencyEdit
          Left = 248
          Top = 302
          Width = 138
          Height = 19
          AutoSize = False
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
          OnButtonClick = sCurrencyEdit9ButtonClick
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object sMaskEdit7: TsMaskEdit
          Left = 189
          Top = 321
          Width = 82
          Height = 19
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          OnDblClick = sMaskEdit1DblClick
          OnKeyUp = Mask_CreateRequestDateKeyUp
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn16: TsBitBtn
          Tag = 905
          Left = 272
          Top = 321
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sMaskEdit8: TsMaskEdit
          Left = 189
          Top = 340
          Width = 82
          Height = 19
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          OnDblClick = sMaskEdit1DblClick
          OnKeyUp = Mask_CreateRequestDateKeyUp
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50976#54952#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn17: TsBitBtn
          Tag = 906
          Left = 272
          Top = 340
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 28
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit43: TsEdit
          Tag = 107
          Left = 189
          Top = 359
          Width = 33
          Height = 19
          Hint = #45236#44397#49888'4277'
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          OnChange = sEdit20Change
          OnDblClick = sEdit20DblClick
          OnKeyUp = sEdit20KeyUp
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#44552#44208#51228#51312#44148
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn18: TsBitBtn
          Tag = 107
          Left = 223
          Top = 359
          Width = 24
          Height = 19
          Cursor = crHandPoint
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit44: TsEdit
          Left = 248
          Top = 359
          Width = 142
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sEdit45: TsEdit
          Tag = 2
          Left = 501
          Top = 264
          Width = 65
          Height = 19
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          OnDblClick = edt_CreateUserCodeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#52636#44277#44553' '#49345#45824#48169
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn19: TsBitBtn
          Tag = 2
          Left = 567
          Top = 264
          Width = 24
          Height = 19
          Cursor = crHandPoint
          TabOrder = 31
          TabStop = False
          OnClick = sBitBtn4Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit46: TsEdit
          Left = 501
          Top = 283
          Width = 201
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
        end
        object sEdit47: TsEdit
          Left = 501
          Top = 302
          Width = 201
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
        end
        object sEdit48: TsEdit
          Left = 501
          Top = 321
          Width = 201
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ExportAreaCode: TsEdit
          Tag = 108
          Left = 501
          Top = 340
          Width = 33
          Height = 19
          Hint = #44397#44032
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 2
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          OnChange = sEdit20Change
          OnDblClick = sEdit20DblClick
          OnKeyUp = sEdit20KeyUp
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#52636#51648#50669
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn20: TsBitBtn
          Tag = 108
          Left = 535
          Top = 340
          Width = 24
          Height = 19
          Cursor = crHandPoint
          TabOrder = 32
          TabStop = False
          OnClick = sBitBtn7Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sEdit50: TsEdit
          Left = 501
          Top = 359
          Width = 201
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#54665#51008#54665'('#54869#51064#44592#44288')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sEdit51: TsEdit
          Left = 501
          Top = 378
          Width = 201
          Height = 19
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          SkinData.SkinSection = 'EDIT'
        end
        object sMemo4: TsMemo
          Left = 161
          Top = 401
          Width = 446
          Height = 69
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 18
          OnChange = sMemo1Change
          OnKeyPress = memoLimit
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#49688#52636#54408#47785
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel17: TsPanel
          Left = 72
          Top = 482
          Width = 281
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
          Color = 16042877
          
          TabOrder = 33
        end
        object edt_Sign1: TsEdit
          Left = 193
          Top = 503
          Width = 189
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_Sign2: TsEdit
          Left = 193
          Top = 522
          Width = 189
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
        end
        object edt_Sign3: TsEdit
          Left = 457
          Top = 503
          Width = 101
          Height = 19
          HelpContext = 1
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_ExportAreaName: TsEdit
          Left = 560
          Top = 340
          Width = 142
          Height = 19
          TabStop = False
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn24: TsBitBtn
          Left = 291
          Top = 23
          Width = 18
          Height = 19
          Cursor = crHandPoint
          TabOrder = 35
          TabStop = False
          OnClick = sBitBtn24Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
      end
    end
    object sTabSheet1: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter2: TsSplitter
        Left = 0
        Top = 0
        Width = 1103
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter6: TsSplitter
        Left = 0
        Top = 35
        Width = 1103
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 3
        Width = 1103
        Height = 32
        SkinData.SkinSection = 'PANEL'
        Align = alTop
        
        TabOrder = 0
        object edt_SearchText: TsEdit
          Left = 86
          Top = 5
          Width = 233
          Height = 25
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Visible = False
          OnKeyUp = edt_SearchTextKeyUp
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 81
          Height = 23
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 4
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 86
          Top = 5
          Width = 82
          Height = 25
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          OnDblClick = sMaskEdit1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn1: TsBitBtn
          Left = 321
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          OnClick = sBitBtn1Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn21: TsBitBtn
          Tag = 900
          Left = 167
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 214
          Top = 5
          Width = 82
          Height = 25
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = sMaskEdit1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn23: TsBitBtn
          Tag = 907
          Left = 295
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel25: TsPanel
          Left = 190
          Top = 5
          Width = 25
          Height = 23
          SkinData.SkinSection = 'PANEL'
          Caption = '~'
          
          TabOrder = 7
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 37
        Width = 1103
        Height = 527
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid1DrawColumnCell
        OnTitleClick = sDBGrid1TitleClick
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK1'
            Title.Alignment = taCenter
            Title.Caption = #49440#53469
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 74
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 213
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 193
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 164
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LOC_AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529
            Width = 97
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LOC_AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 38
            Visible = True
          end>
      end
    end
  end
  object sPanel22: TsPanel [3]
    Left = 0
    Top = 118
    Width = 283
    Height = 714
    SkinData.SkinSection = 'TRANSPARENT'
    BevelOuter = bvNone
    Caption = 'sPanel18'
    
    TabOrder = 1
    object sDBGrid3: TsDBGrid
      Left = 0
      Top = 33
      Width = 283
      Height = 681
      Align = alClient
      Color = clWhite
      Ctl3D = True
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid1DrawColumnCell
      OnTitleClick = sDBGrid1TitleClick
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CHK2'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = #49345#54889
          Width = 30
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 170
          Visible = True
        end>
    end
    object sPanel24: TsPanel
      Left = 0
      Top = 0
      Width = 283
      Height = 33
      SkinData.SkinSection = 'PANEL'
      Align = alTop
      BevelOuter = bvNone
      
      TabOrder = 1
      object Mask_fromDate: TsMaskEdit
        Left = 105
        Top = 5
        Width = 74
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20161122'
        OnDblClick = sMaskEdit1DblClick
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn22: TsBitBtn
        Left = 253
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = sBitBtn22Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object Mask_toDate: TsMaskEdit
        Left = 180
        Top = 5
        Width = 74
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20161122'
        OnDblClick = sMaskEdit1DblClick
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51312#54924
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        SkinData.SkinSection = 'EDIT'
      end
    end
  end
  object Pan_bank: TsPanel [4]
    Left = 980
    Top = 620
    Width = 103
    Height = 113
    SkinData.SkinSection = 'PANEL'
    
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    DesignSize = (
      103
      113)
    object sPanel6: TsPanel
      Left = 1
      Top = 1
      Width = 101
      Height = 34
      SkinData.SkinSection = 'DRAGBAR'
      Align = alTop
      Caption = #50808#44397#54872' '#51008#54665#53076#46300
      
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object sBitBtn3: TsBitBtn
        Left = 242
        Top = 5
        Width = 24
        Height = 23
        Cursor = crHandPoint
        TabOrder = 0
        OnClick = sBitBtn3Click
        ImageIndex = 18
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
    end
    object sDBGrid2: TsDBGrid
      Left = 5
      Top = 39
      Width = 93
      Height = 69
      Anchors = [akLeft, akTop, akRight, akBottom]
      Color = clWhite
      DataSource = dsBank
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDblClick = sDBGrid2DblClick
      OnExit = sBitBtn3Click
      OnKeyUp = sDBGrid2KeyUp
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CODE'
          Title.Alignment = taCenter
          Title.Caption = #53076#46300
          Width = 57
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NAME'
          Title.Caption = #51008#54665#47749
          Width = 179
          Visible = True
        end>
    end
  end
  object sPanel4: TsPanel [5]
    Left = 0
    Top = 44
    Width = 1111
    Height = 32
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 3
    object edt_DocNo1: TsEdit
      Left = 64
      Top = 5
      Width = 121
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_DocNo2: TsEdit
      Left = 186
      Top = 5
      Width = 27
      Height = 23
      Color = clWhite
      Ctl3D = True
      MaxLength = 2
      ParentCtl3D = False
      TabOrder = 1
      OnDblClick = edt_DocNo2DblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sBitBtn2: TsBitBtn
      Left = 212
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 2
      TabStop = False
      OnClick = sBitBtn2Click
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_DocNo3: TsEdit
      Left = 237
      Top = 5
      Width = 92
      Height = 23
      Color = clWhite
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 3
      Text = '161122001'
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object sMaskEdit1: TsMaskEdit
      Left = 398
      Top = 5
      Width = 83
      Height = 23
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      Text = '20161122'
      OnDblClick = sMaskEdit1DblClick
      OnKeyUp = Mask_CreateRequestDateKeyUp
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_UserNo: TsEdit
      Left = 560
      Top = 5
      Width = 57
      Height = 23
      Color = clBtnFace
      Ctl3D = True
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 5
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 480
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      OnClick = btn_CalClick
      ImageIndex = 9
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sCheckBox1: TsCheckBox
      Left = 992
      Top = 6
      Width = 64
      Height = 19
      Caption = #46356#48260#44536
      TabOrder = 7
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edt_msg1: TsEdit
      Tag = 109
      Left = 686
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 2
      ParentCtl3D = False
      TabOrder = 8
      OnDblClick = sEdit20DblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn25: TsBitBtn
      Tag = 109
      Left = 719
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 9
      TabStop = False
      OnClick = sBitBtn7Click
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg2: TsEdit
      Tag = 110
      Left = 786
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 2
      ParentCtl3D = False
      TabOrder = 10
      OnDblClick = sEdit20DblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn26: TsBitBtn
      Tag = 110
      Left = 819
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 11
      TabStop = False
      OnClick = sBitBtn7Click
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sPanel1: TsPanel [6]
    Left = 0
    Top = 0
    Width = 1111
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 4
    object sSpeedButton2: TsSpeedButton
      Left = 360
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 652
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton4: TsSpeedButton
      Left = 731
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'ALPHACOMBOBOX'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 439
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel10: TsLabel
      Left = 8
      Top = 5
      Width = 135
      Height = 17
      Caption = #45236#44397#49888#50857#51109' '#44060#49444#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel11: TsLabel
      Left = 8
      Top = 20
      Width = 44
      Height = 13
      Caption = 'LOCAPP'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 147
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton6: TsSpeedButton
      Left = 910
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1025
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object sButton2: TsButton
      Left = 157
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Left = 224
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnEditClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Left = 291
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 370
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      OnClick = btnCopyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 662
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 449
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      OnClick = btnSaveClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 516
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      OnClick = btnSaveClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Left = 583
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
    object sButton1: TsButton
      Left = 741
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      OnClick = sButton1Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
    end
    object sButton3: TsButton
      Left = 834
      Top = 2
      Width = 74
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      OnClick = sButton3Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 408
  end
  object qryBank: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [Prefix]'
      '      ,[CODE]'
      '      ,[Remark]'
      '      ,[Map]'
      '      ,[NAME]'
      '  FROM [KIS_COMM].[dbo].[CODE2NDD]'
      '  WHERE Prefix = N'#39'APPSPC'#51008#54665#39)
    Left = 24
    Top = 456
  end
  object dsBank: TDataSource
    DataSet = qryBank
    Left = 56
    Top = 456
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO,SUBSTRING(MAINT_NO,16,100) as MAINT_NO2, DATEE, ' +
        'USER_ID, MESSAGE1, MESSAGE2, '
      'BUSINESS, '
      'N4025.DOC_NAME as BUSINESSNAME,'
      
        'OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFE' +
        'RNO7, OFFERNO8, OFFERNO9, OPEN_NO, APP_DATE, DOC_PRD, DELIVERY, ' +
        'EXPIRY,'
      'TRANSPRT, '
      'PSHIP.DOC_NAME as TRANSPRTNAME,'
      
        'GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_BANK2,' +
        ' APPLIC, APPLIC1, APPLIC2, APPLIC3, BENEFC, BENEFC1, BENEFC2, BE' +
        'NEFC3, EXNAME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, ' +
        'DOCCOPY4, DOCCOPY5, DOC_ETC, DOC_ETC1,'
      'LOC_TYPE, '
      'N4487.DOC_NAME as LOC_TYPENAME,'
      'LOC_AMT, LOC_AMTC, '
      'DOC_DTL,'
      'N1001.DOC_NAME as DOC_DTLNAME,'
      
        'DOC_NO, DOC_AMT, DOC_AMTC, LOADDATE, EXPDATE, IM_NAME, IM_NAME1,' +
        ' IM_NAME2, IM_NAME3, DEST,NAT.DOC_NAME as DESTNAME, ISBANK1, ISB' +
        'ANK2,'
      'PAYMENT,'
      'N4277.DOC_NAME as PAYMENTNAME,'
      
        'EXGOOD, EXGOOD1, CHKNAME1, CHKNAME2, CHK1, CHK2, CHK3, PRNO, LCN' +
        'O, BSN_HSCODE, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2,' +
        ' BNFADDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      
        'FROM [LOCAPP] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAPP.LOC_TYPE = N4487.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON LOCAPP.BUSINESS =' +
        ' N4025.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOCAPP.TRANSPRT = P' +
        'SHIP.CODE'
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON LOCAPP.DOC_DTL = ' +
        'N1001.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON LOCAPP.PAYMENT = ' +
        'N4277.CODE'#9
      
        #9#9#9#9#9#9'   LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD w' +
        'ith(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCAPP.DEST = NAT.CODE')
    Left = 24
    Top = 376
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMAINT_NO2: TStringField
      FieldName = 'MAINT_NO2'
      ReadOnly = True
      Size = 35
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryListBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryListTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryListGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryListDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryListDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryListDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryListDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryListDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object qryListLOC_AMT: TBCDField
      FieldName = 'LOC_AMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListLOC_AMTC: TStringField
      FieldName = 'LOC_AMTC'
      Size = 3
    end
    object qryListDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryListDOC_DTLNAME: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryListDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryListLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryListEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryListIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryListIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryListIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryListIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
    object qryListISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryListISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryListPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryListPAYMENTNAME: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object qryListEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryListCHKNAME1: TStringField
      FieldName = 'CHKNAME1'
      Size = 35
    end
    object qryListCHKNAME2: TStringField
      FieldName = 'CHKNAME2'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      OnGetText = qryListCHK1GetText
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListLCNO: TStringField
      FieldName = 'LCNO'
      Size = 35
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListGOODDES1: TStringField
      FieldName = 'GOODDES1'
      Size = 350
    end
    object qryListREMARK1: TStringField
      FieldName = 'REMARK1'
      Size = 350
    end
    object qryListDOC_ETC1: TStringField
      FieldName = 'DOC_ETC1'
      Size = 350
    end
    object qryListEXGOOD1: TStringField
      FieldName = 'EXGOOD1'
      Size = 350
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 376
  end
  object spCopyLOCAPP: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'CopyLOCAPP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@BaseDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@CopyDocNO'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@UserID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 24
    Top = 488
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE LOCAPP'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 24
    Top = 344
  end
  object PopupMenu1: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = PopupMenu1Popup
    Left = 24
    Top = 312
    object N1: TMenuItem
      Caption = #51204#49569#51456#48708
      ImageIndex = 29
      OnClick = sButton1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = btnEditClick
    end
    object N4: TMenuItem
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = btnDelClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object d1: TMenuItem
      Caption = #52636#47141#54616#44592
      object N8: TMenuItem
        Caption = #48120#47532#48372#44592
        ImageIndex = 15
        OnClick = N8Click
      end
      object N9: TMenuItem
        Tag = 1
        Caption = #54532#47536#53944
        ImageIndex = 21
        OnClick = N8Click
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = 'ADMIN'
      Enabled = False
      ImageIndex = 16
    end
  end
end
