inherited UI_INF707_frm: TUI_INF707_frm
  Left = 442
  Top = 134
  Caption = 'UI_INF707_frm'
  ClientWidth = 1114
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sSplitter1: TsSplitter
    Width = 1114
  end
  inherited sSplitter3: TsSplitter
    Width = 1114
  end
  inherited sPageControl1: TsPageControl [2]
    Width = 1114
    OnChange = sPageControl1Change
    inherited sTabSheet1: TsTabSheet
      inherited sSplitter4: TsSplitter
        Width = 1106
      end
      inherited page1_RightPanel: TsPanel
        Width = 806
        DesignSize = (
          806
          559)
        inherited sSpeedButton6: TsSpeedButton
          Left = 398
          Top = 0
          Height = 559
        end
        inherited sPanel8: TsPanel [1]
          Left = 15
          Top = 384
          Width = 370
          Height = 23
        end
        inherited edt_APPDATE: TsMaskEdit
          Left = 106
          Top = 26
          SkinData.CustomColor = False
        end
        inherited edt_ImpCd4: TsEdit [3]
          Left = 106
          Top = 297
        end
        inherited edt_AdBank2: TsEdit [4]
          Left = 512
          Top = 48
          Width = 279
        end
        inherited edt_ImpCd2: TsEdit [5]
          Left = 106
          Top = 253
        end
        inherited edt_In_Mathod1: TsEdit [6]
          Left = 156
          Top = 48
          Width = 229
        end
        inherited edt_ApBank5: TsEdit [7]
          Left = 106
          Top = 158
          Width = 279
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Style = [fsBold]
        end
        inherited edt_ImpCd2_1: TsEdit [8]
          Left = 156
          Top = 253
          Width = 229
        end
        inherited edt_AdBank3: TsEdit [9]
          Left = 512
          Top = 70
          Width = 279
        end
        inherited edt_ApBank2: TsEdit [10]
          Left = 106
          Top = 92
          Width = 279
        end
        inherited edt_ILno5: TsEdit [11]
          Left = 478
          Top = 319
          Width = 172
        end
        inherited edt_ImpCd5_1: TsEdit [12]
          Left = 156
          Top = 319
          Width = 229
        end
        inherited edt_EXName1: TsEdit [13]
          Left = 552
          Top = 408
          Width = 239
          SkinData.CustomColor = False
          BoundLabel.Caption = #49888#52397#50629#52404
        end
        inherited edt_AdPay: TsEdit [14]
          Left = 512
          Top = 114
          Width = 51
          BoundLabel.Font.Color = clBlack
        end
        inherited edt_ApBank4: TsEdit [15]
          Left = 106
          Top = 136
          Width = 279
        end
        inherited edt_AdInfo1: TsEdit [16]
          Left = 106
          Top = 408
          Width = 279
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#51221#48372
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited edt_EXAddr1: TsEdit [17]
          Left = 552
          Top = 474
          Width = 239
        end
        inherited edt_ApBank1: TsEdit [18]
          Left = 106
          Top = 70
          Width = 279
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51032#47280#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        inherited btn_LCNumber5: TsBitBtn [19]
          Left = 720
          Top = 319
          Visible = False
        end
        inherited btn_LCNumber3: TsBitBtn [20]
          Left = 720
          Top = 275
          Visible = False
        end
        inherited edt_ApBank3: TsEdit [21]
          Left = 106
          Top = 114
          Width = 279
        end
        inherited sPanel6: TsPanel [22]
          Left = 421
          Top = 2
          Width = 370
          Height = 23
        end
        inherited edt_AdInfo4: TsEdit [23]
          Left = 106
          Top = 474
          Width = 279
        end
        inherited edt_ILno4: TsEdit [24]
          Left = 478
          Top = 297
          Width = 172
        end
        inherited btn_ImpCd2: TsBitBtn [25]
          Visible = False
        end
        inherited edt_AdInfo3: TsEdit [26]
          Left = 106
          Top = 452
          Width = 279
        end
        inherited btn_ImpCd1: TsBitBtn [27]
          Visible = False
        end
        inherited edt_ImpCd4_1: TsEdit [28]
          Left = 156
          Top = 297
          Width = 229
        end
        inherited edt_AdBank1: TsEdit [29]
          Left = 512
          Top = 26
          Width = 279
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '('#55148#47581')'#53685#51648#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited edt_AdPay1: TsEdit [30]
          Left = 564
          Top = 114
          Width = 227
        end
        inherited edt_ILno1: TsEdit [31]
          Left = 478
          Top = 231
          Width = 172
        end
        inherited edt_EXName2: TsEdit [32]
          Left = 552
          Top = 430
          Width = 239
        end
        inherited btn_ImpCd4: TsBitBtn [33]
          Left = 170
          Top = 253
          Visible = False
        end
        inherited btn_ImpCd3: TsBitBtn [34]
          Left = 170
          Top = 231
          Visible = False
        end
        inherited edt_ILAMT4: TsCurrencyEdit [35]
          Left = 701
          Top = 297
          Width = 88
        end
        inherited sPanel5: TsPanel [36]
          Left = 15
          Top = 2
          Width = 370
          Height = 23
        end
        inherited edt_AdBank: TsEdit [37]
          Left = 520
          Top = 41
          Visible = False
          BoundLabel.Active = False
          BoundLabel.Caption = ''
        end
        inherited btn_NoticeBank: TsBitBtn [38]
          Left = 562
          Top = 41
          Visible = False
        end
        inherited btn_GrantingCredit: TsBitBtn [39]
          Left = 651
          Top = 90
          Visible = False
        end
        inherited btn_LCNumber2: TsBitBtn [40]
          Left = 736
          Top = 253
          Visible = False
        end
        inherited btn_LCNumber4: TsBitBtn [41]
          Left = 736
          Top = 297
          Visible = False
        end
        inherited edt_ImpCd3_1: TsEdit [42]
          Left = 156
          Top = 275
          Width = 229
        end
        inherited btn_LCNumber1: TsBitBtn [43]
          Left = 704
          Top = 231
          Visible = False
        end
        inherited sPanel2: TsPanel [44]
          Left = 15
          Top = 207
          Width = 370
          Height = 23
        end
        inherited edt_EXName3: TsEdit [45]
          Left = 552
          Top = 452
        end
        inherited edt_EXAddr2: TsEdit [46]
          Left = 552
          Top = 496
          Width = 239
        end
        inherited edt_ILAMT5: TsCurrencyEdit [47]
          Left = 701
          Top = 319
          Width = 88
        end
        inherited edt_ILAMT2: TsCurrencyEdit
          Left = 701
          Top = 253
          Width = 88
        end
        inherited edt_AdBank4: TsEdit [49]
          Left = 512
          Top = 92
          Width = 279
        end
        inherited sPanel7: TsPanel [50]
          Left = 421
          Top = 207
          Width = 370
          Height = 23
        end
        inherited btn_OpenMethod: TsBitBtn [51]
          Left = 234
          Top = 48
          Visible = False
        end
        inherited btn_ApBank: TsBitBtn [52]
          Left = 122
          Top = 48
          Visible = False
        end
        inherited edt_ILAMT3: TsCurrencyEdit [53]
          Left = 701
          Top = 275
          Width = 88
        end
        inherited edt_ApBank: TsEdit [54]
          Left = 106
          Top = 70
          Visible = False
          SkinData.CustomColor = False
          BoundLabel.Active = False
        end
        inherited edt_ILno3: TsEdit [55]
          Left = 478
          Top = 275
          Width = 172
        end
        inherited edt_AdInfo5: TsEdit [56]
          Left = 106
          Top = 496
          Width = 279
        end
        inherited edt_AdInfo2: TsEdit [57]
          Left = 106
          Top = 430
          Width = 279
        end
        inherited edt_ImpCd1: TsEdit [58]
          Left = 106
          Top = 231
          BoundLabel.Font.Color = clBlack
        end
        inherited edt_ImpCd1_1: TsEdit [59]
          Left = 156
          Top = 231
          Width = 229
        end
        inherited edt_ImpCd3: TsEdit [60]
          Left = 106
          Top = 275
        end
        inherited edt_ImpCd5: TsEdit [61]
          Left = 106
          Top = 319
        end
        inherited sPanel9: TsPanel [62]
          Left = 421
          Top = 384
          Width = 370
          Height = 23
          Caption = #49888#52397#50629#52404#51221#48372
        end
        inherited edt_In_Mathod: TsEdit [63]
          Left = 106
          Top = 48
          SkinData.CustomColor = False
        end
        inherited edt_ILno2: TsEdit [64]
          Left = 478
          Top = 253
          Width = 172
        end
        inherited edt_ILAMT1: TsCurrencyEdit [65]
          Left = 701
          Top = 231
          Width = 88
        end
        inherited btn_ImpCd5: TsBitBtn [66]
          Left = 170
          Top = 275
          Visible = False
        end
        inherited edt_ILCur1: TsEdit
          Left = 661
          Top = 231
        end
        inherited edt_ILCur2: TsEdit
          Left = 661
          Top = 253
        end
        inherited edt_ILCur3: TsEdit
          Left = 661
          Top = 275
        end
        inherited edt_ILCur4: TsEdit
          Left = 661
          Top = 297
        end
        inherited edt_ILCur5: TsEdit
          Left = 661
          Top = 319
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sSplitter5: TsSplitter
        Width = 1106
      end
      inherited page2_RightPanel: TsPanel
        Width = 806
        inherited sLabel1: TsLabel
          Left = 599
          Top = 445
        end
        object sSpeedButton7: TsSpeedButton [1]
          Left = 398
          Top = -2
          Width = 10
          Height = 563
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        inherited edt_Benefc4: TsEdit [2]
          Left = 127
          Top = 513
          Width = 258
        end
        inherited sPanel19: TsPanel [3]
          Left = 15
          Top = 425
          Width = 370
          Height = 21
        end
        inherited edt_NwcdCur: TsEdit [4]
          Left = 534
          Top = 213
        end
        inherited sPanel18: TsPanel [5]
          Left = 421
          Top = 425
          Width = 370
        end
        inherited sBitBtn2: TsBitBtn [6]
          Left = 667
          Top = 150
          Visible = False
        end
        inherited edt_IncdCur: TsEdit [7]
          Left = 534
          Top = 87
        end
        inherited edt_Applic4: TsEdit [8]
          Left = 127
          Top = 362
          Width = 258
        end
        inherited sPanel15: TsPanel [9]
          Left = 421
          Top = 274
          Width = 370
        end
        inherited edt_Applic5: TsEdit [10]
          Left = 127
          Top = 384
          Width = 258
          BoundLabel.Font.Height = -12
        end
        inherited edt_DecdAmt: TsCurrencyEdit [11]
          Left = 591
          Top = 150
          Width = 200
        end
        inherited edt_exDate: TsMaskEdit [12]
          Left = 534
          Top = 24
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50976#54952#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited sPanel14: TsPanel [13]
          Left = 421
          Top = 191
          Width = 370
        end
        inherited sPanel13: TsPanel [14]
          Left = 421
          Top = 128
          Width = 370
        end
        inherited sPanel12: TsPanel [15]
          Left = 421
          Top = 65
          Width = 370
        end
        inherited edt_Benefc2: TsEdit [16]
          Left = 127
          Top = 469
          Width = 258
        end
        inherited edt_Benefc5: TsEdit [17]
          Left = 127
          Top = 535
          Width = 258
        end
        inherited sPanel3: TsPanel [18]
          Left = 15
          Top = 128
          Width = 370
        end
        inherited sPanel4: TsPanel
          Left = 15
          Top = 191
          Width = 370
          Caption = 'Date / Number Of Amendment'
        end
        inherited edt_BfcdCur: TsEdit [20]
          Left = 534
          Top = 296
        end
        inherited sPanel17: TsPanel [21]
          Left = 15
          Top = 274
          Width = 370
          Height = 21
        end
        inherited sBitBtn4: TsBitBtn [22]
          Left = 659
          Top = 296
          Visible = False
        end
        inherited edt_BfcdAmt: TsCurrencyEdit [23]
          Left = 591
          Top = 296
          Width = 200
        end
        inherited edt_Applic2: TsEdit [24]
          Left = 127
          Top = 318
          Width = 258
        end
        inherited edt_AmdNo: TsCurrencyEdit [25]
          Left = 127
          Top = 235
          Width = 58
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51312#44148#48320#44221#54943#49688
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          SkinData.CustomColor = False
        end
        inherited sPanel16: TsPanel [26]
          Left = 421
          Top = 350
          Width = 370
        end
        inherited edt_Applic1: TsEdit [27]
          Left = 127
          Top = 296
          Width = 258
          SkinData.CustomColor = False
          BoundLabel.Font.Height = -12
        end
        inherited edt_CdPerp: TsCurrencyEdit [28]
          Left = 534
          Top = 447
        end
        inherited sBitBtn6: TsBitBtn [29]
          Left = 215
          Top = 448
          TabOrder = 47
          Visible = False
        end
        inherited sBitBtn3: TsBitBtn [30]
          Left = 591
          Top = 211
          Visible = False
        end
        inherited edt_DecdCur: TsEdit [31]
          Left = 534
          Top = 150
        end
        inherited edt_Benefc3: TsEdit [32]
          Left = 127
          Top = 491
          Width = 258
        end
        inherited sPanel1: TsPanel [33]
          Left = 15
          Top = 2
          Width = 370
          Font.Color = clBlack
        end
        inherited edt_CdPerm: TsCurrencyEdit [34]
          Left = 613
          Top = 447
        end
        inherited edt_Benefc1: TsEdit [35]
          Left = 127
          Top = 447
          Width = 258
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'Beneficiary'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited edt_Applic3: TsEdit [36]
          Left = 127
          Top = 340
          Width = 258
        end
        inherited edt_IssDate: TsMaskEdit [37]
          Left = 126
          Top = 150
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          SkinData.CustomColor = False
        end
        inherited edt_NwcdAmt: TsCurrencyEdit [38]
          Left = 591
          Top = 213
          Width = 200
        end
        inherited edt_Benefc: TsEdit [39]
          Left = 145
          Top = 447
          Visible = False
          SkinData.CustomColor = False
          BoundLabel.Active = False
        end
        inherited edt_CdMax: TsEdit [40]
          Left = 534
          Top = 372
        end
        inherited edt_IncdAmt: TsCurrencyEdit [41]
          Left = 591
          Top = 87
          Width = 200
        end
        inherited btn_Beneficiary: TsBitBtn [42]
          Left = 675
          Top = 87
          Visible = False
        end
        inherited edt_cdNo: TsEdit [43]
          Left = 127
          Top = 24
          Width = 258
          SkinData.CustomColor = False
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#49888#51008#54665' '#52280#51312#49324#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited sPanel11: TsPanel [44]
          Left = 421
          Top = 2
          Width = 370
        end
        inherited edt_CdMax1: TsEdit [45]
          Left = 591
          Top = 372
          Width = 200
        end
        inherited sBitBtn5: TsBitBtn [46]
          Left = 603
          Top = 369
          Visible = False
        end
        object sPanel21: TsPanel
          Left = 15
          Top = 65
          Width = 370
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Receive'#39's Reference (L/C No)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 44
        end
        object edt_RcvRef: TsEdit
          Left = 127
          Top = 87
          Width = 258
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 45
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#49888#51008#54665' '#52280#51312#49324#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AmdDate: TsMaskEdit
          Left = 127
          Top = 213
          Width = 78
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 46
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51312#44148#48320#44221#54943#49688
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sSplitter6: TsSplitter
        Width = 1106
      end
      inherited page3_LeftPanel: TsPanel
        Height = 559
      end
      inherited page3_RightPanel: TsPanel
        Width = 806
        Height = 559
        object sSpeedButton1: TsSpeedButton
          Left = 398
          Top = -2
          Width = 11
          Height = 569
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel26: TsPanel
          Left = 15
          Top = 229
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44060#49444#51008#54665
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
        end
        object sPanel23: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Additional Amounts Covered'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edt_Aacv1: TsEdit
          Left = 15
          Top = 26
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Aacv2: TsEdit
          Left = 15
          Top = 48
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Aacv3: TsEdit
          Left = 15
          Top = 70
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Aacv4: TsEdit
          Left = 15
          Top = 92
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel24: TsPanel
          Left = 421
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Sender to Receive Information'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object edt_SrInfo1: TsEdit
          Left = 421
          Top = 26
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SrInfo2: TsEdit
          Left = 421
          Top = 48
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SrInfo3: TsEdit
          Left = 421
          Top = 70
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SrInfo4: TsEdit
          Left = 421
          Top = 92
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SrInfo5: TsEdit
          Left = 421
          Top = 114
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SrInfo6: TsEdit
          Left = 421
          Top = 136
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OpAddr3: TsEdit
          Left = 74
          Top = 363
          Width = 311
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 17
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OpAddr2: TsEdit
          Left = 74
          Top = 341
          Width = 311
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OpAddr1: TsEdit
          Left = 74
          Top = 319
          Width = 311
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 15
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_OpBank3: TsEdit
          Left = 74
          Top = 297
          Width = 311
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OpBank2: TsEdit
          Left = 74
          Top = 275
          Width = 311
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OpBank1: TsEdit
          Left = 74
          Top = 253
          Width = 311
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel27: TsPanel
          Left = 421
          Top = 229
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Issuing Bank'#39's Reference'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 25
        end
        object edt_IssBank1: TsEdit
          Left = 421
          Top = 253
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 18
          SkinData.SkinSection = 'EDIT'
        end
        object edt_IssBank2: TsEdit
          Left = 421
          Top = 275
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 19
          SkinData.SkinSection = 'EDIT'
        end
        object edt_IssBank3: TsEdit
          Left = 421
          Top = 297
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 20
          SkinData.SkinSection = 'EDIT'
        end
        object edt_IssBank4: TsEdit
          Left = 421
          Top = 319
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 21
          SkinData.SkinSection = 'EDIT'
        end
        object edt_IssBank5: TsEdit
          Left = 421
          Top = 341
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 22
          SkinData.SkinSection = 'EDIT'
        end
        object edt_IssBank6: TsEdit
          Left = 421
          Top = 363
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 23
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object Page4: TsTabSheet [3]
      Caption = 'Page4'
      object sSplitter7: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel10: TsPanel
        Left = 0
        Top = 3
        Width = 305
        Height = 559
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alLeft
        BevelOuter = bvNone
        
        TabOrder = 0
      end
      object page4_RightPanel: TsPanel
        Left = 305
        Top = 3
        Width = 801
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 1
        DesignSize = (
          801
          559)
        object sLabel24: TsLabel
          Left = 18
          Top = 210
          Width = 253
          Height = 15
          Caption = #49688#53441'('#48156#49569')'#51648' : '#50868#49569#49688#45800#51060' DQ '#51068#46412#47564' '#51077#47141#44032#45733
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel27: TsLabel
          Left = 18
          Top = 303
          Width = 245
          Height = 15
          Caption = #52572#51333#47785#51201#51648' : '#50868#49569#49688#45800#51060' DQ '#51068#46412#47564' '#51077#47141#44032#45733
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sSpeedButton8: TsSpeedButton
          Left = 398
          Top = -10
          Width = 11
          Height = 569
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel32: TsPanel
          Left = 421
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Latest Date of Shipment'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edt_lstDate: TsMaskEdit
          Left = 534
          Top = 26
          Width = 96
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 1
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#51333#49440#51201#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel33: TsPanel
          Left = 421
          Top = 94
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Shipment Period'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object edt_shipPD1: TsEdit
          Left = 421
          Top = 118
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD2: TsEdit
          Left = 421
          Top = 140
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD3: TsEdit
          Left = 421
          Top = 162
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD4: TsEdit
          Left = 421
          Top = 184
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD5: TsEdit
          Left = 421
          Top = 206
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD6: TsEdit
          Left = 421
          Top = 228
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel22: TsPanel
          Left = 421
          Top = 278
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Narrative'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          object sLabel46: TsLabel
            Left = 218
            Top = 3
            Width = 147
            Height = 17
            Caption = '['#52572#45824' 35'#54665' 50'#50676' '#51077#47141' '#21487']'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object memo_Narrat1: TsMemo
          Left = 443
          Top = 302
          Width = 328
          Height = 157
          MaxLength = 1750
          ScrollBars = ssVertical
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel37: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Port of Loading/Airport of Departure'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 15
        end
        object edt_SunjukPort: TsEdit
          Left = 15
          Top = 26
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel38: TsPanel
          Left = 15
          Top = 94
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Port of Discharge/Airport of Destination'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object edt_dochackPort: TsEdit
          Left = 15
          Top = 118
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel39: TsPanel
          Left = 15
          Top = 186
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'Place of Taking in Charge/Dispatch from '#183#183#183'/Place of Receipt'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 17
        end
        object edt_loadOn: TsEdit
          Left = 15
          Top = 226
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel40: TsPanel
          Left = 15
          Top = 278
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'PANEL'
          Caption = 
            'Place of Final Destination/For Transportation to '#183#183#183'/Place of De' +
            'livery'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
        object edt_forTran: TsEdit
          Left = 15
          Top = 319
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet7: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      inherited sSplitter2: TsSplitter
        Width = 1106
      end
      inherited dataSearch_Panel: TsPanel
        Width = 1106
        inherited edt_SearchText: TsEdit
          Height = 23
          TabOrder = 6
        end
        inherited com_SearchKeyword: TsComboBox
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        inherited Mask_SearchDate1: TsMaskEdit
          Height = 23
          OnDblClick = sMaskEdit1DblClick
        end
        inherited sBitBtn1: TsBitBtn
          TabOrder = 5
          OnClick = sBitBtn1Click
        end
        inherited sBitBtn21: TsBitBtn
          Tag = 902
          TabOrder = 2
          TabStop = False
          OnClick = btn_CalClick
        end
        inherited Mask_SearchDate2: TsMaskEdit
          Height = 23
          TabOrder = 3
          OnDblClick = sMaskEdit1DblClick
        end
        inherited sBitBtn23: TsBitBtn
          Tag = 903
          TabOrder = 4
          TabStop = False
          OnClick = btn_CalClick
        end
      end
      inherited sDBGrid1: TsDBGrid
        Width = 1106
        TabStop = False
        DataSource = dsList
        OnDrawColumnCell = sDBGrid2DrawColumnCell
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 180
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 82
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'MSEQ'
            Title.Alignment = taCenter
            Title.Caption = #51312#44148#48320#44221#54943#49688
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CD_NO'
            Title.Alignment = taCenter
            Title.Caption = 'L/C'#48264#54840
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51032#47280#51008#54665
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NWCD_AMT'
            Title.Alignment = taCenter
            Title.Caption = #44552#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NWCD_CUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 52
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #49688#49888#51088
            Width = 52
            Visible = True
          end>
      end
    end
  end
  inherited btn_Panel: TsPanel [3]
    Width = 1114
    inherited sSpeedButton2: TsSpeedButton
      Left = 199
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 632
      Visible = False
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 643
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 476
    end
    object sLabel59: TsLabel [4]
      Left = 8
      Top = 5
      Width = 187
      Height = 17
      Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#48320#44221#51025#45813#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel [5]
      Left = 8
      Top = 20
      Width = 36
      Height = 13
      Caption = 'INF707'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    inherited btnExit: TsButton
      OnClick = btnExitClick
    end
    inherited btnNew: TsButton
      Left = 1124
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 847
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 209
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      Left = 618
      Visible = False
    end
    inherited btnPrint: TsButton
      Left = 779
      Visible = False
    end
    inherited btnTemp: TsButton
      Left = 687
      Visible = False
    end
    inherited btnSave: TsButton
      Left = 772
      Visible = False
    end
    inherited btnCancel: TsButton
      Left = 641
      Visible = False
    end
    inherited sButton1: TsButton
      Left = 823
      Visible = False
    end
    inherited sButton3: TsButton
      Visible = False
    end
    object sButton4: TsButton
      Left = 276
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 11
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton2: TsButton
      Tag = 1
      Left = 375
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 12
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  inherited maintno_Panel: TsPanel [4]
    Width = 1114
    inherited sMaskEdit1: TsMaskEdit
      OnDblClick = sMaskEdit1DblClick
    end
    inherited btn_Cal: TsBitBtn
      OnClick = btn_CalClick
    end
    inherited edt_chasu: TsEdit
      Left = 186
    end
    inherited edt_msg1: TsEdit
      Left = 569
    end
    inherited sBitBtn7: TsBitBtn
      Left = 722
      Visible = False
    end
    inherited edt_msg2: TsEdit
      Left = 641
    end
    inherited sBitBtn8: TsBitBtn
      Left = 746
      Visible = False
    end
  end
  inherited sPanel52: TsPanel
    inherited sDBGrid2: TsDBGrid [0]
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      OnDrawColumnCell = sDBGrid2DrawColumnCell
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 70
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 165
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MSEQ'
          Title.Alignment = taCenter
          Title.Caption = #52264#49688
          Width = 36
          Visible = True
        end>
    end
    inherited sPanel53: TsPanel [1]
      inherited sBitBtn22: TsBitBtn
        OnClick = sBitBtn22Click
      end
      inherited Mask_toDate: TsMaskEdit
        BoundLabel.Font.Height = -15
      end
    end
  end
  inherited qryList: TADOQuery
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      
        'SELECT I707_1.MAINT_NO,I707_1.MSEQ,I707_1.AMD_NO,I707_1.MESSAGE1' +
        ',I707_1.MESSAGE2,I707_1.[USER_ID],I707_1.DATEE,I707_1.APP_DATE,I' +
        '707_1.IN_MATHOD,'
      
        #9'   I707_1.AP_BANK,I707_1.AP_BANK1,I707_1.AP_BANK2,I707_1.AP_BAN' +
        'K3,I707_1.AP_BANK4,I707_1.AP_BANK5,'
      
        #9'   I707_1.AD_BANK,I707_1.AD_BANK1,I707_1.AD_BANK2,I707_1.AD_BAN' +
        'K3,I707_1.AD_BANK4,'
      
        #9'   I707_1.IL_NO1,I707_1.IL_NO2,I707_1.IL_NO3,I707_1.IL_NO4,I707' +
        '_1.IL_NO5,'
      
        #9'   I707_1.IL_AMT1,I707_1.IL_AMT2,I707_1.IL_AMT3,I707_1.IL_AMT4,' +
        'I707_1.IL_AMT5,'
      
        #9'   I707_1.IL_CUR1,I707_1.IL_CUR2,I707_1.IL_CUR3,I707_1.IL_CUR4,' +
        'I707_1.IL_CUR5,'
      
        #9'   I707_1.AD_INFO1,I707_1.AD_INFO2,I707_1.AD_INFO3,I707_1.AD_IN' +
        'FO4,I707_1.AD_INFO5,'
      
        #9'   I707_1.CD_NO,I707_1.RCV_REF,I707_1.IBANK_REF,I707_1.ISS_BANK' +
        '1,I707_1.ISS_BANK2,I707_1.ISS_BANK3,I707_1.ISS_BANK4,I707_1.ISS_' +
        'BANK5,I707_1.ISS_ACCNT,'
      
        '                   I707_1.ISS_DATE,I707_1.AMD_DATE,I707_1.EX_DAT' +
        'E,I707_1.EX_PLACE,I707_1.CHK1,I707_1.CHK2,I707_1.CHK3,'
      
        #9'   I707_1.F_INTERFACE,I707_1.IMP_CD1,I707_1.IMP_CD2,I707_1.IMP_' +
        'CD3,I707_1.IMP_CD4,I707_1.IMP_CD5,I707_1.Prno,'
      ''
      
        #9'   I707_2.MAINT_NO,I707_2.MSEQ,I707_2.AMD_NO,I707_2.APPLIC1,I70' +
        '7_2.APPLIC2,I707_2.APPLIC3,I707_2.APPLIC4,I707_2.APPLIC5,'
      
        #9'   I707_2.BENEFC1,I707_2.BENEFC2,I707_2.BENEFC3,I707_2.BENEFC4,' +
        'I707_2.BENEFC5,I707_2.INCD_CUR,I707_2.INCD_AMT,I707_2.DECD_CUR,I' +
        '707_2.DECD_AMT,'
      
        #9'   I707_2.NWCD_CUR,I707_2.NWCD_AMT,I707_2.CD_PERP,I707_2.CD_PER' +
        'M,I707_2.CD_MAX,I707_2.AA_CV1,I707_2.AA_CV2,I707_2.AA_CV3,I707_2' +
        '.AA_CV4,'
      
        #9'   I707_2.LOAD_ON,I707_2.FOR_TRAN,I707_2.LST_DATE,I707_2.SHIP_P' +
        'D,'
      
        #9'   I707_2.SHIP_PD1,I707_2.SHIP_PD2,I707_2.SHIP_PD3,I707_2.SHIP_' +
        'PD4,I707_2.SHIP_PD5,I707_2.SHIP_PD6,'
      
        '                   I707_2.NARRAT,I707_2.NARRAT_1,I707_2.SR_INFO1' +
        ',I707_2.SR_INFO2,I707_2.SR_INFO3,I707_2.SR_INFO4,I707_2.SR_INFO5' +
        ',I707_2.SR_INFO6,'
      
        #9'   I707_2.EX_NAME1,I707_2.EX_NAME2,I707_2.EX_NAME3,I707_2.EX_AD' +
        'DR1,I707_2.EX_ADDR2,'
      
        #9'   I707_2.OP_BANK1,I707_2.OP_BANK2,I707_2.OP_BANK3,I707_2.OP_AD' +
        'DR1,I707_2.OP_ADDR2,I707_2.BFCD_AMT,I707_2.BFCD_CUR,I707_2.SUNJU' +
        'CK_PORT,I707_2.DOCHACK_PORT'
      #9'   '
      #9
      #9'  ,Mathod707.DOC_NAME as mathod_Name'
      #9'  ,IMPCD707_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD707_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD707_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD707_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD707_5.DOC_NAME as Imp_Name_5'
      #9'  ,CDMAX707.DOC_NAME as CDMAX_Name'
      #9'       '
      '  '
      'FROM [dbo].[INF707_1] AS I707_1'
      
        'INNER JOIN [dbo].[INF707_2] AS I707_2 ON I707_1.MAINT_NO = I707_' +
        '2.MAINT_NO AND I707_1.MSEQ = I707_2.MSEQ'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod707 ON I707_1.IN_MATHOD = Mathod' +
        '707.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_1 ON I707_1.IMP_CD1 = IMPCD' +
        '707_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_2 ON I707_1.IMP_CD2 = IMPCD' +
        '707_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_3 ON I707_1.IMP_CD3 = IMPCD' +
        '707_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_4 ON I707_1.IMP_CD4 = IMPCD' +
        '707_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_5 ON I707_1.IMP_CD5 = IMPCD' +
        '707_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX707 ON I707_2.CD_MAX = CDMAX707' +
        '.CODE')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryListAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListRCV_REF: TStringField
      FieldName = 'RCV_REF'
      Size = 35
    end
    object qryListIBANK_REF: TStringField
      FieldName = 'IBANK_REF'
      Size = 35
    end
    object qryListISS_BANK1: TStringField
      FieldName = 'ISS_BANK1'
      Size = 35
    end
    object qryListISS_BANK2: TStringField
      FieldName = 'ISS_BANK2'
      Size = 35
    end
    object qryListISS_BANK3: TStringField
      FieldName = 'ISS_BANK3'
      Size = 35
    end
    object qryListISS_BANK4: TStringField
      FieldName = 'ISS_BANK4'
      Size = 35
    end
    object qryListISS_BANK5: TStringField
      FieldName = 'ISS_BANK5'
      Size = 35
    end
    object qryListISS_ACCNT: TStringField
      FieldName = 'ISS_ACCNT'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListAMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListPrno: TIntegerField
      FieldName = 'Prno'
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListMSEQ_1: TIntegerField
      FieldName = 'MSEQ_1'
    end
    object qryListAMD_NO_1: TIntegerField
      FieldName = 'AMD_NO_1'
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListINCD_CUR: TStringField
      FieldName = 'INCD_CUR'
      Size = 3
    end
    object qryListINCD_AMT: TBCDField
      FieldName = 'INCD_AMT'
      Precision = 18
    end
    object qryListDECD_CUR: TStringField
      FieldName = 'DECD_CUR'
      Size = 3
    end
    object qryListDECD_AMT: TBCDField
      FieldName = 'DECD_AMT'
      Precision = 18
    end
    object qryListNWCD_CUR: TStringField
      FieldName = 'NWCD_CUR'
      Size = 3
    end
    object qryListNWCD_AMT: TBCDField
      FieldName = 'NWCD_AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListNARRAT: TBooleanField
      FieldName = 'NARRAT'
    end
    object qryListNARRAT_1: TMemoField
      FieldName = 'NARRAT_1'
      BlobType = ftMemo
    end
    object qryListSR_INFO1: TStringField
      FieldName = 'SR_INFO1'
      Size = 35
    end
    object qryListSR_INFO2: TStringField
      FieldName = 'SR_INFO2'
      Size = 35
    end
    object qryListSR_INFO3: TStringField
      FieldName = 'SR_INFO3'
      Size = 35
    end
    object qryListSR_INFO4: TStringField
      FieldName = 'SR_INFO4'
      Size = 35
    end
    object qryListSR_INFO5: TStringField
      FieldName = 'SR_INFO5'
      Size = 35
    end
    object qryListSR_INFO6: TStringField
      FieldName = 'SR_INFO6'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListOP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Size = 35
    end
    object qryListOP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Size = 35
    end
    object qryListOP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Size = 35
    end
    object qryListOP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Size = 35
    end
    object qryListOP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Size = 35
    end
    object qryListBFCD_AMT: TBCDField
      FieldName = 'BFCD_AMT'
      Precision = 18
    end
    object qryListBFCD_CUR: TStringField
      FieldName = 'BFCD_CUR'
      Size = 3
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListCDMAX_Name: TStringField
      FieldName = 'CDMAX_Name'
      Size = 100
    end
  end
end
