inherited UI_DOMOFC_frm: TUI_DOMOFC_frm
  Left = 677
  Top = 222
  Caption = '('#44060#49444#51088')'#44397#45236#48156#54665#47932#54408#47588#46020#54869#50557#49436'('#49569#49888')'
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 76
    Width = 1122
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter3: TsSplitter [1]
    Left = 0
    Top = 41
    Width = 1122
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object Page_control: TsPageControl [2]
    Left = 0
    Top = 79
    Width = 1122
    Height = 602
    ActivePage = sTabSheet5
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 2
    TabOrder = 3
    TabStop = False
    TabWidth = 120
    OnChange = Page_controlChange
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter7: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page1_Right: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          814
          559)
        object sSpeedButton9: TsSpeedButton
          Left = 325
          Top = -2
          Width = 10
          Height = 565
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel23: TsPanel
          Left = 8
          Top = 8
          Width = 312
          Height = 24
          BevelOuter = bvNone
          Caption = #48156#54665#51088
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_OFRDATE: TsMaskEdit
          Left = 104
          Top = 33
          Width = 74
          Height = 21
          Color = clYellow
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#54665#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_OFRNO: TsEdit
          Left = 104
          Top = 55
          Width = 217
          Height = 21
          Hint = 'APP_NAME1'
          CharCase = ecUpperCase
          Color = 12582911
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#54665#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_SRCODE: TsEdit
          Tag = 101
          Left = 104
          Top = 77
          Width = 106
          Height = 21
          Hint = 'APP_NAME1'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          OnDblClick = edt_SRCODEDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#54665#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object sBitBtn1: TsBitBtn
          Tag = 101
          Left = 211
          Top = 77
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 4
          TabStop = False
          OnClick = sBitBtn1Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_SRNO: TsEdit
          Left = 104
          Top = 99
          Width = 129
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 17
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47924#50669#45824#47532#51216#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_SRNAME1: TsEdit
          Left = 104
          Top = 121
          Width = 217
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_SRNAME2: TsEdit
          Left = 104
          Top = 143
          Width = 217
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_SRNAME3: TsEdit
          Left = 104
          Top = 165
          Width = 97
          Height = 21
          Hint = 'APP_NAME1'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_SRADDR1: TsEdit
          Left = 104
          Top = 187
          Width = 217
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_SRADDR2: TsEdit
          Left = 104
          Top = 209
          Width = 217
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_SRADDR3: TsEdit
          Left = 104
          Top = 231
          Width = 217
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sPanel5: TsPanel
          Left = 9
          Top = 277
          Width = 312
          Height = 24
          BevelOuter = bvNone
          Caption = #49688#50836#51088
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 41
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_UDCODE: TsEdit
          Tag = 102
          Left = 104
          Top = 302
          Width = 74
          Height = 21
          Hint = 'APP_NAME1'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          OnDblClick = edt_SRCODEDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#50836#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object sBitBtn5: TsBitBtn
          Tag = 102
          Left = 179
          Top = 302
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 42
          TabStop = False
          OnClick = sBitBtn1Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_UDNO: TsEdit
          Left = 104
          Top = 324
          Width = 97
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_UDNAME1: TsEdit
          Left = 104
          Top = 346
          Width = 209
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clYellow
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_UDNAME2: TsEdit
          Left = 104
          Top = 368
          Width = 209
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_UDNAME3: TsEdit
          Left = 104
          Top = 390
          Width = 97
          Height = 21
          Hint = 'APP_NAME1'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_UDADDR1: TsEdit
          Left = 104
          Top = 412
          Width = 209
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_UDADDR2: TsEdit
          Left = 104
          Top = 434
          Width = 209
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_UDADDR3: TsEdit
          Left = 104
          Top = 456
          Width = 209
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_UDRENO: TsEdit
          Left = 104
          Top = 478
          Width = 209
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52280#51312#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object sPanel6: TsPanel
          Left = 339
          Top = 8
          Width = 118
          Height = 24
          BevelOuter = bvNone
          Caption = #50976#54952#44592#44036
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 44
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_OFRSQ1: TsEdit
          Left = 339
          Top = 31
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_OFRSQ2: TsEdit
          Left = 339
          Top = 51
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_OFRSQ3: TsEdit
          Left = 339
          Top = 71
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_OFRSQ4: TsEdit
          Left = 339
          Top = 91
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_OFRSQ5: TsEdit
          Left = 339
          Top = 111
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_TSTINST1: TsEdit
          Left = 339
          Top = 164
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_TSTINST2: TsEdit
          Left = 339
          Top = 184
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_TSTINST3: TsEdit
          Left = 339
          Top = 204
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_TSTINST4: TsEdit
          Left = 339
          Top = 224
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_TSTINST5: TsEdit
          Left = 339
          Top = 244
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 31
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sPanel7: TsPanel
          Left = 339
          Top = 140
          Width = 118
          Height = 24
          BevelOuter = bvNone
          Caption = #44160#49324#48169#48277
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 45
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_PCKINST1: TsEdit
          Left = 339
          Top = 296
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PCKINST2: TsEdit
          Left = 339
          Top = 316
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 33
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PCKINST3: TsEdit
          Left = 339
          Top = 336
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PCKINST4: TsEdit
          Left = 339
          Top = 356
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PCKINST5: TsEdit
          Left = 339
          Top = 376
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sPanel8: TsPanel
          Left = 339
          Top = 273
          Width = 118
          Height = 24
          BevelOuter = bvNone
          Caption = #54252#51109#48169#48277
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 46
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_HSCODE: TsMaskEdit
          Left = 104
          Top = 500
          Width = 97
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '0000.00-0000;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 12
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'HS'#48512#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel10: TsPanel
          Left = 339
          Top = 405
          Width = 118
          Height = 24
          BevelOuter = bvNone
          Caption = #44208#51228#48169#48277
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 47
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_PAYETC1: TsEdit
          Left = 339
          Top = 428
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PAYETC2: TsEdit
          Left = 339
          Top = 448
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 38
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PAYETC3: TsEdit
          Left = 339
          Top = 468
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 39
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PAYETC4: TsEdit
          Left = 339
          Top = 488
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PAYETC5: TsEdit
          Left = 339
          Top = 508
          Width = 470
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 43
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
      end
      object page1_Left: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #47928#49436#44277#53685'2'
      object sSplitter8: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page2_Right: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object sLabel3: TsLabel
          Left = 631
          Top = 536
          Width = 4
          Height = 15
        end
        object edt_ORGN1N: TsEdit
          Tag = 301
          Left = 8
          Top = 33
          Width = 34
          Height = 21
          Hint = #44397#44032
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnDblClick = edt_ORGN1NDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN1LOC: TsEdit
          Left = 66
          Top = 33
          Width = 287
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN2N: TsEdit
          Tag = 302
          Left = 8
          Top = 55
          Width = 34
          Height = 21
          Hint = #44397#44032
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          OnDblClick = edt_ORGN1NDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN2LOC: TsEdit
          Left = 66
          Top = 55
          Width = 287
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN3LOC: TsEdit
          Left = 66
          Top = 77
          Width = 287
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN3N: TsEdit
          Tag = 303
          Left = 8
          Top = 77
          Width = 34
          Height = 21
          Hint = #44397#44032
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          OnDblClick = edt_ORGN1NDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN4N: TsEdit
          Tag = 304
          Left = 8
          Top = 99
          Width = 34
          Height = 21
          Hint = #44397#44032
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          OnDblClick = edt_ORGN1NDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN4LOC: TsEdit
          Left = 66
          Top = 99
          Width = 287
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN5LOC: TsEdit
          Left = 66
          Top = 121
          Width = 287
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN5N: TsEdit
          Tag = 305
          Left = 8
          Top = 121
          Width = 34
          Height = 21
          Hint = #44397#44032
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          OnDblClick = edt_ORGN1NDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object memo_REMARK1: TsMemo
          Left = 357
          Top = 33
          Width = 444
          Height = 109
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          MaxLength = 1050
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 25
          OnChange = memo_REMARK1Change
          OnKeyPress = memo_REMARK1KeyPress
          CharCase = ecUpperCase
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_TRNSID: TsEdit
          Tag = 401
          Left = 138
          Top = 143
          Width = 34
          Height = 21
          Hint = #50868#49569#49688#45800
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          OnDblClick = edt_ORGN1NDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sBitBtn14: TsBitBtn
          Tag = 401
          Left = 173
          Top = 143
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 31
          TabStop = False
          OnClick = sBitBtn6Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_TRNSIDNAME: TsEdit
          Left = 196
          Top = 143
          Width = 98
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_LOADD: TsEdit
          Tag = 402
          Left = 138
          Top = 165
          Width = 34
          Height = 21
          Hint = #44397#44032
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          OnDblClick = edt_ORGN1NDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#44397#44032'/'#49440#51201#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn13: TsBitBtn
          Tag = 402
          Left = 173
          Top = 165
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 33
          TabStop = False
          OnClick = sBitBtn6Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_LOADLOC: TsEdit
          Left = 196
          Top = 165
          Width = 205
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_LOADTXT1: TsEdit
          Left = 138
          Top = 187
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#49884#44592
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_LOADTXT2: TsEdit
          Left = 138
          Top = 207
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_LOADTXT3: TsEdit
          Left = 138
          Top = 227
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_LOADTXT4: TsEdit
          Left = 138
          Top = 247
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_LOADTXT5: TsEdit
          Left = 138
          Top = 267
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_DEST: TsEdit
          Tag = 403
          Left = 538
          Top = 165
          Width = 34
          Height = 21
          Hint = #44397#44032
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          OnDblClick = edt_ORGN1NDblClick
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #46020#52265#44397#44032'/'#46020#52265#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sBitBtn16: TsBitBtn
          Tag = 403
          Left = 573
          Top = 165
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 34
          TabStop = False
          OnClick = sBitBtn6Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_DESTLOC: TsEdit
          Left = 596
          Top = 165
          Width = 205
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_DESTTXT1: TsEdit
          Left = 538
          Top = 187
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #46020#52265#49884#44592
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DESTTXT2: TsEdit
          Left = 538
          Top = 207
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_DESTTXT3: TsEdit
          Left = 538
          Top = 227
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_DESTTXT4: TsEdit
          Left = 538
          Top = 247
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_DESTTXT5: TsEdit
          Left = 538
          Top = 267
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sPanel11: TsPanel
          Left = 8
          Top = 8
          Width = 129
          Height = 24
          BevelOuter = bvNone
          Caption = #50896#49328#51648#44397#44032'/'#51648#48169#47749
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sBitBtn6: TsBitBtn
          Tag = 301
          Left = 43
          Top = 33
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 26
          TabStop = False
          OnClick = sBitBtn6Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn9: TsBitBtn
          Tag = 302
          Left = 43
          Top = 55
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 27
          TabStop = False
          OnClick = sBitBtn6Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn10: TsBitBtn
          Tag = 303
          Left = 43
          Top = 77
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 28
          TabStop = False
          OnClick = sBitBtn6Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn11: TsBitBtn
          Tag = 304
          Left = 43
          Top = 99
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 29
          TabStop = False
          OnClick = sBitBtn6Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn12: TsBitBtn
          Tag = 305
          Left = 43
          Top = 121
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 30
          TabStop = False
          OnClick = sBitBtn6Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel13: TsPanel
          Left = 8
          Top = 143
          Width = 129
          Height = 21
          BevelOuter = bvNone
          Caption = #50868#49569#48169#48277
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel14: TsPanel
          Left = 408
          Top = 165
          Width = 129
          Height = 21
          BevelOuter = bvNone
          Caption = #46020#52265#44397#44032'/'#46020#52265#54637
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel18: TsPanel
          Left = 8
          Top = 187
          Width = 129
          Height = 101
          BevelOuter = bvNone
          Caption = #49440#51201#49884#44592
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 38
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel15: TsPanel
          Left = 8
          Top = 165
          Width = 129
          Height = 21
          BevelOuter = bvNone
          Caption = #49440#51201#44397#44032'/'#49440#51201#54637
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 39
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel19: TsPanel
          Left = 408
          Top = 187
          Width = 129
          Height = 101
          BevelOuter = bvNone
          Caption = #46020#52265#49884#44592
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel12: TsPanel
          Left = 357
          Top = 8
          Width = 129
          Height = 24
          BevelOuter = bvNone
          Caption = #44592#53440#52280#51312#49324#54637
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 41
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel20: TsPanel
          Left = 1
          Top = 289
          Width = 812
          Height = 269
          Align = alBottom
          TabOrder = 42
          SkinData.SkinSection = 'TRANSPARENT'
        end
      end
      object page2_Left: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
      end
    end
    object sTabSheet5: TsTabSheet
      Caption = #49345#54408#45236#50669
      object sSplitter4: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel3: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object detailDBGrid: TsDBGrid
          Left = 1
          Top = 26
          Width = 812
          Height = 176
          Align = alTop
          Color = clWhite
          Ctl3D = False
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          PopupMenu = detailMenu
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clBlack
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME_COD'
              Title.Caption = #54408#47749#53076#46300
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'HS_NO'
              Title.Caption = 'HS'#48264#54840
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY_G'
              Title.Caption = #49688#47049#45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Caption = #49688#47049
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTYG_G'
              Title.Caption = #45800#44032#44592#51456#49688#47049#45800#50948
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTYG'
              Title.Caption = #45800#44032#44592#51456#49688#47049
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRICE_G'
              Title.Caption = #45800#44032#45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRICE'
              Title.Caption = #45800#44032
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT_G'
              Title.Caption = #44552#50529#45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT'
              Title.Caption = #44552#50529
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STQTY_G'
              Title.Caption = #49548#47049#49548#44228#45800#50948
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STQTY'
              Title.Caption = #49548#47049#49548#44228
              Width = 137
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STAMT_G'
              Title.Caption = #44552#50529#49548#44228#45800#50948
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STAMT'
              Title.Caption = #44552#50529#49548#44228
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME1'
              Title.Caption = #54408#47749
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIZE1'
              Title.Caption = #44508#44201
              Width = 100
              Visible = True
            end>
        end
        object sPanel9: TsPanel
          Left = 1
          Top = 1
          Width = 812
          Height = 25
          Align = alTop
          TabOrder = 1
          SkinData.SkinSection = 'TRANSPARENT'
          object sSpeedButton10: TsSpeedButton
            Left = 1
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailAdd: TsSpeedButton
            Tag = 1
            Left = 6
            Top = 1
            Width = 67
            Height = 23
            Caption = #51077#47141
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailAddClick
            OnMouseMove = Btn_DetailAddMouseMove
            Align = alLeft
            OnMouseLeave = Btn_DetailAddMouseLeave
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 2
          end
          object sSpeedButton13: TsSpeedButton
            Left = 73
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailMod: TsSpeedButton
            Tag = 2
            Left = 78
            Top = 1
            Width = 67
            Height = 23
            Caption = #49688#51221
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailAddClick
            OnMouseMove = Btn_DetailAddMouseMove
            Align = alLeft
            OnMouseLeave = Btn_DetailAddMouseLeave
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 3
          end
          object sSpeedButton11: TsSpeedButton
            Left = 145
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailDel: TsSpeedButton
            Tag = 3
            Left = 150
            Top = 1
            Width = 67
            Height = 23
            Caption = #49325#51228
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailAddClick
            OnMouseMove = Btn_DetailAddMouseMove
            Align = alLeft
            OnMouseLeave = Btn_DetailAddMouseLeave
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 1
          end
          object sSpeedButton15: TsSpeedButton
            Left = 217
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailOk: TsSpeedButton
            Tag = 4
            Left = 222
            Top = 1
            Width = 67
            Height = 23
            Caption = #51200#51109
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailOkClick
            OnMouseMove = Btn_DetailAddMouseMove
            Align = alLeft
            OnMouseLeave = Btn_DetailAddMouseLeave
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 17
          end
          object sSpeedButton12: TsSpeedButton
            Left = 356
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailCancel: TsSpeedButton
            Tag = 5
            Left = 289
            Top = 1
            Width = 67
            Height = 23
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = Btn_DetailOkClick
            OnMouseMove = Btn_DetailAddMouseMove
            Align = alLeft
            OnMouseLeave = Btn_DetailAddMouseLeave
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 18
          end
          object GoodsExcelBtn: TsSpeedButton
            Left = 617
            Top = 1
            Width = 110
            Height = 23
            Caption = #50641#49472#44032#51256#50724#44592
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = GoodsExcelBtnClick
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 10
          end
          object GoodsSampleBtn: TsSpeedButton
            Left = 727
            Top = 1
            Width = 84
            Height = 23
            Caption = #50641#49472#49368#54540
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = GoodsSampleBtnClick
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 30
          end
        end
        object sPanel16: TsPanel
          Left = 1
          Top = 202
          Width = 812
          Height = 247
          Align = alTop
          TabOrder = 2
          SkinData.SkinSection = 'PANEL'
          DesignSize = (
            812
            247)
          object sSpeedButton16: TsSpeedButton
            Left = 404
            Top = -59
            Width = 10
            Height = 314
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'SPEEDBUTTON'
          end
          object sDBEdit1: TsDBEdit
            Tag = 501
            Left = 45
            Top = 33
            Width = 74
            Height = 23
            Hint = #54408#47749
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'NAME_COD'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            OnDblClick = sDBEdit1DblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sBitBtn15: TsBitBtn
            Tag = 501
            Left = 120
            Top = 34
            Width = 22
            Height = 22
            Cursor = crHandPoint
            TabOrder = 16
            TabStop = False
            OnClick = sBitBtn15Click
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit2: TsDBEdit
            Left = 205
            Top = 33
            Width = 92
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'HS_NO'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'HS NO'
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sDBMemo1: TsDBMemo
            Left = 45
            Top = 57
            Width = 252
            Height = 86
            Color = clWhite
            Ctl3D = True
            DataField = 'NAME1'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54408#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeftTop
            CharCase = ecUpperCase
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBMemo2: TsDBMemo
            Left = 45
            Top = 144
            Width = 348
            Height = 86
            Color = clWhite
            Ctl3D = True
            DataField = 'SIZE1'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44508#44201
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeftTop
            CharCase = ecUpperCase
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit3: TsDBEdit
            Tag = 502
            Left = 541
            Top = 35
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'QTY_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            OnDblClick = sDBEdit3DblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn17: TsBitBtn
            Tag = 502
            Left = 578
            Top = 35
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 17
            TabStop = False
            OnClick = sBitBtn6Click
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit4: TsDBEdit
            Left = 601
            Top = 35
            Width = 152
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'QTY'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit6: TsDBEdit
            Left = 601
            Top = 59
            Width = 152
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'PRICE'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            OnExit = sDBEdit6Exit
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sBitBtn18: TsBitBtn
            Tag = 503
            Left = 578
            Top = 59
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 18
            TabStop = False
            OnClick = sBitBtn6Click
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit5: TsDBEdit
            Tag = 503
            Left = 541
            Top = 59
            Width = 36
            Height = 23
            Hint = #53685#54868
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'PRICE_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            OnDblClick = sDBEdit3DblClick
            OnExit = sDBEdit5Exit
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sDBEdit7: TsDBEdit
            Tag = 504
            Left = 541
            Top = 83
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'QTYG_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            OnDblClick = sDBEdit3DblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032#44592#51456#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sBitBtn19: TsBitBtn
            Tag = 504
            Left = 578
            Top = 83
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 19
            TabStop = False
            OnClick = sBitBtn6Click
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit8: TsDBEdit
            Left = 601
            Top = 83
            Width = 152
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'QTYG'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            OnExit = sDBEdit6Exit
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit10: TsDBEdit
            Left = 601
            Top = 107
            Width = 152
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 11
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sBitBtn20: TsBitBtn
            Tag = 505
            Left = 578
            Top = 107
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 20
            TabStop = False
            OnClick = sBitBtn6Click
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit9: TsDBEdit
            Tag = 505
            Left = 541
            Top = 107
            Width = 36
            Height = 23
            Hint = #53685#54868
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            OnDblClick = sDBEdit3DblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sDBEdit11: TsDBEdit
            Tag = 506
            Left = 541
            Top = 154
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'STQTY_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 12
            OnDblClick = sDBEdit3DblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49548#47049#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn21: TsBitBtn
            Tag = 506
            Left = 578
            Top = 154
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 21
            TabStop = False
            OnClick = sBitBtn6Click
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit12: TsDBEdit
            Left = 601
            Top = 154
            Width = 152
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'STQTY'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 13
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit14: TsDBEdit
            Left = 601
            Top = 178
            Width = 152
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'STAMT'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 15
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sBitBtn23: TsBitBtn
            Tag = 507
            Left = 578
            Top = 178
            Width = 22
            Height = 23
            Cursor = crHandPoint
            TabOrder = 22
            TabStop = False
            OnClick = sBitBtn6Click
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sDBEdit13: TsDBEdit
            Tag = 507
            Left = 541
            Top = 178
            Width = 36
            Height = 23
            Hint = #53685#54868
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'STAMT_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 14
            OnDblClick = sDBEdit3DblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn26: TsButton
            Left = 541
            Top = 131
            Width = 212
            Height = 21
            Caption = #44228#49328
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 23
            OnClick = sBitBtn261Click
            SkinData.SkinSection = 'BUTTON'
            Reflected = True
            Images = DMICON.System16
            ImageIndex = 7
          end
          object sPanel22: TsPanel
            Left = 4
            Top = 3
            Width = 400
            Height = 24
            BevelOuter = bvNone
            Caption = #49345#54408' '#54408#47749
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sPanel24: TsPanel
            Left = 414
            Top = 3
            Width = 394
            Height = 24
            BevelOuter = bvNone
            Caption = #49688#47049'/'#45800#44032'/'#44552#50529
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 25
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
        end
        object sPanel17: TsPanel
          Left = 1
          Top = 449
          Width = 812
          Height = 109
          Align = alClient
          TabOrder = 3
          SkinData.SkinSection = 'PANEL'
          object sLabel4: TsLabel
            Left = 441
            Top = 83
            Width = 144
            Height = 15
            Caption = #49345#54408#45236#50669' '#51077#47141' '#54980' '#44228#49328#44032#45733
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
          end
          object edt_TQTYCUR: TsEdit
            Tag = 508
            Left = 261
            Top = 40
            Width = 36
            Height = 21
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clYellow
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            OnDblClick = edt_ORGN1NDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn24: TsBitBtn
            Tag = 508
            Left = 298
            Top = 62
            Width = 22
            Height = 21
            Cursor = crHandPoint
            TabOrder = 5
            TabStop = False
            OnClick = sBitBtn6Click
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object edt_TQTY: TsCurrencyEdit
            Left = 321
            Top = 40
            Width = 192
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Grayed = False
            GlyphMode.Blend = 0
            DisplayFormat = '###,###,###.0000;-###,###,###.0000;0'
          end
          object edt_TAMT: TsCurrencyEdit
            Left = 321
            Top = 62
            Width = 192
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Grayed = False
            GlyphMode.Blend = 0
            DisplayFormat = '###,###,###.0000;-###,###,###.0000;0'
          end
          object edt_TAMTCUR: TsEdit
            Tag = 509
            Left = 261
            Top = 62
            Width = 36
            Height = 21
            Hint = #53685#54868
            CharCase = ecUpperCase
            Color = clYellow
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            OnDblClick = edt_ORGN1NDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#54633#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sBitBtn25: TsBitBtn
            Tag = 509
            Left = 298
            Top = 40
            Width = 22
            Height = 21
            Cursor = crHandPoint
            TabOrder = 6
            TabStop = False
            OnClick = sBitBtn6Click
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sBitBtn27: TsButton
            Left = 514
            Top = 40
            Width = 71
            Height = 44
            Caption = #44228#49328
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            OnClick = sBitBtn27Click
            SkinData.SkinSection = 'BUTTON'
            Reflected = True
            Images = DMICON.System16
            ImageIndex = 7
          end
          object sPanel32: TsPanel
            Left = 1
            Top = 1
            Width = 810
            Height = 24
            Align = alTop
            BevelOuter = bvNone
            Caption = #52509' '#54633#44228
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 7
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
        end
      end
      object sPanel4: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
      end
    end
    object sTabSheet6: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      PopupMenu = popMenu1
      object sSplitter2: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 35
        Width = 1114
        Height = 527
        TabStop = False
        Align = alClient
        Color = clWhite
        Ctl3D = False
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        PopupMenu = popMenu1
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid2DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Chk2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'Chk3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 74
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 200
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'OFR_DATE'
            Title.Alignment = taCenter
            Title.Caption = #48156#54665#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OFR_NO'
            Title.Alignment = taCenter
            Title.Caption = #48156#54665#48264#54840
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SR_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #48156#54665#51088
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UD_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #49688#50836#51088
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TQTY'
            Title.Alignment = taCenter
            Title.Caption = #52509#49688#47049
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TQTYCUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAMT'
            Title.Alignment = taCenter
            Title.Caption = #52509#44552#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TAMTCUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 62
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51088
            Width = 62
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'OFR_SQ1'
            Title.Alignment = taCenter
            Title.Caption = #50976#54952#44592#51068
            Width = 82
            Visible = True
          end>
      end
      object sPanel1: TsPanel
        Left = 0
        Top = 3
        Width = 1114
        Height = 32
        Align = alTop
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object edt_SearchText: TsEdit
          Left = 86
          Top = 5
          Width = 234
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 81
          Height = 23
          Alignment = taLeftJustify
          SkinData.SkinSection = 'COMBOBOX'
          VerticalAlignment = taAlignTop
          Style = csDropDownList
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ItemHeight = 17
          ItemIndex = 0
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 86
          Top = 5
          Width = 82
          Height = 21
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn2: TsBitBtn
          Left = 321
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 4
          OnClick = sBitBtn2Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn3: TsBitBtn
          Tag = 902
          Left = 167
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 214
          Top = 5
          Width = 82
          Height = 21
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn4: TsBitBtn
          Tag = 903
          Left = 297
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel2: TsPanel
          Left = 190
          Top = 5
          Width = 25
          Height = 23
          Caption = '~'
          TabOrder = 7
          SkinData.SkinSection = 'PANEL'
        end
      end
    end
  end
  object btn_Panel: TsPanel [3]
    Left = 0
    Top = 0
    Width = 1122
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton2: TsSpeedButton
      Left = 447
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 645
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton4: TsSpeedButton
      Left = 731
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 901
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton1: TsSpeedButton
      Left = 1
      Top = 1
      Width = 4
      Height = 39
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 5
      Width = 148
      Height = 17
      Caption = #44397#45236#48156#54665#47932#54408' '#47588#46020#54869#50557#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 20
      Width = 50
      Height = 13
      Caption = 'DOMOFC'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton6: TsSpeedButton
      Left = 370
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton7: TsSpeedButton
      Left = 654
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton8: TsSpeedButton
      Left = 160
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1040
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 171
      Top = 2
      Width = 65
      Height = 39
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Left = 236
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      OnClick = btnEditClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Left = 301
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 380
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      TabStop = False
      OnClick = btnCopyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 664
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 457
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      TabStop = False
      OnClick = btnSaveClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 522
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      TabStop = False
      OnClick = btnSaveClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Left = 587
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      TabStop = False
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
    object sButton1: TsButton
      Left = 741
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      TabStop = False
      OnClick = sButton1Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
    end
    object sButton3: TsButton
      Left = 834
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      TabStop = False
      OnClick = sButton3Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
  end
  object maintno_Panel: TsPanel [4]
    Left = 0
    Top = 44
    Width = 1122
    Height = 32
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    object edt_MaintNo: TsEdit
      Left = 64
      Top = 5
      Width = 121
      Height = 23
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sMaskEdit1: TsMaskEdit
      Left = 256
      Top = 5
      Width = 81
      Height = 25
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 2
      Text = '20161122'
      OnDblClick = sMaskEdit1DblClick
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_UserNo: TsEdit
      Left = 417
      Top = 5
      Width = 57
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 338
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 1
      TabStop = False
      OnClick = btn_CalClick
      ImageIndex = 9
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sCheckBox1: TsCheckBox
      Left = 944
      Top = 6
      Width = 64
      Height = 16
      TabStop = False
      Caption = #46356#48260#44536
      TabOrder = 6
      ImgChecked = 0
      ImgUnchecked = 0
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 545
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 3
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      OnDblClick = edt_ORGN1NDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn7: TsBitBtn
      Tag = 1
      Left = 578
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 7
      TabStop = False
      OnClick = sBitBtn6Click
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 633
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 3
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
      OnDblClick = edt_ORGN1NDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn8: TsBitBtn
      Tag = 2
      Left = 666
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 8
      TabStop = False
      OnClick = sBitBtn6Click
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object LeftGrid_Panel: TsPanel [5]
    Left = 4
    Top = 119
    Width = 300
    Height = 564
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object sPanel53: TsPanel
      Left = 1
      Top = 0
      Width = 298
      Height = 33
      BevelOuter = bvNone
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      object Mask_fromDate: TsMaskEdit
        Left = 121
        Top = 5
        Width = 74
        Height = 23
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn22: TsBitBtn
        Left = 269
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = sBitBtn22Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object Mask_toDate: TsMaskEdit
        Left = 196
        Top = 5
        Width = 74
        Height = 23
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51312#54924
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        SkinData.SkinSection = 'EDIT'
      end
    end
    object sDBGrid2: TsDBGrid
      Left = -7
      Top = 34
      Width = 304
      Height = 529
      TabStop = False
      Color = clWhite
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      PopupMenu = popMenu1
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid2DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Chk2'
          Title.Alignment = taCenter
          Title.Caption = #49345#54889
          Width = 41
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 164
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 192
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 192
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      ''
      'SELECT *  ,'
      '      TRNS_NAME.DOC_NAME as TRANSNME,'
      '      LOADD_NAME.DOC_NAME as LOADDNAME,'
      '      DEST_NAME.DOC_NAME as DESTNAME'
      #9#9'  '#9#9#9#9'   '
      ' FROM DOMOFC_H1 AS H1'
      ''
      'INNER JOIN DOMOFC_H2 AS H2 ON H1.MAINT_NO = H2.MAINT_NO '
      'INNER JOIN DOMOFC_H3 AS H3 ON H1.MAINT_NO = H3.MAINT_NO'
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#50868#49569#49688#45800#39') TRNS_NAME ON H3.TRNS_ID = TRNS_NAME.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') LOADD_NAME ON H3.LOADD = LOADD_NAME.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') DEST_NAME ON H3.DEST = DEST_NAME.CODE')
    Left = 48
    Top = 192
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListChk1: TStringField
      FieldName = 'Chk1'
      Size = 1
    end
    object qryListChk2: TStringField
      FieldName = 'Chk2'
      OnGetText = qryListChk2GetText
      Size = 1
    end
    object qryListChk3: TStringField
      FieldName = 'Chk3'
      OnGetText = qryListChk3GetText
      Size = 10
    end
    object qryListMaint_Rff: TStringField
      FieldName = 'Maint_Rff'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListD_CHK: TStringField
      FieldName = 'D_CHK'
      Size = 6
    end
    object qryListSR_CODE: TStringField
      FieldName = 'SR_CODE'
      Size = 10
    end
    object qryListSR_NO: TStringField
      FieldName = 'SR_NO'
      Size = 10
    end
    object qryListSR_NAME1: TStringField
      FieldName = 'SR_NAME1'
      Size = 35
    end
    object qryListSR_NAME2: TStringField
      FieldName = 'SR_NAME2'
      Size = 35
    end
    object qryListSR_NAME3: TStringField
      FieldName = 'SR_NAME3'
      Size = 10
    end
    object qryListSR_ADDR1: TStringField
      FieldName = 'SR_ADDR1'
      Size = 35
    end
    object qryListSR_ADDR2: TStringField
      FieldName = 'SR_ADDR2'
      Size = 35
    end
    object qryListSR_ADDR3: TStringField
      FieldName = 'SR_ADDR3'
      Size = 35
    end
    object qryListUD_CODE: TStringField
      FieldName = 'UD_CODE'
      Size = 10
    end
    object qryListUD_NO: TStringField
      FieldName = 'UD_NO'
      Size = 10
    end
    object qryListUD_NAME1: TStringField
      FieldName = 'UD_NAME1'
      Size = 35
    end
    object qryListUD_NAME2: TStringField
      FieldName = 'UD_NAME2'
      Size = 35
    end
    object qryListUD_NAME3: TStringField
      FieldName = 'UD_NAME3'
      Size = 10
    end
    object qryListUD_ADDR1: TStringField
      FieldName = 'UD_ADDR1'
      Size = 35
    end
    object qryListUD_ADDR2: TStringField
      FieldName = 'UD_ADDR2'
      Size = 35
    end
    object qryListUD_ADDR3: TStringField
      FieldName = 'UD_ADDR3'
      Size = 35
    end
    object qryListUD_RENO: TStringField
      FieldName = 'UD_RENO'
      Size = 35
    end
    object qryListOFR_NO: TStringField
      FieldName = 'OFR_NO'
      Size = 35
    end
    object qryListOFR_DATE: TStringField
      FieldName = 'OFR_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListOFR_SQ: TStringField
      FieldName = 'OFR_SQ'
      Size = 1
    end
    object qryListOFR_SQ1: TStringField
      FieldName = 'OFR_SQ1'
      Size = 70
    end
    object qryListOFR_SQ2: TStringField
      FieldName = 'OFR_SQ2'
      Size = 70
    end
    object qryListOFR_SQ3: TStringField
      FieldName = 'OFR_SQ3'
      Size = 70
    end
    object qryListOFR_SQ4: TStringField
      FieldName = 'OFR_SQ4'
      Size = 70
    end
    object qryListOFR_SQ5: TStringField
      FieldName = 'OFR_SQ5'
      Size = 70
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK_1: TMemoField
      FieldName = 'REMARK_1'
      BlobType = ftMemo
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListHS_CODE: TStringField
      FieldName = 'HS_CODE'
      Size = 10
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListTSTINST: TStringField
      FieldName = 'TSTINST'
      Size = 1
    end
    object qryListTSTINST1: TStringField
      FieldName = 'TSTINST1'
      Size = 70
    end
    object qryListTSTINST2: TStringField
      FieldName = 'TSTINST2'
      Size = 70
    end
    object qryListTSTINST3: TStringField
      FieldName = 'TSTINST3'
      Size = 70
    end
    object qryListTSTINST4: TStringField
      FieldName = 'TSTINST4'
      Size = 70
    end
    object qryListTSTINST5: TStringField
      FieldName = 'TSTINST5'
      Size = 70
    end
    object qryListPCKINST: TStringField
      FieldName = 'PCKINST'
      Size = 1
    end
    object qryListPCKINST1: TStringField
      FieldName = 'PCKINST1'
      Size = 70
    end
    object qryListPCKINST2: TStringField
      FieldName = 'PCKINST2'
      Size = 70
    end
    object qryListPCKINST3: TStringField
      FieldName = 'PCKINST3'
      Size = 70
    end
    object qryListPCKINST4: TStringField
      FieldName = 'PCKINST4'
      Size = 70
    end
    object qryListPCKINST5: TStringField
      FieldName = 'PCKINST5'
      Size = 70
    end
    object qryListPAY: TStringField
      FieldName = 'PAY'
      Size = 3
    end
    object qryListPAY_ETC: TStringField
      FieldName = 'PAY_ETC'
      Size = 1
    end
    object qryListPAY_ETC1: TStringField
      FieldName = 'PAY_ETC1'
      Size = 70
    end
    object qryListPAY_ETC2: TStringField
      FieldName = 'PAY_ETC2'
      Size = 70
    end
    object qryListPAY_ETC3: TStringField
      FieldName = 'PAY_ETC3'
      Size = 70
    end
    object qryListPAY_ETC4: TStringField
      FieldName = 'PAY_ETC4'
      Size = 70
    end
    object qryListPAY_ETC5: TStringField
      FieldName = 'PAY_ETC5'
      Size = 70
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListTERM_DEL: TStringField
      FieldName = 'TERM_DEL'
      Size = 3
    end
    object qryListTERM_NAT: TStringField
      FieldName = 'TERM_NAT'
      Size = 3
    end
    object qryListTERM_LOC: TStringField
      FieldName = 'TERM_LOC'
      Size = 70
    end
    object qryListORGN1N: TStringField
      FieldName = 'ORGN1N'
      Size = 3
    end
    object qryListORGN1LOC: TStringField
      FieldName = 'ORGN1LOC'
      Size = 70
    end
    object qryListORGN2N: TStringField
      FieldName = 'ORGN2N'
      Size = 3
    end
    object qryListORGN2LOC: TStringField
      FieldName = 'ORGN2LOC'
      Size = 70
    end
    object qryListORGN3N: TStringField
      FieldName = 'ORGN3N'
      Size = 3
    end
    object qryListORGN3LOC: TStringField
      FieldName = 'ORGN3LOC'
      Size = 70
    end
    object qryListORGN4N: TStringField
      FieldName = 'ORGN4N'
      Size = 3
    end
    object qryListORGN4LOC: TStringField
      FieldName = 'ORGN4LOC'
      Size = 70
    end
    object qryListORGN5N: TStringField
      FieldName = 'ORGN5N'
      Size = 3
    end
    object qryListORGN5LOC: TStringField
      FieldName = 'ORGN5LOC'
      Size = 70
    end
    object qryListTRNS_ID: TStringField
      FieldName = 'TRNS_ID'
      Size = 3
    end
    object qryListLOADD: TStringField
      FieldName = 'LOADD'
      Size = 3
    end
    object qryListLOADLOC: TStringField
      FieldName = 'LOADLOC'
      Size = 70
    end
    object qryListLOADTXT: TStringField
      FieldName = 'LOADTXT'
      Size = 1
    end
    object qryListLOADTXT1: TStringField
      FieldName = 'LOADTXT1'
      Size = 70
    end
    object qryListLOADTXT2: TStringField
      FieldName = 'LOADTXT2'
      Size = 70
    end
    object qryListLOADTXT3: TStringField
      FieldName = 'LOADTXT3'
      Size = 70
    end
    object qryListLOADTXT4: TStringField
      FieldName = 'LOADTXT4'
      Size = 70
    end
    object qryListLOADTXT5: TStringField
      FieldName = 'LOADTXT5'
      Size = 70
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListDESTLOC: TStringField
      FieldName = 'DESTLOC'
      Size = 70
    end
    object qryListDESTTXT: TStringField
      FieldName = 'DESTTXT'
      Size = 1
    end
    object qryListDESTTXT1: TStringField
      FieldName = 'DESTTXT1'
      Size = 70
    end
    object qryListDESTTXT2: TStringField
      FieldName = 'DESTTXT2'
      Size = 70
    end
    object qryListDESTTXT3: TStringField
      FieldName = 'DESTTXT3'
      Size = 70
    end
    object qryListDESTTXT4: TStringField
      FieldName = 'DESTTXT4'
      Size = 70
    end
    object qryListDESTTXT5: TStringField
      FieldName = 'DESTTXT5'
      Size = 70
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListTQTYCUR: TStringField
      FieldName = 'TQTYCUR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListTAMTCUR: TStringField
      FieldName = 'TAMTCUR'
      Size = 3
    end
    object qryListCODE: TStringField
      FieldName = 'CODE'
      Size = 12
    end
    object qryListDOC_NAME: TStringField
      FieldName = 'DOC_NAME'
      Size = 100
    end
    object qryListCODE_1: TStringField
      FieldName = 'CODE_1'
      Size = 12
    end
    object qryListDOC_NAME_1: TStringField
      FieldName = 'DOC_NAME_1'
      Size = 100
    end
    object qryListCODE_2: TStringField
      FieldName = 'CODE_2'
      Size = 12
    end
    object qryListDOC_NAME_2: TStringField
      FieldName = 'DOC_NAME_2'
      Size = 100
    end
    object qryListTRANSNME: TStringField
      FieldName = 'TRANSNME'
      Size = 100
    end
    object qryListLOADDNAME: TStringField
      FieldName = 'LOADDNAME'
      Size = 100
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM DOMOFC_D')
    Left = 48
    Top = 224
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailHS_CHK: TStringField
      FieldName = 'HS_CHK'
      Size = 1
    end
    object qryDetailHS_NO: TStringField
      FieldName = 'HS_NO'
      EditMask = '9999.99-9999;0'
      Size = 12
    end
    object qryDetailNAME_CHK: TStringField
      FieldName = 'NAME_CHK'
      Size = 1
    end
    object qryDetailNAME_COD: TStringField
      FieldName = 'NAME_COD'
      Size = 35
    end
    object qryDetailNAME: TStringField
      FieldName = 'NAME'
    end
    object qryDetailNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryDetailSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryDetailSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryDetailQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryDetailQTYG: TBCDField
      FieldName = 'QTYG'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryDetailPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryDetailAMT: TBCDField
      FieldName = 'AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailAMT_G: TStringField
      FieldName = 'AMT_G'
      Size = 3
    end
    object qryDetailSTQTY: TBCDField
      FieldName = 'STQTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryDetailSTAMT: TBCDField
      FieldName = 'STAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailSTAMT_G: TStringField
      FieldName = 'STAMT_G'
      Size = 3
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 80
    Top = 224
  end
  object spCopyDOMOFC: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'CopyDOMOFC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CopyDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@NewDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 48
    Top = 288
  end
  object detailMenu: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = detailMenuPopup
    Left = 48
    Top = 320
    object N1: TMenuItem
      Caption = #45936#51060#53552' '#52628#44032
      Enabled = False
      ImageIndex = 26
      OnClick = N1Click
    end
    object N2: TMenuItem
      Tag = 1
      Caption = #45936#51060#53552' '#49688#51221
      Enabled = False
      ImageIndex = 3
      OnClick = N1Click
    end
    object N3: TMenuItem
      Tag = 2
      Caption = #45936#51060#53552' '#49325#51228
      Enabled = False
      ImageIndex = 27
      OnClick = N1Click
    end
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE DOMOFC_H1'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 48
    Top = 256
  end
  object popMenu1: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    Left = 80
    Top = 320
    object MenuItem1: TMenuItem
      Caption = #51204#49569#51456#48708
      ImageIndex = 29
      OnClick = sButton1Click
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object MenuItem3: TMenuItem
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = btnEditClick
    end
    object N4: TMenuItem
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = btnDelClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object d1: TMenuItem
      Caption = #52636#47141#54616#44592
      object N8: TMenuItem
        Caption = #48120#47532#48372#44592
        ImageIndex = 15
        OnClick = btnPrintClick
      end
      object N9: TMenuItem
        Tag = 1
        Caption = #54532#47536#53944
        ImageIndex = 21
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = 'ADMIN'
      Enabled = False
      ImageIndex = 16
    end
  end
  object excelOpen: TsOpenDialog
    Filter = 
      #47784#46304' Excel '#54028#51068'(*.xls;*.xlsx)|*.xls;*.xlsx|Excel '#53685#54633#47928#49436'(*.xlsx)|*.xlsx' +
      '|Excel 97 - 2003 '#53685#54633#47928#49436'(*.xls)|*.xls|All Files|*.*'
    Left = 48
    Top = 352
  end
end
