inherited UI_VATBI1_frm: TUI_VATBI1_frm
  Left = 326
  Top = 190
  Caption = #49464#44552#44228#49328#49436'('#49569#49888')'
  PixelsPerInch = 96
  TextHeight = 12
  inherited sSplitter1: TsSplitter
    Top = 41
  end
  inherited sSplitter6: TsSplitter
    Top = 77
  end
  inherited sPanel1: TsPanel
    inherited sSpeedButton2: TsSpeedButton
      Left = 326
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 618
      Top = 3
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 697
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 405
    end
    object sLabel59: TsLabel [4]
      Left = 8
      Top = 5
      Width = 101
      Height = 17
      Caption = #49464#44552#44228#49328#49436'('#49569#49888')'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel [5]
      Left = 9
      Top = 20
      Width = 37
      Height = 13
      Caption = 'VATBI1'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton1: TsSpeedButton [6]
      Left = 113
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton6: TsSpeedButton [7]
      Left = 876
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    inherited btnExit: TsButton
      Images = DMICON.System18
      ImageIndex = 19
    end
    inherited btnNew: TsButton
      Left = 123
      OnClick = btnNewClick
      Images = DMICON.System18
      ImageIndex = 25
    end
    inherited btnEdit: TsButton
      Left = 190
      OnClick = btnEditClick
      Images = DMICON.System18
    end
    inherited btnDel: TsButton
      Left = 257
      OnClick = btnDelClick
      Images = DMICON.System18
      ImageIndex = 26
    end
    inherited btnCopy: TsButton
      Left = 336
      OnClick = btnCopyClick
      Images = DMICON.System18
      ImageIndex = 27
    end
    inherited btnPrint: TsButton
      Left = 628
      OnClick = btnPrintClick
      Images = DMICON.System18
      ImageIndex = 20
    end
    inherited btnTemp: TsButton
      Left = 415
      OnClick = btnSaveClick
      Images = DMICON.System18
      ImageIndex = 11
    end
    inherited btnSave: TsButton
      Left = 482
      OnClick = btnSaveClick
      Images = DMICON.System18
      ImageIndex = 0
    end
    inherited btnCancel: TsButton
      Left = 549
      OnClick = btnCancelClick
      Images = DMICON.System18
      ImageIndex = 17
    end
    inherited btnReady: TsButton
      Left = 707
      OnClick = btnReadyClick
      Images = DMICON.System18
      ImageIndex = 28
    end
    inherited btnSend: TsButton
      Left = 800
      OnClick = btnSendClick
      Images = DMICON.System18
      ImageIndex = 21
    end
  end
  inherited sPanel4: TsPanel
    inherited edt_msg1: TsEdit
      MaxLength = 3
    end
    inherited edt_msg2: TsEdit
      MaxLength = 3
    end
    inherited edt_MAINT_NO: TsEdit
      MaxLength = 35
    end
  end
  inherited Page_control: TsPageControl
    inherited sTabSheet1: TsTabSheet
      inherited page1_RIGHT: TsPanel
        inherited taxPanel: TsPanel
          inherited edt_VATCODE: TsEdit
            MaxLength = 4
          end
          inherited edt_VATTYPE: TsEdit
            MaxLength = 4
          end
          inherited edt_NEWINDICATOR: TsEdit
            MaxLength = 4
          end
          inherited edt_RENO: TsEdit
            MaxLength = 35
          end
          inherited edt_SENO: TsEdit
            MaxLength = 35
          end
          inherited edt_FSNO: TsEdit
            MaxLength = 35
          end
          inherited edt_ACENO: TsEdit
            MaxLength = 35
          end
          inherited edt_RFFNO: TsEdit
            MaxLength = 35
          end
        end
        inherited providerPanel: TsPanel
          inherited edt_SENAME3: TsEdit
            MaxLength = 35
          end
          inherited edt_SENAME2: TsEdit
            MaxLength = 35
          end
          inherited edt_SENAME1: TsEdit
            MaxLength = 35
          end
          inherited edt_SEADDR1: TsEdit
            MaxLength = 35
          end
          inherited edt_SECODE: TsEdit
            MaxLength = 10
          end
          inherited edt_SEADDR2: TsEdit
            MaxLength = 35
          end
          inherited edt_SEADDR3: TsEdit
            MaxLength = 35
          end
          inherited edt_SEADDR4: TsEdit
            MaxLength = 35
          end
          inherited edt_SEADDR5: TsEdit
            MaxLength = 35
          end
          inherited edt_SESAUP1: TsEdit
            Left = 476
            MaxLength = 35
          end
          inherited edt_SEFTX1: TsEdit
            Left = 476
            MaxLength = 40
          end
          inherited edt_SEFTX2: TsEdit
            Left = 476
            MaxLength = 30
          end
          inherited edt_SEFTX3: TsEdit
            Left = 476
            MaxLength = 20
          end
          inherited edt_SEFTX4: TsEdit
            Left = 476
            CharCase = ecNormal
            MaxLength = 20
          end
          inherited edt_SEFTX5: TsEdit
            Left = 612
            CharCase = ecNormal
            MaxLength = 20
          end
          inherited memo_SEUPTA1: TsMemo
            Left = 476
          end
          inherited memo_SEITEM1: TsMemo
            Left = 476
          end
        end
      end
    end
    inherited sTabSheet4: TsTabSheet
      inherited sPanel11: TsPanel
        inherited buyerPanel: TsPanel
          inherited edt_BYSAUP1: TsEdit
            Left = 476
          end
          inherited edt_BYFTX1: TsEdit
            Left = 476
          end
          inherited edt_BYFTX2: TsEdit
            Left = 476
          end
          inherited edt_BYFTX3: TsEdit
            Left = 476
          end
          inherited edt_BYFTX4: TsEdit
            Left = 476
            CharCase = ecNormal
          end
          inherited edt_BYFTX5: TsEdit
            Left = 612
            CharCase = ecNormal
          end
          inherited memo_BYUPTA1: TsMemo
            Left = 476
          end
          inherited memo_BYITEM1: TsMemo
            Left = 476
          end
          inherited sBitBtn7: TsBitBtn
            Left = 476
            OnClick = sBitBtn7Click
          end
          object byAddPanel: TsPanel
            Left = -336
            Top = 248
            Width = 361
            Height = 209
            
            TabOrder = 23
            Visible = False
            object sPanel6: TsPanel
              Left = 1
              Top = 1
              Width = 359
              Height = 40
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
              Align = alTop
              BevelOuter = bvNone
              Caption = #44277#44553#48155#45716#51088' '#45812#45817#51088' '#51221#48372' '#52628#44032
              Color = 16042877
              Ctl3D = False
              
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -15
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = [fsBold]
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
            end
            object edt_BYFTX1_1: TsEdit
              Tag = 203
              Left = 93
              Top = 56
              Width = 250
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = True
              MaxLength = 40
              ParentCtl3D = False
              ParentShowHint = False
              ShowHint = False
              TabOrder = 1
              BoundLabel.Active = True
              BoundLabel.Caption = #45812#45817#48512#49436#47749'(2)'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object edt_BYFTX2_1: TsEdit
              Tag = 203
              Left = 93
              Top = 80
              Width = 250
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = True
              MaxLength = 30
              ParentCtl3D = False
              ParentShowHint = False
              ShowHint = False
              TabOrder = 2
              BoundLabel.Active = True
              BoundLabel.Caption = #45812#45817#51088#47749'(2)'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object edt_BYFTX3_1: TsEdit
              Tag = 203
              Left = 93
              Top = 104
              Width = 250
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = True
              MaxLength = 20
              ParentCtl3D = False
              ParentShowHint = False
              ShowHint = False
              TabOrder = 3
              BoundLabel.Active = True
              BoundLabel.Caption = #51204#54868#48264#54840'(2)'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object edt_BYFTX4_1: TsEdit
              Left = 93
              Top = 128
              Width = 116
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = True
              MaxLength = 20
              ParentCtl3D = False
              TabOrder = 4
              BoundLabel.Active = True
              BoundLabel.Caption = 'E-MAIL(2)'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object edt_BYFTX5_1: TsEdit
              Left = 229
              Top = 128
              Width = 114
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = True
              MaxLength = 20
              ParentCtl3D = False
              TabOrder = 5
              BoundLabel.Active = True
              BoundLabel.Caption = '@'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object sBitBtn24: TsBitBtn
              Left = 100
              Top = 168
              Width = 82
              Height = 25
              Caption = #54869#51064
              TabOrder = 6
              OnClick = sBitBtn25Click
              ImageIndex = 17
              Images = DMICON.System18
            end
            object sBitBtn25: TsBitBtn
              Tag = 1
              Left = 183
              Top = 168
              Width = 82
              Height = 25
              Caption = #52712#49548
              TabOrder = 7
              OnClick = sBitBtn25Click
              ImageIndex = 18
              Images = DMICON.System18
            end
          end
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sPanel10: TsPanel
        inherited sPanel13: TsPanel
          Height = 184
          inherited GoodsBtnPanel: TsPanel
            inherited btn_goodsNew: TsSpeedButton
              OnClick = btn_goodsNewClick
            end
            inherited btn_goodsMod: TsSpeedButton
              OnClick = btn_goodsNewClick
            end
            inherited btn_goodsDel: TsSpeedButton
              OnClick = btn_goodsNewClick
            end
            inherited btn_goodsSave: TsSpeedButton
              OnClick = btn_goodsNewClick
            end
            inherited btn_goodsCancel: TsSpeedButton
              OnClick = btn_goodsNewClick
            end
            object GoodsExcelBtn: TsSpeedButton
              Left = 609
              Top = 1
              Width = 110
              Height = 23
              Caption = #50641#49472#44032#51256#50724#44592
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = GoodsExcelBtnClick
              Align = alRight
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              Images = DMICON.System18
              ImageIndex = 10
            end
            object GoodsSampleBtn: TsSpeedButton
              Left = 719
              Top = 1
              Width = 84
              Height = 23
              Caption = #50641#49472#49368#54540
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = GoodsSampleBtnClick
              Align = alRight
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              Images = DMICON.System18
              ImageIndex = 30
            end
          end
          inherited sDBGrid1: TsDBGrid
            Height = 159
            PopupMenu = goodsMenu
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'SEQ'
                Title.Alignment = taCenter
                Title.Caption = #49692#48264
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NAME_COD'
                Title.Alignment = taCenter
                Title.Caption = #54408#47749#53076#46300
                Width = 120
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'DE_DATE'
                Title.Alignment = taCenter
                Title.Caption = #44277#44553#51068#51088
                Width = 82
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RATE'
                Title.Alignment = taCenter
                Title.Caption = #54872#50984
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'QTY_G'
                Title.Alignment = taCenter
                Title.Caption = #49688#47049#45800#50948
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTY'
                Title.Alignment = taCenter
                Title.Caption = #49688#47049
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'QTYG_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032#44592#51456#49688#47049#45800#50948
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTYG'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032#44592#51456#49688#47049
                Width = 137
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'PRICE_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032#45800#50948
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRICE'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032
                Width = 137
                Visible = True
              end>
          end
        end
        inherited goodsPanel: TsPanel
          Top = 185
          Height = 373
          DesignSize = (
            804
            373)
          object sLabel16: TsLabel [0]
            Left = 56
            Top = 62
            Width = 122
            Height = 13
            Caption = #52572#45824' 4'#54665' 35'#50676' '#51077#47141' '#44032#45733
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel1: TsLabel [1]
            Left = 58
            Top = 143
            Width = 128
            Height = 13
            Caption = #52572#45824' 15'#54665' 70'#50676' '#51077#47141' '#44032#45733
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel2: TsLabel [2]
            Left = 58
            Top = 257
            Width = 128
            Height = 13
            Caption = #52572#45824' 15'#54665' 70'#50676' '#51077#47141' '#44032#45733
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel5: TsLabel [3]
            Left = 46
            Top = 116
            Width = 4
            Height = 13
            Alignment = taRightJustify
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel6: TsLabel [4]
            Left = 45
            Top = 225
            Width = 4
            Height = 13
            Alignment = taRightJustify
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel7: TsLabel [5]
            Left = 46
            Top = 338
            Width = 4
            Height = 13
            Alignment = taRightJustify
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          inherited sSpeedButton14: TsSpeedButton
            Left = 501
            Height = 383
          end
          inherited sPanel22: TsPanel
            Width = 489
            TabOrder = 22
          end
          inherited edt_NAMECOD_D: TsEdit
            Left = 52
            Top = 38
            Width = 123
            MaxLength = 20
          end
          inherited sBitBtn15: TsBitBtn
            Left = 176
            Top = 38
            TabOrder = 23
          end
          inherited memo_NAME1_D: TsMemo
            Left = 52
            Top = 76
            Width = 233
            Height = 55
            ScrollBars = ssVertical
            TabOrder = 1
            OnChange = memo_NAME1_DChange
            OnKeyPress = memoLineLimit
          end
          inherited memo_SIZE1_D: TsMemo
            Left = 52
            Top = 158
            Width = 443
            Lines.Strings = (
              ''
              '')
            ScrollBars = ssVertical
            TabOrder = 2
            OnChange = memo_NAME1_DChange
            OnKeyPress = memoLineLimit
            Text = #13#10#13#10
          end
          inherited sPanel14: TsPanel
            Left = 515
            Width = 279
            TabOrder = 24
          end
          inherited mask_DEDATE_D: TsMaskEdit
            Left = 396
            Top = 38
            TabOrder = 4
          end
          inherited curr_RATE_D: TsCurrencyEdit
            Left = 396
            Top = 62
            TabOrder = 5
          end
          inherited memo_DEREM1_D: TsMemo
            Left = 52
            Top = 271
            Width = 443
            ScrollBars = ssVertical
            TabOrder = 3
            OnChange = memo_NAME1_DChange
            OnKeyPress = memoLineLimit
          end
          inherited edt_QTY_G_D: TsEdit
            Left = 614
            Top = 38
          end
          inherited sBitBtn44: TsBitBtn
            Left = 654
            Top = 38
            TabOrder = 25
          end
          inherited curr_QTY_D: TsCurrencyEdit
            Left = 679
            Top = 38
            OnChange = curr_QTY_DChange
          end
          inherited curr_PRICE_D: TsCurrencyEdit
            Left = 614
            Top = 62
            OnChange = curr_QTY_DChange
          end
          inherited edt_QTYG_G_D: TsEdit
            Left = 614
            Top = 86
          end
          inherited sBitBtn47: TsBitBtn
            Left = 654
            Top = 86
            TabOrder = 26
          end
          inherited curr_QTYG_D: TsCurrencyEdit
            Left = 679
            Top = 86
            OnChange = curr_QTY_DChange
          end
          inherited edt_USAMT_G_D: TsEdit
            Left = 614
            Top = 158
          end
          inherited sBitBtn49: TsBitBtn
            Left = 654
            Top = 158
            TabOrder = 27
          end
          inherited curr_USAMT_D: TsCurrencyEdit
            Left = 679
            Top = 158
          end
          inherited curr_SUPAMT_D: TsCurrencyEdit
            Left = 614
            Top = 110
          end
          inherited curr_TAXAMT_D: TsCurrencyEdit
            Left = 614
            Top = 134
          end
          inherited edt_STQTY_G_D: TsEdit
            Left = 614
            Top = 222
            TabOrder = 15
          end
          inherited sBitBtn9: TsBitBtn
            Left = 654
            Top = 222
            TabOrder = 28
          end
          inherited curr_STQTY_D: TsCurrencyEdit
            Left = 679
            Top = 222
            TabOrder = 16
          end
          inherited curr_SUPSTAMT_D: TsCurrencyEdit
            Left = 614
            Top = 246
            TabOrder = 17
          end
          inherited curr_TAXSTAMT_D: TsCurrencyEdit
            Left = 614
            Top = 270
            TabOrder = 18
          end
          inherited edt_USSTAMT_G_D: TsEdit
            Left = 614
            Top = 294
            TabOrder = 19
          end
          inherited sBitBtn10: TsBitBtn
            Left = 654
            Top = 294
            TabOrder = 29
          end
          inherited curr_USSTAMT_D: TsCurrencyEdit
            Left = 679
            Top = 294
            TabOrder = 20
          end
          inherited sBitBtn8: TsBitBtn
            Left = 614
            Top = 322
            TabOrder = 21
            OnClick = sBitBtn8Click
          end
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      Caption = #54633#44228#44552#50529
      inherited sPanel16: TsPanel
        inherited moneyPanel: TsPanel
          inherited sBevel2: TsBevel
            Top = 87
            Width = 806
          end
          inherited sBevel3: TsBevel
            Top = 248
            Width = 805
          end
          inherited sBevel4: TsBevel
            Top = 388
            Width = 805
          end
          object sLabel8: TsLabel [3]
            Left = 234
            Top = 101
            Width = 353
            Height = 13
            Caption = #52572#45824' 10'#54665' 70'#50676' '#51077#47141' '#44032#45733' ('#44397#49464#52397' '#49888#44256#50857' '#44228#49328#49436#51032' '#44221#50864', '#52572#45824' 450Byte)'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel9: TsLabel [4]
            Left = 137
            Top = 212
            Width = 4
            Height = 13
            Alignment = taRightJustify
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          inherited sPanel18: TsPanel
            Caption = #54633' '#44228' '#44552' '#50529
          end
          inherited mask_DRAWDAT: TsMaskEdit
            Left = 109
            Top = 33
            Width = 105
          end
          inherited curr_SUPAMT: TsCurrencyEdit
            Left = 296
            Top = 57
            OnChange = curr_SUPAMTChange
          end
          inherited curr_TAXAMT: TsCurrencyEdit
            Left = 484
            Top = 57
          end
          inherited curr_DETAILNO: TsCurrencyEdit
            Left = 664
            Top = 57
          end
          inherited memo_REMARK1: TsMemo
            Left = 109
            Top = 116
            Width = 596
            OnChange = memo_NAME1_DChange
            OnKeyPress = memoLineLimit
          end
          inherited edt_AMT11C: TsEdit
            Left = 340
            Top = 271
          end
          inherited sBitBtn11: TsBitBtn
            Left = 380
            Top = 271
          end
          inherited curr_AMT12: TsCurrencyEdit
            Left = 109
            Top = 271
          end
          inherited curr_AMT11: TsCurrencyEdit
            Left = 405
            Top = 271
          end
          inherited curr_AMT22: TsCurrencyEdit
            Left = 109
            Top = 295
          end
          inherited edt_AMT21C: TsEdit
            Left = 340
            Top = 295
          end
          inherited sBitBtn12: TsBitBtn
            Left = 380
            Top = 295
          end
          inherited curr_AMT21: TsCurrencyEdit
            Left = 405
            Top = 295
          end
          inherited curr_AMT32: TsCurrencyEdit
            Left = 109
            Top = 319
          end
          inherited edt_AMT31C: TsEdit
            Left = 340
            Top = 319
          end
          inherited sBitBtn13: TsBitBtn
            Left = 380
            Top = 319
          end
          inherited curr_AMT31: TsCurrencyEdit
            Left = 405
            Top = 319
          end
          inherited curr_AMT42: TsCurrencyEdit
            Left = 109
            Top = 343
          end
          inherited edt_AMT41C: TsEdit
            Left = 340
            Top = 343
          end
          inherited sBitBtn14: TsBitBtn
            Left = 380
            Top = 343
          end
          inherited curr_AMT41: TsCurrencyEdit
            Left = 405
            Top = 343
          end
          inherited edt_INDICATOR: TsEdit
            Left = 589
            Top = 271
          end
          inherited sBitBtn16: TsBitBtn
            Left = 629
            Top = 271
          end
          inherited edt_INDICATORNAME: TsEdit
            Left = 654
            Top = 271
          end
          inherited curr_TAMT: TsCurrencyEdit
            Left = 109
            Top = 411
          end
          inherited curr_SUPTAMT: TsCurrencyEdit
            Left = 109
            Top = 435
          end
          inherited curr_TAXTAMT: TsCurrencyEdit
            Left = 589
            Top = 411
          end
          inherited edt_USTAMTC: TsEdit
            Left = 340
            Top = 411
          end
          inherited sBitBtn17: TsBitBtn
            Left = 380
            Top = 411
          end
          inherited curr_USTAMT: TsCurrencyEdit
            Left = 405
            Top = 411
          end
          inherited edt_TQTYC: TsEdit
            Left = 340
            Top = 435
          end
          inherited sBitBtn18: TsBitBtn
            Left = 380
            Top = 435
          end
          inherited curr_TQTY: TsCurrencyEdit
            Left = 405
            Top = 435
          end
          object sBitBtn26: TsBitBtn
            Left = 109
            Top = 57
            Width = 105
            Height = 23
            Caption = #54633#44228#44552#50529#44228#49328
            TabOrder = 34
            OnClick = sBitBtn26Click
            ImageIndex = 7
            Images = DMICON.System16
            SkinData.SkinSection = 'BUTTON'
          end
        end
      end
    end
    inherited sTabSheet6: TsTabSheet
      inherited sPanel12: TsPanel
        inherited trusteePanel: TsPanel
          inherited edt_AGSAUP1: TsEdit
            Left = 476
          end
          inherited edt_AGFTX1: TsEdit
            Left = 476
          end
          inherited edt_AGFTX2: TsEdit
            Left = 476
          end
          inherited edt_AGFTX3: TsEdit
            Left = 476
          end
          inherited edt_AGFTX4: TsEdit
            Left = 476
          end
          inherited edt_AGFTX5: TsEdit
            Left = 612
          end
          inherited memo_AGUPTA1: TsMemo
            Left = 476
          end
          inherited memo_AGITEM1: TsMemo
            Left = 476
          end
        end
      end
    end
    inherited sTabSheet5: TsTabSheet
      PopupMenu = popMenu1
      inherited sDBGrid2: TsDBGrid
        PopupMenu = popMenu1
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 35
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SE_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #44277#44553#51088
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BY_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #44277#44553#48155#45716#51088
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ACE_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47144#52280#51312#48264#54840
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAMT'
            Title.Alignment = taCenter
            Title.Caption = #52509#44552#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51088' ID'
            Width = 74
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DRAW_DAT'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51068#51088
            Width = 82
            Visible = True
          end>
      end
      inherited sPanel21: TsPanel
        inherited edt_SearchText: TsEdit
          Height = 23
          TabOrder = 2
        end
        inherited com_SearchKeyword: TsComboBox
          TabOrder = 4
          OnSelect = com_SearchKeywordSelect
        end
      end
    end
  end
  inherited sPanel56: TsPanel
    Height = 743
    inherited sDBGrid8: TsDBGrid
      Height = 708
      PopupMenu = popMenu1
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #49345#54889
          Width = 32
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 174
          Visible = True
        end>
    end
    inherited sPanel57: TsPanel
      inherited Mask_fromDate: TsMaskEdit
        Left = 127
      end
      inherited sBitBtn60: TsBitBtn
        Anchors = [akLeft, akTop, akRight]
      end
      inherited mask_toDate: TsMaskEdit
        Left = 200
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 184
  end
  inherited qryList: TADOQuery
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RE_NO, SE_N' +
        'O, FS_NO, ACE_NO, RFF_NO, SE_CODE, SE_SAUP, SE_NAME1, SE_NAME2, ' +
        'SE_ADDR1, SE_ADDR2, SE_ADDR3, SE_UPTA, SE_UPTA1, SE_ITEM, SE_ITE' +
        'M1, BY_CODE, BY_SAUP, BY_NAME1, BY_NAME2, BY_ADDR1, BY_ADDR2, BY' +
        '_ADDR3, BY_UPTA, BY_UPTA1, BY_ITEM, BY_ITEM1, AG_CODE, AG_SAUP, ' +
        'AG_NAME1, AG_NAME2, AG_NAME3, AG_ADDR1, AG_ADDR2, AG_ADDR3, AG_U' +
        'PTA, AG_UPTA1, AG_ITEM, AG_ITEM1, DRAW_DAT, DETAILNO, SUP_AMT, T' +
        'AX_AMT, REMARK, REMARK1, AMT11, AMT11C, AMT12, AMT21, AMT21C, AM' +
        'T22, AMT31, AMT31C, AMT32, AMT41, AMT41C, AMT42, INDICATOR, TAMT' +
        ', SUPTAMT, TAXTAMT, USTAMT, USTAMTC, TQTY, TQTYC, CHK1, CHK2, CH' +
        'K3, PRNO, VAT_CODE, VAT_TYPE, NEW_INDICATOR, SE_ADDR4, SE_ADDR5,' +
        ' SE_SAUP1, SE_SAUP2, SE_SAUP3, SE_FTX1, SE_FTX2, SE_FTX3, SE_FTX' +
        '4, SE_FTX5, BY_SAUP_CODE, BY_ADDR4, BY_ADDR5, BY_SAUP1, BY_SAUP2' +
        ', BY_SAUP3, BY_FTX1, BY_FTX2, BY_FTX3, BY_FTX4, BY_FTX5, BY_FTX1' +
        '_1, BY_FTX2_1, BY_FTX3_1, BY_FTX4_1, BY_FTX5_1, AG_ADDR4, AG_ADD' +
        'R5, AG_SAUP1, AG_SAUP2, AG_SAUP3, AG_FTX1, AG_FTX2, AG_FTX3, '
      '             AG_FTX4, AG_FTX5, SE_NAME3, BY_NAME3, '
      ''
      '             VATBI1_NAME.INDICATOR_NAME as INDICATOR_NAME'
      ''
      'From VATBI1_H'
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as INDICATOR_NAME FROM CODE2NDD with' +
        '(nolock) WHERE Prefix = '#39#50689#49688'/'#52397#44396#39') VATBI1_NAME ON VATBI1_H.INDICAT' +
        'OR = VATBI1_NAME.CODE'
      '')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRE_NO: TStringField
      FieldName = 'RE_NO'
      Size = 35
    end
    object qryListSE_NO: TStringField
      FieldName = 'SE_NO'
      Size = 35
    end
    object qryListFS_NO: TStringField
      FieldName = 'FS_NO'
      Size = 35
    end
    object qryListACE_NO: TStringField
      FieldName = 'ACE_NO'
      Size = 35
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListSE_CODE: TStringField
      FieldName = 'SE_CODE'
      Size = 10
    end
    object qryListSE_SAUP: TStringField
      FieldName = 'SE_SAUP'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_ADDR1: TStringField
      FieldName = 'SE_ADDR1'
      Size = 35
    end
    object qryListSE_ADDR2: TStringField
      FieldName = 'SE_ADDR2'
      Size = 35
    end
    object qryListSE_ADDR3: TStringField
      FieldName = 'SE_ADDR3'
      Size = 35
    end
    object qryListSE_UPTA: TStringField
      FieldName = 'SE_UPTA'
      Size = 35
    end
    object qryListSE_UPTA1: TMemoField
      FieldName = 'SE_UPTA1'
      BlobType = ftMemo
    end
    object qryListSE_ITEM: TStringField
      FieldName = 'SE_ITEM'
      Size = 1
    end
    object qryListSE_ITEM1: TMemoField
      FieldName = 'SE_ITEM1'
      BlobType = ftMemo
    end
    object qryListBY_CODE: TStringField
      FieldName = 'BY_CODE'
      Size = 10
    end
    object qryListBY_SAUP: TStringField
      FieldName = 'BY_SAUP'
      Size = 35
    end
    object qryListBY_NAME1: TStringField
      FieldName = 'BY_NAME1'
      Size = 35
    end
    object qryListBY_NAME2: TStringField
      FieldName = 'BY_NAME2'
      Size = 35
    end
    object qryListBY_ADDR1: TStringField
      FieldName = 'BY_ADDR1'
      Size = 35
    end
    object qryListBY_ADDR2: TStringField
      FieldName = 'BY_ADDR2'
      Size = 35
    end
    object qryListBY_ADDR3: TStringField
      FieldName = 'BY_ADDR3'
      Size = 35
    end
    object qryListBY_UPTA: TStringField
      FieldName = 'BY_UPTA'
      Size = 35
    end
    object qryListBY_UPTA1: TMemoField
      FieldName = 'BY_UPTA1'
      BlobType = ftMemo
    end
    object qryListBY_ITEM: TStringField
      FieldName = 'BY_ITEM'
      Size = 1
    end
    object qryListBY_ITEM1: TMemoField
      FieldName = 'BY_ITEM1'
      BlobType = ftMemo
    end
    object qryListAG_CODE: TStringField
      FieldName = 'AG_CODE'
      Size = 10
    end
    object qryListAG_SAUP: TStringField
      FieldName = 'AG_SAUP'
      Size = 35
    end
    object qryListAG_NAME1: TStringField
      FieldName = 'AG_NAME1'
      Size = 35
    end
    object qryListAG_NAME2: TStringField
      FieldName = 'AG_NAME2'
      Size = 35
    end
    object qryListAG_NAME3: TStringField
      FieldName = 'AG_NAME3'
      Size = 35
    end
    object qryListAG_ADDR1: TStringField
      FieldName = 'AG_ADDR1'
      Size = 35
    end
    object qryListAG_ADDR2: TStringField
      FieldName = 'AG_ADDR2'
      Size = 35
    end
    object qryListAG_ADDR3: TStringField
      FieldName = 'AG_ADDR3'
      Size = 35
    end
    object qryListAG_UPTA: TStringField
      FieldName = 'AG_UPTA'
      Size = 35
    end
    object qryListAG_UPTA1: TMemoField
      FieldName = 'AG_UPTA1'
      BlobType = ftMemo
    end
    object qryListAG_ITEM: TStringField
      FieldName = 'AG_ITEM'
      Size = 1
    end
    object qryListAG_ITEM1: TMemoField
      FieldName = 'AG_ITEM1'
      BlobType = ftMemo
    end
    object qryListDRAW_DAT: TStringField
      FieldName = 'DRAW_DAT'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListDETAILNO: TBCDField
      FieldName = 'DETAILNO'
      Precision = 18
    end
    object qryListSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      Precision = 18
    end
    object qryListTAX_AMT: TBCDField
      FieldName = 'TAX_AMT'
      Precision = 18
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAMT11: TBCDField
      FieldName = 'AMT11'
      Precision = 18
    end
    object qryListAMT11C: TStringField
      FieldName = 'AMT11C'
      Size = 3
    end
    object qryListAMT12: TBCDField
      FieldName = 'AMT12'
      Precision = 18
    end
    object qryListAMT21: TBCDField
      FieldName = 'AMT21'
      Precision = 18
    end
    object qryListAMT21C: TStringField
      FieldName = 'AMT21C'
      Size = 3
    end
    object qryListAMT22: TBCDField
      FieldName = 'AMT22'
      Precision = 18
    end
    object qryListAMT31: TBCDField
      FieldName = 'AMT31'
      Precision = 18
    end
    object qryListAMT31C: TStringField
      FieldName = 'AMT31C'
      Size = 3
    end
    object qryListAMT32: TBCDField
      FieldName = 'AMT32'
      Precision = 18
    end
    object qryListAMT41: TBCDField
      FieldName = 'AMT41'
      Precision = 18
    end
    object qryListAMT41C: TStringField
      FieldName = 'AMT41C'
      Size = 3
    end
    object qryListAMT42: TBCDField
      FieldName = 'AMT42'
      Precision = 18
    end
    object qryListINDICATOR: TStringField
      FieldName = 'INDICATOR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListSUPTAMT: TBCDField
      FieldName = 'SUPTAMT'
      Precision = 18
    end
    object qryListTAXTAMT: TBCDField
      FieldName = 'TAXTAMT'
      Precision = 18
    end
    object qryListUSTAMT: TBCDField
      FieldName = 'USTAMT'
      Precision = 18
    end
    object qryListUSTAMTC: TStringField
      FieldName = 'USTAMTC'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListVAT_CODE: TStringField
      FieldName = 'VAT_CODE'
      Size = 4
    end
    object qryListVAT_TYPE: TStringField
      FieldName = 'VAT_TYPE'
      Size = 4
    end
    object qryListNEW_INDICATOR: TStringField
      FieldName = 'NEW_INDICATOR'
      Size = 4
    end
    object qryListSE_ADDR4: TStringField
      FieldName = 'SE_ADDR4'
      Size = 35
    end
    object qryListSE_ADDR5: TStringField
      FieldName = 'SE_ADDR5'
      Size = 35
    end
    object qryListSE_SAUP1: TStringField
      FieldName = 'SE_SAUP1'
      Size = 4
    end
    object qryListSE_SAUP2: TStringField
      FieldName = 'SE_SAUP2'
      Size = 4
    end
    object qryListSE_SAUP3: TStringField
      FieldName = 'SE_SAUP3'
      Size = 4
    end
    object qryListSE_FTX1: TStringField
      FieldName = 'SE_FTX1'
      Size = 40
    end
    object qryListSE_FTX2: TStringField
      FieldName = 'SE_FTX2'
      Size = 30
    end
    object qryListSE_FTX3: TStringField
      FieldName = 'SE_FTX3'
    end
    object qryListSE_FTX4: TStringField
      FieldName = 'SE_FTX4'
    end
    object qryListSE_FTX5: TStringField
      FieldName = 'SE_FTX5'
    end
    object qryListBY_SAUP_CODE: TStringField
      FieldName = 'BY_SAUP_CODE'
      Size = 4
    end
    object qryListBY_ADDR4: TStringField
      FieldName = 'BY_ADDR4'
      Size = 35
    end
    object qryListBY_ADDR5: TStringField
      FieldName = 'BY_ADDR5'
      Size = 35
    end
    object qryListBY_SAUP1: TStringField
      FieldName = 'BY_SAUP1'
      Size = 4
    end
    object qryListBY_SAUP2: TStringField
      FieldName = 'BY_SAUP2'
      Size = 4
    end
    object qryListBY_SAUP3: TStringField
      FieldName = 'BY_SAUP3'
      Size = 4
    end
    object qryListBY_FTX1: TStringField
      FieldName = 'BY_FTX1'
      Size = 40
    end
    object qryListBY_FTX2: TStringField
      FieldName = 'BY_FTX2'
      Size = 30
    end
    object qryListBY_FTX3: TStringField
      FieldName = 'BY_FTX3'
    end
    object qryListBY_FTX4: TStringField
      FieldName = 'BY_FTX4'
    end
    object qryListBY_FTX5: TStringField
      FieldName = 'BY_FTX5'
    end
    object qryListBY_FTX1_1: TStringField
      FieldName = 'BY_FTX1_1'
      Size = 40
    end
    object qryListBY_FTX2_1: TStringField
      FieldName = 'BY_FTX2_1'
      Size = 30
    end
    object qryListBY_FTX3_1: TStringField
      FieldName = 'BY_FTX3_1'
    end
    object qryListBY_FTX4_1: TStringField
      FieldName = 'BY_FTX4_1'
    end
    object qryListBY_FTX5_1: TStringField
      FieldName = 'BY_FTX5_1'
    end
    object qryListAG_ADDR4: TStringField
      FieldName = 'AG_ADDR4'
      Size = 35
    end
    object qryListAG_ADDR5: TStringField
      FieldName = 'AG_ADDR5'
      Size = 35
    end
    object qryListAG_SAUP1: TStringField
      FieldName = 'AG_SAUP1'
      Size = 4
    end
    object qryListAG_SAUP2: TStringField
      FieldName = 'AG_SAUP2'
      Size = 4
    end
    object qryListAG_SAUP3: TStringField
      FieldName = 'AG_SAUP3'
      Size = 4
    end
    object qryListAG_FTX1: TStringField
      FieldName = 'AG_FTX1'
      Size = 40
    end
    object qryListAG_FTX2: TStringField
      FieldName = 'AG_FTX2'
      Size = 30
    end
    object qryListAG_FTX3: TStringField
      FieldName = 'AG_FTX3'
    end
    object qryListAG_FTX4: TStringField
      FieldName = 'AG_FTX4'
    end
    object qryListAG_FTX5: TStringField
      FieldName = 'AG_FTX5'
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListBY_NAME3: TStringField
      FieldName = 'BY_NAME3'
      Size = 35
    end
    object qryListINDICATOR_NAME: TStringField
      FieldName = 'INDICATOR_NAME'
      Size = 100
    end
  end
  inherited qryGoods: TADOQuery
    AfterInsert = qryGoodsAfterInsert
    AfterEdit = qryGoodsAfterInsert
    AfterScroll = qryGoodsAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        'KEYY, SEQ, DE_DATE, NAME_COD, NAME1, SIZE1, DE_REM1, QTY, QTY_G,' +
        ' QTYG, QTYG_G, PRICE, PRICE_G, SUPAMT, TAXAMT, USAMT, USAMT_G, S' +
        'UPSTAMT, TAXSTAMT, USSTAMT, USSTAMT_G, STQTY, STQTY_G, RATE'
      'FROM VATBI1_D'
      'WHERE KEYY = :MAINT_NO')
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryGoodsDE_DATE: TStringField
      FieldName = 'DE_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryGoodsNAME_COD: TStringField
      FieldName = 'NAME_COD'
    end
    object qryGoodsNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryGoodsSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryGoodsDE_REM1: TMemoField
      FieldName = 'DE_REM1'
      BlobType = ftMemo
    end
    object qryGoodsQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryGoodsQTYG: TBCDField
      FieldName = 'QTYG'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryGoodsPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryGoodsSUPAMT: TBCDField
      FieldName = 'SUPAMT'
      Precision = 18
    end
    object qryGoodsTAXAMT: TBCDField
      FieldName = 'TAXAMT'
      Precision = 18
    end
    object qryGoodsUSAMT: TBCDField
      FieldName = 'USAMT'
      Precision = 18
    end
    object qryGoodsUSAMT_G: TStringField
      FieldName = 'USAMT_G'
      Size = 3
    end
    object qryGoodsSUPSTAMT: TBCDField
      FieldName = 'SUPSTAMT'
      Precision = 18
    end
    object qryGoodsTAXSTAMT: TBCDField
      FieldName = 'TAXSTAMT'
      Precision = 18
    end
    object qryGoodsUSSTAMT: TBCDField
      FieldName = 'USSTAMT'
      Precision = 18
    end
    object qryGoodsUSSTAMT_G: TStringField
      FieldName = 'USSTAMT_G'
      Size = 3
    end
    object qryGoodsSTQTY: TBCDField
      FieldName = 'STQTY'
      Precision = 18
    end
    object qryGoodsSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryGoodsRATE: TBCDField
      FieldName = 'RATE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = Letter
    Left = 24
    Top = 288
  end
  object spCopyVATBI1: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'CopyVATBI1;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CopyDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@NewDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 56
    Top = 288
  end
  object goodsMenu: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = goodsMenuPopup
    Left = 24
    Top = 328
    object N1: TMenuItem
      Caption = #45936#51060#53552' '#52628#44032
      ImageIndex = 26
      OnClick = N1Click
    end
    object N2: TMenuItem
      Tag = 1
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = N1Click
    end
    object N3: TMenuItem
      Tag = 2
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = N1Click
    end
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE VATBI1_H'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 104
    Top = 248
  end
  object popMenu1: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    Left = 56
    Top = 328
    object MenuItem1: TMenuItem
      Caption = #51204#49569#51456#48708
      ImageIndex = 29
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object MenuItem3: TMenuItem
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = btnEditClick
    end
    object N4: TMenuItem
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = btnDelClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object d1: TMenuItem
      Caption = #52636#47141#54616#44592
      object N8: TMenuItem
        Caption = #48120#47532#48372#44592
        ImageIndex = 15
        OnClick = btnPrintClick
      end
      object N9: TMenuItem
        Tag = 1
        Caption = #54532#47536#53944
        ImageIndex = 21
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = 'ADMIN'
      Enabled = False
      ImageIndex = 16
    end
  end
  object excelOpen: TsOpenDialog
    Filter = 
      #47784#46304' Excel '#54028#51068'(*.xls;*.xlsx)|*.xls;*.xlsx|Excel '#53685#54633#47928#49436'(*.xlsx)|*.xlsx' +
      '|Excel 97 - 2003 '#53685#54633#47928#49436'(*.xls)|*.xls|All Files|*.*'
    Left = 88
    Top = 288
  end
end
