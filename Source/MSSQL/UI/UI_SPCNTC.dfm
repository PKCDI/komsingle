inherited UI_SPCNTC_frm: TUI_SPCNTC_frm
  Left = 753
  Top = 32
  Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077')'#52376#47532#51025#45813#49436
  ClientHeight = 687
  ClientWidth = 1114
  Constraints.MaxHeight = 726
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 77
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter7: TsSplitter [1]
    Left = 0
    Top = 41
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel4: TsPanel [2]
    Left = 0
    Top = 44
    Width = 1114
    Height = 33
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    object mask_DATEE: TsMaskEdit
      Left = 374
      Top = 5
      Width = 83
      Height = 23
      TabStop = False
      AutoSize = False
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_USER_ID: TsEdit
      Left = 520
      Top = 5
      Width = 57
      Height = 23
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 657
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      TabStop = False
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 737
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      TabStop = False
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_MAINT_NO: TsEdit
      Left = 64
      Top = 5
      Width = 241
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
  end
  object Page_control: TsPageControl [3]
    Left = 0
    Top = 80
    Width = 1114
    Height = 607
    ActivePage = sTabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 0
    TabOrder = 1
    TabStop = False
    TabWidth = 100
    OnChange = Page_controlChange
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter3: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter4: TsSplitter
        Left = 273
        Top = 2
        Width = 3
        Height = 565
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page1_Left: TsPanel
        Left = 0
        Top = 2
        Width = 273
        Height = 565
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        
        TabOrder = 0
      end
      object page1_Right: TsPanel
        Left = 276
        Top = 2
        Width = 830
        Height = 565
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 1
        object basicPanel: TsPanel
          Left = 1
          Top = 1
          Width = 828
          Height = 83
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 0
          TabStop = True
          object sPanel3: TsPanel
            Left = -2
            Top = 1
            Width = 828
            Height = 19
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #44592#48376#51077#47141#49324#54637
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 0
          end
          object edt_BGMGUBUN: TsEdit
            Tag = 305
            Left = 120
            Top = 24
            Width = 46
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51025#45813#49436' '#44396#48516
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object mask_NTDATE: TsMaskEdit
            Left = 120
            Top = 43
            Width = 72
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #53685#51648#51068#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object mask_CPDATE: TsMaskEdit
            Left = 120
            Top = 62
            Width = 72
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47588#51077#51068#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_DMNO: TsEdit
            Tag = 305
            Left = 520
            Top = 24
            Width = 249
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52628#49900'('#47588#51077')'#51032#47280#49436#13#10#51204#51088#47928#49436#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_LCNO: TsEdit
            Tag = 305
            Left = 520
            Top = 43
            Width = 249
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45236#44397#49888#50857#51109#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CPNO: TsEdit
            Tag = 305
            Left = 520
            Top = 62
            Width = 249
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52628#49900'('#47588#51077')'#49888#52397#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_BGMGUBUNNAME: TsEdit
            Tag = 305
            Left = 167
            Top = 24
            Width = 178
            Height = 19
            Hint = #44277#44553#51088#50857' '#47588#51077#52628#49900#51008#54665
            HelpKeyword = 'APPSPC'#51008#54665
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 7
            Text = #52628#49900'('#47588#51077') '#49849#51064#53685#48372
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
        end
        object appPanel: TsPanel
          Left = 1
          Top = 84
          Width = 828
          Height = 58
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 1
          TabStop = True
          object sPanel10: TsPanel
            Left = -2
            Top = 0
            Width = 828
            Height = 19
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49888#52397#51064
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 0
          end
          object edt_APPSNAME: TsEdit
            Tag = 305
            Left = 120
            Top = 20
            Width = 225
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49888#52397#51064' '#49345#54840#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_APPNAME: TsEdit
            Tag = 305
            Left = 120
            Top = 39
            Width = 225
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#54364#51088#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object mask_APPNO: TsMaskEdit
            Left = 520
            Top = 20
            Width = 104
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            EditMask = '999-99-99999;0;'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 12
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.SkinSection = 'EDIT'
          end
        end
        object noPanel: TsPanel
          Left = 1
          Top = 296
          Width = 828
          Height = 173
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 2
          TabStop = True
          object sPanel9: TsPanel
            Left = -2
            Top = 1
            Width = 828
            Height = 19
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #47932#54408#49688#47161#51613#47749#49436#48264#54840' / '#49464#44552#44228#49328#49436#48264#54840
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 0
          end
          object edt_RCNO: TsEdit
            Tag = 101
            Left = 120
            Top = 21
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47932#54408#49688#47161#51613#47749#49436
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_RCNO2: TsEdit
            Left = 120
            Top = 40
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = '('#51064#49688#51613')'#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_RCNO3: TsEdit
            Left = 120
            Top = 59
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
          end
          object edt_RCNO4: TsEdit
            Left = 120
            Top = 78
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
          end
          object edt_RCNO5: TsEdit
            Left = 120
            Top = 97
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
          end
          object edt_RCNO6: TsEdit
            Left = 120
            Top = 116
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
          end
          object edt_RCNO7: TsEdit
            Left = 120
            Top = 135
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            SkinData.SkinSection = 'EDIT'
          end
          object edt_RCNO8: TsEdit
            Left = 120
            Top = 154
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
          end
          object edt_FINNO: TsEdit
            Tag = 101
            Left = 520
            Top = 21
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49464#44552#44228#49328#49436' '#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_FINNO2: TsEdit
            Left = 520
            Top = 40
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            SkinData.SkinSection = 'EDIT'
          end
          object edt_FINNO3: TsEdit
            Left = 520
            Top = 59
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
          end
          object edt_FINNO4: TsEdit
            Left = 520
            Top = 78
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 12
            SkinData.SkinSection = 'EDIT'
          end
          object edt_FINNO5: TsEdit
            Left = 520
            Top = 97
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 13
            SkinData.SkinSection = 'EDIT'
          end
          object edt_FINNO6: TsEdit
            Left = 520
            Top = 116
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 14
            SkinData.SkinSection = 'EDIT'
          end
          object edt_FINNO7: TsEdit
            Left = 520
            Top = 135
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 15
            SkinData.SkinSection = 'EDIT'
          end
        end
        object bankPanel: TsPanel
          Left = 1
          Top = 142
          Width = 828
          Height = 154
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 3
          TabStop = True
          object sPanel12: TsPanel
            Left = -2
            Top = 1
            Width = 828
            Height = 19
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #52628#49900'('#47588#51077')'#51008#54665
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 0
          end
          object edt_CPBANK: TsEdit
            Tag = 101
            Left = 120
            Top = 21
            Width = 80
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52628#49900'('#47588#51077')'#51008#54665#53076#46300
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CPBANKNAME: TsEdit
            Left = 120
            Top = 40
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51008#54665#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CPBANKBU: TsEdit
            Left = 120
            Top = 59
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = '('#48512')'#51216#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CPBANKELEC: TsEdit
            Left = 120
            Top = 78
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51204#51088#49436#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CPACCOUNTNO: TsEdit
            Left = 120
            Top = 97
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44228#51340#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CPNAME1: TsEdit
            Left = 120
            Top = 116
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44228#51340#51452
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CPNAME2: TsEdit
            Left = 120
            Top = 135
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            SkinData.SkinSection = 'EDIT'
          end
          object edt_CPAMTC: TsEdit
            Tag = 102
            Left = 520
            Top = 21
            Width = 41
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #50808#54868#44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_CPAMTU: TsCurrencyEdit
            Left = 562
            Top = 21
            Width = 142
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_CPAMT: TsCurrencyEdit
            Left = 520
            Top = 40
            Width = 184
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #50896#54868#44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_CPTOTAMTC: TsEdit
            Tag = 102
            Left = 520
            Top = 59
            Width = 41
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52572#51333#51648#44553#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_CPTOTAMT: TsCurrencyEdit
            Left = 562
            Top = 59
            Width = 142
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 12
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_CPTOTCHARGEC: TsEdit
            Tag = 102
            Left = 520
            Top = 78
            Width = 41
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 13
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#49688#47308#54633#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_CPTOTCHARGE: TsCurrencyEdit
            Left = 562
            Top = 78
            Width = 142
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 14
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
        end
        object memoPanel: TsPanel
          Left = 1
          Top = 469
          Width = 828
          Height = 144
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 4
          TabStop = True
          object sPanel14: TsPanel
            Left = -2
            Top = 2
            Width = 828
            Height = 19
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #53685#48372#45236#50857
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 0
          end
          object memo_CPFTX1: TsMemo
            Left = 120
            Top = 22
            Width = 625
            Height = 72
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = []
            MaxLength = 1050
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 1
            CharCase = ecUpperCase
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #53685#48372#45236#50857
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeftTop
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #49464#44552#44228#49328#49436
      object sSplitter6: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter5: TsSplitter
        Left = 273
        Top = 2
        Width = 3
        Height = 565
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page2_Left: TsPanel
        Left = 0
        Top = 2
        Width = 273
        Height = 565
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        
        TabOrder = 0
      end
      object page2_right: TsPanel
        Left = 276
        Top = 2
        Width = 830
        Height = 565
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 1
        object sDBGrid4: TsDBGrid
          Left = 1
          Top = 26
          Width = 828
          Height = 263
          Align = alTop
          Color = clWhite
          Ctl3D = False
          DataSource = dsGoods
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHARGE_NO'
              Title.Alignment = taCenter
              Title.Caption = #44228#49328#49436#48264#54840
              Width = 150
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHARGE_TYPE'
              Title.Alignment = taCenter
              Title.Caption = #50976#54805
              Width = 40
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHARGE_RATE'
              Title.Alignment = taCenter
              Title.Caption = #51201#50857#50836#50984
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BAS_AMT'
              Title.Alignment = taCenter
              Title.Caption = #45824#49345#44552#50529
              Width = 137
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BAS_AMTC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CHA_AMT'
              Title.Alignment = taCenter
              Title.Caption = #49328#52636#44552#50529
              Width = 137
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHA_AMTC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CUX_RATE'
              Title.Alignment = taCenter
              Title.Caption = #51201#50857#54872#50984
              Width = 137
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SES_DAY'
              Title.Alignment = taCenter
              Title.Caption = #51201#50857#51068#49688
              Width = 52
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SES_SDATE'
              Title.Alignment = taCenter
              Title.Caption = #51201#50857#44592#44036'('#49884#51089')'
              Width = 84
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SES_EDATE'
              Title.Alignment = taCenter
              Title.Caption = #51201#50857#44592#44036'('#51333#47308')'
              Width = 84
              Visible = True
            end>
        end
        object billPanel: TsPanel
          Left = 1
          Top = 289
          Width = 828
          Height = 275
          SkinData.SkinSection = 'PANEL'
          Align = alClient
          
          TabOrder = 1
          object sPanel18: TsPanel
            Left = 1
            Top = 1
            Width = 826
            Height = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Align = alTop
            BevelOuter = bvNone
            Caption = #44228#49328#49436
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 0
          end
          object edt_CHARGENO: TsEdit
            Left = 152
            Top = 56
            Width = 233
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44228#49328#49436#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CHARGETYPE: TsEdit
            Tag = 305
            Left = 152
            Top = 80
            Width = 46
            Height = 23
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#49688#47308'('#51060#51088')'#50976#54805
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_CHARGETYPENAME: TsEdit
            Tag = 305
            Left = 199
            Top = 80
            Width = 186
            Height = 23
            Hint = #44277#44553#51088#50857' '#47588#51077#52628#49900#51008#54665
            HelpKeyword = 'APPSPC'#51008#54665
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            Text = #52628#49900'('#47588#51077') '#49849#51064#53685#48372
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object curr_CUXRATE: TsCurrencyEdit
            Left = 496
            Top = 128
            Width = 184
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 4
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51201#50857#54872#50984
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_SESDAY: TsEdit
            Tag = 305
            Left = 152
            Top = 104
            Width = 46
            Height = 23
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51201#50857#51068#49688
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object mask_SESSDATE: TsMaskEdit
            Left = 152
            Top = 128
            Width = 72
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51201#50857#44592#44036
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object mask_SESSEDATE: TsMaskEdit
            Left = 152
            Top = 152
            Width = 72
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            CheckOnExit = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_BASAMTC: TsEdit
            Tag = 102
            Left = 496
            Top = 80
            Width = 41
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45824#49345#44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_BASAMT: TsCurrencyEdit
            Left = 538
            Top = 80
            Width = 142
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 9
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_CHAAMTC: TsEdit
            Tag = 102
            Left = 496
            Top = 104
            Width = 41
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 10
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49328#52636#44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_CHAAMT: TsCurrencyEdit
            Left = 538
            Top = 104
            Width = 142
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_CHARGERATE: TsEdit
            Tag = 305
            Left = 496
            Top = 56
            Width = 184
            Height = 23
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            MaxLength = 3
            ParentCtl3D = False
            TabOrder = 12
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51201#50857#50836#50984
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
        object goodsBtnPanel: TsPanel
          Left = 1
          Top = 1
          Width = 828
          Height = 25
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          
          TabOrder = 2
          object sSpeedButton10: TsSpeedButton
            Left = 1
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object sSpeedButton19: TsSpeedButton
            Left = 73
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_goodsDel: TsSpeedButton
            Tag = 3
            Left = 150
            Top = 1
            Width = 67
            Height = 23
            Cursor = crHandPoint
            Caption = #49325#51228
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 1
          end
          object sSpeedButton22: TsSpeedButton
            Left = 145
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_goodsMod: TsSpeedButton
            Tag = 2
            Left = 78
            Top = 1
            Width = 67
            Height = 23
            Cursor = crHandPoint
            Caption = #49688#51221
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 3
          end
          object sSpeedButton24: TsSpeedButton
            Left = 217
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_goodsSave: TsSpeedButton
            Tag = 4
            Left = 222
            Top = 1
            Width = 67
            Height = 23
            Cursor = crHandPoint
            Caption = #51200#51109
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 17
          end
          object btn_goodsCancel: TsSpeedButton
            Tag = 5
            Left = 289
            Top = 1
            Width = 67
            Height = 23
            Cursor = crHandPoint
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 18
          end
          object sSpeedButton27: TsSpeedButton
            Left = 356
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object btn_goodsNew: TsSpeedButton
            Tag = 1
            Left = 6
            Top = 1
            Width = 67
            Height = 23
            Cursor = crHandPoint
            Caption = #51077#47141
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 2
          end
        end
      end
    end
    object sTabSheet6: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter2: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 34
        Width = 1106
        Height = 533
        TabStop = False
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid8DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'BGM_GUBUN'
            Title.Alignment = taCenter
            Title.Caption = #51025#45813#44396#48516
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CP_NO'
            Title.Alignment = taCenter
            Title.Caption = #52628#49900'('#47588#51077')'#48264#54840
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CP_AMTU'
            Title.Alignment = taCenter
            Title.Caption = #50808#54868#44552#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CP_AMTC'
            Title.Alignment = taCenter
            Title.Caption = #50808#54868#45800#50948
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CP_AMT'
            Title.Alignment = taCenter
            Title.Caption = #50896#54868#44552#50529
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CP_TOTAMT'
            Title.Alignment = taCenter
            Title.Caption = #52572#51333#51648#44553#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CP_TOTAMTC'
            Title.Alignment = taCenter
            Title.Caption = #51648#44553#50529#45800#50948
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CP_BANKNAME'
            Title.Alignment = taCenter
            Title.Caption = #51008#54665#47749
            Width = 150
            Visible = True
          end>
      end
      object sPanel5: TsPanel
        Left = 0
        Top = 2
        Width = 1106
        Height = 32
        SkinData.SkinSection = 'PANEL'
        Align = alTop
        
        TabOrder = 0
        object edt_SearchText: TsEdit
          Left = 138
          Top = 5
          Width = 233
          Height = 25
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 133
          Height = 23
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 2
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #52628#49900'('#47588#51077')'#49888#52397#48264#54840)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 138
          Top = 5
          Width = 82
          Height = 25
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn8: TsBitBtn
          Left = 379
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 5
          OnClick = sBitBtn8Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn22: TsBitBtn
          Tag = 902
          Left = 221
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 4
          TabStop = False
          OnClick = sBitBtn28Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 272
          Top = 5
          Width = 82
          Height = 25
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn28: TsBitBtn
          Tag = 903
          Left = 355
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = sBitBtn28Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel6: TsPanel
          Left = 246
          Top = 5
          Width = 25
          Height = 23
          SkinData.SkinSection = 'PANEL'
          Caption = '~'
          
          TabOrder = 7
        end
      end
    end
  end
  object sPanel56: TsPanel [4]
    Left = 6
    Top = 117
    Width = 267
    Height = 766
    SkinData.SkinSection = 'PANEL'
    
    TabOrder = 2
    object sDBGrid8: TsDBGrid
      Left = 1
      Top = 34
      Width = 265
      Height = 731
      TabStop = False
      Align = alClient
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #44404#47548#52404
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid8DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 170
          Visible = True
        end>
    end
    object sPanel57: TsPanel
      Left = 1
      Top = 1
      Width = 265
      Height = 33
      SkinData.SkinSection = 'PANEL'
      Align = alTop
      BevelOuter = bvNone
      
      TabOrder = 1
      object Mask_fromDate: TsMaskEdit
        Left = 93
        Top = 5
        Width = 72
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20160101'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn60: TsBitBtn
        Left = 239
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = sBitBtn60Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object mask_toDate: TsMaskEdit
        Left = 166
        Top = 5
        Width = 73
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20171231'
        CheckOnExit = True
        SkinData.SkinSection = 'EDIT'
      end
    end
  end
  object sPanel1: TsPanel [5]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 3
    DesignSize = (
      1114
      41)
    object sSpeedButton2: TsSpeedButton
      Left = 191
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 471
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel
      Left = 8
      Top = 5
      Width = 179
      Height = 17
      Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077')'#52376#47532#51025#45813#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel
      Left = 8
      Top = 20
      Width = 42
      Height = 13
      Caption = 'SPCNTC'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object btnExit: TsButton
      Left = 1034
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Left = 201
      Top = 2
      Width = 70
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object sButton4: TsButton
      Left = 271
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton2: TsButton
      Tag = 1
      Left = 370
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 3
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 200
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, BGM_GUBUN, MESSAGE1, MESSAGE2, ' +
        'NT_DATE, CP_DATE, DM_NO, LC_NO, CP_ACCOUNTNO, CP_NAME1, CP_NAME2' +
        ', CP_BANK, CP_BANKNAME, CP_BANKBU, APP_SNAME, APP_NAME, CP_BANKE' +
        'LEC, CP_AMTU, CP_AMTC, CP_AMT, CP_TOTAMT, CP_TOTAMTC, CP_TOTCHAR' +
        'GE, CP_TOTCHARGEC, CP_FTX, CP_FTX1, CHK1, CHK2, CHK3, BSN_HSCODE' +
        ', APP_NO, CP_NO, RC_NO, RC_NO2, RC_NO3, RC_NO4, RC_NO5, RC_NO6, ' +
        'RC_NO7, RC_NO8, FIN_NO, FIN_NO2, FIN_NO3, FIN_NO4, FIN_NO5, FIN_' +
        'NO6, FIN_NO7'
      'FROM SPCNTC_H')
    Left = 24
    Top = 240
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListBGM_GUBUN: TStringField
      FieldName = 'BGM_GUBUN'
      OnGetText = qryListBGM_GUBUNGetText
      Size = 3
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListNT_DATE: TStringField
      FieldName = 'NT_DATE'
      Size = 8
    end
    object qryListCP_DATE: TStringField
      FieldName = 'CP_DATE'
      Size = 8
    end
    object qryListDM_NO: TStringField
      FieldName = 'DM_NO'
      Size = 35
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListCP_ACCOUNTNO: TStringField
      FieldName = 'CP_ACCOUNTNO'
      Size = 17
    end
    object qryListCP_NAME1: TStringField
      FieldName = 'CP_NAME1'
      Size = 35
    end
    object qryListCP_NAME2: TStringField
      FieldName = 'CP_NAME2'
      Size = 35
    end
    object qryListCP_BANK: TStringField
      FieldName = 'CP_BANK'
      Size = 11
    end
    object qryListCP_BANKNAME: TStringField
      FieldName = 'CP_BANKNAME'
      Size = 70
    end
    object qryListCP_BANKBU: TStringField
      FieldName = 'CP_BANKBU'
      Size = 70
    end
    object qryListAPP_SNAME: TStringField
      FieldName = 'APP_SNAME'
      Size = 35
    end
    object qryListAPP_NAME: TStringField
      FieldName = 'APP_NAME'
      Size = 35
    end
    object qryListCP_BANKELEC: TStringField
      FieldName = 'CP_BANKELEC'
      Size = 35
    end
    object qryListCP_AMTU: TBCDField
      FieldName = 'CP_AMTU'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListCP_AMTC: TStringField
      FieldName = 'CP_AMTC'
      Size = 3
    end
    object qryListCP_AMT: TBCDField
      FieldName = 'CP_AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListCP_TOTAMT: TBCDField
      FieldName = 'CP_TOTAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListCP_TOTAMTC: TStringField
      FieldName = 'CP_TOTAMTC'
      Size = 3
    end
    object qryListCP_TOTCHARGE: TBCDField
      FieldName = 'CP_TOTCHARGE'
      Precision = 18
    end
    object qryListCP_TOTCHARGEC: TStringField
      FieldName = 'CP_TOTCHARGEC'
      Size = 3
    end
    object qryListCP_FTX: TStringField
      FieldName = 'CP_FTX'
      Size = 3
    end
    object qryListCP_FTX1: TMemoField
      FieldName = 'CP_FTX1'
      BlobType = ftMemo
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryListAPP_NO: TStringField
      FieldName = 'APP_NO'
      Size = 10
    end
    object qryListCP_NO: TStringField
      FieldName = 'CP_NO'
      Size = 35
    end
    object qryListRC_NO: TStringField
      FieldName = 'RC_NO'
      Size = 35
    end
    object qryListRC_NO2: TStringField
      FieldName = 'RC_NO2'
      Size = 35
    end
    object qryListRC_NO3: TStringField
      FieldName = 'RC_NO3'
      Size = 35
    end
    object qryListRC_NO4: TStringField
      FieldName = 'RC_NO4'
      Size = 35
    end
    object qryListRC_NO5: TStringField
      FieldName = 'RC_NO5'
      Size = 35
    end
    object qryListRC_NO6: TStringField
      FieldName = 'RC_NO6'
      Size = 35
    end
    object qryListRC_NO7: TStringField
      FieldName = 'RC_NO7'
      Size = 35
    end
    object qryListRC_NO8: TStringField
      FieldName = 'RC_NO8'
      Size = 35
    end
    object qryListFIN_NO: TStringField
      FieldName = 'FIN_NO'
      Size = 35
    end
    object qryListFIN_NO2: TStringField
      FieldName = 'FIN_NO2'
      Size = 35
    end
    object qryListFIN_NO3: TStringField
      FieldName = 'FIN_NO3'
      Size = 35
    end
    object qryListFIN_NO4: TStringField
      FieldName = 'FIN_NO4'
      Size = 35
    end
    object qryListFIN_NO5: TStringField
      FieldName = 'FIN_NO5'
      Size = 35
    end
    object qryListFIN_NO6: TStringField
      FieldName = 'FIN_NO6'
      Size = 35
    end
    object qryListFIN_NO7: TStringField
      FieldName = 'FIN_NO7'
      Size = 35
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 240
  end
  object dsGoods: TDataSource
    DataSet = qryGoods
    Left = 56
    Top = 272
  end
  object qryGoods: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryGoodsAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, CHARGE_NO, CHARGE_TYPE, CHARGE_RATE, BAS_AMT, ' +
        'BAS_AMTC, CHA_AMT, CHA_AMTC, CUX_RATE, SES_DAY, SES_SDATE, SES_E' +
        'DATE'
      '       ,SPCNTC_NAME.CHARGE_TYPE_Name as CHARGE_TYPE_Name'
      '                    '
      'FROM SPCNTC_D '
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as CHARGE_TYPE_Name FROM CODE2NDD wi' +
        'th(nolock) WHERE Prefix = '#39'5189FINBIL'#39') SPCNTC_NAME ON SPCNTC_D.' +
        'CHARGE_TYPE = SPCNTC_NAME.CODE'
      ''
      ''
      'WHERE KEYY =  :MAINT_NO ')
    Left = 24
    Top = 272
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryGoodsCHARGE_NO: TStringField
      FieldName = 'CHARGE_NO'
      Size = 35
    end
    object qryGoodsCHARGE_TYPE: TStringField
      FieldName = 'CHARGE_TYPE'
      Size = 3
    end
    object qryGoodsCHARGE_RATE: TStringField
      FieldName = 'CHARGE_RATE'
      Size = 8
    end
    object qryGoodsBAS_AMT: TBCDField
      FieldName = 'BAS_AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsBAS_AMTC: TStringField
      FieldName = 'BAS_AMTC'
      Size = 3
    end
    object qryGoodsCHA_AMT: TBCDField
      FieldName = 'CHA_AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsCHA_AMTC: TStringField
      FieldName = 'CHA_AMTC'
      Size = 3
    end
    object qryGoodsCUX_RATE: TBCDField
      FieldName = 'CUX_RATE'
      Precision = 18
    end
    object qryGoodsSES_DAY: TStringField
      FieldName = 'SES_DAY'
      Size = 3
    end
    object qryGoodsSES_SDATE: TStringField
      FieldName = 'SES_SDATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryGoodsSES_EDATE: TStringField
      FieldName = 'SES_EDATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryGoodsCHARGE_TYPE_Name: TStringField
      FieldName = 'CHARGE_TYPE_Name'
      Size = 100
    end
  end
end
