inherited UI_ADV707_frm: TUI_ADV707_frm
  Left = 418
  Top = 93
  Caption = 'UI_ADV707_frm'
  ClientWidth = 1114
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter3: TsSplitter [1]
    Left = 0
    Top = 76
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel4: TsPanel [2]
    Left = 0
    Top = 44
    Width = 1114
    Height = 32
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 778
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 5
      TabStop = False
      Visible = False
      ImageIndex = 9
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object mask_DATEE: TsMaskEdit
      Left = 302
      Top = 5
      Width = 83
      Height = 23
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_MaintNo: TsEdit
      Left = 64
      Top = 5
      Width = 121
      Height = 21
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clBlack
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn2: TsBitBtn
      Tag = 1
      Left = 730
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 7
      TabStop = False
      Visible = False
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 569
      Top = 5
      Width = 32
      Height = 21
      Hint = #44592#45733#54364#49884
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_UserNo: TsEdit
      Left = 441
      Top = 5
      Width = 57
      Height = 21
      TabStop = False
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sCheckBox1: TsCheckBox
      Left = 880
      Top = 6
      Width = 64
      Height = 16
      TabStop = False
      Caption = #46356#48260#44536
      TabOrder = 6
      Visible = False
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 649
      Top = 5
      Width = 32
      Height = 21
      Hint = #51025#45813#50976#54805
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn3: TsBitBtn
      Tag = 2
      Left = 754
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 8
      TabStop = False
      Visible = False
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_AMDNO: TsEdit
      Left = 185
      Top = 5
      Width = 32
      Height = 21
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 9
      SkinData.SkinSection = 'EDIT'
    end
  end
  object sPanel1: TsPanel [3]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 1
    object sSpeedButton2: TsSpeedButton
      Left = 451
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 174
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel
      Left = 8
      Top = 5
      Width = 161
      Height = 17
      Caption = #49688#52636#49888#50857#51109' '#51312#44148#48320#44221#53685#51648#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel
      Left = 8
      Top = 20
      Width = 41
      Height = 13
      Caption = 'ADV707'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object btnExit: TsButton
      Left = 1036
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Left = 184
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object sButton4: TsButton
      Left = 251
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton2: TsButton
      Tag = 1
      Left = 350
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 3
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  object sPageControl1: TsPageControl [4]
    Left = 0
    Top = 79
    Width = 1114
    Height = 602
    ActivePage = sTabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 0
    TabOrder = 2
    TabStop = False
    TabWidth = 125
    OnChange = sPageControl1Change
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter4: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel5: TsPanel
        Left = 0
        Top = 2
        Width = 270
        Height = 560
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        
        TabOrder = 0
      end
      object sPanel2: TsPanel
        Left = 270
        Top = 2
        Width = 836
        Height = 560
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 1
        DesignSize = (
          836
          560)
        object sSpeedButton1: TsSpeedButton
          Left = 415
          Top = -7
          Width = 9
          Height = 313
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sBevel1: TsBevel
          Left = 0
          Top = 305
          Width = 835
          Height = 1
          Shape = bsBottomLine
        end
        object mask_APPDATE: TsMaskEdit
          Left = 122
          Top = 23
          Width = 83
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 0
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #53685#51648#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPNO: TsEdit
          Left = 122
          Top = 44
          Width = 219
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #53685#51648#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel18: TsPanel
          Left = 20
          Top = 2
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44592#48376#51221#48372
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object mask_AMDDATE: TsMaskEdit
          Left = 122
          Top = 107
          Width = 83
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 3
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51312#44148#48320#44221#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel24: TsPanel
          Left = 20
          Top = 153
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #50896#49688#51061#51088'(Beneficiary-before this amendment)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object edt_BENEFC1: TsEdit
          Left = 122
          Top = 174
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50896#49688#51061#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_BENEFC2: TsEdit
          Left = 122
          Top = 195
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BENEFC3: TsEdit
          Left = 122
          Top = 216
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BENEFC4: TsEdit
          Left = 122
          Top = 237
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AMDNO1: TsEdit
          Left = 122
          Top = 128
          Width = 83
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 11
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51312#44148#48320#44221#54943#49688
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_BENEFC5: TsEdit
          Left = 122
          Top = 258
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BENEFC6: TsEdit
          Left = 122
          Top = 279
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object mask_EXDATE: TsMaskEdit
          Left = 122
          Top = 86
          Width = 83
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 12
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50976#54952#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object mask_ISSDATE: TsMaskEdit
          Left = 122
          Top = 65
          Width = 83
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 13
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object curr_INCDAMT: TsCurrencyEdit
          Left = 588
          Top = 23
          Width = 135
          Height = 21
          AutoSize = False
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
        end
        object edt_INCDCUR: TsEdit
          Left = 552
          Top = 23
          Width = 35
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 3
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109' '#51613#50529#48516
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel13: TsPanel
          Left = 450
          Top = 2
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44552#50529#51221#48372
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object edt_DECDCUR: TsEdit
          Left = 552
          Top = 44
          Width = 35
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 3
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109' '#44048#50529#48516
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object curr_DECDAMT: TsCurrencyEdit
          Left = 588
          Top = 44
          Width = 135
          Height = 21
          AutoSize = False
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 18
          SkinData.SkinSection = 'EDIT'
        end
        object edt_NWCDCUR: TsEdit
          Left = 552
          Top = 65
          Width = 35
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 3
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#44221#54980' '#52572#51333#44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object curr_NWCDAMT: TsCurrencyEdit
          Left = 588
          Top = 65
          Width = 135
          Height = 21
          AutoSize = False
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 20
          SkinData.SkinSection = 'EDIT'
        end
        object curr_PERP: TsCurrencyEdit
          Left = 552
          Top = 86
          Width = 35
          Height = 21
          AutoSize = False
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 21
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44284#48512#51313' '#54728#50857#50984
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object curr_PERM: TsCurrencyEdit
          Left = 610
          Top = 86
          Width = 35
          Height = 21
          AutoSize = False
          TabOrder = 22
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '/ '
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -13
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CDMAX: TsEdit
          Left = 552
          Top = 107
          Width = 171
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 11
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#45824' '#49888#50857#51109' '#44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AACV2: TsEdit
          Left = 552
          Top = 195
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AACV1: TsEdit
          Left = 552
          Top = 174
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48512#44032#44552#50529#48512#45812
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AACV3: TsEdit
          Left = 552
          Top = 216
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AACV4: TsEdit
          Left = 552
          Top = 237
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel14: TsPanel
          Left = 450
          Top = 153
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #48512#44032#44552#50529#48512#45812'(Additional Amount Covered)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 28
        end
        object sPanel21: TsPanel
          Left = 20
          Top = 305
          Width = 800
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44592#53440#51221#48372'('#48512#44032#51221#48372')'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
        end
        object memo_ADDINFO1: TsMemo
          Left = 20
          Top = 326
          Width = 800
          Height = 101
          ScrollBars = ssVertical
          TabOrder = 30
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel12: TsPanel
          Left = 20
          Top = 433
          Width = 802
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44592#53440' '#51312#44148#48320#44221#49324#54637'(Narrative)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 31
        end
        object memo_NARRAT1: TsMemo
          Left = 20
          Top = 454
          Width = 800
          Height = 101
          ScrollBars = ssVertical
          TabOrder = 32
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #47928#49436#44277#53685'2'
      object sSplitter7: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel6: TsPanel
        Left = 0
        Top = 2
        Width = 270
        Height = 560
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        
        TabOrder = 0
      end
      object sPanel7: TsPanel
        Left = 270
        Top = 2
        Width = 836
        Height = 560
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 1
        DesignSize = (
          836
          560)
        object sSpeedButton6: TsSpeedButton
          Left = 415
          Top = -31
          Width = 10
          Height = 591
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sLabel1: TsLabel
          Left = 71
          Top = 471
          Width = 105
          Height = 13
          Caption = '(Sender(s) Reference)'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel2: TsLabel
          Left = 61
          Top = 499
          Width = 112
          Height = 13
          Caption = '(Receiver(s) Reference)'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel3: TsLabel
          Left = 43
          Top = 528
          Width = 130
          Height = 13
          Caption = '(Issuing Bank'#39's Reference)'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sPanel19: TsPanel
          Left = 20
          Top = 2
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #51204#47928#48156#49888#51008#54665
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edt_APBANK: TsEdit
          Left = 122
          Top = 23
          Width = 83
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 11
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 1
          Text = '0123456789'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#47928#48156#49888#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APBANK1: TsEdit
          Left = 122
          Top = 44
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APBANK2: TsEdit
          Left = 122
          Top = 65
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APBANK3: TsEdit
          Left = 122
          Top = 86
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APBANK4: TsEdit
          Left = 122
          Top = 107
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel15: TsPanel
          Left = 20
          Top = 132
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #51204#47928#49688#49888#51008#54665
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object edt_ADBANK: TsEdit
          Left = 122
          Top = 153
          Width = 83
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 11
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 7
          Text = '0123456789'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#47928#49688#49888#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_ADBANK1: TsEdit
          Left = 122
          Top = 174
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ADBANK2: TsEdit
          Left = 122
          Top = 195
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ADBANK3: TsEdit
          Left = 122
          Top = 216
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ADBANK4: TsEdit
          Left = 122
          Top = 237
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel16: TsPanel
          Left = 20
          Top = 434
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #52280#51312#49324#54637'(Reference)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 12
        end
        object edt_CDNO: TsEdit
          Left = 179
          Top = 455
          Width = 211
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#49888#51008#54665' '#52280#51312#49324#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_RCVREF: TsEdit
          Left = 179
          Top = 484
          Width = 211
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#49888#51008#54665' '#52280#51312#49324#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_IBANKREF: TsEdit
          Left = 179
          Top = 513
          Width = 211
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51008#54665' '#52280#51312#49324#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel17: TsPanel
          Left = 20
          Top = 283
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44060#49444#51008#54665'(Issuing Bank)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object edt_ISSBANK: TsEdit
          Left = 122
          Top = 304
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 11
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 17
          Text = '0123456789'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_ISSBANK1: TsEdit
          Left = 122
          Top = 325
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_ISSBANK2: TsEdit
          Left = 122
          Top = 346
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ISSBANK3: TsEdit
          Left = 122
          Top = 367
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ISSBANK4: TsEdit
          Left = 122
          Top = 388
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ISSACCNT: TsEdit
          Left = 122
          Top = 409
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel20: TsPanel
          Left = 450
          Top = 2
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #49688#49888#50526#51008#54665#51221#48372'(Sender to Receiver Information)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 23
        end
        object edt_SRINFO1: TsEdit
          Left = 552
          Top = 23
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SRINFO2: TsEdit
          Left = 552
          Top = 44
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SRINFO3: TsEdit
          Left = 552
          Top = 65
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SRINFO4: TsEdit
          Left = 552
          Top = 86
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SRINFO5: TsEdit
          Left = 552
          Top = 107
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SRINFO6: TsEdit
          Left = 552
          Top = 128
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel22: TsPanel
          Left = 450
          Top = 151
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #53685#51648#51008#54665
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 30
        end
        object edt_EXNAME1: TsEdit
          Left = 552
          Top = 172
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 11
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 31
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47749#51032#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_EXNAME2: TsEdit
          Left = 552
          Top = 193
          Width = 251
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_EXNAME3: TsEdit
          Left = 552
          Top = 214
          Width = 251
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 33
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_EXADDR1: TsEdit
          Left = 552
          Top = 235
          Width = 251
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_EXADDR2: TsEdit
          Left = 552
          Top = 256
          Width = 251
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel8: TsPanel
          Left = 450
          Top = 279
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #49440#51201#51221#48372
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 36
        end
        object edt_LOADON: TsEdit
          Left = 552
          Top = 300
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#53441'('#48156#49569')'#51648
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_FORTRAN: TsEdit
          Left = 552
          Top = 321
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 38
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#51333#46020#52265#51648
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_SUNJUCKPORT: TsEdit
          Left = 552
          Top = 342
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 39
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DOCHACKPORT: TsEdit
          Left = 552
          Top = 363
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #46020#52265#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object mask_LSTDATE: TsMaskEdit
          Left = 552
          Top = 384
          Width = 83
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 41
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#51333#49440#51201#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel35: TsPanel
          Left = 450
          Top = 408
          Width = 370
          Height = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #49440#51201#44592#44036'(Shipment Period)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 42
        end
        object edt_SHIPPD1: TsEdit
          Left = 534
          Top = 429
          Width = 283
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 43
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SHIPPD2: TsEdit
          Left = 534
          Top = 450
          Width = 283
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 44
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SHIPPD3: TsEdit
          Left = 534
          Top = 471
          Width = 283
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 45
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SHIPPD4: TsEdit
          Left = 534
          Top = 492
          Width = 283
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 46
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SHIPPD5: TsEdit
          Left = 534
          Top = 513
          Width = 283
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 47
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SHIPPD6: TsEdit
          Left = 534
          Top = 534
          Width = 283
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 48
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ADBANK5: TsEdit
          Left = 122
          Top = 258
          Width = 268
          Height = 21
          Color = clWhite
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 49
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object sTabSheet9: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter2: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 2
        Width = 1106
        Height = 33
        SkinData.SkinSection = 'PANEL'
        Align = alTop
        
        TabOrder = 0
        object edt_SearchText: TsEdit
          Left = 103
          Top = 5
          Width = 233
          Height = 21
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 101
          Height = 23
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 17
          ItemIndex = 0
          ParentCtl3D = False
          TabOrder = 4
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49888#50857#51109#48264#54840)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 102
          Top = 5
          Width = 82
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn1: TsBitBtn
          Left = 336
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          OnClick = sBitBtn1Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn21: TsBitBtn
          Left = 183
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 230
          Top = 5
          Width = 82
          Height = 21
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn23: TsBitBtn
          Tag = 1
          Left = 312
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel25: TsPanel
          Left = 206
          Top = 5
          Width = 25
          Height = 23
          SkinData.SkinSection = 'PANEL'
          Caption = '~'
          
          TabOrder = 7
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 35
        Width = 1106
        Height = 527
        TabStop = False
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid8DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 220
            Visible = True
          end
          item
            Alignment = taLeftJustify
            Expanded = False
            FieldName = 'AMD_NO1'
            Title.Alignment = taCenter
            Title.Caption = #51312#44148#48320#44221#54943#49688
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CD_NO'
            Title.Alignment = taCenter
            Title.Caption = #49888#50857#51109#48264#54840
            Width = 150
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ISS_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'EX_DATE'
            Title.Alignment = taCenter
            Title.Caption = #50976#54952#44592#51068
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMD_DATE'
            Title.Alignment = taCenter
            Title.Caption = #48320#44221#51068#51088
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NWCD_AMT'
            Title.Alignment = taCenter
            Title.Caption = #52572#51333#44552#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NWCD_CUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 65
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMD_NO'
            Title.Alignment = taCenter
            Title.Caption = #49692#48264
            Width = 50
            Visible = True
          end>
      end
    end
  end
  object sPanel44: TsPanel [5]
    Left = 4
    Top = 118
    Width = 270
    Height = 705
    SkinData.SkinSection = 'PANEL'
    
    TabOrder = 3
    object sPanel57: TsPanel
      Left = 1
      Top = 1
      Width = 268
      Height = 33
      SkinData.SkinSection = 'PANEL'
      Align = alTop
      BevelOuter = bvNone
      
      TabOrder = 0
      object Mask_fromDate: TsMaskEdit
        Left = 94
        Top = 5
        Width = 72
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20160101'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn60: TsBitBtn
        Left = 240
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = sBitBtn60Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object mask_toDate: TsMaskEdit
        Left = 167
        Top = 5
        Width = 73
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20171231'
        CheckOnExit = True
        SkinData.SkinSection = 'EDIT'
      end
    end
    object sDBGrid8: TsDBGrid
      Left = 1
      Top = 34
      Width = 268
      Height = 670
      TabStop = False
      Align = alClient
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #44404#47548#52404
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid8DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 71
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 140
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'AMD_NO'
          Title.Alignment = taCenter
          Title.Caption = #52264#49688
          Width = 30
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 192
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    AfterOpen = qryListAfterScroll
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      
        #9'ADV707.MAINT_NO, ADV707.AMD_NO, AMD_NO1, CHK1, CHK2, CHK3, MESS' +
        'AGE1, MESSAGE2, USER_ID, DATEE, AMD_DATE, APP_DATE, APP_NO, AP_B' +
        'ANK, AP_BANK1, AP_BANK2, AP_BANK3, AP_BANK4, AP_BANK5, AD_BANK, ' +
        'AD_BANK1, AD_BANK2, AD_BANK3, AD_BANK4, AD_BANK5, ADDINFO, ADDIN' +
        'FO_1, CD_NO, RCV_REF, ISS_BANK, ISS_BANK1, '
      
        #9'ISS_BANK2, ISS_BANK3, ISS_BANK4, ISS_BANK5, ISS_ACCNT, ISS_DATE' +
        ', EX_DATE, EX_PLACE, BENEFC1, BENEFC2, BENEFC3, BENEFC4, BENEFC5' +
        ', INCD_CUR, INCD_AMT, DECD_CUR, DECD_AMT, NWCD_CUR, NWCD_AMT, CD' +
        '_PERP, CD_PERM, CD_MAX, AA_CV1, AA_CV2, AA_CV3, AA_CV4, BENEFC6,' +
        ' IBANK_REF, PRNO'
      ''
      
        #9',ADV7072.MAINT_NO, ADV7072.AMD_NO, LOAD_ON, FOR_TRAN, LST_DATE,' +
        ' SHIP_PD, SHIP_PD1, SHIP_PD2, SHIP_PD3, SHIP_PD4, SHIP_PD5, SHIP' +
        '_PD6, NARRAT, NARRAT_1, SR_INFO1, SR_INFO2, SR_INFO3, SR_INFO4, ' +
        'SR_INFO5, SR_INFO6, EX_NAME1, EX_NAME2, EX_NAME3, EX_ADDR1, EX_A' +
        'DDR2, BFCD_AMT, BFCD_CUR, SUNJUCK_PORT, DOCHACK_PORT'
      ''
      'FROM ADV707 AS ADV707'
      
        'INNER JOIN ADV7072 AS ADV7072 ON ADV707.MAINT_NO = ADV7072.MAINT' +
        '_NO AND ADV707.AMD_NO = ADV7072.AMD_NO')
    Left = 16
    Top = 232
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryListAMD_NO1: TIntegerField
      FieldName = 'AMD_NO1'
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_NO: TStringField
      FieldName = 'APP_NO'
      Size = 35
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_BANK5: TStringField
      FieldName = 'AD_BANK5'
      Size = 35
    end
    object qryListADDINFO: TStringField
      FieldName = 'ADDINFO'
      Size = 1
    end
    object qryListADDINFO_1: TMemoField
      FieldName = 'ADDINFO_1'
      BlobType = ftMemo
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListRCV_REF: TStringField
      FieldName = 'RCV_REF'
      Size = 35
    end
    object qryListISS_BANK: TStringField
      FieldName = 'ISS_BANK'
      Size = 11
    end
    object qryListISS_BANK1: TStringField
      FieldName = 'ISS_BANK1'
      Size = 35
    end
    object qryListISS_BANK2: TStringField
      FieldName = 'ISS_BANK2'
      Size = 35
    end
    object qryListISS_BANK3: TStringField
      FieldName = 'ISS_BANK3'
      Size = 35
    end
    object qryListISS_BANK4: TStringField
      FieldName = 'ISS_BANK4'
      Size = 35
    end
    object qryListISS_BANK5: TStringField
      FieldName = 'ISS_BANK5'
      Size = 35
    end
    object qryListISS_ACCNT: TStringField
      FieldName = 'ISS_ACCNT'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListINCD_CUR: TStringField
      FieldName = 'INCD_CUR'
      Size = 3
    end
    object qryListINCD_AMT: TBCDField
      FieldName = 'INCD_AMT'
      Precision = 18
    end
    object qryListDECD_CUR: TStringField
      FieldName = 'DECD_CUR'
      Size = 3
    end
    object qryListDECD_AMT: TBCDField
      FieldName = 'DECD_AMT'
      Precision = 18
    end
    object qryListNWCD_CUR: TStringField
      FieldName = 'NWCD_CUR'
      Size = 3
    end
    object qryListNWCD_AMT: TBCDField
      FieldName = 'NWCD_AMT'
      Precision = 18
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 70
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListBENEFC6: TStringField
      FieldName = 'BENEFC6'
      Size = 35
    end
    object qryListIBANK_REF: TStringField
      FieldName = 'IBANK_REF'
      Size = 35
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListAMD_NO_1: TIntegerField
      FieldName = 'AMD_NO_1'
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListSHIP_PD: TStringField
      FieldName = 'SHIP_PD'
      Size = 1
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListNARRAT: TBooleanField
      FieldName = 'NARRAT'
    end
    object qryListNARRAT_1: TMemoField
      FieldName = 'NARRAT_1'
      BlobType = ftMemo
    end
    object qryListSR_INFO1: TStringField
      FieldName = 'SR_INFO1'
      Size = 35
    end
    object qryListSR_INFO2: TStringField
      FieldName = 'SR_INFO2'
      Size = 35
    end
    object qryListSR_INFO3: TStringField
      FieldName = 'SR_INFO3'
      Size = 35
    end
    object qryListSR_INFO4: TStringField
      FieldName = 'SR_INFO4'
      Size = 35
    end
    object qryListSR_INFO5: TStringField
      FieldName = 'SR_INFO5'
      Size = 35
    end
    object qryListSR_INFO6: TStringField
      FieldName = 'SR_INFO6'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListBFCD_AMT: TBCDField
      FieldName = 'BFCD_AMT'
      Precision = 18
    end
    object qryListBFCD_CUR: TStringField
      FieldName = 'BFCD_CUR'
      Size = 3
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 232
  end
end
