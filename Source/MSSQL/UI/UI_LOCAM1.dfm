inherited UI_LOCAM1_frm: TUI_LOCAM1_frm
  Left = 457
  Top = 161
  Caption = ''
  ClientHeight = 679
  ClientWidth = 1106
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  inherited sSplitter1: TsSplitter
    Width = 1106
  end
  inherited sSplitter4: TsSplitter
    Width = 1106
  end
  inherited sPanel1: TsPanel
    Width = 1106
    inherited sSpeedButton2: TsSpeedButton
      Left = 455
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 178
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 731
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 701
      Visible = False
    end
    object sLabel59: TsLabel [4]
      Left = 8
      Top = 5
      Width = 166
      Height = 17
      Caption = #45236#44397#49888#50857#51109' '#51312#44148#48320#44221' '#53685#51648#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel [5]
      Left = 8
      Top = 20
      Width = 47
      Height = 13
      Caption = 'LOCAM1'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    inherited btnExit: TsButton
      Left = 1025
      OnClick = btnExitClick
    end
    inherited btnNew: TsButton
      Left = 779
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 847
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 188
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      Left = 915
      Visible = False
    end
    inherited btnPrint: TsButton
      Left = 844
      Visible = False
    end
    inherited btnTemp: TsButton
      Left = 625
      Visible = False
    end
    inherited btnSave: TsButton
      Left = 693
      Visible = False
    end
    inherited btnCancel: TsButton
      Left = 761
      Visible = False
    end
    inherited sButton1: TsButton
      Left = 927
      Visible = False
    end
    inherited sButton3: TsButton
      Visible = False
    end
    object sButton4: TsButton
      Left = 255
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 11
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
      Reflected = True
    end
    object sButton2: TsButton
      Tag = 1
      Left = 354
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 12
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
      Reflected = True
    end
  end
  inherited sPageControl1: TsPageControl
    Width = 1106
    Height = 599
    OnChange = sPageControl1Change
    inherited sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      inherited sSplitter3: TsSplitter
        Width = 1098
      end
      inherited sPanel2: TsPanel
        Width = 1098
        Height = 557
        DesignSize = (
          1098
          557)
        inherited sSpeedButton1: TsSpeedButton
          Left = 618
          Height = 682
        end
        inherited sSpeedButton6: TsSpeedButton
          Left = 256
          Top = 0
          Height = 689
        end
        inherited sLabel1: TsLabel
          Left = 401
          Top = 65
        end
        inherited edt_BNFADDR2: TsEdit [3]
          Left = 385
          Top = 320
          Height = 19
          Font.Height = -11
        end
        inherited edt_BNFADDR3: TsEdit [4]
          Left = 385
          Top = 339
          Height = 19
          Font.Height = -11
        end
        inherited edt_BNFMAILID: TsEdit [5]
          Left = 385
          Top = 358
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited sPanel8: TsPanel [6]
          Left = 466
          Top = 358
          Height = 19
        end
        inherited edt_BNFDOMAIN: TsEdit [7]
          Left = 487
          Top = 358
          Width = 121
          Height = 19
          Font.Height = -11
        end
        inherited edt_BNFADDR1: TsEdit [8]
          Left = 385
          Top = 301
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_BENEFC2: TsEdit [9]
          Left = 385
          Top = 263
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_BENEFC1: TsEdit [10]
          Left = 385
          Top = 244
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited sPanel6: TsPanel
          Left = 263
          Top = 223
          Width = 345
        end
        inherited edt_APPADDR3: TsEdit [12]
          Left = 385
          Top = 200
          Height = 19
          Font.Height = -11
        end
        inherited edt_APPADDR2: TsEdit [13]
          Left = 385
          Top = 181
          Height = 19
          Font.Height = -11
        end
        inherited edt_APPADDR1: TsEdit [14]
          Left = 385
          Top = 162
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_APPLIC2: TsEdit [15]
          Left = 385
          Top = 124
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_APPLIC1: TsEdit [16]
          Left = 385
          Top = 105
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        object sDBGrid2: TsDBGrid [17]
          Left = 1
          Top = 28
          Width = 255
          Height = 683
          Color = clWhite
          Ctl3D = False
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 60
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnDrawColumnCell = sDBGrid1DrawColumnCell
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATEE'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MAINT_NO'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 142
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'MSEQ'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = #52264#49688
              Width = 30
              Visible = True
            end>
        end
        inherited sPanel5: TsPanel [18]
          Left = 263
          Top = 84
          Width = 345
        end
        inherited edt_LOC_TYPE: TsEdit [19]
          Left = 385
          Top = 535
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited sEdit21: TsEdit [20]
          Left = 419
          Top = 535
          Width = 189
          Height = 19
          Font.Height = -11
        end
        inherited edt_LOC_AMTC: TsEdit [21]
          Left = 385
          Top = 440
          Height = 19
          Font.Height = -11
          BoundLabel.Caption = #48320#44221' '#54980' '#44552#50529'('#50808#54868')'
          BoundLabel.Font.Height = -11
        end
        inherited edt_LOC_AMT: TsCurrencyEdit [22]
          Left = 419
          Top = 440
          Width = 189
          Height = 19
          Font.Height = -11
        end
        inherited edt_ISSBANK2: TsEdit [23]
          Left = 385
          Top = 421
          Height = 19
          Font.Height = -11
        end
        inherited edt_ISSBANK1: TsEdit [24]
          Left = 434
          Top = 401
          Width = 174
          Height = 19
          Font.Height = -11
        end
        inherited edt_ISSBANK: TsEdit [25]
          Left = 385
          Top = 402
          Width = 48
          Height = 19
          Color = clWhite
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_LC_NO: TsEdit [26]
          Left = 385
          Top = 497
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_CD_PERM: TsEdit [27]
          Left = 437
          Top = 478
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_CD_PERP: TsEdit [28]
          Left = 385
          Top = 478
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_CHKNAME2: TsEdit [29]
          Left = 504
          Top = 327
          Visible = False
          BoundLabel.Caption = ''
        end
        inherited edt_CHKNAME1: TsEdit [30]
          Left = 480
          Top = 343
          Visible = False
          BoundLabel.Caption = ''
        end
        inherited sBitBtn5: TsBitBtn [31]
          Left = 538
          Top = 291
          Visible = False
        end
        inherited edt_BENEFC: TsEdit [32]
          Left = 432
          Top = 291
          Visible = False
          BoundLabel.Caption = ''
        end
        inherited edt_APPLIC: TsEdit [33]
          Left = 448
          Top = 111
          Height = 19
          Visible = False
          BoundLabel.Caption = ''
        end
        inherited sBitBtn7: TsBitBtn [34]
          Left = 457
          Top = 443
          Visible = False
        end
        inherited sBitBtn8: TsBitBtn [35]
          Left = 465
          Top = 455
          Visible = False
        end
        inherited sBitBtn4: TsBitBtn [36]
          Left = 514
          Top = 111
          Height = 19
          Visible = False
        end
        inherited sBitBtn6: TsBitBtn
          Left = 417
          Top = 403
          Visible = False
        end
        inherited sPanel7: TsPanel
          Left = 263
          Top = 381
          Width = 345
        end
        inherited sPanel9: TsPanel
          Left = 638
          Top = 65
          Width = 345
        end
        inherited edt_OFFERNO1: TsEdit
          Left = 790
          Top = 86
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_OFFERNO2: TsEdit
          Left = 790
          Top = 105
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO3: TsEdit
          Left = 790
          Top = 124
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO4: TsEdit
          Left = 790
          Top = 143
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO5: TsEdit
          Left = 790
          Top = 162
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO6: TsEdit
          Left = 790
          Top = 181
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO7: TsEdit
          Left = 790
          Top = 200
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO8: TsEdit
          Left = 790
          Top = 219
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO9: TsEdit
          Left = 790
          Top = 238
          Height = 19
          Font.Height = -11
        end
        inherited edt_AMD_NO: TsEdit
          Left = 367
          Top = 61
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited mask_ISS_DATE: TsMaskEdit
          Left = 367
          Top = 23
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited mask_APP_DATE: TsMaskEdit
          Left = 367
          Top = 42
          Height = 19
          Font.Height = -11
          BoundLabel.Caption = #51312#44148#48320#44221#51068#51088
          BoundLabel.Font.Height = -11
        end
        inherited mask_DELIVERY: TsMaskEdit
          Left = 790
          Top = 23
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited mask_EXPIRY: TsMaskEdit
          Left = 790
          Top = 42
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited sPanel10: TsPanel
          Left = 263
          Top = 2
          Width = 345
        end
        inherited memo_REMARK1: TsMemo
          Left = 638
          Top = 282
          Width = 446
          Height = 63
          Font.Name = #44404#47548#52404
          BoundLabel.Active = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.UseSkinColor = True
        end
        inherited memo_CHGINFO: TsMemo
          Left = 638
          Top = 370
          Width = 446
          Height = 73
          Font.Name = #44404#47548#52404
          BoundLabel.Active = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.UseSkinColor = True
        end
        inherited sPanel17: TsPanel
          Left = 638
          Top = 447
          Width = 345
        end
        inherited edt_EXNAME1: TsEdit
          Left = 793
          Top = 468
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_EXNAME2: TsEdit
          Left = 793
          Top = 487
          Height = 19
          Font.Height = -11
        end
        inherited edt_EXNAME3: TsEdit
          Left = 793
          Top = 506
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_APPLIC3: TsEdit
          Left = 385
          Top = 143
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_BENEFC3: TsEdit
          Left = 385
          Top = 282
          Height = 19
          Font.Height = -11
          BoundLabel.Caption = ''
          BoundLabel.Font.Height = -11
        end
        inherited sPanel12: TsPanel
          Left = 638
          Top = 2
          Width = 345
        end
        object edt_LOC2AMTC: TsEdit
          Tag = 102
          Left = 385
          Top = 459
          Width = 33
          Height = 19
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 61
          OnChange = edt_LOC_TYPEChange
          OnDblClick = edt_LOC_TYPEDblClick
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.Caption = #48320#44221' '#54980' '#44552#50529'('#50896#54868')'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object edt_LOC2AMT: TsCurrencyEdit
          Left = 419
          Top = 459
          Width = 189
          Height = 19
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 62
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Blend = 0
          GlyphMode.Grayed = False
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object edt_EXRATE: TsCurrencyEdit
          Left = 385
          Top = 516
          Width = 92
          Height = 19
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 63
          BoundLabel.Active = True
          BoundLabel.Caption = #47588#47588#44592#51456#50984
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Blend = 0
          GlyphMode.Grayed = False
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object sPanel53: TsPanel
          Left = 1
          Top = 1
          Width = 256
          Height = 27
          BevelOuter = bvNone
          TabOrder = 64
          SkinData.SkinSection = 'PANEL'
          object Mask_fromDate: TsMaskEdit
            Left = 81
            Top = 2
            Width = 74
            Height = 23
            Color = clWhite
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object sBitBtn22: TsBitBtn
            Left = 229
            Top = 2
            Width = 25
            Height = 23
            Cursor = crHandPoint
            TabOrder = 2
            OnClick = sBitBtn22Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 6
            Images = DMICON.System18
          end
          object Mask_toDate: TsMaskEdit
            Left = 155
            Top = 2
            Width = 74
            Height = 23
            Color = clWhite
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51312#54924
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
          end
        end
        object sPanel11: TsPanel
          Left = 638
          Top = 261
          Width = 345
          Height = 20
          Caption = #44592#53440#51221#48372
          Color = 16042877
          TabOrder = 65
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sPanel14: TsPanel
          Left = 638
          Top = 349
          Width = 345
          Height = 20
          Caption = #44592#53440#51312#44148' '#48320#46041#49324#54637
          Color = 16042877
          TabOrder = 66
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sSplitter2: TsSplitter
        Width = 1098
      end
      inherited sPanel3: TsPanel
        Width = 1098
        inherited edt_SearchText: TsEdit
          Left = 106
        end
        inherited com_SearchKeyword: TsComboBox
          Width = 101
          TabOrder = 4
          TabStop = False
          Text = #48320#44221#49888#52397#51068#51088
          Items.Strings = (
            #48320#44221#49888#52397#51068#51088
            #49888#50857#51109#48264#54840
            #44060#49444#51008#54665)
        end
        inherited Mask_SearchDate1: TsMaskEdit
          Left = 106
          Height = 23
          TabOrder = 0
        end
        inherited sBitBtn1: TsBitBtn
          Left = 343
          OnClick = sBitBtn1Click
        end
        inherited sBitBtn21: TsBitBtn
          Left = 188
          TabOrder = 5
          TabStop = False
        end
        inherited Mask_SearchDate2: TsMaskEdit
          Left = 237
          Height = 23
          TabOrder = 1
        end
        inherited sBitBtn23: TsBitBtn
          Left = 319
          TabStop = False
        end
        inherited sPanel25: TsPanel
          Left = 212
        end
      end
      inherited sDBGrid1: TsDBGrid
        Width = 1098
        Height = 525
        DataSource = dsList
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 170
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'MSEQ'
            Title.Alignment = taCenter
            Title.Caption = #52264#49688
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'bgm_ref'
            Title.Alignment = taCenter
            Title.Caption = #51008#54665#49888#52397#48264#54840
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'APPLIC1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51088
            Width = 220
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 220
            Visible = True
          end
          item
            Color = 13041663
            Expanded = False
            FieldName = 'LC_NO'
            Title.Alignment = taCenter
            Title.Caption = #49888#50857#51109#48264#54840
            Width = 210
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMD_DATE'
            Title.Alignment = taCenter
            Title.Caption = #48320#44221#49888#52397#51068
            Width = 95
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ISSBANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 225
            Visible = True
          end>
      end
    end
  end
  inherited sPanel4: TsPanel
    Width = 1106
    inherited edt_UserNo: TsEdit
      Left = 544
    end
    inherited btn_Cal: TsBitBtn
      Visible = False
    end
    inherited edt_msg1: TsEdit
      Left = 662
    end
    inherited sBitBtn25: TsBitBtn
      Left = 695
      Visible = False
    end
    inherited edt_msg2: TsEdit
      Left = 746
    end
    inherited sBitBtn26: TsBitBtn
      Left = 779
      Visible = False
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 336
  end
  inherited qryList: TADOQuery
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      'SELECT '
      
        #9'MAINT_NO, MSEQ, USER_ID, DATEE, MESSAGE1, MESSAGE2, RFF_NO, LC_' +
        'NO, OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OFFERNO6, ' +
        'OFFERNO7, OFFERNO8, OFFERNO9, AMD_DATE, ADV_DATE, ISS_DATE, REMA' +
        'RK, REMARK1, ISSBANK, ISSBANK1, ISSBANK2, APPLIC1, APPLIC2, APPL' +
        'IC3, BENEFC1, BENEFC2, BENEFC3, EXNAME1, EXNAME2, EXNAME3, AMD_N' +
        'O, DELIVERY, EXPIRY, CHGINFO, CHGINFO1, LOC_TYPE, LOC1AMT, LOC1A' +
        'MTC, LOC2AMT, LOC2AMTC, EX_RATE, bgm_ref, CHK1, CHK2, CHK3, PRNO' +
        ', APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADDR3, BN' +
        'FEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      #9',ISNULL(N4487.DOC_NAME,'#39#39') as LOC_TYPENAME'
      
        'FROM LOCAM1 with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON L' +
        'OCAM1.LOC_TYPE = N4487.CODE')
    Top = 368
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListAMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListISSBANK: TStringField
      FieldName = 'ISSBANK'
      Size = 4
    end
    object qryListISSBANK1: TStringField
      FieldName = 'ISSBANK1'
      Size = 35
    end
    object qryListISSBANK2: TStringField
      FieldName = 'ISSBANK2'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListAMD_NO: TBCDField
      FieldName = 'AMD_NO'
      Precision = 18
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListCHGINFO: TStringField
      FieldName = 'CHGINFO'
      Size = 1
    end
    object qryListCHGINFO1: TMemoField
      FieldName = 'CHGINFO1'
      BlobType = ftMemo
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qryListLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object qryListLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qryListLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object qryListEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryListbgm_ref: TStringField
      FieldName = 'bgm_ref'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      ReadOnly = True
      Size = 100
    end
  end
  inherited dsList: TDataSource
    Top = 368
  end
end
