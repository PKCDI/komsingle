inherited UI_DOANTC_frm: TUI_DOANTC_frm
  Left = 19
  Top = 137
  Caption = #49440#51201#49436#47448#46020#52265#53685#48372#49436
  ClientWidth = 1114
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 77
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter2: TsSplitter [1]
    Left = 0
    Top = 41
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel1: TsPanel [2]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      1114
      41)
    object sSpeedButton2: TsSpeedButton
      Left = 129
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 648
      Top = 3
      Width = 8
      Height = 35
      Visible = False
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton4: TsSpeedButton
      Left = 595
      Top = 3
      Width = 8
      Height = 35
      Visible = False
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 406
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel
      Left = 8
      Top = 5
      Width = 117
      Height = 17
      Caption = #49440#51201#49436#47448#46020#52265#53685#48372#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel
      Left = 8
      Top = 20
      Width = 47
      Height = 13
      Caption = 'DOANTC'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object btnExit: TsButton
      Left = 1035
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 779
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = False
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Left = 847
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = False
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Left = 139
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 670
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      Visible = False
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 788
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      Visible = False
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 569
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      Visible = False
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 637
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      Visible = False
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Left = 713
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      Visible = False
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
    object btnReady: TsButton
      Left = 607
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      Visible = False
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
    end
    object btnSend: TsButton
      Left = 703
      Top = 2
      Width = 74
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      Visible = False
      SkinData.SkinSection = 'BUTTON'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
    object sButton4: TsButton
      Left = 206
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton2: TsButton
      Tag = 1
      Left = 305
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [3]
    Left = 0
    Top = 44
    Width = 1114
    Height = 33
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 1
    object mask_DATEE: TsMaskEdit
      Left = 406
      Top = 5
      Width = 83
      Height = 23
      TabStop = False
      AutoSize = False
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_USER_ID: TsEdit
      Left = 536
      Top = 5
      Width = 57
      Height = 23
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_msg1: TsEdit
      Tag = 201
      Left = 657
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      TabStop = False
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_msg2: TsEdit
      Tag = 202
      Left = 729
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      TabStop = False
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_MAINT_NO: TsEdit
      Left = 64
      Top = 5
      Width = 281
      Height = 23
      TabStop = False
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
  end
  object Page_control: TsPageControl [4]
    Left = 0
    Top = 80
    Width = 1114
    Height = 601
    ActivePage = sTabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 0
    TabOrder = 2
    TabStop = False
    TabWidth = 100
    OnChange = Page_controlChange
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter4: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page1_RIGHT: TsPanel
        Left = 255
        Top = 2
        Width = 851
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 0
        object sPanel3: TsPanel
          Left = 1
          Top = 1
          Width = 849
          Height = 128
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 0
          TabStop = True
          object sPanel6: TsPanel
            Left = 0
            Top = 0
            Width = 849
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Align = alTop
            BevelOuter = bvNone
            Caption = #53685#51648#51008#54665
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 1
          end
          object edt_BANKCODE: TsEdit
            Tag = 203
            Left = 154
            Top = 39
            Width = 93
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #53685#51648#51008#54665
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object mask_RESDATE: TsMaskEdit
            Left = 562
            Top = 39
            Width = 93
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 2
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #53685#51648#51068#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
          end
          object edt_BANK1: TsEdit
            Tag = 203
            Left = 154
            Top = 63
            Width = 233
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
          end
          object edt_BANK2: TsEdit
            Tag = 203
            Left = 154
            Top = 87
            Width = 233
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
          end
          object mask_SETDATE: TsMaskEdit
            Left = 562
            Top = 63
            Width = 93
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 5
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52572#51333#44208#51228#51068
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
          end
        end
        object sPanel7: TsPanel
          Left = 1
          Top = 257
          Width = 849
          Height = 152
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 1
          TabStop = True
          DesignSize = (
            849
            152)
          object sSpeedButton1: TsSpeedButton
            Left = 425
            Top = -1
            Width = 10
            Height = 152
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'SPEEDBUTTON'
          end
          object sPanel8: TsPanel
            Left = 0
            Top = 0
            Width = 433
            Height = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49688#49888#50629#52404'('#44144#47000#44256#44061')'
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 0
          end
          object sPanel11: TsPanel
            Left = 424
            Top = 0
            Width = 425
            Height = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #48156#49888#44592#44288' '#48143' '#51204#51088#49436#47749
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 1
          end
          object edt_APPCODE: TsEdit
            Tag = 203
            Left = 154
            Top = 39
            Width = 93
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#49888#50629#52404
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_APPNAME1: TsEdit
            Tag = 203
            Left = 154
            Top = 63
            Width = 233
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
          end
          object edt_APPNAME2: TsEdit
            Tag = 203
            Left = 154
            Top = 87
            Width = 233
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
          end
          object edt_APPNAME3: TsEdit
            Tag = 203
            Left = 154
            Top = 111
            Width = 233
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
          end
          object edt_BKNAME1: TsEdit
            Tag = 203
            Left = 546
            Top = 39
            Width = 233
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48156#49888#44592#44288
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_BKNAME2: TsEdit
            Tag = 203
            Left = 546
            Top = 63
            Width = 233
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 7
            SkinData.SkinSection = 'EDIT'
          end
          object edt_BKNAME3: TsEdit
            Tag = 203
            Left = 546
            Top = 87
            Width = 233
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51204#51088#49436#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
        object sPanel5: TsPanel
          Left = 1
          Top = 129
          Width = 849
          Height = 128
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 2
          TabStop = True
          object sBevel1: TsBevel
            Tag = 40
            Left = 1
            Top = 74
            Width = 848
            Height = 1
            Shape = bsBottomLine
          end
          object sPanel9: TsPanel
            Left = 0
            Top = 0
            Width = 849
            Height = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Align = alTop
            BevelOuter = bvNone
            Caption = #49888#50857#51109'('#44228#50557#49436') / '#49440#54616#51613#44428'('#54637#44277#54868#47932#50868#49569#51109')'
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 1
          end
          object edt_LCG: TsEdit
            Tag = 203
            Left = 154
            Top = 39
            Width = 41
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49888#50857#51109'('#44228#50557#49436')'#13#10#44396#48516' '#48143' '#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_LCNO: TsEdit
            Tag = 203
            Left = 196
            Top = 39
            Width = 199
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
          end
          object edt_AMTC: TsEdit
            Tag = 203
            Left = 154
            Top = 87
            Width = 41
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #50612#51020#44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_AMT: TsCurrencyEdit
            Left = 196
            Top = 87
            Width = 118
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_BLG: TsEdit
            Tag = 203
            Left = 546
            Top = 39
            Width = 41
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49440#54616#51613#44428'('#54637#44277#54868#47932#50868#49569#51109')'#13#10#44396#48516' '#48143' '#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_BLNO: TsEdit
            Tag = 203
            Left = 588
            Top = 39
            Width = 199
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
          end
          object edt_CHRGC: TsEdit
            Tag = 203
            Left = 546
            Top = 87
            Width = 41
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 7
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44592#53440' '#49688#49688#47308
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_CHRG: TsCurrencyEdit
            Left = 588
            Top = 87
            Width = 118
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
        end
        object sPanel10: TsPanel
          Left = 1
          Top = 409
          Width = 849
          Height = 192
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          
          TabOrder = 3
          TabStop = True
          object sPanel12: TsPanel
            Left = 0
            Top = 0
            Width = 849
            Height = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Align = alTop
            BevelOuter = bvNone
            Caption = #44592#53440#49324#54637
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 0
          end
          object memo_REMARK1: TsMemo
            Left = 187
            Top = 39
            Width = 488
            Height = 100
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 1
            CharCase = ecUpperCase
            BoundLabel.Active = True
            BoundLabel.UseSkinColor = False
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44592#53440#51312#44148
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = 5197647
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
          end
        end
      end
      object sPanel2: TsPanel
        Left = 0
        Top = 2
        Width = 255
        Height = 559
        SkinData.SkinSection = 'PANEL'
        Align = alLeft
        
        TabOrder = 1
      end
    end
    object sTabSheet5: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter3: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sDBGrid2: TsDBGrid
        Left = 0
        Top = 34
        Width = 1106
        Height = 527
        TabStop = False
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid8DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 85
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BANK1'
            Title.Alignment = taCenter
            Title.Caption = #53685#51648#51008#54665
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LC_NO'
            Title.Alignment = taCenter
            Title.Caption = #49888#50857#51109'('#44228#50557#49436#48264#54840')'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT'
            Title.Alignment = taCenter
            Title.Caption = #50612#51020#44552#50529
            Width = 120
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 50
            Visible = True
          end>
      end
      object sPanel21: TsPanel
        Left = 0
        Top = 2
        Width = 1106
        Height = 32
        SkinData.SkinSection = 'PANEL'
        Align = alTop
        
        TabOrder = 0
        object edt_SearchText: TsEdit
          Left = 138
          Top = 5
          Width = 237
          Height = 23
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 133
          Height = 23
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 17
          ItemIndex = 0
          ParentCtl3D = False
          TabOrder = 2
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #49888#50857#51109'('#44228#50557#49436')'#48264#54840
            #44288#47532#48264#54840)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 138
          Top = 5
          Width = 82
          Height = 23
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn22: TsBitBtn
          Left = 375
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          OnClick = sBitBtn22Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn23: TsBitBtn
          Tag = 902
          Left = 220
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = sBitBtn23Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 269
          Top = 5
          Width = 82
          Height = 23
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn28: TsBitBtn
          Tag = 903
          Left = 351
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = sBitBtn28Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel23: TsPanel
          Left = 244
          Top = 5
          Width = 25
          Height = 23
          SkinData.SkinSection = 'PANEL'
          Caption = '~'
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 7
        end
      end
    end
  end
  object sPanel56: TsPanel [5]
    Left = 1
    Top = 118
    Width = 256
    Height = 560
    SkinData.SkinSection = 'PANEL'
    
    TabOrder = 3
    object sDBGrid8: TsDBGrid
      Left = 1
      Top = 34
      Width = 254
      Height = 525
      TabStop = False
      Align = alClient
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #44404#47548#52404
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid8DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 175
          Visible = True
        end>
    end
    object sPanel57: TsPanel
      Left = 1
      Top = 1
      Width = 254
      Height = 33
      SkinData.SkinSection = 'PANEL'
      Align = alTop
      BevelOuter = bvNone
      
      TabOrder = 1
      object Mask_fromDate: TsMaskEdit
        Left = 80
        Top = 5
        Width = 72
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20160101'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn60: TsBitBtn
        Left = 226
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = sBitBtn60Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object mask_toDate: TsMaskEdit
        Left = 153
        Top = 5
        Width = 73
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20171231'
        CheckOnExit = True
        SkinData.SkinSection = 'EDIT'
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 200
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      
        'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, APP_CODE, APP_NAME' +
        '1, APP_NAME2, APP_NAME3, BANK_CODE, BANK1, BANK2, LC_G, LC_NO, B' +
        'L_G, BL_NO, AMT, AMTC, CHRG, CHRGC, RES_DATE, SET_DATE, REMARK1,' +
        ' BK_NAME1, BK_NAME2, BK_NAME3, CHK1, CHK2, CHK3, PRNO'
      'FROM DOANTC')
    Left = 24
    Top = 232
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryListAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryListAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryListBANK_CODE: TStringField
      FieldName = 'BANK_CODE'
      Size = 10
    end
    object qryListBANK1: TStringField
      FieldName = 'BANK1'
      Size = 70
    end
    object qryListBANK2: TStringField
      FieldName = 'BANK2'
      Size = 70
    end
    object qryListLC_G: TStringField
      FieldName = 'LC_G'
      Size = 3
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListBL_G: TStringField
      FieldName = 'BL_G'
      Size = 3
    end
    object qryListBL_NO: TStringField
      FieldName = 'BL_NO'
      Size = 35
    end
    object qryListAMT: TBCDField
      FieldName = 'AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListAMTC: TStringField
      FieldName = 'AMTC'
      Size = 3
    end
    object qryListCHRG: TBCDField
      FieldName = 'CHRG'
      Precision = 18
    end
    object qryListCHRGC: TStringField
      FieldName = 'CHRGC'
      Size = 3
    end
    object qryListRES_DATE: TStringField
      FieldName = 'RES_DATE'
      Size = 8
    end
    object qryListSET_DATE: TStringField
      FieldName = 'SET_DATE'
      Size = 8
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 35
    end
    object qryListBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 35
    end
    object qryListBK_NAME3: TStringField
      FieldName = 'BK_NAME3'
      Size = 35
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 232
  end
  object QRCompositeReport1: TQRCompositeReport
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = Letter
    Left = 24
    Top = 264
  end
end
