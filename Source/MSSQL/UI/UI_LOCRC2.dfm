inherited UI_LOCRC2_frm: TUI_LOCRC2_frm
  Left = 798
  Top = 82
  Caption = #45236#44397#49888#50857#51109' '#47932#54408#49688#47161#51613#47749#49436'('#49569#49888')'
  ClientHeight = 691
  ClientWidth = 1047
  Constraints.MaxHeight = 730
  Constraints.MaxWidth = 1138
  OnClose = FormClose
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 15
  inherited sSplitter1: TsSplitter
    Top = 77
    Width = 1047
  end
  object sSplitter12: TsSplitter [1]
    Left = 0
    Top = 41
    Width = 1047
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  inherited sPanel1: TsPanel
    Width = 1047
    TabOrder = 2
    DesignSize = (
      1047
      41)
    inherited sSpeedButton2: TsSpeedButton
      Left = 422
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 712
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 789
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 499
    end
    object sSpeedButton9: TsSpeedButton [4]
      Left = 209
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel [5]
      Left = 8
      Top = 5
      Width = 197
      Height = 17
      Caption = #45236#44397#49888#50857#51109' '#47932#54408#49688#47161#51613#47749#49436'('#49569#49888')'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel [6]
      Left = 8
      Top = 20
      Width = 42
      Height = 13
      Caption = 'LOCRC2'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton11: TsSpeedButton [7]
      Left = 956
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    inherited btnExit: TsButton
      Left = 966
      OnClick = btnExitClick
    end
    inherited btnNew: TsButton
      Left = 218
      OnClick = btnNewClick
    end
    inherited btnEdit: TsButton
      Left = 286
      OnClick = btnEditClick
    end
    inherited btnDel: TsButton
      Left = 354
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      Left = 431
      OnClick = btnCopyClick
    end
    inherited btnPrint: TsButton
      Left = 721
      OnClick = btnPrintClick
    end
    inherited btnTemp: TsButton
      Left = 508
      OnClick = btnSaveClick
    end
    inherited btnSave: TsButton
      Left = 576
      OnClick = btnSaveClick
    end
    inherited btnCancel: TsButton
      Left = 644
      OnClick = btnCancelClick
    end
    inherited btnReady: TsButton
      Left = 798
      Width = 89
      OnClick = btnReadyClick
    end
    inherited btnSend: TsButton
      Left = 888
      Width = 67
      OnClick = btnSendClick
    end
  end
  inherited sPageControl1: TsPageControl
    Top = 80
    Width = 1047
    Height = 611
    ActivePage = sTabSheet2
    PopupMenu = popMenu1
    OnChange = sPageControl1Change
    inherited sTabSheet1: TsTabSheet [0]
      Caption = #47928#49436#44277#53685
      inherited sSplitter3: TsSplitter
        Width = 1039
      end
      inherited sPanel2: TsPanel
        Width = 1039
        Height = 569
        inherited sSplitter4: TsSplitter
          Left = 263
          Height = 567
        end
        inherited sPanel5: TsPanel
          Width = 262
          Height = 567
        end
        inherited sPanel6: TsPanel
          Left = 266
          Width = 772
          Height = 567
          inherited sPanel9: TsPanel
            Width = 772
            Height = 439
            DesignSize = (
              772
              439)
            inherited sSpeedButton1: TsSpeedButton
              Left = 345
              Top = 9
              Height = 421
            end
            inherited edt_APPLIC3: TsEdit [1]
              Left = 102
              Top = 309
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 10
            end
            inherited edt_APPLIC2: TsEdit [2]
              Left = 102
              Top = 285
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
            end
            inherited edt_APPLIC1: TsEdit [3]
              Left = 102
              Top = 237
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 9
            end
            inherited sBitBtn2: TsBitBtn [4]
              Left = 168
              Top = 207
              Height = 21
              TabOrder = 34
              OnClick = sBitBtn5Click
            end
            inherited edt_APPLIC: TsEdit [5]
              Left = 102
              Top = 213
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 10
              OnDblClick = edt_BENEFCDblClick
            end
            inherited edt_BENEFC1: TsEdit [6]
              Left = 102
              Top = 165
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 6
            end
            inherited edt_LOC1AMTC: TsEdit [7]
              Tag = 101
              Left = 473
              Top = 203
              Width = 39
              Height = 21
              Color = clYellow
              Ctl3D = False
              OnDblClick = edt_LOC1AMTCDblClick
              SkinData.CustomColor = True
            end
            inherited edt_APPLIC4: TsEdit [8]
              Left = 102
              Top = 333
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
            end
            inherited sBitBtn8: TsBitBtn [9]
              Tag = 101
              Left = 513
              Top = 203
              TabOrder = 37
              OnClick = sBitBtn8Click
            end
            inherited curr_LOC1AMT: TsCurrencyEdit [10]
              Left = 538
              Top = 203
              Width = 158
              CharCase = ecUpperCase
              Ctl3D = False
              TabOrder = 24
            end
            inherited edt_EX_RATE: TsEdit [11]
              Left = 473
              Top = 225
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              TabOrder = 25
            end
            inherited edt_LOC2AMTC: TsEdit [12]
              Left = 473
              Top = 247
              Height = 21
              TabStop = False
              Ctl3D = False
              TabOrder = 38
            end
            inherited curr_LOC2AMT: TsCurrencyEdit [13]
              Left = 507
              Top = 247
              CharCase = ecUpperCase
              Ctl3D = False
              TabOrder = 26
            end
            inherited mask_LOADDATE: TsMaskEdit [14]
              Left = 473
              Top = 269
              CharCase = ecUpperCase
              TabOrder = 27
            end
            inherited mask_EXPDATE: TsMaskEdit [15]
              Left = 473
              Top = 291
              CharCase = ecUpperCase
              TabOrder = 28
            end
            inherited edt_AP_BANK2: TsEdit [16]
              Left = 473
              Top = 55
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 18
            end
            inherited edt_AP_BANK4: TsEdit [17]
              Left = 473
              Top = 103
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 20
            end
            inherited edt_AP_NAME: TsEdit [18]
              Left = 473
              Top = 127
              Width = 99
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 10
              TabOrder = 21
            end
            inherited edt_AP_BANK3: TsEdit [19]
              Left = 473
              Top = 79
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 19
            end
            inherited sPanel12: TsPanel [20]
              Left = 381
              Top = 158
              Width = 164
              Height = 21
              SkinData.CustomColor = True
              Color = 16042877
              TabOrder = 36
            end
            inherited edt_AP_BANK: TsEdit [21]
              Left = 473
              Top = 7
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 4
              OnDblClick = edt_AP_BANKDblClick
            end
            inherited mask_GET_DAT: TsMaskEdit [22]
              Left = 102
              Top = 15
              OnDblClick = mask_DATEEDblClick
            end
            inherited edt_BSN_HSCODE: TsEdit [23]
              Left = 194
              Top = 109
              Width = 131
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 4
            end
            inherited edt_LOC_NO: TsEdit [24]
              Left = 473
              Top = 181
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
            end
            inherited edt_AP_BANK1: TsEdit [25]
              Left = 473
              Top = 31
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 17
            end
            inherited edt_RFF_NO: TsEdit [26]
              Left = 102
              Top = 87
              Height = 21
              TabStop = False
              Ctl3D = False
              MaxLength = 35
            end
            inherited sBitBtn3: TsBitBtn [27]
              Tag = 2
              Left = 539
              Top = 7
              TabOrder = 35
              OnClick = sBitBtn3Click
            end
            inherited mask_ISS_DAT: TsMaskEdit [28]
              Left = 102
              Top = 39
              TabOrder = 1
              OnDblClick = mask_DATEEDblClick
            end
            inherited sBitBtn5: TsBitBtn [29]
              Tag = 0
              Left = 168
              Top = 141
              Height = 21
              TabOrder = 33
              OnClick = sBitBtn5Click
            end
            inherited edt_BENEFC: TsEdit [30]
              Left = 102
              Top = 141
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 10
              OnDblClick = edt_BENEFCDblClick
            end
            inherited mask_EXP_DAT: TsMaskEdit [31]
              Left = 102
              Top = 63
              TabOrder = 3
              OnDblClick = mask_DATEEDblClick
            end
            inherited edt_AMAINT_NO: TsEdit
              Left = 473
              Top = 313
              Height = 21
              TabStop = False
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 39
            end
            inherited edt_RCT_AMT1C: TsEdit
              Left = 473
              Top = 361
              Width = 39
              Height = 21
              Color = clYellow
              Ctl3D = False
              TabOrder = 29
              OnDblClick = edt_LOC1AMTCDblClick
              SkinData.CustomColor = True
            end
            inherited sBitBtn4: TsBitBtn
              Left = 513
              Top = 361
              TabOrder = 40
              OnClick = sBitBtn8Click
            end
            inherited curr_RCT_AMT1: TsCurrencyEdit
              Left = 538
              Top = 361
              Width = 158
              CharCase = ecUpperCase
              Ctl3D = False
              TabOrder = 30
            end
            inherited edt_RATE: TsEdit
              Left = 473
              Top = 383
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              TabOrder = 31
            end
            inherited edt_RCT_AMT2C: TsEdit
              Left = 473
              Top = 405
              Height = 21
              TabStop = False
              Ctl3D = False
              TabOrder = 41
            end
            inherited curr_RCT_AMT2: TsCurrencyEdit
              Left = 507
              Top = 405
              CharCase = ecUpperCase
              Ctl3D = False
              TabOrder = 32
            end
            inherited edt_APPLIC5: TsEdit
              Left = 102
              Top = 357
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 15
            end
            inherited edt_APPLIC6: TsEdit
              Left = 102
              Top = 381
              Height = 21
              CharCase = ecUpperCase
              Ctl3D = False
              MaxLength = 35
              TabOrder = 16
            end
            inherited edt_BENEFC2: TsMaskEdit
              Left = 102
              Top = 189
              Height = 23
              CharCase = ecUpperCase
              Ctl3D = False
              TabOrder = 7
            end
            inherited edt_APPLIC7: TsMaskEdit
              Left = 102
              Top = 261
              Height = 23
              CharCase = ecUpperCase
              Ctl3D = False
              TabOrder = 10
            end
            object sBitBtn16: TsBitBtn
              Left = 179
              Top = 15
              Width = 24
              Height = 23
              Cursor = crHandPoint
              TabOrder = 42
              TabStop = False
              OnClick = btn_CalClick
              ImageIndex = 9
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object sBitBtn17: TsBitBtn
              Tag = 1
              Left = 179
              Top = 39
              Width = 24
              Height = 23
              Cursor = crHandPoint
              TabOrder = 43
              TabStop = False
              OnClick = btn_CalClick
              ImageIndex = 9
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object sBitBtn18: TsBitBtn
              Tag = 2
              Left = 179
              Top = 63
              Width = 24
              Height = 23
              Cursor = crHandPoint
              TabOrder = 44
              TabStop = False
              OnClick = btn_CalClick
              ImageIndex = 9
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
          end
          inherited sPanel15: TsPanel
            Top = 439
            Width = 772
            Height = 128
            object sLabel6: TsLabel [0]
              Left = 212
              Top = 42
              Width = 4
              Height = 13
              Alignment = taRightJustify
              ParentFont = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
            end
            object sLabel1: TsLabel [1]
              Left = 211
              Top = 98
              Width = 4
              Height = 13
              Alignment = taRightJustify
              ParentFont = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
            end
            inherited memo_LOC_REM1: TsMemo
              Left = 102
              Top = 58
              Width = 595
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 1
              OnChange = memo_REMARK1Change
              OnKeyPress = memoLineLimit
              CharCase = ecUpperCase
              BoundLabel.Layout = sclLeftTop
            end
            inherited memo_REMARK1: TsMemo
              Left = 102
              Top = 2
              Width = 595
              Ctl3D = True
              ParentCtl3D = False
              TabOrder = 0
              OnChange = memo_REMARK1Change
              OnKeyPress = memoLineLimit
              CharCase = ecUpperCase
              BoundLabel.Layout = sclLeftTop
            end
          end
        end
      end
    end
    inherited sTabSheet2: TsTabSheet [1]
      inherited sSplitter5: TsSplitter
        Width = 1039
      end
      inherited sSplitter13: TsSplitter
        Left = 263
        Height = 569
      end
      inherited sPanel20: TsPanel
        Left = 266
        Width = 773
        Height = 569
        inherited sSplitter6: TsSplitter
          Top = 198
          Width = 763
        end
        inherited sSplitter7: TsSplitter
          Top = 200
          Width = 763
        end
        inherited sDBGrid3: TsDBGrid
          Width = 763
          Height = 166
          DataSource = dsGoods
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          PopupMenu = goodsMenu
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME_COD'
              Title.Alignment = taCenter
              Title.Caption = #54408#47749#53076#46300
              Width = 102
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'HS_NO'
              Title.Alignment = taCenter
              Title.Caption = 'HS'#48512#54840
              Width = 95
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 80
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTY_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRICE'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032
              Width = 80
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'PRICE_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT'
              Title.Alignment = taCenter
              Title.Caption = #44552#50529
              Width = 121
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'AMT_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end>
        end
        inherited sPanel21: TsPanel
          Top = 204
          Width = 763
          Height = 234
          DesignSize = (
            763
            234)
          inherited sSpeedButton6: TsSpeedButton
            Left = 410
            Width = 8
            Height = 226
          end
          inherited edt_NAME_COD: TsEdit
            Left = 58
            Top = 26
            Width = 99
            Height = 21
            CharCase = ecUpperCase
            Ctl3D = False
            MaxLength = 35
            OnDblClick = edt_NAME_CODDblClick
            BoundLabel.Active = False
          end
          inherited edt_HS_NO: TsMaskEdit
            Left = 270
            Top = 26
            Width = 94
            Height = 21
            Text = ''
          end
          inherited memo_NAME1: TsMemo
            Left = 58
            Top = 48
            Width = 307
            Height = 73
            ParentCtl3D = False
            CharCase = ecUpperCase
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54408#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Layout = sclLeftTop
          end
          inherited memo_SIZE1: TsMemo
            Left = 58
            Top = 148
            Width = 307
            Height = 73
            ParentCtl3D = False
            CharCase = ecUpperCase
            BoundLabel.Layout = sclLeftTop
          end
          inherited sPanel22: TsPanel
            Left = 12
            Width = 379
            Height = 21
            SkinData.CustomColor = True
            Color = 16042877
            TabOrder = 16
          end
          inherited sPanel23: TsPanel
            Left = 12
            Top = 125
            Width = 379
            Height = 21
            SkinData.CustomColor = True
            Color = 16042877
            TabOrder = 17
          end
          inherited edt_QTY_G: TsEdit
            Left = 531
            Top = 35
            Width = 40
            Height = 21
            Hint = #45800#50948
            Color = clYellow
            Ctl3D = False
            TabOrder = 4
            OnChange = edt_PRICE_GChange
            OnDblClick = edt_LOC1AMTCDblClick
            SkinData.CustomColor = True
            BoundLabel.Font.Style = []
          end
          inherited sBitBtn9: TsBitBtn
            Tag = 107
            Left = 572
            Top = 35
            Width = 22
            Height = 21
            TabOrder = 18
            OnClick = sBitBtn8Click
          end
          inherited curr_QTY: TsCurrencyEdit
            Left = 595
            Top = 35
            Width = 162
            Height = 21
            CharCase = ecUpperCase
            Ctl3D = False
            TabOrder = 5
          end
          inherited edt_PRICE_G: TsEdit
            Left = 531
            Top = 57
            Width = 40
            Height = 21
            Color = clYellow
            Ctl3D = False
            TabOrder = 6
            OnChange = edt_PRICE_GChange
            OnDblClick = edt_LOC1AMTCDblClick
            SkinData.CustomColor = True
            BoundLabel.Font.Style = []
          end
          inherited sBitBtn10: TsBitBtn
            Tag = 108
            Left = 572
            Top = 57
            Width = 22
            Height = 21
            TabOrder = 19
            OnClick = sBitBtn8Click
          end
          inherited curr_PRICE: TsCurrencyEdit
            Left = 595
            Top = 57
            Width = 162
            Height = 21
            CharCase = ecUpperCase
            Ctl3D = False
            TabOrder = 7
            OnChange = curr_PRICEChange
          end
          inherited edt_QTYG_G: TsEdit
            Left = 531
            Top = 79
            Width = 40
            Height = 21
            Hint = #45800#50948
            Ctl3D = False
            TabOrder = 8
            OnDblClick = edt_LOC1AMTCDblClick
            SkinData.CustomColor = True
            BoundLabel.Font.Style = []
          end
          inherited sBitBtn11: TsBitBtn
            Tag = 109
            Left = 572
            Top = 79
            Width = 22
            Height = 21
            TabOrder = 20
            OnClick = sBitBtn8Click
          end
          inherited curr_QTYG: TsCurrencyEdit
            Left = 595
            Top = 79
            Width = 162
            Height = 21
            CharCase = ecUpperCase
            Ctl3D = False
            TabOrder = 9
            OnExit = curr_PRICEChange
          end
          inherited edt_AMT_G: TsEdit
            Left = 531
            Width = 40
            Height = 21
            Color = clYellow
            Ctl3D = False
            TabOrder = 10
            OnDblClick = edt_LOC1AMTCDblClick
            SkinData.CustomColor = True
            BoundLabel.Font.Style = []
          end
          inherited sBitBtn12: TsBitBtn
            Tag = 110
            Left = 572
            Width = 22
            Height = 21
            TabOrder = 21
            OnClick = sBitBtn8Click
          end
          inherited curr_AMT: TsCurrencyEdit
            Left = 595
            Width = 162
            Height = 21
            CharCase = ecUpperCase
            Ctl3D = False
            TabOrder = 11
          end
          inherited edt_STQTY_G: TsEdit
            Left = 531
            Top = 151
            Width = 40
            Height = 21
            Hint = #45800#50948
            Color = clYellow
            Ctl3D = False
            TabOrder = 12
            OnDblClick = edt_LOC1AMTCDblClick
            SkinData.CustomColor = True
            BoundLabel.Font.Style = []
          end
          inherited sBitBtn13: TsBitBtn
            Tag = 111
            Left = 572
            Top = 151
            Width = 22
            Height = 21
            TabOrder = 22
            OnClick = sBitBtn8Click
          end
          inherited curr_STQTY: TsCurrencyEdit
            Left = 595
            Top = 151
            Width = 162
            Height = 21
            CharCase = ecUpperCase
            Ctl3D = False
            TabOrder = 13
          end
          inherited edt_STAMT_G: TsEdit
            Left = 531
            Top = 173
            Width = 40
            Height = 21
            Color = clYellow
            Ctl3D = False
            TabOrder = 14
            OnDblClick = edt_LOC1AMTCDblClick
            SkinData.CustomColor = True
            BoundLabel.Font.Style = []
          end
          inherited sBitBtn14: TsBitBtn
            Tag = 112
            Left = 572
            Top = 173
            Width = 22
            Height = 21
            TabOrder = 23
            OnClick = sBitBtn8Click
          end
          inherited curr_STAMT: TsCurrencyEdit
            Left = 595
            Top = 173
            Width = 162
            Height = 21
            CharCase = ecUpperCase
            Ctl3D = False
            TabOrder = 15
          end
          inherited btn_GoodsCalc: TsButton
            Left = 531
            Top = 126
            Width = 227
            Height = 21
            OnClick = btn_GoodsCalcClick
          end
          inherited sPanel24: TsPanel
            Left = 435
            Width = 379
            Height = 21
            SkinData.CustomColor = True
            Color = 16042877
          end
          inherited sBitBtn15: TsBitBtn
            Left = 158
            Top = 26
            Height = 21
            OnClick = edt_NAME_CODDblClick
            ImageIndex = 25
          end
        end
        inherited Pan_Detail: TsPanel
          Top = 7
          Width = 763
          inherited Btn_GoodsDel: TsSpeedButton
            Cursor = crHandPoint
            OnClick = Btn_GoodsDelClick
            OnMouseMove = Btn_GoodsNewMouseMove
            OnMouseLeave = Btn_GoodsNewMouseLeave
          end
          inherited Btn_GoodsEdit: TsSpeedButton
            Cursor = crHandPoint
            OnClick = Btn_GoodsEditClick
            OnMouseMove = Btn_GoodsNewMouseMove
            OnMouseLeave = Btn_GoodsNewMouseLeave
          end
          inherited Btn_GoodsOK: TsSpeedButton
            Cursor = crHandPoint
            Caption = #51200#51109
            OnClick = Btn_GoodsOKClick
            OnMouseMove = Btn_GoodsNewMouseMove
            OnMouseLeave = Btn_GoodsNewMouseLeave
          end
          inherited Btn_GoodsCancel: TsSpeedButton
            Cursor = crHandPoint
            OnClick = Btn_GoodsCancelClick
            OnMouseMove = Btn_GoodsNewMouseMove
            OnMouseLeave = Btn_GoodsNewMouseLeave
          end
          inherited Btn_GoodsNew: TsSpeedButton
            Cursor = crHandPoint
            OnClick = Btn_GoodsNewClick
            OnMouseMove = Btn_GoodsNewMouseMove
            OnMouseLeave = Btn_GoodsNewMouseLeave
          end
          inherited sSpeedButton18: TsSpeedButton
            Left = 568
            Width = 110
            OnClick = sSpeedButton18Click
            ImageIndex = 10
          end
          object sSpeedButton13: TsSpeedButton
            Left = 678
            Top = 1
            Width = 84
            Height = 23
            Caption = #50641#49472#49368#54540
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = sSpeedButton13Click
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 30
          end
        end
        inherited sPanel30: TsPanel
          Top = 202
          Width = 763
        end
        object sPanel31: TsPanel [6]
          Left = 5
          Top = 5
          Width = 763
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          Color = clSilver
          
          TabOrder = 4
        end
        inherited sPanel14: TsPanel
          Top = 459
          Width = 763
          Height = 105
          Align = alClient
          TabOrder = 5
          inherited sLabel4: TsLabel
            Left = 573
            Top = 60
          end
          inherited btn_TOTAL_CALC: TsButton
            Left = 505
            Top = 29
          end
          inherited edt_TQTY_G: TsEdit
            Left = 289
            Top = 29
          end
          inherited sBitBtn6: TsBitBtn
            Left = 323
            Top = 29
          end
          inherited curr_TQTY: TsCurrencyEdit
            Left = 348
            Top = 29
          end
          inherited edt_TAMT_G: TsEdit
            Left = 289
            Top = 53
          end
          inherited sBitBtn7: TsBitBtn
            Left = 323
            Top = 53
          end
          inherited curr_TAMT: TsCurrencyEdit
            Left = 348
            Top = 53
          end
        end
        inherited sPanel32: TsPanel
          Top = 438
          Width = 763
          Height = 21
          SkinData.CustomColor = True
          Color = 16042877
          TabOrder = 6
        end
      end
      inherited sPanel7: TsPanel
        Width = 263
        Height = 569
      end
    end
    inherited sTabSheet4: TsTabSheet [2]
      inherited sSplitter9: TsSplitter
        Width = 1039
      end
      inherited sSplitter14: TsSplitter
        Left = 263
        Height = 569
      end
      inherited panel1: TsPanel
        Left = 266
        Width = 773
        Height = 569
        object sSplitter8: TsSplitter [0]
          Left = 4
          Top = 269
          Width = 765
          Height = 2
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object sSplitter10: TsSplitter [1]
          Left = 4
          Top = 271
          Width = 765
          Height = 2
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        inherited sPanel19: TsPanel
          Width = 765
          inherited btn_TaxDel: TsSpeedButton
            OnClick = btn_TaxNewClick
          end
          inherited btn_TaxEdit: TsSpeedButton
            OnClick = btn_TaxNewClick
          end
          inherited btn_TaxOK: TsSpeedButton
            Tag = 3
            Caption = #51200#51109
            OnClick = btn_TaxNewClick
          end
          inherited btn_TaxCancel: TsSpeedButton
            Tag = 4
            OnClick = btn_TaxNewClick
          end
          inherited btn_TaxNew: TsSpeedButton
            OnClick = btn_TaxNewClick
          end
          inherited taxExcelBtn: TsSpeedButton
            Left = 570
            Width = 110
            OnClick = taxExcelBtnClick
            ImageIndex = 10
          end
          object taxSampleBtn: TsSpeedButton
            Left = 680
            Top = 1
            Width = 84
            Height = 23
            Caption = #50641#49472#49368#54540
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            OnClick = taxSampleBtnClick
            Align = alRight
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 30
          end
        end
        inherited sDBGrid4: TsDBGrid
          Width = 765
          DataSource = dsTax
          PopupMenu = taxMenu
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_NO'
              Title.Alignment = taCenter
              Title.Caption = #44228#49328#49436#48264#54840
              Width = 205
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_DATE'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51068#51088
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #44277#44553#44032#50529
              Width = 140
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #44277#44553#44032#50529#45800#50948
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAX_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #49464#50529
              Width = 140
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TAX_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #49464#50529#45800#50948
              Width = 60
              Visible = True
            end>
        end
        inherited sPanel28: TsPanel
          Top = 273
          Width = 765
          Height = 292
          Align = alClient
          inherited edt_BILL_NO: TsEdit
            Left = 174
            Top = 117
            Width = 179
            CharCase = ecUpperCase
            MaxLength = 35
            TabOrder = 1
          end
          inherited mask_BILL_DATE: TsMaskEdit
            Left = 174
            Top = 77
            CharCase = ecUpperCase
            TabOrder = 0
          end
          inherited edt_BILL_AMOUNT_UNIT: TsEdit
            Left = 518
            Top = 77
            Width = 43
            TabOrder = 4
          end
          inherited curr_BILL_AMOUNT: TsCurrencyEdit
            Left = 562
            Top = 77
            Width = 175
            CharCase = ecUpperCase
            TabOrder = 2
          end
          inherited edt_TAX_AMOUNT_UNIT: TsEdit
            Left = 518
            Top = 117
            Width = 43
            TabOrder = 5
          end
          inherited curr_TAX_AMOUNT: TsCurrencyEdit
            Left = 562
            Top = 117
            Width = 175
            CharCase = ecUpperCase
            TabOrder = 3
          end
          inherited sPanel18: TsPanel
            Left = 1
            Top = 1
            Width = 763
            SkinData.CustomColor = True
            Align = alTop
            Color = 16042877
          end
        end
      end
      inherited sPanel26: TsPanel
        Width = 263
        Height = 569
      end
    end
    inherited sTabSheet3: TsTabSheet [3]
      inherited sSplitter2: TsSplitter
        Width = 1039
      end
      inherited sSplitter11: TsSplitter
        Top = 343
        Width = 1039
      end
      inherited sDBGrid1: TsDBGrid
        Width = 1039
        Height = 309
        DataSource = dslist
        PopupMenu = popMenu1
        OnDrawColumnCell = sDBGrid2DrawColumnCell
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK1'
            Title.Alignment = taCenter
            Title.Caption = #49440#53469
            Width = 39
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 101
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 216
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 288
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457'ID'
            Width = 74
            Visible = True
          end>
      end
      inherited sPanel33: TsPanel
        Top = 345
        Width = 1039
        Height = 33
        Caption = ''
      end
      inherited sDBGrid5: TsDBGrid
        Top = 378
        Width = 1039
        Height = 193
        DataSource = dsGoods
        OnDrawColumnCell = sDBGrid5DrawColumnCell
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SEQ'
            Title.Alignment = taCenter
            Title.Caption = #49692#48264
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'HS_NO'
            Title.Alignment = taCenter
            Width = 92
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NAME1'
            Title.Alignment = taCenter
            Title.Caption = #54408#47749
            Width = 400
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QTY'
            Title.Alignment = taCenter
            Title.Caption = #49688#47049
            Width = 76
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'QTY_G'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRICE'
            Title.Alignment = taCenter
            Title.Caption = #45800#44032
            Width = 93
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'PRICE_G'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT'
            Title.Alignment = taCenter
            Title.Caption = #44552#50529
            Width = 137
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMT_G'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 30
            Visible = True
          end>
      end
      inherited sPanel3: TsPanel
        Width = 1039
        PopupMenu = popMenu1
        inherited com_SearchKeyword: TsComboBox
          TabOrder = 3
          TabStop = False
        end
        inherited Mask_SearchDate1: TsMaskEdit
          TabOrder = 0
          OnDblClick = mask_DATEEDblClick
        end
        inherited sBitBtn1: TsBitBtn
          TabOrder = 4
          OnClick = sBitBtn1Click
        end
        inherited sBitBtn21: TsBitBtn
          TabOrder = 5
          TabStop = False
          OnClick = btn_CalClick
        end
        inherited Mask_SearchDate2: TsMaskEdit
          TabOrder = 1
          OnDblClick = mask_DATEEDblClick
        end
        inherited sBitBtn23: TsBitBtn
          TabStop = False
          OnClick = btn_CalClick
        end
      end
    end
  end
  inherited sPanel4: TsPanel
    Top = 44
    Width = 1047
    TabOrder = 0
    inherited edt_MAINT_NO: TsEdit
      MaxLength = 35
    end
    inherited mask_DATEE: TsMaskEdit
      OnDblClick = mask_DATEEDblClick
    end
    inherited edt_USER_ID: TsEdit
      TabOrder = 3
    end
    inherited btn_Cal: TsBitBtn
      TabOrder = 2
      OnClick = btn_CalClick
    end
    inherited edt_msg1: TsEdit
      CharCase = ecUpperCase
      MaxLength = 3
      ReadOnly = False
      OnDblClick = edt_LOC1AMTCDblClick
    end
    inherited sBitBtn19: TsBitBtn
      Tag = 105
      OnClick = sBitBtn8Click
    end
    inherited edt_msg2: TsEdit
      CharCase = ecUpperCase
      MaxLength = 3
      ReadOnly = False
      OnDblClick = edt_LOC1AMTCDblClick
    end
    inherited sBitBtn20: TsBitBtn
      Tag = 106
      OnClick = sBitBtn8Click
    end
  end
  inherited left_panel: TsPanel
    Top = 118
    Width = 263
    Height = 570
    inherited sPanel53: TsPanel
      Width = 261
      inherited Mask_fromDate: TsMaskEdit
        Left = 84
        TabStop = True
      end
      inherited sBitBtn22: TsBitBtn
        Left = 234
        TabOrder = 2
        TabStop = True
        OnClick = sBitBtn22Click
      end
      inherited Mask_toDate: TsMaskEdit
        Left = 159
        TabStop = True
        TabOrder = 1
      end
    end
    inherited sDBGrid2: TsDBGrid
      Width = 261
      Height = 535
      TabStop = False
      DataSource = dslist
      OnDrawColumnCell = sDBGrid2DrawColumnCell
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 156
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 104
    Top = 272
  end
  object qrylist: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qrylistAfterOpen
    AfterScroll = qrylistAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO, [USER_ID], DATEE, MESSAGE1, MESSAGE2, RFF_NO, G' +
        'ET_DAT, ISS_DAT, EXP_DAT, BENEFC, BENEFC1, APPLIC, APPLIC1, APPL' +
        'IC2, APPLIC3, APPLIC4, APPLIC5, APPLIC6, RCT_AMT1, RCT_AMT1C, RC' +
        'T_AMT2, RCT_AMT2C, RATE, REMARK, REMARK1, TQTY, TQTY_G, TAMT, TA' +
        'MT_G, LOC_NO, AP_BANK, AP_BANK1, AP_BANK2, AP_BANK3, AP_BANK4, A' +
        'P_NAME, LOC1AMT, LOC1AMTC, LOC2AMT, LOC2AMTC, EX_RATE, LOADDATE,' +
        ' EXPDATE, LOC_REM, LOC_REM1, CHK1, CHK2, CHK3, PRNO, AMAINT_NO, ' +
        'BSN_HSCODE, BENEFC2, APPLIC7'
      'FROM LOCRC2_H')
    Left = 16
    Top = 232
    object qrylistMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qrylistUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qrylistDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qrylistMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qrylistMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qrylistRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qrylistGET_DAT: TStringField
      FieldName = 'GET_DAT'
      Size = 8
    end
    object qrylistISS_DAT: TStringField
      FieldName = 'ISS_DAT'
      Size = 8
    end
    object qrylistEXP_DAT: TStringField
      FieldName = 'EXP_DAT'
      Size = 8
    end
    object qrylistBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qrylistBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qrylistAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qrylistAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qrylistAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qrylistAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 10
    end
    object qrylistAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qrylistAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qrylistAPPLIC6: TStringField
      FieldName = 'APPLIC6'
      Size = 35
    end
    object qrylistRCT_AMT1: TBCDField
      FieldName = 'RCT_AMT1'
      Precision = 18
    end
    object qrylistRCT_AMT1C: TStringField
      FieldName = 'RCT_AMT1C'
      Size = 3
    end
    object qrylistRCT_AMT2: TBCDField
      FieldName = 'RCT_AMT2'
      Precision = 18
    end
    object qrylistRCT_AMT2C: TStringField
      FieldName = 'RCT_AMT2C'
      Size = 3
    end
    object qrylistRATE: TBCDField
      FieldName = 'RATE'
      Precision = 18
    end
    object qrylistREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qrylistREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qrylistTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qrylistTQTY_G: TStringField
      FieldName = 'TQTY_G'
      Size = 3
    end
    object qrylistTAMT: TBCDField
      FieldName = 'TAMT'
      Precision = 18
    end
    object qrylistTAMT_G: TStringField
      FieldName = 'TAMT_G'
      Size = 3
    end
    object qrylistLOC_NO: TStringField
      FieldName = 'LOC_NO'
      Size = 35
    end
    object qrylistAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qrylistAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qrylistAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qrylistAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qrylistAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qrylistAP_NAME: TStringField
      FieldName = 'AP_NAME'
      Size = 10
    end
    object qrylistLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qrylistLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 3
    end
    object qrylistLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qrylistLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 3
    end
    object qrylistEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qrylistLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qrylistEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qrylistLOC_REM: TStringField
      FieldName = 'LOC_REM'
      Size = 1
    end
    object qrylistLOC_REM1: TMemoField
      FieldName = 'LOC_REM1'
      BlobType = ftMemo
    end
    object qrylistCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qrylistCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qrylistCHK2GetText
      Size = 1
    end
    object qrylistCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qrylistCHK3GetText
      Size = 10
    end
    object qrylistPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qrylistAMAINT_NO: TStringField
      FieldName = 'AMAINT_NO'
      Size = 35
    end
    object qrylistBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qrylistBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 10
    end
    object qrylistAPPLIC7: TStringField
      FieldName = 'APPLIC7'
      Size = 10
    end
  end
  object dslist: TDataSource
    DataSet = qrylist
    Left = 48
    Top = 232
  end
  object qryGoods: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterInsert = qryGoodsAfterInsert
    AfterEdit = qryGoodsAfterInsert
    AfterCancel = qryGoodsAfterCancel
    AfterScroll = qryGoodsAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R0071401'
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, HS_NO, NAME_COD, NAME1, SIZE1, QTY, QTY_G, QTY' +
        'G, QTYG_G, PRICE, PRICE_G, AMT, AMT_G, STQTY, STQTY_G, STAMT, ST' +
        'AMT_G, ASEQ, AMAINT_NO'
      'FROM LOCRC2_D'
      'WHERE KEYY = :MAINT_NO')
    Left = 16
    Top = 264
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryGoodsHS_NO: TStringField
      FieldName = 'HS_NO'
      EditMask = '9999.99-9999;0;'
      Size = 10
    end
    object qryGoodsNAME_COD: TStringField
      FieldName = 'NAME_COD'
      Size = 35
    end
    object qryGoodsNAME1: TMemoField
      FieldName = 'NAME1'
      OnGetText = qryGoodsNAME1GetText
      BlobType = ftMemo
    end
    object qryGoodsSIZE1: TMemoField
      FieldName = 'SIZE1'
      OnGetText = qryGoodsSIZE1GetText
      BlobType = ftMemo
    end
    object qryGoodsQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryGoodsQTYG: TBCDField
      FieldName = 'QTYG'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryGoodsPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryGoodsAMT: TBCDField
      FieldName = 'AMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryGoodsAMT_G: TStringField
      FieldName = 'AMT_G'
      Size = 3
    end
    object qryGoodsSTQTY: TBCDField
      FieldName = 'STQTY'
      Precision = 18
    end
    object qryGoodsSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryGoodsSTAMT: TBCDField
      FieldName = 'STAMT'
      Precision = 18
    end
    object qryGoodsSTAMT_G: TStringField
      FieldName = 'STAMT_G'
      Size = 3
    end
    object qryGoodsASEQ: TIntegerField
      FieldName = 'ASEQ'
    end
    object qryGoodsAMAINT_NO: TStringField
      FieldName = 'AMAINT_NO'
      Size = 35
    end
  end
  object dsGoods: TDataSource
    DataSet = qryGoods
    Left = 48
    Top = 264
  end
  object qryTax: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterInsert = qryTaxAfterInsert
    AfterEdit = qryTaxAfterInsert
    AfterCancel = qryTaxAfterCancel
    AfterScroll = qryTaxAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R0071401'
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, BILL_NO, BILL_DATE, BILL_AMOUNT, BILL_AMOUNT_U' +
        'NIT, TAX_AMOUNT, TAX_AMOUNT_UNIT'
      'FROM LOCRC2_TAX'
      'WHERE KEYY = :MAINT_NO')
    Left = 16
    Top = 296
    object qryTaxSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryTaxBILL_NO: TStringField
      FieldName = 'BILL_NO'
      Size = 35
    end
    object qryTaxBILL_DATE: TStringField
      FieldName = 'BILL_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryTaxBILL_AMOUNT: TBCDField
      FieldName = 'BILL_AMOUNT'
      DisplayFormat = '#,##0.####;'
      EditFormat = '#,0.####;0;'
      Precision = 18
    end
    object qryTaxBILL_AMOUNT_UNIT: TStringField
      FieldName = 'BILL_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxTAX_AMOUNT: TBCDField
      FieldName = 'TAX_AMOUNT'
      DisplayFormat = '#,##0.####;'
      EditFormat = '#,0.####;0;'
      Precision = 18
    end
    object qryTaxTAX_AMOUNT_UNIT: TStringField
      FieldName = 'TAX_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
  end
  object dsTax: TDataSource
    DataSet = qryTax
    Left = 48
    Top = 296
  end
  object qryMAX_SEQ: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    Left = 104
    Top = 240
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = Letter
    Left = 160
    Top = 272
  end
  object spCopyLOCRC2: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'CopyLOCRC2;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CopyDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@NewDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 200
    Top = 272
  end
  object goodsMenu: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = popMenuEnabled
    Left = 160
    Top = 304
    object N1: TMenuItem
      Caption = #45936#51060#53552' '#52628#44032
      ImageIndex = 26
      OnClick = Btn_GoodsNewClick
    end
    object N2: TMenuItem
      Tag = 1
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = Btn_GoodsEditClick
    end
    object N3: TMenuItem
      Tag = 2
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = Btn_GoodsDelClick
    end
  end
  object taxMenu: TPopupMenu
    Tag = 1
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = popMenuEnabled
    Left = 192
    Top = 304
    object MenuItem1: TMenuItem
      Caption = #45936#51060#53552' '#52628#44032
      ImageIndex = 26
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Tag = 1
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = MenuItem1Click
    end
    object MenuItem3: TMenuItem
      Tag = 2
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = MenuItem1Click
    end
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE LOCRC2_H'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 136
    Top = 240
  end
  object popMenu1: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    Left = 224
    Top = 304
    object MenuItem4: TMenuItem
      Caption = #51204#49569#51456#48708
      ImageIndex = 29
      OnClick = btnReadyClick
    end
    object MenuItem5: TMenuItem
      Caption = '-'
    end
    object MenuItem6: TMenuItem
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = btnEditClick
    end
    object N4: TMenuItem
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = btnDelClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object d1: TMenuItem
      Caption = #52636#47141#54616#44592
      object N8: TMenuItem
        Caption = #48120#47532#48372#44592
        ImageIndex = 15
        OnClick = btnPrintClick
      end
      object N9: TMenuItem
        Tag = 1
        Caption = #54532#47536#53944
        ImageIndex = 21
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = 'ADMIN'
      Enabled = False
      ImageIndex = 16
    end
  end
  object excelOpen: TsOpenDialog
    Filter = 
      #47784#46304' Excel '#54028#51068'(*.xls;*.xlsx)|*.xls;*.xlsx|Excel '#53685#54633#47928#49436'(*.xlsx)|*.xlsx' +
      '|Excel 97 - 2003 '#53685#54633#47928#49436'(*.xls)|*.xls|All Files|*.*'
    Left = 104
    Top = 304
  end
end
