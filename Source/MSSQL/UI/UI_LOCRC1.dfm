inherited UI_LOCRC1_frm: TUI_LOCRC1_frm
  Left = 2068
  Top = 81
  Caption = #45236#44397#49888#50857#51109' '#47932#54408#49688#47161' '#51613#47749#49436'('#49688#49888')'
  ClientHeight = 691
  ClientWidth = 1114
  Constraints.MaxHeight = 730
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter10: TsSplitter [1]
    Left = 0
    Top = 77
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel4: TsPanel [2]
    Left = 0
    Top = 44
    Width = 1114
    Height = 33
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    object edt_MAINT_NO: TsEdit
      Left = 64
      Top = 5
      Width = 241
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object mask_DATEE: TsMaskEdit
      Left = 366
      Top = 5
      Width = 82
      Height = 23
      AutoSize = False
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_USER_ID: TsEdit
      Left = 512
      Top = 5
      Width = 57
      Height = 23
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 641
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 721
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
  end
  object sPageControl1: TsPageControl [3]
    Left = 0
    Top = 80
    Width = 1114
    Height = 611
    ActivePage = sTabSheet1
    Align = alClient
    TabHeight = 30
    TabIndex = 0
    TabOrder = 1
    TabWidth = 110
    OnChange = sPageControl1Change
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter3: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel2: TsPanel
        Left = 0
        Top = 2
        Width = 1106
        Height = 569
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        
        TabOrder = 0
        object sSplitter4: TsSplitter
          Left = 305
          Top = 1
          Width = 3
          Height = 567
          Cursor = crHSplit
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object sPanel5: TsPanel
          Left = 1
          Top = 1
          Width = 304
          Height = 567
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'sPanel5'
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 0
        end
        object sPanel6: TsPanel
          Left = 308
          Top = 1
          Width = 797
          Height = 567
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          BevelOuter = bvNone
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 1
          object sPanel9: TsPanel
            Left = 0
            Top = 0
            Width = 797
            Height = 425
            SkinData.SkinSection = 'TRANSPARENT'
            Align = alTop
            BevelOuter = bvNone
            
            TabOrder = 0
            DesignSize = (
              797
              425)
            object sSpeedButton1: TsSpeedButton
              Left = 398
              Top = -2
              Width = 10
              Height = 423
              Anchors = [akLeft, akTop, akBottom]
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'SPEEDBUTTON'
            end
            object edt_AP_BANK1: TsEdit
              Left = 526
              Top = 45
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 17
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Caption = #49345#54840
            end
            object edt_RFF_NO: TsEdit
              Left = 134
              Top = 89
              Width = 223
              Height = 21
              HelpContext = 1
              TabStop = False
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              TabOrder = 3
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #48156#44553#48264#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object mask_EXP_DAT: TsMaskEdit
              Left = 134
              Top = 67
              Width = 75
              Height = 21
              AutoSize = False
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #47928#49436#50976#54952#51068#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object sPanel10: TsPanel
              Left = 4
              Top = 158
              Width = 379
              Height = 21
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
              BevelOuter = bvNone
              Caption = #49688#54812#50629#52404' / '#44060#49444#50629#52404
              Color = 16042877
              Ctl3D = False
              
              ParentCtl3D = False
              TabOrder = 33
            end
            object edt_BENEFC: TsEdit
              Left = 134
              Top = 181
              Width = 65
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 5
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 5
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49688#54812#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sBitBtn5: TsBitBtn
              Left = 200
              Top = 180
              Width = 24
              Height = 22
              Cursor = crHandPoint
              TabOrder = 34
              TabStop = False
              ImageIndex = 6
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object mask_ISS_DAT: TsMaskEdit
              Left = 134
              Top = 45
              Width = 75
              Height = 21
              AutoSize = False
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #48156#44553#51068#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object edt_BSN_HSCODE: TsEdit
              Left = 134
              Top = 111
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 4
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #45824#54364#44277#44553#47932#54408' HS'#48512#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object mask_GET_DAT: TsMaskEdit
              Left = 134
              Top = 23
              Width = 74
              Height = 21
              AutoSize = False
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51064#49688#51068#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object edt_AP_BANK: TsEdit
              Tag = 1
              Left = 526
              Top = 23
              Width = 65
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 5
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 16
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44060#49444#51008#54665
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sPanel8: TsPanel
              Left = 4
              Top = 0
              Width = 379
              Height = 21
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
              BevelOuter = bvNone
              Caption = #44592#48376#51077#47141#49324#54637
              Color = 16042877
              Ctl3D = False
              
              ParentCtl3D = False
              TabOrder = 32
            end
            object sPanel12: TsPanel
              Left = 423
              Top = 158
              Width = 379
              Height = 21
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
              BevelOuter = bvNone
              Caption = #45236#44397#49888#50857#51109
              Color = 16042877
              Ctl3D = False
              
              ParentCtl3D = False
              TabOrder = 38
            end
            object edt_AP_BANK3: TsEdit
              Left = 526
              Top = 89
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 19
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51008#54665'('#48512')'#51648#51216#47749
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_AP_NAME: TsEdit
              Left = 526
              Top = 133
              Width = 99
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 21
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51008#54665#51204#51088#49436#47749
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_AP_BANK4: TsEdit
              Left = 526
              Top = 111
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 20
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Caption = #49345#54840
            end
            object edt_AP_BANK2: TsEdit
              Left = 526
              Top = 67
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 18
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Caption = #49345#54840
            end
            object sPanel11: TsPanel
              Left = 423
              Top = 0
              Width = 379
              Height = 21
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
              BevelOuter = bvNone
              Caption = #44060#49444#51008#54665
              Color = 16042877
              Ctl3D = False
              
              ParentCtl3D = False
              TabOrder = 37
            end
            object sBitBtn3: TsBitBtn
              Tag = 2
              Left = 592
              Top = 23
              Width = 25
              Height = 21
              Cursor = crHandPoint
              TabOrder = 36
              TabStop = False
              ImageIndex = 6
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object mask_EXPDATE: TsMaskEdit
              Left = 526
              Top = 291
              Width = 75
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 27
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #50976#54952#51068#51088
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object mask_LOADDATE: TsMaskEdit
              Left = 526
              Top = 269
              Width = 75
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 26
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #47932#54408#51064#46020#44592#51068
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object curr_LOC2AMT: TsCurrencyEdit
              Left = 560
              Top = 247
              Width = 189
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 25
              SkinData.SkinSection = 'EDIT'
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object edt_LOC2AMTC: TsEdit
              Tag = 102
              Left = 526
              Top = 247
              Width = 33
              Height = 21
              Hint = #53685#54868
              HelpContext = 1
              TabStop = False
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 3
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              TabOrder = 40
              Text = 'KRW'
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44060#49444#44552#50529'('#50896#54868')'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_EX_RATE: TsEdit
              Left = 526
              Top = 225
              Width = 99
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 24
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #47588#47588#44592#51456#50984
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object curr_LOC1AMT: TsCurrencyEdit
              Left = 591
              Top = 203
              Width = 158
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 23
              SkinData.SkinSection = 'EDIT'
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object sBitBtn8: TsBitBtn
              Tag = 101
              Left = 566
              Top = 203
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 39
              TabStop = False
              ImageIndex = 25
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object edt_APPLIC4: TsEdit
              Left = 134
              Top = 357
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 13
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51452#49548
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_APPLIC3: TsEdit
              Left = 134
              Top = 335
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 12
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51204#51088#49436#47749
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_APPLIC2: TsEdit
              Left = 134
              Top = 313
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 11
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #47749#51032#51064
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_APPLIC1: TsEdit
              Left = 134
              Top = 269
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 9
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49345#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sBitBtn2: TsBitBtn
              Tag = 1
              Left = 200
              Top = 247
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 35
              TabStop = False
              ImageIndex = 6
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object edt_APPLIC: TsEdit
              Tag = 1
              Left = 134
              Top = 247
              Width = 65
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 5
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 8
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44060#49444#50629#52404
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_BENEFC1: TsEdit
              Left = 134
              Top = 203
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 6
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49345#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object edt_LOC1AMTC: TsEdit
              Tag = 101
              Left = 526
              Top = 203
              Width = 39
              Height = 21
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 3
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 22
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44060#49444#44552#50529'('#50808#54868')'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sPanel13: TsPanel
              Left = 423
              Top = 322
              Width = 379
              Height = 21
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
              BevelOuter = bvNone
              Caption = #51064#49688#44552#50529
              Color = 16042877
              Ctl3D = False
              
              ParentCtl3D = False
              TabOrder = 41
            end
            object edt_RCT_AMT1C: TsEdit
              Tag = 102
              Left = 526
              Top = 345
              Width = 39
              Height = 21
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 3
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 28
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51064#49688#44552#50529'('#50808#54868')'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object sBitBtn4: TsBitBtn
              Tag = 102
              Left = 566
              Top = 345
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 42
              TabStop = False
              ImageIndex = 25
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object curr_RCT_AMT1: TsCurrencyEdit
              Left = 591
              Top = 345
              Width = 158
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 29
              SkinData.SkinSection = 'EDIT'
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object edt_RATE: TsEdit
              Left = 526
              Top = 367
              Width = 66
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 30
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #54872#50984
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
            end
            object edt_RCT_AMT2C: TsEdit
              Tag = 102
              Left = 526
              Top = 389
              Width = 33
              Height = 21
              Hint = #53685#54868
              HelpContext = 1
              TabStop = False
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 3
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              TabOrder = 43
              Text = 'KRW'
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #51064#49688#44552#50529'('#50896#54868')'
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
            object curr_RCT_AMT2: TsCurrencyEdit
              Left = 560
              Top = 389
              Width = 189
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 31
              SkinData.SkinSection = 'EDIT'
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object edt_APPLIC5: TsEdit
              Left = 134
              Top = 379
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 14
              SkinData.SkinSection = 'EDIT'
            end
            object edt_APPLIC6: TsEdit
              Left = 134
              Top = 401
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 15
              SkinData.SkinSection = 'EDIT'
            end
            object edt_BENEFC2: TsMaskEdit
              Left = 134
              Top = 225
              Width = 91
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              EditMask = '999-99-99999;0;'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 12
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 7
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.SkinSection = 'EDIT'
            end
            object edt_APPLIC7: TsMaskEdit
              Left = 134
              Top = 291
              Width = 91
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              EditMask = '999-99-99999;0;'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 12
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 10
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              SkinData.SkinSection = 'EDIT'
            end
            object sBitBtn16: TsBitBtn
              Left = 211
              Top = 23
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 44
              TabStop = False
              ImageIndex = 9
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object sBitBtn17: TsBitBtn
              Tag = 1
              Left = 211
              Top = 45
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 45
              TabStop = False
              ImageIndex = 9
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object sBitBtn18: TsBitBtn
              Tag = 2
              Left = 211
              Top = 67
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 46
              TabStop = False
              ImageIndex = 9
              Images = DMICON.System18
              SkinData.SkinSection = 'BUTTON'
            end
            object edt_LOC_NO: TsEdit
              Left = 526
              Top = 181
              Width = 223
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 47
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #45236#44397#49888#50857#51109#48264#54840
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
            end
          end
          object sPanel15: TsPanel
            Left = 0
            Top = 446
            Width = 797
            Height = 121
            SkinData.SkinSection = 'TRANSPARENT'
            Align = alClient
            BevelOuter = bvNone
            
            TabOrder = 2
            object memo_LOC_REM1: TsMemo
              Left = 177
              Top = 61
              Width = 451
              Height = 55
              ScrollBars = ssVertical
              TabOrder = 1
              CharCase = ecUpperCase
              BoundLabel.Active = True
              BoundLabel.UseSkinColor = False
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #52280#44256#49324#54637
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = 5197647
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              SkinData.SkinSection = 'EDIT'
            end
            object memo_REMARK1: TsMemo
              Left = 177
              Top = 5
              Width = 451
              Height = 55
              ScrollBars = ssVertical
              TabOrder = 0
              CharCase = ecUpperCase
              BoundLabel.Active = True
              BoundLabel.UseSkinColor = False
              BoundLabel.ParentFont = False
              BoundLabel.Caption = #44592#53440#51312#44148
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = 5197647
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              SkinData.SkinSection = 'EDIT'
            end
          end
          object sPanel16: TsPanel
            Left = 0
            Top = 425
            Width = 797
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Align = alTop
            BevelOuter = bvNone
            Caption = #44592#53440#51312#44148' && '#52280#44256#49324#54637
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 1
          end
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #49345#54408#45236#50669
      object sSplitter5: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter12: TsSplitter
        Left = 304
        Top = 2
        Width = 3
        Height = 569
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel20: TsPanel
        Left = 307
        Top = 2
        Width = 799
        Height = 569
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        BorderWidth = 4
        
        TabOrder = 0
        object sSplitter6: TsSplitter
          Left = 5
          Top = 7
          Width = 789
          Height = 2
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object sSplitter7: TsSplitter
          Left = 5
          Top = 410
          Width = 789
          Height = 2
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object sDBGrid3: TsDBGrid
          Left = 5
          Top = 9
          Width = 789
          Height = 166
          Align = alTop
          Color = clWhite
          Ctl3D = False
          DataSource = dsGoods
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME_COD'
              Title.Alignment = taCenter
              Title.Caption = #54408#47749#53076#46300
              Width = 102
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'HS_NO'
              Title.Alignment = taCenter
              Title.Caption = 'HS'#48512#54840
              Width = 95
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 80
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTY_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRICE'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032
              Width = 80
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'PRICE_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT'
              Title.Alignment = taCenter
              Title.Caption = #44552#50529
              Width = 121
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'AMT_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 28
              Visible = True
            end>
        end
        object sPanel21: TsPanel
          Left = 5
          Top = 175
          Width = 789
          Height = 235
          SkinData.SkinSection = 'PANEL'
          Align = alTop
          
          TabOrder = 1
          DesignSize = (
            789
            235)
          object sSpeedButton6: TsSpeedButton
            Left = 390
            Top = 3
            Width = 10
            Height = 227
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'SPEEDBUTTON'
          end
          object sPanel24: TsPanel
            Left = 409
            Top = 3
            Width = 379
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49688#47049'/'#45800#44032'/'#44552#50529
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 25
          end
          object edt_NAME_COD: TsEdit
            Left = 46
            Top = 26
            Width = 99
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
          end
          object edt_HS_NO: TsMaskEdit
            Left = 257
            Top = 26
            Width = 94
            Height = 21
            AutoSize = False
            Color = clWhite
            Ctl3D = False
            EditMask = '0000.00-0000;0;'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 12
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'HS'#48512#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object memo_NAME1: TsMemo
            Left = 46
            Top = 48
            Width = 307
            Height = 73
            TabOrder = 2
            CharCase = ecUpperCase
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54408#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeftTop
            SkinData.SkinSection = 'EDIT'
          end
          object memo_SIZE1: TsMemo
            Left = 46
            Top = 148
            Width = 307
            Height = 73
            TabOrder = 3
            CharCase = ecUpperCase
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44508#44201
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeftTop
            SkinData.SkinSection = 'EDIT'
          end
          object sPanel22: TsPanel
            Left = 1
            Top = 3
            Width = 379
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #49345#54408' '#54408#47749
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 16
          end
          object sPanel23: TsPanel
            Left = 1
            Top = 125
            Width = 379
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #44508#44201
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 17
          end
          object edt_QTY_G: TsEdit
            Tag = 102
            Left = 531
            Top = 42
            Width = 39
            Height = 21
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn9: TsBitBtn
            Tag = 107
            Left = 571
            Top = 42
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 18
            TabStop = False
            Visible = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_QTY: TsCurrencyEdit
            Left = 571
            Top = 42
            Width = 164
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_PRICE_G: TsEdit
            Tag = 102
            Left = 531
            Top = 64
            Width = 39
            Height = 21
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn10: TsBitBtn
            Tag = 108
            Left = 700
            Top = 64
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 19
            TabStop = False
            Visible = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_PRICE: TsCurrencyEdit
            Left = 571
            Top = 64
            Width = 164
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_QTYG_G: TsEdit
            Tag = 102
            Left = 531
            Top = 86
            Width = 39
            Height = 21
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032#44592#51456#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn11: TsBitBtn
            Tag = 109
            Left = 700
            Top = 86
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 20
            TabStop = False
            Visible = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_QTYG: TsCurrencyEdit
            Left = 571
            Top = 86
            Width = 164
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_AMT_G: TsEdit
            Tag = 102
            Left = 531
            Top = 108
            Width = 39
            Height = 21
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn12: TsBitBtn
            Tag = 110
            Left = 700
            Top = 108
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 21
            TabStop = False
            Visible = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_AMT: TsCurrencyEdit
            Left = 571
            Top = 108
            Width = 164
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_STQTY_G: TsEdit
            Tag = 102
            Left = 531
            Top = 158
            Width = 39
            Height = 21
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 12
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn13: TsBitBtn
            Tag = 102
            Left = 700
            Top = 158
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 22
            TabStop = False
            Visible = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_STQTY: TsCurrencyEdit
            Tag = 102
            Left = 571
            Top = 158
            Width = 164
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 13
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_STAMT_G: TsEdit
            Tag = 102
            Left = 531
            Top = 180
            Width = 39
            Height = 21
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 14
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sBitBtn14: TsBitBtn
            Tag = 112
            Left = 700
            Top = 180
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 23
            TabStop = False
            Visible = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object curr_STAMT: TsCurrencyEdit
            Left = 571
            Top = 180
            Width = 164
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 15
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object btn_GoodsCalc: TsButton
            Left = 531
            Top = 133
            Width = 206
            Height = 21
            Caption = #44228#49328
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 24
            SkinData.SkinSection = 'BUTTON'
            Reflected = True
            Images = DMICON.System16
            ImageIndex = 7
          end
          object sBitBtn15: TsBitBtn
            Left = 146
            Top = 26
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 26
            TabStop = False
            Visible = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
        end
        object sPanel30: TsPanel
          Left = 5
          Top = 433
          Width = 789
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          
          TabOrder = 2
        end
        object sPanel31: TsPanel
          Left = 5
          Top = 5
          Width = 789
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          BevelOuter = bvNone
          Color = clSilver
          
          TabOrder = 3
        end
        object sPanel14: TsPanel
          Left = 5
          Top = 435
          Width = 789
          Height = 129
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          BevelOuter = bvNone
          
          TabOrder = 4
          object sBitBtn7: TsBitBtn
            Tag = 104
            Left = 327
            Top = 52
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 6
            TabStop = False
            Visible = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object sBitBtn6: TsBitBtn
            Tag = 103
            Left = 327
            Top = 28
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 4
            TabStop = False
            Visible = False
            ImageIndex = 25
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
          object btn_TOTAL_CALC: TsButton
            Left = 505
            Top = 28
            Width = 67
            Height = 46
            Caption = #44228#49328
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'BUTTON'
            Reflected = True
            Images = DMICON.System16
            ImageIndex = 7
          end
          object edt_TQTY_G: TsEdit
            Tag = 103
            Left = 293
            Top = 28
            Width = 39
            Height = 21
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#54633#44228#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_TQTY: TsCurrencyEdit
            Left = 333
            Top = 28
            Width = 171
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TAMT: TsCurrencyEdit
            Left = 333
            Top = 52
            Width = 171
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TAMT_G: TsEdit
            Tag = 104
            Left = 293
            Top = 52
            Width = 39
            Height = 21
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#54633#44228#44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
        object sPanel32: TsPanel
          Left = 5
          Top = 412
          Width = 789
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Align = alTop
          BevelOuter = bvNone
          Caption = #52509' '#54633#44228
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 5
        end
      end
      object sPanel19: TsPanel
        Left = 0
        Top = 2
        Width = 304
        Height = 569
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'sPanel5'
        Ctl3D = False
        
        ParentCtl3D = False
        TabOrder = 1
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #49464#44552#44228#49328#49436
      object sSplitter9: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter13: TsSplitter
        Left = 304
        Top = 2
        Width = 3
        Height = 569
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object panel1: TsPanel
        Left = 307
        Top = 2
        Width = 799
        Height = 569
        SkinData.SkinSection = 'PANEL'
        Align = alClient
        BevelWidth = 4
        
        TabOrder = 0
        object sSplitter8: TsSplitter
          Left = 4
          Top = 244
          Width = 791
          Height = 4
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object sDBGrid4: TsDBGrid
          Left = 4
          Top = 4
          Width = 791
          Height = 240
          Align = alTop
          Color = clWhite
          Ctl3D = False
          DataSource = dsTax
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_NO'
              Title.Alignment = taCenter
              Title.Caption = #44228#49328#49436#48264#54840
              Width = 205
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_DATE'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51068#51088
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #44277#44553#44032#50529
              Width = 140
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #44277#44553#44032#50529#45800#50948
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAX_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #49464#50529
              Width = 140
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TAX_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #49464#50529#45800#50948
              Width = 60
              Visible = True
            end>
        end
        object sPanel28: TsPanel
          Left = 4
          Top = 248
          Width = 791
          Height = 317
          SkinData.SkinSection = 'PANEL'
          Align = alClient
          
          TabOrder = 1
          object edt_BILL_NO: TsEdit
            Left = 150
            Top = 109
            Width = 223
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49464#44552#44228#49328#49436#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object mask_BILL_DATE: TsMaskEdit
            Left = 150
            Top = 69
            Width = 75
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51089#49457#51068#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_BILL_AMOUNT_UNIT: TsEdit
            Tag = 102
            Left = 494
            Top = 69
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            Text = 'KRW'
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44277#44553#44032#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object curr_BILL_AMOUNT: TsCurrencyEdit
            Left = 528
            Top = 69
            Width = 189
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TAX_AMOUNT_UNIT: TsEdit
            Tag = 102
            Left = 494
            Top = 109
            Width = 33
            Height = 23
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
            Text = 'KRW'
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49464#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object curr_TAX_AMOUNT: TsCurrencyEdit
            Left = 528
            Top = 109
            Width = 189
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object sPanel18: TsPanel
            Left = 1
            Top = 1
            Width = 789
            Height = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Align = alTop
            BevelOuter = bvNone
            Caption = #49464#44552#44228#49328#49436
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 6
          end
        end
      end
      object sPanel26: TsPanel
        Left = 0
        Top = 2
        Width = 304
        Height = 569
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'sPanel5'
        Ctl3D = False
        
        ParentCtl3D = False
        TabOrder = 1
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter2: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter11: TsSplitter
        Left = 0
        Top = 343
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alBottom
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 34
        Width = 1106
        Height = 309
        Align = alClient
        Color = clWhite
        Ctl3D = False
        DataSource = dslist
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid2DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 216
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 288
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457'ID'
            Width = 74
            Visible = True
          end>
      end
      object sPanel33: TsPanel
        Left = 0
        Top = 345
        Width = 1106
        Height = 33
        SkinData.CustomColor = True
        SkinData.SkinSection = 'DRAGBAR'
        Align = alBottom
        BevelOuter = bvNone
        Ctl3D = False
        
        ParentCtl3D = False
        TabOrder = 1
      end
      object sDBGrid5: TsDBGrid
        Left = 0
        Top = 378
        Width = 1106
        Height = 193
        Align = alBottom
        Color = clWhite
        Ctl3D = False
        DataSource = dsGoods
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid5DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SEQ'
            Title.Alignment = taCenter
            Title.Caption = #49692#48264
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'HS_NO'
            Title.Alignment = taCenter
            Width = 92
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NAME_COD'
            Title.Alignment = taCenter
            Title.Caption = #54408#47749
            Width = 400
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QTY'
            Title.Alignment = taCenter
            Title.Caption = #49688#47049
            Width = 76
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'QTY_G'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRICE'
            Title.Alignment = taCenter
            Title.Caption = #45800#44032
            Width = 93
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'PRICE_G'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT'
            Title.Caption = #44552#50529
            Width = 137
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMT_G'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 30
            Visible = True
          end>
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 2
        Width = 1106
        Height = 32
        SkinData.SkinSection = 'PANEL'
        Align = alTop
        
        TabOrder = 3
        object edt_SearchText: TsEdit
          Left = 86
          Top = 5
          Width = 233
          Height = 23
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 81
          Height = 23
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          SkinData.SkinSection = 'COMBOBOX'
          Style = csDropDownList
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 3
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 86
          Top = 5
          Width = 81
          Height = 23
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '20160101'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn1: TsBitBtn
          Left = 319
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 4
          OnClick = sBitBtn1Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn21: TsBitBtn
          Tag = 900
          Left = 167
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 214
          Top = 5
          Width = 81
          Height = 23
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20170609'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn23: TsBitBtn
          Tag = 901
          Left = 295
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel25: TsPanel
          Left = 190
          Top = 5
          Width = 25
          Height = 23
          SkinData.SkinSection = 'PANEL'
          Caption = '~'
          
          TabOrder = 7
        end
      end
    end
  end
  object sPanel1: TsPanel [4]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 2
    DesignSize = (
      1114
      41)
    object sSpeedButton4: TsSpeedButton
      Left = 450
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton2: TsSpeedButton
      Left = 173
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel
      Left = 8
      Top = 5
      Width = 161
      Height = 17
      Caption = #45236#44397#49888#50857#51109' '#47932#54408#49688#47161#51613#47749#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel
      Left = 8
      Top = 20
      Width = 42
      Height = 13
      Caption = 'LOCRC1'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object btnExit: TsButton
      Left = 1035
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Left = 183
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 250
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 2
      OnClick = sButton1Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton1: TsButton
      Tag = 1
      Left = 349
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 3
      OnClick = sButton1Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  object sPanel7: TsPanel [5]
    Left = 4
    Top = 118
    Width = 305
    Height = 569
    SkinData.SkinSection = 'PANEL'
    
    TabOrder = 3
    object sPanel53: TsPanel
      Left = 1
      Top = 1
      Width = 303
      Height = 33
      SkinData.SkinSection = 'PANEL'
      Align = alTop
      BevelOuter = bvNone
      
      TabOrder = 0
      object Mask_fromDate: TsMaskEdit
        Left = 129
        Top = 5
        Width = 73
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20160101'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn22: TsBitBtn
        Left = 276
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = sBitBtn22Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object Mask_toDate: TsMaskEdit
        Left = 203
        Top = 5
        Width = 73
        Height = 23
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20170906'
        CheckOnExit = True
        SkinData.SkinSection = 'EDIT'
      end
    end
    object sDBGrid2: TsDBGrid
      Left = 1
      Top = 34
      Width = 303
      Height = 534
      Align = alClient
      Color = clWhite
      Ctl3D = False
      DataSource = dslist
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid2DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 90
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 200
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 200
  end
  object qrylist: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qrylistAfterOpen
    AfterScroll = qrylistAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO,USER_ID,DATEE,MESSAGE1,MESSAGE2,RFF_NO,GET_DAT,I' +
        'SS_DAT,EXP_DAT,BENEFC,BENEFC1,APPLIC,APPLIC1,APPLIC2,APPLIC3,APP' +
        'LIC4,APPLIC5,APPLIC6 '
      
        '            ,RCT_AMT1,RCT_AMT1C,RCT_AMT2,RCT_AMT2C,RATE,REMARK,R' +
        'EMARK1,TQTY,TQTY_G,TAMT,TAMT_G,LOC_NO,AP_BANK,AP_BANK1,AP_BANK2,' +
        'AP_BANK3,AP_BANK4,AP_NAME'
      
        '            ,LOC1AMT,LOC1AMTC,LOC2AMT,LOC2AMTC,EX_RATE,LOADDATE,' +
        'EXPDATE,LOC_REM,LOC_REM1,CHK1,CHK2,CHK3,NEGODT,NEGOAMT,BSN_HSCOD' +
        'E,PRNO,APPLIC7,BENEFC2,CK_S'
      'FROM LOCRC1_H')
    Left = 16
    Top = 232
    object qrylistMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qrylistUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qrylistDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qrylistMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qrylistMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qrylistRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qrylistGET_DAT: TStringField
      FieldName = 'GET_DAT'
      Size = 8
    end
    object qrylistISS_DAT: TStringField
      FieldName = 'ISS_DAT'
      Size = 8
    end
    object qrylistEXP_DAT: TStringField
      FieldName = 'EXP_DAT'
      Size = 8
    end
    object qrylistBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qrylistBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qrylistAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qrylistAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qrylistAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qrylistAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qrylistAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qrylistAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qrylistAPPLIC6: TStringField
      FieldName = 'APPLIC6'
      Size = 35
    end
    object qrylistRCT_AMT1: TBCDField
      FieldName = 'RCT_AMT1'
      Precision = 18
    end
    object qrylistRCT_AMT1C: TStringField
      FieldName = 'RCT_AMT1C'
      Size = 3
    end
    object qrylistRCT_AMT2: TBCDField
      FieldName = 'RCT_AMT2'
      Precision = 18
    end
    object qrylistRCT_AMT2C: TStringField
      FieldName = 'RCT_AMT2C'
      Size = 3
    end
    object qrylistRATE: TBCDField
      FieldName = 'RATE'
      Precision = 18
    end
    object qrylistREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qrylistREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qrylistTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qrylistTQTY_G: TStringField
      FieldName = 'TQTY_G'
      Size = 3
    end
    object qrylistTAMT: TBCDField
      FieldName = 'TAMT'
      Precision = 18
    end
    object qrylistTAMT_G: TStringField
      FieldName = 'TAMT_G'
      Size = 3
    end
    object qrylistLOC_NO: TStringField
      FieldName = 'LOC_NO'
      Size = 35
    end
    object qrylistAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qrylistAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qrylistAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qrylistAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qrylistAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qrylistAP_NAME: TStringField
      FieldName = 'AP_NAME'
      Size = 17
    end
    object qrylistLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qrylistLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 3
    end
    object qrylistLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qrylistLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 3
    end
    object qrylistEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qrylistLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qrylistEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qrylistLOC_REM: TStringField
      FieldName = 'LOC_REM'
      Size = 1
    end
    object qrylistLOC_REM1: TMemoField
      FieldName = 'LOC_REM1'
      BlobType = ftMemo
    end
    object qrylistCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qrylistCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qrylistCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qrylistNEGODT: TStringField
      FieldName = 'NEGODT'
      Size = 8
    end
    object qrylistNEGOAMT: TBCDField
      FieldName = 'NEGOAMT'
      Precision = 18
    end
    object qrylistBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qrylistPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qrylistAPPLIC7: TStringField
      FieldName = 'APPLIC7'
      Size = 10
    end
    object qrylistBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 10
    end
    object qrylistCK_S: TStringField
      FieldName = 'CK_S'
      Size = 1
    end
  end
  object qryGoods: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryGoodsAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R0071401'
      end>
    SQL.Strings = (
      
        'SELECT KEYY,SEQ,HS_CHK,HS_NO,NAME_CHK,NAME_COD,NAME,NAME1,SIZE,S' +
        'IZE1,QTY,QTY_G,QTYG,QTYG_G,PRICE,PRICE_G,AMT,AMT_G,STQTY,STQTY_G' +
        ',STAMT,STAMT_G'
      'FROM LOCRC1_D'
      'WHERE KEYY = :MAINT_NO')
    Left = 16
    Top = 264
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryGoodsHS_CHK: TStringField
      FieldName = 'HS_CHK'
      Size = 1
    end
    object qryGoodsHS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 10
    end
    object qryGoodsNAME_CHK: TStringField
      FieldName = 'NAME_CHK'
      Size = 1
    end
    object qryGoodsNAME_COD: TStringField
      FieldName = 'NAME_COD'
      Size = 35
    end
    object qryGoodsNAME: TStringField
      FieldName = 'NAME'
    end
    object qryGoodsNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryGoodsSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryGoodsSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryGoodsQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryGoodsQTYG: TBCDField
      FieldName = 'QTYG'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryGoodsPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryGoodsAMT: TBCDField
      FieldName = 'AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsAMT_G: TStringField
      FieldName = 'AMT_G'
      Size = 3
    end
    object qryGoodsSTQTY: TBCDField
      FieldName = 'STQTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryGoodsSTAMT: TBCDField
      FieldName = 'STAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsSTAMT_G: TStringField
      FieldName = 'STAMT_G'
      Size = 3
    end
  end
  object dslist: TDataSource
    DataSet = qrylist
    Left = 48
    Top = 232
  end
  object dsGoods: TDataSource
    DataSet = qryGoods
    Left = 48
    Top = 264
  end
  object dsTax: TDataSource
    DataSet = qryTax
    Left = 48
    Top = 296
  end
  object qryTax: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryTaxAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R0071401'
      end>
    SQL.Strings = (
      
        'SELECT KEYY,SEQ,BILL_NO,BILL_DATE,BILL_AMOUNT,BILL_AMOUNT_UNIT,T' +
        'AX_AMOUNT,TAX_AMOUNT_UNIT'
      'FROM  LOCRC1_TAX '
      'WHERE KEYY = :MAINT_NO')
    Left = 16
    Top = 296
    object qryTaxKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryTaxBILL_NO: TStringField
      FieldName = 'BILL_NO'
      Size = 35
    end
    object qryTaxBILL_DATE: TStringField
      FieldName = 'BILL_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryTaxBILL_AMOUNT: TBCDField
      FieldName = 'BILL_AMOUNT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryTaxBILL_AMOUNT_UNIT: TStringField
      FieldName = 'BILL_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxTAX_AMOUNT: TBCDField
      FieldName = 'TAX_AMOUNT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryTaxTAX_AMOUNT_UNIT: TStringField
      FieldName = 'TAX_AMOUNT_UNIT'
      Size = 3
    end
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = Letter
    Left = 16
    Top = 328
  end
end
