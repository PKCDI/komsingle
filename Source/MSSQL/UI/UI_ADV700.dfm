inherited UI_ADV700_frm: TUI_ADV700_frm
  Left = 2287
  Top = 21
  Caption = #49688#52636#49888#50857#51109#53685#51648#49436
  ClientWidth = 1114
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter3: TsSplitter [1]
    Left = 0
    Top = 76
    Width = 1114
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel1: TsPanel [2]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton2: TsSpeedButton
      Left = 398
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 121
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel
      Left = 8
      Top = 5
      Width = 109
      Height = 17
      Caption = #49688#52636#49888#50857#51109' '#53685#51648#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel
      Left = 8
      Top = 20
      Width = 41
      Height = 13
      Caption = 'ADV700'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object btnExit: TsButton
      Left = 1034
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Left = 131
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object sButton4: TsButton
      Left = 198
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton2: TsButton
      Tag = 1
      Left = 297
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 3
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [3]
    Left = 0
    Top = 44
    Width = 1114
    Height = 32
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 722
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 5
      TabStop = False
      Visible = False
      ImageIndex = 9
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object mask_DATEE: TsMaskEdit
      Left = 254
      Top = 5
      Width = 83
      Height = 23
      Color = clWhite
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49688#49888#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_MaintNo: TsEdit
      Left = 64
      Top = 5
      Width = 121
      Height = 21
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clBlack
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn2: TsBitBtn
      Tag = 1
      Left = 674
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 7
      TabStop = False
      Visible = False
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 521
      Top = 5
      Width = 32
      Height = 21
      Hint = #44592#45733#54364#49884
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_UserNo: TsEdit
      Left = 393
      Top = 5
      Width = 57
      Height = 21
      TabStop = False
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sCheckBox1: TsCheckBox
      Left = 880
      Top = 6
      Width = 64
      Height = 16
      TabStop = False
      Caption = #46356#48260#44536
      TabOrder = 6
      Visible = False
      ImgChecked = 0
      ImgUnchecked = 0
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 601
      Top = 5
      Width = 32
      Height = 21
      Hint = #51025#45813#50976#54805
      Color = clWhite
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn3: TsBitBtn
      Tag = 2
      Left = 698
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 8
      TabStop = False
      Visible = False
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sPageControl1: TsPageControl [4]
    Left = 0
    Top = 79
    Width = 1114
    Height = 602
    ActivePage = sTabSheet3
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 2
    TabOrder = 2
    TabStop = False
    OnChange = sPageControl1Change
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = 'page1'
      object sSplitter4: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel5: TsPanel
        Left = 0
        Top = 2
        Width = 241
        Height = 560
        Align = alLeft
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object sPanel2: TsPanel
        Left = 241
        Top = 2
        Width = 865
        Height = 560
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          865
          560)
        object sSpeedButton1: TsSpeedButton
          Left = 426
          Top = -15
          Width = 10
          Height = 591
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object mask_APPDATE: TsMaskEdit
          Left = 158
          Top = 23
          Width = 83
          Height = 23
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #53685#51648#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPNO: TsEdit
          Left = 158
          Top = 46
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #53685#51648#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel18: TsPanel
          Left = 31
          Top = 2
          Width = 370
          Height = 20
          Caption = #53685#51648#51068#51088
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sPanel19: TsPanel
          Left = 31
          Top = 87
          Width = 370
          Height = 20
          Caption = #44060#49444#51008#54665
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_APBANK: TsEdit
          Left = 158
          Top = 108
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          Text = '0123456789'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APBANK1: TsEdit
          Left = 158
          Top = 129
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APBANK2: TsEdit
          Left = 158
          Top = 150
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APBANK3: TsEdit
          Left = 158
          Top = 171
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APBANK4: TsEdit
          Left = 158
          Top = 192
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel20: TsPanel
          Left = 31
          Top = 233
          Width = 370
          Height = 20
          Caption = #53685#51648#51008#54665
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_ADBANK: TsEdit
          Left = 158
          Top = 254
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #53685#51648#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_ADBANK1: TsEdit
          Left = 158
          Top = 275
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ADBANK2: TsEdit
          Left = 158
          Top = 296
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ADBANK3: TsEdit
          Left = 158
          Top = 317
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ADBANK4: TsEdit
          Left = 158
          Top = 338
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ADBANK5: TsEdit
          Left = 158
          Top = 359
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel21: TsPanel
          Left = 31
          Top = 400
          Width = 370
          Height = 20
          Caption = #44592#53440#51221#48372'('#48512#44032#51221#48372')'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object memo_ADDINFO1: TsMemo
          Left = 31
          Top = 421
          Width = 370
          Height = 117
          Color = clWhite
          Lines.Strings = (
            'memo_ADDINFO1')
          TabOrder = 17
          Text = 'memo_ADDINFO1'
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel22: TsPanel
          Left = 461
          Top = 2
          Width = 370
          Height = 20
          Caption = #49888#50857#51109
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_DOCCD1: TsEdit
          Left = 588
          Top = 24
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109#51333#47448
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DOCCD2: TsEdit
          Left = 588
          Top = 45
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_DOCCD3: TsEdit
          Left = 588
          Top = 66
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CDNO: TsEdit
          Left = 588
          Top = 87
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APPLICABLERULES1: TsEdit
          Left = 588
          Top = 108
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'Applicable Rules'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APPLICABLERULES2: TsEdit
          Left = 588
          Top = 129
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_REEPRE: TsEdit
          Left = 588
          Top = 150
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#53685#51648' '#52280#51312#49324#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object mask_ISSDATE: TsMaskEdit
          Left = 588
          Top = 171
          Width = 83
          Height = 23
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object mask_EXDATE: TsMaskEdit
          Left = 588
          Top = 194
          Width = 83
          Height = 23
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 27
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50976#54952#44592#51068#13#10'(Date of Expiry)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object edt_EXPLACE: TsEdit
          Left = 588
          Top = 217
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50976#54952#51109#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel23: TsPanel
          Left = 461
          Top = 258
          Width = 370
          Height = 20
          Caption = #44060#49444#51032#47280#51064' '#51008#54665'(Applicant Bank)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_APPBANK: TsEdit
          Left = 588
          Top = 279
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APPBANK1: TsEdit
          Left = 588
          Top = 300
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 31
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APPBANK2: TsEdit
          Left = 588
          Top = 321
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPBANK3: TsEdit
          Left = 588
          Top = 342
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 33
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPBANK4: TsEdit
          Left = 588
          Top = 363
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPACCNT: TsEdit
          Left = 588
          Top = 384
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel24: TsPanel
          Left = 461
          Top = 425
          Width = 370
          Height = 20
          Caption = #44060#49444#51032#47280#51064
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_APPLIC1: TsEdit
          Left = 588
          Top = 446
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51032#47280#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APPLIC2: TsEdit
          Left = 588
          Top = 467
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 38
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPLIC3: TsEdit
          Left = 588
          Top = 488
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 39
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_APPLIC4: TsEdit
          Left = 588
          Top = 509
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = 'Page2'
      object sSplitter7: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel6: TsPanel
        Left = 0
        Top = 2
        Width = 241
        Height = 560
        Align = alLeft
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object sPanel7: TsPanel
        Left = 241
        Top = 2
        Width = 865
        Height = 560
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          865
          560)
        object sSpeedButton6: TsSpeedButton
          Left = 426
          Top = -31
          Width = 10
          Height = 591
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sLabel1: TsLabel
          Left = 306
          Top = 238
          Width = 23
          Height = 20
          Caption = '(%)'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sPanel26: TsPanel
          Left = 31
          Top = 2
          Width = 370
          Height = 20
          Caption = #49688#51061#51088'(Beneficiary)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_BENEFC3: TsEdit
          Left = 158
          Top = 65
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BENEFC6: TsEdit
          Left = 158
          Top = 128
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BENEFC2: TsEdit
          Left = 158
          Top = 44
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BENEFC5: TsEdit
          Left = 158
          Top = 107
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_BENEFC4: TsEdit
          Left = 158
          Top = 86
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel27: TsPanel
          Left = 31
          Top = 185
          Width = 370
          Height = 20
          Caption = #44552#50529#51221#48372
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sPanel28: TsPanel
          Left = 31
          Top = 326
          Width = 370
          Height = 20
          Caption = #49888#50857#51109' '#51648#44553#48169#49885' '#48143' '#51008#54665'(Available with...by...)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_BENEFC1: TsEdit
          Left = 158
          Top = 23
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#51061#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_CDCUR: TsEdit
          Left = 158
          Top = 211
          Width = 35
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#44552#50529#13#10'(Currency Code , Amout)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object curr_CDAMT: TsCurrencyEdit
          Left = 194
          Top = 211
          Width = 135
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
        end
        object edt_AVPAY: TsEdit
          Left = 158
          Top = 347
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          Text = '0123456789'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51648#44553#48169#49885
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AVAIL: TsEdit
          Left = 158
          Top = 368
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AVAIL1: TsEdit
          Left = 158
          Top = 389
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 13
          Text = '0123456789'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AVAIL4: TsEdit
          Left = 158
          Top = 452
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AVAIL3: TsEdit
          Left = 158
          Top = 431
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AVAIL2: TsEdit
          Left = 158
          Top = 410
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AVACCNT: TsEdit
          Left = 158
          Top = 473
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 17
          Text = '0123456789'
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel30: TsPanel
          Left = 461
          Top = 2
          Width = 370
          Height = 20
          Caption = #48512#44032#44552#50529#48512#45812'(Additional Amount Coverd)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_AACV1: TsEdit
          Left = 566
          Top = 23
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48512#44032#44552#50529#48512#45812
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AACV2: TsEdit
          Left = 566
          Top = 44
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AACV3: TsEdit
          Left = 566
          Top = 65
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AACV4: TsEdit
          Left = 566
          Top = 86
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel29: TsPanel
          Left = 461
          Top = 133
          Width = 370
          Height = 20
          Caption = #54868#54872#50612#51020' '#51312#44148'(Draft at...)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_DRAFT1: TsEdit
          Left = 566
          Top = 154
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54868#54872#50612#51020#51312#44148
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DRAFT2: TsEdit
          Left = 566
          Top = 175
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_DRAFT3: TsEdit
          Left = 566
          Top = 196
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_DRAFT4: TsEdit
          Left = 566
          Top = 217
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel31: TsPanel
          Left = 461
          Top = 266
          Width = 370
          Height = 20
          Caption = #54844#54633#51648#44553#51312#44148#47749#49464'(Mixed payment Details)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_MIXPAY1: TsEdit
          Left = 566
          Top = 287
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54844#54633#51648#44553#51312#44148#47749#49464
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_MIXPAY3: TsEdit
          Left = 566
          Top = 329
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_MIXPAY2: TsEdit
          Left = 566
          Top = 308
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 31
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_MIXPAY4: TsEdit
          Left = 566
          Top = 350
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel32: TsPanel
          Left = 461
          Top = 399
          Width = 370
          Height = 20
          Caption = #50672#51648#44553#51312#44148#47749#49464'(Deferred Payment Details)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 33
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_DEFPAY1: TsEdit
          Left = 566
          Top = 420
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50672#51648#44553#51312#44148#47749#49464
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DEFPAY2: TsEdit
          Left = 566
          Top = 441
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_DEFPAY3: TsEdit
          Left = 566
          Top = 462
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_DEFPAY4: TsEdit
          Left = 566
          Top = 483
          Width = 265
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object curr_CDPERP: TsCurrencyEdit
          Left = 222
          Top = 238
          Width = 35
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 38
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44284#48512#51313' '#54728#50857#50984#13#10'(Percentage Credit Amount Tolenrance)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
        end
        object curr_CDPERM: TsCurrencyEdit
          Left = 270
          Top = 238
          Width = 35
          Height = 21
          AutoSize = False
          Color = clWhite
          TabOrder = 39
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '/'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -13
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
        end
        object edt_CDMAX: TsEdit
          Left = 158
          Top = 263
          Width = 171
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#45824' '#49888#50857#51109' '#44552#50529#13#10'(Maximum Credit Amount)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = 'Page3'
      object sSplitter5: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel8: TsPanel
        Left = 0
        Top = 2
        Width = 241
        Height = 560
        Align = alLeft
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object sPanel9: TsPanel
        Left = 241
        Top = 2
        Width = 865
        Height = 560
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          865
          560)
        object sSpeedButton7: TsSpeedButton
          Left = 426
          Top = -31
          Width = 10
          Height = 591
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sLabel2: TsLabel
          Left = 159
          Top = 271
          Width = 247
          Height = 12
          Alignment = taRightJustify
          Caption = '(Place of Taking in Charge/Dispatch from '#183#183#183'/Place of Receipt)'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel3: TsLabel
          Left = 131
          Top = 304
          Width = 274
          Height = 12
          Alignment = taRightJustify
          Caption = 
            '(Place of Final Destination/For Transportation to '#183#183#183'/Place of D' +
            'elivery)'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel4: TsLabel
          Left = 157
          Top = 337
          Width = 152
          Height = 12
          Alignment = taRightJustify
          Caption = '(Port of Loading/Airport of Departure)'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel5: TsLabel
          Left = 156
          Top = 370
          Width = 165
          Height = 12
          Alignment = taRightJustify
          Caption = '(Port of Discharge/Airport of Destination)'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sPanel33: TsPanel
          Left = 31
          Top = 2
          Width = 370
          Height = 20
          Caption = #50612#51020#51648#44553#51064'(Drawee)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_DRAWEE: TsEdit
          Left = 158
          Top = 23
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DRAWEE1: TsEdit
          Left = 158
          Top = 44
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DRAWEE2: TsEdit
          Left = 158
          Top = 65
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_DRAWEE3: TsEdit
          Left = 158
          Top = 86
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_DRAWEE4: TsEdit
          Left = 158
          Top = 107
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_DRACCNT: TsEdit
          Left = 158
          Top = 128
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel34: TsPanel
          Left = 31
          Top = 164
          Width = 370
          Height = 20
          Caption = #49440#51201#51221#48372
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_PSHIP: TsEdit
          Left = 280
          Top = 206
          Width = 121
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48516#54624#49440#51201#54728#50857#50668#48512'(Partial Shipment)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_TSHIP: TsEdit
          Left = 280
          Top = 185
          Width = 121
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54872#51201#54728#50857#50668#48512'(Transhipment)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object mask_LSTDATE: TsMaskEdit
          Left = 280
          Top = 227
          Width = 83
          Height = 23
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#51333#49440#51201#51068#51088'(Latest Date Of Shipment)'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object edt_LOADON: TsEdit
          Left = 158
          Top = 250
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#53441'('#48156#49569')'#51648
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_FORTRAN: TsEdit
          Left = 158
          Top = 283
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#51333#46020#52265#51648
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_SUNJUCKPORT: TsEdit
          Left = 158
          Top = 316
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DOCHACKPORT: TsEdit
          Left = 158
          Top = 349
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #46020#52265#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel35: TsPanel
          Left = 31
          Top = 397
          Width = 370
          Height = 20
          Caption = #49440#51201#44592#44036'(Shipment Period)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_SHIPPD4: TsEdit
          Left = 158
          Top = 481
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SHIPPD5: TsEdit
          Left = 158
          Top = 502
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SHIPPD6: TsEdit
          Left = 158
          Top = 523
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SHIPPD1: TsEdit
          Left = 158
          Top = 418
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#44592#44036
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_SHIPPD2: TsEdit
          Left = 158
          Top = 439
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SHIPPD3: TsEdit
          Left = 158
          Top = 460
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel36: TsPanel
          Left = 461
          Top = 2
          Width = 370
          Height = 20
          Caption = #49688#49688#47308#48512#45812#51088'(Charges)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_CHARGE1: TsEdit
          Left = 576
          Top = 23
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#49688#47308#48512#45812#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_CHARGE2: TsEdit
          Left = 576
          Top = 44
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CHARGE3: TsEdit
          Left = 576
          Top = 65
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CHARGE4: TsEdit
          Left = 576
          Top = 86
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CHARGE5: TsEdit
          Left = 576
          Top = 107
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CHARGE6: TsEdit
          Left = 576
          Top = 128
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel37: TsPanel
          Left = 461
          Top = 164
          Width = 370
          Height = 20
          Caption = #49436#47448#51228#49884#44592#44036'(Period for Presentation)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_PDPRSNT1: TsEdit
          Left = 576
          Top = 185
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49436#47448#51228#49884#44592#44036
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_PDPRSNT2: TsEdit
          Left = 576
          Top = 206
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 31
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_PDPRSNT3: TsEdit
          Left = 576
          Top = 227
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_PDPRSNT4: TsEdit
          Left = 576
          Top = 248
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 33
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel38: TsPanel
          Left = 461
          Top = 292
          Width = 370
          Height = 20
          Caption = #54869#51064#51648#49884#47928#50616'(Confirmation Instructions)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_CONFIRMM: TsEdit
          Left = 576
          Top = 313
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54869#51064#51648#49884#47928#50616
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel39: TsPanel
          Left = 461
          Top = 397
          Width = 370
          Height = 20
          Caption = #49345#54872#51008#54665'(Reimbursement Bank)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_REIBANK: TsEdit
          Left = 576
          Top = 418
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_REIBANK1: TsEdit
          Left = 576
          Top = 439
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 38
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_REIBANK2: TsEdit
          Left = 576
          Top = 460
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 39
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_REIBANK3: TsEdit
          Left = 576
          Top = 481
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_REIBANK4: TsEdit
          Left = 576
          Top = 502
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 41
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_REIACCMT: TsEdit
          Left = 576
          Top = 523
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 42
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = 'Page4'
      object sSplitter8: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel10: TsPanel
        Left = 0
        Top = 2
        Width = 241
        Height = 560
        Align = alLeft
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object sPanel11: TsPanel
        Left = 241
        Top = 2
        Width = 865
        Height = 560
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        object memo_DESGOOD1: TsMemo
          Left = 115
          Top = 26
          Width = 633
          Height = 100
          Color = clWhite
          Lines.Strings = (
            'sMemo1')
          TabOrder = 0
          Text = 'sMemo1'
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel40: TsPanel
          Left = 115
          Top = 5
          Width = 633
          Height = 20
          Caption = #49345#54408'('#50857#50669')'#47749#49464'(Descript of Goods and /or Services)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sPanel41: TsPanel
          Left = 115
          Top = 146
          Width = 633
          Height = 20
          Caption = #44396#48708#49436#47448'(Documents Required)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object memo_DOCREQU1: TsMemo
          Left = 116
          Top = 167
          Width = 633
          Height = 100
          Color = clWhite
          Lines.Strings = (
            'sMemo1')
          TabOrder = 3
          Text = 'sMemo1'
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel42: TsPanel
          Left = 115
          Top = 287
          Width = 633
          Height = 20
          Caption = #48512#44032#51312#44148'(Additional Conditions)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object memo_ADDCOND1: TsMemo
          Left = 116
          Top = 308
          Width = 633
          Height = 100
          Color = clWhite
          Lines.Strings = (
            'sMemo1')
          TabOrder = 5
          Text = 'sMemo1'
          SkinData.SkinSection = 'EDIT'
        end
        object memo_INSTRCT1: TsMemo
          Left = 116
          Top = 449
          Width = 633
          Height = 100
          Color = clWhite
          Lines.Strings = (
            'sMemo1')
          TabOrder = 6
          Text = 'sMemo1'
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel43: TsPanel
          Left = 115
          Top = 428
          Width = 633
          Height = 20
          Caption = 
            #47588#51077'/'#51648#44553'/'#51064#49688#51008#54665#50640' '#45824#54620' '#51648#49884#49324#54637'(Instructions to the Paying/Accepting/Negotia' +
            'ting Bank)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
      end
    end
    object sTabSheet5: TsTabSheet
      Caption = 'Page5'
      object sSplitter6: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel12: TsPanel
        Left = 0
        Top = 2
        Width = 241
        Height = 560
        Align = alLeft
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object sPanel13: TsPanel
        Left = 241
        Top = 2
        Width = 865
        Height = 560
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          865
          560)
        object sSpeedButton8: TsSpeedButton
          Left = 426
          Top = 1
          Width = 10
          Height = 353
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel15: TsPanel
          Left = 31
          Top = 2
          Width = 370
          Height = 20
          Caption = #52572#51333#53685#51648#51008#54665'(Advise Through Bank)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_AVTBANK: TsEdit
          Left = 158
          Top = 23
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AVTBANK1: TsEdit
          Left = 158
          Top = 44
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AVTBANK2: TsEdit
          Left = 158
          Top = 65
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AVTBANK3: TsEdit
          Left = 158
          Top = 86
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AVTBANK4: TsEdit
          Left = 158
          Top = 107
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AVTACCNT: TsEdit
          Left = 158
          Top = 128
          Width = 243
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel16: TsPanel
          Left = 461
          Top = 2
          Width = 370
          Height = 20
          Caption = #49688#49888#50526#51008#54665#51221#48372'(Sender to Receiver Information)'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_SNDINFO1: TsEdit
          Left = 576
          Top = 23
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SNDINFO2: TsEdit
          Left = 576
          Top = 44
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SNDINFO3: TsEdit
          Left = 576
          Top = 65
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SNDINFO4: TsEdit
          Left = 576
          Top = 86
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SNDINFO5: TsEdit
          Left = 576
          Top = 107
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_SNDINFO6: TsEdit
          Left = 576
          Top = 128
          Width = 255
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel17: TsPanel
          Left = 31
          Top = 153
          Width = 370
          Height = 20
          Caption = #53685#51648#51008#54665
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object edt_EXNAME1: TsEdit
          Left = 158
          Top = 174
          Width = 81
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 11
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47749#51032#51064
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_EXNAME2: TsEdit
          Left = 158
          Top = 195
          Width = 251
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_EXNAME3: TsEdit
          Left = 158
          Top = 216
          Width = 251
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_EXADDR1: TsEdit
          Left = 158
          Top = 237
          Width = 251
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_EXADDR2: TsEdit
          Left = 158
          Top = 258
          Width = 251
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
      end
      object sPanel14: TsPanel
        Left = 244
        Top = 286
        Width = 862
        Height = 277
        TabOrder = 2
        SkinData.SkinSection = 'PANEL'
        object sSplitter2: TsSplitter
          Left = 1
          Top = 1
          Width = 860
          Height = 3
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        object detailDBGrid: TsDBGrid
          Left = 1
          Top = 4
          Width = 860
          Height = 88
          Align = alTop
          Color = clWhite
          DataSource = dsDetail
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clBlack
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 35
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME_COD'
              Title.Alignment = taCenter
              Title.Caption = #51228#54408#53076#46300
              Width = 183
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 110
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTY_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTYG'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032#44592#51456#49688#47049
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTYG_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRICE'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'PRICE_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT'
              Title.Alignment = taCenter
              Title.Caption = #44552#50529
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'AMT_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 52
              Visible = True
            end>
        end
        object memo_SZIE1: TsDBMemo
          Left = 58
          Top = 120
          Width = 345
          Height = 77
          Color = clWhite
          Ctl3D = True
          DataField = 'SIZE1'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45236#50669
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          CharCase = ecUpperCase
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object memo_NAME1: TsDBMemo
          Left = 58
          Top = 198
          Width = 345
          Height = 75
          Color = clWhite
          Ctl3D = True
          DataField = 'NAME1'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54408#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          CharCase = ecUpperCase
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_QTY_G: TsDBEdit
          Tag = 502
          Left = 547
          Top = 96
          Width = 36
          Height = 23
          Hint = #45800#50948
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          DataField = 'QTY_G'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#47049
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_PRICEG: TsDBEdit
          Tag = 503
          Left = 547
          Top = 120
          Width = 36
          Height = 23
          Hint = #53685#54868
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          DataField = 'PRICE_G'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45800#44032
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_QTY: TsDBEdit
          Left = 584
          Top = 96
          Width = 143
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          DataField = 'QTY'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PRICE: TsDBEdit
          Left = 584
          Top = 120
          Width = 143
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          DataField = 'PRICE'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_QTYG_G: TsDBEdit
          Tag = 504
          Left = 547
          Top = 144
          Width = 36
          Height = 23
          Hint = #45800#50948
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          DataField = 'QTYG_G'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45800#44032#44592#51456#49688#47049
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_AMTG: TsDBEdit
          Tag = 505
          Left = 547
          Top = 168
          Width = 36
          Height = 23
          Hint = #53685#54868
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          DataField = 'AMT_G'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_QTYG: TsDBEdit
          Left = 584
          Top = 144
          Width = 143
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          DataField = 'QTYG'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_AMT: TsDBEdit
          Left = 584
          Top = 168
          Width = 143
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          DataField = 'AMT'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_NAMECOD: TsDBEdit
          Tag = 502
          Left = 58
          Top = 96
          Width = 345
          Height = 23
          Hint = #45800#50948
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          DataField = 'NAME_COD'
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = 4013373
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ImeName = 'Microsoft IME 2010'
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51228#54408#53076#46300
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
      end
    end
    object sTabSheet9: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter9: TsSplitter
        Left = 0
        Top = 0
        Width = 1106
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 2
        Width = 1106
        Height = 33
        Align = alTop
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object edt_SearchText: TsEdit
          Left = 103
          Top = 5
          Width = 233
          Height = 23
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 101
          Height = 23
          Alignment = taLeftJustify
          SkinData.SkinSection = 'COMBOBOX'
          VerticalAlignment = taAlignTop
          Style = csDropDownList
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ItemHeight = 17
          ItemIndex = 0
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #44060#49444#51032#47280#51064)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 102
          Top = 5
          Width = 82
          Height = 23
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn1: TsBitBtn
          Left = 336
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          OnClick = sBitBtn1Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn21: TsBitBtn
          Tag = 902
          Left = 183
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 230
          Top = 5
          Width = 82
          Height = 23
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn23: TsBitBtn
          Tag = 903
          Left = 312
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = sBitBtn21Click
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel25: TsPanel
          Left = 206
          Top = 5
          Width = 25
          Height = 23
          Caption = '~'
          TabOrder = 7
          SkinData.SkinSection = 'PANEL'
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 35
        Width = 1106
        Height = 527
        TabStop = False
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid8DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 220
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ISS_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'EX_DATE'
            Title.Alignment = taCenter
            Title.Caption = #50976#54952#44592#51068
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CD_AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CD_CUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'APPLIC1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51032#47280#51064
            Width = 220
            Visible = True
          end>
      end
    end
  end
  object sPanel44: TsPanel [5]
    Left = 4
    Top = 118
    Width = 241
    Height = 560
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    object sPanel57: TsPanel
      Left = 1
      Top = 1
      Width = 239
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      object Mask_fromDate: TsMaskEdit
        Left = 64
        Top = 5
        Width = 72
        Height = 23
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20160101'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn60: TsBitBtn
        Left = 210
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = sBitBtn60Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object mask_toDate: TsMaskEdit
        Left = 137
        Top = 5
        Width = 73
        Height = 23
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20171231'
        CheckOnExit = True
        SkinData.SkinSection = 'EDIT'
      end
    end
    object sDBGrid8: TsDBGrid
      Left = 1
      Top = 34
      Width = 239
      Height = 525
      TabStop = False
      Align = alClient
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #44404#47548#52404
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid8DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 147
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 176
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT '
      
        #9'ADV700.MAINT_NO, ADV700.Chk1, ADV700.Chk2, ADV700.Chk3, ADV700.' +
        'MESSAGE1, ADV700.MESSAGE2, ADV700.USER_ID, ADV700.DATEE, ADV700.' +
        'APP_DATE, ADV700.APP_NO, ADV700.AP_BANK, ADV700.AP_BANK1, ADV700' +
        '.AP_BANK2, ADV700.AP_BANK3, ADV700.AP_BANK4, ADV700.AP_BANK5, '
      
        #9'ADV700.AD_BANK, ADV700.AD_BANK1, ADV700.AD_BANK2, ADV700.AD_BAN' +
        'K3, ADV700.AD_BANK4, ADV700.AD_BANK5, ADV700.ADDINFO, ADV700.ADD' +
        'INFO_1, ADV700.DOC_CD1, ADV700.DOC_CD2, ADV700.DOC_CD3, ADV700.C' +
        'D_NO, ADV700.REE_PRE, ADV700.ISS_DATE, ADV700.EX_DATE, '
      
        #9'ADV700.EX_PLACE, ADV700.APP_BANK, ADV700.APP_BANK1, ADV700.APP_' +
        'BANK2, ADV700.APP_BANK3, ADV700.APP_BANK4, ADV700.APP_BANK5, ADV' +
        '700.APP_ACCNT, ADV700.APPLIC1, ADV700.APPLIC2, ADV700.APPLIC3, A' +
        'DV700.APPLIC4, ADV700.APPLIC5, '
      
        #9'ADV700.BENEFC1, ADV700.BENEFC2, ADV700.BENEFC3, ADV700.BENEFC4,' +
        ' ADV700.BENEFC5, ADV700.CD_AMT, ADV700.CD_CUR, ADV700.CD_PERP, A' +
        'DV700.CD_PERM, ADV700.CD_MAX, ADV700.AA_CV1, ADV700.AA_CV2, ADV7' +
        '00.AA_CV3, ADV700.AA_CV4, ADV700.BENEFC6, ADV700.PRNO,'
      #9
      
        #9'ADV7001.MAINT_NO, ADV7001.AVAIL, ADV7001.AVAIL1, ADV7001.AVAIL2' +
        ', ADV7001.AVAIL3, ADV7001.AVAIL4, ADV7001.AVAIL5, ADV7001.AV_ACC' +
        'NT, ADV7001.AV_PAY, ADV7001.DRAFT1, ADV7001.DRAFT2, ADV7001.DRAF' +
        'T3, ADV7001.DRAFT4, '
      
        #9'ADV7001.DRAWEE, ADV7001.DRAWEE1, ADV7001.DRAWEE2, ADV7001.DRAWE' +
        'E3, ADV7001.DRAWEE4, ADV7001.DRAWEE5, ADV7001.DR_ACCNT, ADV7001.' +
        'MIX_PAY1, ADV7001.MIX_PAY2, ADV7001.MIX_PAY3, ADV7001.MIX_PAY4, ' +
        'ADV7001.DEF_PAY1, ADV7001.DEF_PAY2, ADV7001.DEF_PAY3, ADV7001.DE' +
        'F_PAY4, '
      
        #9'ADV7001.LST_DATE, ADV7001.SHIP_PD, ADV7001.SHIP_PD1, ADV7001.SH' +
        'IP_PD2, ADV7001.SHIP_PD3, ADV7001.SHIP_PD4, ADV7001.SHIP_PD5, AD' +
        'V7001.SHIP_PD6,'
      ''
      
        #9'ADV7002.MAINT_NO, ADV7002.DESGOOD, ADV7002.DESGOOD_1, ADV7002.D' +
        'OCREQU, ADV7002.DOCREQU_1, ADV7002.ADDCOND, ADV7002.ADDCOND_1, '
      
        #9'ADV7002.CHARGE1, ADV7002.CHARGE2, ADV7002.CHARGE3, ADV7002.CHAR' +
        'GE4, ADV7002.CHARGE5, ADV7002.CHARGE6, ADV7002.PD_PRSNT1, ADV700' +
        '2.PD_PRSNT2, ADV7002.PD_PRSNT3, ADV7002.PD_PRSNT4, '
      
        #9'ADV7002.CONFIRMM, ADV7002.REI_BANK, ADV7002.REI_BANK1, ADV7002.' +
        'REI_BANK2, ADV7002.REI_BANK3, ADV7002.REI_BANK4, ADV7002.REI_BAN' +
        'K5, ADV7002.REI_ACCNT, ADV7002.INSTRCT, ADV7002.INSTRCT_1, ADV70' +
        '02.AVT_BANK, ADV7002.AVT_BANK1, ADV7002.AVT_BANK2, ADV7002.AVT_B' +
        'ANK3, ADV7002.AVT_BANK4, ADV7002.AVT_BANK5, '
      
        #9'ADV7002.AVT_ACCNT, ADV7002.SND_INFO1, ADV7002.SND_INFO2, ADV700' +
        '2.SND_INFO3, ADV7002.SND_INFO4, ADV7002.SND_INFO5, ADV7002.SND_I' +
        'NFO6, ADV7002.EX_NAME1, ADV7002.EX_NAME2, ADV7002.EX_NAME3, ADV7' +
        '002.EX_ADDR1, ADV7002.EX_ADDR2, ADV7002.CHK1, ADV7002.CHK2, ADV7' +
        '002.CHK3, ADV7002.PRNO,'
      #9
      
        #9'ADV7003.MAINT_NO, ADV7003.FOR_TRAN, ADV7003.LOAD_ON, ADV7003.TS' +
        'HIP, ADV7003.PSHIP, ADV7003.APPLICABLE_RULES_1, ADV7003.APPLICAB' +
        'LE_RULES_2, ADV7003.SUNJUCK_PORT, ADV7003.DOCHACK_PORT'
      ''
      ''
      'FROM ADV700 AS ADV700'
      
        'INNER JOIN ADV7001 AS ADV7001 ON ADV700.MAINT_NO = ADV7001.MAINT' +
        '_NO'
      
        'INNER JOIN ADV7002 AS ADV7002 ON ADV700.MAINT_NO = ADV7002.MAINT' +
        '_NO'
      
        'INNER JOIN ADV7003 AS ADV7003 ON ADV700.MAINT_NO = ADV7003.MAINT' +
        '_NO')
    Left = 24
    Top = 208
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListChk1: TBooleanField
      FieldName = 'Chk1'
    end
    object qryListChk2: TStringField
      FieldName = 'Chk2'
      Size = 1
    end
    object qryListChk3: TStringField
      FieldName = 'Chk3'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_NO: TStringField
      FieldName = 'APP_NO'
      Size = 35
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_BANK5: TStringField
      FieldName = 'AD_BANK5'
      Size = 35
    end
    object qryListADDINFO: TStringField
      FieldName = 'ADDINFO'
      Size = 1
    end
    object qryListADDINFO_1: TMemoField
      FieldName = 'ADDINFO_1'
      BlobType = ftMemo
    end
    object qryListDOC_CD1: TStringField
      FieldName = 'DOC_CD1'
      Size = 35
    end
    object qryListDOC_CD2: TStringField
      FieldName = 'DOC_CD2'
      Size = 35
    end
    object qryListDOC_CD3: TStringField
      FieldName = 'DOC_CD3'
      Size = 65
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListREE_PRE: TStringField
      FieldName = 'REE_PRE'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListAPP_BANK: TStringField
      FieldName = 'APP_BANK'
      Size = 11
    end
    object qryListAPP_BANK1: TStringField
      FieldName = 'APP_BANK1'
      Size = 35
    end
    object qryListAPP_BANK2: TStringField
      FieldName = 'APP_BANK2'
      Size = 35
    end
    object qryListAPP_BANK3: TStringField
      FieldName = 'APP_BANK3'
      Size = 35
    end
    object qryListAPP_BANK4: TStringField
      FieldName = 'APP_BANK4'
      Size = 35
    end
    object qryListAPP_BANK5: TStringField
      FieldName = 'APP_BANK5'
      Size = 35
    end
    object qryListAPP_ACCNT: TStringField
      FieldName = 'APP_ACCNT'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListCD_AMT: TBCDField
      FieldName = 'CD_AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListCD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 70
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListBENEFC6: TStringField
      FieldName = 'BENEFC6'
      Size = 35
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListAVAIL: TStringField
      FieldName = 'AVAIL'
      Size = 11
    end
    object qryListAVAIL1: TStringField
      FieldName = 'AVAIL1'
      Size = 35
    end
    object qryListAVAIL2: TStringField
      FieldName = 'AVAIL2'
      Size = 35
    end
    object qryListAVAIL3: TStringField
      FieldName = 'AVAIL3'
      Size = 35
    end
    object qryListAVAIL4: TStringField
      FieldName = 'AVAIL4'
      Size = 35
    end
    object qryListAVAIL5: TStringField
      FieldName = 'AVAIL5'
      Size = 35
    end
    object qryListAV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Size = 35
    end
    object qryListAV_PAY: TStringField
      FieldName = 'AV_PAY'
      Size = 35
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryListDRAFT4: TStringField
      FieldName = 'DRAFT4'
      Size = 35
    end
    object qryListDRAWEE: TStringField
      FieldName = 'DRAWEE'
      Size = 11
    end
    object qryListDRAWEE1: TStringField
      FieldName = 'DRAWEE1'
      Size = 35
    end
    object qryListDRAWEE2: TStringField
      FieldName = 'DRAWEE2'
      Size = 35
    end
    object qryListDRAWEE3: TStringField
      FieldName = 'DRAWEE3'
      Size = 35
    end
    object qryListDRAWEE4: TStringField
      FieldName = 'DRAWEE4'
      Size = 35
    end
    object qryListDRAWEE5: TStringField
      FieldName = 'DRAWEE5'
      Size = 35
    end
    object qryListDR_ACCNT: TStringField
      FieldName = 'DR_ACCNT'
      Size = 35
    end
    object qryListMIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryListMIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryListMIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryListMIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryListDEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryListDEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryListDEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryListDEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD: TStringField
      FieldName = 'SHIP_PD'
      Size = 1
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListDESGOOD: TStringField
      FieldName = 'DESGOOD'
      Size = 1
    end
    object qryListDESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryListDOCREQU: TStringField
      FieldName = 'DOCREQU'
      Size = 1
    end
    object qryListDOCREQU_1: TMemoField
      FieldName = 'DOCREQU_1'
      BlobType = ftMemo
    end
    object qryListADDCOND: TStringField
      FieldName = 'ADDCOND'
      Size = 1
    end
    object qryListADDCOND_1: TMemoField
      FieldName = 'ADDCOND_1'
      BlobType = ftMemo
    end
    object qryListCHARGE1: TStringField
      FieldName = 'CHARGE1'
      Size = 35
    end
    object qryListCHARGE2: TStringField
      FieldName = 'CHARGE2'
      Size = 35
    end
    object qryListCHARGE3: TStringField
      FieldName = 'CHARGE3'
      Size = 35
    end
    object qryListCHARGE4: TStringField
      FieldName = 'CHARGE4'
      Size = 35
    end
    object qryListCHARGE5: TStringField
      FieldName = 'CHARGE5'
      Size = 35
    end
    object qryListCHARGE6: TStringField
      FieldName = 'CHARGE6'
      Size = 35
    end
    object qryListPD_PRSNT1: TStringField
      FieldName = 'PD_PRSNT1'
      Size = 35
    end
    object qryListPD_PRSNT2: TStringField
      FieldName = 'PD_PRSNT2'
      Size = 35
    end
    object qryListPD_PRSNT3: TStringField
      FieldName = 'PD_PRSNT3'
      Size = 35
    end
    object qryListPD_PRSNT4: TStringField
      FieldName = 'PD_PRSNT4'
      Size = 35
    end
    object qryListCONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 35
    end
    object qryListREI_BANK: TStringField
      FieldName = 'REI_BANK'
      Size = 11
    end
    object qryListREI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Size = 35
    end
    object qryListREI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Size = 35
    end
    object qryListREI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Size = 35
    end
    object qryListREI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Size = 35
    end
    object qryListREI_BANK5: TStringField
      FieldName = 'REI_BANK5'
      Size = 35
    end
    object qryListREI_ACCNT: TStringField
      FieldName = 'REI_ACCNT'
      Size = 35
    end
    object qryListINSTRCT: TStringField
      FieldName = 'INSTRCT'
      Size = 1
    end
    object qryListINSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryListAVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Size = 11
    end
    object qryListAVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Size = 35
    end
    object qryListAVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Size = 35
    end
    object qryListAVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Size = 35
    end
    object qryListAVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Size = 35
    end
    object qryListAVT_BANK5: TStringField
      FieldName = 'AVT_BANK5'
      Size = 35
    end
    object qryListAVT_ACCNT: TStringField
      FieldName = 'AVT_ACCNT'
      Size = 35
    end
    object qryListSND_INFO1: TStringField
      FieldName = 'SND_INFO1'
      Size = 35
    end
    object qryListSND_INFO2: TStringField
      FieldName = 'SND_INFO2'
      Size = 35
    end
    object qryListSND_INFO3: TStringField
      FieldName = 'SND_INFO3'
      Size = 35
    end
    object qryListSND_INFO4: TStringField
      FieldName = 'SND_INFO4'
      Size = 35
    end
    object qryListSND_INFO5: TStringField
      FieldName = 'SND_INFO5'
      Size = 35
    end
    object qryListSND_INFO6: TStringField
      FieldName = 'SND_INFO6'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListCHK1_1: TStringField
      FieldName = 'CHK1_1'
      Size = 1
    end
    object qryListCHK2_1: TStringField
      FieldName = 'CHK2_1'
      Size = 1
    end
    object qryListCHK3_1: TStringField
      FieldName = 'CHK3_1'
      Size = 10
    end
    object qryListPRNO_1: TIntegerField
      FieldName = 'PRNO_1'
    end
    object qryListMAINT_NO_3: TStringField
      FieldName = 'MAINT_NO_3'
      Size = 35
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 35
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 35
    end
    object qryListAPPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Size = 30
    end
    object qryListAPPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Size = 35
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 208
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      
        'MAINT_NO, SEQ, LCNUM, NAME_COD, NAME1, SIZE1, QTY, QTY_G, QTYG, ' +
        'QTYG_G, PRICE, PRICE_G, AMT, AMT_G'
      'FROM ADV700_D')
    Left = 24
    Top = 240
    object qryDetailMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailLCNUM: TStringField
      FieldName = 'LCNUM'
      Size = 35
    end
    object qryDetailNAME_COD: TStringField
      FieldName = 'NAME_COD'
    end
    object qryDetailNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryDetailSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryDetailQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryDetailQTYG: TBCDField
      FieldName = 'QTYG'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryDetailPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryDetailAMT: TBCDField
      FieldName = 'AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryDetailAMT_G: TStringField
      FieldName = 'AMT_G'
      Size = 3
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 56
    Top = 240
  end
end
