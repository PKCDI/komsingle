unit UI_LOCAMR_BP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sCheckBox, Buttons, sBitBtn, Mask,
  sMaskEdit, sEdit, ExtCtrls, sSplitter, sButton, sSpeedButton, sPanel,
  sSkinProvider, sComboBox, ComCtrls, sPageControl, Grids, DBGrids,
  acDBGrid, sCustomComboEdit, sCurrEdit, sCurrencyEdit, sMemo, sLabel, DB,
  ADODB, strutils, DateUtils, TypeDefine;       

type
  TUI_LOCAMR_BP_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    sButton1: TsButton;
    sButton3: TsButton;
    sSplitter1: TsSplitter;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet3: TsTabSheet;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sSplitter2: TsSplitter;
    sDBGrid1: TsDBGrid;
    sSplitter3: TsSplitter;
    sPanel2: TsPanel;
    sPanel5: TsPanel;
    edt_APPLIC: TsEdit;
    sBitBtn4: TsBitBtn;
    edt_APPLIC1: TsEdit;
    edt_APPLIC2: TsEdit;
    edt_APPADDR1: TsEdit;
    edt_APPADDR2: TsEdit;
    edt_APPADDR3: TsEdit;
    sPanel6: TsPanel;
    edt_BENEFC: TsEdit;
    sBitBtn5: TsBitBtn;
    edt_BENEFC1: TsEdit;
    edt_BENEFC2: TsEdit;
    edt_BNFADDR1: TsEdit;
    edt_BNFADDR2: TsEdit;
    edt_BNFADDR3: TsEdit;
    edt_BNFMAILID: TsEdit;
    sPanel8: TsPanel;
    edt_BNFDOMAIN: TsEdit;
    edt_CHKNAME1: TsEdit;
    edt_CHKNAME2: TsEdit;
    edt_LOC_TYPE: TsEdit;
    sBitBtn7: TsBitBtn;
    sEdit21: TsEdit;
    edt_LOC_AMTC: TsEdit;
    sBitBtn8: TsBitBtn;
    edt_LOC_AMT: TsCurrencyEdit;
    edt_CD_PERP: TsEdit;
    edt_CD_PERM: TsEdit;
    edt_LC_NO: TsEdit;
    edt_ISSBANK: TsEdit;
    edt_ISSBANK1: TsEdit;
    edt_ISSBANK2: TsEdit;
    sBitBtn6: TsBitBtn;
    sSpeedButton1: TsSpeedButton;
    sPanel7: TsPanel;
    sPanel9: TsPanel;
    edt_OFFERNO1: TsEdit;
    edt_OFFERNO2: TsEdit;
    edt_OFFERNO3: TsEdit;
    edt_OFFERNO4: TsEdit;
    edt_OFFERNO5: TsEdit;
    edt_OFFERNO6: TsEdit;
    edt_OFFERNO7: TsEdit;
    edt_OFFERNO8: TsEdit;
    edt_OFFERNO9: TsEdit;
    sSpeedButton6: TsSpeedButton;
    edt_AMD_NO: TsEdit;
    sLabel1: TsLabel;
    mask_ISS_DATE: TsMaskEdit;
    mask_APP_DATE: TsMaskEdit;
    mask_DELIVERY: TsMaskEdit;
    mask_EXPIRY: TsMaskEdit;
    sPanel10: TsPanel;
    memo_REMARK1: TsMemo;
    memo_CHGINFO: TsMemo;
    sPanel17: TsPanel;
    edt_EXNAME1: TsEdit;
    edt_EXNAME2: TsEdit;
    edt_EXNAME3: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    edt_APPLIC3: TsEdit;
    edt_BENEFC3: TsEdit;
    sPanel12: TsPanel;
    sPanel4: TsPanel;
    edt_DocNo1: TsEdit;
    sMaskEdit1: TsMaskEdit;
    edt_UserNo: TsEdit;
    btn_Cal: TsBitBtn;
    edt_DocNo2: TsEdit;
    edt_msg1: TsEdit;
    sBitBtn25: TsBitBtn;
    edt_msg2: TsEdit;
    sBitBtn26: TsBitBtn;
    sSplitter4: TsSplitter;
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sBitBtn23Click(Sender: TObject);
    procedure edt_LOC_TYPEChange(Sender: TObject);
    procedure edt_LOC_TYPEDblClick(Sender: TObject);
    procedure sBitBtn7Click(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sDBGrid1DblClick(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
  private
    { Private declarations }
    FWorkStyle : String;
    FSQL : String;
  protected
    ProgramControlType : TProgramControlType;
    FBeforeValue : Variant;
    sKeyField : String;        
    function ReadListBetween(fromDate: String; toDate: String; KeyValue : string;KeyCount : integer=0):Boolean; virtual; abstract;
    function ReadList(OrderString : String=''):Integer; virtual; abstract;
    procedure ReadDocument; virtual; abstract;
    procedure NewDocument; virtual; abstract;
    procedure EditDocument; virtual; abstract;
    procedure SaveDocument(Sender : TObject); virtual; abstract;
    procedure CancelDocument; virtual; abstract;
    procedure DeleteDocument; virtual; abstract;
    procedure ButtonEnable(Val : Boolean=true);
    function ValidData:Boolean; virtual; abstract;
    property WorkStyle:String  read FWorkStyle write FWorkStyle;
    property FormSQL: String  read FSQL write FSQL;
  public
    { Public declarations }
  end;

var
  UI_LOCAMR_BP_frm: TUI_LOCAMR_BP_frm;

implementation

uses MSSQL, KISCalendar, Commonlib, CodeContents, MessageDefine,
  Dialog_BANK, Dialog_CodeList;

{$R *.dfm}

procedure TUI_LOCAMR_BP_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if (Sender as TsDBGrid).DataSource.DataSet.RecordCount = 0 then
    exit;

  CHK2Value := StrToIntDef((Sender as TsDBGrid).DataSource.DataSet.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin
    Case CHK2Value of
      2 :
      begin
        IF Column.FieldName = 'CHK2' Then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;
      6 :
      begin
        IF AnsiMatchText( Column.FieldName , ['CHK2','CHK3'] ) Then
          Canvas.Font.Color := clred
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      IF Column.FieldName = 'LC_NO' Then
      begin
        Canvas.Font.Style := [];
        Canvas.Brush.Color := $00C6FFFF;
      end;
    end;
  end;

//    if Column.FieldName = 'CHK3' then
//    begin
//      case AnsiIndexText(RightStr(qryList.FieldByName('CHK3').AsString, 1), ['1', '2', '3', '4']) of
//        0:
//          begin
//            Canvas.Brush.Color := clBlack;
//            Canvas.Font.Color := clWhite;
//          end;
//        1:
//          begin
//            Canvas.Brush.Color := clBlack;
//            Canvas.Font.Color := clYellow;
//          end;
//        2, 3:
//          begin
//            Canvas.Brush.Color := clBlack;
//            Canvas.Font.Color := clRed;
//          end;
//      end;
//    end;
//
//    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
//    begin
//      Canvas.Brush.Color := $00CC8A00;
//      Canvas.Font.Color := clWhite;
//    end;
//
//    DefaultDrawColumnCell(Rect, DataCol, Column, State);

  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $008DDCFA;
      Canvas.Font.Color := clblack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;


procedure TUI_LOCAMR_BP_frm.FormShow(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);
end;

procedure TUI_LOCAMR_BP_frm.ButtonEnable(Val: Boolean);
begin
  btnNew.Enabled := Val;
  btnEdit.Enabled := Val;
  btnDel.Enabled := Val;

  btnTemp.Enabled := not Val;
  btnSave.Enabled := not Val;
  btnCancel.Enabled := not Val;

  btnCopy.Enabled := Val;
  btnPrint.Enabled := Val;

  sButton1.Enabled := Val;
  sButton3.Enabled := Val;
end;

procedure TUI_LOCAMR_BP_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;
//  IF not (ProgramControlType in [ctInsert,ctModify]) Then Exit;
  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_LOCAMR_BP_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate1);
end;

procedure TUI_LOCAMR_BP_frm.sBitBtn23Click(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(Mask_SearchDate2);
end;

procedure TUI_LOCAMR_BP_frm.edt_LOC_TYPEChange(Sender: TObject);
var
  CodeRecord : TCodeRecord;
  sGroup, sCode : string;
begin
  inherited;

  sGroup := (Sender as TsEdit).Hint;
  sCode  := (Sender as TsEdit).Text;
  IF sGroup = '' Then
  begin
    MessageBox(Self.Handle,MSG_GETCODE_ERROR_EMPTY_HINT,'ERROR',MB_OK+MB_ICONERROR);
    Exit;
  end;

  CodeRecord := DMCodeContents.getCodeName(sGroup,sCode);

  Case (Sender as TsEdit).Tag of
    101 : sEdit21.Text := CodeRecord.sName;
  end;
end;

procedure TUI_LOCAMR_BP_frm.edt_LOC_TYPEDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;
  
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    IF Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
    begin
      (Sender as TsEdit).Text := Dialog_CodeList_frm.qryList.FieldByName('CODE').AsString;

      case (Sender as TsEdit).Tag of
        101: sEdit21.Text := Dialog_CodeList_frm.qryList.FieldByName('NAME').AsString;
      end;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;
end;

procedure TUI_LOCAMR_BP_frm.sBitBtn7Click(Sender: TObject);
begin
  inherited;
  Case (sender as TsBitBtn).Tag of
    101: edt_LOC_TYPEDblClick(edt_LOC_TYPE);
    102: edt_LOC_TYPEDblClick(edt_LOC_AMTC);
    109: edt_LOC_TYPEDblClick(edt_msg1);
    110: edt_LOC_TYPEDblClick(edt_msg2);
  end;
end;

procedure TUI_LOCAMR_BP_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;

  IF edt_SearchText.Visible Then
    edt_SearchText.Clear;
end;

procedure TUI_LOCAMR_BP_frm.sDBGrid1DblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsDBGrid).ScreenToClient(Mouse.CursorPos).Y>17 Then
  begin
    If qryList.RecordCount > 0 Then sPageControl1.ActivePageIndex := 0;
  end;
end;

procedure TUI_LOCAMR_BP_frm.btn_CalClick(Sender: TObject);
begin
  inherited;
  Mask_SearchDate1DblClick(sMaskEdit1);
end;

end.
