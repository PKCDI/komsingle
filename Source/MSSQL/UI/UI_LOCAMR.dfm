inherited UI_LOCAMR_frm: TUI_LOCAMR_frm
  Left = 2144
  Top = 46
  Caption = 'UI_LOCAMR_frm'
  ClientWidth = 1106
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  inherited sSplitter1: TsSplitter
    Width = 1106
  end
  inherited sSplitter4: TsSplitter
    Width = 1106
  end
  inherited sPanel1: TsPanel
    Width = 1106
    inherited sSpeedButton2: TsSpeedButton
      Left = 395
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 608
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 687
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 866
      Visible = False
    end
    object sSpeedButton8: TsSpeedButton [4]
      Left = 178
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel10: TsLabel [5]
      Left = 8
      Top = 5
      Width = 166
      Height = 17
      Caption = #45236#44397#49888#50857#51109' '#51312#44148#48320#44221' '#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel11: TsLabel [6]
      Left = 8
      Top = 20
      Width = 48
      Height = 13
      Caption = 'LOCAMR'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    inherited btnExit: TsButton
      Left = 1020
      OnClick = btnExitClick
    end
    inherited btnNew: TsButton
      Left = 188
      OnClick = btnNewClick
    end
    inherited btnEdit: TsButton
      Left = 255
      OnClick = btnEditClick
    end
    inherited btnDel: TsButton
      Left = 326
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      Left = 894
      Visible = False
    end
    inherited btnPrint: TsButton
      Left = 618
      OnClick = btnPrintClick
    end
    inherited btnTemp: TsButton
      Left = 405
      OnClick = btnSaveClick
    end
    inherited btnSave: TsButton
      Left = 472
      OnClick = btnSaveClick
    end
    inherited btnCancel: TsButton
      Left = 539
      OnClick = btnCancelClick
    end
    inherited sButton1: TsButton
      Left = 697
      OnClick = sButton1Click
    end
    inherited sButton3: TsButton
      Left = 790
      OnClick = sButton3Click
    end
  end
  inherited sPageControl1: TsPageControl
    Width = 1106
    OnChanging = sPageControl1Changing
    inherited sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      inherited sSplitter3: TsSplitter
        Width = 1098
      end
      inherited sPanel2: TsPanel
        Width = 1098
        DesignSize = (
          1098
          551)
        inherited sSpeedButton1: TsSpeedButton
          Left = 618
          Top = 0
          Height = 544
        end
        inherited sSpeedButton6: TsSpeedButton
          Left = 256
        end
        inherited sLabel1: TsLabel
          Left = 410
          Top = 66
        end
        object sLabel16: TsLabel [3]
          Left = 970
          Top = 345
          Width = 111
          Height = 13
          Caption = #52572#45824' 5'#54665' 70'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel2: TsLabel [4]
          Left = 970
          Top = 453
          Width = 117
          Height = 13
          Caption = #52572#45824' 10'#54665' 70'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel7: TsLabel [5]
          Left = 984
          Top = 267
          Width = 4
          Height = 13
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel3: TsLabel [6]
          Left = 984
          Top = 364
          Width = 4
          Height = 13
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sDBGrid2: TsDBGrid [7]
          Left = 2
          Top = 29
          Width = 255
          Height = 700
          Color = clWhite
          Ctl3D = False
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          PopupMenu = popMenu1
          TabOrder = 59
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnDrawColumnCell = sDBGrid1DrawColumnCell
          OnTitleClick = sDBGrid1TitleClick
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHK2'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = #49345#54889
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MAINT_NO'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 166
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'MSEQ'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = #52264#49688
              Width = 30
              Visible = True
            end>
        end
        inherited sPanel5: TsPanel
          Left = 263
          Top = 82
          Width = 345
          TabOrder = 46
        end
        inherited edt_APPLIC: TsEdit
          Left = 384
          Top = 103
          Width = 49
          Height = 19
          CharCase = ecUpperCase
          Font.Height = -11
          TabOrder = 3
          OnDblClick = edt_APPLICDblClick
          BoundLabel.Caption = #44060#49444#51032#47280#51064' '#49345#54840
          BoundLabel.Font.Height = -11
        end
        inherited sBitBtn4: TsBitBtn
          Tag = 1
          Left = 434
          Top = 103
          Height = 19
          TabOrder = 47
          OnClick = sBitBtn4Click
        end
        inherited edt_APPLIC1: TsEdit
          Left = 459
          Top = 103
          Width = 148
          Height = 19
          Font.Height = -11
          TabOrder = 4
          BoundLabel.Active = False
          BoundLabel.Caption = 'edt_APPLIC1'
        end
        inherited edt_APPLIC2: TsEdit
          Left = 384
          Top = 122
          Height = 19
          Font.Height = -11
          TabOrder = 5
          BoundLabel.Font.Height = -11
        end
        inherited edt_APPADDR1: TsEdit
          Left = 384
          Top = 141
          Height = 19
          Font.Height = -11
          TabOrder = 6
          BoundLabel.Font.Height = -11
        end
        inherited edt_APPADDR2: TsEdit
          Left = 384
          Top = 160
          Height = 19
          Font.Height = -11
          TabOrder = 7
        end
        inherited edt_APPADDR3: TsEdit
          Left = 384
          Top = 179
          Height = 19
          Font.Height = -11
          TabOrder = 8
        end
        inherited sPanel6: TsPanel
          Left = 263
          Top = 220
          Width = 345
          TabOrder = 48
        end
        inherited edt_BENEFC: TsEdit
          Left = 384
          Top = 241
          Width = 49
          Height = 19
          CharCase = ecUpperCase
          Font.Height = -11
          TabOrder = 10
          OnDblClick = edt_APPLICDblClick
          BoundLabel.Caption = #49688#54812#51088' '#49345#54840
          BoundLabel.Font.Height = -11
        end
        inherited sBitBtn5: TsBitBtn
          Tag = 2
          Left = 434
          Top = 241
          Height = 19
          TabOrder = 49
          OnClick = sBitBtn4Click
        end
        inherited edt_BENEFC1: TsEdit
          Left = 459
          Top = 241
          Width = 148
          Height = 19
          Font.Height = -11
          BoundLabel.Active = False
        end
        inherited edt_BENEFC2: TsEdit
          Left = 384
          Top = 260
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_BNFADDR1: TsEdit
          Left = 384
          Top = 279
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_BNFADDR2: TsEdit
          Left = 384
          Top = 298
          Height = 19
          Font.Height = -11
        end
        inherited edt_BNFADDR3: TsEdit
          Left = 384
          Top = 317
          Height = 19
          Font.Height = -11
        end
        inherited edt_BNFMAILID: TsEdit
          Left = 384
          Top = 355
          Height = 19
          Font.Height = -11
          TabOrder = 17
          BoundLabel.Font.Height = -11
        end
        inherited sPanel8: TsPanel
          Left = 466
          Top = 355
          Height = 19
          TabOrder = 50
        end
        inherited edt_BNFDOMAIN: TsEdit
          Left = 487
          Top = 355
          Height = 19
          Font.Height = -11
        end
        inherited edt_CHKNAME1: TsEdit
          Left = 384
          Top = 374
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_CHKNAME2: TsEdit
          Left = 384
          Top = 393
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_LOC_TYPE: TsEdit
          Left = 383
          Top = 475
          Height = 19
          Font.Height = -11
          TabOrder = 24
          BoundLabel.Font.Height = -11
        end
        inherited sBitBtn7: TsBitBtn
          Left = 417
          Top = 475
          Height = 19
          TabOrder = 51
        end
        inherited sEdit21: TsEdit
          Left = 442
          Top = 475
          Height = 19
          Font.Height = -11
          TabOrder = 52
        end
        inherited edt_LOC_AMTC: TsEdit
          Left = 383
          Top = 494
          Height = 19
          Font.Height = -11
          TabOrder = 25
          BoundLabel.Font.Height = -11
        end
        inherited sBitBtn8: TsBitBtn
          Left = 417
          Top = 494
          Height = 19
          TabOrder = 53
        end
        inherited edt_LOC_AMT: TsCurrencyEdit
          Left = 442
          Top = 494
          Height = 19
          Font.Height = -11
          BoundLabel.ParentFont = False
          BoundLabel.Font.Height = -12
        end
        inherited edt_CD_PERP: TsEdit
          Left = 383
          Top = 513
          Height = 19
          Font.Height = -11
          BoundLabel.Font.Height = -11
        end
        inherited edt_CD_PERM: TsEdit
          Left = 442
          Top = 513
          Height = 19
          Font.Height = -11
        end
        inherited edt_LC_NO: TsEdit
          Left = 383
          Top = 532
          Height = 19
          Color = clWhite
          Font.Color = clBlack
          Font.Height = -11
          ReadOnly = False
          BoundLabel.Font.Height = -11
        end
        inherited edt_ISSBANK: TsEdit
          Left = 383
          Top = 437
          Width = 49
          Height = 19
          CharCase = ecUpperCase
          Font.Height = -11
          TabOrder = 21
          OnDblClick = edt_ISSBANKDblClick
          BoundLabel.Font.Height = -11
        end
        inherited edt_ISSBANK1: TsEdit
          Left = 459
          Top = 437
          Width = 148
          Height = 19
          Font.Height = -11
          TabOrder = 22
        end
        inherited edt_ISSBANK2: TsEdit
          Left = 383
          Top = 456
          Height = 19
          Font.Height = -11
          TabOrder = 23
        end
        inherited sBitBtn6: TsBitBtn
          Left = 433
          Top = 437
          Height = 19
          TabOrder = 54
          OnClick = sBitBtn6Click
        end
        inherited sPanel7: TsPanel
          Left = 263
          Top = 416
          Width = 345
          TabOrder = 55
        end
        inherited sPanel9: TsPanel
          Left = 638
          Top = 65
          Width = 345
          TabOrder = 56
        end
        inherited edt_OFFERNO1: TsEdit
          Left = 804
          Top = 86
          Height = 19
          Font.Height = -11
          TabOrder = 32
          BoundLabel.Font.Height = -11
        end
        inherited edt_OFFERNO2: TsEdit
          Left = 804
          Top = 105
          Height = 19
          Font.Height = -11
          TabOrder = 33
        end
        inherited edt_OFFERNO3: TsEdit
          Left = 804
          Top = 124
          Height = 19
          Font.Height = -11
          TabOrder = 34
        end
        inherited edt_OFFERNO4: TsEdit
          Left = 804
          Top = 143
          Height = 19
          Font.Height = -11
          TabOrder = 35
        end
        inherited edt_OFFERNO5: TsEdit
          Left = 804
          Top = 162
          Height = 19
          Font.Height = -11
          TabOrder = 36
        end
        inherited edt_OFFERNO6: TsEdit
          Left = 804
          Top = 181
          Height = 19
          Font.Height = -11
          TabOrder = 37
        end
        inherited edt_OFFERNO7: TsEdit
          Left = 804
          Top = 200
          Height = 19
          Font.Height = -11
          TabOrder = 38
        end
        inherited edt_OFFERNO8: TsEdit
          Left = 804
          Top = 219
          Height = 19
          Font.Height = -11
          TabOrder = 39
        end
        inherited edt_OFFERNO9: TsEdit
          Left = 804
          Top = 238
          Height = 19
          Font.Height = -11
          TabOrder = 40
        end
        inherited edt_AMD_NO: TsEdit
          Left = 376
          Top = 61
          Height = 19
          Font.Height = -11
          TabOrder = 2
          BoundLabel.Font.Height = -11
        end
        inherited mask_ISS_DATE: TsMaskEdit
          Left = 376
          Top = 23
          Height = 19
          Font.Height = -11
          TabOrder = 0
          BoundLabel.Font.Height = -11
        end
        inherited mask_APP_DATE: TsMaskEdit
          Left = 376
          Top = 42
          Height = 19
          Font.Height = -11
          TabOrder = 1
          BoundLabel.Font.Height = -11
        end
        inherited mask_DELIVERY: TsMaskEdit
          Left = 868
          Top = 23
          Height = 19
          Font.Height = -11
          TabOrder = 30
          BoundLabel.Font.Height = -11
        end
        inherited mask_EXPIRY: TsMaskEdit
          Left = 868
          Top = 42
          Height = 19
          Font.Height = -11
          TabOrder = 31
          BoundLabel.Font.Height = -11
        end
        inherited sPanel10: TsPanel
          Left = 263
          Top = 2
          Width = 345
          TabOrder = 57
        end
        inherited memo_REMARK1: TsMemo
          Left = 638
          Top = 282
          Width = 446
          Height = 63
          Font.Name = #44404#47548#52404
          ParentCtl3D = False
          TabOrder = 41
          OnChange = memo_REMARK1Change
          OnKeyPress = memo_REMARK1KeyPress
          BoundLabel.Active = False
          BoundLabel.UseSkinColor = True
          BoundLabel.Caption = 'memo_REMARK1'
          BoundLabel.Layout = sclTopLeft
        end
        inherited memo_CHGINFO: TsMemo
          Left = 638
          Top = 380
          Width = 446
          Height = 73
          Font.Height = -11
          Font.Name = #44404#47548#52404
          TabOrder = 42
          OnChange = memo_REMARK1Change
          OnKeyPress = memo_REMARK1KeyPress
          BoundLabel.Active = False
          BoundLabel.UseSkinColor = True
          BoundLabel.Caption = #44592#53440#51312#44148#13#10#48320#44221#49324#54637
          BoundLabel.Layout = sclLeftTop
        end
        inherited sPanel17: TsPanel
          Left = 638
          Top = 470
          Width = 345
          TabOrder = 58
        end
        inherited edt_EXNAME1: TsEdit
          Left = 761
          Top = 491
          Height = 19
          Font.Height = -11
          TabOrder = 43
          BoundLabel.Font.Height = -11
        end
        inherited edt_EXNAME2: TsEdit
          Left = 761
          Top = 510
          Height = 19
          Font.Height = -11
          TabOrder = 44
        end
        inherited edt_EXNAME3: TsEdit
          Left = 761
          Top = 529
          Height = 19
          Font.Height = -11
          TabOrder = 45
          BoundLabel.Font.Height = -11
        end
        inherited edt_APPLIC3: TsEdit
          Left = 384
          Top = 198
          Height = 19
          Font.Height = -11
          TabOrder = 9
          BoundLabel.Font.Height = -11
        end
        inherited edt_BENEFC3: TsEdit
          Left = 384
          Top = 336
          Height = 19
          Font.Height = -11
          TabOrder = 16
          BoundLabel.Font.Height = -11
        end
        inherited sPanel12: TsPanel
          Left = 638
          Top = 2
          Width = 345
          TabOrder = 60
        end
        object sPanel13: TsPanel
          Left = 2
          Top = 2
          Width = 255
          Height = 31
          TabOrder = 61
          SkinData.SkinSection = 'PANEL'
          object Mask_fromDate: TsMaskEdit
            Left = 79
            Top = 2
            Width = 74
            Height = 23
            Color = clWhite
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #46321#47197#51068#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
          end
          object Mask_toDate: TsMaskEdit
            Left = 154
            Top = 2
            Width = 74
            Height = 23
            Color = clWhite
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51312#54924
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.SkinSection = 'EDIT'
          end
          object sBitBtn22: TsBitBtn
            Left = 228
            Top = 2
            Width = 25
            Height = 23
            Cursor = crHandPoint
            TabOrder = 2
            OnClick = sBitBtn22Click
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
        end
        object sPanel11: TsPanel
          Left = 638
          Top = 261
          Width = 345
          Height = 20
          Caption = #44592#53440#51221#48372
          Color = 16042877
          TabOrder = 62
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sPanel14: TsPanel
          Left = 638
          Top = 359
          Width = 345
          Height = 20
          Caption = #44592#53440#51312#44148' '#48320#46041#49324#54637
          Color = 16042877
          TabOrder = 63
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      PopupMenu = popMenu1
      inherited sSplitter2: TsSplitter
        Width = 1098
      end
      inherited sDBGrid1: TsDBGrid [1]
        Width = 1098
        DataSource = dsList
        PopupMenu = popMenu1
        OnTitleClick = sDBGrid1TitleClick
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 74
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 95
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 249
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'MSEQ'
            Title.Alignment = taCenter
            Title.Caption = #52264#49688
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #48320#44221#49888#52397#51068
            Width = 80
            Visible = True
          end
          item
            Alignment = taCenter
            Color = 13041663
            Expanded = False
            FieldName = 'LC_NO'
            Title.Alignment = taCenter
            Title.Caption = #49888#50857#51109#48264#54840
            Width = 143
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'ISS_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ISSBANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ISSBANK2'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665#51648#51216
            Width = 122
            Visible = True
          end>
      end
      inherited sPanel3: TsPanel [2]
        Width = 1098
        inherited edt_SearchText: TsEdit
          Left = 106
        end
        inherited com_SearchKeyword: TsComboBox
          Width = 101
          TabOrder = 4
          TabStop = False
          Items.Strings = (
            #46321#47197#51068#51088
            #49888#50857#51109#48264#54840
            #44060#49444#51008#54665)
        end
        inherited Mask_SearchDate1: TsMaskEdit
          Left = 106
          Height = 23
          TabOrder = 0
        end
        inherited sBitBtn1: TsBitBtn
          Left = 340
          OnClick = sBitBtn1Click
        end
        inherited sBitBtn21: TsBitBtn
          Left = 187
          TabOrder = 5
          TabStop = False
        end
        inherited Mask_SearchDate2: TsMaskEdit
          Left = 234
          Height = 23
          TabOrder = 1
        end
        inherited sBitBtn23: TsBitBtn
          Left = 315
          TabStop = False
        end
        inherited sPanel25: TsPanel
          Left = 210
        end
      end
    end
  end
  inherited sPanel4: TsPanel
    Width = 1106
    inherited edt_msg1: TsEdit
      CharCase = ecUpperCase
    end
    inherited edt_msg2: TsEdit
      CharCase = ecUpperCase
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 408
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      
        'SELECT MAINT_NO, MSEQ, AMD_NO, [USER_ID], DATEE, MESSAGE1, MESSA' +
        'GE2, LC_NO, OFFERNO1, OFFERNO2, OFFERNO3, OFFERNO4, OFFERNO5, OF' +
        'FERNO6, OFFERNO7, OFFERNO8, OFFERNO9, APP_DATE, ISS_DATE, REMARK' +
        ', REMARK1, ISSBANK, ISSBANK1, ISSBANK2, APPLIC, APPLIC1, APPLIC2' +
        ', APPLIC3, BENEFC, BENEFC1, BENEFC2, BENEFC3, EXNAME1, EXNAME2, ' +
        'EXNAME3, DELIVERY, EXPIRY, CHGINFO, CHGINFO1, LOC_TYPE, N4487.DO' +
        'C_NAME as LOC_TYPENAME, LOC_AMT, LOC_AMTC, CHKNAME1, CHKNAME2, C' +
        'HK1, CHK2, CHK3, PRNO, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, B' +
        'NFADDR2, BNFADDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM'
      
        'FROM LOCAMR with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON L' +
        'OCAMR.LOC_TYPE = N4487.CODE')
    Left = 16
    Top = 344
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryListAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListISSBANK: TStringField
      FieldName = 'ISSBANK'
      Size = 4
    end
    object qryListISSBANK1: TStringField
      FieldName = 'ISSBANK1'
      Size = 35
    end
    object qryListISSBANK2: TStringField
      FieldName = 'ISSBANK2'
      Size = 35
    end
    object qryListAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListCHGINFO: TStringField
      FieldName = 'CHGINFO'
      Size = 1
    end
    object qryListCHGINFO1: TMemoField
      FieldName = 'CHGINFO1'
      BlobType = ftMemo
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC_AMT: TBCDField
      FieldName = 'LOC_AMT'
      Precision = 18
    end
    object qryListLOC_AMTC: TStringField
      FieldName = 'LOC_AMTC'
      Size = 19
    end
    object qryListCHKNAME1: TStringField
      FieldName = 'CHKNAME1'
      Size = 35
    end
    object qryListCHKNAME2: TStringField
      FieldName = 'CHKNAME2'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
  end
  inherited dsList: TDataSource
    Left = 48
    Top = 344
  end
  object sp_attachLOCAMRfromLOCADV: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'AttachLOCAMRfromLOCADV;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DOC_COUNT'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 16
    Top = 376
  end
  object sp_attachLOCAMRfromLOCAMA: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'attachLOCAMRfromLOCAMA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DOC_COUNT'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 48
    Top = 376
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE LOCAMR'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 80
    Top = 344
  end
  object popMenu1: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    Left = 16
    Top = 440
    object MenuItem1: TMenuItem
      Caption = #51204#49569#51456#48708
      ImageIndex = 29
      OnClick = sButton1Click
    end
    object MenuItem2: TMenuItem
      Caption = '-'
    end
    object MenuItem3: TMenuItem
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = btnEditClick
    end
    object N4: TMenuItem
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = btnDelClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object d1: TMenuItem
      Caption = #52636#47141#54616#44592
      object N8: TMenuItem
        Caption = #48120#47532#48372#44592
        ImageIndex = 15
        OnClick = btnPrintClick
      end
      object N9: TMenuItem
        Tag = 1
        Caption = #54532#47536#53944
        ImageIndex = 21
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = 'ADMIN'
      Enabled = False
      ImageIndex = 16
    end
  end
end
