unit UI_APPSPC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, Grids, DBGrids, acDBGrid,
  ExtCtrls, sSplitter, ComCtrls, sPageControl, Buttons, sBitBtn, Mask,
  sMaskEdit, sEdit, sButton, sSpeedButton, sPanel, sSkinProvider, sListBox,
  sLabel, sCustomComboEdit, sCurrEdit, sCurrencyEdit, DB, ADODB, MSSQL,
  sMemo, TypeDefine, Menus, QuickRpt, sScrollBox;

type
  TUI_APPSPC_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel4: TsPanel;
    mask_DATEE: TsMaskEdit;
    edt_USER_ID: TsEdit;
    btn_Cal: TsBitBtn;
    edt_msg1: TsEdit;
    sBitBtn19: TsBitBtn;
    edt_msg2: TsEdit;
    sBitBtn20: TsBitBtn;
    sSplitter1: TsSplitter;
    Page_control: TsPageControl;
    sTabSheet1: TsTabSheet;
    page1_Left: TsPanel;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    page3_Left: TsPanel;
    page3_Right: TsPanel;
    sTabSheet4: TsTabSheet;
    page4_Right: TsPanel;
    page4_Left: TsPanel;
    sTabSheet6: TsTabSheet;
    sSplitter2: TsSplitter;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn8: TsBitBtn;
    sBitBtn22: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn28: TsBitBtn;
    sPanel6: TsPanel;
    sSplitter3: TsSplitter;
    sSplitter6: TsSplitter;
    sSplitter4: TsSplitter;
    page1_Right: TsPanel;
    sSplitter5: TsSplitter;
    sSplitter8: TsSplitter;
    sSplitter9: TsSplitter;
    taxPanel2: TsPanel;
    page2_Left: TsPanel;
    sPanel31: TsPanel;
    sPanel44: TsPanel;
    taxDbPanel: TsPanel;
    sDBGrid5: TsDBGrid;
    taxBtnPanel: TsPanel;
    sSpeedButton34: TsSpeedButton;
    sSpeedButton35: TsSpeedButton;
    btn_taxDel: TsSpeedButton;
    sSpeedButton37: TsSpeedButton;
    btn_taxMod: TsSpeedButton;
    sSpeedButton39: TsSpeedButton;
    btn_taxSave: TsSpeedButton;
    btn_taxCancel: TsSpeedButton;
    sSpeedButton42: TsSpeedButton;
    btn_taxNew: TsSpeedButton;
    taxPanel1: TsPanel;
    sPanel52: TsPanel;
    edt_TaxVBMAINTNO: TsEdit;
    sBitBtn45: TsBitBtn;
    edt_TaxDOCNO: TsEdit;
    mask_TaxISSDATE: TsMaskEdit;
    edt_TaxVBRENO: TsEdit;
    edt_TaxVBSENO: TsEdit;
    edt_TaxVBFSNO: TsEdit;
    edt_TaxVBDMNO: TsEdit;
    sPageControl1: TsPageControl;
    sTabSheet7: TsTabSheet;
    sTabSheet8: TsTabSheet;
    sTabSheet9: TsTabSheet;
    sPanel29: TsPanel;
    edt_TaxSECODE: TsEdit;
    edt_TaxSEADDR2: TsEdit;
    mask_TaxSESAUPNO: TsMaskEdit;
    edt_TaxSENAME: TsEdit;
    sBitBtn30: TsBitBtn;
    edt_TaxSESNMAE: TsEdit;
    edt_TaxSEADDR1: TsEdit;
    edt_TaxSEADDR3: TsEdit;
    edt_TaxSEELEC: TsEdit;
    edt_TaxSGUPTAI1_1: TsEdit;
    edt_TaxSGUPTAI1_2: TsEdit;
    edt_TaxSGUPTAI1_3: TsEdit;
    edt_TaxHNITEM1_1: TsEdit;
    edt_TaxHNITEM1_2: TsEdit;
    edt_TaxHNITEM1_3: TsEdit;
    curr_TaxISSEXPAMT: TsCurrencyEdit;
    curr_TaxPAIAMTU1: TsCurrencyEdit;
    sBitBtn12: TsBitBtn;
    edt_TaxPAIAMTC1: TsEdit;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    curr_TaxPAIAMT2: TsCurrencyEdit;
    curr_TaxPAIAMTU2: TsCurrencyEdit;
    edt_TaxPAIAMTC2: TsEdit;
    sBitBtn13: TsBitBtn;
    curr_TaxPAIAMT3: TsCurrencyEdit;
    curr_TaxPAIAMTU3: TsCurrencyEdit;
    edt_TaxPAIAMTC3: TsEdit;
    sBitBtn14: TsBitBtn;
    curr_TaxPAIAMT4: TsCurrencyEdit;
    curr_TaxPAIAMTU4: TsCurrencyEdit;
    edt_TaxPAIAMTC4: TsEdit;
    sBitBtn15: TsBitBtn;
    sPanel10: TsPanel;
    edt_TaxBYCODE: TsEdit;
    sBitBtn16: TsBitBtn;
    edt_TaxBYSNAME: TsEdit;
    edt_TaxBYNAME: TsEdit;
    mask_TaxBYSAUPNO: TsMaskEdit;
    edt_TaxBYADDR1: TsEdit;
    edt_TaxBYADDR2: TsEdit;
    edt_TaxBYADDR3: TsEdit;
    edt_TaxBYELEC: TsEdit;
    edt_TaxSGUPTAI2_1: TsEdit;
    edt_TaxSGUPTAI2_2: TsEdit;
    edt_TaxSGUPTAI2_3: TsEdit;
    edt_TaxHNITEM2_1: TsEdit;
    edt_TaxHNITEM2_2: TsEdit;
    edt_TaxHNITEM2_3: TsEdit;
    edt_TaxAGCODE: TsEdit;
    sBitBtn17: TsBitBtn;
    edt_TaxAGSNAME: TsEdit;
    edt_TaxAGNAME: TsEdit;
    mask_TaxAGSAUPNO: TsMaskEdit;
    edt_TaxAGADDR1: TsEdit;
    edt_TaxAGADDR2: TsEdit;
    edt_TaxAGADDR3: TsEdit;
    edt_TaxAGELEC: TsEdit;
    sSpeedButton6: TsSpeedButton;
    curr_TaxPAIAMT1: TsCurrencyEdit;
    curr_TaxISSEXPAMTU: TsCurrencyEdit;
    curr_TaxTOTCNT: TsCurrencyEdit;
    edt_TaxVBGUBUN: TsEdit;
    sBitBtn18: TsBitBtn;
    edt_TaxTOTCNTC: TsEdit;
    sBitBtn21: TsBitBtn;
    sPanel12: TsPanel;
    edt_TaxREMARK1_1: TsEdit;
    edt_TaxREMARK1_2: TsEdit;
    edt_TaxREMARK1_3: TsEdit;
    edt_TaxREMARK1_4: TsEdit;
    edt_TaxREMARK1_5: TsEdit;
    taxPanel3: TsPanel;
    sPanel14: TsPanel;
    taxItemBtnPanel: TsPanel;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton9: TsSpeedButton;
    btn_taxItemDel: TsSpeedButton;
    sSpeedButton44: TsSpeedButton;
    btn_taxItemMod: TsSpeedButton;
    sSpeedButton46: TsSpeedButton;
    btn_taxItemSave: TsSpeedButton;
    btn_taxItemCancel: TsSpeedButton;
    sSpeedButton49: TsSpeedButton;
    btn_taxItemNew: TsSpeedButton;
    sDBGrid2: TsDBGrid;
    edt_TaxLINENO: TsEdit;
    mask_TaxDEDATE: TsMaskEdit;
    edt_TaxSIZE1: TsEdit;
    sBitBtn23: TsBitBtn;
    sBitBtn24: TsBitBtn;
    edt_TaxIMDCODE1: TsEdit;
    edt_TaxQTYC: TsEdit;
    sBitBtn25: TsBitBtn;
    edt_TaxTOTQTYC: TsEdit;
    sBitBtn26: TsBitBtn;
    curr_TaxTOTQTY: TsCurrencyEdit;
    curr_TaxPRICEG: TsCurrencyEdit;
    curr_TaxSUPTOTAMT: TsCurrencyEdit;
    sBitBtn27: TsBitBtn;
    sBitBtn29: TsBitBtn;
    edt_TaxPRICEC: TsEdit;
    edt_TaxSUPTOTAMTC: TsEdit;
    curr_TaxQTY: TsCurrencyEdit;
    curr_TaxPRICE: TsCurrencyEdit;
    edt_TaxSUPAMTC: TsEdit;
    sBitBtn47: TsBitBtn;
    curr_TaxSUPAMT: TsCurrencyEdit;
    edt_TaxVBAMTC: TsEdit;
    sBitBtn46: TsBitBtn;
    curr_TaxVBAMT: TsCurrencyEdit;
    edt_VBTOTAMTC: TsEdit;
    sBitBtn48: TsBitBtn;
    curr_VBTOTAMT: TsCurrencyEdit;
    edt_TaxVBTAXC: TsEdit;
    sBitBtn49: TsBitBtn;
    curr_TaxVBTAX: TsCurrencyEdit;
    edt_TaxVBTOTTAXC: TsEdit;
    sBitBtn50: TsBitBtn;
    curr_TaxVBTOTTAX: TsCurrencyEdit;
    curr_TaxCUXRATE: TsCurrencyEdit;
    edt_TaxBIGO1: TsEdit;
    sBitBtn51: TsBitBtn;
    sSplitter11: TsSplitter;
    sSplitter12: TsSplitter;
    InvDbPanel: TsPanel;
    sDBGrid6: TsDBGrid;
    invBtnPanel: TsPanel;
    sSpeedButton51: TsSpeedButton;
    sSpeedButton52: TsSpeedButton;
    btn_invDel: TsSpeedButton;
    sSpeedButton54: TsSpeedButton;
    btn_invMod: TsSpeedButton;
    sSpeedButton56: TsSpeedButton;
    btn_invSave: TsSpeedButton;
    btn_invCancel: TsSpeedButton;
    sSpeedButton59: TsSpeedButton;
    btn_invNew: TsSpeedButton;
    invPanel2: TsPanel;
    sSpeedButton61: TsSpeedButton;
    sPanel23: TsPanel;
    sPanel24: TsPanel;
    invPanel1: TsPanel;
    sSpeedButton62: TsSpeedButton;
    sPanel26: TsPanel;
    sPanel27: TsPanel;
    edt_InvREMARK1_1: TsEdit;
    edt_InvREMARK1_2: TsEdit;
    edt_InvREMARK1_3: TsEdit;
    edt_InvREMARK1_4: TsEdit;
    edt_InvREMARK1_5: TsEdit;
    mask_InvISSDATE: TsMaskEdit;
    edt_InvDOCNO: TsEdit;
    curr_InvTOTCNT: TsCurrencyEdit;
    edt_InvTOTCNTC: TsEdit;
    sBitBtn54: TsBitBtn;
    curr_InvISSTOTAMT: TsCurrencyEdit;
    edt_InvISSTOTAMTC: TsEdit;
    sBitBtn55: TsBitBtn;
    edt_InvSECODE: TsEdit;
    edt_InvSESNAME: TsEdit;
    edt_InvSENAME: TsEdit;
    edt_InvSEADDR1: TsEdit;
    edt_InvSEADDR3: TsEdit;
    edt_InvSEADDR2: TsEdit;
    sBitBtn52: TsBitBtn;
    edt_InvBYCODE: TsEdit;
    sBitBtn53: TsBitBtn;
    edt_InvBYNAME: TsEdit;
    edt_InvBYSNAME: TsEdit;
    edt_InvBYADDR1: TsEdit;
    edt_InvBYADDR2: TsEdit;
    edt_InvBYADDR3: TsEdit;
    invPanel3: TsPanel;
    sSplitter13: TsSplitter;
    sPanel54: TsPanel;
    invItemBtnPanel: TsPanel;
    sSpeedButton63: TsSpeedButton;
    sSpeedButton64: TsSpeedButton;
    btn_invItemDel: TsSpeedButton;
    sSpeedButton66: TsSpeedButton;
    btn_invItemMod: TsSpeedButton;
    sSpeedButton68: TsSpeedButton;
    btn_invItemSave: TsSpeedButton;
    btn_invItemCancel: TsSpeedButton;
    sSpeedButton71: TsSpeedButton;
    btn_invItemNew: TsSpeedButton;
    sDBGrid7: TsDBGrid;
    edt_InvLINENO: TsEdit;
    edt_InvSIZE1: TsEdit;
    sBitBtn56: TsBitBtn;
    sBitBtn57: TsBitBtn;
    edt_InvIMDCODE1: TsEdit;
    edt_InvQTYC: TsEdit;
    sBitBtn58: TsBitBtn;
    edt_InvTOTQTYC: TsEdit;
    sBitBtn59: TsBitBtn;
    curr_InvTOTQTY: TsCurrencyEdit;
    curr_InvPRICEG: TsCurrencyEdit;
    sBitBtn61: TsBitBtn;
    edt_InvPRICEC: TsEdit;
    curr_InvQTY: TsCurrencyEdit;
    curr_InvPRICE: TsCurrencyEdit;
    edt_InvSUPAMTC: TsEdit;
    sBitBtn63: TsBitBtn;
    curr_InvSUPAMT: TsCurrencyEdit;
    edt_InvSUPTOTAMTC: TsEdit;
    sBitBtn64: TsBitBtn;
    curr_InvSUPTOTAMT: TsCurrencyEdit;
    sPanel56: TsPanel;
    sDBGrid8: TsDBGrid;
    sPanel57: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn60: TsBitBtn;
    mask_toDate: TsMaskEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryGoods: TADOQuery;
    dsGoods: TDataSource;
    TaxBIGOPanel: TsPanel;
    edt_Panel_TaxBIGO1: TsEdit;
    edt_TaxBIGO2: TsEdit;
    edt_TaxBIGO3: TsEdit;
    edt_TaxBIGO4: TsEdit;
    sButton8: TsButton;
    sButton9: TsButton;
    edt_TaxBIGO5: TsEdit;
    TaxSIZEPanel: TsPanel;
    edt_Panel_TaxSIZE1: TsEdit;
    edt_TaxSIZE2: TsEdit;
    edt_TaxSIZE3: TsEdit;
    edt_TaxSIZE4: TsEdit;
    sButton6: TsButton;
    sButton7: TsButton;
    edt_TaxSIZE5: TsEdit;
    edt_TaxSIZE6: TsEdit;
    edt_TaxSIZE7: TsEdit;
    edt_TaxSIZE10: TsEdit;
    edt_TaxSIZE9: TsEdit;
    edt_TaxSIZE8: TsEdit;
    TaxIMDCODEPanel: TsPanel;
    edt_Panel_TaxIMDCODE1: TsEdit;
    edt_TaxIMDCODE2: TsEdit;
    edt_TaxIMDCODE3: TsEdit;
    edt_TaxIMDCODE4: TsEdit;
    sButton4: TsButton;
    sButton5: TsButton;
    GoodsSIZE_Panel: TsPanel;
    panel_edt_size1: TsEdit;
    edt_GoodsSIZE2: TsEdit;
    edt_GoodsSIZE3: TsEdit;
    edt_GoodsSIZE4: TsEdit;
    sButton2: TsButton;
    sButton3: TsButton;
    edt_GoodsSIZE5: TsEdit;
    edt_GoodsSIZE6: TsEdit;
    edt_GoodsSIZE7: TsEdit;
    edt_GoodsSIZE10: TsEdit;
    edt_GoodsSIZE9: TsEdit;
    edt_GoodsSIZE8: TsEdit;
    GoodsimdcodePanel: TsPanel;
    imdcodePanel_edit1: TsEdit;
    edt_GoodsIMDCODE2: TsEdit;
    edt_GoodsIMDCODE3: TsEdit;
    edt_GoodsIMDCODE4: TsEdit;
    imdcodeOk: TsButton;
    sButton1: TsButton;
    InvIMCODEPanel: TsPanel;
    edt_Panel_InvIMDCODE1: TsEdit;
    edt_InvIMDCODE2: TsEdit;
    edt_InvIMDCODE3: TsEdit;
    edt_InvIMDCODE4: TsEdit;
    sButton10: TsButton;
    sButton11: TsButton;
    InvSIZEPanel: TsPanel;
    edt_Panel_InvSIZE1: TsEdit;
    edt_InvSIZE2: TsEdit;
    edt_InvSIZE3: TsEdit;
    edt_InvSIZE4: TsEdit;
    sButton12: TsButton;
    sButton13: TsButton;
    edt_InvSIZE5: TsEdit;
    edt_InvSIZE6: TsEdit;
    edt_InvSIZE7: TsEdit;
    edt_InvSIZE10: TsEdit;
    edt_InvSIZE9: TsEdit;
    edt_InvSIZE8: TsEdit;
    qryGoodsItem: TADOQuery;
    dsGoodsitem: TDataSource;
    qryTax: TADOQuery;
    dsTax: TDataSource;
    edt_TaxVBDETAILNO: TsEdit;
    qryTaxItem: TADOQuery;
    dsTaxItem: TDataSource;
    qryInv: TADOQuery;
    dsInv: TDataSource;
    qryInvItem: TADOQuery;
    dsInvItem: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListBGM_GUBUN: TStringField;
    qryListBGM1: TStringField;
    qryListBGM2: TStringField;
    qryListBGM3: TStringField;
    qryListBGM4: TStringField;
    qryListCP_NO: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_SNAME: TStringField;
    qryListAPP_SAUPNO: TStringField;
    qryListAPP_NAME: TStringField;
    qryListAPP_ELEC: TStringField;
    qryListAPP_ADDR1: TStringField;
    qryListAPP_ADDR2: TStringField;
    qryListAPP_ADDR3: TStringField;
    qryListCP_BANK: TStringField;
    qryListCP_BANKNAME: TStringField;
    qryListCP_BANKBU: TStringField;
    qryListCP_ACCOUNTNO: TStringField;
    qryListCP_NAME1: TStringField;
    qryListCP_NAME2: TStringField;
    qryListCP_CURR: TStringField;
    qryListDF_SAUPNO: TStringField;
    qryListDF_NAME1: TStringField;
    qryListDF_NAME2: TStringField;
    qryListDF_NAME3: TStringField;
    qryListDF_EMAIL1: TStringField;
    qryListDF_EMAIL2: TStringField;
    qryListCP_ADD_ACCOUNTNO1: TStringField;
    qryListCP_ADD_NAME1: TStringField;
    qryListCP_ADD_CURR1: TStringField;
    qryListCP_ADD_ACCOUNTNO2: TStringField;
    qryListCP_ADD_NAME2: TStringField;
    qryListCP_ADD_CURR2: TStringField;
    qryListCP_AMTC: TStringField;
    qryListCP_AMT: TBCDField;
    qryListCP_AMTU: TBCDField;
    qryListCP_CUX: TBCDField;
    qryListKEYY: TStringField;
    qryListDOC_NO: TStringField;
    qryListISS_DATE: TStringField;
    qryListLA_BANKBUCODE: TStringField;
    qryListLA_BANKNAMEP: TStringField;
    qryListLR_GUBUN: TStringField;
    qryListLR_NO: TStringField;
    qryListLA_BANKBUP: TStringField;
    qryListDOC_GUBUN: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsDOC_GUBUN: TStringField;
    qryGoodsSEQ: TIntegerField;
    qryGoodsDOC_NO: TStringField;
    qryGoodsLR_NO2: TStringField;
    qryGoodsBSN_HSCODE: TStringField;
    qryGoodsISS_DATE: TStringField;
    qryGoodsACC_DATE: TStringField;
    qryGoodsVAL_DATE: TStringField;
    qryGoodsBY_CODE: TStringField;
    qryGoodsBY_SNAME: TStringField;
    qryGoodsBY_NAME: TStringField;
    qryGoodsBY_ELEC: TStringField;
    qryGoodsBY_ADDR1: TStringField;
    qryGoodsBY_ADDR2: TStringField;
    qryGoodsBY_ADDR3: TStringField;
    qryGoodsLR_NO: TStringField;
    qryGoodsGET_DATE: TStringField;
    qryGoodsEXP_DATE: TStringField;
    qryGoodsSE_CODE: TStringField;
    qryGoodsSE_SNAME: TStringField;
    qryGoodsLA_BANK: TStringField;
    qryGoodsLA_BANKNAME: TStringField;
    qryGoodsLA_BANKBU: TStringField;
    qryGoodsLA_ELEC: TStringField;
    qryGoodsEXCH: TBCDField;
    qryGoodsISS_AMT: TBCDField;
    qryGoodsISS_AMTC: TStringField;
    qryGoodsISS_AMTU: TBCDField;
    qryGoodsISS_EXPAMT: TBCDField;
    qryGoodsISS_EXPAMTC: TStringField;
    qryGoodsISS_EXPAMTU: TBCDField;
    qryGoodsTOTCNTC: TStringField;
    qryGoodsTOTCNT: TBCDField;
    qryGoodsISS_TOTAMTC: TStringField;
    qryGoodsISS_TOTAMT: TBCDField;
    qryGoodsREMARK1_1: TStringField;
    qryGoodsREMARK1_2: TStringField;
    qryGoodsREMARK1_3: TStringField;
    qryGoodsREMARK1_4: TStringField;
    qryGoodsREMARK1_5: TStringField;
    qryGoodsREMARK2_1: TStringField;
    qryGoodsREMARK2_2: TStringField;
    qryGoodsREMARK2_3: TStringField;
    qryGoodsREMARK2_4: TStringField;
    qryGoodsREMARK2_5: TStringField;
    qryTaxKEYY: TStringField;
    qryTaxDOC_GUBUN: TStringField;
    qryTaxSEQ: TIntegerField;
    qryTaxDOC_NO: TStringField;
    qryTaxVB_RENO: TStringField;
    qryTaxVB_SENO: TStringField;
    qryTaxVB_FSNO: TStringField;
    qryTaxVB_MAINTNO: TStringField;
    qryTaxISS_DATE: TStringField;
    qryTaxVB_DMNO: TStringField;
    qryTaxSE_CODE: TStringField;
    qryTaxSE_SNAME: TStringField;
    qryTaxSE_NAME: TStringField;
    qryTaxSE_SAUPNO: TStringField;
    qryTaxSE_ADDR1: TStringField;
    qryTaxSE_ADDR2: TStringField;
    qryTaxSE_ADDR3: TStringField;
    qryTaxSE_ELEC: TStringField;
    qryTaxSG_UPTAI1_1: TStringField;
    qryTaxSG_UPTAI1_2: TStringField;
    qryTaxSG_UPTAI1_3: TStringField;
    qryTaxHN_ITEM1_1: TStringField;
    qryTaxHN_ITEM1_2: TStringField;
    qryTaxHN_ITEM1_3: TStringField;
    qryTaxBY_CODE: TStringField;
    qryTaxBY_SNAME: TStringField;
    qryTaxBY_NAME: TStringField;
    qryTaxBY_SAUPNO: TStringField;
    qryTaxBY_ADDR1: TStringField;
    qryTaxBY_ADDR2: TStringField;
    qryTaxBY_ADDR3: TStringField;
    qryTaxBY_ELEC: TStringField;
    qryTaxSG_UPTAI2_1: TStringField;
    qryTaxSG_UPTAI2_2: TStringField;
    qryTaxSG_UPTAI2_3: TStringField;
    qryTaxHN_ITEM2_1: TStringField;
    qryTaxHN_ITEM2_2: TStringField;
    qryTaxHN_ITEM2_3: TStringField;
    qryTaxAG_CODE: TStringField;
    qryTaxAG_SNAME: TStringField;
    qryTaxAG_NAME: TStringField;
    qryTaxAG_SAUPNO: TStringField;
    qryTaxAG_ADDR1: TStringField;
    qryTaxAG_ADDR2: TStringField;
    qryTaxAG_ADDR3: TStringField;
    qryTaxAG_ELEC: TStringField;
    qryTaxPAI_AMT1: TBCDField;
    qryTaxPAI_AMT2: TBCDField;
    qryTaxPAI_AMT3: TBCDField;
    qryTaxPAI_AMT4: TBCDField;
    qryTaxPAI_AMTC1: TStringField;
    qryTaxPAI_AMTC2: TStringField;
    qryTaxPAI_AMTC3: TStringField;
    qryTaxPAI_AMTC4: TStringField;
    qryTaxPAI_AMTU1: TBCDField;
    qryTaxPAI_AMTU2: TBCDField;
    qryTaxPAI_AMTU3: TBCDField;
    qryTaxPAI_AMTU4: TBCDField;
    qryTaxISS_EXPAMT: TBCDField;
    qryTaxISS_EXPAMTU: TBCDField;
    qryTaxTOTCNT: TBCDField;
    qryTaxVB_GUBUN: TStringField;
    qryTaxVB_DETAILNO: TStringField;
    qryTaxREMARK1_1: TStringField;
    qryTaxREMARK1_2: TStringField;
    qryTaxREMARK1_3: TStringField;
    qryTaxREMARK1_4: TStringField;
    qryTaxREMARK1_5: TStringField;
    qryInvKEYY: TStringField;
    qryInvDOC_GUBUN: TStringField;
    qryInvSEQ: TIntegerField;
    qryInvDOC_NO: TStringField;
    qryInvISS_DATE: TStringField;
    qryInvTOTCNT: TBCDField;
    qryInvTOTCNTC: TStringField;
    qryInvISS_TOTAMT: TBCDField;
    qryInvISS_TOTAMTC: TStringField;
    qryInvREMARK1_1: TStringField;
    qryInvREMARK1_2: TStringField;
    qryInvREMARK1_3: TStringField;
    qryInvREMARK1_4: TStringField;
    qryInvREMARK1_5: TStringField;
    qryInvSE_CODE: TStringField;
    qryInvSE_SNAME: TStringField;
    qryInvSE_NAME: TStringField;
    qryInvSE_ADDR1: TStringField;
    qryInvSE_ADDR2: TStringField;
    qryInvSE_ADDR3: TStringField;
    qryInvBY_CODE: TStringField;
    qryInvBY_SNAME: TStringField;
    qryInvBY_NAME: TStringField;
    qryInvBY_ADDR1: TStringField;
    qryInvBY_ADDR2: TStringField;
    qryInvBY_ADDR3: TStringField;
    qryTaxTOTCNTC: TStringField;
    qryGoodsItemKEYY: TStringField;
    qryGoodsItemDOC_GUBUN: TStringField;
    qryGoodsItemSEQ: TIntegerField;
    qryGoodsItemLINE_NO: TStringField;
    qryGoodsItemHS_NO: TStringField;
    qryGoodsItemIMD_CODE1: TStringField;
    qryGoodsItemIMD_CODE2: TStringField;
    qryGoodsItemIMD_CODE3: TStringField;
    qryGoodsItemIMD_CODE4: TStringField;
    qryGoodsItemSIZE1: TStringField;
    qryGoodsItemSIZE2: TStringField;
    qryGoodsItemSIZE3: TStringField;
    qryGoodsItemSIZE4: TStringField;
    qryGoodsItemSIZE5: TStringField;
    qryGoodsItemSIZE6: TStringField;
    qryGoodsItemSIZE7: TStringField;
    qryGoodsItemSIZE8: TStringField;
    qryGoodsItemSIZE9: TStringField;
    qryGoodsItemSIZE10: TStringField;
    qryGoodsItemQTYC: TStringField;
    qryGoodsItemQTY: TBCDField;
    qryGoodsItemTOTQTYC: TStringField;
    qryGoodsItemTOTQTY: TBCDField;
    qryGoodsItemPRICE: TBCDField;
    qryGoodsItemPRICEC: TStringField;
    qryGoodsItemPRICE_G: TBCDField;
    qryGoodsItemSUP_AMTC: TStringField;
    qryGoodsItemSUP_AMT: TBCDField;
    qryGoodsItemSUP_TOTAMTC: TStringField;
    qryGoodsItemSUP_TOTAMT: TBCDField;
    qryTaxItemKEYY: TStringField;
    qryTaxItemDOC_GUBUN: TStringField;
    qryTaxItemSEQ: TIntegerField;
    qryTaxItemLINE_NO: TStringField;
    qryTaxItemDE_DATE: TStringField;
    qryTaxItemIMD_CODE1: TStringField;
    qryTaxItemIMD_CODE2: TStringField;
    qryTaxItemIMD_CODE3: TStringField;
    qryTaxItemIMD_CODE4: TStringField;
    qryTaxItemSIZE1: TStringField;
    qryTaxItemSIZE2: TStringField;
    qryTaxItemSIZE3: TStringField;
    qryTaxItemSIZE4: TStringField;
    qryTaxItemSIZE5: TStringField;
    qryTaxItemSIZE6: TStringField;
    qryTaxItemSIZE7: TStringField;
    qryTaxItemSIZE8: TStringField;
    qryTaxItemSIZE9: TStringField;
    qryTaxItemSIZE10: TStringField;
    qryTaxItemQTYC: TStringField;
    qryTaxItemQTY: TBCDField;
    qryTaxItemTOTQTYC: TStringField;
    qryTaxItemTOTQTY: TBCDField;
    qryTaxItemCUX_RATE: TBCDField;
    qryTaxItemPRICE: TBCDField;
    qryTaxItemPRICEC: TStringField;
    qryTaxItemPRICE_G: TBCDField;
    qryTaxItemSUP_AMTC: TStringField;
    qryTaxItemSUP_AMT: TBCDField;
    qryTaxItemSUP_TOTAMTC: TStringField;
    qryTaxItemSUP_TOTAMT: TBCDField;
    qryTaxItemVB_AMTC: TStringField;
    qryTaxItemVB_AMT: TBCDField;
    qryTaxItemVB_TOTAMTC: TStringField;
    qryTaxItemVB_TOTAMT: TBCDField;
    qryTaxItemVB_TAXC: TStringField;
    qryTaxItemVB_TAX: TBCDField;
    qryTaxItemVB_TOTTAXC: TStringField;
    qryTaxItemVB_TOTTAX: TBCDField;
    qryTaxItemBIGO1: TStringField;
    qryTaxItemBIGO2: TStringField;
    qryTaxItemBIGO3: TStringField;
    qryTaxItemBIGO4: TStringField;
    qryTaxItemBIGO5: TStringField;
    qryInvItemKEYY: TStringField;
    qryInvItemDOC_GUBUN: TStringField;
    qryInvItemSEQ: TIntegerField;
    qryInvItemLINE_NO: TStringField;
    qryInvItemIMD_CODE1: TStringField;
    qryInvItemIMD_CODE2: TStringField;
    qryInvItemIMD_CODE3: TStringField;
    qryInvItemIMD_CODE4: TStringField;
    qryInvItemSIZE1: TStringField;
    qryInvItemSIZE2: TStringField;
    qryInvItemSIZE3: TStringField;
    qryInvItemSIZE4: TStringField;
    qryInvItemSIZE5: TStringField;
    qryInvItemSIZE6: TStringField;
    qryInvItemSIZE7: TStringField;
    qryInvItemSIZE8: TStringField;
    qryInvItemSIZE9: TStringField;
    qryInvItemSIZE10: TStringField;
    qryInvItemQTYC: TStringField;
    qryInvItemQTY: TBCDField;
    qryInvItemTOTQTYC: TStringField;
    qryInvItemTOTQTY: TBCDField;
    qryInvItemPRICE: TBCDField;
    qryInvItemPRICEC: TStringField;
    qryInvItemPRICE_G: TBCDField;
    qryInvItemSUP_AMTC: TStringField;
    qryInvItemSUP_AMT: TBCDField;
    qryInvItemSUP_TOTAMTC: TStringField;
    qryInvItemSUP_TOTAMT: TBCDField;
    qryMAX_SEQ: TADOQuery;
    qryGoodsLR_GUBUN: TStringField;
    goodsMenu: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    TaxMenu: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    invMenu: TPopupMenu;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    goodsItemMenu: TPopupMenu;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    TaxItemMenu: TPopupMenu;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    invItemMenu: TPopupMenu;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    sSplitter7: TsSplitter;
    qryMAX_LINENO: TADOQuery;
    qryListTOTDOC_NO: TStringField;
    edt_MAINT_NO: TsEdit;
    qryCheckCancel: TADOQuery;
    qryCheckCancelKEYY: TStringField;
    qryCheckCancelDOC_GUBUN: TStringField;
    qryCheckCancelSEQ: TIntegerField;
    qryCheckCancelLINE_NO: TStringField;
    qryCheckCancelHS_NO: TStringField;
    qryCheckCancelDE_DATE: TStringField;
    qryCheckCancelIMD_CODE1: TStringField;
    qryCheckCancelIMD_CODE2: TStringField;
    qryCheckCancelIMD_CODE3: TStringField;
    qryCheckCancelIMD_CODE4: TStringField;
    qryCheckCancelSIZE1: TStringField;
    qryCheckCancelSIZE2: TStringField;
    qryCheckCancelSIZE3: TStringField;
    qryCheckCancelSIZE4: TStringField;
    qryCheckCancelSIZE5: TStringField;
    qryCheckCancelSIZE6: TStringField;
    qryCheckCancelSIZE7: TStringField;
    qryCheckCancelSIZE8: TStringField;
    qryCheckCancelSIZE9: TStringField;
    qryCheckCancelSIZE10: TStringField;
    qryCheckCancelQTY: TBCDField;
    qryCheckCancelQTYC: TStringField;
    qryCheckCancelTOTQTY: TBCDField;
    qryCheckCancelTOTQTYC: TStringField;
    qryCheckCancelPRICE: TBCDField;
    qryCheckCancelPRICE_G: TBCDField;
    qryCheckCancelPRICEC: TStringField;
    qryCheckCancelCUX_RATE: TBCDField;
    qryCheckCancelSUP_AMT: TBCDField;
    qryCheckCancelSUP_AMTC: TStringField;
    qryCheckCancelVB_TAX: TBCDField;
    qryCheckCancelVB_TAXC: TStringField;
    qryCheckCancelVB_AMT: TBCDField;
    qryCheckCancelVB_AMTC: TStringField;
    qryCheckCancelSUP_TOTAMT: TBCDField;
    qryCheckCancelSUP_TOTAMTC: TStringField;
    qryCheckCancelVB_TOTTAX: TBCDField;
    qryCheckCancelVB_TOTTAXC: TStringField;
    qryCheckCancelVB_TOTAMT: TBCDField;
    qryCheckCancelVB_TOTAMTC: TStringField;
    qryCheckCancelBIGO1: TStringField;
    qryCheckCancelBIGO2: TStringField;
    qryCheckCancelBIGO3: TStringField;
    qryCheckCancelBIGO4: TStringField;
    qryCheckCancelBIGO5: TStringField;
    QRCompositeReport1: TQRCompositeReport;
    sSplitter14: TsSplitter;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSpeedButton14: TsSpeedButton;
    sSpeedButton16: TsSpeedButton;
    sSpeedButton18: TsSpeedButton;
    spCopyAPPSPC: TADOStoredProc;
    qryReady: TADOQuery;
    qryListAPP_DATE: TStringField;
    qryListLA_TYPE: TStringField;
    popMenu1: TPopupMenu;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    MenuItem18: TMenuItem;
    N4: TMenuItem;
    N7: TMenuItem;
    d1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    sScrollBox1: TsScrollBox;
    basicPanel1: TsPanel;
    sLabel2: TsLabel;
    sLabel1: TsLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    combo_BGMGUBUN: TsComboBox;
    edt_CPNO: TsEdit;
    sPanel8: TsPanel;
    edt_BGM1: TsEdit;
    edt_BGM2: TsEdit;
    sBitBtn1: TsBitBtn;
    edt_BGM3: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_BGM4: TsEdit;
    basicPanel2: TsPanel;
    sSpeedButton1: TsSpeedButton;
    sPanel11: TsPanel;
    edt_APPCODE: TsEdit;
    sBitBtn5: TsBitBtn;
    edt_APPSNAME: TsEdit;
    edt_APPNAME: TsEdit;
    edt_APPSAUPNO: TsMaskEdit;
    edt_APPADDR1: TsEdit;
    edt_APPADDR2: TsEdit;
    edt_APPADDR3: TsEdit;
    edt_APPELEC: TsEdit;
    edt_CPBANK: TsEdit;
    sBitBtn3: TsBitBtn;
    edt_CPBANKBU: TsEdit;
    edt_CPNAME1: TsEdit;
    edt_CPACCOUNTNO: TsEdit;
    edt_CPNAME2: TsEdit;
    edt_CPCURR: TsEdit;
    sBitBtn6: TsBitBtn;
    sPanel19: TsPanel;
    edt_CPBANKNAME: TsEdit;
    basicPanel3: TsPanel;
    sSpeedButton8: TsSpeedButton;
    sPanel20: TsPanel;
    sPanel9: TsPanel;
    mask_DFSAUPNO: TsMaskEdit;
    edt_DFNAME1: TsEdit;
    edt_DFNAME2: TsEdit;
    edt_DFNAME3: TsEdit;
    edt_DFEMAIL1: TsEdit;
    edt_DFEMAIL2: TsEdit;
    edt_CPADDACCOUNTNO1: TsEdit;
    edt_CPADDNAME1: TsEdit;
    edt_CPADDCURR1: TsEdit;
    sBitBtn7: TsBitBtn;
    edt_CPADDACCOUNTNO2: TsEdit;
    edt_CPADDNAME2: TsEdit;
    edt_CPADDCURR2: TsEdit;
    sBitBtn9: TsBitBtn;
    sBitBtn65: TsBitBtn;
    basicPanel4: TsPanel;
    sPanel32: TsPanel;
    edt_CPAMTC: TsEdit;
    sBitBtn4: TsBitBtn;
    curr_CPAMTU: TsCurrencyEdit;
    curr_CPCUX: TsCurrencyEdit;
    edt_RCT_AMT2C: TsEdit;
    curr_CPAMT: TsCurrencyEdit;
    basicPanel5: TsPanel;
    sPanel22: TsPanel;
    edt_DOCNO: TsEdit;
    sBitBtn10: TsBitBtn;
    mask_ISSDATE: TsMaskEdit;
    edt_LABANKBUCODE: TsEdit;
    sBitBtn11: TsBitBtn;
    edt_LABANKNAMEP: TsEdit;
    combo_LATYPE: TsComboBox;
    edt_LRNO: TsEdit;
    edt_LABANKBUP: TsEdit;
    sScrollBox2: TsScrollBox;
    goodsDbPanel: TsPanel;
    sDBGrid3: TsDBGrid;
    goodsBtnPanel: TsPanel;
    sSpeedButton10: TsSpeedButton;
    sSpeedButton19: TsSpeedButton;
    btn_goodsDel: TsSpeedButton;
    sSpeedButton22: TsSpeedButton;
    btn_goodsMod: TsSpeedButton;
    sSpeedButton24: TsSpeedButton;
    btn_goodsSave: TsSpeedButton;
    btn_goodsCancel: TsSpeedButton;
    sSpeedButton27: TsSpeedButton;
    btn_goodsNew: TsSpeedButton;
    goodsPanel1: TsPanel;
    sSpeedButton29: TsSpeedButton;
    sPanel36: TsPanel;
    sPanel37: TsPanel;
    edt_GoodsDOCNO: TsEdit;
    sBitBtn31: TsBitBtn;
    edt_GoodsBYNAME: TsEdit;
    edt_GoodsBYELEC: TsEdit;
    edt_GoodsBYADDR1: TsEdit;
    edt_GoodsBYADDR2: TsEdit;
    edt_GoodsBYADDR3: TsEdit;
    mask_GoodsVALDATE: TsMaskEdit;
    mask_GoodsACCDATE: TsMaskEdit;
    mask_GoodsISSDATE: TsMaskEdit;
    mask_GoodsBSNHSCODE: TsMaskEdit;
    edt_GoodsLRNO2: TsEdit;
    edt_GoodsBYCODE: TsEdit;
    sBitBtn62: TsBitBtn;
    edt_GoodsBYSNAME: TsEdit;
    goodsPanel2: TsPanel;
    sSpeedButton11: TsSpeedButton;
    edt_GoodsSECODE: TsEdit;
    sBitBtn32: TsBitBtn;
    sPanel38: TsPanel;
    edt_GoodsLRNO: TsEdit;
    mask_GoodsGETDATE: TsMaskEdit;
    mask_GoodsEXPDATE: TsMaskEdit;
    sPanel39: TsPanel;
    edt_GoodsSESNAME: TsEdit;
    goodsPanel3: TsPanel;
    sPanel41: TsPanel;
    edt_GoodsLABANK: TsEdit;
    sBitBtn33: TsBitBtn;
    edt_goodsLAELEC: TsEdit;
    curr_goodsISSAMT: TsCurrencyEdit;
    curr_goodsISSEXPAMT: TsCurrencyEdit;
    edt_goodsTOTCNTC: TsEdit;
    curr_goodsTOTCNT: TsCurrencyEdit;
    edt_GoodsLABANKBU: TsEdit;
    curr_goodsEXCH: TsCurrencyEdit;
    edt_goodsISSAMTC: TsEdit;
    sBitBtn34: TsBitBtn;
    curr_goodsISSAMTU: TsCurrencyEdit;
    curr_goodsISSEXPAMTU: TsCurrencyEdit;
    curr_goodsISSTOTAMT: TsCurrencyEdit;
    edt_goodsISSEXPAMTC: TsEdit;
    sBitBtn35: TsBitBtn;
    sBitBtn36: TsBitBtn;
    edt_goodsISSTOTAMTC: TsEdit;
    sBitBtn37: TsBitBtn;
    edt_GoodsLABANKNAME: TsEdit;
    goodsPanel4: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sPanel43: TsPanel;
    sPanel45: TsPanel;
    edt_GoodsREMARK2_1: TsEdit;
    edt_GoodsREMARK2_3: TsEdit;
    edt_GoodsREMARK2_4: TsEdit;
    edt_GoodsREMARK2_5: TsEdit;
    edt_GoodsREMARK2_2: TsEdit;
    edt_GoodsREMARK1_3: TsEdit;
    edt_GoodsREMARK1_5: TsEdit;
    edt_GoodsREMARK1_2: TsEdit;
    edt_GoodsREMARK1_4: TsEdit;
    edt_GoodsREMARK1_1: TsEdit;
    goodsPanel5: TsPanel;
    sSplitter10: TsSplitter;
    sPanel47: TsPanel;
    goodsItemBtnPanel: TsPanel;
    sSpeedButton13: TsSpeedButton;
    sSpeedButton15: TsSpeedButton;
    btn_goodsItemDel: TsSpeedButton;
    sSpeedButton17: TsSpeedButton;
    btn_goodsItemMod: TsSpeedButton;
    sSpeedButton20: TsSpeedButton;
    btn_goodsItemSave: TsSpeedButton;
    btn_goodsItemCancel: TsSpeedButton;
    sSpeedButton32: TsSpeedButton;
    btn_goodsItemNew: TsSpeedButton;
    sDBGrid4: TsDBGrid;
    edt_GoodsLINENO: TsEdit;
    mask_GoodsHSNO: TsMaskEdit;
    edt_GoodsIMDCODE1: TsEdit;
    edt_GoodsSIZE1: TsEdit;
    sBitBtn38: TsBitBtn;
    sBitBtn39: TsBitBtn;
    curr_GoodsTOTQTY: TsCurrencyEdit;
    curr_GoodsPRICEG: TsCurrencyEdit;
    curr_GoodsSUPTOTAMT: TsCurrencyEdit;
    sBitBtn40: TsBitBtn;
    sBitBtn41: TsBitBtn;
    sBitBtn42: TsBitBtn;
    edt_GoodsTOTQTYC: TsEdit;
    edt_GoodsPRICEC: TsEdit;
    edt_GoodsSUPTOTAMTC: TsEdit;
    curr_GoodsQTY: TsCurrencyEdit;
    curr_GoodsSUPAMT: TsCurrencyEdit;
    sBitBtn43: TsBitBtn;
    edt_GoodsSUPAMTC: TsEdit;
    edt_GoodsQTYC: TsEdit;
    sBitBtn44: TsBitBtn;
    curr_GoodsPRICE: TsCurrencyEdit;
    procedure FormShow(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure imdcodeOkClick(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sBitBtn23Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure edt_APPCODEDblClick(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure edt_CPBANKDblClick(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure edt_CPCURRDblClick(Sender: TObject);
    procedure sBitBtn6Click(Sender: TObject);
    procedure sBitBtn51Click(Sender: TObject);
    procedure sButton8Click(Sender: TObject);
    procedure Page_controlChange(Sender: TObject);
    procedure qryGoodsAfterScroll(DataSet: TDataSet);
    procedure qryGoodsItemAfterScroll(DataSet: TDataSet);
    procedure qryTaxAfterScroll(DataSet: TDataSet);
    procedure qryTaxItemAfterScroll(DataSet: TDataSet);
    procedure qryInvAfterScroll(DataSet: TDataSet);
    procedure qryInvItemAfterScroll(DataSet: TDataSet);
    procedure mask_DATEEDblClick(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn8Click(Sender: TObject);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sDBGrid8DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btn_goodsNewClick(Sender: TObject);
    procedure qryGoodsAfterInsert(DataSet: TDataSet);
    procedure qryGoodsAfterCancel(DataSet: TDataSet);
    procedure qryGoodsItemAfterInsert(DataSet: TDataSet);
    procedure btn_goodsItemNewClick(Sender: TObject);
    procedure btn_taxNewClick(Sender: TObject);
    procedure qryTaxAfterInsert(DataSet: TDataSet);
    procedure qryTaxAfterCancel(DataSet: TDataSet);
    procedure btn_taxItemNewClick(Sender: TObject);
    procedure qryTaxItemAfterInsert(DataSet: TDataSet);
    procedure btn_invNewClick(Sender: TObject);
    procedure qryInvAfterInsert(DataSet: TDataSet);
    procedure qryInvAfterCancel(DataSet: TDataSet);
    procedure btn_invItemNewClick(Sender: TObject);
    procedure qryInvItemAfterInsert(DataSet: TDataSet);
    procedure qryInvItemAfterCancel(DataSet: TDataSet);
    procedure btnSaveClick(Sender: TObject);
    procedure goodsBtnPanelClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure sBitBtn60Click(Sender: TObject);
    procedure sBitBtn10Click(Sender: TObject);
    procedure sBitBtn31Click(Sender: TObject);
    procedure sBitBtn45Click(Sender: TObject);
    procedure PopMenuClick(Sender: TObject);
    procedure PopMenuEnabled(Sender: TObject);
    procedure qryListBGM_GUBUNGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure Page_controlChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnPrintClick(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sBitBtn65Click(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure qryListCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    
  private
    { Private declarations }
    APPSPC_SQL : String;
    APPSPCGOODSITEM_SQL : string;
    APPSPCTAXITEM_SQL : string;
    APPSPCINVITEM_SQL : string;
    procedure ReadDocument;
    procedure GoodsReadDocument;
    procedure GoodsItemReadDocument;
    procedure TaxReadDocument;
    procedure TaxItemReadDocument;
    procedure InvReadDocument;
    procedure InvItemReadDocument;

    procedure NewDocument;
    procedure CancelDocument;
    procedure DeleteDocument;
    procedure EditDocument;
                           
    procedure SaveDocument(Sender : TObject); //전체저장
    procedure GoodsSaveDocument;              //물품수령증명서 저장
    procedure GoodsItemSaveDocument;          //물품수령증명서 품목내역 저장
    procedure TaxSaveDocument;                //세금계산서저장
    procedure TaxItemSaveDocument;            //세금계산서품목내역 저장
    procedure InvSaveDocument;                //상업송장 저장
    procedure InvItemSaveDocument;            //상업송장 품목 저장

    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;
    
    function MAX_SEQ(TableName : String; DocGubun : String):Integer;//마지막순번
    function MAX_LINENO(TableName : String; DocGubun: String; AdoQuery: TADOQuery):Integer;//마지막일련번호
    function CHECK_VALIDITY:String;

    procedure ButtonEnable(Val : Boolean=true);
    procedure GoodsButtonEnable(Val : Boolean=true);
    procedure GoodsItemButtonEnable(Val : Boolean=true);
    procedure TaxButtonEnable(Val : Boolean=true);
    procedure TaxItemButtonEnable(Val : Boolean=true);
    procedure InvButtonEnable(Val : Boolean=true);
    procedure InvItemButtonEnable(Val : Boolean=true);

    procedure ReadyDocument(DocNo : String);

    //마지막 콤포넌트에서 탭이나 엔터키 누르면 다음페이지로 이동하는 프로시져
    procedure DialogKey(var msg : TCMDialogKey); message CM_DIALOGKEY;

  public
    { Public declarations }

  protected
    { protected declaration }
    ProgramControlType : TProgramControlType;
  end;

var
  UI_APPSPC_frm: TUI_APPSPC_frm;

implementation

{$R *.dfm}

uses Dialog_SearchCustom, Dialog_BANK, Dialog_CodeList, KISCalendar
     ,Commonlib, DateUtils, MessageDefine, StrUtils, AutoNo, VarDefine, Dlg_Customer,
  Dlg_ErrorMessage, SQLCreator, Dialog_AttachFromLOCAD1, Dialog_AttachFromLOCRC1, Dialog_AttachFromVATBI2,
  APPSPC_PRINT, Dialog_CopyAPPSPC, DocumentSend, Dlg_RecvSelect, CreateDocuments;

procedure TUI_APPSPC_frm.FormShow(Sender: TObject);
begin
  inherited;
  Page_control.ActivePageIndex := 0;

  mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ProgramControlType := ctView;
  
  EnabledControlValue(sPanel4);

  //문서정보
  EnabledControlValue(basicPanel1);
  EnabledControlValue(basicPanel2);
  EnabledControlValue(basicPanel3);
  EnabledControlValue(basicPanel4);
  EnabledControlValue(basicPanel5);
  //물품수령증명서
  EnabledControlValue(goodsDbPanel);
  EnabledControlValue(goodsPanel1);
  EnabledControlValue(goodsPanel2);
  EnabledControlValue(goodsPanel3);
  EnabledControlValue(goodsPanel4);
  EnabledControlValue(goodsPanel5);
  //세금계산서
  EnabledControlValue(taxDbPanel);
  EnabledControlValue(taxPanel1);
  EnabledControlValue(taxPanel2);
  EnabledControlValue(taxPanel3);
  EnabledControlValue(sTabSheet7);
  EnabledControlValue(sTabSheet8);
  EnabledControlValue(sTabSheet9);
  //상업송장
  EnabledControlValue(InvDbPanel);
  EnabledControlValue(invPanel1);
  EnabledControlValue(invPanel2);
  EnabledControlValue(invPanel3);

  ReadList(Mask_fromDate.Text,mask_toDate.Text,'');


end;

//품목 패널보여주기
procedure TUI_APPSPC_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    //물품수령증명서
    401 :
      begin
        GoodsimdcodePanel.Top := 500;
        GoodsimdcodePanel.Left := 288;
        imdcodePanel_edit1.Text := edt_GoodsIMDCODE1.Text
      end;
    //세금계산서
    402:
      begin
        TaxIMDCODEPanel.Top := 500;
        TaxIMDCODEPanel.Left := 288
      end;

    //상업송장
    403:
      begin
        InvIMCODEPanel.Top := 510 ;
        InvIMCODEPanel.Left := 165;
      end;
  end;

end;

//품목패널 확인
procedure TUI_APPSPC_frm.imdcodeOkClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsButton).Tag of
    //물품수령증명서
    401:
      begin
        edt_GoodsIMDCODE1.Text := imdcodePanel_edit1.Text;
        GoodsimdcodePanel.Top := 1000;
      end;
    //세금계산서
    402:
      begin
        edt_TaxIMDCODE1.Text := edt_Panel_TaxIMDCODE1.Text;
        TaxIMDCODEPanel.Top := 1000;
      end;
    //상업송장
    403:
      begin
        edt_InvIMDCODE1.Text := edt_Panel_InvIMDCODE1.Text;
        InvIMCODEPanel.Top := 1000;
      end;
  end;

end;

//품목패널 취소
procedure TUI_APPSPC_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsButton).Tag of
    401 :
      begin
        imdcodePanel_edit1.Text := edt_GoodsIMDCODE1.Text;
        GoodsimdcodePanel.Top := 1000; //물품수령증명서
      end;
    402 :
      begin
        edt_Panel_TaxIMDCODE1.Text := edt_TaxIMDCODE1.Text;
        TaxIMDCODEPanel.Top := 1000; //세금계산서
      end;
    403 :
      begin
        edt_Panel_InvIMDCODE1.Text := edt_InvIMDCODE1.Text;
        InvIMCODEPanel.Top := 1000; //상업송장
      end;
  end;
end;

//규격 패널보여주기
procedure TUI_APPSPC_frm.sBitBtn23Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    //물품수령증명서
    401:
      begin
        GoodsSIZE_Panel.Top := 450;
        GoodsSIZE_Panel.Left := 288;
        panel_edt_size1.Text := edt_GoodsSIZE1.Text;
      end;
     //세금계산서
     402:
      begin
        TaxSIZEPanel.Top := 450;
        TaxSIZEPanel.Left := 288;
      end;
     //상업송장
     403:
      begin
        InvSIZEPanel.Top := 400;
        InvSIZEPanel.Left := 160;
      end;
  end;

end;

//규격패널 확인
procedure TUI_APPSPC_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsButton).Tag of
    //물품수령증명서
    401:
      begin
        edt_GoodsSIZE1.Text := panel_edt_size1.Text;
        GoodsSIZE_Panel.Top := 1000;
      end;
    //세금계산서
    402:
      begin
        edt_TaxSIZE1.Text := edt_Panel_TaxSIZE1.Text;
        TaxSIZEPanel.Top := 1000;
      end;
    //상업송장
    403:
      begin
        edt_InvSIZE1.Text := edt_Panel_InvSIZE1.Text;
        InvSIZEPanel.Top := 1000;
      end;
  end;

end;

//규격패널 취소
procedure TUI_APPSPC_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsButton).Tag of
    401 :
      begin
        panel_edt_size1.Text := edt_GoodsSIZE1.Text;
        GoodsSIZE_Panel.Top := 1000; //물품수령증명
      end;
    402 :
      begin
        edt_Panel_TaxSIZE1.Text := edt_TaxSIZE1.Text;
        TaxSIZEPanel.Top := 1000;  //세금계산서서
      end;

    403 :
      begin
        edt_Panel_InvSIZE1.Text := edt_InvSIZE1.Text;
        InvSIZEPanel.Top := 1000; //상업송장
      end;
  end;
end;

//참조사항패널 활성화
procedure TUI_APPSPC_frm.sBitBtn51Click(Sender: TObject);
begin
  inherited;
  TaxBIGOPanel.Top := 500;
  TaxBIGOPanel.Left := 270;
end;

//참조사항패널 확인 취소
procedure TUI_APPSPC_frm.sButton8Click(Sender: TObject);
begin
  inherited;
  if (Sender as TsButton).Tag = 0 then
  begin
    edt_TaxBIGO1.Text := edt_Panel_TaxBIGO1.Text;
    TaxBIGOPanel.Top := 1000;
  end
  else if (Sender as TsButton).Tag = 1 then
  begin
    edt_Panel_TaxBIGO1.Text := edt_TaxBIGO1.Text;
    TaxBIGOPanel.Top := 1000;
  end;
end;

procedure TUI_APPSPC_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  //
end;

procedure TUI_APPSPC_frm.FormCreate(Sender: TObject);
begin
  inherited;
  APPSPC_SQL := qryList.SQL.Text;
  APPSPCGOODSITEM_SQL := qryGoodsItem.SQL.Text;
  APPSPCTAXITEM_SQL := qryTaxItem.SQL.Text;
  APPSPCINVITEM_SQL := qryInvItem.SQL.Text;
  ProgramControlType := ctView;       

end;

procedure TUI_APPSPC_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
  Action := caFree;
  UI_APPSPC_frm := nil;
end;

procedure TUI_APPSPC_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

function TUI_APPSPC_frm.ReadList(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
  Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := APPSPC_SQL;
    SQL.Add(' WHERE  SPC_D1.DOC_GUBUN = '+QuotedStr('2AP') + ' AND SPC_D1.LR_GUBUN = ' + QuotedStr('LC'));
    SQL.Add(' AND DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_APPSPC_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if Page_control.ActivePageIndex = 4 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := APPSPC_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        SQL.Add(' AND SPC_D1.DOC_GUBUN = '+QuotedStr('2AP'));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 :
      begin
        SQL.Add(' WHERE SPC_H.MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
        SQL.Add(' AND SPC_D1.DOC_GUBUN = '+QuotedStr('2AP'));
      end;
      2 :
      begin
        SQL.Add(' WHERE DF_NAME1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
        SQL.Add(' AND SPC_D1.DOC_GUBUN = '+QuotedStr('2AP'));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add(' ORDER BY DATEE ASC ');
    end;

    Open;
  end;
end;

procedure TUI_APPSPC_frm.ReadDocument;
var
  LRTypeCode : String;
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  mask_DATEE.Text := qryListAPP_DATE.AsString;
  edt_USER_ID.Text := qrylistUSER_ID.AsString;
  edt_msg1.Text := qrylistMESSAGE1.AsString;
  edt_msg2.Text := qrylistMESSAGE2.AsString;

//------------------------------------------------------------------------------
// 기본입력사항
//------------------------------------------------------------------------------
  //전자문서구분
  if qryListBGM_GUBUN.AsString = '2BJ' then
    combo_BGMGUBUN.ItemIndex := 1
  else if qryListBGM_GUBUN.AsString = '2BK' then
    combo_BGMGUBUN.ItemIndex := 0;

  //문서번호
  edt_BGM1.Text := qryListBGM1.AsString;
  edt_BGM2.Text := qryListBGM2.AsString;
  edt_BGM3.Text := qryListBGM3.AsString;
  edt_BGM4.Text := qryListBGM4.AsString;

  //신청번호
  edt_CPNO.Text := qryListCP_NO.AsString;

//------------------------------------------------------------------------------
// 신청인
//------------------------------------------------------------------------------
  //상호코드
  edt_APPCODE.Text := qryListAPP_CODE.AsString;
  //상호
  edt_APPSNAME.Text := qryListAPP_SNAME.AsString;
  //대표자
  edt_APPNAME.Text := qryListAPP_NAME.AsString;
  //사업자등록번호
  edt_APPSAUPNO.Text := qryListAPP_SAUPNO.AsString;
  //주소
  edt_APPADDR1.Text := qryListAPP_ADDR1.AsString;
  edt_APPADDR2.Text := qryListAPP_ADDR2.AsString;
  edt_APPADDR3.Text := qryListAPP_ADDR3.AsString;
  //전자서명
  edt_APPELEC.Text := qryListAPP_ELEC.AsString;
//------------------------------------------------------------------------------
// 구매자
//------------------------------------------------------------------------------
  //사업자등록번호
  mask_DFSAUPNO.Text := qryListDF_SAUPNO.AsString;
  //상호
  edt_DFNAME1.Text := qryListDF_NAME1.AsString;
  edt_DFNAME2.Text := qryListDF_NAME2.AsString;
  edt_DFNAME3.Text := qryListDF_NAME3.AsString;
  //E-Mail
  edt_DFEMAIL1.Text := qryListDF_EMAIL1.AsString;
  edt_DFEMAIL2.Text := qryListDF_EMAIL2.AsString;
//------------------------------------------------------------------------------
// 추심은행
//------------------------------------------------------------------------------
  //은행코드
  edt_CPBANK.Text := qryListCP_BANK.AsString;
  //은행명
  edt_CPBANKNAME.Text := qryListCP_BANKNAME.AsString;
  //지점명
  edt_CPBANKBU.Text := qryListCP_BANKBU.AsString;
  //계좌번호
  edt_CPACCOUNTNO.Text := qryListCP_ACCOUNTNO.AsString;
  //계좌주
  edt_CPNAME1.Text := qryListCP_NAME1.AsString;
  edt_CPNAME2.Text := qryListCP_NAME2.AsString;
  //계좌통화
  edt_CPCURR.Text := qryListCP_CURR.AsString;
//------------------------------------------------------------------------------
// 추가계좌
//------------------------------------------------------------------------------
  //계좌번호1
  edt_CPADDACCOUNTNO1.Text := qryListCP_ADD_ACCOUNTNO1.AsString;
  //계좌주1
  edt_CPADDNAME1.Text := qryListCP_ADD_NAME1.AsString;
  //계좌통화
  edt_CPADDCURR1.Text := qryListCP_ADD_CURR1.AsString;
  //계좌번호2
  edt_CPADDACCOUNTNO2.Text := qryListCP_ADD_ACCOUNTNO2.AsString;
  //계좌주2
  edt_CPADDNAME2.Text := qryListCP_ADD_NAME2.AsString;
  //계좌통화
  edt_CPADDCURR2.Text := qryListCP_ADD_CURR2.AsString;
//------------------------------------------------------------------------------
// 추심금액
//------------------------------------------------------------------------------
  //추심금액 외화단위
  edt_CPAMTC.Text := qryListCP_AMTC.AsString;
  //추심금액 외화
  curr_CPAMTU.Value := qryListCP_AMTU.AsCurrency;
  //적용환율
  curr_CPCUX.Text := qryListCP_CUX.AsString;
  //추심금액 원화
  curr_CPAMT.Value := qryListCP_AMT.AsCurrency;
//------------------------------------------------------------------------------
// 내국신용장
//------------------------------------------------------------------------------
  //문서번호
  edt_DOCNO.Text := qryListDOC_NO.AsString;
  //개설일자
  mask_ISSDATE.Text := qryListISS_DATE.AsString;
  //개설은행
  edt_LABANKBUCODE.Text := qryListLA_BANKBUCODE.AsString;
  edt_LABANKNAMEP.Text := qryListLA_BANKNAMEP.AsString;
  //내국신용장 번호
  edt_LRNO.Text := qryListLR_NO.AsString;
  //개설은행지점명
  edt_LABANKBUP.Text := qryListLA_BANKBUP.AsString;
  //종류
  if qryListLA_TYPE.AsString = '2AA' then combo_LATYPE.ItemIndex := 0;
  if qryListLA_TYPE.AsString = '2AB' then combo_LATYPE.ItemIndex := 1;
  if qryListLA_TYPE.AsString = '2AC' then combo_LATYPE.ItemIndex := 2;
  if qryListLA_TYPE.AsString = '' then combo_LATYPE.ItemIndex := -1;



end;

procedure TUI_APPSPC_frm.GoodsReadDocument;
begin
  inherited;
  if not qryGoods.Active then Exit;
  if qryGoods.RecordCount = 0 then Exit;

//------------------------------------------------------------------------------
//물품수령증명서
//------------------------------------------------------------------------------
  //문서번호
  edt_GoodsDOCNO.Text := qryGoodsDOC_NO.AsString;
  //발급번호
  edt_GoodsLRNO2.Text := qryGoodsLR_NO2.AsString;
  //HS부호
  mask_GoodsBSNHSCODE.Text := qryGoodsBSN_HSCODE.AsString;
  //발급일자
  mask_GoodsISSDATE.Text := qryGoodsISS_DATE.AsString;
  //인수일자
  mask_GoodsACCDATE.Text := qryGoodsACC_DATE.AsString;
  //유효기일
  mask_GoodsVALDATE.Text := qryGoodsVAL_DATE.AsString;

//------------------------------------------------------------------------------
//물품수령인
//------------------------------------------------------------------------------
  //상호코드
  edt_GoodsBYCODE.Text := qryGoodsBY_CODE.AsString;
  //상호
  edt_GoodsBYSNAME.Text := qryGoodsBY_SNAME.AsString;
  //대표자명
  edt_GoodsBYNAME.Text := qryGoodsBY_NAME.AsString;
  //전자서명
  edt_GoodsBYELEC.Text := qryGoodsBY_ELEC.AsString;
  //주소
  edt_GoodsBYADDR1.Text := qryGoodsBY_ADDR1.AsString;
  edt_GoodsBYADDR2.Text := qryGoodsBY_ADDR2.AsString;
  edt_GoodsBYADDR3.Text := qryGoodsBY_ADDR3.AsString;

//------------------------------------------------------------------------------
//내국신용장
//------------------------------------------------------------------------------
  //발급번호
  edt_GoodsLRNO.Text := qryGoodsLR_NO.AsString;
  //물품인도기일
  mask_GoodsGETDATE.Text := qryGoodsGET_DATE.AsString;
  //유효기일
  mask_GoodsEXPDATE.Text := qryGoodsEXP_DATE.AsString;

//------------------------------------------------------------------------------
//공급자
//------------------------------------------------------------------------------
  //공급자코드
  edt_GoodsSECODE.Text := qryGoodsSE_CODE.AsString;
  //공급자
  edt_GoodsSESNAME.Text := qryGoodsSE_SNAME.AsString;

//------------------------------------------------------------------------------
//개설은행
//------------------------------------------------------------------------------
  //은행코드
  edt_GoodsLABANK.Text := qryGoodsLA_BANK.AsString;
  //은행
  edt_GoodsLABANKNAME.Text := qryGoodsLA_BANKNAME.AsString;
  //지점명
  edt_GoodsLABANKBU.Text := qryGoodsLA_BANKBU.AsString;
  //전자서명
  edt_goodsLAELEC.Text := qryGoodsLA_ELEC.AsString;
  //매매기준율
  curr_goodsEXCH.Value := qryGoodsEXCH.AsCurrency;
  //개설금액
  curr_goodsISSAMT.Value := qryGoodsISS_AMT.AsCurrency;
  //개설금액 외화
  curr_goodsISSAMTU.Value := qryGoodsISS_AMTU.AsCurrency;
  //개설금액 외화단위
  edt_goodsISSAMTC.Text := qryGoodsISS_AMTC.AsString;
  //인수금액
  curr_goodsISSEXPAMT.Value := qryGoodsISS_EXPAMT.AsCurrency;
  //인수금액외화
  curr_goodsISSEXPAMTU.Value := qryGoodsISS_EXPAMTU.AsCurrency;
  //인수금액 외화단위
  edt_goodsISSEXPAMTC.Text := qryGoodsISS_EXPAMTC.AsString;
  //총수량 단위
  edt_goodsTOTCNTC.Text := qryGoodsTOTCNTC.AsString;
  //총수량
  curr_goodsTOTCNT.Value := qryGoodsTOTCNT.AsCurrency;
  //총금액단위
  edt_goodsISSTOTAMTC.Text := qryGoodsISS_TOTAMTC.AsString;
  //총금액
  curr_goodsISSTOTAMT.Value := qryGoodsISS_TOTAMT.AsCurrency;

//------------------------------------------------------------------------------
//기타조건
//------------------------------------------------------------------------------
  edt_GoodsREMARK2_1.Text := qryGoodsREMARK2_1.AsString;
  edt_GoodsREMARK2_2.Text := qryGoodsREMARK2_2.AsString;
  edt_GoodsREMARK2_3.Text := qryGoodsREMARK2_3.AsString;
  edt_GoodsREMARK2_4.Text := qryGoodsREMARK2_4.AsString;
  edt_GoodsREMARK2_5.Text := qryGoodsREMARK2_5.AsString;

//------------------------------------------------------------------------------
//참조사항
//------------------------------------------------------------------------------
  edt_GoodsREMARK1_1.Text := qryGoodsREMARK1_1.AsString;
  edt_GoodsREMARK1_2.Text := qryGoodsREMARK1_2.AsString;
  edt_GoodsREMARK1_3.Text := qryGoodsREMARK1_3.AsString;
  edt_GoodsREMARK1_4.Text := qryGoodsREMARK1_4.AsString;
  edt_GoodsREMARK1_5.Text := qryGoodsREMARK1_5.AsString;
end;

procedure TUI_APPSPC_frm.GoodsItemReadDocument;
begin
  if not qryGoodsItem.Active then Exit;
  if qryGoodsItem.RecordCount = 0 then Exit;

  //일련번호
  edt_GoodsLINENO.Text := qryGoodsItemLINE_NO.AsString;
  //HS부호
  mask_GoodsHSNO.Text := qryGoodsItemHS_NO.AsString;
  //품목
  edt_GoodsIMDCODE1.Text := qryGoodsItemIMD_CODE1.AsString;
  edt_GoodsIMDCODE2.Text := qryGoodsItemIMD_CODE2.AsString;
  edt_GoodsIMDCODE3.Text := qryGoodsItemIMD_CODE3.AsString;
  edt_GoodsIMDCODE4.Text := qryGoodsItemIMD_CODE4.AsString;
  edt_Panel_InvIMDCODE1.Text := qryGoodsItemIMD_CODE1.AsString;
  //규격
  edt_GoodsSIZE1.Text := qryGoodsItemSIZE1.AsString;
  edt_GoodsSIZE2.Text := qryGoodsItemSIZE2.AsString;
  edt_GoodsSIZE3.Text := qryGoodsItemSIZE3.AsString;
  edt_GoodsSIZE4.Text := qryGoodsItemSIZE4.AsString;
  edt_GoodsSIZE5.Text := qryGoodsItemSIZE5.AsString;
  edt_GoodsSIZE6.Text := qryGoodsItemSIZE6.AsString;
  edt_GoodsSIZE7.Text := qryGoodsItemSIZE7.AsString;
  edt_GoodsSIZE8.Text := qryGoodsItemSIZE8.AsString;
  edt_GoodsSIZE9.Text := qryGoodsItemSIZE9.AsString;
  edt_GoodsSIZE10.Text := qryGoodsItemSIZE10.AsString;
  panel_edt_size1.Text := qryGoodsItemSIZE1.AsString;
  //수량단위
  edt_GoodsQTYC.Text := qryGoodsItemQTYC.AsString;
  //수량
  curr_GoodsQTY.Value := qryGoodsItemQTY.AsCurrency;
  //수량소계단위
  edt_GoodsTOTQTYC.Text := qryGoodsItemTOTQTYC.AsString;
  //수량소계
  curr_GoodsTOTQTY.Value := qryGoodsItemTOTQTY.AsCurrency;
  //단가
  curr_GoodsPRICE.Value := qryGoodsItemPRICE.AsCurrency;
  //기준수량단위
  edt_GoodsPRICEC.Text := qryGoodsItemPRICEC.AsString;
  //기준수량
  curr_GoodsPRICEG.Value := qryGoodsItemPRICE_G.AsCurrency;
  //금액단위
  edt_GoodsSUPAMTC.Text := qryGoodsItemSUP_AMTC.AsString;
  //금액
  curr_GoodsSUPAMT.Value := qryGoodsItemSUP_AMT.AsCurrency;
  //금액소계단위
  edt_GoodsSUPTOTAMTC.Text := qryGoodsItemSUP_TOTAMTC.Text;
  //금액소계
  curr_GoodsSUPTOTAMT.Value := qryGoodsItemSUP_TOTAMT.AsCurrency;

end;

procedure TUI_APPSPC_frm.TaxReadDocument;
begin
  if not qryTax.Active then Exit;
  if qryTax.RecordCount = 0 then Exit;

//------------------------------------------------------------------------------
//기본입력사항
//------------------------------------------------------------------------------
  //문서번호
  edt_TaxDOCNO.Text := qryTaxDOC_NO.AsString;
  // 권 / 호 / 일련번호
  edt_TaxVBRENO.Text := qryTaxVB_RENO.AsString;
  edt_TaxVBSENO.Text := qryTaxVB_SENO.AsString;
  edt_TaxVBFSNO.Text := qryTaxVB_FSNO.AsString;
  //관리번호
  edt_TaxVBMAINTNO.Text := qryTaxVB_MAINTNO.AsString;
  //작성일
  mask_TaxISSDATE.Text := qryTaxISS_DATE.AsString;
  //참조번호
  edt_TaxVBDMNO.Text := qryTaxVB_DMNO.AsString;
//------------------------------------------------------------------------------
//공급자
//------------------------------------------------------------------------------
  //상호코드
  edt_TaxSECODE.Text := qryTaxSE_CODE.AsString;
  //상호
  edt_TaxSESNMAE.Text := qryTaxSE_SNAME.AsString;
  //대표자
  edt_TaxSENAME.Text := qryTaxSE_NAME.AsString;
  //사업자번호
  mask_TaxSESAUPNO.Text := qryTaxSE_SAUPNO.AsString;
  //주소
  edt_TaxSEADDR1.Text := qryTaxSE_ADDR1.AsString;
  edt_TaxSEADDR2.Text := qryTaxSE_ADDR2.AsString;
  edt_TaxSEADDR3.Text := qryTaxSE_ADDR3.AsString;
  //전자서명
  edt_TaxSEELEC.Text := qryTaxSE_ELEC.AsString;
  //업태
  edt_TaxSGUPTAI1_1.Text := qryTaxSG_UPTAI1_1.AsString;
  edt_TaxSGUPTAI1_2.Text := qryTaxSG_UPTAI1_2.AsString;
  edt_TaxSGUPTAI1_3.Text := qryTaxSG_UPTAI1_3.AsString;
  //종목
  edt_TaxHNITEM1_1.Text := qryTaxHN_ITEM1_1.AsString;
  edt_TaxHNITEM1_2.Text := qryTaxHN_ITEM1_2.AsString;
  edt_TaxHNITEM1_3.Text := qryTaxHN_ITEM1_3.AsString;
//------------------------------------------------------------------------------
//공급받는자
//------------------------------------------------------------------------------
  //상호코드
  edt_TaxBYCODE.Text := qryTaxBY_CODE.AsString;
  //상호
  edt_TaxBYSNAME.Text := qryTaxBY_SNAME.AsString;
  //대표자
  edt_TaxBYNAME.Text := qryTaxBY_NAME.AsString;
  //사업자번호
  mask_TaxBYSAUPNO.Text := qryTaxBY_SAUPNO.AsString;
  //주소
  edt_TaxBYADDR1.Text := qryTaxBY_ADDR1.AsString;
  edt_TaxBYADDR2.Text := qryTaxBY_ADDR2.AsString;
  edt_TaxBYADDR3.Text := qryTaxBY_ADDR3.AsString;
  //전자서명
  edt_TaxBYELEC.Text := qryTaxBY_ELEC.AsString;
  //업태
  edt_TaxSGUPTAI2_1.Text := qryTaxSG_UPTAI2_1.AsString;
  edt_TaxSGUPTAI2_2.Text := qryTaxSG_UPTAI2_2.AsString;
  edt_TaxSGUPTAI2_3.Text := qryTaxSG_UPTAI2_3.AsString;
  //종목
  edt_TaxHNITEM2_1.Text := qryTaxHN_ITEM2_1.AsString;
  edt_TaxHNITEM2_2.Text := qryTaxHN_ITEM2_2.AsString;
  edt_TaxHNITEM2_3.Text := qryTaxHN_ITEM2_3.AsString;
//------------------------------------------------------------------------------
//수탁자
//------------------------------------------------------------------------------
  //상호코드
  edt_TaxAGCODE.Text := qryTaxAG_CODE.AsString;
  //상호
  edt_TaxAGSNAME.Text := qryTaxAG_SNAME.AsString;
  //대표자
  edt_TaxAGNAME.Text := qryTaxAG_NAME.AsString;
  //사업자번호
  mask_TaxAGSAUPNO.Text := qryTaxAG_SAUPNO.AsString;
  //주소
  edt_TaxAGADDR1.Text := qryTaxAG_ADDR1.AsString;
  edt_TaxAGADDR2.Text := qryTaxAG_ADDR2.AsString;
  edt_TaxAGADDR3.Text := qryTaxAG_ADDR3.AsString;
  //전자서명
  edt_TaxSEELEC.Text := qryTaxAG_ELEC.AsString;
//------------------------------------------------------------------------------
//결제방법
//------------------------------------------------------------------------------
  //현금원화
  curr_TaxPAIAMT1.Value := qryTaxPAI_AMT1.AsCurrency;
  //현금외화
  curr_TaxPAIAMTU1.Value := qryTaxPAI_AMTU1.AsCurrency;
  //현금외화단위
  edt_TaxPAIAMTC1.Text := qryTaxPAI_AMTC1.AsString;
  //수표원화
  curr_TaxPAIAMT2.Value := qryTaxPAI_AMT2.AsCurrency;
  //수표외화
  curr_TaxPAIAMTU2.Value := qryTaxPAI_AMTU2.AsCurrency;
  //수표외화단위
  edt_TaxPAIAMTC2.Text := qryTaxPAI_AMTC2.AsString;
  //어음원화
  curr_TaxPAIAMT3.Value := qryTaxPAI_AMT3.AsCurrency;
  //어음외화
  curr_TaxPAIAMTU3.Value := qryTaxPAI_AMTU3.AsCurrency;
  //어음외화단위
  edt_TaxPAIAMTC3.Text := qryTaxPAI_AMTC3.AsString;
  //외상원화
  curr_TaxPAIAMT4.Value := qryTaxPAI_AMT4.AsCurrency;
  //외상외화
  curr_TaxPAIAMTU4.Value := qryTaxPAI_AMTU4.AsCurrency;
  //외상외화단위
  edt_TaxPAIAMTC4.Text := qryTaxPAI_AMTC4.AsString;
//------------------------------------------------------------------------------
//금액 및 수량
//------------------------------------------------------------------------------
  //공급가액
  curr_TaxISSEXPAMT.Value := qryTaxISS_EXPAMT.AsCurrency;
  //세액
  curr_TaxISSEXPAMTU.Value := qryTaxISS_EXPAMTU.AsCurrency;
  //총수량
  curr_TaxTOTCNT.Value := qryTaxTOTCNT.AsCurrency;
  //총수량단위
  edt_TaxTOTCNTC.Text := qryTaxTOTCNTC.AsString;
  //영수/청구구분
  edt_TaxVBGUBUN.Text := qryTaxVB_GUBUN.AsString;
  //공란수
  edt_TaxVBDETAILNO.Text := qryTaxVB_DETAILNO.AsString;
//------------------------------------------------------------------------------
//비고
//------------------------------------------------------------------------------
  edt_TaxREMARK1_1.Text := qryTaxREMARK1_1.AsString;
  edt_TaxREMARK1_2.Text := qryTaxREMARK1_2.AsString;
  edt_TaxREMARK1_3.Text := qryTaxREMARK1_3.AsString;
  edt_TaxREMARK1_4.Text := qryTaxREMARK1_4.AsString;
  edt_TaxREMARK1_5.Text := qryTaxREMARK1_5.AsString;
end;

procedure TUI_APPSPC_frm.TaxItemReadDocument;
begin
  if not qryTaxItem.Active then Exit;
  if qryTaxItem.RecordCount = 0 then Exit;

  //일련번호
  edt_TaxLINENO.Text := qryTaxItemLINE_NO.AsString;
  //공급일자
  mask_TaxDEDATE.Text := qryTaxItemDE_DATE.AsString;
  //품목
  edt_TaxIMDCODE1.Text := qryTaxItemIMD_CODE1.AsString;
  edt_TaxIMDCODE2.Text := qryTaxItemIMD_CODE2.AsString;
  edt_TaxIMDCODE3.Text := qryTaxItemIMD_CODE3.AsString;
  edt_TaxIMDCODE4.Text := qryTaxItemIMD_CODE4.AsString;
  edt_Panel_TaxIMDCODE1.Text := qryTaxItemIMD_CODE1.AsString;
  //규격
  edt_TaxSIZE1.Text := qryTaxItemSIZE1.AsString;
  edt_TaxSIZE2.Text := qryTaxItemSIZE2.AsString;
  edt_TaxSIZE3.Text := qryTaxItemSIZE3.AsString;
  edt_TaxSIZE4.Text := qryTaxItemSIZE4.AsString;
  edt_TaxSIZE5.Text := qryTaxItemSIZE5.AsString;
  edt_TaxSIZE6.Text := qryTaxItemSIZE6.AsString;
  edt_TaxSIZE7.Text := qryTaxItemSIZE7.AsString;
  edt_TaxSIZE8.Text := qryTaxItemSIZE8.AsString;
  edt_TaxSIZE9.Text := qryTaxItemSIZE9.AsString;
  edt_TaxSIZE10.Text := qryTaxItemSIZE10.AsString;
  edt_Panel_TaxSIZE1.Text := qryTaxItemSIZE1.AsString;
  //수량단위
  edt_TaxQTYC.Text := qryTaxItemQTYC.AsString;
  //수량
  curr_TaxQTY.Value := qryTaxItemQTY.AsCurrency;
  //수량소계단위
  edt_TaxTOTQTYC.Text := qryTaxItemTOTQTYC.AsString;
  //수량소계
  curr_TaxTOTQTY.Value := qryTaxItemTOTQTY.AsCurrency;
  //환율
  curr_TaxCUXRATE.Value := qryTaxItemCUX_RATE.AsCurrency;
  //단가
  curr_TaxPRICE.Value := qryTaxItemPRICE.AsCurrency;
  //기준수량단위
  edt_TaxPRICEC.Text := qryTaxItemPRICEC.AsString;
  //기준수량
  curr_TaxPRICEG.Value := qryTaxItemPRICE_G.AsCurrency;
  //공급가액단위
  edt_TaxSUPAMTC.Text := qryTaxItemSUP_AMTC.AsString;
  //공급가액
  curr_TaxSUPAMT.Value := qryTaxItemSUP_AMT.AsCurrency;
  //공급가액소계단위
  edt_TaxSUPTOTAMTC.Text := qryTaxItemSUP_TOTAMTC.AsString;
  //공급가액소계
  curr_TaxSUPTOTAMT.Value := qryTaxItemSUP_TOTAMT.AsCurrency;
  //공급가액외화단위
  edt_TaxVBAMTC.Text := qryTaxItemVB_AMTC.AsString;
  //공급가액외화
  curr_TaxVBAMT.Value := qryTaxItemVB_AMT.AsCurrency;
  //공급가액외화소계단위
  edt_VBTOTAMTC.Text := qryTaxItemVB_TOTAMTC.AsString;
  //공급가액외화소계
  curr_VBTOTAMT.Value := qryTaxItemVB_TOTAMT.AsCurrency;
  //세액단위
  edt_TaxVBTAXC.Text := qryTaxItemVB_TAXC.AsString;
  //세액
  curr_TaxVBTAX.Value := qryTaxItemVB_TAX.AsCurrency;
  //세액소계단위
  edt_TaxVBTOTTAXC.Text := qryTaxItemVB_TOTTAXC.AsString;
  //세액소계
  curr_TaxVBTOTTAX.Value := qryTaxItemVB_TOTTAX.AsCurrency;
  //참조사항
  edt_TaxBIGO1.Text := qryTaxItemBIGO1.AsString;
  edt_TaxBIGO2.Text := qryTaxItemBIGO2.AsString;
  edt_TaxBIGO3.Text := qryTaxItemBIGO3.AsString;
  edt_TaxBIGO4.Text := qryTaxItemBIGO4.AsString;
  edt_TaxBIGO5.Text := qryTaxItemBIGO5.AsString;
  edt_Panel_TaxBIGO1.Text := qryTaxItemBIGO1.AsString;
end;

procedure TUI_APPSPC_frm.InvReadDocument;
begin
  if not qryInv.Active then Exit;
  if qryInv.RecordCount = 0 then Exit;

//------------------------------------------------------------------------------
// 상업송장증명서
//------------------------------------------------------------------------------
  //문서번호
  edt_InvDOCNO.Text := qryInvDOC_NO.AsString;
  //발급일자
  mask_InvISSDATE.Text := qryInvISS_DATE.AsString;
  //총수량
  curr_InvTOTCNT.Value := qryInvTOTCNT.AsCurrency;
  //총수량단위
  edt_InvTOTCNTC.Text := qryInvTOTCNTC.AsString;
  //총금액
  curr_InvISSTOTAMT.Value := qryInvISS_TOTAMT.AsCurrency;
  //총금액단위
  edt_InvISSTOTAMTC.Text := qryInvISS_TOTAMTC.AsString;
//------------------------------------------------------------------------------
// 비고
//------------------------------------------------------------------------------
  //항차
  edt_InvREMARK1_1.Text := qryInvREMARK1_1.AsString;
  //선명
  edt_InvREMARK1_2.Text := qryInvREMARK1_2.AsString;
  //선적지
  edt_InvREMARK1_3.Text := qryInvREMARK1_3.AsString;
  //도착지
  edt_InvREMARK1_4.Text := qryInvREMARK1_4.AsString;
  //인도조건
  edt_InvREMARK1_5.Text := qryInvREMARK1_5.AsString;
//------------------------------------------------------------------------------
// 물품공급자(수출자)
//------------------------------------------------------------------------------
  //상호코드
  edt_InvSECODE.Text := qryInvSE_CODE.AsString;
  //상호
  edt_InvSESNAME.Text := qryInvSE_SNAME.AsString;
  //대표자
  edt_InvSENAME.Text := qryInvSE_NAME.AsString;
  //주소
  edt_InvSEADDR1.Text := qryInvSE_ADDR1.AsString;
  edt_InvSEADDR2.Text := qryInvSE_ADDR2.AsString;
  edt_InvSEADDR3.Text := qryInvSE_ADDR3.AsString;
//------------------------------------------------------------------------------
// 공급받는자(수입자)
//------------------------------------------------------------------------------
  //상호코드
  edt_InvBYCODE.Text := qryInvSE_CODE.AsString;
  //상호
  edt_InvBYSNAME.Text := qryInvSE_SNAME.AsString;
  //대표자
  edt_InvBYNAME.Text := qryInvSE_NAME.AsString;
  //주소
  edt_InvBYADDR1.Text := qryInvSE_ADDR1.AsString;
  edt_InvBYADDR2.Text := qryInvSE_ADDR2.AsString;
  edt_InvBYADDR3.Text := qryInvSE_ADDR3.AsString;
end;

procedure TUI_APPSPC_frm.InvItemReadDocument;
begin
  if not qryInv.Active then Exit;
  if qryInv.RecordCount = 0 then Exit;

  //일련번호
  edt_InvLINENO.Text := qryInvItemLINE_NO.AsString;
  //품목
  edt_InvIMDCODE1.Text := qryInvItemIMD_CODE1.AsString;
  edt_InvIMDCODE2.Text := qryInvItemIMD_CODE2.AsString;
  edt_InvIMDCODE3.Text := qryInvItemIMD_CODE3.AsString;
  edt_InvIMDCODE4.Text := qryInvItemIMD_CODE4.AsString;
  edt_Panel_InvIMDCODE1.Text := qryInvItemIMD_CODE1.AsString;
  //규격
  edt_InvSIZE1.Text := qryInvItemSIZE1.AsString;
  edt_InvSIZE2.Text := qryInvItemSIZE2.AsString;
  edt_InvSIZE3.Text := qryInvItemSIZE3.AsString;
  edt_InvSIZE4.Text := qryInvItemSIZE4.AsString;
  edt_InvSIZE5.Text := qryInvItemSIZE5.AsString;
  edt_InvSIZE6.Text := qryInvItemSIZE6.AsString;
  edt_InvSIZE7.Text := qryInvItemSIZE7.AsString;
  edt_InvSIZE8.Text := qryInvItemSIZE8.AsString;
  edt_InvSIZE9.Text := qryInvItemSIZE9.AsString;
  edt_InvSIZE10.Text := qryInvItemSIZE10.AsString;
  edt_Panel_InvSIZE1.Text := qryInvItemSIZE1.AsString;
  //수량단위
  edt_InvQTYC.Text := qryInvItemQTYC.AsString;
  //수량
  curr_InvQTY.Value := qryInvItemQTY.AsCurrency;
  //수량소계단위
  edt_InvTOTQTYC.Text := qryInvItemQTYC.AsString;
  //수량소계
  curr_InvTOTQTY.Value := qryInvItemQTY.AsCurrency;
  //단가
  curr_InvPRICE.Value := qryInvItemPRICE.AsCurrency;
  //기준수량단위
  edt_InvPRICEC.Text := qryInvItemPRICEC.AsString;
  //기준수량
  curr_InvPRICEG.Value := qryInvItemPRICE_G.AsCurrency;
  //공급가액외화단위
  edt_InvSUPAMTC.Text := qryInvItemSUP_AMTC.AsString;
  //공급가액외화
  curr_InvSUPAMT.Value := qryInvItemSUP_AMT.AsCurrency;
  //금액소계단위
  edt_InvSUPTOTAMTC.Text := qryInvItemSUP_TOTAMTC.AsString;
  //금액소계
  curr_InvSUPTOTAMT.Value := qryInvItemSUP_TOTAMT.AsCurrency;
end;

procedure TUI_APPSPC_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsbrowse then ReadDocument;

  case Page_control.ActivePageIndex of
    //물품수령증명서
    1 :
      begin
        //------------------------------------------------------------------------------
        // 물품수령증명서 OPEN
        //------------------------------------------------------------------------------
          ClearControlValue(goodsPanel1);
          ClearControlValue(goodsPanel2);
          ClearControlValue(goodsPanel3);
          ClearControlValue(goodsPanel4);
          ClearControlValue(goodsPanel5);                                                                               

          with qryGoods do
          begin
            Close;
            Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
            Open;
          end;

          with qryGoodsItem do
          begin
            Close;
            SQL.Clear;
            SQL.Text := APPSPCGOODSITEM_SQL;
            Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
            Parameters.ParamByName('SEQNO').Value := qryGoods.FieldByName('SEQ').AsInteger;
            //ShowMessage(SQL.Text);
            Open;
          end;

        if qryGoods.State = dsBrowse then GoodsReadDocument;
        if qryGoodsItem.State = dsBrowse then GoodsItemReadDocument;
      end;

    //세금계산서
    2 :
      begin
        //------------------------------------------------------------------------------
        // 세금계산서 OPEN
        //------------------------------------------------------------------------------
          ClearControlValue(taxPanel1);
          ClearControlValue(taxPanel2);
          ClearControlValue(taxPanel3);
          ClearControlValue(sTabSheet7);
          ClearControlValue(sTabSheet8);
          ClearControlValue(sTabSheet9);
          with qryTax do
          begin
            Close;
            Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
            Open;
          end;

          with qryTaxItem do
          begin
            Close;
            SQL.Clear;
            sql.Text := APPSPCTAXITEM_SQL;
            Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
            Parameters.ParamByName('SEQNO').Value := qryTax.FieldByName('SEQ').AsInteger;
            Open;
          end;

        if qryTax.State = dsBrowse then TaxReadDocument;
        if qryTaxItem.State = dsBrowse then TaxItemReadDocument;
      end;

    //상업송장
    3 :
      begin
        //------------------------------------------------------------------------------
        // 물품송장 OPEN
        //------------------------------------------------------------------------------
        ClearControlValue(invPanel1);
        ClearControlValue(invPanel2);
        ClearControlValue(invPanel3);
        with qryInv do
        begin
          Close;
          Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
          Open;
        end;

        with qryInvItem do
        begin
          Close;
          SQL.Clear;
          SQL.Text := APPSPCINVITEM_SQL;
          Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
          Parameters.ParamByName('SEQNO').Value := qryInv.FieldByName('SEQ').AsInteger;
          Open;
        end;
        
        if qryInv.State = dsBrowse then InvReadDocument;
        if qryInvItem.State = dsBrowse then InvItemReadDocument;
      end;
  end;
end;

procedure TUI_APPSPC_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then
  begin
    qrylistAfterScroll(qrylist);
  end;
end;

procedure TUI_APPSPC_frm.edt_APPCODEDblClick(Sender: TObject);
begin
 inherited;
  Dialog_SearchCustom_frm := TDialog_SearchCustom_frm.Create(Self);
  Dialog_SearchCustom_frm.Caption := '업체정보';
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      IF Dialog_SearchCustom_frm.openDialog((Sender as TsEdit).Text) = mrOK then
      begin
        (Sender as TsEdit).Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        case (Sender as TsEdit).Tag of
          //신청인 상호코드
          101 :
          begin
              //상호
              edt_APPSNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              //대표자
              edt_APPNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
              //사업자등록번호
              edt_APPSAUPNO.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('SAUP_NO').AsString;
              //주소
              edt_APPADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_APPADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              edt_APPADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
              //전자서명
              edt_APPELEC.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('JENJA').AsString;
          end;

          //물품수령증명서 물품수령인
          102 :
          begin
              //상호
              edt_GoodsBYSNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              //대표자
              edt_GoodsBYNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
              //전자서명
              edt_GoodsBYELEC.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('JENJA').AsString;
              //주소
              edt_GoodsBYADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_GoodsBYADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              edt_GoodsBYADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
          end;

          //세금계산서 공급자
          103:
          begin
              //상호
              edt_TaxSESNMAE.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              //대표자
              edt_TaxSENAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
              //사업자등록번호
              mask_TaxSESAUPNO.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('SAUP_NO').AsString;
              //주소
              edt_TaxSEADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_TaxSEADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              edt_TaxSEADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
              //전자서명
              edt_TaxSEELEC.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('JENJA').AsString;
          end;

          //세금계산서 공급받는자
          104:
          begin
              //상호
              edt_TaxBYSNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              //대표자
              edt_TaxBYNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
              //사업자등록번호
              mask_TaxBYSAUPNO.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('SAUP_NO').AsString;
              //주소
              edt_TaxBYADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_TaxBYADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              edt_TaxBYADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
              //전자서명
              edt_TaxBYELEC.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('JENJA').AsString;
          end;

          //세금계산서 수탁자
          105:
          begin
              //상호
              edt_TaxAGSNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              //대표자
              edt_TaxAGNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
              //사업자등록번호
              mask_TaxAGSAUPNO.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('SAUP_NO').AsString;
              //주소
              edt_TaxAGADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_TaxAGADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              edt_TaxAGADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
              //전자서명
              edt_TaxAGELEC.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('JENJA').AsString;
          end;

          //상업송장 물품공급자(수출자)
          106 :
          begin
              //상호
              edt_InvSESNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              //대표자
              edt_InvSENAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
              //주소
              edt_InvSEADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_InvSEADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              edt_InvSEADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
          end;

          //상업송장 공급받는자(수입자)
          107 :
          begin
              //상호
              edt_InvBYSNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              //대표자
              edt_InvBYNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
              //주소
              edt_InvBYADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_InvBYADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              edt_InvBYADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
          end;

          //물품수령증명서 - 공급자
          108:
          begin
            edt_GoodsSESNAME.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
          end;
        end;
      end;
    end;
  finally
    FreeAndNil(Dialog_SearchCustom_frm);
  end;

end;

procedure TUI_APPSPC_frm.sBitBtn5Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    101 : edt_APPCODEDblClick(edt_APPCODE);
    102 : edt_APPCODEDblClick(edt_GoodsBYCODE);
    103 : edt_APPCODEDblClick(edt_TaxSECODE);
    104 : edt_APPCODEDblClick(edt_TaxBYCODE);
    105 : edt_APPCODEDblClick(edt_TaxAGCODE);
    106 : edt_APPCODEDblClick(edt_InvSECODE);
    107 : edt_APPCODEDblClick(edt_InvBYCODE);
    108 : edt_APPCODEDblClick(edt_GoodsSECODE);
  end;
end;

procedure TUI_APPSPC_frm.edt_CPBANKDblClick(Sender: TObject);
begin
  inherited;
  Dialog_BANK_frm := TDialog_BANK_frm.Create(Self);
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      IF Dialog_BANK_frm.openDialog((Sender as TsEdit).Text) = mrOK Then
      begin
        (Sender as TsEdit).Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        case (Sender as TsEdit).tag of
          //문서정보 추심은행
          201:
            begin
              edt_CPBANKNAME.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKNAME1').AsString;
              edt_CPBANKBU.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKBRANCH1').AsString;
              edt_CPACCOUNTNO.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BankAccount').AsString;
            end;

          //문서정보 내국신용장 개설은행
          202:
            begin
              edt_LABANKNAMEP.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKNAME1').AsString;
              edt_LABANKBUP.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKBRANCH1').AsString;
            end;

          //물품수령증명서 개설은행
          203:
          begin
            edt_GoodsLABANKNAME.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKNAME1').AsString;
            edt_GoodsLABANKBU.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKBRANCH1').AsString;
          end;

        end;
      End;
    end;
  finally
     FreeAndNil( Dialog_BANK_frm );
  end;
end;

procedure TUI_APPSPC_frm.sBitBtn3Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    201 : edt_CPBANKDblClick(edt_CPBANK);
    202 : edt_CPBANKDblClick(edt_LABANKBUCODE);
    203 : edt_CPBANKDblClick(edt_GoodsLABANK);
  end;
end;

procedure TUI_APPSPC_frm.edt_CPCURRDblClick(Sender: TObject);
begin
  inherited;
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      //문서번호의 SHOW HINT가 필요한 EDIT
      if (Sender as TsEdit).Tag = 336 then
      begin
        if Dialog_CodeList_frm.openDialog((Sender as TsEdit).HelpKeyword) = mrOK then
        begin
            //선택된 코드 출력
            (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        end;
      end
      else if (Sender as TsEdit).Tag = 337 then
      begin
        if Dialog_CodeList_frm.openDialog((Sender as TsEdit).HelpKeyword) = mrOK then
        begin
            //선택된 코드 출력
            (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        end;
      end
      //힌트출력이 필요가 없는 EDIT
      ELSE
      begin
        IF Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
        begin
          //선택된 코드 출력
            (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        end;
      END;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;

end;

procedure TUI_APPSPC_frm.sBitBtn6Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of

    301 : edt_CPCURRDblClick(edt_CPCURR);
    302 : edt_CPCURRDblClick(edt_CPADDCURR1);
    303 : edt_CPCURRDblClick(edt_CPADDCURR2);
    304 : edt_CPCURRDblClick(edt_CPAMTC);
    305 : edt_CPCURRDblClick(edt_goodsTOTCNTC);
    306 : edt_CPCURRDblClick(edt_goodsISSAMTC);
    307 : edt_CPCURRDblClick(edt_goodsISSEXPAMTC);
    308 : edt_CPCURRDblClick(edt_goodsISSTOTAMTC);
    309 : edt_CPCURRDblClick(edt_GoodsQTYC);
    310 : edt_CPCURRDblClick(edt_GoodsSUPAMTC);
    311 : edt_CPCURRDblClick(edt_GoodsTOTQTYC);
    312 : edt_CPCURRDblClick(edt_GoodsPRICEC);
    313 : edt_CPCURRDblClick(edt_GoodsSUPTOTAMTC);
    314 : edt_CPCURRDblClick(edt_TaxPAIAMTC1);
    315 : edt_CPCURRDblClick(edt_TaxPAIAMTC2);
    316 : edt_CPCURRDblClick(edt_TaxPAIAMTC3);
    317 : edt_CPCURRDblClick(edt_TaxPAIAMTC4);
    318 : edt_CPCURRDblClick(edt_TaxVBGUBUN);
    319 : edt_CPCURRDblClick(edt_TaxTOTCNTC);
    320 : edt_CPCURRDblClick(edt_TaxQTYC);
    321 : edt_CPCURRDblClick(edt_TaxSUPAMTC);
    322 : edt_CPCURRDblClick(edt_TaxVBAMTC);
    323 : edt_CPCURRDblClick(edt_TaxVBTAXC);
    324 : edt_CPCURRDblClick(edt_TaxTOTQTYC);
    325 : edt_CPCURRDblClick(edt_TaxPRICEC);
    326 : edt_CPCURRDblClick(edt_TaxSUPTOTAMTC);
    327 : edt_CPCURRDblClick(edt_VBTOTAMTC);
    328 : edt_CPCURRDblClick(edt_TaxVBTOTTAXC);
    329 : edt_CPCURRDblClick(edt_InvTOTCNTC);
    330 : edt_CPCURRDblClick(edt_InvISSTOTAMTC);
    331 : edt_CPCURRDblClick(edt_InvQTYC);
    332 : edt_CPCURRDblClick(edt_InvSUPAMTC);
    333 : edt_CPCURRDblClick(edt_InvTOTQTYC);
    334 : edt_CPCURRDblClick(edt_InvPRICEC);
    335 : edt_CPCURRDblClick(edt_InvSUPTOTAMTC);
    336 : edt_CPCURRDblClick(edt_BGM2);
    337 : edt_CPCURRDblClick(edt_BGM3);
    338 : edt_CPCURRDblClick(edt_msg1);
    339 : edt_CPCURRDblClick(edt_msg2);
  end;
end;

procedure TUI_APPSPC_frm.Page_controlChange(Sender: TObject);
begin
  inherited;
  if (Sender as TsPageControl).ActivePageIndex = 4 then
  begin
    //관리번호가있는 패널  숨김
    //sPanel4.Visible := False;
    //판매대금추심 왼쪽 DBGrid 숨김
    sPanel56.Visible := False;
  end
  else
  begin
    //관리번호가있는 패널
    //sPanel4.Visible := True;
    //판매대금추심 왼쪽 DBGrid 숨김
    sPanel56.Visible := True;
  end;

  //데이터 신규,수정 작성시 데이터조회 페이지 검색부분 비활성화
  if ProgramControlType in [ctInsert , ctModify] then
    EnabledControlValue(sPanel5)
  else
    EnabledControlValue(sPanel5,False);

  case Page_control.ActivePageIndex of
    //문서정보
    0 : if qryList.State = dsBrowse then ReadDocument;

    //물품수령증명서
    1 :
      begin
        with qryGoods do
        begin
          Close;
          Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
          Open;
        end;

        with qryGoodsItem do
        begin
          Close;
          SQL.Clear;
          SQL.Text := APPSPCGOODSITEM_SQL;
          Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;

          //물품수령증명서가 1건이라도 있을경우
          if qryGoods.RecordCount > 0 then
            Parameters.ParamByName('SEQNO').Value := qryGoodsSEQ.AsInteger
          //물품수령증명서가 1건도 없을경우
          else
            Parameters.ParamByName('SEQNO').Value := 0;

          Open;
          //ShowMessage(sql.Text);
        end;
        
        //데이터가 없을경우 EDIT를 모두  클리어시켜준다(클리어시켜주지 않으면 다른 키값으 데이터가 남아있음)
        if qryGoods.RecordCount = 0 then
        begin
          ClearControlValue(goodsPanel1);
          ClearControlValue(goodsPanel2);
          ClearControlValue(goodsPanel3);
          ClearControlValue(goodsPanel4);
        end;
        if qryGoodsItem.RecordCount = 0 then
        begin
          ClearControlValue(goodsPanel5);
        end;

      end;

    //세금계산서
    2 :
      begin
        with qryTax do
        begin
          Close;
          Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
          Open;
        end;

        with qryTaxItem do
        begin
          Close;
          Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
          //세금계산서가 1건이라도 있을경우
          if qryTax.RecordCount > 0 then
            Parameters.ParamByName('SEQNO').Value := qryGoodsSEQ.AsInteger
          //세금계산서가 1건도 없을경우
          else
            Parameters.ParamByName('SEQNO').Value := 0;
          Open;
        end;

        //세금계산서 공급자부분
        sPageControl1.ActivePageIndex := 0;

        //데이터가 없을경우 EDIT를 모두  클리어시켜준다(클리어시켜주지 않으면 다른 키값으 데이터가 남아있음)
        if qryTax.RecordCount = 0 then
        begin
          ClearControlValue(taxPanel1);
          ClearControlValue(taxPanel2);
          ClearControlValue(sTabSheet7);
          ClearControlValue(sTabSheet8);
          ClearControlValue(sTabSheet9);
        end;

        if qryTaxItem.RecordCount = 0 then ClearControlValue(taxPanel3);

      end;

    //상업송장
    3 :
      begin
        with qryInv do
        begin
          Close;
          Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
          Open;
        end;

        with qryInvItem do
        begin
          Close;
          Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
          //상업송장이 1건이라도 있을경우
          if qryInv.RecordCount > 0 then
            Parameters.ParamByName('SEQNO').Value := qryInvSEQ.AsInteger
          else
            Parameters.ParamByName('SEQNO').Value := 0;
          Open;
        end;

        //데이터가 없을경우 EDIT를 모두  클리어시켜준다(클리어시켜주지 않으면 다른 키값으 데이터가 남아있음)
        if qryInv.RecordCount = 0 then
        begin
          ClearControlValue(invPanel1);
          ClearControlValue(invPanel1);
        end;

        if qryInvItem.RecordCount = 0 then ClearControlValue(invPanel3);

      end;
  end;

end;

procedure TUI_APPSPC_frm.qryGoodsAfterScroll(DataSet: TDataSet);
begin
  inherited;
    if qryList.State = dsBrowse then ReadDocument; //관리번호를 다시불러오기 위함
    if (qryGoods.State = dsBrowse) then GoodsReadDocument;
     
//      if (qryList.State in [dsInsert]) and (qryGoods.State in [dsInsert,dsEdit]) then
//      begin
//        if qryGoods.RecordCount  = 0 then
//        begin
//          qryGoods.FieldByName('SEQ').AsInteger := 1
//        end
//      end;

      qryGoodsItem.Close;
      qryGoodsItem.SQL.Clear;
      qryGoodsItem.SQL.Text := APPSPCGOODSITEM_SQL;
      qryGoodsItem.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;

      //물품수령증명서가 1건이라도 있을경우 해당 SEQ를 대입
      if qryGoods.RecordCount > 0 then
        qryGoodsItem.Parameters.ParamByName('SEQNO').Value := qryGoodsSEQ.AsInteger
      //물품수령증명서가 1건도 없을경우 0을 대입
      else
        qryGoodsItem.Parameters.ParamByName('SEQNO').Value := 0;

        //ShowMessage(qryGoodsItem.SQL.Text);
      qryGoodsItem.Open;

end;

//물품수령증명서 - 품목내역
procedure TUI_APPSPC_frm.qryGoodsItemAfterScroll(DataSet: TDataSet);

begin
  inherited;

  if qryGoodsItem.State = dsBrowse then GoodsItemReadDocument;
  
end;

//세금계산서
procedure TUI_APPSPC_frm.qryTaxAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
  if qryTax.State = dsBrowse then TaxReadDocument;

  qryTaxItem.Close;
  qryTaxItem.SQL.Clear;
  qryTaxItem.SQL.Text := APPSPCTAXITEM_SQL;
  qryTaxItem.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
  //세금계산서가 1건이라도 있을경우 해당 SEQ를 대입
  if qryTax.RecordCount > 0 then
    qryTaxItem.Parameters.ParamByName('SEQNO').Value := qryTaxSEQ.AsInteger
  //세금계산서가 1건도 없을경우 0을대입
  else
    qryTaxItem.Parameters.ParamByName('SEQNO').Value := 0;
  //ShowMessage(qryGoodsItem.SQL.Text);
  qryTaxItem.Open;
end;

//세금계산서 - 품목내역
procedure TUI_APPSPC_frm.qryTaxItemAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryTaxItem.State = dsBrowse then TaxItemReadDocument;
end;

//상업송장
procedure TUI_APPSPC_frm.qryInvAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
  if qryInv.State = dsBrowse then InvReadDocument;

  qryInvItem.Close;
  qryInvItem.SQL.Clear;
  qryInvItem.SQL.Text := APPSPCINVITEM_SQL;
  qryInvItem.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
  if qryInv.RecordCount > 0 then
    qryInvItem.Parameters.ParamByName('SEQNO').Value := qryInvSEQ.AsInteger
  else
    qryInvItem.Parameters.ParamByName('SEQNO').Value := 0;
  qryInvItem.Open;
end;

//상업송장 - 품목내역
procedure TUI_APPSPC_frm.qryInvItemAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryInvItem.State = dsBrowse then InvItemReadDocument;
end;

procedure TUI_APPSPC_frm.mask_DATEEDblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

//if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

 (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TUI_APPSPC_frm.btn_CalClick(Sender: TObject);
begin
  inherited;


  case (Sender as TsBitBtn).Tag of
    901 : mask_DATEEDblClick(mask_DATEE);
    902 : mask_DATEEDblClick(Mask_SearchDate1);
    903 : mask_DATEEDblClick(Mask_SearchDate2);
  end;
end;

procedure TUI_APPSPC_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel6.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn22.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn28.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_APPSPC_frm.sBitBtn8Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_APPSPC_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;
begin
  inherited;
  nIndex := StrToIntDef( qryListCHK2.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TUI_APPSPC_frm.sDBGrid8DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin

    case CHK2Value of
      9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

      6 :
      begin
        if AnsiMatchText( Column.FieldName , ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;

end;

procedure TUI_APPSPC_frm.NewDocument;
begin
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctInsert;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
  GoodsButtonEnable(True);
  TaxButtonEnable(True);
  InvButtonEnable(True);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  Page_control.ActivePageIndex := 0;
  Page_controlChange(Page_control);
  sDBGrid8.Enabled := False;
  sDBGrid3.Enabled := False;
  sDBGrid4.Enabled := False;
  sDBGrid5.Enabled := False;
  sDBGrid2.Enabled := False;
  sDBGrid6.Enabled := False;
  sDBGrid7.Enabled := False;
  sDBGrid1.Enabled := False;
//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Append;
//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
    EnabledControlValue(sPanel4 , False);
  //----------------------------------------------------------------------------
  //문서정보
  //----------------------------------------------------------------------------
    ClearControlValue(basicPanel1);
    ClearControlValue(basicPanel2);
    ClearControlValue(basicPanel3);
    ClearControlValue(basicPanel4);
    ClearControlValue(basicPanel5);

    EnabledControlValue(basicPanel1 , False);
    EnabledControlValue(basicPanel2 , False);
    EnabledControlValue(basicPanel3 , False);
    EnabledControlValue(basicPanel4 , False);
    EnabledControlValue(basicPanel5 , False);

  //----------------------------------------------------------------------------
  //물품수령증명서
  //----------------------------------------------------------------------------
    ClearControlValue(goodsPanel1);
    ClearControlValue(goodsPanel2);
    ClearControlValue(goodsPanel3);
    ClearControlValue(goodsPanel4);
    ClearControlValue(goodsPanel5);

  //----------------------------------------------------------------------------
  //세금계산서
  //----------------------------------------------------------------------------
    ClearControlValue(taxPanel1);
    ClearControlValue(taxPanel2);
    ClearControlValue(taxPanel3);
    ClearControlValue(sTabSheet7);
    ClearControlValue(sTabSheet8);
    ClearControlValue(sTabSheet9);

  //----------------------------------------------------------------------------
  //상업송장
  //----------------------------------------------------------------------------
    ClearControlValue(invPanel1);
    ClearControlValue(invPanel2);
    ClearControlValue(invPanel3);

//------------------------------------------------------------------------------
//기초자료
//------------------------------------------------------------------------------
  //관리번호 문서번호(BGM4)
  edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPSPC');
  //신청일자
  mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
  //사용자
  edt_USER_ID.Text := LoginData.sID;
  //문서기능
  edt_msg1.Text := '9';
  //유형
  edt_msg2.Text := 'AB';
  //문서번호 자동생성
  edt_BGM4.Text := edt_MAINT_NO.Text;

  //추심금액(원화)통화
  edt_RCT_AMT2C.Text := 'KRW';
  //------------------------------------------------------------------------------
  //신청인 정보
  //------------------------------------------------------------------------------
  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try
    with Dlg_Customer_frm do
    begin
      if isValue('00000') then
        edt_APPCODE.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
        edt_APPSNAME.Text := DataSource1.DataSet.FieldByName('ENAME').AsString;
        edt_APPNAME.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
        edt_APPSAUPNO.Text := DataSource1.DataSet.FieldByName('SAUP_NO').AsString;
        edt_APPADDR1.Text := DataSource1.DataSet.FieldByName('ADDR1').AsString;
        edt_APPADDR2.Text := DataSource1.DataSet.FieldByName('ADDR2').AsString;
        edt_APPADDR3.Text := DataSource1.DataSet.FieldByName('ADDR3').AsString;
        edt_APPELEC.Text := DataSource1.DataSet.FieldByName('JENJA').AsString;
        //문서번호 BGM1 = 수발신인식별자(최대 12자리)
        edt_BGM1.Text := DataSource1.DataSet.FieldByName('ESU1').AsString;
    end;
  finally
    FreeAndNil(Dlg_Customer_frm);
  end;

end;

procedure TUI_APPSPC_frm.ButtonEnable(Val: Boolean);
begin
  btnNew.Enabled := Val;
  btnEdit.Enabled := Val;
  btnDel.Enabled := Val;

  btnTemp.Enabled := not Val;
  btnSave.Enabled := not Val;
  btnCancel.Enabled := not Val;

  btnCopy.Enabled := Val;
  btnPrint.Enabled := Val;
end;

procedure TUI_APPSPC_frm.GoodsButtonEnable(Val: Boolean);
begin
  btn_goodsNew.Enabled := Val;
  btn_goodsMod.Enabled := Val;
  btn_goodsDel.Enabled := Val;
  btn_goodsSave.Enabled := not Val;
  btn_goodsCancel.Enabled := not Val;
end;

procedure TUI_APPSPC_frm.GoodsItemButtonEnable(Val: Boolean);
begin
  btn_goodsItemNew.Enabled := Val;
  btn_goodsItemMod.Enabled := Val;
  btn_goodsItemDel.Enabled := Val;
  btn_goodsItemSave.Enabled := not Val;
  btn_goodsItemCancel.Enabled := not Val;
end;

procedure TUI_APPSPC_frm.TaxButtonEnable(Val: Boolean);
begin
  btn_taxNew.Enabled := Val;
  btn_taxMod.Enabled := Val;
  btn_taxDel.Enabled := Val;
  btn_taxSave.Enabled := not Val;
  btn_taxCancel.Enabled := not Val;
end;

procedure TUI_APPSPC_frm.TaxItemButtonEnable(Val: Boolean);
begin
  btn_taxItemNew.Enabled := Val;
  btn_taxItemMod.Enabled := Val;
  btn_taxItemDel.Enabled := Val;
  btn_taxItemSave.Enabled := not Val;
  btn_taxItemCancel.Enabled := not Val;
end;

procedure TUI_APPSPC_frm.InvButtonEnable(Val: Boolean);
begin
  btn_invNew.Enabled := Val;
  btn_invMod.Enabled := Val;
  btn_invDel.Enabled := Val;
  btn_invSave.Enabled := not Val;
  btn_invCancel.Enabled := not Val;
end;

procedure TUI_APPSPC_frm.InvItemButtonEnable(Val: Boolean);
begin
  btn_invItemNew.Enabled := Val;
  btn_invItemMod.Enabled := Val;
  btn_invItemDel.Enabled := Val;
  btn_invItemSave.Enabled := not Val;
  btn_invItemCancel.Enabled := not Val;
end;

procedure TUI_APPSPC_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  NewDocument;
end;

procedure TUI_APPSPC_frm.CancelDocument;
begin
 inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(True);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  Page_control.ActivePageIndex := 0;
  Page_controlChange(Page_control);
  sDBGrid8.Enabled := True;
  sDBGrid3.Enabled := True;
  sDBGrid4.Enabled := True;
  sDBGrid5.Enabled := True;
  sDBGrid2.Enabled := True;
  sDBGrid6.Enabled := True;
  sDBGrid7.Enabled := True;
  sDBGrid1.Enabled := True;

//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4);

  EnabledControlValue(basicPanel1);
  EnabledControlValue(basicPanel2);
  EnabledControlValue(basicPanel3);
  EnabledControlValue(basicPanel4);
  EnabledControlValue(basicPanel5);

  ClearControlValue(basicPanel1);
  ClearControlValue(basicPanel2);
  ClearControlValue(basicPanel3);
  ClearControlValue(basicPanel4);
  ClearControlValue(basicPanel5);

  //물품수령증명서
  EnabledControlValue(goodsBtnPanel);
  EnabledControlValue(goodsItemBtnPanel);
  EnabledControlValue(goodsPanel1);
  EnabledControlValue(goodsPanel2);
  EnabledControlValue(goodsPanel3);
  EnabledControlValue(goodsPanel4);
  EnabledControlValue(goodsPanel5);

  ClearControlValue(goodsBtnPanel);
  ClearControlValue(goodsItemBtnPanel);
  ClearControlValue(goodsPanel1);
  ClearControlValue(goodsPanel2);
  ClearControlValue(goodsPanel3);
  ClearControlValue(goodsPanel4);
  ClearControlValue(goodsPanel5);

  //세금계산서
  EnabledControlValue(taxBtnPanel);
  EnabledControlValue(taxItemBtnPanel);
  EnabledControlValue(taxPanel1);
  EnabledControlValue(taxPanel2);
  EnabledControlValue(taxPanel3);
  EnabledControlValue(sTabSheet7);
  EnabledControlValue(sTabSheet8);
  EnabledControlValue(sTabSheet9);

  ClearControlValue(taxBtnPanel);
  ClearControlValue(taxItemBtnPanel);
  ClearControlValue(taxPanel1);
  ClearControlValue(taxPanel2);
  ClearControlValue(taxPanel3);
  ClearControlValue(sTabSheet7);
  ClearControlValue(sTabSheet8);
  ClearControlValue(sTabSheet9);

  //상업송장
  EnabledControlValue(invBtnPanel);
  EnabledControlValue(invItemBtnPanel);
  EnabledControlValue(invPanel1);
  EnabledControlValue(invPanel2);
  EnabledControlValue(invPanel3);

  ClearControlValue(invBtnPanel);
  ClearControlValue(invItemBtnPanel);
  ClearControlValue(invPanel1);
  ClearControlValue(invPanel2);
  ClearControlValue(invPanel3);

  ClearControlValue(GoodsimdcodePanel);
  ClearControlValue(GoodsSIZE_Panel);

  ClearControlValue(TaxIMDCODEPanel);
  ClearControlValue(TaxSIZEPanel);
  ClearControlValue(TaxBIGOPanel);

  ClearControlValue(InvIMCODEPanel);
  ClearControlValue(InvSIZEPanel);

  //트랜잭션 롤백
  if DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.RollbackTrans;
end;

procedure TUI_APPSPC_frm.btnCancelClick(Sender: TObject);
var
  CreateDate : TDateTime;
begin
  inherited;

  IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_CANCEL),'취소확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
  begin
    CancelDocument;

    //새로고침
    CreateDate := ConvertStr2Date(mask_DATEE.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             edt_MAINT_NO.Text);
  end;

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(basicPanel1);
    ClearControlValue(basicPanel2);
    ClearControlValue(basicPanel3);
    ClearControlValue(basicPanel4);
    ClearControlValue(basicPanel5);
  end;
end;

procedure TUI_APPSPC_frm.btn_goodsNewClick(Sender: TObject);
var
  maint_no : String;
  nCursor : integer;
begin
  inherited;
  case (Sender as TsSpeedButton).Tag of
    //물품수령증명서 입력
    1 :
      begin
        sDBGrid3.Enabled := False;
        //품목내역 버튼 활성화
        GoodsItemButtonEnable(True);

        //물품수령증명서가 신규작성되고있는 관리번호로 셀렉트한다.
        qryGoods.Close;
        qryGoods.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
        qryGoods.Open;

        //처음 1건 작성시 APPEND실행 -> AfterScroll -> 다음라인
        //그 이후 작성시 APPEND실행되기전 -> AfterScroll -> APPEND실행 -> 다음라인 }
        qryGoods.Append;
        qryGoods.FieldByName('SEQ').AsInteger := MAX_SEQ('APPSPC_D1','2AH');

        //품목내역 새로고침(해당 물품수령증명서 순번에 따른 품목내역 조회를 위하여 사용)
        qryGoodsItem.Close;
        qryGoodsItem.SQL.Clear;
        qryGoodsItem.SQL.Text := APPSPCGOODSITEM_SQL;
        qryGoodsItem.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
        qryGoodsItem.Parameters.ParamByName('SEQNO').Value := qryGoods.FieldByName('SEQ').AsInteger;
        qryGoodsItem.Open;

        //물품수령증명서를 취소하면 롤백을 하기위해사용
        qryCheckCancel.Close;
        qryCheckCancel.Parameters.ParamByName('KEYY').Value := edt_MAINT_NO.Text;
        qryCheckCancel.Parameters.ParamByName('DocGubun').Value := '2AH';
        qryCheckCancel.Parameters.ParamByName('SEQ').Value := qryGoods.FieldByName('SEQ').AsInteger;
        qryCheckCancel.Open;

        //ShowMessage(IntToStr(qryCheckCancel.RecordCount));

        ClearControlValue(goodsPanel1);
        ClearControlValue(goodsPanel2);
        ClearControlValue(goodsPanel3);
        ClearControlValue(goodsPanel4);
        ClearControlValue(goodsPanel5);
        ClearControlValue(GoodsSIZE_Panel);
        ClearControlValue(GoodsimdcodePanel);
      end;

    //물품수령증명서 수정
    2 :
      begin
        if qryGoods.RecordCount > 0 then
        begin
          sDBGrid3.Enabled := False;
          //품목내역 버튼 활성화
          GoodsItemButtonEnable(True);
          qryGoods.Edit;

          //물품수령증명서를 취소하면 롤백을 하기위해사용
          qryCheckCancel.Close;
          qryCheckCancel.Parameters.ParamByName('KEYY').Value := edt_MAINT_NO.Text;
          qryCheckCancel.Parameters.ParamByName('DocGubun').Value := '2AH';
          qryCheckCancel.Parameters.ParamByName('SEQ').Value := qryGoods.FieldByName('SEQ').AsInteger;
          qryCheckCancel.Open;
        end;
      end;
      
    //물품수령증명서 삭제
    3 :
      begin
        if qryGoods.RecordCount > 0 then
        begin
          with TADOQuery.Create(nil) do
          begin
            Connection := DMMssql.KISConnect;
            maint_no := edt_MAINT_NO.Text;
            try
               try
                IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'물품수령증명서 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
                 begin
                   nCursor := qryGoods.RecNo;

                   SQL.Text :=  'DELETE FROM APPSPC_D1  WHERE KEYY =' + QuotedStr(maint_no) + 'and DOC_GUBUN = '+ QuotedStr('2AH') +
                                ' and SEQ ='+IntToStr(qryGoodsSEQ.AsInteger);
                   ExecSQL;

                   SQL.Text :=  'DELETE FROM APPSPC_D2  WHERE KEYY =' + QuotedStr(maint_no) + 'and DOC_GUBUN = '+ QuotedStr('2AH') +
                                ' and SEQ ='+IntToStr(qryGoodsSEQ.AsInteger);
                   ExecSQL;

                   qryGoods.Close;
                   qryGoods.Open;

                   qryGoodsItem.Close;
                   qryGoodsItem.Open;

                   if qryGoods.RecordCount = 0 then
                   begin
                     ClearControlValue(goodsPanel1);
                     ClearControlValue(goodsPanel2);
                     ClearControlValue(goodsPanel3);
                     ClearControlValue(goodsPanel4);
                     ClearControlValue(goodsPanel5);
                   end;

                   if (qryGoods.RecordCount > 0 ) AND (qryGoods.RecordCount >= nCursor) Then
                   begin
                     qryGoods.MoveBy(nCursor-1);
                   end
                   else
                     qryGoods.First;
                 end;

               except
                 on E:Exception do
                 begin
                   DMMssql.KISConnect.RollbackTrans;
                   MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
                 end;
               end;
            finally
             Close;
             Free;
            end;
          end;
        end
      end;

    //물품수령증명서 저장
    4 :
      begin
        //품목내역이 작성중일경우 물품수령증명서 저장 불가
        if edt_GoodsLINENO.Enabled = True then //일련번호가 활성화되어있으면 품목내역 작성중인것
        begin
         if MessageBox(Self.Handle,'품목내역을 작성중입니다.','저장확인',MB_OK+MB_ICONINFORMATION) = ID_OK then Exit;
        end;

        GoodsSaveDocument;
        qryGoods.Post;

        sDBGrid3.Enabled := True;

        btn_goodsItemNew.Enabled := False;
        btn_goodsItemMod.Enabled := False;
        btn_goodsItemDel.Enabled := False;
        btn_goodsItemSave.Enabled := False;
        btn_goodsItemCancel.Enabled := False;
      end;
      
    //물품수령증명서 취소
    5 :
      begin
        if qryGoods.RecordCount = 0 then
        begin
          ClearControlValue(goodsPanel1);
          ClearControlValue(goodsPanel2);
          ClearControlValue(goodsPanel3);
          ClearControlValue(goodsPanel4);
          ClearControlValue(GoodsSIZE_Panel);
          ClearControlValue(GoodsimdcodePanel);
        end;

      //------------------------------------------------------------------------
      //물품수령증명서 품목내역관련
      //------------------------------------------------------------------------
        { 1.물품수령증명서를 취소했기때문에 수정된 품목내역을 삭제하고 기존의 품목내역으로 다시 변경한다.(수정중 취소하는경우)
          2.신규작성중 취소작업을 하는경우
          3.물품수령 증명서를 복사해왔을경우 물품수령증명서 품목내역에
           데이터가 저장되기때문에 작성시 종료하면 품목내역의 데이터도 삭제한다 }
        if qryGoods.State in [dsEdit,dsInsert] then
        begin
          with TADOQuery.Create(nil) do
          begin
            Connection := DMMssql.KISConnect;
            maint_no := edt_MAINT_NO.Text;
            try
              SQL.Text :=  'DELETE FROM APPSPC_D2  WHERE KEYY =' + QuotedStr(maint_no) + ' and DOC_GUBUN = '+ QuotedStr('2AH') +
                           ' and SEQ ='+IntToStr(qryGoodsSEQ.AsInteger);
              //ShowMessage(SQL.Text);
              ExecSQL;
              qryGoodsItem.Close;
              qryGoodsItem.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
              qryGoodsItem.Parameters.ParamByName('SEQNO').Value := qryGoodsSEQ.AsInteger;
              qryGoodsItem.Open;
            finally
             Close;
             Free;
            end;
          end;

          if qryCheckCancel.RecordCount > 0 then
          begin
            //기존에 있던 데이터를 다시 저장
            qryGoodsItem.First;
            qryCheckCancel.First;
            while not qryCheckCancel.Eof do
            begin
              with qryGoodsItem do
              begin
                //추가시작
                Append;

                //KEYY
                FieldByName('KEYY').AsString := qryCheckCancelKEYY.AsString;
                //순번
                FieldByName('SEQ').AsInteger := qryCheckCancelSEQ.AsInteger;
                //문서구분
                FieldByName('DOC_GUBUN').AsString := qryCheckCancelDOC_GUBUN.AsString;
              //------------------------------------------------------------------
              // 품목내역
              //------------------------------------------------------------------
                //일련번호
                FieldByName('LINE_NO').AsString := qryCheckCancelLINE_NO.AsString;
                //HS부호
                FieldByName('HS_NO').AsString := qryCheckCancelHS_NO.AsString;
                //품목
                FieldByName('IMD_CODE1').AsString := qryCheckCancelIMD_CODE1.AsString;
                FieldByName('IMD_CODE2').AsString := qryCheckCancelIMD_CODE2.AsString;
                FieldByName('IMD_CODE3').AsString := qryCheckCancelIMD_CODE3.AsString;
                FieldByName('IMD_CODE4').AsString := qryCheckCancelIMD_CODE4.AsString;
                //규격
                FieldByName('SIZE1').AsString := qryCheckCancelSIZE1.AsString;
                FieldByName('SIZE2').AsString := qryCheckCancelSIZE2.AsString;
                FieldByName('SIZE3').AsString := qryCheckCancelSIZE3.AsString;
                FieldByName('SIZE4').AsString := qryCheckCancelSIZE4.AsString;
                FieldByName('SIZE5').AsString := qryCheckCancelSIZE5.AsString;
                FieldByName('SIZE6').AsString := qryCheckCancelSIZE6.AsString;
                FieldByName('SIZE7').AsString := qryCheckCancelSIZE7.AsString;
                FieldByName('SIZE8').AsString := qryCheckCancelSIZE8.AsString;
                FieldByName('SIZE9').AsString := qryCheckCancelSIZE9.AsString;
                FieldByName('SIZE10').AsString := qryCheckCancelSIZE10.AsString;
                //수량 단위
                FieldByName('QTYC').AsString := qryCheckCancelQTYC.AsString;
                //수량
                FieldByName('QTY').AsCurrency := qryCheckCancelQTY.AsCurrency;
                //수량소계 단위
                FieldByName('TOTQTYC').AsString := qryCheckCancelTOTQTYC.AsString;
                //수량소계
                FieldByName('TOTQTY').AsCurrency := qryCheckCancelTOTQTY.AsCurrency;
                //단가
                FieldByName('PRICE').AsCurrency := qryCheckCancelPRICE.AsCurrency;
                //기준수량 단위
                FieldByName('PRICEC').AsString := qryCheckCancelPRICEC.AsString;
                //기준수량
                FieldByName('PRICE_G').AsCurrency := qryCheckCancelPRICE_G.AsCurrency;
                //금액 단위
                FieldByName('SUP_AMTC').AsString := qryCheckCancelSUP_AMTC.AsString;
                //금액
                FieldByName('SUP_AMT').AsCurrency := qryCheckCancelSUP_AMT.AsCurrency;
                //금액소계 단위
                FieldByName('SUP_TOTAMTC').AsString := qryCheckCancelSUP_TOTAMTC.AsString;
                //금액
                FieldByName('SUP_TOTAMT').AsCurrency := qryCheckCancelSUP_TOTAMT.AsCurrency;

                //필드저장
                Post;
              end;
              qryCheckCancel.Next;
            end;
          end;
        end;
        //품목 내역의 모든 데이터를 삭제하면 EDIT를 클리어시킨다.
        if qryGoodsItem.RecordCount = 0 then
          ClearControlValue(goodsPanel5)
        else
          GoodsItemReadDocument;

        btn_goodsItemNew.Enabled := False;
        btn_goodsItemMod.Enabled := False;
        btn_goodsItemDel.Enabled := False;
        btn_goodsItemSave.Enabled := False;
        btn_goodsItemCancel.Enabled := False;
        EnabledControlValue(goodsPanel5 , True);

        sDBGrid3.Enabled := True;
        qryGoods.Cancel;
      end; // 취소이벤트 END
  end;
end;

procedure TUI_APPSPC_frm.qryGoodsAfterInsert(DataSet: TDataSet);
begin
  inherited;
  GoodsButtonEnable(False);
  EnabledControlValue(goodsPanel1 , False);
  EnabledControlValue(goodsPanel2 , False);
  EnabledControlValue(goodsPanel3 , False);
  EnabledControlValue(goodsPanel4 , False);
end;

procedure TUI_APPSPC_frm.qryGoodsAfterCancel(DataSet: TDataSet);
begin
  inherited;
  GoodsButtonEnable(True);
  EnabledControlValue(goodsPanel1);
  EnabledControlValue(goodsPanel2);
  EnabledControlValue(goodsPanel3);
  EnabledControlValue(goodsPanel4);
end;

procedure TUI_APPSPC_frm.btn_goodsItemNewClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsSpeedButton).Tag of
    //물품수령증명서 품목입력
    1 :
      begin
        sDBGrid4.Enabled := False;
        ClearControlValue(goodsPanel5);
        ClearControlValue(GoodsimdcodePanel);
        ClearControlValue(GoodsSIZE_Panel);
        qryGoodsItem.Append;

        if qryGoodsItem.RecordCount  = 0 then
        begin
          edt_GoodsLINENO.Text := '0001';
          qryGoodsItem.FieldByName('SEQ').AsInteger := MAX_SEQ('APPSPC_D2','2AH');
        end
        else
        begin
          edt_GoodsLINENO.Text := Format('%.4d',[MAX_LINENO('APPSPC_D2','2AH',qryGoods)]);
        end;
      end;

    //물품수령증명서 품목수정
    2 :
      begin
        if qryGoodsItem.RecordCount > 0 then
        begin
          sDBGrid4.Enabled := False;
          qryGoodsItem.Edit;
        end;
      end;

    //물품수령증명서 품목삭제
    3 :
      begin
        if qryGoodsItem.RecordCount > 0 then
        begin
          IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'품목내역 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
            qryGoodsItem.Delete;
        end;
      end;

    //물품수령증명서 품목저장
    4 :
      begin
        GoodsItemSaveDocument;
        qryGoodsItem.Post;
        sDBGrid4.Enabled := True;
        //버튼, 품목내역 패널
        GoodsItemButtonEnable(True);
        EnabledControlValue(goodsPanel5 , True);
      end;

    //물품수령증명서 품목취소
    5 :
      begin
        qryGoodsItem.Cancel;
        sDBGrid4.Enabled := True;
        GoodsItemButtonEnable(True);
        EnabledControlValue(goodsPanel5 , True);
      end;
  end;
end;

procedure TUI_APPSPC_frm.qryGoodsItemAfterInsert(DataSet: TDataSet);
begin
  inherited;
  GoodsItemButtonEnable(False);
  EnabledControlValue(goodsPanel5 , False);
end;

procedure TUI_APPSPC_frm.btn_taxNewClick(Sender: TObject);
var
  maint_no : String;
  nCursor : integer;
begin
  inherited;
  case (Sender as TsSpeedButton).Tag of
    //세금계산서 입력
    1 :
      begin
        sDBGrid5.Enabled := False;
        //품목내역 버튼 활성화
        TaxItemButtonEnable(True);

        //세금계산서를 신규작성하고있는 관리번호를 셀렉트한다.
        qryTax.Close;
        qryTax.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
        qryTax.Open;

        qryTax.Append;
        qryTax.FieldByName('SEQ').AsInteger := MAX_SEQ('APPSPC_D1','2AJ');

        //ShowMessage(qryTax.FieldByName('SEQ').AsString);

        //품목내역 새로고침(해당 세금계산서 순번에 따른 품목내역 조회를 위해 사용)
        qryTaxItem.Close;
        qryTaxItem.SQL.Clear;
        qryTaxItem.SQL.Text := APPSPCTAXITEM_SQL;
        qryTaxItem.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
        qryTaxItem.Parameters.ParamByName('SEQNO').Value := qryTax.FieldByName('SEQ').AsInteger;
        qryTaxItem.Open;

        //세금계산서를 취소하면 롤백하기 위해 사용(변경전 데이터 셀렉트)
        qryCheckCancel.Close;
        qryCheckCancel.Parameters.ParamByName('KEYY').Value := edt_MAINT_NO.Text;
        qryCheckCancel.Parameters.ParamByName('DocGubun').Value := '2AJ';
        qryCheckCancel.Parameters.ParamByName('SEQ').Value := qryTax.FieldByName('SEQ').AsString;
        qryCheckCancel.Open;

        ClearControlValue(taxPanel1);
        ClearControlValue(taxPanel2);
        ClearControlValue(sTabSheet7);
        ClearControlValue(sTabSheet8);
        ClearControlValue(sTabSheet9);
        ClearControlValue(taxPanel3);;
        ClearControlValue(TaxIMDCODEPanel);
        ClearControlValue(TaxSIZEPanel);
      end;

    //세금계산서 수정
    2 :
      begin
        if qryTax.RecordCount > 0 then
        begin
          sDBGrid5.Enabled := False;
          TaxItemButtonEnable(True);
          qryTax.Edit;

          //세금계산서를 취소하면 롤백하기 위해 사용(변경전 데이터 셀렉트)
          qryCheckCancel.Close;
          qryCheckCancel.Parameters.ParamByName('KEYY').Value := edt_MAINT_NO.Text;
          qryCheckCancel.Parameters.ParamByName('DocGubun').Value := '2AJ';
          qryCheckCancel.Parameters.ParamByName('SEQ').Value := qryTax.FieldByName('SEQ').AsString;
          qryCheckCancel.Open;
        end;
      end;

    //세금계산서 삭제
    3 :
      begin
        if qryTax.RecordCount > 0 then
        begin
          with TADOQuery.Create(nil) do
          begin
            Connection := DMMssql.KISConnect;
            maint_no := edt_MAINT_NO.Text;
            try
               try
                IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'세금계산서 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
                 begin
                   nCursor := qryTax.RecNo;

                   SQL.Text :=  'DELETE FROM APPSPC_D1  WHERE KEYY =' + QuotedStr(maint_no) + 'and DOC_GUBUN = '+ QuotedStr('2AJ') +
                                ' and SEQ ='+IntToStr(qryTaxSEQ.AsInteger);
                   ExecSQL;

                   SQL.Text :=  'DELETE FROM APPSPC_D2  WHERE KEYY =' + QuotedStr(maint_no) + 'and DOC_GUBUN = '+ QuotedStr('2AJ') +
                                ' and SEQ ='+IntToStr(qryTaxSEQ.AsInteger);
                   ExecSQL;

                   qryTax.Close;
                   qryTax.Open;

                   qryTaxItem.Close;
                   qryTaxItem.Open;

                   if qryTax.RecordCount = 0 then
                   begin
                     ClearControlValue(taxPanel1);
                     ClearControlValue(taxPanel2);
                     ClearControlValue(sTabSheet7);
                     ClearControlValue(sTabSheet8);
                     ClearControlValue(sTabSheet9);
                     ClearControlValue(taxPanel3);;
                     ClearControlValue(TaxIMDCODEPanel);
                     ClearControlValue(TaxSIZEPanel);
                   end;

                   if (qryTax.RecordCount > 0 ) AND (qryTax.RecordCount >= nCursor) Then
                   begin
                     qryTax.MoveBy(nCursor-1);
                   end
                   else
                     qryTax.First;
                 end;

               except
                 on E:Exception do
                 begin
                   DMMssql.KISConnect.RollbackTrans;
                   MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
                 end;
               end;
            finally
             Close;
             Free;
            end;
          end;
        end;;
      end;

    //세금계산서 저장
    4 :
      begin
        //품목내역이 작성중일경우 세금계산서 저장 불가
        if qryTaxItem.State in [dsEdit , dsInsert] then
        begin
         if MessageBox(Self.Handle,'품목내역을 작성중입니다.','저장확인',MB_OK+MB_ICONINFORMATION) = ID_OK then Exit;
        end;

        TaxSaveDocument;
        qryTax.Post;

        sDBGrid5.Enabled := True;

        btn_taxItemNew.Enabled := False;
        btn_taxItemMod.Enabled := False;
        btn_taxItemDel.Enabled := False;
        btn_taxItemSave.Enabled := False;
        btn_taxItemCancel.Enabled := False;
      end;

    //세금계산서 취소
    5 :
      begin
        if qryTax.RecordCount = 0 then
        begin
          ClearControlValue(taxPanel1);
          ClearControlValue(taxPanel2);
          ClearControlValue(sTabSheet7);
          ClearControlValue(sTabSheet8);
          ClearControlValue(sTabSheet9);
          ClearControlValue(taxPanel3);;
          ClearControlValue(TaxIMDCODEPanel);
          ClearControlValue(TaxSIZEPanel);
        end;

      //------------------------------------------------------------------------
      //세금계산서 품목내역관련
      //------------------------------------------------------------------------
        { 1.세금계산서를 취소했기때문에 수정된 품목내역을 삭제하고 기존의 품목내역으로 다시 변경한다.(수정중 취소하는경우)
          2.신규작성중 취소작업을 하는경우
          3.세금계산서를 복사해왔을경우 세금계산서 품목내역에
           데이터가 저장되기때문에 작성시 종료하면 품목내역의 데이터도 삭제한다 }
        if qryTax.State in [dsEdit,dsInsert] then
        begin
          with TADOQuery.Create(nil) do
          begin
            Connection := DMMssql.KISConnect;
            maint_no := edt_MAINT_NO.Text;
            try
              SQL.Text :=  'DELETE FROM APPSPC_D2  WHERE KEYY =' + QuotedStr(maint_no) + ' and DOC_GUBUN = '+ QuotedStr('2AJ') +
                           ' and SEQ ='+IntToStr(qryTaxSEQ.AsInteger);
              //ShowMessage(SQL.Text);
              ExecSQL;
              qryTaxItem.Close;
              qryTaxItem.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
              qryTaxItem.Parameters.ParamByName('SEQNO').Value := qryTaxSEQ.AsInteger;
              qryTaxItem.Open;
            finally
             Close;
             Free;
            end;
          end;

          if qryCheckCancel.RecordCount > 0 then
          begin
            //기존에 있던 데이터를 다시 저장
            qryTaxItem.First;
            qryCheckCancel.First;
            while not qryCheckCancel.Eof do
            begin
              with qryTaxItem do
              begin
                //추가시작
                Append;

                //KEYY
                FieldByName('KEYY').AsString := qryCheckCancelKEYY.AsString;
                //순번
                FieldByName('SEQ').AsInteger := qryCheckCancelSEQ.AsInteger;
                //문서구분
                FieldByName('DOC_GUBUN').AsString := qryCheckCancelDOC_GUBUN.AsString;
                //일련번호
                FieldByName('LINE_NO').AsString := qryCheckCancelLINE_NO.AsString;
                //공급일자
                FieldByName('DE_DATE').AsString := qryCheckCancelDE_DATE.AsString;
                //품목
                FieldByName('IMD_CODE1').AsString := qryCheckCancelIMD_CODE1.AsString;
                FieldByName('IMD_CODE2').AsString := qryCheckCancelIMD_CODE2.AsString;
                FieldByName('IMD_CODE3').AsString := qryCheckCancelIMD_CODE3.AsString;
                FieldByName('IMD_CODE4').AsString := qryCheckCancelIMD_CODE4.AsString;
                //규격
                FieldByName('SIZE1').AsString := qryCheckCancelSIZE1.AsString;
                FieldByName('SIZE2').AsString := qryCheckCancelSIZE2.AsString;
                FieldByName('SIZE3').AsString := qryCheckCancelSIZE3.AsString;
                FieldByName('SIZE4').AsString := qryCheckCancelSIZE4.AsString;
                FieldByName('SIZE5').AsString := qryCheckCancelSIZE5.AsString;
                FieldByName('SIZE6').AsString := qryCheckCancelSIZE6.AsString;
                FieldByName('SIZE7').AsString := qryCheckCancelSIZE7.AsString;
                FieldByName('SIZE8').AsString := qryCheckCancelSIZE8.AsString;
                FieldByName('SIZE9').AsString := qryCheckCancelSIZE9.AsString;
                FieldByName('SIZE10').AsString := qryCheckCancelSIZE10.AsString;
                //수량단위
                FieldByName('QTYC').AsString := qryCheckCancelQTYC.AsString;
                //수량
                FieldByName('QTY').AsCurrency := qryCheckCancelQTY.AsCurrency;
                //수량소계단위
                FieldByName('TOTQTYC').AsString := qryCheckCancelTOTQTYC.AsString;
                //수량소계
                FieldByName('TOTQTY').AsCurrency := qryCheckCancelTOTQTY.AsCurrency;
                //환율
                FieldByName('CUX_RATE').AsCurrency := qryCheckCancelCUX_RATE.AsCurrency;
                //단가
                FieldByName('PRICE').AsCurrency := qryCheckCancelPRICE.AsCurrency;
                //기준수량단위
                FieldByName('PRICEC').AsString := qryCheckCancelPRICEC.AsString;
                //기준수량
                FieldByName('PRICE_G').AsCurrency := qryCheckCancelPRICE_G.AsCurrency;
                //공급가액단위
                FieldByName('SUP_AMTC').AsString := qryCheckCancelSUP_AMTC.AsString;
                //공급가액
                FieldByName('SUP_AMT').AsCurrency := qryCheckCancelSUP_AMT.AsCurrency;
                //공급가액소계단위
                FieldByName('SUP_TOTAMTC').AsString := qryCheckCancelSUP_TOTAMTC.AsString;
                //공급가액소계
                FieldByName('SUP_TOTAMT').AsCurrency := qryCheckCancelSUP_TOTAMT.AsCurrency;
                //공급가액외화단위
                FieldByName('VB_AMTC').AsString := qryCheckCancelVB_AMTC.AsString;
                //공급가액외화
                FieldByName('VB_AMT').AsCurrency := qryCheckCancelVB_AMT.AsCurrency;
                //공급가액외화소계단위
                FieldByName('VB_TOTAMTC').AsString := qryCheckCancelVB_TOTAMTC.AsString;
                //공급가액외화소계
                 FieldByName('VB_TOTAMT').AsCurrency := qryCheckCancelVB_TOTAMT.AsCurrency;
                //세액단위
                FieldByName('VB_TAXC').AsString := qryCheckCancelVB_TAXC.AsString;
                //세액
                FieldByName('VB_TAX').AsCurrency := qryCheckCancelVB_TAX.AsCurrency;
                //세액소계단위
                FieldByName('VB_TOTTAXC').AsString := qryCheckCancelVB_TOTTAXC.AsString;
                //세액소계
                FieldByName('VB_TOTTAX').AsCurrency := qryCheckCancelVB_TOTTAX.AsCurrency;
                //참조사항
                FieldByName('BIGO1').AsString := qryCheckCancelBIGO1.AsString;
                FieldByName('BIGO2').AsString := qryCheckCancelBIGO2.AsString;
                FieldByName('BIGO3').AsString := qryCheckCancelBIGO3.AsString;
                FieldByName('BIGO4').AsString := qryCheckCancelBIGO4.AsString;
                FieldByName('BIGO5').AsString := qryCheckCancelBIGO5.AsString;
                //필드저장
                Post;
              end;
              qryCheckCancel.Next;
            end;
          end;
        end;

        //품목 내역의 모든 데이터를 삭제하면 EDIT를 클리어시킨다.
        if qryTaxItem.RecordCount = 0 then
          ClearControlValue(taxPanel3)
        else
          TaxItemReadDocument;

        btn_taxItemNew.Enabled := False;
        btn_taxItemMod.Enabled := False;
        btn_taxItemDel.Enabled := False;
        btn_taxItemSave.Enabled := False;
        btn_taxItemCancel.Enabled := False;
        EnabledControlValue(taxPanel3,True);

        sDBGrid5.Enabled := True;
        qryTax.Cancel;
      end;
  end;
end;

procedure TUI_APPSPC_frm.qryTaxAfterInsert(DataSet: TDataSet);
begin
  inherited;
  TaxButtonEnable(False);
  EnabledControlValue(taxPanel1 , False);
  EnabledControlValue(taxPanel2 , False);
  EnabledControlValue(sTabSheet7 , False);
  EnabledControlValue(sTabSheet8 , False);
  EnabledControlValue(sTabSheet9 , False);
end;

procedure TUI_APPSPC_frm.qryTaxAfterCancel(DataSet: TDataSet);
begin
  inherited;
  TaxButtonEnable(True);
  EnabledControlValue(taxPanel1);
  EnabledControlValue(taxPanel2);
  EnabledControlValue(sTabSheet7);
  EnabledControlValue(sTabSheet8);
  EnabledControlValue(sTabSheet9);
end;

procedure TUI_APPSPC_frm.btn_taxItemNewClick(Sender: TObject);
begin
  inherited;

  case (Sender as TsSpeedButton).Tag of

    //세금계산서 품목내역 입력
    1 :
      begin
        sDBGrid2.Enabled := False;
        TaxItemButtonEnable(False);
        ClearControlValue(taxPanel3);
        ClearControlValue(TaxIMDCODEPanel);
        ClearControlValue(TaxSIZEPanel);
        ClearControlValue(TaxBIGOPanel);
        qryTaxItem.Append;

        if qryTaxItem.RecordCount  = 0 then
        begin
          edt_TaxLINENO.Text := '0001';
          qryTaxItem.FieldByName('SEQ').AsInteger := MAX_SEQ('APPSPC_D2','2AJ');
        end
        else
        begin
          edt_TaxLINENO.Text := Format('%.4d',[MAX_LINENO('APPSPC_D2','2AJ',qryTax)]);
        end;
      end;

    //세금계산서 품목내역 수정
    2 :
      begin
        if qryTaxItem.RecordCount > 0 then
        begin
          TaxItemButtonEnable(False);
          sDBGrid2.Enabled := False;
          qryTaxItem.Edit;
        end;
      end;

    //세금계산서 품목내역 삭제
    3 :
      begin
        if qryTaxItem.RecordCount > 0 then
        begin
          IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'품목내역 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
            qryTaxItem.Delete;
        end;
      end;

    //세금계산서 품목내역 저장
    4 :
      begin
        TaxItemSaveDocument;
        qryTaxItem.Post;
        sDBGrid2.Enabled := True;
        //버튼,품목내역 패널
        TaxItemButtonEnable(True);
        EnabledControlValue(taxPanel3 , True);
      end;

    //세금계산서 품목내역 취소
    5 :
      begin
        qryTaxItem.Cancel;
        sDBGrid2.Enabled := True;
        //버튼,품목내역 패널
        TaxItemButtonEnable(True);
        EnabledControlValue(taxPanel3 , True);
      end;
  end;
end;

procedure TUI_APPSPC_frm.qryTaxItemAfterInsert(DataSet: TDataSet);
begin
  inherited;
  TaxItemButtonEnable(False);
  EnabledControlValue(taxPanel3 , False);
end;

procedure TUI_APPSPC_frm.btn_invNewClick(Sender: TObject);
var
  maint_no : String;
  nCursor : integer;
begin
  inherited;
  case (Sender as TsSpeedButton).Tag of
    //상업송장 입력
    1 :
      begin
        sDBGrid6.Enabled := False;
        //품목내역 버튼 활성화
        InvItemButtonEnable(True);

        //신규 작성되고있는 관리번호를 셀렉트
        qryInv.Close;
        qryInv.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
        qryInv.Open;

        qryInv.Append;
        qryInv.FieldByName('SEQ').AsInteger := MAX_SEQ('APPSPC_D1','1BW');

        //품목내역 새로고침(해당 상업송장 순번에 따른 품목내역조회를 위하여 사용)
        qryInvItem.Close;
        qryInvItem.SQL.Clear;
        qryInvItem.SQL.Text := APPSPCINVITEM_SQL;
        qryInvItem.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
        qryInvItem.Parameters.ParamByName('SEQNO').Value := qryInv.FieldByName('SEQ').AsInteger;
        qryInvItem.Open;

        //상업송장을 취소하면 롤백을 하기위해 사용
        qryCheckCancel.Close;
        qryCheckCancel.Parameters.ParamByName('KEYY').Value := edt_MAINT_NO.Text;
        qryCheckCancel.Parameters.ParamByName('DocGubun').Value := '1BW';
        qryCheckCancel.Parameters.ParamByName('SEQ').Value := qryInv.FieldByName('SEQ').AsInteger;
        qryCheckCancel.Open;

        ClearControlValue(invPanel1);
        ClearControlValue(invPanel2);
        ClearControlValue(invPanel3);
        ClearControlValue(InvIMCODEPanel);
        ClearControlValue(InvSIZEPanel);
      end;

    //상업송장 수정
    2 :
      begin
        if qryInv.RecordCount > 0 then
        begin
          sDBGrid6.Enabled := False;
          InvItemButtonEnable(True);
          qryInv.Edit;


          //수정중 취소를 하면 롤백하기 위해 사용
          qryCheckCancel.Close;
          qryCheckCancel.Parameters.ParamByName('KEYY').Value := edt_MAINT_NO.Text;
          qryCheckCancel.Parameters.ParamByName('DocGubun').Value := '2AH';
          qryCheckCancel.Parameters.ParamByName('SEQ').Value := qryInv.FieldByName('SEQ').AsInteger;
          qryCheckCancel.Open;
        end;
      end;

    //상업송장 삭제
    3 :
      begin
        if qryInv.RecordCount > 0 then
        begin
          with TADOQuery.Create(nil) do
          begin
            Connection := DMMssql.KISConnect;
            maint_no := edt_MAINT_NO.Text;
            try
              try
                IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'상업송장 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
                begin
                   nCursor := qryGoods.RecNo;

                   SQL.Text :=  'DELETE FROM APPSPC_D1  WHERE KEYY =' + QuotedStr(maint_no) + 'and DOC_GUBUN = '+ QuotedStr('1BW') +
                                ' and SEQ ='+IntToStr(qryInvSEQ.AsInteger);
                   ExecSQL;

                   SQL.Text :=  'DELETE FROM APPSPC_D2  WHERE KEYY =' + QuotedStr(maint_no) + 'and DOC_GUBUN = '+ QuotedStr('1BW') +
                                ' and SEQ ='+IntToStr(qryInvSEQ.AsInteger);
                   ExecSQL;

                   qryInv.Close;
                   qryInv.Open;

                   qryInvItem.Close;
                   qryInvItem.Open;

                   if qryInv.RecordCount = 0 then
                   begin
                     ClearControlValue(invPanel1);
                     ClearControlValue(invPanel2);
                     ClearControlValue(invPanel3);
                     ClearControlValue(InvIMCODEPanel);
                     ClearControlValue(InvSIZEPanel);
                   end;

                   if (qryInv.RecordCount > 0 ) AND (qryInv.RecordCount >= nCursor) Then
                   begin
                     qryInv.MoveBy(nCursor-1);
                   end
                   else
                     qryInv.First;
                end;
              except
                on E:Exception do
                 begin
                   DMMssql.KISConnect.RollbackTrans;
                   MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
                 end;
              end;
            finally
              Close;
              Free;
            end;
          end;
        end;
      end;

    //상업송장 저장
    4 :
      begin
        //품목내역이 작성중일경우 상업송장 저장불가
        if qryInvItem.State in [dsEdit , dsInsert] then
        begin
          if MessageBox(Self.Handle,'품목내역을 작성중입니다.','저장확인',MB_OK+MB_ICONINFORMATION) = ID_OK then Exit;
        end;

        InvSaveDocument;
        qryInv.Post;

        sDBGrid5.Enabled := True;

        btn_invItemNew.Enabled := False;
        btn_invItemMod.Enabled := False;
        btn_invItemDel.Enabled := False;
        btn_invItemSave.Enabled := False;
        btn_invItemCancel.Enabled := False;
      end;

    //상업송장 취소
    5 :
      begin
        if qryInv.RecordCount = 0 then
        begin
          ClearControlValue(invPanel1);
          ClearControlValue(invPanel2);
          ClearControlValue(invPanel3);
          ClearControlValue(InvIMCODEPanel);
          ClearControlValue(InvSIZEPanel);
        end;

      //------------------------------------------------------------------------
      //상업송장 품목내역관련
      //------------------------------------------------------------------------
        { 1.상업송장를 취소했기때문에 수정된 품목내역을 삭제하고 기존의 품목내역으로 다시 변경한다.(수정중 취소하는경우)
          2.신규작성중 취소작업을 하는경우 }

        if qryInv.State in [dsEdit , dsInsert] then
        begin
          with TADOQuery.Create(nil) do
          begin
            Connection := DMMssql.KISConnect;
            maint_no := edt_MAINT_NO.Text;
            try
              SQL.Text :=  'DELETE FROM APPSPC_D2  WHERE KEYY =' + QuotedStr(maint_no) + ' and DOC_GUBUN = '+ QuotedStr('1BW') +
                           ' and SEQ ='+IntToStr(qryInvSEQ.AsInteger);
              //ShowMessage(SQL.Text);
              ExecSQL;
              qryInvItem.Close;
              qryInvItem.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
              qryInvItem.Parameters.ParamByName('SEQNO').Value := qryInvSEQ.AsInteger;
              qryInvItem.Open;
            finally
              Close;
              Free;
            end;
          end;

          if qryCheckCancel.RecordCount > 0 then
          begin
            //기존에 있던 데이터를 다시저장
            qryInvItem.First;
            qryCheckCancel.First;
            while not qryCheckCancel.Eof do
            begin
              with qryInvItem do
              begin
                Append;
                //KEYY
                FieldByName('KEYY').AsString := qryCheckCancelKEYY.AsString;
                //순번
                FieldByName('SEQ').AsInteger := qryCheckCancelSEQ.AsInteger;
                //문서구분
                FieldByName('DOC_GUBUN').AsString := qryCheckCancelDOC_GUBUN.AsString;
                //일련번호
                FieldByName('LINE_NO').AsString := qryCheckCancelLINE_NO.AsString;
                //품목
                FieldByName('IMD_CODE1').AsString := qryCheckCancelIMD_CODE1.AsString;
                FieldByName('IMD_CODE4').AsString := qryCheckCancelIMD_CODE2.AsString;
                FieldByName('IMD_CODE3').AsString := qryCheckCancelIMD_CODE3.AsString;
                FieldByName('IMD_CODE2').AsString := qryCheckCancelIMD_CODE4.AsString;
                //규격
                FieldByName('SIZE1').AsString := qryCheckCancelSIZE1.AsString;
                FieldByName('SIZE2').AsString := qryCheckCancelSIZE2.AsString;
                FieldByName('SIZE3').AsString := qryCheckCancelSIZE3.AsString;
                FieldByName('SIZE4').AsString := qryCheckCancelSIZE4.AsString;
                FieldByName('SIZE5').AsString := qryCheckCancelSIZE5.AsString;
                FieldByName('SIZE6').AsString := qryCheckCancelSIZE6.AsString;
                FieldByName('SIZE7').AsString := qryCheckCancelSIZE7.AsString;
                FieldByName('SIZE8').AsString := qryCheckCancelSIZE8.AsString;
                FieldByName('SIZE9').AsString := qryCheckCancelSIZE9.AsString;
                FieldByName('SIZE10').AsString := qryCheckCancelSIZE10.AsString;
                //수량단위
                FieldByName('QTYC').AsString := qryCheckCancelQTYC.AsString;
                //수량
                FieldByName('QTY').AsCurrency := qryCheckCancelQTY.AsCurrency;
                //수량소계단위
                FieldByName('TOTQTYC').AsString := qryCheckCancelTOTQTYC.AsString;
                //수량소계
                FieldByName('TOTQTY').AsCurrency := qryCheckCancelTOTQTY.AsCurrency;
                //단가
                FieldByName('PRICE').AsCurrency := qryCheckCancelPRICE.AsCurrency;
                //기준수량단위
                FieldByName('PRICEC').AsString := qryCheckCancelPRICEC.AsString;
                //기준수량
                FieldByName('PRICE_G').AsCurrency := qryCheckCancelPRICE_G.AsCurrency;
                //공급가액외화단위
                FieldByName('SUP_AMTC').AsString := qryCheckCancelSUP_AMTC.AsString;
                //공급가액외화
                FieldByName('SUP_AMT').AsCurrency := qryCheckCancelSUP_AMT.AsCurrency;
                //금액소계단위
                FieldByName('SUP_TOTAMTC').AsString := qryCheckCancelSUP_TOTAMTC.AsString;
                //금액소계
                FieldByName('SUP_TOTAMT').AsCurrency := qryCheckCancelSUP_TOTAMT.AsCurrency;
                //필드저장
                Post;
              end;
            end;
          end;
        end;

        //품목내역의 모든데이터를 삭제할경우 EDIT를 클리어
        if qryInvItem.RecordCount = 0 then
          ClearControlValue(invPanel3)
        else
          InvItemReadDocument;

        btn_invItemNew.Enabled := False;
        btn_invItemMod.Enabled := False;
        btn_invItemDel.Enabled := False;
        btn_invItemSave.Enabled := False;
        btn_invItemCancel.Enabled := False;

        sDBGrid6.Enabled := True;
        qryInv.Cancel;
      end;
  end;
end;

procedure TUI_APPSPC_frm.qryInvAfterInsert(DataSet: TDataSet);
begin
  inherited;
  InvButtonEnable(False);
  EnabledControlValue(invPanel1 , False);
  EnabledControlValue(invPanel2 , False);
end;

procedure TUI_APPSPC_frm.qryInvAfterCancel(DataSet: TDataSet);
begin
  inherited;
  InvButtonEnable(True);
  EnabledControlValue(invPanel1);
  EnabledControlValue(invPanel2);
end;

procedure TUI_APPSPC_frm.btn_invItemNewClick(Sender: TObject);
begin
  inherited;

  case (Sender as TsSpeedButton).Tag of
    //상업송장 품목 내역입력
    1 :
      begin
        sDBGrid7.Enabled := False;
        InvItemButtonEnable(False);
        ClearControlValue(invPanel3);
        ClearControlValue(InvIMCODEPanel);
        ClearControlValue(InvSIZEPanel);
        qryInvItem.Append;

        if qryInvItem.RecordCount = 0 then
        begin
          edt_InvLINENO.Text := '0001';
          qryInvItem.FieldByName('SEQ').AsInteger := MAX_SEQ('APPSPC_D2','1BW');
        end
        else
        begin
          edt_InvLINENO.Text := Format('%.4d',[MAX_LINENO('APPSPC_D2','1BW',qryInv)]);
        end;
      end;

    //상업송장 품목 내역수정
    2 :
      begin
        if qryInvItem.RecordCount > 0 then
        begin
          sDBGrid7.Enabled := False;
          InvItemButtonEnable(False);
          qryInvItem.Edit;
        end;
      end;

    //상업송장 품목 내역삭제
    3 :
      begin
        if qryInvItem.RecordCount > 0 then
        begin
          IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'품목내역 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
            qryInvItem.Delete;
        end;
      end;

    //상업송장 품목 내역저장
    4 :
      begin
        InvItemSaveDocument;
        qryInvItem.Post;
        sDBGrid7.Enabled := True;
        InvItemButtonEnable(True);
        EnabledControlValue(invPanel3, True);
      end;

    //상업송장 품목 내역취소
    5 :
      begin
        qryInvItem.Cancel;
        sDBGrid7.Enabled := True;
        InvItemButtonEnable(True);
        EnabledControlValue(invPanel3, True);
      end;
  end;
end;

procedure TUI_APPSPC_frm.qryInvItemAfterInsert(DataSet: TDataSet);
begin
  inherited;
  InvItemButtonEnable(False);
  EnabledControlValue(invPanel3 , False);
end;

procedure TUI_APPSPC_frm.qryInvItemAfterCancel(DataSet: TDataSet);
begin
  inherited;
  InvItemButtonEnable(True);
  EnabledControlValue(invPanel3);
end;

function TUI_APPSPC_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  i : Integer;
begin
  ErrMsg := TStringList.Create;

  Try

    IF Trim(edt_BGM2.Text) = '' THEN
      ErrMsg.Add('[문서정보] 문서번호(추심은행코드)를 입력해야합니다.');

    IF Trim(edt_BGM3.Text) = '' THEN
      ErrMsg.Add('[문서정보] 문서번호(개설은행코드)를 입력해야합니다.');

    IF Trim(edt_CPNO.Text) = '' THEN
      ErrMsg.Add('[문서정보] 신청번호를 입력해야합니다.');

    IF Trim(edt_APPCODE.Text) = '' THEN
      ErrMsg.Add('[문서정보] 신청인 상호코드를 입력해야합니다.');

    IF Trim(edt_APPSNAME.Text) = '' THEN
      ErrMsg.Add('[문서정보] 신청인 상호를 입력해야합니다.');

    IF Trim(edt_APPNAME.Text) = '' THEN
      ErrMsg.Add('[문서정보] 신청인 대표자를 입력해야합니다.');

    IF Trim(edt_APPSAUPNO.Text) = '' THEN
      ErrMsg.Add('[문서정보] 신청인 사업자등록번호를 입력해야합니다.');

    IF (Trim(edt_APPADDR1.Text) = '') and (Trim(edt_APPADDR2.Text) = '') and (Trim(edt_APPADDR3.Text) = '')THEN
      ErrMsg.Add('[문서정보] 신청인 주소를 입력해야합니다.');

    IF Trim(edt_APPELEC.Text) = '' THEN
      ErrMsg.Add('[문서정보] 신청인 전자서명을 입력해야합니다.');

    IF Trim(edt_CPBANK.Text) = '' THEN
      ErrMsg.Add('[문서정보] 추심은행 은행코드를 입력해야합니다.');

    IF Trim(edt_CPBANKNAME.Text) = '' THEN
      ErrMsg.Add('[문서정보] 추심은행 은행명을 입력해야합니다.');

    IF Trim(edt_CPBANKBU.Text) = '' THEN
      ErrMsg.Add('[문서정보] 추심은행 지점명을 입력해야합니다.');

    IF Trim(edt_CPACCOUNTNO.Text) = '' THEN
      ErrMsg.Add('[문서정보] 추심은행 계좌번호를 입력해야합니다.');

    IF (Trim(edt_CPNAME1.Text) = '') and (Trim(edt_CPNAME2.Text) = '') THEN
      ErrMsg.Add('[문서정보] 추심은행 계좌주를 입력해야합니다.');

    IF Trim(edt_CPCURR.Text) = '' THEN
      ErrMsg.Add('[문서정보] 추심은행 계좌통화를 입력해야합니다.');

    IF Trim(mask_DFSAUPNO.Text) = '' THEN
      ErrMsg.Add('[문서정보] 구매자 사업자등록번호를 입력해야합니다.');

    IF (Trim(edt_DFNAME1.Text) = '') and (Trim(edt_DFNAME2.Text) = '') and (Trim(edt_DFNAME3.Text) = '') THEN
      ErrMsg.Add('[문서정보] 구매자 상호를 입력해야합니다.');

    if combo_LATYPE.ItemIndex = 0 then
    begin
      IF Trim(edt_CPAMTC.Text) = '' THEN
      ErrMsg.Add('[문서정보] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 추심금액(외화)단위는 필수입니다.');

      IF Trim(curr_CPAMTU.Text) = '0' THEN
      ErrMsg.Add('[문서정보] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 추심금액(외화)는 필수입니다.');

      IF Trim(curr_CPAMT.Text) = '0' THEN
      ErrMsg.Add('[문서정보] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 추심금액(원화)는 필수입니다.');
    end
    else if combo_LATYPE.ItemIndex = 1 then
    begin
      IF Trim(edt_CPAMTC.Text) = '' THEN
      ErrMsg.Add('[문서정보] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 추심금액(외화)단위는 필수입니다.');

      IF Trim(curr_CPAMTU.Text) = '0' THEN
      ErrMsg.Add('[문서정보] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 추심금액(외화)는 필수입니다.');
    end
    else if combo_LATYPE.ItemIndex = 2 then
    begin
      IF Trim(curr_CPAMT.Text) = '0' THEN
      ErrMsg.Add('[문서정보] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 추심금액(원화)는 필수입니다.');
    end;

    IF Trim(edt_DOCNO.Text) = '' THEN
      ErrMsg.Add('[문서정보] 내국신용장 문서번호를 입력해야합니다.');

    IF Trim(mask_ISSDATE.Text) = '' THEN
      ErrMsg.Add('[문서정보] 내국신용장 개설일자를 입력해야합니다.');

    IF Trim(edt_LRNO.Text) = '' THEN
      ErrMsg.Add('[문서정보] 내국신용장번호를 입력해야합니다.');

    IF Trim(edt_LABANKBUCODE.Text) = '' THEN
      ErrMsg.Add('[문서정보] 내국신용장 개설은행코드를 입력해야합니다.');

    IF Trim(edt_LABANKNAMEP.Text) = '' THEN
      ErrMsg.Add('[문서정보] 내국신용장 개설은행명을 입력해야합니다.');

    IF Trim(edt_LABANKBUP.Text) = '' THEN
      ErrMsg.Add('[문서정보] 내국신용장 개설은행지점명을 입력해야합니다.');

  //----------------------------------------------------------------------------
  //물품수령증명서
  //----------------------------------------------------------------------------
    qryGoods.Close;
    qryGoods.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
    qryGoods.Open;
    if qryGoods.RecordCount = 0 then
    begin
       ErrMsg.Add('[물품수령증명서] 물품수령증명서는 1건이상 작성되어야 합니다.');
    end
    else
    begin
      i := 0;

      //ShowMessage(IntToStr(qryGoods.recordcount));

      qryGoods.First;
      while not qryGoods.Eof do
      begin
        Inc(i);

        IF Trim(qryGoodsDOC_NO.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 문서번호를 입력해야합니다.');

        IF Trim(qryGoodsSE_SNAME.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 공급자 상호를 입력해야합니다.');

        IF Trim(qryGoodsBY_SNAME.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 물품수령인 상호를 입력해야합니다.');

        IF Trim(qryGoodsBY_NAME.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 물품수령인 대표자명을 입력해야합니다.');

        IF Trim(qryGoodsBY_ELEC.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 물품수령인 전자서명을 입력해야합니다.');

        IF (Trim(qryGoodsBY_ADDR1.AsString) = '') and (Trim(qryGoodsBY_ADDR1.AsString) = '') and (Trim(qryGoodsBY_ADDR1.AsString) = '')  THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 물품수령인 주소를 입력해야합니다.');

        IF Trim(qryGoodsBY_ELEC.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 물품수령인 전자서명을 입력해야합니다.');

        IF Trim(qryGoodsLR_NO.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 내국신용장 발급번호를 입력해야합니다.');

        IF Trim(qryGoodsGET_DATE.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 내국신용장 물품인도기일을 입력해야합니다.');

        IF Trim(qryGoodsEXP_DATE.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 내국신용장 유효기일을 입력해야합니다.');

        IF Trim(qryGoodsLR_NO2.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 물품수령증명서 발급번호를 입력해야합니다.');

        IF Trim(qryGoodsISS_DATE.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 물품수령증명서 발급일자를 입력해야합니다.');

        IF Trim(qryGoodsACC_DATE.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 물품수령증명서 인수일자를 입력해야합니다.');

        IF Trim(qryGoodsLA_BANK.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 개설은행 은행코드를 입력해야합니다.');

        IF Trim(qryGoodsLA_BANKNAME.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 개설은행 은행명을 입력해야합니다.');

        IF Trim(qryGoodsLA_BANKBU.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 개설은행 지점명을 입력해야합니다.');

        IF Trim(qryGoodsLA_ELEC.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 개설은행 전자서명을 입력해야합니다.');

        IF Trim(qryGoodsLA_BANKBU.AsString) = '' THEN
          ErrMsg.Add('[물품수령증명서] '+IntToStr(i)+'행 개설은행 지점명을 입력해야합니다.');

        if combo_LATYPE.ItemIndex = 0 then
        begin
          IF Trim(qryGoodsISS_AMT.AsString) = '0' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 개설금액은 필수입니다.');

          IF Trim(qryGoodsISS_AMTC.AsString) = '' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 개설금액(외화) 통화는 필수입니다.');

          IF Trim(qryGoodsISS_AMTU.AsString) = '0' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 개설금액(외화)는 필수입니다.');

          IF Trim(qryGoodsISS_EXPAMT.AsString) = '0' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 인수금액은 필수입니다.');

          IF Trim(qryGoodsISS_EXPAMTC.AsString) = '' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 인수금액(외화) 통화는 필수입니다.');

          IF Trim(qryGoodsISS_EXPAMTU.AsString) = '0' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 원화표시 외화부기 내국신용장일때 인수금액(외화)는 필수입니다.');
        end
        else if combo_LATYPE.ItemIndex = 1 then
        begin
          IF Trim(qryGoodsISS_AMTC.AsString) = '' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 외화표시 내국신용장일때 개설금액(외화) 통화는 필수입니다.');

          IF Trim(qryGoodsISS_AMTU.AsString) = '0' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 외화표시 내국신용장일때 개설금액(외화)는 필수입니다.');

          IF Trim(qryGoodsISS_EXPAMTC.AsString) = '' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 외화표시 내국신용장일때 인수금액(외화) 통화는 필수입니다.');

          IF Trim(qryGoodsISS_EXPAMTU.AsString) = '0' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 외화표시 내국신용장일때 인수금액(외화)는 필수입니다.');
        end
        else if combo_LATYPE.ItemIndex = 2 then
        begin
          IF Trim(qryGoodsISS_AMT.AsString) = '0' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 원화표시 내국신용장일때 개설금액은 필수입니다.');

          IF Trim(qryGoodsISS_EXPAMT.AsString) = '0' THEN
            ErrMsg.Add('[물품수령증명서] 내국신용장 종류가 원화표시 내국신용장일때 인수금액은 필수입니다.');
        end;
        qryGoods.Next;
      end;
    end;

  //----------------------------------------------------------------------------
  //세금계산서 , 상업송장 둘다 없거나 두가지 모두 작성되었을 경우
  //----------------------------------------------------------------------------
    qryTax.Close;
    qryTax.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
    qryTax.Open;

    qryInv.Close;
    qryInv.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
    qryInv.Open;

    if (qryTax.RecordCount = 0) and (qryInv.RecordCount = 0) then
    begin
      ErrMsg.Add('세금계산서와 상업송장 중 한가지는 입력해야합니다.');
    end
    else if (qryTax.RecordCount >= 1 ) and (qryInv.RecordCount >= 1) then
    begin
      ErrMsg.Add('세금계산서와 상업송장 중 한가지만 입력해야합니다.');
    end;
  //----------------------------------------------------------------------------
  //세금계산서 1건 이상일 경우
  //----------------------------------------------------------------------------
    if qryTax.RecordCount >= 1 then
    begin
      i := 0;
      qryTax.First;
      while not qryTax.Eof do
      begin

        inc(i);

        IF Trim(qryTaxDOC_NO.AsString) = '' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 문서번호를 입력해야합니다.');

        IF Trim(qryTaxISS_DATE.AsString) = '' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 작성일을 입력해야합니다.');

        IF (Trim(qryTaxSE_CODE.AsString) = '') or (Trim(qryTaxSE_SNAME.AsString) = '') THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급자 상호를 입력해야합니다.');

        IF Trim(qryTaxSE_NAME.AsString) = '' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급자 대표자명을 입력해야합니다.');

        IF Trim(qryTaxSE_SAUPNO.AsString) = '' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급자 사업자번호를 입력해야합니다.');

        IF (Trim(qryTaxSE_ADDR1.AsString) = '') and (Trim(qryTaxSE_ADDR2.AsString) = '') and (Trim(qryTaxSE_ADDR3.AsString) = '') THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급자 주소를 입력해야합니다.');

        IF (Trim(qryTaxBY_CODE.AsString) = '') or (Trim(qryTaxBY_SNAME.AsString) = '') THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급받는자 상호를 입력해야합니다.');

        IF Trim(qryTaxBY_NAME.AsString) = '' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급받는자 대표자명을 입력해야합니다.');

        IF Trim(qryTaxBY_NAME.AsString) = '' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급받는자 대표자명을 입력해야합니다.');

        IF Trim(qryTaxBY_SAUPNO.AsString) = '' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급받는자 사업자번호를 입력해야합니다.');

        IF (Trim(qryTaxBY_ADDR1.AsString) = '') and (Trim(qryTaxBY_ADDR2.AsString) = '') and (Trim(qryTaxBY_ADDR3.AsString) = '') THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급받는자 주소를 입력해야합니다.');

        IF Trim(qryTaxISS_EXPAMT.AsString) = '0' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 공급가액을 입력해야합니다.');

        IF Trim(qryTaxTOTCNT.AsString) = '0' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 총수량을 입력해야합니다.');

        IF Trim(qryTaxTOTCNTC.AsString) = '' THEN
            ErrMsg.Add('[세금계산서] '+IntToStr(i)+'행 총수량 단위를 입력해야합니다.');

        qryTax.Next;
      end;
    end;

  //----------------------------------------------------------------------------
  //상업송장이 1건 이상일 경우
  //----------------------------------------------------------------------------
    if qryInv.RecordCount >= 1 then
    begin
      i := 0;
      qryInv.First;
      while not qryInv.Eof do
      begin

        Inc(i);

        IF Trim(qryInvDOC_NO.AsString) = '' THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+' 상업송장번호를 입력해야합니다.');

        IF Trim(qryInvISS_DATE.AsString) = '' THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+'행 발급일자를 입력해야합니다.');

        IF Trim(qryInvISS_TOTAMT.AsString) = '0' THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+'행 총금액을 입력해야합니다.');

        IF Trim(qryInvISS_TOTAMTC.AsString) = '' THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+'행 총금액 단위를 입력해야합니다.');

        IF (Trim(qryInvSE_CODE.AsString) = '') and (Trim(qryInvSE_SNAME.AsString) = '') THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+'행 공급자 상호를 입력해야합니다.');

        IF Trim(qryInvSE_NAME.AsString) = '' THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+'행 공급자 대표자명을 입력해야합니다.');

        IF (Trim(qryInvSE_ADDR1.AsString) = '') and (Trim(qryInvSE_ADDR2.AsString) = '') and (Trim(qryInvSE_ADDR3.AsString) = '') THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+'행 공급자 주소를 입력해야합니다.');

        IF (Trim(qryInvBY_CODE.AsString) = '') and (Trim(qryInvBY_SNAME.AsString) = '') THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+'행 공급받는자 상호를 입력해야합니다.');

        IF Trim(qryInvBY_NAME.AsString) = '' THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+'행 공급받는자 대표자명을 입력해야합니다.');

        IF (Trim(qryInvBY_ADDR1.AsString) = '') and (Trim(qryInvBY_ADDR2.AsString) = '') and (Trim(qryInvBY_ADDR3.AsString) = '') THEN
            ErrMsg.Add('[상업송장] '+IntToStr(i)+'행 공급받는자 주소를 입력해야합니다.');

        qryInv.Next;
      end;
    end;

    Result := ErrMsg.Text;
  finally
    qryGoods.Close;
    qryTax.Close;
    qryInv.Close;
    ErrMsg.Free;
  end;
end;

procedure TUI_APPSPC_frm.SaveDocument(Sender: TObject);
var
  SQLCreate : TSQLCreate;
  DocNo : String;
  CreateDate : TDateTime;
  LATypeCode : String;
begin

  try
    //--------------------------------------------------------------------------
    // 유효성 검사
    //--------------------------------------------------------------------------
    if (Sender as TsButton).Tag = 1 then
    begin
      Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
      if Dlg_ErrorMessage_frm.Run_ErrorMessage( '판매대금추심(매입)의뢰서' ,CHECK_VALIDITY ) Then
      begin
        FreeAndNil(Dlg_ErrorMessage_frm);
        Exit;
      end;
    end;

  //----------------------------------------------------------------------------
  // SQL생성기
  //----------------------------------------------------------------------------
    SQLCreate := TSQLCreate.Create;

        with SQLCreate do
        begin
          DocNo := edt_MAINT_NO.Text;

        //----------------------------------------------------------------------
        // 문서헤더 및 APPSPC_H.DB
        //----------------------------------------------------------------------

          Case ProgramControlType of
            ctInsert :
            begin
              DMLType := dmlInsert;
              ADDValue('MAINT_NO',DocNo);
            end;

            ctModify :
            begin
              DMLType := dmlUpdate;
              ADDWhere('MAINT_NO',DocNo);
            end;
          end;

          SQLHeader('APPSPC_H');

           //문서저장구분(0:임시, 1:저장)
          ADDValue('CHK2' , IntToStr((Sender as TsButton).Tag));
          //사용자
          ADDValue('USER_ID', edt_USER_ID.Text);
          //문서기능
          ADDValue('MESSAGE1' , edt_msg1.Text);
          //신청일자
          ADDValue('DATEE' , mask_DATEE.Text);
          ADDValue('APP_DATE' , mask_DATEE.Text);
          //전자문서구분
          ADDValue('BGM_GUBUN' , Copy(combo_BGMGUBUN.Text,0,3));
          //문서번호
          ADDValue('BGM1' , edt_BGM1.Text);
          ADDValue('BGM2' , edt_BGM2.Text);
          ADDValue('BGM3' , edt_BGM3.Text);
          ADDValue('BGM4' , edt_BGM4.Text);
          //신청번호
          ADDValue('CP_NO' , edt_CPNO.Text);
        //----------------------------------------------------------------------
        //신청인
        //----------------------------------------------------------------------
          //상호코드
          ADDValue('APP_CODE' , edt_APPCODE.Text);
          //상호
          ADDValue('APP_SNAME' , edt_APPSNAME.Text );
          //대표자
          ADDValue('APP_NAME' , edt_APPNAME.Text );
          //사업자등록번호
          ADDValue('APP_SAUPNO' , edt_APPSAUPNO.Text );
          //주소
          ADDValue('APP_ADDR1' , edt_APPADDR1.Text );
          ADDValue('APP_ADDR2' , edt_APPADDR2.Text );
          ADDValue('APP_ADDR3' , edt_APPADDR3.Text );
          //전자서명
          ADDValue('APP_ELEC' , edt_APPELEC.Text);

        //----------------------------------------------------------------------
        //구매자
        //----------------------------------------------------------------------
          //사업자등록번호
          ADDValue('DF_SAUPNO' , mask_DFSAUPNO.Text );
          //상호
          ADDValue('DF_NAME1' , edt_DFNAME1.Text );
          ADDValue('DF_NAME2' , edt_DFNAME2.Text );
          ADDValue('DF_NAME3' , edt_DFNAME3.Text );
          //E_MAIL
          ADDValue('DF_EMAIL1' , edt_DFEMAIL1.Text );
          ADDValue('DF_EMAIL2' , edt_DFEMAIL2.Text);
        //----------------------------------------------------------------------
        //추심은행
        //----------------------------------------------------------------------
          //은행코드
          ADDValue('CP_BANK' , edt_CPBANK.Text );
          //은행명
          ADDValue('CP_BANKNAME' , edt_CPBANKNAME.Text );
          //지점명
          ADDValue('CP_BANKBU' , edt_CPBANKBU.Text );
          //계좌번호
          ADDValue('CP_ACCOUNTNO' , edt_CPACCOUNTNO.Text );
          //계좌주
          ADDValue('CP_NAME1' , edt_CPNAME1.Text);
          ADDValue('CP_NAME2' , edt_CPNAME2.Text);
          //계좌통화
          ADDValue('CP_CURR' , edt_CPCURR.Text);
        //----------------------------------------------------------------------
        //추심계좌
        //----------------------------------------------------------------------
          //계좌번호1
          ADDValue('CP_ADD_ACCOUNTNO1' , edt_CPADDACCOUNTNO1.Text);
          //계좌주1
          ADDValue('CP_ADD_NAME1' ,edt_CPADDNAME1.Text);
          //계좌통화1
          ADDValue('CP_ADD_CURR1' , edt_CPADDCURR1.Text);
          //계좌번호2
          ADDValue('CP_ADD_ACCOUNTNO2' , edt_CPADDACCOUNTNO2.Text);
          //계좌주2
          ADDValue('CP_ADD_NAME2' , edt_CPADDNAME2.Text);
          //계좌통화2
          ADDValue('CP_ADD_CURR2' , edt_CPADDCURR2.Text);
        //----------------------------------------------------------------------
        //추심금액
        //----------------------------------------------------------------------
          //추심금액 (외화) 통화
          ADDValue('CP_AMTC' , edt_CPAMTC.Text );
          //추심금액 (외화)
          ADDValue('CP_AMTU' , curr_CPAMTU.Text);
          //적용환율
          ADDValue('CP_CUX' , curr_CPCUX.Text);
          //추심금액 (원화)
          ADDValue('CP_AMT' , curr_CPAMT.Text);

        end;

        with TADOQuery.Create(nil) do
        begin
          try
            Connection := DMMssql.KISConnect;
            SQL.Text := SQLCreate.CreateSQL;
            //if sCheckBox1.Checked then
              //Clipboard.AsText := SQL.Text;
            ExecSQL;
          finally
            Close;
            Free;
          end;
        end;

  //----------------------------------------------------------------------------
  // APPSPC_D1 (문서정보 내국신용장 부분 저장)
       with SQLCreate do
        begin
          DocNo := edt_MAINT_NO.Text;
          SQLHeader('APPSPC_D1');

          Case ProgramControlType of
            ctInsert :
            begin
              DMLType := dmlInsert;
              ADDValue('KEYY',DocNo);
              //문서구분
              ADDValue('DOC_GUBUN' , '2AP' );
              //SEQ
              if MAX_SEQ('APPSPC_D1','2AP') = 0 then
                ADDValue('SEQ' , '1')
              else
                ADDValue('SEQ' , IntToStr(MAX_SEQ('APPSPC_D1','2AP')));
            end;

            ctModify :
            begin
              DMLType := dmlUpdate;
              ADDWhere('KEYY',DocNo);
              ADDWhere('DOC_GuBun','2AP');
            end;
          end;

          //문서번호
          ADDValue('DOC_NO' , edt_DOCNO.Text);
          //종류
          case combo_LATYPE.ItemIndex of
            0 : LATypeCode := '2AA';
            1 : LATypeCode := '2AB';
            2 : LATypeCode := '2AC';
          end;
          ADDValue('LA_TYPE' , LATypeCode);
          //개설일자
          ADDValue('ISS_DATE' , mask_ISSDATE.Text);
          //내국신용장번호
          ADDValue('LR_NO' , edt_LRNO.Text);
          //개설은행
          ADDValue('LA_BANKBUCODE' , edt_LABANKBUCODE.Text );
          ADDValue('LA_BANKNAMEP' , edt_LABANKNAMEP.Text );
          //개설은행지점명
          ADDValue('LA_BANKBUP' , edt_LABANKBUP.Text );

        end;

        with TADOQuery.Create(nil) do
         begin
           try
             Connection := DMMssql.KISConnect;
             SQL.Text := SQLCreate.CreateSQL;
             //if sCheckBox1.Checked then
             // Clipboard.AsText := SQL.Text;
             ExecSQL;
           finally
             Close;
             Free;
           end;
         end;


    //------------------------------------------------------------------------------
    // 프로그램 제어
    //------------------------------------------------------------------------------
      ProgramControlType := ctView;

    //------------------------------------------------------------------------------
    // 버튼정리
    //------------------------------------------------------------------------------
      ButtonEnable(True);
    //------------------------------------------------------------------------------
    // 접근제어
    //------------------------------------------------------------------------------
      Page_control.ActivePageIndex := 0;
      Page_controlChange(Page_control);
      sDBGrid8.Enabled := True;
      sDBGrid3.Enabled := True;
      sDBGrid4.Enabled := True;
      sDBGrid5.Enabled := True;
      sDBGrid2.Enabled := True;
      sDBGrid6.Enabled := True;
      sDBGrid7.Enabled := True;
      sDBGrid1.Enabled := True;

    //------------------------------------------------------------------------------
    // 데이터제어
    //------------------------------------------------------------------------------
      EnabledControlValue(sPanel4);
      
      EnabledControlValue(basicPanel1);
      EnabledControlValue(basicPanel2);
      EnabledControlValue(basicPanel3);
      EnabledControlValue(basicPanel4);
      EnabledControlValue(basicPanel5);

      //물품수령증명서
      EnabledControlValue(goodsBtnPanel);
      EnabledControlValue(goodsItemBtnPanel);
      EnabledControlValue(goodsPanel1);
      EnabledControlValue(goodsPanel2);
      EnabledControlValue(goodsPanel3);
      EnabledControlValue(goodsPanel4);
      EnabledControlValue(goodsPanel5);

      //세금계산서
      EnabledControlValue(taxBtnPanel);
      EnabledControlValue(taxItemBtnPanel);
      EnabledControlValue(taxPanel1);
      EnabledControlValue(taxPanel2);
      EnabledControlValue(taxPanel3);
      EnabledControlValue(sTabSheet7);
      EnabledControlValue(sTabSheet8);
      EnabledControlValue(sTabSheet9);

      //상업송장
      EnabledControlValue(invBtnPanel);
      EnabledControlValue(invItemBtnPanel);
      EnabledControlValue(invPanel1);
      EnabledControlValue(invPanel2);
      EnabledControlValue(invpanel3);

    //----------------------------------------------------------------------------
    //새로고침
    //----------------------------------------------------------------------------
    CreateDate := ConvertStr2Date(mask_DATEE.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             DocNo);

    //트랜잭션 저장
    if DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.CommitTrans;

//  qryList.Close;
//   qryList.Open;

  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message),'저장오류',MB_OK+MB_ICONERROR);
    end;
  end;

end;

function TUI_APPSPC_frm.MAX_SEQ(TableName: String; DocGubun: String): Integer;
begin
  with qryMAX_SEQ do
  begin
    Close;
    if DocGubun <> '' then
      SQL.Text := 'SELECT MAX(SEQ) AS MaxSEQ FROM '+ TableName +' WHERE KEYY =' + QuotedStr(edt_MAINT_NO.Text) +
                  ' and  DOC_GUBUN =' + QuotedStr(DocGubun)
    else
      SQL.Text := 'SELECT MAX(SEQ) AS MaxSEQ FROM '+ TableName +' WHERE KEYY =' + QuotedStr(edt_MAINT_NO.Text);
    Open;

    Result := FieldByName('MaxSEQ').AsInteger + 1;
  end;
end;

function TUI_APPSPC_frm.MAX_LINENO(TableName: String; DocGubun: String; AdoQuery: TADOQuery): Integer;
begin
  with qryMAX_LINENO do
  begin
    //ShowMessage(qryGoods.FieldByName('SEQ').AsString);
    Close;
    if DocGubun <> '' then
      SQL.Text := 'SELECT MAX(LINE_NO) AS MaxLINE FROM '+ TableName +' WHERE KEYY =' + QuotedStr(edt_MAINT_NO.Text) + 'and SEQ =' +
                   IntToStr(AdoQuery.FieldByName('SEQ').AsInteger) + 'and  DOC_GUBUN =' + QuotedStr(DocGubun)
    else
      SQL.Text := 'SELECT MAX(LINE_NO) AS MaxLINE FROM '+ TableName +' WHERE KEYY =' + QuotedStr(edt_MAINT_NO.Text) + 'and SEQ =' +
                   IntToStr(AdoQuery.FieldByName('SEQ').AsInteger);
    Open;

    Result := FieldByName('MaxLINE').AsInteger + 1;
  end;
end;

procedure TUI_APPSPC_frm.btnSaveClick(Sender: TObject);
begin
  inherited;
  //문서 작성중일경우 전체저장 불가
   if (qryList.State in [dsInsert , dsEdit]) and
     ((qryGoods.State in [dsInsert , dsEdit]) or (qryGoodsItem.State in [dsInsert , dsEdit]) or
     (qryTax.State in [dsInsert , dsEdit]) or (qryTaxItem.State in [dsInsert , dsEdit]) or
     (qryInv.State in [dsInsert , dsEdit]) or (qryInvItem.State in [dsInsert , dsEdit])) then
  begin
   if MessageBox(Self.Handle,'작성중인 문서가 있습니다.','저장확인',MB_OK+MB_ICONINFORMATION) = ID_OK then Exit;
  end;
  SaveDocument(Sender);
end;

//물품수령증명서 저장 프로시져
procedure TUI_APPSPC_frm.GoodsSaveDocument;
begin
  with qryGoods do
  begin

    //KEYY
    FieldByName('KEYY').AsString := edt_MAINT_NO.Text;
    //문서구분
    FieldByName('DOC_GUBUN').AsString := '2AH';
    //문서구분2
    FieldByName('LR_GUBUN').AsString := 'CRG';

  //----------------------------------------------------------------------------
  // 물품수령증명서
  //----------------------------------------------------------------------------
    //문서번호
    FieldByName('DOC_NO').AsString := edt_GoodsDOCNO.Text;
    //발급번호
    FieldByName('LR_NO2').AsString := edt_GoodsLRNO2.Text;
    //HS부호
    FieldByName('BSN_HSCODE').AsString := mask_GoodsBSNHSCODE.Text;
    //발급일자
    FieldByName('ISS_DATE').AsString := mask_GoodsISSDATE.Text;
    //인수일자
    FieldByName('ACC_DATE').AsString := mask_GoodsACCDATE.Text;
    //유효기일
    FieldByName('VAL_DATE').AsString := mask_GoodsVALDATE.Text;
  //----------------------------------------------------------------------------
  // 물품수령인
  //----------------------------------------------------------------------------
    //상호코드
    FieldByName('BY_CODE').AsString := edt_GoodsBYCODE.Text;
    //상호
    FieldByName('BY_SNAME').AsString := edt_GoodsBYSNAME.Text;
    //대표자명
    FieldByName('BY_NAME').AsString := edt_GoodsBYNAME.Text;
    //전자서명
    FieldByName('BY_ELEC').AsString := edt_GoodsBYELEC.Text;
    //주소
    FieldByName('BY_ADDR1').AsString := edt_GoodsBYADDR1.Text;
    FieldByName('BY_ADDR2').AsString := edt_GoodsBYADDR2.Text;
    FieldByName('BY_ADDR3').AsString := edt_GoodsBYADDR3.Text;
  //----------------------------------------------------------------------------
  // 내국신용장
  //----------------------------------------------------------------------------
    //발급번호
    FieldByName('LR_NO').AsString := edt_GoodsLRNO.Text;
    //물품인도기일
    FieldByName('GET_DATE').AsString := mask_GoodsGETDATE.Text;
    //유효기일
    FieldByName('EXP_DATE').AsString := mask_GoodsEXPDATE.Text;
  //----------------------------------------------------------------------------
  // 공급자
  //----------------------------------------------------------------------------
    //코드
    FieldByName('SE_CODE').AsString := edt_GoodsSECODE.Text;
    //이름
    FieldByName('SE_SNAME').AsString := edt_GoodsSESNAME.Text;
  //----------------------------------------------------------------------------
  // 개설은행
  //----------------------------------------------------------------------------
    //은행코드
    FieldByName('LA_BANK').AsString := edt_GoodsLABANK.Text;
    //은행명
    FieldByName('LA_BANKNAME').AsString := edt_GoodsLABANKNAME.Text;
    //지점명
    FieldByName('LA_BANKBU').AsString := edt_GoodsLABANKBU.Text;
    //전자서명
    FieldByName('LA_ELEC').AsString := edt_goodsLAELEC.Text;
    //매매기준율
    FieldByName('EXCH').AsString := curr_goodsEXCH.Text;
    //개설금액
    FieldByName('ISS_AMT').AsString := curr_goodsISSAMT.Text;
    //개설금액(외화) 통화
    FieldByName('ISS_AMTC').AsString := edt_goodsISSAMTC.Text;
    //개설금액 (외화)
    FieldByName('ISS_AMTU').AsString := curr_goodsISSAMTU.Text;
    //인수금액
    FieldByName('ISS_EXPAMT').AsString := curr_goodsISSEXPAMT.Text;
    //인수금액 (외화) 통화
    FieldByName('ISS_EXPAMTC').AsString := edt_goodsISSEXPAMTC.Text;
    //인수금액 (외화)
    FieldByName('ISS_EXPAMTU').AsString := curr_goodsISSEXPAMTU.Text;
    //총수량 단위
    FieldByName('TOTCNTC').AsString := edt_goodsTOTCNTC.Text;
    //총수량
    FieldByName('TOTCNT').AsString := curr_goodsTOTCNT.Text;
    //총금액 통화
    FieldByName('ISS_TOTAMTC').AsString := edt_goodsISSTOTAMTC.Text;
    //총금액
    FieldByName('ISS_TOTAMT').AsString := curr_goodsISSTOTAMT.Text;
  //----------------------------------------------------------------------------
  // 기타조건
  //----------------------------------------------------------------------------
    FieldByName('REMARK2_1').AsString := edt_GoodsREMARK2_1.Text;
    FieldByName('REMARK2_2').AsString := edt_GoodsREMARK2_2.Text;
    FieldByName('REMARK2_3').AsString := edt_GoodsREMARK2_3.Text;
    FieldByName('REMARK2_4').AsString := edt_GoodsREMARK2_4.Text;
    FieldByName('REMARK2_5').AsString := edt_GoodsREMARK2_5.Text;
  //----------------------------------------------------------------------------
  // 참조사항
  //----------------------------------------------------------------------------
    FieldByName('REMARK1_1').AsString := edt_GoodsREMARK1_1.Text;
    FieldByName('REMARK1_2').AsString := edt_GoodsREMARK1_2.Text;
    FieldByName('REMARK1_3').AsString := edt_GoodsREMARK1_3.Text;
    FieldByName('REMARK1_4').AsString := edt_GoodsREMARK1_4.Text;
    FieldByName('REMARK1_5').AsString := edt_GoodsREMARK1_5.Text;
  end;
end;

//물품수령증명서 품목내역 저장 프로시져
procedure TUI_APPSPC_frm.GoodsItemSaveDocument;
begin
  with qryGoodsItem do
  begin

    //KEYY
    FieldByName('KEYY').AsString := edt_MAINT_NO.Text;

    //순번 [APPSPC_D1의 순번(SEQ)을 사용해 품목내역을 구분한다.
    if qryGoodsItem.RecordCount  <> 0 then
      FieldByName('SEQ').AsInteger := qryGoods.FieldByName('SEQ').AsInteger;

    //문서구분
    FieldByName('DOC_GUBUN').AsString := '2AH';   
  //----------------------------------------------------------------------------
  // 품목내역
  //----------------------------------------------------------------------------
    //일련번호
    FieldByName('LINE_NO').AsString := edt_GoodsLINENO.Text;
    //HS부호
    FieldByName('HS_NO').AsString := mask_GoodsHSNO.Text;
    //품목
    FieldByName('IMD_CODE1').AsString := edt_GoodsIMDCODE1.Text;
    FieldByName('IMD_CODE2').AsString := edt_GoodsIMDCODE2.Text;
    FieldByName('IMD_CODE3').AsString := edt_GoodsIMDCODE3.Text;
    FieldByName('IMD_CODE4').AsString := edt_GoodsIMDCODE4.Text;
    //규격
    FieldByName('SIZE1').AsString := edt_GoodsSIZE1.Text;
    FieldByName('SIZE2').AsString := edt_GoodsSIZE2.Text;
    FieldByName('SIZE3').AsString := edt_GoodsSIZE3.Text;
    FieldByName('SIZE4').AsString := edt_GoodsSIZE4.Text;
    FieldByName('SIZE5').AsString := edt_GoodsSIZE5.Text;
    FieldByName('SIZE6').AsString := edt_GoodsSIZE6.Text;
    FieldByName('SIZE7').AsString := edt_GoodsSIZE7.Text;
    FieldByName('SIZE8').AsString := edt_GoodsSIZE8.Text;
    FieldByName('SIZE9').AsString := edt_GoodsSIZE9.Text;
    FieldByName('SIZE10').AsString := edt_GoodsSIZE10.Text;
    //수량 단위
    FieldByName('QTYC').AsString := edt_GoodsQTYC.Text;
    //수량
    FieldByName('QTY').AsString := curr_GoodsQTY.Text;
    //수량소계 단위
    FieldByName('TOTQTYC').AsString := edt_GoodsTOTQTYC.Text;
    //수량소계
    FieldByName('TOTQTY').AsString := curr_GoodsTOTQTY.Text;
    //단가
    FieldByName('PRICE').AsString := curr_GoodsPRICE.Text;
    //기준수량 단위
    FieldByName('PRICEC').AsString := edt_GoodsPRICEC.Text;
    //기준수량
    FieldByName('PRICE_G').AsString := curr_GoodsPRICEG.Text;
    //금액 단위
    FieldByName('SUP_AMTC').AsString := edt_GoodsSUPAMTC.Text;
    //금액
    FieldByName('SUP_AMT').AsString := curr_GoodsSUPAMT.Text;
    //금액소계 단위
    FieldByName('SUP_TOTAMTC').AsString := edt_GoodsSUPTOTAMTC.Text;
    //금액
    FieldByName('SUP_TOTAMT').AsString := curr_GoodsSUPTOTAMT.Text;
  end;
end;

procedure TUI_APPSPC_frm.TaxSaveDocument;
begin
  with qryTax do
  begin

    //KEYY
    FieldByName('KEYY').AsString := edt_MAINT_NO.Text;
    //문서구분
    FieldByName('DOC_GUBUN').AsString := '2AJ';

  //----------------------------------------------------------------------------
  //  기본입력사항
  //----------------------------------------------------------------------------
    //문서번호
    FieldByName('DOC_NO').AsString := edt_TaxDOCNO.Text;
    //권
    FieldByName('VB_RENO').AsString := edt_TaxVBRENO.Text;
    //호
    FieldByName('VB_SENO').AsString := edt_TaxVBSENO.Text;
    //일련번호
    FieldByName('VB_FSNO').AsString := edt_TaxVBFSNO.Text;
    //관리번호
    FieldByName('VB_MAINTNO').AsString := edt_TaxVBMAINTNO.Text;
    //작성일
    FieldByName('ISS_DATE').AsString := mask_TaxISSDATE.Text;
    //참조번호
    FieldByName('VB_DMNO').AsString := edt_TaxVBDMNO.Text;
  //----------------------------------------------------------------------------
  //  공급자
  //----------------------------------------------------------------------------
    //상호코드
    FieldByName('SE_CODE').AsString := edt_TaxSECODE.Text;
    //상호
    FieldByName('SE_SNAME').AsString := edt_TaxSESNMAE.Text;
    //대표자
    FieldByName('SE_NAME').AsString := edt_TaxSENAME.Text;
    //사업자번호
    FieldByName('SE_SAUPNO').AsString := mask_TaxSESAUPNO.Text;
    //주소
    FieldByName('SE_ADDR1').AsString := edt_TaxSEADDR1.Text;
    FieldByName('SE_ADDR2').AsString := edt_TaxSEADDR2.Text;
    FieldByName('SE_ADDR3').AsString := edt_TaxSEADDR3.Text;
    //전자서명
    FieldByName('SE_ELEC').AsString := edt_TaxSEELEC.Text;
    //업태
    FieldByName('SG_UPTAI1_1').AsString := edt_TaxSGUPTAI1_1.Text;
    FieldByName('SG_UPTAI1_2').AsString := edt_TaxSGUPTAI1_2.Text;
    FieldByName('SG_UPTAI1_3').AsString := edt_TaxSGUPTAI1_3.Text;
    //종목
    FieldByName('HN_ITEM1_1').AsString := edt_TaxHNITEM1_1.Text;
    FieldByName('HN_ITEM1_2').AsString := edt_TaxHNITEM1_2.Text;
    FieldByName('HN_ITEM1_3').AsString := edt_TaxHNITEM1_3.Text;
  //----------------------------------------------------------------------------
  //  공급받는자
  //----------------------------------------------------------------------------
    //상호코드
    FieldByName('BY_CODE').AsString := edt_TaxBYCODE.Text;
    //상호
    FieldByName('BY_SNAME').AsString := edt_TaxBYSNAME.Text;
    //대표자
    FieldByName('BY_NAME').AsString := edt_TaxBYNAME.Text;
    //사업자번호
    FieldByName('BY_SAUPNO').AsString := mask_TaxBYSAUPNO.Text;
    //주소
    FieldByName('BY_ADDR1').AsString := edt_TaxBYADDR1.Text;
    FieldByName('BY_ADDR2').AsString := edt_TaxBYADDR2.Text;
    FieldByName('BY_ADDR3').AsString := edt_TaxBYADDR3.Text;
    //전자서명
    FieldByName('BY_ELEC').AsString := edt_TaxBYELEC.Text;
    //업태
    FieldByName('SG_UPTAI2_1').AsString := edt_TaxSGUPTAI2_1.Text;
    FieldByName('SG_UPTAI2_2').AsString := edt_TaxSGUPTAI2_2.Text;
    FieldByName('SG_UPTAI2_3').AsString := edt_TaxSGUPTAI2_3.Text;
    //종목
    FieldByName('HN_ITEM2_1').AsString := edt_TaxHNITEM2_1.Text;
    FieldByName('HN_ITEM2_2').AsString := edt_TaxHNITEM2_2.Text;
    FieldByName('HN_ITEM2_3').AsString := edt_TaxHNITEM2_3.Text;
  //----------------------------------------------------------------------------
  //  수탁자
  //----------------------------------------------------------------------------
    //상호코드
    FieldByName('AG_CODE').AsString := edt_TaxAGCODE.Text;
    //상호
    FieldByName('AG_SNAME').AsString := edt_TaxAGSNAME.Text;
    //대표자
    FieldByName('AG_NAME').AsString := edt_TaxAGNAME.Text;
    //사업자번호
    FieldByName('AG_SAUPNO').AsString := mask_TaxAGSAUPNO.Text;
    //주소
    FieldByName('AG_ADDR1').AsString := edt_TaxAGADDR1.Text;
    FieldByName('AG_ADDR2').AsString := edt_TaxAGADDR2.Text;
    FieldByName('AG_ADDR3').AsString := edt_TaxAGADDR3.Text;
    //전자서명
    FieldByName('AG_ELEC').AsString := edt_TaxAGELEC.Text;
  //----------------------------------------------------------------------------
  //  결제방법
  //----------------------------------------------------------------------------
    //현금 원화금액
    FieldByName('PAI_AMT1').AsString := curr_TaxPAIAMT1.Text;
    //현금 외화금액
    FieldByName('PAI_AMTU1').AsString := curr_TaxPAIAMTU1.Text;
    //현금 외화금액단위
    FieldByName('PAI_AMTC1').AsString := edt_TaxPAIAMTC1.Text;
    //수표 원화금액
    FieldByName('PAI_AMT2').AsString := curr_TaxPAIAMT2.Text;
    //수표 외화금액
    FieldByName('PAI_AMTU2').AsString := curr_TaxPAIAMTU2.Text;
    //수표 외화금액단위
    FieldByName('PAI_AMTC2').AsString := edt_TaxPAIAMTC2.Text;
    //어음 원화금액
    FieldByName('PAI_AMT3').AsString := curr_TaxPAIAMT3.Text;
    //어음 외화금액
    FieldByName('PAI_AMTU3').AsString := curr_TaxPAIAMTU3.Text;
    //어음 외화금액단위
    FieldByName('PAI_AMTC3').AsString := edt_TaxPAIAMTC3.Text;
    //외상 원화금액
    FieldByName('PAI_AMT4').AsString := curr_TaxPAIAMT4.Text;
    //외상 외화금액
    FieldByName('PAI_AMTU4').AsString := curr_TaxPAIAMTU4.Text;
    //외상 외화금액단위
    FieldByName('PAI_AMTC4').AsString := edt_TaxPAIAMTC4.Text;
  //----------------------------------------------------------------------------
  //  금액 및 수량
  //----------------------------------------------------------------------------
    //공급가액
    FieldByName('ISS_EXPAMT').AsString := curr_TaxISSEXPAMT.Text;
    //세액
    FieldByName('ISS_EXPAMTU').AsString := curr_TaxISSEXPAMTU.Text;
    //총수량
    FieldByName('TOTCNT').AsString := curr_TaxTOTCNT.Text;
    //총수량 단위
    FieldByName('TOTCNTC').AsString := edt_TaxTOTCNTC.Text;
    //영수/청구 구분
    FieldByName('VB_GUBUN').AsString := edt_TaxVBGUBUN.Text;
    //공란수
    FieldByName('VB_DETAILNO').AsString := edt_TaxVBDETAILNO.Text;
  //----------------------------------------------------------------------------
  //  비고
  //----------------------------------------------------------------------------
    FieldByName('REMARK1_1').AsString := edt_TaxREMARK1_1.Text;
    FieldByName('REMARK1_1').AsString := edt_TaxREMARK1_2.Text;
    FieldByName('REMARK1_1').AsString := edt_TaxREMARK1_3.Text;
    FieldByName('REMARK1_1').AsString := edt_TaxREMARK1_4.Text;
    FieldByName('REMARK1_1').AsString := edt_TaxREMARK1_5.Text;
  end;
end;

//세금계산서 품목내역 저장
procedure TUI_APPSPC_frm.TaxItemSaveDocument;
begin
 with qryTaxItem do
  begin

    //KEYY
    FieldByName('KEYY').AsString := edt_MAINT_NO.Text;

    //순번 [APPSPC_D1의 순번(SEQ)을 사용해 품목내역을 구분한다.
    if qryTaxItem.RecordCount  <> 0 then
      FieldByName('SEQ').AsInteger := qryTax.FieldByName('SEQ').AsInteger;
   //ShowMessage(qryTaxItem.FieldByName('SEQ').AsString);
    //문서구분
    FieldByName('DOC_GUBUN').AsString := '2AJ';
    //일련번호
    FieldByName('LINE_NO').AsString := edt_TaxLINENO.Text;
    //공급일자
    FieldByName('DE_DATE').AsString := mask_TaxDEDATE.Text;
    //품목
    FieldByName('IMD_CODE1').AsString := edt_TaxIMDCODE1.Text;
    FieldByName('IMD_CODE2').AsString := edt_TaxIMDCODE2.Text;
    FieldByName('IMD_CODE3').AsString := edt_TaxIMDCODE3.Text;
    FieldByName('IMD_CODE4').AsString := edt_TaxIMDCODE4.Text;
    //규격
    FieldByName('SIZE1').AsString := edt_TaxSIZE1.Text;
    FieldByName('SIZE2').AsString := edt_TaxSIZE2.Text;
    FieldByName('SIZE3').AsString := edt_TaxSIZE3.Text;
    FieldByName('SIZE4').AsString := edt_TaxSIZE4.Text;
    FieldByName('SIZE5').AsString := edt_TaxSIZE5.Text;
    FieldByName('SIZE6').AsString := edt_TaxSIZE6.Text;
    FieldByName('SIZE7').AsString := edt_TaxSIZE7.Text;
    FieldByName('SIZE8').AsString := edt_TaxSIZE8.Text;
    FieldByName('SIZE9').AsString := edt_TaxSIZE9.Text;
    FieldByName('SIZE10').AsString := edt_TaxSIZE10.Text;
    //수량단위
    FieldByName('QTYC').AsString := edt_TaxQTYC.Text;
    //수량
    FieldByName('QTY').AsString := curr_TaxQTY.Text;
    //수량소계단위
    FieldByName('TOTQTYC').AsString := edt_TaxTOTQTYC.Text;
    //수량소계
    FieldByName('TOTQTY').AsString := curr_TaxTOTQTY.Text;
    //환율
    FieldByName('CUX_RATE').AsString := curr_TaxCUXRATE.Text;
    //단가
    FieldByName('PRICE').AsString := curr_TaxPRICE.Text;
    //기준수량단위
    FieldByName('PRICEC').AsString := edt_TaxPRICEC.Text;
    //기준수량
    FieldByName('PRICE_G').AsString := curr_TaxPRICEG.Text;
    //공급가액단위
    FieldByName('SUP_AMTC').AsString := edt_TaxSUPAMTC.Text;
    //공급가액
    FieldByName('SUP_AMT').AsString := curr_TaxSUPAMT.Text;
    //공급가액소계단위
    FieldByName('SUP_TOTAMTC').AsString := edt_TaxSUPTOTAMTC.Text;
    //공급가액소계
    FieldByName('SUP_TOTAMT').AsString := curr_TaxSUPTOTAMT.Text;
    //공급가액외화단위
    FieldByName('VB_AMTC').AsString := edt_TaxVBAMTC.Text;
    //공급가액외화
    FieldByName('VB_AMT').AsString := curr_TaxVBAMT.Text;
    //공급가액외화소계단위
    FieldByName('VB_TOTAMTC').AsString := edt_VBTOTAMTC.Text;
    //공급가액외화소계
     FieldByName('VB_TOTAMT').AsString := curr_TaxVBAMT.Text;
    //세액단위
    FieldByName('VB_TAXC').AsString := edt_TaxVBTAXC.Text;
    //세액
    FieldByName('VB_TAX').AsString := curr_TaxVBTAX.Text;
    //세액소계단위
    FieldByName('VB_TOTTAXC').AsString := edt_TaxVBTOTTAXC.Text;
    //세액소계
    FieldByName('VB_TOTTAX').AsString := curr_TaxVBTOTTAX.Text;
    //참조사항
    FieldByName('BIGO1').AsString := edt_TaxBIGO1.Text;
    FieldByName('BIGO2').AsString := edt_TaxBIGO2.Text;
    FieldByName('BIGO3').AsString := edt_TaxBIGO3.Text;
    FieldByName('BIGO4').AsString := edt_TaxBIGO4.Text;
    FieldByName('BIGO5').AsString := edt_TaxBIGO5.Text;
  end;
end;

procedure TUI_APPSPC_frm.InvSaveDocument;
begin
  with qryInv do
  begin
    if qryInv.State = dsInsert then
    begin
      //KEYY
      FieldByName('KEYY').AsString := edt_MAINT_NO.Text;

      //순번
      if qryInv.RecordCount  = 0 then
        FieldByName('SEQ').AsInteger := 1
      else
      begin
        FieldByName('SEQ').AsInteger := MAX_SEQ('APPSPC_D1','1BW');
      end;

      //문서구분
      FieldByName('DOC_GUBUN').AsString := '1BW';
    end;
  //----------------------------------------------------------------------------
  //  상업송장증명서
  //----------------------------------------------------------------------------
    //문서번호
    FieldByName('DOC_NO').AsString := edt_InvDOCNO.Text;
    //발급일자
    FieldByName('ISS_DATE').AsString := mask_InvISSDATE.Text;
    //총수량
    FieldByName('TOTCNT').AsString := curr_InvTOTCNT.Text;
    //총수량단위
    FieldByName('TOTCNTC').AsString :=  edt_InvTOTCNTC.Text;
    //총금액
    FieldByName('ISS_TOTAMT').AsString := curr_InvISSTOTAMT.Text;
    //총금액단위
    FieldByName('ISS_TOTAMTC').AsString := edt_InvISSTOTAMTC.Text;
  //----------------------------------------------------------------------------
  // 비고
  //----------------------------------------------------------------------------
    //항차
    FieldByName('REMARK1_1').AsString := edt_InvREMARK1_1.Text;
    //선명
    FieldByName('REMARK1_2').AsString := edt_InvREMARK1_2.Text;
    //선적지
    FieldByName('REMARK1_3').AsString := edt_InvREMARK1_3.Text;
    //도착지
    FieldByName('REMARK1_4').AsString := edt_InvREMARK1_4.Text;
    //인도조건
    FieldByName('REMARK1_5').AsString := edt_InvREMARK1_5.Text;
  //----------------------------------------------------------------------------
  // 물품공급자(수출자)
  //----------------------------------------------------------------------------
    //상호코드
    FieldByName('SE_CODE').AsString := edt_InvSECODE.Text;
    //상호
    FieldByName('SE_SNAME').AsString := edt_InvSESNAME.Text;
    //대표자
    FieldByName('SE_NAME').AsString := edt_InvSENAME.Text;
    //주소
    FieldByName('SE_ADDR1').AsString := edt_InvSEADDR1.Text;
    FieldByName('SE_ADDR2').AsString := edt_InvSEADDR2.Text;
    FieldByName('SE_ADDR3').AsString := edt_InvSEADDR3.Text;
  //----------------------------------------------------------------------------
  //  공급받는자(수입자)
  //----------------------------------------------------------------------------
    //상호코드
    FieldByName('BY_CODE').AsString := edt_InvBYCODE.Text;
    //상호
    FieldByName('BY_SNAME').AsString := edt_InvBYSNAME.Text;
    //대표자
    FieldByName('BY_NAME').AsString := edt_InvBYNAME.Text;
    //주소
    FieldByName('BY_ADDR1').AsString := edt_InvBYADDR1.Text;
    FieldByName('BY_ADDR2').AsString := edt_InvBYADDR2.Text;
    FieldByName('BY_ADDR3').AsString := edt_InvBYADDR3.Text;
  end;
end;

procedure TUI_APPSPC_frm.InvItemSaveDocument;
begin
  with qryInvItem do
  begin
    //KEYY
    FieldByName('KEYY').AsString := edt_MAINT_NO.Text;
    //순번 [APPSPC_D1의 순번(SEQ)을 사용해 품목내역을 구분한다.
    if qryInvItem.RecordCount  <> 0 then
      FieldByName('SEQ').AsInteger := qryInv.FieldByName('SEQ').AsInteger;
    //문서구분
    FieldByName('DOC_GUBUN').AsString := '1BW';
    //일련번호
    FieldByName('LINE_NO').AsString := edt_InvLINENO.Text;
    //품목
    FieldByName('IMD_CODE1').AsString := edt_InvIMDCODE1.Text;
    FieldByName('IMD_CODE4').AsString := edt_InvIMDCODE2.Text;
    FieldByName('IMD_CODE3').AsString := edt_InvIMDCODE3.Text;
    FieldByName('IMD_CODE2').AsString := edt_InvIMDCODE4.Text;
    //규격
    FieldByName('SIZE1').AsString := edt_InvSIZE1.Text;
    FieldByName('SIZE2').AsString := edt_InvSIZE2.Text;
    FieldByName('SIZE3').AsString := edt_InvSIZE2.Text;
    FieldByName('SIZE4').AsString := edt_InvSIZE4.Text;
    FieldByName('SIZE5').AsString := edt_InvSIZE5.Text;
    FieldByName('SIZE6').AsString := edt_InvSIZE6.Text;
    FieldByName('SIZE7').AsString := edt_InvSIZE7.Text;
    FieldByName('SIZE8').AsString := edt_InvSIZE8.Text;
    FieldByName('SIZE9').AsString := edt_InvSIZE9.Text;
    FieldByName('SIZE10').AsString := edt_InvSIZE10.Text;
    //수량단위
    FieldByName('QTYC').AsString := edt_InvQTYC.Text;
    //수량
    FieldByName('QTY').AsString := curr_InvQTY.Text;
    //수량소계단위
    FieldByName('TOTQTYC').AsString := edt_InvTOTQTYC.Text;
    //수량소계
    FieldByName('TOTQTY').AsString := curr_InvTOTQTY.Text;
    //단가
    FieldByName('PRICE').AsString := curr_InvPRICE.Text;
    //기준수량단위
    FieldByName('PRICEC').AsString := edt_InvPRICEC.Text;
    //기준수량
    FieldByName('PRICE_G').AsString := curr_InvPRICEG.Text;
    //공급가액외화단위
    FieldByName('SUP_AMTC').AsString := edt_InvSUPAMTC.Text;
    //공급가액외화
    FieldByName('SUP_AMT').AsString := curr_InvSUPAMT.Text;
    //금액소계단위
    FieldByName('SUP_TOTAMTC').AsString := edt_InvSUPTOTAMTC.Text;
    //금액소계
    FieldByName('SUP_TOTAMT').AsString := curr_InvSUPTOTAMT.Text;
  end;
end;

procedure TUI_APPSPC_frm.goodsBtnPanelClick(Sender: TObject);
begin
  inherited;
  //ShowMessage(IntToStr(sDBGrid3.DataSource.DataSet.RecordCount));
end;

procedure TUI_APPSPC_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;


        SQL.Text :=  'DELETE FROM APPSPC_H  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM APPSPC_D1 WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM APPSPC_D2 WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    Close;
    Free;
   end;
  end;

end;

procedure TUI_APPSPC_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(basicPanel1);
    ClearControlValue(basicPanel2);
    ClearControlValue(basicPanel3);
    ClearControlValue(basicPanel4);
    ClearControlValue(basicPanel5);
  end;

end;

procedure TUI_APPSPC_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  if AnsiMatchText(qryListCHK2.AsString,['','0','1','2','5','6']) then
  begin
    EditDocument;
  end
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_EDIT,'수정불가', MB_OK+MB_ICONINFORMATION);
end;

procedure TUI_APPSPC_frm.EditDocument;
begin
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctModify;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Edit;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
  GoodsButtonEnable(True);
  TaxButtonEnable(True);
  InvButtonEnable(True);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  Page_control.ActivePageIndex := 0;
  Page_controlChange(Page_control);
  sDBGrid8.Enabled := False;
  sDBGrid3.Enabled := False;
  sDBGrid4.Enabled := False;
  sDBGrid5.Enabled := False;
  sDBGrid2.Enabled := False;
  sDBGrid6.Enabled := False;
  sDBGrid7.Enabled := False;
  sDBGrid1.Enabled := False;
//------------------------------------------------------------------------------
//  데이터제어
//------------------------------------------------------------------------------
    EnabledControlValue(sPanel4 , False);
    EnabledControlValue(sPanel57 , False);
    EnabledControlValue(basicPanel1 , False);
    EnabledControlValue(basicPanel2 , False);
    EnabledControlValue(basicPanel3 , False);
    EnabledControlValue(basicPanel4 , False);
    EnabledControlValue(basicPanel5 , False);
end;

procedure TUI_APPSPC_frm.sBitBtn60Click(Sender: TObject);
begin
  inherited;
    ReadList(Mask_fromDate.Text,mask_toDate.Text,'');
    Mask_SearchDate1.Text := Mask_fromDate.Text;
    Mask_SearchDate2.Text := mask_toDate.Text;
end;

procedure TUI_APPSPC_frm.sBitBtn10Click(Sender: TObject);

begin
  inherited;

  Dialog_AttachFromLOCAD1_frm := TDialog_AttachFromLOCAD1_frm.Create(self);
  try
      if Dialog_AttachFromLOCAD1_frm.openDialog = mrOK then
      begin
        edt_DOCNO.Text := Dialog_AttachFromLOCAD1_frm.sDBGrid1.DataSource.DataSet.FieldByName('MAINT_NO').AsString;
        mask_ISSDATE.Text := Dialog_AttachFromLOCAD1_frm.sDBGrid1.DataSource.DataSet.FieldByName('ISS_DATE').AsString;
        edt_LRNO.Text := Dialog_AttachFromLOCAD1_frm.sDBGrid1.DataSource.DataSet.FieldByName('LC_NO').AsString;
        edt_LABANKBUCODE.Text := Dialog_AttachFromLOCAD1_frm.sDBGrid1.DataSource.DataSet.FieldByName('AP_BANK').AsString;
        edt_LABANKNAMEP.Text := Dialog_AttachFromLOCAD1_frm.sDBGrid1.DataSource.DataSet.FieldByName('AP_BANK1').AsString;
        edt_LABANKBUP.Text := Dialog_AttachFromLOCAD1_frm.sDBGrid1.DataSource.DataSet.FieldByName('AP_BANK2').AsString;
      end;
  finally

    FreeAndNil(Dialog_AttachFromLOCAD1_frm);

  end;

end;

procedure TUI_APPSPC_frm.sBitBtn31Click(Sender: TObject);
var
  i : Integer;
  remarkMemo,locRemMemo,imdCodeMemo,sizeMemo : TStringList;
begin
  inherited;
  ClearControlValue(goodsPanel1);
  ClearControlValue(goodsPanel2);
  ClearControlValue(goodsPanel3);
  ClearControlValue(goodsPanel4);
  ClearControlValue(goodsPanel5);
  ClearControlValue(GoodsimdcodePanel);
  ClearControlValue(GoodsSIZE_Panel);

  sDBGrid4.Enabled := True;

  Dialog_AttachFromLOCRC1_frm := TDialog_AttachFromLOCRC1_frm.Create(Self);
  try
      if Dialog_AttachFromLOCRC1_frm.openDialog = mrOK then
      begin
        if not (qryGoods.State in [dsEdit , dsInsert]) then qryGoods.Append;
        //----------------------------------------------------------------------
        //물품수령증명서
        //----------------------------------------------------------------------
          //문서번호
          edt_GoodsDOCNO.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('MAINT_NO').AsString;
          //발급번호
          edt_GoodsLRNO2.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('RFF_NO').AsString;
          //HS부호
          mask_GoodsBSNHSCODE.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('BSN_HSCODE').AsString;
          //발급일자
          mask_GoodsISSDATE.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('ISS_DAT').AsString;
          //인수일자
          mask_GoodsACCDATE.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('GET_DAT').AsString;
          //유효기일
          mask_GoodsVALDATE.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('EXP_DAT').AsString;
        //----------------------------------------------------------------------
        //물품수령인
        //----------------------------------------------------------------------
          //상호코드
          edt_GoodsBYCODE.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('APPLIC').AsString;
          //상호
          edt_GoodsBYSNAME.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('APPLIC1').AsString;
          //대표자명
          edt_GoodsBYNAME.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('APPLIC2').AsString;
          //전자서명
          edt_GoodsBYELEC.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('APPLIC3').AsString;
          //주소
          edt_GoodsBYADDR1.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('APPLIC4').AsString;
          edt_GoodsBYADDR2.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('APPLIC5').AsString;
          edt_GoodsBYADDR3.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('APPLIC6').AsString;
        //----------------------------------------------------------------------
        //내국신용장
        //----------------------------------------------------------------------
          //발급번호
          edt_GoodsLRNO.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('LOC_NO').AsString;
          //물품인도기일
          mask_GoodsGETDATE.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('LOADDATE').AsString;
          //유효기일
          mask_GoodsEXPDATE.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('EXPDATE').AsString;
        //----------------------------------------------------------------------
        //공급자
        //----------------------------------------------------------------------
          //공급자 코드
          edt_GoodsSECODE.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('BENEFC').AsString;
          //공급자
          edt_GoodsSESNAME.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('BENEFC1').AsString;
        //----------------------------------------------------------------------
        //개설은행
        //----------------------------------------------------------------------
          //은행코드
          edt_GoodsLABANK.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('AP_BANK').AsString;
          //은행명
          edt_GoodsLABANKNAME.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('AP_BANK1').AsString;
          //지점명
          edt_GoodsLABANKBU.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('AP_BANK3').AsString;
          //전자서명
          edt_goodsLAELEC.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('AP_NAME').AsString;
          //매매기준율
          curr_goodsEXCH.Value := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('EX_RATE').AsInteger;
          //개설금액
          curr_goodsISSAMT.Value := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('LOC2AMT').AsInteger;
          //개설금액(외화)통화
          edt_goodsISSAMTC.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('LOC1AMTC').AsString;
          //개설금액(외화)
          curr_goodsISSAMTU.Value := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('LOC1AMT').AsInteger;
          //인수금액
          curr_goodsISSEXPAMT.Value := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('RCT_AMT2').AsInteger;
          //인수금액(외화)통화
          edt_goodsISSEXPAMTC.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('RCT_AMT1C').AsString;
          //인수금액(외화)
          curr_goodsISSEXPAMTU.Value := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('RCT_AMT1').AsInteger;
          //총수량 단위
          edt_goodsTOTCNTC.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('TQTY_G').AsString;
          //총수량
          curr_goodsTOTCNT.Value := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('TQTY').AsInteger;
          //총금액 단위
          edt_goodsISSTOTAMTC.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('TAMT_G').AsString;
          //총금액
          curr_goodsISSTOTAMT.Value := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('TAMT').AsInteger;
        //----------------------------------------------------------------------
        //기타조건
        //----------------------------------------------------------------------
          //TStringList생성
          try
            remarkMemo := TStringList.Create;
            //remarkMemo에 REMARK1 대입
            remarkMemo.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('REMARK1').AsString;
            if remarkMemo.Text <> '' then
            begin
              for i := 0 to remarkMemo.Count-1 do
              begin
               (FindComponent('edt_GoodsREMARK2_' + IntToStr(i+1)) as TsEdit).Text :=  remarkMemo[i];
              end;
            end;
          finally
            remarkMemo.Free;
          end;
        //----------------------------------------------------------------------
        //참조사항
        //----------------------------------------------------------------------
          try
            locRemMemo := TStringList.Create;
            locRemMemo.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid1.DataSource.DataSet.FieldByName('LOC_REM1').AsString;
            if locRemMemo.Text <> '' then
            begin

              for i := 0 to locRemMemo.Count-1 do
              begin
                (FindComponent('edt_GoodsREMARK1_' + IntToStr(i+1)) as TsEdit).Text := locRemMemo[i];
              end;
            end;
          finally
            locRemMemo.Free;
          end;
      end;

  //----------------------------------------------------------------------------
  // 품목내역
  //----------------------------------------------------------------------------
    if Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.RecordCount > 0 then
    begin
      //등록시 품목내역에 DB가 저장되어있으면 삭제후 추가
      if qryGoodsItem.RecordCount > 0 then
      begin
        qryGoodsItem.First;
        while not qryGoodsItem.Eof do
        begin
          qryGoodsItem.Delete;
          //qryGoodsItem.Next;
        end;
      end;
      
      Dialog_AttachFromLOCRC1_frm.qryGoods.First;
      while not Dialog_AttachFromLOCRC1_frm.qryGoods.Eof do
      begin

        qryGoodsItem.Append;
          //KEYY값저장----------------------------------------------------------
            qryGoodsItemKEYY.AsString := edt_MAINT_NO.Text;
          //--------------------------------------------------------------------
          //SEQ순번저장----------------------------------------------------------
            qryGoodsItemSEQ.AsInteger := qryGoods.FieldByName('SEQ').AsInteger;
          //--------------------------------------------------------------------
          //DOC_GUBUN 저장------------------------------------------------------
            qryGoodsItemDOC_GUBUN.AsString := '2AH';
          //--------------------------------------------------------------------
          //일련번호------------------------------------------------------------
            qryGoodsItemLINE_NO.AsString := Format('%.4d',[Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('SEQ').AsInteger]);
          //--------------------------------------------------------------------
          //HS부호--------------------------------------------------------------
            qryGoodsItemHS_NO.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('HS_NO').AsString;
          //--------------------------------------------------------------------
          //품목----------------------------------------------------------------
            try
              imdCodeMemo := TStringList.Create;
              imdCodeMemo.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('NAME1').AsString;
              if imdCodeMemo.Text <> '' then
              begin
                for i := 0 to imdCodeMemo.Count-1 do
                begin
                  qryGoodsItem.FieldByName('IMD_CODE'+IntToStr(i+1)).AsString := imdCodeMemo[i];
                end;
              end;
            finally
              imdCodeMemo.Free;
            end;
          //--------------------------------------------------------------------
          //규격----------------------------------------------------------------
            try
              sizeMemo := TStringList.Create;
              sizeMemo.Text := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('SIZE1').AsString;
              if sizeMemo.Text <> '' then
              begin
                for i := 0 to sizeMemo.Count-1 do
                begin
                  qryGoodsItem.FieldByName('SIZE'+IntToStr(i+1)).AsString := sizeMemo[i];
                end;
              end;
            finally
              sizeMemo.Free;
            end;
          //--------------------------------------------------------------------
          //수량단위------------------------------------------------------------
            qryGoodsItemQTYC.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('QTY_G').AsString;
          //--------------------------------------------------------------------
          //수량
            qryGoodsItemQTY.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('QTY').AsString;
          //--------------------------------------------------------------------
          //수량소계단위
            qryGoodsItemTOTQTYC.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('STQTY_G').AsString;
          //--------------------------------------------------------------------
          //수량소계
            qryGoodsItemTOTQTY.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('STQTY').AsString;
          //--------------------------------------------------------------------
          //단가
            qryGoodsItemPRICE.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('PRICE').AsString;
          //--------------------------------------------------------------------
          //기준수량단위
            qryGoodsItemPRICEC.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('QTYG_G').AsString;
          //--------------------------------------------------------------------
          //기준수량
            qryGoodsItemPRICE_G.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('QTYG').AsString;
          //--------------------------------------------------------------------
          //금액단위
            qryGoodsItemSUP_AMTC.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('AMT_G').AsString;
          //--------------------------------------------------------------------
          //금액
            qryGoodsItemSUP_AMT.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('AMT').AsString;
          //--------------------------------------------------------------------
          //금액소계단위
            qryGoodsItemSUP_TOTAMTC.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('STAMT_G').AsString;
          //--------------------------------------------------------------------
          //금액소계
            qryGoodsItemSUP_TOTAMT.AsString := Dialog_AttachFromLOCRC1_frm.sDBGrid2.DataSource.DataSet.FieldByName('STAMT').AsString;
          //--------------------------------------------------------------------

        qryGoodsItem.Post;

        Dialog_AttachFromLOCRC1_frm.qryGoods.Next;
      end;

      GoodsItemReadDocument;
      //버튼 및 품목내역
      GoodsItemButtonEnable(True);
      EnabledControlValue(goodsPanel5 , True);      
    end;
  finally
    FreeAndNil(Dialog_AttachFromLOCRC1_frm);
  end;
end;

procedure TUI_APPSPC_frm.sBitBtn45Click(Sender: TObject);
var
  i : Integer;
  uptaiMemo,itemMemo,imdCodeMemo,sizeMemo,bigoMemo,remarkMemo : TStringList;
begin
  inherited;
  ClearControlValue(taxPanel1);
  ClearControlValue(taxPanel2);
  ClearControlValue(taxPanel3);
  ClearControlValue(sTabSheet7);
  ClearControlValue(sTabSheet8);
  ClearControlValue(sTabSheet9);
  ClearControlValue(TaxIMDCODEPanel);
  ClearControlValue(TaxSIZEPanel);
  ClearControlValue(TaxBIGOPanel);

  sDBGrid2.Enabled := True;

  Dialog_AttachFromVATBI2_frm := TDialog_AttachFromVATBI2_frm.Create(Self);
  try
    if Dialog_AttachFromVATBI2_frm.openDialog = mrOK then
    begin
        if not (qryTax.State in [dsEdit , dsInsert]) then qryTax.Append;
        //----------------------------------------------------------------------
        //기본입력사항
        //----------------------------------------------------------------------
          //문서번호
          edt_TaxDOCNO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('MAINT_NO').AsString;
          //권/호/일련번호
          edt_TaxVBRENO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('RE_NO').AsString;
          edt_TaxVBSENO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_NO').AsString;
          edt_TaxVBFSNO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('FS_NO').AsString;
          //관리번호 := 세금계산서번호
          edt_TaxVBMAINTNO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('RFF_NO').AsString;
          //작성일
          mask_TaxISSDATE.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('DRAW_DAT').AsString;
          //참조번호
          edt_TaxVBDMNO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('ACE_NO').AsString;
        //----------------------------------------------------------------------
        //공급자
        //----------------------------------------------------------------------
          //상호
          edt_TaxSECODE.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_CODE').AsString;
          edt_TaxSESNMAE.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_NAME1').AsString;
          //대표자
          edt_TaxSENAME.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_NAME3').AsString;
          //사업자번호
          mask_TaxSESAUPNO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_SAUP').AsString;
          //주소
          edt_TaxSEADDR1.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_ADDR1').AsString;
          edt_TaxSEADDR2.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_ADDR2').AsString;
          edt_TaxSEADDR3.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_ADDR3').AsString;
          //업태
          try
            uptaiMemo := TStringList.Create;
            uptaiMemo.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_UPTA1').AsString;
            if uptaiMemo.Text <> '' then
            begin
              for i := 0 to uptaiMemo.Count-1 do
              begin
               (FindComponent('edt_TaxSGUPTAI1_' + IntToStr(i+1)) as TsEdit).Text :=  uptaiMemo[i];
              end;
            end;
          finally
            uptaiMemo.Free;
          end;
          //종목
          try
            itemMemo := TStringList.Create;
            itemMemo.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SE_ITEM1').AsString;
            if itemMemo.Text <> '' then
            begin
              for i := 0 to itemMemo.Count-1 do
              begin
               (FindComponent('edt_TaxHNITEM1_' + IntToStr(i+1)) as TsEdit).Text :=  itemMemo[i];
              end;
            end;
          finally
            itemMemo.Free;
          end;
        //----------------------------------------------------------------------
        //공급받는자
        //----------------------------------------------------------------------
          //상호
          edt_TaxBYCODE.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('BY_CODE').AsString;
          edt_TaxBYSNAME.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('BY_NAME1').AsString;
          //대표자
          edt_TaxBYNAME.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('BY_NAME3').AsString;
          //사업자번호
          mask_TaxBYSAUPNO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('BY_SAUP').AsString;
          //주소
          edt_TaxBYADDR1.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('BY_ADDR1').AsString;
          edt_TaxBYADDR2.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('BY_ADDR2').AsString;
          edt_TaxBYADDR3.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('BY_ADDR3').AsString;
          //업태
          try
            uptaiMemo := TStringList.Create;
            uptaiMemo.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('BY_UPTA1').AsString;
            if uptaiMemo.Text <> '' then
            begin
              for i := 0 to uptaiMemo.Count-1 do
              begin
               (FindComponent('edt_TaxSGUPTAI2_' + IntToStr(i+1)) as TsEdit).Text :=  uptaiMemo[i];
              end;
            end;
          finally
            uptaiMemo.Free;
          end;
          //종목
          try
            itemMemo := TStringList.Create;
            itemMemo.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('BY_ITEM1').AsString;
            if itemMemo.Text <> '' then
            begin
              for i := 0 to itemMemo.Count-1 do
              begin
               (FindComponent('edt_TaxHNITEM2_' + IntToStr(i+1)) as TsEdit).Text :=  itemMemo[i];
              end;
            end;
          finally
            itemMemo.Free;
          end;
        //----------------------------------------------------------------------
        //수탁자
        //----------------------------------------------------------------------
          //상호
          edt_TaxAGCODE.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AG_CODE').AsString;
          edt_TaxAGSNAME.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AG_NAME1').AsString;
          //대표자
          edt_TaxAGNAME.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AG_NAME3').AsString;
          //사업자번호
          mask_TaxAGSAUPNO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AG_SAUP').AsString;
          //주소
          edt_TaxAGADDR1.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AG_ADDR1').AsString;
          edt_TaxAGADDR2.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AG_ADDR2').AsString;
          edt_TaxAGADDR3.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AG_ADDR3').AsString;
        //----------------------------------------------------------------------
        //결제조건
        //----------------------------------------------------------------------
          //현금원화
          curr_TaxPAIAMT1.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT12').AsCurrency;
          //현금외화
          edt_TaxPAIAMTC1.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT11C').AsString;
          curr_TaxPAIAMTU1.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT11').AsCurrency;
          //수표원화
          curr_TaxPAIAMT2.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT22').AsCurrency;;
          //수표외화
          edt_TaxPAIAMTC2.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT21C').AsString;
          curr_TaxPAIAMTU2.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT21').AsCurrency;
          //어음원화
          curr_TaxPAIAMT3.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT32').AsCurrency;;
          //어음외화
          edt_TaxPAIAMTC3.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT31C').AsString;
          curr_TaxPAIAMTU3.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT31').AsCurrency;
          //외상원화
          curr_TaxPAIAMT4.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT42').AsCurrency;;
          //외상외화
          edt_TaxPAIAMTC4.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT41C').AsString;
          curr_TaxPAIAMTU4.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('AMT41').AsCurrency;
        //----------------------------------------------------------------------
        //금액및수량
        //----------------------------------------------------------------------
          //공급가액
          curr_TaxISSEXPAMT.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('SUP_AMT').AsCurrency;
          //세액
          curr_TaxISSEXPAMTU.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('TAXTAMT').AsCurrency;
          //총수량
          edt_TaxTOTCNTC.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('TQTYC').AsString;
          curr_TaxTOTCNT.Value := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('TQTY').AsCurrency;
          //영수/청구구분
          edt_TaxVBGUBUN.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('INDICATOR').AsString;
          //공란수
          edt_TaxVBDETAILNO.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('DETAILNO').AsString;
        //----------------------------------------------------------------------
        //비고
        //----------------------------------------------------------------------
          try
            remarkMemo := TStringList.Create;
            remarkMemo.Text := Dialog_AttachFromVATBI2_frm.sDBGrid1.DataSource.DataSet.FieldByName('REMARK1').AsString;
            if remarkMemo.Text <> '' then
            begin
              for i := 0 to remarkMemo.Count-1 do
              begin
               (FindComponent('edt_TaxREMARK1_' + IntToStr(i+1)) as TsEdit).Text :=  remarkMemo[i];
              end;
            end;
          finally
            remarkMemo.Free;
          end;
  //----------------------------------------------------------------------------
  // 품목내역
  //----------------------------------------------------------------------------
      if Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.RecordCount > 0 then
      begin
        //등록시 품목내역에 DB가 저장되어있으면 삭제후 추가
        if qryTaxItem.RecordCount > 0 then
        begin
          qryTaxItem.First;
          while not qryTaxItem.Eof do
          begin
            qryTaxItem.Delete;
          end;
        end;

        Dialog_AttachFromVATBI2_frm.qryGoods.First;
        while not Dialog_AttachFromVATBI2_frm.qryGoods.Eof do
        begin

          qryTaxItem.Append;
            //KEYY값저장----------------------------------------------------------
              qryTaxItemKEYY.AsString := edt_MAINT_NO.Text;
            //--------------------------------------------------------------------
            //DOC_GUBUN 저장------------------------------------------------------
              qryTaxItemDOC_GUBUN.AsString := '2AJ';
            //--------------------------------------------------------------------
            //SEQ순번저장----------------------------------------------------------
              qryTaxItemSEQ.AsInteger := qryTax.FieldByName('SEQ').AsInteger;
            //일련번호------------------------------------------------------------
              qryTaxItemLINE_NO.AsString := Format('%.4d',[Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('SEQ').AsInteger]);
            //--------------------------------------------------------------------
            //--------------------------------------------------------------------
            //품목----------------------------------------------------------------
              try
                imdCodeMemo := TStringList.Create;
                imdCodeMemo.Text := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('NAME1').AsString;
                if imdCodeMemo.Text <> '' then
                begin
                  for i := 0 to imdCodeMemo.Count-1 do
                  begin
                    qryTaxItem.FieldByName('IMD_CODE'+IntToStr(i+1)).AsString := imdCodeMemo[i];
                  end;
                end;
              finally
                imdCodeMemo.Free;
              end;
            //--------------------------------------------------------------------
            //규격----------------------------------------------------------------
              try
                sizeMemo := TStringList.Create;
                sizeMemo.Text := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('SIZE1').AsString;
                if sizeMemo.Text <> '' then
                begin
                  for i := 0 to sizeMemo.Count-1 do
                  begin
                    qryTaxItem.FieldByName('SIZE'+IntToStr(i+1)).AsString := sizeMemo[i];
                  end;
                end;
              finally
                sizeMemo.Free;
              end;
            //--------------------------------------------------------------------
            //수량단위------------------------------------------------------------
              qryTaxItemQTYC.AsString := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('QTY_G').AsString;
            //--------------------------------------------------------------------
            //수량
              qryTaxItemQTY.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('QTY').AsCurrency;
            //--------------------------------------------------------------------
            //수량소계단위
              qryTaxItemTOTQTYC.AsString := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('STQTY_G').AsString;
            //--------------------------------------------------------------------
            //수량소계
              qryTaxItemTOTQTY.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('STQTY').AsCurrency;
            //--------------------------------------------------------------------
            //단가
              qryTaxItemPRICE.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('PRICE').AsCurrency;
            //--------------------------------------------------------------------
            //기준수량단위
              qryTaxItemPRICEC.AsString := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('QTYG_G').AsString;
            //--------------------------------------------------------------------
            //기준수량
              qryTaxItemPRICE_G.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('QTYG').AsCurrency;
            //--------------------------------------------------------------------
            //공급가액단위
              qryTaxItemSUP_AMTC.AsString := 'KRW';
            //--------------------------------------------------------------------
            //공급가액
              qryTaxItemSUP_AMT.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('SUPAMT').AsCurrency;
            //--------------------------------------------------------------------
            //--------------------------------------------------------------------
            //공급가액 소계단위
              qryTaxItemSUP_TOTAMTC.AsString := 'KRW';
            //--------------------------------------------------------------------
            //공급가액 소계
              qryTaxItemSUP_TOTAMT.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('SUPSTAMT').AsCurrency;
            //--------------------------------------------------------------------
            //공급가액외화단위
              qryTaxItemVB_AMTC.AsString := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('USAMT_G').AsString;
            //--------------------------------------------------------------------
            //공급가액외화
              qryTaxItemVB_AMT.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('USAMT').AsCurrency;
            //--------------------------------------------------------------------
            //공급가액외화소계 단위
              qryTaxItemVB_TOTAMTC.AsString := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('USSTAMT_G').AsString;
            //--------------------------------------------------------------------
            //공급가액외화소계
              qryTaxItemVB_TOTAMT.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('USSTAMT').AsCurrency;
            //--------------------------------------------------------------------
            //세액통화
              qryTaxItemVB_TAXC.AsString := 'KRW';
            //--------------------------------------------------------------------
            //세액
              qryTaxItemVB_TAX.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('TAXAMT').AsCurrency;
            //--------------------------------------------------------------------
            //세액소계통화
              qryTaxItemVB_TOTTAXC.AsString := 'KRW';
            //--------------------------------------------------------------------
            //세액소계
              qryTaxItemVB_TOTTAX.AsCurrency := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('TAXSTAMT').AsCurrency;
            //--------------------------------------------------------------------
            //참조사항
              try
                bigoMemo := TStringList.Create;
                bigoMemo.Text := Dialog_AttachFromVATBI2_frm.sDBGrid2.DataSource.DataSet.FieldByName('DE_REM1').AsString;
                if bigoMemo.Text <> '' then
                begin
                  for i := 0 to bigoMemo.Count-1 do
                  begin
                    qryTaxItem.FieldByName('BIGO'+IntToStr(i+1)).AsString := bigoMemo[i];
                  end;
                end;
              finally
                bigoMemo.Free;
              end;

          qryTaxItem.Post;

          Dialog_AttachFromVATBI2_frm.qryGoods.Next;
        end;

        TaxItemReadDocument;
        //버튼 및 품목내역
        TaxItemButtonEnable(True);
        EnabledControlValue(taxPanel3 , True);
      end;
    end;
  finally
    FreeAndNil(Dialog_AttachFromVATBI2_frm);
  end;
end;

procedure TUI_APPSPC_frm.PopMenuEnabled(Sender: TObject);
begin
  inherited;
//  ShowMessage(IntToStr((Sender as TPopupMenu).tag));
  case (Sender as TPopupMenu).Tag of
    //물품수령증명서
    0:
      begin
        N1.Enabled :=  ProgramControlType in [ctInsert , ctModify];
        N2.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryGoods.RecordCount > 0);
        N3.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryGoods.RecordCount > 0);
      end;
    //세금계산서
    1:
      begin
        MenuItem1.Enabled :=  ProgramControlType in [ctInsert , ctModify];
        MenuItem2.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryTax.RecordCount > 0);
        MenuItem3.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryTax.RecordCount > 0);
      end;
    //상업송장
    2:
      begin
        MenuItem4.Enabled :=  ProgramControlType in [ctInsert , ctModify];
        MenuItem5.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryInv.RecordCount > 0);
        MenuItem6.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryInv.RecordCount > 0);
      end;
    //물품수령증명서 품목내역
    3:
      begin
        MenuItem7.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryGoods.State in [dsEdit,dsInsert]);
        MenuItem8.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryGoodsItem.RecordCount > 0);
        MenuItem9.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryGoodsItem.RecordCount > 0);
      end;
    //세금계산서 품목내역
    4:
      begin
        MenuItem10.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryTax.State in [dsEdit,dsInsert]);
        MenuItem11.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryTaxItem.RecordCount > 0);
        MenuItem12.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryTaxItem.RecordCount > 0);
      end;
    //상업송장 품목내역
    5:
      begin
        MenuItem13.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryInv.State in [dsEdit,dsInsert]);
        MenuItem14.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryInvItem.RecordCount > 0);
        MenuItem15.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryInvItem.RecordCount > 0);
      end;
  end;
end;

procedure TUI_APPSPC_frm.PopMenuClick(Sender: TObject);
begin
  inherited;

  case (Sender as TMenuItem).Tag of
    //물품수령증명서
    0 : btn_goodsNewClick(btn_goodsNew);
    1 : btn_goodsNewClick(btn_goodsMod);
    2 : btn_goodsNewClick(btn_goodsDel);

    //물품수령증명서 품목내역
    3 : btn_goodsItemNewClick(btn_goodsItemNew);
    4 : btn_goodsItemNewClick(btn_goodsItemMod);
    5 : btn_goodsItemNewClick(btn_goodsItemDel);

    //세금계산서
    6: btn_taxNewClick(btn_taxNew);
    7: btn_taxNewClick(btn_taxMod);
    8: btn_taxNewClick(btn_taxDel);

    //세금계산서 품목내역
    9: btn_taxItemNewClick(btn_taxItemNew);
    10: btn_taxItemNewClick(btn_taxItemMod);
    11: btn_taxItemNewClick(btn_taxItemDel);

    //상업송장
    12: btn_invNewClick(btn_invNew);
    13: btn_invNewClick(btn_invMod);
    14: btn_invNewClick(btn_invDel);

    //세금계산서 품목내역
    15: btn_invItemNewClick(btn_invItemNew);
    16: btn_invItemNewClick(btn_invItemMod);
    17: btn_invItemNewClick(btn_invItemDel);
  end;
end;

procedure TUI_APPSPC_frm.qryListBGM_GUBUNGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  if Trim(qryListBGM_GUBUN.AsString) = '2BK' then
    Text := '추심'
  else if Trim(qryListBGM_GUBUN.AsString) = '2BJ' then
    Text := '매입';
end;

procedure TUI_APPSPC_frm.Page_controlChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  //해당 페이지의 문서를 작업중일경우 이동이 불가능함.
  if (qryList.State in [dsInsert , dsEdit]) and
     ((qryGoods.State in [dsInsert , dsEdit]) or (qryGoodsItem.State in [dsInsert , dsEdit]) or
     (qryTax.State in [dsInsert , dsEdit]) or (qryTaxItem.State in [dsInsert , dsEdit]) or
     (qryInv.State in [dsInsert , dsEdit]) or (qryInvItem.State in [dsInsert , dsEdit])) then
  begin
    MessageBox(Self.Handle,'문서가 저장되지 않았습니다.','저장확인',MB_OK+MB_ICONINFORMATION);
    AllowChange := False;
  end;
end;

procedure TUI_APPSPC_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  APPSPC_PRINT_frm := TAPPSPC_PRINT_frm.Create(Self);
  try
    APPSPC_PRINT_frm.MaintNo := edt_MAINT_NO.Text;
    QRCompositeReport1.Preview;
  finally
    FreeAndNil(APPSPC_PRINT_frm);
  end;
end;

procedure TUI_APPSPC_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(APPSPC_PRINT_frm);
end;

procedure TUI_APPSPC_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  //
end;

procedure TUI_APPSPC_frm.sBitBtn65Click(Sender: TObject);
begin
  //문서정보 - 구매자
  inherited;
  Dialog_SearchCustom_frm := TDialog_SearchCustom_frm.Create(Self);
  Dialog_SearchCustom_frm.Caption := '업체정보';
  try
    IF Dialog_SearchCustom_frm.openDialog() = mrOK then
    begin
      //사업자 등록번호
      mask_DFSAUPNO.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('SAUP_NO').AsString;
      //상호
      edt_DFNAME1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
      edt_DFNAME2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
      //이메일
      edt_DFEMAIL1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('EMAIL_ID').AsString;
      edt_DFEMAIL2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('EMAIL_DOMAIN').AsString;
    end;  
  finally
    FreeAndNil(Dialog_SearchCustom_frm);
  end;

end;

procedure TUI_APPSPC_frm.btnCopyClick(Sender: TObject);
var
  copyDocNo , newDocNo : String;
begin
  inherited;
    if qryList.RecordCount = 0 then Exit;

    Dialog_CopyAPPSPC_frm := TDialog_CopyAPPSPC_frm.Create(Self);
      try
          newDocNo := DMAutoNo.GetDocumentNoAutoInc('APPSPC');
          copyDocNo :=Dialog_CopyAPPSPC_frm.openDialog;

          //ShowMessage(copyDocNo);

          IF Trim(copyDocNo) = '' then Exit;

          //------------------------------------------------------------------------------
          // COPY
          //------------------------------------------------------------------------------
          with spCopyAPPSPC do
          begin
            Close;
            Parameters.ParamByName('@CopyDocNo').Value := copyDocNo;
            Parameters.ParamByName('@NewDocNo').Value :=  newDocNo;
            Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

            if not DMMssql.KISConnect.InTransaction then
              DMMssql.KISConnect.BeginTrans;

            try
              ExecProc;
            except
              on e:Exception do
              begin
                DMMssql.KISConnect.RollbackTrans;
                MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
              end;
            end;

          end;
        // ReadListBetween(해당년도 1월1일 , 오늘날짜 , 새로복사 된  관리번호)
        if  ReadList(FormatDateTime('YYYYMMDD', StartOfTheYear(Now)),FormatDateTime('YYYYMMDD',Now),newDocNo) Then
        begin

          EditDocument;
          edt_MAINT_NO.Text := newDocNo;

        end
        else
          raise Exception.Create('복사한 데이터를 찾을수 없습니다');

      finally

        FreeAndNil(Dialog_CopyAPPSPC_frm);

      end;

end;

procedure TUI_APPSPC_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_APPSPC_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;
  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := APPSPC(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'APPSPC';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APPSPC_H';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        Readlist(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_APPSPC_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_APPSPC_frm.FormActivate(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;

  if not DMMssql.KISConnect.InTransaction then
  begin
    qryList.Close;
    qryList.Open;
  end;
end;

procedure TUI_APPSPC_frm.qryListCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var                                                         
  nIndex : integer;  
begin
  inherited;

  IF Length(qryListCHK3.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(qryListCHK3.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;
end;

procedure TUI_APPSPC_frm.DialogKey(var msg: TCMDialogKey);
begin
  if ActiveControl = nil then Exit;

  if (ActiveControl.Name = 'edt_LABANKBUP') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
  begin
    Page_control.ActivePageIndex := 1;
    Self.KeyPreview := False;
  end
  else
  begin
    Self.KeyPreview := True;
    inherited;
  end;
end;

end.
