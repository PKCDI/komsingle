unit UI_APPLOG_BP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, ComCtrls, sPageControl, Buttons,
  sBitBtn, Mask, sMaskEdit, StdCtrls, sEdit, ExtCtrls, sSplitter, sButton,
  sLabel, sSpeedButton, sPanel, sCustomComboEdit, sCurrEdit, sCurrencyEdit,
  sComboBox, Grids, DBGrids, acDBGrid, TypeDefine;

type
  TUI_APPLOG_BP_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sSpeedButton9: TsSpeedButton;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSpeedButton11: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sSplitter12: TsSplitter;
    sPanel4: TsPanel;
    edt_MAINT_NO: TsEdit;
    mask_DATEE: TsMaskEdit;
    edt_USER_ID: TsEdit;
    btn_Cal: TsBitBtn;
    edt_msg2: TsEdit;
    sBitBtn19: TsBitBtn;
    edt_msg3: TsEdit;
    sBitBtn20: TsBitBtn;
    sSplitter1: TsSplitter;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sSplitter3: TsSplitter;
    sTabSheet3: TsTabSheet;
    sPanel2: TsPanel;
    sSplitter4: TsSplitter;
    sPanel3: TsPanel;
    sPanel7: TsPanel;
    edt_msg1: TsEdit;
    sBitBtn6: TsBitBtn;
    edt_msg1Name: TsEdit;
    edt_LCG: TsEdit;
    sBitBtn1: TsBitBtn;
    edt_LCGNAME: TsEdit;
    edt_LCNO: TsEdit;
    edt_BLG: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_BLGNAME: TsEdit;
    edt_BLNO: TsEdit;
    sPanel8: TsPanel;
    mask_BLDATE: TsMaskEdit;
    sBitBtn16: TsBitBtn;
    mask_APPDATE: TsMaskEdit;
    sBitBtn3: TsBitBtn;
    mask_ARDATE: TsMaskEdit;
    sBitBtn4: TsBitBtn;
    edt_CARRIER1: TsEdit;
    sPanel9: TsPanel;
    edt_CRNAME1: TsEdit;
    edt_CRNAME2: TsEdit;
    edt_CRNAME3: TsEdit;
    sBitBtn5: TsBitBtn;
    edt_CRCODENAME3: TsEdit;
    edt_B5NAME1: TsEdit;
    edt_B5NAME2: TsEdit;
    edt_B5NAME3: TsEdit;
    sPanel11: TsPanel;
    edt_SENAME1: TsEdit;
    edt_SENAME2: TsEdit;
    edt_SENAME3: TsEdit;
    edt_CNNAME1: TsEdit;
    edt_CNNAME2: TsEdit;
    edt_CNNAME3: TsEdit;
    sPanel10: TsPanel;
    edt_LOADLOC: TsEdit;
    sBitBtn7: TsBitBtn;
    edt_LOADLOCNAME: TsEdit;
    edt_LOADTXT: TsEdit;
    edt_ARRLOC: TsEdit;
    sBitBtn8: TsBitBtn;
    edt_ARRLOCNAME: TsEdit;
    edt_ARRTXT: TsEdit;
    edt_INVAMTC: TsEdit;
    sBitBtn9: TsBitBtn;
    curr_INVAMT: TsCurrencyEdit;
    edt_PACQTYC: TsEdit;
    sBitBtn10: TsBitBtn;
    curr_PACQTY: TsCurrencyEdit;
    sSpeedButton1: TsSpeedButton;
    sSplitter2: TsSplitter;
    sPanel6: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn13: TsBitBtn;
    sBitBtn14: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn28: TsBitBtn;
    sPanel15: TsPanel;
    sDBGrid1: TsDBGrid;
    edt_CARRIER2: TsEdit;
    sTabSheet2: TsTabSheet;
    sPanel16: TsPanel;
    sSplitter5: TsSplitter;
    sSplitter6: TsSplitter;
    sPanel17: TsPanel;
    sPanel18: TsPanel;
    sPanel53: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    sDBGrid2: TsDBGrid;
    sPanel12: TsPanel;
    edt_SPMARK1: TsEdit;
    edt_SPMARK2: TsEdit;
    edt_SPMARK3: TsEdit;
    edt_SPMARK5: TsEdit;
    edt_SPMARK4: TsEdit;
    edt_SPMARK7: TsEdit;
    edt_SPMARK6: TsEdit;
    edt_SPMARK9: TsEdit;
    edt_SPMARK8: TsEdit;
    edt_SPMARK10: TsEdit;
    sPanel13: TsPanel;
    edt_GOODS1: TsEdit;
    edt_GOODS2: TsEdit;
    edt_GOODS3: TsEdit;
    edt_GOODS4: TsEdit;
    edt_GOODS5: TsEdit;
    sPanel14: TsPanel;
    edt_MSNAME1: TsEdit;
    edt_MSNAME2: TsEdit;
    edt_MSNAME3: TsEdit;
    edt_AXNAME1: TsEdit;
    edt_AXNAME2: TsEdit;
    edt_AXNAME3: TsEdit;
    sPanel5: TsPanel;
    edt_TRMPAYC: TsEdit;
    sBitBtn11: TsBitBtn;
    edt_TRMPAYCNAME: TsEdit;
    edt_TRMPAY: TsEdit;
    edt_BANKCD: TsEdit;
    sBitBtn12: TsBitBtn;
    edt_BANKTXT: TsEdit;
    edt_BANKBR: TsEdit;
    edt_GOODS6: TsEdit;
    edt_GOODS7: TsEdit;
    edt_GOODS8: TsEdit;
    edt_GOODS9: TsEdit;
    edt_GOODS10: TsEdit;
    sSpeedButton7: TsSpeedButton;
    procedure btnExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure edt_CodeClickEvent(Sender: TObject);
    procedure edt_BANKCDDblClick(Sender: TObject);
    procedure btn_CodeClickEvent(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    ProgramControlType : TProgramControlType;
    procedure ReadDocument; virtual; abstract;
    procedure ButtonEnable(val: Boolean);
    procedure DeleteDocumnet; virtual; abstract;

  end;

var
  UI_APPLOG_BP_frm: TUI_APPLOG_BP_frm;

implementation

{$R *.dfm}

uses Dateutils, commonlib, strUtils, KISCalendar, Dialog_CodeList,
  Dialog_BANK;

procedure TUI_APPLOG_BP_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_APPLOG_BP_frm.FormShow(Sender: TObject);
begin
  inherited;
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD' , StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD' , Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD' , StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD' , Now);
  sPageControl1.ActivePageIndex := 0;

  ButtonEnable(True);

end;

procedure TUI_APPLOG_BP_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;

end;

procedure TUI_APPLOG_BP_frm.edt_CodeClickEvent(Sender: TObject);
begin
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      if Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
      begin
          //선택된 코드 출력
          (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;

          //선택된 코드의 값까지 출력
          case (Sender as TsEdit).Tag of
              //운송유형
              101 : edt_msg1Name.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              //신용장(계약서)
              102 : edt_LCGNAME.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              //선하증권 or 항공화물운송장
              103 : edt_BLGNAME.Text  := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              //선박회사
              104 : edt_CRCODENAME3.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              //선적항
              105 : edt_LOADLOCNAME.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              //도착항
              106 : edt_ARRLOCNAME.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              //결제구분 및 기간
              107 : edt_TRMPAYCNAME.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;              
          end;
      end;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;
end;

procedure TUI_APPLOG_BP_frm.edt_BANKCDDblClick(Sender: TObject);
begin
  inherited;
    Dialog_BANK_frm := TDialog_BANK_frm.Create(Self);
  try
    If Dialog_BANK_frm.openDialog(edt_BANKCD.Text) = mrOk Then
    begin
      edt_BANKCD.Text := Dialog_BANK_frm.qryBank.FieldByName('CODE').AsString;
      edt_BANKTXT.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKNAME1').AsString + ' '
                          + Dialog_BANK_frm.qryBank.FieldByName('BANKNAME2').AsString;
      edt_BANKBR.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKBRANCH1').AsString + ' '
                          + Dialog_BANK_frm.qryBank.FieldByName('BANKBRANCH2').AsString;   
    end;
  finally
    FreeAndNil(Dialog_BANK_frm);
  end;
end;

procedure TUI_APPLOG_BP_frm.btn_CodeClickEvent(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of  
    1 : edt_CodeClickEvent(edt_msg1);
    2 : edt_CodeClickEvent(edt_LCG);
    3 : edt_CodeClickEvent(edt_BLG);
    4 : edt_CodeClickEvent(edt_INVAMTC);
    5 : edt_CodeClickEvent(edt_PACQTYC);
    6 : edt_CodeClickEvent(edt_CRNAME3);
    7 : edt_CodeClickEvent(edt_LOADLOC);
    8 : edt_CodeClickEvent(edt_ARRLOC);
    9 : edt_CodeClickEvent(edt_TRMPAYC);
    10 : edt_BANKCDDblClick(edt_BANKCD);
    11 : edt_CodeClickEvent(edt_msg2);
    12 : edt_CodeClickEvent(edt_msg3);
  end;

end;

procedure TUI_APPLOG_BP_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel15.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn14.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn28.Visible := com_SearchKeyword.ItemIndex = 0;

  IF edt_SearchText.Visible Then
    edt_SearchText.Clear;
end;

procedure TUI_APPLOG_BP_frm.ButtonEnable(val: Boolean);
begin
  btnNew.Enabled := Val;
  btnEdit.Enabled := Val;
  btnDel.Enabled := Val;

  btnTemp.Enabled := not Val;
  btnSave.Enabled := not Val;
  btnCancel.Enabled := not Val;

  btnCopy.Enabled := Val;
  btnPrint.Enabled := Val;

  btnReady.Enabled := Val;
  btnSend.Enabled := Val;
end;

procedure TUI_APPLOG_BP_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel18.Visible := not (sPageControl1.ActivePageIndex = 2);
end;

end.
