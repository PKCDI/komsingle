unit UI_LOCRC2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_LOCRC2_BP, sSkinProvider, StdCtrls, sMemo, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, Grids, DBGrids, acDBGrid, Buttons, sBitBtn,
  Mask, sMaskEdit, sComboBox, sEdit, ComCtrls, sPageControl, sButton,
  sSpeedButton, ExtCtrls, sPanel, sSplitter, DB, ADODB, StrUtils, sLabel, TypeDefine,
  QuickRpt, Menus, QRCtrls, sDialogs;

type
  TUI_LOCRC2_frm = class(TUI_LOCRC2_BP_frm)
    qrylist: TADOQuery;
    dslist: TDataSource;
    qryGoods: TADOQuery;
    dsGoods: TDataSource;
    qrylistMAINT_NO: TStringField;
    qrylistUSER_ID: TStringField;
    qrylistDATEE: TStringField;
    qrylistMESSAGE1: TStringField;
    qrylistMESSAGE2: TStringField;
    qrylistRFF_NO: TStringField;
    qrylistGET_DAT: TStringField;
    qrylistISS_DAT: TStringField;
    qrylistEXP_DAT: TStringField;
    qrylistBENEFC: TStringField;
    qrylistBENEFC1: TStringField;
    qrylistAPPLIC: TStringField;
    qrylistAPPLIC1: TStringField;
    qrylistAPPLIC2: TStringField;
    qrylistAPPLIC3: TStringField;
    qrylistAPPLIC4: TStringField;
    qrylistAPPLIC5: TStringField;
    qrylistAPPLIC6: TStringField;
    qrylistRCT_AMT1: TBCDField;
    qrylistRCT_AMT1C: TStringField;
    qrylistRCT_AMT2: TBCDField;
    qrylistRCT_AMT2C: TStringField;
    qrylistRATE: TBCDField;
    qrylistREMARK: TStringField;
    qrylistREMARK1: TMemoField;
    qrylistTQTY: TBCDField;
    qrylistTQTY_G: TStringField;
    qrylistTAMT: TBCDField;
    qrylistTAMT_G: TStringField;
    qrylistLOC_NO: TStringField;
    qrylistAP_BANK: TStringField;
    qrylistAP_BANK1: TStringField;
    qrylistAP_BANK2: TStringField;
    qrylistAP_BANK3: TStringField;
    qrylistAP_BANK4: TStringField;
    qrylistAP_NAME: TStringField;
    qrylistLOC1AMT: TBCDField;
    qrylistLOC1AMTC: TStringField;
    qrylistLOC2AMT: TBCDField;
    qrylistLOC2AMTC: TStringField;
    qrylistEX_RATE: TBCDField;
    qrylistLOADDATE: TStringField;
    qrylistEXPDATE: TStringField;
    qrylistLOC_REM: TStringField;
    qrylistLOC_REM1: TMemoField;
    qrylistCHK1: TBooleanField;
    qrylistCHK2: TStringField;
    qrylistCHK3: TStringField;
    qrylistPRNO: TIntegerField;
    qrylistAMAINT_NO: TStringField;
    qrylistBSN_HSCODE: TStringField;
    qrylistBENEFC2: TStringField;
    qrylistAPPLIC7: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TIntegerField;
    qryGoodsHS_NO: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsAMT: TBCDField;
    qryGoodsAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsSTAMT: TBCDField;
    qryGoodsSTAMT_G: TStringField;
    qryGoodsASEQ: TIntegerField;
    qryGoodsAMAINT_NO: TStringField;
    qryTax: TADOQuery;
    dsTax: TDataSource;
    sPanel31: TsPanel;
    sBitBtn16: TsBitBtn;
    sBitBtn17: TsBitBtn;
    sBitBtn18: TsBitBtn;
    qryTaxSEQ: TBCDField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    sSplitter8: TsSplitter;
    qryMAX_SEQ: TADOQuery;
    qryTaxKEYY: TStringField;
    QRCompositeReport1: TQRCompositeReport;
    spCopyLOCRC2: TADOStoredProc;
    goodsMenu: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    taxMenu: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    sSplitter10: TsSplitter;
    sLabel6: TsLabel;
    sLabel1: TsLabel;
    sSplitter12: TsSplitter;
    sSpeedButton9: TsSpeedButton;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSpeedButton11: TsSpeedButton;
    qryReady: TADOQuery;
    popMenu1: TPopupMenu;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    N4: TMenuItem;
    N7: TMenuItem;
    d1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    excelOpen: TsOpenDialog;
    sSpeedButton13: TsSpeedButton;
    taxSampleBtn: TsSpeedButton;
    procedure qryGoodsNAME1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryGoodsSIZE1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qrylistCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qrylistCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qrylistAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure edt_BENEFCDblClick(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure edt_LOC1AMTCDblClick(Sender: TObject);
    procedure sBitBtn8Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure mask_DATEEDblClick(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
    procedure qrylistAfterOpen(DataSet: TDataSet);
    procedure qryGoodsAfterScroll(DataSet: TDataSet);
    procedure Btn_GoodsNewMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Btn_GoodsNewMouseLeave(Sender: TObject);
    procedure Btn_GoodsNewClick(Sender: TObject);
    procedure Btn_GoodsCancelClick(Sender: TObject);
    procedure Btn_GoodsEditClick(Sender: TObject);
    procedure Btn_GoodsDelClick(Sender: TObject);
    procedure qryGoodsAfterInsert(DataSet: TDataSet);
    procedure qryGoodsAfterCancel(DataSet: TDataSet);
    procedure edt_PRICE_GChange(Sender: TObject);
    procedure qryTaxAfterScroll(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure btn_GoodsCalcClick(Sender: TObject);
    procedure curr_PRICEChange(Sender: TObject);
    procedure Btn_GoodsOKClick(Sender: TObject);
    procedure edt_AP_BANKDblClick(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btn_TaxNewClick(Sender: TObject);
    procedure qryTaxAfterInsert(DataSet: TDataSet);
    procedure qryTaxAfterCancel(DataSet: TDataSet);
    procedure btnEditClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btn_TOTAL_CALCClick(Sender: TObject);
    procedure edt_NAME_CODDblClick(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure MenuItem1Click(Sender: TObject);
    procedure memo_REMARK1Change(Sender: TObject);
    procedure memoLineLimit(Sender: TObject; var Key: Char);
    procedure popMenuEnabled(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure sSpeedButton18Click(Sender: TObject);
    procedure sSpeedButton13Click(Sender: TObject);
    procedure taxExcelBtnClick(Sender: TObject);
    procedure taxSampleBtnClick(Sender: TObject);
  private
    { Private declarations }
//    ProgramControlType : TProgramControlType;
    procedure NewDocument;
    procedure NewDocumentFromLOCAPP;
    procedure CancelDocument;
    procedure GoodsButton(Val :Boolean=True);
    procedure TaxButton(Val :Boolean=True);
    function AutoDocNo(onlyReturn: Boolean): String;
    procedure GoodsSaveDocument; //상품명세 저장
    procedure TaxSaveDocument; //세금계산서 저장
    procedure SaveDocument(Sender : TObject); //전체저장
    function MAX_SEQ(TableName : String):Integer;//마지막순번
    procedure EditDocument;
    procedure DeleteDocument;

    function CHECK_VALIDITY:String;
    function ReadListBetween(fromDate: String; toDate: String; KeyValue : string): Boolean;
    
    procedure ReadyDocument(DocNo : String);

    //마지막 콤포넌트에서 탭이나 엔터키 누르면 다음페이지로 이동하는 프로시져
    procedure DialogKey(var msg : TCMDialogKey); message CM_DIALOGKEY;

  protected
    procedure ReadDocument; override;
    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;
    function ReadList(OrderString: String; KeyValue: String = ''):Boolean; overload;
    procedure ReadGoods; override;
    procedure ReadTax; override;
  public

    { Public declarations }
  end;

var
  UI_LOCRC2_frm: TUI_LOCRC2_frm;

implementation

uses MSSQL, Commonlib, Dialog_SearchCustom, Dialog_BANK, Dialog_CodeList,
  AutoNo, CodeContents, KISCalendar, VarDefine, MessageDefine, SQLCreator, Dlg_ErrorMessage,
  Dialog_ItemList, LOCRC2_PRINT, LOCRC2_PRINT2, Dialog_CopyLOCRC2, DateUtils,
  DocumentSend, Dlg_RecvSelect, CreateDocuments , ComObj, excel2000;

{$R *.dfm}

function TUI_LOCRC2_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  nYear,nMonth,nDay : Word;

begin
  ErrMsg := TStringList.Create;
  Try

    IF Trim(edt_BSN_HSCODE.Text) = '' THEN
      ErrMsg.Add('[문서내용] 대표공급물품HS부호를 입력하세요.');

    IF Trim(edt_BENEFC.Text) = '' THEN
      ErrMsg.Add('[문서내용] 수혜자를 입력해야합니다.');

    IF Trim(edt_BENEFC2.Text) = '' THEN
      ErrMsg.Add('[문서내용] 수혜자 사업자등록번호를 입력해야합니다.');

    IF Trim(edt_APPLIC.Text) = '' THEN
      ErrMsg.Add('[문서내용] 개설업체를 입력해야합니다.');

    IF Trim(edt_APPLIC7.Text) = '' THEN
      ErrMsg.Add('[문서내용] 개설업체 사업자등록번호를 입력해야합니다.');

    IF Trim(edt_APPLIC2.Text) = '' THEN
      ErrMsg.Add('[문서내용] 명의인을 입력해야합니다.');

    IF Trim(edt_AP_BANK.Text) = '' THEN
      ErrMsg.Add('[문서내용] 개설은행을 입력해야합니다.');

    IF Trim(edt_AP_BANK3.Text) = '' THEN
      ErrMsg.Add('[문서내용] 은행(부)지점명을 입력해야합니다.');

    IF Trim(edt_LOC_NO.Text) = '' THEN
      ErrMsg.Add('[문서내용] 내국신용장번호를 입력해야합니다.');

    IF (Trim(edt_LOC1AMTC.Text) = '') or (Trim(curr_LOC1AMT.Text) = '0') THEN
      ErrMsg.Add('[문서내용] 개설금액(외화)을 입력해야합니다.');

    IF Trim(curr_LOC2AMT.Text) = '0' THEN
      ErrMsg.Add('[문서내용] 개설금액(원화)을 입력해야합니다.');

    IF Trim(mask_LOADDATE.Text) = '' THEN
      ErrMsg.Add('[문서내용] 물품인도기일을 입력해야합니다.');

    IF Trim(mask_EXPDATE.Text) = '' THEN
      ErrMsg.Add('[문서내용] 유효일자를 입력해야합니다.');

    IF (Trim(edt_RCT_AMT1C.Text) = '') or (Trim(curr_RCT_AMT1.Text) = '0') THEN
      ErrMsg.Add('[문서내용] 인수금액(외화)을 입력해야합니다.');

    IF Trim(curr_RCT_AMT2.Text) = '0' THEN
      ErrMsg.Add('[문서내용] 인수금액(원화)을 입력해야합니다.');

    IF qryGoods.RecordCount = 0 then
      ErrMsg.Add('[상품내역] 제품을 등록해야 합니다.');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

procedure TUI_LOCRC2_frm.qryGoodsNAME1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  IF qryGoods.RecordCount > 0 Then
    Text := qryGoodsNAME1.AsString;
end;

procedure TUI_LOCRC2_frm.qryGoodsSIZE1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  IF qryGoods.RecordCount > 0 Then
    Text := qryGoodsSIZE1.AsString;
end;

procedure TUI_LOCRC2_frm.qrylistCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;
begin
  inherited;
  nIndex := StrToIntDef( qryListCHK2.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TUI_LOCRC2_frm.qrylistCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;  
begin
  inherited;

  IF Length(qryListCHK3.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(qryListCHK3.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;
end;

procedure TUI_LOCRC2_frm.ReadDocument;
begin
  inherited;
  IF not qrylist.Active Then ClearControlValue(sPanel9);
  IF qrylist.RecordCount = 0 Then ClearControlValue(sPanel9);

  edt_MAINT_NO.Text := qrylistMAINT_NO.AsString;
  mask_DATEE.Text := qrylistDATEE.AsString;
  edt_USER_ID.Text := qrylistUSER_ID.AsString;
  edt_msg1.Text := qrylistMESSAGE1.AsString;
  edt_msg2.Text := qrylistMESSAGE2.AsString;
//------------------------------------------------------------------------------
// 기본입력사항
//------------------------------------------------------------------------------
  //인수일자
  mask_GET_DAT.Text := qrylistGET_DAT.AsString;
  //발급일자
  mask_ISS_DAT.Text := qrylistISS_DAT.AsString;
  //문서유효일자
  mask_EXP_DAT.Text := qrylistEXP_DAT.AsString;
  //발급번호
  edt_RFF_NO.Text := qrylistRFF_NO.AsString;
  //대표공급물품 HS부호
  edt_BSN_HSCODE.Text := qrylistBSN_HSCODE.AsString;
//------------------------------------------------------------------------------
// 수혜업체/개설업체
//------------------------------------------------------------------------------
  //수혜자
  edt_BENEFC.Text := qrylistBENEFC.AsString;
  edt_BENEFC1.Text := qrylistBENEFC1.AsString;
  edt_BENEFC2.Text := qrylistBENEFC2.AsString;
  //개설업체
  //코드
  edt_APPLIC.Text  := qrylistAPPLIC.AsString;
  //상호
  edt_APPLIC1.Text := qrylistAPPLIC1.AsString;
  //명의인
  edt_APPLIC2.Text := qrylistAPPLIC2.AsString;
  //전자서명
  edt_APPLIC3.Text := qrylistAPPLIC3.AsString;
  //주소1
  edt_APPLIC4.Text := qrylistAPPLIC4.AsString;
  //주소2
  edt_APPLIC5.Text := qrylistAPPLIC5.AsString;
  //주소3
  edt_APPLIC6.Text := qrylistAPPLIC6.AsString;
  //사업자등록번호
  edt_APPLIC7.Text := qrylistAPPLIC7.AsString;
//------------------------------------------------------------------------------
// 개설은행
//------------------------------------------------------------------------------
  //개설은행
  edt_AP_BANK.Text := qrylistAP_BANK.AsString;
  edt_AP_BANK1.Text := qrylistAP_BANK1.AsString;
  edt_AP_BANK2.Text := qrylistAP_BANK2.AsString;
  //지점명
  edt_AP_BANK3.Text := qrylistAP_BANK3.AsString;
  edt_AP_BANK4.Text := qrylistAP_BANK4.AsString;
  //전자서명
  edt_AP_NAME.Text := qrylistAP_NAME.AsString;
//------------------------------------------------------------------------------
// 내국신용장
//------------------------------------------------------------------------------
  //내국신용장 번호
  edt_LOC_NO.Text := qrylistLOC_NO.AsString;
  //개설금액(외화)
  edt_LOC1AMTC.Text := qrylistLOC1AMTC.AsString;
  curr_LOC1AMT.Value := qrylistLOC1AMT.AsCurrency;
  //매매기준율
  edt_EX_RATE.Text := qrylistEX_RATE.AsString;
  //개설금액(원화)
  curr_LOC2AMT.Value := qrylistLOC2AMT.AsCurrency;
  //물품인도기일
  mask_LOADDATE.Text := qrylistLOADDATE.AsString;
  //유효일자
  mask_EXPDATE.Text := qrylistEXPDATE.AsString;
  //신용장관리번호
  edt_AMAINT_NO.Text := qrylistAMAINT_NO.AsString;
//------------------------------------------------------------------------------
// 인수금액
//------------------------------------------------------------------------------
  //인수금액(외화)
  edt_RCT_AMT1C.Text := qrylistRCT_AMT1C.AsString;
  curr_RCT_AMT1.Value := qrylistRCT_AMT1.AsCurrency;
  //환율
  edt_RATE.Text := qrylistRATE.AsString;
  //인수금액(원화)
  edt_RCT_AMT2C.Text := qrylistRCT_AMT2C.AsString;
  curr_RCT_AMT2.Value := qrylistRCT_AMT2.AsCurrency;
//------------------------------------------------------------------------------
// 총합계
//------------------------------------------------------------------------------
  //총합계수량
  edt_TQTY_G.Text := qrylistTQTY_G.AsString;
  curr_TQTY.Value := qrylistTQTY.AsCurrency;
  //총합계금액
  edt_TAMT_G.Text := qrylistTAMT_G.AsString;
  curr_TAMT.Value := qrylistTAMT.AsCurrency;
//------------------------------------------------------------------------------
// 기타조건/참고사항
//------------------------------------------------------------------------------
  //기타조건
  memo_REMARK1.Lines.Text := qrylistREMARK1.AsString;
  //참고사항
  memo_LOC_REM1.Lines.Text := qrylistLOC_REM1.AsString;
end;

procedure TUI_LOCRC2_frm.qrylistAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadDocument;
//------------------------------------------------------------------------------
// 상품명세내역 OPEN
//------------------------------------------------------------------------------
  ClearControlValue(sPanel21); 
  with qryGoods do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := qrylistMAINT_NO.AsString;
    Open;
  end;
//------------------------------------------------------------------------------
// 세금계산서 내역 OPEN
//------------------------------------------------------------------------------
  ClearControlValue(sPanel28);
  with qryTax do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := qrylistMAINT_NO.AsString;
    Open;
  end;

//메모 행열 출력
  sLabel6.Caption := '';
  sLabel1.Caption := '';
end;

procedure TUI_LOCRC2_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qrylist.SQL.Text;
end;

procedure TUI_LOCRC2_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_LOCRC2_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  ReadList(Mask_fromDate.Text,Mask_toDate.Text,'');
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := Mask_toDate.Text;
  
end;

function TUI_LOCRC2_frm.ReadList(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
  Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('ORDER BY DATEE ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

function TUI_LOCRC2_frm.ReadList(OrderString, KeyValue: String): Boolean;
begin

end;

procedure TUI_LOCRC2_frm.edt_BENEFCDblClick(Sender: TObject);
begin
  inherited;
  //If (Sender as TsEdit).ReadOnly Then Exit;
  if not (Sender as TsEdit).Enabled then Exit;

  Dialog_SearchCustom_frm := TDialog_SearchCustom_frm.Create(Application);
  try
    IF Dialog_SearchCustom_frm.openDialog((Sender as TsEdit).Text) = mrOk then
    begin
      IF (Sender as TsEdit).Tag = 0 Then
      begin
        edt_BENEFC.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('CODE').AsString;
        edt_BENEFC1.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ENAME').AsString;
        edt_BENEFC2.Text := Dialog_SearchCustom_frm.qryList.FieldByName('SAUP_NO').AsString;
      end
      else
      IF (Sender as TsEdit).Tag = 1 Then
      begin
        edt_APPLIC.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('CODE').AsString;                           
        edt_APPLIC1.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ENAME').AsString;
        edt_APPLIC2.Text := Dialog_SearchCustom_frm.qryList.FieldByName('REP_NAME').AsString;
        edt_APPLIC4.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR1').AsString;
        edt_APPLIC5.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR2').AsString;
        edt_APPLIC6.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR3').AsString;
        edt_APPLIC7.Text := Dialog_SearchCustom_frm.qryList.FieldByName('SAUP_NO').AsString;
        edt_APPLIC3.Text := Dialog_SearchCustom_frm.qryList.FieldByName('Jenja').AsString;
      end
    end;
  finally
    FreeAndNil(Dialog_SearchCustom_frm);
  end;
end;
procedure TUI_LOCRC2_frm.sBitBtn5Click(Sender: TObject);
begin
  inherited;
  Case (Sender as TsBitBtn).Tag of
    0: edt_BENEFCDblClick(edt_BENEFC);
    1: edt_BENEFCDblClick(edt_APPLIC);
  end;
end;

procedure TUI_LOCRC2_frm.edt_AP_BANKDblClick(Sender: TObject);
begin
  inherited;
    Dialog_BANK_frm := TDialog_BANK_frm.Create(Self);
  try
    If Dialog_BANK_frm.openDialog(edt_AP_BANK.Text) = mrOk Then
    begin
      edt_AP_BANK.Text := Dialog_BANK_frm.qryBank.FieldByName('CODE').AsString;
      edt_AP_BANK1.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKNAME1').AsString;
      edt_AP_BANK2.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKNAME2').AsString;
      edt_AP_BANK3.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKBRANCH1').AsString;
      edt_AP_BANK4.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKBRANCH2').AsString;
    end;
  finally
    FreeAndNil(Dialog_BANK_frm);
  end;
end;

procedure TUI_LOCRC2_frm.sBitBtn3Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    //개설은행
    2 : edt_AP_BANKDblClick(edt_AP_BANK);

  end;
end;

procedure TUI_LOCRC2_frm.edt_LOC1AMTCDblClick(Sender: TObject);
begin
  inherited;
  //IF (Sender as TsEdit).ReadOnly Then Exit;
  if not (Sender as TsEdit).Enabled then Exit;

  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    IF Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
    begin
      (Sender as TsEdit).Text := Dialog_CodeList_frm.qryList.FieldByName('CODE').AsString;

//      case (Sender as TsEdit).Tag of
//        101: sEdit21.Text := Dialog_CodeList_frm.qryList.FieldByName('NAME').AsString;
//      end;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;
end;

procedure TUI_LOCRC2_frm.sBitBtn8Click(Sender: TObject);
begin
  inherited;
  Case (Sender as TsBitBtn).Tag of
    101 : edt_LOC1AMTCDblClick(edt_LOC1AMTC);
    102 : edt_LOC1AMTCDblClick(edt_RCT_AMT1C);
    103 : edt_LOC1AMTCDblClick(edt_TQTY_G);
    104 : edt_LOC1AMTCDblClick(edt_TAMT_G);
    105 : edt_LOC1AMTCDblClick(edt_msg1);
    106 : edt_LOC1AMTCDblClick(edt_msg2);
    107 : edt_LOC1AMTCDblClick(edt_QTY_G);
    108 : edt_LOC1AMTCDblClick(edt_PRICE_G);
    109 : edt_LOC1AMTCDblClick(edt_QTYG_G);
    110 : edt_LOC1AMTCDblClick(edt_AMT_G);
    111 : edt_LOC1AMTCDblClick(edt_STQTY_G);
    112 : edt_LOC1AMTCDblClick(edt_STAMT_G);
  end;
end;

procedure TUI_LOCRC2_frm.FormShow(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctView;

  sPageControl1.ActivePageIndex := 0;

  EnabledControlValue(sPanel4);

  EnabledControlValue(sPanel9);
  EnabledControlValue(sPanel14);
  EnabledControlValue(sPanel15);
  EnabledControlValue(sPanel21);
  EnabledControlValue(sPanel28);
  EnabledControlValue(sPanel53 , False);

  com_SearchKeyword.ItemIndex := 0;
  ReadList(Mask_SearchDate1.Text, Mask_SearchDate2.Text,'');

  if qrylist.RecordCount = 0 then
  begin
    ClearControlValue(sPanel9);
    ClearControlValue(sPanel14);
    ClearControlValue(sPanel15);
  end;  

end;

procedure TUI_LOCRC2_frm.NewDocument;
var
  LOCAL_MAINT_NO : String;
begin
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctInsert;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sDBGrid1.Enabled :=False;
  sDBGrid2.Enabled := False;
  sDBGrid5.Enabled := False;  
  sPageControl1.ActivePageIndex := 0;
  sPageControl1Change(sPageControl1);
//------------------------------------------------------------------------------
// 읽기전용 해제
//------------------------------------------------------------------------------

  EnabledControlValue(sPanel4,false);
  EnabledControlValue(sPanel9,false);
  EnabledControlValue(sPanel14,false);
  EnabledControlValue(sPanel15,false);
  EnabledControlValue(sPanel53);
  {
  ReadOnlyControlValue(sPanel4,false);
  ReadOnlyControlValue(sPanel9,false);
  ReadOnlyControlValue(sPanel14,false);
  ReadOnlyControlValue(sPanel15,false);
  }
//------------------------------------------------------------------------------
// 버튼제어
//------------------------------------------------------------------------------
  ButtonEnable(False);
  GoodsButton;
  TaxButton;

//------------------------------------------------------------------------------
// 제출번호
//------------------------------------------------------------------------------
  LOCAL_MAINT_NO := AutoDocNo(True);

//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
  qryList.Append;

//------------------------------------------------------------------------------
// 제출번호 입력
//------------------------------------------------------------------------------
  edt_MAINT_NO.Text := LOCAL_MAINT_NO;
  edt_RFF_NO.Text := LOCAL_MAINT_NO;
//------------------------------------------------------------------------------
// 인수일자 설정
//------------------------------------------------------------------------------
  mask_GET_DAT.Text := FormatDateTime('YYYYMMDD',Now); 
//------------------------------------------------------------------------------
// 등록일자 설정
//------------------------------------------------------------------------------
  mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
  qrylistDATEE.AsString := mask_DATEE.Text;
//------------------------------------------------------------------------------
// 사용자
//------------------------------------------------------------------------------
  edt_USER_ID.Text := LoginData.sID;
//------------------------------------------------------------------------------
// 문서기능
//------------------------------------------------------------------------------
  edt_msg1.Text := '9';
//------------------------------------------------------------------------------
// 유형
//------------------------------------------------------------------------------
  edt_msg2.Text := 'AB';
//------------------------------------------------------------------------------
// 발급일자
//------------------------------------------------------------------------------
  mask_ISS_DAT.Text := FormatDateTime('YYYYMMDD',Now);
//------------------------------------------------------------------------------
// 개설의뢰인 셋팅
//------------------------------------------------------------------------------
  IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
  begin
    edt_APPLIC.Text   :=  DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
    edt_APPLIC1.Text  :=  DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    edt_APPLIC2.Text  :=  DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
    edt_APPLIC3.Text  := DMCodeContents.GEOLAECHEO.FieldByName('jenja').AsString;
    edt_APPLIC4.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
    edt_APPLIC5.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
    edt_APPLIC6.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR3').AsString;
    edt_APPLIC7.Text  := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
  end;
//------------------------------------------------------------------------------
// 개설금액(원화) 단위
//-----------------------------------------------------------------------------
  edt_LOC2AMTC.TEXT := 'KRW';
//------------------------------------------------------------------------------
// 인수금액(원화) 단위
//-----------------------------------------------------------------------------
  edt_RCT_AMT2C.Text := 'KRW';
end;

procedure TUI_LOCRC2_frm.NewDocumentFromLOCAPP;
begin

end;

procedure TUI_LOCRC2_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  NewDocument;
end;

procedure TUI_LOCRC2_frm.CancelDocument;
begin
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;

//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sDBGrid1.Enabled := True;
  sDBGrid2.Enabled := True;  
  sDBGrid5.Enabled := True;
  sPageControl1.ActivePageIndex := 0;
  sPageControl1Change(sPageControl1);
//------------------------------------------------------------------------------
// 읽기전용
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel9);
  EnabledControlValue(sPanel14);
  EnabledControlValue(sPanel15);
  EnabledControlValue(sPanel53 , False);
  {
  ReadOnlyControlValue(sPanel4);
  ReadOnlyControlValue(sPanel9);
  ReadOnlyControlValue(sPanel14);
  ReadOnlyControlValue(sPanel15);
  }
//------------------------------------------------------------------------------
// 버튼제어
//------------------------------------------------------------------------------
  ButtonEnable(True);
  GoodsButton(False);
  TaxButton(False);

  if DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.RollbackTrans;

  qryList.Close;
  qryList.Open;
end;

procedure TUI_LOCRC2_frm.btnCancelClick(Sender: TObject);
begin
  inherited;
  CancelDocument;
end;

procedure TUI_LOCRC2_frm.GoodsButton(Val: Boolean);
begin
  Btn_GoodsNew.Enabled  := Val;
  Btn_GoodsEdit.Enabled := Val and (qryGoods.RecordCount > 0);
  Btn_GoodsDel.Enabled  := Val and (qryGoods.RecordCount > 0);
  Btn_GoodsOK.Enabled   := (not Val) and (not (ProgramControlType = ctView));
  Btn_GoodsCancel.Enabled := (not Val) and (not (ProgramControlType = ctView));
  sSpeedButton18.Enabled := Val; //엑셀가져오기 버튼
  sSpeedButton13.Enabled := Val; //엑셀 샘플만들기
end;

procedure TUI_LOCRC2_frm.TaxButton(Val: Boolean);
begin
  Btn_TaxNew.Enabled  := Val;
  Btn_TaxEdit.Enabled := Val and (qryTax.RecordCount > 0);
  Btn_TaxDel.Enabled  := Val and (qryTax.RecordCount > 0);
  Btn_TaxOK.Enabled   := (not Val) and (not (ProgramControlType = ctView));
  Btn_TaxCancel.Enabled := (not Val) and (not (ProgramControlType = ctView));
  taxExcelBtn.Enabled := Val; //엑셀가져오기 버튼
  taxSampleBtn.Enabled := Val; //엑셀 샘플만들기
end;

function TUI_LOCRC2_frm.AutoDocNo(onlyReturn : Boolean):String;
var
  AutoNumbering : TAutoNumbering;
begin
  Result := '';
//------------------------------------------------------------------------------
// 문서 자동사용확인
//------------------------------------------------------------------------------
  AutoNumbering := DMCodeContents.AutoNumberingForLOCAPP;

  if not onlyReturn Then
  begin
    //------------------------------------------------------------------------------
    // 신규버튼을 눌렀을경우 자동으로 EDIT에 관리번호를 넣는다
    //------------------------------------------------------------------------------
    edt_MAINT_NO.ReadOnly := true;

    IF  edt_MAINT_NO.ReadOnly Then
      edt_MAINT_NO.Color := clBtnFace
    else
      edt_MAINT_NO.Color := clWhite;

    IF AutoNumbering.LOCAPP_NO <> '' Then
      edt_MAINT_NO.Text := 'LOCRCT'+AutoNumbering.LOCAPP_NO
    else
      edt_MAINT_NO.Text := 'LOCRCT';

//    edt_DocNo2.Clear;
//    IF AutoNumbering.LOCAPP_BANK <> '' Then
//      edt_DocNo2.Text := AutoNumbering.LOCAPP_BANK;

    edt_MAINT_NO.Text := edt_MAINT_NO.Text +'-'+ DMAutoNo.GetDocumentNoAutoInc('LOCRCT');
    edt_RFF_NO.Text := edt_MAINT_NO.Text;
  end
  else
  begin
    //------------------------------------------------------------------------------
    // onlyReturn는 EDIT에 자동으로 넣지않고 리턴값으로 관리번호를 받는다(COPY용)
    //------------------------------------------------------------------------------
    IF AutoNumbering.LOCRCT_NO = '' Then
      raise Exception.Create('LOCRCT 식별자가 지정되어있지 않습니다. 식별자를 입력하세요');

    Result := 'LOCRCT'+AutoNumbering.LOCRCT_NO+'-'+DMAutoNo.GetDocumentNoAutoInc('LOCRCT');
  end;
end;

procedure TUI_LOCRC2_frm.mask_DATEEDblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  IF not (ProgramControlType in [ctInsert,ctModify,ctView]) Then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_LOCRC2_frm.btn_CalClick(Sender: TObject);
begin
  inherited;
  Case (Sender as TsBitBtn).Tag of
    0: mask_DATEEDblClick(mask_GET_DAT);
    1: mask_DATEEDblClick(mask_ISS_DAT);
    2: mask_DATEEDblClick(mask_EXP_DAT);
    901: mask_DATEEDblClick(mask_DATEE);
    900: mask_DATEEDblClick(Mask_SearchDate1);
    907: mask_DATEEDblClick(Mask_SearchDate2);
  end;

end;

procedure TUI_LOCRC2_frm.ReadGoods;
begin
//------------------------------------------------------------------------------
// 상품
//------------------------------------------------------------------------------
  edt_NAME_COD.Text := qryGoodsNAME_COD.AsString;
  edt_HS_NO.Text := qryGoodsHS_NO.AsString;
  memo_NAME1.Text := qryGoodsNAME1.AsString;
  memo_SIZE1.Text := qryGoodsSIZE1.AsString;
//------------------------------------------------------------------------------
// 수량/단가/금액
//------------------------------------------------------------------------------
  edt_QTY_G.Text := qryGoodsQTY_G.AsString;
  edt_QTYG_G.Text := qryGoodsQTYG_G.AsString;
  edt_PRICE_G.Text := qryGoodsPRICE_G.AsString;
  edt_AMT_G.Text := qryGoodsAMT_G.AsString;
  edt_STQTY_G.Text := qryGoodsSTQTY_G.AsString;
  edt_STAMT_G.Text := qryGoodsSTAMT_G.AsString;

  curr_QTY.Value := qryGoodsQTY.AsCurrency;
  curr_QTYG.Value := qryGoodsQTYG.AsCurrency;
  curr_PRICE.Value :=  qryGoodsPRICE.AsCurrency;
  curr_AMT.Value := qryGoodsAMT.AsCurrency;
  curr_STQTY.Value := qryGoodsSTQTY.AsCurrency;
  curr_STAMT.Value := qryGoodsSTAMT.AsCurrency;


end;

procedure TUI_LOCRC2_frm.ReadTax;
begin
  inherited;
  //세금계산서번호
  edt_BILL_NO.Text := qryTaxBILL_NO.AsString;
  //작성일자
  mask_BILL_DATE.Text := qryTaxBILL_DATE.AsString;
  //공급가액
  edt_BILL_AMOUNT_UNIT.Text := qryTaxBILL_AMOUNT_UNIT.AsString;
  curr_BILL_AMOUNT.Value := qryTaxBILL_AMOUNT.AsCurrency;
  //세액
  edt_TAX_AMOUNT_UNIT.Text := qryTaxTAX_AMOUNT_UNIT.AsString;
  curr_TAX_AMOUNT.Value := qryTaxTAX_AMOUNT.AsCurrency;

end;

procedure TUI_LOCRC2_frm.qrylistAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then
  begin
    qrylistAfterScroll(qrylist);
  end;
end;

procedure TUI_LOCRC2_frm.qryGoodsAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadGoods;
end;

procedure TUI_LOCRC2_frm.Btn_GoodsNewMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  (Sender as TsSpeedButton).Font.Style := [fsBold, fsUnderline];
end;

procedure TUI_LOCRC2_frm.Btn_GoodsNewMouseLeave(Sender: TObject);
begin
  inherited;
  (Sender as TsSpeedButton).Font.Style := [fsBold];
end;

procedure TUI_LOCRC2_frm.Btn_GoodsNewClick(Sender: TObject);
begin
  inherited;
  sDBGrid3.Enabled := False;
  qryGoods.Append;
end;

procedure TUI_LOCRC2_frm.Btn_GoodsEditClick(Sender: TObject);
begin
  inherited;
  if qryGoods.RecordCount > 0 then
  begin
    sDBGrid3.Enabled := False;
    qryGoods.Edit;
  end;
end;

procedure TUI_LOCRC2_frm.Btn_GoodsDelClick(Sender: TObject);
begin
  inherited;
  if qryGoods.RecordCount > 0 then
  begin
    IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'상품내역 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      qryGoods.Delete;
  end;
end;

procedure TUI_LOCRC2_frm.Btn_GoodsCancelClick(Sender: TObject);
begin
  inherited;
  sDBGrid3.Enabled := True;
  qryGoods.Cancel;
end;

procedure TUI_LOCRC2_frm.qryGoodsAfterInsert(DataSet: TDataSet);
begin
  inherited;
  GoodsButton(False);
  EnabledControlValue(sPanel21,False);
  //ReadOnlyControlValue(sPanel21,False);
end;

procedure TUI_LOCRC2_frm.qryGoodsAfterCancel(DataSet: TDataSet);
begin
  inherited;
  GoodsButton(True);
  EnabledControlValue(sPanel21);
  //ReadOnlyControlValue(sPanel21);
end;

procedure TUI_LOCRC2_frm.edt_PRICE_GChange(Sender: TObject);
begin
  inherited;
  if edt_PRICE_G.Focused Then
  begin
    edt_AMT_G.Text   := edt_PRICE_G.Text;
//    {$MESSAGE ERROR '여기서부터 개발'}
  end;

  if edt_QTYG_G.Focused then
  begin
    edt_QTYG_G.Text := edt_QTY_G.Text;
  end;

end;

procedure TUI_LOCRC2_frm.qryTaxAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadTax;
end;

procedure TUI_LOCRC2_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
  Action := caFree;
  UI_LOCRC2_frm := nil;
end;

procedure TUI_LOCRC2_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCRC2_frm.btn_GoodsCalcClick(Sender: TObject);
begin
  inherited;
  //소량소계의 단위는 수량의 단위와 같다
  edt_STQTY_G.Text := edt_QTY_G.Text;
  //소량소계의 값은 수량과 같다
  curr_STQTY.Value := curr_QTY.Value;
  //금액소계의 단위는 금액의 단위와 같다
  edt_STAMT_G.Text := edt_AMT_G.Text;
  //금액소계의 값은 금액과 같다
  curr_STAMT.Value := curr_AMT.Value;
end;

procedure TUI_LOCRC2_frm.curr_PRICEChange(Sender: TObject);
begin
  inherited;
  if curr_PRICE.ReadOnly = False then
  begin
    if (Trim(curr_QTY.Text) = '') or (curr_QTY.Value = 0) then
    begin
      curr_QTY.Value := 1;
    end;

    if (curr_QTYG.Value = 0) or (Trim(curr_QTYG.Text) = '') then
    begin
      //금액  = (수량 * 단가) / 1 ->단가기준수량이 0또는 '' 일때는 1로계산
      curr_AMT.Value := (curr_QTY.Value * curr_PRICE.Value);
    end
    else
    begin
      //금액  = (수량 * 단가) / 단가기준수량
      curr_AMT.Value := (curr_QTY.Value * curr_PRICE.Value) / curr_QTYG.Value;
    end;



  end;
end;

procedure TUI_LOCRC2_frm.SaveDocument(Sender: TObject);
var
  SQLCreate : TSQLCreate;
  DocNo : String;
  CreateDate : TDateTime;
begin

  try
    //--------------------------------------------------------------------------
    // 유효성 검사
    //--------------------------------------------------------------------------
    if (Sender as TsButton).Tag = 1 then
    begin
      Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
      if Dlg_ErrorMessage_frm.Run_ErrorMessage( '내국신용장 물품수령 증명서' ,CHECK_VALIDITY ) Then
      begin
        FreeAndNil(Dlg_ErrorMessage_frm);
        Exit;
      end;
    end;
    
  //----------------------------------------------------------------------------
  // SQL생성기
  //----------------------------------------------------------------------------
  SQLCreate := TSQLCreate.Create;
        with SQLCreate do
        begin
          DocNo := edt_MAINT_NO.Text;

        //----------------------------------------------------------------------
        // 문서헤더 및 LOCRC2_H.DB
        //----------------------------------------------------------------------

          Case ProgramControlType of
            ctInsert :
            begin
              DMLType := dmlInsert;
              ADDValue('MAINT_NO',DocNo);
            end;

            ctModify :
            begin
              DMLType := dmlUpdate;
              ADDWhere('MAINT_NO',DocNo);
            end;
          end;

          SQLHeader('LOCRC2_H');

           //문서저장구분(0:임시, 1:저장)
          ADDValue('CHK2',IntToStr((Sender as TsButton).Tag));
          //사용자
          ADDValue('USER_ID',edt_USER_ID.Text);
          //등록일자
          ADDValue('DATEE',mask_DATEE.Text);
          //문서기능
          ADDValue('MESSAGE1',edt_msg1.Text);
          //유형
          ADDValue('MESSAGE2',edt_msg2.Text);
          //인수일자
          ADDValue('GET_DAT',mask_GET_DAT.Text);
          //발급일자
          ADDValue('ISS_DAT',mask_ISS_DAT.Text);
          //문서유효일자
          ADDValue('EXP_DAT',mask_EXP_DAT.Text);
          //발급번호
          ADDValue('RFF_NO',edt_RFF_NO.Text);
          //대표공급물품 HS부호
          ADDValue('BSN_HSCODE',edt_BSN_HSCODE.Text);
          //수혜업체
          ADDValue('BENEFC',edt_BENEFC.Text);
          ADDValue('BENEFC1',edt_BENEFC1.Text);
          //수혜업체 사업자등록번호
          ADDValue('BENEFC2',edt_BENEFC2.Text);
          //개설업체
          ADDValue('APPLIC',edt_APPLIC.Text);
          ADDValue('APPLIC1',edt_APPLIC1.Text);
          //개설업체 사업자등록번호
          ADDValue('APPLIC7',edt_APPLIC7.Text);
          //명의인
          ADDValue('APPLIC2',edt_APPLIC2.Text);
          //전자서명
          ADDValue('APPLIC3',edt_APPLIC3.Text);
          //주소
          ADDValue('APPLIC4',edt_APPLIC4.Text);
          ADDValue('APPLIC5',edt_APPLIC5.Text);
          ADDValue('APPLIC6',edt_APPLIC6.Text);
          //개설은행
          ADDValue('AP_BANK',edt_AP_BANK.Text);
          ADDValue('AP_BANK1',edt_AP_BANK1.Text);
          ADDValue('AP_BANK2',edt_AP_BANK2.Text);
          //은행(부)지점명
          ADDValue('AP_BANK3',edt_AP_BANK3.Text);
          ADDValue('AP_BANK4',edt_AP_BANK4.Text);
          //전자서명
          ADDValue('AP_NAME',edt_AP_NAME.Text);
          //내국신용장번호
          ADDValue('LOC_NO',edt_LOC_NO.Text);
          //개설금액(외화)
          ADDValue('LOC1AMTC',edt_LOC1AMTC.Text);
          ADDValue('LOC1AMT',curr_LOC1AMT.Text);
          //매매기준율
          if edt_EX_RATE.Text = '' then
            ADDValue('EX_RATE','0')
          else
            ADDValue('EX_RATE',edt_EX_RATE.Text);
          //개설금액(원화)
          ADDValue('LOC2AMTC',edt_LOC2AMTC.Text);
          ADDValue('LOC2AMT',curr_LOC2AMT.Text);
          //물품인도기일
          ADDValue('LOADDATE',mask_LOADDATE.Text);
          //유효일자
          ADDValue('EXPDATE',mask_EXPDATE.Text);
          //신용장관리번호
          ADDValue('AMAINT_NO',edt_AMAINT_NO.Text);
          //인수금액(외화)
          ADDValue('RCT_AMT1C',edt_RCT_AMT1C.Text);
          ADDValue('RCT_AMT1',curr_RCT_AMT1.Text);
          //환율
          if edt_RATE.Text = '' then
            ADDValue('RATE','0')
          else
            ADDValue('RATE',edt_RATE.Text);
          //인수금액(원화)
          ADDValue('RCT_AMT2C',edt_RCT_AMT2C.Text);
          ADDValue('RCT_AMT2',curr_RCT_AMT2.Text);
          //총합계수량
          ADDValue('TQTY_G',edt_TQTY_G.Text);
          ADDValue('TQTY',curr_TQTY.Text);
          //총합계금액
          ADDValue('TAMT_G',edt_TAMT_G.Text);
          ADDValue('TAMT',curr_TAMT.Text);
          //기타조건
          ADDValue('REMARK1',memo_REMARK1.Text);
          //참고사항
          ADDValue('LOC_REM1',memo_LOC_REM1.Text);
        end;

        with TADOQuery.Create(nil) do
        begin
          try
            Connection := DMMssql.KISConnect;
            SQL.Text := SQLCreate.CreateSQL;
            //if sCheckBox1.Checked then
              //Clipboard.AsText := SQL.Text;
            ExecSQL;
          finally
            Close;
            Free;
          end;
        end;

    //------------------------------------------------------------------------------
    // 프로그램 제어
    //------------------------------------------------------------------------------
      ProgramControlType := ctView;

    //------------------------------------------------------------------------------
    // 접근제어
    //------------------------------------------------------------------------------
      sDBGrid1.Enabled := True;
      sDBGrid2.Enabled := True;
      sDBGrid5.Enabled := True;
      sPageControl1.ActivePageIndex := 0;
      sPageControl1Change(sPageControl1);
    //------------------------------------------------------------------------------
    // 읽기전용
    //------------------------------------------------------------------------------
      EnabledControlValue(sPanel4);
      EnabledControlValue(sPanel9);
      EnabledControlValue(sPanel14);
      EnabledControlValue(sPanel15);
      EnabledControlValue(sPanel53 , False);
      {
      ReadOnlyControlValue(sPanel4);
      ReadOnlyControlValue(sPanel9);
      ReadOnlyControlValue(sPanel14);
      ReadOnlyControlValue(sPanel15);
      }
    //------------------------------------------------------------------------------
    // 버튼제어
    //------------------------------------------------------------------------------
      ButtonEnable(True);
      GoodsButton(False);
      TaxButton(False);

    //----------------------------------------------------------------------------
    //새로고침
    //----------------------------------------------------------------------------
    CreateDate := ConvertStr2Date(mask_DATEE.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             DocNo);

    if DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.CommitTrans;

//  qryList.Close;
//   qryList.Open;

  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message),'저장오류',MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TUI_LOCRC2_frm.btnSaveClick(Sender: TObject);
begin
  inherited;
  SaveDocument(Sender);
end;

function TUI_LOCRC2_frm.MAX_SEQ(TableName: String): Integer;
begin
  with qryMAX_SEQ do
  begin
    Close;
    SQL.Text := 'SELECT MAX(SEQ) AS MaxSEQ FROM '+ TableName +' WHERE KEYY =' + QuotedStr(edt_MAINT_NO.Text);
    Open;

    Result := FieldByName('MaxSEQ').AsInteger + 1;
  end;
end;

procedure TUI_LOCRC2_frm.GoodsSaveDocument;
begin
  if qryGoods.State = dsInsert then
  begin
    //KEYY
    qryGoods.FieldByName('KEYY').AsString := edt_MAINT_NO.Text;

    //순번
    if qryGoods.RecordCount = 0 then
      qryGoods.FieldByName('SEQ').AsInteger := 1
    else
    begin
      qryGoods.FieldByName('SEQ').AsInteger := MAX_SEQ('LOCRC2_D');
    end;
  end;

  //품명코드
  qryGoods.FieldByName('NAME_COD').AsString := edt_NAME_COD.Text;
  //품명
  qryGoods.FieldByName('NAME1').AsString := memo_NAME1.Text;
  //규격
  qryGoods.FieldByName('SIZE1').AsString := memo_SIZE1.Text;
  //수량
  qryGoods.FieldByName('QTY_G').AsString := edt_QTY_G.Text;
  qryGoods.FieldByName('QTY').AsString := curr_QTY.Text;
  //단가
  qryGoods.FieldByName('PRICE_G').AsString := edt_PRICE_G.Text;
  qryGoods.FieldByName('PRICE').AsString := curr_PRICE.Text;
  //단가기준수량
  qryGoods.FieldByName('QTYG_G').AsString := edt_QTYG_G.Text;
  qryGoods.FieldByName('QTYG').AsString := curr_QTYG.Text;
  //금액
  qryGoods.FieldByName('AMT_G').AsString := edt_AMT_G.Text;
  qryGoods.FieldByName('AMT').AsString := curr_AMT.Text;
  //수량소계
  qryGoods.FieldByName('STQTY_G').AsString := edt_STQTY_G.Text;
  qryGoods.FieldByName('STQTY').AsString := curr_STQTY.Text;
  //금액소계
  qryGoods.FieldByName('STAMT_G').AsString := edt_STAMT_G.Text;
  qryGoods.FieldByName('STAMT').AsString := curr_STAMT.Text;
  //HS부호
  qryGoods.FieldByName('HS_NO').AsString := edt_HS_NO.Text;
  qryGoods.Post;

end;

procedure TUI_LOCRC2_frm.Btn_GoodsOKClick(Sender: TObject);
begin
  inherited;
  GoodsSaveDocument;

  GoodsButton(True);
  EnabledControlValue(sPanel21);
  //ReadOnlyControlValue(sPanel21);
  sDBGrid3.Enabled := True;

end;

procedure TUI_LOCRC2_frm.btn_TaxNewClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsSpeedButton).Tag of
    //신규
    0 :
    begin
      sDBGrid4.Enabled := False;
      qryTax.Append;

      //------------------------------------------------------------------------
      // 세금계산서 공금가액
      //------------------------------------------------------------------------
       edt_BILL_AMOUNT_UNIT.Text := 'KRW';
      //------------------------------------------------------------------------
      // 세금계산서 공금가액
      //------------------------------------------------------------------------
       edt_TAX_AMOUNT_UNIT.Text := 'KRW';
    end;

    //수정
    1 :
    begin
      if qryTax.RecordCount > 0 then
      begin
        sDBGrid4.Enabled := False;
        qryTax.Edit;
      end;
    end;
    //삭제
    2 :
    begin
      if qryTax.RecordCount > 0 then
      begin
        IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'상품내역 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
          qryTax.Delete;
      end;
    end;

    //저장
    3 :
    begin
      sDBGrid4.Enabled := True;
      TaxSaveDocument;
    end;
    //취소
    4 :
    begin
      sDBGrid4.Enabled := True;
      qryTax.Cancel;
    end;
  end;
end;

procedure TUI_LOCRC2_frm.qryTaxAfterInsert(DataSet: TDataSet);
begin
  inherited;
  TaxButton(False);
  EnabledControlValue(sPanel28,False);
end;

procedure TUI_LOCRC2_frm.qryTaxAfterCancel(DataSet: TDataSet);
begin
  inherited;
  TaxButton(True);
  EnabledControlValue(sPanel28);
  //ReadOnlyControlValue(sPanel21);
end;

procedure TUI_LOCRC2_frm.TaxSaveDocument;
begin
  if qryTax.State = dsInsert then
  begin
    //KEYY
    qryTax.FieldByName('KEYY').AsString := edt_MAINT_NO.Text;

    //순번
    if qryTax.RecordCount = 0 then
      qryTax.FieldByName('SEQ').AsInteger := 1
    else
    begin
      qryTax.FieldByName('SEQ').AsInteger := MAX_SEQ('LOCRC2_TAX');
    end;
  end;

  with qryTax do
  begin
    //세금계산서 번호
    FieldByName('BILL_NO').AsString := edt_BILL_NO.Text;
    //작성일자
    FieldByName('BILL_DATE').AsString := mask_BILL_DATE.Text;
    //공금가액단위
    FieldByName('BILL_AMOUNT_UNIT').AsString := edt_BILL_AMOUNT_UNIT.Text;
    //공급가액
    FieldByName('BILL_AMOUNT').AsString := curr_BILL_AMOUNT.Text;
    //세액단위
    FieldByName('TAX_AMOUNT_UNIT').AsString := edt_TAX_AMOUNT_UNIT.Text;
    //세액
    FieldByName('TAX_AMOUNT').AsString := curr_TAX_AMOUNT.Text;
  end;

  TaxButton(True);
  EnabledControlValue(sPanel28);

  qryTax.Post;
end;

procedure TUI_LOCRC2_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  if AnsiMatchText(qryListCHK2.AsString,['','0','1','2','5','6']) then
  begin
    EditDocument;
  end
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_EDIT,'수정불가', MB_OK+MB_ICONINFORMATION);
end;

procedure TUI_LOCRC2_frm.EditDocument;
begin

//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctModify;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sDBGrid1.Enabled :=False;
  sDBGrid2.Enabled := False;
  sDBGrid5.Enabled := False;
  sPageControl1.ActivePageIndex := 0;
  sPageControl1Change(sPageControl1);
//------------------------------------------------------------------------------
// 읽기전용 해제
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4,false);
  EnabledControlValue(sPanel9,false);
  EnabledControlValue(sPanel14,false);
  EnabledControlValue(sPanel15,false);
  EnabledControlValue(sPanel53);
  {
  ReadOnlyControlValue(sPanel4,false);
  ReadOnlyControlValue(sPanel9,false);
  ReadOnlyControlValue(sPanel14,false);
  ReadOnlyControlValue(sPanel15,false);
  }
//------------------------------------------------------------------------------
// 버튼제어
//------------------------------------------------------------------------------
  ButtonEnable(False);
  GoodsButton;
  TaxButton;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
  qryList.Edit;

end;

procedure TUI_LOCRC2_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;
end;

procedure TUI_LOCRC2_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;


        SQL.Text :=  'DELETE FROM LOCRC2_H  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM LOCRC2_D WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM LOCRC2_TAX WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;
end;

procedure TUI_LOCRC2_frm.btn_TOTAL_CALCClick(Sender: TObject);
var
  totalAmount,totalMoney : Double;
begin
  inherited;

  totalAmount := 0;
  totalMoney := 0;

  if qryGoods.RecordCount > 0 then
  begin
    qryGoods.First;

    //총수량의 단위는 첫번쨰 레코드의 단위와 같다
    edt_TQTY_G.Text := qryGoods.FieldByName('QTY_G').AsString;
    //총금액의 단위는 첫번째 레코드의 단위와 같다
    edt_TAMT_G.Text := qryGoods.FieldByName('AMT_G').AsString;

    while not qryGoods.Eof do
    begin
      //총수량 = 수량소계의 합
      totalAmount := qryGoods.FieldByName('STQTY').AsFloat + totalAmount;
      //총금액 = 금액소계의 합
      totalMoney := qryGoods.FieldByName('STAMT').AsFloat + totalMoney;
      qryGoods.Next;
    end;

     curr_TQTY.Value := totalAmount;
     curr_TAMT.Value := totalMoney;
  end
  else
  begin
     MessageBox(Self.Handle,MSG_ERR_CALC,'계산오류',MB_OK+MB_ICONINFORMATION);
  end;



end;

//품명정보 불러오기
procedure TUI_LOCRC2_frm.edt_NAME_CODDblClick(Sender: TObject);
begin                                                                     
  inherited;
  Dialog_ItemList_frm := TDialog_ItemList_frm.Create(Self);
  try
    if Dialog_ItemList_frm.openDialog = mrok then
    begin
        //선택된 코드 품명 규격 출력
        edt_NAME_COD.Text:= Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        //HSCODE
        edt_HS_NO.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('HSCODE').AsString;
        //품명
        memo_NAME1.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('FNAME').AsString;
        //규격
        memo_SIZE1.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('MSIZE').AsString;
        //수량단위
        edt_QTY_G.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('QTYC').AsString;
        //수량
        curr_QTY.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('QTY_U').AsString;
        //단가단위
        edt_PRICE_G.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('PRICEG').AsString;
        //단가
        curr_PRICE.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('PRICE').AsString;
        //단가기준수량단위
        edt_QTYG_G.Text := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('QTY_UG').AsString;
        //금액단위
        edt_AMT_G.Text := edt_PRICE_G.Text;
    end;
  finally
    FreeAndNil(Dialog_ItemList_frm);
  end;
end;

procedure TUI_LOCRC2_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  LOCRC2_PRINT_frm := TLOCRC2_PRINT_frm.Create(Self);
  LOCRC2_PRINT2_frm := TLOCRC2_PRINT2_frm.Create(Self);

  try
    LOCRC2_PRINT_frm.DocNo := edt_MAINT_NO.Text;
    LOCRC2_PRINT2.LOCRC2_PRINT2_frm.DocNo := edt_MAINT_NO.Text;
    QRCompositeReport1.Prepare;
    QRCompositeReport1.Preview;
  finally
    FreeAndNil(LOCRC2_PRINT_frm);
    FreeAndNil(LOCRC2_PRINT2_frm);
  end;
end;

procedure TUI_LOCRC2_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(LOCRC2_PRINT_frm);
  QRCompositeReport1.Reports.Add(LOCRC2_PRINT2_frm);
end;                        

procedure TUI_LOCRC2_frm.btnCopyClick(Sender: TObject);
var
  copyDocNo , newDocNo : String;
begin
  inherited;
  Dialog_CopyLOCRC2_frm := TDialog_CopyLOCRC2_frm.Create(Self);
  try
      newDocNo := AutoDocNo(True);
      copyDocNo := Dialog_CopyLOCRC2_frm.openDialog;

      //ShowMessage(copyDocNo);

      IF Trim(copyDocNo) = '' then Exit;

      //------------------------------------------------------------------------------
      // COPY
      //------------------------------------------------------------------------------
      with spCopyLOCRC2 do
      begin
        Close;
        Parameters.ParamByName('@CopyDocNo').Value := copyDocNo;
        Parameters.ParamByName('@NewDocNo').Value :=  newDocNo;
        Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

        if not DMMssql.KISConnect.InTransaction then
          DMMssql.KISConnect.BeginTrans;

        try
          ExecProc;
        except
          on e:Exception do
          begin
            DMMssql.KISConnect.RollbackTrans;
            MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
          end;
        end;

      end;
    // ReadListBetween(해당년도 1월1일 , 오늘날짜 , 새로복사 된  관리번호)
    if  ReadListBetween(FormatDateTime('YYYYMMDD', StartOfTheYear(Now)),FormatDateTime('YYYYMMDD',Now),newDocNo) Then
    begin

      EditDocument;
      mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
      mask_GET_DAT.Text := FormatDateTime('YYYYMMDD',Now);
      mask_ISS_DAT.Text := FormatDateTime('YYYYMMDD',Now);
      edt_MAINT_NO.Text := newDocNo;
    
    end
    else
      raise Exception.Create('복사한 데이터를 찾을수 없습니다');

    sPageControl1.ActivePageIndex := 0;

  finally

    FreeAndNil(Dialog_CopyLOCRC2_frm);

  end;
end;

function TUI_LOCRC2_frm.ReadListBetween(fromDate, toDate,
  KeyValue: string): Boolean;
begin
  Result := False;
  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC');
    Open;

    //ShowMessage(SQL.Text);

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;

  end;
end;

procedure TUI_LOCRC2_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 3 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := FSQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 :
      begin
        SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :
      begin
        SQL.Add(' WHERE BENEFC1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end;
    Open;
  end;
end;

procedure TUI_LOCRC2_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin

    case CHK2Value of
      9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

      6 :
      begin
        if AnsiMatchText( Column.FieldName , ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;

end;

procedure TUI_LOCRC2_frm.MenuItem1Click(Sender: TObject);
begin
  inherited;
  case (Sender as TMenuItem).Tag of
    0 : btn_TaxNewClick(btn_TaxNew);
    1 : btn_TaxNewClick(btn_TaxEdit);
    2 : btn_TaxNewClick(btn_TaxDel);
  end;
end;

procedure TUI_LOCRC2_frm.memo_REMARK1Change(Sender: TObject);
var
  memoRow , memoCol : Integer;
begin
  inherited;
  //메모의 라인번호
  memorow := (Sender as TsMemo).Perform(EM_LINEFROMCHAR,(Sender as TsMemo).SelStart,0);
  //메모의 컬럼번호
  memoCol := (Sender as TsMemo).SelStart - (Sender as TsMemo).Perform(EM_LINEINDEX,memorow,0);

  if (Sender as TsMemo).Name = 'memo_REMARK1' then
    sLabel6.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  if (Sender as TsMemo).Name = 'memo_LOC_REM1' then
    sLabel1.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
end;

procedure TUI_LOCRC2_frm.memoLineLimit(Sender: TObject; var Key: Char);
var
  limitcount : Integer;
begin
  limitcount := 6;
  if (Key = #13) AND ((Sender as TsMemo).lines.count >= limitCount-1) then Key := #0;

end;

procedure TUI_LOCRC2_frm.popMenuEnabled(Sender: TObject);
begin
  inherited;
  case (Sender as TPopupMenu).Tag of
   0:
    begin
      N1.Enabled :=  ProgramControlType in [ctInsert , ctModify];
      N2.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryGoods.RecordCount > 0);
      N3.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryGoods.RecordCount > 0);
    end;
   1:
    begin
      MenuItem1.Enabled :=  ProgramControlType in [ctInsert , ctModify];
      MenuItem2.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryTax.RecordCount > 0);
      MenuItem3.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryTax.RecordCount > 0);
    end
  end;
end;

procedure TUI_LOCRC2_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  //데이터 신규,수정 작성시 데이터조회 페이지 검색부분 비활성화
  if ProgramControlType in [ctInsert , ctModify] then
    EnabledControlValue(sPanel3)
  else
    EnabledControlValue(sPanel3,False);

  if (Sender as TsPageControl).ActivePageIndex = 3 then
  begin
    left_panel.Visible := not (sPageControl1.ActivePageIndex = 3);
  end
  else
    left_panel.Visible := True;
end;

procedure TUI_LOCRC2_frm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  //
end;

procedure TUI_LOCRC2_frm.sDBGrid5DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $008DDCFA;
      Canvas.Font.Color := clblack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TUI_LOCRC2_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
    if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_LOCRC2_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := LOCRC2(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'LOCRC2';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'LOCRC2_H';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        Readlist(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_LOCRC2_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_LOCRC2_frm.FormActivate(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;

  if not DMMssql.KISConnect.InTransaction then
  begin

    qryList.Close;
    qryList.Open;

  end;
end;

procedure TUI_LOCRC2_frm.sSpeedButton18Click(Sender: TObject);
var
  ExcelApp , ExcelBook , ExcelSheet : Variant;
  ExcelfileName : String;
  ExcelRows, ExcelCols, StartRow : Integer;
  i : Integer;
begin
  //상품내역 엑셀가져오기-------------------------------------------------------
  inherited;
  try
    ExcelApp := CreateOleObject('Excel.Application');
  except
    ShowMessage('Excel이 설치되어 있지 않습니다.');
    Exit;
  end;

  //excelOpen.Filter := 'All Files (*.*)|*.*|Microsoft Excel 통합문서(*.xls)|*.xls|엑셀2007(*.xlsx)|*.xlsx';

  if not excelOpen.Execute then Exit;

  //excelOpen.FileName을 사용해도 되지만 윈도우상에 버그로 글씨가 깨지거나함
  ExcelfileName := excelOpen.Files.Strings[0]; //선택된 엑셀파일이름

  try
    with TADOQuery.Create(nil) do
    begin
      Connection := DMMssql.KISConnect;
      try
        SQL.Text :=  'DELETE FROM LOCRC2_D WHERE KEYY =' + QuotedStr(edt_MAINT_NO.Text);
        ExecSQL;

        qryGoods.Close;
        qrygoods.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
        qryGoods.Open;

      finally
        Close;
        Free;
      end;
    end;

    ExcelApp.Visible := False; //엑셀보이지 않기
    ExcelApp.DisplayAlerts := False; //메시지표시않기

    ExcelBook := ExcelApp.WorkBooks.Open(ExcelfileName);
    ExcelSheet := ExcelBook.Sheets['Sheet1'];

    ExcelRows := ExcelApp.ActiveSheet.UsedRange.Rows.count; //총 row수 구하기
    ExcelCols := ExcelApp.ActiveSheet.UsedRange.Columns.count; //총 Col수 구하기

    for i := 2 to ExcelRows do
    begin
      qryGoods.Append;

      qryGoods.FieldByName('KEYY').AsString := edt_MAINT_NO.Text;
      qryGoods.FieldByName('SEQ').AsInteger := i-1;
      qryGoods.FieldByName('NAME_COD').AsString := ExcelSheet.Cells[i,1].Value; //품명
      qryGoods.FieldByName('NAME1').AsString := ExcelSheet.Cells[i,2].Value; //품명
      qryGoods.FieldByName('SIZE1').AsString := ExcelSheet.Cells[i,3].Value; //규격
      qryGoods.FieldByName('HS_NO').AsString := ExcelSheet.Cells[i,4].Value; //hs부호
      qryGoods.FieldByName('QTY').AsCurrency := ExcelSheet.Cells[i,5].Value; //수량
      qryGoods.FieldByName('QTY_G').AsString := ExcelSheet.Cells[i,6].Value; //수량단위
      qryGoods.FieldByName('PRICE').AsCurrency := ExcelSheet.Cells[i,7].Value; //단가
      qryGoods.FieldByName('PRICE_G').AsString := ExcelSheet.Cells[i,8].Value; //단가단위
      qryGoods.FieldByName('QTYG').AsCurrency := ExcelSheet.Cells[i,9].Value; //단가기준수량
      qryGoods.FieldByName('QTYG_G').AsString := ExcelSheet.Cells[i,10].Value; //단가기준수량단위
      qryGoods.FieldByName('AMT').AsCurrency := ExcelSheet.Cells[i,11].Value; //금액
      qryGoods.FieldByName('AMT_G').AsString := ExcelSheet.Cells[i,12].Value; //금액단위
      qryGoods.FieldByName('STQTY').AsCurrency := ExcelSheet.Cells[i,13].Value; //수량소계
      qryGoods.FieldByName('STQTY_G').AsString := ExcelSheet.Cells[i,14].Value; //수량소계단위
      qryGoods.FieldByName('STAMT').AsCurrency := ExcelSheet.Cells[i,15].Value; //수금액소계
      qryGoods.FieldByName('STAMT_G').AsString := ExcelSheet.Cells[i,16].Value; //금액소계단위

      qryGoods.Post;
    end;
    //ShowMessage( ExcelSheet.cells[1,2].value ) ;

    //접근제어------------------------------------------------------------------
    GoodsButton(True); //버튼정리;
    EnabledControlValue(sPanel21);
    //새로고침------------------------------------------------------------------
    qryGoods.Close;
    qrygoods.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
    qryGoods.Open;

  finally
    ExcelBook.Close;
    ExcelApp.Quit;
    ExcelApp := Unassigned;
    Finalize(ExcelSheet);
    Finalize(ExcelBook);
    Finalize(ExcelApp);
  end;
end;

procedure TUI_LOCRC2_frm.sSpeedButton13Click(Sender: TObject);  
var
   ExcelApp , ExcelBook , ExcelSheet : Variant;
   i : Integer;
begin
  //상품내역 엑셀샘플----------------------------------------------------------
  inherited;
  try
    ExcelApp := CreateOleObject('Excel.Application');
  except
    sShowMessage('Excel이 설치되어 있지 않습니다.');
    Exit;
  end;

  ExcelApp.WorkBooks.Add; //새로운페이지 생성
  ExcelBook := ExcelApp.ActiveWorkBook; //워크북 추가
//  ExcelBook.Sheets.Add; //워크시트추가

  try
    ExcelSheet := ExcelBook.WorkSheets[1]; //작업할 워크시트 선택

    //셀에 타이틀에 들어갈 내용들 입력
    ExcelSheet.Cells[1,1].Value := '품명코드(선택)';
    ExcelSheet.Cells[1,2].Value := '품      명';
    ExcelSheet.Cells[1,3].Value := '규격(선택)';
    ExcelSheet.Cells[1,4].Value := 'HS부호(숫자만입력)';
    ExcelSheet.Cells[1,5].Value := '수      량';
    ExcelSheet.Cells[1,6].Value := '수량단위';
    ExcelSheet.Cells[1,7].Value := '단      가';
    ExcelSheet.Cells[1,8].Value := '단가통화';
    ExcelSheet.Cells[1,9].Value := '단가기준수량(선택)';
    ExcelSheet.Cells[1,10].Value := '단가기준수량단위(선택)';
    ExcelSheet.Cells[1,11].Value := '금     액';
    ExcelSheet.Cells[1,12].Value := '금액통화';
    ExcelSheet.Cells[1,13].Value := '수량소계';
    ExcelSheet.Cells[1,14].Value := '수량소계단위';
    ExcelSheet.Cells[1,15].Value := '금액소계';
    ExcelSheet.Cells[1,16].Value := '금액소계통화';

    //셀정렬
    for i := 1 to ExcelApp.ActiveSheet.UsedRange.Columns.count+1 do
    begin
      ExcelApp.Range[ExcelSheet.Cells[1,i],ExcelSheet.Cells[1,i]].HorizontalAlignment := xlHAlignCenter;

      //2018년 2월 2일 : 미그에선 품명만 필수이지만 김과장님께서 은행쪽에선 다받아서 처리한다고함  
      if (i = 1) or ( i = 3) or (i = 9) or (i = 10) then Continue;
      
      ExcelApp.Range[ExcelSheet.Cells[1,i],ExcelSheet.Cells[1,i]].font.bold := True; //폰트변경
    end;
    ExcelSheet.Columns.AutoFit; //Cell 사이즈 변경

    ExcelApp.Visible := True; //엑셀보이기
  //ExcelApp.DisplayAlerts := True; //메시지표시

  finally
    ExcelBook.Close;
    ExcelBook := Unassigned;
    ExcelSheet := Unassigned;
  end;

end;

procedure TUI_LOCRC2_frm.taxExcelBtnClick(Sender: TObject);
var
  ExcelApp , ExcelBook , ExcelSheet : Variant;
  ExcelfileName : String;
  ExcelRows, ExcelCols, StartRow : Integer;
  i : Integer;
begin
  //세금계산서 엑셀가져오기-------------------------------------------------------
  inherited;
  try
    ExcelApp := CreateOleObject('Excel.Application');
  except
    sShowMessage('Excel이 설치되어 있지 않습니다.');
    Exit;
  end;

  //excelOpen.Filter := 'All Files (*.*)|*.*|Microsoft Excel 통합문서(*.xls)|*.xls|엑셀2007(*.xlsx)|*.xlsx';

  if not excelOpen.Execute then Exit;

  //excelOpen.FileName을 사용해도 되지만 윈도우상에 버그로 글씨가 깨지거나함
  ExcelfileName := excelOpen.Files.Strings[0]; //선택된 엑셀파일이름

  try
    with TADOQuery.Create(nil) do
    begin
      Connection := DMMssql.KISConnect;
      try
        SQL.Text :=  'DELETE FROM LOCRC2_TAX WHERE KEYY =' + QuotedStr(edt_MAINT_NO.Text);
        ExecSQL;

        qryTax.Close;
        qryTax.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
        qryTax.Open;

      finally
        Close;
        Free;
      end;
    end;

    ExcelApp.Visible := False; //엑셀보이지 않기
    ExcelApp.DisplayAlerts := False; //메시지표시않기

    ExcelBook := ExcelApp.WorkBooks.Open(ExcelfileName);
    ExcelSheet := ExcelBook.Sheets['Sheet1'];

    ExcelRows := ExcelApp.ActiveSheet.UsedRange.Rows.count; //총 row수 구하기
    ExcelCols := ExcelApp.ActiveSheet.UsedRange.Columns.count; //총 Col수 구하기

    for i := 2 to ExcelRows do
    begin
      qryTax.Append;

      qryTax.FieldByName('KEYY').AsString := edt_MAINT_NO.Text;
      qryTax.FieldByName('SEQ').AsInteger := i-1;
      qryTax.FieldByName('BILL_DATE').AsString := ExcelSheet.Cells[i,1].Value; //작성일자
      qryTax.FieldByName('BILL_NO').AsString := ExcelSheet.Cells[i,2].Value; //세금계산서번호
      qryTax.FieldByName('BILL_AMOUNT').AsCurrency := ExcelSheet.Cells[i,3].Value; //공급가액
      qryTax.FieldByName('BILL_AMOUNT_UNIT').AsString := 'KRW';
      qryTax.FieldByName('TAX_AMOUNT').AsCurrency := ExcelSheet.Cells[i,4].Value; //세액
      qryTax.FieldByName('TAX_AMOUNT_UNIT').AsString := 'KRW';

      qryTax.Post;
    end;
    //ShowMessage( ExcelSheet.cells[1,2].value ) ;

    //접근제어------------------------------------------------------------------
    TaxButton(True); //버튼정리;
    EnabledControlValue(sPanel28);
    //새로고침------------------------------------------------------------------
    qryTax.Close;
    qryTax.Parameters.ParamByName('MAINT_NO').Value := edt_MAINT_NO.Text;
    qryTax.Open;

  finally
    ExcelBook.Close;
    ExcelApp.Quit;
    ExcelApp := Unassigned;
    Finalize(ExcelSheet);
    Finalize(ExcelBook);
    Finalize(ExcelApp);
  end;
end;

procedure TUI_LOCRC2_frm.taxSampleBtnClick(Sender: TObject);
var
   ExcelApp , ExcelBook , ExcelSheet : Variant;
   i : Integer;
begin
  //세금계산서 엑셀샘플----------------------------------------------------------
  inherited;
  try
    ExcelApp := CreateOleObject('Excel.Application');
  except
    sShowMessage('Excel이 설치되어 있지 않습니다.');
    Exit;
  end;

  ExcelApp.WorkBooks.Add; //새로운페이지 생성
  ExcelBook := ExcelApp.ActiveWorkBook; //워크북 추가
//  ExcelBook.Sheets.Add; //워크시트추가

  try
    ExcelSheet := ExcelBook.WorkSheets[1]; //작업할 워크시트 선택

    //셀에 타이틀에 들어갈 내용들 입력
    ExcelSheet.Cells[1,1].Value := '작성일자';
    ExcelSheet.Cells[1,2].Value := '세금계산서번호';
    ExcelSheet.Cells[1,3].Value := '공급가액';
    ExcelSheet.Cells[1,4].Value := '세액';

    //셀정렬
    for i := 1 to ExcelApp.ActiveSheet.UsedRange.Columns.count+1 do
    begin
      ExcelApp.Range[ExcelSheet.Cells[1,i],ExcelSheet.Cells[1,i]].HorizontalAlignment := xlHAlignCenter;
      ExcelApp.Range[ExcelSheet.Cells[1,i],ExcelSheet.Cells[1,i]].font.bold := True; //폰트변경
    end;
    ExcelSheet.Columns.AutoFit; //Cell 사이즈 변경

    ExcelApp.Visible := True; //엑셀보이기
  //ExcelApp.DisplayAlerts := True; //메시지표시

  finally
    ExcelBook.Close;
    ExcelBook := Unassigned;
    ExcelSheet := Unassigned;
  end;

end;

procedure TUI_LOCRC2_frm.DialogKey(var msg: TCMDialogKey);
begin
  if ActiveControl = nil then Exit;

  if (ActiveControl.Name = 'memo_LOC_REM1') and (msg.CharCode = VK_TAB) then
  begin
    sPageControl1.ActivePageIndex := 1;
    Self.KeyPreview := False;
  end
  else
  begin
    Self.KeyPreview := True;
    inherited;
  end;
end;

end.

