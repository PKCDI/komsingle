unit UI_LOCAMR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_LOCAMR_BP, DB, ADODB, sSkinProvider, StdCtrls, sMemo,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, sLabel, Grids, DBGrids,
  acDBGrid, Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit, ComCtrls,
  sPageControl, sButton, sSpeedButton, ExtCtrls, sPanel, sSplitter, strutils,
  sCheckBox, Clipbrd, DateUtils, Menus;

type
  TUI_LOCAMR_frm = class(TUI_LOCAMR_BP_frm)
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListLC_NO: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListAPP_DATE: TStringField;
    qryListISS_DATE: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListISSBANK: TStringField;
    qryListISSBANK1: TStringField;
    qryListISSBANK2: TStringField;
    qryListAPPLIC: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListCHGINFO: TStringField;
    qryListCHGINFO1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC_AMT: TBCDField;
    qryListLOC_AMTC: TStringField;
    qryListCHKNAME1: TStringField;
    qryListCHKNAME2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    sDBGrid2: TsDBGrid;
    sp_attachLOCAMRfromLOCADV: TADOStoredProc;
    qryListLOC_TYPENAME: TStringField;
    sLabel16: TsLabel;
    sLabel2: TsLabel;
    sLabel7: TsLabel;
    sLabel3: TsLabel;
    sSpeedButton8: TsSpeedButton;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sPanel13: TsPanel;
    Mask_fromDate: TsMaskEdit;
    Mask_toDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    sp_attachLOCAMRfromLOCAMA: TADOStoredProc;
    qryReady: TADOQuery;
    popMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    N4: TMenuItem;
    N7: TMenuItem;
    d1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    sPanel11: TsPanel;
    sPanel14: TsPanel;
    procedure FormShow(Sender: TObject);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryListCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sBitBtn1Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sPageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure edt_APPLICDblClick(Sender: TObject);
    procedure edt_ISSBANKDblClick(Sender: TObject);
    procedure sBitBtn6Click(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnEditClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure sDBGrid1TitleClick(Column: TColumn);
    procedure memo_REMARK1Change(Sender: TObject);
    procedure memo_REMARK1KeyPress(Sender: TObject; var Key: Char);
    procedure sBitBtn22Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
  private
    procedure AttachDocument;
    function ReadList(OrderString: String=''): Integer; override;
    function ReadListBetween(fromDate: String; toDate: String; KeyValue : string;KeyCount : integer=0): Boolean; override;
    procedure ReadDocument; override;
    procedure SaveDocument(Sender : TObject); override;
    procedure NewDocument; override;
    procedure CancelDocument; override;
    procedure EditDocument; override;
    function ValidData: Boolean; override;

    procedure ReadyDocument(DocNo : String);

  protected
    procedure DeleteDocument; override;
  public
    { Public declarations }
  end;

var
  UI_LOCAMR_frm: TUI_LOCAMR_frm;

implementation

uses Commonlib, TypeDefine, Dialog_CodeList, Dialog_SearchCustom,
  Dialog_BANK, dlg_CreateDocumentChoice_LOCAMR, Dialog_AttachFromLOCADV,
  MSSQL, MessageDefine, VarDefine, SQLCreator, AutoNo, LOCAMR_PRINT, Dialog_AttachFromLOCAMA,
  CreateDocuments, DocumentSend, Dlg_RecvSelect ;

{$R *.dfm}

{ TUI_LOCAMR_frm }

function TUI_LOCAMR_frm.ReadList(OrderString: String): Integer;
begin
// 0 변경신청일
// 1 신용장번호
// 2 개설은행
  with qryList do
  begin
    Close;
    SQL.Text := FormSQL;
    Case com_SearchKeyword.ItemIndex of
      0:
      begin
        ReadListBetween(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'',0);
        Exit;
      end;
      1: SQL.Add('WHERE LC_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE ISSBANK1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    IF OrderString <> '' then
    begin
      SQL.Add(OrderString);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ');
    end;

    Open;
  end;
end;

procedure TUI_LOCAMR_frm.FormShow(Sender: TObject);
begin
  inherited;
  FormSQL := qryList.SQL.Text;
  WorkStyle := '신청서';
  Self.Caption := '내국신용장 조건변경 신청서';

  ProgramControlType := ctView;

  sPageControl1.ActivePageIndex := 0;
  ClearControlValue(sPanel2);
  //ReadOnlyControlValue(sPanel2);
  EnabledControlValue(sPanel2);
  EnabledControlValue(sPanel4);

  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);

  ReadListBetween(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'');
end;

procedure TUI_LOCAMR_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;
begin
  inherited;
  nIndex := StrToIntDef( qryListCHK2.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TUI_LOCAMR_frm.qryListCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;  
begin
  inherited;

  IF Length(qryListCHK3.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(qryListCHK3.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;
end;

procedure TUI_LOCAMR_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_LOCAMR_frm.ReadDocument;
begin
  inherited;
  IF not qryList.Active Then Exit;
  IF qryList.RecordCount = 0 Then Exit;

//------------------------------------------------------------------------------
// 기본정보
//------------------------------------------------------------------------------
  //관리번호
  edt_DocNo1.Text := qryListMAINT_NO.AsString;
  edt_DocNo2.Text := qryListMSEQ.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //등록일자
  sMaskEdit1.Text := qryListDATEE.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;
  //조건변경회차
  edt_AMD_NO.Text := qryListAMD_NO.AsString;
  //개설일자
  mask_ISS_DATE.Text := qryListISS_DATE.AsString;
  //변경신청일자
  mask_APP_DATE.Text := qryListAPP_DATE.AsString;
  //변경후 물품인도기일
  mask_DELIVERY.Text := qryListDELIVERY.AsString;
  //변경후 유효기일
  mask_EXPIRY.Text := qryListEXPIRY.AsString;
//------------------------------------------------------------------------------
// 개설의뢰인
//------------------------------------------------------------------------------
  //개설의뢰인코드
  edt_APPLIC.Text := qryListAPPLIC.AsString;
  //상호
  edt_APPLIC1.Text := qryListAPPLIC1.AsString;
  //대표자
  edt_APPLIC2.Text := qryListAPPLIC2.AsString;
  //주소
  edt_APPADDR1.Text := qryListAPPADDR1.AsString;
  edt_APPADDR2.Text := qryListAPPADDR2.AsString;
  edt_APPADDR3.Text := qryListAPPADDR3.AsString;
  //사업자등록번호
  edt_APPLIC3.Text := qryListAPPLIC3.AsString;
//------------------------------------------------------------------------------
// 수혜자
//------------------------------------------------------------------------------
  //수혜자코드
  edt_BENEFC.Text := qryListBENEFC.AsString;
  //상호
  edt_BENEFC1.Text := qryListBENEFC1.AsString;
  //대표자
  edt_BENEFC2.Text := qryListBENEFC2.AsString;
  //주소
  edt_BNFADDR1.Text := qryListBNFADDR1.AsString;
  edt_BNFADDR2.Text := qryListBNFADDR2.AsString;
  edt_BNFADDR3.Text := qryListBNFADDR3.AsString;
  //사업자등록번호
  edt_BENEFC3.Text := qryListBENEFC3.AsString;
  //이메일
  edt_BNFMAILID.Text := qryListBNFEMAILID.AsString;
  edt_BNFDOMAIN.Text := qryListBNFDOMAIN.AsString;
  //식별자
  edt_CHKNAME1.Text := qryListCHKNAME1.AsString;
  edt_CHKNAME2.Text := qryListCHKNAME2.AsString;
//------------------------------------------------------------------------------
// 개설은행
//------------------------------------------------------------------------------
  //개설은행
  edt_ISSBANK.Text  := qryListISSBANK.AsString;
  edt_ISSBANK1.Text := qryListISSBANK1.AsString;
  edt_ISSBANK2.Text := qryListISSBANK2.AsString;
  //신용장종류
  edt_LOC_TYPE.Text := qryListLOC_TYPE.AsString;
  //변경후개설금액
  edt_LOC_AMTC.Text := qryListLOC_AMTC.AsString;
  edt_LOC_AMT.Value := qryListLOC_AMT.AsCurrency;
  //허용오차
  edt_CD_PERP.Text := qryListCD_PERP.AsString;
  edt_CD_PERM.Text := qryListCD_PERM.AsString;
  //내국신용장번호
  edt_LC_NO.Text := qryListLC_NO.AsString;
//------------------------------------------------------------------------------
// 매도확약서
//------------------------------------------------------------------------------
  edt_OFFERNO1.Text := qryListOFFERNO1.AsString;
  edt_OFFERNO2.Text := qryListOFFERNO2.AsString;
  edt_OFFERNO3.Text := qryListOFFERNO3.AsString;
  edt_OFFERNO4.Text := qryListOFFERNO4.AsString;
  edt_OFFERNO5.Text := qryListOFFERNO5.AsString;
  edt_OFFERNO6.Text := qryListOFFERNO6.AsString;
  edt_OFFERNO7.Text := qryListOFFERNO7.AsString;
  edt_OFFERNO8.Text := qryListOFFERNO8.AsString;
  edt_OFFERNO9.Text := qryListOFFERNO9.AsString;
//------------------------------------------------------------------------------
// 기타정보
//------------------------------------------------------------------------------
  memo_REMARK1.Lines.Text := qryListREMARK1.AsString;
//------------------------------------------------------------------------------
// 기타조건변경사항
//------------------------------------------------------------------------------
  memo_CHGINFO.Lines.Text := qryListCHGINFO1.AsString;
//------------------------------------------------------------------------------
// 발신기관 전자서명
//------------------------------------------------------------------------------
  edt_EXNAME1.Text := qryListEXNAME1.AsString;
  edt_EXNAME2.Text := qryListEXNAME2.AsString;
  edt_EXNAME3.Text := qryListEXNAME3.AsString;
end;

procedure TUI_LOCAMR_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadDocument;
  //메모 행열 출력
  sLabel3.Caption := '';
  sLabel3.Caption := '';
end;

procedure TUI_LOCAMR_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  If DataSet.RecordCount = 0 Then ReadDocument;
  btnDel.Enabled := DataSet.RecordCount > 0;
  btnPrint.Enabled := DataSet.RecordCount > 0;
  sButton1.Enabled := DataSet.RecordCount > 0;
end;

procedure TUI_LOCAMR_frm.sPageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
//  AllowChange := qryList.RecordCount > 0;
end;

procedure TUI_LOCAMR_frm.NewDocument;
var
  DocNo : String;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctInsert;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(false);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex := 0;
  sTabSheet3.Enabled := False;
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  ClearControlValue(sPanel2);
//  ReadOnlyControlValue(sPanel4,false);
//  ReadOnlyControlValue(sPanel2,false);
  EnabledControlValue(sPanel4,False);
  EnabledControlValue(sPanel2,False);

//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Append;

//------------------------------------------------------------------------------
// 관리번호를 직접입력해야한다.
//------------------------------------------------------------------------------
  //DocNo := DMAutoNo.GetDocumentNo('LOCAMR');
  //DocNo := DMAutoNo.GetDocumentNoAutoInc('LOCAMR');
  //edt_DocNo1.Text := DocNo;

  //------------------------------------------------------------------------------
  // 기초자료
  //------------------------------------------------------------------------------
  qryListCHK2.AsInteger := 0;
  qryListMAINT_NO.AsString := DocNo;
  edt_DocNo2.Text := '1';
  edt_UserNo.Text := LoginData.sID;
  edt_AMD_NO.Text := '1';
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
  mask_APP_DATE.Text := FormatDateTime('YYYYMMDD',Now);
  edt_msg1.Text := '9';
  edt_msg2.Text := 'AB';

  //신규등록시 관리번호와 내국신용장번호 입력이가능하도록함.

  edt_DocNo1.Text := '';
  edt_DocNo1.ReadOnly := False;
  edt_DocNo1.Color := clWhite;

  edt_LC_NO.ReadOnly := False;
  edt_LC_NO.Color := clWhite;

//  IF DocNo = '' Then
//  begin
//    edt_DocNo1.ReadOnly := False;
//    edt_DocNo1.Color := clWhite;
//  end
//  else
//  begin
//    edt_DocNo1.Text := DocNo;
//    edt_DocNo1.ReadOnly := True;
//    edt_DocNo1.Color := clBtnFace;
//    //------------------------------------------------------------------------------
//    // 기초자료
//    //------------------------------------------------------------------------------
//    qryListCHK2.AsInteger := 0;
//    qryListMAINT_NO.AsString := DocNo;
//    edt_DocNo2.Text := '1';
//    edt_UserNo.Text := LoginData.sID;
//    edt_AMD_NO.Text := '1';
//    sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
//    mask_APP_DATE.Text := FormatDateTime('YYYYMMDD',Now);
//    edt_msg1.Text := '9';
//    edt_msg2.Text := 'AB';
//  end;
end;

procedure TUI_LOCAMR_frm.SaveDocument(Sender : TObject);
var
  SQLCreate : TSQLCreate;
  DocNo,DocCount : String;
  CreateDate : TDateTime;

begin
//------------------------------------------------------------------------------
// 데이터저장
//------------------------------------------------------------------------------
  try
//------------------------------------------------------------------------------
// 유효성 검사
//------------------------------------------------------------------------------
    IF (Sender as TsButton).Tag = 1 Then
    begin
      IF ValidData Then Exit;
    end;

//------------------------------------------------------------------------------
// SQL생성기
//------------------------------------------------------------------------------
    SQLCreate := TSQLCreate.Create;
    try
      with SQLCreate do
      begin
        DocNo := edt_DocNo1.Text;
        DocCount := edt_DocNo2.Text;
        
        Case ProgramControlType of
          ctInsert :
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO',DocNo);
            ADDValue('MSEQ',DocCount);
          end;

          ctModify :
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO',DocNo);
            ADDWhere('MSEQ',DocCount);
          end;
        end;

        //------------------------------------------------------------------------------
        // 문서헤더
        //------------------------------------------------------------------------------
        SQLHeader('LOCAMR');        

        //문서저장구분(0:임시, 1:저장)
        ADDValue('CHK2',IntToStr((Sender as TsButton).Tag));
        //ShowMessage( IntToStr((Sender as TsButton).Tag) );

        //등록일자
        ADDValue('DATEE',sMaskEdit1.Text);

        //유저ID
        ADDValue('USER_ID',edt_UserNo.Text);

        //메세지1
        ADDValue('MESSAGE1',edt_msg1.Text);
        //메세지2
        ADDValue('MESSAGE2',edt_msg2.Text);

        //------------------------------------------------------------------------------
        // 개설의뢰인
        //------------------------------------------------------------------------------
        //개설의뢰인코드
        ADDValue('APPLIC',edt_APPLIC.Text);
        //상호
        ADDValue('APPLIC1',edt_APPLIC1.Text);
        //대표자명
        ADDValue('APPLIC2',edt_APPLIC2.Text);
        //주소
        ADDValue('APPADDR1',edt_APPADDR1.Text);
        ADDValue('APPADDR2',edt_APPADDR2.Text);
        ADDValue('APPADDR3',edt_APPADDR3.Text);
        //사업자등록번호
        ADDValue('APPLIC3',edt_APPLIC3.Text);

        //------------------------------------------------------------------------------
        // 수혜자
        //------------------------------------------------------------------------------
        //수혜자코드
        ADDValue('BENEFC',edt_BENEFC.Text);
        //상호
        ADDValue('BENEFC1',edt_BENEFC1.Text);
        //대표자
        ADDValue('BENEFC2',edt_BENEFC2.Text);
        //수혜자주소
        ADDValue('BNFADDR1',edt_BNFADDR1.Text);
        ADDValue('BNFADDR2',edt_BNFADDR2.Text);
        ADDValue('BNFADDR3',edt_BNFADDR3.Text);
        //사업자등록번호
        ADDValue('BENEFC3',edt_BENEFC3.Text);
        //이메일
        ADDValue('BNFEMAILID',edt_BNFMAILID.Text);
        ADDValue('BNFDOMAIN',edt_BNFDOMAIN.Text);
        //수발신인식별자
        ADDValue('CHKNAME1',edt_CHKNAME1.Text);
        //수발신인 상세 식별자
        ADDValue('CHKNAME2',edt_CHKNAME2.Text);

        //------------------------------------------------------------------------------
        // 개설은행/신용장
        //------------------------------------------------------------------------------
        //개설은행
        ADDValue('ISSBANK',edt_ISSBANK.Text);
        ADDValue('ISSBANK1',edt_ISSBANK1.Text);
        ADDValue('ISSBANK2',edt_ISSBANK2.Text);
        //신용장종류
        ADDValue('LOC_TYPE',edt_LOC_TYPE.Text);
        //개설금액(단위/금액)
        ADDValue('LOC_AMTC',edt_LOC_AMTC.Text);
        ADDValue('LOC_AMT',edt_LOC_AMT.Text);
        //허용오차
        ADDValue('CD_PERP',edt_CD_PERP.Text);
        ADDValue('CD_PERM',edt_CD_PERM.Text);
        //개설근거별용도
//        ADDValue('BUSINESS', sEdit25.Text);
        //L/C번호
        ADDValue('LC_NO',edt_LC_NO.Text);

        //------------------------------------------------------------------------------
        // 물품매도확약서
        //------------------------------------------------------------------------------
        ADDValue('OFFERNO1',edt_OFFERNO1.Text);
        ADDValue('OFFERNO2',edt_OFFERNO2.Text);
        ADDValue('OFFERNO3',edt_OFFERNO3.Text);
        ADDValue('OFFERNO4',edt_OFFERNO4.Text);
        ADDValue('OFFERNO5',edt_OFFERNO5.Text);
        ADDValue('OFFERNO6',edt_OFFERNO6.Text);
        ADDValue('OFFERNO7',edt_OFFERNO7.Text);
        ADDValue('OFFERNO8',edt_OFFERNO8.Text);
        ADDValue('OFFERNO9',edt_OFFERNO9.Text);

        //------------------------------------------------------------------------------
        // 개설관련
        //------------------------------------------------------------------------------
        //조건변경회차
        ADDValue('AMD_NO',edt_AMD_NO.Text);
        //개설일자
        ADDValue('ISS_DATE',mask_ISS_DATE.Text);
        //변경신청일자
        ADDValue('APP_DATE',mask_APP_DATE.Text);
        //변경 후 인도기일
        ADDValue('DELIVERY',mask_DELIVERY.Text);
        //변경 후 유효기일
        ADDValue('EXPIRY',mask_EXPIRY.Text);

        //------------------------------------------------------------------------------
        // 기타정보, 기타조건변경사항
        //------------------------------------------------------------------------------
        ADDValue('REMARK',IntToStr(memo_REMARK1.Lines.count));
        ADDValue('REMARK1',memo_REMARK1.Lines.Text);
        ADDValue('CHGINFO',IntToStr(memo_CHGINFO.Lines.count));
        ADDValue('CHGINFO1',memo_CHGINFO.Lines.Text);

        //------------------------------------------------------------------------------
        // 발신기관전자서명
        //------------------------------------------------------------------------------
        ADDValue('EXNAME1',edt_EXNAME1.Text);
        ADDValue('EXNAME2',edt_EXNAME2.Text);
        ADDValue('EXNAME3',edt_EXNAME3.Text);
      end;

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate.CreateSQL;
//          If sCheckBox1.Checked Then
//            Clipboard.AsText := SQL.Text;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;
    finally
      SQLCreate.Free;
    end;

    IF DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.CommitTrans;
  //------------------------------------------------------------------------------
  // 프로그램제어
  //------------------------------------------------------------------------------
    ProgramControlType := ctView;
  //------------------------------------------------------------------------------
  // 접근제어
  //------------------------------------------------------------------------------
    sDBGrid1.Enabled := True;
    sDBGrid2.Enabled := True;
    sTabSheet3.Enabled := True;

  //------------------------------------------------------------------------------
  // 읽기전용
  //------------------------------------------------------------------------------
//    ReadOnlyControlValue(sPanel4);
//    ReadOnlyControlValue(sPanel2);
    EnabledControlValue(sPanel4);
    EnabledControlValue(sPanel2);

    //데이터를 불러오지 않고 신규작성시 관리번호와 내국신용장번호가 입력가능하므로 저장시 다시변경
    edt_DocNo1.ReadOnly := True;
    edt_DocNo1.Color := clBtnFace;

    edt_LC_NO.ReadOnly := True;
    edt_LC_NO.Color := clBtnFace;

  //------------------------------------------------------------------------------
  // 버튼설정
  //------------------------------------------------------------------------------
    ButtonEnable(True);

  //------------------------------------------------------------------------------
  // 새로고침
  //------------------------------------------------------------------------------
    CreateDate := ConvertStr2Date(sMaskEdit1.Text);
    ReadListBetween(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             DocNo,StrToInt(DocCount));

    MessageBox(Self.Handle,MSG_SYSTEM_SAVE_OK,'문서작성완료',MB_OK+MB_ICONINFORMATION);
  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message),'저장오류',MB_OK+MB_ICONERROR);
//      DMMssql.KISConnect.RollbackTrans;
    end;
  end;

end;

procedure TUI_LOCAMR_frm.CancelDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(true);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
//  sPageControl1.ActivePageIndex := 1;
  sTabSheet3.Enabled := True;
  sDBGrid1.Enabled := True;
  sDBGrid2.Enabled := True;

  //데이터를 불러오지 않고 신규작성시 관리번호와 내국신용장번호가 입력가능하므로 취소시 다시변경
  edt_DocNo1.ReadOnly := True;
  edt_DocNo1.Color := clBtnFace;

  edt_LC_NO.ReadOnly := True;
  edt_LC_NO.Color := clBtnFace;

//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
//  ReadOnlyControlValue(sPanel2,True);
  EnabledControlValue(sPanel2,True);
  EnabledControlValue(sPanel4,True);
//------------------------------------------------------------------------------
// 만약 트랜잭션이 잡혀있다면 캔슬
//------------------------------------------------------------------------------
  IF DMMssql.KISConnect.InTransaction Then DMMssql.RollbackTrans;
  qryList.Cancel;  
//------------------------------------------------------------------------------
// 새로고침
//------------------------------------------------------------------------------
  ReadList;
end;

procedure TUI_LOCAMR_frm.btnNewClick(Sender: TObject);
var
  DocNo : String;
begin
  inherited;
  dlg_CreateDocumentChoice_LOCAMR_frm := Tdlg_CreateDocumentChoice_LOCAMR_frm.Create(Self);
  try
    Case dlg_CreateDocumentChoice_LOCAMR_frm.ShowModal of
      //신규등록
      mrRetry : NewDocument;
      //응답서에서 선택
      mrOk : AttachDocument;
      //조건변경통지서에서 선택
      mrYes : AttachDocument;
    end;

  finally
    FreeAndNil( dlg_CreateDocumentChoice_LOCAMR_frm );
  end;
end;

procedure TUI_LOCAMR_frm.btnCancelClick(Sender: TObject);
begin
  inherited;
  CancelDocument;
end;

procedure TUI_LOCAMR_frm.edt_APPLICDblClick(Sender: TObject);
begin
  inherited;
  If (Sender as TsEdit).ReadOnly Then Exit;

  Dialog_SearchCustom_frm := TDialog_SearchCustom_frm.Create(Application);
  try
    IF Dialog_SearchCustom_frm.openDialog((Sender as TsEdit).Text) = mrOk then
    begin
      IF (Sender as TsEdit).Tag = 0 Then
      begin
        edt_APPLIC.Text   := Dialog_SearchCustom_frm.qryList.FieldByName('CODE').AsString;
        edt_APPLIC1.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ENAME').AsString;
        edt_APPLIC2.Text := Dialog_SearchCustom_frm.qryList.FieldByName('REP_NAME').AsString;
        edt_APPADDR1.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR1').AsString;
        edt_APPADDR2.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR2').AsString;
        edt_APPADDR3.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR3').AsString;
        edt_APPLIC3.Text := Dialog_SearchCustom_frm.qryList.FieldByName('SAUP_NO').AsString;
      end
      else
      IF (Sender as TsEdit).Tag = 1 Then
      begin
        edt_BENEFC.Text   := Dialog_SearchCustom_frm.qryList.FieldByName('CODE').AsString;
        edt_BENEFC1.Text := Dialog_SearchCustom_frm.qryList.FieldByName('ENAME').AsString;
        edt_BENEFC2.Text := Dialog_SearchCustom_frm.qryList.FieldByName('REP_NAME').AsString;
        edt_BNFADDR1.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR1').AsString;
        edt_BNFADDR2.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR2').AsString;
        edt_BNFADDR3.Text  := Dialog_SearchCustom_frm.qryList.FieldByName('ADDR3').AsString;
        edt_BENEFC3.Text   := Dialog_SearchCustom_frm.qryList.FieldByName('SAUP_NO').AsString;
        edt_BNFMAILID.Text := Dialog_SearchCustom_frm.qryList.FieldByName('EMAIL_ID').AsString;
        edt_BNFDOMAIN.Text := Dialog_SearchCustom_frm.qryList.FieldByName('EMAIL_DOMAIN').AsString;
      end
    end;
  finally
    FreeAndNil(Dialog_SearchCustom_frm);
  end;
end;

procedure TUI_LOCAMR_frm.edt_ISSBANKDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  Dialog_BANK_frm := TDialog_BANK_frm.Create(Self);
  try
    If Dialog_BANK_frm.openDialog(edt_ISSBANK.Text) = mrOk Then
    begin
      edt_ISSBANK.Text := Dialog_BANK_frm.qryBank.FieldByName('CODE').AsString;
      edt_ISSBANK1.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKNAME1').AsString;
      edt_ISSBANK2.Text := Dialog_BANK_frm.qryBank.FieldByName('BANKBRANCH1').AsString;
    end;
  finally
    FreeAndNil(Dialog_BANK_frm);
  end;
end;

procedure TUI_LOCAMR_frm.sBitBtn6Click(Sender: TObject);
begin
  inherited;
  edt_ISSBANKDblClick(edt_ISSBANK);
end;

procedure TUI_LOCAMR_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCAMR_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LOCAMR_frm := nil;
end;

procedure TUI_LOCAMR_frm.AttachDocument;
var
  DocNo : string;
  DocCount : Integer;
begin

  if dlg_CreateDocumentChoice_LOCAMR_frm.ModalResult = mrOk then
  begin
    Dialog_AttachFromLOCADV_frm := TDialog_AttachFromLOCADV_frm.Create(Self);
    try
      DocNo := Dialog_AttachFromLOCADV_frm.openDialog;

      IF Trim(DocNo) = '' Then Exit;
      //------------------------------------------------------------------------------
      // COPYLOCAPP
      //------------------------------------------------------------------------------
      with sp_attachLOCAMRfromLOCADV do
      begin
        Close;
        Parameters.ParamByName('@DocNo').Value := DocNo;
        Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

        IF not DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;

        try
          ExecProc;
          DocCount := Parameters.ParamByName('@DOC_COUNT').Value;
        except
          on E:Exception do
          begin
            DMMssql.KISConnect.RollbackTrans;
            MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+E.Message),'복사오류',MB_OK+MB_ICONERROR);
          end;
        end;
      end;

      if ReadListBetween(FormatDateTime('YYYYMMDD',Now),FormatDateTime('YYYYMMDD',Now),DocNo,DocCount) Then
      begin
        EditDocument;
        edt_DocNo1.ReadOnly := True;
        edt_DocNo2.ReadOnly := True;
      end
      else
        raise Exception.Create('복사한 데이터를 찾을 수 없습니다');

  //    MessageBox(Self.Handle,MSG_SYSTEM_COPY_OK,'복사완료',MB_OK+MB_ICONINFORMATION);

    finally
      FreeAndNil( Dialog_AttachFromLOCADV_frm );
    end;
  end
  else if dlg_CreateDocumentChoice_LOCAMR_frm.ModalResult = mrYes then
  begin
    Dialog_AttachFromLOCAMA_frm := TDialog_AttachFromLOCAMA_frm.Create(Self);
    try
      DocNo := Dialog_AttachFromLOCAMA_frm.openDialog;

      IF Trim(DocNo) = '' Then Exit;
      //------------------------------------------------------------------------------
      // COPYLOCAPP
      //------------------------------------------------------------------------------
      with sp_attachLOCAMRfromLOCAMA do
      begin
        Close;
        Parameters.ParamByName('@DocNo').Value := DocNo;
        Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

        IF not DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;

        try
          ExecProc;
          DocCount := Parameters.ParamByName('@DOC_COUNT').Value;
        except
          on E:Exception do
          begin
            DMMssql.KISConnect.RollbackTrans;
            MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+E.Message),'복사오류',MB_OK+MB_ICONERROR);
          end;
        end;
      end;

      if ReadListBetween(FormatDateTime('YYYYMMDD',Now),FormatDateTime('YYYYMMDD',Now),DocNo,DocCount) Then
      begin
        EditDocument;
        edt_DocNo1.ReadOnly := True;
        edt_DocNo2.ReadOnly := True;
      end
      else
        raise Exception.Create('복사한 데이터를 찾을 수 없습니다');

      
    finally
      FreeAndNil(Dialog_AttachFromLOCAMA_frm);
    end;
  end;



end;

function TUI_LOCAMR_frm.ReadListBetween(fromDate, toDate, KeyValue: String; KeyCount : integer): Boolean;
begin

  Result := False;
  with qryList do
  begin
    Close;
    SQL.Text := FormSQL;
    SQL.Add('WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('ORDER BY DATEE ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO;MSEQ',VarArrayOf([KeyValue,KeyCount]),[]);
    end;

  end;
end;

procedure TUI_LOCAMR_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  if not qrylist.Active then Exit;

  EditDocument;
end;

procedure TUI_LOCAMR_frm.EditDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctModify;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(false);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex := 0;
  sTabSheet3.Enabled := False;
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
//  ClearControlValue(sPanel2);
//  ReadOnlyControlValue(sPanel4,false);
//  ReadOnlyControlValue(sPanel2,false);
  EnabledControlValue(sPanel4,False);
  EnabledControlValue(sPanel2,False);
end;

procedure TUI_LOCAMR_frm.btnSaveClick(Sender: TObject);
begin
  inherited;
//  ShowMessage(IntToStr((Sender as TsButton).Tag));
  SaveDocument(Sender);
end;

function TUI_LOCAMR_frm.ValidData: Boolean;
begin
  Result := False;
end;

procedure TUI_LOCAMR_frm.DeleteDocument;
var
  DocNo,DocCount : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    DocNo := qryListMAINT_NO.AsString;
    DocCount := qryListMSEQ.AsString;
    try
      try
        IF MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_LINE+#13#10+DocNo+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;
          SQL.Text := 'DELETE FROM LOCAMR WHERE MAINT_NO = '+QuotedStr(DocNo)+' AND MSEQ = '+DocCount;
          ExecSQL;
          DMMssql.KISConnect.CommitTrans;

          qryList.Close;
          qrylist.open;
          
          IF (qryList.RecordCount > 0) AND (qryList.RecordCount >= nCursor) Then
          begin
            qryList.MoveBy(nCursor-1);
          end
          else
            qryList.First;
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;
procedure TUI_LOCAMR_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  if not qrylist.Active then Exit;

  DeleteDocument;
end;

procedure TUI_LOCAMR_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  LOCAMR_PRINT_frm := TLOCAMR_PRINT_frm.Create(Self);
  try
    LOCAMR_PRINT_frm.PrintDocument(qryList.Fields,True);
  finally
    FreeAndNil( LOCAMR_PRINT_frm );
  end;
end;

procedure TUI_LOCAMR_frm.sDBGrid1TitleClick(Column: TColumn);
begin
  inherited;
  if sKeyField <> Column.FieldName Then
  begin
    sKeyField := Column.FieldName;
    Readlist('ORDER BY '+sKeyField);
    qryList.Tag := 0;
  end
  else
  begin
    IF qryList.Tag = 0 Then
    begin
      Readlist('ORDER BY '+sKeyField+' DESC');
      qryList.Tag := 1;
    end
    else
    begin
      Readlist('ORDER BY '+sKeyField);
      qryList.Tag := 0;
    end;
  end;
end;


procedure TUI_LOCAMR_frm.memo_REMARK1Change(Sender: TObject);
var
  memorow,memoCol : integer;
begin
  inherited;
  //메모의 라인번호
  memorow := (Sender as TsMemo).Perform(EM_LINEFROMCHAR,(Sender as TsMemo).SelStart,0);
  //메모의 컬럼번호
  memoCol := (Sender as TsMemo).SelStart - (Sender as TsMemo).Perform(EM_LINEINDEX,memorow,0);

  if (Sender as TsMemo).Name = 'memo_REMARK1' then
  begin
    if memo_REMARK1.Text = '' then
      slabel7.Caption := ''
    else
      sLabel7.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  end
  else if (Sender as TsMemo).Name = 'memo_CHGINFO' then
  begin
    if memo_CHGINFO.Text = '' then
      slabel3.Caption := ''
    else
      sLabel3.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  end;
end;

procedure TUI_LOCAMR_frm.memo_REMARK1KeyPress(Sender: TObject;
  var Key: Char);
var
  limitcount : Integer;
begin
  if (Sender as TsMemo).Name = 'memo_REMARK1' then limitcount := 6
  else if (Sender as TsMemo).Name = 'memo_CHGINFO' then limitcount := 11;

  if (Key = #13) AND ((Sender as TsMemo).lines.count >= limitCount-1) then Key := #0;
end;

procedure TUI_LOCAMR_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  ReadListBetween(Mask_fromDate.Text,Mask_toDate.Text,'');
end;

procedure TUI_LOCAMR_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_LOCAMR_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := LOCAMR(RecvDoc,0, RecvCode);
      ReadyDateTime := Now;
      with qryReady do          
      begin
        Parameters.ParamByName('DOCID').Value := 'LOCAMR';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'LOCAMR';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        ReadListBetween(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;

end;

procedure TUI_LOCAMR_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_LOCAMR_frm.FormActivate(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;

  if not DMMssql.KISConnect.InTransaction then
  begin

    qryList.Close;
    qryList.Open;

  end;
end;

procedure TUI_LOCAMR_frm.sBitBtn4Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    1 : edt_APPLICDblClick(edt_APPLIC); //개설의뢰인
    2 : edt_APPLICDblClick(edt_BENEFC); //수혜자
  end

end;

end.


