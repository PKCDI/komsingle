unit UI_APP700_BP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sLabel, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, Grids, DBGrids, acDBGrid, sComboBox, ExtCtrls, sSplitter,
  sScrollBox, ComCtrls, sPageControl, sCheckBox, Mask, sMaskEdit, Buttons,
  sBitBtn, sEdit, sButton, sSpeedButton, sPanel, sSkinProvider, sMemo , TypeDefine,
  DB, ADODB, sBevel ;

type
  TUI_APP700_BP_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    sButton1: TsButton;
    sButton3: TsButton;
    sPanel4: TsPanel;
    edt_MaintNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    edt_UserNo: TsEdit;
    btn_Cal: TsBitBtn;
    sCheckBox1: TsCheckBox;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel18: TsPanel;
    sTabSheet2: TsTabSheet;
    sPanel23: TsPanel;
    sTabSheet7: TsTabSheet;
    sSplitter2: TsSplitter;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel21: TsPanel;
    sPanel26: TsPanel;
    sPanel24: TsPanel;
    edt_cdPerP: TsCurrencyEdit;
    edt_cdPerM: TsCurrencyEdit;
    edt_CdMax1: TsEdit;
    edt_CdMax: TsEdit;
    sLabel9: TsLabel;
    sPanel22: TsPanel;
    sPanel20: TsPanel;
    edt_Cdcur: TsEdit;
    btn_CurrencyCode: TsBitBtn;
    edt_Cdamt: TsCurrencyEdit;
    edt_Applic5: TsEdit;
    edt_Applic4: TsEdit;
    edt_Applic3: TsEdit;
    edt_Applic2: TsEdit;
    edt_Applic1: TsEdit;
    sPanel17: TsPanel;
    sPanel19: TsPanel;
    edt_Benefc: TsEdit;
    edt_Benefc1: TsEdit;
    edt_Benefc2: TsEdit;
    edt_Benefc3: TsEdit;
    edt_Benefc4: TsEdit;
    edt_Benefc5: TsEdit;
    edt_exPlace: TsEdit;
    edt_exDate: TsMaskEdit;
    sPanel16: TsPanel;
    edt_Doccd1: TsEdit;
    sPanel15: TsPanel;
    edt_Doccd: TsEdit;
    btn_Doccd: TsBitBtn;
    btn_MaximumCreditCode: TsBitBtn;
    edt_TermPR_M: TsEdit;
    btn_TermsOfPrice: TsBitBtn;
    edt_TermPR: TsEdit;
    sPanel11: TsPanel;
    edt_plTerm: TsEdit;
    sPanel12: TsPanel;
    edt_aaCcv1: TsEdit;
    edt_aaCcv3: TsEdit;
    edt_aaCcv4: TsEdit;
    sTabSheet3: TsTabSheet;
    sPanel13: TsPanel;
    sPanel14: TsPanel;
    btn_Beneficiary: TsBitBtn;
    edt_aaCcv2: TsEdit;
    sPanel28: TsPanel;
    edt_Draft1: TsEdit;
    edt_Draft3: TsEdit;
    edt_Draft2: TsEdit;
    sPanel29: TsPanel;
    edt_mixPay1: TsEdit;
    edt_mixPay2: TsEdit;
    edt_mixPay3: TsEdit;
    edt_mixPay4: TsEdit;
    sPanel30: TsPanel;
    edt_defPay1: TsEdit;
    edt_defPay2: TsEdit;
    edt_defPay3: TsEdit;
    edt_defPay4: TsEdit;
    sSpeedButton1: TsSpeedButton;
    sPanel10: TsPanel;
    btn_DescriptionServices: TsBitBtn;
    sLabel16: TsLabel;
    memo_Desgood1: TsMemo;
    sTabSheet4: TsTabSheet;
    sPanel27: TsPanel;
    sPanel31: TsPanel;
    sTabSheet5: TsTabSheet;
    sPanel42: TsPanel;
    sPanel43: TsPanel;
    sTabSheet6: TsTabSheet;
    sPanel44: TsPanel;
    sPanel45: TsPanel;
    sLabel46: TsLabel;
    sLabel48: TsLabel;
    sLabel50: TsLabel;
    check_acd2AA: TsCheckBox;
    sPanel46: TsPanel;
    edt_acd2AA1: TsEdit;
    check_acd2AB: TsCheckBox;
    check_acd2AC: TsCheckBox;
    check_acd2AD: TsCheckBox;
    check_acd2AE: TsCheckBox;
    memo_acd2AE1: TsMemo;
    sBitBtn34: TsBitBtn;
    sPanel48: TsPanel;
    edt_Charge: TsEdit;
    sBitBtn35: TsBitBtn;
    sPanel49: TsPanel;
    sLabel52: TsLabel;
    edt_period: TsEdit;
    sLabel53: TsLabel;
    sLabel54: TsLabel;
    sPanel50: TsPanel;
    edt_Confirm: TsEdit;
    sBitBtn36: TsBitBtn;
    sPanel51: TsPanel;
    sPanel5: TsPanel;
    edt_In_Mathod1: TsEdit;
    sPanel2: TsPanel;
    edt_ImpCd1_1: TsEdit;
    btn_ImpCd1: TsBitBtn;
    btn_ImpCd2: TsBitBtn;
    edt_ImpCd2_1: TsEdit;
    edt_ImpCd3_1: TsEdit;
    btn_ImpCd3: TsBitBtn;
    btn_ImpCd4: TsBitBtn;
    edt_ImpCd4_1: TsEdit;
    edt_ImpCd5_1: TsEdit;
    btn_ImpCd5: TsBitBtn;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    btn_LCNumber5: TsBitBtn;
    btn_LCNumber4: TsBitBtn;
    btn_LCNumber3: TsBitBtn;
    btn_LCNumber2: TsBitBtn;
    btn_LCNumber1: TsBitBtn;
    sPanel7: TsPanel;
    btn_GrantingCredit: TsBitBtn;
    edt_AdPay1: TsEdit;
    sPanel6: TsPanel;
    sPanel52: TsPanel;
    sPanel53: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    sDBGrid2: TsDBGrid;
    btn_OpenMethod: TsBitBtn;
    btn_ApBank: TsBitBtn;
    btn_NoticeBank: TsBitBtn;
    edt_Charge1: TsEdit;
    edt_Confirm1: TsEdit;
    edt_Datee: TsMaskEdit;
    edt_In_Mathod: TsEdit;
    edt_ApBank: TsEdit;
    edt_ApBank1: TsEdit;
    edt_ApBank2: TsEdit;
    edt_ApBank3: TsEdit;
    edt_ApBank4: TsEdit;
    edt_ApBank5: TsEdit;
    edt_AdBank: TsEdit;
    edt_AdBank1: TsEdit;
    edt_AdBank2: TsEdit;
    edt_AdBank3: TsEdit;
    edt_AdBank4: TsEdit;
    edt_AdPay: TsEdit;
    edt_ImpCd1: TsEdit;
    edt_ImpCd2: TsEdit;
    edt_ImpCd3: TsEdit;
    edt_ImpCd4: TsEdit;
    edt_ImpCd5: TsEdit;
    edt_ILno1: TsEdit;
    edt_ILCur1: TsEdit;
    edt_ILAMT1: TsCurrencyEdit;
    edt_ILno2: TsEdit;
    edt_ILCur2: TsEdit;
    edt_ILAMT2: TsCurrencyEdit;
    edt_ILno3: TsEdit;
    edt_ILCur3: TsEdit;
    edt_ILAMT3: TsCurrencyEdit;
    edt_ILno4: TsEdit;
    edt_ILCur4: TsEdit;
    edt_ILAMT4: TsCurrencyEdit;
    edt_ILno5: TsEdit;
    edt_ILCur5: TsEdit;
    edt_ILAMT5: TsCurrencyEdit;
    edt_AdInfo1: TsEdit;
    edt_AdInfo2: TsEdit;
    edt_AdInfo3: TsEdit;
    edt_AdInfo4: TsEdit;
    edt_AdInfo5: TsEdit;
    edt_EXName1: TsEdit;
    edt_EXName2: TsEdit;
    edt_EXName3: TsEdit;
    edt_EXAddr1: TsEdit;
    edt_EXAddr2: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    edt_msg1: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_msg2: TsEdit;
    sBitBtn3: TsBitBtn;
    sSpeedButton6: TsSpeedButton;
    sSplitter1: TsSplitter;
    sSplitter3: TsSplitter;
    sSplitter4: TsSplitter;
    sSplitter5: TsSplitter;
    sSplitter6: TsSplitter;
    sSplitter7: TsSplitter;
    sSplitter8: TsSplitter;
    sSplitter9: TsSplitter;
    sLabel2: TsLabel;
    sLabel4: TsLabel;
    sLabel6: TsLabel;
    sLabel8: TsLabel;
    sLabel1: TsLabel;
    sLabel3: TsLabel;
    sLabel5: TsLabel;
    sLabel7: TsLabel;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sLabel12: TsLabel;
    sLabel13: TsLabel;
    sLabel14: TsLabel;
    sLabel15: TsLabel;
    sLabel49: TsLabel;
    sLabel47: TsLabel;
    sLabel51: TsLabel;
    sLabel55: TsLabel;
    procedure btn_CreateBankCodeDblClick(Sender: TObject);
    procedure edt_ApBankDblClick(Sender: TObject);
    procedure edt_CodeTypeDblClick(Sender: TObject);
    procedure btn_CodeTypeClick(Sender: TObject);
    procedure edt_CreateUserCodeDblClick(Sender: TObject);
    procedure btn_CreateUserCodeDblClick(Sender: TObject);
    procedure memoDblClick(Sender: TObject);
    procedure btn_DescriptionServicesClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);


  private
    { Private declarations }
    FSQL : String;
  public
    { Public declarations }
  protected
    { protected declaration }
    ProgramControlType : TProgramControlType;
    procedure NewDocument; virtual; abstract;
    procedure CancelDocument; virtual; abstract;
    procedure SaveDocument(Sender : TObject); virtual; abstract;
    procedure ReadDocument; virtual; abstract;
    procedure DeleteDocument; virtual; abstract;
    procedure EditDocument; virtual; abstract;

    procedure ButtonEnable(Val : Boolean=true);

    function ValidData:Boolean; virtual; abstract;
    property FormSQL: String  read FSQL write FSQL;
  end;

var
  UI_APP700_BP_frm: TUI_APP700_BP_frm;

implementation

uses Commonlib , Dialog_CodeList , Dialog_BANK , Dialog_SearchCustom, MSSQL, Dialog_MemoList,
     CodeContents, MessageDefine ;

{$R *.dfm}

{ TUI_APP700_BP_frm }

procedure TUI_APP700_BP_frm.ButtonEnable(Val: Boolean);
begin
  btnNew.Enabled := Val;
  btnEdit.Enabled := Val;
  btnDel.Enabled := Val;

  btnTemp.Enabled := not Val;
  btnSave.Enabled := not Val;
  btnCancel.Enabled := not Val;

  btnCopy.Enabled := Val;
  btnPrint.Enabled := Val;
  
end;

//은행Code 관련
procedure TUI_APP700_BP_frm.btn_CreateBankCodeDblClick(Sender: TObject);
begin
  inherited;
    Case (Sender as TBitBtn).Tag of
      //개설의뢰은행
      102: edt_ApBankDblClick(edt_ApBank);
      //통지은행
      103: edt_ApBankDblClick(edt_AdBank);
    end;

end;

//은행Code 관련
procedure TUI_APP700_BP_frm.edt_ApBankDblClick(Sender: TObject);
begin
  inherited;
  Dialog_BANK_frm := TDialog_BANK_frm.Create(Self);
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      IF Dialog_BANK_frm.openDialog((Sender as TsEdit).Text) = mrOK Then
      begin
        (Sender as TsEdit).Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        case (Sender as TsEdit).tag of
          //개설의뢰은행
          102:
            begin
              edt_ApBank1.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKNAME1').AsString;
              edt_ApBank2.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKNAME2').AsString;
              edt_ApBank3.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKBRANCH1').AsString;
              edt_ApBank4.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKBRANCH2').AsString;
              
            end;
          //통지은행
          103:
            begin
              edt_AdBank1.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKNAME1').AsString;
              edt_AdBank2.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKNAME2').AsString;
              edt_AdBank3.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKBRANCH1').AsString;
              edt_AdBank4.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKBRANCH2').AsString;
            end;
        end;
      End;
    end;
  finally
     FreeAndNil( Dialog_BANK_frm );
  end;
end;


//USER CODE 관련
procedure TUI_APP700_BP_frm.btn_CreateUserCodeDblClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of

    //Beneficiary(수혜자)
    116 : edt_CreateUserCodeDblClick(edt_Benefc);

  end;

end;

//USER CODE 관련
procedure TUI_APP700_BP_frm.edt_CreateUserCodeDblClick(Sender: TObject);
begin
  inherited;
  Dialog_SearchCustom_frm := TDialog_SearchCustom_frm.Create(Self);

  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      IF Dialog_SearchCustom_frm.openDialog((Sender as TsEdit).Text) = mrOK then
      begin
        (Sender as TsEdit).Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        case (Sender as TsEdit).Tag of
          //Beneficiary(수혜자)
          116 :
          begin
              edt_Benefc1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              edt_Benefc2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_Name').AsString;
              edt_Benefc3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_Benefc4.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              //CUSTOM DB에 계좌번호라는 필드가 없음 
              //edt_Benefc5.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('TRAD_NO').AsString;
          end;
        end;
      end;
    end;
  finally
    FreeAndNil(Dialog_SearchCustom_frm);
  end;

end;


//CodeType 관련
procedure TUI_APP700_BP_frm.edt_CodeTypeDblClick(Sender: TObject);
begin
  inherited;
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      if Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
      begin
          //선택된 코드 출력
          (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;

          //선택된 코드의 값까지 출력
          case (Sender as TsEdit).Tag of

              //개설방법
              101 : edt_In_Mathod1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //신용공여
              104 : edt_AdPay1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //수입용도
              105 : edt_ImpCd1_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              106 : edt_ImpCd2_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              107 : edt_ImpCd3_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              108 : edt_ImpCd4_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              109 : edt_ImpCd5_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //Form of Documentary Credit
              115 : edt_Doccd1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //Maximum Credit Amount
              118 : edt_CdMax1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //Terms of Price
              119 : edt_TermPR_M.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //CHARGE
              120 : edt_Charge1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //Confirmation Instructions
              121 : edt_Confirm1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

          end;
      end;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;

end;

//CodeType 관련
procedure TUI_APP700_BP_frm.btn_CodeTypeClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    //문서기능
    1 : edt_CodeTypeDblClick(edt_msg1);

    //유형
    2 : edt_CodeTypeDblClick(edt_msg2);

    //개설방법
    101 : edt_CodeTypeDblClick(edt_In_Mathod);

    //통지은행
    //103 : edt_CodeTypeDblClick(edt_AdBank);

    //신용공여
    104 : edt_CodeTypeDblClick(edt_AdPay);

    //수입용도
    105 : edt_CodeTypeDblClick(edt_ImpCd1);
    106 : edt_CodeTypeDblClick(edt_ImpCd2);
    107 : edt_CodeTypeDblClick(edt_ImpCd3);
    108 : edt_CodeTypeDblClick(edt_ImpCd4);
    109 : edt_CodeTypeDblClick(edt_ImpCd5);

    //I/L 번호
    110 : edt_CodeTypeDblClick(edt_ILCur1);
    111 : edt_CodeTypeDblClick(edt_ILCur2);
    112 : edt_CodeTypeDblClick(edt_ILCur3);
    113 : edt_CodeTypeDblClick(edt_ILCur4);
    114 : edt_CodeTypeDblClick(edt_ILCur5);

    //Form of Documentary Credit
    115 : edt_CodeTypeDblClick(edt_Doccd);

    //Currency Code,Amount
    117 : edt_CodeTypeDblClick(edt_Cdcur);

    //Maximum Credit Amount
    118 : edt_CodeTypeDblClick(edt_CdMax);

    //Terms of Price
    119 : edt_CodeTypeDblClick(edt_TermPR);

    //CHARGE
    120 : edt_CodeTypeDblClick(edt_Charge);

    //Confirmation Instructions
    121 : edt_CodeTypeDblClick(edt_Confirm);

  end;

end;

procedure TUI_APP700_BP_frm.memoDblClick(Sender: TObject);
begin
  inherited;
  Dialog_MemoList_frm := TDialog_MemoList_frm.Create(Self);

  try
    if (Sender as TsMemo).ReadOnly = False then
    begin

      if Dialog_MemoList_frm.openDialog = mrok then
      begin
        if Dialog_MemoList_frm.writeRadioGroup.ItemIndex = 0 then
        begin
            if Dialog_MemoList_frm.codeRadioGroup.ItemIndex = 0 then
              (Sender as TsMemo).Text := Dialog_MemoList_frm.sDBGrid1.DataSource.DataSet.FieldByName('D_MEMO').AsString
            else if Dialog_MemoList_frm.codeRadioGroup.ItemIndex = 1 then
              (Sender as TsMemo).Text := Dialog_MemoList_frm.sDBGrid1.DataSource.DataSet.FieldByName('FNAME').AsString;
        end
        else if  Dialog_MemoList_frm.writeRadioGroup.ItemIndex = 1 then
        begin
            if Dialog_MemoList_frm.codeRadioGroup.ItemIndex = 0 then
              (Sender as TsMemo).Text := (Sender as TsMemo).Text + #13#10 + Dialog_MemoList_frm.sDBGrid1.DataSource.DataSet.FieldByName('D_MEMO').AsString
            else if Dialog_MemoList_frm.codeRadioGroup.ItemIndex = 1 then
              (Sender as TsMemo).Text := (Sender as TsMemo).Text + #13#10 + Dialog_MemoList_frm.sDBGrid1.DataSource.DataSet.FieldByName('FNAME').AsString;
        end;
      end;
    end;
  finally

    FreeAndNil(Dialog_MemoList_frm);

  end;

end;

procedure TUI_APP700_BP_frm.btn_DescriptionServicesClick(Sender: TObject);
begin
  inherited;
   case (Sender as TsBitBtn).Tag of
     122 : memoDblClick(memo_Desgood1);

     123 : memoDblClick(memo_acd2AE1);
   end;

end;

procedure TUI_APP700_BP_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  //ChildForm에 OnKeyPress이벤트
end;

end.

