unit UI_ADV707;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, Grids, DBGrids, acDBGrid, StdCtrls,
  sComboBox, sCustomComboEdit, sCurrEdit, sCurrencyEdit, sMemo, ComCtrls,
  sPageControl, sButton, sLabel, Buttons, sSpeedButton, sCheckBox, sEdit,
  Mask, sMaskEdit, sBitBtn, ExtCtrls, sPanel, sSplitter, sBevel, DB, ADODB;

type
  TUI_ADV707_frm = class(TChildForm_frm)
    sSplitter1: TsSplitter;
    sSplitter3: TsSplitter;
    sPanel4: TsPanel;
    btn_Cal: TsBitBtn;
    mask_DATEE: TsMaskEdit;
    edt_MaintNo: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_msg1: TsEdit;
    edt_UserNo: TsEdit;
    sCheckBox1: TsCheckBox;
    edt_msg2: TsEdit;
    sBitBtn3: TsBitBtn;
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    btnExit: TsButton;
    btnDel: TsButton;
    sButton4: TsButton;
    sButton2: TsButton;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sSplitter4: TsSplitter;
    sPanel5: TsPanel;
    sPanel2: TsPanel;
    mask_APPDATE: TsMaskEdit;
    edt_APPNO: TsEdit;
    sPanel18: TsPanel;
    mask_AMDDATE: TsMaskEdit;
    sPanel24: TsPanel;
    edt_BENEFC1: TsEdit;
    edt_BENEFC2: TsEdit;
    edt_BENEFC3: TsEdit;
    edt_BENEFC4: TsEdit;
    sTabSheet2: TsTabSheet;
    sSplitter7: TsSplitter;
    sPanel6: TsPanel;
    sPanel7: TsPanel;
    sSpeedButton6: TsSpeedButton;
    sTabSheet9: TsTabSheet;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sDBGrid1: TsDBGrid;
    edt_AMDNO1: TsEdit;
    edt_BENEFC5: TsEdit;
    edt_BENEFC6: TsEdit;
    mask_EXDATE: TsMaskEdit;
    mask_ISSDATE: TsMaskEdit;
    sSpeedButton1: TsSpeedButton;
    curr_INCDAMT: TsCurrencyEdit;
    edt_INCDCUR: TsEdit;
    sPanel13: TsPanel;
    edt_DECDCUR: TsEdit;
    curr_DECDAMT: TsCurrencyEdit;
    edt_NWCDCUR: TsEdit;
    curr_NWCDAMT: TsCurrencyEdit;
    curr_PERP: TsCurrencyEdit;
    curr_PERM: TsCurrencyEdit;
    edt_CDMAX: TsEdit;
    edt_AACV2: TsEdit;
    edt_AACV1: TsEdit;
    edt_AACV3: TsEdit;
    edt_AACV4: TsEdit;
    sPanel14: TsPanel;
    sPanel21: TsPanel;
    sPanel12: TsPanel;
    memo_NARRAT1: TsMemo;
    sPanel19: TsPanel;
    edt_APBANK: TsEdit;
    edt_APBANK1: TsEdit;
    edt_APBANK2: TsEdit;
    edt_APBANK3: TsEdit;
    edt_APBANK4: TsEdit;
    sPanel15: TsPanel;
    edt_ADBANK: TsEdit;
    edt_ADBANK1: TsEdit;
    edt_ADBANK2: TsEdit;
    edt_ADBANK3: TsEdit;
    edt_ADBANK4: TsEdit;
    sPanel16: TsPanel;
    edt_CDNO: TsEdit;
    edt_RCVREF: TsEdit;
    edt_IBANKREF: TsEdit;
    sPanel17: TsPanel;
    edt_ISSBANK: TsEdit;
    edt_ISSBANK1: TsEdit;
    edt_ISSBANK2: TsEdit;
    edt_ISSBANK3: TsEdit;
    edt_ISSBANK4: TsEdit;
    edt_ISSACCNT: TsEdit;
    sPanel20: TsPanel;
    edt_SRINFO1: TsEdit;
    edt_SRINFO2: TsEdit;
    edt_SRINFO3: TsEdit;
    edt_SRINFO4: TsEdit;
    edt_SRINFO5: TsEdit;
    edt_SRINFO6: TsEdit;
    sPanel22: TsPanel;
    edt_EXNAME1: TsEdit;
    edt_EXNAME2: TsEdit;
    edt_EXNAME3: TsEdit;
    edt_EXADDR1: TsEdit;
    edt_EXADDR2: TsEdit;
    sPanel8: TsPanel;
    edt_LOADON: TsEdit;
    edt_FORTRAN: TsEdit;
    edt_SUNJUCKPORT: TsEdit;
    edt_DOCHACKPORT: TsEdit;
    mask_LSTDATE: TsMaskEdit;
    sPanel35: TsPanel;
    edt_SHIPPD1: TsEdit;
    edt_SHIPPD2: TsEdit;
    edt_SHIPPD3: TsEdit;
    edt_SHIPPD4: TsEdit;
    edt_SHIPPD5: TsEdit;
    edt_SHIPPD6: TsEdit;
    sPanel44: TsPanel;
    sPanel57: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn60: TsBitBtn;
    mask_toDate: TsMaskEdit;
    sDBGrid8: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListAMD_NO: TIntegerField;
    qryListAMD_NO1: TIntegerField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAMD_DATE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListAPP_NO: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_BANK5: TStringField;
    qryListADDINFO: TStringField;
    qryListADDINFO_1: TMemoField;
    qryListCD_NO: TStringField;
    qryListRCV_REF: TStringField;
    qryListISS_BANK: TStringField;
    qryListISS_BANK1: TStringField;
    qryListISS_BANK2: TStringField;
    qryListISS_BANK3: TStringField;
    qryListISS_BANK4: TStringField;
    qryListISS_BANK5: TStringField;
    qryListISS_ACCNT: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListBENEFC6: TStringField;
    qryListIBANK_REF: TStringField;
    qryListPRNO: TIntegerField;
    qryListMAINT_NO_1: TStringField;
    qryListAMD_NO_1: TIntegerField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListSR_INFO1: TStringField;
    qryListSR_INFO2: TStringField;
    qryListSR_INFO3: TStringField;
    qryListSR_INFO4: TStringField;
    qryListSR_INFO5: TStringField;
    qryListSR_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    edt_AMDNO: TsEdit;
    memo_ADDINFO1: TsMemo;
    edt_ADBANK5: TsEdit;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sBevel1: TsBevel;
    sSplitter2: TsSplitter;
    procedure sPageControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure sDBGrid8DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure sBitBtn60Click(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
  private
    { Private declarations }
    ADV707_SQL : String;
    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;

    procedure ReadDocument;
    procedure DeleteDocument;
  public
    { Public declarations }
  end;

var
  UI_ADV707_frm: TUI_ADV707_frm;

implementation

{$R *.dfm}

uses MSSQL, Commonlib, DateUtils, MessageDefine, StrUtils, KISCalendar, ADV707_PRINT;

procedure TUI_ADV707_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  //sPanel4.Visible := not (sPageControl1.ActivePageIndex = 2);
  sPanel44.Visible := not (sPageControl1.ActivePageIndex = 2);
end;

procedure TUI_ADV707_frm.FormCreate(Sender: TObject);
begin
  inherited;
  sPageControl1.ActivePageIndex := 0;
  ADV707_SQL := qryList.SQL.Text;
end;

procedure TUI_ADV707_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_ADV707_frm := nil;
end;

procedure TUI_ADV707_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_ADV707_frm.FormShow(Sender: TObject);
begin
  inherited;
  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel2);
  EnabledControlValue(sPanel7);

  mask_fromDate.Text := FormatDateTime('YYYYMMDD' , StartOfTheYear(Now));
  mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  mask_SearchDate1.Text := FormatDateTime('YYYYMMDD' , StartOfTheYear(Now));
  mask_SearchDate2.Text := FormatDateTime('YYYYMMDD' , Now);

  ReadList(Mask_fromDate.Text , mask_toDate.Text , '');

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
    ClearControlValue(sPanel7);
  end;
end;

procedure TUI_ADV707_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 2 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := ADV707_SQL;
    case com_SearchKeyword.ItemIndex of
      0 : //수신일자
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 : //관리번호
      begin
        SQL.Add(' WHERE ADV707.MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :  //신용장번호
      begin
        SQL.Add(' WHERE CP_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end;
    Open;
  end;

end;

function TUI_ADV707_frm.Readlist(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
  Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := ADV707_SQL;
    SQL.Add('WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_ADV707_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_ADV707_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_ADV707_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    0 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    1 : Mask_SearchDate1DblClick(Mask_SearchDate2);
  end;      
end;

procedure TUI_ADV707_frm.ReadDocument;
begin
  IF not qrylist.Active Then Exit;
  IF qrylist.RecordCount = 0 Then Exit;

  //관리번호
  edt_MaintNo.Text := qryListMAINT_NO.AsString;
  edt_AMDNO.Text := qryListAMD_NO.AsString;
  //수신일자
  mask_DATEE.Text := qryListDATEE.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;

  //통지일자
  mask_APPDATE.Text := qryListAPP_DATE.AsString;
  //통지번호
  edt_APPNO.Text := qryListAPP_NO.AsString;
  //개설일자
  mask_ISSDATE.Text := qryListISS_DATE.AsString;
  //유효기일
  mask_EXDATE.Text := qryListEX_DATE.AsString;
  //조건변경일자
  mask_AMDDATE.Text := qryListAMD_DATE.AsString;
  //조건변경횟수
  edt_AMDNO1.Text := qryListAMD_NO1.AsString;
  //원수익자
  edt_BENEFC1.Text := qryListBENEFC1.AsString;
  edt_BENEFC2.Text := qryListBENEFC2.AsString;
  edt_BENEFC3.Text := qryListBENEFC3.AsString;
  edt_BENEFC4.Text := qryListBENEFC4.AsString;
  edt_BENEFC5.Text := qryListBENEFC5.AsString;
  edt_BENEFC6.Text := qryListBENEFC6.AsString;
  //신용장증액분
  edt_INCDCUR.Text := qryListINCD_CUR.AsString;
  curr_INCDAMT.Value := qryListINCD_AMT.AsCurrency;
  //신용장감액분
  edt_DECDCUR.Text := qryListDECD_CUR.AsString;
  curr_DECDAMT.Value := qryListDECD_AMT.AsCurrency;
  //변경후최종금액
  edt_NWCDCUR.Text := qryListNWCD_CUR.AsString;
  curr_NWCDAMT.Value := qryListNWCD_AMT.AsCurrency;
  //과부족 허용율
  curr_PERP.Value := qryListCD_PERP.AsCurrency;
  curr_PERM.Value := qryListCD_PERM.AsCurrency;
  //최대신용장금액
  
  //부가금액부담
  edt_AACV1.Text := qryListAA_CV1.AsString;
  edt_AACV2.Text := qryListAA_CV2.AsString;
  edt_AACV3.Text := qryListAA_CV3.AsString;
  edt_AACV4.Text := qryListAA_CV4.AsString;
  //기타정보
  memo_ADDINFO1.Lines.Text := qryListADDINFO_1.AsString;
  //기타조건변경사항
  memo_NARRAT1.Lines.Text := qryListNARRAT_1.AsString;
  //전문발신은행
  edt_APBANK.Text := qryListAP_BANK.AsString;
  edt_APBANK1.Text := qryListAP_BANK1.AsString;
  edt_APBANK2.Text := qryListAP_BANK2.AsString;
  edt_APBANK3.Text := qryListAP_BANK3.AsString;
  edt_APBANK4.Text := qryListAP_BANK4.AsString;
  //전문수신은행
  edt_ADBANK.Text := qryListAD_BANK.AsString;
  edt_ADBANK1.Text := qryListAD_BANK1.AsString;
  edt_ADBANK2.Text := qryListAD_BANK2.AsString;
  edt_ADBANK3.Text := qryListAD_BANK3.AsString;
  edt_ADBANK4.Text := qryListAD_BANK4.AsString;
  edt_ADBANK5.Text := qryListAD_BANK5.AsString;
  //개설은행
  edt_ISSBANK.Text := qryListISS_BANK.AsString;
  edt_ISSBANK1.Text := qryListISS_BANK1.AsString;
  edt_ISSBANK2.Text := qryListISS_BANK2.AsString;
  edt_ISSBANK3.Text := qryListISS_BANK3.AsString;
  edt_ISSBANK4.Text := qryListISS_BANK4.AsString;
  edt_ISSACCNT.Text := qryListISS_ACCNT.AsString;
  //참조사항
  edt_CDNO.Text := qryListCD_NO.AsString; //발신은행 , 신용장번호
  edt_RCVREF.Text := qryListRCV_REF.AsString; //수신은행
  edt_IBANKREF.Text := qryListIBANK_REF.AsString; //개설은행
  //수신은행앞정보
  edt_SRINFO1.Text := qryListSR_INFO1.AsString;
  edt_SRINFO2.Text := qryListSR_INFO2.AsString;
  edt_SRINFO3.Text := qryListSR_INFO3.AsString;
  edt_SRINFO4.Text := qryListSR_INFO4.AsString;
  edt_SRINFO5.Text := qryListSR_INFO5.AsString;
  edt_SRINFO6.Text := qryListSR_INFO6.AsString;
  //통지은행
  edt_EXNAME1.Text := qryListEX_NAME1.AsString;
  edt_EXNAME2.Text := qryListEX_NAME2.AsString;
  edt_EXNAME3.Text := qryListEX_NAME3.AsString;
  edt_EXADDR1.Text := qryListEX_ADDR1.AsString;
  edt_EXADDR2.Text := qryListEX_ADDR2.AsString;
  //수탁발송지
  edt_LOADON.Text := qryListLOAD_ON.AsString;
  //최종도착지
  edt_FORTRAN.Text := qryListFOR_TRAN.AsString;
  //선적항
  edt_SUNJUCKPORT.Text := qryListSUNJUCK_PORT.AsString;
  //도착항
  edt_DOCHACKPORT.Text := qryListDOCHACK_PORT.AsString;
  //최종선적일자
  mask_LSTDATE.Text := qryListLST_DATE.AsString;
  //선적기간
  edt_SHIPPD1.Text := qryListSHIP_PD1.AsString;
  edt_SHIPPD2.Text := qryListSHIP_PD2.AsString;
  edt_SHIPPD3.Text := qryListSHIP_PD3.AsString;
  edt_SHIPPD4.Text := qryListSHIP_PD4.AsString;
  edt_SHIPPD5.Text := qryListSHIP_PD5.AsString;
  edt_SHIPPD6.Text := qryListSHIP_PD6.AsString;


end;

procedure TUI_ADV707_frm.sDBGrid8DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_ADV707_frm.DeleteDocument;
var
  maint_no , amd_NO : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MaintNo.Text;
    amd_NO := edt_AMDNO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM ADV707  WHERE MAINT_NO =' + QuotedStr(maint_no) + ' and AMD_NO = ' + QuotedStr(amd_no)  ;
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV7072  WHERE MAINT_NO =' + QuotedStr(maint_no) + ' and AMD_NO = ' + QuotedStr(amd_no) ;
        ExecSQL;


        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally

    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;

    Close;
    Free;
   end;
  end;
end;

procedure TUI_ADV707_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadDocument;
end;

procedure TUI_ADV707_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;
end;

procedure TUI_ADV707_frm.sBitBtn60Click(Sender: TObject);
begin
  inherited;
  ReadList(Mask_fromDate.Text, mask_toDate.Text ,'');

  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := mask_toDate.Text;

  //삭제 후 레코드가 없으면 클리어
  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
    ClearControlValue(sPanel7);
  end;
end;

procedure TUI_ADV707_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist();
  //삭제 후 레코드가 없으면 클리어
  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
    ClearControlValue(sPanel7);
  end;

end;

procedure TUI_ADV707_frm.sButton4Click(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  
  ADV707_PRINT_frm := TADV707_PRINT_frm.Create(Self);
  try
    ADV707_PRINT_frm.MaintNo := edt_MaintNo.Text;
    ADV707_PRINT_frm.AmdNo := edt_AMDNO.Text;

    ADV707_PRINT_frm.Prepare;
    case (Sender as TsButton).Tag of
      0 :
      begin
        ADV707_PRINT_frm.PrinterSetup;
        if ADV707_PRINT_frm.Tag = 0 then ADV707_PRINT_frm.Print;
      end;
      1 : ADV707_PRINT_frm.Preview;
    end;

  finally
    FreeAndNil(ADV707_PRINT_frm);
  end;
end;

end.
