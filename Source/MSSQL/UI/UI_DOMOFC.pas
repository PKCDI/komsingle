unit UI_DOMOFC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, ExtCtrls, sSplitter, Grids, DBGrids, acDBGrid,
  StdCtrls, sComboBox, sCheckBox, Buttons, sBitBtn, Mask, sMaskEdit, sEdit,
  ComCtrls, sPageControl, sButton, sSpeedButton, sPanel, sSkinProvider, DB,
  ADODB, sGroupBox, sLabel, sMemo, DBCtrls, sDBMemo, sDBEdit, TypeDefine,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, Menus, sDialogs;

type
  TUI_DOMOFC_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    sButton1: TsButton;
    sButton3: TsButton;
    maintno_Panel: TsPanel;
    edt_MaintNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    edt_UserNo: TsEdit;
    btn_Cal: TsBitBtn;
    sCheckBox1: TsCheckBox;
    edt_msg1: TsEdit;
    sBitBtn7: TsBitBtn;
    edt_msg2: TsEdit;
    sBitBtn8: TsBitBtn;
    LeftGrid_Panel: TsPanel;
    sPanel53: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    sDBGrid2: TsDBGrid;
    Page_control: TsPageControl;
    sTabSheet1: TsTabSheet;
    page1_Right: TsPanel;
    page1_Left: TsPanel;
    sTabSheet2: TsTabSheet;
    page2_Right: TsPanel;
    page2_Left: TsPanel;
    sTabSheet5: TsTabSheet;
    sTabSheet6: TsTabSheet;
    sSplitter2: TsSplitter;
    sPanel1: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn4: TsBitBtn;
    sPanel2: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel3: TsPanel;
    sPanel4: TsPanel;
    detailDBGrid: TsDBGrid;
    dsList: TDataSource;
    qryList: TADOQuery;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    sPanel9: TsPanel;
    sSpeedButton10: TsSpeedButton;
    Btn_DetailAdd: TsSpeedButton;
    sSpeedButton13: TsSpeedButton;
    Btn_DetailMod: TsSpeedButton;
    sSpeedButton11: TsSpeedButton;
    Btn_DetailDel: TsSpeedButton;
    sSpeedButton15: TsSpeedButton;
    Btn_DetailOk: TsSpeedButton;
    sSpeedButton12: TsSpeedButton;
    Btn_DetailCancel: TsSpeedButton;
    qryListMAINT_NO: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListChk1: TStringField;
    qryListChk2: TStringField;
    qryListChk3: TStringField;
    qryListMaint_Rff: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListD_CHK: TStringField;
    qryListSR_CODE: TStringField;
    qryListSR_NO: TStringField;
    qryListSR_NAME1: TStringField;
    qryListSR_NAME2: TStringField;
    qryListSR_NAME3: TStringField;
    qryListSR_ADDR1: TStringField;
    qryListSR_ADDR2: TStringField;
    qryListSR_ADDR3: TStringField;
    qryListUD_CODE: TStringField;
    qryListUD_NO: TStringField;
    qryListUD_NAME1: TStringField;
    qryListUD_NAME2: TStringField;
    qryListUD_NAME3: TStringField;
    qryListUD_ADDR1: TStringField;
    qryListUD_ADDR2: TStringField;
    qryListUD_ADDR3: TStringField;
    qryListUD_RENO: TStringField;
    qryListOFR_NO: TStringField;
    qryListOFR_DATE: TStringField;
    qryListOFR_SQ: TStringField;
    qryListOFR_SQ1: TStringField;
    qryListOFR_SQ2: TStringField;
    qryListOFR_SQ3: TStringField;
    qryListOFR_SQ4: TStringField;
    qryListOFR_SQ5: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK_1: TMemoField;
    qryListPRNO: TIntegerField;
    qryListHS_CODE: TStringField;
    qryListMAINT_NO_1: TStringField;
    qryListTSTINST: TStringField;
    qryListTSTINST1: TStringField;
    qryListTSTINST2: TStringField;
    qryListTSTINST3: TStringField;
    qryListTSTINST4: TStringField;
    qryListTSTINST5: TStringField;
    qryListPCKINST: TStringField;
    qryListPCKINST1: TStringField;
    qryListPCKINST2: TStringField;
    qryListPCKINST3: TStringField;
    qryListPCKINST4: TStringField;
    qryListPCKINST5: TStringField;
    qryListPAY: TStringField;
    qryListPAY_ETC: TStringField;
    qryListPAY_ETC1: TStringField;
    qryListPAY_ETC2: TStringField;
    qryListPAY_ETC3: TStringField;
    qryListPAY_ETC4: TStringField;
    qryListPAY_ETC5: TStringField;
    qryListMAINT_NO_2: TStringField;
    qryListTERM_DEL: TStringField;
    qryListTERM_NAT: TStringField;
    qryListTERM_LOC: TStringField;
    qryListORGN1N: TStringField;
    qryListORGN1LOC: TStringField;
    qryListORGN2N: TStringField;
    qryListORGN2LOC: TStringField;
    qryListORGN3N: TStringField;
    qryListORGN3LOC: TStringField;
    qryListORGN4N: TStringField;
    qryListORGN4LOC: TStringField;
    qryListORGN5N: TStringField;
    qryListORGN5LOC: TStringField;
    qryListTRNS_ID: TStringField;
    qryListLOADD: TStringField;
    qryListLOADLOC: TStringField;
    qryListLOADTXT: TStringField;
    qryListLOADTXT1: TStringField;
    qryListLOADTXT2: TStringField;
    qryListLOADTXT3: TStringField;
    qryListLOADTXT4: TStringField;
    qryListLOADTXT5: TStringField;
    qryListDEST: TStringField;
    qryListDESTLOC: TStringField;
    qryListDESTTXT: TStringField;
    qryListDESTTXT1: TStringField;
    qryListDESTTXT2: TStringField;
    qryListDESTTXT3: TStringField;
    qryListDESTTXT4: TStringField;
    qryListDESTTXT5: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYCUR: TStringField;
    qryListTAMT: TBCDField;
    qryListTAMTCUR: TStringField;
    qryListCODE: TStringField;
    qryListDOC_NAME: TStringField;
    qryListCODE_1: TStringField;
    qryListDOC_NAME_1: TStringField;
    qryListCODE_2: TStringField;
    qryListDOC_NAME_2: TStringField;
    qryListTRANSNME: TStringField;
    qryListLOADDNAME: TStringField;
    qryListDESTNAME: TStringField;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_CHK: TStringField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME_CHK: TStringField;
    qryDetailNAME_COD: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTY_G: TStringField;
    qryDetailQTYG: TBCDField;
    qryDetailQTYG_G: TStringField;
    qryDetailPRICE: TBCDField;
    qryDetailPRICE_G: TStringField;
    qryDetailAMT: TBCDField;
    qryDetailAMT_G: TStringField;
    qryDetailSTQTY: TBCDField;
    qryDetailSTQTY_G: TStringField;
    qryDetailSTAMT: TBCDField;
    qryDetailSTAMT_G: TStringField;
    spCopyDOMOFC: TADOStoredProc;
    detailMenu: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    sSplitter1: TsSplitter;
    sSplitter3: TsSplitter;
    sSplitter4: TsSplitter;
    sSplitter7: TsSplitter;
    sSplitter8: TsSplitter;
    sSpeedButton1: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    qryReady: TADOQuery;
    popMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    N4: TMenuItem;
    N7: TMenuItem;
    d1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    sPanel23: TsPanel;
    edt_OFRDATE: TsMaskEdit;
    edt_OFRNO: TsEdit;
    edt_SRCODE: TsEdit;
    sBitBtn1: TsBitBtn;
    edt_SRNO: TsEdit;
    edt_SRNAME1: TsEdit;
    edt_SRNAME2: TsEdit;
    edt_SRNAME3: TsEdit;
    edt_SRADDR1: TsEdit;
    edt_SRADDR2: TsEdit;
    edt_SRADDR3: TsEdit;
    sPanel5: TsPanel;
    edt_UDCODE: TsEdit;
    sBitBtn5: TsBitBtn;
    edt_UDNO: TsEdit;
    edt_UDNAME1: TsEdit;
    edt_UDNAME2: TsEdit;
    edt_UDNAME3: TsEdit;
    edt_UDADDR1: TsEdit;
    edt_UDADDR2: TsEdit;
    edt_UDADDR3: TsEdit;
    edt_UDRENO: TsEdit;
    sSpeedButton9: TsSpeedButton;
    sPanel6: TsPanel;
    edt_OFRSQ1: TsEdit;
    edt_OFRSQ2: TsEdit;
    edt_OFRSQ3: TsEdit;
    edt_OFRSQ4: TsEdit;
    edt_OFRSQ5: TsEdit;
    edt_TSTINST1: TsEdit;
    edt_TSTINST2: TsEdit;
    edt_TSTINST3: TsEdit;
    edt_TSTINST4: TsEdit;
    edt_TSTINST5: TsEdit;
    sPanel7: TsPanel;
    edt_PCKINST1: TsEdit;
    edt_PCKINST2: TsEdit;
    edt_PCKINST3: TsEdit;
    edt_PCKINST4: TsEdit;
    edt_PCKINST5: TsEdit;
    sPanel8: TsPanel;
    edt_HSCODE: TsMaskEdit;
    sPanel10: TsPanel;
    edt_PAYETC1: TsEdit;
    edt_PAYETC2: TsEdit;
    edt_PAYETC3: TsEdit;
    edt_PAYETC4: TsEdit;
    edt_PAYETC5: TsEdit;
    edt_ORGN1N: TsEdit;
    sBitBtn6: TsBitBtn;
    edt_ORGN1LOC: TsEdit;
    edt_ORGN2N: TsEdit;
    sBitBtn9: TsBitBtn;
    edt_ORGN2LOC: TsEdit;
    edt_ORGN3LOC: TsEdit;
    sBitBtn10: TsBitBtn;
    edt_ORGN3N: TsEdit;
    edt_ORGN4N: TsEdit;
    sBitBtn11: TsBitBtn;
    edt_ORGN4LOC: TsEdit;
    edt_ORGN5LOC: TsEdit;
    sBitBtn12: TsBitBtn;
    edt_ORGN5N: TsEdit;
    memo_REMARK1: TsMemo;
    edt_TRNSID: TsEdit;
    sBitBtn14: TsBitBtn;
    edt_TRNSIDNAME: TsEdit;
    edt_LOADD: TsEdit;
    sBitBtn13: TsBitBtn;
    edt_LOADLOC: TsEdit;
    edt_LOADTXT1: TsEdit;
    edt_LOADTXT2: TsEdit;
    edt_LOADTXT3: TsEdit;
    edt_LOADTXT4: TsEdit;
    edt_LOADTXT5: TsEdit;
    edt_DEST: TsEdit;
    sBitBtn16: TsBitBtn;
    edt_DESTLOC: TsEdit;
    edt_DESTTXT1: TsEdit;
    edt_DESTTXT2: TsEdit;
    edt_DESTTXT3: TsEdit;
    edt_DESTTXT4: TsEdit;
    edt_DESTTXT5: TsEdit;
    sLabel3: TsLabel;
    sPanel16: TsPanel;
    sDBEdit1: TsDBEdit;
    sBitBtn15: TsBitBtn;
    sDBEdit2: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBEdit3: TsDBEdit;
    sBitBtn17: TsBitBtn;
    sDBEdit4: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sBitBtn18: TsBitBtn;
    sDBEdit5: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sBitBtn19: TsBitBtn;
    sDBEdit8: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sBitBtn20: TsBitBtn;
    sDBEdit9: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sBitBtn21: TsBitBtn;
    sDBEdit12: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sBitBtn23: TsBitBtn;
    sDBEdit13: TsDBEdit;
    sBitBtn26: TsButton;
    sPanel17: TsPanel;
    edt_TQTYCUR: TsEdit;
    sBitBtn24: TsBitBtn;
    edt_TQTY: TsCurrencyEdit;
    edt_TAMT: TsCurrencyEdit;
    edt_TAMTCUR: TsEdit;
    sBitBtn25: TsBitBtn;
    sBitBtn27: TsButton;
    GoodsExcelBtn: TsSpeedButton;
    GoodsSampleBtn: TsSpeedButton;
    excelOpen: TsOpenDialog;
    sSpeedButton16: TsSpeedButton;
    sPanel22: TsPanel;
    sPanel24: TsPanel;
    sPanel32: TsPanel;
    sLabel4: TsLabel;
    sPanel11: TsPanel;
    sPanel13: TsPanel;
    sPanel14: TsPanel;
    sPanel18: TsPanel;
    sPanel15: TsPanel;
    sPanel19: TsPanel;
    sPanel12: TsPanel;
    sPanel20: TsPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Page_controlChange(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure sMaskEdit1DblClick(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
    procedure edt_SRCODEDblClick(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure edt_ORGN1NDblClick(Sender: TObject);
    procedure sBitBtn6Click(Sender: TObject);
    procedure Btn_DetailAddClick(Sender: TObject);
    procedure Btn_DetailOkClick(Sender: TObject);
    procedure sDBEdit3DblClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryListChk2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure btnPrintClick(Sender: TObject);
    procedure sDBEdit6Exit(Sender: TObject);
    procedure sBitBtn261Click(Sender: TObject);
    procedure sBitBtn27Click(Sender: TObject);
    procedure memo_REMARK1Change(Sender: TObject);
    procedure Btn_DetailAddMouseLeave(Sender: TObject);
    procedure Btn_DetailAddMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure btnCopyClick(Sender: TObject);
    procedure sDBEdit1DblClick(Sender: TObject);
    procedure sBitBtn15Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure memo_REMARK1KeyPress(Sender: TObject; var Key: Char);
    procedure detailMenuPopup(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure sButton1Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure qryListChk3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure GoodsExcelBtnClick(Sender: TObject);
    procedure GoodsSampleBtnClick(Sender: TObject);
    procedure edt_PAYETC5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBEdit5Exit(Sender: TObject);
  private
    { Private declarations }
    DOMOFC_SQL : string;
    DOMOFCDETAIL_SQL : String;
    procedure ReadDocument;
    procedure Readlist(fromDate, toDate : String; KeyValue : String=''); overload;
    procedure Readlist(OrderSyntax : string = ''); overload;

    procedure NewDocument;
    procedure CancelDocument;
    procedure EditDocument;
    procedure SaveDocument(Sender : TObject);
    procedure DeleteDocument;

    procedure ButtonEnable(Val : Boolean=true);
    procedure ButtonEnablePage5(val: Boolean=true);

    procedure ReadyDocument(DocNo : String);

   function CHECK_VALIDITY:String;
   function ReadListBetween(fromDate: String; toDate: String; KeyValue : string): Boolean;

   //마지막 콤포넌트에서 탭이나 엔터키 누르면 다음페이지로 이동하는 프로시져
   procedure DialogKey(var msg : TCMDialogKey); message CM_DIALOGKEY;

  public
    { Public declarations }

  protected
    { protected declaration }
//    ProgramControlType : TProgramControlType;
  end;

var
  UI_DOMOFC_frm: TUI_DOMOFC_frm;

implementation

{$R *.dfm}

uses MSSQL, StrUtils, DateUtils, MessageDefine, Commonlib, VarDefine, AutoNo
     ,SQLCreator, Clipbrd, KISCalendar, Dialog_SearchCustom,
  Dialog_CodeList, Dialog_ItemList, Dlg_ErrorMessage, DOMOFC_PRINT,
  QuickRpt, Dialog_CopyDOMOFC, DocumentSend, Dlg_RecvSelect, CreateDocuments,
  ComObj, excel2000;

procedure TUI_DOMOFC_frm.FormCreate(Sender: TObject);
begin
  inherited;
  DOMOFC_SQL := Self.qryList.SQL.Text;
  DOMOFCDETAIL_SQL := Self.qryDetail.SQL.Text;
  Page_control.ActivePageIndex := 0;
  ProgramControlType := ctView;

end;

procedure TUI_DOMOFC_frm.FormActivate(Sender: TObject);
var
  KeyValue : string;
begin
  inherited;

  if not qryList.Active then Exit;

  if not DMMssql.KISConnect.InTransaction then
  begin

    qryList.Close;
    qryList.Open;

  end;
end;

procedure TUI_DOMOFC_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
  ACtion := caFree;
  UI_DOMOFC_frm := nil;
  inherited;
end;

procedure TUI_DOMOFC_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_DOMOFC_frm.ReadDocument;
begin

  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

//----------------------------------------------------------------------------
//기본정보
//----------------------------------------------------------------------------
  //관리번호
  edt_MaintNo.Text := qryListMAINT_NO.AsString;
  //등록일자
  sMaskEdit1.Text := qryListDATEE.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;
  //신청일자
  edt_OFRDATE.Text := qryListOFR_DATE.AsString;                                                
  //발행번호
  edt_OFRNO.Text := qryListOFR_NO.AsString;

//----------------------------------------------------------------------------
//발행자
//----------------------------------------------------------------------------
  //발행자
  edt_SRCODE.Text := qryListSR_CODE.AsString;
  //무역대리점 등록번호
  edt_SRNO.Text := qryListSR_NO.AsString;
  //상호
  edt_SRNAME1.Text := qryListSR_NAME1.AsString;
  edt_SRNAME2.Text := qryListSR_NAME2.AsString;
  //전자서명
  edt_SRNAME3.Text := qryListSR_NAME3.AsString;
  //주소
  edt_SRADDR1.Text := qryListSR_ADDR1.AsString;
  edt_SRADDR2.Text := qryListSR_ADDR2.AsString;
  edt_SRADDR3.Text := qryListSR_ADDR3.AsString;

//----------------------------------------------------------------------------
//수요자
//----------------------------------------------------------------------------
  //수요자
  edt_UDCODE.Text := qryListUD_CODE.AsString;
  //사업자등록번호
  edt_UDNO.Text := qryListUD_NO.AsString;
  //상호
  edt_UDNAME1.Text := qryListUD_NAME1.AsString;
  edt_UDNAME2.Text := qryListUD_NAME2.AsString;
  //전자서명
  edt_UDNAME3.Text := qryListUD_NAME3.AsString;
  //주소
  edt_UDADDR1.Text := qryListUD_ADDR1.AsString;
  edt_UDADDR2.Text := qryListUD_ADDR2.AsString;
  edt_UDADDR3.Text := qryListUD_ADDR3.AsString;
  //수요자참조번호
  edt_UDRENO.Text := qryListUD_RENO.AsString;
  //대표공급물품 HS부호
  edt_HSCODE.Text := qryListHS_CODE.AsString;
  //유효기간
  edt_OFRSQ1.Text := qryListOFR_SQ1.AsString;
  edt_OFRSQ2.Text := qryListOFR_SQ2.AsString;
  edt_OFRSQ3.Text := qryListOFR_SQ3.AsString;
  edt_OFRSQ4.Text := qryListOFR_SQ4.AsString;
  edt_OFRSQ5.Text := qryListOFR_SQ5.AsString;

//----------------------------------------------------------------------------
//Page2
//----------------------------------------------------------------------------
  //검사방법
  edt_TSTINST1.Text := qryListTSTINST1.AsString;
  edt_TSTINST2.Text := qryListTSTINST2.AsString;
  edt_TSTINST3.Text := qryListTSTINST3.AsString;
  edt_TSTINST4.Text := qryListTSTINST4.AsString;
  edt_TSTINST5.Text := qryListTSTINST5.AsString;
  //포장방법
  edt_PCKINST1.Text := qryListPCKINST1.AsString;
  edt_PCKINST2.Text := qryListPCKINST2.AsString;
  edt_PCKINST3.Text := qryListPCKINST3.AsString;
  edt_PCKINST4.Text := qryListPCKINST4.AsString;
  edt_PCKINST5.Text := qryListPCKINST5.AsString;
  //결제조건
  edt_PAYETC1.Text := qryListPAY_ETC1.AsString;
  edt_PAYETC2.Text := qryListPAY_ETC2.AsString;
  edt_PAYETC3.Text := qryListPAY_ETC3.AsString;
  edt_PAYETC4.Text := qryListPAY_ETC4.AsString;
  edt_PAYETC5.Text := qryListPAY_ETC5.AsString;

//----------------------------------------------------------------------------
//Page3
//----------------------------------------------------------------------------
  //원산지국가
  edt_ORGN1N.Text := qryListORGN1N.AsString;
  edt_ORGN2N.Text := qryListORGN2N.AsString;
  edt_ORGN3N.Text := qryListORGN3N.AsString;
  edt_ORGN4N.Text := qryListORGN4N.AsString;
  edt_ORGN5N.Text := qryListORGN5N.AsString;
  //지방명
  edt_ORGN1LOC.Text := qryListORGN1LOC.AsString;
  edt_ORGN2LOC.Text := qryListORGN2LOC.AsString;
  edt_ORGN3LOC.Text := qryListORGN3LOC.AsString;
  edt_ORGN4LOC.Text := qryListORGN4LOC.AsString;
  edt_ORGN5LOC.Text := qryListORGN5LOC.AsString;
  //기타참조사항
  memo_REMARK1.Text := qryListREMARK_1.AsString;

//----------------------------------------------------------------------------
//Page4
//----------------------------------------------------------------------------
  //운송방법
  edt_TRNSID.Text := qryListTRNS_ID.AsString;
  edt_TRNSIDNAME.Text := qryListTRANSNME.AsString;
  //선적국가
  edt_LOADD.Text := qryListLOADD.AsString;
  //선적항
  edt_LOADLOC.Text := qryListLOADLOC.AsString;
  //선적시기
  edt_LOADTXT1.Text := qryListLOADTXT1.AsString;
  edt_LOADTXT2.Text := qryListLOADTXT2.AsString;
  edt_LOADTXT3.Text := qryListLOADTXT3.AsString;
  edt_LOADTXT4.Text := qryListLOADTXT4.AsString;
  edt_LOADTXT5.Text := qryListLOADTXT5.AsString;
  //도착국가
  edt_DEST.Text := qryListDEST.AsString;
  //도착항
  edt_DESTLOC.Text := qryListDESTLOC.AsString;
  //도착시기
  edt_DESTTXT1.Text := qryListDESTTXT1.AsString;
  edt_DESTTXT2.Text := qryListDESTTXT2.AsString;
  edt_DESTTXT3.Text := qryListDESTTXT3.AsString;
  edt_DESTTXT4.Text := qryListDESTTXT4.AsString;
  edt_DESTTXT5.Text := qryListDESTTXT5.AsString;

//----------------------------------------------------------------------------
//Page4
//----------------------------------------------------------------------------
  //총수량단위
  edt_TQTYCUR.Text := qryListTQTYCUR.AsString;
  //총수량
  edt_TQTY.Value := qryListTQTY.AsCurrency;
  //총합계단위
  edt_TAMTCUR.Text := qryListTQTYCUR.AsString;
  edt_TAMT.Value := qryListTAMT.AsCurrency;

  //Page5 Detail DBGrid 활성
  with qryDetail do
  begin
    Close;
    SQL.Text :=  DOMOFCDETAIL_SQL;
    SQL.Add(' WHERE KEYY = ' + QuotedStr(edt_MaintNo.Text));
    Open;
  end;

end;

procedure TUI_DOMOFC_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if not DataSet.RecordCount = 0 then ReadDocument;
end;

procedure TUI_DOMOFC_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
  sLabel3.Caption := '';
end;

procedure TUI_DOMOFC_frm.Readlist(fromDate, toDate, KeyValue: String);
begin
  with qryList do
  begin
    Close;
    SQL.Text := DOMOFC_SQL;
    SQL.Add(' WHERE DATEE BETWEEN '+ QuotedStr(fromDate)+' AND '+QuotedStr(toDate) );
    SQL.Add(' ORDER BY DATEE ');
    Open;

    if Trim(KeyValue) <> '' then
    begin
      Locate('MAINT_NO',KeyValue,[]);
    end;

    //데이터조회탭 부분
    com_SearchKeyword.ItemIndex := 0;
    Mask_SearchDate1.Text := fromDate;
    Mask_SearchDate2.Text := toDate;
  end;
end;

procedure TUI_DOMOFC_frm.FormShow(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctView;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);

  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Readlist(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'');

  EnabledControlValue(maintno_Panel);
  //문서내용1
  EnabledControlValue(page1_Right);
  //문서내용2
  EnabledControlValue(page2_Right);
  //상품내역
  EnabledControlValue(sPanel16);
  EnabledControlValue(sPanel17);

end;

procedure TUI_DOMOFC_frm.Page_controlChange(Sender: TObject);
begin
  inherited;    
  if (Sender as TsPageControl).ActivePageIndex = 3 then
  begin
    //관리번호가있는 패널  숨김
    //maintno_Panel.Visible := not (Page_control.ActivePageIndex = 3);
    //DBGrid3이 위치해 있는 패널 숨김
    LeftGrid_Panel.Visible := not (Page_control.ActivePageIndex = 3);
  end
  else
  begin
    //관리번호가있는 패널
    //maintno_Panel.Visible := True;
    //DBGrid3이 위치해 있는 패널
     LeftGrid_Panel.Visible := True;
  end;

  //데이터 신규,수정 작성시 데이터조회 페이지 검색부분 비활성화
  if ProgramControlType in [ctInsert , ctModify] then
    EnabledControlValue(sPanel1)
  else
    EnabledControlValue(sPanel1,False);

end;

procedure TUI_DOMOFC_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel2.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn3.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn4.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_DOMOFC_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if Page_control.ActivePageIndex = 3 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := DOMOFC_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        Mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 : SQL.Add(' WHERE H1.MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
      2 : SQL.Add(' WHERE UD_NAME1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ');
    end;

    Open;

  end;
end;

procedure TUI_DOMOFC_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_DOMOFC_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;

  if AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text])then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text,'');
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := Mask_toDate.Text;
end;

procedure TUI_DOMOFC_frm.ButtonEnable(Val: Boolean);
begin
  btnNew.Enabled := Val;
  btnEdit.Enabled := Val;
  btnDel.Enabled := Val;

  btnTemp.Enabled := not Val;
  btnSave.Enabled := not Val;
  btnCancel.Enabled := not Val;

  btnCopy.Enabled := Val;
  btnPrint.Enabled := Val;
end;

procedure TUI_DOMOFC_frm.ButtonEnablePage5(Val: Boolean);
begin
  Btn_DetailAdd.Enabled := Val;
  Btn_DetailMod.Enabled := Val;
  Btn_DetailDel.Enabled := Val;
  Btn_DetailOk.Enabled     := not Val;
  Btn_DetailCancel.Enabled := not Val;
  GoodsExcelBtn.Enabled := val;
  GoodsSampleBtn.Enabled := val;
end;

procedure TUI_DOMOFC_frm.btnNewClick(Sender: TObject);
begin
  inherited;

  if not DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.BeginTrans;

  NewDocument;
  edt_MaintNo.SetFocus;
end;

procedure TUI_DOMOFC_frm.NewDocument;
begin
 inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctInsert;
//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Append;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
  ButtonEnablePage5(True);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  Page_control.ActivePageIndex := 0;
  Page_controlChange(Page_control);
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------

  ClearControlValue(page1_Right);
  ClearControlValue(page2_Right);
  //page5
  EnabledControlValue(sPanel16);
  ClearControlValue(sPanel17);
  
  //상단의 관리번호 , 등록일자, ... 패널
  EnabledControlValue(maintno_Panel , False);
  //page1
  EnabledControlValue(page1_Right , False);
  EnabledControlValue(page2_Right , False);
  //page5
  //EnabledControlValue(sGroupBox11 , False);
  EnabledControlValue(sPanel17, False);
  //LeftDBGrid 검색 패널
  EnabledControlValue(sPanel53);




//------------------------------------------------------------------------------
//기초자료
//------------------------------------------------------------------------------
  //사용자
  edt_UserNo.Text := LoginData.sID;

   //관리번호
  edt_MaintNo.Text := DMAutoNo.GetDocumentNoAutoInc('DOMOFC');
  edt_MaintNo.ReadOnly := False;
  edt_MaintNo.Color := clWhite;

  //등록일자
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);

  //문서기능
  edt_msg1.ReadOnly := False;
  edt_msg1.Color := clWhite;
  edt_msg1.Text := '9';
  sBitBtn2.Enabled := True;

  //유형
  edt_msg2.ReadOnly := False;
  edt_msg2.Color := clWhite;
  edt_msg2.Text := 'AB';
  sBitBtn3.Enabled := True;

  //신청일자
  edt_OFRDATE.Text := FormatDateTime('YYYYMMDD',Now);

  with qryDetail do
  begin
    Close;
    SQL.Text :=  DOMOFCDETAIL_SQL;
    SQL.Add(' WHERE KEYY = ' + QuotedStr(edt_MaintNo.Text));
    Open;
  end;


end;

procedure TUI_DOMOFC_frm.btnCancelClick(Sender: TObject);
var
  CreateDate : TDateTime;
begin
  inherited;
  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;

  IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_CANCEL),'취소확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
  begin
    CancelDocument;

    //새로고침
    CreateDate := ConvertStr2Date(sMaskEdit1.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             edt_MaintNo.Text);

    if qryList.RecordCount = 0 then
    begin
      ClearControlValue(page1_Right);
      ClearControlValue(page2_Right);
      ClearControlValue(sPanel16);
      ClearControlValue(sPanel17);
    end;
  end;
end;

procedure TUI_DOMOFC_frm.CancelDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(True);
  //page5 버튼
  EnabledControlValue(sPanel9);

//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  Page_control.ActivePageIndex := 0;
  Page_controlChange(Page_control);
  sDBGrid1.Enabled := True;
  sDBGrid2.Enabled := True;
//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  //상단의 관리번호 , 등록일자, ... 패널
  EnabledControlValue(maintno_Panel);
  //문서정보
  EnabledControlValue(page1_Right);
  EnabledControlValue(page2_Right);
  //상품내역
  EnabledControlValue(sPanel16);
  EnabledControlValue(sPanel17);
  //LeftDBGrid 검색 패널
  EnabledControlValue(sPanel53,False);

end;

procedure TUI_DOMOFC_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  if not DMMssql.KISConnect.InTransaction then
    DMMssql.KISConnect.BeginTrans
  else
  begin
    DMMssql.KISConnect.RollbackTrans;
    DMMssql.KISConnect.BeginTrans;
  end;

  if AnsiMatchText(qryListCHK2.AsString,['','0','1','2','5','6']) then
  begin
    EditDocument;
    edt_OFRDATE.SetFocus;
  end
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_EDIT,'수정불가', MB_OK+MB_ICONINFORMATION);

end;

procedure TUI_DOMOFC_frm.EditDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctModify;
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
  ButtonEnablePage5(True);

//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  Page_control.ActivePageIndex := 0;
  Page_controlChange(Page_control);
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;

//------------------------------------------------------------------------------
// 읽기전용 해제
//------------------------------------------------------------------------------
  EnabledControlValue(maintno_Panel , False);
  //관리번호 수정불가
  edt_MaintNo.Enabled := False;
  //등록일자 수정가능
  sMaskEdit1.ReadOnly := False;
  btn_Cal.Enabled := True;
  //문서기능 수정가능
  edt_msg1.ReadOnly := False;
  sBitBtn7.Enabled := True;
  //유형 수정가능
  edt_msg2.ReadOnly := False;
  sBitBtn8.Enabled := True;

  //문서내용
  EnabledControlValue(page1_Right , False);
  EnabledControlValue(page2_Right , False);
  //PAGE5
  EnabledControlValue(sPanel16);
  EnabledControlValue(sPanel17, False);
  //LeftDBGrid 검색 패널
  EnabledControlValue(sPanel53);


end;


procedure TUI_DOMOFC_frm.SaveDocument(Sender: TObject);
var
  SQLCreate : TSQLCreate;
  DocNo,DocCount : String;
  CreateDate : TDateTime;
begin
//------------------------------------------------------------------------------
//데이터저장
//------------------------------------------------------------------------------
  try
//------------------------------------------------------------------------------
// 유효성 검사
//------------------------------------------------------------------------------
    if (Sender as TsButton).Tag = 1 then
    begin
      Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
      if Dlg_ErrorMessage_frm.Run_ErrorMessage( '국내발행물품매도확약서' ,CHECK_VALIDITY ) Then
      begin
        FreeAndNil(Dlg_ErrorMessage_frm);
        Exit;
      end;
    end;
//------------------------------------------------------------------------------
// SQL생성기
//------------------------------------------------------------------------------
    SQLCreate := TSQLCreate.Create;
    try
        with SQLCreate do
        begin
          DocNo := edt_MaintNo.Text;

        //----------------------------------------------------------------------
        // 문서헤더 및 DOMOFC_H1.DB
        //----------------------------------------------------------------------

          Case ProgramControlType of
            ctInsert :
            begin
              DMLType := dmlInsert;
              ADDValue('MAINT_NO',DocNo);
            end;

            ctModify :
            begin
              DMLType := dmlUpdate;
              ADDWhere('MAINT_NO',DocNo);
            end;
          end;

          SQLHeader('DOMOFC_H1');

          //문서저장구분(0:임시, 1:저장)
          ADDValue('CHK2',IntToStr((Sender as TsButton).Tag));
          //등록일자
          ADDValue('DATEE',sMaskEdit1.Text);
          //유저ID
          ADDValue('USER_ID',edt_UserNo.Text);
          //메세지1
          ADDValue('MESSAGE1',edt_msg1.Text);
          //메세지2
          ADDValue('MESSAGE2',edt_msg2.Text);
          //신청일자
          ADDValue('OFR_DATE',edt_OFRDATE.Text);
          //발행번호
          ADDValue('OFR_NO',edt_OFRNO.Text);
          //발행자
          ADDValue('SR_CODE',edt_SRCODE.Text);
          //무역대리점 등록번호
          ADDValue('SR_NO',edt_SRNO.Text);
          //상호
          ADDValue('SR_NAME1',edt_SRNAME1.Text);
          ADDValue('SR_NAME2',edt_SRNAME2.Text);
          //전자서명
          ADDValue('SR_NAME3',edt_SRNAME3.Text);
          //주소
          ADDValue('SR_ADDR1',edt_SRADDR1.Text);
          ADDValue('SR_ADDR2',edt_SRADDR2.Text);
          ADDValue('SR_ADDR3',edt_SRADDR3.Text);
          //수혜자
          ADDValue('UD_CODE',edt_UDCODE.Text);
          //사업자등록번호
          ADDValue('UD_NO',edt_UDNO.Text);
          //상호
          ADDValue('UD_NAME1',edt_UDNAME1.Text);
          ADDValue('UD_NAME2',edt_UDNAME2.Text);
          //전자서명
          ADDValue('UD_NAME3',edt_UDNAME3.Text);
          //주소
          ADDValue('UD_ADDR1',edt_UDADDR1.Text);
          ADDValue('UD_ADDR2',edt_UDADDR2.Text);
          ADDValue('UD_ADDR3',edt_UDADDR3.Text);
          //수요자참조번호
          ADDValue('UD_RENO',edt_UDRENO.Text);
          //대표공급물품 HS코드
          ADDValue('HS_CODE',edt_HSCODE.Text);
          //유호기간
          ADDValue('OFR_SQ1',edt_OFRSQ1.Text);
          ADDValue('OFR_SQ2',edt_OFRSQ2.Text);
          ADDValue('OFR_SQ3',edt_OFRSQ3.Text);
          ADDValue('OFR_SQ4',edt_OFRSQ4.Text);
          ADDValue('OFR_SQ5',edt_OFRSQ5.Text);
          //기타참조사항
          ADDValue('REMARK_1',memo_REMARK1.Text);

        end;

        with TADOQuery.Create(nil) do
        begin
          try
            Connection := DMMssql.KISConnect;
            SQL.Text := SQLCreate.CreateSQL;
            if sCheckBox1.Checked then
              Clipboard.AsText := SQL.Text;
            ExecSQL;
          finally
            Close;
            Free;
          end;
        end;

    //--------------------------------------------------------------------------
    // DOMOFC_H2
    //--------------------------------------------------------------------------
      with SQLCreate do
      begin

        SQLHeader('DOMOFC_H2');
        DocNo := edt_MaintNo.Text;

        Case ProgramControlType of
          ctInsert :
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO',DocNo);
          end;

          ctModify :
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO',DocNo);
          end;
        end;

        //검사방법
        ADDValue('TSTINST1',edt_TSTINST1.Text);
        ADDValue('TSTINST2',edt_TSTINST2.Text);
        ADDValue('TSTINST3',edt_TSTINST3.Text);
        ADDValue('TSTINST4',edt_TSTINST4.Text);
        ADDValue('TSTINST5',edt_TSTINST5.Text);
        //포장방법
        ADDValue('PCKINST1',edt_PCKINST1.Text);
        ADDValue('PCKINST2',edt_PCKINST2.Text);
        ADDValue('PCKINST3',edt_PCKINST3.Text);
        ADDValue('PCKINST4',edt_PCKINST4.Text);
        ADDValue('PCKINST5',edt_PCKINST5.Text);
        //결제조건
        ADDValue('PAY_ETC1',edt_PAYETC1.Text);
        ADDValue('PAY_ETC2',edt_PAYETC2.Text);
        ADDValue('PAY_ETC3',edt_PAYETC3.Text);
        ADDValue('PAY_ETC4',edt_PAYETC4.Text);
        ADDValue('PAY_ETC5',edt_PAYETC5.Text);

      end;

      with TADOQuery.Create(nil) do
       begin
         try
           Connection := DMMssql.KISConnect;
           SQL.Text := SQLCreate.CreateSQL;
           if sCheckBox1.Checked then
             Clipboard.AsText := SQL.Text;
           ExecSQL;
         finally
           Close;
           Free;
         end;
       end;

    //--------------------------------------------------------------------------
    // DOMOFC_H3
    //--------------------------------------------------------------------------
      with SQLCreate do
      begin
        DocNo := edt_MaintNo.Text;
        SQLHeader('DOMOFC_H3');

        Case ProgramControlType of
          ctInsert :
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO',DocNo);
          end;

          ctModify :
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO',DocNo);
          end;
        end;

        //원산지국가 , 지방명
        ADDValue('ORGN1N',edt_ORGN1N.Text);
        ADDValue('ORGN1LOC',edt_ORGN1LOC.Text);
        ADDValue('ORGN2N',edt_ORGN2N.Text);
        ADDValue('ORGN2LOC',edt_ORGN2LOC.Text);
        ADDValue('ORGN3N',edt_ORGN3N.Text);
        ADDValue('ORGN3LOC',edt_ORGN3LOC.Text);
        ADDValue('ORGN4N',edt_ORGN4N.Text);
        ADDValue('ORGN4LOC',edt_ORGN4LOC.Text);
        ADDValue('ORGN5N',edt_ORGN5N.Text);
        ADDValue('ORGN5LOC',edt_ORGN5LOC.Text);

        //운송방법
        ADDValue('TRNS_ID',edt_TRNSID.Text);
        //선적국가
        ADDValue('LOADD',edt_LOADD.Text);
        //선적항
        ADDValue('LOADLOC',edt_LOADLOC.Text);
        //선적시기
        ADDValue('LOADTXT1',edt_LOADTXT1.Text);
        ADDValue('LOADTXT2',edt_LOADTXT2.Text);
        ADDValue('LOADTXT3',edt_LOADTXT3.Text);
        ADDValue('LOADTXT4',edt_LOADTXT4.Text);
        ADDValue('LOADTXT5',edt_LOADTXT5.Text);
        //도착국가
        ADDValue('DEST',edt_DEST.Text);
        //도착항
        ADDValue('DESTLOC',edt_DESTLOC.Text);
        //도착시기
        ADDValue('DESTTXT1',edt_DESTTXT1.Text);
        ADDValue('DESTTXT2',edt_DESTTXT2.Text);
        ADDValue('DESTTXT3',edt_DESTTXT3.Text);
        ADDValue('DESTTXT4',edt_DESTTXT4.Text);
        ADDValue('DESTTXT5',edt_DESTTXT5.Text);
        //총수량단위
        ADDValue('TQTYCUR',edt_TQTYCUR.Text);
        //총수량
        ADDValue('TQTY',edt_TQTY.Text);
        //총금액단위
         ADDValue('TAMTCUR',edt_TAMTCUR.Text);
         //총금액
         ADDValue('TAMT',edt_TAMT.Text);
      end;

      with TADOQuery.Create(nil) do
       begin
         try
           Connection := DMMssql.KISConnect;
           SQL.Text := SQLCreate.CreateSQL;
           if sCheckBox1.Checked then
             Clipboard.AsText := SQL.Text;
           ExecSQL;
         finally
           Close;
           Free;
         end;
       end;

    finally
      SQLCreate.Free;
    end;

  //------------------------------------------------------------------------------
  // 프로그램 제어
  //------------------------------------------------------------------------------
    ProgramControlType := ctView;
  //------------------------------------------------------------------------------
  // 버튼정리
  //------------------------------------------------------------------------------
    ButtonEnable(True);
    //page5 버튼
    EnabledControlValue(sPanel9);

  //------------------------------------------------------------------------------
  // 접근제어
  //------------------------------------------------------------------------------
    Page_control.ActivePageIndex := 0;
    Page_controlChange(Page_control);
    sDBGrid1.Enabled := True;
    sDBGrid2.Enabled := True;
  //------------------------------------------------------------------------------
  // 데이터제어
  //------------------------------------------------------------------------------
    //상단의 관리번호 , 등록일자, ... 패널
    EnabledControlValue(maintno_Panel);
    //문서정보
    EnabledControlValue(page1_Right);
    EnabledControlValue(page2_Right);
    //상품내역
    EnabledControlValue(sPanel16);
    EnabledControlValue(sPanel17);
    //LeftDBGrid 검색 패널
    EnabledControlValue(sPanel53,False);


  //----------------------------------------------------------------------------
  //새로고침
  //----------------------------------------------------------------------------
    CreateDate := ConvertStr2Date(sMaskEdit1.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             DocNo);

    DMMssql.KISConnect.CommitTrans;

  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message),'저장오류',MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TUI_DOMOFC_frm.btnSaveClick(Sender: TObject);
begin
  inherited;
  SaveDocument(Sender);         

end;

procedure TUI_DOMOFC_frm.sMaskEdit1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

 (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TUI_DOMOFC_frm.btn_CalClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    901 : sMaskEdit1DblClick(sMaskEdit1);
    902 : sMaskEdit1DblClick(Mask_SearchDate1);
    903 : sMaskEdit1DblClick(Mask_SearchDate2);
  end;
end;

//------------------------------------------------------------------------------
//User Code
//------------------------------------------------------------------------------
procedure TUI_DOMOFC_frm.edt_SRCODEDblClick(Sender: TObject);
begin
  inherited;
  Dialog_SearchCustom_frm := TDialog_SearchCustom_frm.Create(Self);

  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      IF Dialog_SearchCustom_frm.openDialog((Sender as TsEdit).Text) = mrOK then
      begin
        (Sender as TsEdit).Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        case (Sender as TsEdit).Tag of
          //발행자
          101 :
          begin
              edt_SRCODE.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
              edt_SRNO.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('TRAD_NO').AsString;
              edt_SRNAME1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              edt_SRNAME2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
              edt_SRNAME3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('Jenja').AsString;
              edt_SRADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_SRADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              edt_SRADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
          end;

          //수혜자
          102 :
          begin
              edt_UDCODE.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
              edt_UDNO.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('SAUP_NO').AsString;
              edt_UDNAME1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              edt_UDNAME2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_NAME').AsString;
              edt_UDNAME3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('Jenja').AsString;
              edt_UDADDR1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_UDADDR2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              edt_UDADDR3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR3').AsString;
          end;
        end;
      end;
    end;
  finally
    FreeAndNil(Dialog_SearchCustom_frm);
  end;
end;

procedure TUI_DOMOFC_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    101 : edt_SRCODEDblClick(edt_SRCODE);
    102 : edt_SRCODEDblClick(edt_UDCODE);
  end;
end;

//CODE 관련
procedure TUI_DOMOFC_frm.edt_ORGN1NDblClick(Sender: TObject);
begin
  inherited;
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    if ((Sender as TsEdit).ReadOnly = False) then
    begin
      if Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
      begin
          //선택된 코드 출력
          (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;

          //선택된 코드의 값까지 출력
          case (Sender as TsEdit).Tag of

              //원산지국가,지방명
//              301 : edt_ORGN1LOC.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
//              302 : edt_ORGN2LOC.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
//              303 : edt_ORGN3LOC.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
//              304 : edt_ORGN4LOC.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
//              305 : edt_ORGN5LOC.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              //운송방법
              401 : edt_TRNSIDNAME.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              //선적국가
              //402 : edt_LOADLOC.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              //403 : edt_DESTLOC.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
          end;
      end;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;

end;

//CODE 관련
procedure TUI_DOMOFC_frm.sBitBtn6Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    //문서기능
      1 : edt_ORGN1NDblClick(edt_msg1);
    //유형
      2 : edt_ORGN1NDblClick(edt_msg2);
    301 : edt_ORGN1NDblClick(edt_ORGN1N);
    302 : edt_ORGN1NDblClick(edt_ORGN2N);
    303 : edt_ORGN1NDblClick(edt_ORGN3N);
    304 : edt_ORGN1NDblClick(edt_ORGN4N);
    305 : edt_ORGN1NDblClick(edt_ORGN5N);
    401 : edt_ORGN1NDblClick(edt_TRNSID);
    402 : edt_ORGN1NDblClick(edt_LOADD);
    403 : edt_ORGN1NDblClick(edt_DEST);
    502 : sDBEdit3DblClick(sDBEdit3);
    503 : sDBEdit3DblClick(sDBEdit5);
    504 : sDBEdit3DblClick(sDBEdit7);
    505 : sDBEdit3DblClick(sDBEdit9);
    506 : sDBEdit3DblClick(sDBEdit11);
    507 : sDBEdit3DblClick(sDBEdit13);
    508 : edt_ORGN1NDblClick(edt_TQTYCUR);
    509 : edt_ORGN1NDblClick(edt_TAMTCUR);

  end;
end;

procedure TUI_DOMOFC_frm.Btn_DetailAddClick(Sender: TObject);
begin
  inherited;
  if not qryDetail.Active then Exit;

  EnabledControlValue(sPanel16,false);
  sBitBtn27.Enabled := False;

  case (Sender as TsSpeedButton).Tag of

    1 :
      begin
        qryDetail.Append;
        qryDetail.FieldByName('SEQ').AsInteger := qryDetail.RecordCount+1;
        ButtonEnablePage5(False);
        detailDBGrid.Enabled := False;

      end;

    2 :
      begin
        if qryDetail.RecordCount > 0 then
        begin
          qryDetail.Edit;
          ButtonEnablePage5(False);
          detailDBGrid.Enabled := False;
        end;
      end;

    3 :
      begin
        if qryDetail.RecordCount > 0 then
        begin
          if ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'삭제후 저장시에는 복구가 불가능합니다.') then
          begin
            qryDetail.Delete;
          end;

          EnabledControlValue(sPanel16);
          EnabledControlValue(sPanel17,False);
        end;
      end;

  end;
end;

procedure TUI_DOMOFC_frm.Btn_DetailOkClick(Sender: TObject);
begin
  inherited;
  if not qryDetail.Active then Exit;
  ButtonEnablePage5();

  case (Sender as TsSpeedButton).Tag of

    4 :
      begin
        qryDetail.FieldByName('KEYY').AsString := edt_MaintNo.Text;
        qryDetail.Post;
        detailDBGrid.Enabled := True;
      end;

    5 :
      begin
        qryDetail.Cancel;
        detailDBGrid.Enabled := True;  
      end;
  end;

  EnabledControlValue(sPanel16);
  sBitBtn27.Enabled := True;

end;

//DBEDIT CODE 관련
procedure TUI_DOMOFC_frm.sDBEdit3DblClick(Sender: TObject);
begin
  inherited;
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    if ((Sender as TsDBEdit).ReadOnly = False) then
    begin
      if Dialog_CodeList_frm.openDialog((Sender as TsDBEdit).Hint) = mrOK then
      begin
          //선택된 코드 출력
          //(Sender as TsDBEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
           (Sender as TsDBEdit).DataSource.DataSet.FieldByName((Sender as TsDBEdit).DataField).AsString
                            := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
      end;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;
end;

//DBEDIT ITEM 관련
function TUI_DOMOFC_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  nYear,nMonth,nDay : Word;

begin
  ErrMsg := TStringList.Create;

  Try

    IF Trim(edt_OFRNO.Text) = '' THEN
      ErrMsg.Add('[Page1] 발행번호를 입력해야합니다.');

    IF Trim(edt_OFRDATE.Text) = '' THEN
      ErrMsg.Add('[Page1] 발행일자를 입력해야합니다.');

    IF Trim(edt_SRNO.Text) = '' THEN
      ErrMsg.Add('[Page1] 무역대리점등록번호를 입력해야합니다.');

    IF Trim(edt_SRNAME1.Text) = '' THEN
      ErrMsg.Add('[Page1] 발행자 상호를 입력해야합니다.');

    IF Trim(edt_UDNO.Text) = '' THEN
      ErrMsg.Add('[Page1]  수요자 사업자번호를 입력해야합니다.');

    IF Trim(edt_UDNAME1.Text) = '' THEN
      ErrMsg.Add('[Page1] 수요자 상호를 입력해야합니다.');

    IF Trim(edt_TQTYCUR.Text) = '' THEN
      ErrMsg.Add('[Page5] 총수량단위를 입력해야합니다.');

    IF Trim(edt_TQTY.Text) = '' THEN
      ErrMsg.Add('[Page5] 총수량을 입력해야합니다.');

    IF Trim(edt_TAMTCUR.Text) = '' THEN
      ErrMsg.Add('[Page5] 총합계 단위를 입력해야합니다.');

    IF Trim(edt_TAMT.Text) = '' THEN
      ErrMsg.Add('[Page5] 총합계를 입력해야합니다.');

    if qryDetail.RecordCount = 0 then
      ErrMsg.Add('[Page5] 제품을 등록해야 합니다.');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

procedure TUI_DOMOFC_frm.btnDelClick(Sender: TObject);
begin
  inherited;

  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;
end;

procedure TUI_DOMOFC_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MaintNo.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM DOMOFC_D WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFC_H1 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFC_H2 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFC_H3 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else if qryList.RecordCount = 0 then
        begin
          ClearControlValue(page1_Right);
          ClearControlValue(page2_Right);
          ClearControlValue(sPanel16);
          ClearControlValue(sPanel17);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;
end;

procedure TUI_DOMOFC_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);
                                                                                                            
  with Sender as TsDBGrid do
  begin

    case CHK2Value of
      9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

      6 :
      begin
        if AnsiMatchText( Column.FieldName , ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_DOMOFC_frm.qryListChk2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;
begin
  inherited;
  nIndex := StrToIntDef( qryListCHK2.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TUI_DOMOFC_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DOMOFC_PRINT_frm := TDOMOFC_PRINT_frm.Create(Self);
  try
    DOMOFC_PRINT_frm.PrintDocument(qryList.Fields,qryDetail.Fields ,True);
  finally
    FreeAndNil(DOMOFC_PRINT_frm);
  end;
  
end;

procedure TUI_DOMOFC_frm.sDBEdit6Exit(Sender: TObject);
begin
  inherited;
  if sDBEdit6.ReadOnly = False then
  begin
    if qryDetail.FieldByName('QTY').AsString = '' then
    begin
      qryDetail.FieldByName('QTY').AsFloat := 1;
    end;

    //단가 기준수량 입력전 값을 1을 대입
    //if qryDetail.FieldByName('QTYG').AsString = '' then
    //  qryDetail.FieldByName('QTYG').AsFloat := 1;
      
    // (수량 * 단가) / 단가기준수량 = 금액
    if (Trim(qryDetail.FieldByName('QTYG').AsString) = '') or (qryDetail.FieldByName('QTYG').AsCurrency = 0) then
    begin
      qryDetail.FieldByName('AMT').AsCurrency := qryDetail.FieldByName('QTY').AsCurrency * qryDetail.FieldByName('PRICE').AsCurrency ;
    end
    else
    begin
      qryDetail.FieldByName('AMT').AsCurrency := (qryDetail.FieldByName('QTY').AsCurrency * qryDetail.FieldByName('PRICE').AsCurrency ) / qryDetail.FieldByName('QTYG').AsCurrency;
    end; 
  end;
end;

procedure TUI_DOMOFC_frm.sBitBtn261Click(Sender: TObject);
begin
  inherited;

  //소량소계의 단위는 수량의 단위와 같다
  qryDetail.FieldByName('STQTY_G').AsString := qryDetail.FieldByName('QTY_G').AsString;
  //소량소계의 값은 수량과 같다
  qryDetail.FieldByName('STQTY').AsInteger :=  qryDetail.FieldByName('QTY').AsInteger;
  //금액소계의 단위는 금액의 단위와 같다
  qryDetail.FieldByName('STAMT_G').AsString := qryDetail.FieldByName('AMT_G').AsString;
  //금액소계의 값은 금액과 같다
   qryDetail.FieldByName('STAMT').AsInteger := qryDetail.FieldByName('AMT').AsInteger;
end;

procedure TUI_DOMOFC_frm.sBitBtn27Click(Sender: TObject);
var
  totalAmount,totalMoney : Double;
begin
  inherited;

  totalAmount := 0;
  totalMoney := 0;

  qryDetail.First;

  //총수량의 단위는 첫번쨰 레코드의 단위와 같다
  edt_TQTYCUR.Text := qryDetail.FieldByName('STQTY_G').AsString;
  //총금액의 단위는 첫번째 레코드의 단위와 같다
  edt_TAMTCUR.Text := qryDetail.FieldByName('STAMT_G').AsString;

  while not qryDetail.Eof do
  begin
    //총수량
    totalAmount := qryDetail.FieldByName('STQTY').AsFloat + totalAmount;
    //총금액
    totalMoney := qryDetail.FieldByName('STAMT').AsFloat + totalMoney;

    qryDetail.Next;
  end;

   edt_TQTY.Value := totalAmount;
   edt_TAMT.Value := totalMoney;
    
end;

procedure TUI_DOMOFC_frm.memo_REMARK1Change(Sender: TObject);
var
  memoRow , memoCol : Integer;
begin
  inherited;
    //메모의 라인번호
    memoRow := memo_REMARK1.Perform(EM_LINEFROMCHAR,memo_REMARK1.SelStart,0);
    //메모의 컬럼번호
    memoCol := memo_REMARK1.SelStart - memo_REMARK1.Perform(EM_LINEINDEX, memoRow, 0);

    sLabel3.Caption := IntToStr(memoRow+1)+'행   ' + IntToStr(memoCol)+'열';


end;

procedure TUI_DOMOFC_frm.Btn_DetailAddMouseLeave(Sender: TObject);
begin
  inherited;
  (Sender as TsSpeedButton).Font.Style := [fsBold];
end;

procedure TUI_DOMOFC_frm.Btn_DetailAddMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  (Sender as TsSpeedButton).Font.Style := [fsBold, fsUnderline];
end;

procedure TUI_DOMOFC_frm.btnCopyClick(Sender: TObject);
var
  copyDocNo , newDocNo : String;
begin
  inherited;
    if qryList.RecordCount = 0 then Exit;
       
    Dialog_CopyDOMOFC_frm := TDialog_CopyDOMOFC_frm.Create(Self);
      try
          newDocNo := DMAutoNo.GetDocumentNoAutoInc('DOMOFC');
          copyDocNo := Dialog_CopyDOMOFC_frm.openDialog;

          //ShowMessage(copyDocNo);

          IF Trim(copyDocNo) = '' then Exit;

          //------------------------------------------------------------------------------
          // COPY
          //------------------------------------------------------------------------------
          with spCopyDOMOFC do
          begin
            Close;
            Parameters.ParamByName('@CopyDocNo').Value := copyDocNo;
            Parameters.ParamByName('@NewDocNo').Value :=  newDocNo;
            Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

            if not DMMssql.KISConnect.InTransaction then
              DMMssql.KISConnect.BeginTrans;

            try
              ExecProc;
            except
              on e:Exception do
              begin
                DMMssql.KISConnect.RollbackTrans;
                MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
              end;
            end;

          end;
        // ReadListBetween(해당년도 1월1일 , 오늘날짜 , 새로복사 된  관리번호)
        if  ReadListBetween(FormatDateTime('YYYYMMDD', StartOfTheYear(Now)),FormatDateTime('YYYYMMDD',Now),newDocNo) Then
        begin

          EditDocument;
          sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
          edt_OFRDATE.Text := FormatDateTime('YYYYMMDD',Now);
          edt_MaintNo.Text := newDocNo;
    
        end
        else
          raise Exception.Create('복사한 데이터를 찾을수 없습니다');


      finally

        FreeAndNil(Dialog_CopyDOMOFC_frm);

      end;

end;

function TUI_DOMOFC_frm.ReadListBetween(fromDate, toDate,
  KeyValue: string): Boolean;
begin
  Result := False;
  with qryList do
  begin
    Close;
    SQL.Text := DOMOFC_SQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC');
    Open;

    //ShowMessage(SQL.Text);

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;

  end;
end;

procedure TUI_DOMOFC_frm.sDBEdit1DblClick(Sender: TObject);
begin
  inherited;
  Dialog_ItemList_frm := TDialog_ItemList_frm.Create(Self);
  try
    if ((Sender as TsDBEdit).ReadOnly = False) then
    begin
      if Dialog_ItemList_frm.openDialog = mrok then
      begin
          //선택된 코드 품명 규격 출력
          qryDetail.FieldByName('NAME_COD').AsString := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
          //HSCODE
          qryDetail.FieldByName('HS_NO').AsString := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('HSCODE').AsString;
          //품명
          qryDetail.FieldByName('NAME1').AsString := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('FNAME').AsString;
          //규격
          qryDetail.FieldByName('SIZE1').AsString := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('MSIZE').AsString;
          //수량단위
          qryDetail.FieldByName('QTY_G').AsString := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('QTYC').AsString;
          //수량
          qryDetail.FieldByName('QTY').AsString := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('QTY_U').AsString;
          //단가단위
          qryDetail.FieldByName('PRICE_G').AsString := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('PRICEG').AsString;
          //단가
          qryDetail.FieldByName('PRICE').AsString := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('PRICE').AsString;
          //단가기준수량단위
          qryDetail.FieldByName('QTYG_G').AsString := Dialog_ItemList_frm.sDBGrid1.DataSource.DataSet.FieldByName('QTY_UG').AsString;
          //금액단위
          qryDetail.FieldByName('AMT_G').AsString := qryDetail.FieldByName('PRICE_G').AsString;
      end;
    end;
  finally
    FreeAndNil(Dialog_ItemList_frm);
  end;
end;

procedure TUI_DOMOFC_frm.sBitBtn15Click(Sender: TObject);
begin
  inherited;
  sDBEdit1DblClick(sDBEdit1);
end;

procedure TUI_DOMOFC_frm.N1Click(Sender: TObject);
begin
  if not qryDetail.Active then Exit;   

  if ProgramControlType in [ctInsert , ctModify] then
  begin
     EnabledControlValue(sPanel16,false);
    sBitBtn27.Enabled := False;

    case (Sender as TMenuItem).Tag of

      0 :
        begin
          qryDetail.Append;
          qryDetail.FieldByName('SEQ').AsInteger := qryDetail.RecordCount+1;
          ButtonEnablePage5(False);
          detailDBGrid.Enabled := False;

        end;
      1 :
        begin
          if qryDetail.RecordCount > 0 then
          begin
            qryDetail.Edit;
            ButtonEnablePage5(False);
            detailDBGrid.Enabled := False;
          end;
        end;  
      2 :
        begin
          if qryDetail.RecordCount > 0 then
          begin
            if ConfirmMessage('해당 데이터를 삭제하시겠습니까?'#13#10'삭제후 저장시에는 복구가 불가능합니다.') then
            begin
              qryDetail.Delete;
            end;
          end;
        end;
    end;
  end;
end;

procedure TUI_DOMOFC_frm.memo_REMARK1KeyPress(Sender: TObject;
  var Key: Char);
var
  limitcount : Integer;
begin
  limitcount := 15;
  if (Key = #13) AND ((Sender as TsMemo).lines.count >= limitCount-1) then Key := #0;

end;

procedure TUI_DOMOFC_frm.detailMenuPopup(Sender: TObject);
begin
  inherited;
    N1.Enabled :=  ProgramControlType in [ctInsert , ctModify];
    N2.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryDetail.RecordCount > 0);
    N3.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryDetail.RecordCount > 0);
end;

procedure TUI_DOMOFC_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
//

end;

procedure TUI_DOMOFC_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_DOMOFC_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := DOMOFC(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'DOMOFC';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'DOMOFC_H1';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        Readlist(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;

end;

procedure TUI_DOMOFC_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_DOMOFC_frm.qryListChk3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var                                                         
  nIndex : integer;  
begin
  inherited;

  IF Length(qryListCHK3.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(qryListCHK3.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;
end;

procedure TUI_DOMOFC_frm.GoodsExcelBtnClick(Sender: TObject);
var
  ExcelApp , ExcelBook , ExcelSheet : Variant;
  ExcelfileName : String;
  ExcelRows, ExcelCols, StartRow : Integer;
  i : Integer;
begin
  //세금계산서 엑셀가져오기-------------------------------------------------------
  inherited;
  try
    ExcelApp := CreateOleObject('Excel.Application');
  except
    ShowMessage('Excel이 설치되어 있지 않습니다.');
    Exit;
  end;

  //excelOpen.Filter := 'All Files (*.*)|*.*|Microsoft Excel 통합문서(*.xls)|*.xls|엑셀2007(*.xlsx)|*.xlsx';

  if not excelOpen.Execute then Exit;

  //excelOpen.FileName을 사용해도 되지만 윈도우상에 버그로 글씨가 깨지거나함
  ExcelfileName := excelOpen.Files.Strings[0]; //선택된 엑셀파일이름

  try
    with TADOQuery.Create(nil) do
    begin
      Connection := DMMssql.KISConnect;
      try
        SQL.Text :=  'DELETE FROM DOMOFC_D WHERE KEYY =' + QuotedStr(edt_MaintNo.Text);
        ExecSQL;

        qryDetail.Close;
        qryDetail.SQL.Text :=  DOMOFCDETAIL_SQL;
        qryDetail.SQL.Add(' WHERE KEYY = ' + QuotedStr(edt_MaintNo.Text));
        qryDetail.Open;

      finally
        Close;
        Free;
      end;
    end;

    ExcelApp.Visible := False; //엑셀보이지 않기
    ExcelApp.DisplayAlerts := False; //메시지표시않기

    ExcelBook := ExcelApp.WorkBooks.Open(ExcelfileName);
    ExcelSheet := ExcelBook.Sheets['Sheet1'];

    ExcelRows := ExcelApp.ActiveSheet.UsedRange.Rows.count; //총 row수 구하기
    ExcelCols := ExcelApp.ActiveSheet.UsedRange.Columns.count; //총 Col수 구하기

    for i := 2 to ExcelRows do
    begin
      qryDetail.Append;

      qryDetail.FieldByName('KEYY').AsString := edt_MaintNo.Text;
      qryDetail.FieldByName('SEQ').AsInteger := i-1;
      qryDetail.FieldByName('NAME_COD').AsString := ExcelSheet.Cells[i,1].Value; //품명
      qryDetail.FieldByName('NAME1').AsString :=    ExcelSheet.Cells[i,2].Value; //품명
      qryDetail.FieldByName('SIZE1').AsString :=    ExcelSheet.Cells[i,3].Value; //규격
      qryDetail.FieldByName('HS_NO').AsString :=    ExcelSheet.Cells[i,4].Value; //hs부호
      qryDetail.FieldByName('QTY').AsCurrency :=    ExcelSheet.Cells[i,5].Value; //수량
      qryDetail.FieldByName('QTY_G').AsString :=    ExcelSheet.Cells[i,6].Value; //수량단위
      qryDetail.FieldByName('PRICE').AsCurrency := ExcelSheet.Cells[i,7].Value; //단가
      qryDetail.FieldByName('PRICE_G').AsString := ExcelSheet.Cells[i,8].Value; //단가단위
      qryDetail.FieldByName('QTYG').AsCurrency := ExcelSheet.Cells[i,9].Value; //단가기준수량
      qryDetail.FieldByName('QTYG_G').AsString := ExcelSheet.Cells[i,10].Value; //단가기준수량단위
      qryDetail.FieldByName('AMT').AsCurrency := ExcelSheet.Cells[i,11].Value; //금액
      qryDetail.FieldByName('AMT_G').AsString := ExcelSheet.Cells[i,12].Value; //금액단위
      qryDetail.FieldByName('STQTY').AsCurrency := ExcelSheet.Cells[i,13].Value; //수량소계
      qryDetail.FieldByName('STQTY_G').AsString := ExcelSheet.Cells[i,14].Value; //수량소계단위
      qryDetail.FieldByName('STAMT').AsCurrency := ExcelSheet.Cells[i,15].Value; //수금액소계
      qryDetail.FieldByName('STAMT_G').AsString := ExcelSheet.Cells[i,16].Value; //금액소계단위

      qryDetail.Post;
    end;
    //ShowMessage( ExcelSheet.cells[1,2].value ) ;

    //접근제어------------------------------------------------------------------
    ButtonEnablePage5(True); //버튼정리
    EnabledControlValue(sPanel16);
    //새로고침------------------------------------------------------------------
    qryDetail.Close;
    qryDetail.SQL.Text :=  DOMOFCDETAIL_SQL;
    qryDetail.SQL.Add(' WHERE KEYY = ' + QuotedStr(edt_MaintNo.Text));
    qryDetail.Open;

  finally
    ExcelBook.Close;
    ExcelApp.Quit;
    ExcelApp := Unassigned;
    Finalize(ExcelSheet);
    Finalize(ExcelBook);
    Finalize(ExcelApp);
  end;

end;

procedure TUI_DOMOFC_frm.GoodsSampleBtnClick(Sender: TObject);
var
   ExcelApp , ExcelBook , ExcelSheet : Variant;
   i : Integer;
begin
  //상품내역 엑셀샘플----------------------------------------------------------
  inherited;
  try
    ExcelApp := CreateOleObject('Excel.Application');
  except
    ShowMessage('Excel이 설치되어 있지 않습니다.');
    Exit;
  end;

  ExcelApp.WorkBooks.Add; //새로운페이지 생성
  ExcelBook := ExcelApp.ActiveWorkBook; //워크북 추가
//  ExcelBook.Sheets.Add; //워크시트추가

  try
    ExcelSheet := ExcelBook.WorkSheets[1]; //작업할 워크시트 선택

    //셀에 타이틀에 들어갈 내용들 입력
    ExcelSheet.Cells[1,1].Value := '품명코드(선택)';
    ExcelSheet.Cells[1,2].Value := '품      명';
    ExcelSheet.Cells[1,3].Value := '규격(선택)';
    ExcelSheet.Cells[1,4].Value := 'HS부호(숫자만입력)';
    ExcelSheet.Cells[1,5].Value := '수      량';
    ExcelSheet.Cells[1,6].Value := '수량단위';
    ExcelSheet.Cells[1,7].Value := '단      가';
    ExcelSheet.Cells[1,8].Value := '단가통화';
    ExcelSheet.Cells[1,9].Value := '단가기준수량(선택)';
    ExcelSheet.Cells[1,10].Value := '단가기준수량단위(선택)';
    ExcelSheet.Cells[1,11].Value := '금     액';
    ExcelSheet.Cells[1,12].Value := '금액통화';
    ExcelSheet.Cells[1,13].Value := '수량소계';
    ExcelSheet.Cells[1,14].Value := '수량소계단위';
    ExcelSheet.Cells[1,15].Value := '금액소계';
    ExcelSheet.Cells[1,16].Value := '금액소계통화';

    //셀정렬
    for i := 1 to ExcelApp.ActiveSheet.UsedRange.Columns.count+1 do
    begin
      ExcelApp.Range[ExcelSheet.Cells[1,i],ExcelSheet.Cells[1,i]].HorizontalAlignment := xlHAlignCenter;

      //2018년 2월 2일 : 미그에선 품명만 필수이지만 김과장님께서 은행쪽에선 다받아서 처리한다고함  
      if (i = 1) or ( i = 3) or (i = 9) or (i = 10) then Continue;
      
      ExcelApp.Range[ExcelSheet.Cells[1,i],ExcelSheet.Cells[1,i]].font.bold := True; //폰트변경
    end;
    ExcelSheet.Columns.AutoFit; //Cell 사이즈 변경

    ExcelApp.Visible := True; //엑셀보이기
  //ExcelApp.DisplayAlerts := True; //메시지표시

  finally
    ExcelBook.Close;
    ExcelBook := Unassigned;
    ExcelSheet := Unassigned;
  end;
end;

procedure TUI_DOMOFC_frm.edt_PAYETC5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  ShowMessage(IntToStr(key));
end;

procedure TUI_DOMOFC_frm.DialogKey(var msg: TCMDialogKey);
begin
   if ActiveControl = nil then Exit;

   if (ActiveControl.Name = 'edt_PAYETC5') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
   begin
     Page_control.ActivePageIndex := 1;
     //edt_ORGN1N.SetFocus;
     Self.KeyPreview := False;
   end
   else if (ActiveControl.Name = 'memo_REMARK1') and (msg.CharCode = VK_TAB) then
   begin
     Page_control.ActivePageIndex := 2;
     Self.KeyPreview := False;
   end
   else
   begin
     Self.KeyPreview := True;
     inherited;
   end;
end;

procedure TUI_DOMOFC_frm.sDBEdit5Exit(Sender: TObject);
begin
  inherited;
  qryDetail.FieldByName('AMT_G').AsString := qryDetail.FieldByName('PRICE_G').AsString
end;

end.
