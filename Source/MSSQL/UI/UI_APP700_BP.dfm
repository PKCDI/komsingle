inherited UI_APP700_BP_frm: TUI_APP700_BP_frm
  Left = 505
  Top = 125
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 15
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1122
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter3: TsSplitter [1]
    Left = 0
    Top = 76
    Width = 1122
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPageControl1: TsPageControl [2]
    Left = 0
    Top = 79
    Width = 1122
    Height = 602
    ActivePage = sTabSheet2
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 1
    TabOrder = 1
    TabStop = False
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = 'Page1'
      object sSplitter4: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel18: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        BevelOuter = bvNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        SkinData.SkinSection = 'TRANSPARENT'
      end
      object sPanel51: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          814
          559)
        object sSpeedButton6: TsSpeedButton
          Left = 404
          Top = 48
          Width = 10
          Height = 419
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel5: TsPanel
          Left = 32
          Top = 48
          Width = 319
          Height = 23
          Caption = #44060#49444#51008#54665
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 45
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_In_Mathod1: TsEdit
          Left = 185
          Top = 106
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel2: TsPanel
          Left = 32
          Top = 272
          Width = 321
          Height = 23
          Caption = #49688#51077#50857#46020
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 46
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_ImpCd1_1: TsEdit
          Left = 185
          Top = 308
          Width = 168
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 47
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object btn_ImpCd1: TsBitBtn
          Tag = 105
          Left = 162
          Top = 308
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 48
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_ImpCd2: TsBitBtn
          Tag = 106
          Left = 162
          Top = 330
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 49
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_ImpCd2_1: TsEdit
          Left = 185
          Top = 330
          Width = 168
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 50
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ImpCd3_1: TsEdit
          Left = 185
          Top = 352
          Width = 168
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 51
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object btn_ImpCd3: TsBitBtn
          Tag = 107
          Left = 162
          Top = 352
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 52
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_ImpCd4: TsBitBtn
          Tag = 108
          Left = 162
          Top = 374
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 53
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_ImpCd4_1: TsEdit
          Left = 185
          Top = 374
          Width = 168
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 54
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ImpCd5_1: TsEdit
          Left = 185
          Top = 396
          Width = 168
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 55
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object btn_ImpCd5: TsBitBtn
          Tag = 109
          Left = 162
          Top = 396
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 56
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel8: TsPanel
          Left = 32
          Top = 452
          Width = 321
          Height = 25
          Caption = #44592#53440#51221#48372
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 57
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel9: TsPanel
          Left = 472
          Top = 452
          Width = 321
          Height = 25
          Caption = #47749#51032#51064#51221#48372
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 58
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object btn_LCNumber5: TsBitBtn
          Tag = 114
          Left = 685
          Top = 396
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 59
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_LCNumber4: TsBitBtn
          Tag = 113
          Left = 685
          Top = 374
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 60
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_LCNumber3: TsBitBtn
          Tag = 112
          Left = 685
          Top = 352
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 61
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_LCNumber2: TsBitBtn
          Tag = 111
          Left = 685
          Top = 330
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 62
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_LCNumber1: TsBitBtn
          Tag = 110
          Left = 685
          Top = 308
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 63
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel7: TsPanel
          Left = 472
          Top = 272
          Width = 321
          Height = 23
          Caption = 'I/L'#48264#54840
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 64
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object btn_GrantingCredit: TsBitBtn
          Tag = 104
          Left = 602
          Top = 172
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 65
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_AdPay1: TsEdit
          Left = 625
          Top = 172
          Width = 168
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 66
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel6: TsPanel
          Left = 472
          Top = 48
          Width = 321
          Height = 23
          Caption = #53685#51648#51008#54665
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 67
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object btn_OpenMethod: TsBitBtn
          Tag = 101
          Left = 162
          Top = 106
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 68
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_ApBank: TsBitBtn
          Tag = 102
          Left = 162
          Top = 128
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 69
          TabStop = False
          OnClick = btn_CreateBankCodeDblClick
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_NoticeBank: TsBitBtn
          Tag = 103
          Left = 602
          Top = 84
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 70
          TabStop = False
          OnClick = btn_CreateBankCodeDblClick
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Datee: TsMaskEdit
          Left = 112
          Top = 84
          Width = 73
          Height = 21
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#52397#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_In_Mathod: TsEdit
          Tag = 101
          Left = 112
          Top = 106
          Width = 49
          Height = 21
          Hint = #44060#49444#48169#48277
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#48169#48277
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_ApBank: TsEdit
          Tag = 102
          Left = 112
          Top = 128
          Width = 49
          Height = 21
          Hint = #51008#54665
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          OnDblClick = edt_ApBankDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51032#47280#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_ApBank1: TsEdit
          Left = 185
          Top = 128
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ApBank2: TsEdit
          Left = 185
          Top = 150
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ApBank3: TsEdit
          Left = 185
          Top = 172
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ApBank4: TsEdit
          Left = 185
          Top = 194
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ApBank5: TsEdit
          Left = 113
          Top = 216
          Width = 240
          Height = 19
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'Tel.'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AdBank: TsEdit
          Tag = 103
          Left = 552
          Top = 84
          Width = 49
          Height = 21
          Hint = #51008#54665
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          OnDblClick = edt_ApBankDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '('#55148#47581')'#53685#51648#51008#54665
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AdBank1: TsEdit
          Left = 625
          Top = 84
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AdBank2: TsEdit
          Left = 625
          Top = 106
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AdBank3: TsEdit
          Left = 625
          Top = 128
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AdBank4: TsEdit
          Left = 625
          Top = 150
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AdPay: TsEdit
          Tag = 104
          Left = 552
          Top = 172
          Width = 49
          Height = 21
          Hint = #49888#50857#44277#50668
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#44277#50668
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_ImpCd1: TsEdit
          Tag = 105
          Left = 112
          Top = 308
          Width = 49
          Height = 21
          Hint = 'IMP_CD'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#51077#50857#46020
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_ImpCd2: TsEdit
          Tag = 106
          Left = 112
          Top = 330
          Width = 49
          Height = 21
          Hint = 'IMP_CD'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ImpCd3: TsEdit
          Tag = 107
          Left = 112
          Top = 352
          Width = 49
          Height = 21
          Hint = 'IMP_CD'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ImpCd4: TsEdit
          Tag = 108
          Left = 112
          Top = 374
          Width = 49
          Height = 21
          Hint = 'IMP_CD'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ImpCd5: TsEdit
          Tag = 109
          Left = 112
          Top = 396
          Width = 49
          Height = 21
          Hint = 'IMP_CD'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILno1: TsEdit
          Left = 473
          Top = 308
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILCur1: TsEdit
          Tag = 110
          Left = 649
          Top = 308
          Width = 35
          Height = 21
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILAMT1: TsCurrencyEdit
          Left = 708
          Top = 308
          Width = 85
          Height = 21
          AutoSize = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 3
          DisplayFormat = '#,##0.###;0'
        end
        object edt_ILno2: TsEdit
          Left = 473
          Top = 330
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILCur2: TsEdit
          Tag = 111
          Left = 649
          Top = 330
          Width = 35
          Height = 21
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILAMT2: TsCurrencyEdit
          Left = 708
          Top = 330
          Width = 85
          Height = 21
          AutoSize = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 3
          DisplayFormat = '#,##0.###;0'
        end
        object edt_ILno3: TsEdit
          Left = 473
          Top = 352
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILCur3: TsEdit
          Tag = 112
          Left = 649
          Top = 352
          Width = 35
          Height = 21
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 27
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILAMT3: TsCurrencyEdit
          Left = 708
          Top = 352
          Width = 85
          Height = 21
          AutoSize = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 3
          DisplayFormat = '#,##0.###;0'
        end
        object edt_ILno4: TsEdit
          Left = 473
          Top = 374
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILCur4: TsEdit
          Tag = 113
          Left = 649
          Top = 374
          Width = 35
          Height = 21
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 30
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILAMT4: TsCurrencyEdit
          Left = 708
          Top = 374
          Width = 85
          Height = 21
          AutoSize = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 31
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 3
          DisplayFormat = '#,##0.###;0'
        end
        object edt_ILno5: TsEdit
          Left = 473
          Top = 396
          Width = 168
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILCur5: TsEdit
          Tag = 114
          Left = 649
          Top = 396
          Width = 35
          Height = 21
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 33
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_ILAMT5: TsCurrencyEdit
          Left = 708
          Top = 396
          Width = 85
          Height = 21
          AutoSize = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 3
          DisplayFormat = '#,##0.###;0'
        end
        object edt_AdInfo1: TsEdit
          Left = 33
          Top = 490
          Width = 320
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AdInfo2: TsEdit
          Left = 33
          Top = 512
          Width = 320
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AdInfo3: TsEdit
          Left = 33
          Top = 534
          Width = 320
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AdInfo4: TsEdit
          Left = 33
          Top = 556
          Width = 320
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 38
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_AdInfo5: TsEdit
          Left = 33
          Top = 578
          Width = 320
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 39
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_EXName1: TsEdit
          Left = 521
          Top = 490
          Width = 216
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#52397#50629#52404
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_EXName2: TsEdit
          Left = 521
          Top = 512
          Width = 216
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 41
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_EXName3: TsEdit
          Left = 521
          Top = 534
          Width = 80
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 42
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49885#48324#48512#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_EXAddr1: TsEdit
          Left = 521
          Top = 556
          Width = 216
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 43
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_EXAddr2: TsEdit
          Left = 521
          Top = 578
          Width = 216
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 44
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = 'Page2'
      object sSplitter7: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel23: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        SkinData.SkinSection = 'TRANSPARENT'
      end
      object sPanel21: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        object sLabel9: TsLabel
          Left = 472
          Top = 425
          Width = 19
          Height = 17
          Caption = '(%)'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object btn_TermsOfPrice: TsBitBtn
          Tag = 119
          Left = 323
          Top = 458
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 39
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_Doccd: TsBitBtn
          Tag = 115
          Left = 89
          Top = 46
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 37
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_MaximumCreditCode: TsBitBtn
          Tag = 118
          Left = 323
          Top = 392
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 38
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_CurrencyCode: TsBitBtn
          Tag = 117
          Left = 323
          Top = 359
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 31
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object btn_Beneficiary: TsBitBtn
          Tag = 116
          Left = 567
          Top = 165
          Width = 19
          Height = 21
          Cursor = crHandPoint
          TabOrder = 42
          TabStop = False
          OnClick = btn_CreateUserCodeDblClick
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel26: TsPanel
          Left = 32
          Top = 458
          Width = 254
          Height = 21
          Caption = 'Terms of Price'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel7: TsLabel
            Left = 0
            Top = 1
            Width = 23
            Height = 17
            Caption = '45A'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object sPanel24: TsPanel
          Left = 32
          Top = 425
          Width = 254
          Height = 21
          Caption = 'Percentage Credit Amount Tolerance'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel5: TsLabel
            Left = 3
            Top = 2
            Width = 23
            Height = 17
            Caption = '39A'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_cdPerP: TsCurrencyEdit
          Left = 312
          Top = 425
          Width = 54
          Height = 21
          AutoSize = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '+'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -16
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object edt_cdPerM: TsCurrencyEdit
          Left = 408
          Top = 425
          Width = 54
          Height = 21
          AutoSize = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '/  -'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -16
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object edt_CdMax1: TsEdit
          Left = 346
          Top = 392
          Width = 187
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_CdMax: TsEdit
          Tag = 118
          Left = 287
          Top = 392
          Width = 35
          Height = 21
          Hint = 'CD_MAX'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          OnDblClick = edt_CodeTypeDblClick
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel22: TsPanel
          Left = 32
          Top = 392
          Width = 254
          Height = 21
          Caption = 'Maximum Credit Amount'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel3: TsLabel
            Left = 0
            Top = 2
            Width = 22
            Height = 17
            Caption = '39B'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object sPanel20: TsPanel
          Left = 32
          Top = 359
          Width = 254
          Height = 21
          Caption = 'Currency Code,Amount'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel1: TsLabel
            Left = 4
            Top = 2
            Width = 22
            Height = 17
            Caption = '32B'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_Cdcur: TsEdit
          Tag = 117
          Left = 287
          Top = 359
          Width = 35
          Height = 21
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Cdamt: TsCurrencyEdit
          Left = 346
          Top = 359
          Width = 123
          Height = 21
          AutoSize = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 3
          DisplayFormat = '#,##0.###;0'
        end
        object edt_Applic5: TsEdit
          Left = 105
          Top = 253
          Width = 245
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'Tel.'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -13
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_Applic4: TsEdit
          Left = 105
          Top = 231
          Width = 245
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Applic3: TsEdit
          Left = 105
          Top = 209
          Width = 245
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Applic2: TsEdit
          Left = 105
          Top = 187
          Width = 245
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Applic1: TsEdit
          Left = 105
          Top = 165
          Width = 245
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'Applicant'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sPanel17: TsPanel
          Left = 55
          Top = 140
          Width = 190
          Height = 21
          Caption = 'Applicant'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel6: TsLabel
            Left = 6
            Top = 3
            Width = 14
            Height = 17
            Caption = '50'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object sPanel19: TsPanel
          Left = 455
          Top = 140
          Width = 190
          Height = 21
          Caption = 'Beneficiary'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 33
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel8: TsLabel
            Left = 1
            Top = 3
            Width = 14
            Height = 17
            Caption = '59'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_Benefc: TsEdit
          Tag = 116
          Left = 521
          Top = 165
          Width = 45
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          OnDblClick = edt_CreateUserCodeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'Beneficiary'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_Benefc1: TsEdit
          Left = 521
          Top = 187
          Width = 253
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Benefc2: TsEdit
          Left = 521
          Top = 209
          Width = 253
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Benefc3: TsEdit
          Left = 521
          Top = 231
          Width = 253
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Benefc4: TsEdit
          Left = 521
          Top = 253
          Width = 253
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = #44228#51340#48264#54840
        end
        object edt_Benefc5: TsEdit
          Left = 521
          Top = 275
          Width = 253
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_exPlace: TsEdit
          Left = 498
          Top = 68
          Width = 197
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '(Place) '
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_exDate: TsMaskEdit
          Left = 498
          Top = 46
          Width = 78
          Height = 21
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = '(Date) '
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel16: TsPanel
          Left = 440
          Top = 21
          Width = 209
          Height = 21
          Caption = 'Date and Place of Expiry'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel4: TsLabel
            Left = 2
            Top = 1
            Width = 23
            Height = 17
            Caption = '31D'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_Doccd1: TsEdit
          Left = 112
          Top = 46
          Width = 238
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel15: TsPanel
          Left = 8
          Top = 21
          Width = 240
          Height = 21
          Caption = 'Form of Documentary Credit'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel2: TsLabel
            Left = 8
            Top = 2
            Width = 23
            Height = 17
            Caption = '40A'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_Doccd: TsEdit
          Tag = 115
          Left = 55
          Top = 46
          Width = 33
          Height = 21
          Hint = 'DOC_CD'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_TermPR_M: TsEdit
          Left = 346
          Top = 458
          Width = 279
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          OnDblClick = btn_CodeTypeClick
          SkinData.SkinSection = 'EDIT'
        end
        object edt_TermPR: TsEdit
          Tag = 119
          Left = 287
          Top = 458
          Width = 35
          Height = 21
          Hint = #44032#44201#51312#44148
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel11: TsPanel
          Left = 32
          Top = 491
          Width = 254
          Height = 21
          Caption = 'Place of Terms of Price'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel10: TsLabel
            Left = 0
            Top = 1
            Width = 23
            Height = 17
            Caption = '45A'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_plTerm: TsEdit
          Left = 287
          Top = 491
          Width = 338
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel12: TsPanel
          Left = 32
          Top = 524
          Width = 254
          Height = 21
          Caption = 'Additional Amount Covered'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 41
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel11: TsLabel
            Left = 0
            Top = 2
            Width = 22
            Height = 17
            Caption = '39C'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_aaCcv1: TsEdit
          Left = 287
          Top = 524
          Width = 338
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
          SkinData.SkinSection = 'EDIT'
        end
        object edt_aaCcv2: TsEdit
          Left = 287
          Top = 546
          Width = 338
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
          SkinData.SkinSection = 'EDIT'
        end
        object edt_aaCcv3: TsEdit
          Left = 287
          Top = 568
          Width = 338
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object edt_aaCcv4: TsEdit
          Left = 287
          Top = 590
          Width = 338
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = 'Page3'
      object sSplitter5: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel13: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        SkinData.SkinSection = 'TRANSPARENT'
      end
      object sPanel14: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          814
          559)
        object sSpeedButton1: TsSpeedButton
          Left = 306
          Top = 56
          Width = 10
          Height = 379
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sLabel16: TsLabel
          Left = 616
          Top = 82
          Width = 146
          Height = 17
          Caption = #52572#45824' 400'#54665' 64'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sPanel28: TsPanel
          Left = 8
          Top = 77
          Width = 297
          Height = 23
          Caption = 'Drafts at ... ('#54868#54872#50612#51020#51312#44148')'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel12: TsLabel
            Left = 1
            Top = 2
            Width = 22
            Height = 17
            Caption = '42C'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_Draft1: TsEdit
          Left = 32
          Top = 102
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Draft3: TsEdit
          Left = 32
          Top = 146
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Draft2: TsEdit
          Left = 32
          Top = 124
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel29: TsPanel
          Left = 8
          Top = 245
          Width = 297
          Height = 23
          Caption = 'Mixed Payment Detail('#54844#54633#51648#44553#51312#44148')'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel13: TsLabel
            Left = 1
            Top = 2
            Width = 22
            Height = 17
            Caption = '42C'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_mixPay1: TsEdit
          Left = 32
          Top = 270
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
        object edt_mixPay2: TsEdit
          Left = 32
          Top = 292
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.SkinSection = 'EDIT'
        end
        object edt_mixPay3: TsEdit
          Left = 32
          Top = 314
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
        end
        object edt_mixPay4: TsEdit
          Left = 32
          Top = 336
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel30: TsPanel
          Left = 8
          Top = 435
          Width = 297
          Height = 23
          Caption = 'Deferred Payment Details('#50672#51648#44553#51312#44148#47749#49464')'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel14: TsLabel
            Left = 1
            Top = 2
            Width = 21
            Height = 17
            Caption = '42P'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_defPay1: TsEdit
          Left = 32
          Top = 460
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
        end
        object edt_defPay2: TsEdit
          Left = 32
          Top = 482
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object edt_defPay3: TsEdit
          Left = 32
          Top = 504
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
        object edt_defPay4: TsEdit
          Left = 32
          Top = 526
          Width = 241
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel10: TsPanel
          Left = 336
          Top = 77
          Width = 257
          Height = 23
          Caption = 'Description of Goods and/or Services'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel15: TsLabel
            Left = 1
            Top = 2
            Width = 23
            Height = 17
            Caption = '45A'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object btn_DescriptionServices: TsBitBtn
          Tag = 122
          Left = 594
          Top = 79
          Width = 19
          Height = 21
          Cursor = crHandPoint
          TabOrder = 16
          TabStop = False
          OnClick = btn_DescriptionServicesClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object memo_Desgood1: TsMemo
          Left = 336
          Top = 112
          Width = 481
          Height = 417
          Color = clWhite
          Ctl3D = True
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 25600
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 12
          OnDblClick = memoDblClick
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = 'Page4'
      object sSplitter8: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel27: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        SkinData.SkinSection = 'TRANSPARENT'
      end
      object sPanel31: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
      end
    end
    object sTabSheet5: TsTabSheet
      Caption = 'Page5'
      object sSplitter6: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel42: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        SkinData.SkinSection = 'TRANSPARENT'
      end
      object sPanel43: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
      end
    end
    object sTabSheet6: TsTabSheet
      Caption = 'Page6'
      object sSplitter9: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel44: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        SkinData.SkinSection = 'TRANSPARENT'
      end
      object sPanel45: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        object sLabel46: TsLabel
          Left = 256
          Top = 133
          Width = 146
          Height = 17
          Caption = #52572#45824' 400'#54665' 64'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel48: TsLabel
          Left = 40
          Top = 377
          Width = 522
          Height = 17
          Caption = 
            'ALL BANKING COMMISSIONS AND CHARGES INCLUDING REIMBURSEMENT CHAR' +
            'GES'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel50: TsLabel
          Left = 40
          Top = 399
          Width = 245
          Height = 17
          Caption = 'OUTSIDE KOREA ARE FOR ACCOUNT OF'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel52: TsLabel
          Left = 40
          Top = 474
          Width = 246
          Height = 17
          Caption = 'DOCUMENTS TO BE PRESENTED WITHIN'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel53: TsLabel
          Left = 328
          Top = 474
          Width = 228
          Height = 17
          Caption = 'DAYS AFTER THE DATE OF SHIPMENT'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel54: TsLabel
          Left = 40
          Top = 496
          Width = 259
          Height = 17
          Caption = 'BUT WITHIN THE VALIDITY OF THE CREDIT'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object check_acd2AA: TsCheckBox
          Left = 40
          Top = 48
          Width = 104
          Height = 19
          Caption = 'SHIPMENT BY'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object sPanel46: TsPanel
          Left = 38
          Top = 20
          Width = 265
          Height = 25
          Caption = '(C)Additional conditions..'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel49: TsLabel
            Left = 1
            Top = 3
            Width = 23
            Height = 17
            Caption = '45A'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_acd2AA1: TsEdit
          Left = 141
          Top = 48
          Width = 292
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object check_acd2AB: TsCheckBox
          Left = 40
          Top = 70
          Width = 463
          Height = 19
          Caption = 'ACCEPTANCE COMMISSION DISCOUNT CHARGES ARE FOR BUYER`S ACCOUNT'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object check_acd2AC: TsCheckBox
          Left = 40
          Top = 91
          Width = 316
          Height = 19
          Caption = 'ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object check_acd2AD: TsCheckBox
          Left = 40
          Top = 112
          Width = 237
          Height = 19
          Caption = 'LATE PRESENTATION B/L ACCEPTABLE'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object check_acd2AE: TsCheckBox
          Left = 40
          Top = 132
          Width = 211
          Height = 19
          Caption = 'OTHER CONDITION(S)    ( if any )'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object memo_acd2AE1: TsMemo
          Left = 147
          Top = 154
          Width = 494
          Height = 169
          TabStop = False
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 25600
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 10
          OnDblClick = memoDblClick
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn34: TsBitBtn
          Tag = 123
          Left = 127
          Top = 154
          Width = 19
          Height = 21
          Cursor = crHandPoint
          Enabled = False
          TabOrder = 11
          TabStop = False
          OnClick = btn_DescriptionServicesClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel48: TsPanel
          Left = 38
          Top = 348
          Width = 265
          Height = 25
          Caption = 'Charges'
          Color = 16042877
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel47: TsLabel
            Left = 1
            Top = 3
            Width = 22
            Height = 17
            Caption = '71B'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_Charge: TsEdit
          Tag = 120
          Left = 293
          Top = 397
          Width = 33
          Height = 21
          Hint = 'CHARGE'
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          OnDblClick = edt_CodeTypeDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn35: TsBitBtn
          Tag = 120
          Left = 327
          Top = 397
          Width = 19
          Height = 21
          Cursor = crHandPoint
          TabOrder = 13
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel49: TsPanel
          Left = 38
          Top = 444
          Width = 265
          Height = 25
          Caption = 'Period for Presentation'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel51: TsLabel
            Left = 1
            Top = 3
            Width = 14
            Height = 17
            Caption = '48'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_period: TsEdit
          Left = 293
          Top = 472
          Width = 33
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel50: TsPanel
          Left = 38
          Top = 541
          Width = 265
          Height = 25
          Caption = 'Confirmation Instructions'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel55: TsLabel
            Left = 1
            Top = 3
            Width = 14
            Height = 17
            Caption = '49'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_Confirm: TsEdit
          Tag = 121
          Left = 39
          Top = 570
          Width = 33
          Height = 21
          Hint = 'CONFIRM'
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          OnDblClick = edt_CodeTypeDblClick
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn36: TsBitBtn
          Tag = 121
          Left = 73
          Top = 570
          Width = 19
          Height = 21
          Cursor = crHandPoint
          TabOrder = 16
          TabStop = False
          OnClick = btn_CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Charge1: TsEdit
          Left = 347
          Top = 397
          Width = 214
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Confirm1: TsEdit
          Left = 93
          Top = 570
          Width = 210
          Height = 21
          HelpContext = 1
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    object sTabSheet7: TsTabSheet
      Caption = #45936#51060#53552#51312#54924' [F3]'
      object sSplitter2: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 33
        Width = 1114
        Height = 529
        TabStop = False
        Align = alClient
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK1'
            Title.Alignment = taCenter
            Title.Caption = #49440#53469
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #51200#51109
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 74
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 213
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 193
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 164
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LOC_AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529
            Width = 97
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'LOC_AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 38
            Visible = True
          end>
      end
      object sPanel3: TsPanel
        Left = 0
        Top = 3
        Width = 1114
        Height = 30
        Align = alTop
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object edt_SearchText: TsEdit
          Left = 86
          Top = 5
          Width = 233
          Height = 21
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 81
          Height = 23
          Alignment = taLeftJustify
          SkinData.SkinSection = 'COMBOBOX'
          VerticalAlignment = taAlignTop
          Style = csDropDownList
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ItemHeight = 17
          ItemIndex = 0
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          Text = #46321#47197#51068#51088
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 86
          Top = 5
          Width = 82
          Height = 21
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn1: TsBitBtn
          Left = 320
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 5
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn21: TsBitBtn
          Tag = 900
          Left = 167
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 2
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 214
          Top = 5
          Width = 82
          Height = 21
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          Text = '20161125'
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn23: TsBitBtn
          Tag = 907
          Left = 296
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 4
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel25: TsPanel
          Left = 190
          Top = 5
          Width = 25
          Height = 23
          Caption = '~'
          TabOrder = 7
          SkinData.SkinSection = 'PANEL'
        end
      end
    end
  end
  object sPanel1: TsPanel [3]
    Left = 0
    Top = 0
    Width = 1122
    Height = 41
    Align = alTop
    TabOrder = 2
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton2: TsSpeedButton
      Left = 210
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 512
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton4: TsSpeedButton
      Left = 595
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 293
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 1055
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 3
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Left = 71
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Left = 139
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Left = 222
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 524
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 305
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 373
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Left = 441
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
    end
    object sButton1: TsButton
      Left = 607
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
    end
    object sButton3: TsButton
      Left = 703
      Top = 2
      Width = 74
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [4]
    Left = 0
    Top = 44
    Width = 1122
    Height = 32
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object edt_MaintNo: TsEdit
      Left = 64
      Top = 5
      Width = 121
      Height = 23
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sMaskEdit1: TsMaskEdit
      Left = 246
      Top = 5
      Width = 83
      Height = 25
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_UserNo: TsEdit
      Left = 409
      Top = 5
      Width = 57
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 330
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 5
      TabStop = False
      ImageIndex = 9
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object sCheckBox1: TsCheckBox
      Left = 992
      Top = 6
      Width = 64
      Height = 19
      TabStop = False
      Caption = #46356#48260#44536
      TabOrder = 6
      ImgChecked = 0
      ImgUnchecked = 0
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 529
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      OnDblClick = edt_CodeTypeDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn2: TsBitBtn
      Tag = 1
      Left = 562
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 7
      TabStop = False
      OnClick = btn_CodeTypeClick
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 617
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      OnDblClick = edt_CodeTypeDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sBitBtn3: TsBitBtn
      Tag = 2
      Left = 650
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      Enabled = False
      TabOrder = 8
      TabStop = False
      OnClick = btn_CodeTypeClick
      ImageIndex = 25
      Images = DMICON.System18
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object sPanel52: TsPanel [5]
    Left = 3
    Top = 118
    Width = 300
    Height = 625
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    object sDBGrid2: TsDBGrid
      Left = 1
      Top = 34
      Width = 298
      Height = 590
      TabStop = False
      Align = alClient
      Color = clWhite
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object sPanel53: TsPanel
      Left = 1
      Top = 1
      Width = 298
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      object Mask_fromDate: TsMaskEdit
        Left = 121
        Top = 5
        Width = 74
        Height = 23
        TabStop = False
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn22: TsBitBtn
        Left = 269
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 1
        TabStop = False
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object Mask_toDate: TsMaskEdit
        Left = 196
        Top = 5
        Width = 74
        Height = 23
        TabStop = False
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51312#54924
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        SkinData.SkinSection = 'EDIT'
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 192
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      '')
    Left = 56
    Top = 192
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 88
    Top = 192
  end
end
