unit UI_APP700;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, ChildForm, sSkinProvider, UI_APP700_BP,
  Grids, DBGrids, acDBGrid, StdCtrls, sComboBox, ExtCtrls, sSplitter,
  sMemo, sCustomComboEdit, sCurrEdit, sCurrencyEdit, sLabel, sScrollBox,
  ComCtrls, sPageControl, sCheckBox, Mask, sMaskEdit, Buttons, sBitBtn,
  sEdit, sButton, sSpeedButton, sPanel, DB, ADODB, Clipbrd, zlibpas, sBevel,
  Menus;

type
  TUI_APP700_frm = class(TUI_APP700_BP_frm)            
    sPanel32: TsPanel;
    edt_lstDate: TsMaskEdit;
    sPanel33: TsPanel;
    edt_shipPD1: TsEdit;
    edt_shipPD2: TsEdit;
    edt_shipPD3: TsEdit;
    edt_shipPD4: TsEdit;
    edt_shipPD5: TsEdit;
    edt_shipPD6: TsEdit;
    edt_Tship: TsEdit;
    sBitBtn26: TsBitBtn;
    edt_Tship1: TsEdit;
    sPanel35: TsPanel;
    edt_Pship: TsEdit;
    sBitBtn17: TsBitBtn;
    edt_Pship1: TsEdit;
    sPanel34: TsPanel;
    sPanel37: TsPanel;
    edt_SunjukPort: TsEdit;
    edt_dochackPort: TsEdit;
    sPanel38: TsPanel;
    sLabel24: TsLabel;
    edt_loadOn: TsEdit;
    sPanel39: TsPanel;
    sLabel27: TsLabel;
    sPanel40: TsPanel;
    sLabel28: TsLabel;
    edt_forTran: TsEdit;
    sLabel30: TsLabel;
    edt_Origin: TsEdit;
    sBitBtn28: TsBitBtn;
    edt_Origin_M: TsEdit;
    sPanel41: TsPanel;
    sPanel47: TsPanel;
    edt_Doc380_1: TsEdit;
    check_Doc380: TsCheckBox;
    sLabel31: TsLabel;
    check_Doc705: TsCheckBox;
    edt_Doc705Gubun: TsEdit;
    sBitBtn29: TsBitBtn;
    sLabel33: TsLabel;
    sLabel32: TsLabel;
    edt_Doc705_1: TsEdit;
    sLabel35: TsLabel;
    sLabel36: TsLabel;
    edt_Doc705_3: TsEdit;
    sBitBtn30: TsBitBtn;
    edt_Doc705_3_1: TsEdit;
    sLabel37: TsLabel;
    edt_Doc705_2: TsEdit;
    edt_Doc705_4: TsEdit;
    check_Doc740: TsCheckBox;
    edt_Doc740_1: TsEdit;
    edt_Doc740_2: TsEdit;
    edt_Doc740_4: TsEdit;
    sLabel42: TsLabel;
    edt_Doc740_3_1: TsEdit;
    sBitBtn32: TsBitBtn;
    edt_Doc740_3: TsEdit;
    sLabel39: TsLabel;
    check_Doc760: TsCheckBox;
    sLabel40: TsLabel;
    edt_Doc760_1: TsEdit;
    edt_Doc760_3: TsEdit;
    sBitBtn31: TsBitBtn;
    edt_Doc760_3_1: TsEdit;
    sLabel43: TsLabel;
    edt_Doc760_2: TsEdit;
    edt_Doc760_4: TsEdit;
    check_Doc530: TsCheckBox;
    sLabel38: TsLabel;
    edt_Doc530_1: TsEdit;
    edt_Doc530_2: TsEdit;
    check_Doc271: TsCheckBox;
    edt_Doc271_1: TsEdit;
    sLabel44: TsLabel;
    check_Doc861: TsCheckBox;
    check_doc2AA: TsCheckBox;
    sLabel45: TsLabel;
    memo_doc2AA1: TsMemo;
    sBitBtn33: TsBitBtn;
    sLabel41: TsLabel;
    queryCheck: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_PAY: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListDOC_CD: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListprno: TIntegerField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListMAINT_NO_1: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListCD_AMT: TBCDField;
    qryListCD_CUR: TStringField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListDESGOOD_1: TMemoField;
    qryListMAINT_NO_2: TStringField;
    qryListDOC_380: TBooleanField;
    qryListDOC_380_1: TBCDField;
    qryListDOC_705: TBooleanField;
    qryListDOC_705_1: TStringField;
    qryListDOC_705_2: TStringField;
    qryListDOC_705_3: TStringField;
    qryListDOC_705_4: TStringField;
    qryListDOC_740: TBooleanField;
    qryListDOC_740_1: TStringField;
    qryListDOC_740_2: TStringField;
    qryListDOC_740_3: TStringField;
    qryListDOC_740_4: TStringField;
    qryListDOC_530: TBooleanField;
    qryListDOC_530_1: TStringField;
    qryListDOC_530_2: TStringField;
    qryListDOC_271: TBooleanField;
    qryListDOC_271_1: TBCDField;
    qryListDOC_861: TBooleanField;
    qryListDOC_2AA: TBooleanField;
    qryListDOC_2AA_1: TMemoField;
    qryListACD_2AA: TBooleanField;
    qryListACD_2AA_1: TStringField;
    qryListACD_2AB: TBooleanField;
    qryListACD_2AC: TBooleanField;
    qryListACD_2AD: TBooleanField;
    qryListACD_2AE: TBooleanField;
    qryListACD_2AE_1: TMemoField;
    qryListCHARGE: TStringField;
    qryListPERIOD: TBCDField;
    qryListCONFIRMM: TStringField;
    qryListINSTRCT: TBooleanField;
    qryListINSTRCT_1: TMemoField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListORIGIN: TStringField;
    qryListORIGIN_M: TStringField;
    qryListPL_TERM: TStringField;
    qryListTERM_PR: TStringField;
    qryListTERM_PR_M: TStringField;
    qryListSRBUHO: TStringField;
    qryListDOC_705_GUBUN: TStringField;
    qryListCARRIAGE: TStringField;
    qryListDOC_760: TBooleanField;
    qryListDOC_760_1: TStringField;
    qryListDOC_760_2: TStringField;
    qryListDOC_760_3: TStringField;
    qryListDOC_760_4: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListmathod_Name: TStringField;
    qryListpay_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListDOC_Name: TStringField;
    qryListCDMAX_Name: TStringField;
    qryListpship_Name: TStringField;
    qryListtship_Name: TStringField;
    qryListdoc705_Name: TStringField;
    qryListdoc740_Name: TStringField;
    qryListdoc760_Name: TStringField;
    qryListCHARGE_Name: TStringField;
    qryListCONFIRM_Name: TStringField;
    qryListCARRIAGE_NAME: TStringField;
    sp_attachAPP700fromAPP700: TADOStoredProc;
    sSpeedButton7: TsSpeedButton;
    sLabel56: TsLabel;
    sLabel57: TsLabel;
    sLabel58: TsLabel;
    popMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N7: TMenuItem;
    d1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    qryReady: TADOQuery;
    sPanel36: TsPanel;
    edt_Carriage: TsEdit;
    sBitBtn27: TsBitBtn;
    edt_Carriage1: TsEdit;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSpeedButton8: TsSpeedButton;
    sSpeedButton9: TsSpeedButton;
    sSpeedButton10: TsSpeedButton;
    sLabel17: TsLabel;
    sLabel18: TsLabel;
    sLabel19: TsLabel;
    sLabel20: TsLabel;
    sLabel26: TsLabel;
    sLabel21: TsLabel;
    sLabel22: TsLabel;
    sLabel25: TsLabel;
    sLabel23: TsLabel;
    sLabel29: TsLabel;
    sLabel34: TsLabel;

    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNewClick(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btn_APP700CodeTypeClick(Sender: TObject);
    procedure edt_APP700CodeTypeDblClick(Sender: TObject);
    procedure check_Doc380Click(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sBitBtn22Click(Sender: TObject);                                       
    procedure edt_In_MathodChange(Sender: TObject);
    procedure edt_Doc705GubunChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnTempClick(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
    procedure sMaskEdit1DblClick(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn33Click(Sender: TObject);
    procedure memo_doc2AA1DblClick(Sender: TObject);
    procedure checkPage6Click(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure memo_RowColPrint(Sender: TObject);
    procedure popMenu1Popup(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure qryListCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure memoLineLimit(Sender: TObject; var Key: Char);
    procedure Mask_fromDateKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }

    FTempDocNo: string; //관리번호 생성시 사용
    APP700_SQL : String;
    
    procedure NewDocument; override;
    procedure CancelDocument; override;
    procedure SaveDocument(Sender : TObject); override;
    procedure ReadDocument; override;
    procedure DeleteDocument; override;
    procedure EditDocument; override;

    procedure page5CheckEvent(Owner : TsPanel; Sender : TsCheckBox);
    procedure Readlist(fromDate, toDate : String;KeyValue : String='');overload;
    procedure Readlist(OrderSyntax : string = '');overload;

    procedure ReadyDocument(DocNo : String);

    //데이터 검증
    function CHECK_VALIDITY:String;
    function ReadListBetween(fromDate: String; toDate: String; KeyValue : string): Boolean; 
    //function ValidData: Boolean; override;

   //마지막 콤포넌트에서 탭이나 엔터키 누르면 다음페이지로 이동하는 프로시져
   procedure DialogKey(var msg : TCMDialogKey); message CM_DIALOGKEY;

  public
    { Public declarations }

  end;

var
  UI_APP700_frm: TUI_APP700_frm;

implementation

uses dlg_CreateDocumentChoice_APP700, MSSQL , TypeDefine , Commonlib , Dialog_CodeList , Dialog_BANK , Dialog_SearchCustom,
      SQLCreator , MessageDefine , DateUtils , CodeContents , AutoNo, Dlg_Customer, Config_MSSQL, StrUtils, VarDefine,
      KISCalendar, Dlg_ErrorMessage, Dialog_MemoList, Dialog_CopyAPP700, APP700_PRINT,
      DocumentSend, Dlg_RecvSelect, CreateDocuments, LivartData ;

{$R *.dfm}


procedure TUI_APP700_frm.FormShow(Sender: TObject);
begin
  inherited;

  Self.Caption := '취소불능화환신용장 개설 신청서';

  ProgramControlType := ctView;

  sPageControl1.ActivePageIndex := 0;

  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel51);
  EnabledControlValue(sPanel21);
  EnabledControlValue(sPanel14);
  EnabledControlValue(sPanel31);
  EnabledControlValue(sPanel43);
  EnabledControlValue(sPanel45);


  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);

  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Readlist(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'');
       
end;

procedure TUI_APP700_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  if DMMssql.KISConnect.InTransaction then
    DMMssql.KISConnect.RollbackTrans;

  Close;

end;

procedure TUI_APP700_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_APP700_frm := nil;
end;

procedure TUI_APP700_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  //트랜잭션 시작
  if not DMMssql.KISConnect.InTransaction then
    DMMssql.KISConnect.BeginTrans
  else
  begin
    DMMssql.KISConnect.RollbackTrans;
    DMMssql.KISConnect.BeginTrans;
  end;

//  dlg_CreateDocumentChoice_APP700_frm := Tdlg_CreateDocumentChoice_APP700_frm.Create(Self);
//
//  try
//    case dlg_CreateDocumentChoice_APP700_frm.ShowModal of
//      //신규등록
//      mrRetry : NewDocument;
//      //수입승인 신청서 목록에서 선택
//      mrOk : ;
//      //외화획득용 수입승인 신청서 목록에서 선택
//      mrYes : ;
//    end;
//  finally
//    FreeAndNil( dlg_CreateDocumentChoice_APP700_frm );
//  end;

    //신규입력시 다른 업무에 문서를 복사해올 필요가 없을거같다는 의견으로 인해 수정 
    NewDocument;
end;

procedure TUI_APP700_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  if (Sender as TsPageControl).ActivePageIndex = 6 then
  begin
    //관리번호가있는 패널  숨김
    //sPanel4.Visible := not (sPageControl1.ActivePageIndex = 6);
    //DBGrid3이 위치해 있는 패널 숨김
    sPanel52.Visible := not (sPageControl1.ActivePageIndex = 6);
  end
  else
  begin
    //관리번호가있는 패널
    //sPanel4.Visible := True;
    //DBGrid3이 위치해 있는 패널
    sPanel52.Visible := True;
  end;

  //데이터 신규,수정 작성시 데이터조회 페이지 검색부분 비활성화
  if ProgramControlType in [ctInsert , ctModify] then
    EnabledControlValue(sPanel3)
  else
    EnabledControlValue(sPanel3,False);
end;


procedure TUI_APP700_frm.NewDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctInsert;
  
//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Append;

//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex := 0;
  sPageControl1Change(sPageControl1);
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  ClearControlValue(sPanel51);  //page1
  ClearControlValue(sPanel21);  //page2
  ClearControlValue(sPanel14);  //page3
  ClearControlValue(sPanel31);  //page4
  ClearControlValue(sPanel43);  //page5
  ClearControlValue(sPanel45);  //page6

  EnabledControlValue(sPanel4 , False);
  EnabledControlValue(sPanel51 , False);
  EnabledControlValue(sPanel21 , False);
  EnabledControlValue(sPanel14 , False);
  EnabledControlValue(sPanel31 , False);
  EnabledControlValue(sPanel43 , False);
  EnabledControlValue(sPanel45 , False);
  EnabledControlValue(sPanel53);

  memo_doc2AA1.ReadOnly := True;
  memo_acd2AE1.ReadOnly := True;

  //PAGE5 코드버튼
  sBitBtn29.Enabled := check_Doc705.Checked;
  sBitBtn30.Enabled := check_Doc705.Checked;
  sBitBtn31.Enabled := check_Doc760.Checked;
  sBitBtn32.Enabled := check_Doc740.Checked;
  sBitBtn33.Enabled := check_doc2AA.Checked;
  //PAGE6 메모버튼
  sBitBtn34.Enabled := check_acd2AE.Checked;

  //edt_MaintNo.SetFocus;

//------------------------------------------------------------------------------
//기초자료
//------------------------------------------------------------------------------
  edt_Datee.Text := FormatDateTime('YYYYMMDD',now);

  //사용자
  edt_UserNo.Text := LoginData.sID;

  //등록일자
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);

//------------------------------------------------------------------------------
//관리번호 생성
//------------------------------------------------------------------------------
  edt_MaintNo.ReadOnly := False;
  edt_MaintNo.Color := clWhite;

  edt_MaintNo.Text := DMAutoNo.GetDocumentNoAutoInc('APP700');

//------------------------------------------------------------------------------
//문서기능 / 유형
//------------------------------------------------------------------------------
  edt_msg1.ReadOnly := False;
  edt_msg1.Color := clWhite;
  edt_msg1.Text := '9';
  sBitBtn2.Enabled := True;

  edt_msg2.ReadOnly := False;
  edt_msg2.Color := clWhite;
  edt_msg2.Text := 'AB';
  sBitBtn3.Enabled := True;


//------------------------------------------------------------------------------
//명의인 정보
//------------------------------------------------------------------------------
  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try

    with Dlg_Customer_frm do
    begin
      if isValue('00000') then
      begin
        edt_EXName1.Text := DataSource1.DataSet.FieldByName('ENAME').AsString;
        edt_EXName2.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
        edt_EXName3.Text := DataSource1.DataSet.FieldByName('JENJA').AsString;
        edt_EXAddr1.Text := DataSource1.DataSet.FieldByName('ADDR1').AsString;
        edt_EXAddr2.Text := DataSource1.DataSet.FieldByName('ADDR2').AsString;
      end;
    end;
  finally
    FreeAndNil(Dlg_Customer_frm);
  end;

//------------------------------------------------------------------------------
//Applicant
//------------------------------------------------------------------------------
  Config_MSSQL_frm := TConfig_MSSQL_frm.Create(Self);
  try
    with Config_MSSQL_frm do
    begin
        qryConfig.Active := True;
        edt_Applic1.Text := qryConfigENAME1.AsString;
        edt_Applic2.Text := qryConfigEADDR1.AsString;
        edt_Applic3.Text := qryConfigEADDR2.AsString;
        edt_Applic4.Text := qryConfigEADDR3.AsString;
        edt_Applic5.Text := qryConfigENAME3.AsString;
    end;
  finally
    Config_MSSQL_frm.qryConfig.Active := False;
    FreeAndNil(Config_MSSQL_frm);
  end;

end;


procedure TUI_APP700_frm.CancelDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(True);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex:= 0;
  sPageControl1Change(sPageControl1);
  sDBGrid1.Enabled := True;
  sDBGrid2.Enabled := True;

//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel51);
  EnabledControlValue(sPanel21);
  EnabledControlValue(sPanel14);
  EnabledControlValue(sPanel31);
  EnabledControlValue(sPanel43);
  EnabledControlValue(sPanel45);
  EnabledControlValue(sPanel53,False);

//  ClearControlValue(sPanel51);
//  ClearControlValue(sPanel21);
//  ClearControlValue(sPanel14);
//  ClearControlValue(sPanel31);
//  ClearControlValue(sPanel43);
//  ClearControlValue(sPanel45);

  //등록일자 초기화
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);

  //문서기능,유형 버튼
  sBitBtn2.Enabled := False;
  sBitBtn3.Enabled := False;

  //체크해제
  check_Doc380.Checked := False;
  check_Doc705.Checked := False;
  check_Doc740.Checked := False;
  check_Doc760.Checked := False;
  check_Doc530.Checked := False;
  check_Doc271.Checked := False;
  check_Doc861.Checked := False;
  check_doc2AA.Checked := False;

  check_acd2AA.Checked := False;
  check_acd2AB.Checked := False;
  check_acd2AC.Checked := False;
  check_acd2AD.Checked := False;
  check_acd2AE.Checked := False;

end;

procedure TUI_APP700_frm.btnCancelClick(Sender: TObject);
var
  CreateDate : TDateTime;
begin
  inherited;

  if DMMssql.KISConnect.InTransaction then
     DMMssql.KISConnect.RollbackTrans; // 트랜잭션 취소

  IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_CANCEL),'취소확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
  begin
    CancelDocument;

  //----------------------------------------------------------------------------
  //새로고침
  //----------------------------------------------------------------------------
    CreateDate := ConvertStr2Date(sMaskEdit1.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)));
  end;

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel51);
    ClearControlValue(sPanel21);
    ClearControlValue(sPanel14);
    ClearControlValue(sPanel31);
    ClearControlValue(sPanel43);
    ClearControlValue(sPanel45);
  end;
  
end;



procedure TUI_APP700_frm.btn_APP700CodeTypeClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    //Partial Shipment
    122 : edt_APP700CodeTypeDblClick(edt_Pship);
    //Transhipment
    123 : edt_APP700CodeTypeDblClick(edt_Tship);
    //운송수단
    124 : edt_APP700CodeTypeDblClick(edt_Carriage);
    //Country  of Origin
    125 : edt_APP700CodeTypeDblClick(edt_Origin);
    //Full SET종류
    501 : edt_APP700CodeTypeDblClick(edt_Doc705Gubun);
    //MARKED FREIGHT_1
    502 : edt_APP700CodeTypeDblClick(edt_Doc705_3);
    //MARKED FREIGHT_2
    503 : edt_APP700CodeTypeDblClick(edt_Doc740_3);
    //MARKED FREIGHT_3
    504 : edt_APP700CodeTypeDblClick(edt_Doc760_3);
  end;


end;



procedure TUI_APP700_frm.edt_APP700CodeTypeDblClick(Sender: TObject);
begin
  inherited;
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      if Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
      begin
        //선택된 코드 출력
        (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;

        //선택된 코드의 값까지 출력
        case (Sender as TsEdit).Tag of
           //Partial Shipment 분할선적
           122 :  edt_Pship1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
           //Transhipment 환적
           123 :  edt_Tship1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
           //운송수단
           124 :  edt_Carriage1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
           //Country  of Origin
           125 :  edt_Origin_M.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
           //MARKED FREIGHT_1
           127 : edt_Doc705_3_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
           //MARKED FREIGHT_2
           128 : edt_Doc740_3_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
           //MARKED FREIGHT_3
           129 : edt_Doc760_3_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
           //FULL SET MARKED FREIGHT
           502 : edt_Doc705_3_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
           //AIRWAY VILL CONSIGNED MARKED FREIGHT
           503 : edt_Doc740_3_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
           //FULL SET OF CLEAN MARKED FRIGHT
           504 : edt_Doc760_3_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
        end;
      end;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;

end;


procedure TUI_APP700_frm.page5CheckEvent(Owner : TsPanel; Sender : TsCheckBox);
var
  i : Integer;
begin
    inherited;

    for i := 0 to Owner.ControlCount-1 do
    begin
      IF Owner.Controls[i] is TsEdit Then
      begin
        IF (Owner.Controls[i] as TsEdit).Tag = Sender.Tag Then
        begin
          (Owner.Controls[i] as TsEdit).ReadOnly := not Sender.Checked;
          if Sender.Checked then
            (Owner.Controls[i] as TsEdit).Color := clWhite
          else
          begin

            if (Owner.Controls[i].Name = 'edt_Doc740_3') or (Owner.Controls[i].Name = 'edt_Doc740_3_1') or
               (Owner.Controls[i].Name = 'edt_Doc760_3') or (Owner.Controls[i].Name = 'edt_Doc760_3_1') or (Owner.Controls[i].Name = 'edt_Doc705Gubun') then
            begin
              (Owner.Controls[i] as TsEdit).Color := clBtnFace;
            end
            else
            begin
              (Owner.Controls[i] as TsEdit).Color := clBtnFace;
              (Owner.Controls[i] as TsEdit).Clear;
            end;

            //edt_DOC705Gubun에 값에따라 출력되는 Label 초기화
            sLabel32.Caption := '';
            sLabel35.Caption := '';
          end;
        end;
      end;

      if Owner.Controls[i] is TsBitBtn then
      begin
        if (Owner.Controls[i] as TsBitBtn).Hint = Sender.Hint then
        begin
          if Sender.Checked = True then
            (Owner.Controls[i] as TsBitBtn).Enabled := True
          else
            (Owner.Controls[i] as TsBitBtn).Enabled := False;
        end;
      end;
    end;

    if Sender.Name = 'check_Doc705' then
    begin
      if check_Doc705.Checked = True then
      begin

        sBitBtn29.Enabled := True;
        sBitBtn30.Enabled := True;

        edt_Doc705_3.ReadOnly := False;

        edt_Doc705Gubun.Color := clWhite;
        edt_Doc705_1.Color := clWhite;
        edt_Doc705_2.Color := clWhite;
        edt_Doc705_3.Color := clWhite;
        edt_Doc705_3_1.Color := clWhite;
        edt_Doc705_4.Color := clWhite;
      end
      else
      begin
        sBitBtn29.Enabled := False;
        sBitBtn30.Enabled := False;

        edt_Doc705_3.ReadOnly := True;

        edt_Doc705Gubun.Color := clBtnFace;
        edt_Doc705_1.Color := clBtnFace;
        edt_Doc705_2.Color := clBtnFace;
        edt_Doc705_3.Color := clBtnFace;
        edt_Doc705_3_1.Color := clBtnFace;
        edt_Doc705_4.Color := clBtnFace;

        edt_Doc705_1.Clear;
        edt_Doc705_2.Clear;
        //edt_Doc705_3.Clear;
        //edt_Doc705_3_1.Clear;
        edt_Doc705_4.Clear;
      end;
    end;

    if Sender.Name = 'check_doc2AA' then
    begin
      if check_doc2AA.Checked = True then
      begin
        sBitBtn33.Enabled := True;
        memo_doc2AA1.Color := clWhite;
        memo_doc2AA1.ReadOnly := False;
      end
      else
      begin
        sBitBtn33.Enabled := False;
        memo_doc2AA1.Color := clBtnFace;
        memo_doc2AA1.Clear;
        memo_doc2AA1.ReadOnly := True;
      end;
    end;


    if Sender.Name = 'check_acd2AE' then
    begin
      if check_acd2AE.Checked = True then
      begin
        sBitBtn34.Enabled := True;
        memo_acd2AE1.Color := clWhite;
        memo_acd2AE1.ReadOnly := False;
      end
      else
      begin
        sBitBtn34.Enabled := False;
        memo_acd2AE1.Color := clBtnFace;
        memo_acd2AE1.Clear;
        memo_acd2AE1.ReadOnly := True;
      end;
    end;
end;

procedure TUI_APP700_frm.check_Doc380Click(Sender: TObject);
begin
  inherited;
  page5CheckEvent(sPanel43,(Sender as TsCheckBox));
end;

procedure TUI_APP700_frm.SaveDocument;
var
  SQLCreate : TSQLCreate;
  DocNo,DocCount : String;
  CreateDate : TDateTime;
begin
  inherited;
//------------------------------------------------------------------------------
// 데이터저장
//------------------------------------------------------------------------------
  try
//------------------------------------------------------------------------------
// 유효성 검사
//------------------------------------------------------------------------------
    if (Sender as TsButton).Tag = 1 then
    begin
      Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
      if Dlg_ErrorMessage_frm.Run_ErrorMessage( '취소불능화환신용장 개설 신청서' ,CHECK_VALIDITY ) Then
      begin
        FreeAndNil(Dlg_ErrorMessage_frm);
        Exit;
      end;
    end;
//------------------------------------------------------------------------------
// SQL생성기
//------------------------------------------------------------------------------
    SQLCreate := TSQLCreate.Create;
    try
        with SQLCreate do
        begin
          DocNo := edt_MaintNo.Text;

          Case ProgramControlType of
            ctInsert :
            begin
              DMLType := dmlInsert;
              ADDValue('MAINT_NO',DocNo);
            end;

            ctModify :
            begin
              DMLType := dmlUpdate;
              ADDWhere('MAINT_NO',DocNo);
            end;
          end;


      //----------------------------------------------------------------------
      // 문서헤더 및 APP700_1.DB
      //----------------------------------------------------------------------
          SQLHeader('APP700_1');

          //문서저장구분(0:임시, 1:저장)
          ADDValue('CHK2',IntToStr((Sender as TsButton).Tag));

          //등록일자
          ADDValue('DATEE',sMaskEdit1.Text);

          //유저ID
          ADDValue('USER_ID',edt_UserNo.Text);

          //메세지1
          ADDValue('MESSAGE1',edt_msg1.Text);
          //메세지2
          ADDValue('MESSAGE2',edt_msg2.Text);

          //APP_DATE
          ADDValue('APP_DATE',edt_Datee.Text);
          
        //----------------------------------------------------------------------
        // 개설은행
        //----------------------------------------------------------------------
          //개설방법
          ADDValue('IN_MATHOD',edt_In_Mathod.Text);

          //개설은행
          ADDValue('AP_BANK',edt_ApBank.Text);
          ADDValue('AP_BANK1',edt_ApBank1.Text);
          ADDValue('AP_BANK2',edt_ApBank2.Text);
          ADDValue('AP_BANK3',edt_ApBank3.Text);
          ADDValue('AP_BANK4',edt_ApBank4.Text);
          ADDValue('AP_BANK5',edt_ApBank5.Text);

        //----------------------------------------------------------------------
        //통지은행
        //----------------------------------------------------------------------
          ADDValue('AD_BANK',edt_AdBank.Text);
          ADDValue('AD_BANK1',edt_AdBank1.Text);
          ADDValue('AD_BANK2',edt_AdBank2.Text);
          ADDValue('AD_BANK3',edt_AdBank3.Text);
          ADDValue('AD_BANK4',edt_AdBank4.Text);

          //신용공여
          ADDValue('AD_PAY',edt_AdPay.Text);

        //----------------------------------------------------------------------
        //수입용도
        //----------------------------------------------------------------------
          ADDValue('IMP_CD1',edt_ImpCd1.Text);
          ADDValue('IMP_CD2',edt_ImpCd2.Text);
          ADDValue('IMP_CD3',edt_ImpCd3.Text);
          ADDValue('IMP_CD4',edt_ImpCd4.Text);
          ADDValue('IMP_CD5',edt_ImpCd5.Text);

        //----------------------------------------------------------------------
        //I/L번호
        //----------------------------------------------------------------------
          ADDValue('IL_NO1',edt_ILno1.Text);
          ADDValue('IL_NO2',edt_ILno2.Text);
          ADDValue('IL_NO3',edt_ILno3.Text);
          ADDValue('IL_NO4',edt_ILno4.Text);
          ADDValue('IL_NO5',edt_ILno5.Text);

          ADDValue('IL_CUR1',edt_ILCur1.Text);
          ADDValue('IL_CUR2',edt_ILCur2.Text);
          ADDValue('IL_CUR3',edt_ILCur3.Text);
          ADDValue('IL_CUR4',edt_ILCur4.Text);
          ADDValue('IL_CUR5',edt_ILCur5.Text);

          ADDValue('IL_AMT1',edt_ILAMT1.Text);
          ADDValue('IL_AMT2',edt_ILAMT2.Text);
          ADDValue('IL_AMT3',edt_ILAMT3.Text);
          ADDValue('IL_AMT4',edt_ILAMT4.Text);
          ADDValue('IL_AMT5',edt_ILAMT5.Text);

        //----------------------------------------------------------------------
        //기타정보
        //----------------------------------------------------------------------
          ADDValue('AD_INFO1',edt_AdInfo1.Text);
          ADDValue('AD_INFO2',edt_AdInfo2.Text);
          ADDValue('AD_INFO3',edt_AdInfo3.Text);
          ADDValue('AD_INFO4',edt_AdInfo4.Text);
          ADDValue('AD_INFO5',edt_AdInfo5.Text);

        //----------------------------------------------------------------------
        //Form of Documentary Credit
        //----------------------------------------------------------------------
          ADDValue('DOC_CD',edt_Doccd.Text);

        //----------------------------------------------------------------------
        //Date and Place of Expiry
        //----------------------------------------------------------------------
          ADDValue('EX_DATE',edt_exDate.Text);
          ADDValue('EX_PLACE',edt_exPlace.Text);
        end;

        with TADOQuery.Create(nil) do
        begin
          try
            Connection := DMMssql.KISConnect;
            SQL.Text := SQLCreate.CreateSQL;
            if sCheckBox1.Checked then
              Clipboard.AsText := SQL.Text;
            ExecSQL;
          finally
            Close;
            Free;
          end;
        end;

    //--------------------------------------------------------------------------
    // APP700_2.DB INSERT 시작
    //--------------------------------------------------------------------------

      with SQLCreate do
      begin
        DocNo := edt_MaintNo.Text;

        SQLHeader('APP700_2');

        Case ProgramControlType of
          ctInsert :
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO',DocNo);
          end;

          ctModify :
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO',DocNo);
          end;
        end;


        //----------------------------------------------------------------------
        //Applicant
        //----------------------------------------------------------------------
          ADDValue('APPLIC1',edt_Applic1.Text);
          ADDValue('APPLIC2',edt_Applic2.Text);
          ADDValue('APPLIC3',edt_Applic3.Text);
          ADDValue('APPLIC4',edt_Applic4.Text);
          ADDValue('APPLIC5',edt_Applic5.Text);

        //----------------------------------------------------------------------
        //Beneficiary
        //----------------------------------------------------------------------
          ADDValue('BENEFC',edt_Benefc.Text);
          ADDValue('BENEFC1',edt_Benefc1.Text);
          ADDValue('BENEFC2',edt_Benefc2.Text);
          ADDValue('BENEFC3',edt_Benefc3.Text);
          ADDValue('BENEFC4',edt_Benefc4.Text);
          ADDValue('BENEFC5',edt_Benefc5.Text);

        //----------------------------------------------------------------------
        //Currency Code,Amount
        //----------------------------------------------------------------------
          ADDValue('CD_CUR',edt_Cdcur.Text);
          ADDValue('CD_AMT',edt_Cdamt.Text);

        //----------------------------------------------------------------------
        //Percentage Credit Amount Tolerance
        //----------------------------------------------------------------------
         ADDValue('CD_PERP',edt_cdPerP.Text);
         ADDValue('CD_PERM',edt_cdPerM.Text);

        //----------------------------------------------------------------------
        //Maximum Credit Amount
        //----------------------------------------------------------------------
          ADDValue('CD_MAX',edt_CdMax.Text);

        //----------------------------------------------------------------------
        //Additional Amount Covered
        //----------------------------------------------------------------------
          ADDValue('AA_CV1',edt_aaCcv1.Text);
          ADDValue('AA_CV2',edt_aaCcv2.Text);
          ADDValue('AA_CV3',edt_aaCcv3.Text);
          ADDValue('AA_CV4',edt_aaCcv4.Text);

        //----------------------------------------------------------------------
        //Drafts at ... (화환어음조건)
        //----------------------------------------------------------------------
          ADDValue('DRAFT1',edt_Draft1.Text);
          ADDValue('DRAFT2',edt_Draft2.Text);
          ADDValue('DRAFT3',edt_Draft3.Text);          

        //----------------------------------------------------------------------
        //Mixed Payment Detail(혼합지급조건)
        //----------------------------------------------------------------------
          ADDValue('MIX_PAY1',edt_mixPay1.Text);
          ADDValue('MIX_PAY2',edt_MixPay2.Text);
          ADDValue('MIX_PAY3',edt_MixPay3.Text);
          ADDValue('MIX_PAY4',edt_MixPay4.Text);

        //----------------------------------------------------------------------
        //Deferred Payment Details(연지급조건명세)
        //----------------------------------------------------------------------
          ADDValue('DEF_PAY1',edt_defPay1.Text);
          ADDValue('DEF_PAY2',edt_defPay2.Text);
          ADDValue('DEF_PAY3',edt_defPay3.Text);
          ADDValue('DEF_PAY4',edt_defPay4.Text);

        //----------------------------------------------------------------------
        //Description of Goods and/or Services
        //----------------------------------------------------------------------
          ADDValue('DESGOOD_1',memo_Desgood1.Lines.Text);

        //----------------------------------------------------------------------
        //Latest Date of Shipment
        //----------------------------------------------------------------------
          ADDValue('LST_DATE',edt_lstDate.Text);

        //----------------------------------------------------------------------
        //Shipment Period
        //----------------------------------------------------------------------
          ADDValue('SHIP_PD1',edt_shipPD1.Text);
          ADDValue('SHIP_PD2',edt_shipPD2.Text);
          ADDValue('SHIP_PD3',edt_shipPD3.Text);
          ADDValue('SHIP_PD4',edt_shipPD4.Text);
          ADDValue('SHIP_PD5',edt_shipPD5.Text);
          ADDValue('SHIP_PD6',edt_shipPD6.Text);

        //----------------------------------------------------------------------
        //Partial Shipment
        //----------------------------------------------------------------------
          ADDValue('PSHIP',edt_Pship.Text);

        //----------------------------------------------------------------------
        //Transhipment
        //----------------------------------------------------------------------
          ADDValue('TSHIP',edt_Tship.Text);

        //----------------------------------------------------------------------
        //Place of Taking in Charge/Dispatch from ···/Place of Receipt
        //----------------------------------------------------------------------
          ADDValue('LOAD_ON',edt_loadOn.Text);

        //----------------------------------------------------------------------
        //Place of Final Destination/For Transportation to ···/Place of Delivery
        //----------------------------------------------------------------------
          ADDValue('FOR_TRAN',edt_forTran.Text);

      end;

      with TADOQuery.Create(nil) do
       begin
         try
           Connection := DMMssql.KISConnect;
           SQL.Text := SQLCreate.CreateSQL;
           if sCheckBox1.Checked then
             Clipboard.AsText := SQL.Text;
           ExecSQL;
         finally
           Close;
           Free;
         end;
       end;


    //--------------------------------------------------------------------------
    // APP700_3.DB INSERT 시작
    //--------------------------------------------------------------------------

      with SQLCreate do
      begin
        DocNo := edt_MaintNo.Text;

        SQLHeader('APP700_3');

        Case ProgramControlType of
          ctInsert :
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO',DocNo);
          end;

          ctModify :
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO',DocNo);
          end;
        end;

        //----------------------------------------------------------------------
        //명의인정보
        //----------------------------------------------------------------------
        //명의인
          ADDValue('EX_NAME1',edt_EXName1.Text);
          ADDValue('EX_NAME2',edt_EXName2.Text);
        //식별부호
          ADDValue('EX_NAME3',edt_EXName3.Text);
        //주소
          ADDValue('EX_ADDR1',edt_EXAddr1.Text);
          ADDValue('EX_ADDR2',edt_EXAddr2.Text);

        //----------------------------------------------------------------------
        //Terms of Price
        //----------------------------------------------------------------------
          ADDValue('TERM_PR',edt_TermPR.Text);
          ADDValue('TERM_PR_M',edt_TermPR_M.Text);

        //----------------------------------------------------------------------
        //Place of Terms of Price
        //----------------------------------------------------------------------
          ADDValue('PL_TERM',edt_plTerm.Text);

        //----------------------------------------------------------------------
        //운송수단
        //----------------------------------------------------------------------
          ADDValue('CARRIAGE',edt_Carriage.Text);

        //----------------------------------------------------------------------
        //선적항 Port of Loading/Airport of Departure
        //----------------------------------------------------------------------
          ADDValue('SUNJUCK_PORT',edt_SunjukPort.Text);
          
        //----------------------------------------------------------------------
        //도착항 Port of Discharge/Airport of Destination
        //----------------------------------------------------------------------
          ADDValue('DOCHACK_PORT',edt_dochackPort.Text);

        //----------------------------------------------------------------------
        //Country of Origin
        //----------------------------------------------------------------------
          ADDValue('ORIGIN',edt_Origin.Text);
          ADDValue('ORIGIN_M',edt_Origin_M.Text);

        //----------------------------------------------------------------------
        //Documents Required ...
        //----------------------------------------------------------------------
          //ASSIGNED COMMERCIAL
          if check_Doc380.Checked = True then
          begin

            ADDValue('DOC_380','1');
            //COPIES
            ADDValue('DOC_380_1',edt_Doc380_1.Text);

          end
          else ADDValue('DOC_380','0');

          //--------------------------------------------------------------------
          //FULL SET OF CLEAN ON BOARD OCEAN
          //--------------------------------------------------------------------
          if check_Doc705.Checked = True then
          begin

            ADDValue('DOC_705', '1');
            ADDValue('DOC_705_GUBUN',edt_Doc705Gubun.Text);
            ADDValue('DOC_705_1',edt_Doc705_1.Text);
            ADDValue('DOC_705_2',edt_Doc705_2.Text);
            ADDValue('DOC_705_3',edt_Doc705_3.Text);
            ADDValue('DOC_705_4',edt_Doc705_4.Text);

          end
          else ADDValue('DOC_705', '0');

          //--------------------------------------------------------------------
          //AIRWAY VILL CONSIGNED TO
          //--------------------------------------------------------------------
          if check_Doc740.Checked = True then
          begin

            ADDValue('DOC_740','1');
            ADDValue('DOC_740_1',edt_Doc740_1.Text);
            ADDValue('DOC_740_2',edt_Doc740_2.Text);
            ADDValue('DOC_740_3',edt_Doc740_3.Text);
            ADDValue('DOC_740_4',edt_Doc740_4.Text);

          end
          else ADDValue('DOC_740','0');

          //--------------------------------------------------------------------
          //FULL SET OF CLEAN MULTIMODAL TRANSPORT DOCUMENT MADE OUT TO
          //--------------------------------------------------------------------
          if check_Doc760.Checked = True then
          begin

            ADDValue('DOC_760','1');
            ADDValue('DOC_760_1',edt_Doc760_1.Text);
            ADDValue('DOC_760_2',edt_Doc760_2.Text);
            ADDValue('DOC_760_3',edt_Doc760_3.Text);
            ADDValue('DOC_760_4',edt_Doc760_4.Text);

          end
          else ADDValue('DOC_760','0');

          //--------------------------------------------------------------------
          //FULL SET OF INSURANCE POLICIES
          //--------------------------------------------------------------------
          if check_Doc530.Checked = True then
          begin

            ADDValue('DOC_530','1');
            ADDValue('DOC_530_1',edt_Doc530_1.Text);
            ADDValue('DOC_530_2',edt_Doc530_2.Text);

          end
          else ADDValue('DOC_530','0');

          //--------------------------------------------------------------------
          //PACKING LIST IN
          //--------------------------------------------------------------------
          if check_Doc271.Checked = True then
          begin

            ADDValue('DOC_271','1');
            ADDValue('DOC_271_1',edt_Doc271_1.Text);

          end
          else ADDValue('DOC_271','0');

          //--------------------------------------------------------------------
          //CRETIFICATE OF ORIGIN
          //--------------------------------------------------------------------
          if check_Doc861.Checked = True then
            ADDValue('DOC_861','1')
          else
            ADDValue('DOC_861','0');

          //--------------------------------------------------------------------
          //OTHER DOCUMENT(S) ( if any )
          //--------------------------------------------------------------------
          if check_doc2AA.Checked = True then
          begin
            ADDValue('DOC_2AA','1');
            ADDValue('DOC_2AA_1',memo_doc2AA1.Lines.Text);

          end
          else  ADDValue('DOC_2AA','0');

        //----------------------------------------------------------------------
        //(C)Additional conditions..
        //----------------------------------------------------------------------
          //SHIPMENT BY
          if check_acd2AA.Checked = True then
          begin
            ADDValue('ACD_2AA','1');
            ADDValue('ACD_2AA_1',edt_acd2AA1.Text);
          end
          else ADDValue('ACD_2AA','0');

          //--------------------------------------------------------------------
          //ACCEPTANCE COMMISSION  DISCOUN
          //--------------------------------------------------------------------
          if check_acd2AB.Checked = True then
            ADDValue('ACD_2AB','1')
          else  ADDValue('ACD_2AB','0');

          //--------------------------------------------------------------------
          //ALL DOCUMENTS MUST BEAR OUR CR
          //--------------------------------------------------------------------
          if check_acd2AC.Checked = True then
            ADDValue('ACD_2AC','1')
          else  ADDValue('ACD_2AC','0');

          //--------------------------------------------------------------------
          //LATE PRESENTATION B/L ACCEPTAB
          //--------------------------------------------------------------------
          if check_acd2AD.Checked = True then
            ADDValue('ACD_2AD','1')
          else  ADDValue('ACD_2AD','0');

          //--------------------------------------------------------------------
          //OTHER CONDITION(S)
          //--------------------------------------------------------------------
          if check_acd2AE.Checked = True then
          begin

            ADDValue('ACD_2AE','1');
            ADDValue('ACD_2AE_1',memo_acd2AE1.Lines.Text);

          end
          else ADDValue('ACD_2AE','0');

          //--------------------------------------------------------------------
          //CHARGE
          //--------------------------------------------------------------------
            ADDValue('CHARGE',edt_Charge.Text);

          //--------------------------------------------------------------------
          //Period for Presentation
          //--------------------------------------------------------------------
          if edt_period.Text = '' then
            ADDValue('PERIOD','0')
          else
            ADDValue('PERIOD',edt_period.Text);

          //--------------------------------------------------------------------
          //Confirmation Instructions
          //--------------------------------------------------------------------
            ADDValue('CONFIRMM',edt_Confirm.Text);
      end;

      with TADOQuery.Create(nil) do
       begin
         try
           Connection := DMMssql.KISConnect;
           SQL.Text := SQLCreate.CreateSQL;
           if sCheckBox1.Checked then
             Clipboard.AsText := SQL.Text;
           ExecSQL;
         finally
           Close;
           Free;
         end;
       end;



    finally
      SQLCreate.Free;
    end;

  //----------------------------------------------------------------------------
  // 프로그램제어
  //----------------------------------------------------------------------------
    ProgramControlType := ctView;
  //----------------------------------------------------------------------------
  // 접근제어
  //----------------------------------------------------------------------------
    sPageControl1.ActivePageIndex:= 0;
    sPageControl1Change(sPageControl1);
    sDBGrid1.Enabled := True;
    sDBGrid2.Enabled := True;

  //----------------------------------------------------------------------------
  // 읽기전용
  //----------------------------------------------------------------------------
    EnabledControlValue(sPanel4);
    EnabledControlValue(sPanel51);
    EnabledControlValue(sPanel21);
    EnabledControlValue(sPanel14);
    EnabledControlValue(sPanel31);
    EnabledControlValue(sPanel43);
    EnabledControlValue(sPanel45);
    EnabledControlValue(sPanel53, False);

  //----------------------------------------------------------------------------
  // 버튼설정
  //----------------------------------------------------------------------------
    ButtonEnable(True);

  //체크해제
    check_Doc380.Checked := False;
    check_Doc705.Checked := False;
    check_Doc740.Checked := False;
    check_Doc760.Checked := False;
    check_Doc530.Checked := False;
    check_Doc271.Checked := False;
    check_Doc861.Checked := False;
    check_doc2AA.Checked := False;

    check_acd2AA.Checked := False;
    check_acd2AB.Checked := False;
    check_acd2AC.Checked := False;
    check_acd2AD.Checked := False;
    check_acd2AE.Checked := False;

    //문서기능 , 유형 버튼
    sBitBtn2.Enabled := True;
    sBitBtn3.Enabled := True;

  //----------------------------------------------------------------------------
  //새로고침
  //----------------------------------------------------------------------------
    CreateDate := ConvertStr2Date(sMaskEdit1.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             DocNo);


    Case (Sender as TsButton).Tag of
      0:MessageBox(Self.Handle,MSG_SYSTEM_SAVETEMP_OK,'문서작성완료',MB_OK+MB_ICONINFORMATION);    
      1:MessageBox(Self.Handle,MSG_SYSTEM_SAVE_OK,'문서작성완료',MB_OK+MB_ICONINFORMATION);
    end;
    
  //트랜잭션 저장
  DMMssql.KISConnect.CommitTrans;
  
  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message),'저장오류',MB_OK+MB_ICONERROR);
    end;

  end;

end;


procedure TUI_APP700_frm.btnSaveClick(Sender: TObject);
begin
  inherited;
  SaveDocument(Sender);

end;

procedure TUI_APP700_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;
begin
  inherited;
  nIndex := StrToIntDef( qryListCHK2.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TUI_APP700_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  if AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text])then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text);
end;

procedure TUI_APP700_frm.ReadDocument;
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

//------------------------------------------------------------------------------
//기본정보
//------------------------------------------------------------------------------
  //관리번호
  edt_MaintNo.Text := qryListMAINT_NO.AsString;
  //등록일자
  sMaskEdit1.Text := qryListDATEE.AsString;
  //유저번호
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;


//------------------------------------------------------------------------------
//개설은행
//------------------------------------------------------------------------------
  //신청일자
  edt_Datee.Text := qryListAPP_DATE.AsString;
  //개설방법
  edt_In_Mathod.Text := qryListIN_MATHOD.AsString;
  edt_In_Mathod1.Text := qryListmathod_Name.AsString;
  //개설의뢰은행
  edt_ApBank.Text := qryListAP_BANK.AsString;
  edt_ApBank1.Text := qryListAP_BANK1.AsString;
  edt_ApBank2.Text := qryListAP_BANK2.AsString;
  edt_ApBank3.Text := qryListAP_BANK3.AsString;
  edt_ApBank4.Text := qryListAP_BANK4.AsString;
  edt_ApBank5.Text := qryListAP_BANK5.AsString;

//------------------------------------------------------------------------------
//통지은행
//------------------------------------------------------------------------------
  //(희망)통지은행
  edt_AdBank.Text := qryListAD_BANK.AsString;
  edt_AdBank1.Text := qryListAD_BANK1.AsString;
  edt_AdBank2.Text := qryListAD_BANK2.AsString;
  edt_AdBank3.Text := qryListAD_BANK3.AsString;
  edt_AdBank4.Text := qryListAD_BANK4.AsString;
  //신용공여
  edt_AdPay.Text := qryListAD_PAY.AsString;
  edt_AdPay1.Text := qryListpay_Name.AsString;

//------------------------------------------------------------------------------
//수입용도
//------------------------------------------------------------------------------
  edt_ImpCd1.Text := qryListIMP_CD1.AsString;
  edt_ImpCd1_1.Text := qryListImp_Name_1.AsString;

  edt_ImpCd2.Text := qryListIMP_CD2.AsString;
  edt_ImpCd2_1.Text := qryListImp_Name_2.AsString;

  edt_ImpCd3.Text := qryListIMP_CD3.AsString;
  edt_ImpCd3_1.Text := qryListImp_Name_3.AsString;

  edt_ImpCd4.Text := qryListIMP_CD4.AsString;
  edt_ImpCd4_1.Text := qryListImp_Name_4.AsString;

  edt_ImpCd5.Text := qryListIMP_CD5.AsString;
  edt_ImpCd5_1.Text := qryListImp_Name_5.AsString;
//------------------------------------------------------------------------------
// I/L번호
//------------------------------------------------------------------------------
  edt_ILno1.Text := qryListIL_NO1.AsString;
  edt_ILno2.Text := qryListIL_NO2.AsString;
  edt_ILno3.Text := qryListIL_NO3.AsString;
  edt_ILno4.Text := qryListIL_NO4.AsString;
  edt_ILno5.Text := qryListIL_NO5.AsString;

  edt_ILCur1.Text := qryListIL_CUR1.AsString;
  edt_ILCur2.Text := qryListIL_CUR2.AsString;
  edt_ILCur3.Text := qryListIL_CUR3.AsString;
  edt_ILCur4.Text := qryListIL_CUR4.AsString;
  edt_ILCur5.Text := qryListIL_CUR5.AsString;

  edt_ILAMT1.Value := qryListIL_AMT1.AsCurrency;
  edt_ILAMT2.Value := qryListIL_AMT2.AsCurrency;
  edt_ILAMT3.Value := qryListIL_AMT3.AsCurrency;
  edt_ILAMT4.Value := qryListIL_AMT4.AsCurrency;
  edt_ILAMT5.Value := qryListIL_AMT5.AsCurrency;

//------------------------------------------------------------------------------
// 기타정보
//------------------------------------------------------------------------------
  edt_AdInfo1.Text := qryListAD_INFO1.AsString;
  edt_AdInfo2.Text := qryListAD_INFO2.AsString;
  edt_AdInfo3.Text := qryListAD_INFO3.AsString;
  edt_AdInfo4.Text := qryListAD_INFO4.AsString;
  edt_AdInfo5.Text := qryListAD_INFO5.AsString;

//------------------------------------------------------------------------------
// 명의인정보
//------------------------------------------------------------------------------
  //명의인
  edt_EXName1.Text := qryListEX_NAME1.AsString;
  edt_EXName2.Text := qryListEX_NAME2.AsString;
  //식별부호
  edt_EXName3.Text := qryListEX_NAME3.AsString;
  //주소
  edt_EXAddr1.Text := qryListEX_ADDR1.AsString;
  edt_EXAddr2.Text := qryListEX_ADDR2.AsString;

//------------------------------------------------------------------------------
//Form of Documentary Credit
//------------------------------------------------------------------------------
  edt_Doccd.Text := qryListDOC_CD.AsString;
  edt_Doccd1.Text := qryListDOC_Name.AsString;

//------------------------------------------------------------------------------
//Date and Place of EXpiry
//------------------------------------------------------------------------------
  edt_exDate.Text := qryListEX_DATE.AsString;
  edt_exPlace.Text := qryListEX_PLACE.AsString;

//------------------------------------------------------------------------------
// Applicant
//------------------------------------------------------------------------------
  edt_Applic1.Text := qryListAPPLIC1.AsString;
  edt_Applic2.Text := qryListAPPLIC2.AsString;
  edt_Applic3.Text := qryListAPPLIC3.AsString;
  edt_Applic4.Text := qryListAPPLIC4.AsString;
  //TEL
  edt_Applic5.Text := qryListAPPLIC5.AsString;

//------------------------------------------------------------------------------
// Beneficiary
//------------------------------------------------------------------------------
  edt_Benefc.Text := qryListBENEFC.AsString;
  edt_Benefc1.Text := qryListBENEFC1.AsString;
  edt_Benefc2.Text := qryListBENEFC2.AsString;
  edt_Benefc3.Text := qryListBENEFC3.AsString;
  edt_Benefc4.Text := qryListBENEFC4.AsString;
  //계좌번호
  edt_Benefc5.Text := qryListBENEFC5.AsString;

//------------------------------------------------------------------------------
// Currency Code,Amount
//------------------------------------------------------------------------------
  edt_Cdcur.Text := qryListCD_CUR.AsString;
  edt_Cdamt.Value :=qryListCD_AMT.AsCurrency;

//------------------------------------------------------------------------------
// Maximum Credit Amount
//------------------------------------------------------------------------------
  edt_CdMax.Text := qryListCD_MAX.AsString;
  edt_CdMax1.Text := qryListCDMAX_Name.AsString;

//------------------------------------------------------------------------------
// Percentage Credit Amount Tolerance
//------------------------------------------------------------------------------
  edt_cdPerP.Value := qryListCD_PERP.AsCurrency;
  edt_cdPerM.Value := qryListCD_PERM.AsCurrency;

//------------------------------------------------------------------------------
// Terms of Price
//-----------------------------------------------------------------------------
  edt_TermPR.Text := qryListTERM_PR.AsString;
  edt_TermPR_M.Text := qryListTERM_PR_M.AsString;

//------------------------------------------------------------------------------
// Place of Term of Price
//------------------------------------------------------------------------------
  edt_plTerm.Text := qryListPL_TERM.AsString;

//------------------------------------------------------------------------------
// Additional Amount Covered
//------------------------------------------------------------------------------
  edt_aaCcv1.Text := qryListAA_CV1.AsString;
  edt_aaCcv2.Text := qryListAA_CV2.AsString;
  edt_aaCcv3.Text := qryListAA_CV3.AsString;
  edt_aaCcv4.Text := qryListAA_CV4.AsString;

//------------------------------------------------------------------------------
// Drafts at...(화환어음조건)
//------------------------------------------------------------------------------
  edt_Draft1.Text := qryListDRAFT1.AsString;
  edt_Draft2.Text := qryListDRAFT2.AsString;
  edt_Draft3.Text := qryListDRAFT3.AsString;

//------------------------------------------------------------------------------
// Mixed Payment Detail(혼합지급조건)
//------------------------------------------------------------------------------
  edt_mixPay1.Text := qryListMIX_PAY1.AsString;
  edt_mixPay2.Text := qryListMIX_PAY2.AsString;
  edt_mixPay3.Text := qryListMIX_PAY3.AsString;
  edt_mixPay4.Text := qryListMIX_PAY4.AsString;

//------------------------------------------------------------------------------
// Deferred Payment Details(연지급조건명세)
//------------------------------------------------------------------------------
  edt_defPay1.Text := qryListDEF_PAY1.AsString;
  edt_defPay2.Text := qryListDEF_PAY2.AsString;
  edt_defPay3.Text := qryListDEF_PAY3.AsString;
  edt_defPay4.Text := qryListDEF_PAY4.AsString;

//------------------------------------------------------------------------------
//Description of Goods and/or Services
//------------------------------------------------------------------------------
  memo_Desgood1.Lines.Text := qryListDESGOOD_1.AsString;

//------------------------------------------------------------------------------
// Latest Date of Shipment
//------------------------------------------------------------------------------
  edt_lstDate.Text := qryListLST_DATE.AsString;

//------------------------------------------------------------------------------
//Shipment Period
//------------------------------------------------------------------------------
  edt_shipPD1.Text := qryListSHIP_PD1.AsString;
  edt_shipPD2.Text := qryListSHIP_PD2.AsString;
  edt_shipPD3.Text := qryListSHIP_PD3.AsString;
  edt_shipPD4.Text := qryListSHIP_PD4.AsString;
  edt_shipPD5.Text := qryListSHIP_PD5.AsString;
  edt_shipPD6.Text := qryListSHIP_PD6.AsString;

//------------------------------------------------------------------------------
// Partial Shipment
//------------------------------------------------------------------------------
  edt_Pship.Text := qryListPSHIP.AsString;
  edt_Pship1.Text := qryListpship_Name.AsString;

//------------------------------------------------------------------------------
// Transhipment
//------------------------------------------------------------------------------
  edt_Tship.Text := qryListTSHIP.AsString;
  edt_Tship1.Text := qryListtship_Name.AsString;

//------------------------------------------------------------------------------
// 운송수단
//------------------------------------------------------------------------------
  edt_Carriage.Text := qryListCARRIAGE.AsString;
  edt_Carriage1.Text := qryListCARRIAGE_NAME.AsString;

//------------------------------------------------------------------------------
// Port of Loading/Airport of Departure
//------------------------------------------------------------------------------
  edt_SunjukPort.Text := qryListSUNJUCK_PORT.AsString;

//------------------------------------------------------------------------------
// Port of Discharge/Airport of Destination
//------------------------------------------------------------------------------
  edt_dochackPort.Text := qryListDOCHACK_PORT.AsString;

//------------------------------------------------------------------------------
// Place of Taking in Charge/Dispatch from ···/Place of Receipt
//------------------------------------------------------------------------------
  edt_loadOn.Text := qryListLOAD_ON.AsString;

//------------------------------------------------------------------------------
// Place of Final Destination/For Transportation to ···/Place of Delivery
//------------------------------------------------------------------------------
  edt_forTran.Text := qryListFOR_TRAN.AsString;

//------------------------------------------------------------------------------
// Country of Origin
//------------------------------------------------------------------------------
  edt_Origin.Text := qryListORIGIN.AsString;
  edt_Origin_M.Text := qryListORIGIN_M.AsString;

//------------------------------------------------------------------------------
//Documents Required ...
//------------------------------------------------------------------------------
  //ASSIGNED COMMERCIAL
  check_Doc380.Checked := qryListDOC_380.AsBoolean;
  //COPIES
  edt_Doc380_1.Text := qryListDOC_380_1.Text;
  //Full Set Check
  check_Doc705.Checked := qryListDOC_705.AsBoolean;
  //Full Set
  edt_Doc705Gubun.Text := qryListDOC_705_GUBUN.AsString;

    if edt_Doc705Gubun.Text = '705' then
    begin
      sLabel32.Caption := 'FULL SET';
      sLabel35.Caption := 'THE ORDER OF';
    end
    else if edt_Doc705Gubun.Text = '706' then
    begin
      sLabel32.Caption := 'FULL SET';
      sLabel35.Caption := ''
    end
    else if edt_Doc705Gubun.Text = '707' then
    begin
      sLabel32.Caption := '  COPY';
      sLabel35.Caption := ''
    end
    else if edt_Doc705Gubun.Text = '717' then
    begin
      sLabel32.Caption := ' 1/3 SET';
      sLabel35.Caption := ''
    end
    else if edt_Doc705Gubun.Text = '718' then
    begin
      sLabel32.Caption := ' 2/3 SET';
      sLabel35.Caption := ''
    end
    else
    begin
      sLabel32.Caption := '';
      sLabel35.Caption := '';
    end;

  edt_Doc705_1.Text := qryListDOC_705_1.AsString;
  edt_Doc705_2.Text := qryListDOC_705_2.AsString;
  edt_Doc705_3.Text := qryListDOC_705_3.AsString;
  edt_Doc705_3_1.Text := qryListdoc705_Name.AsString;
  edt_Doc705_4.Text := qryListDOC_705_4.AsString;

  //AIRWAY VILL CONSIGNED
  check_Doc740.Checked := qryListDOC_740.AsBoolean;
  edt_Doc740_1.Text := qryListDOC_740_1.Text;
  edt_Doc740_2.Text := qryListDOC_740_2.Text;
  edt_Doc740_3.Text := qryListDOC_740_3.Text;
  edt_Doc740_3_1.Text := qryListdoc740_Name.AsString;
  edt_Doc740_4.Text := qryListDOC_740_4.Text;

  //FULL SET OF CLEAN MULTIMODAL TRANSPORT ...
  check_Doc760.Checked := qryListDOC_760.AsBoolean;
  edt_Doc760_1.Text := qryListDOC_760_1.AsString;
  edt_Doc760_2.Text := qryListDOC_760_2.AsString;
  edt_Doc760_3.Text := qryListDOC_760_3.AsString;
  edt_Doc760_3_1.Text := qryListdoc760_Name.AsString;
  edt_Doc760_4.Text := qryListDOC_760_4.AsString;

  //FULL SET OF INSURANCE POLICIES...
  check_Doc530.Checked := qryListDOC_530.AsBoolean;
  edt_Doc530_1.Text := qryListDOC_530_1.AsString;
  edt_Doc530_2.Text := qryListDOC_530_2.AsString;

  //PACKING LIST IN
  check_Doc271.Checked := qryListDOC_271.AsBoolean;
  edt_Doc271_1.Text := qryListDOC_271_1.AsString;

  //CRETIFICATE OF ORIGIN
  check_Doc861.Checked := qryListDOC_861.AsBoolean;

  //OTHER DOCUMENT(S) ( if any )
  check_doc2AA.Checked := qryListDOC_2AA.AsBoolean;
  memo_doc2AA1.Lines.Text := qryListDOC_2AA_1.AsString;

  //SHIPMENT BY
  check_acd2AA.Checked := qryListACD_2AA.AsBoolean;
  edt_acd2AA1.Text := qryListACD_2AA_1.AsString;

  //ACCEPTANCE COMMISSION DISCOUNT CHARGES
  check_acd2AB.Checked := qryListACD_2AB.AsBoolean;

  //ALL DOCUMENTS MUST BEAR OUR CREDIT NUMBER
  check_acd2AC.Checked := qryListACD_2AC.AsBoolean;

  //LATE PRESENTATION B/L ACCEPTABLE
  check_acd2AD.Checked := qryListACD_2AD.AsBoolean;

  //OTHER CONDITION(S)    ( if any )
  check_acd2AE.Checked := qryListACD_2AE.AsBoolean;
  memo_acd2AE1.Lines.Text := qryListACD_2AE_1.AsString;

  //CHARGES
  edt_Charge.Text := qryListCHARGE.AsString;
  edt_Charge1.Text := qryListCHARGE_Name.AsString;

  //Period for PRESentation
  edt_period.Text := qryListPERIOD.AsString;

  //Confirmation Instructions
  edt_Confirm.Text := qryListCONFIRMM.AsString;
  edt_Confirm1.Text := qryListCONFIRM_Name.AsString;


//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel51);
  EnabledControlValue(sPanel21);
  EnabledControlValue(sPanel14);
  EnabledControlValue(sPanel31);
  EnabledControlValue(sPanel43);
  EnabledControlValue(sPanel45);
//------------------------------------------------------------------------------


end;

procedure TUI_APP700_frm.edt_In_MathodChange(Sender: TObject);
var
  CodeRecord : TCodeRecord;
  sGroup,sCode : String;
begin
  // Edit에 notFound가 출력되는것을 방지
  if not (Sender as TsEdit).Focused then Exit;  

  sGroup := (Sender as TsEdit).Hint;
  sCode := (Sender as TsEdit).Text;

  CodeRecord := DMCodeContents.getCodeName(sGroup,sCode);

  case (Sender as TsEdit).Tag of
    //개설방법
    101 : edt_In_Mathod1.Text := CodeRecord.sName;
    //신용공여
    104 : edt_AdPay1.Text := CodeRecord.sName;
    //수입용도
    105 : edt_ImpCd1_1.Text := CodeRecord.sName;
    106 : edt_ImpCd2_1.Text := CodeRecord.sName;
    107 : edt_ImpCd3_1.Text := CodeRecord.sName;
    108 : edt_ImpCd4_1.Text := CodeRecord.sName;
    109 : edt_ImpCd5_1.Text := CodeRecord.sName;
    //Form of Documentary Credit
    115 : edt_Doccd1.Text := CodeRecord.sName;
    //Maximum Credit Amount
    118 : edt_CdMax1.Text := CodeRecord.sName;
    //Terms of Price
    119 : edt_TermPR_M.Text := CodeRecord.sName;
    //CHARGE
    120 : edt_Charge1.Text := CodeRecord.sName;
    //Confirmation Instructions
     121 : edt_Confirm1.Text := CodeRecord.sName;
    //운송수단
    124 : edt_Carriage1.Text := CodeRecord.sName;
    //Partial Shipment
    122 : edt_Pship1.Text := CodeRecord.sName;
    //Transhipment
    123 : edt_Tship1.Text := CodeRecord.sName;
    //Country of Origin
    125 : edt_Origin_M.Text := CodeRecord.sName;
    //Documents Required ... 705
    502 : edt_Doc705_3_1.Text := CodeRecord.sName;
    //Documents Required ... 740
    503 : edt_Doc740_3_1.Text := CodeRecord.sName;
    //Documents Required ... 760
    504 : edt_Doc760_3_1.Text := CodeRecord.sName;

  end;

end;


procedure TUI_APP700_frm.edt_Doc705GubunChange(Sender: TObject);
begin
  inherited;
  //page5 - Doc705Gubun값에 따른 label 변경
  if check_Doc705.Checked = True then
  begin
    if edt_Doc705Gubun.Text = '705' then
    begin
      sLabel32.Caption := 'FULL SET';
      sLabel35.Caption := 'THE ORDER OF';
    end
    else if edt_Doc705Gubun.Text = '706' then
    begin
      sLabel32.Caption := 'FULL SET';
      sLabel35.Caption := ''
    end
    else if edt_Doc705Gubun.Text = '707' then
    begin
      sLabel32.Caption := '  COPY';
      sLabel35.Caption := ''
    end
    else if edt_Doc705Gubun.Text = '717' then
    begin
      sLabel32.Caption := ' 1/3 SET';
      sLabel35.Caption := ''
    end
    else if edt_Doc705Gubun.Text = '718' then
    begin
      sLabel32.Caption := ' 2/3 SET';
      sLabel35.Caption := ''
    end
  end;
end;

procedure TUI_APP700_frm.Readlist(fromDate, toDate, KeyValue: String);
begin
  with qryList do
  begin
    Close;
    SQL.Text := APP700_SQL;
    SQL.Add('WHERE DATEE between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Locate('MAINT_NO',KeyValue,[]);
    end;
    
    com_SearchKeyword.ItemIndex := 0;
    Mask_SearchDate1.Text := fromDate;
    Mask_SearchDate2.Text := toDate;
  end;
end;

procedure TUI_APP700_frm.FormCreate(Sender: TObject);
begin
  inherited;
  APP700_SQL := Self.qryList.SQL.Text;
  sPageControl1.ActivePageIndex := 0;
  ProgramControlType := ctView;
end;

procedure TUI_APP700_frm.FormActivate(Sender: TObject);
var
  KeyValue : String;
begin
  inherited;

  if not qryList.Active then Exit;

  if not DMMssql.KISConnect.InTransaction then
  begin
    KeyValue := qryListMAINT_NO.AsString;
    Readlist(Mask_fromDate.Text, Mask_toDate.Text, '');
    qryList.Locate('MAINT_NO',KeyValue,[]);
  end;
end;

procedure TUI_APP700_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin

    case CHK2Value of
      9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

      6 :
      begin
        if AnsiMatchText( Column.FieldName , ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_APP700_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then ReadDocument;
end;

procedure TUI_APP700_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
end;

//저장시 유효성 검사 (필수입력사항 체크)
//function TUI_APP700_frm.ValidData: Boolean;
//var
//  sComponentName : string;
//begin
//  sComponentName := CheckEmptyValue(sPanel51);
//
//  IF sComponentName <> '' then
//  begin
//    MessageBox(Self.Handle,PChar(sComponentName+'를 입력하세요'),'저장오류',MB_OK+MB_ICONERROR);
//  end;
//    Result := sComponentName <> '';
//end;


procedure TUI_APP700_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MaintNo.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;
        SQL.Text :=  'DELETE FROM APP700_1 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM APP700_2 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM APP700_3 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;
end;

procedure TUI_APP700_frm.btnDelClick(Sender: TObject);
begin
  inherited;

  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel51);  //page1
    ClearControlValue(sPanel21);  //page2
    ClearControlValue(sPanel14);  //page3
    ClearControlValue(sPanel31);  //page4
    ClearControlValue(sPanel43);  //page5
    ClearControlValue(sPanel45);  //page6
    ClearControlValue(sPanel4);
  end;
end;

procedure TUI_APP700_frm.EditDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctModify;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  if not DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.BeginTrans;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex:= 0;
  sPageControl1Change(sPageControl1);
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
//------------------------------------------------------------------------------
// 읽기전용 해제
//------------------------------------------------------------------------------
 EnabledControlValue(sPanel4,False);
  //관리번호 수정불가
  edt_MaintNo.Enabled := False;
  //등록일자 수정가능
  sMaskEdit1.ReadOnly := False;
  btn_Cal.Enabled := True;
  //문서기능 수정가능
  edt_msg1.ReadOnly := False;
  sBitBtn2.Enabled := True;
  //유형 수정가능
  edt_msg2.ReadOnly := False;
  sBitBtn3.Enabled := True;

  EnabledControlValue(sPanel51,False);
  EnabledControlValue(sPanel21,False);
  EnabledControlValue(sPanel14,False);
  EnabledControlValue(sPanel31,False);
  EnabledControlValue(sPanel43,False);
  EnabledControlValue(sPanel45,False);
  EnabledControlValue(sPanel53);

  //관리번호는 수정불가
  edt_MaintNo.ReadOnly := True;

//------------------------------------------------------------------------------
// 버튼설정
//------------------------------------------------------------------------------
  ButtonEnable(false);

end;

procedure TUI_APP700_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  if AnsiMatchText(qryListCHK2.AsString,['','0','1','2','5','6']) then
  begin
    EditDocument;
  end
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_EDIT,'수정불가', MB_OK+MB_ICONINFORMATION);
end;

procedure TUI_APP700_frm.btnTempClick(Sender: TObject);
begin
  inherited;
  SaveDocument(Sender);
end;

procedure TUI_APP700_frm.btn_CalClick(Sender: TObject);
begin
  inherited;


  case (Sender as TsBitBtn).Tag of
    901 : sMaskEdit1DblClick(sMaskEdit1);
    902 : sMaskEdit1DblClick(Mask_SearchDate1);
    903 : sMaskEdit1DblClick(Mask_SearchDate2);
  end;

end;

procedure TUI_APP700_frm.sMaskEdit1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

 (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TUI_APP700_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  if ProgramControlType in [ctInsert,ctModify] then
    edt_Datee.Text := sMaskEdit1.Text;
end;

procedure TUI_APP700_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 6 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  
  with qryList do
  begin
    Close;
    sql.Text := APP700_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        Mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 : SQL.Add(' WHERE A700_1.MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
      2 : SQL.Add(' WHERE BENEFC1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ASC ');
    end;

    Open;
    
  end;
end;

procedure TUI_APP700_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_APP700_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist();
  Mask_fromDate.Text := Mask_SearchDate1.Text;
  Mask_toDate.Text := Mask_SearchDate2.Text;

end;

function TUI_APP700_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  nYear,nMonth,nDay : Word;

begin
  ErrMsg := TStringList.Create;

  Try

    IF Trim(edt_In_Mathod.Text) = '' THEN
      ErrMsg.Add('[Page1] 개설방법을 입력해야 합니다');

    IF Trim(edt_ApBank.Text) = '' THEN
      ErrMsg.Add('[Page1] 개설(의뢰)은행을 입력해야 합니다');

    if (Trim(edt_exDate.Text) <> '') AND (StrToInt(edt_Datee.Text) > StrToInt(edt_exDate.Text))  then
      ErrMsg.Add('[Page1] 신청일자가 Date of Expiry 보다 클 수 없습니다 ');

    if (Trim(edt_lstDate.Text) <> '') AND (StrToInt(edt_Datee.Text) > StrToInt(edt_lstDate.Text))  then
      ErrMsg.Add('[Page1] 신청일자가 Latest Date of Shipment 보다 클 수 없습니다 ');

    if Trim(edt_Doccd.Text) = '' then
      ErrMsg.Add('[Page2] Form of Documentary Credit를 입력해야 합니다');

    if Trim(edt_exDate.Text) = '' then
      ErrMsg.Add('[Page2] Date of Expiry를 입력해야 합니다');

    if Trim(edt_exPlace.Text) = '' then
      ErrMsg.Add('[Page2] Place of Expiry를 입력해야 합니다');

    if (Trim(edt_Benefc1.Text) = '') and (Trim(edt_Benefc2.Text) = '') and (Trim(edt_Benefc3.Text) = '') and (Trim(edt_Benefc4.Text) = '')  then
      ErrMsg.Add('[Page2] Beneficiary를 입력해야 합니다');

    if (Trim(edt_Cdcur.Text) = '') or (Trim(edt_Cdamt.Text) = '') then
      ErrMsg.Add('[Page2] Currency Code,Amount를입력해야 합니다');

    if (Trim(edt_TermPR.Text) = '') or (Trim(edt_TermPR_M.Text) = '') then
      ErrMsg.Add('[Page2] Terms of Price를입력해야 합니다');

    if (Trim(edt_Draft1.Text) = '') and (Trim(edt_Draft2.Text) = '') and (Trim(edt_Draft3.Text) = '') and
       (Trim(edt_mixPay1.Text) = '') and (Trim(edt_mixPay2.Text) = '') and (Trim(edt_mixPay3.Text) = '') and (Trim(edt_mixPay4.Text) = '') and
       (Trim(edt_defPay1.Text) = '') and (Trim(edt_defPay2.Text) = '') and (Trim(edt_defPay3.Text) = '') and (Trim(edt_defPay4.Text) = '')  then
      ErrMsg.Add('[Page3] 대금지불조건을 입력해야 합니다');

    if Trim(edt_lstDate.Text) = '' then
      ErrMsg.Add('[Page4] Latest Date of Shipment를 입력해야 합니다');

    if Trim(edt_Pship.Text) = '' then
      ErrMsg.Add('[Page4] Partial Shipment를 입력해야 합니다');

    if Trim(edt_Tship.Text) = '' then
      ErrMsg.Add('[Page4] Transhipment를 입력해야 합니다');

    if Trim(edt_Carriage.Text) = '' then
      ErrMsg.Add('[Page4] 운송수단을 입력해야 합니다');

    if Trim(edt_Charge.Text) = '' then
      ErrMsg.Add('[Page6] Charges를 입력해야 합니다');

    if (edt_Carriage.Text <> 'DQ') and (Trim(edt_loadOn.Text) <> '') then
      ErrMsg.Add('[Page4] 수탁(발송)지는 운송수단이 DQ 일때만 입력가능합니다.');

    if (edt_Carriage.Text <> 'DQ') and (Trim(edt_forTran.Text) <> '') then
      ErrMsg.Add('[Page4] 최종목적지는 운송수단이 DQ 일때만 입력가능합니다.');

    if (edt_Carriage.Text = 'DQ') and (Trim(edt_SunjukPort.Text) <> '') then
      ErrMsg.Add('[Page4] 운송수단이 DQ(복합운송/ETC)일때는 선적항을 입력할 수 없습니다.');

    if (edt_Carriage.Text = 'DQ') and (Trim(edt_dochackPort.Text) <> '') then
      ErrMsg.Add('[Page4] 운송수단이 DQ(복합운송/ETC)일때는 도착항을 입력할 수 없습니다.');

    IF (check_Doc380.Checked = false) and (check_Doc705.Checked = false) and (check_Doc740.Checked = false) and
       (check_Doc760.Checked = false) and (check_Doc530.Checked = false) and (check_Doc271.Checked = False) and
        (check_Doc861.Checked = false) and (check_doc2AA.Checked = false) then
      ErrMsg.Add('[Page5] Documents Required를 입력해야 합니다');

    if (check_Doc740.Checked = True) and (Trim(edt_Doc740_1.Text) = '') then
      ErrMsg.Add('[Page5] AIRWAY VILL CONSIGNED TO... 의 첫줄을 입력해야 합니다. ');

    if (check_Doc740.Checked = True) and (Trim(edt_Doc740_4.Text) = '') then
      ErrMsg.Add('[Page5] AIRWAY VILL CONSIGNED TO... 의 NOTIFY를 입력해야 합니다. ');

    if (edt_Carriage.Text = 'DT') and (check_Doc760.Checked = True) then
       ErrMsg.Add('[Page5] 운송수단이 DT(해상/항공)일때는 주요구비서류의 복합운송관련부분을 입력할 수 없습니다.');

    if (edt_Carriage.Text = 'DQ') and (check_Doc705.Checked = True) then
       ErrMsg.Add('[Page5] 운송수단이 DQ(복합운송/기타)일때는 주요구비서류의 B/L부분을 입력할 수 없습니다.');

    if (edt_Carriage.Text = 'DQ') and (check_Doc740.Checked = True) then
       ErrMsg.Add('[Page5] 운송수단이 DQ(복합운송/기타)일때는 주요구비서류의 Air Waybill부분을 입력할 수 없습니다.');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;


procedure TUI_APP700_frm.sBitBtn33Click(Sender: TObject);
begin
  inherited;
  memo_doc2AA1DblClick(memo_doc2AA1);

end;

procedure TUI_APP700_frm.memo_doc2AA1DblClick(Sender: TObject);
begin
  inherited;
  Dialog_MemoList_frm := TDialog_MemoList_frm.Create(Self);

  try
    if (Sender as TsMemo).ReadOnly = False then
    begin
      if Dialog_MemoList_frm.openDialog = mrok then
      begin
        if Dialog_MemoList_frm.writeRadioGroup.ItemIndex = 0 then
        begin
            if Dialog_MemoList_frm.codeRadioGroup.ItemIndex = 0 then
              (Sender as TsMemo).Lines.Text := Dialog_MemoList_frm.sDBGrid1.DataSource.DataSet.FieldByName('D_MEMO').AsString
            else if Dialog_MemoList_frm.codeRadioGroup.ItemIndex = 1 then
              (Sender as TsMemo).Lines.Text := Dialog_MemoList_frm.sDBGrid1.DataSource.DataSet.FieldByName('FNAME').AsString;
        end
        else if  Dialog_MemoList_frm.writeRadioGroup.ItemIndex = 1 then
        begin
            if Dialog_MemoList_frm.codeRadioGroup.ItemIndex = 0 then
              (Sender as TsMemo).Lines.Text := (Sender as TsMemo).Lines.Text + #13#10 + Dialog_MemoList_frm.sDBGrid1.DataSource.DataSet.FieldByName('D_MEMO').AsString
            else if Dialog_MemoList_frm.codeRadioGroup.ItemIndex = 1 then
              (Sender as TsMemo).Lines.Text := (Sender as TsMemo).Lines.Text + #13#10 + Dialog_MemoList_frm.sDBGrid1.DataSource.DataSet.FieldByName('FNAME').AsString;
        end;
      end;
    end;
  finally

    FreeAndNil(Dialog_MemoList_frm);

  end;
end;

procedure TUI_APP700_frm.checkPage6Click(Sender: TObject);
begin
  inherited;
  page5CheckEvent(sPanel45,(Sender as TsCheckBox));
end;

procedure TUI_APP700_frm.btnCopyClick(Sender: TObject);
var
  copyDocNo , newDocNo : String;
begin
  inherited;

    Dialog_CopyAPP700_frm := TDialog_CopyAPP700_frm.Create(Self);

      try
          newDocNo := DMAutoNo.GetDocumentNoAutoInc('APP700');
          copyDocNo := Dialog_CopyAPP700_frm.openDialog;

          //ShowMessage(copyDocNo);

          IF Trim(copyDocNo) = '' then Exit;

          //------------------------------------------------------------------------------
          // COPY
          //------------------------------------------------------------------------------
          with sp_attachAPP700fromAPP700 do
          begin
            Close;
            Parameters.ParamByName('@CopyDocNo').Value := copyDocNo;
            Parameters.ParamByName('@NewDocNo').Value :=  newDocNo;
            Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

            if not DMMssql.KISConnect.InTransaction then
              DMMssql.KISConnect.BeginTrans;

            try
              ExecProc;
            except
              on e:Exception do
              begin
                DMMssql.KISConnect.RollbackTrans;
                MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
              end;
            end;

          end;
        // ReadListBetween(해당년도 1월1일 , 오늘날짜 , 새로복사 된  관리번호)
        if  ReadListBetween(FormatDateTime('YYYYMMDD', StartOfTheYear(Now)),FormatDateTime('YYYYMMDD',Now),newDocNo) Then
        begin

          EditDocument;
          sMaskEdit1.Text := FormatDateTime('YYYYMMDD',Now);
          edt_Datee.Text := FormatDateTime('YYYYMMDD',Now);
          edt_MaintNo.Text := newDocNo;
    
        end
        else
          raise Exception.Create('복사한 데이터를 찾을수 없습니다');


      finally

        FreeAndNil(Dialog_CopyAPP700_frm);

      end;
      

end;

function TUI_APP700_frm.ReadListBetween(fromDate, toDate,KeyValue: string): Boolean;
begin
    Result := False;
  with qryList do
  begin
    Close;
    SQL.Text := APP700_SQL;
    SQL.Add(' WHERE APP_DATE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY APP_DATE ASC');
    Open;

    //ShowMessage(SQL.Text);

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;

  end;
end;


procedure TUI_APP700_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  APP700_PRINT_frm := TAPP700_PRINT_frm.Create(Self);
  try

    APP700_PRINT_frm.PrintDocument(qryList.Fields,True);

  finally
    FreeAndNil( APP700_PRINT_frm );
  end;
end;

procedure TUI_APP700_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  //
end;

procedure TUI_APP700_frm.memo_RowColPrint(Sender: TObject);
var
  memorow,memoCol : integer;
begin
  inherited;
  //메모의 라인번호
  memorow := (Sender as TsMemo).Perform(EM_LINEFROMCHAR,(Sender as TsMemo).SelStart,0);
  //메모의 컬럼번호
  memoCol := (Sender as TsMemo).SelStart - (Sender as TsMemo).Perform(EM_LINEINDEX,memorow,0);  

  if (Sender as TsMemo).Hint = 'DesGood' then
    sLabel56.Caption := IntToStr(memoRow+1)+'행   ' + IntToStr(memoCol)+'열'
  else if (Sender as TsMemo).Hint = 'doc2AA' then
    sLabel57.Caption := IntToStr(memoRow+1)+'행   ' + IntToStr(memoCol)+'열'
  else if (Sender as TsMemo).Hint = 'acd2AE' then
    sLabel58.Caption := IntToStr(memoRow+1)+'행   ' + IntToStr(memoCol)+'열';
end;

procedure TUI_APP700_frm.popMenu1Popup(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then
  begin
    N1.Enabled := False;
    N3.Enabled := False;
    N4.Enabled := False;
    d1.Enabled := False;
  end
  else
  begin
    N1.Enabled := not ( qryListCHK2.AsInteger in [5,9] );
    N3.Enabled := not ( qryListCHK2.AsInteger in [5,9] );
    N4.Enabled := not ( qryListCHK2.AsInteger in [5,9] );
  end;

end;

procedure TUI_APP700_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := APP700(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'APP700';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APP700_1';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

//        Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(ReadyDateTime)),
//                 FormatDateTime('YYYYMMDD',EndOfTheYear(ReadyDateTime)),
//                 RecvDoc);
        Readlist(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_APP700_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_APP700_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_APP700_frm.qryListCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var                                                         
  nIndex : integer;  
begin
  inherited;

  IF Length(qryListCHK3.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(qryListCHK3.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;
end;

procedure TUI_APP700_frm.memoLineLimit(Sender: TObject;
  var Key: Char);
var
  limitcount : Integer;
begin
  limitcount := 400;
  if (Key = #13) AND ((Sender as TsMemo).lines.count >= limitCount-1) then Key := #0;
end;

procedure TUI_APP700_frm.Mask_fromDateKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
//
end;

procedure TUI_APP700_frm.DialogKey(var msg: TCMDialogKey);
begin
  if ActiveControl = nil then Exit;

   if (ActiveControl.Name = 'edt_EXAddr2') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
   begin
     sPageControl1.ActivePageIndex := 1;
     Self.KeyPreview := False;
   end
   else if (ActiveControl.Name = 'edt_aaCcv4') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
   begin
     sPageControl1.ActivePageIndex := 2;
     Self.KeyPreview := False;
   end
   else if (ActiveControl.Name = 'memo_Desgood1') and (msg.CharCode = VK_TAB) then
   begin
     sPageControl1.ActivePageIndex := 3;
     Self.KeyPreview := False;
   end
   else if (ActiveControl.Name = 'edt_Origin_M') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
   begin
     sPageControl1.ActivePageIndex := 4;
     Self.KeyPreview := False;
   end
   else if (ActiveControl.Name = 'memo_doc2AA1') and (msg.CharCode = VK_TAB) then
   begin
     sPageControl1.ActivePageIndex := 5;
     Self.KeyPreview := False;
   end
   else
   begin
     Self.KeyPreview := True;
     inherited;
   end;
end;

end.



