unit UI_LOCAMA;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_LOCAMR_BP, DB, ADODB, sSkinProvider, StdCtrls, sMemo,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, sLabel, Grids, DBGrids,
  acDBGrid, Buttons, sBitBtn, Mask, sMaskEdit, sComboBox, sEdit, ComCtrls,
  sPageControl, sButton, sSpeedButton, ExtCtrls, sPanel, sSplitter;

type
  TUI_LOCAMA_frm = class(TUI_LOCAMR_BP_frm)
    edt_LOC1AMTC: TsEdit;
    edt_LOC1AMT: TsCurrencyEdit;
    edt_LOC2AMTC: TsEdit;
    edt_LOC2AMT: TsCurrencyEdit;
    edt_EX_RATE: TsEdit;
    qryListMAINT_NO: TStringField;
    qryListMSEQ: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListBGM_REF: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListRFF_NO: TStringField;
    qryListLC_NO: TStringField;
    qryListOFFERNO1: TStringField;
    qryListOFFERNO2: TStringField;
    qryListOFFERNO3: TStringField;
    qryListOFFERNO4: TStringField;
    qryListOFFERNO5: TStringField;
    qryListOFFERNO6: TStringField;
    qryListOFFERNO7: TStringField;
    qryListOFFERNO8: TStringField;
    qryListOFFERNO9: TStringField;
    qryListAMD_DATE: TStringField;
    qryListADV_DATE: TStringField;
    qryListISS_DATE: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListISSBANK: TStringField;
    qryListISSBANK1: TStringField;
    qryListISSBANK2: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListEXNAME1: TStringField;
    qryListEXNAME2: TStringField;
    qryListEXNAME3: TStringField;
    qryListDELIVERY: TStringField;
    qryListEXPIRY: TStringField;
    qryListCHGINFO: TStringField;
    qryListCHGINFO1: TMemoField;
    qryListLOC_TYPE: TStringField;
    qryListLOC1AMT: TBCDField;
    qryListLOC1AMTC: TStringField;
    qryListLOC2AMT: TBCDField;
    qryListLOC2AMTC: TStringField;
    qryListEX_RATE: TBCDField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListAPPADDR1: TStringField;
    qryListAPPADDR2: TStringField;
    qryListAPPADDR3: TStringField;
    qryListBNFADDR1: TStringField;
    qryListBNFADDR2: TStringField;
    qryListBNFADDR3: TStringField;
    qryListBNFEMAILID: TStringField;
    qryListBNFDOMAIN: TStringField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListLOC_TYPENAME: TStringField;
    sDBGrid2: TsDBGrid;
    sSpeedButton8: TsSpeedButton;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sButton2: TsButton;
    sPanel13: TsPanel;
    Mask_fromDate: TsMaskEdit;
    Mask_toDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    sPanel11: TsPanel;
    sPanel14: TsPanel;
    procedure FormShow(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sBitBtn1Click(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnPrintClick(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
  private
  protected
    procedure ReadDocument; override;
    function ReadList(OrderString: String = ''): Integer; override;
    function ReadListBetween(fromDate: String; toDate: String; KeyValue: String; KeyCount: Integer = 0): Boolean; override;
    procedure DeleteDocument; override;
    { Private declarations }

  public
    { Public declarations }
  end;

var
  UI_LOCAMA_frm: TUI_LOCAMA_frm;

implementation

uses Commonlib, TypeDefine, MSSQL, MessageDefine, LOCAMA_PRINT, DateUtils;

{$R *.dfm}

{ TUI_LOCAMA_frm }

procedure TUI_LOCAMA_frm.ReadDocument;
begin
  inherited;
  IF not qryList.Active Then Exit;
  IF qryList.RecordCount = 0 Then Exit;

//------------------------------------------------------------------------------
// 기본정보
//------------------------------------------------------------------------------
  //관리번호
  edt_DocNo1.Text := qryListMAINT_NO.AsString;
  edt_DocNo2.Text := qryListMSEQ.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  //등록일자
  sMaskEdit1.Text := qryListDATEE.AsString;
  //문서기능
  edt_msg1.Text := qryListMESSAGE1.AsString;
  //유형
  edt_msg2.Text := qryListMESSAGE2.AsString;
  //조건변경회차
  edt_AMD_NO.Text := qryListAMD_NO.AsString;
  //개설일자
  mask_ISS_DATE.Text := qryListISS_DATE.AsString;
  //변경신청일자
  mask_APP_DATE.Text := qryListAMD_DATE.AsString;
  //변경후 물품인도기일
  mask_DELIVERY.Text := qryListDELIVERY.AsString;
  //변경후 유효기일
  mask_EXPIRY.Text := qryListEXPIRY.AsString;
//------------------------------------------------------------------------------
// 개설의뢰인
//------------------------------------------------------------------------------
  //개설의뢰인코드
//  edt_APPLIC.Text := qryListAPPLIC.AsString;
  //상호
  edt_APPLIC1.Text := qryListAPPLIC1.AsString;
  //대표자
  edt_APPLIC2.Text := qryListAPPLIC2.AsString;
  //주소
  edt_APPADDR1.Text := qryListAPPADDR1.AsString;
  edt_APPADDR2.Text := qryListAPPADDR2.AsString;
  edt_APPADDR3.Text := qryListAPPADDR3.AsString;
  //사업자등록번호
  edt_APPLIC3.Text := qryListAPPLIC3.AsString;
//------------------------------------------------------------------------------
// 수혜자
//------------------------------------------------------------------------------
  //수혜자코드
//  edt_BENEFC.Text := qryListBENEFC.AsString;
  //상호
  edt_BENEFC1.Text := qryListBENEFC1.AsString;
  //대표자
  edt_BENEFC2.Text := qryListBENEFC2.AsString;
  //주소
  edt_BNFADDR1.Text := qryListBNFADDR1.AsString;
  edt_BNFADDR2.Text := qryListBNFADDR2.AsString;
  edt_BNFADDR3.Text := qryListBNFADDR3.AsString;
  //사업자등록번호
  edt_BENEFC3.Text := qryListBENEFC3.AsString;
  //이메일
  edt_BNFMAILID.Text := qryListBNFEMAILID.AsString;
  edt_BNFDOMAIN.Text := qryListBNFDOMAIN.AsString;
  //식별자
//  edt_CHKNAME1.Text := qryListCHKNAME1.AsString;
//  edt_CHKNAME2.Text := qryListCHKNAME2.AsString;
//------------------------------------------------------------------------------
// 개설은행
//------------------------------------------------------------------------------
  //개설은행
  edt_ISSBANK.Text  := qryListISSBANK.AsString;
  edt_ISSBANK1.Text := qryListISSBANK1.AsString;
  edt_ISSBANK2.Text := qryListISSBANK2.AsString;
  //신용장종류
  edt_LOC_TYPE.Text := qryListLOC_TYPE.AsString;
  //변경후 금액(외화/원화)   1이 외화 2가 원화
  edt_LOC1AMTC.Text := qryListLOC1AMTC.AsString;
  edt_LOC1AMT.Value := qryListLOC1AMT.AsCurrency;
  edt_LOC2AMTC.Text := qryListLOC2AMTC.AsString;
  edt_LOC2AMT.Value := qryListLOC2AMT.AsCurrency;

  //허용오차
  edt_CD_PERP.Text := qryListCD_PERP.AsString;
  edt_CD_PERM.Text := qryListCD_PERM.AsString;
  //내국신용장번호
  edt_LC_NO.Text := qryListLC_NO.AsString;
  //매매기준율
  edt_EX_RATE.Text := qryListEX_RATE.AsString;
//------------------------------------------------------------------------------
// 매도확약서
//------------------------------------------------------------------------------
  edt_OFFERNO1.Text := qryListOFFERNO1.AsString;
  edt_OFFERNO2.Text := qryListOFFERNO2.AsString;
  edt_OFFERNO3.Text := qryListOFFERNO3.AsString;
  edt_OFFERNO4.Text := qryListOFFERNO4.AsString;
  edt_OFFERNO5.Text := qryListOFFERNO5.AsString;
  edt_OFFERNO6.Text := qryListOFFERNO6.AsString;
  edt_OFFERNO7.Text := qryListOFFERNO7.AsString;
  edt_OFFERNO8.Text := qryListOFFERNO8.AsString;
  edt_OFFERNO9.Text := qryListOFFERNO9.AsString;
//------------------------------------------------------------------------------
// 기타정보
//------------------------------------------------------------------------------
  memo_REMARK1.Lines.Text := qryListREMARK1.AsString;
//------------------------------------------------------------------------------
// 기타조건변경사항
//------------------------------------------------------------------------------
  memo_CHGINFO.Lines.Text := qryListCHGINFO1.AsString;
//------------------------------------------------------------------------------
// 발신기관 전자서명
//------------------------------------------------------------------------------
  edt_EXNAME1.Text := qryListEXNAME1.AsString;
  edt_EXNAME2.Text := qryListEXNAME2.AsString;
  edt_EXNAME3.Text := qryListEXNAME3.AsString;
end;

function TUI_LOCAMA_frm.ReadList(OrderString: String): Integer;
begin
// 0 변경신청일
// 1 신용장번호
// 2 개설은행
  with qryList do
  begin
    Close;
    SQL.Text := FormSQL;
    Case com_SearchKeyword.ItemIndex of
      0:
      begin
        ReadListBetween(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'',0);
        Exit;
      end;
      1: SQL.Add('WHERE LC_NO LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
      2: SQL.Add('WHERE ISSBANK1 LIKE '+QuotedStr('%'+edt_SearchText.Text+'%'));
    end;

    IF OrderString <> '' then
    begin
      SQL.Add(OrderString);
    end
    else
    begin
      SQL.Add('ORDER BY AMD_DATE ');
    end;

    Open;
  end;
end;

function TUI_LOCAMA_frm.ReadListBetween(fromDate, toDate, KeyValue: String;
  KeyCount: Integer): Boolean;
begin
  Result := false;

  with qryList do
  begin
    Close;
    SQL.Text := FormSQL;
    SQL.Add('WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('ORDER BY DATEE ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO;MSEQ',VarArrayOf([KeyValue,KeyCount]),[]);
    end;

  end;
end;

procedure TUI_LOCAMA_frm.FormShow(Sender: TObject);
begin
  inherited;
  FormSQL := qryList.SQL.Text;
  WorkStyle := '응답서';
  Self.Caption := '내국신용장 조건변경 통지서';

  ProgramControlType := ctView;

  sPageControl1.ActivePageIndex := 0;
  ClearControlValue(sPanel2);
//  ReadOnlyControlValue(sPanel2);
//  ReadOnlyControlValue(sPanel4);
  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel2);

  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);

  ReadListBetween(Mask_SearchDate1.Text,Mask_SearchDate2.Text,'');

//   ShowMessage( FormatDateTime('YY',Now));

end;

procedure TUI_LOCAMA_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadDocument;
end;

procedure TUI_LOCAMA_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then ReadDocument;
//  btnDel.Enabled := DataSet.RecordCount > 0;
//  btnPrint.Enabled := DataSet.RecordCount > 0;
end;

procedure TUI_LOCAMA_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
  Mask_fromDate.Text := Mask_SearchDate1.Text;
  Mask_toDate.Text := Mask_SearchDate2.Text;
  
end;

procedure TUI_LOCAMA_frm.DeleteDocument;
var
  DocNo,DocCount : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    DocNo := qryListMAINT_NO.AsString;
    DocCount := qryListMSEQ.AsString;
    try
      try
        IF MessageBox(Self.Handle,PCHAR('내국신용장 조건변경 통지서'#13#10+MSG_SYSTEM_LINE2+#13#10+DocNo+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;
          SQL.Text := 'DELETE FROM LOCAMA WHERE MAINT_NO = '+QuotedStr(DocNo)+' AND MSEQ = '+DocCount;
          ExecSQL;
          DMMssql.KISConnect.CommitTrans;

          qryList.Close;
          qrylist.open;
          
          IF (qryList.RecordCount > 0) AND (qryList.RecordCount >= nCursor) Then
          begin
            qryList.MoveBy(nCursor-1);
          end
          else
            qryList.First;
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_LOCAMA_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  if not qrylist.Active then Exit;

  DeleteDocument;
end;

procedure TUI_LOCAMA_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCAMA_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LOCAMA_frm := nil;
end;

procedure TUI_LOCAMA_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  
  LOCAMA_PRINT_frm := TLOCAMA_PRINT_frm.Create(Self);
  try
    case (Sender as TsButton).Tag of
      //바로출력
      0 : LOCAMA_PRINT_frm.PrintDocument(qryList.Fields,False);
      //미리보기
      1 : LOCAMA_PRINT_frm.PrintDocument(qryList.Fields,True);
    end;
  finally
    FreeAndNil( LOCAMA_PRINT_frm );
  end;  
end;

procedure TUI_LOCAMA_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  ReadListBetween(Mask_fromDate.Text,Mask_toDate.Text,'');
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := Mask_toDate.Text;
end;

end.
