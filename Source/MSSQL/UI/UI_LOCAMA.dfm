inherited UI_LOCAMA_frm: TUI_LOCAMA_frm
  Left = 2087
  Top = 69
  Caption = 'UI_LOCAMA_frm'
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel1: TsPanel
    inherited sSpeedButton2: TsSpeedButton
      Left = 455
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 704
      Visible = False
    end
    inherited sSpeedButton4: TsSpeedButton
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 749
      Visible = False
    end
    object sSpeedButton8: TsSpeedButton [4]
      Left = 178
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel10: TsLabel [5]
      Left = 8
      Top = 5
      Width = 166
      Height = 17
      Caption = #45236#44397#49888#50857#51109' '#51312#44148#48320#44221' '#53685#51648#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel11: TsLabel [6]
      Left = 8
      Top = 20
      Width = 49
      Height = 13
      Caption = 'LOCAMA'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    inherited btnExit: TsButton
      Left = 1025
      OnClick = btnExitClick
    end
    inherited btnNew: TsButton
      Left = 739
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 735
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 188
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      Left = 710
      Visible = False
    end
    inherited btnPrint: TsButton
      Left = 255
      Width = 99
      Caption = #48148#47196#52636#47141
      OnClick = btnPrintClick
    end
    inherited btnTemp: TsButton
      Left = 561
      Visible = False
    end
    inherited btnSave: TsButton
      Left = 629
      Visible = False
    end
    inherited btnCancel: TsButton
      Left = 697
      Visible = False
    end
    inherited sButton1: TsButton
      Visible = False
    end
    inherited sButton3: TsButton
      Visible = False
    end
    object sButton2: TsButton
      Tag = 1
      Left = 354
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 11
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  inherited sPageControl1: TsPageControl
    inherited sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      inherited sPanel2: TsPanel
        Height = 565
        DesignSize = (
          1106
          565)
        inherited sSpeedButton1: TsSpeedButton
          Left = 618
          Top = -9
          Height = 551
        end
        inherited sSpeedButton6: TsSpeedButton
          Left = 256
          Top = 0
          Height = 558
        end
        inherited sLabel1: TsLabel
          Left = 402
          Top = 67
        end
        inherited sPanel5: TsPanel
          Left = 263
          Top = 84
          Width = 345
        end
        inherited edt_APPLIC: TsEdit
          Left = 128
          Top = 597
          Visible = False
          BoundLabel.Active = False
        end
        inherited sBitBtn4: TsBitBtn
          Left = 194
          Top = 597
          Visible = False
        end
        inherited edt_APPLIC1: TsEdit
          Left = 385
          Top = 105
          Height = 19
          Font.Height = -11
        end
        inherited edt_APPLIC2: TsEdit
          Left = 385
          Top = 124
          Height = 19
          Font.Height = -11
        end
        inherited edt_APPADDR1: TsEdit
          Left = 385
          Top = 162
          Height = 19
          Font.Height = -11
        end
        inherited edt_APPADDR2: TsEdit
          Left = 385
          Top = 181
          Height = 19
          Font.Height = -11
        end
        inherited edt_APPADDR3: TsEdit
          Left = 385
          Top = 200
          Height = 19
          Font.Height = -11
        end
        inherited sPanel6: TsPanel
          Left = 263
          Top = 223
          Width = 345
        end
        inherited edt_BENEFC: TsEdit
          Left = 128
          Top = 573
          Visible = False
          BoundLabel.Active = False
        end
        inherited sBitBtn5: TsBitBtn
          Left = 194
          Top = 573
          Visible = False
        end
        inherited edt_BENEFC1: TsEdit
          Left = 385
          Top = 244
          Height = 19
          Font.Height = -11
        end
        inherited edt_BENEFC2: TsEdit
          Left = 385
          Top = 263
          Height = 19
          Font.Height = -11
        end
        inherited edt_BNFADDR1: TsEdit
          Left = 385
          Top = 301
          Height = 19
          Font.Height = -11
        end
        inherited edt_BNFADDR2: TsEdit
          Left = 385
          Top = 320
          Height = 19
          Font.Height = -11
        end
        inherited edt_BNFADDR3: TsEdit
          Left = 385
          Top = 339
          Height = 19
          Font.Height = -11
        end
        inherited edt_BNFMAILID: TsEdit
          Left = 385
          Top = 358
          Height = 19
          Font.Height = -11
        end
        inherited sPanel8: TsPanel
          Left = 467
          Top = 358
          Height = 19
          ParentFont = False
        end
        inherited edt_BNFDOMAIN: TsEdit
          Left = 488
          Top = 358
          Height = 19
          Font.Height = -11
        end
        inherited edt_CHKNAME1: TsEdit
          Left = 112
          Top = 525
          Visible = False
        end
        inherited edt_CHKNAME2: TsEdit
          Left = 112
          Top = 547
          Visible = False
        end
        inherited edt_LOC_TYPE: TsEdit
          Left = 385
          Top = 535
          Height = 19
          Font.Height = -11
          OnChange = nil
          OnDblClick = nil
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        inherited sBitBtn7: TsBitBtn
          Left = 49
          Top = 449
          Visible = False
        end
        inherited sEdit21: TsEdit
          Left = 419
          Top = 535
          Width = 189
          Height = 19
          Font.Height = -11
        end
        inherited edt_LOC_AMTC: TsEdit
          Left = 15
          Top = 471
          Visible = False
          BoundLabel.Active = False
        end
        inherited sBitBtn8: TsBitBtn
          Left = 49
          Top = 471
          Visible = False
        end
        inherited edt_LOC_AMT: TsCurrencyEdit
          Left = 74
          Top = 471
          Width = 145
          Visible = False
        end
        inherited edt_CD_PERP: TsEdit
          Left = 385
          Top = 478
          Height = 19
          Font.Height = -11
        end
        inherited edt_CD_PERM: TsEdit
          Left = 440
          Top = 478
          Height = 19
          Font.Height = -11
        end
        inherited edt_LC_NO: TsEdit
          Left = 385
          Top = 497
          Height = 19
          Font.Height = -11
        end
        inherited edt_ISSBANK: TsEdit
          Left = 385
          Top = 402
          Width = 56
          Height = 19
          Color = clWhite
          Font.Height = -11
        end
        inherited edt_ISSBANK1: TsEdit
          Left = 442
          Top = 402
          Width = 166
          Height = 19
          Font.Height = -11
        end
        inherited edt_ISSBANK2: TsEdit
          Left = 385
          Top = 421
          Height = 19
          Font.Height = -11
        end
        inherited sBitBtn6: TsBitBtn
          Left = 193
          Top = 503
          Visible = False
        end
        inherited sPanel7: TsPanel
          Left = 263
          Top = 381
          Width = 345
        end
        inherited sPanel9: TsPanel
          Left = 638
          Top = 65
          Width = 345
        end
        inherited edt_OFFERNO1: TsEdit
          Left = 804
          Top = 86
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO2: TsEdit
          Left = 804
          Top = 105
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO3: TsEdit
          Left = 804
          Top = 124
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO4: TsEdit
          Left = 804
          Top = 143
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO5: TsEdit
          Left = 804
          Top = 162
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO6: TsEdit
          Left = 804
          Top = 181
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO7: TsEdit
          Left = 804
          Top = 200
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO8: TsEdit
          Left = 804
          Top = 219
          Height = 19
          Font.Height = -11
        end
        inherited edt_OFFERNO9: TsEdit
          Left = 804
          Top = 238
          Height = 19
          Font.Height = -11
        end
        inherited edt_AMD_NO: TsEdit
          Left = 367
          Top = 61
          Height = 19
          Font.Height = -11
        end
        inherited mask_ISS_DATE: TsMaskEdit
          Left = 367
          Top = 23
          Height = 19
          Font.Height = -11
        end
        inherited mask_APP_DATE: TsMaskEdit
          Left = 367
          Top = 42
          Height = 19
          Font.Height = -11
          BoundLabel.Caption = #51312#44148#48320#44221#51068#51088
        end
        inherited mask_DELIVERY: TsMaskEdit
          Left = 868
          Top = 23
          Height = 19
          Font.Height = -11
        end
        inherited mask_EXPIRY: TsMaskEdit
          Left = 868
          Top = 42
          Height = 19
          Font.Height = -11
        end
        inherited sPanel10: TsPanel
          Left = 263
          Top = 2
          Width = 345
        end
        inherited memo_REMARK1: TsMemo
          Left = 638
          Top = 282
          Width = 446
          Height = 63
          Font.Name = #44404#47548#52404
          BoundLabel.Active = False
          BoundLabel.UseSkinColor = True
        end
        inherited memo_CHGINFO: TsMemo
          Left = 638
          Top = 370
          Width = 446
          Height = 73
          Font.Height = -11
          Font.Name = #44404#47548#52404
          BoundLabel.Active = False
          BoundLabel.UseSkinColor = True
          BoundLabel.Caption = #44592#53440#51312#44148#13#10#48320#44221#49324#54637
        end
        inherited sPanel17: TsPanel
          Left = 638
          Top = 447
          Width = 345
        end
        inherited edt_EXNAME1: TsEdit
          Left = 785
          Top = 468
          Height = 19
          Font.Height = -11
        end
        inherited edt_EXNAME2: TsEdit
          Left = 785
          Top = 487
          Height = 19
          Font.Height = -11
        end
        inherited edt_EXNAME3: TsEdit
          Left = 785
          Top = 506
          Height = 19
          Font.Height = -11
        end
        inherited edt_APPLIC3: TsEdit
          Left = 385
          Top = 143
          Height = 19
          Font.Height = -11
        end
        inherited edt_BENEFC3: TsEdit
          Left = 385
          Top = 282
          Height = 19
          Font.Height = -11
        end
        inherited sPanel12: TsPanel
          Left = 638
          Top = 2
          Width = 345
          TabOrder = 65
        end
        object edt_LOC1AMTC: TsEdit
          Tag = 102
          Left = 385
          Top = 440
          Width = 33
          Height = 19
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 59
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#44221' '#54980' '#44552#50529'('#50808#54868')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_LOC1AMT: TsCurrencyEdit
          Left = 419
          Top = 440
          Width = 189
          Height = 19
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 60
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object edt_LOC2AMTC: TsEdit
          Tag = 102
          Left = 385
          Top = 459
          Width = 33
          Height = 19
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 61
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#44221' '#54980' '#44552#50529'('#50896#54868')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_LOC2AMT: TsCurrencyEdit
          Left = 419
          Top = 459
          Width = 189
          Height = 19
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 62
          SkinData.SkinSection = 'EDIT'
          GlyphMode.Grayed = False
          GlyphMode.Blend = 0
          DecimalPlaces = 4
          DisplayFormat = '#,0.####;0'
        end
        object edt_EX_RATE: TsEdit
          Tag = 102
          Left = 385
          Top = 516
          Width = 106
          Height = 19
          Hint = #53685#54868
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 63
          OnChange = edt_LOC_TYPEChange
          OnDblClick = edt_LOC_TYPEDblClick
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47588#47588' '#44592#51456#50984
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object sDBGrid2: TsDBGrid
          Left = 2
          Top = 29
          Width = 255
          Height = 654
          Color = clWhite
          Ctl3D = False
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 64
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnDrawColumnCell = sDBGrid1DrawColumnCell
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATEE'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MAINT_NO'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 142
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'MSEQ'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = #52264#49688
              Width = 30
              Visible = True
            end>
        end
        object sPanel13: TsPanel
          Left = 2
          Top = 2
          Width = 255
          Height = 27
          TabOrder = 66
          SkinData.SkinSection = 'PANEL'
          object Mask_fromDate: TsMaskEdit
            Left = 83
            Top = 2
            Width = 72
            Height = 23
            Color = clWhite
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #46321#47197#51068#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
          end
          object Mask_toDate: TsMaskEdit
            Left = 156
            Top = 2
            Width = 72
            Height = 23
            Color = clWhite
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51312#54924
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.SkinSection = 'EDIT'
          end
          object sBitBtn22: TsBitBtn
            Left = 229
            Top = 2
            Width = 25
            Height = 23
            Cursor = crHandPoint
            TabOrder = 2
            OnClick = sBitBtn22Click
            ImageIndex = 6
            Images = DMICON.System18
            SkinData.SkinSection = 'BUTTON'
          end
        end
        object sPanel11: TsPanel
          Left = 638
          Top = 261
          Width = 345
          Height = 20
          Caption = #44592#53440#51221#48372
          Color = 16042877
          TabOrder = 67
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
        object sPanel14: TsPanel
          Left = 638
          Top = 349
          Width = 345
          Height = 20
          Caption = #44592#53440#51312#44148' '#48320#46041#49324#54637
          Color = 16042877
          TabOrder = 68
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sPanel3: TsPanel
        inherited edt_SearchText: TsEdit
          Left = 102
        end
        inherited com_SearchKeyword: TsComboBox
          Width = 97
          TabOrder = 3
          TabStop = False
          Items.Strings = (
            #46321#47197#51068#51088
            #49888#50857#51109#48264#54840
            #44060#49444#51008#54665)
        end
        inherited Mask_SearchDate1: TsMaskEdit
          Left = 102
          TabOrder = 0
        end
        inherited sBitBtn1: TsBitBtn
          Left = 335
          TabOrder = 4
          OnClick = sBitBtn1Click
        end
        inherited sBitBtn21: TsBitBtn
          Left = 183
          TabOrder = 5
          TabStop = False
        end
        inherited Mask_SearchDate2: TsMaskEdit
          Left = 230
          TabOrder = 1
        end
        inherited sBitBtn23: TsBitBtn
          Left = 311
          TabStop = False
        end
        inherited sPanel25: TsPanel
          Left = 206
        end
      end
      inherited sDBGrid1: TsDBGrid
        Height = 533
        DataSource = dsList
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Color = clBtnFace
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 171
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'MSEQ'
            Title.Alignment = taCenter
            Title.Caption = #52264#49688
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BGM_REF'
            Title.Alignment = taCenter
            Title.Caption = #51008#54665#49888#52397#48264#54840
            Width = 188
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'APPLIC1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51088
            Width = 230
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ISSBANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 168
            Visible = True
          end
          item
            Alignment = taCenter
            Color = 13041663
            Expanded = False
            FieldName = 'LC_NO'
            Title.Alignment = taCenter
            Title.Caption = #49888#50857#51109#48264#54840
            Width = 217
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'AMD_DATE'
            Title.Alignment = taCenter
            Title.Caption = #48320#44221#49888#52397#51068
            Width = 95
            Visible = True
          end>
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 288
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      
        'SELECT MAINT_NO, MSEQ, AMD_NO, [USER_ID], DATEE, BGM_REF, MESSAG' +
        'E1, MESSAGE2, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFFERNO3, OFFER' +
        'NO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO9, AMD_DATE,' +
        ' ADV_DATE, ISS_DATE, REMARK, REMARK1, ISSBANK, ISSBANK1, ISSBANK' +
        '2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNAME1' +
        ', EXNAME2, EXNAME3, DELIVERY, EXPIRY, CHGINFO, CHGINFO1, LOC_TYP' +
        'E, LOC1AMT, LOC1AMTC, LOC2AMT, LOC2AMTC, EX_RATE, CHK1, CHK2, CH' +
        'K3, PRNO, APPADDR1, APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFA' +
        'DDR3, BNFEMAILID, BNFDOMAIN, CD_PERP, CD_PERM,'
      '        ISNULL(N4487.DOC_NAME,'#39#39') as LOC_TYPENAME'
      
        'FROM [LOCAMA] with(nolock) LEFT JOIN (SELECT CODE,NAME as DOC_NA' +
        'ME FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON' +
        ' LOCAMA.LOC_TYPE = N4487.CODE')
    Left = 16
    Top = 256
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
    object qryListAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListBGM_REF: TStringField
      FieldName = 'BGM_REF'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryListOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryListOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryListOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryListOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryListOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryListOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryListOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryListOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryListAMD_DATE: TStringField
      FieldName = 'AMD_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      Size = 8
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListISSBANK: TStringField
      FieldName = 'ISSBANK'
      Size = 4
    end
    object qryListISSBANK1: TStringField
      FieldName = 'ISSBANK1'
      Size = 35
    end
    object qryListISSBANK2: TStringField
      FieldName = 'ISSBANK2'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryListEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryListEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryListDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryListEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryListCHGINFO: TStringField
      FieldName = 'CHGINFO'
      Size = 1
    end
    object qryListCHGINFO1: TMemoField
      FieldName = 'CHGINFO1'
      BlobType = ftMemo
    end
    object qryListLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryListLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qryListLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object qryListLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qryListLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object qryListEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryListAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryListAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryListBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryListBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryListBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryListBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryListBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      ReadOnly = True
      Size = 100
    end
  end
  inherited dsList: TDataSource
    Left = 48
    Top = 256
  end
end
