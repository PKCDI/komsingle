inherited UI_ADV710_NEW_frm: TUI_ADV710_NEW_frm
  Left = 687
  Top = 69
  Caption = '[ADV710]'#53440#54665#44221#50976#49888#50857#51109' '#53685#51648#49436
  PixelsPerInch = 96
  TextHeight = 12
  inherited btn_Panel: TsPanel
    inherited sLabel7: TsLabel
      Left = 16
      Width = 176
      Caption = #53440#54665#44221#50976#49888#50857#51109' '#53685#51648#49436
    end
    inherited sLabel6: TsLabel
      Left = 67
      Caption = '(ADV710)'
    end
    inherited sPanel6: TsPanel
      inherited msk_Datee: TsMaskEdit
        Left = 408
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.ParentFont = False
      end
      inherited com_func: TsComboBox
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.ParentFont = False
      end
      inherited com_type: TsComboBox
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.ParentFont = False
      end
      inherited edt_userno: TsEdit
        Width = 25
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.ParentFont = False
      end
    end
  end
  inherited sPanel4: TsPanel
    inherited sDBgrid1: TsDBGrid
      OnDrawColumnCell = DrawColumnCell
      OnTitleClick = sDBgrid1TitleClick
    end
  end
  inherited sPanel3: TsPanel
    inherited sPageControl1: TsPageControl
      inherited sTabSheet1: TsTabSheet
        inherited sPanel7: TsPanel
          inherited sPanel127: TsPanel
            Top = 232
          end
          inherited sPanel128: TsPanel
            Top = 232
          end
          inherited memo_ADDINFO1: TsMemo
            Top = 256
          end
          inherited sPanel129: TsPanel
            Top = 232
          end
          object sPanel143: TsPanel
            Left = 9
            Top = 192
            Width = 53
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '20'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 26
          end
          object sPanel144: TsPanel
            Left = 63
            Top = 192
            Width = 119
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #48156#49888#51008#54665' '#52280#51312#49324#54637
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
          end
          object edt_ADV_NO: TsEdit
            Left = 183
            Top = 192
            Width = 276
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 28
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
        end
      end
      inherited sTabSheet3: TsTabSheet
        inherited sPanel27: TsPanel
          Enabled = False
          inherited edt_DOCCD1: TsEdit
            Width = 183
            Text = ''
          end
          inherited edt_CDNO: TsEdit
            Width = 183
          end
          object sPanel145: TsPanel [37]
            Left = 337
            Top = 233
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '52a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 53
          end
          object sPanel146: TsPanel [38]
            Left = 372
            Top = 233
            Width = 165
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Issuing Bank'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 54
          end
          object edt_ISSBANK: TsEdit [39]
            Left = 538
            Top = 233
            Width = 123
            Height = 23
            Hint = #44060#49444#51032#47280#51064' '#51008#54665'(BIC'#53076#46300')'
            Color = clWhite
            Enabled = False
            MaxLength = 11
            ParentShowHint = False
            ShowHint = True
            TabOrder = 55
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ISSBANK1: TsEdit [40]
            Left = 409
            Top = 257
            Width = 252
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 56
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel147: TsPanel [41]
            Left = 337
            Top = 257
            Width = 71
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51008#54665#47749
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 57
          end
          object edt_ISSBANK2: TsEdit [42]
            Left = 409
            Top = 279
            Width = 252
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 58
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ISSBANK3: TsEdit [43]
            Left = 409
            Top = 301
            Width = 252
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 59
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ISSBANK4: TsEdit [44]
            Left = 409
            Top = 323
            Width = 252
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 60
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ISSBANK5: TsEdit [45]
            Left = 409
            Top = 347
            Width = 252
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 61
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel148: TsPanel [46]
            Left = 337
            Top = 347
            Width = 71
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 62
          end
          inherited sPanel39: TsPanel
            Left = 337
          end
          inherited sPanel41: TsPanel
            Left = 372
            Width = 289
          end
          inherited edt_BENEFC1: TsEdit
            Left = 337
            Width = 324
          end
          inherited edt_BENEFC2: TsEdit
            Left = 337
            Width = 324
          end
          inherited edt_BENEFC3: TsEdit
            Left = 337
            Width = 324
          end
          inherited edt_BENEFC4: TsEdit
            Left = 337
            Width = 324
          end
          inherited edt_BENEFC5: TsEdit
            Left = 409
            Width = 252
          end
          inherited sPanel42: TsPanel
            Left = 337
          end
          inherited sPanel142: TsPanel
            Left = 337
          end
          inherited edt_BENEFC6: TsEdit
            Left = 409
            Width = 252
          end
          inherited curr_CDAMT: TsCurrencyEdit
            TabOrder = 63
          end
          object edt_DOCCD2: TsEdit
            Left = 418
            Top = 57
            Width = 246
            Height = 23
            Hint = #49888#50857#51109#51333#47448
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 64
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
        end
      end
      inherited sTabSheet2: TsTabSheet
        inherited sPanel34: TsPanel
          inherited edt_DRAFT4: TsEdit
            Visible = False
          end
        end
      end
      inherited sTabSheet6: TsTabSheet
        inherited sPanel71: TsPanel
          inherited sPanel104: TsPanel
            Top = 186
          end
          inherited sPanel105: TsPanel
            Top = 186
          end
          inherited sPanel106: TsPanel
            Top = 186
          end
          inherited edt_PERIOD_IN_DAYS: TsEdit
            Top = 210
          end
          inherited sPanel107: TsPanel
            Top = 210
          end
          inherited edt_PERIOD_DETAIL: TsEdit
            Top = 210
          end
          inherited sPanel108: TsPanel
            Left = 350
            Top = 186
          end
          inherited sPanel109: TsPanel
            Left = 385
            Top = 186
          end
          inherited edt_CONFIRMM: TsEdit
            Left = 567
            Top = 186
            Width = 102
          end
          inherited sPanel116: TsPanel
            Top = 211
          end
          inherited sPanel117: TsPanel
            Top = 211
          end
          inherited sPanel118: TsPanel
            Top = 211
          end
          inherited sPanel119: TsPanel
            Top = 235
          end
          inherited edt_REQ_BANK: TsEdit
            Top = 235
          end
          inherited sPanel120: TsPanel
            Top = 259
          end
          inherited edt_REQ_BANK1: TsEdit
            Top = 259
          end
          inherited edt_REQ_BANK2: TsEdit
            Top = 281
          end
          inherited edt_REQ_BANK3: TsEdit
            Top = 303
          end
          inherited edt_REQ_BANK4: TsEdit
            Top = 325
          end
          inherited sPanel121: TsPanel
            Top = 353
          end
          inherited sPanel122: TsPanel
            Top = 353
          end
          inherited sPanel123: TsPanel
            Top = 353
          end
          inherited memo_INSTRCT1: TsMemo
            Top = 377
          end
          object edt_CHARGE4: TsEdit
            Left = 9
            Top = 99
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 41
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_CHARGE5: TsEdit
            Left = 9
            Top = 121
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 42
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_CHARGE6: TsEdit
            Left = 9
            Top = 143
            Width = 319
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 43
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
        end
      end
      inherited sTabSheet8: TsTabSheet
        inherited sPanel124: TsPanel
          inherited sPanel125: TsPanel
            Height = 27
          end
          inherited sPanel134: TsPanel
            Height = 27
          end
          inherited sPanel135: TsPanel
            Height = 27
          end
          inherited sPanel136: TsPanel
            Top = 37
          end
          inherited edt_AVTBANK: TsEdit
            Top = 37
          end
          inherited sPanel137: TsPanel
            Top = 61
          end
          inherited edt_AVTBANK1: TsEdit
            Top = 61
          end
          inherited edt_AVTBANK2: TsEdit
            Top = 83
          end
          inherited edt_AVTBANK3: TsEdit
            Top = 105
          end
          inherited edt_AVTBANK4: TsEdit
            Top = 127
          end
          inherited sPanel138: TsPanel
            Top = 151
          end
          inherited edt_AVTACCNT: TsEdit
            Top = 151
          end
          inherited edt_EXNAME1: TsEdit
            Top = 210
          end
          inherited edt_EXNAME2: TsEdit
            Top = 234
          end
          inherited edt_EXNAME3: TsEdit
            Top = 258
          end
          inherited edt_EXADDR1: TsEdit
            Top = 282
          end
          inherited sPanel59: TsPanel
            Top = 182
            Height = 27
          end
          inherited edt_EXADDR2: TsEdit
            Top = 306
          end
          inherited sPanel132: TsPanel
            Top = 182
            Height = 27
          end
          inherited sPanel89: TsPanel
            Top = 210
          end
          inherited sPanel131: TsPanel
            Top = 258
          end
          inherited sPanel133: TsPanel
            Top = 282
          end
          object sPanel149: TsPanel [31]
            Left = 336
            Top = 182
            Width = 34
            Height = 27
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '50B'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 31
          end
          object sPanel150: TsPanel [32]
            Left = 371
            Top = 182
            Width = 196
            Height = 27
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Non-Bank Issuer'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object edt_NISS_BANK1: TsEdit [33]
            Left = 336
            Top = 210
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 33
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_NISS_BANK2: TsEdit [34]
            Left = 336
            Top = 232
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 34
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_NISS_BANK3: TsEdit [35]
            Left = 336
            Top = 254
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 35
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel151: TsPanel [36]
            Left = 568
            Top = 182
            Width = 102
            Height = 27
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #48708#51008#54665#44060#49444#51088
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 36
          end
          object edt_NISS_BANK4: TsEdit [37]
            Left = 336
            Top = 276
            Width = 334
            Height = 23
            Color = clWhite
            Enabled = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 37
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          inherited sPanel152: TsPanel
            TabOrder = 38
          end
          inherited sPanel153: TsPanel
            TabOrder = 39
          end
          inherited sPanel154: TsPanel
            TabOrder = 40
          end
          inherited memo_Special: TsMemo
            TabOrder = 41
          end
        end
      end
      inherited sTabSheet4: TsTabSheet
        inherited sDBGrid3: TsDBGrid
          OnDrawColumnCell = DrawColumnCell
          OnTitleClick = sDBgrid1TitleClick
        end
      end
    end
  end
  inherited qryList: TADOQuery
    AfterOpen = nil
    AfterScroll = nil
    Left = 8
    Top = 16
  end
  inherited dsList: TDataSource
    DataSet = qryADV710
    Left = 40
    Top = 64
  end
  object qryADV710: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryADV710AfterOpen
    AfterScroll = qryADV710AfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180817'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT A.MAINT_NO, CHK1, CHK2, CHK3, MESSAGE1, MESSAGE2, USER_ID' +
        ', DATEE, APP_DATE, ISS_DATE, PRE_DATE1, PRE_DATE2, PRE_DATE3, PR' +
        'E_DATE4,'
      
        'APP_NO, LIC_NO, ADV_NO, REF_NO, DRAFT, MIX_PAY, DEF_PAY, ADDINFO' +
        ', ADDINFO_1, DOC_CD1, DOC_CD2, DOC_CD3, PRNO, DRAFT1, DRAFT2,'
      
        'MIX_PAY1, MIX_PAY2, MIX_PAY3, DEF_PAY1, DEF_PAY2, DEF_PAY3, CO_B' +
        'ANK, CO_BANK1, CO_BANK2, CO_BANK3, CO_BANK4, PERIOD_TXT, SPECIAL' +
        '_DESC, SPECIAL_DESC_1, PERIOD_DAYS,'
      
        'AA_CV1, AA_CV2, AA_CV3, AA_CV4, PSHIP, TSHIP, DESGOOD, DESGOOD_1' +
        ', DOCREQU, DOCREQU_1, ADDCOND, ADDCOND_1,'
      
        'CHARGE1, CHARGE2, CHARGE3, CHARGE4, CHARGE5, CHARGE6, PD_PRSNT1,' +
        ' PD_PRSNT2, PD_PRSNT3, PD_PRSNT4, CONFIRMM, INSTRCT, INSTRCT_1,'
      
        'SND_INFO1, SND_INFO2, SND_INFO3, SND_INFO4, SND_INFO5, SND_INFO6' +
        ', SE_BANK, SE_BANK1, SE_BANK2, SE_BANK3, SE_BANK4, SE_BANK5,'
      
        'RE_BANK, RE_BANK1, RE_BANK2, RE_BANK3, RE_BANK4, RE_BANK5, RE_TE' +
        'L, ISS_BANK, ISS_BANK1, ISS_BANK2, ISS_BANK3, ISS_BANK4, ISS_BAN' +
        'K5, ISS_ACCNT,'
      
        'APP_BANK, APP_BANK1, APP_BANK2, APP_BANK3, APP_BANK4, APP_BANK5,' +
        ' APP_ACCNT,'
      
        'AV_AIL, AV_AIL1, AV_AIL2, AV_AIL3, AV_AIL4, AV_AIL5, AV_ACCNT, A' +
        'V_PAY, DR_AWEE, DR_AWEE1, DR_AWEE2, DR_AWEE3, DR_AWEE4, DR_AWEE5' +
        ', DR_ACCNT,'
      
        'REI_BANK, REI_BANK1, REI_BANK2, REI_BANK3, REI_BANK4, REI_BANK5,' +
        ' REI_ACCNT, AVT_BANK, AVT_BANK1, AVT_BANK2, AVT_BANK3, AVT_BANK4' +
        ', AVT_BANK5,'
      
        'AVT_ACCNT, AVT_TEL, APPLIC1, APPLIC2, APPLIC3, APPLIC4, BENEFC1,' +
        ' BENEFC2, BENEFC3, BENEFC4, BENEFC5, EX_NAME1, EX_NAME2, EX_NAME' +
        '3, EX_ADDR1, EX_ADDR2,'
      
        'BEN_TEL, EX_DATE, EX_PLACE, CD_AMT, CD_CUR, CD_PERP, CD_PERM, CD' +
        '_MAX, CD_AMTC'
      
        'LOAD_ON, FOR_TRAN, LST_DATE, SHIP_PD, SHIP_PD1, SHIP_PD2, SHIP_P' +
        'D3, SHIP_PD4, SHIP_PD5, SHIP_PD6,'
      
        'APPLICABLE_RULES_1, APPLICABLE_RULES_2, SUNJUCK_PORT, DOCHACK_PO' +
        'RT, Non_Bank_Issuer1, Non_Bank_Issuer2, Non_Bank_Issuer3, Non_Ba' +
        'nk_Issuer4'
      'FROM ADV710 A INNER JOIN ADV7102 B ON A.MAINT_NO = B.MAINT_NO'
      '              INNER JOIN ADV7103 C ON A.MAINT_NO = C.MAINT_NO'
      '              INNER JOIN ADV7104 D ON A.MAINT_NO = D.MAINT_NO'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(A.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 8
    Top = 64
    object qryADV710MAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryADV710CHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryADV710CHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryADV710CHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryADV710MESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryADV710MESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryADV710USER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryADV710DATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryADV710APP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryADV710ISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryADV710PRE_DATE1: TStringField
      FieldName = 'PRE_DATE1'
      Size = 35
    end
    object qryADV710PRE_DATE2: TStringField
      FieldName = 'PRE_DATE2'
      Size = 35
    end
    object qryADV710PRE_DATE3: TStringField
      FieldName = 'PRE_DATE3'
      Size = 35
    end
    object qryADV710PRE_DATE4: TStringField
      FieldName = 'PRE_DATE4'
      Size = 35
    end
    object qryADV710APP_NO: TStringField
      FieldName = 'APP_NO'
      Size = 35
    end
    object qryADV710LIC_NO: TStringField
      FieldName = 'LIC_NO'
      Size = 35
    end
    object qryADV710ADV_NO: TStringField
      FieldName = 'ADV_NO'
      Size = 35
    end
    object qryADV710REF_NO: TStringField
      FieldName = 'REF_NO'
      Size = 35
    end
    object qryADV710DRAFT: TStringField
      FieldName = 'DRAFT'
      Size = 35
    end
    object qryADV710MIX_PAY: TStringField
      FieldName = 'MIX_PAY'
      Size = 35
    end
    object qryADV710DEF_PAY: TStringField
      FieldName = 'DEF_PAY'
      Size = 35
    end
    object qryADV710ADDINFO: TStringField
      FieldName = 'ADDINFO'
      Size = 1
    end
    object qryADV710ADDINFO_1: TMemoField
      FieldName = 'ADDINFO_1'
      BlobType = ftMemo
    end
    object qryADV710DOC_CD1: TStringField
      FieldName = 'DOC_CD1'
      Size = 35
    end
    object qryADV710DOC_CD2: TStringField
      FieldName = 'DOC_CD2'
      Size = 35
    end
    object qryADV710DOC_CD3: TStringField
      FieldName = 'DOC_CD3'
      Size = 65
    end
    object qryADV710PRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryADV710DRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryADV710DRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryADV710MIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryADV710MIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryADV710MIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryADV710DEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryADV710DEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryADV710DEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryADV710CO_BANK: TStringField
      FieldName = 'CO_BANK'
      Size = 11
    end
    object qryADV710CO_BANK1: TStringField
      FieldName = 'CO_BANK1'
      Size = 35
    end
    object qryADV710CO_BANK2: TStringField
      FieldName = 'CO_BANK2'
      Size = 35
    end
    object qryADV710CO_BANK3: TStringField
      FieldName = 'CO_BANK3'
      Size = 35
    end
    object qryADV710CO_BANK4: TStringField
      FieldName = 'CO_BANK4'
      Size = 35
    end
    object qryADV710PERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 35
    end
    object qryADV710SPECIAL_DESC: TStringField
      FieldName = 'SPECIAL_DESC'
      Size = 1
    end
    object qryADV710AA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryADV710AA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryADV710AA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryADV710AA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryADV710PSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 35
    end
    object qryADV710TSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 35
    end
    object qryADV710DESGOOD: TStringField
      FieldName = 'DESGOOD'
      Size = 1
    end
    object qryADV710DESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryADV710DOCREQU: TStringField
      FieldName = 'DOCREQU'
      Size = 1
    end
    object qryADV710DOCREQU_1: TMemoField
      FieldName = 'DOCREQU_1'
      BlobType = ftMemo
    end
    object qryADV710ADDCOND: TStringField
      FieldName = 'ADDCOND'
      Size = 1
    end
    object qryADV710ADDCOND_1: TMemoField
      FieldName = 'ADDCOND_1'
      BlobType = ftMemo
    end
    object qryADV710CHARGE1: TStringField
      FieldName = 'CHARGE1'
      Size = 35
    end
    object qryADV710CHARGE2: TStringField
      FieldName = 'CHARGE2'
      Size = 35
    end
    object qryADV710CHARGE3: TStringField
      FieldName = 'CHARGE3'
      Size = 35
    end
    object qryADV710CHARGE4: TStringField
      FieldName = 'CHARGE4'
      Size = 35
    end
    object qryADV710CHARGE5: TStringField
      FieldName = 'CHARGE5'
      Size = 35
    end
    object qryADV710CHARGE6: TStringField
      FieldName = 'CHARGE6'
      Size = 35
    end
    object qryADV710PD_PRSNT1: TStringField
      FieldName = 'PD_PRSNT1'
      Size = 35
    end
    object qryADV710PD_PRSNT2: TStringField
      FieldName = 'PD_PRSNT2'
      Size = 35
    end
    object qryADV710PD_PRSNT3: TStringField
      FieldName = 'PD_PRSNT3'
      Size = 35
    end
    object qryADV710PD_PRSNT4: TStringField
      FieldName = 'PD_PRSNT4'
      Size = 35
    end
    object qryADV710CONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 35
    end
    object qryADV710INSTRCT: TStringField
      FieldName = 'INSTRCT'
      Size = 1
    end
    object qryADV710INSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryADV710SND_INFO1: TStringField
      FieldName = 'SND_INFO1'
      Size = 35
    end
    object qryADV710SND_INFO2: TStringField
      FieldName = 'SND_INFO2'
      Size = 35
    end
    object qryADV710SND_INFO3: TStringField
      FieldName = 'SND_INFO3'
      Size = 35
    end
    object qryADV710SND_INFO4: TStringField
      FieldName = 'SND_INFO4'
      Size = 35
    end
    object qryADV710SND_INFO5: TStringField
      FieldName = 'SND_INFO5'
      Size = 35
    end
    object qryADV710SND_INFO6: TStringField
      FieldName = 'SND_INFO6'
      Size = 35
    end
    object qryADV710SE_BANK: TStringField
      FieldName = 'SE_BANK'
      Size = 11
    end
    object qryADV710SE_BANK1: TStringField
      FieldName = 'SE_BANK1'
      Size = 35
    end
    object qryADV710SE_BANK2: TStringField
      FieldName = 'SE_BANK2'
      Size = 35
    end
    object qryADV710SE_BANK3: TStringField
      FieldName = 'SE_BANK3'
      Size = 35
    end
    object qryADV710SE_BANK4: TStringField
      FieldName = 'SE_BANK4'
      Size = 35
    end
    object qryADV710SE_BANK5: TStringField
      FieldName = 'SE_BANK5'
      Size = 35
    end
    object qryADV710RE_BANK: TStringField
      FieldName = 'RE_BANK'
      Size = 11
    end
    object qryADV710RE_BANK1: TStringField
      FieldName = 'RE_BANK1'
      Size = 35
    end
    object qryADV710RE_BANK2: TStringField
      FieldName = 'RE_BANK2'
      Size = 35
    end
    object qryADV710RE_BANK3: TStringField
      FieldName = 'RE_BANK3'
      Size = 35
    end
    object qryADV710RE_BANK4: TStringField
      FieldName = 'RE_BANK4'
      Size = 35
    end
    object qryADV710RE_BANK5: TStringField
      FieldName = 'RE_BANK5'
      Size = 35
    end
    object qryADV710RE_TEL: TStringField
      FieldName = 'RE_TEL'
      Size = 25
    end
    object qryADV710ISS_BANK: TStringField
      FieldName = 'ISS_BANK'
      Size = 11
    end
    object qryADV710ISS_BANK1: TStringField
      FieldName = 'ISS_BANK1'
      Size = 35
    end
    object qryADV710ISS_BANK2: TStringField
      FieldName = 'ISS_BANK2'
      Size = 35
    end
    object qryADV710ISS_BANK3: TStringField
      FieldName = 'ISS_BANK3'
      Size = 35
    end
    object qryADV710ISS_BANK4: TStringField
      FieldName = 'ISS_BANK4'
      Size = 35
    end
    object qryADV710ISS_BANK5: TStringField
      FieldName = 'ISS_BANK5'
      Size = 35
    end
    object qryADV710ISS_ACCNT: TStringField
      FieldName = 'ISS_ACCNT'
      Size = 35
    end
    object qryADV710APP_BANK: TStringField
      FieldName = 'APP_BANK'
      Size = 11
    end
    object qryADV710APP_BANK1: TStringField
      FieldName = 'APP_BANK1'
      Size = 35
    end
    object qryADV710APP_BANK2: TStringField
      FieldName = 'APP_BANK2'
      Size = 35
    end
    object qryADV710APP_BANK3: TStringField
      FieldName = 'APP_BANK3'
      Size = 35
    end
    object qryADV710APP_BANK4: TStringField
      FieldName = 'APP_BANK4'
      Size = 35
    end
    object qryADV710APP_BANK5: TStringField
      FieldName = 'APP_BANK5'
      Size = 35
    end
    object qryADV710AV_AIL: TStringField
      FieldName = 'AV_AIL'
      Size = 11
    end
    object qryADV710AV_AIL1: TStringField
      FieldName = 'AV_AIL1'
      Size = 35
    end
    object qryADV710AV_AIL2: TStringField
      FieldName = 'AV_AIL2'
      Size = 35
    end
    object qryADV710AV_AIL3: TStringField
      FieldName = 'AV_AIL3'
      Size = 35
    end
    object qryADV710AV_AIL4: TStringField
      FieldName = 'AV_AIL4'
      Size = 35
    end
    object qryADV710AV_AIL5: TStringField
      FieldName = 'AV_AIL5'
      Size = 35
    end
    object qryADV710AV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Size = 35
    end
    object qryADV710AV_PAY: TStringField
      FieldName = 'AV_PAY'
      Size = 35
    end
    object qryADV710DR_AWEE: TStringField
      FieldName = 'DR_AWEE'
      Size = 11
    end
    object qryADV710DR_AWEE1: TStringField
      FieldName = 'DR_AWEE1'
      Size = 35
    end
    object qryADV710DR_AWEE2: TStringField
      FieldName = 'DR_AWEE2'
      Size = 35
    end
    object qryADV710DR_AWEE3: TStringField
      FieldName = 'DR_AWEE3'
      Size = 35
    end
    object qryADV710DR_AWEE4: TStringField
      FieldName = 'DR_AWEE4'
      Size = 35
    end
    object qryADV710DR_AWEE5: TStringField
      FieldName = 'DR_AWEE5'
      Size = 35
    end
    object qryADV710DR_ACCNT: TStringField
      FieldName = 'DR_ACCNT'
      Size = 35
    end
    object qryADV710REI_BANK: TStringField
      FieldName = 'REI_BANK'
      Size = 11
    end
    object qryADV710REI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Size = 35
    end
    object qryADV710REI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Size = 35
    end
    object qryADV710REI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Size = 35
    end
    object qryADV710REI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Size = 35
    end
    object qryADV710REI_BANK5: TStringField
      FieldName = 'REI_BANK5'
      Size = 35
    end
    object qryADV710REI_ACCNT: TStringField
      FieldName = 'REI_ACCNT'
      Size = 35
    end
    object qryADV710AVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Size = 11
    end
    object qryADV710AVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Size = 35
    end
    object qryADV710AVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Size = 35
    end
    object qryADV710AVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Size = 35
    end
    object qryADV710AVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Size = 35
    end
    object qryADV710AVT_BANK5: TStringField
      FieldName = 'AVT_BANK5'
      Size = 35
    end
    object qryADV710AVT_ACCNT: TStringField
      FieldName = 'AVT_ACCNT'
      Size = 35
    end
    object qryADV710AVT_TEL: TStringField
      FieldName = 'AVT_TEL'
      Size = 25
    end
    object qryADV710APPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryADV710APPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryADV710APPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryADV710APPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryADV710BENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryADV710BENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryADV710BENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryADV710BENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryADV710BENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryADV710EX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryADV710EX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryADV710EX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryADV710EX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryADV710EX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryADV710BEN_TEL: TStringField
      FieldName = 'BEN_TEL'
      Size = 25
    end
    object qryADV710EX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryADV710EX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryADV710CD_AMT: TBCDField
      FieldName = 'CD_AMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryADV710CD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryADV710CD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryADV710CD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryADV710CD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 70
    end
    object qryADV710LOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryADV710FOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryADV710LST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryADV710SHIP_PD: TStringField
      FieldName = 'SHIP_PD'
      Size = 1
    end
    object qryADV710SHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryADV710SHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryADV710SHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryADV710SHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryADV710SHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryADV710SHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryADV710APPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Size = 30
    end
    object qryADV710APPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Size = 35
    end
    object qryADV710SUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryADV710DOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryADV710Non_Bank_Issuer1: TStringField
      FieldName = 'Non_Bank_Issuer1'
      Size = 35
    end
    object qryADV710Non_Bank_Issuer2: TStringField
      FieldName = 'Non_Bank_Issuer2'
      Size = 35
    end
    object qryADV710Non_Bank_Issuer3: TStringField
      FieldName = 'Non_Bank_Issuer3'
      Size = 35
    end
    object qryADV710Non_Bank_Issuer4: TStringField
      FieldName = 'Non_Bank_Issuer4'
      Size = 35
    end
    object qryADV710APP_ACCNT: TStringField
      FieldName = 'APP_ACCNT'
      Size = 35
    end
    object qryADV710SPECIAL_DESC_1: TMemoField
      FieldName = 'SPECIAL_DESC_1'
      BlobType = ftMemo
    end
    object qryADV710PERIOD_DAYS: TIntegerField
      FieldName = 'PERIOD_DAYS'
    end
  end
end
