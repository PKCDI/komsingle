inherited UI_ATTNTC_NEW_frm: TUI_ATTNTC_NEW_frm
  Left = 462
  Top = 158
  BorderWidth = 4
  Caption = '[ATTNTC] '#44540#44144#49436#47448#52392#48512' '#51025#45813#49436
  ClientHeight = 673
  ClientWidth = 958
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 958
    Height = 72
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      958
      72)
    object sLabel7: TsLabel
      Left = 24
      Top = 13
      Width = 159
      Height = 23
      Caption = #44540#44144#49436#47448#52392#48512' '#51025#45813#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 67
      Top = 36
      Width = 73
      Height = 21
      Caption = '(ATTNTC)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 197
      Top = 4
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 884
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 213
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 201
      Top = 41
      Width = 753
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 2
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 3
        Width = 265
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 392
        Top = 3
        Width = 77
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 600
        Top = 3
        Width = 39
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 704
        Top = 3
        Width = 47
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 520
        Top = 3
        Width = 25
        Height = 23
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 315
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 313
      Height = 543
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 189
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 90
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 313
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 230
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 57
        Top = 29
        Width = 171
        Height = 23
        TabOrder = 0
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 57
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 150
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 2
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 241
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 315
    Top = 72
    Width = 643
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 1
      Width = 641
      Height = 599
      ActivePage = sTabSheet1
      Align = alClient
      TabHeight = 26
      TabOrder = 1
      TabPadding = 15
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 633
          Height = 563
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          DesignSize = (
            633
            563)
          object Shape1: TShape
            Left = 17
            Top = 99
            Width = 600
            Height = 1
            Anchors = [akLeft, akTop, akRight]
            Brush.Color = 14540252
            Pen.Color = 14540252
          end
          object Shape2: TShape
            Left = 17
            Top = 308
            Width = 600
            Height = 1
            Anchors = [akLeft, akTop, akRight]
            Brush.Color = 14540252
            Pen.Color = 14540252
          end
          object edt_DOC_NO: TsEdit
            Left = 184
            Top = 40
            Width = 265
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 0
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#51088#47928#49436#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_ATT_DOCNO: TsEdit
            Left = 184
            Top = 64
            Width = 265
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44540#44144#49436#47448' '#52392#48512#49436#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_DOC_CD: TsEdit
            Left = 184
            Top = 16
            Width = 41
            Height = 23
            Color = clWhite
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            MaxLength = 35
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #47928#49436#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DOC_CDNM: TsEdit
            Left = 226
            Top = 16
            Width = 223
            Height = 23
            Color = clBtnFace
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_EX_NAME1: TsEdit
            Left = 80
            Top = 108
            Width = 233
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #48156#49888#51064#49345#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_EX_NAME2: TsEdit
            Left = 80
            Top = 132
            Width = 233
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49885#48324#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_EX_NAME3: TsEdit
            Left = 80
            Top = 156
            Width = 233
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_EX_ELEC: TsEdit
            Left = 80
            Top = 180
            Width = 233
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49324#50629#51088#48264#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_SR_NAME1: TsEdit
            Left = 384
            Top = 108
            Width = 233
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#49888#51064#49345#54840
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_SR_NAME2: TsEdit
            Left = 384
            Top = 132
            Width = 233
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49885#48324#51088
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_SR_NAME3: TsEdit
            Left = 384
            Top = 156
            Width = 233
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51008#54665#53076#46300
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object memo_FTX_DOC1: TsMemo
            Left = 80
            Top = 211
            Width = 537
            Height = 89
            Enabled = False
            TabOrder = 11
            BoundLabel.Active = True
            BoundLabel.Caption = #48708#44256
          end
          object sDBGrid2: TsDBGrid
            Left = 17
            Top = 344
            Width = 598
            Height = 81
            Color = clWhite
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
            ParentFont = False
            ReadOnly = True
            TabOrder = 12
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'SEQ'
                Title.Alignment = taCenter
                Title.Caption = #49692#48264
                Width = 38
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ATT_CK'
                Title.Alignment = taCenter
                Title.Caption = #52392#48512
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ATT_NO'
                Title.Alignment = taCenter
                Title.Caption = #49436#47448#47928#49436#48264#54840
                Width = 161
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ATT_CD'
                Title.Alignment = taCenter
                Title.Caption = #53076#46300
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ATT_CDNM'
                Title.Caption = #44540#44144#49436#47448#47749
                Width = 285
                Visible = True
              end>
          end
          object sPanel8: TsPanel
            Left = 17
            Top = 319
            Width = 118
            Height = 24
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #44592#53440#51221#48372
            Color = 16765090
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 13
          end
          object sMemo1: TsMemo
            Left = 17
            Top = 445
            Width = 598
            Height = 73
            Enabled = False
            TabOrder = 14
            BoundLabel.Active = True
            BoundLabel.Caption = #44592#53440#51221#48372
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclTopLeft
            BoundLabel.ParentFont = False
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        TabVisible = False
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 633
          Height = 531
          Align = alClient
          Color = clGray
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = 12582911
              Expanded = False
              FieldName = 'MAINT_NO'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_SQ1'
              Title.Alignment = taCenter
              Title.Caption = #50976#54952#44592#44036
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_DATE'
              Title.Alignment = taCenter
              Title.Caption = #48156#54665#51068#51088
              Width = 82
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_NO'
              Title.Alignment = taCenter
              Title.Caption = #48156#54665#48264#54840
              Width = 150
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'UD_NAME1'
              Title.Alignment = taCenter
              Title.Caption = #49688#50836#51088
              Width = 150
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'TQTY'
              Title.Alignment = taCenter
              Title.Caption = #52509#49688#47049
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'TQTYCUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 50
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'TAMT'
              Title.Alignment = taCenter
              Title.Caption = #52509#44552#50529
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'TAMTCUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 50
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'USER_ID'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51088
              Width = 50
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 633
          Height = 32
          Align = alTop
          
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 0
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 1
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
          end
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 48
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20181130'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, ATT_DOCNO, ' +
        'DOC_CD, NAME as DOC_CDNAME, DOC_NO, CHK1, CHK2, CHK3, RECV_CUSTC' +
        'D, EX_NAME1, EX_NAME2, EX_NAME3, EX_ELEC, SR_NAME1, SR_NAME2, SR' +
        '_NAME3, SR_ELEC, FTX_DOC1, FTX_DOC2'
      'FROM ATTNTC_H '
      
        'LEFT JOIN (SELECT Prefix, CODE, NAME FROM CODE2NDD WHERE prefix ' +
        '= '#39'ATTNTC_DOC'#39') DOC ON ATTNTC_H.DOC_CD = DOC.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 48
    Top = 192
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 2
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 2
    end
    object qryListATT_DOCNO: TStringField
      FieldName = 'ATT_DOCNO'
      Size = 35
    end
    object qryListDOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object qryListDOC_CDNAME: TStringField
      FieldName = 'DOC_CDNAME'
      Size = 100
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListRECV_CUSTCD: TStringField
      FieldName = 'RECV_CUSTCD'
      Size = 35
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ELEC: TStringField
      FieldName = 'EX_ELEC'
      Size = 17
    end
    object qryListSR_NAME1: TStringField
      FieldName = 'SR_NAME1'
      Size = 35
    end
    object qryListSR_NAME2: TStringField
      FieldName = 'SR_NAME2'
      Size = 35
    end
    object qryListSR_NAME3: TStringField
      FieldName = 'SR_NAME3'
      Size = 35
    end
    object qryListSR_ELEC: TStringField
      FieldName = 'SR_ELEC'
      Size = 17
    end
    object qryListFTX_DOC1: TMemoField
      FieldName = 'FTX_DOC1'
      BlobType = ftMemo
    end
    object qryListFTX_DOC2: TMemoField
      FieldName = 'FTX_DOC2'
      BlobType = ftMemo
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 192
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryDetailAfterOpen
    AfterScroll = qryDetailAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, ATT_CK, ATT_NO, ATT_CD, NAME as ATT_CDNM, ATT_' +
        'ELEC, MDOC1, MDOC2, MDOC3, MDOC4, MDOC5, DDOC1, DDOC2, DDOC3, DD' +
        'OC4, DDOC5'
      
        'FROM ATTNTC_D LEFT JOIN (SELECT Prefix, CODE, NAME FROM CODE2NDD' +
        ' WHERE prefix = '#39'ATTNTC_CDD'#39') DOC ON ATTNTC_D.ATT_CD = DOC.CODE'
      'WHERE KEYY = :KEYY')
    Left = 48
    Top = 232
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailATT_CK: TStringField
      FieldName = 'ATT_CK'
      Size = 1
    end
    object qryDetailATT_NO: TStringField
      FieldName = 'ATT_NO'
      Size = 35
    end
    object qryDetailATT_CD: TStringField
      FieldName = 'ATT_CD'
      Size = 3
    end
    object qryDetailATT_CDNM: TStringField
      FieldName = 'ATT_CDNM'
      Size = 100
    end
    object qryDetailATT_ELEC: TStringField
      FieldName = 'ATT_ELEC'
      Size = 35
    end
    object qryDetailMDOC1: TStringField
      FieldName = 'MDOC1'
      Size = 70
    end
    object qryDetailMDOC2: TStringField
      FieldName = 'MDOC2'
      Size = 70
    end
    object qryDetailMDOC3: TStringField
      FieldName = 'MDOC3'
      Size = 70
    end
    object qryDetailMDOC4: TStringField
      FieldName = 'MDOC4'
      Size = 70
    end
    object qryDetailMDOC5: TStringField
      FieldName = 'MDOC5'
      Size = 70
    end
    object qryDetailDDOC1: TStringField
      FieldName = 'DDOC1'
      Size = 70
    end
    object qryDetailDDOC2: TStringField
      FieldName = 'DDOC2'
      Size = 70
    end
    object qryDetailDDOC3: TStringField
      FieldName = 'DDOC3'
      Size = 70
    end
    object qryDetailDDOC4: TStringField
      FieldName = 'DDOC4'
      Size = 70
    end
    object qryDetailDDOC5: TStringField
      FieldName = 'DDOC5'
      Size = 70
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 80
    Top = 232
  end
end
