inherited UI_DEBADV_NEW_frm: TUI_DEBADV_NEW_frm
  Left = 515
  Top = 219
  BorderWidth = 4
  Caption = '[DEBADV] '#52636#44552#53685#51648#49436
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  Scaled = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 72
    Align = alTop
    
    TabOrder = 0
    object sSpeedButton4: TsSpeedButton
      Left = 1025
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 103
      Top = 13
      Width = 85
      Height = 23
      Caption = #52636#44552#53685#51648#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton6: TsSpeedButton
      Left = 433
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton8: TsSpeedButton
      Left = 283
      Top = 3
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
    end
    object sLabel5: TsLabel
      Left = 107
      Top = 37
      Width = 77
      Height = 21
      Caption = '(DEBADV)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object btnExit: TsButton
      Left = 1040
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 295
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 364
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 289
      Top = 40
      Width = 820
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 3
      object edt_MAINT_NO: TsEdit
        Left = 60
        Top = 4
        Width = 213
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 335
        Top = 4
        Width = 89
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 668
        Top = 4
        Width = 39
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 1
        TabOrder = 2
        TabStop = False
        Text = '9: Original'
        Items.Strings = (
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 768
        Top = 4
        Width = 47
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'NA: No acknowledgement needed')
      end
      object edt_userno: TsEdit
        Left = 468
        Top = 4
        Width = 29
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
      object comBank: TsComboBox
        Left = 954
        Top = 28
        Width = 155
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = -1
        TabOrder = 5
        Visible = False
      end
    end
    object QRShape1: TQRShape
      Left = 289
      Top = 40
      Width = 820
      Height = 1
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        2.645833333333330000
        764.645833333333000000
        105.833333333333000000
        2169.583333333330000000)
      Brush.Color = clBtnFace
      Pen.Color = clBtnFace
      Shape = qrsRectangle
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 350
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 348
      Height = 543
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = DrawColumnCell
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 222
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 92
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 348
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 258
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 77
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 77
        Top = 4
        Width = 74
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 170
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 273
        Top = 4
        Width = 66
        Height = 45
        Caption = #51312#54924
        TabOrder = 3
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPageControl1: TsPageControl [2]
    Left = 350
    Top = 72
    Width = 764
    Height = 601
    ActivePage = sTabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 26
    TabOrder = 2
    OnChange = sPageControl1Change
    TabPadding = 15
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sPanel7: TsPanel
        Left = 0
        Top = 0
        Width = 756
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object Shape1: TShape
          Left = 9
          Top = 42
          Width = 737
          Height = 1
          Brush.Color = clGray
          Pen.Color = clGray
        end
        object msk_ADV_DATE: TsMaskEdit
          Left = 88
          Top = 12
          Width = 86
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 0
          Color = 12582911
          BoundLabel.Active = True
          BoundLabel.Caption = #53685#51648#51068#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object msk_WIT_DT: TsMaskEdit
          Left = 272
          Top = 12
          Width = 88
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 1
          Color = 12582911
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#52636#44552#51068#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object msk_DEP_DT: TsMaskEdit
          Left = 456
          Top = 12
          Width = 90
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 2
          Color = 12582911
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#51077#44552#51068#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object msk_EXT_DT: TsMaskEdit
          Left = 640
          Top = 12
          Width = 92
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 3
          Color = 12582911
          BoundLabel.Active = True
          BoundLabel.Caption = #51648#44553#51060#54665#51068#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sPanel2: TsPanel
          Left = 23
          Top = 62
          Width = 122
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #44592#53440#52280#51312#48264#54840
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object edt_ETC_REFNO: TsEdit
          Left = 146
          Top = 62
          Width = 221
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 5
          SkinData.CustomColor = True
          BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel8: TsPanel
          Left = 23
          Top = 89
          Width = 122
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #52636#44552#49345#49464#45236#50669
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object sPanel9: TsPanel
          Left = 23
          Top = 204
          Width = 122
          Height = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #44592#53440#51221#48372#13#10'(Ordered bank)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
        object memo_PAY_DTL: TsMemo
          Tag = 122
          Left = 146
          Top = 89
          Width = 605
          Height = 112
          Hint = 'DesGood'
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 25600
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 8
          WordWrap = False
          BoundLabel.Caption = #51077#44552#49345#49464#45236#50669
          BoundLabel.Layout = sclLeftTop
          SkinData.CustomFont = True
        end
        object ETC_INFO1: TsMemo
          Tag = 122
          Left = 146
          Top = 204
          Width = 605
          Height = 117
          Hint = 'DesGood'
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 25600
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 9
          WordWrap = False
          BoundLabel.Caption = #44592#53440#51221#48372'1'
          BoundLabel.Layout = sclLeftTop
          SkinData.CustomFont = True
        end
        object ETC_INFO2: TsMemo
          Tag = 122
          Left = 146
          Top = 324
          Width = 605
          Height = 117
          Hint = 'DesGood'
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 25600
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 10
          WordWrap = False
          BoundLabel.Caption = #44592#53440#51221#48372'1'
          BoundLabel.Layout = sclLeftTop
          SkinData.CustomFont = True
        end
        object sPanel1: TsPanel
          Left = 23
          Top = 324
          Width = 122
          Height = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #44592#53440#51221#48372#13#10'(Ordering Customer)'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 11
        end
        object edt_ETC_REFCD: TsEdit
          Left = 368
          Top = 62
          Width = 39
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 2
          TabOrder = 12
          SkinData.CustomColor = True
          BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sEdit45: TsEdit
          Left = 408
          Top = 62
          Width = 201
          Height = 23
          Color = clBtnFace
          Enabled = False
          TabOrder = 13
          SkinData.CustomColor = True
          BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = 'PAGE2'
      object sPanel11: TsPanel
        Left = 0
        Top = 0
        Width = 756
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sPanel3: TsPanel
          Left = 23
          Top = 22
          Width = 272
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #50808#54868#44228#51340' '#52636#44552#44552#50529
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edt60_AMT_UNIT: TsEdit
          Tag = 1016
          Left = 107
          Top = 47
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 1
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52636#44552#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object cur60_AMT: TsCurrencyEdit
          Left = 142
          Top = 47
          Width = 153
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 2
          Color = clWhite
          SkinData.CustomColor = True
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object edt60_BASE_UNIT: TsEdit
          Tag = 1016
          Left = 107
          Top = 71
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 3
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44592#51456#53685#54868
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt60_DEST_UNIT: TsEdit
          Tag = 1016
          Left = 203
          Top = 71
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 4
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #47785#51201#53685#54868
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object cur60_RATE: TsCurrencyEdit
          Left = 107
          Top = 95
          Width = 188
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 5
          BoundLabel.Active = True
          BoundLabel.Caption = #54872#50984
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object msk60_RATE_STDT: TsMaskEdit
          Left = 107
          Top = 119
          Width = 93
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 6
          BoundLabel.Active = True
          BoundLabel.Caption = #54872#50984#51201#50857#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;0'
        end
        object msk60_RATE_EDDT: TsMaskEdit
          Left = 201
          Top = 119
          Width = 94
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 7
          CheckOnExit = True
          EditMask = '9999-99-99;0'
        end
        object edt60_D1_RFF_CD: TsEdit
          Left = 387
          Top = 47
          Width = 39
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 8
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52280#51312#48264#54840#53076#46300
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt60_WIT_ACNT: TsEdit
          Left = 387
          Top = 95
          Width = 230
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 9
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52636#44552#44228#51340#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sEdit6: TsEdit
          Left = 427
          Top = 47
          Width = 190
          Height = 23
          Color = clBtnFace
          Enabled = False
          TabOrder = 10
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt60_D1_RFF_NO: TsEdit
          Left = 387
          Top = 71
          Width = 230
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 11
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52280#51312#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel10: TsPanel
          Left = 23
          Top = 158
          Width = 272
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #50896#54868#44228#51340' '#52636#44552#44552#50529
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 12
        end
        object edt9_AMT_UNIT: TsEdit
          Tag = 1016
          Left = 107
          Top = 183
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 13
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52636#44552#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object cur9_AMT: TsCurrencyEdit
          Left = 142
          Top = 183
          Width = 153
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 14
          Color = clWhite
          SkinData.CustomColor = True
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object edt9_BASE_UNIT: TsEdit
          Tag = 1016
          Left = 107
          Top = 207
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 15
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44592#51456#53685#54868
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt9_DEST_UNIT: TsEdit
          Tag = 1016
          Left = 203
          Top = 207
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 16
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #47785#51201#53685#54868
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object cur9_RATE: TsCurrencyEdit
          Left = 107
          Top = 231
          Width = 188
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 17
          BoundLabel.Active = True
          BoundLabel.Caption = #54872#50984
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object msk9_RATE_STDT: TsMaskEdit
          Left = 107
          Top = 255
          Width = 93
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 18
          BoundLabel.Active = True
          BoundLabel.Caption = #54872#50984#51201#50857#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;0'
        end
        object msk9_RATE_EDDT: TsMaskEdit
          Left = 201
          Top = 255
          Width = 94
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 19
          CheckOnExit = True
          EditMask = '9999-99-99;0'
        end
        object edt9_D1_RFF_CD: TsEdit
          Left = 387
          Top = 183
          Width = 39
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 20
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52280#51312#48264#54840#53076#46300
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt9_WIT_ACNT: TsEdit
          Left = 387
          Top = 231
          Width = 230
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 21
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52636#44552#44228#51340#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sEdit13: TsEdit
          Left = 427
          Top = 183
          Width = 190
          Height = 23
          Color = clBtnFace
          Enabled = False
          TabOrder = 22
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt9_D1_RFF_NO: TsEdit
          Left = 387
          Top = 207
          Width = 230
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 23
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52280#51312#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel12: TsPanel
          Left = 23
          Top = 294
          Width = 272
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51648#44553#44552#50529
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
        end
        object edt98_AMT_UNIT: TsEdit
          Tag = 1016
          Left = 107
          Top = 319
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 25
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52636#44552#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object cur98_AMT: TsCurrencyEdit
          Left = 142
          Top = 319
          Width = 153
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 26
          Color = clWhite
          SkinData.CustomColor = True
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object sPanel13: TsPanel
          Left = 23
          Top = 358
          Width = 272
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #54872#51204#44552#50529
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
        end
        object edt36_AMT_UNIT: TsEdit
          Tag = 1016
          Left = 107
          Top = 383
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 28
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52636#44552#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object cur36_AMT: TsCurrencyEdit
          Left = 142
          Top = 383
          Width = 153
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 29
          Color = clWhite
          SkinData.CustomColor = True
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object edt36_BASE_UNIT: TsEdit
          Tag = 1016
          Left = 107
          Top = 407
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 30
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44592#51456#53685#54868
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt36_DEST_UNIT: TsEdit
          Tag = 1016
          Left = 203
          Top = 407
          Width = 34
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 31
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #47785#51201#53685#54868
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object cur36_RATE: TsCurrencyEdit
          Left = 107
          Top = 431
          Width = 188
          Height = 23
          AutoSize = False
          
          Enabled = False
          TabOrder = 32
          BoundLabel.Active = True
          BoundLabel.Caption = #54872#50984
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object msk36_RATE_STDT: TsMaskEdit
          Left = 107
          Top = 455
          Width = 93
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 33
          BoundLabel.Active = True
          BoundLabel.Caption = #54872#50984#51201#50857#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;0'
        end
        object msk36_RATE_EDDT: TsMaskEdit
          Left = 201
          Top = 455
          Width = 94
          Height = 23
          AutoSize = False
          
          Enabled = False
          MaxLength = 10
          TabOrder = 34
          CheckOnExit = True
          EditMask = '9999-99-99;0'
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = 'PAGE3'
      object sPanel14: TsPanel
        Left = 0
        Top = 0
        Width = 756
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sPanel15: TsPanel
          Left = 16
          Top = 22
          Width = 177
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51648#44553#51032#47280#51064#51008#54665
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object edt_CLT_BKCD: TsEdit
          Left = 91
          Top = 46
          Width = 102
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 11
          TabOrder = 1
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CLT_MAN: TsEdit
          Left = 91
          Top = 70
          Width = 38
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 2
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#44592#44288
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CLT_BKNM: TsEdit
          Left = 91
          Top = 94
          Width = 558
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 3
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CLT_BRNM: TsEdit
          Left = 91
          Top = 118
          Width = 558
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 4
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51648#51216#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel16: TsPanel
          Left = 16
          Top = 150
          Width = 177
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49688#51061#51088#51008#54665
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object edt_BEN_ACNTNM: TsEdit
          Left = 291
          Top = 246
          Width = 286
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 6
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#51452
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_ACNT: TsEdit
          Left = 91
          Top = 246
          Width = 150
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 17
          TabOrder = 7
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44228#51340#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_UNIT: TsEdit
          Left = 91
          Top = 270
          Width = 38
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 8
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #53685#54868#53076#46300
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_BKCD: TsEdit
          Left = 91
          Top = 174
          Width = 102
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 11
          TabOrder = 9
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#53076#46300
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_MAN: TsEdit
          Left = 91
          Top = 294
          Width = 38
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 3
          TabOrder = 10
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#44592#44288
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_BKNM: TsEdit
          Left = 91
          Top = 198
          Width = 558
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 11
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51008#54665#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_BRNM: TsEdit
          Left = 91
          Top = 222
          Width = 558
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 12
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51648#51216#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel17: TsPanel
          Left = 15
          Top = 326
          Width = 113
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #49688#51061#51088'('#49345#54840'/'#51452#49548')'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 13
        end
        object edt_BEN_NM1: TsEdit
          Left = 129
          Top = 326
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 14
          SkinData.CustomColor = True
          BoundLabel.Caption = #49688#51061#51088#51008#54665
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_NM2: TsEdit
          Left = 129
          Top = 350
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 15
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_NM3: TsEdit
          Left = 129
          Top = 374
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 16
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_NM4: TsEdit
          Left = 129
          Top = 398
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 17
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_NM5: TsEdit
          Left = 129
          Top = 422
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 18
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel18: TsPanel
          Left = 375
          Top = 326
          Width = 128
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51648#44553#51032#47280#51064'('#49345#54840'/'#51452#49548')'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 19
        end
        object edt_CLT_NM1: TsEdit
          Left = 505
          Top = 326
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 20
          SkinData.CustomColor = True
          BoundLabel.Caption = #49688#51061#51088#51008#54665
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CLT_NM2: TsEdit
          Left = 505
          Top = 350
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 21
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CLT_NM3: TsEdit
          Left = 505
          Top = 374
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 22
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CLT_NM4: TsEdit
          Left = 505
          Top = 398
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 23
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CLT_NM5: TsEdit
          Left = 505
          Top = 422
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 24
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_BEN_NM6: TsEdit
          Left = 129
          Top = 446
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 25
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_CLT_NM6: TsEdit
          Left = 505
          Top = 446
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 26
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel23: TsPanel
          Left = 15
          Top = 478
          Width = 113
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #51204#51088#49436#47749
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
        end
        object edt_SIGN1: TsEdit
          Left = 129
          Top = 478
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 28
          SkinData.CustomColor = True
          BoundLabel.Caption = #49688#51061#51088#51008#54665
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_SIGN2: TsEdit
          Left = 129
          Top = 502
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 29
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_SIGN3: TsEdit
          Left = 129
          Top = 526
          Width = 234
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 70
          TabOrder = 30
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
      end
    end
    object sTabSheet6: TsTabSheet
      Caption = 'PAGE4'
      object sPanel30: TsPanel
        Left = 0
        Top = 0
        Width = 756
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sDBGrid4: TsDBGrid
          Left = 24
          Top = 38
          Width = 727
          Height = 121
          Color = clWhite
          Constraints.MaxWidth = 727
          Constraints.MinWidth = 727
          Ctl3D = True
          DataSource = dsFC1
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomFont = True
          Columns = <
            item
              Alignment = taCenter
              Color = clBtnFace
              Expanded = False
              FieldName = 'FC1CD'
              Title.Alignment = taCenter
              Title.Caption = #53076#46300
              Width = 44
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FC1NM'
              Title.Alignment = taCenter
              Title.Caption = #49688#49688#47308#48512#45812#51088
              Width = 280
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FC1AMT1'
              Title.Alignment = taCenter
              Title.Caption = #48512#44032#49688#49688#47308#54633#44228
              Width = 137
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FC1AMT1C'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868#45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FC1AMT2'
              Title.Alignment = taCenter
              Title.Caption = #48512#44032#49688#49688#47308#54633#44228
              Width = 137
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FC1AMT2C'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868#45800#50948
              Width = 52
              Visible = True
            end>
        end
        object sPanel21: TsPanel
          Left = 24
          Top = 14
          Width = 154
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #48512#44032#49688#49688#47308' '#48512#45812#51088
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object sPanel22: TsPanel
          Left = 24
          Top = 166
          Width = 154
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #48512#44032#49688#49688#47308' '#50976#54805
          Color = 9549311
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object sDBGrid5: TsDBGrid
          Left = 24
          Top = 190
          Width = 727
          Height = 331
          Color = clWhite
          Constraints.MaxWidth = 727
          Constraints.MinWidth = 727
          Ctl3D = True
          DataSource = dsFC2
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomFont = True
          Columns = <
            item
              Alignment = taCenter
              Color = clBtnFace
              Expanded = False
              FieldName = 'FC1CH'
              Title.Alignment = taCenter
              Title.Caption = #50976#54805
              Width = 36
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FC1CHNM'
              Title.Alignment = taCenter
              Title.Caption = #50976#54805#47749
              Width = 188
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FC1SA1'
              Title.Alignment = taCenter
              Title.Caption = #49688#49688#47308#44552#50529
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FC1SA1C'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FC1SA2'
              Title.Alignment = taCenter
              Title.Caption = #54872#51204#44552#50529
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FC1SA2C'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 36
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FC1RATE1'
              Title.Alignment = taCenter
              Title.Caption = #44592#51456#53685#54868
              Width = 52
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'FC1RATE2'
              Title.Alignment = taCenter
              Title.Caption = #47785#51201#53685#54868
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FC1RATE'
              Title.Alignment = taCenter
              Title.Caption = #54872#50984
              Width = 99
              Visible = True
            end>
        end
      end
    end
    object sTabSheet5: TsTabSheet
      Caption = 'PAGE5'
      object sPanel34: TsPanel
        Left = 0
        Top = 0
        Width = 756
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sDBGrid2: TsDBGrid
          Left = 15
          Top = 10
          Width = 727
          Height = 543
          Color = clWhite
          Constraints.MaxWidth = 727
          Constraints.MinWidth = 727
          Ctl3D = True
          DataSource = dsDOCLIST
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomFont = True
          Columns = <
            item
              Alignment = taCenter
              Color = clBtnFace
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RELIST_NO'
              Title.Alignment = taCenter
              Title.Caption = #49436#47448#48264#54840
              Width = 164
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ViewType'
              Title.Alignment = taCenter
              Title.Caption = #44288#47144#49436#47448
              Width = 224
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RELIST_ADDNO'
              Title.Alignment = taCenter
              Title.Caption = #48512#44032#49436#47448#48264#54840
              Width = 189
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'RELIST_APPDT'
              Title.Alignment = taCenter
              Title.Caption = #47928#49436#48156#54665#51068
              Width = 88
              Visible = True
            end>
        end
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sDBGrid3: TsDBGrid
        Left = 0
        Top = 32
        Width = 756
        Height = 533
        Align = alClient
        Color = clGray
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        SkinData.CustomColor = True
        Columns = <
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 36
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 60
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Caption = #44288#47532#48264#54840
            Width = 200
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 172
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 164
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 82
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'LOC_AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529
            Width = 97
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'LOC_AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 38
            Visible = True
          end>
      end
      object sPanel24: TsPanel
        Left = 0
        Top = 0
        Width = 756
        Height = 32
        Align = alTop
        
        TabOrder = 1
        object sSpeedButton12: TsSpeedButton
          Left = 230
          Top = 4
          Width = 11
          Height = 23
          ButtonStyle = tbsDivider
        end
        object sMaskEdit3: TsMaskEdit
          Tag = -1
          Left = 57
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 0
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sMaskEdit4: TsMaskEdit
          Tag = -1
          Left = 150
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 1
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sBitBtn5: TsBitBtn
          Tag = 1
          Left = 469
          Top = 5
          Width = 66
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
        end
        object sEdit1: TsEdit
          Tag = -1
          Left = 297
          Top = 5
          Width = 171
          Height = 23
          TabOrder = 3
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 88
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20191231'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, ADV_DT, WIT' +
        '_DT, DEP_DT, EXT_DT, ETC_REFNO, ETC_REFCD, CHK1, CHK2, CHK3,'
      
        '       PAY_DTL, ETC_INFO1, ETC_INFO2, CLT_BKCD, CLT_MAN, CLT_BKN' +
        'M, CLT_BRNM, '
      
        '       BEN_BKCD, BEN_BKNM, BEN_BRNM, BEN_ACNT, BEN_ACNTNM, BEN_U' +
        'NIT, BEN_MAN, '
      '       BEN_NM1, BEN_NM2, BEN_NM3, BEN_NM4, BEN_NM5, BEN_NM6,'
      '       CLT_NM1, CLT_NM2, CLT_NM3, CLT_NM4, CLT_NM5, CLT_NM6,'
      '       SIGN1, SIGN2, SIGN3'
      'FROM [DEBADV_HEADER]'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 48
    Top = 152
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListADV_DT: TStringField
      FieldName = 'ADV_DT'
      Size = 8
    end
    object qryListWIT_DT: TStringField
      FieldName = 'WIT_DT'
      Size = 8
    end
    object qryListDEP_DT: TStringField
      FieldName = 'DEP_DT'
      Size = 8
    end
    object qryListEXT_DT: TStringField
      FieldName = 'EXT_DT'
      Size = 8
    end
    object qryListETC_REFCD: TStringField
      FieldName = 'ETC_REFCD'
      Size = 2
    end
    object qryListETC_REFNO: TStringField
      FieldName = 'ETC_REFNO'
      Size = 35
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPAY_DTL: TStringField
      FieldName = 'PAY_DTL'
      Size = 350
    end
    object qryListETC_INFO1: TStringField
      FieldName = 'ETC_INFO1'
      Size = 350
    end
    object qryListETC_INFO2: TStringField
      FieldName = 'ETC_INFO2'
      Size = 350
    end
    object qryListCLT_BKCD: TStringField
      FieldName = 'CLT_BKCD'
      Size = 11
    end
    object qryListCLT_MAN: TStringField
      FieldName = 'CLT_MAN'
      Size = 3
    end
    object qryListCLT_BKNM: TStringField
      FieldName = 'CLT_BKNM'
      Size = 70
    end
    object qryListCLT_BRNM: TStringField
      FieldName = 'CLT_BRNM'
      Size = 70
    end
    object qryListBEN_BKCD: TStringField
      FieldName = 'BEN_BKCD'
      Size = 11
    end
    object qryListBEN_BKNM: TStringField
      FieldName = 'BEN_BKNM'
      Size = 70
    end
    object qryListBEN_BRNM: TStringField
      FieldName = 'BEN_BRNM'
      Size = 70
    end
    object qryListBEN_ACNT: TStringField
      FieldName = 'BEN_ACNT'
      Size = 17
    end
    object qryListBEN_ACNTNM: TStringField
      FieldName = 'BEN_ACNTNM'
      Size = 35
    end
    object qryListBEN_UNIT: TStringField
      FieldName = 'BEN_UNIT'
      Size = 3
    end
    object qryListBEN_MAN: TStringField
      FieldName = 'BEN_MAN'
      Size = 3
    end
    object qryListBEN_NM1: TStringField
      FieldName = 'BEN_NM1'
      Size = 35
    end
    object qryListBEN_NM2: TStringField
      FieldName = 'BEN_NM2'
      Size = 35
    end
    object qryListBEN_NM3: TStringField
      FieldName = 'BEN_NM3'
      Size = 35
    end
    object qryListBEN_NM4: TStringField
      FieldName = 'BEN_NM4'
      Size = 35
    end
    object qryListBEN_NM5: TStringField
      FieldName = 'BEN_NM5'
      Size = 35
    end
    object qryListBEN_NM6: TStringField
      FieldName = 'BEN_NM6'
      Size = 35
    end
    object qryListCLT_NM1: TStringField
      FieldName = 'CLT_NM1'
      Size = 35
    end
    object qryListCLT_NM2: TStringField
      FieldName = 'CLT_NM2'
      Size = 35
    end
    object qryListCLT_NM3: TStringField
      FieldName = 'CLT_NM3'
      Size = 35
    end
    object qryListCLT_NM4: TStringField
      FieldName = 'CLT_NM4'
      Size = 35
    end
    object qryListCLT_NM5: TStringField
      FieldName = 'CLT_NM5'
      Size = 35
    end
    object qryListCLT_NM6: TStringField
      FieldName = 'CLT_NM6'
      Size = 35
    end
    object qryListSIGN1: TStringField
      FieldName = 'SIGN1'
      Size = 35
    end
    object qryListSIGN2: TStringField
      FieldName = 'SIGN2'
      Size = 35
    end
    object qryListSIGN3: TStringField
      FieldName = 'SIGN3'
      Size = 35
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 152
  end
  object qryDTL: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT CD5025, AMT_UNIT, AMT, BASE_UNIT, DEST_UNIT, RATE, RATE_S' +
        'TDT, RATE_EDDT, D1_RFF_CD, NAME as D1_RFF_CDNM, D1_RFF_NO, WIT_A' +
        'CNT'
      
        'FROM [DEBADV_DTL] LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHE' +
        'RE Prefix = '#39'1153DEBADV'#39') CD ON D1_RFF_CD = CODE'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 48
    Top = 184
    object qryDTLCD5025: TStringField
      FieldName = 'CD5025'
      Size = 2
    end
    object qryDTLAMT_UNIT: TStringField
      FieldName = 'AMT_UNIT'
      Size = 3
    end
    object qryDTLAMT: TBCDField
      FieldName = 'AMT'
      Precision = 18
    end
    object qryDTLBASE_UNIT: TStringField
      FieldName = 'BASE_UNIT'
      Size = 3
    end
    object qryDTLDEST_UNIT: TStringField
      FieldName = 'DEST_UNIT'
      Size = 3
    end
    object qryDTLRATE: TBCDField
      FieldName = 'RATE'
      Precision = 10
      Size = 5
    end
    object qryDTLRATE_STDT: TDateField
      FieldName = 'RATE_STDT'
    end
    object qryDTLRATE_EDDT: TDateField
      FieldName = 'RATE_EDDT'
    end
    object qryDTLD1_RFF_CD: TStringField
      FieldName = 'D1_RFF_CD'
      Size = 3
    end
    object qryDTLD1_RFF_NO: TStringField
      FieldName = 'D1_RFF_NO'
      Size = 35
    end
    object qryDTLWIT_ACNT: TStringField
      FieldName = 'WIT_ACNT'
      Size = 35
    end
    object qryDTLD1_RFF_CDNM: TStringField
      FieldName = 'D1_RFF_CDNM'
      Size = 100
    end
  end
  object dsDTL: TDataSource
    DataSet = qryDTL
    Left = 80
    Top = 184
  end
  object qryFC1: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryFC1AfterOpen
    AfterScroll = qryFC1AfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'DEB550LD2QN1808003880001-FN'
      end>
    SQL.Strings = (
      
        'SELECT FC1CD, NAME as FC1NM, FC1AMT1, FC1AMT1C, FC1AMT2, FC1AMT2' +
        'C'
      
        'FROM DEBADV_FC1 LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE' +
        ' prefix = '#39#49688#49688#47308#48512#45812#39') CD ON FC1CD = CD.CODE'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 48
    Top = 216
    object qryFC1FC1CD: TStringField
      FieldName = 'FC1CD'
      Size = 3
    end
    object qryFC1FC1NM: TStringField
      FieldName = 'FC1NM'
      Size = 100
    end
    object qryFC1FC1AMT1: TBCDField
      FieldName = 'FC1AMT1'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryFC1FC1AMT1C: TStringField
      FieldName = 'FC1AMT1C'
      Size = 3
    end
    object qryFC1FC1AMT2: TBCDField
      FieldName = 'FC1AMT2'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryFC1FC1AMT2C: TStringField
      FieldName = 'FC1AMT2C'
      Size = 3
    end
  end
  object dsFC1: TDataSource
    DataSet = qryFC1
    Left = 80
    Top = 216
  end
  object qryFC2: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'FC1CD'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT FC1SEQ, FC1CH, NAME as FC1CHNM, FC1SA1, FC1SA1C, FC1SA2, ' +
        'FC1SA2C, FC1RATE1, FC1RATE2, FC1RATE, FC1RD1, FC1RD2'
      
        'FROM DEBADV_FC2 LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE' +
        ' prefix = '#39'5189DEBADV'#39') CD ON FC1CH = CD.CODE'
      'WHERE MAINT_NO = :MAINT_NO'
      'AND FC1CD = :FC1CD')
    Left = 48
    Top = 248
    object qryFC2FC1SEQ: TIntegerField
      FieldName = 'FC1SEQ'
    end
    object qryFC2FC1CH: TStringField
      FieldName = 'FC1CH'
      Size = 3
    end
    object qryFC2FC1CHNM: TStringField
      FieldName = 'FC1CHNM'
      Size = 100
    end
    object qryFC2FC1SA1: TBCDField
      FieldName = 'FC1SA1'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryFC2FC1SA1C: TStringField
      FieldName = 'FC1SA1C'
      Size = 3
    end
    object qryFC2FC1SA2: TBCDField
      FieldName = 'FC1SA2'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryFC2FC1SA2C: TStringField
      FieldName = 'FC1SA2C'
      Size = 3
    end
    object qryFC2FC1RATE1: TStringField
      FieldName = 'FC1RATE1'
      Size = 3
    end
    object qryFC2FC1RATE2: TStringField
      FieldName = 'FC1RATE2'
      Size = 3
    end
    object qryFC2FC1RATE: TBCDField
      FieldName = 'FC1RATE'
      DisplayFormat = '#,0.#####'
      Precision = 18
    end
    object qryFC2FC1RD1: TStringField
      FieldName = 'FC1RD1'
      Size = 8
    end
    object qryFC2FC1RD2: TStringField
      FieldName = 'FC1RD2'
      Size = 8
    end
  end
  object dsFC2: TDataSource
    DataSet = qryFC2
    Left = 80
    Top = 248
  end
  object qryDOCLIST: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'DEB550LD2QN1808003880001-FN'
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, SEQ, RELIST_TYPE, RTRIM(LTRIM(NAME)) as ViewTyp' +
        'e, RELIST_NO, RELIST_ADDNO, RELIST_APPDT'
      
        'FROM DEBADV_DOCLIST LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD W' +
        'HERE prefix = '#39'1001'#39') CD ON RELIST_TYPE = CD.CODE'
      'WHERE MAINT_NO = :MAINT_NO')
    Left = 48
    Top = 280
    object qryDOCLISTMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryDOCLISTSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDOCLISTRELIST_TYPE: TStringField
      FieldName = 'RELIST_TYPE'
      Size = 3
    end
    object qryDOCLISTRELIST_NO: TStringField
      FieldName = 'RELIST_NO'
      Size = 35
    end
    object qryDOCLISTRELIST_ADDNO: TStringField
      FieldName = 'RELIST_ADDNO'
      Size = 35
    end
    object qryDOCLISTRELIST_APPDT: TStringField
      FieldName = 'RELIST_APPDT'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryDOCLISTViewType: TStringField
      FieldName = 'ViewType'
      ReadOnly = True
      Size = 100
    end
  end
  object dsDOCLIST: TDataSource
    DataSet = qryDOCLIST
    Left = 80
    Top = 280
  end
end
