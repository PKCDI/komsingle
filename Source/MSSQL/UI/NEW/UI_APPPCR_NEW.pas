unit UI_APPPCR_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, QuickRpt, QRCtrls, StdCtrls,
  sComboBox, Mask, sMaskEdit, sEdit, sCheckBox, sButton, sLabel, Buttons,
  sSpeedButton, ExtCtrls, sPanel, sMemo, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, ComCtrls, sPageControl, sBitBtn, Grids, DBGrids, acDBGrid,
  DB, ADODB, StrUtils, TypeDefine, CodeDialogParent, DateUtils, sDialogs, Clipbrd;

type
  TUI_APPPCR_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sLabel5: TsLabel;
    sSpeedButton3: TsSpeedButton;
    btnExit: TsButton;
    Btn_New: TsButton;
    Btn_Modify: TsButton;
    Btn_Del: TsButton;
    btnCopy: TsButton;
    Btn_Print: TsButton;
    Btn_Temp: TsButton;
    Btn_Save: TsButton;
    Btn_Cancel: TsButton;
    Btn_Ready: TsButton;
    btn_Send: TsButton;
    sCheckBox1: TsCheckBox;
    sPanel6: TsPanel;
    edt_MAINT_NO_Header: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    comBank: TsComboBox;
    QRShape1: TQRShape;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sSpeedButton10: TsSpeedButton;
    Edt_ApplicationCode: TsEdit;
    Edt_ApplicationName1: TsEdit;
    Edt_ApplicationAddr1: TsEdit;
    Edt_ApplicationAddr2: TsEdit;
    Edt_ApplicationAddr3: TsEdit;
    Edt_ApplicationSAUPNO: TsEdit;
    sPanel11: TsPanel;
    sPanel8: TsPanel;
    sTabSheetDetail: TsTabSheet;
    sPanel27: TsPanel;
    Edt_ApplicationName2: TsEdit;
    Edt_ApplicationAXNAME1: TsEdit;
    Edt_ApplicationElectronicSign: TsEdit;
    Edt_ApplicationAPPDATE: TsMaskEdit;
    edt_N4025: TsEdit;
    edt_N4025_NM: TsEdit;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    Edt_DocumentsSection: TsEdit;
    Edt_DocumentsSection_NM: TsEdit;
    Edt_BuyConfirmDocuments: TsEdit;
    sLabel6: TsLabel;
    Edt_SupplyCode: TsEdit;
    Edt_SupplySaupNo: TsEdit;
    Edt_SupplyName: TsEdit;
    Edt_SupplyCEO: TsEdit;
    Edt_SupplyEID: TsEdit;
    Edt_SupplyAddr1: TsEdit;
    Edt_SupplyAddr2: TsEdit;
    edt_CHG_G: TsEdit;
    edt_CHG_G_NM: TsEdit;
    sLabel3: TsLabel;
    curr_C2_AMT: TsCurrencyEdit;
    sPanel9: TsPanel;
    curr_C1_AMT: TsCurrencyEdit;
    edt_C1_C: TsEdit;
    sPanel10: TsPanel;
    edt_BK_CD: TsEdit;
    edt_BK_NAME1: TsEdit;
    edt_BK_NAME2: TsEdit;
    sDBGrid2: TsDBGrid;
    sPanel1: TsPanel;
    sPanel12: TsPanel;
    sPanel13: TsPanel;
    Btn_DetailAdd: TsSpeedButton;
    Btn_DetailEdit: TsSpeedButton;
    Btn_DetailDel: TsSpeedButton;
    sSpeedButton14: TsSpeedButton;
    Btn_DetailOk: TsSpeedButton;
    Btn_DetailCancel: TsSpeedButton;
    GoodsExcelBtn: TsSpeedButton;
    GoodsSampleBtn: TsSpeedButton;
    sSpeedButton13: TsSpeedButton;
    sPanel14: TsPanel;
    msk_HS_NO: TsMaskEdit;
    msk_APP_DATE: TsMaskEdit;
    memo_NAME1: TsMemo;
    memo_Size: TsMemo;
    memo_Remark: TsMemo;
    edt_NAMESS: TsEdit;
    sPanel15: TsPanel;
    edt_QTYC: TsEdit;
    curr_QTY: TsCurrencyEdit;
    curr_PRI2: TsCurrencyEdit;
    edt_PRI_BASEC: TsEdit;
    curr_PRI_BASE: TsCurrencyEdit;
    curr_PRI1: TsCurrencyEdit;
    edt_AMT1C: TsEdit;
    curr_AMT1: TsCurrencyEdit;
    curr_AMT2: TsCurrencyEdit;
    edt_ACHG_G: TsEdit;
    curr_AC1_AMT: TsCurrencyEdit;
    curr_AC2_AMT: TsCurrencyEdit;
    sTabSheetDocument: TsTabSheet;
    sPanel17: TsPanel;
    sDBGrid3: TsDBGrid;
    sPanel18: TsPanel;
    btn_docNew: TsSpeedButton;
    btn_DocEdit: TsSpeedButton;
    btn_DocDel: TsSpeedButton;
    sSpeedButton17: TsSpeedButton;
    btn_DocOk: TsSpeedButton;
    btn_DocCancel: TsSpeedButton;
    btn_DocExcel: TsSpeedButton;
    btn_DocSample: TsSpeedButton;
    sPanel19: TsPanel;
    sPanel23: TsPanel;
    sTabSheetTax: TsTabSheet;
    sPanel24: TsPanel;
    sDBGrid4: TsDBGrid;
    sPanel25: TsPanel;
    btn_TaxNew: TsSpeedButton;
    btn_TaxEdit: TsSpeedButton;
    btn_TaxDel: TsSpeedButton;
    sSpeedButton26: TsSpeedButton;
    btn_TaxOk: TsSpeedButton;
    btn_TaxCancel: TsSpeedButton;
    btn_TaxExcel: TsSpeedButton;
    btn_TaxSample: TsSpeedButton;
    sPanel26: TsPanel;
    sPanel32: TsPanel;
    edt_BILL_NO: TsEdit;
    msk_BILL_DATE: TsMaskEdit;
    edt_ITEM_NAME1: TsEdit;
    edt_ITEM_NAME2: TsEdit;
    edt_ITEM_NAME3: TsEdit;
    edt_ITEM_NAME4: TsEdit;
    edt_ITEM_NAME5: TsEdit;
    memo_DESC: TsMemo;
    edt_BILL_AMOUNT_UNIT: TsEdit;
    curr_BILL_AMOUNT: TsCurrencyEdit;
    edt_TAX_AMOUNT_UNIT: TsEdit;
    curr_TAX_AMOUNT: TsCurrencyEdit;
    edt_QUANTITY_UNIT: TsEdit;
    curr_QUANTITY: TsCurrencyEdit;
    Shape1: TShape;
    sPanel28: TsPanel;
    sPanel20: TsPanel;
    sSpeedButton22: TsSpeedButton;
    edt_BNAME: TsEdit;
    msk_BHS_NO: TsMaskEdit;
    memo_BNAME1: TsMemo;
    memo_BSIZE1: TsMemo;
    edt_BAMTC: TsEdit;
    curr_BAMT: TsCurrencyEdit;
    msk_SHIP_DATE: TsMaskEdit;
    sPanel21: TsPanel;
    edt_DOC_G: TsEdit;
    edt_DOC_D: TsEdit;
    edt_BAL_CD: TsEdit;
    edt_BAL_NAME1: TsEdit;
    edt_BAL_NAME2: TsEdit;
    edt_NAT: TsEdit;
    edt_DOC_G_NM: TsEdit;
    sLabel4: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    sPanel22: TsPanel;
    sTabSheet5: TsTabSheet;
    sPanel30: TsPanel;
    sSpeedButton31: TsSpeedButton;
    sMaskEdit8: TsMaskEdit;
    sMaskEdit9: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit47: TsEdit;
    sDBGrid5: TsDBGrid;
    sButton1: TsButton;
    sButton3: TsButton;
    sButton4: TsButton;
    sButton5: TsButton;
    sButton6: TsButton;
    sButton7: TsButton;
    sButton8: TsButton;
    sButton9: TsButton;
    sButton10: TsButton;
    sButton11: TsButton;
    sButton12: TsButton;
    sButton15: TsButton;
    sButton16: TsButton;
    sButton17: TsButton;
    sButton18: TsButton;
    sButton19: TsButton;
    sButton20: TsButton;
    sButton21: TsButton;
    sButton22: TsButton;
    sButton23: TsButton;
    sPanel31: TsPanel;
    qryAPPPCR_H: TADOQuery;
    qryAPPPCR_HMAINT_NO: TStringField;
    qryAPPPCR_HUSER_ID: TStringField;
    qryAPPPCR_HDATEE: TStringField;
    qryAPPPCR_HCHK1: TStringField;
    qryAPPPCR_HCHK2: TStringField;
    qryAPPPCR_HCHK3: TStringField;
    qryAPPPCR_HMESSAGE1: TStringField;
    qryAPPPCR_HMESSAGE2: TStringField;
    qryAPPPCR_HAPP_CODE: TStringField;
    qryAPPPCR_HAPP_NAME1: TStringField;
    qryAPPPCR_HAPP_NAME2: TStringField;
    qryAPPPCR_HAPP_NAME3: TStringField;
    qryAPPPCR_HAPP_ADDR1: TStringField;
    qryAPPPCR_HAPP_ADDR2: TStringField;
    qryAPPPCR_HAPP_ADDR3: TStringField;
    qryAPPPCR_HAX_NAME1: TStringField;
    qryAPPPCR_HAX_NAME2: TStringField;
    qryAPPPCR_HAX_NAME3: TStringField;
    qryAPPPCR_HSUP_CODE: TStringField;
    qryAPPPCR_HSUP_NAME1: TStringField;
    qryAPPPCR_HSUP_NAME2: TStringField;
    qryAPPPCR_HSUP_NAME3: TStringField;
    qryAPPPCR_HSUP_ADDR1: TStringField;
    qryAPPPCR_HSUP_ADDR2: TStringField;
    qryAPPPCR_HSUP_ADDR3: TStringField;
    qryAPPPCR_HITM_G1: TStringField;
    qryAPPPCR_HITM_G2: TStringField;
    qryAPPPCR_HITM_G3: TStringField;
    qryAPPPCR_HTQTY: TBCDField;
    qryAPPPCR_HTQTYC: TStringField;
    qryAPPPCR_HTAMT1: TBCDField;
    qryAPPPCR_HTAMT1C: TStringField;
    qryAPPPCR_HTAMT2: TBCDField;
    qryAPPPCR_HTAMT2C: TStringField;
    qryAPPPCR_HCHG_G: TStringField;
    qryAPPPCR_HC1_AMT: TBCDField;
    qryAPPPCR_HC1_C: TStringField;
    qryAPPPCR_HC2_AMT: TBCDField;
    qryAPPPCR_HBK_NAME1: TStringField;
    qryAPPPCR_HBK_NAME2: TStringField;
    qryAPPPCR_HPRNO: TIntegerField;
    qryAPPPCR_HPCrLic_No: TStringField;
    qryAPPPCR_HChgCd: TStringField;
    qryAPPPCR_HAPP_DATE: TStringField;
    qryAPPPCR_HDOC_G: TStringField;
    qryAPPPCR_HDOC_D: TStringField;
    qryAPPPCR_HBHS_NO: TStringField;
    qryAPPPCR_HBNAME: TStringField;
    qryAPPPCR_HBNAME1: TMemoField;
    qryAPPPCR_HBAMT: TBCDField;
    qryAPPPCR_HBAMTC: TStringField;
    qryAPPPCR_HEXP_DATE: TStringField;
    qryAPPPCR_HSHIP_DATE: TStringField;
    qryAPPPCR_HBSIZE1: TMemoField;
    qryAPPPCR_HBAL_CD: TStringField;
    qryAPPPCR_HBAL_NAME1: TStringField;
    qryAPPPCR_HBAL_NAME2: TStringField;
    qryAPPPCR_HF_INTERFACE: TStringField;
    qryAPPPCR_HSRBUHO: TStringField;
    qryAPPPCR_HITM_GBN: TStringField;
    qryAPPPCR_HITM_GNAME: TStringField;
    qryAPPPCR_HISSUE_GBN: TStringField;
    qryAPPPCR_HDOC_GBN: TStringField;
    qryAPPPCR_HDOC_NO: TStringField;
    qryAPPPCR_HBAMT2: TBCDField;
    qryAPPPCR_HBAMTC2: TStringField;
    qryAPPPCR_HBAMT3: TBCDField;
    qryAPPPCR_HBAMTC3: TStringField;
    qryAPPPCR_HPCRLIC_ISS_NO: TStringField;
    qryAPPPCR_HFIN_CODE: TStringField;
    qryAPPPCR_HFIN_NAME: TStringField;
    qryAPPPCR_HFIN_NAME2: TStringField;
    qryAPPPCR_HNEGO_APPDT: TStringField;
    qryAPPPCR_HEMAIL_ID: TStringField;
    qryAPPPCR_HEMAIL_DOMAIN: TStringField;
    qryAPPPCR_HBK_CD: TStringField;
    dsList: TDataSource;
    qryAPPPCR_HITM_GBN_NM: TStringField;
    qryAPPPCR_HChgCd_NM: TStringField;
    qryAPPPCR_HCHG_G_NM: TStringField;
    qryDetail: TADOQuery;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailREMARK: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTYC: TStringField;
    qryDetailAMT1: TBCDField;
    qryDetailAMT1C: TStringField;
    qryDetailAMT2: TBCDField;
    qryDetailPRI1: TBCDField;
    qryDetailPRI2: TBCDField;
    qryDetailPRI_BASE: TBCDField;
    qryDetailPRI_BASEC: TStringField;
    qryDetailACHG_G: TStringField;
    qryDetailAC1_AMT: TBCDField;
    qryDetailAC1_C: TStringField;
    qryDetailAC2_AMT: TBCDField;
    qryDetailLOC_TYPE: TStringField;
    qryDetailNAMESS: TStringField;
    qryDetailSIZESS: TStringField;
    qryDetailAPP_DATE: TStringField;
    qryDetailITM_NUM: TStringField;
    dsDetail: TDataSource;
    qryDetailCOMPANY_CD: TStringField;
    sPanel16: TsPanel;
    edt_TQTYC: TsEdit;
    curr_TQTY: TsCurrencyEdit;
    curr_TAMT2: TsCurrencyEdit;
    edt_TAMT1C: TsEdit;
    curr_TAMT1: TsCurrencyEdit;
    sButton2: TsButton;
    sButton13: TsButton;
    sButton14: TsButton;
    sPanel33: TsPanel;
    sEdit2: TsEdit;
    edt_AC1_C: TsEdit;
    qryDocument: TADOQuery;
    qryDocumentKEYY: TStringField;
    qryDocumentSEQ: TIntegerField;
    qryDocumentDOC_G: TStringField;
    qryDocumentDOC_D: TStringField;
    qryDocumentBHS_NO: TStringField;
    qryDocumentBNAME: TStringField;
    qryDocumentBNAME1: TMemoField;
    qryDocumentBAMT: TBCDField;
    qryDocumentBAMTC: TStringField;
    qryDocumentEXP_DATE: TStringField;
    qryDocumentSHIP_DATE: TStringField;
    qryDocumentBSIZE1: TMemoField;
    qryDocumentBAL_NAME1: TStringField;
    qryDocumentBAL_NAME2: TStringField;
    qryDocumentBAL_CD: TStringField;
    qryDocumentNAT: TStringField;
    qryDocumentDOC_NAME: TStringField;
    dsDocument: TDataSource;
    edt_COMPAY_CD: TsEdit;
    qryTax: TADOQuery;
    qryTaxKEYY: TStringField;
    qryTaxDATEE: TStringField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxITEM_NAME1: TStringField;
    qryTaxITEM_NAME2: TStringField;
    qryTaxITEM_NAME3: TStringField;
    qryTaxITEM_NAME4: TStringField;
    qryTaxITEM_NAME5: TStringField;
    qryTaxITEM_DESC: TMemoField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    qryTaxQUANTITY: TBCDField;
    qryTaxQUANTITY_UNIT: TStringField;
    qryTaxSEQ: TIntegerField;
    dsTax: TDataSource;
    sPanel34: TsPanel;
    edt_TEMPNO: TsEdit;
    qryDetailBackup: TADOQuery;
    qryDetailBackupKEYY: TStringField;
    qryDetailBackupSEQ: TIntegerField;
    qryDetailBackupHS_NO: TStringField;
    qryDetailBackupNAME: TStringField;
    qryDetailBackupNAME1: TMemoField;
    qryDetailBackupSIZE: TStringField;
    qryDetailBackupSIZE1: TMemoField;
    qryDetailBackupREMARK: TMemoField;
    qryDetailBackupQTY: TBCDField;
    qryDetailBackupQTYC: TStringField;
    qryDetailBackupAMT1: TBCDField;
    qryDetailBackupAMT1C: TStringField;
    qryDetailBackupAMT2: TBCDField;
    qryDetailBackupPRI1: TBCDField;
    qryDetailBackupPRI2: TBCDField;
    qryDetailBackupPRI_BASE: TBCDField;
    qryDetailBackupPRI_BASEC: TStringField;
    qryDetailBackupACHG_G: TStringField;
    qryDetailBackupAC1_AMT: TBCDField;
    qryDetailBackupAC1_C: TStringField;
    qryDetailBackupAC2_AMT: TBCDField;
    qryDetailBackupLOC_TYPE: TStringField;
    qryDetailBackupNAMESS: TStringField;
    qryDetailBackupSIZESS: TStringField;
    qryDetailBackupAPP_DATE: TStringField;
    qryDetailBackupITM_NUM: TStringField;
    qryDetailBackupCOMPANY_CD: TStringField;
    sButton24: TsButton;
    excelOpen: TsOpenDialog;
    qryDocumentBackup: TADOQuery;
    qryDocumentBackupKEYY: TStringField;
    qryDocumentBackupSEQ: TIntegerField;
    qryDocumentBackupDOC_G: TStringField;
    qryDocumentBackupDOC_D: TStringField;
    qryDocumentBackupBHS_NO: TStringField;
    qryDocumentBackupBNAME: TStringField;
    qryDocumentBackupBNAME1: TMemoField;
    qryDocumentBackupBAMT: TBCDField;
    qryDocumentBackupBAMTC: TStringField;
    qryDocumentBackupEXP_DATE: TStringField;
    qryDocumentBackupSHIP_DATE: TStringField;
    qryDocumentBackupBSIZE1: TMemoField;
    qryDocumentBackupBAL_NAME1: TStringField;
    qryDocumentBackupBAL_NAME2: TStringField;
    qryDocumentBackupBAL_CD: TStringField;
    qryDocumentBackupNAT: TStringField;
    sPanel35: TsPanel;
    qryTaxBackup: TADOQuery;
    qryTaxBackupKEYY: TStringField;
    qryTaxBackupSEQ: TIntegerField;
    qryTaxBackupDATEE: TStringField;
    qryTaxBackupBILL_NO: TStringField;
    qryTaxBackupBILL_DATE: TStringField;
    qryTaxBackupITEM_NAME1: TStringField;
    qryTaxBackupITEM_NAME2: TStringField;
    qryTaxBackupITEM_NAME3: TStringField;
    qryTaxBackupITEM_NAME4: TStringField;
    qryTaxBackupITEM_NAME5: TStringField;
    qryTaxBackupITEM_DESC: TMemoField;
    qryTaxBackupBILL_AMOUNT: TBCDField;
    qryTaxBackupBILL_AMOUNT_UNIT: TStringField;
    qryTaxBackupTAX_AMOUNT: TBCDField;
    qryTaxBackupTAX_AMOUNT_UNIT: TStringField;
    qryTaxBackupQUANTITY: TBCDField;
    qryTaxBackupQUANTITY_UNIT: TStringField;
    sPanel36: TsPanel;
    qryCopyDoc: TADOQuery;
    QRCompositeReport1: TQRCompositeReport;
    qryReady: TADOQuery;
    qryCalcTotal: TADOQuery;
    sCheckBox2: TsCheckBox;
    procedure sPageControl1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sDBGrid1TitleClick(Column: TColumn);
    procedure qryAPPPCR_HAfterScroll(DataSet: TDataSet);
    procedure qryDetailNAME1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryDetailAfterScroll(DataSet: TDataSet);
    procedure sDBGrid2TitleClick(Column: TColumn);
    procedure qryAPPPCR_HAfterOpen(DataSet: TDataSet);
    procedure qryDetailAfterOpen(DataSet: TDataSet);
    procedure qryDocumentAfterScroll(DataSet: TDataSet);
    procedure qryDocumentAfterOpen(DataSet: TDataSet);
    procedure sDBGrid3TitleClick(Column: TColumn);
    procedure qryTaxAfterScroll(DataSet: TDataSet);
    procedure qryTaxAfterOpen(DataSet: TDataSet);
    procedure sDBGrid4TitleClick(Column: TColumn);
    procedure btnExitClick(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure edt_SearchNoChange(Sender: TObject);
    procedure sEdit47Change(Sender: TObject);
    procedure Btn_NewClick(Sender: TObject);
    procedure Btn_CancelClick(Sender: TObject);
    procedure edt_N4025DblClick(Sender: TObject);
    procedure edt_N4025Change(Sender: TObject);
    procedure Edt_ApplicationCodeDblClick(Sender: TObject);
    procedure edt_BK_CDDblClick(Sender: TObject);
    procedure sDBGrid5TitleClick(Column: TColumn);
    procedure Btn_DetailAddClick(Sender: TObject);
    procedure Btn_DetailCancelClick(Sender: TObject);
    procedure Btn_DetailOkClick(Sender: TObject);
    procedure GoodsSampleBtnClick(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
    procedure Btn_ModifyClick(Sender: TObject);
    procedure Btn_DelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Btn_TempClick(Sender: TObject);
    procedure Btn_DetailDelClick(Sender: TObject);
    procedure edt_NAMESSDblClick(Sender: TObject);
    procedure sButton8Click(Sender: TObject);
    procedure GoodsExcelBtnClick(Sender: TObject);
    procedure btn_docNewClick(Sender: TObject);
    procedure btn_DocCancelClick(Sender: TObject);
    procedure btn_DocDelClick(Sender: TObject);
    procedure btn_DocOkClick(Sender: TObject);
    procedure btn_DocSampleClick(Sender: TObject);
    procedure btn_TaxNewClick(Sender: TObject);
    procedure btn_TaxCancelClick(Sender: TObject);
    procedure btn_TaxDelClick(Sender: TObject);
    procedure btn_TaxOkClick(Sender: TObject);
    procedure btn_DocExcelClick(Sender: TObject);
    procedure btn_TaxSampleClick(Sender: TObject);
    procedure btn_TaxExcelClick(Sender: TObject);
    procedure edt_BNAMEDblClick(Sender: TObject);
    procedure edt_BAL_CDDblClick(Sender: TObject);
    procedure sButton15Click(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure Btn_PrintClick(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure Btn_ReadyClick(Sender: TObject);
    procedure sButton18Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure btn_SendClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure curr_QTYExit(Sender: TObject);
  private
    { Private declarations }
    FCompareDocNo : String;
    FTMP_DOCNO : String;
    FSQL : String;
    FDetailSQL : String;
    FDocumentSQL : String;
    FTaxSQL : String;
    uDialog : TCodeDialogParent_frm;
    function ReadListOnly(sortField : String = ''):integer;
    function ReadDetail(sortField : String = ''):Integer;
    function ReadDocument(sortField : String = ''):Integer;
    function ReadTax(sortField : String= ''):Integer;
    function UpdateNewSeqQuery(sTablename, sKey : String):String;

    procedure RollbackDetail;
    procedure RollbackDocument;
    procedure RollbackTax;

    procedure RollbackAll;
    procedure CommitAll;

    procedure setData;
    procedure setDataDetail;
    procedure setDocument;
    procedure setTax;

    procedure ButtonControl;
    procedure PageControlTagStar;
    procedure ReadyDoc;
  public
    { Public declarations }
  end;

var
  UI_APPPCR_NEW_frm: TUI_APPPCR_NEW_frm;

implementation

uses
  ICON, MSSQL, Commonlib, VarDefine, AutoNo, CodeContents, CD_NK4025, CD_APPPCR_GUBUN, CD_BYEONDONG, CD_AMT_UNIT, CD_UNIT, CD_CUST, CD_BANK, NewExcelWriter, MessageDefine, SQLCreator, CD_ITEMLIST, NewExcelReader, CD_APPPCR_DOC, CD_NATION, APPPCR_PRINT, APPPCR_PRINT1, APPPCR_PRINT2, Preview, Dlg_RecvSelect, CreateDocuments, DocumentSend;
{$R *.dfm}

procedure TUI_APPPCR_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 4;
end;

procedure TUI_APPPCR_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_APPPCR_NEW_frm := nil;
end;

procedure TUI_APPPCR_NEW_frm.FormCreate(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctView;
  FSQL := qryAPPPCR_H.SQL.Text;
  FDetailSQL := qryDetail.SQL.Text;
  FDocumentSQL := qryDocument.SQL.Text;
  FTaxSQL := qryTax.SQL.Text;

  FTMP_DOCNO := '';

  ReadOnlyControl(sPanel6);
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel12);
  ReadOnlyControl(sPanel19);
  ReadOnlyControl(sPanel26);

end;

function TUI_APPPCR_NEW_frm.ReadListOnly(sortField : String = ''):integer;
begin
  ClearGridTitleCaption(sDBGrid1);
  ClearGridTitleCaption(sDBGrid5);

  with qryAPPPCR_H do
  Begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE between '+QuotedStr(sMaskEdit1.Text)+' and '+QuotedStr(sMaskEdit2.Text));

    IF Length(Trim(edt_SearchNo.Text)) > 0 Then
      SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+Trim(edt_SearchNo.Text)+'%'));

    IF sortField = '' Then
      SQL.Add('ORDER BY DATEE DESC, MAINT_NO DESC')
    else
    begin
      if (qryAPPPCR_H.Filter = '') OR (qryAPPPCR_H.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryAPPPCR_H.Filter := sortField;
        Tag := 0;
      end
      else
      if qryAPPPCR_H.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;

      if sortField <> 'MAINT_NO' then
        SQL.Add(', MAINT_NO DESC');
    end;

    Open;
  end;

  result := qryAPPPCR_H.Tag;
end;

procedure TUI_APPPCR_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  qryAPPPCR_H.Filter := '';
  ReadListOnly;
end;

procedure TUI_APPPCR_NEW_frm.sDBGrid1TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  nRst := ReadListOnly(Column.FieldName);
  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

procedure TUI_APPPCR_NEW_frm.setData;
begin
  //------------------------------------------------------------------------------
  // 공통사항
  //------------------------------------------------------------------------------
  edt_MAINT_NO_Header.Text := qryAPPPCR_HMAINT_NO.AsString;
  msk_Datee.Text := qryAPPPCR_HDATEE.AsString;
  edt_userno.Text := qryAPPPCR_HUSER_ID.AsString;

  CM_INDEX(com_func, [':'], 0, qryAPPPCR_HMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryAPPPCR_HMESSAGE2.AsString);

  Edt_ApplicationCode.Text   := qryAPPPCR_HAPP_CODE.AsString;
  Edt_ApplicationSAUPNO.Text := qryAPPPCR_HAX_NAME2.AsString;
  Edt_ApplicationName1.Text := qryAPPPCR_HAPP_NAME1.AsString;
  Edt_ApplicationName2.Text := qryAPPPCR_HAPP_NAME2.AsString;
  Edt_ApplicationAddr1.Text := qryAPPPCR_HAPP_ADDR1.AsString;
  Edt_ApplicationAddr2.Text := qryAPPPCR_HAPP_ADDR2.AsString;
  Edt_ApplicationAddr3.Text := qryAPPPCR_HAPP_ADDR3.AsString;
  Edt_ApplicationAXNAME1.Text := qryAPPPCR_HAX_NAME1.AsString;
  Edt_ApplicationElectronicSign.Text := qryAPPPCR_HAX_NAME3.AsString;
  Edt_ApplicationAPPDATE.Text := qryAPPPCR_HAPP_DATE.AsString;

  edt_N4025.Text := qryAPPPCR_HITM_GBN.AsString;
  edt_N4025_NM.Text := qryAPPPCR_HITM_GBN_NM.AsString;

  Edt_DocumentsSection.Text    := qryAPPPCR_HChgCd.AsString;
  Edt_DocumentsSection_NM.Text := qryAPPPCR_HChgCd_NM.AsString;
  Edt_BuyConfirmDocuments.Text := qryAPPPCR_HPCrLic_No.AsString;

  if Edt_DocumentsSection.Text = '2CH' then
  begin
    Edt_BuyConfirmDocuments.Color := clWhite;
    Edt_BuyConfirmDocuments.BoundLabel.Font.Color := clBlack;
  end
  else
  begin
    Edt_BuyConfirmDocuments.Color := clBtnFace;
    Edt_BuyConfirmDocuments.BoundLabel.Font.Color := clGray;
  end;

  Edt_SupplyCode.Text   := qryAPPPCR_HSUP_CODE.AsString;
  Edt_SupplySaupNo.Text := qryAPPPCR_HSUP_ADDR3.AsString;
  Edt_SupplyName.Text   := qryAPPPCR_HSUP_NAME1.AsString;
  Edt_SupplyCEO.Text    := qryAPPPCR_HSUP_NAME2.AsString;
  Edt_SupplyEID.Text    := qryAPPPCR_HEMAIL_ID.AsString+'@'+qryAPPPCR_HEMAIL_DOMAIN.AsString;
  IF Edt_SupplyEID.Text = '@' Then Edt_SupplyEID.Clear;
  Edt_SupplyAddr1.Text  := qryAPPPCR_HSUP_ADDR1.AsString;
  Edt_SupplyAddr2.Text  := qryAPPPCR_HSUP_ADDR2.AsString;

  edt_CHG_G.Text := qryAPPPCR_HCHG_G.AsString;
  edt_CHG_G_NM.Text := qryAPPPCR_HCHG_G_NM.AsString;
  curr_C2_AMT.Value := qryAPPPCR_HC2_AMT.AsFloat;
  curr_C1_AMT.Value := qryAPPPCR_HC1_AMT.AsFloat;
  edt_C1_C.Text := qryAPPPCR_HC1_C.AsString;

  edt_TQTYC.Text  := qryAPPPCR_HTQTYC.AsString;
  curr_TQTY.Value := qryAPPPCR_HTQTY.AsFloat;
  curr_TAMT2.Value := qryAPPPCR_HTAMT2.AsFloat;
  edt_TAMT1C.Text := qryAPPPCR_HTAMT1C.AsString;
  curr_TAMT1.Value := qryAPPPCR_HTAMT1.AsFloat;

  edt_BK_CD.Text := qryAPPPCR_HBK_CD.AsString;
  edt_BK_NAME1.Text := qryAPPPCR_HBK_NAME1.AsString;
  edt_BK_NAME2.Text := qryAPPPCR_HBK_NAME2.AsString;
end;

procedure TUI_APPPCR_NEW_frm.qryAPPPCR_HAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setData;
  qryDetail.Filter := '';
  ReadDetail;
  qryDocument.Filter := '';
  ReadDocument;
  qryTax.Filter := '';
  ReadTax;

//------------------------------------------------------------------------------
// 각탭의 변경사항이 있는지 체크하는 캡션 초기화
//------------------------------------------------------------------------------
  sTabSheetDetail.Tag := 0;
  sTabSheetTax.Tag := 0;
  sTabSheetDocument.Tag := 0;
  PageControlTagStar;
end;

procedure TUI_APPPCR_NEW_frm.qryDetailNAME1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  Text := AnsiReplaceText(Sender.AsString, #13#10, ' ');
end;

function TUI_APPPCR_NEW_frm.ReadDetail(sortField: String):integer;
begin
  ClearGridTitleCaption(sDBGrid2);
  with qryDetail do
  begin
    Close;
    SQL.Text := FDetailSQL;
    IF ProgramControlType = ctInsert Then
      Parameters[0].Value := FTMP_DOCNO
    else
    IF ProgramControlType = ctModify Then
    begin
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      SQL.Add('AND KEYY = '+QuotedStr(FTMP_DOCNO));
    end
    else
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;


    IF sortField = '' Then
      SQL.Add('ORDER BY SEQ')
    else
    begin
      if (qryDetail.Filter = '') OR (qryDetail.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryDetail.Filter := sortField;
        Tag := 0;
      end
      else
      if qryDetail.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;
    end;

    Open;
  end;

  result := qryDetail.Tag;
end;

procedure TUI_APPPCR_NEW_frm.setDataDetail;
begin
  msk_HS_NO.Text    := qryDetailHS_NO.AsString;
  msk_APP_DATE.Text := qryDetailAPP_DATE.AsString;
  edt_NAMESS.Text   := qryDetailNAMESS.AsString;

  memo_NAME1.Text  := qryDetailNAME1.AsString;
  memo_Size.Text   := qryDetailSIZE1.AsString;
  memo_Remark.Text := qryDetailREMARK.AsString;

  edt_COMPAY_CD.Text := qryDetailCOMPANY_CD.AsString;

  edt_QTYC.Text  := qryDetailQTYC.AsString;
  curr_QTY.Value := qryDetailQTY.AsFloat;
  curr_PRI2.Value := qryDetailPRI2.AsFloat;
  edt_PRI_BASEC.Text := qryDetailPRI_BASEC.AsString;
  curr_PRI_BASE.Value := qryDetailPRI_BASE.AsFloat;
  curr_PRI1.Value := qryDetailPRI1.AsFloat;
  edt_AMT1C.Text := qryDetailAMT1C.AsString;
  curr_AMT1.Value := qryDetailAMT1.AsFloat;
  curr_AMT2.Value := qryDetailAMT2.AsFloat;

  edt_ACHG_G.Text := qryDetailACHG_G.AsString;
  curr_AC1_AMT.Value := qryDetailAC1_AMT.AsFloat;
  edt_AC1_C.Text := qryDetailAC1_C.AsString;
  curr_AC2_AMT.Value := qryDetailAC2_AMT.AsFloat;
end;

procedure TUI_APPPCR_NEW_frm.qryDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setDataDetail;
end;

procedure TUI_APPPCR_NEW_frm.sDBGrid2TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  if sTabSheetDetail.Tag <> 0 Then Exit;

  If Column.Field.DataType = ftMemo Then
  begin
    MessageBox(Self.Handle, 'Memo필드는 정렬할 수 없습니다', '정렬오류', MB_OK+MB_ICONQUESTION);
    Exit;
  end;

  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  nRst := ReadDetail(Column.FieldName);
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

procedure TUI_APPPCR_NEW_frm.qryAPPPCR_HAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 Then qryAPPPCR_HAfterScroll(DataSet);
end;

procedure TUI_APPPCR_NEW_frm.qryDetailAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryDetailAfterScroll(Dataset);
end;

function TUI_APPPCR_NEW_frm.ReadDocument(sortField: String): Integer;
begin
  ClearGridTitleCaption(sDBGrid3);
  with qryDocument do
  begin
    Close;
    SQL.Text := FDocumentSQL;
    IF ProgramControlType = ctInsert Then
      Parameters[0].Value := FTMP_DOCNO
    else
    IF ProgramControlType = ctModify Then
    begin
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      SQL.Add('AND KEYY = '+QuotedStr(FTMP_DOCNO));
    end
    else
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;

    IF sortField = '' Then
      SQL.Add('ORDER BY SEQ')
    else
    begin
      if (qryDocument.Filter = '') OR (qryDocument.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryDocument.Filter := sortField;
        Tag := 0;
      end
      else
      if qryDocument.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;
    end;

    Open;
  end;

  result := qryDocument.Tag;
end;

procedure TUI_APPPCR_NEW_frm.setDocument;
begin
  edt_BNAME.Text := qryDocumentBNAME.AsString;
  msk_BHS_NO.Text := qryDocumentBHS_NO.AsString;
  memo_BNAME1.Text := qryDocumentBNAME1.AsString;
  memo_BSIZE1.Text := qryDocumentBSIZE1.AsString;
  edt_BAMTC.Text := qryDocumentBAMTC.AsString;
  curr_BAMT.Value := qryDocumentBAMT.AsFloat;
  msk_SHIP_DATE.Text := qryDocumentSHIP_DATE.AsString;

  edt_DOC_G.Text := qryDocumentDOC_G.AsString;
  edt_DOC_G_NM.Text := qryDocumentDOC_NAME.AsString;
  edt_DOC_D.Text := qryDocumentDOC_D.AsString;
  edt_BAL_CD.Text := qryDocumentBAL_CD.AsString;
  edt_BAL_NAME1.Text := qryDocumentBAL_NAME1.AsString;
  edt_BAL_NAME2.Text := qryDocumentBAL_NAME2.AsString;

  edt_NAT.Text := qryDocumentNAT.AsString;
end;

procedure TUI_APPPCR_NEW_frm.qryDocumentAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setDocument;
end;

procedure TUI_APPPCR_NEW_frm.qryDocumentAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryDocumentAfterScroll(Dataset);
end;

procedure TUI_APPPCR_NEW_frm.sDBGrid3TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  if sTabSheetDocument.Tag <> 0 Then Exit;

  If Column.Field.DataType = ftMemo Then
  begin
    MessageBox(Self.Handle, 'Memo필드는 정렬할 수 없습니다', '정렬오류', MB_OK+MB_ICONQUESTION);
    Exit;
  end;

  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  nRst := ReadDocument(Column.FieldName);
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

function TUI_APPPCR_NEW_frm.ReadTax(sortField: String): Integer;
begin
  ClearGridTitleCaption(sDBGrid4);
  with qryTax do
  begin
    Close;
    SQL.Text := FTaxSQL;
    IF ProgramControlType = ctInsert Then
      Parameters[0].Value := FTMP_DOCNO
    else
    IF ProgramControlType = ctModify Then
    begin
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      SQL.Add('AND KEYY = '+QuotedStr(FTMP_DOCNO));
    end
    else
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;

    IF sortField = '' Then
      SQL.Add('ORDER BY SEQ')
    else
    begin
      if (qryTax.Filter = '') OR (qryTax.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryTax.Filter := sortField;
        Tag := 0;
      end
      else
      if qryTax.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;
    end;

    Open;
  end;

  result := qryTax.Tag;
end;

procedure TUI_APPPCR_NEW_frm.setTax;
begin
  edt_BILL_NO.Text := qryTaxBILL_NO.AsString;
  msk_BILL_DATE.Text := qryTaxBILL_DATE.AsString;
  edt_ITEM_NAME1.Text := qryTaxITEM_NAME1.AsString;
  edt_ITEM_NAME2.Text := qryTaxITEM_NAME2.AsString;
  edt_ITEM_NAME3.Text := qryTaxITEM_NAME3.AsString;
  edt_ITEM_NAME4.Text := qryTaxITEM_NAME4.AsString;
  edt_ITEM_NAME5.Text := qryTaxITEM_NAME5.AsString;
  memo_DESC.Text := qryTaxITEM_DESC.AsString;
  edt_BILL_AMOUNT_UNIT.Text := qryTaxBILL_AMOUNT_UNIT.AsString;
  curr_BILL_AMOUNT.Value := qryTaxBILL_AMOUNT.AsFloat;
  edt_TAX_AMOUNT_UNIT.Text := qryTaxTAX_AMOUNT_UNIT.AsString;
  curr_TAX_AMOUNT.Value := qryTaxTAX_AMOUNT.AsFloat;
  edt_QUANTITY_UNIT.Text := qryTaxQUANTITY_UNIT.AsString;
  curr_QUANTITY.Value := qryTaxQUANTITY.AsFloat;
end;

procedure TUI_APPPCR_NEW_frm.qryTaxAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setTax;
end;

procedure TUI_APPPCR_NEW_frm.qryTaxAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryTaxAfterScroll(Dataset);
end;

procedure TUI_APPPCR_NEW_frm.sDBGrid4TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  if sTabSheetTax.Tag <> 0 Then Exit;

  If Column.Field.DataType = ftMemo Then
  begin
    MessageBox(Self.Handle, 'Memo필드는 정렬할 수 없습니다', '정렬오류', MB_OK+MB_ICONQUESTION);
    Exit;
  end;

  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  nRst := ReadTax(Column.FieldName);
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

procedure TUI_APPPCR_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_APPPCR_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  case (Sender as TsMaskEdit).Tag of
    0: sMaskEdit8.Text := sMaskEdit1.Text;
    1: sMaskEdit9.Text := sMaskEdit2.Text;
    2: sMaskEdit1.Text := sMaskEdit8.Text;
    3: sMaskEdit2.Text := sMaskEdit9.Text;
  end;
end;

procedure TUI_APPPCR_NEW_frm.edt_SearchNoChange(Sender: TObject);
begin
  inherited;
  sEdit47.Text := edt_SearchNo.Text;
end;

procedure TUI_APPPCR_NEW_frm.sEdit47Change(Sender: TObject);
begin
  inherited;
  edt_SearchNo.Text := sEdit47.Text;
end;

procedure TUI_APPPCR_NEW_frm.Btn_NewClick(Sender: TObject);
begin
  inherited;
  // 문서모드 - 작성으로 변경
  ProgramControlType := ctInsert;
  ReadOnlyControl(sPanel5);
  // 입력잠금 해제
  ReadOnlyControl(sPanel6, False);
  ReadOnlyControl(sPanel7, False);

  ClearControl(sPanel7);

  // 임시번호부여
  FTMP_DOCNO := 'APPPCR_TMP_'+FormatDateTime('YYMMDDHHNNSSMS', Now);
  edt_TEMPNO.Text := FTMP_DOCNO;

  FCompareDocNo := DMAutoNo.GetDocumentNo('APPPCR');
  edt_MAINT_NO_Header.Text  := FCompareDocNo;
  edt_MAINT_NO_Header.Color := clWhite;

  // 등록일자
  msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
  // 사용자
  edt_userno.Text := LoginData.sID;

  // 문서기능, 유형
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;

  // 신청인
  IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
  begin
    Edt_ApplicationCode.Text := DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
    Edt_ApplicationName1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    Edt_ApplicationName2.Text := DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
    Edt_ApplicationAddr1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
    Edt_ApplicationAddr2.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
    Edt_ApplicationAddr3.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR3').AsString;
    Edt_ApplicationAXNAME1.Text := DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
    Edt_ApplicationSAUPNO.Text := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
    Edt_ApplicationElectronicSign.Text := DMCodeContents.GEOLAECHEO.FieldByName('Jenja').AsString;
  end;

  // 발행일자
  Edt_ApplicationAPPDATE.Text := FormatDateTime('YYYYMMDD', Now);

  Edt_DocumentsSection.Text := '2CG';
  CD_APPPCR_GUBUN_frm := TCD_APPPCR_GUBUN_frm.Create(Self);
  try
    Edt_DocumentsSection_NM.Text := CD_APPPCR_GUBUN_frm.GetCodeText(Edt_DocumentsSection.Text);
  finally
    FreeAndNil( CD_APPPCR_GUBUN_frm );
  end;

  Edt_BuyConfirmDocuments.ReadOnly := not (Edt_DocumentsSection.Text = '2CH');
  Edt_BuyConfirmDocuments.TabStop := not Edt_BuyConfirmDocuments.ReadOnly;
  if Edt_BuyConfirmDocuments.ReadOnly Then
  begin
    Edt_BuyConfirmDocuments.Color := clBtnFace;
    Edt_BuyConfirmDocuments.BoundLabel.Font.Color := clGray;
  end
  else
  begin
    Edt_BuyConfirmDocuments.Color := clWhite;
    Edt_BuyConfirmDocuments.BoundLabel.Font.Color := clBlack;
  end;

  edt_TAMT1C.Text := 'USD';

  sPanel29.Visible := True;
  ButtonControl;

  ReadDetail;
  ReadDocument;
  ReadTax;

  sPageControl1.ActivePageIndex := 0;
  sPageControl1Change(sPageControl1);
end;

procedure TUI_APPPCR_NEW_frm.Btn_CancelClick(Sender: TObject);
begin
  inherited;
  try
    DMMssql.BeginTrans;
    //------------------------------------------------------------------------------
    // 신규 - 취소시 입력데이터 전부 삭제
    //------------------------------------------------------------------------------
    if ProgramControlType = ctInsert Then
    begin
      RollbackAll;
    end
    else
    //------------------------------------------------------------------------------
    // 수정-취소시 기존데이터로 되돌리기
    //------------------------------------------------------------------------------
    // 상품내역
    if ProgramControlType = ctModify then
    begin
      if sTabSheetDetail.Tag = 1 Then
      begin
        RollbackDetail;
        qryDetailBackup.Close;
      end;

      if sTabSheetDocument.Tag = 1 Then
      begin
        RollbackDocument;
        qryDocumentBackup.Close;
      end;

      if sTabSheetTax.Tag = 1 Then
      begin
        RollbackTax;
        qryTaxBackup.Close;
      end;
    end;

    DMMssql.CommitTrans;
  except
    on E:Exception do
    begin
      DMMssql.RollbackTrans;
      MessageBox(Self.Handle, PCHAR('ERROR'#13#10+e.Message), 'RollBack Error', MB_OK+MB_ICONERROR);
    end;
  end;

  // 문서모드 - 뷰어로 변경
  ProgramControlType := ctView;
  sDBGrid2.Tag := 0;
  sDBGrid3.Tag := 0;
  sDBGrid4.Tag := 0;
  ReadOnlyControl(sPanel5, False);
  ReadOnlyControl(sPanel6);

  // 입력잠금
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel12);
  ReadOnlyControl(sPanel19);
  ReadOnlyControl(sPanel26);

  //임시번호 삭제
  FTMP_DOCNO := '';
  edt_TEMPNO.Clear;

  sPanel29.Visible := False;
  ButtonControl;

  setData;
  ReadDetail;
  ReadDocument;
  ReadTax;

//------------------------------------------------------------------------------
// 각탭의 변경사항이 있는지 체크하는 캡션 초기화
//------------------------------------------------------------------------------
  sTabSheetDetail.Tag := 0;
  sTabSheetTax.Tag := 0;
  sTabSheetDocument.Tag := 0;
  PageControlTagStar;

end;

procedure TUI_APPPCR_NEW_frm.ButtonControl;
begin

  // Button State
  // 문서공통
  Btn_New.Enabled := ProgramControlType = ctView;
  Btn_Modify.Enabled := ProgramControlType = ctView;
  Btn_Del.Enabled := ProgramControlType = ctView;
  btnCopy.Enabled := ProgramControlType = ctView;

  Btn_Temp.Enabled := ProgramControlType in [ctInsert, ctModify];
  Btn_Save.Enabled := ProgramControlType in [ctInsert, ctModify];
  Btn_Cancel.Enabled := ProgramControlType in [ctInsert, ctModify];

  Btn_Print.Enabled := ProgramControlType = ctView;
  Btn_Ready.Enabled := ProgramControlType = ctView;
  btn_Send.Enabled  := ProgramControlType = ctView;

  sButton2.Enabled := ProgramControlType in [ctInsert, ctModify];

  // 상품내역
  Btn_DetailAdd.Enabled  := not Btn_New.Enabled and (sDBGrid2.Tag = 0);
  Btn_DetailEdit.Enabled := Btn_DetailAdd.Enabled and (qryDetail.RecordCount > 0);
  Btn_DetailDel.Enabled  := Btn_DetailEdit.Enabled;
  Btn_DetailOk.Enabled   := not Btn_DetailAdd.Enabled and (sDBGrid2.Tag <> 0);
  Btn_DetailCancel.Enabled := Btn_DetailOk.Enabled;
  GoodsExcelBtn.Enabled := Btn_DetailAdd.Enabled;
  GoodsSampleBtn.Enabled := Btn_DetailAdd.Enabled;

  //근거서류
  btn_docNew.Enabled  := not Btn_New.Enabled and (sDBGrid3.Tag = 0);
  btn_DocEdit.Enabled := btn_docNew.Enabled and (qryDocument.RecordCount > 0);
  btn_DocDel.Enabled  := btn_DocEdit.Enabled;
  btn_DocOk.Enabled   := not btn_docNew.Enabled and (sDBGrid3.Tag <> 0);
  btn_DocCancel.Enabled := btn_DocOk.Enabled;
  btn_DocExcel.Enabled  := btn_docNew.Enabled;
  btn_DocSample.Enabled := btn_docNew.Enabled;

  //세금계산서
  btn_TaxNew.Enabled  := not Btn_New.Enabled and (sDBGrid4.Tag = 0);
  btn_TaxEdit.Enabled := btn_TaxNew.Enabled and (qryTax.RecordCount > 0);
  btn_TaxDel.Enabled  := btn_TaxEdit.Enabled;
  btn_TaxOk.Enabled   := not btn_TaxNew.Enabled and (sDBGrid4.Tag <> 0);
  btn_TaxCancel.Enabled := btn_TaxOk.Enabled;
  btn_TaxExcel.Enabled  := btn_TaxNew.Enabled;
  btn_TaxSample.Enabled := btn_TaxNew.Enabled;
//------------------------------------------------------------------------------
// Grid 제어
//------------------------------------------------------------------------------
  sDBGrid1.Enabled := ProgramControlType = ctView;
  sDBGrid2.Enabled := (sDBGrid2.Tag = 0);
  sDBGrid3.Enabled := (sDBGrid3.Tag = 0);
  sDBGrid4.Enabled := (sDBGrid4.Tag = 0);

  if ProgramControlType = ctView Then
  begin
    sPanel3.Color := INPUT_NO_COLOR;
    sPanel2.Color := sPanel3.Color;
    sPanel8.Color := sPanel3.Color;
    sPanel31.Color := sPanel3.Color;
    sPanel11.Color := sPanel3.Color;
    sPanel16.Color := sPanel3.Color;
    sPanel10.Color := sPanel3.Color;
  end
  else
  begin
    sPanel3.Color := INPUT_OK_COLOR;
    sPanel2.Color := sPanel3.Color;
    sPanel8.Color := sPanel3.Color;
    sPanel31.Color := sPanel3.Color;
    sPanel11.Color := sPanel3.Color;
    sPanel16.Color := sPanel3.Color;
    sPanel10.Color := sPanel3.Color;
  end;

  // 상품내역 작업내용 변경
  if sDBGrid2.Tag = 0 Then
  begin
    sPanel34.Visible := False;
    sPanel14.Color := INPUT_NO_COLOR;
    sPanel15.Color := sPanel14.Color;
    sPanel33.Color := sPanel14.Color;
  end
  else
  begin
    if sDBGrid2.Tag = 1 Then
      sPanel34.Visible := True;

    sPanel14.Color := INPUT_OK_COLOR;
    sPanel15.Color := sPanel14.Color;
    sPanel33.Color := sPanel14.Color;
  end;

  // 근거서류 작업내용 변경
  if sDBGrid3.Tag = 0 Then
  begin
    sPanel35.Visible := False;
    sPanel20.Color := INPUT_NO_COLOR;
    sPanel21.Color := sPanel20.Color;
  end
  else
  begin
    if sDBGrid3.Tag = 1 Then
      sPanel35.Visible := True;

    sPanel20.Color := INPUT_OK_COLOR;
    sPanel21.Color := sPanel20.Color;
  end;

  // 세금계산서 작업내용 변경
  if sDBGrid4.Tag = 0 Then
  begin
    sPanel36.Visible := False;
    sPanel28.Color := INPUT_NO_COLOR;
  end
  else
  begin
    if sDBGrid4.Tag = 1 Then
      sPanel36.Visible := True;

    sPanel28.Color := INPUT_OK_COLOR;
  end;

  sBitBtn1.Enabled := Btn_New.Enabled;
  sBitBtn5.Enabled := Btn_New.Enabled;
end;

procedure TUI_APPPCR_NEW_frm.edt_N4025DblClick(Sender: TObject);
var
  nTag : Integer;
begin
  IF ProgramControlType = ctView Then Exit;
//------------------------------------------------------------------------------
// 상품내역,근거서류,세금계산서 페이지 컨트롤
//------------------------------------------------------------------------------
  nTag := (Sender as TsEdit).Tag;
  if nTag >= 5000 Then
  begin
    if sDBGrid4.Tag = 0 Then Exit
  end
  else
  if nTag >= 4000 Then
  begin
    if sDBGrid3.Tag = 0 Then Exit
  end
  else
  if nTag >= 3000 Then
  begin
    if sDBGrid2.Tag = 0 Then Exit;
  end;

  Case (Sender as TsEdit).Tag of
    // 공급물품 명세구분
    1000: uDialog := TCD_NK4025_frm.Create(Self);
    1001: uDialog := TCD_APPPCR_GUBUN_frm.Create(Self);
    1002, 3002: uDialog := TCD_BYEONDONG_frm.Create(Self);
    2000, 3000, 4000, 5000 : uDialog := TCD_AMT_UNIT_frm.Create(Self);
    2001, 3001, 5001: uDialog := TCD_UNIT_frm.Create(Self);
    4010 : uDialog := TCD_APPPCR_DOC_frm.Create(Self);
    4011 : uDialog := TCD_NATION_frm.Create(Self);
  end;

  try
    if uDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := uDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        //개설은행
        1000 : edt_N4025_NM.Text := uDialog.Values[1];
        1001 : Edt_DocumentsSection_NM.Text := uDialog.Values[1];
        1002 : edt_CHG_G_NM.Text := uDialog.Values[1];

        3002 : sEdit2.Text := uDialog.Values[1];
        4010 : edt_DOC_G_NM.Text := uDialog.Values[1];
      end;
    end;
  finally
    FreeAndNil(uDialog);
  end;
end;

procedure TUI_APPPCR_NEW_frm.edt_N4025Change(Sender: TObject);
var
  uDialog : TCodeDialogParent_frm;
  sSearchText : String;
begin
  inherited;
  IF ProgramControlType = ctView Then Exit;
//------------------------------------------------------------------------------
// 상품내역페이지 컨트롤
//------------------------------------------------------------------------------
  IF ((Sender as TsEdit).Tag >= 3000) AND (sDBGrid2.Tag = 0) Then Exit;

  IF Length(Trim((Sender as TsEdit).Text)) = 0 then
  begin
    case (Sender as TsEdit).Tag of
      1000: edt_N4025_NM.Clear;
      1001: Edt_DocumentsSection_NM.Clear;
      1002: edt_CHG_G_NM.Clear;
      3002: sEdit2.Text;
    end;
  end
  else
  begin
    If Length(Trim((Sender as TsEdit).Text)) IN [(Sender as TsEdit).MaxLength-1,(Sender as TsEdit).MaxLength] Then
    begin
      Case (Sender as TsEdit).Tag of
        // 공급물품 명세구분
        1000: uDialog := TCD_NK4025_frm.Create(Self);
        1001: uDialog := TCD_APPPCR_GUBUN_frm.Create(Self);
        1002, 3002: uDialog := TCD_BYEONDONG_frm.Create(Self);
      end;

      try
        sSearchText := (Sender as TsEdit).Text;
        sSearchText := uDialog.GetCodeText(sSearchText);
        case (Sender as TsEdit).Tag of
          1000: edt_N4025_NM.Text := sSearchText;
          1001:
          begin
            Edt_BuyConfirmDocuments.ReadOnly := not ((Sender as TsEdit).Text = '2CH');
            Edt_BuyConfirmDocuments.TabStop := not Edt_BuyConfirmDocuments.ReadOnly;
            if Edt_BuyConfirmDocuments.ReadOnly Then
            begin
              Edt_BuyConfirmDocuments.Color := clBtnFace;
              Edt_BuyConfirmDocuments.BoundLabel.Font.Color := clGray;
            end
            else
            begin
              Edt_BuyConfirmDocuments.Color := clWhite;
              Edt_BuyConfirmDocuments.BoundLabel.Font.Color := clBlack;
            end;
            Edt_DocumentsSection_NM.Text := sSearchText;
          end;
          1002: edt_CHG_G_NM.Text := sSearchText;
          3002: sEdit2.Text := sSearchText;
        end;
      finally
        FreeAndNil(uDialog);
      end;
    end;
  end;
end;

procedure TUI_APPPCR_NEW_frm.Edt_ApplicationCodeDblClick(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then Exit;

  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_CUST_frm.Values[0];

      case (Sender as TsEdit).Tag of
        101:
          begin
            Edt_ApplicationName1.Text := CD_CUST_frm.Field('ENAME').AsString;
            Edt_ApplicationName2.Text := CD_CUST_frm.Field('REP_NAME').AsString;
            Edt_ApplicationAddr1.Text := CD_CUST_frm.Field('ADDR1').AsString;
            Edt_ApplicationAddr2.Text := CD_CUST_frm.Field('ADDR2').AsString;
            Edt_ApplicationAddr3.Text := CD_CUST_frm.Field('ADDR3').AsString;
            Edt_ApplicationAXNAME1.Text := CD_CUST_frm.Field('REP_NAME').AsString;
            Edt_ApplicationSAUPNO.Text := CD_CUST_frm.Field('SAUP_NO').AsString;
            Edt_ApplicationElectronicSign.Text := CD_CUST_frm.Field('Jenja').AsString;
          end;

        102:
          begin
            Edt_SupplySaupNo.Text := CD_CUST_frm.Field('SAUP_NO').AsString;
            Edt_SupplyName.Text   := CD_CUST_frm.Field('ENAME').AsString;
            Edt_SupplyCEO.Text    := CD_CUST_frm.Field('REP_NAME').AsString;
            Edt_SupplyAddr1.Text  := CD_CUST_frm.Field('ADDR1').AsString;
            Edt_SupplyAddr2.Text  := CD_CUST_frm.Field('ADDR2').AsString;
            Edt_SupplyEID.Text    := CD_CUST_frm.Field('EMAIL_ID').AsString+'@'+CD_CUST_frm.Field('EMAIL_DOMAIN').AsString;
            if Trim(Edt_SupplyEID.Text) = '@' Then
              Edt_SupplyEID.Clear;
          end;
      end;

    end;

  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_APPPCR_NEW_frm.edt_BK_CDDblClick(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then Exit;

  CD_BANK_frm := TCD_BANK_frm.Create(Self);
  try
    IF CD_BANK_frm.openDialog(0, edt_BK_CD.Text) Then
    begin
      edt_BK_CD.Text := CD_BANK_frm.Field('CODE').AsString;
      edt_BK_NAME1.Text := CD_BANK_frm.Field('BANKNAME1').AsString;
      edt_BK_NAME2.Text := CD_BANK_frm.Field('BANKBRANCH1').AsString;
    end;
  finally
    FreeAndNil(CD_BANK_frm);
  end;
end;

procedure TUI_APPPCR_NEW_frm.sDBGrid5TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  if not Btn_New.Enabled Then Exit; 

  nRst := ReadListOnly(Column.FieldName);
  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

procedure TUI_APPPCR_NEW_frm.Btn_DetailAddClick(Sender: TObject);
begin
  inherited;
  sDBGrid2.Tag := (Sender as TsSpeedButton).Tag;
  ButtonControl;

  if (not qryDetailBackup.Active) and (ProgramControlType = ctModify) then
  begin
    with qryDetailBackup do
    begin
      Close;
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      Open;
    end;
  end;

  if sDBGrid2.Tag = 1 Then
    ClearControl(sPanel12);

  ReadOnlyControl(sPanel12, false);
end;

procedure TUI_APPPCR_NEW_frm.Btn_DetailCancelClick(Sender: TObject);
begin
  inherited;
  sDBGrid2.Tag := 0;
  ButtonControl;
  ReadOnlyControl(sPanel12);
  setDataDetail;
end;

procedure TUI_APPPCR_NEW_frm.Btn_DetailOkClick(Sender: TObject);
var
  sc : TSQLCreate;
  sKey : String;
  nSeq : Integer;
begin
  inherited;
  //HS부호 누락확인
  if Trim(msk_HS_NO.Text) = '' then
  begin
    msk_HS_NO.SetFocus;
    MessageBox(self.Handle, MSG_APPPCR_DETAIL_NO_HS, '데이터확인', MB_OK+MB_ICONINFORMATION);
    Exit;
  end
  else
  if Trim(msk_APP_DATE.Text) = '' then
  begin
    msk_APP_DATE.SetFocus;
    MessageBox(self.Handle, MSG_APPPCR_DETAIL_NO_APPDATE, '데이터확인', MB_OK+MB_ICONINFORMATION);
    Exit;
  end
  else
  if Trim(memo_NAME1.Text) = '' Then
  begin
    memo_NAME1.SetFocus;
    MessageBox(self.Handle, MSG_APPPCR_DETAIL_NO_PUM_NM, '데이터확인', MB_OK+MB_ICONINFORMATION);
    Exit;
  end
  else
  if Trim(edt_QTYC.Text) = '' Then
  begin
    edt_QTYC.SetFocus;
    MessageBox(self.Handle, MSG_APPPCR_DETAIL_NOT_QTYC, '데이터확인', MB_OK+MB_ICONINFORMATION);
    Exit;
  end
  else
  if curr_QTY.Value = 0 Then
  begin
    if MessageBox(self.Handle, MSG_APPPCR_DETAIL_QTY_ZERO, '데이터확인', MB_OKCANCEL+MB_ICONINFORMATION) = ID_CANCEL then
    begin
      curr_QTY.SetFocus;
      Exit;
    end;
  end
  else
  if curr_PRI2.Value = 0 Then
  begin
    if MessageBox(self.Handle, MSG_APPPCR_DETAIL_PRI2_ZERO, '데이터확인', MB_OKCANCEL+MB_ICONINFORMATION) = ID_CANCEL then
    begin
      curr_PRI2.SetFocus;
      Exit;
    end;
  end
  else
  if Trim(edt_AMT1C.Text) = '' Then
  begin
    edt_AMT1C.SetFocus;
    MessageBox(self.Handle, MSG_APPPCR_DETAIL_NOT_AMT1C, '데이터확인', MB_OK+MB_ICONINFORMATION);
    Exit;
  end
  else
  if curr_AMT1.Value = 0 Then
  begin
    if MessageBox(self.Handle, MSG_APPPCR_DETAIL_AMT1_ZERO, '데이터확인', MB_OKCANCEL+MB_ICONINFORMATION) = ID_CANCEL then
    begin
      curr_AMT1.SetFocus;
      Exit;
    end;
  end;
//------------------------------------------------------------------------------
// 저장시작
// 문서전체가 신규추가일경우는 임시번호로 각각추가하여 마지막 저장시에 관리번호로 UPDATE
//------------------------------------------------------------------------------
  IF ProgramControlType = ctInsert Then
    sKey := FTMP_DOCNO
  else
  IF ProgramControlType = ctModify Then
    sKey := qryAPPPCR_HMAINT_NO.AsString;

  sc := TSQLCreate.Create;
  try
    if sDBGrid2.Tag = 1 Then
    begin
      //INSERT
      nSeq := getIntFromQuery('SELECT ISNULL(MAX(SEQ),0)+1 as MAX_SEQ FROM APPPCRD1 WHERE KEYY = '+QuotedStr(sKey));

      sc.DMLType := dmlInsert;
      sc.SQLHeader('APPPCRD1');
      sc.ADDValue('KEYY', sKey);
      sc.ADDValue('SEQ', nSeq);
    end
    else
    if sDBGrid2.Tag = 2 Then
    begin
      //EDIT
      nSeq := qryDetailSEQ.AsInteger;

      sc.DMLType := dmlUpdate;
      sc.SQLHeader('APPPCRD1');
      sc.ADDWhere('KEYY', sKey);
      sc.ADDWhere('SEQ', nSeq);
    end;

    sc.ADDValue('HS_NO', msk_HS_NO.Text);
    sc.ADDValue('NAME1', memo_NAME1.Text);
    sc.ADDValue('SIZE1', memo_Size.Text);
    sc.ADDValue('REMARK', memo_Remark.Text);
    sc.ADDValue('QTY', curr_QTY.Value);
    sc.ADDValue('QTYC', edt_QTYC.Text);
    sc.ADDValue('AMT1', curr_AMT1.Value);
    sc.ADDValue('AMT1C', edt_AMT1C.Text);
    sc.ADDValue('AMT2', curr_AMT2.Text);
    sc.ADDValue('PRI1', curr_PRI1.Value);
    sc.ADDValue('PRI2', curr_PRI2.Value);
    sc.ADDValue('PRI_BASE', curr_PRI_BASE.value);
    sc.ADDValue('PRI_BASEC', edt_PRI_BASEC.Text);
    sc.ADDValue('ACHG_G', edt_ACHG_G.Text);
    sc.ADDValue('AC1_AMT', curr_AC1_AMT.Value);
    sc.ADDValue('AC1_C', edt_AC1_C.Text);
    sc.ADDValue('AC2_AMT', curr_AC2_AMT.Value);
    sc.ADDValue('LOC_TYPE', '');
    sc.ADDValue('NAMESS', edt_NAMESS.Text);
    sc.ADDValue('SIZESS', '');
    sc.ADDValue('APP_DATE', msk_APP_DATE.Text);
    sc.ADDValue('ITM_NUM', '');
    sc.ADDValue('COMPANY_CD', edt_COMPAY_CD.Text);

    ExecSQL(sc.CreateSQL);
  finally
    sc.Free;
  end;
//------------------------------------------------------------------------------
// 저장완료
//------------------------------------------------------------------------------
  qryDetail.Close;
  qryDetail.Open;

  qryDetail.Locate('KEYY;SEQ', VarArrayOf([sKey, nSeq]), []);

  sDBGrid2.Tag := 0;
  ButtonControl;
  ReadOnlyControl(sPanel12);

//------------------------------------------------------------------------------
// 변경사항이 있으면 캡션에 * 표시
//------------------------------------------------------------------------------
  sTabSheetDetail.Tag := 1;
  PageControlTagStar;
end;

procedure TUI_APPPCR_NEW_frm.GoodsSampleBtnClick(Sender: TObject);
var
  EW : TExcelWriter;
begin
  //상품내역 엑셀샘플----------------------------------------------------------
  inherited;
  EW := TExcelWriter.Create;
  try
    EW.Cells(1,  1, '품명코드(선택)', taCenter);
    EW.Cells(2,  1, '품      명', taCenter);
    EW.Cells(3,  1, '규격(선택)', taCenter);
    EW.Cells(4,  1, '비고(선택)', taCenter);
    EW.Cells(5,  1, 'HS부호(숫자만입력)', taCenter);
    EW.Cells(6,  1, '구매공급일', taCenter);
    EW.Cells(7,  1, '수      량', taCenter);
    EW.Cells(8,  1, '수량단위', taCenter);
    EW.Cells(9,  1, '단      가', taCenter);
    EW.Cells(10, 1, '단가기준수량(선택)', taCenter);
    EW.Cells(11, 1, '단가기준수량단위(선택)', taCenter);
    EW.Cells(12, 1, '단가(USD금액부기)(선택)', taCenter);
    EW.Cells(13, 1, '금     액', taCenter);
    EW.Cells(14, 1, '금액통화', taCenter);
    EW.Cells(15, 1, '금액(USD금액부기)(선택)', taCenter);
    EW.Cells(16, 1, '변동구분/금액(USD)', taCenter);
    EW.Cells(17, 1, '변동구분/금액(USD)통화', taCenter);
    EW.Cells(18, 1, '변동금액', taCenter);
    EW.Cells(19, 1, '변동금액통화', taCenter); 

    EW.Autofit;

    EW.ShowExcel;
  except
    on E:Exception do
    begin
      EW.Free;
      MessageBox(Self.Handle, PChar('ERROR'#13#10+E.Message), 'ERROR', MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TUI_APPPCR_NEW_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsButton).Tag of
    0: Edt_ApplicationCodeDblClick(Edt_ApplicationCode);
    1: Edt_ApplicationCodeDblClick(Edt_SupplyCode);
  end;
end;

procedure TUI_APPPCR_NEW_frm.sButton4Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsButton).Tag of
    0: edt_N4025DblClick( edt_N4025 );
    1: edt_N4025DblClick( Edt_DocumentsSection );
    2: edt_N4025DblClick( edt_CHG_G );
    3: edt_N4025DblClick( edt_C1_C );
    4: edt_N4025DblClick( edt_TQTYC );
    5: edt_N4025DblClick( edt_TAMT1C );
//------------------------------------------------------------------------------
// 상품내역페이지
//------------------------------------------------------------------------------
    6: edt_N4025DblClick( edt_QTYC );
    7: edt_N4025DblClick( edt_PRI_BASEC );
    8: edt_N4025DblClick( edt_AMT1C );
    9: edt_N4025DblClick( edt_AC1_C );
//------------------------------------------------------------------------------
// 근거서류페이지
//------------------------------------------------------------------------------
    10: edt_N4025DblClick( edt_BAMTC );
    11: edt_N4025DblClick( edt_DOC_G );
    12: edt_N4025DblClick( edt_NAT );
//------------------------------------------------------------------------------
// 세금계산서 페이지
//------------------------------------------------------------------------------
    13: edt_N4025DblClick( edt_BILL_AMOUNT_UNIT );
    14: edt_N4025DblClick( edt_TAX_AMOUNT_UNIT );
    15: edt_N4025DblClick( edt_QUANTITY_UNIT );
  end;
end;

procedure TUI_APPPCR_NEW_frm.Btn_ModifyClick(Sender: TObject);
begin
  inherited;
  // 문서모드 - 작성으로 변경
  ProgramControlType := ctModify;
  ReadOnlyControl(sPanel5);

  // 입력잠금 해제
  edt_MAINT_NO_Header.Color := clBtnFace;
  msk_Datee.Color := clBtnFace;
  ReadOnlyControl(sPanel6);
  ReadOnlyControl(sPanel7, False);

  // 임시번호
  FTMP_DOCNO := 'APPPCR_TMP_'+FormatDateTime('YYMMDDHHNNSSMS', Now);
  edt_TEMPNO.Text := FTMP_DOCNO;

  ButtonControl;
end;

const
  delTables : array [0..3] of string = ('APPPCRD1', 'APPPCRD2', 'APPPCR_TAX', 'APPPCR_H');
procedure TUI_APPPCR_NEW_frm.Btn_DelClick(Sender: TObject);
var
  sc : TSQLCreate;
  i, nidx : Integer;
  sMAINT_NO : String;
begin
  inherited;
  IF MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  sMAINT_NO := qryAPPPCR_HMAINT_NO.AsString;
  nidx := qryAPPPCR_H.RecNo;

  sc := TSQLCreate.Create;
  try
    DMMssql.BeginTrans;
    try
      for i := 0 to High(delTables) do
      begin
        sc.DMLType := dmlDelete;
        sc.SQLHeader(delTables[i]);
        if i = 3 Then
          sc.ADDWhere('MAINT_NO', sMAINT_NO)
        else
          sc.ADDWhere('KEYY', sMAINT_NO);

        ExecSQL(sc.CreateSQL);
      end;

      DMMssql.CommitTrans;

      qryAPPPCR_H.Close;
      qryAPPPCR_H.Open;

      if nidx > 1 Then
        qryAPPPCR_H.MoveBy(nidx-1);

      MessageBox(Self.Handle, MSG_SYSTEM_DEL_OK, '삭제완료', MB_OK+MB_ICONINFORMATION);
    except
      on e:Exception do
      begin
        DMMssql.RollbackTrans;
        MessageBox(Self.Handle, PChar('ERROR'#13#10+e.Message), '삭제오류', MB_OK+MB_ICONERROR);
      end;
    end;
  finally
    sc.Free;
  end;
end;

procedure TUI_APPPCR_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sPageControl1.ActivePageIndex := 0;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', IncMonth(StartOfTheMonth( Now ),-1));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', Now);
  sMaskEdit8.Text := sMaskEdit1.Text;
  sMaskEdit9.Text := sMaskEdit2.Text;

  RefreshButton(sPanel7);

  ReadListOnly;
end;

procedure TUI_APPPCR_NEW_frm.Btn_TempClick(Sender: TObject);
var
  sc : TSQLCreate;
  sKey : String;
  workSession : TProgramControlType;
begin
  inherited;
  // 신규입력시 관리번호 누락
  if (ProgramControlType = ctInsert) and (Trim(edt_MAINT_NO_Header.Text) = '') Then
  begin
    edt_MAINT_NO_Header.SetFocus;
    MessageBox(Self.Handle, MSG_SYSTEM_EMPTY_MAINT_NO, '관리번호 누락', MB_OK+MB_ICONINFORMATION);
    exit;
  end;

  if (Sender as TsButton).Tag = 1 Then
  begin
    // 상품내역, 근거서류, 세금계산서 작성중 확인
    if sDBGrid2.Tag <> 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      MessageBox(Self.Handle, MSG_APPPCR_DETAIL_WRITING, '[상품내역]작성중', MB_OK+MB_ICONINFORMATION);
      Exit;
    end
    else if sDBGrid3.Tag <> 0 Then
    begin
      sPageControl1.ActivePageIndex := 2;
      MessageBox(Self.Handle, MSG_APPPCR_DOCUMENT_WRITING, '[근거서류]작성중', MB_OK+MB_ICONINFORMATION);
      Exit;
    end
    else if sDBGrid4.Tag <> 0 Then
    begin
      sPageControl1.ActivePageIndex := 3;
      MessageBox(Self.Handle, MSG_APPPCR_TAX_WRITING, '[세금계산서]작성중', MB_OK+MB_ICONINFORMATION);
      Exit;
    end

    // 데이터체크, 누락체크
  end;
  workSession := ProgramControlType;
//------------------------------------------------------------------------------
// 신규
//------------------------------------------------------------------------------
  sKey := Trim(edt_MAINT_NO_Header.Text);
  sc := TSQLCreate.Create;
  try
    if ProgramControlType = ctInsert Then
    begin
      sc.DMLType := dmlInsert;
      sc.ADDValue('MAINT_NO', sKey);
    end
    else
    if ProgramControlType = ctModify then
    begin
      sc.DMLType := dmlUpdate;
      sc.ADDWhere('MAINT_NO', sKey);
    end;
    sc.SQLHeader('APPPCR_H');

    sc.ADDValue('USER_ID', edt_userno.Text);
    sc.ADDValue('DATEE', msk_Datee.Text);
    sc.ADDValue('CHK1', '');
    sc.ADDValue('CHK2', (Sender as TsButton).Tag);
    sc.ADDValue('CHK3', '');
    sc.ADDValue('MESSAGE1',CM_TEXT(com_func,[':']));
    sc.ADDValue('MESSAGE2',CM_TEXT(com_type,[':']));
    sc.ADDValue('APP_CODE', Edt_ApplicationCode.Text );
    sc.ADDValue('APP_NAME1', Edt_ApplicationName1.Text );
    sc.ADDValue('APP_NAME2', Edt_ApplicationName2.Text );
//    sc.ADDValue('APP_NAME3', '');
    sc.ADDValue('APP_ADDR1', Edt_ApplicationAddr1.Text );
    sc.ADDValue('APP_ADDR2', Edt_ApplicationAddr2.Text );
    sc.ADDValue('APP_ADDR3', Edt_ApplicationAddr3.Text );
    sc.ADDValue('AX_NAME1', Edt_ApplicationAXNAME1.Text);
    sc.ADDValue('AX_NAME2', Edt_ApplicationSAUPNO.Text);
    sc.ADDValue('AX_NAME3', Edt_ApplicationElectronicSign.Text);
    sc.ADDValue('SUP_CODE', Edt_SupplyCode.Text);
    sc.ADDValue('SUP_NAME1', Edt_SupplyName.Text);
    sc.ADDValue('SUP_NAME2', Edt_SupplyCEO.Text);
//    sc.ADDValue('SUP_NAME3', '');
    sc.ADDValue('SUP_ADDR1', Edt_SupplyAddr1.Text );
    sc.ADDValue('SUP_ADDR2', Edt_SupplyAddr2.Text );
    sc.ADDValue('SUP_ADDR3', Edt_SupplySaupNo.Text );
    sc.ADDValue('ITM_G1', '');
    sc.ADDValue('ITM_G2', '');
    sc.ADDValue('ITM_G3', '');
    sc.ADDValue('TQTY', curr_TQTY.value );
    sc.ADDValue('TQTYC', edt_TQTYC.Text );
    sc.ADDValue('TAMT1', curr_TAMT1.value);
    sc.ADDValue('TAMT1C', edt_TAMT1C.Text);
    sc.ADDValue('TAMT2', curr_TAMT2.value);
    sc.ADDValue('TAMT2C', 'USD');
    sc.ADDValue('CHG_G', edt_CHG_G.Text);
    sc.ADDValue('C1_AMT', curr_C1_AMT.value);
    sc.ADDValue('C1_C', edt_C1_C.Text);
    sc.ADDValue('C2_AMT', curr_C2_AMT.value);
    sc.ADDValue('BK_NAME1', edt_BK_NAME1.Text);
    sc.ADDValue('BK_NAME2', edt_BK_NAME2.Text);
    sc.ADDValue('PRNO', 0);
    sc.ADDValue('PCrLic_No', Edt_BuyConfirmDocuments.Text);
    sc.ADDValue('ChgCd', Edt_DocumentsSection.Text);
    sc.ADDValue('APP_DATE', Edt_ApplicationAPPDATE.Text);
    sc.ADDValue('ITM_GBN', edt_N4025.Text );
    sc.ADDValue('ITM_GNAME', edt_N4025_NM.Text );
    if Length(Trim(Edt_SupplyEID.Text)) > 0 then
    begin
      sc.ADDValue('EMAIL_ID', LeftStr(Trim(Edt_SupplyEID.Text), Pos('@',Trim(Edt_SupplyEID.Text))-1));
      sc.ADDValue('EMAIL_DOMAIN', MidStr(Trim(Edt_SupplyEID.Text), Pos('@',Trim(Edt_SupplyEID.Text))+1, 30));
    end;
    sc.ADDValue('BK_CD', edt_BK_CD.Text );

    ExecSQL(sc.CreateSQL);
//------------------------------------------------------------------------------
// 저장완료
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// 임시번호를 전부 문서번호로변경
//------------------------------------------------------------------------------
    CommitAll;
//------------------------------------------------------------------------------
// 제출번호 증가
//------------------------------------------------------------------------------
    if FCompareDocNo = edt_MAINT_NO_Header.Text then
      DMAutoNo.RenewNo;


    // 문서모드 - 뷰어로 변경
    ProgramControlType := ctView;
    sDBGrid2.Tag := 0;
    sDBGrid3.Tag := 0;
    sDBGrid4.Tag := 0;
    ReadOnlyControl(sPanel5, False);
    ReadOnlyControl(sPanel6);

    // 입력잠금
    ReadOnlyControl(sPanel7);
    ReadOnlyControl(sPanel12);
    ReadOnlyControl(sPanel19);
    ReadOnlyControl(sPanel26);

    //임시번호 삭제
    FTMP_DOCNO := '';
    edt_TEMPNO.Clear;

    sPanel29.Visible := False;
    ButtonControl;

    if workSession = ctInsert Then
    begin
      sMaskEdit1.Text := FormatDateTime('YYYYMMDD', IncMonth(StartOfTheMonth( Now ),-1));
      sMaskEdit2.Text := FormatDateTime('YYYYMMDD', Now);
      sMaskEdit8.Text := sMaskEdit1.Text;
      sMaskEdit9.Text := sMaskEdit2.Text;
      ReadListOnly;

      qryAPPPCR_H.Locate('MAINT_NO', sKey, []);
    end
    else
    if workSession = ctModify Then
    begin
      qryAPPPCR_H.Close;
      qryAPPPCR_H.Open;

      qryAPPPCR_H.Locate('MAINT_NO', sKey, []);
    end;

    workSession := ctView;
//    ReadDetail;
//    ReadDocument;
//    ReadTax;


  finally
    sc.Free;
  end;
end;

procedure TUI_APPPCR_NEW_frm.Btn_DetailDelClick(Sender: TObject);
var
  sc :TSQLCreate;
  nRecNo : Integer;
begin
  inherited;
  if MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '상품내역 삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then Exit;

  if not qryDetailBackup.Active then
  begin
    with qryDetailBackup do
    begin
      Close;
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      Open;
    end;
  end;

  sc := TSQLCreate.Create;
  try
    sc.DMLType := dmlDelete;
    sc.SQLHeader('APPPCRD1');
    sc.ADDWhere('KEYY', qryDetailKEYY.AsString);
    sc.ADDWhere('SEQ', qryDetailSEQ.asInteger);
    ExecSQL(sc.CreateSQL);

    // 순번 재정렬
    ExecSQL(UpdateNewSeqQuery('APPPCRD1', qryDetailKEYY.AsString));
  finally
    sc.Free;
  end;
  nRecNo := qryDetail.RecNo;
//------------------------------------------------------------------------------
// 삭제완료
//------------------------------------------------------------------------------
  qryDetail.Close;
  qryDetail.Open;

  if nRecNo > 1 Then
  begin
    qryDetail.MoveBy(nRecNo-1);
  end;

  sDBGrid2.Tag := 0;
  ButtonControl;
  ReadOnlyControl(sPanel12);

//------------------------------------------------------------------------------
// 변경사항이 있으면 캡션에 * 표시
//------------------------------------------------------------------------------
  sTabSheetDetail.Tag := 1;
  PageControlTagStar;
end;

function TUI_APPPCR_NEW_frm.UpdateNewSeqQuery(sTablename, sKey: String):String;
begin
  Result := 'UPDATE '+sTablename+' SET SEQ = NEW_SEQ FROM'#13#10+
            '(SELECT KEYY, SEQ, ROW_NUMBER() OVER(ORDER BY SEQ) as NEW_SEQ FROM '+sTablename+' WHERE KEYY = '+QuotedStr(sKey)+') UP_TABLE'#13#10+
            'WHERE '+sTablename+'.KEYY = UP_TABLE.KEYY AND '+sTablename+'.SEQ = UP_TABLE.SEQ';
end;

procedure TUI_APPPCR_NEW_frm.PageControlTagStar;
var
  i : integer;
begin
  for i := 1 to sPageControl1.PageCount-1 do
  begin
    sPageControl1.Pages[i].Caption := AnsiReplaceText(sPageControl1.Pages[i].Caption, '*', '');
    if sPageControl1.Pages[i].Tag = 1 Then
      sPageControl1.Pages[i].Caption := sPageControl1.Pages[i].Caption+'*';
  end;
end;

procedure TUI_APPPCR_NEW_frm.RollbackDetail;
var
  sc : TSQLCreate;
  i : Integer;
begin
  sc := TSQLCreate.Create;
  try
    try
      //------------------------------------------------------------------------------
      // 기존 입력된자료 삭제
      //------------------------------------------------------------------------------
      sc.DMLType := dmlDelete;
      sc.SQLHeader('APPPCRD1');
      sc.ADDWhere('KEYY', qryDetailKEYY.AsString);
      ExecSQL(sc.CreateSQL);
      //------------------------------------------------------------------------------
      // 복구
      //------------------------------------------------------------------------------
      while not qryDetailBackup.Eof do
      begin
        sc.DMLType := dmlInsert;
        sc.SQLHeader('APPPCRD1');
        for i := 0 to qryDetailBackup.FieldCount-1 do
        begin
          case qryDetailBackup.Fields[i].DataType of
            ftString, ftMemo : sc.ADDValue(qryDetailBackup.Fields[i].FieldName, qryDetailBackup.Fields[i].AsString);
            ftInteger : sc.ADDValue(qryDetailBackup.Fields[i].FieldName, qryDetailBackup.Fields[i].asInteger);
            ftBCD, ftFloat, ftCurrency : sc.ADDValue(qryDetailBackup.Fields[i].FieldName, qryDetailBackup.Fields[i].asFloat);
            ftBoolean : sc.ADDValue(qryDetailBackup.Fields[i].FieldName, qryDetailBackup.Fields[i].asBoolean);
          end;
        end;
        ExecSQL(sc.CreateSQL);

        qryDetailBackup.Next;
      end;
    except
      on e:Exception do
      begin
        MessageBox(Self.Handle, PChar('ERROR'#13#10+e.Message), 'RollBack error', MB_OK+MB_ICONERROR);
//        AddLog(e.Message);
      end;
    end;
  finally
    sc.Free;
    qryDetailBackup.Close;
  end;
end;

procedure TUI_APPPCR_NEW_frm.RollbackAll;
var
  sc : TSQLCreate;
begin
  sc := TSQLCreate.Create;
  try
    sc.DMLType := dmlDelete;
    sc.SQLHeader('APPPCR_TAX');
    sc.ADDWhere('KEYY', FTMP_DOCNO);
    ExecSQL(sc.CreateSQL);

    sc.DMLType := dmlDelete;
    sc.SQLHeader('APPPCRD2');
    sc.ADDWhere('KEYY', FTMP_DOCNO);
    ExecSQL(sc.CreateSQL);

    sc.DMLType := dmlDelete;
    sc.SQLHeader('APPPCRD1');
    sc.ADDWhere('KEYY', FTMP_DOCNO);
    ExecSQL(sc.CreateSQL);
  finally
    sc.Free;
  end;
end;

procedure TUI_APPPCR_NEW_frm.CommitAll;
var
  sc : TSQLCreate;
begin
  sc := TSQLCreate.Create;
  try
    sc.DMLType := dmlUpdate;
    sc.SQLHeader('APPPCR_TAX');
    sc.ADDValue('KEYY', edt_MAINT_NO_Header.Text);
    sc.ADDWhere('KEYY', FTMP_DOCNO);
    ExecSQL(sc.CreateSQL);

    sc.DMLType := dmlUpdate;
    sc.SQLHeader('APPPCRD2');
    sc.ADDValue('KEYY', edt_MAINT_NO_Header.Text);
    sc.ADDWhere('KEYY', FTMP_DOCNO);
    ExecSQL(sc.CreateSQL);

    sc.DMLType := dmlUpdate;
    sc.SQLHeader('APPPCRD1');
    sc.ADDValue('KEYY', edt_MAINT_NO_Header.Text);
    sc.ADDWhere('KEYY', FTMP_DOCNO);
    ExecSQL(sc.CreateSQL);
  finally
    sc.Free;
//------------------------------------------------------------------------------
// 임시데이터셋을 Close
//------------------------------------------------------------------------------
    qryDetailBackup.Close;
    qryDocumentBackup.Close;
    qryTaxBackup.Close;
  end;
end;

procedure TUI_APPPCR_NEW_frm.edt_NAMESSDblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid2.Tag = 0 Then Exit;

  CD_ITEMLIST_frm := TCD_ITEMLIST_frm.Create(Self);
  try
    if CD_ITEMLIST_frm.OpenDialog(0,Trim(edt_NAMESS.Text)) Then
    begin
      edt_NAMESS.Text := CD_ITEMLIST_frm.Values[0];
      msk_HS_NO.Text  := CD_ITEMLIST_frm.Values[1];
      memo_NAME1.Text := CD_ITEMLIST_frm.Values[2];
      memo_Size.Text  := CD_ITEMLIST_frm.Values[4];
    end;
  finally
    FreeAndNil(CD_ITEMLIST_frm);
  end;
end;

procedure TUI_APPPCR_NEW_frm.sButton8Click(Sender: TObject);
begin
  inherited;
  edt_NAMESSDblClick(edt_NAMESS);
end;

procedure TUI_APPPCR_NEW_frm.GoodsExcelBtnClick(Sender: TObject);
var
  ER : TExcelReader;
  sc : TSQLCreate;
  ExcelfileName, sKey : String;
  i : Integer;
  tmpStr : String;
begin
  inherited;
  if not excelOpen.Execute then Exit;

  //excelOpen.FileName을 사용해도 되지만 윈도우상에 버그로 글씨가 깨지거나함
  ExcelfileName := excelOpen.Files.Strings[0]; //선택된 엑셀파일이름

  IF MessageBox(Self.Handle, MSG_EXCEL_IMPORT_ALLDATA_DEL, '가져오기확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then Exit;

  IF ProgramControlType = ctInsert Then
    sKey := FTMP_DOCNO
  else
  IF ProgramControlType = ctModify Then
    sKey := qryAPPPCR_HMAINT_NO.AsString;

  if (not qryDetailBackup.Active) and (ProgramControlType = ctModify) then
  begin
    with qryDetailBackup do
    begin
      Close;
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      Open;
    end;
  end;

  try
    DMMssql.BeginTrans;
    try
      //------------------------------------------------------------------------------
      // 기존 데이터 삭제
      //------------------------------------------------------------------------------
      sc := TSQLCreate.Create;
      sc.DMLType := dmlDelete;
      sc.SQLHeader('APPPCRD1');
      sc.ADDWhere('KEYY', sKey);
      ExecSQL(sc.CreateSQL);

      ER := TExcelReader.Create(ExcelfileName);

      for i := 2 to ER.RowCount do
      begin
        sc.DMLType := dmlInsert;
        sc.SQLHeader('APPPCRD1');
        sc.ADDValue('KEYY', sKey);
        sc.ADDValue('SEQ', i-1);
        sc.ADDValue('NAMESS',   ER.asString(i, 1));//품명코드
        sc.ADDValue('NAME1',    ER.asString(i, 2));//품명
        sc.ADDValue('SIZE',     ER.asString(i, 3));//규격
        sc.ADDValue('REMARK',   ER.asString(i, 4));//비고
        tmpStr := ER.asString(i, 5);
        tmpStr := AnsiReplaceText(AnsiReplaceText(tmpStr,'-',''),'.','');
        sc.ADDValue('HS_NO',    tmpStr);//hs부호
        sc.ADDValue('APP_DATE', ER.asString(i, 6));//구매공급일
        sc.ADDValue('QTY',      ER.asString(i, 7), vtInteger);//수량
        sc.ADDValue('QTYC',     ER.asString(i, 8));//수량단위
        sc.ADDValue('PRI2',     ER.asString(i, 9), vtInteger);//단가
        sc.ADDValue('PRI_BASE', ER.asString(i, 10), vtInteger);//단가기준수량
        sc.ADDValue('PRI_BASEC',ER.asString(i, 11));//단가기준수량단위
        sc.ADDValue('PRI1',     ER.asString(i, 12), vtInteger);//단가(USD금액부기)
        sc.ADDValue('AMT1',     ER.asString(i, 13), vtInteger);//금액
        sc.ADDValue('AMT1C',    ER.asString(i, 14));//금액단위
        sc.ADDValue('AMT2',     ER.asString(i, 15), vtInteger);//금액(USD금액부기)
        sc.ADDValue('AC1_AMT',  ER.asString(i, 16), vtInteger);//변동구분/금액(USD)
        sc.ADDValue('ACHG_G',   ER.asString(i, 17));//변동구분/금액(USD)단위
        sc.ADDValue('AC2_AMT',  ER.asString(i, 18), vtInteger);//변동금액
        sc.ADDValue('AC1_C',    ER.asString(i, 19));//변동금액단위
        ExecSQL(sc.CreateSQL);
      end;

      DMMssql.CommitTrans;

    //------------------------------------------------------------------------------
    // 변경사항이 있으면 캡션에 * 표시
    //------------------------------------------------------------------------------
      sTabSheetDetail.Tag := 1;
      PageControlTagStar;

      qryDetail.Close;
      qryDetail.Open;

      MessageBox(Self.Handle, MSG_EXCEl_IMPORT_OK , 'Excel Import Complete', MB_OK+MB_ICONINFORMATION);
    except
      on e:exception do
      begin
        DMMssql.RollbackTrans;
        MessageBox(Self.Handle, PChar('ERROR'#13#10+e.Message), 'Excel Import Error', MB_OK+MB_ICONERROR);
      end;
    end;
  finally
    ER.Free;
    sc.Free;
  end;
end;

procedure TUI_APPPCR_NEW_frm.btn_docNewClick(Sender: TObject);
begin
  inherited;
  sDBGrid3.Tag := (Sender as TsSpeedButton).Tag;
  ButtonControl;

  if (not qryDocumentBackup.Active) and (ProgramControlType = ctModify) then
  begin
    with qryDocumentBackup do
    begin
      Close;
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      Open;
    end;
  end;

  if sDBGrid3.Tag = 1 Then
    ClearControl(sPanel19);

  ReadOnlyControl(sPanel19, false);
end;

procedure TUI_APPPCR_NEW_frm.btn_DocCancelClick(Sender: TObject);
begin
  inherited;
  sDBGrid3.Tag := 0;
  ButtonControl;
  ReadOnlyControl(sPanel19);
  setDocument;
end;

procedure TUI_APPPCR_NEW_frm.btn_DocDelClick(Sender: TObject);
var
  sc :TSQLCreate;
  nRecNo : Integer;
begin
  inherited;
  if MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '근거서류 삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then Exit;

  if not qryDocumentBackup.Active then
  begin
    with qryDocumentBackup do
    begin
      Close;
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      Open;
    end;
  end;

  sc := TSQLCreate.Create;
  try
    sc.DMLType := dmlDelete;
    sc.SQLHeader('APPPCRD2');
    sc.ADDWhere('KEYY', qryDocumentBackupKEYY.AsString);
    sc.ADDWhere('SEQ', qryDocumentBackupSEQ.asInteger);
    ExecSQL(sc.CreateSQL);

    // 순번 재정렬
    ExecSQL(UpdateNewSeqQuery('APPPCRD2', qryDocumentBackupKEYY.AsString));
  finally
    sc.Free;
  end;
  nRecNo := qryDocument.RecNo;
//------------------------------------------------------------------------------
// 삭제완료
//------------------------------------------------------------------------------
  qryDocument.Close;
  qryDocument.Open;

  if nRecNo > 1 Then
  begin
    qryDocument.MoveBy(nRecNo-1);
  end;

  sDBGrid3.Tag := 0;
  ButtonControl;
  ReadOnlyControl(sPanel19);

//------------------------------------------------------------------------------
// 변경사항이 있으면 캡션에 * 표시
//------------------------------------------------------------------------------
  sTabSheetDocument.Tag := 1;
  PageControlTagStar;
end;

procedure TUI_APPPCR_NEW_frm.RollbackDocument;
var
  sc : TSQLCreate;
  i : Integer;
begin
  sc := TSQLCreate.Create;
  try
    try
      //------------------------------------------------------------------------------
      // 기존 입력된자료 삭제
      //------------------------------------------------------------------------------
      sc.DMLType := dmlDelete;
      sc.SQLHeader('APPPCRD2');
      sc.ADDWhere('KEYY', qryDocumentKEYY.AsString);
      ExecSQL(sc.CreateSQL);
      //------------------------------------------------------------------------------
      // 복구
      //------------------------------------------------------------------------------
      while not qryDocumentBackup.Eof do
      begin
        sc.DMLType := dmlInsert;
        sc.SQLHeader('APPPCRD2');
        for i := 0 to qryDocumentBackup.FieldCount-1 do
        begin
          case qryDocumentBackup.Fields[i].DataType of
            ftString, ftMemo : sc.ADDValue(qryDocumentBackup.Fields[i].FieldName, qryDocumentBackup.Fields[i].AsString);
            ftInteger : sc.ADDValue(qryDocumentBackup.Fields[i].FieldName, qryDocumentBackup.Fields[i].asInteger);
            ftBCD, ftFloat, ftCurrency : sc.ADDValue(qryDocumentBackup.Fields[i].FieldName, qryDocumentBackup.Fields[i].asFloat);
            ftBoolean : sc.ADDValue(qryDocumentBackup.Fields[i].FieldName, qryDocumentBackup.Fields[i].asBoolean);
          end;
        end;
        ExecSQL(sc.CreateSQL);

        qryDocumentBackup.Next;
      end;
    except
      on e:Exception do
      begin
        MessageBox(Self.Handle, PChar('ERROR'#13#10+e.Message), 'RollBack error', MB_OK+MB_ICONERROR);
//        AddLog(e.Message);
      end;
    end;
  finally
    sc.Free;
    qryDocumentBackup.Close;
  end;
end;

procedure TUI_APPPCR_NEW_frm.btn_DocOkClick(Sender: TObject);
var
  sc : TSQLCreate;
  sKey : String;
  nSeq : Integer;
begin
  inherited;
//------------------------------------------------------------------------------
// 저장시작
// 문서전체가 신규추가일경우는 임시번호로 각각추가하여 마지막 저장시에 관리번호로 UPDATE
//------------------------------------------------------------------------------
  IF ProgramControlType = ctInsert Then
    sKey := FTMP_DOCNO
  else
  IF ProgramControlType = ctModify Then
    sKey := qryAPPPCR_HMAINT_NO.AsString;

  sc := TSQLCreate.Create;
  try
    if sDBGrid3.Tag = 1 Then
    begin
      //INSERT
      nSeq := getIntFromQuery('SELECT ISNULL(MAX(SEQ),0)+1 as MAX_SEQ FROM APPPCRD2 WHERE KEYY = '+QuotedStr(sKey));

      sc.DMLType := dmlInsert;
      sc.SQLHeader('APPPCRD2');
      sc.ADDValue('KEYY', sKey);
      sc.ADDValue('SEQ', nSeq);
    end
    else
    if sDBGrid3.Tag = 2 Then
    begin
      //EDIT
      nSeq := qryDocumentSEQ.AsInteger;

      sc.DMLType := dmlUpdate;
      sc.SQLHeader('APPPCRD2');
      sc.ADDWhere('KEYY', sKey);
      sc.ADDWhere('SEQ', nSeq);
    end;

    sc.ADDValue('DOC_G', edt_DOC_G.Text);
    sc.ADDValue('DOC_D', edt_DOC_D.Text);
    sc.ADDValue('BHS_NO', msk_BHS_NO.Text);
    sc.ADDValue('BNAME', edt_BNAME.Text);
    sc.ADDValue('BNAME1', memo_BNAME1.Text);
    sc.ADDValue('BAMT', curr_BAMT.value);
    sc.ADDValue('BAMTC', edt_BAMTC.Text);
    sc.ADDValue('EXP_DATE', '');
    sc.ADDValue('SHIP_DATE', msk_SHIP_DATE.Text);
    sc.ADDValue('BSIZE1', memo_BSIZE1.Text);
    sc.ADDValue('BAL_NAME1', edt_BAL_NAME1.Text);
    sc.ADDValue('BAL_NAME2', edt_BAL_NAME2.Text);
    sc.ADDValue('BAL_CD', edt_BAL_CD.Text);
    sc.ADDValue('NAT', edt_NAT.Text);

    ExecSQL(sc.CreateSQL);
  finally
    sc.Free;
  end;
//------------------------------------------------------------------------------
// 저장완료
//------------------------------------------------------------------------------
  qryDocument.Close;
  qryDocument.Open;

  qryDocument.Locate('KEYY;SEQ', VarArrayOf([sKey, nSeq]), []);

  sDBGrid3.Tag := 0;
  ButtonControl;
  ReadOnlyControl(sPanel19);

//------------------------------------------------------------------------------
// 변경사항이 있으면 캡션에 * 표시
//------------------------------------------------------------------------------
  sTabSheetDocument.Tag := 1;
  PageControlTagStar;
end;

procedure TUI_APPPCR_NEW_frm.btn_DocSampleClick(Sender: TObject);
var
  EW : TExcelWriter;
begin
  //상품내역 엑셀샘플----------------------------------------------------------
  inherited;
  EW := TExcelWriter.Create;
  try
    EW.Cells(1, 1, '근거서류', taCenter);
    EW.Cells(2, 1, '근거서류번호', taCenter);
    EW.Cells(3, 1, '품목코드(선택)', taCenter);
    EW.Cells(4, 1, '품    명', taCenter);
    EW.Cells(5, 1, '규격(선택)', taCenter);
    EW.Cells(6, 1, 'HS부호(숫자만입력)', taCenter);
    EW.Cells(7, 1, '근거서류발급기관코드', taCenter);
    EW.Cells(8, 1, '근거서류발급기관1', taCenter);
    EW.Cells(9, 1, '근거서류발급기관2', taCenter);
    EW.Cells(10, 1, '수출대상코드', taCenter);
    EW.Cells(11, 1, '총 금 액', taCenter);
    EW.Cells(12, 1, '총금액통화', taCenter);
    EW.Cells(13, 1, '선적기일', taCenter);

    EW.Autofit;

    EW.ShowExcel;
  except
    on E:Exception do
    begin
      EW.Free;
      MessageBox(Self.Handle, PChar('ERROR'#13#10+E.Message), 'ERROR', MB_OK+MB_ICONERROR);
    end;
  end;
end;


procedure TUI_APPPCR_NEW_frm.btn_TaxNewClick(Sender: TObject);
begin
  inherited;
  sDBGrid4.Tag := (Sender as TsSpeedButton).Tag;
  ButtonControl;

  if (not qryTaxBackup.Active) and (ProgramControlType = ctModify) then
  begin
    with qryTaxBackup do
    begin
      Close;
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      Open;
    end;
  end;

  if sDBGrid4.Tag = 1 Then
    ClearControl(sPanel26);

  ReadOnlyControl(sPanel26, false);
end;

procedure TUI_APPPCR_NEW_frm.btn_TaxCancelClick(Sender: TObject);
begin
  inherited;
  sDBGrid4.Tag := 0;
  ButtonControl;
  ReadOnlyControl(sPanel26);
  setTax;
end;

procedure TUI_APPPCR_NEW_frm.btn_TaxDelClick(Sender: TObject);
var
  sc :TSQLCreate;
  nRecNo : Integer;
begin
  inherited;
  if MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '세금계산서 삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then Exit;

  if not qryTaxBackup.Active then
  begin
    with qryTaxBackup do
    begin
      Close;
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      Open;
    end;
  end;

  sc := TSQLCreate.Create;
  try
    sc.DMLType := dmlDelete;
    sc.SQLHeader('APPPCR_TAX');
    sc.ADDWhere('KEYY', qryTaxKEYY.AsString);
    sc.ADDWhere('SEQ', qryTaxSEQ.asInteger);
    ExecSQL(sc.CreateSQL);

    // 순번 재정렬
    ExecSQL(UpdateNewSeqQuery('APPPCR_TAX', qryTaxBackupKEYY.AsString));
  finally
    sc.Free;
  end;
  nRecNo := qryTax.RecNo;
//------------------------------------------------------------------------------
// 삭제완료
//------------------------------------------------------------------------------
  qryTax.Close;
  qryTax.Open;

  if nRecNo > 1 Then
  begin
    qryTax.MoveBy(nRecNo-1);
  end;

  sDBGrid4.Tag := 0;
  ButtonControl;
  ReadOnlyControl(sPanel26);

//------------------------------------------------------------------------------
// 변경사항이 있으면 캡션에 * 표시
//------------------------------------------------------------------------------
  sTabSheetTax.Tag := 1;
  PageControlTagStar;
end;

procedure TUI_APPPCR_NEW_frm.btn_TaxOkClick(Sender: TObject);
var
  sc : TSQLCreate;
  sKey : String;
  nSeq : Integer;
begin
  inherited;
//------------------------------------------------------------------------------
// 저장시작
// 문서전체가 신규추가일경우는 임시번호로 각각추가하여 마지막 저장시에 관리번호로 UPDATE
//------------------------------------------------------------------------------
  IF ProgramControlType = ctInsert Then
    sKey := FTMP_DOCNO
  else
  IF ProgramControlType = ctModify Then
    sKey := qryAPPPCR_HMAINT_NO.AsString;

  sc := TSQLCreate.Create;
  try
    if sDBGrid4.Tag = 1 Then
    begin
      //INSERT
      nSeq := getIntFromQuery('SELECT ISNULL(MAX(SEQ),0)+1 as MAX_SEQ FROM APPPCR_TAX WHERE KEYY = '+QuotedStr(sKey));

      sc.DMLType := dmlInsert;
      sc.SQLHeader('APPPCR_TAX');
      sc.ADDValue('KEYY', sKey);
      sc.ADDValue('SEQ', nSeq);
    end
    else
    if sDBGrid4.Tag = 2 Then
    begin
      //EDIT
      nSeq := qryDocumentSEQ.AsInteger;

      sc.DMLType := dmlUpdate;
      sc.SQLHeader('APPPCR_TAX');
      sc.ADDWhere('KEYY', sKey);
      sc.ADDWhere('SEQ', nSeq);
    end;

    sc.ADDValue('DATEE',  '');
    sc.ADDValue('BILL_NO', edt_BILL_NO.Text);
    sc.ADDValue('BILL_DATE', msk_BILL_DATE.Text);
    sc.ADDValue('ITEM_NAME1', edt_ITEM_NAME1.Text);
    sc.ADDValue('ITEM_NAME2', edt_ITEM_NAME2.Text);
    sc.ADDValue('ITEM_NAME3', edt_ITEM_NAME3.Text);
    sc.ADDValue('ITEM_NAME4', edt_ITEM_NAME4.Text);
    sc.ADDValue('ITEM_NAME5', edt_ITEM_NAME5.Text);
    sc.ADDValue('ITEM_DESC', memo_DESC.Text);
    sc.ADDValue('BILL_AMOUNT', curr_BILL_AMOUNT.value);
    sc.ADDValue('BILL_AMOUNT_UNIT', edt_BILL_AMOUNT_UNIT.Text);
    sc.ADDValue('TAX_AMOUNT', curr_TAX_AMOUNT.Value);
    sc.ADDValue('TAX_AMOUNT_UNIT', edt_TAX_AMOUNT_UNIT.Text);
    sc.ADDValue('QUANTITY', curr_QUANTITY.Value);
    sc.ADDValue('QUANTITY_UNIT', edt_QUANTITY_UNIT.Text);

    ExecSQL(sc.CreateSQL);
  finally
    sc.Free;
  end;
//------------------------------------------------------------------------------
// 저장완료
//------------------------------------------------------------------------------
  qryTax.Close;
  qryTax.Open;

  qryTax.Locate('KEYY;SEQ', VarArrayOf([sKey, nSeq]), []);

  sDBGrid4.Tag := 0;
  ButtonControl;
  ReadOnlyControl(sPanel26);

//------------------------------------------------------------------------------
// 변경사항이 있으면 캡션에 * 표시
//------------------------------------------------------------------------------
  sTabSheetTax.Tag := 1;
  PageControlTagStar;
end;

procedure TUI_APPPCR_NEW_frm.RollbackTax;
var
  sc : TSQLCreate;
  i : Integer;
begin
  sc := TSQLCreate.Create;
  try
    try
      //------------------------------------------------------------------------------
      // 기존 입력된자료 삭제
      //------------------------------------------------------------------------------
      sc.DMLType := dmlDelete;
      sc.SQLHeader('APPPCR_TAX');
      sc.ADDWhere('KEYY', qryTaxKEYY.AsString);
      ExecSQL(sc.CreateSQL);
      //------------------------------------------------------------------------------
      // 복구
      //------------------------------------------------------------------------------
      while not qryTaxBackup.Eof do
      begin
        sc.DMLType := dmlInsert;
        sc.SQLHeader('APPPCR_TAX');
        for i := 0 to qryTaxBackup.FieldCount-1 do
        begin
          case qryTaxBackup.Fields[i].DataType of
            ftString, ftMemo : sc.ADDValue(qryTaxBackup.Fields[i].FieldName, qryTaxBackup.Fields[i].AsString);
            ftInteger : sc.ADDValue(qryTaxBackup.Fields[i].FieldName, qryTaxBackup.Fields[i].asInteger);
            ftBCD, ftFloat, ftCurrency : sc.ADDValue(qryTaxBackup.Fields[i].FieldName, qryTaxBackup.Fields[i].asFloat);
            ftBoolean : sc.ADDValue(qryTaxBackup.Fields[i].FieldName, qryTaxBackup.Fields[i].asBoolean);
          end;
        end;
        ExecSQL(sc.CreateSQL);

        qryTaxBackup.Next;
      end;
    except
      on e:Exception do
      begin
        MessageBox(Self.Handle, PChar('ERROR'#13#10+e.Message), 'RollBack error', MB_OK+MB_ICONERROR);
      end;
    end;
  finally
    sc.Free;
    qryTaxBackup.Close;
  end;
end;

procedure TUI_APPPCR_NEW_frm.btn_DocExcelClick(Sender: TObject);
var
  ER : TExcelReader;
  sc : TSQLCreate;
  ExcelfileName, sKey : String;
  i : Integer;
  tmpStr : String;
begin
  inherited;
  if not excelOpen.Execute then Exit;

  //excelOpen.FileName을 사용해도 되지만 윈도우상에 버그로 글씨가 깨지거나함
  ExcelfileName := excelOpen.Files.Strings[0]; //선택된 엑셀파일이름

  IF MessageBox(Self.Handle, MSG_EXCEL_IMPORT_ALLDATA_DEL, '가져오기확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then Exit;

  IF ProgramControlType = ctInsert Then
    sKey := FTMP_DOCNO
  else
  IF ProgramControlType = ctModify Then
    sKey := qryAPPPCR_HMAINT_NO.AsString;

  if (not qryDocumentBackup.Active) and (ProgramControlType = ctModify) then
  begin
    with qryDocumentBackup do
    begin
      Close;
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      Open;
    end;
  end;

  try
    DMMssql.BeginTrans;
    try
      //------------------------------------------------------------------------------
      // 기존 데이터 삭제
      //------------------------------------------------------------------------------
      sc := TSQLCreate.Create;
      sc.DMLType := dmlDelete;
      sc.SQLHeader('APPPCRD2');
      sc.ADDWhere('KEYY', sKey);
      ExecSQL(sc.CreateSQL);

      ER := TExcelReader.Create(ExcelfileName);

      for i := 2 to ER.RowCount do
      begin
        sc.DMLType := dmlInsert;
        sc.SQLHeader('APPPCRD2');
        sc.ADDValue('KEYY', sKey);
        sc.ADDValue('SEQ', i-1);

        sc.ADDValue('DOC_G',    ER.asString(i,1)); //근거서류
        sc.ADDValue('DOC_D',    ER.asString(i,2)); //근거서류번호
        sc.ADDValue('BNAME',    ER.asString(i,3)); //품명코드
        sc.ADDValue('BNAME1',   ER.asString(i,4)); //품명
        sc.ADDValue('BSIZE1',   ER.asString(i,5)); //규격
        tmpStr := ER.asString(i,6);
        tmpStr := AnsiReplaceText(AnsiReplaceText(tmpStr,'.',''),'-','');
        sc.ADDValue('BHS_NO',   tmpStr); //세번부호
        sc.ADDValue('BAL_CD',   ER.asString(i,7)); //근거서류발급기관코드
        sc.ADDValue('BAL_NAME1',ER.asString(i,8)); //근거서류발급기관
        sc.ADDValue('BAL_NAME2',ER.asString(i,9));
        sc.ADDValue('NAT',      ER.asString(i,10));//수출대상 국가코드
        sc.ADDValue('BAMT',     ER.asString(i,11), vtInteger); //총금액
        sc.ADDValue('BAMTC',    ER.asString(i,12)); //총금액통화
        sc.ADDValue('SHIP_DATE',ER.asString(i,13)); //선적기일

        ExecSQL(sc.CreateSQL);
      end;

      DMMssql.CommitTrans;

    //------------------------------------------------------------------------------
    // 변경사항이 있으면 캡션에 * 표시
    //------------------------------------------------------------------------------
      sTabSheetDocument.Tag := 1;
      PageControlTagStar;

      qryDocument.Close;
      qryDocument.Open;

      MessageBox(Self.Handle, MSG_EXCEL_IMPORT_OK , 'Excel Import Complete', MB_OK+MB_ICONINFORMATION);
    except
      on e:exception do
      begin
        DMMssql.RollbackTrans;
        MessageBox(Self.Handle, PChar('ERROR'#13#10+e.Message), 'Excel Import Error', MB_OK+MB_ICONERROR);
      end;
    end;
  finally
    ER.Free;
    sc.Free;
  end;
end;

procedure TUI_APPPCR_NEW_frm.btn_TaxSampleClick(Sender: TObject);
var
  EW : TExcelWriter;
begin
  //상품내역 엑셀샘플----------------------------------------------------------
  inherited;
  EW := TExcelWriter.Create;
  try
    EW.Cells(1, 1, '세금계산서번호', taCenter);
    EW.Cells(2, 1, '작성일자', taCenter);
    EW.Cells(3, 1, '품목1', taCenter);
    EW.Cells(4, 1, '품목2', taCenter);
    EW.Cells(5, 1, '품목3', taCenter);
    EW.Cells(6, 1, '품목4', taCenter);
    EW.Cells(7, 1, '품목5', taCenter);
    EW.Cells(8, 1, '규격', taCenter);
    EW.Cells(9, 1, '공급가액', taCenter);
    EW.Cells(10, 1, '공급가액통화', taCenter);
    EW.Cells(11, 1, '세액', taCenter);
    EW.Cells(12, 1, '세액통화', taCenter);
    EW.Cells(13, 1, '수량', taCenter);
    EW.Cells(14, 1, '수량단위', taCenter);

    EW.Autofit;

    EW.ShowExcel;
  except
    on E:Exception do
    begin
      EW.Free;
      MessageBox(Self.Handle, PChar('ERROR'#13#10+E.Message), 'ERROR', MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TUI_APPPCR_NEW_frm.btn_TaxExcelClick(Sender: TObject);
var
  ER : TExcelReader;
  sc : TSQLCreate;
  ExcelfileName, sKey : String;
  i : Integer;
  tmpStr : String;
begin
  inherited;
  if not excelOpen.Execute then Exit;

  //excelOpen.FileName을 사용해도 되지만 윈도우상에 버그로 글씨가 깨지거나함
  ExcelfileName := excelOpen.Files.Strings[0]; //선택된 엑셀파일이름

  IF MessageBox(Self.Handle, MSG_EXCEL_IMPORT_ALLDATA_DEL, '가져오기확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL then Exit;

  IF ProgramControlType = ctInsert Then
    sKey := FTMP_DOCNO
  else
  IF ProgramControlType = ctModify Then
    sKey := qryAPPPCR_HMAINT_NO.AsString;

  if (not qryTaxBackup.Active) and (ProgramControlType = ctModify) then
  begin
    with qryTaxBackup do
    begin
      Close;
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      Open;
    end;
  end;

  try
    DMMssql.BeginTrans;
    try
      //------------------------------------------------------------------------------
      // 기존 데이터 삭제
      //------------------------------------------------------------------------------
      sc := TSQLCreate.Create;
      sc.DMLType := dmlDelete;
      sc.SQLHeader('APPPCR_TAX');
      sc.ADDWhere('KEYY', sKey);
      ExecSQL(sc.CreateSQL);

      ER := TExcelReader.Create(ExcelfileName);

      for i := 2 to ER.RowCount do
      begin
        sc.DMLType := dmlInsert;
        sc.SQLHeader('APPPCR_TAX');
        sc.ADDValue('KEYY', sKey);
        sc.ADDValue('SEQ', i-1);

        sc.ADDValue('BILL_NO',    ER.asString(i,1)); //세금계산서번호
        sc.ADDValue('BILL_DATE',  ER.asString(i,2)); //작성일자
        sc.ADDValue('ITEM_NAME1', ER.asString(i,3)); //품목
        sc.ADDValue('ITEM_NAME2', ER.asString(i,4));
        sc.ADDValue('ITEM_NAME3', ER.asString(i,5));
        sc.ADDValue('ITEM_NAME4', ER.asString(i,6));
        sc.ADDValue('ITEM_NAME5', ER.asString(i,7));
        sc.ADDValue('ITEM_DESC',  ER.asString(i,8)); //규격
        sc.ADDValue('BILL_AMOUNT',   ER.asString(i,9), vtInteger);//공급가액
        sc.ADDValue('BILL_AMOUNT_UNIT',ER.asString(i,10));//공급가액통화
        sc.ADDValue('TAX_AMOUNT',    ER.asString(i,11), vtInteger);//세액
        sc.ADDValue('TAX_AMOUNT_UNIT', ER.asString(i,12));//세액통화
        sc.ADDValue('QUANTITY',      ER.asString(i,13), vtInteger);//수량
        sc.ADDValue('QUANTITY_UNIT',   ER.asString(i,14));//수량단위

        ExecSQL(sc.CreateSQL);
      end;

      DMMssql.CommitTrans;

    //------------------------------------------------------------------------------
    // 변경사항이 있으면 캡션에 * 표시
    //------------------------------------------------------------------------------
      sTabSheetTax.Tag := 1;
      PageControlTagStar;

      qryTax.Close;
      qryTax.Open;

      MessageBox(Self.Handle, MSG_EXCEL_IMPORT_OK , 'Excel Import Complete', MB_OK+MB_ICONINFORMATION);
    except
      on e:exception do
      begin
        DMMssql.RollbackTrans;
        MessageBox(Self.Handle, PChar('ERROR'#13#10+e.Message), 'Excel Import Error', MB_OK+MB_ICONERROR);
      end;
    end;
  finally
    ER.Free;
    sc.Free;
  end;
end;
procedure TUI_APPPCR_NEW_frm.edt_BNAMEDblClick(Sender: TObject);
begin
  inherited;
  if sDBGrid3.Tag = 0 Then Exit;

  CD_ITEMLIST_frm := TCD_ITEMLIST_frm.Create(Self);
  try
    if CD_ITEMLIST_frm.OpenDialog(0,Trim(edt_BNAME.Text)) Then
    begin
      edt_BNAME.Text   := CD_ITEMLIST_frm.Values[0];
      msk_BHS_NO.Text  := CD_ITEMLIST_frm.Values[1];
      memo_BNAME1.Text := CD_ITEMLIST_frm.Values[2];
      memo_BSIZE1.Text := CD_ITEMLIST_frm.Values[4];
    end;
  finally
    FreeAndNil(CD_ITEMLIST_frm);
  end;
end;

procedure TUI_APPPCR_NEW_frm.edt_BAL_CDDblClick(Sender: TObject);
begin
  inherited;
  IF sDBGrid3.Tag = 0 Then Exit;

  CD_BANK_frm := TCD_BANK_frm.Create(Self);
  try
    IF CD_BANK_frm.openDialog(0, edt_BAL_CD.Text) Then
    begin
      edt_BAL_CD.Text := CD_BANK_frm.Field('CODE').AsString;
      edt_BAL_NAME1.Text := CD_BANK_frm.Field('BANKNAME1').AsString;
      edt_BAL_NAME2.Text := CD_BANK_frm.Field('BANKBRANCH1').AsString;
    end;
  finally
    FreeAndNil(CD_BANK_frm);
  end;
end;

procedure TUI_APPPCR_NEW_frm.sButton15Click(Sender: TObject);
begin
  inherited;
  edt_BNAMEDblClick(edt_BNAME);
end;

procedure TUI_APPPCR_NEW_frm.btnCopyClick(Sender: TObject);
begin
  inherited;
  if MessageBox(Self.Handle, MSG_COPY_QUESTION, '복사확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  // 문서모드 - 작성으로 변경
  ProgramControlType := ctInsert;
  ReadOnlyControl(sPanel5);
  // 입력잠금 해제
  ReadOnlyControl(sPanel6, False);
  ReadOnlyControl(sPanel7, False);

  // 임시번호부여
  FTMP_DOCNO := 'APPPCR_TMP_'+FormatDateTime('YYMMDDHHNNSSMS', Now);
  edt_TEMPNO.Text := FTMP_DOCNO;

  FCompareDocNo := DMAutoNo.GetDocumentNo('APPPCR');
  edt_MAINT_NO_Header.Text  := FCompareDocNo;
  edt_MAINT_NO_Header.Color := clWhite;

  // 등록일자
  msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
  // 사용자
  edt_userno.Text := LoginData.sID;

  // 검색으로 찾은 데이터의 Detail들을 임시번호로 복사
  DMMssql.BeginTrans;
  try
    with qryCopyDoc do
    begin
      Close;
      // originalKey
      Parameters[0].Value := qryAPPPCR_HMAINT_NO.AsString;
      // NewKey
      Parameters[1].Value := FTMP_DOCNO;
      ExecSQL;
      DMMssql.CommitTrans;

    end;
  except
    on E:Exception do
    begin
      DMMssql.RollbackTrans;
      MessageBox(Self.handle, PChar('ERROR'#13#10+E.Message), 'Copy Error', MB_OK+MB_ICONERROR);
    end;
  end;

  sPanel29.Visible := True;
  ButtonControl;

  ReadDetail;
  ReadDocument;
  ReadTax;

  sPageControl1.ActivePageIndex := 0;
  sPageControl1Change(sPageControl1);
end;

procedure TUI_APPPCR_NEW_frm.Btn_PrintClick(Sender: TObject);
begin
  inherited;
  if qryAPPPCR_H.RecordCount = 0 Then Exit;

  APPPCR_PRINT_FRM := TAPPPCR_PRINT_FRM.Create(Self);
  APPPCR_PRINT1_FRM := TAPPPCR_PRINT1_FRM.Create(Self);
  APPPCR_PRINT2_FRM := TAPPPCR_PRINT2_FRM.Create(Self);

  Preview_frm := TPreview_frm.Create(Self);
  try
    APPPCR_PRINT_FRM.DocNo := qryAPPPCR_HMAINT_NO.AsString;
    APPPCR_PRINT1_FRM.DocNo := qryAPPPCR_HMAINT_NO.AsString;
    APPPCR_PRINT2_FRM.DocNo := qryAPPPCR_HMAINT_NO.AsString;
    QRCompositeReport1.Prepare;
    APPPCR_PRINT2_FRM.Totalpage := APPPCR_PRINT2_FRM.QRPrinter.PageCount;

    Preview_frm.CompositeRepost := QRCompositeReport1;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(APPPCR_PRINT_FRM);
    FreeAndNil(APPPCR_PRINT1_FRM);
    FreeAndNil(APPPCR_PRINT2_FRM);
  end;

end;

procedure TUI_APPPCR_NEW_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(APPPCR_PRINT_FRM);
  QRCompositeReport1.Reports.Add(APPPCR_PRINT1_FRM);
  QRCompositeReport1.Reports.Add(APPPCR_PRINT2_FRM);
end;

procedure TUI_APPPCR_NEW_frm.Btn_ReadyClick(Sender: TObject);
begin
  if qryAPPPCR_HCHK2.AsString = '0' then
    raise Exception.Create('임시문서는 전송할 수 없습니다');
  if qryAPPPCR_HCHK2.AsString = '' then
    raise Exception.Create('정상적으로 저장된 문서가 아닙니다.');
  if qryAPPPCR_HCHK3.AsString <> '' then
  begin
//    if StrToInt(RightStr(qryListCHK3.AsString, 1)) = 5 then
//    begin
//      if ConfirmMessage('전송완료된 문서입니다. 재전송 하시겠습니까?') then
//      begin
//        Exit;
//      end;
//    end
//    else
      ReadyDoc;
  end
  else
    ReadyDoc;

end;

procedure TUI_APPPCR_NEW_frm.ReadyDoc;
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
  nIdx : integer;
begin
  inherited;
  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryAPPPCR_HMAINT_NO.AsString;
      RecvFlat := APPPCR(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Close;
        Parameters.ParamByName('DOCID').Value := 'APPPCR';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryAPPPCR_HCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APPPCR_H';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        nIdx := qryAPPPCR_H.RecNo;

        ExecSQL;
        qryAPPPCR_H.Close;
        qryAPPPCR_H.Open;

        if nIdx > 1 Then
          qryAPPPCR_H.MoveBy(nIdx);


      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_APPPCR_NEW_frm.sButton18Click(Sender: TObject);
begin
  inherited;
  edt_BAL_CDDblClick(edt_BAL_CD);
end;

procedure TUI_APPPCR_NEW_frm.sButton2Click(Sender: TObject);
var
  sKey : String;
begin
  inherited;
  IF sDBGrid2.Tag <> 0 Then
  begin
    ShowMessage('상품내역이 입력중입니다. 저장 후에 진행하세요');
    Exit;
  end;

  IF ProgramControlType = ctInsert Then
    sKey := FTMP_DOCNO
  else
  IF ProgramControlType = ctModify Then
    sKey := qryAPPPCR_HMAINT_NO.AsString;

  with qryCalcTotal do
  begin
    Close;
    Parameters.ParamByName('KEY').Value := sKey;
    Open;

    try
      IF qryCalcTotal.RecordCount = 0 Then
      begin
        MessageBox(Self.Handle,MSG_ERR_CALC,'계산오류',MB_OK+MB_ICONINFORMATION);
      end
      else
      begin
        curr_TQTY.Text := FormatFloat('#,0.000',qryCalcTotal.FieldByName('TQTY').AsCurrency);
//        edt_TQTYC.Text := qryCalcTotal.FieldByName('QTYC').AsString;
        curr_TAMT1.Text := FormatFloat('#,0.000',qryCalcTotal.FieldByName('TAMT1').AsCurrency);
        edt_TAMT1C.Text := qryCalcTotal.FieldByName('AMT1C').AsString;

        IF edt_TAMT1C.Text = 'USD' Then
          curr_TAMT2.Text := FormatFloat('#,0.000',qryCalcTotal.FieldByName('TAMT1').AsCurrency);

      end;
    finally
      qryCalcTotal.Close;
    end;
  end;
end;

procedure TUI_APPPCR_NEW_frm.btn_SendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_APPPCR_NEW_frm.FormActivate(Sender: TObject);
var
  BMK : Pointer;
begin
  inherited;
  BMK := qryAPPPCR_H.GetBookmark;
  qryAPPPCR_H.Close;
  qryAPPPCR_H.Open;
  try
  IF qryAPPPCR_H.BookmarkValid(BMK) Then
    qryAPPPCR_H.GotoBookmark(BMK);
  finally
    qryAPPPCR_H.FreeBookmark(BMK);
  end;
end;

procedure TUI_APPPCR_NEW_frm.curr_QTYExit(Sender: TObject);
var
  sumValue : Extended;
begin
  inherited;
  if sDBGrid2.Tag = 0 Then Exit;

  sumValue := curr_PRI2.Value * curr_QTY.Value;
  if (CompareExtended(curr_AMT1.Value, sumValue) <> 0) and (sCheckBox2.Checked) then
  begin
    curr_AMT1.Value := sumValue;
  end;
end;

end.
