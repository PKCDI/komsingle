unit UI_LOGUAR_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_APPLOG_NEW, DB, ADODB, sSkinProvider, StdCtrls, sComboBox,
  sMemo, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ExtCtrls, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sButton, sLabel, sSpeedButton, sPanel, StrUtils, DateUtils;

type
  TUI_LOGUAR_NEW_frm = class(TUI_APPLOG_NEW_FRM)
    msk_LGDATE: TsMaskEdit;
    edt_LGNO: TsEdit;
    Shape2: TShape;
    Shape3: TShape;
    msk_LCDATE: TsMaskEdit;
    edt_USERMAINTNO: TsEdit;
    qryListLG_NO: TStringField;
    qryListLG_DATE: TStringField;
    qryListLC_DATE: TStringField;
    qryListUSERMAINT_NO: TStringField;
    procedure FormShow(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
  private
  protected
    procedure ReadData; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_LOGUAR_NEW_frm: TUI_LOGUAR_NEW_frm;

implementation

uses ChildForm, LOGUAR_PRINT, Preview, MSSQL, MessageDefine;

{$R *.dfm}

{ TUI_LOGUAR_NEW_frm }


procedure TUI_LOGUAR_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  sBitBtn1Click(nil);


  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_LOGUAR_NEW_frm.ReadData;
begin
  inherited;
  CM_INDEX(com_func, [':'], 0, qryListMESSAGE2.AsString, 2);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE3.AsString, 2);

  msk_LGDATE.Text := qryListLG_DATE.AsString;
  edt_LGNO.Text   := qryListLG_NO.AsString;
  msk_LCDATE.Text := qryListLC_DATE.AsString;
  edt_USERMAINTNO.Text := qryListUSERMAINT_NO.AsString;
end;

procedure TUI_LOGUAR_NEW_frm.btnPrintClick(Sender: TObject);
begin
//  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  LOGUAR_PRINT_frm := TLOGUAR_PRINT_frm.Create(Self);
  Preview_frm := TPreview_frm.Create(Self);
  try
    LOGUAR_PRINT_frm.MaintNo := qryListMAINT_NO.AsString;
    Preview_frm.Report := LOGUAR_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(LOGUAR_PRINT_frm);
  end;

end;

procedure TUI_LOGUAR_NEW_frm.btnDelClick(Sender: TObject);
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  IF not DMMssql.inTrans Then
    DMMssql.KISConnect.BeginTrans;
  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
    try
      try
        IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+'수입화물 선취보증(인도승락)서'#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;

          SQL.Text :=  'DELETE FROM LOGUAR WHERE MAINT_NO =' + QuotedStr(maint_no);
          ExecSQL;

          //트랜잭션 커밋
          DMMssql.CommitTrans;

          qryList.Close;
          qryList.Open;

          IF qryList.RecordCount > 1 Then
            qryList.MoveBy(nCursor-1);
            
        end
        else
        begin
          if DMMssql.inTrans then DMMssql.RollbackTrans;
        end;
      except
        on E:Exception do
        begin
          DMMssql.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
     Close;
     Free;
    end;
  end; 
end;
procedure TUI_LOGUAR_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  inherited;
  Action := caFree;
  UI_LOGUAR_NEW_frm := nil;
end;

procedure TUI_LOGUAR_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

end.
