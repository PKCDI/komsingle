inherited UI_APPSPC_NEW_DOC1_frm: TUI_APPSPC_NEW_DOC1_frm
  Left = 1026
  Top = 165
  Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077')-'#47932#54408#49688#47161#51613#47749#49436
  ClientHeight = 679
  ClientWidth = 784
  FormStyle = fsNormal
  OldCreateOrder = True
  Visible = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel17: TsPanel [0]
    Left = 0
    Top = 0
    Width = 784
    Height = 679
    Align = alClient
    
    TabOrder = 0
    object sPanel14: TsPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 491
      Align = alTop
      
      TabOrder = 0
      object sSpeedButton17: TsSpeedButton
        Left = 386
        Top = 41
        Width = 11
        Height = 426
        ButtonStyle = tbsDivider
      end
      object msk_VAL_DATE: TsMaskEdit
        Left = 279
        Top = 141
        Width = 74
        Height = 23
        CharCase = ecUpperCase
        Ctl3D = True
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 5
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #50976#54952#44592#51068
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object msk_ACC_DATE: TsMaskEdit
        Left = 104
        Top = 141
        Width = 74
        Height = 23
        CharCase = ecUpperCase
        Ctl3D = True
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 4
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51064#49688#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object msk_ISS_DATE: TsMaskEdit
        Left = 104
        Top = 117
        Width = 74
        Height = 23
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #48156#44553#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        SkinData.SkinSection = 'EDIT'
      end
      object msk_BSN_HSCODE: TsMaskEdit
        Left = 104
        Top = 93
        Width = 73
        Height = 23
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        EditMask = '9999.99-9999;0;'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 12
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = 'HS'#48512#54840
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object edt_SE_CODE: TsEdit
        Tag = 101
        Left = 104
        Top = 173
        Width = 49
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = 12775866
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 6
        OnDblClick = CODEDblClick
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44277#44553#51088' '#49345#54840
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_SE_SNAME: TsEdit
        Left = 154
        Top = 173
        Width = 200
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 7
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_LR_NO: TsEdit
        Left = 504
        Top = 45
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 15
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #45236#44397#49888#50857#51109' '#48264#54840
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object msk_GET_DATE: TsMaskEdit
        Left = 504
        Top = 69
        Width = 74
        Height = 23
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 16
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #47932#54408#51064#46020#44592#51068
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object msk_EXP_DATE: TsMaskEdit
        Left = 679
        Top = 68
        Width = 74
        Height = 23
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 17
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #50976#54952#44592#51068
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object edt_LA_BANK: TsEdit
        Tag = 103
        Left = 504
        Top = 101
        Width = 49
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = 12775866
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 18
        OnDblClick = CODEDblClick
        OnExit = edt_LA_BANKExit
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44060#49444#51008#54665#53076#46300
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_LA_BANKNAME: TsEdit
        Left = 554
        Top = 101
        Width = 200
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 19
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_LA_ELEC: TsEdit
        Left = 504
        Top = 149
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 21
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51204#51088#49436#47749
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_LA_BANKBU: TsEdit
        Left = 504
        Top = 125
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 20
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51648#51216#47749
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_ISS_AMTC: TsEdit
        Tag = 104
        Left = 504
        Top = 221
        Width = 49
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = 12775866
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 24
        OnDblClick = CODEDblClick
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44060#49444#44552#50529'('#50808#54868')'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_ISS_EXPAMTC: TsEdit
        Tag = 105
        Left = 504
        Top = 269
        Width = 49
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = 12775866
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 27
        OnDblClick = CODEDblClick
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51064#49688#44552#50529'('#50808#54868')'
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_ISS_TOTAMTC: TsEdit
        Tag = 107
        Left = 504
        Top = 317
        Width = 49
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = 12775866
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 31
        OnDblClick = CODEDblClick
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #52509#44552#50529
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_BY_CODE: TsEdit
        Tag = 102
        Left = 104
        Top = 205
        Width = 49
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = 12775866
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 8
        OnDblClick = CODEDblClick
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #47932#54408#49688#47161#51064' '#49345#54840
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_BY_SNAME: TsEdit
        Left = 154
        Top = 205
        Width = 200
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 9
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_BY_NAME: TsEdit
        Left = 104
        Top = 229
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 10
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #45824#54364#51088#47749
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_BY_ELEC: TsEdit
        Left = 104
        Top = 253
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 11
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51204#51088#49436#47749
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_BY_ADDR1: TsEdit
        Left = 104
        Top = 277
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 12
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51452#49548
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_BY_ADDR2: TsEdit
        Left = 104
        Top = 299
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 13
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_BY_ADDR3: TsEdit
        Left = 104
        Top = 321
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 14
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_DOC_NO: TsEdit
        Left = 104
        Top = 45
        Width = 226
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #47928#49436#48264#54840
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_LR_NO2: TsEdit
        Left = 104
        Top = 69
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        OnDblClick = btnAdd_IMDClick
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #48156#44553#48264#54840
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK2_1: TsEdit
        Left = 104
        Top = 353
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 33
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44592#53440#51312#44148
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK2_2: TsEdit
        Left = 104
        Top = 375
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 34
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK2_3: TsEdit
        Left = 104
        Top = 397
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 35
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK2_4: TsEdit
        Left = 104
        Top = 419
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 36
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK2_5: TsEdit
        Left = 104
        Top = 441
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 37
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK1_1: TsEdit
        Left = 504
        Top = 353
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 38
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #52280#51312#49324#54637
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK1_2: TsEdit
        Left = 504
        Top = 375
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 39
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK1_3: TsEdit
        Left = 504
        Top = 397
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 40
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK1_4: TsEdit
        Left = 504
        Top = 419
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 41
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_REMARK1_5: TsEdit
        Left = 504
        Top = 441
        Width = 250
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = clWhite
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 42
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object edt_TOTCNTC: TsEdit
        Tag = 106
        Left = 504
        Top = 293
        Width = 49
        Height = 23
        HelpContext = 1
        CharCase = ecUpperCase
        Color = 12775866
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 29
        OnDblClick = CODEDblClick
        SkinData.CustomColor = True
        SkinData.SkinSection = 'EDIT'
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #52509#49688#47049
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
      end
      object btnSave: TsButton
        Tag = 1
        Left = 640
        Top = 3
        Width = 70
        Height = 37
        Cursor = crHandPoint
        Caption = #51200#51109
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 43
        TabStop = False
        OnClick = btnSaveClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 7
        ContentMargin = 8
      end
      object btnCancel: TsButton
        Tag = 2
        Left = 717
        Top = 3
        Width = 65
        Height = 37
        Cursor = crHandPoint
        Caption = #52712#49548
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ModalResult = 2
        ParentFont = False
        TabOrder = 44
        TabStop = False
        OnClick = btnCancelClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 18
        ContentMargin = 8
      end
      object edt_EXCH: TsCurrencyEdit
        Left = 504
        Top = 173
        Width = 250
        Height = 23
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 22
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #47588#47588#44592#51456#50984
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
      end
      object edt_ISS_AMT: TsCurrencyEdit
        Left = 504
        Top = 197
        Width = 250
        Height = 23
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 23
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44060#49444#44552#50529
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
      end
      object edt_ISS_EXPAMT: TsCurrencyEdit
        Left = 504
        Top = 245
        Width = 250
        Height = 23
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 26
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51064#49688#44552#50529
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
      end
      object edt_ISS_AMTU: TsCurrencyEdit
        Left = 554
        Top = 221
        Width = 200
        Height = 23
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 25
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
      end
      object edt_ISS_EXPAMTU: TsCurrencyEdit
        Left = 554
        Top = 269
        Width = 200
        Height = 23
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 28
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
      end
      object edt_TOTCNT: TsCurrencyEdit
        Left = 554
        Top = 293
        Width = 200
        Height = 23
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 30
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
      end
      object edt_ISS_TOTAMT: TsCurrencyEdit
        Left = 554
        Top = 317
        Width = 200
        Height = 23
        Ctl3D = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 32
        BoundLabel.ParentFont = False
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
      end
      object sBitBtn26: TsBitBtn
        Tag = 102
        Left = 331
        Top = 45
        Width = 23
        Height = 23
        Cursor = crHandPoint
        TabOrder = 45
        TabStop = False
        OnClick = sBitBtn26Click
        ImageIndex = 25
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
    end
    object sPanel15: TsPanel
      Left = 1
      Top = 492
      Width = 782
      Height = 186
      Align = alClient
      
      TabOrder = 1
      object sPanel19: TsPanel
        Left = 1
        Top = 1
        Width = 780
        Height = 23
        SkinData.ColorTone = clGradientInactiveCaption
        Align = alTop
        Caption = #47932#54408#49688#47161#51613#47749#49436' '#54408#47785#45236#50669
        Color = clBtnText
        
        TabOrder = 0
      end
      object sPanel18: TsPanel
        Left = 1
        Top = 24
        Width = 373
        Height = 161
        Align = alLeft
        
        TabOrder = 1
        object sDBGrid4: TsDBGrid
          Left = 1
          Top = 32
          Width = 371
          Height = 128
          Align = alClient
          Color = clWhite
          DataSource = dsListD2
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'LINE_NO'
              Title.Alignment = taCenter
              Title.Caption = #48264#54840
              Width = 34
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'HS_NO'
              Title.Alignment = taCenter
              Title.Caption = 'HS'#48512#54840
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049#13#10
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRICE'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUP_AMT'
              Title.Alignment = taCenter
              Title.Caption = #44552#50529
              Width = 72
              Visible = True
            end>
        end
        object sPanel2: TsPanel
          Left = 1
          Top = 1
          Width = 371
          Height = 31
          Align = alTop
          
          TabOrder = 1
          object btnEdtD2: TsBitBtn
            Tag = 3
            Left = 148
            Top = -1
            Width = 75
            Height = 32
            Caption = #49688#51221
            TabOrder = 0
            OnClick = btnEdtD2Click
            ImageIndex = 3
            Images = DMICON.System18
          end
          object btnDelD2: TsBitBtn
            Tag = 5
            Left = 296
            Top = -1
            Width = 75
            Height = 32
            Caption = #49325#51228
            TabOrder = 1
            OnClick = btnDelD2Click
            ImageIndex = 1
            Images = DMICON.System18
          end
          object btnSaveD2: TsBitBtn
            Tag = 4
            Left = 222
            Top = -1
            Width = 75
            Height = 32
            Caption = #51200#51109
            Enabled = False
            TabOrder = 2
            OnClick = btnSaveD2Click
            ImageIndex = 11
            Images = DMICON.System18
          end
          object btnCancelD2: TsBitBtn
            Tag = 2
            Left = 74
            Top = -1
            Width = 75
            Height = 32
            Caption = #52712#49548
            Enabled = False
            TabOrder = 3
            OnClick = btnCancelD2Click
            ImageIndex = 18
            Images = DMICON.System18
          end
          object btnAddD2: TsBitBtn
            Tag = 1
            Left = 0
            Top = -1
            Width = 75
            Height = 32
            Caption = #51077#47141
            TabOrder = 4
            OnClick = btnAddD2Click
            ImageIndex = 2
            Images = DMICON.System18
          end
        end
      end
      object sPanel16: TsPanel
        Left = 374
        Top = 24
        Width = 407
        Height = 161
        Align = alClient
        
        TabOrder = 2
        object btnAdd_SIZE: TsSpeedButton
          Left = 367
          Top = 58
          Width = 22
          Height = 23
          Caption = '+'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          OnClick = btnAdd_SIZEClick
        end
        object btnAdd_IMD: TsSpeedButton
          Left = 367
          Top = 34
          Width = 22
          Height = 23
          Caption = '+'
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          OnClick = btnAdd_IMDClick
        end
        object edt_LINE_NO: TsEdit
          Left = 67
          Top = 10
          Width = 42
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51068#47144#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_HS_NO: TsMaskEdit
          Left = 257
          Top = 10
          Width = 93
          Height = 23
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          Enabled = False
          EditMask = '9999.99-9999;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 12
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'HS'#48512#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_QTYC: TsEdit
          Tag = 108
          Left = 67
          Top = 82
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#47049
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_TOTQTYC: TsEdit
          Tag = 109
          Left = 257
          Top = 82
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#47049#49548#44228
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_PRICEC: TsEdit
          Tag = 110
          Left = 257
          Top = 106
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#51456#49688#47049
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_SUP_AMTC: TsEdit
          Tag = 111
          Left = 67
          Top = 130
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44552#50529
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_SUP_TOTAMTC: TsEdit
          Tag = 112
          Left = 257
          Top = 130
          Width = 42
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 12775866
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          OnDblClick = CODEDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44552#50529#49548#44228
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_SIZE: TsEdit
          Left = 67
          Top = 58
          Width = 299
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          OnExit = edt_SIZEExit
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44508#44201
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_IMD_CODE: TsEdit
          Left = 67
          Top = 34
          Width = 299
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          OnExit = edt_IMD_CODEExit
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54408#47785
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_QTY: TsCurrencyEdit
          Left = 110
          Top = 82
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_SUP_AMT: TsCurrencyEdit
          Left = 110
          Top = 130
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_TOTQTY: TsCurrencyEdit
          Left = 300
          Top = 82
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_PRICE_G: TsCurrencyEdit
          Left = 300
          Top = 106
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_SUP_TOTAMT: TsCurrencyEdit
          Left = 300
          Top = 130
          Width = 89
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
        object edt_PRICE: TsCurrencyEdit
          Left = 67
          Top = 106
          Width = 132
          Height = 23
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45800#44032
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          DisplayFormat = '###,###,##0.00;-###,###,##0.00;0'
        end
      end
    end
  end
  object sPanel3: TsPanel [1]
    Left = 411
    Top = 328
    Width = 330
    Height = 269
    
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object sLabel2: TsLabel
      Left = 153
      Top = 5
      Width = 24
      Height = 15
      Caption = #44508#44201
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object edt_SIZE1: TsEdit
      Left = 15
      Top = 27
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 0
      OnExit = edt_SIZE1Exit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE2: TsEdit
      Left = 15
      Top = 49
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE3: TsEdit
      Left = 15
      Top = 71
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE4: TsEdit
      Left = 15
      Top = 93
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sButton2: TsButton
      Tag = 2
      Left = 265
      Top = 5
      Width = 65
      Height = 17
      Cursor = crHandPoint
      Caption = #45803#44592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 17
      ContentMargin = 8
    end
    object edt_SIZE5: TsEdit
      Left = 15
      Top = 115
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 4
      OnExit = edt_IMD_CODE1Exit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE6: TsEdit
      Left = 15
      Top = 137
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 5
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE7: TsEdit
      Left = 15
      Top = 159
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 6
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE8: TsEdit
      Left = 15
      Top = 181
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 7
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE9: TsEdit
      Left = 15
      Top = 203
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 8
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_SIZE10: TsEdit
      Left = 15
      Top = 225
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 9
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
  end
  object sPanel1: TsPanel [2]
    Left = 411
    Top = 440
    Width = 330
    Height = 133
    
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object sLabel1: TsLabel
      Left = 153
      Top = 5
      Width = 24
      Height = 15
      Caption = #54408#47785
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
    end
    object edt_IMD_CODE1: TsEdit
      Left = 15
      Top = 27
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 0
      OnExit = edt_IMD_CODE1Exit
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_IMD_CODE2: TsEdit
      Left = 15
      Top = 49
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_IMD_CODE3: TsEdit
      Left = 15
      Top = 71
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_IMD_CODE4: TsEdit
      Left = 15
      Top = 93
      Width = 299
      Height = 23
      HelpContext = 1
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      MaxLength = 35
      ParentCtl3D = False
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.ParentFont = False
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sButton1: TsButton
      Tag = 2
      Left = 265
      Top = 5
      Width = 65
      Height = 17
      Cursor = crHandPoint
      Caption = #45803#44592
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      TabStop = False
      OnClick = sButton1Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 17
      ContentMargin = 8
    end
  end
  object qryListD2: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterScroll = qryListD2AfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOC_GUBUN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT * FROM APPSPC_D2 WHERE KEYY = :KEYY and DOC_GUBUN = :DOC_' +
        'GUBUN and SEQ = :SEQ')
    Left = 48
    Top = 560
    object qryListD2KEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryListD2DOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryListD2SEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryListD2LINE_NO: TStringField
      FieldName = 'LINE_NO'
      Size = 8
    end
    object qryListD2HS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 35
    end
    object qryListD2DE_DATE: TStringField
      FieldName = 'DE_DATE'
      Size = 35
    end
    object qryListD2IMD_CODE1: TStringField
      FieldName = 'IMD_CODE1'
      Size = 35
    end
    object qryListD2IMD_CODE2: TStringField
      FieldName = 'IMD_CODE2'
      Size = 35
    end
    object qryListD2IMD_CODE3: TStringField
      FieldName = 'IMD_CODE3'
      Size = 35
    end
    object qryListD2IMD_CODE4: TStringField
      FieldName = 'IMD_CODE4'
      Size = 35
    end
    object qryListD2SIZE1: TStringField
      FieldName = 'SIZE1'
      Size = 70
    end
    object qryListD2SIZE2: TStringField
      FieldName = 'SIZE2'
      Size = 70
    end
    object qryListD2SIZE3: TStringField
      FieldName = 'SIZE3'
      Size = 70
    end
    object qryListD2SIZE4: TStringField
      FieldName = 'SIZE4'
      Size = 70
    end
    object qryListD2SIZE5: TStringField
      FieldName = 'SIZE5'
      Size = 70
    end
    object qryListD2SIZE6: TStringField
      FieldName = 'SIZE6'
      Size = 70
    end
    object qryListD2SIZE7: TStringField
      FieldName = 'SIZE7'
      Size = 70
    end
    object qryListD2SIZE8: TStringField
      FieldName = 'SIZE8'
      Size = 70
    end
    object qryListD2SIZE9: TStringField
      FieldName = 'SIZE9'
      Size = 70
    end
    object qryListD2SIZE10: TStringField
      FieldName = 'SIZE10'
      Size = 70
    end
    object qryListD2QTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '###,###,##0.00;0;'
      Precision = 18
    end
    object qryListD2QTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryListD2TOTQTY: TBCDField
      FieldName = 'TOTQTY'
      Precision = 18
    end
    object qryListD2TOTQTYC: TStringField
      FieldName = 'TOTQTYC'
      Size = 3
    end
    object qryListD2PRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '###,###,##0.00;0;'
      Precision = 18
    end
    object qryListD2PRICE_G: TBCDField
      FieldName = 'PRICE_G'
      Precision = 18
    end
    object qryListD2PRICEC: TStringField
      FieldName = 'PRICEC'
      Size = 3
    end
    object qryListD2CUX_RATE: TBCDField
      FieldName = 'CUX_RATE'
      Precision = 18
    end
    object qryListD2SUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      DisplayFormat = '###,###,##0.00;0;'
      Precision = 18
    end
    object qryListD2SUP_AMTC: TStringField
      FieldName = 'SUP_AMTC'
      Size = 3
    end
    object qryListD2VB_TAX: TBCDField
      FieldName = 'VB_TAX'
      Precision = 18
    end
    object qryListD2VB_TAXC: TStringField
      FieldName = 'VB_TAXC'
      Size = 3
    end
    object qryListD2VB_AMT: TBCDField
      FieldName = 'VB_AMT'
      Precision = 18
    end
    object qryListD2VB_AMTC: TStringField
      FieldName = 'VB_AMTC'
      Size = 3
    end
    object qryListD2SUP_TOTAMT: TBCDField
      FieldName = 'SUP_TOTAMT'
      Precision = 18
    end
    object qryListD2SUP_TOTAMTC: TStringField
      FieldName = 'SUP_TOTAMTC'
      Size = 3
    end
    object qryListD2VB_TOTTAX: TBCDField
      FieldName = 'VB_TOTTAX'
      Precision = 18
    end
    object qryListD2VB_TOTTAXC: TStringField
      FieldName = 'VB_TOTTAXC'
      Size = 3
    end
    object qryListD2VB_TOTAMT: TBCDField
      FieldName = 'VB_TOTAMT'
      Precision = 18
    end
    object qryListD2VB_TOTAMTC: TStringField
      FieldName = 'VB_TOTAMTC'
      Size = 3
    end
    object qryListD2BIGO1: TStringField
      FieldName = 'BIGO1'
      Size = 70
    end
    object qryListD2BIGO2: TStringField
      FieldName = 'BIGO2'
      Size = 70
    end
    object qryListD2BIGO3: TStringField
      FieldName = 'BIGO3'
      Size = 70
    end
    object qryListD2BIGO4: TStringField
      FieldName = 'BIGO4'
      Size = 70
    end
    object qryListD2BIGO5: TStringField
      FieldName = 'BIGO5'
      Size = 70
    end
  end
  object dsListD2: TDataSource
    DataSet = qryListD2
    Left = 88
    Top = 560
  end
  object qryDOCClick: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, HS_CHK, HS_NO, NAME_CHK, NAME_COD, NAME, NAME1' +
        ', SIZE, SIZE1, QTY, QTY_G, QTYG, QTYG_G, PRICE, PRICE_G, AMT, AM' +
        'T_G, STQTY, STQTY_G, STAMT, STAMT_G'
      'FROM LOCRC1_D'
      'WHERE KEYY = :KEYY')
    Left = 360
    Top = 48
  end
  object qryD1Cancel: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'DOC_GUBUN'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'KEYY2'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'DELETE FROM APPSPC_D2 WHERE KEYY = :KEYY and SEQ = :SEQ and DOC_' +
        'GUBUN = :DOC_GUBUN '
      'UPDATE APPSPC_D2 SET KEYY = :KEYY2 WHERE KEYY = '#39'0'#39)
    Left = 752
    Top = 48
  end
end
