unit UI_APPSPC_NEW_DOC1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, Buttons, sSpeedButton, Grids, DBGrids, acDBGrid,
  StdCtrls, sMemo, sEdit, Mask, sMaskEdit, sBitBtn, ExtCtrls, sPanel,
  sSkinProvider, sButton, DB, ADODB, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, sLabel, sToolEdit;

type
  TUI_APPSPC_NEW_DOC1_frm = class(TChildForm_frm)
    sPanel17: TsPanel;
    sPanel14: TsPanel;
    msk_VAL_DATE: TsMaskEdit;
    msk_ACC_DATE: TsMaskEdit;
    msk_ISS_DATE: TsMaskEdit;
    msk_BSN_HSCODE: TsMaskEdit;
    edt_SE_CODE: TsEdit;
    edt_SE_SNAME: TsEdit;
    edt_LR_NO: TsEdit;
    msk_GET_DATE: TsMaskEdit;
    msk_EXP_DATE: TsMaskEdit;
    edt_LA_BANK: TsEdit;
    edt_LA_BANKNAME: TsEdit;
    edt_LA_ELEC: TsEdit;
    edt_LA_BANKBU: TsEdit;
    edt_ISS_AMTC: TsEdit;
    edt_ISS_EXPAMTC: TsEdit;
    edt_ISS_TOTAMTC: TsEdit;
    edt_BY_CODE: TsEdit;
    edt_BY_SNAME: TsEdit;
    edt_BY_NAME: TsEdit;
    edt_BY_ELEC: TsEdit;
    edt_BY_ADDR1: TsEdit;
    edt_BY_ADDR2: TsEdit;
    edt_BY_ADDR3: TsEdit;
    edt_DOC_NO: TsEdit;
    edt_LR_NO2: TsEdit;
    sPanel15: TsPanel;
    sPanel19: TsPanel;
    sPanel18: TsPanel;
    sDBGrid4: TsDBGrid;
    sPanel16: TsPanel;
    edt_LINE_NO: TsEdit;
    edt_HS_NO: TsMaskEdit;
    edt_QTYC: TsEdit;
    edt_TOTQTYC: TsEdit;
    edt_PRICEC: TsEdit;
    edt_SUP_AMTC: TsEdit;
    edt_SUP_TOTAMTC: TsEdit;
    sPanel2: TsPanel;
    edt_REMARK2_1: TsEdit;
    edt_REMARK2_2: TsEdit;
    edt_REMARK2_3: TsEdit;
    edt_REMARK2_4: TsEdit;
    edt_REMARK2_5: TsEdit;
    edt_REMARK1_1: TsEdit;
    edt_REMARK1_2: TsEdit;
    edt_REMARK1_3: TsEdit;
    edt_REMARK1_4: TsEdit;
    edt_REMARK1_5: TsEdit;
    sSpeedButton17: TsSpeedButton;
    edt_TOTCNTC: TsEdit;
    btnSave: TsButton;
    btnCancel: TsButton;
    qryListD2: TADOQuery;
    dsListD2: TDataSource;
    btnAdd_SIZE: TsSpeedButton;
    btnAdd_IMD: TsSpeedButton;
    edt_SIZE: TsEdit;
    edt_IMD_CODE: TsEdit;
    edt_EXCH: TsCurrencyEdit;
    edt_ISS_AMT: TsCurrencyEdit;
    edt_ISS_EXPAMT: TsCurrencyEdit;
    edt_ISS_AMTU: TsCurrencyEdit;
    edt_ISS_EXPAMTU: TsCurrencyEdit;
    edt_TOTCNT: TsCurrencyEdit;
    edt_ISS_TOTAMT: TsCurrencyEdit;
    edt_QTY: TsCurrencyEdit;
    edt_SUP_AMT: TsCurrencyEdit;
    edt_TOTQTY: TsCurrencyEdit;
    edt_PRICE_G: TsCurrencyEdit;
    edt_SUP_TOTAMT: TsCurrencyEdit;
    edt_PRICE: TsCurrencyEdit;
    sPanel1: TsPanel;
    edt_IMD_CODE1: TsEdit;
    edt_IMD_CODE2: TsEdit;
    edt_IMD_CODE3: TsEdit;
    edt_IMD_CODE4: TsEdit;
    sLabel1: TsLabel;
    sButton1: TsButton;
    sPanel3: TsPanel;
    sLabel2: TsLabel;
    edt_SIZE1: TsEdit;
    edt_SIZE2: TsEdit;
    edt_SIZE3: TsEdit;
    edt_SIZE4: TsEdit;
    sButton2: TsButton;
    edt_SIZE5: TsEdit;
    edt_SIZE6: TsEdit;
    edt_SIZE7: TsEdit;
    edt_SIZE8: TsEdit;
    edt_SIZE9: TsEdit;
    edt_SIZE10: TsEdit;
    btnAddD2: TsBitBtn;
    btnCancelD2: TsBitBtn;
    btnEdtD2: TsBitBtn;
    btnSaveD2: TsBitBtn;
    btnDelD2: TsBitBtn;
    qryDOCClick: TADOQuery;
    sBitBtn26: TsBitBtn;
    qryD1Cancel: TADOQuery;
    qryListD2KEYY: TStringField;
    qryListD2DOC_GUBUN: TStringField;
    qryListD2SEQ: TIntegerField;
    qryListD2LINE_NO: TStringField;
    qryListD2HS_NO: TStringField;
    qryListD2DE_DATE: TStringField;
    qryListD2IMD_CODE1: TStringField;
    qryListD2IMD_CODE2: TStringField;
    qryListD2IMD_CODE3: TStringField;
    qryListD2IMD_CODE4: TStringField;
    qryListD2SIZE1: TStringField;
    qryListD2SIZE2: TStringField;
    qryListD2SIZE3: TStringField;
    qryListD2SIZE4: TStringField;
    qryListD2SIZE5: TStringField;
    qryListD2SIZE6: TStringField;
    qryListD2SIZE7: TStringField;
    qryListD2SIZE8: TStringField;
    qryListD2SIZE9: TStringField;
    qryListD2SIZE10: TStringField;
    qryListD2QTY: TBCDField;
    qryListD2QTYC: TStringField;
    qryListD2TOTQTY: TBCDField;
    qryListD2TOTQTYC: TStringField;
    qryListD2PRICE: TBCDField;
    qryListD2PRICE_G: TBCDField;
    qryListD2PRICEC: TStringField;
    qryListD2CUX_RATE: TBCDField;
    qryListD2SUP_AMT: TBCDField;
    qryListD2SUP_AMTC: TStringField;
    qryListD2VB_TAX: TBCDField;
    qryListD2VB_TAXC: TStringField;
    qryListD2VB_AMT: TBCDField;
    qryListD2VB_AMTC: TStringField;
    qryListD2SUP_TOTAMT: TBCDField;
    qryListD2SUP_TOTAMTC: TStringField;
    qryListD2VB_TOTTAX: TBCDField;
    qryListD2VB_TOTTAXC: TStringField;
    qryListD2VB_TOTAMT: TBCDField;
    qryListD2VB_TOTAMTC: TStringField;
    qryListD2BIGO1: TStringField;
    qryListD2BIGO2: TStringField;
    qryListD2BIGO3: TStringField;
    qryListD2BIGO4: TStringField;
    qryListD2BIGO5: TStringField;
    procedure qryListD2AfterScroll(DataSet: TDataSet);
    procedure btnCancelClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnAddD2Click(Sender: TObject);
    procedure btnCancelD2Click(Sender: TObject);
    procedure btnEdtD2Click(Sender: TObject);
    procedure btnSaveD2Click(Sender: TObject);
    procedure btnAdd_IMDClick(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure edt_IMD_CODE1Exit(Sender: TObject);
    procedure edt_IMD_CODEExit(Sender: TObject);
    procedure edt_SIZE1Exit(Sender: TObject);
    procedure edt_SIZEExit(Sender: TObject);
    procedure btnAdd_SIZEClick(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure btnDelD2Click(Sender: TObject);
    procedure CODEDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn26Click(Sender: TObject);
    procedure edt_LA_BANKExit(Sender: TObject);
  private
    mode_D2 :Integer;//1신규 2수정
    ReadWrite_D2 : Boolean;//True = 쓰기, False = 읽기.
    procedure BtnControl(tag:Integer);
    //저장시 예외처리
    function ErrorCheck:String;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_APPSPC_NEW_DOC1_frm: TUI_APPSPC_NEW_DOC1_frm;

implementation

uses
  ICON, MSSQL, UI_APPSPC_NEW, AutoNo,VarDefine,TypeDefine,
  CD_CUST, SQLCreator, CD_AMT_UNIT, CD_BANK, CD_APPSPCBANK, CodeContents,
  CodeDialogParent, CD_UNIT, dlg_CopyAPPSPCfromLOCRC1,
  dlg_SelectCopyDocument, Dlg_ErrorMessage;

{$R *.dfm}

procedure TUI_APPSPC_NEW_DOC1_frm.qryListD2AfterScroll(DataSet: TDataSet);
begin
  inherited;
  //일련번호
  edt_LINE_NO.Text := qryListD2.FieldByName('LINE_NO').AsString;
  //HS부호
  edt_HS_NO.Text := qryListD2.FieldByName('HS_NO').AsString;
  //품목
  edt_IMD_CODE.Text := qryListD2.FieldByName('IMD_CODE1').AsString;
  edt_IMD_CODE1.Text := qryListD2.FieldByName('IMD_CODE1').AsString;
  edt_IMD_CODE2.Text := qryListD2.FieldByName('IMD_CODE2').AsString;
  edt_IMD_CODE3.Text := qryListD2.FieldByName('IMD_CODE3').AsString;
  edt_IMD_CODE4.Text := qryListD2.FieldByName('IMD_CODE4').AsString;
  //규격
  edt_SIZE.Text := qryListD2.FieldByName('SIZE1').AsString;
  edt_SIZE1.Text := qryListD2.FieldByName('SIZE1').AsString;
  edt_SIZE2.Text := qryListD2.FieldByName('SIZE2').AsString;
  edt_SIZE3.Text := qryListD2.FieldByName('SIZE3').AsString;
  edt_SIZE4.Text := qryListD2.FieldByName('SIZE4').AsString;
  edt_SIZE5.Text := qryListD2.FieldByName('SIZE5').AsString;
  edt_SIZE6.Text := qryListD2.FieldByName('SIZE6').AsString;
  edt_SIZE7.Text := qryListD2.FieldByName('SIZE7').AsString;
  edt_SIZE8.Text := qryListD2.FieldByName('SIZE8').AsString;
  edt_SIZE9.Text := qryListD2.FieldByName('SIZE9').AsString;
  edt_SIZE10.Text := qryListD2.FieldByName('SIZE10').AsString;
  //수량단위
  edt_QTYC.Text := qryListD2.FieldByName('QTYC').AsString;
  //수량
  edt_QTY.Text := FloatToStr(qryListD2.FieldByName('QTY').AsFloat);
  //수량소계단위
  edt_TOTQTYC.Text := qryListD2.FieldByName('TOTQTYC').AsString;
  //수량소계
  edt_TOTQTY.Text := qryListD2.FieldByName('TOTQTY').AsString;
  //단가
  edt_PRICE.Text := FloatToStr(qryListD2.FieldByName('PRICE').AsFloat);
  //기준수량단위
  edt_PRICEC.Text := qryListD2.FieldByName('PRICEC').AsString;
  //기준수량
  edt_PRICE_G.Text := FloatToStr(qryListD2.FieldByName('PRICE_G').AsFloat);
  //금액단위
  edt_SUP_AMTC.Text := qryListD2.FieldByName('SUP_AMTC').AsString;
  //금액
  edt_SUP_AMT.Text := FloatToStr(qryListD2.FieldByName('SUP_AMT').AsFloat);
  //금액소계단위
  edt_SUP_TOTAMTC.Text := qryListD2.FieldByName('SUP_TOTAMTC').AsString;
  //금액소계
  edt_SUP_TOTAMT.Text := FloatToStr(qryListD2.FieldByName('SUP_TOTAMT').AsFloat);

end;

procedure TUI_APPSPC_NEW_DOC1_frm.btnCancelClick(Sender: TObject);
begin
  inherited;

  case UI_APPSPC_NEW_frm.mode_D1 of
    1://신규-취소
    begin
      with TADOQuery.Create(Nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := 'DELETE FROM APPSPC_D1 WHERE KEYY ='+QuotedStr(UI_APPSPC_NEW_frm.Maint_No)+' and DOC_GUBUN = '+QuotedStr('2AH')+'and SEQ = '+QuotedStr(IntToStr(UI_APPSPC_NEW_frm.SEQ));
          SQL.Add('DELETE FROM APPSPC_D2 WHERE KEYY ='+QuotedStr(UI_APPSPC_NEW_frm.Maint_No)+' AND DOC_GUBUN = '+QuotedStr('2AH')+' AND SEQ ='+QuotedStr(IntToStr(UI_APPSPC_NEW_frm.SEQ)));
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;
    end;
    2://수정취소
    begin
      qryD1Cancel.Close;
      qryD1Cancel.Parameters.ParamByName('KEYY').Value := UI_APPSPC_NEW_frm.Maint_No;
      qryD1Cancel.Parameters.ParamByName('SEQ').Value := UI_APPSPC_NEW_frm.SEQ;
      qryD1Cancel.Parameters.ParamByName('DOC_GUBUN').Value := '2AH';
      qryD1Cancel.Parameters.ParamByName('KEYY2').Value := UI_APPSPC_NEW_frm.Maint_No;
      qryD1Cancel.ExecSQL;
    end;
  end;
  Close
end;

procedure TUI_APPSPC_NEW_DOC1_frm.btnSaveClick(Sender: TObject);
var
  SQLCreate_D1 : TSQLCreate;
begin
  inherited;
  if ReadWrite_D2 then
  begin
    ShowMessage('현재 입력중인 품목사항이 있습니다.');
    exit;
  end;

  //유효성검사
  Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
  if Dlg_ErrorMessage_frm.Run_ErrorMessage( '판매대금추심(매입)의뢰서 - 물품수령증명서' ,ErrorCheck ) Then
  begin
    FreeAndNil(Dlg_ErrorMessage_frm);
    exit;
  end;

  SQLCreate_D1 := TSQLCreate.Create;

  with SQLCreate_D1 do
  begin
     DMLType := dmlUpdate;
     SQLCreate_D1.SQLHeader('APPSPC_D1');

     ADDWhere('KEYY',UI_APPSPC_NEW_frm.Maint_No);
     ADDWhere('DOC_GUBUN','2AH');
     ADDWhere('SEQ',UI_APPSPC_NEW_frm.SEQ);

     //문서번호
     ADDValue('DOC_NO', edt_DOC_NO.Text);
     //발급번호
     ADDValue('LR_NO2', edt_LR_NO2.Text);
     //HS부호
     ADDValue('BSN_HSCODE', msk_BSN_HSCODE.Text);
     //발급일자
     ADDValue('ISS_DATE', msk_ISS_DATE.Text);
     //인수일자
     ADDValue('ACC_DATE', msk_ACC_DATE.Text);
     //유효기일
     ADDValue('VAL_DATE',msk_VAL_DATE.Text);

     //공급자상호 코드
     ADDValue('SE_CODE', edt_SE_CODE.Text);
     //공급자상호
     ADDValue('SE_SNAME', edt_SE_SNAME.Text);

     //물품수령인 상호 코드
     ADDValue('BY_CODE', edt_BY_CODE.Text);
     //물품수령인 상호
     ADDValue('BY_SNAME', edt_BY_SNAME.Text);
     //물품수령인 대표자명
     ADDValue('BY_NAME', edt_BY_NAME.Text);
     //물품수령인 전자서명
     ADDValue('BY_ELEC', edt_BY_ELEC.Text);
     //물품수령인 주소
     ADDValue('BY_ADDR1',edt_BY_ADDR1.Text);
     ADDValue('BY_ADDR2',edt_BY_ADDR2.Text);
     ADDValue('BY_ADDR3',edt_BY_ADDR3.Text);

     //내국신용장 번호
     ADDValue('LR_NO', edt_LR_NO.Text);
     //물품인도기일
     ADDValue('GET_DATE', msk_GET_DATE.Text);
     ///유효기일
     ADDValue('EXP_DATE',msk_EXP_DATE.Text);
     ADDValue('LR_GUBUN','CRG');

     //은행코드
     ADDValue('LA_BANK', edt_LA_BANK.Text);
     //은행명
     ADDValue('LA_BANKNAME', edt_LA_BANKNAME.Text);
     //지점명
     ADDValue('LA_BANKBU', edt_LA_BANKBU.Text);
     //전자서명
     ADDValue('LA_ELEC', edt_LA_ELEC.Text);
     //매매기준율
     ADDValue('EXCH',StrToFloat(edt_EXCH.Text));
     //개설금액
     ADDValue('ISS_AMT', StrToFloat(edt_ISS_AMT.Text));
     //개설금액(외화) 통화단위
     ADDValue('ISS_AMTC', edt_ISS_AMTC.Text);
     //개설금액(외화)
     ADDValue('ISS_AMTU', StrToFloat(edt_ISS_AMTU.Text));
     //인수금액
     ADDValue('ISS_EXPAMT',StrToFloat(edt_ISS_EXPAMT.Text));
     //인수금액(외화) 통화단위
     ADDValue('ISS_EXPAMTC',edt_ISS_EXPAMTC.Text);
     //인수금액(외화)
     ADDValue('ISS_EXPAMTU',StrToFloat(edt_ISS_EXPAMTU.Text));
     //총수량 단위
     ADDValue('TOTCNTC', edt_TOTCNTC.Text);
     //총수량
     ADDValue('TOTCNT', StrToFloat(edt_TOTCNT.Text));
     //총금액 통화단위
     ADDValue('ISS_TOTAMTC',edt_ISS_TOTAMTC.Text);
     //총금액
     ADDValue('ISS_TOTAMT', StrToFloat(edt_ISS_TOTAMT.Text));
     //기타조건
     ADDValue('REMARK2_1',edt_REMARK2_1.Text);
     ADDValue('REMARK2_2',edt_REMARK2_2.Text);
     ADDValue('REMARK2_3',edt_REMARK2_3.Text);
     ADDValue('REMARK2_4',edt_REMARK2_4.Text);
     ADDValue('REMARK2_5',edt_REMARK2_5.Text);
     //참조사항
     ADDValue('REMARK1_1', edt_REMARK1_1.Text);
     ADDValue('REMARK1_2', edt_REMARK1_2.Text);
     ADDValue('REMARK1_3', edt_REMARK1_3.Text);
     ADDValue('REMARK1_4', edt_REMARK1_4.Text);
     ADDValue('REMARK1_5', edt_REMARK1_5.Text);
  end;

  with TADOQuery.Create(nil) do
  begin
      try
          Connection := DMMssql.KISConnect;
          SQL.Text := SQLCreate_D1.CreateSQL +#13#10;
          SQL.Add('DELETE FROM APPSPC_D2 WHERE KEYY='+QuotedStr('0'));
          ExecSQL;
      finally
          close;
          free;
      end;
  end;

  Close;

  with UI_APPSPC_NEW_frm.qryListD1_2AH do
  begin
    Close;
    Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
    Open;
    UI_APPSPC_NEW_frm.sPanel60.Caption := '물품수령증명서 : '+IntToStr(RecordCount)+'건';
  end;


//  if UI_APPSPC_NEW_frm.mode = 2 then //수정모드에서 저장시
//  begin
//     if DMMssql.inTrans then
//      DMMssql.CommitTrans;
//  end;
end;

procedure TUI_APPSPC_NEW_DOC1_frm.btnAddD2Click(Sender: TObject);
begin
  inherited;
  mode_D2 := 1;//신규
  ReadWrite_D2 := True;
  EnableControl(sPanel16);
  ClearControl(sPanel16);
  ClearControl(sPanel1);
  ClearControl(sPanel3);
  BtnControl(1);
  edt_LINE_NO.Enabled := False;
  edt_LINE_NO.Text := Format('%0.4d',[qryListD2.recordCount+1]);
end;

procedure TUI_APPSPC_NEW_DOC1_frm.btnCancelD2Click(Sender: TObject);
begin
  inherited;
  EnableControl(sPanel16,False);
  BtnControl(2);
  qryListD2.First;
  ReadWrite_D2 := False;
end;

procedure TUI_APPSPC_NEW_DOC1_frm.btnEdtD2Click(Sender: TObject);
begin
  inherited;
  if qryListD2.RecordCount = 0 then
  begin
    ShowMessage('수정할 물품수령증명서 품목내역이 없습니다.');
    exit;
  end;
  ReadWrite_D2 := True;
  mode_D2 := 2; //수정
  EnableControl(sPanel16);
  BtnControl(3);
  edt_LINE_NO.Enabled := False;
end;

procedure TUI_APPSPC_NEW_DOC1_frm.btnSaveD2Click(Sender: TObject);
var
  SQLCreate_D2:TSQLCreate;
  Line_No:String;
begin
  inherited;
  SQLCreate_D2 := TSQLCreate.Create;
  with SQLCreate_D2 do
  begin
    case mode_D2 of
      1://신규-저장
      begin
        DMLType := dmlInsert;
        SQLHeader('APPSPC_D2');

        ADDValue('KEYY',UI_APPSPC_NEW_frm.Maint_No);
        ADDValue('DOC_GUBUN','2AH');
        ADDValue('SEQ',UI_APPSPC_NEW_frm.SEQ);
        ADDValue('LINE_NO',edt_LINE_NO.Text);

        Line_No := edt_LINE_NO.Text;

        //HS부호
        ADDValue('HS_NO',edt_HS_NO.Text);
        //품목
        ADDValue('IMD_CODE1',edt_IMD_CODE1.Text);
        ADDValue('IMD_CODE2',edt_IMD_CODE2.Text);
        ADDValue('IMD_CODE3',edt_IMD_CODE3.Text);
        ADDValue('IMD_CODE4',edt_IMD_CODE4.Text);

        //규격
        ADDValue('SIZE1',edt_SIZE1.Text);
        ADDValue('SIZE2',edt_SIZE2.Text);
        ADDValue('SIZE3',edt_SIZE3.Text);
        ADDValue('SIZE4',edt_SIZE4.Text);
        ADDValue('SIZE5',edt_SIZE5.Text);
        ADDValue('SIZE6',edt_SIZE6.Text);
        ADDValue('SIZE7',edt_SIZE7.Text);
        ADDValue('SIZE8',edt_SIZE8.Text);
        ADDValue('SIZE9',edt_SIZE9.Text);
        ADDValue('SIZE10',edt_SIZE10.Text);

        //수량 단위
        ADDValue('QTYC',edt_QTYC.Text);
        //수량
        ADDValue('QTY',edt_QTY.Value);
        //수량소계 단위
        ADDValue('TOTQTYC', edt_TOTQTYC.Text);
        //수량소계
        ADDValue('TOTQTY', edt_TOTQTY.Value);
        //단가
        ADDValue('PRICE', edt_PRICE.Value);
        //기준수량 단위
        ADDValue('PRICEC',edt_PRICEC.Text);
        //기준수량
        ADDValue('PRICE_G',edt_PRICE_G.Value);
        //금액단위
        ADDValue('SUP_AMTC',edt_SUP_AMTC.Text);
        //금액
        ADDValue('SUP_AMT',edt_SUP_AMT.Value);
        //금액소계단위
        ADDValue('SUP_TOTAMTC',edt_SUP_TOTAMTC.Text);
        //금액소계
        ADDValue('SUP_TOTAMT',edt_SUP_TOTAMT.Value);

      end;
      2://수정-저장
      begin
        DMLType := dmlUpdate;
        SQLHeader('APPSPC_D2');

        ADDWhere('KEYY',UI_APPSPC_NEW_frm.Maint_No);
        ADDWhere('DOC_GUBUN','2AH');
        ADDWhere('SEQ',UI_APPSPC_NEW_frm.SEQ);
        ADDWhere('LINE_NO',edt_LINE_NO.Text);

        Line_No := edt_LINE_NO.Text;

        //HS부호
        ADDValue('HS_NO',edt_HS_NO.Text);
        //품목
        ADDValue('IMD_CODE1',edt_IMD_CODE1.Text);
        ADDValue('IMD_CODE2',edt_IMD_CODE2.Text);
        ADDValue('IMD_CODE3',edt_IMD_CODE3.Text);
        ADDValue('IMD_CODE4',edt_IMD_CODE4.Text);
        //규격
        ADDValue('SIZE1',edt_SIZE1.Text);
        ADDValue('SIZE2',edt_SIZE2.Text);
        ADDValue('SIZE3',edt_SIZE3.Text);
        ADDValue('SIZE4',edt_SIZE4.Text);
        ADDValue('SIZE5',edt_SIZE5.Text);
        ADDValue('SIZE6',edt_SIZE6.Text);
        ADDValue('SIZE7',edt_SIZE7.Text);
        ADDValue('SIZE8',edt_SIZE8.Text);
        ADDValue('SIZE9',edt_SIZE9.Text);
        ADDValue('SIZE10',edt_SIZE10.Text);

        //수량 단위
        ADDValue('QTYC',edt_QTYC.Text);
        //수량
        ADDValue('QTY',edt_QTY.Value);
        //수량소계 단위
        ADDValue('TOTQTYC', edt_TOTQTYC.Text);
        //수량소계
        ADDValue('TOTQTY', edt_TOTQTY.Value);
        //단가
        ADDValue('PRICE', edt_PRICE.Value);
        //기준수량 단위
        ADDValue('PRICEC',edt_PRICEC.Text);
        //기준수량
        ADDValue('PRICE_G',edt_PRICE_G.Value);
        //금액단위
        ADDValue('SUP_AMTC',edt_SUP_AMTC.Text);
        //금액
        ADDValue('SUP_AMT',edt_SUP_AMT.Value);
        //금액소계단위
        ADDValue('SUP_TOTAMTC',edt_SUP_TOTAMTC.Text);
        //금액소계
        ADDValue('SUP_TOTAMT',edt_SUP_TOTAMT.Value);
      end;
    end;
  end;

  with TADOQuery.Create(Nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := SQLCreate_D2.CreateSQL;
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;

  with qryListD2 do
  begin
    Close;
    Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
    Parameters[1].Value := '2AH';
    Parameters[2].Value := UI_APPSPC_NEW_frm.SEQ;
    Open;
    sDBGrid4.Fields[1].EditMask := '9999.99-9999;0;';
    qryListD2.Locate('LINE_NO', Line_No, [] ) ;
  end;


  EnableControl(sPanel16, False);
  BtnControl(4);
  ReadWrite_D2 := False;
end;

procedure TUI_APPSPC_NEW_DOC1_frm.btnAdd_IMDClick(Sender: TObject);
begin
  inherited;
  if btnAdd_IMD.Caption = '+' then
  begin
    btnAdd_IMD.Caption := '-';
    sPanel1.Visible := True;
    edt_IMD_CODE1.SetFocus;
  end
  else if btnAdd_IMD.Caption = '-' then
    sButton1Click(Self);
end;

procedure TUI_APPSPC_NEW_DOC1_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  sPanel1.Visible := False;
  btnAdd_IMD.Caption := '+'
end;

procedure TUI_APPSPC_NEW_DOC1_frm.edt_IMD_CODE1Exit(Sender: TObject);
begin
  inherited;
  edt_IMD_CODE.Text := edt_IMD_CODE1.Text;
end;

procedure TUI_APPSPC_NEW_DOC1_frm.edt_IMD_CODEExit(Sender: TObject);
begin
  inherited;
  edt_IMD_CODE1.Text := edt_IMD_CODE.Text;

end;

procedure TUI_APPSPC_NEW_DOC1_frm.edt_SIZE1Exit(Sender: TObject);
begin
  inherited;
  edt_SIZE.Text := edt_SIZE1.Text;
end;

procedure TUI_APPSPC_NEW_DOC1_frm.edt_SIZEExit(Sender: TObject);
begin
  inherited;
  edt_SIZE1.Text := edt_SIZE.Text;
end;

procedure TUI_APPSPC_NEW_DOC1_frm.btnAdd_SIZEClick(Sender: TObject);
begin
  inherited;
  if btnAdd_SIZE.Caption = '+' then
  begin
    btnAdd_SIZE.Caption := '-';
    sPanel3.Visible := True;
    edt_SIZE1.SetFocus;
  end
  else if btnAdd_SIZE.Caption = '-' then
    sButton2Click(Self);
end;

procedure TUI_APPSPC_NEW_DOC1_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  sPanel3.Visible := False;
  btnAdd_SIZE.Caption := '+';
end;

procedure TUI_APPSPC_NEW_DOC1_frm.btnDelD2Click(Sender: TObject);
var
  SQLCreate_D2:TSQLCreate;
  sSQL_D2:String;
  iLINE_NO,iSize:Integer;
begin
  inherited;    
  if qryListD2.RecordCount = 0 then
  begin
    ShowMessage('삭제할 물품수령증명서 품목내역이 없습니다.');
    exit;
  end;
  
  SQLCreate_D2 := TSQLCreate.Create;
  with SQLCreate_D2 do
  begin
    DMLType := dmlDelete;
    SQLHeader('APPSPC_D2');
    ADDWhere('KEYY',UI_APPSPC_NEW_frm.Maint_No);
    ADDWhere('DOC_GUBUN','2AH');
    ADDWhere('SEQ',UI_APPSPC_NEW_frm.SEQ);
    ADDWhere('LINE_NO',qryListD2.FieldByName('Line_No').AsString);
  end;
  iLINE_NO := qryListD2.FieldByName('Line_No').AsInteger;
  ShowMessage(IntToStr(iLINE_NO));

//  SQL_D2 := 'UPDATE APPSPC_D2 SET SEQ = CONVERT(INT,SEQ-1) WHERE CONVERT(INT,SEQ1)';
//  SQL_D1 := 'UPDATE APPSPC_D1 SET SEQ = SEQ -1 WHERE KEYY = '+QuotedStr(Maint_No)+'AND SEQ > '+IntToStr(SEQ)+'AND DOC_GUBUN = '+QuotedStr('2AH');
  sSQL_D2 := 'UPDATE APPSPC_D2 ';
  sSQL_D2 := sSQL_D2 + 'SET LINE_NO = REPLICATE('+QuotedStr('0')+',4-LEN(CONVERT(INT,LINE_NO)-1)) + CONVERT(VARCHAR(8),CONVERT(INT,LINE_NO)-1)';
  sSQL_D2 := sSQL_D2 + 'WHERE CONVERT(INT,LINE_NO) > '+IntToStr(iLINE_NO)+' and KEYY ='+UI_APPSPC_NEW_frm.Maint_No+' and SEQ = '+IntToStr(UI_APPSPC_NEW_frm.SEQ)+' and DOC_GUBUN = '+QuotedStr('2AH');
//  ShowMessage(sSQL_D2);
  with TADOQuery.Create(Nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := SQLCreate_D2.CreateSQL+#13#10+sSQL_D2;
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;

  qryListD2.Close;
  qryListD2.Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
  qryListD2.Parameters[1].Value := '2AH';
  qryListD2.Parameters[2].Value := UI_APPSPC_NEW_frm.SEQ;
  qryListD2.Open;

  sDBGrid4.Fields[1].EditMask := '9999.99-9999;0;';
  BtnControl(5);
  ReadWrite_D2 := False;
end;

procedure TUI_APPSPC_NEW_DOC1_frm.BtnControl(Tag:Integer);
begin
  case Tag of
    1://입력
    begin
      btnAddD2.Enabled := False;
      btnCancelD2.Enabled := True;
      btnEdtD2.Enabled := False;
      btnSaveD2.Enabled := True;
      btnDelD2.Enabled := False;
      sDBGrid4.Enabled := False;
    end;
    2://취소
    begin
      btnAddD2.Enabled := True;
      btnCancelD2.Enabled := False;
      btnEdtD2.Enabled := True;
      btnSaveD2.Enabled := False;
      btnDelD2.Enabled := True;
      sDBGrid4.Enabled := True;
    end;
    3://수정
    begin
      btnAddD2.Enabled := False;
      btnCancelD2.Enabled := True;
      btnEdtD2.Enabled := False;
      btnSaveD2.Enabled := True;
      btnDelD2.Enabled := False;
      sDBGrid4.Enabled := False;
    end;
    4://저장
    begin
      btnAddD2.Enabled := True;
      btnCancelD2.Enabled := False;
      btnEdtD2.Enabled := True;
      btnSaveD2.Enabled := False;
      btnDelD2.Enabled := True;
      sDBGrid4.Enabled := True;
    end;
    5://삭제
    begin
      btnAddD2.Enabled := True;
      btnCancelD2.Enabled := False;
      btnEdtD2.Enabled := True;
      btnSaveD2.Enabled := False;
      btnDelD2.Enabled := True;
      sDBGrid4.Enabled := True;
    end;

  end;
END;

procedure TUI_APPSPC_NEW_DOC1_frm.CODEDblClick(Sender: TObject);
var
  CodeDialog:TCodeDialogParent_frm;

begin
  inherited;
  CodeDialog := nil;
  case TsEdit(Sender).Tag of
    //거래처코드
    101,102: CodeDialog := TCD_CUST_frm.Create(Self);
    //은행코드
    103: CodeDialog := TCD_BANK_frm.Create(Self);
    //통화단위코드
    104,105,107,111,112: CodeDialog := TCD_AMT_UNIT_frm.Create(Self);
    //수량단위코드
    106,108,109,110: CodeDialog := TCD_UNIT_frm.Create(Self);
  end;

  try
    if CodeDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CodeDialog.FieldValues('Code');
      case TsEdit(Sender).Tag of

        101://공급자상호
        begin
          edt_SE_SNAME.Text := CodeDialog.FieldValues('ENAME');
        end;

        102://물품수령인상호
        begin
          edt_BY_SNAME.Text := CodeDialog.FieldValues('ENAME');
          edt_BY_NAME.Text := CodeDialog.FieldValues('REP_NAME');
          edt_BY_ELEC.Text := CodeDialog.FieldValues('Jenja');
          edt_BY_ADDR1.Text := CodeDialog.FieldValues('ADDR1');
          edt_BY_ADDR2.Text := CodeDialog.FieldValues('ADDR2');
          edt_BY_ADDR3.Text := CodeDialog.FieldValues('ADDR3');
        end;

        103://개설은행코드
        begin
          edt_LA_BANKNAME.Text := CodeDialog.Values[1];
          edt_LA_BANKBU.Text := CodeDialog.Values[2];
        end;
      end;
    end;

  finally
    FreeAndNil(CodeDialog);
  end;
end;
procedure TUI_APPSPC_NEW_DOC1_frm.FormShow(Sender: TObject);
begin
  sDBGrid4.Fields[1].EditMask := '9999.99-9999;0;';
//  qryListD2QTY.EditFormat := '###,###,##0.00;0;';
end;

procedure TUI_APPSPC_NEW_DOC1_frm.sBitBtn26Click(Sender: TObject);
var
  SQLCreate_D2:TSQLCreate;
  isCopy:Boolean;
  i:Integer;
begin
  inherited;
  dlg_CopyAPPSPCfromLOCRC1_frm := Tdlg_CopyAPPSPCfromLOCRC1_frm.Create(Self);
  try
    with dlg_CopyAPPSPCfromLOCRC1_frm do
    begin
          isCopy := OpenDialog(0);
          IF not isCopy Then
            Abort;

          if qryListD2.RecordCount > 0 then
          begin
            if MessageDlg('작성된 품목내역이 있습니다. 덮어쓰시겠습니까?',mtWarning,mbOKCancel,0) = mrOk then
            begin
              with TADOQuery.Create(Nil) do
              begin
                try
                  Connection := DMMssql.KISConnect;
                  SQL.Text := 'DELETE FROM APPSPC_D2 WHERE KEYY='+QuotedStr(UI_APPSPC_NEW_frm.Maint_No);
                  ExecSQL;
                finally
                  Close;
                  Free;
                end;
              end;
            end
            else
              exit;
          end;

          //문서번호
          edt_DOC_NO.Text := FieldValues('MAINT_NO');
          //발급번호
          edt_LR_NO2.Text := FieldValues('RFF_NO');
          //HS부호
          msk_BSN_HSCODE.Text := FieldValues('BSN_HSCODE');
          //발급일자
          msk_ISS_DATE.Text := FieldValues('ISS_DAT');
          //인수일자
          msk_ACC_DATE.Text := FieldValues('GET_DAT');
          //유효기일
          msk_VAL_DATE.Text := FieldValues('EXP_DAT');

          //내국신용장번호
          edt_LR_NO.Text := FieldValues('LOC_NO');
          //물품인도기일
          msk_GET_DATE.Text := FieldValues('LOADDATE');
          //유효기일
          msk_EXP_DATE.Text := FieldValues('EXPDATE');

          //공급자코드
          edt_SE_CODE.Text := FieldValues('BENEFC');
          //공급자상호
          edt_SE_SNAME.Text := FieldValues('BENEFC1');

          //물품수령인코드
          edt_BY_CODE.Text := FieldValues('APPLIC');
          //물품수령인상호
          edt_BY_SNAME.Text := FieldValues('APPLIC1');
          //대표자명
          edt_BY_NAME.Text := FieldValues('APPLIC2');
          //전자서명
          edt_BY_ELEC.Text := FieldValues('APPLIC3');
          //주소
          edt_BY_ADDR1.Text := FieldValues('APPLIC4');
          edt_BY_ADDR2.Text := FieldValues('APPLIC5');
          edt_BY_ADDR3.Text := FieldValues('APPLIC6');

          //개설은행코드
          edt_LA_BANK.Text := FieldValues('AP_BANK');
          //개설은행명
          edt_LA_BANKNAME.Text := FieldValues('AP_BANK1');
          //지점명
          edt_LA_BANKBU.Text := FieldValues('AP_BANK3');
          //전자서명
          edt_LA_ELEC.Text := FieldValues('AP_NAME');
          //매매기준율
          edt_EXCH.Text := FieldValues('EX_RATE');
          //개설금액
          edt_ISS_AMT.Text := FieldValues('LOC2AMT');
          //개설금액(외화)
          edt_ISS_AMTU.Text := FieldValues('LOC1AMT');
          //개설금액 통화단위
          edt_ISS_AMTC.Text := FieldValues('LOC1AMTC');
          //인수금액
          edt_ISS_EXPAMT.Text := FieldValues('RCT_AMT2');

          //인수금액(외화)
          edt_ISS_EXPAMTU.Text := FieldValues('RCT_AMT1');
          //인수금액 통화단위
          edt_ISS_EXPAMTC.Text := FieldValues('RCT_AMT1C');
          //총수량
          edt_TOTCNT.Text := FieldValues('TQTY');
          //총수량단위
          edt_TOTCNTC.Text := FieldValues('TQTY_G');
          //총금액
          edt_ISS_TOTAMT.Text := FieldValues('TAMT');
          //총금액 통화단위
          edt_ISS_TOTAMTC.Text := FieldValues('TAMT_G');


    end;
  finally
    FreeAndNil(dlg_CopyAPPSPCfromLOCRC1_frm);//dlg_CopyAPPSPCfromLOCRC1_frm);
  end;

  with qryDOCClick do
  begin
      Close;
      Parameters[0].Value := edt_DOC_NO.Text;
      Open;
  end;

  SQLCreate_D2 := TSQLCreate.Create;
  qryDOCClick.First;

  for i:= 1 to qryDOCClick.RecordCount do
  begin
    with SQLCreate_D2 do
    begin
      DMLType := dmlInsert;
      SQLHeader('APPSPC_D2');
      ADDValue('KEYY', UI_APPSPC_NEW_frm.Maint_No);
      ADDValue('DOC_GUBUN', '2AH');
      ADDValue('SEQ', UI_APPSPC_NEW_frm.SEQ);
      ADDValue('LINE_NO', Format('%0.4d',[qryDOCClick.FieldByName('SEQ').AsInteger]));
      ADDValue('HS_NO', qryDOCClick.FieldByName('HS_NO').AsString);
      //규격
      ADDValue('SIZE1', qryDOCClick.FieldByName('SIZE').AsString);
      //수량
      ADDValue('QTY', qryDOCClick.FieldByName('QTY').AsFloat);
      //수량단위
      ADDValue('QTYC', qryDOCClick.FieldByName('QTY_G').AsString);
      //수량소계
      ADDValue('TOTQTY', qryDOCClick.FieldByName('STQTY').AsFloat);
      //수량소계단위
      ADDValue('TOTQTYC', qryDOCClick.FieldByName('STQTY_G').AsString);
      //단가
      ADDValue('PRICE', qryDOCClick.FieldByName('PRICE').AsFloat);
      //기준수량
      ADDValue('PRICE_G', qryDOCClick.FieldByName('QTYG').AsFloat);
      //기준수량단위
      ADDValue('PRICEC', qryDOCClick.FieldByName('QTYG_G').AsString);
      //금액
      ADDValue('SUP_AMT', qryDOCClick.FieldByNAME('AMT').AsFloat);
      //금액통화단위
      ADDValue('SUP_AMTC', qryDOCClick.FieldByNAME('AMT_G').AsString);
      //금액소계
      ADDValue('SUP_TOTAMT', qryDOCClick.FieldByName('STAMT').AsFloat);
      //금액소계 통화단위
      ADDValue('SUP_TOTAMTC', qryDOCClick.FieldByName('STAMT_G').AsString);

    end;

    with TADOQuery.Create(Nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SQLCreate_D2.CreateSQL;
        ExecSQL;
      finally
        Close;
        Free;
      end;
    end;

    qryDOCClick.Next;
  end;

  qryListD2.Close;
  qryListD2.Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
  qryListD2.Parameters[1].Value := '2AH';
  qryListD2.Parameters[2].Value := UI_APPSPC_NEW_frm.SEQ;
  qryListD2.Open;

  sDBGrid4.Fields[1].EditMask := '9999.99-9999;0;';
  BtnControl(5);
end;

function TUI_APPSPC_NEW_DOC1_frm.ErrorCheck:String;
var
  ErrMsg:TStringList;
begin
  ErrMsg := TStringList.Create;
  Try
    IF Trim(edt_DOC_NO.Text) = '' THEN
      ErrMsg.Add('[물품수령증명서] 문서번호를 입력하세요');
    IF Trim(edt_LR_NO2.Text) = '' THEN
      ErrMsg.Add('[물품수령증명서] 발급번호를 입력하세요');
    IF Trim(msk_ISS_DATE.Text) = '' THEN
      ErrMsg.Add('[물품수령증명서] 발급일자를 입력하세요');
    IF Trim(msk_ACC_DATE.Text) = '' THEN
      ErrMsg.Add('[물품수령증명서] 인수일자를 입력하세요');

    IF Trim(edt_SE_SNAME.Text) = '' THEN
      ErrMsg.Add('[공급자] 상호를 입력하세요');

    IF Trim(edt_BY_SNAME.Text) = '' THEN
      ErrMsg.Add('[물품수령인] 상호를 입력하세요');
    IF Trim(edt_BY_SNAME.Text) = '' THEN
      ErrMsg.Add('[물품수령인] 대표자명을 입력하세요');
    IF Trim(edt_BY_SNAME.Text) = '' THEN
      ErrMsg.Add('[물품수령인] 전자서명을 입력재하세요');
    IF Trim(edt_BY_ADDR1.Text) = '' THEN
      ErrMsg.Add('[물품수령인] 주소를 입력하세요');

    IF Trim(edt_LR_NO.Text) = '' THEN
      ErrMsg.Add('[내국신용장] 번호를 입력하세요');
    IF Trim(msk_GET_DATE.Text) = '' THEN
      ErrMsg.Add('[내국신용장] 물품인도기일을 입력하세요');
    IF Trim(msk_EXP_DATE.Text) = '' THEN
      ErrMsg.Add('[내국신용장] 유효기일을 입력하세요');
    IF Trim(edt_LA_BANK.Text) = '' THEN
      ErrMsg.Add('[내국신용장] 개설은행 코드를 입력하세요');
    IF Trim(edt_LA_BANKNAME.Text) = '' THEN
      ErrMsg.Add('[내국신용장] 개설은행명을 입력하세요');
    IF Trim(edt_LA_BANKBU.Text) = '' THEN
      ErrMsg.Add('[내국신용장] 개설은행 지점명을 입력하세요');
    IF Trim(edt_LA_ELEC.Text) = '' THEN
      ErrMsg.Add('[내국신용장] 개설은행 전자서명을 입력하세요');


    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;


end;

procedure TUI_APPSPC_NEW_DOC1_frm.edt_LA_BANKExit(Sender: TObject);
begin
  inherited;
  
  CD_BANK_frm := TCD_BANK_frm.Create(Self);
  try
    case TsEdit(Sender).Tag of
      103:
      begin
        edt_LA_BANKNAME.Text := CD_BANK_frm.GetCodeText(edt_LA_BANK.Text).sName;
        edt_LA_BANKBU.Text :=  CD_BANK_frm.GetCodeText(edt_LA_BANK.Text).sBrunch;
      end;
    end;
  finally
    FreeAndNil(CD_BANK_frm);
  end;

end;

end.
































