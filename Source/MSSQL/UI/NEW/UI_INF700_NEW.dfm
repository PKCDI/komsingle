inherited UI_INF700_NEW_frm: TUI_INF700_NEW_frm
  Left = 658
  Top = 163
  Caption = '[INF700]'#52712#49548#48520#45733#54868#54872#49888#50857#51109' '#51025#45813#49436
  ClientWidth = 1058
  PixelsPerInch = 96
  TextHeight = 12
  inherited btn_Panel: TsPanel
    Width = 1058
    OnClick = btn_PanelClick
    inherited sImage2: TsImage
      Left = 251
      Top = 5
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 817
      Top = -5
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 834
      Top = 11
      Visible = False
    end
    inherited sLabel7: TsLabel
      Left = 32
      Width = 210
      Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#51025#45813#49436
    end
    inherited sLabel6: TsLabel
      Left = 111
      Width = 53
      Caption = 'INF700'
    end
    inherited sSpeedButton6: TsSpeedButton
      Left = 422
    end
    inherited sSpeedButton7: TsSpeedButton
      Left = 1054
      Top = 27
      Visible = False
    end
    inherited sButton13: TsButton
      Left = 434
      Top = 6
      Width = 164
      Hint = #49440#53469#54620' '#45936#51060#53552#47484' ERP'#50640' '#50732#47549#45768#45796
      Caption = #49440#53469#45936#51060#53552' UPLOAD'
    end
    inherited btnNew: TsButton
      Left = 857
      Top = 11
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 923
      Top = 11
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 284
    end
    inherited btnPrint: TsButton
      Left = 353
    end
    inherited btnTemp: TsButton
      Left = 856
      Top = 27
      Visible = False
    end
    inherited btnSave: TsButton
      Left = 922
      Top = 27
      Visible = False
    end
    inherited btnCancel: TsButton
      Left = 988
      Top = 27
      Visible = False
    end
    inherited btnReady: TsButton
      Left = 794
      Top = 11
      Visible = False
    end
    inherited btnSend: TsButton
      Left = 888
      Top = 11
      Visible = False
    end
    inherited btnCopy: TsButton
      Left = 782
      Top = 26
      Visible = False
    end
    inherited sPanel6: TsPanel
      inherited msk_Datee: TsMaskEdit
        Left = 328
      end
      inherited edt_userno: TsEdit
        Left = 456
      end
    end
  end
  inherited sPanel4: TsPanel
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 181
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MSEQ'
          Title.Alignment = taCenter
          Title.Caption = #49692#48264
          Width = 35
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 94
          Visible = True
        end>
    end
    inherited sPanel5: TsPanel
      inherited sSpeedButton9: TsSpeedButton
        Left = 254
      end
      inherited edt_SearchNo: TsEdit
        Left = 81
        BoundLabel.Active = False
      end
      inherited sMaskEdit1: TsMaskEdit
        Left = 81
      end
      inherited sMaskEdit2: TsMaskEdit
        Left = 174
      end
      inherited sBitBtn1: TsBitBtn
        Left = 266
      end
      object sComboBox1: TsComboBox
        Left = 7
        Top = 28
        Width = 73
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        Style = csDropDownList
        ItemHeight = 17
        ItemIndex = 1
        TabOrder = 4
        Text = 'LC'#48264#54840
        OnChange = sComboBox1Change
        Items.Strings = (
          #44288#47532#48264#54840
          'LC'#48264#54840)
      end
    end
    inherited sPanel29: TsPanel
      Left = 232
      Top = 576
      Width = 108
      Height = 55
      Align = alNone
    end
  end
  inherited sPanel3: TsPanel
    Width = 717
    inherited sPageControl1: TsPageControl
      Width = 715
      ActivePage = sTabSheet3
      inherited sTabSheet1: TsTabSheet
        inherited sPanel7: TsPanel
          Width = 707
          inherited sSpeedButton10: TsSpeedButton
            Height = 271
          end
          inherited msk_APP_DATE: TsMaskEdit
            Enabled = False
          end
          inherited edt_IN_MATHOD: TsEdit
            Enabled = False
          end
          inherited edt_IN_MATHOD_NM: TsEdit
            Width = 201
            Enabled = False
          end
          inherited edt_ImpCd1: TsEdit
            Top = 311
            Enabled = False
          end
          inherited edt_ImpCd1_NM: TsEdit
            Top = 311
            Width = 202
            Enabled = False
          end
          inherited edt_ImpCd2: TsEdit
            Top = 333
            Enabled = False
          end
          inherited edt_ImpCd2_NM: TsEdit
            Top = 333
            Width = 202
            Enabled = False
          end
          inherited edt_ImpCd3: TsEdit
            Top = 355
            Enabled = False
          end
          inherited edt_ImpCd3_NM: TsEdit
            Top = 355
            Width = 202
            Enabled = False
          end
          inherited edt_ImpCd4: TsEdit
            Top = 377
            Enabled = False
          end
          inherited edt_ImpCd4_NM: TsEdit
            Top = 377
            Width = 202
            Enabled = False
          end
          inherited edt_ImpCd5: TsEdit
            Top = 399
            Enabled = False
          end
          inherited edt_ImpCd5_NM: TsEdit
            Top = 399
            Width = 202
            Enabled = False
          end
          inherited edt_AdPay: TsEdit
            Top = 250
            Enabled = False
          end
          inherited edt_AdPay1: TsEdit
            Top = 250
            Enabled = False
          end
          inherited edt_ApBank: TsEdit
            Enabled = False
          end
          inherited edt_ApBank1: TsEdit
            Width = 201
            Enabled = False
          end
          inherited edt_ApBank3: TsEdit
            Width = 251
            Enabled = False
          end
          inherited edt_AdBank: TsEdit
            Left = 13
            Top = 450
            Visible = False
          end
          inherited edt_AdBank1: TsEdit
            Left = 93
            Top = 178
            Width = 251
            Enabled = False
            BoundLabel.Active = True
            BoundLabel.Caption = '('#55148#47581')'#53685#51648#51008#54665
          end
          inherited edt_AdBank3: TsEdit
            Top = 202
            Width = 251
            Enabled = False
          end
          inherited edt_AdBank_BIC: TsEdit
            Top = 226
            Enabled = False
          end
          inherited edt_AdInfo1: TsEdit
            Left = 669
            Top = 503
            Visible = False
          end
          inherited edt_AdInfo2: TsEdit
            Left = 669
            Top = 525
            Visible = False
          end
          inherited edt_AdInfo5: TsEdit
            Left = 669
            Top = 591
            Visible = False
          end
          inherited edt_AdInfo3: TsEdit
            Left = 669
            Top = 547
            Visible = False
          end
          inherited edt_AdInfo4: TsEdit
            Left = 669
            Top = 569
            Visible = False
          end
          inherited sEdit2: TsEdit
            Top = 226
            Enabled = False
          end
          inherited edt_ILno1: TsEdit
            Top = 311
            Enabled = False
          end
          inherited edt_ILno2: TsEdit
            Top = 333
            Enabled = False
          end
          inherited edt_ILno5: TsEdit
            Top = 399
            Enabled = False
          end
          inherited edt_ILno3: TsEdit
            Top = 355
            Enabled = False
          end
          inherited edt_ILno4: TsEdit
            Top = 377
            Enabled = False
          end
          inherited sPanel8: TsPanel
            Top = 153
          end
          inherited sPanel9: TsPanel
            Top = 286
          end
          inherited sPanel10: TsPanel
            Left = 2
            Top = 428
            Width = 705
          end
          inherited sPanel11: TsPanel
            Top = 286
          end
          inherited edt_ILCur1: TsEdit
            Top = 311
            Enabled = False
          end
          inherited edt_ILCur2: TsEdit
            Top = 333
            Enabled = False
          end
          inherited edt_ILCur5: TsEdit
            Top = 399
            Enabled = False
          end
          inherited edt_ILCur3: TsEdit
            Top = 355
            Enabled = False
          end
          inherited edt_ILCur4: TsEdit
            Top = 377
            Enabled = False
          end
          inherited edt_ILAMT1: TsCurrencyEdit
            Top = 311
            Enabled = False
          end
          inherited edt_ILAMT2: TsCurrencyEdit
            Top = 333
            Enabled = False
          end
          inherited edt_ILAMT3: TsCurrencyEdit
            Top = 355
            Enabled = False
          end
          inherited edt_ILAMT4: TsCurrencyEdit
            Top = 377
            Enabled = False
          end
          inherited edt_ILAMT5: TsCurrencyEdit
            Top = 399
            Enabled = False
          end
          inherited sPanel12: TsPanel
            Top = 5
          end
          inherited edt_EXName1: TsEdit
            Top = 30
            Enabled = False
          end
          inherited edt_EXName2: TsEdit
            Top = 54
            Enabled = False
          end
          inherited edt_EXName3: TsEdit
            Top = 78
            Enabled = False
          end
          inherited edt_EXAddr1: TsEdit
            Top = 102
            Enabled = False
          end
          inherited edt_EXAddr2: TsEdit
            Top = 126
            Enabled = False
          end
          inherited sPanel59: TsPanel
            Top = 548
            Visible = False
          end
          inherited com_Carriage: TsComboBox
            Top = 548
            Enabled = False
            Visible = False
          end
          inherited sPanel89: TsPanel
            Top = 572
            Visible = False
          end
          inherited edt_ApBank5: TsEdit
            Width = 251
            Enabled = False
          end
          inherited sPanel126: TsPanel
            Top = 286
            TabOrder = 60
          end
          inherited sPanel127: TsPanel
            Top = 286
          end
          object memo_Adinfo: TsMemo
            Left = 2
            Top = 452
            Width = 705
            Height = 85
            ReadOnly = True
            ScrollBars = ssBoth
            TabOrder = 58
            WordWrap = False
          end
        end
      end
      inherited sTabSheet3: TsTabSheet
        inherited sPanel27: TsPanel
          Width = 707
          Enabled = False
          inherited sLabel3: TsLabel
            Top = 333
          end
          inherited sLabel4: TsLabel
            Left = 548
            Top = 469
          end
          inherited edt_Benefc3: TsEdit [2]
            Top = 237
            Enabled = False
          end
          inherited edt_Benefc4: TsEdit [3]
            Top = 259
            Enabled = False
          end
          inherited sPanel16: TsPanel [4]
            Top = 169
          end
          inherited edt_Applic1: TsEdit [5]
            Top = 193
            Enabled = False
          end
          inherited sPanel32: TsPanel [6]
            Left = 4
            Top = 425
            Width = 325
          end
          object sButton1: TsButton [7]
            Left = 432
            Top = 425
            Width = 273
            Height = 23
            Cursor = crHandPoint
            Caption = 'Show Description of Goods and/or Services'
            TabOrder = 58
            OnClick = sButton1Click
            Images = DMICON.System16
            ImageIndex = 0
          end
          inherited sPanel62: TsPanel [8]
            Top = 281
          end
          object sPanel92: TsPanel [9]
            Left = 4
            Top = 49
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '20'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 48
          end
          inherited sPanel31: TsPanel [10]
            Top = 401
          end
          inherited sPanel33: TsPanel [11]
            Left = 543
            Top = 481
            Width = 50
            Height = 50
            Visible = False
          end
          inherited edt_Cdcur: TsEdit [12]
            Top = 305
            Enabled = False
          end
          inherited edt_exPlace: TsEdit [13]
            Top = 97
            Enabled = False
          end
          inherited sPanel18: TsPanel [14]
            Top = 169
          end
          inherited edt_Applic2: TsEdit
            Top = 215
            Enabled = False
          end
          inherited sPanel21: TsPanel [16]
            Top = 305
          end
          inherited edt_Applic4: TsEdit
            Top = 259
            Enabled = False
          end
          inherited edt_cdPerM: TsCurrencyEdit [18]
            Top = 329
            Enabled = False
          end
          object sPanel96: TsPanel [19]
            Left = 4
            Top = 121
            Width = 34
            Height = 47
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '40E'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 54
          end
          inherited msk_exDate: TsMaskEdit [20]
            Top = 97
            Enabled = False
          end
          inherited edt_Applic3: TsEdit [21]
            Top = 237
            Enabled = False
          end
          inherited sPanel30: TsPanel [22]
            Top = 377
          end
          inherited edt_Benefc1: TsEdit [23]
            Left = 356
            Top = 193
            Width = 349
            Enabled = False
          end
          inherited sPanel26: TsPanel [24]
            Top = 353
            Height = 71
          end
          inherited sPanel1: TsPanel [25]
            Top = 25
          end
          inherited sPanel15: TsPanel [26]
            Top = 25
          end
          inherited edt_TermPR_M: TsEdit [27]
            Top = 353
            Enabled = False
          end
          inherited edt_plTerm: TsEdit [28]
            Top = 377
            Enabled = False
          end
          inherited edt_Benefc: TsEdit [29]
            Top = 193
            Enabled = False
            Visible = False
          end
          inherited sPanel28: TsPanel [30]
            Top = 353
          end
          object sPanel90: TsPanel [31]
            Left = 4
            Top = 1
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '31C'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 45
          end
          inherited sPanel22: TsPanel [32]
            Top = 305
          end
          inherited sPanel23: TsPanel [33]
            Top = 329
          end
          inherited sPanel14: TsPanel [34]
            Top = 97
          end
          inherited sPanel17: TsPanel [35]
            Top = 169
          end
          inherited edt_Doccd1: TsEdit [36]
            Top = 25
            Enabled = False
            ReadOnly = False
          end
          inherited edt_Doccd: TsEdit [37]
            Top = 25
            Enabled = False
          end
          inherited edt_Benefc2: TsEdit [38]
            Top = 215
            Enabled = False
          end
          inherited edt_TermPR: TsEdit [39]
            Top = 353
            Enabled = False
          end
          inherited edt_Cdamt: TsCurrencyEdit [40]
            Top = 305
            Enabled = False
          end
          inherited edt_Applic5: TsEdit [41]
            Top = 281
            Enabled = False
          end
          inherited edt_Origin: TsEdit [42]
            Top = 401
            Enabled = False
          end
          inherited sPanel19: TsPanel [43]
            Top = 169
          end
          inherited memo_Desgood1: TsMemo [44]
            Left = 4
            Top = 449
            Width = 701
            Height = 90
            ReadOnly = True
          end
          inherited edt_Benefc5: TsEdit [45]
            Top = 281
            Enabled = False
          end
          inherited sPanel13: TsPanel [46]
            Top = 97
          end
          object sPanel94: TsPanel [47]
            Left = 4
            Top = 73
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '23'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 51
          end
          inherited sPanel25: TsPanel [48]
            Top = 329
          end
          inherited edt_cdPerP: TsCurrencyEdit [49]
            Top = 329
            Enabled = False
          end
          inherited edt_Origin_M: TsEdit [50]
            Top = 401
            Enabled = False
          end
          inherited sPanel88: TsPanel
            Top = 281
          end
          object edt_docCredutNo: TsEdit
            Left = 239
            Top = 49
            Width = 466
            Height = 23
            Color = 12582911
            Enabled = False
            MaxLength = 10
            TabOrder = 50
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object sPanel93: TsPanel
            Left = 39
            Top = 49
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Documentary Credit Number'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 49
          end
          object edt_refPreAdvice: TsEdit
            Left = 239
            Top = 73
            Width = 211
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 10
            TabOrder = 53
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object sPanel97: TsPanel
            Left = 39
            Top = 121
            Width = 199
            Height = 47
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Applicable Rules'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 55
          end
          object sPanel95: TsPanel
            Left = 39
            Top = 73
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Reference to Pre-Advice'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 52
          end
          object edt_appRules1: TsEdit
            Left = 239
            Top = 121
            Width = 210
            Height = 23
            Enabled = False
            TabOrder = 56
            Text = 'UCPUPR LATEST VERSION'
          end
          object edt_appRules2: TsEdit
            Left = 239
            Top = 145
            Width = 466
            Height = 23
            Color = clBtnFace
            Enabled = False
            TabOrder = 57
            SkinData.CustomColor = True
          end
          object sPanel91: TsPanel
            Left = 39
            Top = 1
            Width = 199
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Date of Issue'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 46
          end
          object msk_dateOfIssue: TsMaskEdit
            Left = 239
            Top = 1
            Width = 80
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            TabOrder = 47
            Color = clWhite
            BoundLabel.Caption = 'DATE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
          end
        end
      end
      inherited sTabSheet2: TsTabSheet
        inherited sPanel34: TsPanel
          Width = 707
          inherited edt_aaCcv1: TsEdit
            Enabled = False
          end
          inherited edt_aaCcv2: TsEdit
            Enabled = False
          end
          inherited edt_aaCcv3: TsEdit
            Enabled = False
          end
          inherited edt_aaCcv4: TsEdit
            Enabled = False
          end
          inherited sPanel38: TsPanel
            Top = 118
          end
          inherited sPanel39: TsPanel
            Top = 118
          end
          inherited edt_Draft1: TsEdit
            Top = 155
            Width = 292
            Enabled = False
          end
          inherited edt_Draft2: TsEdit
            Top = 177
            Width = 292
            Enabled = False
          end
          inherited edt_Draft3: TsEdit
            Top = 199
            Width = 292
            Enabled = False
          end
          inherited sPanel40: TsPanel
            Top = 223
          end
          inherited edt_mixPay1: TsEdit
            Top = 260
            Width = 292
            Enabled = False
          end
          inherited edt_mixPay2: TsEdit
            Top = 282
            Width = 292
            Enabled = False
          end
          inherited edt_mixPay3: TsEdit
            Top = 304
            Width = 292
            Enabled = False
          end
          inherited edt_mixPay4: TsEdit
            Top = 326
            Width = 292
            Enabled = False
          end
          inherited sPanel41: TsPanel
            Top = 350
          end
          inherited edt_defPay1: TsEdit
            Top = 387
            Width = 292
            Enabled = False
          end
          inherited edt_defPay2: TsEdit
            Top = 409
            Width = 292
            Enabled = False
          end
          inherited edt_defPay3: TsEdit
            Top = 431
            Width = 292
            Enabled = False
          end
          inherited edt_defPay4: TsEdit
            Top = 453
            Width = 292
            Enabled = False
          end
          inherited sPanel42: TsPanel
            Top = 350
          end
          inherited sPanel43: TsPanel
            Top = 260
          end
          inherited sPanel44: TsPanel
            Top = 260
          end
          inherited sPanel45: TsPanel
            Top = 156
          end
          inherited msk_lstDate: TsMaskEdit
            Top = 260
            Enabled = False
          end
          inherited sPanel46: TsPanel
            Top = 285
          end
          inherited sPanel47: TsPanel
            Top = 285
          end
          inherited edt_shipPD1: TsEdit
            Top = 309
            Enabled = False
          end
          inherited edt_shipPD2: TsEdit
            Top = 331
            Enabled = False
          end
          inherited edt_shipPD3: TsEdit
            Top = 353
            Enabled = False
          end
          inherited edt_shipPD4: TsEdit
            Top = 375
            Enabled = False
          end
          inherited edt_shipPD5: TsEdit
            Top = 397
            Enabled = False
          end
          inherited edt_shipPD6: TsEdit
            Top = 419
            Enabled = False
          end
          inherited sPanel50: TsPanel
            Top = 28
          end
          inherited sPanel51: TsPanel
            Top = 28
          end
          inherited sPanel52: TsPanel
            Top = 59
          end
          inherited sPanel53: TsPanel
            Top = 59
          end
          inherited edt_SunjukPort: TsEdit
            Top = 83
            Enabled = False
          end
          inherited sPanel54: TsPanel
            Top = 205
          end
          inherited sPanel55: TsPanel
            Top = 205
          end
          inherited edt_dochackPort: TsEdit
            Top = 229
            Enabled = False
          end
          inherited sPanel56: TsPanel
            Top = 108
          end
          inherited sPanel57: TsPanel
            Top = 108
          end
          inherited sPanel58: TsPanel
            Top = 156
          end
          inherited edt_loadOn: TsEdit
            Top = 132
            Enabled = False
          end
          inherited edt_forTran: TsEdit
            Top = 180
            Enabled = False
          end
          inherited com_Pship: TsComboBox
            Enabled = False
          end
          inherited com_TShip: TsComboBox
            Top = 28
            Enabled = False
          end
          inherited sPanel84: TsPanel
            Top = 223
          end
        end
      end
      object sTabSheet7: TsTabSheet [3]
        Caption = 'PAGE3'
        object sPanel102: TsPanel
          Left = 0
          Top = 0
          Width = 707
          Height = 563
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sPanel103: TsPanel
            Left = 3
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '41a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object sPanel104: TsPanel
            Left = 38
            Top = 4
            Width = 317
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Available With By('#49888#50857#51109#51648#44553#48169#49885' '#48143' '#51008#54665')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object edt_Avail1: TsEdit
            Left = 113
            Top = 28
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_Avail2: TsEdit
            Left = 113
            Top = 50
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_Avail5: TsEdit
            Left = 113
            Top = 118
            Width = 127
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 11
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AvAccnt: TsEdit
            Left = 113
            Top = 142
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel105: TsPanel
            Left = 3
            Top = 28
            Width = 109
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49888#50857#51109#51648#44553#51008#54665
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
          object sPanel106: TsPanel
            Left = 3
            Top = 118
            Width = 109
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'BIC'#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 7
          end
          object sPanel107: TsPanel
            Left = 3
            Top = 142
            Width = 109
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 8
          end
          object sPanel108: TsPanel
            Left = 3
            Top = 166
            Width = 109
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49888#50857#51109#51648#44553#48169#49885
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 9
          end
          object edt_AvPay: TsEdit
            Left = 113
            Top = 166
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_Avail4: TsEdit
            Left = 113
            Top = 94
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_Avail3: TsEdit
            Left = 113
            Top = 72
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel109: TsPanel
            Left = 3
            Top = 364
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '53a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 13
          end
          object sPanel110: TsPanel
            Left = 38
            Top = 364
            Width = 317
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Reimbursing Bank('#49345#54872#51008#54665')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 14
          end
          object edt_ReiBank1: TsEdit
            Left = 113
            Top = 388
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ReiBank2: TsEdit
            Left = 113
            Top = 410
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel111: TsPanel
            Left = 3
            Top = 388
            Width = 109
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49345#54872#51008#54665
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object edt_ReiBank4: TsEdit
            Left = 113
            Top = 454
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ReiBank3: TsEdit
            Left = 113
            Top = 432
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel112: TsPanel
            Left = 3
            Top = 478
            Width = 109
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'BIC'#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 20
          end
          object edt_ReiBank: TsEdit
            Left = 113
            Top = 478
            Width = 127
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 11
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel113: TsPanel
            Left = 3
            Top = 502
            Width = 109
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 22
          end
          object edt_ReiAccnt: TsEdit
            Left = 113
            Top = 502
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel114: TsPanel
            Left = 3
            Top = 196
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '42a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 24
          end
          object sPanel115: TsPanel
            Left = 38
            Top = 196
            Width = 317
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Drawee('#50612#51020#51648#44553#51064')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
          end
          object edt_Drawee1: TsEdit
            Left = 113
            Top = 220
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_Drawee2: TsEdit
            Left = 113
            Top = 242
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 27
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_Drawee_bic: TsEdit
            Left = 113
            Top = 310
            Width = 127
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 11
            TabOrder = 28
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_DrAccnt: TsEdit
            Left = 113
            Top = 334
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 29
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel116: TsPanel
            Left = 3
            Top = 220
            Width = 109
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #50612#51020#51648#44553#51064
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
          end
          object sPanel117: TsPanel
            Left = 3
            Top = 310
            Width = 109
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'BIC'#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 31
          end
          object sPanel118: TsPanel
            Left = 3
            Top = 334
            Width = 109
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 32
          end
          object edt_Drawee4: TsEdit
            Left = 113
            Top = 286
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 33
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_Drawee3: TsEdit
            Left = 113
            Top = 264
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 34
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel98: TsPanel
            Left = 357
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '57a'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 35
          end
          object sPanel99: TsPanel
            Left = 392
            Top = 4
            Width = 317
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '"Advise Through" Bank('#52572#51333#53685#51648#51008#54665')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 36
          end
          object edt_Avtbank1: TsEdit
            Left = 467
            Top = 28
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 37
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_Avtbank2: TsEdit
            Left = 467
            Top = 50
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 38
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel119: TsPanel
            Left = 357
            Top = 28
            Width = 109
            Height = 89
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #52572#51333#53685#51648#51008#54665
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 39
          end
          object edt_Avtbank4: TsEdit
            Left = 467
            Top = 94
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 40
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_Avtbank3: TsEdit
            Left = 467
            Top = 72
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 41
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel120: TsPanel
            Left = 357
            Top = 118
            Width = 109
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'BIC'#53076#46300
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 42
          end
          object edt_Avtbank: TsEdit
            Left = 467
            Top = 118
            Width = 127
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 11
            TabOrder = 43
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel121: TsPanel
            Left = 357
            Top = 142
            Width = 109
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44228#51340#48264#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 44
          end
          object edt_AvtAccnt: TsEdit
            Left = 467
            Top = 142
            Width = 242
            Height = 23
            Color = clWhite
            Enabled = False
            TabOrder = 45
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel100: TsPanel
            Left = 357
            Top = 196
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '72Z'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 46
          end
          object sPanel101: TsPanel
            Left = 392
            Top = 196
            Width = 317
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Sender to Receive Information('#49688#49888#51008#54665#50526' '#51221#48372')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 47
          end
          object edt_sndInfo1: TsEdit
            Left = 357
            Top = 220
            Width = 352
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 48
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_sndInfo2: TsEdit
            Left = 357
            Top = 242
            Width = 352
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 49
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_sndInfo3: TsEdit
            Left = 357
            Top = 264
            Width = 352
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 35
            TabOrder = 50
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel122: TsPanel
            Left = 434
            Top = 364
            Width = 275
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44060#49444#51008#54665' '#51204#51088#49436#47749
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 51
          end
          object edt_OpBank1: TsEdit
            Left = 357
            Top = 388
            Width = 352
            Height = 23
            Color = clBtnFace
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 52
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OpBank2: TsEdit
            Left = 357
            Top = 410
            Width = 352
            Height = 23
            Color = clBtnFace
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 53
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OpBank3: TsEdit
            Left = 434
            Top = 434
            Width = 275
            Height = 23
            Color = clBtnFace
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 54
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OpAddr1: TsEdit
            Left = 434
            Top = 458
            Width = 275
            Height = 23
            Color = clBtnFace
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 55
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OpAddr2: TsEdit
            Left = 434
            Top = 480
            Width = 275
            Height = 23
            Color = clBtnFace
            Enabled = False
            MaxLength = 35
            ReadOnly = True
            TabOrder = 56
            SkinData.CustomColor = True
            BoundLabel.Caption = 'PLACE'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel123: TsPanel
            Left = 357
            Top = 434
            Width = 76
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49885#48324#48512#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 57
          end
          object sPanel124: TsPanel
            Left = 357
            Top = 458
            Width = 76
            Height = 45
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51452#49548
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 58
          end
          object sPanel125: TsPanel
            Left = 357
            Top = 364
            Width = 76
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'NON-SWIFT'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 59
          end
        end
      end
      inherited sTabSheet5: TsTabSheet
        Caption = 'PAGE 4'
        inherited sPanel35: TsPanel
          Width = 707
          inherited chk_380: TsCheckBox
            Width = 223
            Enabled = False
          end
          inherited edt_380_1: TsEdit
            Enabled = False
          end
          inherited com_705Gubun: TsComboBox
            Enabled = False
          end
          inherited chk_705: TsCheckBox
            Enabled = False
          end
          inherited edt_705_1: TsEdit
            Enabled = False
          end
          inherited edt_705_2: TsEdit
            Enabled = False
          end
          inherited com_705_3: TsComboBox
            Enabled = False
          end
          inherited edt_705_4: TsEdit
            Enabled = False
          end
          inherited chk_740: TsCheckBox
            Width = 199
            Enabled = False
          end
          inherited edt_740_1: TsEdit
            Enabled = False
          end
          inherited edt_740_2: TsEdit
            Enabled = False
          end
          inherited com_740_3: TsComboBox
            Enabled = False
          end
          inherited edt_740_4: TsEdit
            Enabled = False
          end
          inherited chk_760: TsCheckBox
            Width = 473
            Enabled = False
          end
          inherited edt_760_1: TsEdit
            Enabled = False
          end
          inherited edt_760_2: TsEdit
            Enabled = False
          end
          inherited com_760_3: TsComboBox
            Enabled = False
          end
          inherited edt_760_4: TsEdit
            Enabled = False
          end
          inherited chk_530: TsCheckBox
            Width = 643
            Enabled = False
          end
          inherited edt_530_1: TsEdit
            Enabled = False
          end
          inherited edt_530_2: TsEdit
            Enabled = False
          end
          inherited chk_271: TsCheckBox
            Width = 126
            Enabled = False
          end
          inherited edt_271_1: TsEdit
            Enabled = False
          end
          inherited chk_861: TsCheckBox
            Width = 163
            Enabled = False
          end
          inherited chk_2AA: TsCheckBox
            Width = 206
            Enabled = False
          end
          inherited memo_2AA_1: TsMemo
            ReadOnly = True
          end
        end
      end
      inherited sTabSheet6: TsTabSheet
        Caption = 'PAGE 5'
        inherited sPanel71: TsPanel
          Width = 707
          inherited chk_ACD2AA: TsCheckBox
            Width = 102
            Enabled = False
          end
          inherited edt_ACD2AA_1: TsEdit
            Enabled = False
          end
          inherited chk_ACD2AB: TsCheckBox
            Width = 481
            Enabled = False
          end
          inherited chk_ACD2AC: TsCheckBox
            Width = 314
            Enabled = False
          end
          inherited chk_ACD2AD: TsCheckBox
            Width = 235
            Enabled = False
          end
          inherited chk_ACD2AE: TsCheckBox
            Width = 197
            Enabled = False
          end
          inherited memo_ACD2AE_1: TsMemo
            ReadOnly = True
          end
          inherited com_Charge: TsComboBox
            Enabled = False
          end
          inherited memo_Charge_1: TsMemo
            ReadOnly = True
          end
          inherited edt_period: TsEdit
            Enabled = False
          end
          inherited com_PeriodIndex: TsComboBox
            Enabled = False
          end
          inherited edt_PeriodDetail: TsEdit
            Enabled = False
          end
          inherited com_Confirm: TsComboBox
            Enabled = False
          end
          inherited sPanel82: TsPanel
            Caption = '58a'
          end
          inherited edt_CONFIRM_BICCD: TsEdit
            Enabled = False
          end
          inherited sEdit70: TsEdit
            Enabled = False
          end
          inherited edt_CONFIRM_BANKNM: TsEdit
            Enabled = False
          end
          inherited edt_CONFIRM_BANKBR: TsEdit
            Enabled = False
          end
          inherited memo_SPECIAL_PAY: TsMemo
            ReadOnly = True
          end
          inherited edt_CONFIRM_BICNM: TsEdit
            Enabled = False
          end
        end
      end
      object sTabSheet8: TsTabSheet [6]
        Caption = 'PAGE 6'
        object sPanel130: TsPanel
          Left = 0
          Top = 0
          Width = 707
          Height = 563
          Align = alClient
          
          TabOrder = 0
          object sPanel129: TsPanel
            Left = 39
            Top = 4
            Width = 338
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'Instructions to the Paying/Accepting/Negotiating Bank'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object sPanel128: TsPanel
            Left = 4
            Top = 4
            Width = 34
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = '78'
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object memo_INSTRCT_1: TsMemo
            Left = 4
            Top = 28
            Width = 701
            Height = 493
            Ctl3D = True
            ParentCtl3D = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 2
            WordWrap = False
          end
        end
      end
      inherited sTabSheet4: TsTabSheet
        inherited sDBGrid3: TsDBGrid
          Width = 707
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'BENEFC1'
              Title.Alignment = taCenter
              Title.Caption = #49688#54812#51088
              Width = 172
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'AP_BANK1'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51008#54665
              Width = 164
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'APP_DATE'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51068#51088
              Width = 82
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'CD_AMT'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#44552#50529
              Width = 97
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'CD_CUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 38
              Visible = True
            end>
        end
        inherited sPanel24: TsPanel
          Width = 707
          inherited sSpeedButton12: TsSpeedButton
            Left = 232
          end
          inherited sBitBtn5: TsBitBtn
            Left = 493
          end
          inherited sEdit1: TsEdit
            Left = 321
            BoundLabel.Active = False
          end
          object sComboBox2: TsComboBox
            Left = 247
            Top = 4
            Width = 73
            Height = 23
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 1
            TabOrder = 4
            Text = 'LC'#48264#54840
            OnChange = sComboBox2Change
            Items.Strings = (
              #44288#47532#48264#54840
              'LC'#48264#54840)
          end
        end
      end
      object sTabSheet9: TsTabSheet
        Caption = 'sTabSheet9'
        TabVisible = False
        object sMemo1: TsMemo
          Left = 0
          Top = 0
          Width = 711
          Height = 539
          Align = alClient
          ScrollBars = ssBoth
          TabOrder = 0
          WordWrap = False
        end
        object sButton2: TsButton
          Left = 637
          Top = 475
          Width = 44
          Height = 35
          Cursor = crHandPoint
          Hint = #49440#53469#54620' '#45936#51060#53552#47484' ERP'#50640' '#50732#47549#45768#45796
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          TabStop = False
          OnClick = sButton2Click
          SkinData.SkinSection = 'BUTTON'
          Reflected = True
          Images = DMICON.System26
          ImageIndex = 22
          ContentMargin = 8
        end
      end
    end
  end
  inherited qryList: TADOQuery
    Left = 16
    Top = 624
  end
  inherited dsList: TDataSource
    DataSet = qryINF700
    Left = 120
    Top = 224
  end
  object qryINF700: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryINF700AfterOpen
    AfterScroll = qryINF700AfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20100101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180730'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      ''
      
        'SELECT I700_1.MAINT_NO, I700_1.MSEQ, I700_1.MESSAGE1,I700_1.MESS' +
        'AGE2,I700_1.[USER_ID],I700_1.DATEE,I700_1.APP_DATE,I700_1.IN_MAT' +
        'HOD,I700_1.AP_BANK,I700_1.AP_BANK1,I700_1.AP_BANK2,I700_1.AP_BAN' +
        'K3,I700_1.AP_BANK4,I700_1.AP_BANK5,I700_1.AD_BANK,I700_1.AD_BANK' +
        '1'
      
        '      ,I700_1.AD_BANK2,I700_1.AD_BANK3,I700_1.AD_BANK4,I700_1.AD' +
        '_PAY,I700_1.IL_NO1,I700_1.IL_NO2,I700_1.IL_NO3,I700_1.IL_NO4,I70' +
        '0_1.IL_NO5,I700_1.IL_AMT1,I700_1.IL_AMT2,I700_1.IL_AMT3,I700_1.I' +
        'L_AMT4,I700_1.IL_AMT5,I700_1.IL_CUR1,I700_1.IL_CUR2,I700_1.IL_CU' +
        'R3'
      
        '      ,I700_1.IL_CUR4,I700_1.IL_CUR5,I700_1.AD_INFO1,I700_1.AD_I' +
        'NFO2,I700_1.AD_INFO3,I700_1.AD_INFO4,I700_1.AD_INFO5,I700_1.DOC_' +
        'CD,I700_1.CD_NO,I700_1.REF_PRE,I700_1.ISS_DATE,I700_1.EX_DATE,I7' +
        '00_1.EX_PLACE,I700_1.CHK1,I700_1.CHK2,I700_1.CHK3,I700_1.prno'
      
        '      ,I700_1.F_INTERFACE,I700_1.IMP_CD1,I700_1.IMP_CD2,I700_1.I' +
        'MP_CD3,I700_1.IMP_CD4,I700_1.IMP_CD5,'
      ''
      
        '       I700_2.MAINT_NO,I700_2.APP_BANK,I700_2.APP_BANK1,I700_2.A' +
        'PP_BANK2,I700_2.APP_BANK3,I700_2.APP_BANK4,I700_2.APP_BANK5,I700' +
        '_2.APP_ACCNT,I700_2.APPLIC1,I700_2.APPLIC2,I700_2.APPLIC3,I700_2' +
        '.APPLIC4,I700_2.APPLIC5,I700_2.BENEFC1'
      
        '      ,I700_2.BENEFC2,I700_2.BENEFC3,I700_2.BENEFC4,I700_2.BENEF' +
        'C5,I700_2.CD_AMT,I700_2.CD_CUR,I700_2.CD_PERP,I700_2.CD_PERM,I70' +
        '0_2.CD_MAX,I700_2.AA_CV1,I700_2.AA_CV2,I700_2.AA_CV3,I700_2.AA_C' +
        'V4,I700_2.AVAIL,I700_2.AVAIL1,I700_2.AVAIL2,I700_2.AVAIL3'
      
        '      ,I700_2.AVAIL4,I700_2.AV_ACCNT,I700_2.AV_PAY,I700_2.DRAFT1' +
        ',I700_2.DRAFT2,I700_2.DRAFT3,I700_2.DRAWEE,I700_2.DRAWEE1,I700_2' +
        '.DRAWEE2,I700_2.DRAWEE3,I700_2.DRAWEE4,I700_2.DR_ACCNT,'
      ''
      
        '       I700_3.MAINT_NO,I700_3.PSHIP,I700_3.TSHIP,I700_3.LOAD_ON,' +
        'I700_3.FOR_TRAN,I700_3.LST_DATE,I700_3.SHIP_PD,I700_3.SHIP_PD1,I' +
        '700_3.SHIP_PD2,I700_3.SHIP_PD3,I700_3.SHIP_PD4,I700_3.SHIP_PD5,I' +
        '700_3.SHIP_PD6,I700_3.DESGOOD,I700_3.DESGOOD_1,I700_3.TERM_PR'
      
        '      ,I700_3.TERM_PR_M,I700_3.PL_TERM,I700_3.ORIGIN,I700_3.ORIG' +
        'IN_M,I700_3.DOC_380,I700_3.DOC_380_1,I700_3.DOC_705,I700_3.DOC_7' +
        '05_1,I700_3.DOC_705_2,I700_3.DOC_705_3,I700_3.DOC_705_4,I700_3.D' +
        'OC_740,I700_3.DOC_740_1,I700_3.DOC_740_2,I700_3.DOC_740_3'
      
        '      ,I700_3.DOC_740_4,I700_3.DOC_530,I700_3.DOC_530_1,I700_3.D' +
        'OC_530_2,I700_3.DOC_271,I700_3.DOC_271_1,I700_3.DOC_861,I700_3.D' +
        'OC_2AA,I700_3.DOC_2AA_1,I700_3.ACD_2AA,I700_3.ACD_2AA_1,I700_3.A' +
        'CD_2AB,I700_3.ACD_2AC,I700_3.ACD_2AD,I700_3.ACD_2AE'
      
        '      ,I700_3.ACD_2AE_1,I700_3.CHARGE,I700_3.PERIOD,I700_3.CONFI' +
        'RMM,I700_3.DEF_PAY1,I700_3.DEF_PAY2,I700_3.DEF_PAY3,I700_3.DEF_P' +
        'AY4,I700_3.DOC_705_GUBUN,'
      ''
      
        '       I700_4.MAINT_NO,I700_4.REI_BANK,I700_4.REI_BANK1,I700_4.R' +
        'EI_BANK2,I700_4.REI_BANK3,I700_4.REI_BANK4,I700_4.REI_BANK5,I700' +
        '_4.REI_ACNNT,I700_4.INSTRCT,I700_4.INSTRCT_1,I700_4.AVT_BANK,I70' +
        '0_4.AVT_BANK1,I700_4.AVT_BANK2,I700_4.AVT_BANK3'
      
        '      ,I700_4.AVT_BANK4,I700_4.AVT_BANK5,I700_4.AVT_ACCNT,I700_4' +
        '.SND_INFO1,I700_4.SND_INFO2,I700_4.SND_INFO3,I700_4.SND_INFO4,I7' +
        '00_4.SND_INFO5,I700_4.SND_INFO6,I700_4.EX_NAME1,I700_4.EX_NAME2,' +
        'I700_4.EX_NAME3,I700_4.EX_ADDR1,I700_4.EX_ADDR2'
      
        '      ,I700_4.EX_ADDR3,I700_4.OP_BANK1,I700_4.OP_BANK2,I700_4.OP' +
        '_BANK3,I700_4.OP_ADDR1,I700_4.OP_ADDR2,I700_4.OP_ADDR3,I700_4.MI' +
        'X_PAY1,I700_4.MIX_PAY2,I700_4.MIX_PAY3,I700_4.MIX_PAY4'
      
        '      ,I700_4.APPLICABLE_RULES_1,I700_4.APPLICABLE_RULES_2,I700_' +
        '4.DOC_760,I700_4.DOC_760_1,I700_4.DOC_760_2,I700_4.DOC_760_3,I70' +
        '0_4.DOC_760_4,I700_4.SUNJUCK_PORT,I700_4.DOCHACK_PORT'
      #9'  ,Mathod700.DOC_NAME as mathod_Name'
      #9'  ,Pay700.DOC_NAME as pay_Name'
      #9'  ,IMPCD700_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD700_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD700_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD700_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD700_5.DOC_NAME as Imp_Name_5'
      #9'  ,DocCD700.DOC_NAME as DOC_Name'
      #9'  ,CDMAX700.DOC_NAME as CDMAX_Name'
      #9'  ,Pship700.DOC_NAME as pship_Name'
      #9'  ,Tship700.DOC_NAME as tship_Name'
      #9'  ,doc705_700.DOC_NAME as doc705_Name'
      #9'  ,doc740_700.DOC_NAME as doc740_Name'
      #9'  ,doc760_700.DOC_NAME as doc760_Name'
      #9'  ,CHARGE700.DOC_NAME as CHARGE_Name'
      #9'  ,CONFIRM700.DOC_NAME as CONFIRM_Name'
      
        ', CHARGE_1, CONFIRM_BICCD, CONFIRM_BANKNM, CONFIRM_BANKBR, PERIO' +
        'D_IDX, PERIOD_TXT, SPECIAL_PAY, AVAIL_BIC, DRAWEE_BIC'
      ''
      'FROM [dbo].[INF700_1] AS I700_1'
      
        'LEFT JOIN [dbo].[INF700_2] AS I700_2 ON I700_1.MAINT_NO = I700_2' +
        '.MAINT_NO AND I700_1.MSEQ = I700_2.MSEQ'
      
        'LEFT JOIN [dbo].[INF700_3] AS I700_3 ON I700_1.MAINT_NO = I700_3' +
        '.MAINT_NO AND I700_1.MSEQ = I700_3.MSEQ '
      
        'LEFT JOIN [dbo].[INF700_4] AS I700_4 ON I700_1.MAINT_NO = I700_4' +
        '.MAINT_NO AND I700_1.MSEQ = I700_4.MSEQ'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod700 ON I700_1.IN_MATHOD = Mathod' +
        '700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay700 ON I700_1.AD_PAY = Pay700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_1 ON I700_1.IMP_CD1 = IMPCD' +
        '700_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_2 ON I700_1.IMP_CD2 = IMPCD' +
        '700_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_3 ON I700_1.IMP_CD3 = IMPCD' +
        '700_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_4 ON I700_1.IMP_CD4 = IMPCD' +
        '700_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_5 ON I700_1.IMP_CD5 = IMPCD' +
        '700_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39') DocCD700 ON I700_1.DOC_CD = DocCD700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX700 ON I700_2.CD_MAX = CDMAX700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'PSHIP'#39') Pship700 ON I700_3.PSHIP = Pship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'TSHIP'#39') Tship700 ON I700_3.TSHIP = Tship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_705'#39') doc705_700 ON I700_3.DOC_705_3 = do' +
        'c705_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc740_700 ON I700_3.DOC_740_3 = do' +
        'c740_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc760_700 ON I700_4.DOC_760_3 = do' +
        'c760_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CHARGE'#39') CHARGE700 ON I700_3.CHARGE = CHARGE7' +
        '00.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CONFIRM'#39') CONFIRM700 ON I700_3.CONFIRMM = CON' +
        'FIRM700.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(I700_1.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 88
    Top = 224
    object qryINF700MAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryINF700MESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryINF700MESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryINF700USER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 15
    end
    object qryINF700DATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryINF700APP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryINF700IN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryINF700AP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 11
    end
    object qryINF700AP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryINF700AP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryINF700AP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryINF700AP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryINF700AP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryINF700AD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 11
    end
    object qryINF700AD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryINF700AD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryINF700AD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryINF700AD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryINF700AD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object qryINF700IL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryINF700IL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryINF700IL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryINF700IL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryINF700IL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryINF700IL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryINF700IL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryINF700IL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryINF700IL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryINF700IL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryINF700IL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryINF700IL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryINF700IL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryINF700IL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryINF700IL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryINF700AD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryINF700AD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryINF700AD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryINF700AD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryINF700AD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryINF700DOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object qryINF700CD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryINF700REF_PRE: TStringField
      FieldName = 'REF_PRE'
      Size = 35
    end
    object qryINF700ISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryINF700EX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryINF700EX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryINF700CHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryINF700CHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryINF700CHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryINF700prno: TIntegerField
      FieldName = 'prno'
    end
    object qryINF700F_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryINF700IMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryINF700IMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryINF700IMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryINF700IMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryINF700IMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryINF700MAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryINF700APP_BANK: TStringField
      FieldName = 'APP_BANK'
      Size = 11
    end
    object qryINF700APP_BANK1: TStringField
      FieldName = 'APP_BANK1'
      Size = 35
    end
    object qryINF700APP_BANK2: TStringField
      FieldName = 'APP_BANK2'
      Size = 35
    end
    object qryINF700APP_BANK3: TStringField
      FieldName = 'APP_BANK3'
      Size = 35
    end
    object qryINF700APP_BANK4: TStringField
      FieldName = 'APP_BANK4'
      Size = 35
    end
    object qryINF700APP_BANK5: TStringField
      FieldName = 'APP_BANK5'
      Size = 35
    end
    object qryINF700APP_ACCNT: TStringField
      FieldName = 'APP_ACCNT'
      Size = 35
    end
    object qryINF700APPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryINF700APPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryINF700APPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryINF700APPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryINF700APPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryINF700BENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryINF700BENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryINF700BENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryINF700BENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryINF700BENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryINF700CD_AMT: TBCDField
      FieldName = 'CD_AMT'
      Precision = 18
    end
    object qryINF700CD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryINF700CD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryINF700CD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryINF700CD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryINF700AA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryINF700AA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryINF700AA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryINF700AA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryINF700AVAIL: TStringField
      FieldName = 'AVAIL'
      Size = 11
    end
    object qryINF700AVAIL1: TStringField
      FieldName = 'AVAIL1'
      Size = 35
    end
    object qryINF700AVAIL2: TStringField
      FieldName = 'AVAIL2'
      Size = 35
    end
    object qryINF700AVAIL3: TStringField
      FieldName = 'AVAIL3'
      Size = 35
    end
    object qryINF700AVAIL4: TStringField
      FieldName = 'AVAIL4'
      Size = 35
    end
    object qryINF700AV_ACCNT: TStringField
      FieldName = 'AV_ACCNT'
      Size = 35
    end
    object qryINF700AV_PAY: TStringField
      FieldName = 'AV_PAY'
      Size = 35
    end
    object qryINF700DRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryINF700DRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryINF700DRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryINF700DRAWEE: TStringField
      FieldName = 'DRAWEE'
      Size = 11
    end
    object qryINF700DRAWEE1: TStringField
      FieldName = 'DRAWEE1'
      Size = 35
    end
    object qryINF700DRAWEE2: TStringField
      FieldName = 'DRAWEE2'
      Size = 35
    end
    object qryINF700DRAWEE3: TStringField
      FieldName = 'DRAWEE3'
      Size = 35
    end
    object qryINF700DRAWEE4: TStringField
      FieldName = 'DRAWEE4'
      Size = 35
    end
    object qryINF700DR_ACCNT: TStringField
      FieldName = 'DR_ACCNT'
      Size = 35
    end
    object qryINF700MAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryINF700PSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object qryINF700TSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object qryINF700LOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryINF700FOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryINF700LST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryINF700SHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object qryINF700SHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryINF700SHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryINF700SHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryINF700SHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryINF700SHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryINF700SHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryINF700DESGOOD: TBooleanField
      FieldName = 'DESGOOD'
    end
    object qryINF700DESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryINF700TERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object qryINF700TERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object qryINF700PL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object qryINF700ORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object qryINF700ORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object qryINF700DOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object qryINF700DOC_380_1: TBCDField
      FieldName = 'DOC_380_1'
      Precision = 18
    end
    object qryINF700DOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object qryINF700DOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object qryINF700DOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object qryINF700DOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object qryINF700DOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object qryINF700DOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object qryINF700DOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object qryINF700DOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object qryINF700DOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object qryINF700DOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object qryINF700DOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object qryINF700DOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object qryINF700DOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object qryINF700DOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object qryINF700DOC_271_1: TBCDField
      FieldName = 'DOC_271_1'
      Precision = 18
    end
    object qryINF700DOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object qryINF700DOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object qryINF700DOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
    end
    object qryINF700ACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object qryINF700ACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object qryINF700ACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object qryINF700ACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object qryINF700ACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object qryINF700ACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object qryINF700ACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
    end
    object qryINF700CHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryINF700PERIOD: TBCDField
      FieldName = 'PERIOD'
      Precision = 18
    end
    object qryINF700CONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object qryINF700DEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryINF700DEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryINF700DEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryINF700DEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryINF700DOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object qryINF700MAINT_NO_3: TStringField
      FieldName = 'MAINT_NO_3'
      Size = 35
    end
    object qryINF700REI_BANK: TStringField
      FieldName = 'REI_BANK'
      Size = 11
    end
    object qryINF700REI_BANK1: TStringField
      FieldName = 'REI_BANK1'
      Size = 35
    end
    object qryINF700REI_BANK2: TStringField
      FieldName = 'REI_BANK2'
      Size = 35
    end
    object qryINF700REI_BANK3: TStringField
      FieldName = 'REI_BANK3'
      Size = 35
    end
    object qryINF700REI_BANK4: TStringField
      FieldName = 'REI_BANK4'
      Size = 35
    end
    object qryINF700REI_BANK5: TStringField
      FieldName = 'REI_BANK5'
      Size = 35
    end
    object qryINF700REI_ACNNT: TStringField
      FieldName = 'REI_ACNNT'
      Size = 35
    end
    object qryINF700INSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object qryINF700INSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryINF700AVT_BANK: TStringField
      FieldName = 'AVT_BANK'
      Size = 11
    end
    object qryINF700AVT_BANK1: TStringField
      FieldName = 'AVT_BANK1'
      Size = 35
    end
    object qryINF700AVT_BANK2: TStringField
      FieldName = 'AVT_BANK2'
      Size = 35
    end
    object qryINF700AVT_BANK3: TStringField
      FieldName = 'AVT_BANK3'
      Size = 35
    end
    object qryINF700AVT_BANK4: TStringField
      FieldName = 'AVT_BANK4'
      Size = 35
    end
    object qryINF700AVT_BANK5: TStringField
      FieldName = 'AVT_BANK5'
      Size = 35
    end
    object qryINF700AVT_ACCNT: TStringField
      FieldName = 'AVT_ACCNT'
      Size = 35
    end
    object qryINF700SND_INFO1: TStringField
      FieldName = 'SND_INFO1'
      Size = 35
    end
    object qryINF700SND_INFO2: TStringField
      FieldName = 'SND_INFO2'
      Size = 35
    end
    object qryINF700SND_INFO3: TStringField
      FieldName = 'SND_INFO3'
      Size = 35
    end
    object qryINF700SND_INFO4: TStringField
      FieldName = 'SND_INFO4'
      Size = 35
    end
    object qryINF700SND_INFO5: TStringField
      FieldName = 'SND_INFO5'
      Size = 35
    end
    object qryINF700SND_INFO6: TStringField
      FieldName = 'SND_INFO6'
      Size = 35
    end
    object qryINF700EX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryINF700EX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryINF700EX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryINF700EX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryINF700EX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryINF700EX_ADDR3: TStringField
      FieldName = 'EX_ADDR3'
      Size = 35
    end
    object qryINF700OP_BANK1: TStringField
      FieldName = 'OP_BANK1'
      Size = 35
    end
    object qryINF700OP_BANK2: TStringField
      FieldName = 'OP_BANK2'
      Size = 35
    end
    object qryINF700OP_BANK3: TStringField
      FieldName = 'OP_BANK3'
      Size = 35
    end
    object qryINF700OP_ADDR1: TStringField
      FieldName = 'OP_ADDR1'
      Size = 35
    end
    object qryINF700OP_ADDR2: TStringField
      FieldName = 'OP_ADDR2'
      Size = 35
    end
    object qryINF700OP_ADDR3: TStringField
      FieldName = 'OP_ADDR3'
      Size = 35
    end
    object qryINF700MIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryINF700MIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryINF700MIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryINF700MIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryINF700APPLICABLE_RULES_1: TStringField
      FieldName = 'APPLICABLE_RULES_1'
      Size = 30
    end
    object qryINF700APPLICABLE_RULES_2: TStringField
      FieldName = 'APPLICABLE_RULES_2'
      Size = 35
    end
    object qryINF700DOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object qryINF700DOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object qryINF700DOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object qryINF700DOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object qryINF700DOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object qryINF700SUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryINF700DOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryINF700mathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryINF700pay_Name: TStringField
      FieldName = 'pay_Name'
      Size = 100
    end
    object qryINF700Imp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryINF700Imp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryINF700Imp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryINF700Imp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryINF700Imp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryINF700DOC_Name: TStringField
      FieldName = 'DOC_Name'
      Size = 100
    end
    object qryINF700CDMAX_Name: TStringField
      FieldName = 'CDMAX_Name'
      Size = 100
    end
    object qryINF700pship_Name: TStringField
      FieldName = 'pship_Name'
      Size = 100
    end
    object qryINF700tship_Name: TStringField
      FieldName = 'tship_Name'
      Size = 100
    end
    object qryINF700doc705_Name: TStringField
      FieldName = 'doc705_Name'
      Size = 100
    end
    object qryINF700doc740_Name: TStringField
      FieldName = 'doc740_Name'
      Size = 100
    end
    object qryINF700doc760_Name: TStringField
      FieldName = 'doc760_Name'
      Size = 100
    end
    object qryINF700CHARGE_Name: TStringField
      FieldName = 'CHARGE_Name'
      Size = 100
    end
    object qryINF700CONFIRM_Name: TStringField
      FieldName = 'CONFIRM_Name'
      Size = 100
    end
    object qryINF700CHARGE_1: TMemoField
      FieldName = 'CHARGE_1'
      BlobType = ftMemo
    end
    object qryINF700CONFIRM_BICCD: TStringField
      FieldName = 'CONFIRM_BICCD'
      Size = 11
    end
    object qryINF700CONFIRM_BANKNM: TStringField
      FieldName = 'CONFIRM_BANKNM'
      Size = 70
    end
    object qryINF700CONFIRM_BANKBR: TStringField
      FieldName = 'CONFIRM_BANKBR'
      Size = 70
    end
    object qryINF700PERIOD_IDX: TIntegerField
      FieldName = 'PERIOD_IDX'
    end
    object qryINF700PERIOD_TXT: TStringField
      FieldName = 'PERIOD_TXT'
      Size = 35
    end
    object qryINF700SPECIAL_PAY: TMemoField
      FieldName = 'SPECIAL_PAY'
      BlobType = ftMemo
    end
    object qryINF700AVAIL_BIC: TStringField
      FieldName = 'AVAIL_BIC'
      Size = 11
    end
    object qryINF700DRAWEE_BIC: TStringField
      FieldName = 'DRAWEE_BIC'
      Size = 11
    end
    object qryINF700MSEQ: TIntegerField
      FieldName = 'MSEQ'
    end
  end
  object qryCopy: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryINF700AfterOpen
    AfterScroll = qryINF700AfterScroll
    Parameters = <>
    SQL.Strings = (
      ''
      
        'SELECT I700_1.MAINT_NO, I700_1.MSEQ, I700_1.MESSAGE1,I700_1.MESS' +
        'AGE2,I700_1.[USER_ID],I700_1.DATEE,I700_1.APP_DATE,I700_1.IN_MAT' +
        'HOD,I700_1.AP_BANK,I700_1.AP_BANK1,I700_1.AP_BANK2,I700_1.AP_BAN' +
        'K3,I700_1.AP_BANK4,I700_1.AP_BANK5,I700_1.AD_BANK,I700_1.AD_BANK' +
        '1'
      
        '      ,I700_1.AD_BANK2,I700_1.AD_BANK3,I700_1.AD_BANK4,I700_1.AD' +
        '_PAY,I700_1.IL_NO1,I700_1.IL_NO2,I700_1.IL_NO3,I700_1.IL_NO4,I70' +
        '0_1.IL_NO5,I700_1.IL_AMT1,I700_1.IL_AMT2,I700_1.IL_AMT3,I700_1.I' +
        'L_AMT4,I700_1.IL_AMT5,I700_1.IL_CUR1,I700_1.IL_CUR2,I700_1.IL_CU' +
        'R3'
      
        '      ,I700_1.IL_CUR4,I700_1.IL_CUR5,I700_1.AD_INFO1,I700_1.AD_I' +
        'NFO2,I700_1.AD_INFO3,I700_1.AD_INFO4,I700_1.AD_INFO5,I700_1.DOC_' +
        'CD,I700_1.CD_NO,I700_1.REF_PRE,I700_1.ISS_DATE,I700_1.EX_DATE,I7' +
        '00_1.EX_PLACE,I700_1.CHK1,I700_1.CHK2,I700_1.CHK3,I700_1.prno'
      
        '      ,I700_1.F_INTERFACE,I700_1.IMP_CD1,I700_1.IMP_CD2,I700_1.I' +
        'MP_CD3,I700_1.IMP_CD4,I700_1.IMP_CD5,'
      ''
      
        '       I700_2.MAINT_NO,I700_2.APP_BANK,I700_2.APP_BANK1,I700_2.A' +
        'PP_BANK2,I700_2.APP_BANK3,I700_2.APP_BANK4,I700_2.APP_BANK5,I700' +
        '_2.APP_ACCNT,I700_2.APPLIC1,I700_2.APPLIC2,I700_2.APPLIC3,I700_2' +
        '.APPLIC4,I700_2.APPLIC5,I700_2.BENEFC1'
      
        '      ,I700_2.BENEFC2,I700_2.BENEFC3,I700_2.BENEFC4,I700_2.BENEF' +
        'C5,I700_2.CD_AMT,I700_2.CD_CUR,I700_2.CD_PERP,I700_2.CD_PERM,I70' +
        '0_2.CD_MAX,I700_2.AA_CV1,I700_2.AA_CV2,I700_2.AA_CV3,I700_2.AA_C' +
        'V4,I700_2.AVAIL,I700_2.AVAIL1,I700_2.AVAIL2,I700_2.AVAIL3'
      
        '      ,I700_2.AVAIL4,I700_2.AV_ACCNT,I700_2.AV_PAY,I700_2.DRAFT1' +
        ',I700_2.DRAFT2,I700_2.DRAFT3,I700_2.DRAWEE,I700_2.DRAWEE1,I700_2' +
        '.DRAWEE2,I700_2.DRAWEE3,I700_2.DRAWEE4,I700_2.DR_ACCNT,'
      ''
      
        '       I700_3.MAINT_NO,I700_3.PSHIP,I700_3.TSHIP,I700_3.LOAD_ON,' +
        'I700_3.FOR_TRAN,I700_3.LST_DATE,I700_3.SHIP_PD,I700_3.SHIP_PD1,I' +
        '700_3.SHIP_PD2,I700_3.SHIP_PD3,I700_3.SHIP_PD4,I700_3.SHIP_PD5,I' +
        '700_3.SHIP_PD6,I700_3.DESGOOD,I700_3.DESGOOD_1,I700_3.TERM_PR'
      
        '      ,I700_3.TERM_PR_M,I700_3.PL_TERM,I700_3.ORIGIN,I700_3.ORIG' +
        'IN_M,I700_3.DOC_380,I700_3.DOC_380_1,I700_3.DOC_705,I700_3.DOC_7' +
        '05_1,I700_3.DOC_705_2,I700_3.DOC_705_3,I700_3.DOC_705_4,I700_3.D' +
        'OC_740,I700_3.DOC_740_1,I700_3.DOC_740_2,I700_3.DOC_740_3'
      
        '      ,I700_3.DOC_740_4,I700_3.DOC_530,I700_3.DOC_530_1,I700_3.D' +
        'OC_530_2,I700_3.DOC_271,I700_3.DOC_271_1,I700_3.DOC_861,I700_3.D' +
        'OC_2AA,I700_3.DOC_2AA_1,I700_3.ACD_2AA,I700_3.ACD_2AA_1,I700_3.A' +
        'CD_2AB,I700_3.ACD_2AC,I700_3.ACD_2AD,I700_3.ACD_2AE'
      
        '      ,I700_3.ACD_2AE_1,I700_3.CHARGE,I700_3.PERIOD,I700_3.CONFI' +
        'RMM,I700_3.DEF_PAY1,I700_3.DEF_PAY2,I700_3.DEF_PAY3,I700_3.DEF_P' +
        'AY4,I700_3.DOC_705_GUBUN,'
      ''
      
        '       I700_4.MAINT_NO,I700_4.REI_BANK,I700_4.REI_BANK1,I700_4.R' +
        'EI_BANK2,I700_4.REI_BANK3,I700_4.REI_BANK4,I700_4.REI_BANK5,I700' +
        '_4.REI_ACNNT,I700_4.INSTRCT,I700_4.INSTRCT_1,I700_4.AVT_BANK,I70' +
        '0_4.AVT_BANK1,I700_4.AVT_BANK2,I700_4.AVT_BANK3'
      
        '      ,I700_4.AVT_BANK4,I700_4.AVT_BANK5,I700_4.AVT_ACCNT,I700_4' +
        '.SND_INFO1,I700_4.SND_INFO2,I700_4.SND_INFO3,I700_4.SND_INFO4,I7' +
        '00_4.SND_INFO5,I700_4.SND_INFO6,I700_4.EX_NAME1,I700_4.EX_NAME2,' +
        'I700_4.EX_NAME3,I700_4.EX_ADDR1,I700_4.EX_ADDR2'
      
        '      ,I700_4.EX_ADDR3,I700_4.OP_BANK1,I700_4.OP_BANK2,I700_4.OP' +
        '_BANK3,I700_4.OP_ADDR1,I700_4.OP_ADDR2,I700_4.OP_ADDR3,I700_4.MI' +
        'X_PAY1,I700_4.MIX_PAY2,I700_4.MIX_PAY3,I700_4.MIX_PAY4'
      
        '      ,I700_4.APPLICABLE_RULES_1,I700_4.APPLICABLE_RULES_2,I700_' +
        '4.DOC_760,I700_4.DOC_760_1,I700_4.DOC_760_2,I700_4.DOC_760_3,I70' +
        '0_4.DOC_760_4,I700_4.SUNJUCK_PORT,I700_4.DOCHACK_PORT'
      #9'  ,Mathod700.DOC_NAME as mathod_Name'
      #9'  ,Pay700.DOC_NAME as pay_Name'
      #9'  ,IMPCD700_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD700_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD700_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD700_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD700_5.DOC_NAME as Imp_Name_5'
      #9'  ,DocCD700.DOC_NAME as DOC_Name'
      #9'  ,CDMAX700.DOC_NAME as CDMAX_Name'
      #9'  ,Pship700.DOC_NAME as pship_Name'
      #9'  ,Tship700.DOC_NAME as tship_Name'
      #9'  ,doc705_700.DOC_NAME as doc705_Name'
      #9'  ,doc740_700.DOC_NAME as doc740_Name'
      #9'  ,doc760_700.DOC_NAME as doc760_Name'
      #9'  ,CHARGE700.DOC_NAME as CHARGE_Name'
      #9'  ,CONFIRM700.DOC_NAME as CONFIRM_Name'
      
        ', CHARGE_1, CONFIRM_BICCD, CONFIRM_BANKNM, CONFIRM_BANKBR, PERIO' +
        'D_IDX, PERIOD_TXT, SPECIAL_PAY, AVAIL_BIC, DRAWEE_BIC'
      ''
      'FROM [dbo].[INF700_1] AS I700_1'
      
        'LEFT JOIN [dbo].[INF700_2] AS I700_2 ON I700_1.MAINT_NO = I700_2' +
        '.MAINT_NO AND I700_1.MSEQ = I700_2.MSEQ'
      
        'LEFT JOIN [dbo].[INF700_3] AS I700_3 ON I700_1.MAINT_NO = I700_3' +
        '.MAINT_NO AND I700_1.MSEQ = I700_3.MSEQ '
      
        'LEFT JOIN [dbo].[INF700_4] AS I700_4 ON I700_1.MAINT_NO = I700_4' +
        '.MAINT_NO AND I700_1.MSEQ = I700_4.MSEQ'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod700 ON I700_1.IN_MATHOD = Mathod' +
        '700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay700 ON I700_1.AD_PAY = Pay700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_1 ON I700_1.IMP_CD1 = IMPCD' +
        '700_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_2 ON I700_1.IMP_CD2 = IMPCD' +
        '700_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_3 ON I700_1.IMP_CD3 = IMPCD' +
        '700_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_4 ON I700_1.IMP_CD4 = IMPCD' +
        '700_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_5 ON I700_1.IMP_CD5 = IMPCD' +
        '700_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39') DocCD700 ON I700_1.DOC_CD = DocCD700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX700 ON I700_2.CD_MAX = CDMAX700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'PSHIP'#39') Pship700 ON I700_3.PSHIP = Pship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'TSHIP'#39') Tship700 ON I700_3.TSHIP = Tship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_705'#39') doc705_700 ON I700_3.DOC_705_3 = do' +
        'c705_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc740_700 ON I700_3.DOC_740_3 = do' +
        'c740_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc760_700 ON I700_4.DOC_760_3 = do' +
        'c760_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CHARGE'#39') CHARGE700 ON I700_3.CHARGE = CHARGE7' +
        '00.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CONFIRM'#39') CONFIRM700 ON I700_3.CONFIRMM = CON' +
        'FIRM700.CODE'
      '')
    Left = 152
    Top = 224
  end
end
