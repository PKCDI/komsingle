inherited UI_LOCAD1_NEW_frm: TUI_LOCAD1_NEW_frm
  Left = 627
  Top = 144
  Caption = '[LOCADV] '#45236#44397#49888#50857#51109' '#53685#51648#49436' - '#49688#54812#51088' '#46041#48372#51204#49569
  PixelsPerInch = 96
  TextHeight = 12
  inherited btn_Panel: TsPanel
    inherited sLabel7: TsLabel
      Left = 23
      Width = 256
      Caption = #45236#44397#49888#50857#51109' '#44060#49444#51025#45813#49436'('#46041#48372#51204#49569')'
    end
    inherited sLabel5: TsLabel
      Left = 113
    end
    inherited btnNew: TsButton
      Left = 227
      Top = 34
    end
    inherited btnEdit: TsButton
      Left = 315
    end
    inherited btnDel: TsButton
      Left = 321
    end
    inherited btnPrint: TsButton
      Left = 387
    end
    inherited sPanel6: TsPanel
      inherited com_func: TsComboBox
        Left = 648
        Width = 49
        Items.Strings = (
          '1:'
          '4:'
          '6:'
          '9:'
          '43:')
      end
    end
    inherited QRShape1: TQRShape
      Size.Values = (
        2.64583333333333
        809.625
        105.833333333333
        2129.89583333333)
    end
  end
  inherited sPanel3: TsPanel
    inherited sPageControl1: TsPageControl
      ActivePage = sTabSheet3
      TabIndex = 1
    end
  end
  inherited qryLOCADV: TADOQuery
    SQL.Strings = (
      
        'SELECT MAINT_NO, CHK1, CHK2, CHK3, USER_ID, DATEE, BGM_REF, MESS' +
        'AGE1, MESSAGE2, BUSINESS, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFF' +
        'ERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO' +
        '9, OPEN_NO, ADV_DATE, ISS_DATE, DOC_PRD, DELIVERY, EXPIRY, TRANS' +
        'PRT, GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_B' +
        'ANK2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNA' +
        'ME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, DOCCOPY4, D' +
        'OCCOPY5, DOC_ETC, DOC_ETC1, LOC_TYPE, LOC1AMT, LOC1AMTC, LOC2AMT' +
        ', LOC2AMTC, EX_RATE, DOC_DTL, DOC_NO, DOC_AMT, DOC_AMTC, LOADDAT' +
        'E, EXPDATE, IM_NAME, IM_NAME1, IM_NAME2, IM_NAME3, DEST, ISBANK1' +
        ', ISBANK2, PAYMENT, EXGOOD, EXGOOD1, PRNO, BSN_HSCODE, APPADDR1,' +
        ' APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADDR3, BNFEMAILID, B' +
        'NFDOMAIN, CD_PERP, CD_PERM'
      '      ,N4025.DOC_NAME as BUSINESSNAME'
      '      ,PSHIP.DOC_NAME as TRANSPRTNAME'
      '      ,N4487.DOC_NAME as LOC_TYPENAME'
      '      ,N1001.DOC_NAME as DOC_DTLNAME '
      '      ,N4277.DOC_NAME as PAYMENTNAME'
      '      ,NAT.DOC_NAME as DESTNAME'
      
        'FROM LOCAD1 LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2ND' +
        'D with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON LOCAD1.LOC_TYP' +
        'E = N4487.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON L' +
        'OCAD1.BUSINESS = N4025.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOC' +
        'AD1.TRANSPRT = PSHIP.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON L' +
        'OCAD1.PAYMENT = N4277.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON L' +
        'OCAD1.DOC_DTL = N1001.CODE'#9
      
        #9'         LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD ' +
        'with(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCAD1.DEST = NAT.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(LOCAD1.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
  end
end
