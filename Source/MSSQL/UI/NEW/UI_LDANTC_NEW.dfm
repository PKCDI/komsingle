inherited UI_LDANTC_NEW_frm: TUI_LDANTC_NEW_frm
  Left = 435
  Top = 72
  BorderWidth = 4
  Caption = '[LDANTC] '#45236#44397#49888#50857#51109' '#54032#47588#52628#49900#46020#52265' '#53685#48372#49436
  ClientHeight = 661
  ClientWidth = 942
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 942
    Height = 72
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      942
      72)
    object sLabel7: TsLabel
      Left = 26
      Top = 13
      Width = 193
      Height = 23
      Caption = #54032#47588#45824#44552#52628#49900#46020#52265' '#53685#48372#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 85
      Top = 36
      Width = 75
      Height = 21
      Caption = '(LDANTC)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 240
      Top = 4
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 868
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 253
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 319
      Top = 2
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 256
      Top = 41
      Width = 684
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 3
      object edt_MAINT_NO: TsEdit
        Left = 51
        Top = 3
        Width = 230
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 339
        Top = 3
        Width = 77
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 539
        Top = 3
        Width = 39
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 635
        Top = 3
        Width = 47
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 459
        Top = 3
        Width = 25
        Height = 23
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 315
    Height = 589
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 313
      Height = 531
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 189
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 90
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 313
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 230
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 57
        Top = 29
        Width = 171
        Height = 23
        TabOrder = 0
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 57
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 150
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 2
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 241
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 315
    Top = 72
    Width = 627
    Height = 589
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 1
      Width = 625
      Height = 587
      ActivePage = sTabSheet1
      Align = alClient
      TabHeight = 26
      TabOrder = 1
      TabPadding = 15
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 617
          Height = 551
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          DesignSize = (
            617
            551)
          object Shape1: TShape
            Left = 9
            Top = 43
            Width = 600
            Height = 1
            Anchors = [akLeft, akTop, akRight]
            Brush.Color = 14540252
            Pen.Color = 14540252
          end
          object Shape2: TShape
            Left = 9
            Top = 134
            Width = 600
            Height = 1
            Anchors = [akLeft, akTop, akRight]
            Brush.Color = 14540252
            Pen.Color = 14540252
          end
          object Shape3: TShape
            Left = 9
            Top = 457
            Width = 600
            Height = 1
            Anchors = [akLeft, akTop, akRight]
            Brush.Color = 14540252
            Pen.Color = 14540252
          end
          object sDBEdit1: TsDBEdit
            Left = 104
            Top = 9
            Width = 41
            Height = 23
            Color = clWhite
            DataField = 'BGM_GUBUN'
            DataSource = dsList
            Enabled = False
            TabOrder = 0
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#49888#47928#49436#51333#47448
          end
          object sDBEdit2: TsDBEdit
            Left = 146
            Top = 9
            Width = 263
            Height = 23
            Color = clBtnFace
            DataField = 'BGM_NM'
            DataSource = dsList
            Enabled = False
            TabOrder = 1
            SkinData.CustomColor = True
          end
          object sDBEdit3: TsDBEdit
            Left = 104
            Top = 53
            Width = 201
            Height = 23
            Color = 13434879
            DataField = 'BK_NAME1'
            DataSource = dsList
            Enabled = False
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #48156#49888#44592#44288
          end
          object sDBEdit4: TsDBEdit
            Left = 104
            Top = 77
            Width = 201
            Height = 23
            Color = clWhite
            DataField = 'BK_NAME2'
            DataSource = dsList
            Enabled = False
            TabOrder = 3
          end
          object sDBEdit5: TsDBEdit
            Left = 104
            Top = 101
            Width = 89
            Height = 23
            Color = clWhite
            DataField = 'BK_NAME3'
            DataSource = dsList
            Enabled = False
            TabOrder = 4
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#51088#49436#47749
          end
          object sDBEdit6: TsDBEdit
            Left = 368
            Top = 53
            Width = 234
            Height = 23
            Color = 13434879
            DataField = 'APP_NAME1'
            DataSource = dsList
            Enabled = False
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#49888#51064
          end
          object sDBEdit7: TsDBEdit
            Left = 368
            Top = 77
            Width = 234
            Height = 23
            Color = clWhite
            DataField = 'APP_NAME2'
            DataSource = dsList
            Enabled = False
            TabOrder = 6
          end
          object sDBEdit8: TsDBEdit
            Left = 368
            Top = 101
            Width = 234
            Height = 23
            Color = clWhite
            DataField = 'APP_NAME3'
            DataSource = dsList
            Enabled = False
            TabOrder = 7
            BoundLabel.Caption = #49688#49888#51064
          end
          object sDBEdit9: TsDBEdit
            Left = 104
            Top = 145
            Width = 265
            Height = 23
            Color = clWhite
            DataField = 'LC_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 8
            BoundLabel.Active = True
            BoundLabel.Caption = #45236#44397#49888#50857#51109#48264#54840
          end
          object sDBEdit10: TsDBEdit
            Left = 104
            Top = 169
            Width = 33
            Height = 23
            Color = 12775866
            DataField = 'AMT1C'
            DataSource = dsList
            Enabled = False
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #50808#54868#44552#50529
          end
          object sDBEdit11: TsDBEdit
            Left = 138
            Top = 169
            Width = 167
            Height = 23
            Color = 12582911
            DataField = 'AMT1'
            DataSource = dsList
            Enabled = False
            TabOrder = 10
            SkinData.CustomColor = True
          end
          object sDBEdit12: TsDBEdit
            Left = 104
            Top = 193
            Width = 105
            Height = 23
            Color = clWhite
            DataField = 'RATE'
            DataSource = dsList
            Enabled = False
            TabOrder = 11
            BoundLabel.Active = True
            BoundLabel.Caption = #51201#50857#54872#50984
          end
          object sDBEdit13: TsDBEdit
            Left = 104
            Top = 217
            Width = 105
            Height = 23
            Color = clWhite
            DataField = 'RES_DATE'
            DataSource = dsList
            Enabled = False
            TabOrder = 12
            BoundLabel.Active = True
            BoundLabel.Caption = #53685#51648#51068#51088
          end
          object sDBEdit14: TsDBEdit
            Left = 392
            Top = 169
            Width = 33
            Height = 23
            Color = 12775866
            DataField = 'AMT2C'
            DataSource = dsList
            Enabled = False
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #50896#54868#54872#49328#44552#50529
          end
          object sDBEdit15: TsDBEdit
            Left = 426
            Top = 169
            Width = 167
            Height = 23
            Color = 12582911
            DataField = 'AMT2'
            DataSource = dsList
            Enabled = False
            TabOrder = 14
            SkinData.CustomColor = True
          end
          object sDBEdit16: TsDBEdit
            Left = 392
            Top = 193
            Width = 105
            Height = 23
            Color = clWhite
            DataField = 'NG_DATE'
            DataSource = dsList
            Enabled = False
            TabOrder = 15
            BoundLabel.Active = True
            BoundLabel.Caption = 'NEGO'#51068#51088
          end
          object sDBEdit17: TsDBEdit
            Left = 392
            Top = 217
            Width = 105
            Height = 23
            Color = clWhite
            DataField = 'SET_DATE'
            DataSource = dsList
            Enabled = False
            TabOrder = 16
            BoundLabel.Active = True
            BoundLabel.Caption = #52572#51333#44208#51228#51068
          end
          object sPageControl2: TsPageControl
            Left = 16
            Top = 256
            Width = 585
            Height = 193
            ActivePage = sTabSheet2
            Images = DMICON.System18
            TabHeight = 28
            TabOrder = 17
            TabPadding = 10
            object sTabSheet2: TsTabSheet
              Caption = #47932#54408#49688#47161#51613#47749#49436'('#51064#49688#51613')'#48264#54840
              ImageIndex = -1
              object lst_RCNO: TsListBox
                Left = 0
                Top = 0
                Width = 577
                Height = 155
                Align = alClient
                Enabled = False
                ItemHeight = 18
                TabOrder = 0
              end
            end
            object sTabSheet3: TsTabSheet
              Caption = #49464#44552#44228#49328#49436#48264#54840
              ImageIndex = -1
              object lst_FINNO: TsListBox
                Left = 0
                Top = 0
                Width = 577
                Height = 155
                Align = alClient
                Enabled = False
                ItemHeight = 18
                TabOrder = 0
              end
            end
            object sTabSheet5: TsTabSheet
              Caption = #44592#53440#49324#54637
              ImageIndex = -1
              object sDBMemo1: TsDBMemo
                Left = 0
                Top = 0
                Width = 577
                Height = 155
                Align = alClient
                Color = clWhite
                DataField = 'REMAKR1'
                DataSource = dsList
                Enabled = False
                ReadOnly = True
                TabOrder = 0
              end
            end
          end
          object sDBEdit18: TsDBEdit
            Left = 104
            Top = 468
            Width = 193
            Height = 23
            Color = clWhite
            DataField = 'BANK1'
            DataSource = dsList
            Enabled = False
            TabOrder = 18
            BoundLabel.Active = True
            BoundLabel.Caption = #53685#51648#51008#54665#47749
          end
          object sDBEdit19: TsDBEdit
            Left = 104
            Top = 492
            Width = 193
            Height = 23
            Color = clWhite
            DataField = 'BANK2'
            DataSource = dsList
            Enabled = False
            TabOrder = 19
            BoundLabel.Active = True
            BoundLabel.Caption = #51648#51216#47749
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        TabVisible = False
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 617
          Height = 519
          Align = alClient
          Color = clGray
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = 12582911
              Expanded = False
              FieldName = 'MAINT_NO'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = [fsBold]
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_SQ1'
              Title.Alignment = taCenter
              Title.Caption = #50976#54952#44592#44036
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_DATE'
              Title.Alignment = taCenter
              Title.Caption = #48156#54665#51068#51088
              Width = 82
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'OFR_NO'
              Title.Alignment = taCenter
              Title.Caption = #48156#54665#48264#54840
              Width = 150
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'UD_NAME1'
              Title.Alignment = taCenter
              Title.Caption = #49688#50836#51088
              Width = 150
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'TQTY'
              Title.Alignment = taCenter
              Title.Caption = #52509#49688#47049
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'TQTYCUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 50
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'TAMT'
              Title.Alignment = taCenter
              Title.Caption = #52509#44552#50529
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'TAMTCUR'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 50
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'USER_ID'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51088
              Width = 50
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 617
          Height = 32
          Align = alTop
          
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 0
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 1
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
          end
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 144
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20181130'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, APP_CODE, A' +
        'PP_NAME1, APP_NAME2, APP_NAME3, BK_CODE, BK_NAME1, BK_NAME2, BK_' +
        'NAME3,'
      
        '       LC_NO, RC_NO, AMT1, AMT1C, AMT2, AMT2C, RATE, RES_DATE, S' +
        'ET_DATE, NG_DATE, REMAKR1, BANK1, BANK2, CHK1, CHK2, CHK3, PRNO,' +
        ' BGM_GUBUN, NAME as BGM_NM,'
      
        '       FIN_NO, FIN_NO2, FIN_NO3, FIN_NO4, FIN_NO5, FIN_NO6, FIN_' +
        'NO7, RC_NO2, RC_NO3, RC_NO4, RC_NO5, RC_NO6, RC_NO7, RC_NO8 '
      'FROM LDANTC'
      
        'LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'LDANT' +
        'C_CD'#39') CD ON BGM_GUBUN = CD.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 48
    Top = 192
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryListAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryListAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryListBK_CODE: TStringField
      FieldName = 'BK_CODE'
      Size = 10
    end
    object qryListBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 35
    end
    object qryListBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 35
    end
    object qryListBK_NAME3: TStringField
      FieldName = 'BK_NAME3'
      Size = 35
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListRC_NO: TStringField
      FieldName = 'RC_NO'
      Size = 35
    end
    object qryListAMT1: TBCDField
      FieldName = 'AMT1'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListAMT1C: TStringField
      FieldName = 'AMT1C'
      Size = 3
    end
    object qryListAMT2: TBCDField
      FieldName = 'AMT2'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListAMT2C: TStringField
      FieldName = 'AMT2C'
      Size = 3
    end
    object qryListRATE: TBCDField
      FieldName = 'RATE'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListRES_DATE: TStringField
      FieldName = 'RES_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListSET_DATE: TStringField
      FieldName = 'SET_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListNG_DATE: TStringField
      FieldName = 'NG_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListREMAKR1: TMemoField
      FieldName = 'REMAKR1'
      BlobType = ftMemo
    end
    object qryListBANK1: TStringField
      FieldName = 'BANK1'
      Size = 70
    end
    object qryListBANK2: TStringField
      FieldName = 'BANK2'
      Size = 70
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListBGM_GUBUN: TStringField
      FieldName = 'BGM_GUBUN'
      Size = 3
    end
    object qryListBGM_NM: TStringField
      FieldName = 'BGM_NM'
      Size = 100
    end
    object qryListFIN_NO: TStringField
      FieldName = 'FIN_NO'
      Size = 35
    end
    object qryListFIN_NO2: TStringField
      FieldName = 'FIN_NO2'
      Size = 35
    end
    object qryListFIN_NO3: TStringField
      FieldName = 'FIN_NO3'
      Size = 35
    end
    object qryListFIN_NO4: TStringField
      FieldName = 'FIN_NO4'
      Size = 35
    end
    object qryListFIN_NO5: TStringField
      FieldName = 'FIN_NO5'
      Size = 35
    end
    object qryListFIN_NO6: TStringField
      FieldName = 'FIN_NO6'
      Size = 35
    end
    object qryListFIN_NO7: TStringField
      FieldName = 'FIN_NO7'
      Size = 35
    end
    object qryListRC_NO2: TStringField
      FieldName = 'RC_NO2'
      Size = 35
    end
    object qryListRC_NO3: TStringField
      FieldName = 'RC_NO3'
      Size = 35
    end
    object qryListRC_NO4: TStringField
      FieldName = 'RC_NO4'
      Size = 35
    end
    object qryListRC_NO5: TStringField
      FieldName = 'RC_NO5'
      Size = 35
    end
    object qryListRC_NO6: TStringField
      FieldName = 'RC_NO6'
      Size = 35
    end
    object qryListRC_NO7: TStringField
      FieldName = 'RC_NO7'
      Size = 35
    end
    object qryListRC_NO8: TStringField
      FieldName = 'RC_NO8'
      Size = 35
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 192
  end
end
