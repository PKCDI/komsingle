unit UI_ATTNTC_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, StdCtrls, sComboBox, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sButton, sSpeedButton, sLabel, ExtCtrls, sPanel, sSkinProvider,
  sMemo, DateUtils, StrUtils;

type
  TUI_ATTNTC_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    edt_DOC_NO: TsEdit;
    edt_ATT_DOCNO: TsEdit;
    edt_DOC_CD: TsEdit;
    edt_DOC_CDNM: TsEdit;
    Shape1: TShape;
    edt_EX_NAME1: TsEdit;
    edt_EX_NAME2: TsEdit;
    edt_EX_NAME3: TsEdit;
    edt_EX_ELEC: TsEdit;
    edt_SR_NAME1: TsEdit;
    edt_SR_NAME2: TsEdit;
    edt_SR_NAME3: TsEdit;
    Shape2: TShape;
    memo_FTX_DOC1: TsMemo;
    sDBGrid2: TsDBGrid;
    sPanel8: TsPanel;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListATT_DOCNO: TStringField;
    qryListDOC_CD: TStringField;
    qryListDOC_CDNAME: TStringField;
    qryListDOC_NO: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListRECV_CUSTCD: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ELEC: TStringField;
    qryListSR_NAME1: TStringField;
    qryListSR_NAME2: TStringField;
    qryListSR_NAME3: TStringField;
    qryListSR_ELEC: TStringField;
    qryListFTX_DOC1: TMemoField;
    qryListFTX_DOC2: TMemoField;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    sMemo1: TsMemo;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailATT_CK: TStringField;
    qryDetailATT_NO: TStringField;
    qryDetailATT_CD: TStringField;
    qryDetailATT_CDNM: TStringField;
    qryDetailATT_ELEC: TStringField;
    qryDetailMDOC1: TStringField;
    qryDetailMDOC2: TStringField;
    qryDetailMDOC3: TStringField;
    qryDetailMDOC4: TStringField;
    qryDetailMDOC5: TStringField;
    qryDetailDDOC1: TStringField;
    qryDetailDDOC2: TStringField;
    qryDetailDDOC3: TStringField;
    qryDetailDDOC4: TStringField;
    qryDetailDDOC5: TStringField;
    sPanel6: TsPanel;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryDetailAfterOpen(DataSet: TDataSet);
    procedure qryDetailAfterScroll(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
  private
    procedure ReadDetail;
  protected
    procedure ReadList;
    procedure ReadData; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_ATTNTC_NEW_frm: TUI_ATTNTC_NEW_frm;

implementation

uses
  MessageDefine, MSSQL, ICON;

{$R *.dfm}

{ TUI_ATTNTC_NEW_frm }

procedure TUI_ATTNTC_NEW_frm.ReadData;
begin
  inherited;
  with qryList do
  begin
    ClearControl(sPanel6);
    if RecordCount > 0 then
    begin

      //------------------------------------------------------------------------------
      // 데이터 가져오기
      //------------------------------------------------------------------------------
      edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
      msk_Datee.Text := qryListDATEE.AsString;
      edt_userno.Text := qryListUSER_ID.AsString;

      CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 3);
      CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

      edt_DOC_CD.Text := qryListDOC_CD.AsString;
      edt_DOC_CDNM.Text := qryListDOC_CDNAME.AsString;
      edt_DOC_NO.Text   := qryListDOC_NO.AsString;
      edt_ATT_DOCNO.Text := qryListATT_DOCNO.AsString;

      edt_EX_NAME1.Text := qryListEX_NAME1.AsString;
      edt_EX_NAME2.Text := qryListEX_NAME2.AsString;
      edt_EX_NAME3.Text := qryListEX_NAME3.AsString;
      edt_EX_ELEC.Text  := qryListEX_ELEC.AsString;

      edt_SR_NAME1.Text := qryListSR_NAME1.AsString;
      edt_SR_NAME2.Text := qryListSR_NAME2.AsString;
      edt_SR_NAME3.Text := qryListSR_NAME3.AsString;

      memo_FTX_DOC1.Lines.Text := qryListFTX_DOC1.AsString;
    end;
  end;
end;

procedure TUI_ATTNTC_NEW_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    Open;
  end;
end;

procedure TUI_ATTNTC_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_ATTNTC_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  sBitBtn1Click(nil);

  ReadOnlyControl(sPanel7,false);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_ATTNTC_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_ATTNTC_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_ATTNTC_NEW_frm := nil;
end;

procedure TUI_ATTNTC_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
  ReadDetail;
end;

procedure TUI_ATTNTC_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then
    qryListAfterScroll(DataSet);
end;

procedure TUI_ATTNTC_NEW_frm.ReadDetail;
begin
  with qryDetail do
  begin
    Close;
    Parameters[0].value := qryListMAINT_NO.AsString;
    Open;
  end;
end;

procedure TUI_ATTNTC_NEW_frm.qryDetailAfterOpen(DataSet: TDataSet);
begin
  inherited;
  sPanel8.Caption := '기타정보('+IntToStr(DataSet.RecordCount)+'건)';
  if DataSet.RecordCount = 0 Then qryDetailAfterScroll(DataSet);
end;

procedure TUI_ATTNTC_NEW_frm.qryDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  sMemo1.Clear;
  if Trim(qryDetailMDOC1.AsString) <> '' Then
    sMemo1.Lines.Add(Trim(qryDetailMDOC1.AsString));
  if Trim(qryDetailMDOC2.AsString) <> '' Then
    sMemo1.Lines.Add(Trim(qryDetailMDOC2.AsString));
  if Trim(qryDetailMDOC3.AsString) <> '' Then
    sMemo1.Lines.Add(Trim(qryDetailMDOC3.AsString));
  if Trim(qryDetailMDOC4.AsString) <> '' Then
    sMemo1.Lines.Add(Trim(qryDetailMDOC4.AsString));
  if Trim(qryDetailMDOC5.AsString) <> '' Then
    sMemo1.Lines.Add(Trim(qryDetailMDOC5.AsString));
end;

procedure TUI_ATTNTC_NEW_frm.btnDelClick(Sender: TObject);
var
  nIDX :Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  if not qrylist.Active then Exit;

  IF MessageBox(Self.Handle, PChar(MSG_SYSTEM_DEL_CONFIRM+MSG_SYSTEM_LINE+'근거서류첨부 응답서'#13#10+qryListMAINT_NO.AsString),'데이터 삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  DMMssql.BeginTrans;
  try
    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        Close;
        SQL.Text := 'DELETE FROM ATTNTC_D WHERE KEYY = '+QuotedStr(qryListMAINT_NO.AsString);
        ExecSQL;

        Close;
        SQL.Text := 'DELETE FROM ATTNTC_H WHERE MAINT_NO = '+QuotedStr(qryListMAINT_NO.AsString);
        ExecSQL;

        DMMssql.CommitTrans;
      finally
        Close;
        Free;
      end;
    end;
    nIDX := qryList.RecNo;
    qryList.Close;
    qryList.Open;

    IF qryList.RecordCount > 1 Then
      qryList.MoveBy(nIDX-1);
  except
    on E:Exception do
    begin
      DMMssql.RollbackTrans;
      MessageBox(Self.Handle, PChar(MSG_SYSTEM_DEL_ERR+#13#10+E.Message), '삭제오류', MB_OK+MB_ICONERROR);
    end;
  end;
end;

end.
