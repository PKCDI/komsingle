unit UI_APPRMI_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sCheckBox, sMemo,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl,
  Buttons, sBitBtn, Grids, DBGrids, acDBGrid, sComboBox, Mask, sMaskEdit,
  sEdit, sButton, sLabel, sSpeedButton, ExtCtrls, acImage, sPanel, DateUtils,
  acAlphaHints, BankPassword, TypeDefine, CodeDialogParent, DB, ADODB, SQLCreator,
  StrUtils;

type
  TUI_APPRMI_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sImage2: TsImage;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    sButton13: TsButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    btnCopy: TsButton;
    sPanel6: TsPanel;
    edtMAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    edt_userno: TsEdit;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    msk_APP_DATE: TsMaskEdit;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    msk_TRN_HOPE_DT: TsMaskEdit;
    msk_IMPT_SCHEDULE_DT: TsMaskEdit;
    edt_ETC_NO: TsEdit;
    edt_TRD_CD: TsEdit;
    sEdit3: TsEdit;
    edt_IMPT_REMIT_CD: TsEdit;
    sEdit5: TsEdit;
    edt_IMPT_TRAN_FEE_CD: TsEdit;
    sEdit6: TsEdit;
    sLabel12: TsLabel;
    edt_REMIT_DESC1: TsEdit;
    edt_REMIT_DESC2: TsEdit;
    edt_REMIT_DESC3: TsEdit;
    edt_REMIT_DESC4: TsEdit;
    edt_REMIT_CD: TsEdit;
    sEdit9: TsEdit;
    edt_ADDED_FEE_TAR: TsEdit;
    sEdit11: TsEdit;
    edt_TOTAL_AMT_UNIT: TsEdit;
    curr_TOTAL_AMT: TsCurrencyEdit;
    edt_FORE_AMT_UNIT: TsEdit;
    curr_FORE_AMT: TsCurrencyEdit;
    sImage3: TsImage;
    sAlphaHints1: TsAlphaHints;
    edtOD_BANK: TsEdit;
    edtOD_BANK1: TsEdit;
    edtOD_BANK2: TsEdit;
    edtOD_NAME1: TsEdit;
    edtOD_NAME2: TsEdit;
    edtOD_Nation: TsEdit;
    edtOD_CURR: TsEdit;
    edtOD_ACCNT2: TsEdit;
    edtOD_ACCNT1: TsEdit;
    sPanel15: TsPanel;
    edtEC_BANK: TsEdit;
    edtEC_BANK1: TsEdit;
    edtEC_BANK2: TsEdit;
    edtEC_NAME1: TsEdit;
    edtEC_NAME2: TsEdit;
    edtEC_Nation: TsEdit;
    edtEC_ACCNT2: TsEdit;
    edtEC_ACCNT1: TsEdit;
    sPanel14: TsPanel;
    sPanel16: TsPanel;
    sPanel17: TsPanel;
    sPanel13: TsPanel;
    edtIT_BANK: TsEdit;
    edtIT_BANK1: TsEdit;
    edtIT_BANK2: TsEdit;
    edtIT_NAME1: TsEdit;
    edtIT_NAME2: TsEdit;
    edtIT_Nation: TsEdit;
    edtIT_CURR: TsEdit;
    edtIT_ACCNT2: TsEdit;
    edtIT_ACCNT1: TsEdit;
    Edt_ApplicationName1: TsEdit;
    Edt_ApplicationSAUPNO: TsEdit;
    Edt_ApplicationCode: TsEdit;
    Edt_ApplicationAddr1: TsEdit;
    Edt_ApplicationAddr2: TsEdit;
    edtBN_BANK: TsEdit;
    edtBN_BANK1: TsEdit;
    edtBN_BANK2: TsEdit;
    edtBN_NAME1: TsEdit;
    edtBN_NAME2: TsEdit;
    edtBN_Nation: TsEdit;
    edtBN_CURR: TsEdit;
    edtBN_ACCNT2: TsEdit;
    edtBN_ACCNT1: TsEdit;
    edtBEN_CODE: TsEdit;
    edtBEN_NAME1: TsEdit;
    edtBEN_NAME2: TsEdit;
    edtBEN_NAME3: TsEdit;
    edtBEN_STR1: TsEdit;
    edtBEN_STR2: TsEdit;
    edtBEN_TELE: TsEdit;
    edtBEN_STR3: TsEdit;
    edtBEN_NATION: TsEdit;
    sTabSheet2: TsTabSheet;
    panDoc: TsPanel;
    edt_ONLY_PWD: TsEdit;
    Edt_ApplicationElectronicSign: TsEdit;
    sSpeedButton1: TsSpeedButton;
    edtDoc_RFF_NO: TsEdit;
    currDoc_AMT: TsCurrencyEdit;
    currDoc_CIF_AMT: TsCurrencyEdit;
    mskDoc_HS_CODE: TsMaskEdit;
    edtDoc_GOODS_NM: TsEdit;
    edtDoc_IMPT_CD: TsEdit;
    sEdit8: TsEdit;
    mskDoc_CONT_DT: TsMaskEdit;
    edtDoc_TOD_CD: TsEdit;
    sEdit12: TsEdit;
    edtDoc_TOD_REMARK: TsEdit;
    edtDoc_EXPORT_NAT_CD: TsEdit;
    sMemo1: TsMemo;
    edtDoc_EXPORT_OW_NM: TsEdit;
    edtDoc_IMPORT_OW_NM: TsEdit;
    edtDoc_EXPORT_OW_ADDR: TsEdit;
    edtDoc_IMPORT_OW_ADDR: TsEdit;
    edtDoc_EXPORT_OW_NAT_CD: TsEdit;
    edtDoc_IMPORT_OW_NAT_CD: TsEdit;
    sDBGrid2: TsDBGrid;
    btnDocNew: TsButton;
    btnDocEdit: TsButton;
    btnDocDel: TsButton;
    btnDocExcel: TsButton;
    sSpeedButton2: TsSpeedButton;
    qryRFF: TADOQuery;
    dsRFF: TDataSource;
    edtDoc_AMT_UNIT: TsEdit;
    edtDoc_REMIT_AMT: TsEdit;
    currDoc_REMIT_AMT_UNIT: TsCurrencyEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    btnDocSave: TsButton;
    btnDocCancel: TsButton;
    edtDoc_CIF_AMT_UNIT: TsEdit;
    qryAPPRMI_H: TADOQuery;
    dsAPPRMI_H: TDataSource;
    qryAPPRMI_HMAINT_NO: TStringField;
    qryAPPRMI_HAPP_DT: TStringField;
    qryAPPRMI_HREG_DT: TDateTimeField;
    qryAPPRMI_HUSER_ID: TStringField;
    qryAPPRMI_HTRN_HOPE_DT: TStringField;
    qryAPPRMI_HIMPT_SCHEDULE_DT: TStringField;
    qryAPPRMI_HETC_NO: TStringField;
    qryAPPRMI_HTRD_CD: TStringField;
    qryAPPRMI_HIMPT_REMIT_CD: TStringField;
    qryAPPRMI_HIMPT_TRAN_FEE_CD: TStringField;
    qryAPPRMI_HREMIT_DESC: TStringField;
    qryAPPRMI_HREMIT_CD: TStringField;
    qryAPPRMI_HADDED_FEE_TAR: TStringField;
    qryAPPRMI_HTOTAL_AMT: TBCDField;
    qryAPPRMI_HTOTAL_AMT_UNIT: TStringField;
    qryAPPRMI_HFORE_AMT: TBCDField;
    qryAPPRMI_HFORE_AMT_UNIT: TStringField;
    qryAPPRMI_HONLY_PWD: TStringField;
    qryAPPRMI_HAUTH_NM1: TStringField;
    qryAPPRMI_HAUTH_NM2: TStringField;
    qryAPPRMI_HAUTH_NM3: TStringField;
    qryAPPRMI_HMESSAGE1: TStringField;
    qryAPPRMI_HMESSAGE2: TStringField;
    qryAPPRMI_HCHK2: TIntegerField;
    qryAPPRMI_HTRD_NM: TStringField;
    qryAPPRMI_HIMPT_REMIT_NM: TStringField;
    qryAPPRMI_HIMPT_TRAN_FEE_NM: TStringField;
    qryAPPRMI_HREMIT_NM: TStringField;
    qryAPPRMI_HADDED_FEE_TAR_NM: TStringField;
    procedure FormShow(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnNewClick(Sender: TObject);
    procedure edt_ADDED_FEE_TARDblClick(Sender: TObject);
    procedure edt_TRD_CDKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtBEN_CODEDblClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure edtBN_BANKDblClick(Sender: TObject);
    procedure edtOD_BANKDblClick(Sender: TObject);
    procedure btnDocNewClick(Sender: TObject);
    procedure btnDocSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryAPPRMI_HAfterScroll(DataSet: TDataSet);
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    CDDialog : TCodeDialogParent_frm;
    tempKey : String;
    FSQL : String;
    procedure createTempKey;
    procedure BtnControl;
    procedure ReadDocList(DocNo : String);
    procedure newDocument(CopyDocNo : String='');

    procedure DocBtnControl;
    procedure insertRFF;

    procedure ReadAPPRMI_H;
    procedure ReadAPPRMI_H_DATA;
    procedure ReadAPPRMI_NAD(MAINT_NO : String);

    procedure SaveData(Sender: TObject);
    procedure SaveDataNAD;

    procedure SaveTempData;
    procedure DeleteTempData;
  public
    { Public declarations }
  end;

var
  UI_APPRMI_NEW_frm: TUI_APPRMI_NEW_frm;

implementation

uses
  MSSQL, VarDefine, Commonlib, CodeContents, CD_ChargeTo, CD_PAYORDBFCD, CD_4487, CD_4383, CD_SNDCD, CD_AMT_UNIT, CD_NATION, CD_CUST, CD_BANK_SWIFT, CD_BANK, AutoNo;

{$R *.dfm}

procedure TUI_APPRMI_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYY-MM-DD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYY-MM-DD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

//  sBitBtn1Click(nil);
//
//  EnableControl(sPanel6, false);
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel27);
//  EnableControl(sPanel34, False);
//  EnableControl(sPanel35, False);
//  EnableControl(sPanel71, False);

  sPageControl1.ActivePageIndex := 0;

  ReadAPPRMI_H;  

end;

procedure TUI_APPRMI_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_APPRMI_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_APPRMI_NEW_frm := nil;
end;

procedure TUI_APPRMI_NEW_frm.btnNewClick(Sender: TObject);
begin
  createTempKey;
  newDocument();
end;

procedure TUI_APPRMI_NEW_frm.edt_ADDED_FEE_TARDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  Case (Sender as TsEdit).Tag of
    //통화단위
    98 : CDDialog := TCD_AMT_UNIT_frm.Create(Self);
    // 거래형태구분
    100 : CDDialog := TCD_PAYORDBFCD_frm.Create(Self);
    // 지급지시서용도
    101 : CDDialog := TCD_4487_frm.Create(Self);
    // 납부수수료유형
    102 : CDDialog := TCD_4383_frm.Create(Self);
    // 송금방법
    103 : CDDialog := TCD_SNDCD_frm.Create(Self);
    // 수수료부담자
    104 : CDDialog := TCD_ChargeTo_frm.Create(Self);
    // 국가
    105 : CDDialog := TCD_NATION_frm.Create(Self);
  end;


  try
    if CDDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CDDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        100 : sEdit3.Text  := CDDialog.Values[1];
        101 :
        begin
          sEdit5.Text  := CDDialog.Values[1];
          edt_IMPT_TRAN_FEE_CD.clear;
          sEdit6.clear;
          edt_IMPT_TRAN_FEE_CD.Enabled := (Sender as TsEdit).Text = '2AJ';
          sEdit6.Enabled := edt_IMPT_TRAN_FEE_CD.Enabled;
        end;
        102 : sEdit6.Text := CDDialog.Values[1];
        103 : sEdit9.Text := CDDialog.Values[1];
        104 : sEdit11.Text := CDDialog.Values[1];
      end;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

procedure TUI_APPRMI_NEW_frm.edt_TRD_CDKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TMP_STR : string;
begin
  inherited;
  Case (Sender as TsEdit).Tag of
    // 거래형태구분
    100 : CDDialog := TCD_PAYORDBFCD_frm.Create(Self);
    // 지급지시서용도
    101 : CDDialog := TCD_4487_frm.Create(Self);
    // 납부수수료유형
    102 : CDDialog := TCD_4383_frm.Create(Self);
    // 송금방법
    103 : CDDialog := TCD_SNDCD_frm.Create(Self);
    // 수수료부담자
    104 : CDDialog := TCD_ChargeTo_frm.Create(Self);
  end;

  try
    TMP_STR := CDDialog.GetCodeText((Sender as TsEdit).Text);
    Case (Sender as TsEdit).Tag of
      100 : sEdit3.Text := TMP_STR;
      101 :
      begin
        sEdit5.Text  := TMP_STR;
        edt_IMPT_TRAN_FEE_CD.clear;
        sEdit6.clear;
        edt_IMPT_TRAN_FEE_CD.Enabled := (Sender as TsEdit).Text = '2AJ';
        sEdit6.Enabled := edt_IMPT_TRAN_FEE_CD.Enabled;
      end;
      102 : sEdit6.Text := TMP_STR;
      103 : sEdit9.Text := TMP_STR;
      104 : sEdit11.Text := TMP_STR;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

procedure TUI_APPRMI_NEW_frm.edtBEN_CODEDblClick(Sender: TObject);
begin
  IF (Sender as TsEdit).ReadOnly Then Exit;

  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_CUST_frm.Values[0];

      case (Sender as TsEdit).Tag of
        //지급의뢰인
//        101:
//        begin
//          edtAPP_NAME1.Text := CD_CUST_frm.FieldValues('ENAME');
//          edtAPP_NAME2.Text := CD_CUST_frm.FieldValues('REP_NAME');
//          edtAPP_NAME3.Text := CD_CUST_frm.FieldValues('SAUP_NO');
//          edtAPP_STR1.Text := CD_CUST_frm.FieldValues('ADDR1');
//          edtAPP_TELE.Text := CD_CUST_frm.FieldValues('REP_TEL');
//        end;
        //수익자
        102:
        begin
          edtBEN_NAME1.Text := CD_CUST_frm.FieldValues('ENAME');
          edtBEN_STR1.Text  := CD_CUST_frm.FieldValues('ADDR1');
          edtBEN_STR2.Text  := CD_CUST_frm.FieldValues('ADDR2');
          edtBEN_STR3.Text  := CD_CUST_frm.FieldValues('ADDR3');
          edtBEN_TELE.Text  := CD_CUST_frm.FieldValues('REP_TEL');
          edtBEN_NATION.Text := CD_CUST_frm.FieldValues('NAT');
        end;
      end;

    end;

  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_APPRMI_NEW_frm.BtnControl;
begin
  btnNew.Enabled := ProgramControlType = ctView;
  btnEdit.Enabled := btnNew.Enabled;
  btnDel.Enabled := btnEdit.Enabled;
  btnCopy.Enabled := btnEdit.Enabled;

  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := btnTemp.Enabled;
  btnCancel.Enabled := btnTemp.Enabled;

  btnPrint.Enabled := btnEdit.Enabled;
  btnSend.Enabled  := btnEdit.Enabled;
  btnReady.Enabled  := btnEdit.Enabled;

  // 서브 버튼
  if ProgramControlType = ctView then
  begin
    btnDocNew.Enabled := false;
    btnDocEdit.Enabled := btnDocNew.Enabled;
    btnDocDel.Enabled := btnDocNew.Enabled;
    btnDocCancel.Enabled := btnDocNew.Enabled;
    btnDocSave.Enabled := btnDocCancel.Enabled;
  end
  else
  begin
    btnDocNew.Enabled := true;
    btnDocEdit.Enabled := btnDocNew.Enabled;
    btnDocDel.Enabled := btnDocNew.Enabled;
    btnDocCancel.Enabled := not btnDocNew.Enabled;
    btnDocSave.Enabled := btnDocCancel.Enabled;
  end;
end;

procedure TUI_APPRMI_NEW_frm.btnCancelClick(Sender: TObject);
begin
  inherited;
  if (Sender as TsButton).Name = 'btnSave' then
  begin
    DMMssql.BeginTrans;
    try
      SaveData(Sender);
      SaveDataNAD;
      SaveTempData;
      DMMssql.CommitTrans;
    except
      on E:Exception do
      begin
        DMMssql.RollbackTrans;
      end;
    end;

  end
  else
  if (Sender as TsButton).Name = 'btnCancel' then
  begin
    DeleteTempData;
  end;

  ProgramControlType := ctView;
  ReadOnlyControl(sPanel7, false);
  ReadOnlyControl(sPanel27, false);
  BtnControl;

end;

procedure TUI_APPRMI_NEW_frm.edtBN_BANKDblClick(Sender: TObject);
var
  CD_BANK_SWIFT : TCD_BANK_SWIFT_frm;
begin
  inherited;

  IF (Sender as TsEdit).ReadOnly Then Exit;

  CD_BANK_SWIFT := TCD_BANK_SWIFT_frm.Create(Self);
  try
    if CD_BANK_SWIFT.OpenDialog(0,(Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_BANK_SWIFT.Values[0];
       case (Sender as TsEdit).Tag of
          //수익자 은행
          102:
          begin
            edtBN_Nation.Text := CD_BANK_SWIFT.Field('BANK_NAT_CD').AsString;
            edtBN_BANK1.Text := CD_BANK_SWIFT.Field('BANK_NM1').AsString;
            edtBN_BANK2.Text := CD_BANK_SWIFT.Field('BANK_NM2').AsString;
            edtBN_NAME1.Text := CD_BANK_SWIFT.Field('BANK_BNCH_NM1').AsString;
            edtBN_NAME2.Text := CD_BANK_SWIFT.Field('BANK_BNCH_NM2').AsString;
            edtBN_ACCNT2.Text := CD_BANK_SWIFT.Field('ACCT_NM').AsString;
            edtBN_CURR.Text := CD_BANK_SWIFT.Field('BANK_UNIT').AsString;
          end;
       end;
    end;
  finally
    FreeAndNil( CD_BANK_SWIFT );
  end;

end;

procedure TUI_APPRMI_NEW_frm.edtOD_BANKDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  CDDialog := TCD_BANK_frm.Create(Self);
  try
    if CDDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CDDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        // 지급의뢰인은행 외화원금계좌
        101:
        begin
          edtOD_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtOD_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
        // 중간경유 은행
        103:
        begin
          edtIT_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtIT_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
        // 지급의뢰인 원화원금계좌
        104:
        begin
          edtEC_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtEC_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
      end;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

procedure TUI_APPRMI_NEW_frm.btnDocNewClick(Sender: TObject);
begin
  inherited;
  btnDocNew.Tag := 1;
  DocBtnControl;
  ClearControl(panDoc);
end;

procedure TUI_APPRMI_NEW_frm.newDocument(CopyDocNo: String);
begin
  //문서모드
  ProgramControlType := ctInsert;
  BtnControl;
  ReadOnlyControl(sPanel5);

  // 입력잠금 해제
  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel27, False);

  //패널 초기화
  ClearControl(sPanel7);

  //날짜 초기화
  msk_APP_DATE.Text := FormatDateTime('YYYYMMDD', Now);
  msk_TRN_HOPE_DT.Text := FormatDateTime('YYYYMMDD', Now);

  msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
  edtMAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPRMI');
  edt_userno.Text := LoginData.sID;

  edt_TOTAL_AMT_UNIT.Text := 'KRW';

  try
    //거래형태 구분 초기화
    edt_TRD_CD.Text := 'P11';
    CDDialog := TCD_PAYORDBFCD_frm.Create(Self);
    sEdit3.Text := CDDialog.GetCodeText(edt_TRD_CD.Text);

    //수입송금신청용도 초기화
    edt_IMPT_REMIT_CD.Text := '2AM';
    CDDialog := TCD_4487_frm.Create(Self);
    sEdit5.Text := CDDialog.GetCodeText(edt_IMPT_REMIT_CD.Text);

    //송금방법
    edt_REMIT_CD.Text := '4';
    CDDialog := TCD_SNDCD_frm.Create(Self);
    sEdit9.Text := CDDialog.GetCodeText(edt_REMIT_CD.Text);

    //부가수수료 부담자
    edt_ADDED_FEE_TAR.Text := '14';
    CDDialog := TCD_ChargeTo_frm.Create(Self);
    sEdit11.Text := CDDialog.GetCodeText(edt_ADDED_FEE_TAR.Text);

  finally
    FreeAndNil(CDDialog);
  end;


  // 신청인
  DMCodeContents.CODEREFRESH;
  IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
  begin
    Edt_ApplicationCode.Text := DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
    Edt_ApplicationName1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    Edt_ApplicationSAUPNO.Text := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
    Edt_ApplicationAddr1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
    Edt_ApplicationAddr2.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
    Edt_ApplicationElectronicSign.Text := DMCodeContents.GEOLAECHEO.FieldByName('Jenja').AsString;
  end;
end;

procedure TUI_APPRMI_NEW_frm.createTempKey;
begin
  tempKey := AnsiReplaceText( getStringFromQuery('SELECT NEWID()',0) , '{', '' );
  tempKey := AnsiReplaceText( tempKey , '}', '');
  tempKey := AnsiReplaceText( tempKey , '-', '');  
end;

procedure TUI_APPRMI_NEW_frm.DocBtnControl;
begin
  btnDocNew.Enabled := btnDocNew.Tag = 0;
  btnDocEdit.Enabled := btnDocNew.Enabled;
  btnDocDel.Enabled := btnDocNew.Enabled;
  btnDocSave.Enabled := btnDocNew.Tag = 1;
  btnDocCancel.Enabled := btnDocSave.Enabled;
end;

procedure TUI_APPRMI_NEW_frm.btnDocSaveClick(Sender: TObject);
begin
  inherited;
  btnDocNew.Tag := 0;
  DocBtnControl;

  if (Sender as TsButton).Name = 'btnDocSave' then
  begin
    insertRFF;
  end;


  ReadDocList(edtMAINT_NO.Text);
end;

procedure TUI_APPRMI_NEW_frm.ReadDocList(DocNo : String);
begin
  with qryRFF do
  begin
    Close;
    Parameters[0].Value := DocNo;
    Parameters[1].Value := tempKey;
    Open;
  end;
end;

procedure TUI_APPRMI_NEW_frm.insertRFF;
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;
  try
    with SQLCreator do
    begin
      DMLType := dmlInsert;
      SQLHeader('APPRMI_RFF');
      ADDValue('MAINT_NO', tempKey);
      ADDValue('RFF_NO', edtDoc_RFF_NO.Text);
      ADDValue('HS_CD', mskDoc_HS_CODE.Text);
      ADDValue('CONT_DT', mskDoc_CONT_DT.Text);
      ADDValue('EXPORT_NAT_CD', edtDoc_EXPORT_NAT_CD.Text);
      ADDValue('IMPT_CD', edtDoc_IMPT_CD.Text);
      ADDValue('AMT', currDoc_AMT.Value);
      ADDValue('AMT_UNIT', edtDoc_AMT_UNIT.Text);
      ADDValue('REMIT_AMT', currDoc_AMT.Value);
      ADDValue('REMIT_AMT_UNIT', edtDoc_AMT_UNIT.Text);
      ADDValue('CIF_AMT', currDoc_CIF_AMT.Value);
      ADDValue('CIF_AMT_UNIT', edtDoc_CIF_AMT_UNIT.Text);
      ADDValue('GOODS_NM', edtDoc_GOODS_NM.Text);
      ADDValue('TOD_CD', edtDoc_TOD_CD.Text);
      ADDValue('TOD_REMARK', edtDoc_TOD_REMARK.Text);
      ADDValue('EXPORT_OW_NM', edtDoc_EXPORT_OW_NM.Text);
      ADDValue('EXPORT_OW_ADDR', edtDoc_EXPORT_OW_ADDR.Text);
      ADDValue('EXPORT_OW_NAT_CD', edtDoc_EXPORT_OW_NAT_CD.Text);
      ADDValue('IMPORT_OW_NM', edtDoc_IMPORT_OW_NM.Text);
      ADDValue('IMPORT_OW_ADDR', edtDoc_IMPORT_OW_ADDR.Text);
      ADDValue('IMPORT_OW_NAT_CD', edtDoc_IMPORT_OW_NAT_CD.Text);
      ADDValue('DETAIL_DESC1', sMemo1.Lines.Strings[0]);
      ADDValue('DETAIL_DESC2', sMemo1.Lines.Strings[1]);
      ADDValue('DETAIL_DESC3', sMemo1.Lines.Strings[2]);
      ADDValue('DETAIL_DESC4', sMemo1.Lines.Strings[3]);
      ADDValue('DETAIL_DESC5', sMemo1.Lines.Strings[4]);
      ExecSQL(CreateSQL);     
    end;
  finally
    SQLCreator.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.DeleteTempData;
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;

  try
    with SQLCreator do
    begin
      DMLType := dmlDelete;
      SQLHeader('APPRMI_RFF');
      ADDWhere('MAINT_NO', tempKey);
      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
  end;

end;

procedure TUI_APPRMI_NEW_frm.SaveTempData;
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;

  try
    with SQLCreator do
    begin
      DMLType := dmlUpdate;
      SQLHeader('APPRMI_RFF');
      ADDValue('MAINT_NO', edtMAINT_NO.Text);
      ADDWhere('MAINT_NO', tempKey);
      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
  end;

end;

procedure TUI_APPRMI_NEW_frm.SaveData(Sender : TObject);
var
  SQLCreator : TSQLCreate;
  TEMP_STR_LIST : TStringList;
  BankPassword : TEncryptBankPasword;
begin
  SQLCreator := TSQLCreate.Create;
  TEMP_STR_LIST := TStringList.Create;
  BankPassword := TEncryptBankPasword.Create;
  
  try
    with SQLCreator do
    begin
      case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO', edtMAINT_NO.Text);
        end;

        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO', edtMAINT_NO.Text);
        end;
      end;

      SQLHeader('APPRMI_H');
      ADDValue('MESSAGE1', CM_TEXT(com_func,[':']));
      ADDValue('MESSAGE2', CM_TEXT(com_type,[':']));
      ADDValue('CHK2', (Sender as TsButton).Tag);
      ADDValue('APP_DT', msk_APP_DATE.Text);
      ADDValue('REG_DT', 'getdate()', vtVariant);
      ADDValue('USER_ID', edt_userno.Text);
      ADDValue('TRN_HOPE_DT', msk_TRN_HOPE_DT.Text);
      ADDValue('IMPT_SCHEDULE_DT', msk_IMPT_SCHEDULE_DT.Text);
      ADDValue('ETC_NO', edt_ETC_NO.Text);
      ADDValue('TRD_CD', edt_TRD_CD.Text);
      ADDValue('IMPT_REMIT_CD', edt_IMPT_REMIT_CD.Text);
      ADDValue('IMPT_TRAN_FEE_CD', edt_IMPT_TRAN_FEE_CD.Text);

      //송금내역
      IF Trim(edt_REMIT_DESC1.Text) <> '' then
        TEMP_STR_LIST.Add(edt_REMIT_DESC1.Text);
      IF Trim(edt_REMIT_DESC2.Text) <> '' then
        TEMP_STR_LIST.Add(edt_REMIT_DESC2.Text);
      IF Trim(edt_REMIT_DESC3.Text) <> '' then
        TEMP_STR_LIST.Add(edt_REMIT_DESC3.Text);
      IF Trim(edt_REMIT_DESC4.Text) <> '' then
        TEMP_STR_LIST.Add(edt_REMIT_DESC4.Text);
      ADDValue('REMIT_DESC', TEMP_STR_LIST.Text);

      ADDValue('REMIT_CD', edt_REMIT_CD.Text);
      ADDValue('ADDED_FEE_TAR', edt_ADDED_FEE_TAR.Text);
      ADDValue('TOTAL_AMT', curr_TOTAL_AMT.Value);
      ADDValue('TOTAL_AMT_UNIT', edt_TOTAL_AMT_UNIT.Text);
      ADDValue('FORE_AMT', curr_FORE_AMT.Value);
      ADDValue('FORE_AMT_UNIT', edt_FORE_AMT_UNIT.Text);
      BankPassword.Key := RightStr(msk_APP_DATE.Text,4);
      ADDValue('ONLY_PWD', BankPassword.EncryptBankPassword(edt_ONLY_PWD.Text));
      ADDValue('AUTH_NM3', Edt_ApplicationElectronicSign.Text);

      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
    TEMP_STR_LIST.Free;
    BankPassword.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.ReadAPPRMI_H;
begin
  with qryAPPRMI_H do
  begin
    Close;
    SQL.Text := FSQL;
    Parameters[0].Value := sMaskEdit1.Text;
    Parameters[1].Value := sMaskEdit2.Text;
    IF Trim(edt_SearchNo.Text) <> '' Then
    begin
      SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+edt_SearchNo.text+'%'));
    end;
    Open;
  end;
end;

procedure TUI_APPRMI_NEW_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryAPPRMI_H.SQL.Text;
end;

procedure TUI_APPRMI_NEW_frm.ReadAPPRMI_H_DATA;
var
  TEMP_STR_LIST : TStringList;
  BankPassword : TEncryptBankPasword;
begin
  TEMP_STR_LIST := TStringList.Create;
  BankPassword := TEncryptBankPasword.Create;
  try
    with qryAPPRMI_H do
    begin
      edtMAINT_NO.Text := qryAPPRMI_HMAINT_NO.AsString;
      msk_Datee.Text := qryAPPRMI_HREG_DT.AsString;
      edt_userno.Text := qryAPPRMI_HUSER_ID.AsString;
      CM_INDEX(com_func, [':'], 0, qryAPPRMI_HMESSAGE1.AsString, 1);
      CM_INDEX(com_type, [':'], 0, qryAPPRMI_HMESSAGE2.AsString);
      msk_APP_DATE.Text := qryAPPRMI_HAPP_DT.AsString;
      msk_IMPT_SCHEDULE_DT.Text := qryAPPRMI_HIMPT_SCHEDULE_DT.AsString;
      msk_TRN_HOPE_DT.Text := qryAPPRMI_HTRN_HOPE_DT.AsString;
      edt_ETC_NO.Text := qryAPPRMI_HETC_NO.AsString;
      edt_TOTAL_AMT_UNIT.Text := qryAPPRMI_HTOTAL_AMT_UNIT.AsString;
      curr_TOTAL_AMT.Value := qryAPPRMI_HTOTAL_AMT.AsCurrency;
      edt_FORE_AMT_UNIT.Text := qryAPPRMI_HFORE_AMT_UNIT.AsString;
      curr_FORE_AMT.Value := qryAPPRMI_HFORE_AMT.AsCurrency;
      edt_TRD_CD.Text := qryAPPRMI_HTRD_CD.AsString;
      sEdit3.Text := qryAPPRMI_HTRD_NM.AsString;
      edt_IMPT_REMIT_CD.Text := qryAPPRMI_HIMPT_REMIT_CD.AsString;
      sEdit5.Text := qryAPPRMI_HIMPT_REMIT_NM.AsString;
      edt_IMPT_TRAN_FEE_CD.Text := qryAPPRMI_HIMPT_TRAN_FEE_CD.AsString;
      sEdit6.Text := qryAPPRMI_HIMPT_TRAN_FEE_NM.AsString;
      edt_REMIT_CD.Text := qryAPPRMI_HREMIT_CD.AsString;
      sEdit9.Text := qryAPPRMI_HREMIT_NM.AsString;
      TEMP_STR_LIST.Text := qryAPPRMI_HREMIT_DESC.AsString;
      if TEMP_STR_LIST.Count > 0 Then
        edt_REMIT_DESC1.Text := TEMP_STR_LIST.Strings[0];
      if TEMP_STR_LIST.Count > 1 Then
        edt_REMIT_DESC2.Text := TEMP_STR_LIST.Strings[1];
      if TEMP_STR_LIST.Count > 2 Then
        edt_REMIT_DESC3.Text := TEMP_STR_LIST.Strings[2];
      if TEMP_STR_LIST.Count > 3 Then
        edt_REMIT_DESC4.Text := TEMP_STR_LIST.Strings[3];
      edt_ADDED_FEE_TAR.Text := qryAPPRMI_HADDED_FEE_TAR.AsString;
      sEdit11.Text := qryAPPRMI_HADDED_FEE_TAR_NM.AsString;
      Edt_ApplicationElectronicSign.Text := qryAPPRMI_HAUTH_NM3.AsString;
      BankPassword.Key := RightStr(qryAPPRMI_HAPP_DT.AsString,4);
      edt_ONLY_PWD.Text := BankPassword.DescryptBankPassword(qryAPPRMI_HONLY_PWD.AsString);
    end;
  finally
    TEMP_STR_LIST.Free;
    BankPassword.Free;
  end;
end;

procedure TUI_APPRMI_NEW_frm.qryAPPRMI_HAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ClearControl(sPanel7);
  ReadAPPRMI_H_DATA;
  ReadAPPRMI_NAD(qryAPPRMI_HMAINT_NO.AsString);
end;

procedure TUI_APPRMI_NEW_frm.SaveDataNAD;
var
  SQLCreator : TSQLCreate;
begin
  SQLCreator := TSQLCreate.Create;

  //전체 삭제 후 다시 INSERT
  try
    with SQLCreator do
    begin
      //전체 삭제 후
      DMLType := dmlDelete;
      SQLHeader('APPRMI_NAD');
      ADDWhere('MAINT_NO', edtMAINT_NO.Text);
      ExecSQL(CreateSQL);

      //다시 INSERT
      // 송금 의뢰인
      DMLType := dmlInsert;
      SQLHeader('APPRMI_NAD');
      ADDValue('MAINT_NO', edtMAINT_NO.Text);
      ADDValue('CODE', Edt_ApplicationCode.Text);
      ADDValue('TAG', 'OY');
      ADDValue('BUS_NO', Edt_ApplicationSAUPNO.Text);
      ADDValue('BUS_NM1', Edt_ApplicationName1.Text);
      ADDValue('BUS_ADDR1', Edt_ApplicationAddr1.Text);
      ADDValue('BUS_ADDR2', Edt_ApplicationAddr2.Text);
      ExecSQL(CreateSQL);

      // 수익자
      DMLType := dmlInsert;
      SQLHeader('APPRMI_NAD');
      ADDValue('MAINT_NO', edtMAINT_NO.Text);
      ADDValue('CODE', edtBEN_CODE.Text);      
      ADDValue('TAG', 'BE');
      ADDValue('BUS_NO', '');      
      ADDValue('BUS_NM1', edtBEN_NAME1.Text);
      ADDValue('BUS_NM2', edtBEN_NAME2.Text);
      ADDValue('BUS_NM3', edtBEN_NAME3.Text);
      ADDValue('BUS_ADDR1', edtBEN_STR1.Text);
      ADDValue('BUS_ADDR2', edtBEN_STR2.Text);
      ADDValue('BUS_ADDR3', edtBEN_STR3.Text);
      ADDValue('NAT_CD', edtBEN_NATION.Text);
      ADDValue('TEL_NO', edtBEN_TELE.Text);
      ExecSQL(CreateSQL);
    end;
  finally
    SQLCreator.Free;
  end;
end;
procedure TUI_APPRMI_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadAPPRMI_H;
end;

procedure TUI_APPRMI_NEW_frm.ReadAPPRMI_NAD(MAINT_NO : String);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT MAINT_NO, TAG, CODE, BUS_NO, BUS_NM1, BUS_NM2, BUS_NM3, BUS_ADDR1, BUS_ADDR2, BUS_ADDR3, NAT_CD, TEL_NO FROM APPRMI_NAD WHERE MAINT_NO = '+QuotedStr(MAINT_NO);
      Open;

      while not eof do
      begin
        // 송금의뢰인
        if FieldByName('TAG').AsString = 'OY' then
        begin
          Edt_ApplicationCode.Text := FieldByName('CODE').AsString;
          Edt_ApplicationName1.Text := FieldByName('BUS_NM1').AsString;
          Edt_ApplicationSAUPNO.Text := FieldByName('BUS_NO').AsString;
          Edt_ApplicationAddr1.Text := FieldByName('BUS_ADDR1').AsString;
          Edt_ApplicationAddr2.Text := FieldByName('BUS_ADDR2').AsString;
        end;

        // 송금의뢰인
        if FieldByName('TAG').AsString = 'BE' then
        begin
          edtBEN_CODE.Text := FieldByName('CODE').AsString;
          edtBEN_NAME1.Text := FieldByName('BUS_NM1').AsString;
          edtBEN_NAME2.Text := FieldByName('BUS_NM2').AsString;
          edtBEN_NAME3.Text := FieldByName('BUS_NM3').AsString;
          edtBEN_STR1.Text := FieldByName('BUS_ADDR1').AsString;
          edtBEN_STR2.Text := FieldByName('BUS_ADDR2').AsString;
          edtBEN_STR3.Text := FieldByName('BUS_ADDR3').AsString;
          edtBEN_TELE.Text := FieldByName('TEL_NO').AsString;
          edtBEN_NATION.Text := FieldByName('NAT_CD').AsString;
        end;

        Next;
      end;

      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

end.

