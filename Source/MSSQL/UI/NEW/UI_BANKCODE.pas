unit UI_BANKCODE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, Grids, DBGrids, acDBGrid, StdCtrls,
  sComboBox, sEdit, sButton, Buttons, sSpeedButton, ExtCtrls, sPanel,
  sSkinProvider;

type
  TUI_BANKCODE_frm = class(TChildForm_frm)
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    sSpeedButton6: TsSpeedButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnExit: TsButton;
    sPanel1: TsPanel;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sPanel8: TsPanel;
    edt_FindText: TsEdit;
    sComboBox1: TsComboBox;
    sButton1: TsButton;
    sDBGrid1: TsDBGrid;
    qryList: TADOQuery;
    dsList: TDataSource;
    procedure btnNewClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure edt_FindTextKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure doRefresh;
    procedure ReadList;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_BANKCODE_frm: TUI_BANKCODE_frm;

implementation

uses
  Dlg_Bank, TypeDefine;

{$R *.dfm}

procedure TUI_BANKCODE_frm.btnNewClick(Sender: TObject);
var
  TempControl : TProgramControlType;
begin
  inherited;
  Dlg_Bank_frm := TDlg_Bank_frm.Create(Self);

  Case (sender as TsButton).Tag of
    0: TempControl := ctInsert;
    1: TempControl := ctModify;
    2: TempControl := ctDelete;
  end;

  try
    IF Dlg_Bank_frm.Run(TempControl,qryList.Fields) = mrOk Then
    begin
      Case TempControl of
        ctInsert,ctDelete :
        begin
          qryList.Close;
          qryList.Open;
        end;

        ctModify :
        begin
          doRefresh;
        end;

      end;
    end;
  finally
    FreeAndNil(Dlg_Bank_frm);
  end;
end;

procedure TUI_BANKCODE_frm.doRefresh;
var
  BMK : TBookmark;
begin
  BMK := qryList.GetBookmark;

  qryList.Close;
  qryList.Open;

  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);

  qryList.FreeBookmark(BMK);
end;

procedure TUI_BANKCODE_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    SQL.Text := 'SELECT * FROM BANKCODE';
    Case sComboBox1.ItemIndex of
      0: SQL.Add('WHERE [CODE] LIKE '+QuotedStr('%'+edt_FindText.Text+'%'));
      1: SQL.Add('WHERE [ENAME1] LIKE '+QuotedStr('%'+edt_FindText.Text+'%'));
    end;
    Open;
  end;
end;

procedure TUI_BANKCODE_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_BANKCODE_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_BANKCODE_frm.edt_FindTextKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN Then ReadList;
end;

procedure TUI_BANKCODE_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_BANKCODE_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_BANKCODE_frm := nil;
end;

end.
