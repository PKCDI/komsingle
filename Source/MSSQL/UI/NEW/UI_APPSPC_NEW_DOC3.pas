unit UI_APPSPC_NEW_DOC3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, Buttons, sSpeedButton, Grids, DBGrids, acDBGrid,
  StdCtrls, sLabel, Mask, sMaskEdit, sBitBtn, sEdit, ExtCtrls, sPanel,
  sSkinProvider, sButton, DB, ADODB, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit;

type
  TUI_APPSPC_NEW_DOC3_frm = class(TChildForm_frm)
    sPanel17: TsPanel;
    sPanel15: TsPanel;
    sPanel19: TsPanel;
    sPanel18: TsPanel;
    sDBGrid4: TsDBGrid;
    sPanel2: TsPanel;
    btnEdtD2: TsBitBtn;
    btnDelD2: TsBitBtn;
    btnSaveD2: TsBitBtn;
    btnCancelD2: TsBitBtn;
    btnAddD2: TsBitBtn;
    sPanel16: TsPanel;
    btnAdd_SIZE: TsSpeedButton;
    btnAdd_IMD: TsSpeedButton;
    edt_LINE_NO: TsEdit;
    edt_SIZE: TsEdit;
    edt_IMD_CODE: TsEdit;
    edt_QTYC: TsEdit;
    edt_PRICEC: TsEdit;
    edt_VB_AMTC: TsEdit;
    edt_SUP_TOTAMTC: TsEdit;
    sPanel14: TsPanel;
    sPanel3: TsPanel;
    sPanel4: TsPanel;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sPanel7: TsPanel;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    sPanel12: TsPanel;
    edt_DOC_NO: TsEdit;
    msk_ISS_DATE: TsMaskEdit;
    edt_TOTCNTC: TsEdit;
    edt_ISS_TOTAMTC: TsEdit;
    edt_REMARK1_1: TsEdit;
    edt_REMARK1_2: TsEdit;
    edt_REMARK1_3: TsEdit;
    edt_REMARK1_4: TsEdit;
    edt_REMARK1_5: TsEdit;
    edt_SE_CODE: TsEdit;
    edt_SE_SNAME: TsEdit;
    edt_SE_NAME: TsEdit;
    edt_SE_ADDR1: TsEdit;
    edt_SE_ADDR2: TsEdit;
    edt_SE_ADDR3: TsEdit;
    edt_BY_CODE: TsEdit;
    edt_BY_SNAME: TsEdit;
    edt_BY_NAME: TsEdit;
    edt_BY_ADDR1: TsEdit;
    edt_BY_ADDR2: TsEdit;
    edt_BY_ADDR3: TsEdit;
    sPanel1: TsPanel;
    btnSave: TsButton;
    btnCancel: TsButton;
    qryListD2: TADOQuery;
    dsListD2: TDataSource;
    edt_TOTCNT: TsCurrencyEdit;
    edt_ISS_TOTAMT: TsCurrencyEdit;
    sPanel13: TsPanel;
    sLabel2: TsLabel;
    edt_SIZE1: TsEdit;
    edt_SIZE2: TsEdit;
    edt_SIZE3: TsEdit;
    edt_SIZE4: TsEdit;
    sButton2: TsButton;
    edt_SIZE5: TsEdit;
    edt_SIZE6: TsEdit;
    edt_SIZE7: TsEdit;
    edt_SIZE8: TsEdit;
    edt_SIZE9: TsEdit;
    edt_SIZE10: TsEdit;
    sPanel20: TsPanel;
    sLabel1: TsLabel;
    edt_IMD_CODE1: TsEdit;
    edt_IMD_CODE2: TsEdit;
    edt_IMD_CODE3: TsEdit;
    edt_IMD_CODE4: TsEdit;
    sButton1: TsButton;
    edt_PRICE: TsCurrencyEdit;
    edt_QTY: TsCurrencyEdit;
    edt_VB_AMT: TsCurrencyEdit;
    edt_PRICE_G: TsCurrencyEdit;
    edt_SUP_TOTAMT: TsCurrencyEdit;
    qryD1Cancel: TADOQuery;
    edt_TOTQTYC: TsEdit;
    edt_TOTQTY: TsCurrencyEdit;
    qryListD2KEYY: TStringField;
    qryListD2DOC_GUBUN: TStringField;
    qryListD2SEQ: TIntegerField;
    qryListD2LINE_NO: TStringField;
    qryListD2HS_NO: TStringField;
    qryListD2DE_DATE: TStringField;
    qryListD2IMD_CODE1: TStringField;
    qryListD2IMD_CODE2: TStringField;
    qryListD2IMD_CODE3: TStringField;
    qryListD2IMD_CODE4: TStringField;
    qryListD2SIZE1: TStringField;
    qryListD2SIZE2: TStringField;
    qryListD2SIZE3: TStringField;
    qryListD2SIZE4: TStringField;
    qryListD2SIZE5: TStringField;
    qryListD2SIZE6: TStringField;
    qryListD2SIZE7: TStringField;
    qryListD2SIZE8: TStringField;
    qryListD2SIZE9: TStringField;
    qryListD2SIZE10: TStringField;
    qryListD2QTY: TBCDField;
    qryListD2QTYC: TStringField;
    qryListD2TOTQTY: TBCDField;
    qryListD2TOTQTYC: TStringField;
    qryListD2PRICE: TBCDField;
    qryListD2PRICE_G: TBCDField;
    qryListD2PRICEC: TStringField;
    qryListD2CUX_RATE: TBCDField;
    qryListD2SUP_AMT: TBCDField;
    qryListD2SUP_AMTC: TStringField;
    qryListD2VB_TAX: TBCDField;
    qryListD2VB_TAXC: TStringField;
    qryListD2VB_AMT: TBCDField;
    qryListD2VB_AMTC: TStringField;
    qryListD2SUP_TOTAMT: TBCDField;
    qryListD2SUP_TOTAMTC: TStringField;
    qryListD2VB_TOTTAX: TBCDField;
    qryListD2VB_TOTTAXC: TStringField;
    qryListD2VB_TOTAMT: TBCDField;
    qryListD2VB_TOTAMTC: TStringField;
    qryListD2BIGO1: TStringField;
    qryListD2BIGO2: TStringField;
    qryListD2BIGO3: TStringField;
    qryListD2BIGO4: TStringField;
    qryListD2BIGO5: TStringField;
    procedure btnCancelClick(Sender: TObject);
    procedure qryListD2AfterScroll(DataSet: TDataSet);
    procedure btnSaveClick(Sender: TObject);
    procedure btnAddD2Click(Sender: TObject);
    procedure btnCancelD2Click(Sender: TObject);
    procedure btnEdtD2Click(Sender: TObject);
    procedure btnDelD2Click(Sender: TObject);
    procedure btnSaveD2Click(Sender: TObject);
    procedure edt_IMD_CODEExit(Sender: TObject);
    procedure edt_SIZEExit(Sender: TObject);
    procedure btnAdd_IMDClick(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure btnAdd_SIZEClick(Sender: TObject);
    procedure edt_SIZE1Exit(Sender: TObject);
    procedure edt_IMD_CODE1Exit(Sender: TObject);
    procedure CODEDblClick(Sender: TObject);
  private     
    mode_D2:Integer;
    ReadWrite_D2:Boolean;
    function ErrorCheck:String;
    procedure BtnControl(tag:Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_APPSPC_NEW_DOC3_frm: TUI_APPSPC_NEW_DOC3_frm;

implementation

uses
  ICON, MSSQL, UI_APPSPC_NEW, AutoNo,VarDefine,TypeDefine,
  CD_CUST, SQLCreator, CD_AMT_UNIT, CD_BANK, CD_APPSPCBANK, CodeContents,
  CodeDialogParent, CD_UNIT, Dlg_ErrorMessage;

{$R *.dfm}

procedure TUI_APPSPC_NEW_DOC3_frm.btnCancelClick(Sender: TObject);
begin
  inherited;

  
  case UI_APPSPC_NEW_frm.mode_D1 of
    1://신규-취소
    begin
      with TADOQuery.Create(Nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := 'DELETE FROM APPSPC_D1 WHERE KEYY ='+QuotedStr(UI_APPSPC_NEW_frm.Maint_No)+' and DOC_GUBUN = '+QuotedStr('1BW')+'and SEQ = '+QuotedStr(IntToStr(UI_APPSPC_NEW_frm.SEQ));
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;
    end;
    2://수정-취소
    begin
      qryD1Cancel.Close;
      qryD1Cancel.Parameters.ParamByName('KEYY').Value := UI_APPSPC_NEW_frm.Maint_No;
      qryD1Cancel.Parameters.ParamByName('SEQ').Value := UI_APPSPC_NEW_frm.SEQ;
      qryD1Cancel.Parameters.ParamByName('DOC_GUBUN').Value := '1BW';
      qryD1Cancel.Parameters.ParamByName('KEYY2').Value := UI_APPSPC_NEW_frm.Maint_No;
      qryD1Cancel.ExecSQL;
    end;
  end;
  Close;
end;

procedure TUI_APPSPC_NEW_DOC3_frm.qryListD2AfterScroll(DataSet: TDataSet);
begin
  inherited;
  //일련번호
  edt_LINE_NO.Text := qryListD2.FIeldByName('LINE_NO').AsString;
  //품목
  edt_IMD_CODE.Text := qryListD2.FIeldByName('IMD_CODE1').AsString;
  edt_IMD_CODE1.Text := qryListD2.FIeldByName('IMD_CODE1').AsString;
  edt_IMD_CODE2.Text := qryListD2.FIeldByName('IMD_CODE2').AsString;
  edt_IMD_CODE3.Text := qryListD2.FIeldByName('IMD_CODE3').AsString;
  edt_IMD_CODE4.Text := qryListD2.FIeldByName('IMD_CODE4').AsString;
  //규격
  edt_SIZE.Text := qryListD2.FIeldByName('SIZE1').AsString;
  edt_SIZE1.Text := qryListD2.FIeldByName('SIZE1').AsString;
  edt_SIZE2.Text := qryListD2.FIeldByName('SIZE2').AsString;
  edt_SIZE3.Text := qryListD2.FIeldByName('SIZE3').AsString;
  edt_SIZE4.Text := qryListD2.FIeldByName('SIZE4').AsString;
  edt_SIZE5.Text := qryListD2.FIeldByName('SIZE5').AsString;
  edt_SIZE6.Text := qryListD2.FIeldByName('SIZE6').AsString;
  edt_SIZE7.Text := qryListD2.FIeldByName('SIZE7').AsString;
  edt_SIZE8.Text := qryListD2.FIeldByName('SIZE8').AsString;
  edt_SIZE9.Text := qryListD2.FIeldByName('SIZE9').AsString;
  edt_SIZE10.Text := qryListD2.FIeldByName('SIZE10').AsString;
  //수량 단위
  edt_QTYC.Text := qryListD2.FIeldByName('QTYC').AsString;
  //수량
  edt_QTY.Text := FloatToStr(qryListD2.FIeldByName('QTY').AsFloat);
  //수량소계 단위
//  edt_TOTQTYC.Text := qryListD2.FIeldByName('TOTQTYC').AsString;
  //수량소계
//  edt_TOTQTY.Text := FloatToStr(qryListD2.FIeldByName('TOTQTY').AsFloat);
  //단가
  edt_PRICE.Text := FloatToStr(qryListD2.FIeldByName('PRICE').AsFloat);
  //기준수량코드
  edt_PRICEC.Text := qryListD2.FIeldByName('PRICEC').AsString;
  //기준수량
  edt_PRICE_G.Text := FloatToStr(qryListD2.FIeldByName('PRICE_G').AsFloat);
  //공급가액외화 통화단위
  edt_VB_AMTC.Text := qryListD2.FIeldByName('VB_AMTC').AsString;
  //공급가액외화
  edt_VB_AMT.Text := FloatToStr(qryListD2.FIeldByName('VB_AMT').AsFloat);
//  //공급가액외화 소계 통화단위
//  edt_SUP_TOTAMTC.Text := qryListD2.FIeldByName('SUP_TOTAMTC').AsString;
//  //공급가액외화 소계
//  edt_SUP_TOTAMT.Text := FloatToStr(qryListD2.FIeldByName('SUP_TOTAMT').AsFloat);

end;

procedure TUI_APPSPC_NEW_DOC3_frm.btnSaveClick(Sender: TObject);
var
  SQLCreate_D1:TSQLCreate;
begin
  inherited;

  if ReadWrite_D2 then
  begin
    ShowMessage('작성중인 품목사항이 있습니다.');
    exit;
  end;

  //유효성검사
  Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
  if Dlg_ErrorMessage_frm.Run_ErrorMessage( '판매대금추심(매입)의뢰서 - 상업송장' ,ErrorCheck ) Then
  begin
    FreeAndNil(Dlg_ErrorMessage_frm);
    Exit;
  end;

  SQLCreate_D1 := TSQLCreate.Create;
  with SQLCreate_D1 do
  begin
    DMLType := dmlUpdate;
    SQLHeader('APPSPC_D1');

    ADDWhere('KEYY',UI_APPSPC_NEW_frm.Maint_No);
    ADDWhere('DOC_GUBUN','1BW');
    ADDWhere('SEQ', UI_APPSPC_NEW_frm.SEQ);

    //문서번호
    ADDValue('DOC_NO',edt_DOC_NO.Text);
    //발급일자
    ADDValue('ISS_DATE',msk_ISS_DATE.Text);
    //총수량 단위
    ADDValue('TOTCNTC',edt_TOTCNTC.Text);
    //총수량
    ADDValue('TOTCNT',edt_TOTCNT.Value);
    //총금액
    ADDValue('ISS_TOTAMT',edt_ISS_TOTAMT.Value);
    //총금액 단위
    ADDValue('ISS_TOTAMTC',edt_ISS_TOTAMTC.Text);

    //항차
    ADDValue('REMARK1_1',edt_REMARK1_1.Text);
    //선명
    ADDValue('REMARK1_2',edt_REMARK1_2.Text);
    //선적지
    ADDValue('REMARK1_3',edt_REMARK1_3.Text);
    //도착지
    ADDValue('REMARK1_4',edt_REMARK1_4.Text);
    //인도조건
    ADDValue('REMARK1_5',edt_REMARK1_5.Text);

    //물품공급자(수출자)
    //상호코드
    ADDValue('SE_CODE', edt_SE_CODE.Text);
    //상호
    ADDValue('SE_SNAME', edt_SE_SNAME.Text);
    //대표자
    ADDValue('SE_NAME', edt_SE_NAME.Text);
    //주소
    ADDValue('SE_ADDR1', edt_SE_ADDR1.Text);
    ADDValue('SE_ADDR2', edt_SE_ADDR2.Text);
    ADDValue('SE_ADDR3', edt_SE_ADDR3.Text);

    //공급받는자(수입자)
    //상호코드
    ADDValue('BY_CODE',edt_BY_CODE.Text);
    //상호
    ADDValue('BY_SNAME', edt_BY_SNAME.Text);
    //대표자
    ADDValue('BY_NAME', edt_BY_NAME.Text);
    //주소                                   
    ADDValue('BY_ADDR1', edt_BY_ADDR1.Text);
    ADDValue('BY_ADDR2', edt_BY_ADDR2.Text);
    ADDValue('BY_ADDR3', edt_BY_ADDR3.Text);


    with TADOQuery.Create(Nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SQLCreate_D1.CreateSQL +#13#10;
        SQL.Add('DELETE FROM APPSPC_D2 WHERE KEYY='+QuotedStr('0'));
        ExecSQL;
        ShowMessage('저장되었습니다.');
      finally
        close;
        free;
      end;
    end;
  end;

  Close;

  with UI_APPSPC_NEW_frm.qryListD1_1BW do
  begin
    Close;
    Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
    Open;
    UI_APPSPC_NEW_frm.sPanel56.Caption := '상업송장 : '+IntToStr(RecordCount)+'건';
  end;

end;

procedure TUI_APPSPC_NEW_DOC3_frm.btnAddD2Click(Sender: TObject);
begin
  inherited;
  mode_D2 := 1;//신규
  ReadWrite_D2 := True;
  EnableControl(sPanel16);
  ClearControl(sPanel16);
  ClearControl(sPanel13);
  ClearControl(sPanel20);
  BtnControl(1);
  edt_LINE_NO.Enabled := False;
  edt_LINE_NO.Text := Format('%0.4d',[qryListD2.recordCount+1]);

end;

procedure TUI_APPSPC_NEW_DOC3_frm.btnCancelD2Click(Sender: TObject);
begin
  inherited;
  EnableControl(sPanel16,False);
  BtnControl(2);
  qryListD2.First;
  ReadWrite_D2 := False;

end;

procedure TUI_APPSPC_NEW_DOC3_frm.btnEdtD2Click(Sender: TObject);
begin
  inherited;    
  if qryListD2.RecordCount = 0 then
  begin
    ShowMessage('수정할 상업송장 품목내역이 없습니다.');
    exit;
  end;
  ReadWrite_D2 := True;
  mode_D2 := 2; //수정
  EnableControl(sPanel16);
  BtnControl(3);
  edt_LINE_NO.Enabled := False;

end;

procedure TUI_APPSPC_NEW_DOC3_frm.BtnControl(tag:Integer);
begin
    case Tag of
    1://입력
    begin
      btnAddD2.Enabled := False;
      btnCancelD2.Enabled := True;
      btnEdtD2.Enabled := False;
      btnSaveD2.Enabled := True;
      btnDelD2.Enabled := False;
    end;
    2://취소
    begin
      btnAddD2.Enabled := True;
      btnCancelD2.Enabled := False;
      btnEdtD2.Enabled := True;
      btnSaveD2.Enabled := False;
      btnDelD2.Enabled := True;
    end;
    3://수정
    begin
      btnAddD2.Enabled := False;
      btnCancelD2.Enabled := True;
      btnEdtD2.Enabled := False;
      btnSaveD2.Enabled := True;
      btnDelD2.Enabled := False;
    end;
    4://저장
    begin
      btnAddD2.Enabled := True;
      btnCancelD2.Enabled := False;
      btnEdtD2.Enabled := True;
      btnSaveD2.Enabled := False;
      btnDelD2.Enabled := True;
    end;
    5://삭제
    begin 
      btnAddD2.Enabled := True;
      btnCancelD2.Enabled := False;
      btnEdtD2.Enabled := True;
      btnSaveD2.Enabled := False;
      btnDelD2.Enabled := True;
    end;
  end;
end;

procedure TUI_APPSPC_NEW_DOC3_frm.btnDelD2Click(Sender: TObject);
var
  SQLCreate_D2:TSQLCreate;
  sSQL_D2:String;
  iLINE_NO:Integer;
begin
  inherited;      
  if qryListD2.RecordCount = 0 then
  begin
    ShowMessage('삭제할 상업송장 품목내역이 없습니다.');
    exit;
  end;
  
  SQLCreate_D2 := TSQLCreate.Create;
  with SQLCreate_D2 do
  begin
    DMLType := dmlDelete;
    SQLHeader('APPSPC_D2');
    ADDWhere('KEYY',UI_APPSPC_NEW_frm.Maint_No);
    ADDWhere('DOC_GUBUN','1BW');
    ADDWhere('SEQ',UI_APPSPC_NEW_frm.SEQ);
    ADDWhere('LINE_NO',qryListD2.FieldByName('Line_No').AsString);
  end;
  iLINE_NO := qryListD2.FieldByName('Line_No').AsInteger;

//  SQL_D2 := 'UPDATE APPSPC_D2 SET SEQ = CONVERT(INT,SEQ-1) WHERE CONVERT(INT,SEQ1)';
//  SQL_D1 := 'UPDATE APPSPC_D1 SET SEQ = SEQ -1 WHERE KEYY = '+QuotedStr(Maint_No)+'AND SEQ > '+IntToStr(SEQ)+'AND DOC_GUBUN = '+QuotedStr('1BW');
  sSQL_D2 := 'UPDATE APPSPC_D2 ';
  sSQL_D2 := sSQL_D2 + 'SET LINE_NO = REPLICATE('+QuotedStr('0')+',4-LEN(CONVERT(INT,LINE_NO)-1)) + CONVERT(VARCHAR(8),CONVERT(INT,LINE_NO)-1)';
  sSQL_D2 := sSQL_D2 + 'WHERE CONVERT(INT,LINE_NO) > '+IntToStr(iLINE_NO)+' and KEYY ='+QuotedStr(UI_APPSPC_NEW_frm.Maint_No)+' and SEQ = '+IntToStr(UI_APPSPC_NEW_frm.SEQ)+' and DOC_GUBUN = '+QuotedStr('1BW');
//  ShowMessage(sSQL_D2);
  with TADOQuery.Create(Nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := SQLCreate_D2.CreateSQL+#13#10+sSQL_D2;
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;

  qryListD2.Close;
  qryListD2.Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
  qryListD2.Parameters[1].Value := '1BW';
  qryListD2.Parameters[2].Value := UI_APPSPC_NEW_frm.SEQ;
  qryListD2.Open;
  
  BtnControl(5);
end;

procedure TUI_APPSPC_NEW_DOC3_frm.btnSaveD2Click(Sender: TObject);
var
  SQLCreate_D2:TSQLCreate;
  Line_No:String;
begin
  inherited;
  SQLCreate_D2 := TSQLCreate.Create;
  with SQLCreate_D2 do
  begin
    case mode_D2 of
      1://신규-저장
      begin
        DMLType := dmlInsert;
        SQLHeader('APPSPC_D2');

        ADDValue('KEYY',UI_APPSPC_NEW_frm.Maint_No);
        ADDValue('DOC_GUBUN','1BW');
        ADDValue('SEQ',UI_APPSPC_NEW_frm.SEQ);
        ADDValue('LINE_NO',edt_LINE_NO.Text);

        ADDValue('IMD_CODE1', edt_IMD_CODE.Text);
        ADDValue('IMD_CODE2', edt_IMD_CODE2.Text);
        ADDValue('IMD_CODE3', edt_IMD_CODE3.Text);
        ADDValue('IMD_CODE4', edt_IMD_CODE4.Text);

        ADDValue('SIZE1', edt_SIZE.Text);
        ADDValue('SIZE2', edt_SIZE2.Text);
        ADDValue('SIZE3', edt_SIZE3.Text);
        ADDValue('SIZE4', edt_SIZE4.Text);
        ADDValue('SIZE5', edt_SIZE5.Text);
        ADDValue('SIZE6', edt_SIZE6.Text);
        ADDValue('SIZE7', edt_SIZE7.Text);
        ADDValue('SIZE8', edt_SIZE8.Text);
        ADDValue('SIZE9', edt_SIZE9.Text);
        ADDValue('SIZE10', edt_SIZE10.Text);

        //수량 단위
        ADDValue('QTYC', edt_QTYC.Text);
        //수량
        ADDValue('QTY', edt_QTY.Value);
        //수량소계 단위
//        ADDValue('TOTQTYC', edt_TOTQTYC.Text);
        //수량소계
//        ADDValue('TOTQTY', edt_TOTQTY.Value);
        //단가
        ADDValue('PRICE', edt_PRICE.Value);
        //기준수량 단위
        ADDValue('PRICEC', edt_PRICEC.Text);
        //기준수량
        ADDValue('PRICE_G', edt_PRICE_G.Value);
        //공급가액외화 통화단위
        ADDValue('VB_AMTC', edt_VB_AMTC.Text);
        //공급가액외화
        ADDValue('VB_AMT', edt_VB_AMT.Value);
//        //공급가액외화소계 통화단위
//        ADDValue('SUP_TOTAMTC', edt_SUP_TOTAMTC.Text);
//        //공급가액외화소계
//        ADDValue('SUP_TOTAMT', edt_SUP_TOTAMT.Value);
      end;
      2://수정-저장
      begin
        DMLType := dmlUpdate;
        SQLHeader('APPSPC_D2');

        ADDWhere('KEYY',UI_APPSPC_NEW_frm.Maint_No);
        ADDWhere('DOC_GUBUN','1BW');
        ADDWhere('SEQ',UI_APPSPC_NEW_frm.SEQ);
        ADDWhere('LINE_NO',edt_LINE_NO.Text);

        ADDValue('IMD_CODE1', edt_IMD_CODE.Text);
        ADDValue('IMD_CODE2', edt_IMD_CODE2.Text);
        ADDValue('IMD_CODE3', edt_IMD_CODE3.Text);
        ADDValue('IMD_CODE4', edt_IMD_CODE4.Text);

        ADDValue('SIZE1', edt_SIZE.Text);
        ADDValue('SIZE2', edt_SIZE2.Text);
        ADDValue('SIZE3', edt_SIZE3.Text);
        ADDValue('SIZE4', edt_SIZE4.Text);
        ADDValue('SIZE5', edt_SIZE5.Text);
        ADDValue('SIZE6', edt_SIZE6.Text);
        ADDValue('SIZE7', edt_SIZE7.Text);
        ADDValue('SIZE8', edt_SIZE8.Text);
        ADDValue('SIZE9', edt_SIZE9.Text);
        ADDValue('SIZE10', edt_SIZE10.Text);

        //수량 단위
        ADDValue('QTYC', edt_QTYC.Text);
        //수량
        ADDValue('QTY', edt_QTY.Value);
        //수량소계 단위
//        ADDValue('TOTQTYC', edt_TOTQTYC.Text);
        //수량소계
//        ADDValue('TOTQTY', edt_TOTQTY.Value);
        //단가
        ADDValue('PRICE', edt_PRICE.Value);
        //기준수량 단위
        ADDValue('PRICEC', edt_PRICEC.Text);
        //기준수량
        ADDValue('PRICE_G', edt_PRICE_G.Value);
        //공급가액외화 통화단위
        ADDValue('VB_AMTC', edt_VB_AMTC.Text);
        //공급가액외화
        ADDValue('VB_AMT', edt_VB_AMT.Value);
//        //공급가액외화소계 통화단위
//        ADDValue('SUP_TOTAMTC', edt_SUP_TOTAMTC.Text);
//        //공급가액외화소계
//        ADDValue('SUP_TOTAMT', edt_SUP_TOTAMT.Value);
      end;
    end;
  end;

  with TADOQuery.Create(Nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := SQLCreate_D2.CreateSQL;
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;

  with qryListD2 do
  begin
    Close;
    Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
    Parameters[1].Value := '1BW';
    Parameters[2].Value := UI_APPSPC_NEW_frm.SEQ;
    Open;
    qryListD2.Locate('LINE_NO', Line_No, [] ) ;
  end;

  EnableControl(sPanel16, False);
  BtnControl(4);
  ReadWrite_D2 := False;
  
end;

procedure TUI_APPSPC_NEW_DOC3_frm.edt_IMD_CODEExit(Sender: TObject);
begin
  inherited;
  edt_IMD_CODE1.Text := edt_IMD_CODE.Text;
end;

procedure TUI_APPSPC_NEW_DOC3_frm.edt_SIZEExit(Sender: TObject);
begin
  inherited;
  edt_SIZE1.Text := edt_SIZE.Text;
end;

procedure TUI_APPSPC_NEW_DOC3_frm.btnAdd_IMDClick(Sender: TObject);
begin
  inherited;
  
  if btnAdd_IMD.Caption = '+' then
  begin
    btnAdd_IMD.Caption := '-';
    sPanel20.Visible := True;
    edt_IMD_CODE1.SetFocus;
  end
  else if btnAdd_IMD.Caption = '-' then
    sButton1Click(Application);

end;

procedure TUI_APPSPC_NEW_DOC3_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  sPanel20.Visible := False;
  btnAdd_IMD.Caption := '+'

end;

procedure TUI_APPSPC_NEW_DOC3_frm.sButton2Click(Sender: TObject);
begin
  inherited;    
  sPanel13.Visible := False;
  btnAdd_SIZE.Caption := '+';

end;

procedure TUI_APPSPC_NEW_DOC3_frm.btnAdd_SIZEClick(Sender: TObject);
begin
  inherited;       
  if btnAdd_SIZE.Caption = '+' then
  begin
    btnAdd_SIZE.Caption := '-';
    sPanel13.Visible := True;
    edt_SIZE1.SetFocus;
  end
  else if btnAdd_SIZE.Caption = '-' then
    sButton2Click(Self);

end;

procedure TUI_APPSPC_NEW_DOC3_frm.edt_SIZE1Exit(Sender: TObject);
begin
  inherited;
  edt_SIZE.Text := edt_SIZE1.Text;
end;

procedure TUI_APPSPC_NEW_DOC3_frm.edt_IMD_CODE1Exit(Sender: TObject);
begin
  inherited;
  edt_IMD_CODE.Text := edt_IMD_CODE1.Text;
end;

procedure TUI_APPSPC_NEW_DOC3_frm.CODEDblClick(Sender: TObject);
var
  CodeDialog : TCodeDialogParent_frm;
begin
  inherited;
  CodeDialog := nil;
  case TsEdit(Sender).Tag of
    //거래처코드
    103,104: CodeDialog := TCD_CUST_frm.Create(Self);
    //통화단위
    102,108,109: CodeDialog := TCD_AMT_UNIT_frm.Create(Self);
    //수량단위
    101,105,106,107: CodeDialog := TCD_UNIT_frm.Create(Self);
  end;

  try
    if CodeDialog.OpenDialog(0, TsEdit(Sender).Text) then
    begin
      TsEdit(Sender).Text := CodeDialog.FieldValues('Code');
      case TsEdit(Sender).Tag of
        103://공급자
        begin
          edt_SE_SNAME.Text := CodeDialog.FieldValues('ENAME');
          edt_SE_NAME.Text := CodeDialog.FieldValues('REP_NAME');
          edt_SE_ADDR1.Text := CodeDialog.FieldValues('ADDR1');
          edt_SE_ADDR2.Text := CodeDialog.FieldValues('ADDR2');
          edt_SE_ADDR3.Text := CodeDialog.FieldValues('ADDR3');
        end;
        104://공급받는자
        begin
          edt_BY_SNAME.Text := CodeDialog.FieldValues('ENAME');
          edt_BY_NAME.Text := CodeDialog.FieldValues('REP_NAME');
          edt_BY_ADDR1.Text := CodeDialog.FieldValues('ADDR1');
          edt_BY_ADDR2.Text := CodeDialog.FieldValues('ADDR2');
          edt_BY_ADDR3.Text := CodeDialog.FieldValues('ADDR3');
        end;
      end;
    end;

  finally
    FreeAndNil(CodeDialog);
  end;
end;

function TUI_APPSPC_NEW_DOC3_frm.ErrorCheck:String;
var
  ErrMsg:TStringList;
begin
  ErrMsg := TStringList.Create;
  Try
    IF Trim(edt_DOC_NO.Text) = '' THEN
      ErrMsg.Add('[상업송장] 문서번호를 입력하세요');
    IF Trim(msk_ISS_DATE.Text) = '' THEN
      ErrMsg.Add('[상업송장] 발급일자를 입력하세요');
    IF edt_ISS_TOTAMT.Value = 0 THEN
      ErrMsg.Add('[상업송장] 총금액을 입력하세요');
    if Trim(edt_ISS_TOTAMTC.Text) = '' then
      ErrMsg.Add('[상업송장] 총금액 통화단위를 입력하세요');


    IF Trim(edt_SE_SNAME.Text) = '' THEN
      ErrMsg.Add('[공급자] 상호를 입력하세요');
    IF Trim(edt_SE_NAME.Text) = '' THEN
      ErrMsg.Add('[공급자] 대표자명를 입력하세요');
    IF Trim(edt_SE_ADDR1.Text) = '' THEN
      ErrMsg.Add('[공급자] 주소를 입력하세요');

    IF Trim(edt_BY_SNAME.Text) = '' THEN
      ErrMsg.Add('[공급받는자] 상호를 입력하세요');
    IF Trim(edt_BY_NAME.Text) = '' THEN
      ErrMsg.Add('[공급받는자] 대표자명을 입력하세요');
    IF Trim(edt_BY_ADDR1.Text) = '' THEN
      ErrMsg.Add('[공급받는자] 주소를 입력하세요');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;

end;

end.
