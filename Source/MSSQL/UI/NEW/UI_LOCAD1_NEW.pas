unit UI_LOCAD1_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_LOCADV_NEW, DB, ADODB, sSkinProvider, StdCtrls, sMemo,
  sComboBox, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sCheckBox, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel,
  QuickRpt, QRCtrls;

type
  TUI_LOCAD1_NEW_frm = class(TUI_LOCADV_NEW_frm)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure ReadData; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_LOCAD1_NEW_frm: TUI_LOCAD1_NEW_frm;

implementation

uses ChildForm;

{$R *.dfm}

procedure TUI_LOCAD1_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  inherited;
  Action := caFree;
  UI_LOCAD1_NEW_frm := nil;
end;

procedure TUI_LOCAD1_NEW_frm.ReadData;
begin
  inherited;
  CM_INDEX(com_func, [':'], 0, qryLOCADVMESSAGE1.AsString, 0);
  CM_INDEX(com_type, [':'], 0, qryLOCADVMESSAGE2.AsString, 2);
end;

procedure TUI_LOCAD1_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadOnlyControl(spanel7,false);
  ReadOnlyControl(spanel27,false);

end;

end.
