inherited UI_LOCADV_NEW_frm: TUI_LOCADV_NEW_frm
  Left = 518
  Top = 221
  Caption = '[LODADV] '#45236#44397#49888#50857#51109' '#44060#49444#51025#45813#49436
  PixelsPerInch = 96
  TextHeight = 12
  inherited btn_Panel: TsPanel
    inherited sSpeedButton4: TsSpeedButton
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Visible = False
    end
    inherited sLabel7: TsLabel
      Caption = #45236#44397#49888#50857#51109' '#44060#49444#51025#45813#49436
    end
    inherited sSpeedButton6: TsSpeedButton
      Visible = False
    end
    inherited sSpeedButton7: TsSpeedButton
      Visible = False
    end
    inherited sSpeedButton2: TsSpeedButton
      Visible = False
    end
    inherited sLabel5: TsLabel
      Width = 76
      Caption = '(LOCADV)'
    end
    inherited btnNew: TsButton
      Left = 203
      Top = 65
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 179
      Top = 65
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 319
    end
    inherited btnCopy: TsButton
      Visible = False
    end
    inherited btnPrint: TsButton
      Left = 385
    end
    inherited btnTemp: TsButton
      Visible = False
    end
    inherited btnSave: TsButton
      Visible = False
    end
    inherited btnCancel: TsButton
      Visible = False
    end
    inherited btnReady: TsButton
      Visible = False
    end
    inherited btnSend: TsButton
      Visible = False
    end
    inherited sPanel6: TsPanel
      inherited sSpeedButton1: TsSpeedButton
        Left = 266
      end
      inherited edt_MAINT_NO_Header: TsEdit
        Left = 74
        Enabled = False
      end
      inherited msk_Datee: TsMaskEdit
        Left = 405
        Enabled = False
      end
      inherited com_func: TsComboBox
        Left = 659
        Width = 41
        Enabled = False
      end
      inherited com_type: TsComboBox
        Enabled = False
      end
      inherited edt_userno: TsEdit
        Enabled = False
      end
      inherited edt_MAINT_NO_Serial: TsEdit
        Left = 250
        Enabled = False
      end
      inherited edt_MAINT_NO_Bank: TsEdit
        Left = 218
        Enabled = False
      end
    end
    inherited QRShape1: TQRShape
      Size.Values = (
        2.64583333333333
        809.625
        105.833333333333
        2129.89583333333)
    end
  end
  inherited sPanel4: TsPanel
    inherited sDBGrid1: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 202
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 102
          Visible = True
        end>
    end
    inherited sPanel29: TsPanel
      Left = 288
      Top = 584
      Width = 92
      Height = 47
      Align = alNone
    end
  end
  inherited sPanel3: TsPanel
    inherited sPageControl1: TsPageControl
      ActivePage = sTabSheet3
      TabIndex = 1
      inherited sTabSheet1: TsTabSheet
        inherited sPanel7: TsPanel
          inherited sLabel3: TsLabel
            Left = 491
            Enabled = False
          end
          inherited edt_wcd: TsEdit
            Top = 28
            Visible = False
          end
          inherited edt_wsangho: TsEdit
            Enabled = False
          end
          inherited edt_wceo: TsEdit
            Enabled = False
          end
          inherited edt_waddr1: TsEdit
            Enabled = False
          end
          inherited edt_waddr2: TsEdit
            Enabled = False
          end
          inherited edt_waddr3: TsEdit
            Enabled = False
          end
          inherited edt_rcd: TsEdit
            Top = 174
            Visible = False
          end
          inherited edt_rsaupno: TsEdit
            Enabled = False
          end
          inherited edt_rsangho: TsEdit
            Enabled = False
          end
          inherited edt_rceo: TsEdit
            Enabled = False
          end
          inherited edt_remail: TsEdit
            Enabled = False
          end
          inherited edt_raddr1: TsEdit
            Enabled = False
          end
          inherited edt_raddr2: TsEdit
            Enabled = False
          end
          inherited edt_raddr3: TsEdit
            Enabled = False
          end
          inherited edt_iden1: TsEdit
            Left = 91
            Width = 265
            Enabled = False
            BoundLabel.Caption = #49885#48324#51088
          end
          inherited edt_iden2: TsEdit
            Left = -157
            Top = 198
            Visible = False
          end
          inherited edt_doc1: TsEdit
            Enabled = False
          end
          inherited edt_doc2: TsEdit
            Enabled = False
          end
          inherited edt_doc3: TsEdit
            Enabled = False
          end
          inherited edt_doc4: TsEdit
            Enabled = False
          end
          inherited edt_doc5: TsEdit
            Enabled = False
          end
          inherited edt_wsaupno: TsEdit
            Enabled = False
          end
          inherited edt_cbank: TsEdit
            Top = 345
            Enabled = False
          end
          inherited edt_cbanknm: TsEdit
            Top = 345
            Enabled = False
            ReadOnly = False
          end
          inherited edt_cbrunch: TsEdit
            Top = 369
            Enabled = False
          end
          inherited edt_cType: TsEdit
            Top = 393
            Enabled = False
          end
          inherited sEdit8: TsEdit
            Top = 393
            Enabled = False
            ReadOnly = False
          end
          inherited edt_LOCAMT_UNIT: TsEdit
            Top = 417
            Enabled = False
            BoundLabel.Caption = #44060#49444#44552#50529'('#50808#54868')'
          end
          inherited edt_perr: TsEdit
            Width = 34
            Enabled = False
          end
          inherited cur_LOCAMT: TsCurrencyEdit
            Top = 417
            Enabled = False
          end
          inherited edt_merr: TsEdit
            Left = 144
            Width = 34
            Enabled = False
          end
          inherited edt_cyong: TsEdit
            Enabled = False
          end
          inherited edt_lcno: TsEdit
            Left = 458
            Top = 342
            Width = 247
            Enabled = False
            ReadOnly = False
          end
          inherited edt_doc6: TsEdit
            Enabled = False
          end
          inherited edt_doc7: TsEdit
            Enabled = False
          end
          inherited edt_doc8: TsEdit
            Enabled = False
          end
          inherited edt_doc9: TsEdit
            Enabled = False
          end
          inherited cur_AttachDoc1: TsCurrencyEdit
            Enabled = False
          end
          inherited cur_AttachDoc4: TsCurrencyEdit
            Enabled = False
          end
          inherited cur_AttachDoc5: TsCurrencyEdit
            Enabled = False
          end
          inherited cur_AttachDoc2: TsCurrencyEdit
            Enabled = False
          end
          inherited cur_AttachDoc3: TsCurrencyEdit
            Enabled = False
          end
          inherited cur_chasu: TsCurrencyEdit
            Left = 457
            Enabled = False
          end
          inherited msk_CrtDT: TsMaskEdit
            Left = 457
            Enabled = False
          end
          inherited msk_IndoDt: TsMaskEdit
            Left = 625
            Top = 292
            Enabled = False
          end
          inherited msk_ExpiryDT: TsMaskEdit
            Left = 625
            Top = 316
            Enabled = False
          end
          inherited edt_isPart: TsEdit
            Left = 457
            Top = 368
            Enabled = False
          end
          inherited sEdit20: TsEdit
            Left = 491
            Top = 368
            Width = 214
            Enabled = False
            ReadOnly = False
          end
          inherited comDocPeriod: TsComboBox
            Enabled = False
          end
          inherited edt_cyongnm: TsEdit
            Enabled = False
            ReadOnly = False
          end
          object sPanel9: TsPanel
            Left = 8
            Top = 4
            Width = 137
            Height = 23
            SkinData.CustomColor = True
            Caption = #44060#49444#51032#47280#51064
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 53
          end
          object sPanel10: TsPanel
            Left = 8
            Top = 150
            Width = 137
            Height = 23
            SkinData.CustomColor = True
            Caption = #49688#54812#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 54
          end
          object sEdit2: TsEdit
            Tag = 1002
            Left = 91
            Top = 441
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = clBtnFace
            Enabled = False
            TabOrder = 55
            Text = 'KRW'
            OnDblClick = edt_cbankDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44060#49444#44552#50529'('#50896#54868')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
          end
          object cur_LOCAMT_W: TsCurrencyEdit
            Left = 141
            Top = 441
            Width = 215
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            Enabled = False
            TabOrder = 56
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object sEdit3: TsEdit
            Left = 256
            Top = 465
            Width = 100
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 10
            TabOrder = 57
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #47588#47588#44592#51456#50984
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object msk_issdate: TsMaskEdit
            Left = 457
            Top = 316
            Width = 81
            Height = 23
            AutoSize = False
            Color = 12582911
            Enabled = False
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 58
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #53685#51648#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            SkinData.CustomColor = True
          end
        end
      end
      inherited sTabSheet3: TsTabSheet
        inherited sPanel27: TsPanel
          inherited msk_hscd: TsMaskEdit
            Enabled = False
          end
          inherited memo_goods: TsMemo
            Enabled = False
          end
          inherited memo_etcdoc: TsMemo
            Enabled = False
          end
          inherited memo_etcinfo: TsMemo
            Enabled = False
          end
          inherited edt_OriginType: TsEdit
            Enabled = False
          end
          inherited sEdit22: TsEdit
            Enabled = False
          end
          inherited edt_OriginLCNO: TsEdit
            Enabled = False
          end
          inherited edt_OriginAmtUnit: TsEdit
            Enabled = False
          end
          inherited edt_OriginAmt: TsCurrencyEdit
            Enabled = False
          end
          inherited msk_OriginShipDT: TsMaskEdit
            Enabled = False
          end
          inherited msk_OriginVaildDT: TsMaskEdit
            Enabled = False
          end
          inherited edt_AmtType: TsEdit
            Enabled = False
          end
          inherited sEdit26: TsEdit
            Enabled = False
          end
          inherited edt_OriginCD: TsEdit
            Enabled = False
          end
          inherited edt_OriginSangho: TsEdit
            Enabled = False
          end
          inherited edt_OriginCeo: TsEdit
            Enabled = False
          end
          inherited edt_OriginAddr1: TsEdit
            Enabled = False
          end
          inherited edt_OriginExportArea: TsEdit
            Enabled = False
          end
          inherited sEdit32: TsEdit
            Enabled = False
          end
          inherited edt_Originbank1: TsEdit
            Enabled = False
          end
          inherited edt_Originbank2: TsEdit
            Enabled = False
          end
          inherited memo_ExportGoods: TsMemo
            Enabled = False
          end
          inherited edt_sign1: TsEdit
            Enabled = False
          end
          inherited edt_sign2: TsEdit
            Enabled = False
          end
          inherited edt_sign3: TsEdit
            Enabled = False
          end
        end
      end
      inherited sTabSheet4: TsTabSheet
        inherited sDBGrid3: TsDBGrid
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Caption = #44288#47532#48264#54840
              Width = 200
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'BENEFC1'
              Title.Alignment = taCenter
              Title.Caption = #49688#54812#51088
              Width = 172
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'AP_BANK1'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51008#54665
              Width = 164
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'APP_DATE'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#51068#51088
              Width = 82
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'LOC_AMT'
              Title.Alignment = taCenter
              Title.Caption = #44060#49444#44552#50529
              Width = 97
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'LOC_AMTC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 38
              Visible = True
            end>
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 48
  end
  inherited qryList: TADOQuery
    AfterOpen = nil
    AfterScroll = nil
    Top = 600
  end
  inherited dsList: TDataSource
    DataSet = qryLOCADV
    Left = 48
    Top = 160
  end
  inherited qryReady: TADOQuery
    Top = 632
  end
  object qryLOCADV: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryLOCADVAfterOpen
    AfterScroll = qryLOCADVAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180718'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, CHK1, CHK2, CHK3, USER_ID, DATEE, BGM_REF, MESS' +
        'AGE1, MESSAGE2, BUSINESS, RFF_NO, LC_NO, OFFERNO1, OFFERNO2, OFF' +
        'ERNO3, OFFERNO4, OFFERNO5, OFFERNO6, OFFERNO7, OFFERNO8, OFFERNO' +
        '9, OPEN_NO, ADV_DATE, ISS_DATE, DOC_PRD, DELIVERY, EXPIRY, TRANS' +
        'PRT, GOODDES, GOODDES1, REMARK, REMARK1, AP_BANK, AP_BANK1, AP_B' +
        'ANK2, APPLIC1, APPLIC2, APPLIC3, BENEFC1, BENEFC2, BENEFC3, EXNA' +
        'ME1, EXNAME2, EXNAME3, DOCCOPY1, DOCCOPY2, DOCCOPY3, DOCCOPY4, D' +
        'OCCOPY5, DOC_ETC, DOC_ETC1, LOC_TYPE, LOC1AMT, LOC1AMTC, LOC2AMT' +
        ', LOC2AMTC, EX_RATE, DOC_DTL, DOC_NO, DOC_AMT, DOC_AMTC, LOADDAT' +
        'E, EXPDATE, IM_NAME, IM_NAME1, IM_NAME2, IM_NAME3, DEST, ISBANK1' +
        ', ISBANK2, PAYMENT, EXGOOD, EXGOOD1, PRNO, BSN_HSCODE, APPADDR1,' +
        ' APPADDR2, APPADDR3, BNFADDR1, BNFADDR2, BNFADDR3, BNFEMAILID, B' +
        'NFDOMAIN, CD_PERP, CD_PERM'
      '      ,N4025.DOC_NAME as BUSINESSNAME'
      '      ,PSHIP.DOC_NAME as TRANSPRTNAME'
      '      ,N4487.DOC_NAME as LOC_TYPENAME'
      '      ,N1001.DOC_NAME as DOC_DTLNAME '
      '      ,N4277.DOC_NAME as PAYMENTNAME'
      '      ,NAT.DOC_NAME as DESTNAME'
      
        'FROM LOCADV LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2ND' +
        'D with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4487'#39') N4487 ON LOCADV.LOC_TYP' +
        'E = N4487.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON L' +
        'OCADV.BUSINESS = N4025.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39'PSHIP'#39') PSHIP ON LOC' +
        'ADV.TRANSPRT = PSHIP.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'4277'#39') N4277 ON L' +
        'OCADV.PAYMENT = N4277.CODE'
      
        '                         LEFT JOIN (SELECT CODE,NAME as DOC_NAME' +
        ' FROM CODE2NDD with(nolock)'#9'WHERE Prefix = '#39#45236#44397#49888'1001'#39') N1001 ON L' +
        'OCADV.DOC_DTL = N1001.CODE'#9
      
        #9'         LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD ' +
        'with(nolock)'#9'WHERE Prefix = '#39#44397#44032#39') NAT ON LOCADV.DEST = NAT.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(LOCADV.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 16
    Top = 160
    object qryLOCADVMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryLOCADVCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryLOCADVCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryLOCADVCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryLOCADVUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryLOCADVDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryLOCADVBGM_REF: TStringField
      FieldName = 'BGM_REF'
      Size = 35
    end
    object qryLOCADVMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryLOCADVMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryLOCADVBUSINESS: TStringField
      FieldName = 'BUSINESS'
      Size = 3
    end
    object qryLOCADVRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryLOCADVLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryLOCADVOFFERNO1: TStringField
      FieldName = 'OFFERNO1'
      Size = 35
    end
    object qryLOCADVOFFERNO2: TStringField
      FieldName = 'OFFERNO2'
      Size = 35
    end
    object qryLOCADVOFFERNO3: TStringField
      FieldName = 'OFFERNO3'
      Size = 35
    end
    object qryLOCADVOFFERNO4: TStringField
      FieldName = 'OFFERNO4'
      Size = 35
    end
    object qryLOCADVOFFERNO5: TStringField
      FieldName = 'OFFERNO5'
      Size = 35
    end
    object qryLOCADVOFFERNO6: TStringField
      FieldName = 'OFFERNO6'
      Size = 35
    end
    object qryLOCADVOFFERNO7: TStringField
      FieldName = 'OFFERNO7'
      Size = 35
    end
    object qryLOCADVOFFERNO8: TStringField
      FieldName = 'OFFERNO8'
      Size = 35
    end
    object qryLOCADVOFFERNO9: TStringField
      FieldName = 'OFFERNO9'
      Size = 35
    end
    object qryLOCADVOPEN_NO: TBCDField
      FieldName = 'OPEN_NO'
      Precision = 18
    end
    object qryLOCADVADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      Size = 8
    end
    object qryLOCADVISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryLOCADVDOC_PRD: TBCDField
      FieldName = 'DOC_PRD'
      Precision = 18
    end
    object qryLOCADVDELIVERY: TStringField
      FieldName = 'DELIVERY'
      Size = 8
    end
    object qryLOCADVEXPIRY: TStringField
      FieldName = 'EXPIRY'
      Size = 8
    end
    object qryLOCADVTRANSPRT: TStringField
      FieldName = 'TRANSPRT'
      Size = 3
    end
    object qryLOCADVGOODDES: TStringField
      FieldName = 'GOODDES'
      Size = 1
    end
    object qryLOCADVGOODDES1: TMemoField
      FieldName = 'GOODDES1'
      BlobType = ftMemo
    end
    object qryLOCADVREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryLOCADVREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryLOCADVAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryLOCADVAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryLOCADVAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryLOCADVAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryLOCADVAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryLOCADVAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryLOCADVBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryLOCADVBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryLOCADVBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryLOCADVEXNAME1: TStringField
      FieldName = 'EXNAME1'
      Size = 35
    end
    object qryLOCADVEXNAME2: TStringField
      FieldName = 'EXNAME2'
      Size = 35
    end
    object qryLOCADVEXNAME3: TStringField
      FieldName = 'EXNAME3'
      Size = 35
    end
    object qryLOCADVDOCCOPY1: TBCDField
      FieldName = 'DOCCOPY1'
      Precision = 18
    end
    object qryLOCADVDOCCOPY2: TBCDField
      FieldName = 'DOCCOPY2'
      Precision = 18
    end
    object qryLOCADVDOCCOPY3: TBCDField
      FieldName = 'DOCCOPY3'
      Precision = 18
    end
    object qryLOCADVDOCCOPY4: TBCDField
      FieldName = 'DOCCOPY4'
      Precision = 18
    end
    object qryLOCADVDOCCOPY5: TBCDField
      FieldName = 'DOCCOPY5'
      Precision = 18
    end
    object qryLOCADVDOC_ETC: TStringField
      FieldName = 'DOC_ETC'
      Size = 1
    end
    object qryLOCADVDOC_ETC1: TMemoField
      FieldName = 'DOC_ETC1'
      BlobType = ftMemo
    end
    object qryLOCADVLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryLOCADVLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qryLOCADVLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 19
    end
    object qryLOCADVLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qryLOCADVLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 19
    end
    object qryLOCADVEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qryLOCADVDOC_DTL: TStringField
      FieldName = 'DOC_DTL'
      Size = 3
    end
    object qryLOCADVDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryLOCADVDOC_AMT: TBCDField
      FieldName = 'DOC_AMT'
      Precision = 18
    end
    object qryLOCADVDOC_AMTC: TStringField
      FieldName = 'DOC_AMTC'
      Size = 3
    end
    object qryLOCADVLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qryLOCADVEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qryLOCADVIM_NAME: TStringField
      FieldName = 'IM_NAME'
      Size = 10
    end
    object qryLOCADVIM_NAME1: TStringField
      FieldName = 'IM_NAME1'
      Size = 35
    end
    object qryLOCADVIM_NAME2: TStringField
      FieldName = 'IM_NAME2'
      Size = 35
    end
    object qryLOCADVIM_NAME3: TStringField
      FieldName = 'IM_NAME3'
      Size = 35
    end
    object qryLOCADVDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryLOCADVISBANK1: TStringField
      FieldName = 'ISBANK1'
      Size = 35
    end
    object qryLOCADVISBANK2: TStringField
      FieldName = 'ISBANK2'
      Size = 35
    end
    object qryLOCADVPAYMENT: TStringField
      FieldName = 'PAYMENT'
      Size = 3
    end
    object qryLOCADVEXGOOD: TStringField
      FieldName = 'EXGOOD'
      Size = 1
    end
    object qryLOCADVEXGOOD1: TMemoField
      FieldName = 'EXGOOD1'
      BlobType = ftMemo
    end
    object qryLOCADVPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryLOCADVBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryLOCADVAPPADDR1: TStringField
      FieldName = 'APPADDR1'
      Size = 35
    end
    object qryLOCADVAPPADDR2: TStringField
      FieldName = 'APPADDR2'
      Size = 35
    end
    object qryLOCADVAPPADDR3: TStringField
      FieldName = 'APPADDR3'
      Size = 35
    end
    object qryLOCADVBNFADDR1: TStringField
      FieldName = 'BNFADDR1'
      Size = 35
    end
    object qryLOCADVBNFADDR2: TStringField
      FieldName = 'BNFADDR2'
      Size = 35
    end
    object qryLOCADVBNFADDR3: TStringField
      FieldName = 'BNFADDR3'
      Size = 35
    end
    object qryLOCADVBNFEMAILID: TStringField
      FieldName = 'BNFEMAILID'
      Size = 35
    end
    object qryLOCADVBNFDOMAIN: TStringField
      FieldName = 'BNFDOMAIN'
      Size = 35
    end
    object qryLOCADVCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryLOCADVCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryLOCADVBUSINESSNAME: TStringField
      FieldName = 'BUSINESSNAME'
      Size = 100
    end
    object qryLOCADVTRANSPRTNAME: TStringField
      FieldName = 'TRANSPRTNAME'
      Size = 100
    end
    object qryLOCADVLOC_TYPENAME: TStringField
      FieldName = 'LOC_TYPENAME'
      Size = 100
    end
    object qryLOCADVDOC_DTLNAME: TStringField
      FieldName = 'DOC_DTLNAME'
      Size = 100
    end
    object qryLOCADVPAYMENTNAME: TStringField
      FieldName = 'PAYMENTNAME'
      Size = 100
    end
    object qryLOCADVDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
  end
end
