inherited UI_BANKCODE_SWIFT_frm: TUI_BANKCODE_SWIFT_frm
  Left = 479
  Top = 237
  BorderWidth = 4
  Caption = #54644#50808#51008#54665#53076#46300
  ClientHeight = 380
  ClientWidth = 808
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel2: TsPanel [0]
    Left = 0
    Top = 0
    Width = 808
    Height = 46
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alTop
    BorderWidth = 1
    DoubleBuffered = False
    TabOrder = 0
    object sPanel3: TsPanel
      Left = 2
      Top = 2
      Width = 804
      Height = 42
      Align = alClient
      DoubleBuffered = False
      TabOrder = 0
      DesignSize = (
        804
        42)
      object sSpeedButton6: TsSpeedButton
        Left = 218
        Top = 4
        Width = 8
        Height = 35
        ButtonStyle = tbsDivider
      end
      object btnNew: TsButton
        Left = 5
        Top = 4
        Width = 70
        Height = 35
        Cursor = crHandPoint
        Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
        Caption = #49888#44508
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = btnNewClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 32
        ContentMargin = 8
      end
      object btnEdit: TsButton
        Tag = 1
        Left = 76
        Top = 4
        Width = 70
        Height = 35
        Cursor = crHandPoint
        Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
        Caption = #49688#51221
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = False
        OnClick = btnEditClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 3
        ContentMargin = 8
      end
      object btnDel: TsButton
        Tag = 2
        Left = 147
        Top = 4
        Width = 70
        Height = 35
        Cursor = crHandPoint
        Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
        Caption = #49325#51228
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        TabStop = False
        OnClick = btnDelClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 27
        ContentMargin = 8
      end
      object btnExit: TsButton
        Left = 722
        Top = 3
        Width = 78
        Height = 35
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #45803#44592
        TabOrder = 3
        TabStop = False
        OnClick = btnExitClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 20
        ContentMargin = 12
      end
    end
  end
  object sPanel5: TsPanel [1]
    Left = 0
    Top = 46
    Width = 808
    Height = 334
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    DoubleBuffered = False
    TabOrder = 1
    object sPanel6: TsPanel
      Left = 1
      Top = 34
      Width = 806
      Height = 2
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alTop
      DoubleBuffered = False
      TabOrder = 0
    end
    object sPanel8: TsPanel
      Left = 1
      Top = 1
      Width = 806
      Height = 33
      Align = alTop
      DoubleBuffered = False
      TabOrder = 1
      object edt_FindText: TsEdit
        Left = 106
        Top = 5
        Width = 141
        Height = 23
        TabOrder = 0
      end
      object sComboBox1: TsComboBox
        Left = 8
        Top = 5
        Width = 97
        Height = 23
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 1
        Text = 'SWIFT CODE'
        Items.Strings = (
          'SWIFT CODE'
          #51008#54665#47749)
      end
      object sButton1: TsButton
        Left = 248
        Top = 5
        Width = 58
        Height = 23
        Caption = #51312#54924
        TabOrder = 2
        OnClick = sButton1Click
      end
    end
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 36
      Width = 806
      Height = 297
      Align = alClient
      Color = clWhite
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 2
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDblClick = sDBGrid1DblClick
      DefaultRowHeight = 19
      Columns = <
        item
          Alignment = taCenter
          Color = clBtnFace
          Expanded = False
          FieldName = 'BANK_CD'
          Title.Alignment = taCenter
          Title.Caption = #51008#54665#53076#46300
          Width = 114
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BANK_NM1'
          Title.Caption = #51008#54665#47749
          Width = 162
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'BANK_BNCH_NM1'
          Title.Alignment = taCenter
          Title.Caption = #51648#51216#47749
          Width = 71
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'BANK_NAT_CD'
          Title.Alignment = taCenter
          Title.Caption = #44397#44032
          Width = 50
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'BANK_UNIT'
          Title.Alignment = taCenter
          Title.Caption = #53685#54868
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ACCT_NM'
          Title.Caption = #49688#52712#51064
          Width = 150
          Visible = True
        end>
    end
  end
  object pan_input: TsPanel [2]
    Left = 209
    Top = 102
    Width = 389
    Height = 267
    DoubleBuffered = False
    TabOrder = 2
    Visible = False
    object edt_code: TsEdit
      Left = 104
      Top = 40
      Width = 145
      Height = 23
      Color = 12582911
      MaxLength = 11
      TabOrder = 0
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = 'SWIFT CODE'
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = [fsBold]
      BoundLabel.ParentFont = False
    end
    object edt_banknm1: TsEdit
      Left = 104
      Top = 64
      Width = 241
      Height = 23
      Color = 12582911
      MaxLength = 35
      TabOrder = 2
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = #51008#54665#47749
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = [fsBold]
      BoundLabel.ParentFont = False
    end
    object edt_banknm2: TsEdit
      Left = 104
      Top = 88
      Width = 241
      Height = 23
      MaxLength = 35
      TabOrder = 3
      BoundLabel.Caption = #51008#54665#47749
    end
    object edt_branchnm1: TsEdit
      Left = 104
      Top = 112
      Width = 241
      Height = 23
      Color = clWhite
      MaxLength = 35
      TabOrder = 4
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = #51648#51216#47749
    end
    object edt_branchnm2: TsEdit
      Left = 104
      Top = 136
      Width = 241
      Height = 23
      MaxLength = 35
      TabOrder = 5
      BoundLabel.Caption = #51008#54665#47749
    end
    object edt_nat: TsEdit
      Left = 312
      Top = 40
      Width = 33
      Height = 23
      Color = 12582911
      MaxLength = 2
      TabOrder = 1
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = #44397#44032#53076#46300
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = [fsBold]
      BoundLabel.ParentFont = False
    end
    object edt_unit: TsEdit
      Left = 104
      Top = 160
      Width = 41
      Height = 23
      Color = 12582911
      MaxLength = 3
      TabOrder = 6
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = #53685#54868#53076#46300
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = [fsBold]
      BoundLabel.ParentFont = False
    end
    object edt_accnm: TsEdit
      Left = 104
      Top = 184
      Width = 241
      Height = 23
      Color = 12582911
      MaxLength = 35
      TabOrder = 7
      SkinData.CustomColor = True
      BoundLabel.Active = True
      BoundLabel.Caption = #49688#52712#51064
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = [fsBold]
      BoundLabel.ParentFont = False
    end
    object TsPanel
      Left = 1
      Top = 1
      Width = 387
      Height = 24
      SkinData.SkinSection = 'DRAGBAR'
      Align = alTop
      DoubleBuffered = False
      TabOrder = 8
      object sLabel1: TsLabel
        Left = 8
        Top = 5
        Width = 89
        Height = 15
        AutoSize = False
        Caption = #51008#54665#53076#46300' '#44288#47532
      end
    end
    object sButton2: TsButton
      Left = 192
      Top = 216
      Width = 75
      Height = 33
      Caption = #51200#51109
      TabOrder = 9
      TabStop = False
      OnClick = sButton2Click
      Images = DMICON.System26
      ImageIndex = 0
    end
    object sButton3: TsButton
      Left = 272
      Top = 216
      Width = 75
      Height = 33
      Caption = #52712#49548
      TabOrder = 10
      TabStop = False
      OnClick = sButton3Click
      Images = DMICON.System26
      ImageIndex = 18
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 352
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [BANK_CD]'
      '      ,[BANK_NM1]'
      '      ,[BANK_NM2]'
      '      ,[BANK_BNCH_NM1]'
      '      ,[BANK_BNCH_NM2]'
      '      ,[ACCT_NM]'
      '      ,[BANK_NAT_CD]'
      '      ,[IS_USE]'
      '      ,[BANK_UNIT]'
      '  FROM BANKCODE_SWIFT')
    Left = 24
    Top = 144
    object qryListBANK_CD: TStringField
      FieldName = 'BANK_CD'
      Size = 11
    end
    object qryListBANK_NM1: TStringField
      FieldName = 'BANK_NM1'
      Size = 35
    end
    object qryListBANK_NM2: TStringField
      FieldName = 'BANK_NM2'
      Size = 35
    end
    object qryListBANK_BNCH_NM1: TStringField
      FieldName = 'BANK_BNCH_NM1'
      Size = 35
    end
    object qryListBANK_BNCH_NM2: TStringField
      FieldName = 'BANK_BNCH_NM2'
      Size = 35
    end
    object qryListACCT_NM: TStringField
      FieldName = 'ACCT_NM'
      Size = 35
    end
    object qryListBANK_NAT_CD: TStringField
      FieldName = 'BANK_NAT_CD'
      Size = 2
    end
    object qryListIS_USE: TBooleanField
      FieldName = 'IS_USE'
    end
    object qryListBANK_UNIT: TStringField
      FieldName = 'BANK_UNIT'
      Size = 3
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 144
  end
end
