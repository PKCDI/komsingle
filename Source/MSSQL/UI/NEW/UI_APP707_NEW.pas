unit UI_APP707_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sCheckBox, sMemo, sComboBox, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl, Buttons, sBitBtn, Mask,
  sMaskEdit, sEdit, Grids, DBGrids, acDBGrid, sButton, sLabel, sSpeedButton,
  ExtCtrls, sPanel, sSkinProvider, DB, ADODB, StrUtils, DateUtils, TypeDefine,
  Clipbrd, Menus;

type
  TUI_APP707_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPage1: TsPanel;
    sPanel2: TsPanel;
    sTabSheet3: TsTabSheet;
    sPage2: TsPanel;
    sTabSheet2: TsTabSheet;
    sPage3: TsPanel;
    sPanel43: TsPanel;
    sPanel44: TsPanel;
    sPanel45: TsPanel;
    msk_lstDate: TsMaskEdit;
    sPanel48: TsPanel;
    sPanel49: TsPanel;
    sPanel50: TsPanel;
    sPanel51: TsPanel;
    sPanel52: TsPanel;
    sPanel53: TsPanel;
    edt_SunjukPort: TsEdit;
    sPanel54: TsPanel;
    sPanel55: TsPanel;
    edt_dochackPort: TsEdit;
    sPanel56: TsPanel;
    sPanel57: TsPanel;
    sPanel58: TsPanel;
    edt_loadOn: TsEdit;
    edt_forTran: TsEdit;
    com_Pship: TsComboBox;
    com_TShip: TsComboBox;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    sPanel10: TsPanel;
    msk_IssDate: TsMaskEdit;
    sPanel11: TsPanel;
    sPanel12: TsPanel;
    edt_APPDATE: TsMaskEdit;
    edt_In_Mathod1: TsEdit;
    edt_In_Mathod: TsEdit;
    sPanel8: TsPanel;
    edt_ApBank: TsEdit;
    edt_ApBank1: TsEdit;
    edt_ApBank2: TsEdit;
    edt_ApBank3: TsEdit;
    edt_ApBank4: TsEdit;
    edt_ApBank5: TsEdit;
    sPanel9: TsPanel;
    sPanel59: TsPanel;
    sPanel89: TsPanel;
    sPanel90: TsPanel;
    curr_AMD_NO: TsCurrencyEdit;
    sPanel91: TsPanel;
    sPanel92: TsPanel;
    edt_AdBank1: TsEdit;
    edt_AdBank2: TsEdit;
    edt_AdBank3: TsEdit;
    edt_AdBank4: TsEdit;
    sPanel95: TsPanel;
    edt_AdBank: TsEdit;
    sPanel94: TsPanel;
    sPanel96: TsPanel;
    com_isCancel: TsComboBox;
    sPanel97: TsPanel;
    sPanel98: TsPanel;
    edt_ImpCd1: TsEdit;
    edt_ImpCd1_1: TsEdit;
    sPanel99: TsPanel;
    edt_ILno1: TsEdit;
    sPanel100: TsPanel;
    edt_ILCur1: TsEdit;
    sPanel101: TsPanel;
    curr_ILAMT1: TsCurrencyEdit;
    edt_ImpCd2: TsEdit;
    edt_ImpCd1_2: TsEdit;
    edt_ILno2: TsEdit;
    edt_ILCur2: TsEdit;
    curr_ILAMT2: TsCurrencyEdit;
    edt_ImpCd3: TsEdit;
    edt_ImpCd1_3: TsEdit;
    edt_ILno3: TsEdit;
    edt_ILCur3: TsEdit;
    curr_ILAMT3: TsCurrencyEdit;
    edt_ImpCd4: TsEdit;
    edt_ImpCd1_4: TsEdit;
    edt_ILno4: TsEdit;
    edt_ILCur4: TsEdit;
    curr_ILAMT4: TsCurrencyEdit;
    edt_ImpCd5: TsEdit;
    edt_ImpCd1_5: TsEdit;
    edt_ILno5: TsEdit;
    edt_ILCur5: TsEdit;
    curr_ILAMT5: TsCurrencyEdit;
    sPanel102: TsPanel;
    sPanel103: TsPanel;
    edt_AdInfo1: TsEdit;
    edt_AdInfo2: TsEdit;
    edt_AdInfo3: TsEdit;
    edt_AdInfo4: TsEdit;
    edt_AdInfo5: TsEdit;
    sPanel1: TsPanel;
    sPanel13: TsPanel;
    edt_cdNo: TsEdit;
    sPanel14: TsPanel;
    edt_Applic1: TsEdit;
    edt_Applic2: TsEdit;
    edt_Applic3: TsEdit;
    edt_Applic4: TsEdit;
    sPanel16: TsPanel;
    sPanel15: TsPanel;
    edt_Benefc1: TsEdit;
    edt_Benefc2: TsEdit;
    edt_Benefc3: TsEdit;
    edt_Benefc4: TsEdit;
    sPanel17: TsPanel;
    edt_Benefc5: TsEdit;
    sPanel18: TsPanel;
    sPanel19: TsPanel;
    sPanel21: TsPanel;
    sPanel22: TsPanel;
    sPanel23: TsPanel;
    sPanel25: TsPanel;
    edt_CdPerp: TsCurrencyEdit;
    edt_CdPerm: TsCurrencyEdit;
    sLabel3: TsLabel;
    sPanel88: TsPanel;
    sPanel62: TsPanel;
    edt_IncdCur: TsEdit;
    edt_IncdAmt: TsCurrencyEdit;
    edt_DecdCur: TsEdit;
    edt_DecdAmt: TsCurrencyEdit;
    sTabSheet5: TsTabSheet;
    sPage4: TsPanel;
    sPanel71: TsPanel;
    sPanel72: TsPanel;
    edt_DRAFT1: TsEdit;
    edt_DRAFT2: TsEdit;
    edt_DRAFT3: TsEdit;
    sPanel73: TsPanel;
    edt_MIX1: TsEdit;
    edt_MIX2: TsEdit;
    edt_MIX3: TsEdit;
    edt_MIX4: TsEdit;
    sPanel74: TsPanel;
    edt_DEFPAY1: TsEdit;
    edt_DEFPAY2: TsEdit;
    edt_DEFPAY3: TsEdit;
    edt_DEFPAY4: TsEdit;
    sPanel75: TsPanel;
    sPanel76: TsPanel;
    sPanel36: TsPanel;
    sPanel37: TsPanel;
    edt_aaCcv1: TsEdit;
    edt_aaCcv2: TsEdit;
    edt_aaCcv3: TsEdit;
    edt_aaCcv4: TsEdit;
    sPanel46: TsPanel;
    sPanel47: TsPanel;
    edt_shipPD1: TsEdit;
    edt_shipPD2: TsEdit;
    edt_shipPD3: TsEdit;
    edt_shipPD4: TsEdit;
    edt_shipPD5: TsEdit;
    edt_shipPD6: TsEdit;
    memo_GoodsDesc: TsMemo;
    sPanel38: TsPanel;
    sPanel39: TsPanel;
    sTabSheet6: TsTabSheet;
    sPage5: TsPanel;
    memo_DOCREQ: TsMemo;
    sPanel67: TsPanel;
    sPanel68: TsPanel;
    sPanel41: TsPanel;
    sButton1: TsButton;
    sButton2: TsButton;
    sButton3: TsButton;
    sPanel42: TsPanel;
    sButton4: TsButton;
    sButton5: TsButton;
    sButton6: TsButton;
    memo_SPECIAL_PAY: TsMemo;
    sPanel64: TsPanel;
    sPanel65: TsPanel;
    sPanel66: TsPanel;
    sButton10: TsButton;
    sButton11: TsButton;
    sButton12: TsButton;
    sTabSheet7: TsTabSheet;
    sPage6: TsPanel;
    sPanel82: TsPanel;
    edt_EXName1: TsEdit;
    edt_EXName2: TsEdit;
    edt_EXName3: TsEdit;
    edt_EXAddr1: TsEdit;
    edt_EXAddr2: TsEdit;
    sPanel83: TsPanel;
    sPanel84: TsPanel;
    sPanel85: TsPanel;
    sPanel86: TsPanel;
    sPanel87: TsPanel;
    sPanel104: TsPanel;
    edt_period: TsEdit;
    com_PeriodIndex: TsComboBox;
    edt_PeriodDetail: TsEdit;
    sPanel80: TsPanel;
    sPanel81: TsPanel;
    com_Confirm: TsComboBox;
    sPanel69: TsPanel;
    sPanel70: TsPanel;
    edt_CONFIRM_BIC: TsEdit;
    edt_CONFIRM1: TsEdit;
    edt_CONFIRM2: TsEdit;
    edt_CONFIRM4: TsEdit;
    edt_CONFIRM3: TsEdit;
    sPanel77: TsPanel;
    sPanel78: TsPanel;
    msk_exDate: TsMaskEdit;
    edt_exPlace: TsEdit;
    sPanel105: TsPanel;
    sPanel106: TsPanel;
    edt_Doccd: TsEdit;
    edt_Doccd1: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListMSeq: TIntegerField;
    qryListAMD_NO: TIntegerField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListIN_MATHOD: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_PAY: TStringField;
    qryListIL_NO1: TStringField;
    qryListIL_NO2: TStringField;
    qryListIL_NO3: TStringField;
    qryListIL_NO4: TStringField;
    qryListIL_NO5: TStringField;
    qryListIL_AMT1: TBCDField;
    qryListIL_AMT2: TBCDField;
    qryListIL_AMT3: TBCDField;
    qryListIL_AMT4: TBCDField;
    qryListIL_AMT5: TBCDField;
    qryListIL_CUR1: TStringField;
    qryListIL_CUR2: TStringField;
    qryListIL_CUR3: TStringField;
    qryListIL_CUR4: TStringField;
    qryListIL_CUR5: TStringField;
    qryListAD_INFO1: TStringField;
    qryListAD_INFO2: TStringField;
    qryListAD_INFO3: TStringField;
    qryListAD_INFO4: TStringField;
    qryListAD_INFO5: TStringField;
    qryListCD_NO: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListprno: TIntegerField;
    qryListF_INTERFACE: TStringField;
    qryListIMP_CD1: TStringField;
    qryListIMP_CD2: TStringField;
    qryListIMP_CD3: TStringField;
    qryListIMP_CD4: TStringField;
    qryListIMP_CD5: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListINCD_CUR: TStringField;
    qryListINCD_AMT: TBCDField;
    qryListDECD_CUR: TStringField;
    qryListDECD_AMT: TBCDField;
    qryListNWCD_CUR: TStringField;
    qryListNWCD_AMT: TBCDField;
    qryListCD_PERP: TIntegerField;
    qryListCD_PERM: TIntegerField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListLOAD_ON: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TBooleanField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListNARRAT: TBooleanField;
    qryListNARRAT_1: TMemoField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListSRBUHO: TStringField;
    qryListBFCD_AMT: TBCDField;
    qryListBFCD_CUR: TStringField;
    qryListCARRIAGE: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListmathod_Name: TStringField;
    qryListpay_Name: TStringField;
    qryListImp_Name_1: TStringField;
    qryListImp_Name_2: TStringField;
    qryListImp_Name_3: TStringField;
    qryListImp_Name_4: TStringField;
    qryListImp_Name_5: TStringField;
    qryListCarriage_Name: TStringField;
    memo_ADD_CONDITION: TsMemo;
    sPanel60: TsPanel;
    sPanel61: TsPanel;
    sPanel63: TsPanel;
    sButton7: TsButton;
    sButton8: TsButton;
    sButton9: TsButton;
    qryListIS_CANCEL: TStringField;
    qryListDOC_CD: TStringField;
    qryListCHARGE: TStringField;
    qryListCHARGE_1: TMemoField;
    qryListAMD_CHARGE: TStringField;
    qryListAMD_CHARGE_1: TMemoField;
    qryListSPECIAL_PAY: TMemoField;
    qryListGOODS_DESC: TMemoField;
    qryListDOC_REQ: TMemoField;
    qryListADD_CONDITION: TMemoField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListDEFPAY1: TStringField;
    qryListDEFPAY2: TStringField;
    qryListDEFPAY3: TStringField;
    qryListDEFPAY4: TStringField;
    qryListPERIOD_DAYS: TIntegerField;
    qryListPERIOD_IDX: TIntegerField;
    qryListPERIOD_DETAIL: TStringField;
    qryListCONFIRM: TStringField;
    qryListCONFIRM_BIC: TStringField;
    qryListCONFIRM1: TStringField;
    qryListCONFIRM2: TStringField;
    qryListCONFIRM3: TStringField;
    qryListCONFIRM4: TStringField;
    qryListDOC_CDNM: TStringField;
    edt_C_METHOD: TsEdit;
    edt_C_METHODNM: TsEdit;
    qryListPSHIP: TStringField;
    qryListTSHIP: TStringField;
    qryListMIX1: TStringField;
    qryListMIX2: TStringField;
    qryListMIX3: TStringField;
    qryListMIX4: TStringField;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sCheckBox2: TsCheckBox;
    edt_APPCHG1: TsEdit;
    edt_APPCHG2: TsEdit;
    edt_APPCHG3: TsEdit;
    edt_APPCHG4: TsEdit;
    qryListAPPLIC_CHG: TBooleanField;
    qryListAPPLIC_CHG1: TStringField;
    qryListAPPLIC_CHG2: TStringField;
    qryListAPPLIC_CHG3: TStringField;
    qryListAPPLIC_CHG4: TStringField;
    qryListBENEFC_CHG: TBooleanField;
    qryReady: TADOQuery;
    sButton13: TsButton;
    PopupMenu1: TPopupMenu;
    x1: TMenuItem;
    ADOStoredProc1: TADOStoredProc;
    sLabel4: TsLabel;
    edt_Applic5: TsEdit;
    sPanel6: TsPanel;
    memo_charge_1: TsMemo;
    sPanel26: TsPanel;
    sPanel28: TsPanel;
    com_amd_charge: TsComboBox;
    memo_amd_charge_1: TsMemo;
    sPanel33: TsPanel;
    sPanel30: TsPanel;
    sPanel31: TsPanel;
    com_charge: TsComboBox;
    sPanel32: TsPanel;
    sPanel7: TsPanel;
    sPanel27: TsPanel;
    sComboBox1: TsComboBox;
    sComboBox2: TsComboBox;
    qryCopy: TADOQuery;
    sHeader: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    edt_MSEQ: TsEdit;
    procedure FormShow(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sBitBtn1Click(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure edt_In_MathodDblClick(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure btnTempClick(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure sCheckBox1Click(Sender: TObject);
    procedure com_chargeSelect(Sender: TObject);
    procedure sButton10Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
    procedure sButton7Click(Sender: TObject);
    procedure edt_C_METHODExit(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnReadyClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure sButton13Click(Sender: TObject);
    procedure x1Click(Sender: TObject);
    procedure btn_PanelClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure sComboBox1Change(Sender: TObject);
    procedure sComboBox2Change(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
  private
    procedure ReadList(SortField: TColumn = nil);
    procedure ButtonEnable;
    procedure InitData;
    function CHECK_VALIDITY: string;
    procedure NewDoc;
    procedure InitDataFromAPP700;
    function DataProcess(nType: integer): TDOCNO;
    procedure InputControl;
    procedure DeleteDocument;
    function getAMDNO(MAINT_NO, MSEQ : String):integer;
  protected
    procedure ReadyDocument(DocNo: string); override;
  protected
    procedure ReadData; override;
    function getCompareDocno(DOCNO: string): Integer; override;
    procedure TXT_SELECT(Sender: TObject; var sMemo: TsMemo);

    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_APP707_NEW_frm: TUI_APP707_NEW_frm;

implementation

uses
  VarDefine, AutoNo, CodeContents, MSSQL, CodeDialogParent, CD_OPENDOC, CD_BIC,
  CD_BANK, CD_IMP_CD, ICON, CD_CUST, CD_DOC_CD, CD_C_METHOD, CD_AMT_UNIT,
  dlg_CDC_APP707, dlg_CreateDocumentChoice, dlg_CopyAPP707fromINF700,
  dlg_SelectCopyDocument, Dlg_ErrorMessage, SQLCreator, MessageDefine,
  CreateDocuments, DocumentSend, Dlg_RecvSelect, LivartData, LivartModule, QR_APP707_PRN,
  Preview;

{$R *.dfm}


//수수료부담자 변경 MIG가 좀 이상함 전화해서 확인할것
//목차의 내용
//16. 수수료부담자 변경 71D
//17. 신용장 변경수수료 부담자 71N
//
//Original MIG P.74
//AGENT Agent's Commission
//COMM Our Commission
//CORCOM Our Correspondent's Commission
//DISC Commercial Discount
//INSUR Insurance Premium
//POST Our Postage
//STAMP Stamp Duty
//TELECHAR Teletransmission Charges
//WAREHOUS Wharfing and Warehouse
//
//Page2부터 시작

procedure TUI_APP707_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctView;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  com_Pship.Perform(CB_SETDROPPEDWIDTH, 300, 0);
  com_Tship.Perform(CB_SETDROPPEDWIDTH, 300, 0);

  sBitBtn1Click(nil);

  EnableControl(sHeader, false);
  EnableControl(sPage1, false);
  EnableControl(sPage2, False);
  EnableControl(sPage3, False);
  EnableControl(sPage4, False);
  EnableControl(sPage5, False);
  EnableControl(sPage6, False);

  sPageControl1.ActivePageIndex := 0;

  sButton13.Visible := LoginData.sCompanyNo = '1358132239';
end;

procedure TUI_APP707_NEW_frm.ReadData;
begin
  inherited;
//------------------------------------------------------------------------------
// 헤더
//------------------------------------------------------------------------------
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  edt_MSEQ.Text := qryListMSeq.AsString;
  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;
  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

//------------------------------------------------------------------------------
// PAGE 1
//------------------------------------------------------------------------------
  edt_APPDATE.Text := qryListAPP_DATE.AsString;
  msk_IssDate.Text := qryListISS_DATE.AsString;
  curr_AMD_NO.Value := qryListAMD_NO.AsInteger;
  if UpperCase(qryListIS_CANCEL.AsString) = 'CR' then
    com_isCancel.ItemIndex := 1
  else
    com_isCancel.ItemIndex := 0;
  edt_cdNo.Text := qryListCD_NO.AsString;

  edt_In_Mathod.Text := qryListIN_MATHOD.AsString;
  edt_In_Mathod1.Text := qryListmathod_Name.AsString;
  edt_ApBank.Text := qryListAP_BANK.AsString;
  edt_ApBank1.Text := qryListAP_BANK1.AsString;
  edt_ApBank2.Text := qryListAP_BANK2.AsString;
  edt_ApBank3.Text := qryListAP_BANK3.AsString;
  edt_ApBank4.Text := qryListAP_BANK4.AsString;
  edt_ApBank5.Text := qryListAP_BANK5.AsString;

  edt_AdBank.Text := qryListAD_BANK.AsString;
  edt_AdBank1.Text := qryListAD_BANK1.AsString;
  edt_AdBank2.Text := qryListAD_BANK2.AsString;
  edt_AdBank3.Text := qryListAD_BANK3.AsString;
  edt_AdBank4.Text := qryListAD_BANK4.AsString;

  edt_ImpCd1.Text := qryListIMP_CD1.AsString;
  edt_ImpCd2.Text := qryListIMP_CD2.AsString;
  edt_ImpCd3.Text := qryListIMP_CD3.AsString;
  edt_ImpCd4.Text := qryListIMP_CD4.AsString;
  edt_ImpCd5.Text := qryListIMP_CD5.AsString;
  edt_ImpCd1_1.Text := qryListImp_Name_1.AsString;
  edt_ImpCd1_2.Text := qryListImp_Name_2.AsString;
  edt_ImpCd1_3.Text := qryListImp_Name_3.AsString;
  edt_ImpCd1_4.Text := qryListImp_Name_4.AsString;
  edt_ImpCd1_5.Text := qryListImp_Name_5.AsString;
  edt_ILno1.Text := qryListIL_NO1.AsString;
  edt_ILno2.Text := qryListIL_NO2.AsString;
  edt_ILno3.Text := qryListIL_NO3.AsString;
  edt_ILno4.Text := qryListIL_NO4.AsString;
  edt_ILno5.Text := qryListIL_NO5.AsString;
  edt_ILCur1.Text := qryListIL_CUR1.AsString;
  edt_ILCur2.Text := qryListIL_CUR2.AsString;
  edt_ILCur3.Text := qryListIL_CUR3.AsString;
  edt_ILCur4.Text := qryListIL_CUR4.AsString;
  edt_ILCur5.Text := qryListIL_CUR5.AsString;
  curr_ILAMT1.Text := qryListIL_AMT1.AsString;
  curr_ILAMT2.Text := qryListIL_AMT2.AsString;
  curr_ILAMT3.Text := qryListIL_AMT3.AsString;
  curr_ILAMT4.Text := qryListIL_AMT4.AsString;
  curr_ILAMT5.Text := qryListIL_AMT5.AsString;
  edt_AdInfo1.Text := qryListAD_INFO1.AsString;
  edt_AdInfo2.Text := qryListAD_INFO2.AsString;
  edt_AdInfo3.Text := qryListAD_INFO3.AsString;
  edt_AdInfo4.Text := qryListAD_INFO4.AsString;
  edt_AdInfo5.Text := qryListAD_INFO5.AsString;
//------------------------------------------------------------------------------
// PAGE 2
//------------------------------------------------------------------------------
  edt_Applic1.Text := qryListAPPLIC1.AsString;
  edt_Applic2.Text := qryListAPPLIC2.AsString;
  edt_Applic3.Text := qryListAPPLIC3.AsString;
  edt_Applic4.Text := qryListAPPLIC4.AsString;
  edt_Applic5.Text := qryListAPPLIC5.AsString;

//  sCheckBox1.Checked := qryListAPPLIC_CHG.AsBoolean;
//  if sCheckBox1.Checked then
  if qryListAPPLIC_CHG.AsBoolean Then 
  begin
    edt_APPCHG1.Text := qryListAPPLIC_CHG1.AsString;
    edt_APPCHG2.Text := qryListAPPLIC_CHG2.AsString;
    edt_APPCHG3.Text := qryListAPPLIC_CHG3.AsString;
    edt_APPCHG4.Text := qryListAPPLIC_CHG4.AsString;
  end
  else
  begin
    edt_APPCHG1.Clear;
    edt_APPCHG2.Clear;
    edt_APPCHG3.Clear;
    edt_APPCHG4.Clear;
  end;
  
  sCheckBox2.Checked := qryListBENEFC_CHG.AsBoolean;
  if sCheckBox2.Checked then
  begin
    edt_Benefc1.Text := qryListBENEFC1.AsString;
    edt_Benefc2.Text := qryListBENEFC2.AsString;
    edt_Benefc3.Text := qryListBENEFC3.AsString;
    edt_Benefc4.Text := qryListBENEFC4.AsString;
    edt_Benefc5.Text := qryListBENEFC5.AsString;
  end;

  edt_Doccd.Text := qryListDOC_CD.AsString;
  edt_Doccd1.Text := qryListDOC_CDNM.AsString;
  //20190109 이전차수와 비교하여 동일할 경우 전송보내지말것
  edt_C_METHOD.Text := qryListCARRIAGE.AsString;
  edt_C_METHODNM.Text := qryListCarriage_Name.AsString;
  msk_exDate.Text := qryListEX_DATE.AsString;
  edt_exPlace.Text := qryListEX_PLACE.AsString;
  edt_IncdCur.Text := qryListINCD_CUR.AsString;
  edt_IncdAmt.Value := qryListINCD_AMT.AsCurrency;
  edt_DecdCur.Text := qryListDECD_CUR.AsString;
  edt_DecdAmt.Value := qryListDECD_AMT.AsCurrency;
  edt_CdPerp.Value := qryListCD_PERP.AsCurrency;
  edt_CdPerm.Value := qryListCD_PERM.AsCurrency;

  com_charge.ItemIndex := AnsiIndexText(UpperCase(qryListCHARGE.AsString), ['', '2AF', '2AG', '2AH']);
  if com_charge.ItemIndex = 3 then
    memo_charge_1.Text := qryListCHARGE_1.AsString
  else
    memo_charge_1.Clear;

  com_amd_charge.ItemIndex := AnsiIndexText(UpperCase(qryListAMD_CHARGE.AsString), ['', '2AC', '2AD', '2AE']);
  if com_amd_charge.ItemIndex = 3 then
    memo_amd_charge_1.Text := qryListAMD_CHARGE_1.AsString
  else
    memo_amd_charge_1.Clear;

//------------------------------------------------------------------------------
// PAGE 3
//------------------------------------------------------------------------------
  CM_INDEX(com_PShip, [':'], 0, qryListPSHIP.AsString, 0);
  CM_INDEX(com_TShip, [':'], 0, qryListTSHIP.AsString, 0);
  edt_loadOn.Text := qryListLOAD_ON.AsString;
  edt_forTran.Text := qryListFOR_TRAN.AsString;
  msk_lstDate.Text := qryListLST_DATE.AsString;
  edt_SunjukPort.Text := qryListSUNJUCK_PORT.AsString;
  edt_dochackPort.Text := qryListDOCHACK_PORT.AsString;
  edt_shipPD1.Text := qryListSHIP_PD1.AsString;
  edt_shipPD2.Text := qryListSHIP_PD2.AsString;
  edt_shipPD3.Text := qryListSHIP_PD3.AsString;
  edt_shipPD4.Text := qryListSHIP_PD4.AsString;
  edt_shipPD5.Text := qryListSHIP_PD5.AsString;
  edt_shipPD6.Text := qryListSHIP_PD6.AsString;
  memo_SPECIAL_PAY.Text := qryListSPECIAL_PAY.AsString;
//------------------------------------------------------------------------------
// PAGE 4
//------------------------------------------------------------------------------
  edt_aaCcv1.Text := qryListAA_CV1.AsString;
  edt_Aaccv2.Text := qryListAA_CV2.AsString;
  edt_Aaccv3.Text := qryListAA_CV3.AsString;
  edt_Aaccv4.Text := qryListAA_CV4.AsString;
  edt_MIX1.Text := qryListMIX1.AsString;
  edt_MIX2.Text := qryListMIX2.AsString;
  edt_MIX3.Text := qryListMIX3.AsString;
  edt_MIX4.Text := qryListMIX4.AsString;
  edt_DRAFT1.Text := qryListDRAFT1.AsString;
  edt_DRAFT2.Text := qryListDRAFT2.AsString;
  edt_DRAFT3.Text := qryListDRAFT3.AsString;
  edt_DEFPAY1.Text := qryListDEFPAY1.AsString;
  edt_DEFPAY2.Text := qryListDEFPAY2.AsString;
  edt_DEFPAY3.Text := qryListDEFPAY3.AsString;
  edt_DEFPAY4.Text := qryListDEFPAY4.AsString;
  memo_GoodsDesc.Text := qryListGOODS_DESC.AsString;
//------------------------------------------------------------------------------
// PAGE 5
//------------------------------------------------------------------------------
  memo_DOCREQ.Text := qryListDOC_REQ.AsString;

//------------------------------------------------------------------------------
// PAGE 6
//------------------------------------------------------------------------------
  memo_ADD_CONDITION.Text := qryListADD_CONDITION.AsString;
  edt_period.Text := qryListPERIOD_DAYS.AsString;
  com_PeriodIndex.ItemIndex := qryListPERIOD_IDX.AsInteger;
  if com_PeriodIndex.ItemIndex = 1 then
    edt_PeriodDetail.Text := qryListPERIOD_DETAIL.AsString
  ELSE
    edt_PeriodDetail.Clear;  
  //DA: WITHOUT, DB: MAY ADD, DC: CONFIRM
  com_Confirm.ItemIndex := AnsiIndexText(UpperCase(qryListCONFIRM.AsString), ['', 'DA', 'DB', 'DC']);
  edt_CONFIRM_BIC.Text := qryListCONFIRM_BIC.AsString;
  edt_CONFIRM1.Text := qryListCONFIRM1.AsString;
  edt_CONFIRM2.Text := qryListCONFIRM2.AsString;
  edt_CONFIRM3.Text := qryListCONFIRM3.AsString;
  edt_CONFIRM4.Text := qryListCONFIRM4.AsString;
  //명의인
  edt_EXName1.Text := qryListEX_NAME1.AsString;
  edt_EXName2.Text := qryListEX_NAME2.AsString;
  //식별부호
  edt_EXName3.Text := qryListEX_NAME3.AsString;
  //주소
  edt_EXAddr1.Text := qryListEX_ADDR1.AsString;
  edt_EXAddr2.Text := qryListEX_ADDR2.AsString;

end;

procedure TUI_APP707_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_APP707_NEW_frm.ReadList(SortField: TColumn);
var
  tmpFieldNm: string;
begin
  with qryList do
  begin
    Close;

    SQL.Text := qryCopy.SQL.Text;
    SQL.Add('WHERE (DATEE BETWEEN :FDATE AND :TDATE) AND ');
    case sComboBox1.ItemIndex of
      //관리번호
      0: SQL.Add('(A707_1.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))');
      //LC번호
      1: SQL.Add('(A707_1.CD_NO LIKE :MAINT_NO OR (1=:ALLDATA))');
    end;
    SQL.Add('ORDER BY DATEE DESC');

    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    if SortField <> nil then
    begin
      tmpFieldNm := SortField.FieldName;
      if SortField.FieldName = 'MAINT_NO' then
        tmpFieldNm := 'A707_1.MAINT_NO';

      if LeftStr(qryList.SQL.Strings[qryList.SQL.Count - 1], Length('ORDER BY')) = 'ORDER BY' then
      begin
        if Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count - 1], 4)) = 'ASC' then
          qryList.SQL.Strings[qryList.SQL.Count - 1] := 'ORDER BY ' + tmpFieldNm + ' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count - 1] := 'ORDER BY ' + tmpFieldNm + ' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY ' + tmpFieldNm + ' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_APP707_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_APP707_NEW_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsButton).Tag = 1 Then
  begin
    if not AnsiMatchText(qryListCHK2.AsString, ['', '0', '1', '5', '6', '8']) then
    begin
      MessageBox(Self.Handle, MSG_SYSTEM_NOT_EDIT, '수정불가', MB_OK + MB_ICONINFORMATION);
      Exit;
    end;
  end;

  if (Sender as TsButton).Tag = 0 then
  begin
    dlg_CDC_APP707_frm := Tdlg_CDC_APP707_frm.Create(Self);
    try
      case dlg_CDC_APP707_frm.ShowModal of
        //내국신용장 개설응답서 가져오기
        mrOk:
          begin
            dlg_CopyAPP707fromINF700_frm := Tdlg_CopyAPP707fromINF700_frm.Create(Self);
            try
              if dlg_CopyAPP707fromINF700_frm.ShowModal = mrCancel then
                Abort;

              InitDataFromAPP700;

            finally
              FreeAndNil(dlg_CopyAPP707fromINF700_frm);
            end;
          end;
        //내국신용장 조건변경 통지서 가져오기
        mrYes:
          begin
//          dlg_CopyLOCAMRfromLOCAMA_frm := Tdlg_CopyLOCAMRfromLOCAMA_frm.Create(Self);
//          try
//            if dlg_CopyLOCAMRfromLOCAMA_frm.ShowModal = mrCancel Then Abort;
//
//            InitDataFromLOCAMA;
//
//          finally
//            FreeAndNil(dlg_CopyLOCAMRfromLOCAMA_frm);
//          end;
          end;
        //신규작성
        mrIgnore:
          begin
            InitData;
            NewDoc;
          end;
        //취소
        mrCancel:
          Abort;
      end;
    finally
      FreeAndNil(dlg_CDC_APP707_frm);
    end;
  end;

  case (Sender as TsButton).Tag of
    0:
      ProgramControlType := ctInsert;
    1:
      ProgramControlType := ctModify;
  end;

  EnableControl(sHeader);
  EnableControl(sPage1);
  EnableControl(sPage2);
  EnableControl(sPage3);
  EnableControl(sPage4);
  EnableControl(sPage5);
  EnableControl(sPage6);

  if ProgramControlType = ctmodify then
  begin
    EnableControl(sHeader, false);
  end;

  ButtonEnable;

  sPanel29.Visible := ProgramControlType = ctInsert;

  if ProgramControlType = ctInsert then
    sPageControl1.ActivePageIndex := 0;

  InputControl;

end;

procedure TUI_APP707_NEW_frm.ButtonEnable;
begin
  inherited;
  btnNew.Enabled := ProgramControlType = ctView;
  btnEdit.Enabled := btnNew.Enabled;
  btnDel.Enabled := btnNew.Enabled;
//  btnCopy.Enabled := btnNew.Enabled;

  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := not btnNew.Enabled;
  btnCancel.Enabled := not btnNew.Enabled;
  btnPrint.Enabled := btnNew.Enabled;
  btnReady.Enabled := btnNew.Enabled;
  btnSend.Enabled := btnNew.Enabled;

  sDBGrid1.Enabled := btnNew.Enabled;
  sDBGrid3.Enabled := btnNew.Enabled;
end;

procedure TUI_APP707_NEW_frm.InitData;
begin
  ClearControl(sPage1);
  ClearControl(sPage2);
  ClearControl(sPage3);
  ClearControl(sPage4);
  ClearControl(sPage5);
  ClearControl(sPage6);

  //관리번호
  edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APP707');
  edt_MSEQ.Text := '1';

  //헤더
  msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text := LoginData.sID;
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;

  //개설신청일
  edt_APPDATE.Text := FormatDateTime('YYYYMMDD', Now);

  //------------------------------------------------------------------------------
  // 발신기관 전자서명
  //------------------------------------------------------------------------------
  edt_EXName1.Text := DMCodeContents.ConfigNAME1.AsString;
  edt_EXName2.Text := DMCodeContents.ConfigNAME2.AsString;
  edt_EXName3.Text := DMCodeContents.ConfigSIGN.AsString;
  edt_EXAddr1.Text := DMCodeContents.ConfigADDR1.Text;
  edt_EXAddr2.Text := DMCodeContents.ConfigADDR2.Text;
end;

procedure TUI_APP707_NEW_frm.btnCancelClick(Sender: TObject);
var
  sKey, sMseq: string;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;

//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sHeader, false);
  EnableControl(sPage1, false);
  EnableControl(sPage2, False);
  EnableControl(sPage3, False);
  EnableControl(sPage4, False);
  EnableControl(sPage5, False);
  EnableControl(sPage6, False);

//------------------------------------------------------------------------------
// 버튼활성화
//------------------------------------------------------------------------------
  ButtonEnable;

  if DMMssql.inTrans then
  begin
    case (Sender as TsButton).Tag of
      0, 1:
        DMMssql.CommitTrans;
      2:
        DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  sKey := qryListMAINT_NO.AsString;
  sMseq := qryListMSeq.AsString;
  Readlist;

  if not AnsiMatchText('', [sKey, sMseq]) then
    qryList.Locate('MAINT_NO;MSEQ', VarArrayOf([sKey, sMseq]), []);
end;

function TUI_APP707_NEW_frm.CHECK_VALIDITY: string;
var
  ErrMsg: TStringList;
  TempStr: string;
  nCount: Integer;
begin
  ErrMsg := TStringList.Create;
  try
    //------------------------------------------------------------------------------
    // 공통
    //------------------------------------------------------------------------------
    AddErrMsg(ErrMsg, edt_MSEQ, '[PAGE1] 관리번호-순번을 입력하세요');
    AddErrMsg(ErrMsg, msk_datee, '[PAGE1] 등록일자를 입력하세요');
    AddErrMsg(ErrMsg, edt_IN_MATHOD, '[PAGE1] 개설방법을 입력해야 합니다');
    AddErrMsg(ErrMsg, edt_ApBank, '[PAGE1] 개설(의뢰)은행[코드]을 입력해야합니다');

    TempStr := edt_ApBank1.Text + edt_ApBank2.Text + edt_ApBank3.Text + edt_ApBank4.Text;
    if Trim(TempStr) = '' then
      AddErrMsg(ErrMsg, '[PAGE1] 개설(의뢰)은행을 입력해야합니다');

//    AddErrMsg(ErrMsg, edt_C_METHOD, '[PAGE2] 운송수단을 입력해야합니다');

    IF ((msk_exDate.Text <> '') AND (Trim(edt_exPlace.Text) = '')) OR
       ((msk_exDate.Text = '') AND (Trim(edt_exPlace.Text) <> ''))
    Then
    begin
      AddErrMsg(ErrMsg,'[PAGE1] 31D|유효기일/서류제시장소는 같이 입력되어야합니다');
    end;


    nCount := 0;
    TempStr := Trim(edt_DRAFT1.Text + edt_DRAFT2.Text + edt_DRAFT3.Text);
    if TempStr <> '' then
      Inc(nCount);
    TempStr := Trim(edt_MIX1.Text + edt_MIX2.Text + edt_MIX3.Text + edt_MIX4.Text);
    if TempStr <> '' then
      Inc(nCount);
    TempStr := Trim(edt_DEFPAY1.Text + edt_DEFPAY2.Text + edt_DEFPAY3.Text + edt_DEFPAY4.Text);
    if TempStr <> '' then
      Inc(nCount);

    if nCount > 1 then
      AddErrMsg(ErrMsg, '[PAGE4] 화환어음조건, 혼합지급조건, 매입/연지금조건명세 중 하나만 입력가능합니다');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

procedure TUI_APP707_NEW_frm.edt_In_MathodDblClick(Sender: TObject);
var
  uDialog: TCodeDialogParent_frm;
begin
  inherited;
  uDialog := nil;
  if ProgramControlType = ctView then
    Exit;

  case (Sender as TsEdit).Tag of
    //개설방법
    1000:
      uDialog := TCD_OPENDOC_frm.Create(Self);
    1001:
      uDialog := TCD_BANK_frm.Create(Self);
    1002:
      uDialog := TCD_BIC_frm.Create(Self);
    1003, 1004, 1005, 1006, 1007:
      uDialog := TCD_IMP_CD_frm.Create(Self);
    //신용장종류
    1008:
      uDialog := TCD_DOC_CD_frm.Create(Self);
    //운송수단
    1009:
      uDialog := TCD_C_METHOD_frm.Create(Self);
    //통화
    1010, 1011:
      uDialog := TCD_AMT_UNIT_frm.Create(Self);
  end;

  try
    if uDialog = nil then
      Exit;

    if uDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := uDialog.Values[0];

      case (Sender as TsEdit).Tag of
        //개설방법
        1000:
          edt_In_Mathod1.Text := uDialog.Values[1];
        //개설의뢰은행
        1001:
          begin
            edt_ApBank1.Text := uDialog.Values[1];
            edt_ApBank2.Text := uDialog.Values[3];
            edt_ApBank3.Text := uDialog.Values[2];
            edt_ApBank4.Text := uDialog.Values[4];
          end;
        //수입용도
        1003:
          edt_ImpCd1_1.Text := uDialog.Values[1];
        1004:
          edt_ImpCd1_2.Text := uDialog.Values[1];
        1005:
          edt_ImpCd1_3.Text := uDialog.Values[1];
        1006:
          edt_ImpCd1_4.Text := uDialog.Values[1];
        1007:
          edt_ImpCd1_5.Text := uDialog.Values[1];
        //수입신용장종류
        1008:
          edt_Doccd1.Text := uDialog.Values[1];
        //운송수단
        1009:
          edt_C_METHODNM.Text := uDialog.Values[1];
      end;
    end;
  finally
    FreeAndNil(uDialog);
  end;
end;

procedure TUI_APP707_NEW_frm.sBitBtn2Click(Sender: TObject);
var
  uDialog: TCodeDialogParent_frm;
begin
  inherited;
  uDialog := nil;
  if ProgramControlType = ctView then
    Exit;

  case (Sender as TsBitBtn).Tag of
    //개설방법
    2000, 2001:
      uDialog := TCD_CUST_frm.Create(Self);
  end;

  try
    if uDialog = nil then
      Exit;

    if uDialog.OpenDialog(0, '') then
    begin
      case (Sender as TsBitBtn).Tag of
        2000:
          begin
            edt_APPCHG1.Text := uDialog.FieldValues('ENAME');
            edt_APPCHG2.Text := uDialog.FieldValues('REP_NAME');
            edt_APPCHG3.Text := uDialog.FieldValues('ADDR1');
            edt_APPCHG4.Text := uDialog.FieldValues('ADDR2');
          end;
        //수익자
        2001:
          begin
            edt_Benefc1.Text := uDialog.FieldValues('ENAME');
            edt_Benefc2.Text := uDialog.FieldValues('REP_NAME');
            edt_Benefc3.Text := uDialog.FieldValues('ADDR1');
            edt_Benefc4.Text := uDialog.FieldValues('ADDR2');
          end;
      end;
    end;
  finally
    FreeAndNil(uDialog);
  end;
end;

procedure TUI_APP707_NEW_frm.NewDoc;
begin
  //조건변경 신청일
  edt_APPDATE.Text := FormatDateTime('YYYYMMDD', Now);
  curr_AMD_NO.Value := 1;
  //------------------------------------------------------------------------------
  // 개설의뢰인 셋팅
  //------------------------------------------------------------------------------
  if DMCodeContents.GEOLAECHEO.Locate('CODE', '00000', []) then
  begin
    edt_EXName1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    edt_EXName2.Text := DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
    edt_EXName3.Text := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
    edt_EXAddr1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
    edt_EXAddr2.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
  end;

end;

procedure TUI_APP707_NEW_frm.InitDataFromAPP700;
var
  TempStr: string;
  nPos: Integer;
begin
  ClearControl(sPage1);
  ClearControl(sPage2);
  ClearControl(sPage3);
  ClearControl(sPage4);
  ClearControl(sPage5);
  ClearControl(sPage6);
  
  //관리번호
  edt_MAINT_NO.Text := dlg_CopyAPP707fromINF700_frm.FieldString('MAINT_NO');
  edt_MSEQ.Text := dlg_CopyAPP707fromINF700_frm.FieldString('MSEQ');
//  edt_MSEQ.Text := IntToStr(getCompareDocno(edt_MAINT_NO.Text));

  //헤더
  msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text := LoginData.sID;
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;

  with dlg_CopyAPP707fromINF700_frm do
  begin
    edt_APPDATE.Text := FormatDateTime('YYYYMMDD', Now);
    msk_IssDate.Text := FieldString('ISS_DATE');
    //2019-03-06
    //조건변경횟수를 찾아서 마지막것으로 뿌려줌
    curr_AMD_NO.Value := getAMDNO(edt_MAINT_NO.Text, edt_MSEQ.Text);

    //발신은행 참조사항(신용장번호 an..16)
    edt_cdNo.Text := FieldString('CD_NO');
    //개설방법
    edt_In_Mathod.Text := FieldString('IN_MATHOD');
    edt_In_Mathod1.Text := FieldString('mathod_Name');
    //운송수단
    edt_C_METHOD.Text := qryListCARRIAGE.AsString;
    edt_C_METHODNM.Text := qryListCarriage_Name.AsString;
    //개설(의뢰)은행
    edt_ApBank.Text := FieldString('AP_BANK');
    edt_ApBank1.Text := FieldString('AP_BANK1');
    edt_ApBank2.Text := FieldString('AP_BANK2');
    edt_ApBank3.Text := FieldString('AP_BANK3');
    edt_ApBank4.Text := FieldString('AP_BANK4');
    edt_ApBank5.Text := FieldString('AP_BANK5');
    //통지은행
//    edt_AdBank.Text := FieldString('AD_BANK');
//    edt_AdBank1.Text := FieldString('AD_BANK1');
//    edt_AdBank2.Text := FieldString('AD_BANK2');
//    edt_AdBank3.Text := FieldString('AD_BANK3');
//    edt_AdBank4.Text := FieldString('AD_BANK4');
    //수입용도
//    edt_ImpCd1.Text   := FieldString('IMP_CD1');
//    edt_ImpCd1_1.Text := FieldString('Imp_Name_1');
//    edt_ILno1.Text    := FieldString('IL_NO1');
//    edt_ILCur1.Text   := FieldString('IL_CUR1');
//    curr_ILAMT1.Value := FieldCurr('IL_AMT1');
//
//    edt_ImpCd2.Text   := FieldString('IMP_CD2');
//    edt_ImpCd1_2.Text := FieldString('Imp_Name_2');
//    edt_ILno2.Text    := FieldString('IL_NO2');
//    edt_ILCur2.Text   := FieldString('IL_CUR2');
//    curr_ILAMT2.Value := FieldCurr('IL_AMT2');
//
//    edt_ImpCd3.Text   := FieldString('IMP_CD3');
//    edt_ImpCd1_3.Text := FieldString('Imp_Name_3');
//    edt_ILno3.Text    := FieldString('IL_NO3');
//    edt_ILCur3.Text   := FieldString('IL_CUR3');
//    curr_ILAMT3.Value := FieldCurr('IL_AMT3');
//
//    edt_ImpCd4.Text   := FieldString('IMP_CD4');
//    edt_ImpCd1_4.Text := FieldString('Imp_Name_4');
//    edt_ILno4.Text    := FieldString('IL_NO4');
//    edt_ILCur4.Text   := FieldString('IL_CUR4');
//    curr_ILAMT4.Value := FieldCurr('IL_AMT4');
//
//    edt_ImpCd5.Text   := FieldString('IMP_CD5');
//    edt_ImpCd1_5.Text := FieldString('Imp_Name_5');
//    edt_ILno5.Text    := FieldString('IL_NO5');
//    edt_ILCur5.Text   := FieldString('IL_CUR5');
//    curr_ILAMT5.Value := FieldCurr('IL_AMT5');
    //개설의뢰인
    edt_Applic1.Text := FieldString('APPLIC1');
    edt_Applic2.Text := FieldString('APPLIC2');
    edt_Applic3.Text := FieldString('APPLIC3');
    edt_Applic4.Text := FieldString('APPLIC4');
    edt_Applic4.Text := FieldString('APPLIC5');

    edt_APPCHG1.Clear;
    edt_APPCHG2.Clear;
    edt_APPCHG3.Clear;
    edt_APPCHG4.Clear;

    //수익자
    edt_Benefc1.Text := FieldString('BENEFC1');
    edt_Benefc2.Text := FieldString('BENEFC2');
    edt_Benefc3.Text := FieldString('BENEFC3');
    edt_Benefc4.Text := FieldString('BENEFC4');
    edt_Benefc5.Text := FieldString('BENEFC5');

    //기타정보
//    edt_AdInfo1.Text := qryListAD_INFO1.AsString;
//    edt_AdInfo2.Text := qryListAD_INFO2.AsString;
//    edt_AdInfo3.Text := qryListAD_INFO3.AsString;
//    edt_AdInfo4.Text := qryListAD_INFO4.AsString;
//    edt_AdInfo5.Text := qryListAD_INFO5.AsString;

    if DMCodeContents.GEOLAECHEO.Locate('CODE', '00000', []) then
    begin
      edt_EXName1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
      edt_EXName2.Text := DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
      edt_EXName3.Text := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
      edt_EXAddr1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
      edt_EXAddr2.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
    end;

  end;
end;

function TUI_APP707_NEW_frm.getCompareDocno(DOCNO: string): Integer;
begin
  Result := 1;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT ISNULL(MAX(MSEQ),0) as MAX_SEQ FROM APP707_1 WHERE MAINT_NO = ' + QuotedStr(DOCNO);
      Open;

      Result := FieldByName('MAX_SEQ').AsInteger + 1;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_APP707_NEW_frm.btnTempClick(Sender: TObject);
var
  DOCNO: TDOCNO;
begin
  inherited;
//------------------------------------------------------------------------------
// 유효성 검사
//------------------------------------------------------------------------------
  if (Sender as TsButton).Tag = 1 then
  begin
    Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
    if Dlg_ErrorMessage_frm.Run_ErrorMessage('취소불능화환신용장 조건변경신청서', CHECK_VALIDITY) then
    begin
      FreeAndNil(Dlg_ErrorMessage_frm);
      Exit;
    end;
  end;

  try
    DOCNO := DataProcess((Sender as TsButton).Tag);
  except
    on E: Exception do
    begin
      MessageBox(Self.Handle, PChar(E.Message), '데이터처리 오류', MB_OK + MB_ICONERROR);
      if DMMssql.inTrans then
        DMMssql.RollbackTrans;
    end;
  end;

  ProgramControlType := ctView;
  ButtonEnable;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sHeader, False);
  EnableControl(sPage1, False);
  EnableControl(sPage2, False);
  EnableControl(sPage3, False);
  EnableControl(sPage4, False);
  EnableControl(sPage5, False);
  EnableControl(sPage6, False);

  if DMMssql.inTrans then
  begin
    case (Sender as TsButton).Tag of
      0, 1:
        DMMssql.CommitTrans;
      2:
        DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  Readlist;
  qryList.Locate('MAINT_NO;MSEQ;AMD_NO', VarArrayOf([DOCNO.MAINT_NO, DOCNO.MSEQ, DOCNO.AMD_NO]), []);
end;

procedure TUI_APP707_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
//  inherited;
  if DataSet.RecordCount = 0 then
    qryListAfterScroll(DataSet);
end;

function TUI_APP707_NEW_frm.DataProcess(nType: integer): TDOCNO;
var
  MAINT_NO: string;
  SQLCreate: TSQLCreate;
  TempStr: string;
  nIdx: integer;
begin
  if nType = 2 then
    Exit;
  //------------------------------------------------------------------------------
  // SQL생성기
  //------------------------------------------------------------------------------
  SQLCreate := TSQLCreate.Create;
  try
    Result.MAINT_NO := edt_MAINT_NO.Text;
    Result.MSEQ := StrToInt(edt_MSEQ.Text);
    Result.AMD_NO := StrToInt(curr_AMD_NO.Text);

    with SQLCreate do
    begin
      case ProgramControlType of
        ctInsert:
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO', Result.MAINT_NO);
            ADDValue('MSEQ', Result.MSEQ);
            ADDValue('AMD_NO', Result.AMD_NO);
          end;

        ctModify:
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO', Result.MAINT_NO);
            ADDWhere('MSEQ', Result.MSEQ);
            ADDWhere('AMD_NO', Result.AMD_NO);
          end;
      end;

      SQLHeader('APP707_1');

//      ADDValue('AMD_NO', Result.AMD_NO);
      //문서저장구분(0:임시, 1:저장)
      ADDValue('CHK2', nType);
      //등록일자
      ADDValue('DATEE', msk_Datee.Text);
      //유저ID
      ADDValue('USER_ID', edt_userno.Text);
      //메세지1
      ADDValue('MESSAGE1', CM_TEXT(com_func, [':']));
      //메세지2
      ADDValue('MESSAGE2', CM_TEXT(com_type, [':']));

      //조건변경신청일자
      ADDValue('APP_DATE', edt_APPDATE.Text);
      //개설방법
      ADDValue('IN_MATHOD', edt_In_Mathod.Text);
      //개설의뢰은행
      ADDValue('AP_BANK', edt_ApBank.Text);
      ADDValue('AP_BANK1', edt_ApBank1.Text);
      ADDValue('AP_BANK2', edt_ApBank2.Text);
      ADDValue('AP_BANK3', edt_ApBank3.Text);
      ADDValue('AP_BANK4', edt_ApBank4.Text);
      ADDValue('AP_BANK5', edt_ApBank5.Text);
      //통지은행
      ADDValue('AD_BANK', edt_AdBank.Text);
      ADDValue('AD_BANK1', edt_AdBank1.Text);
      ADDValue('AD_BANK2', edt_AdBank2.Text);
      ADDValue('AD_BANK3', edt_AdBank3.Text);
      ADDValue('AD_BANK4', edt_AdBank4.Text);
      //수입용도
      ADDValue('IMP_CD1', edt_ImpCd1.Text);
      ADDValue('IMP_CD2', edt_ImpCd2.Text);
      ADDValue('IMP_CD3', edt_ImpCd3.Text);
      ADDValue('IMP_CD4', edt_ImpCd4.Text);
      ADDValue('IMP_CD5', edt_ImpCd5.Text);
      ADDValue('IL_NO1', edt_ILno1.Text);
      ADDValue('IL_NO2', edt_ILno2.Text);
      ADDValue('IL_NO3', edt_ILno3.Text);
      ADDValue('IL_NO4', edt_ILno4.Text);
      ADDValue('IL_NO5', edt_ILno5.Text);
      ADDValue('IL_AMT1', curr_ILAMT1.Value);
      ADDValue('IL_AMT2', curr_ILAMT2.Value);
      ADDValue('IL_AMT3', curr_ILAMT3.Value);
      ADDValue('IL_AMT4', curr_ILAMT4.Value);
      ADDValue('IL_AMT5', curr_ILAMT5.Value);
      ADDValue('IL_CUR1', edt_ILCur1.Text);
      ADDValue('IL_CUR2', edt_ILCur2.Text);
      ADDValue('IL_CUR3', edt_ILCur3.Text);
      ADDValue('IL_CUR4', edt_ILCur4.Text);
      ADDValue('IL_CUR5', edt_ILCur5.Text);
      //기타정보
      ADDValue('AD_INFO1', edt_AdInfo1.Text);
      ADDValue('AD_INFO2', edt_AdInfo2.Text);
      ADDValue('AD_INFO3', edt_AdInfo3.Text);
      ADDValue('AD_INFO4', edt_AdInfo4.Text);
      ADDValue('AD_INFO5', edt_AdInfo5.Text);
      //신용장번호
      ADDValue('CD_NO', edt_cdNo.Text);
      //개설일자
      ADDValue('ISS_DATE', msk_IssDate.Text);
      //유효일자/장소
      ADDValue('EX_DATE', msk_exDate.Text);
      ADDValue('EX_PLACE', edt_exPlace.Text);
      //취소요청
      if com_isCancel.ItemIndex = 1 then
        ADDValue('IS_CANCEL', 'CR')
      else
        ADDValue('IS_CANCEL', '');

      ADDValue('DOC_CD', edt_Doccd.Text);
      ADDValue('PSHIP', CM_TEXT(com_PSHIP, [':']));
      ADDValue('TSHIP', CM_TEXT(com_TSHIP, [':']));

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          Clipboard.AsText := SQLCreate.FieldList;
          SQL.Text := SQLCreate.CreateSQL;
          Clipboard.AsText := SQL.Text;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;

      //------------------------------------------------------------------------------
      // APP707_2
      //------------------------------------------------------------------------------
      case ProgramControlType of
        ctInsert:
          begin
            DMLType := dmlInsert;
            ADDValue('MAINT_NO', Result.MAINT_NO);
            ADDValue('MSEQ', Result.MSEQ);
            ADDValue('AMD_NO', Result.AMD_NO);
          end;

        ctModify:
          begin
            DMLType := dmlUpdate;
            ADDWhere('MAINT_NO', Result.MAINT_NO);
            ADDWhere('MSEQ', Result.MSEQ);
            ADDWhere('AMD_NO', Result.AMD_NO);
          end;
      end;

      SQLHeader('APP707_2');
//      ADDValue('AMD_NO', Result.AMD_NO);
      //개설의뢰인
      ADDValue('APPLIC1', edt_Applic1.Text);
      ADDValue('APPLIC2', edt_Applic2.Text);
      ADDValue('APPLIC3', edt_Applic3.Text);
      ADDValue('APPLIC4', edt_Applic4.Text);
      ADDValue('APPLIC5', edt_Applic5.Text);

      ADDValue('APPLIC_CHG1', edt_APPCHG1.Text);
      ADDValue('APPLIC_CHG2', edt_APPCHG2.Text);
      ADDValue('APPLIC_CHG3', edt_APPCHG3.Text);
      ADDValue('APPLIC_CHG4', edt_APPCHG4.Text);

      IF Trim(edt_APPCHG1.Text + edt_APPCHG2.Text + edt_APPCHG3.Text + edt_APPCHG4.Text) <> '' Then
        ADDValue('APPLIC_CHG', 1)
      else
        ADDValue('APPLIC_CHG', 0);

//      if sCheckBox1.Checked then
//      begin
//        ADDValue('APPLIC_CHG', 1);
//
//        ADDValue('APPLIC_CHG1', edt_Applic1.Text);
//        ADDValue('APPLIC_CHG2', edt_Applic2.Text);
//        ADDValue('APPLIC_CHG3', edt_Applic3.Text);
//        ADDValue('APPLIC_CHG4', edt_Applic4.Text);
//      end
//      else
//      begin
//        ADDValue('APPLIC_CHG', 0);
//
//        ADDValue('APPLIC_CHG1', '');
//        ADDValue('APPLIC_CHG2', '');
//        ADDValue('APPLIC_CHG3', '');
//        ADDValue('APPLIC_CHG4', '');
//      end;

      if sCheckBox2.Checked then
      begin
        ADDValue('BENEFC_CHG', 1);
        ADDValue('BENEFC1', edt_Benefc1.Text);
        ADDValue('BENEFC2', edt_Benefc2.Text);
        ADDValue('BENEFC3', edt_Benefc3.Text);
        ADDValue('BENEFC4', edt_Benefc4.Text);
        ADDValue('BENEFC5', edt_Benefc5.Text);
      end
      else
      begin
        ADDValue('BENEFC_CHG', 0);
        ADDValue('BENEFC1', '');
        ADDValue('BENEFC2', '');
        ADDValue('BENEFC3', '');
        ADDValue('BENEFC4', '');
        ADDValue('BENEFC5', '');
      end;

      ADDValue('INCD_CUR', edt_IncdCur.Text);
      ADDValue('INCD_AMT', edt_IncdAmt.Value);
      ADDValue('DECD_CUR', edt_DecdCur.Text);
      ADDValue('DECD_AMT', edt_DecdAmt.Value);
      ADDValue('CD_PERP', edt_CdPerp.value);
      ADDValue('CD_PERM', edt_CdPerm.value);

      ADDValue('AA_CV1', edt_aaCcv1.Text);
      ADDValue('AA_CV2', edt_aaCcv2.Text);
      ADDValue('AA_CV3', edt_aaCcv3.Text);
      ADDValue('AA_CV4', edt_aaCcv4.Text);

      ADDValue('LOAD_ON', edt_loadOn.Text);
      ADDValue('FOR_TRAN', edt_forTran.Text);
      ADDValue('LST_DATE', msk_lstDate.Text);

      ADDValue('SHIP_PD1', edt_shipPD1.Text);
      ADDValue('SHIP_PD2', edt_shipPD2.Text);
      ADDValue('SHIP_PD3', edt_shipPD3.Text);
      ADDValue('SHIP_PD4', edt_shipPD4.Text);
      ADDValue('SHIP_PD5', edt_shipPD5.Text);
      ADDValue('SHIP_PD6', edt_shipPD6.Text);

      ADDValue('EX_NAME1', edt_EXName1.Text);
      ADDValue('EX_NAME2', edt_EXName2.Text);
      ADDValue('EX_NAME3', edt_EXName3.Text);
      ADDValue('EX_ADDR1', edt_EXAddr1.Text);
      ADDValue('EX_ADDR2', edt_EXAddr2.Text);

      ADDValue('CARRIAGE', edt_C_METHOD.Text);
      ADDValue('SUNJUCK_PORT', edt_SunjukPort.Text);
      ADDValue('DOCHACK_PORT', edt_dochackPort.Text);

      case com_charge.ItemIndex of
        0:
          begin
            ADDValue('CHARGE', '');
            ADDValue('CHARGE_1', '');
          end;

        1, 2:
          begin
            ADDValue('CHARGE', CM_TEXT(com_charge, [':']));
            ADDValue('CHARGE_1', '');
          end;

        3:
          begin
            ADDValue('CHARGE', '2AH');
            ADDValue('CHARGE_1', memo_charge_1.Text);
          end;
      end;

      case com_amd_charge.ItemIndex of
        0:
          begin
            ADDValue('AMD_CHARGE', '');
            ADDValue('AMD_CHARGE_1', '');
          end;

        1, 2:
          begin
            ADDValue('AMD_CHARGE', CM_TEXT(com_amd_charge, [':']));
            ADDValue('AMD_CHARGE_1', '');
          end;

        3:
          begin
            ADDValue('AMD_CHARGE', '2AE');
            ADDValue('AMD_CHARGE_1', memo_charge_1.Text);
          end;
      end;

      ADDValue('SPECIAL_PAY', memo_SPECIAL_PAY.Text);
      ADDValue('GOODS_DESC', memo_GoodsDesc.Text);
      ADDValue('DOC_REQ', memo_DOCREQ.Text);
      ADDValue('ADD_CONDITION', memo_ADD_CONDITION.Text);
      ADDValue('DRAFT1', edt_DRAFT1.Text);
      ADDValue('DRAFT2', edt_DRAFT2.Text);
      ADDValue('DRAFT3', edt_DRAFT3.Text);
      ADDValue('MIX1', edt_MIX1.Text);
      ADDValue('MIX2', edt_MIX2.Text);
      ADDValue('MIX3', edt_MIX3.Text);
      ADDValue('MIX4', edt_MIX4.Text);
      ADDValue('DEFPAY1', edt_DEFPAY1.Text);
      ADDValue('DEFPAY2', edt_DEFPAY2.Text);
      ADDValue('DEFPAY3', edt_DEFPAY3.Text);
      ADDValue('DEFPAY4', edt_DEFPAY4.Text);
      ADDValue('PERIOD_DAYS', edt_period.Text);
      ADDValue('PERIOD_IDX', com_PeriodIndex.itemindex);
      ADDValue('PERIOD_DETAIL', edt_PeriodDetail.Text);
      ADDValue('CONFIRM', CM_TEXT(com_Confirm, [':']));
      ADDValue('CONFIRM_BIC', edt_CONFIRM_BIC.Text);
      ADDValue('CONFIRM1', edt_CONFIRM1.Text);
      ADDValue('CONFIRM2', edt_CONFIRM2.Text);
      ADDValue('CONFIRM3', edt_CONFIRM3.Text);
      ADDValue('CONFIRM4', edt_CONFIRM4.Text);

      with TADOQuery.Create(nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          Clipboard.AsText := SQLCreate.FieldList;
          SQL.Text := SQLCreate.CreateSQL;
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;
    end;

  finally
    SQLCreate.Free;
  end;
end;

procedure TUI_APP707_NEW_frm.InputControl;
begin
//  edt_Applic1.Enabled := sCheckBox1.Checked and (ProgramControlType <> ctView);
//  sBitBtn2.Enabled := edt_Applic1.Enabled;
//  edt_Applic2.Enabled := edt_Applic1.Enabled;
//  edt_Applic3.Enabled := edt_Applic1.Enabled;
//  edt_Applic4.Enabled := edt_Applic1.Enabled;

  edt_Benefc1.Enabled := sCheckBox2.Checked and (ProgramControlType <> ctView);
  sBitBtn3.Enabled := edt_Benefc1.Enabled;
  edt_Benefc2.Enabled := edt_Benefc1.Enabled;
  edt_Benefc3.Enabled := edt_Benefc1.Enabled;
  edt_Benefc4.Enabled := edt_Benefc1.Enabled;
  edt_Benefc5.Enabled := edt_Benefc1.Enabled;

  edt_loadOn.Enabled := edt_C_METHOD.Text = 'DQ';
  edt_forTran.Enabled := edt_C_METHOD.Text = 'DQ';

  memo_charge_1.Enabled := (com_charge.ItemIndex = 3) and (ProgramControlType <> ctView);
  memo_amd_charge_1.Enabled := (com_amd_charge.ItemIndex = 3) and (ProgramControlType <> ctView);

end;

procedure TUI_APP707_NEW_frm.sCheckBox1Click(Sender: TObject);
begin
  inherited;
  InputControl;
end;

procedure TUI_APP707_NEW_frm.com_chargeSelect(Sender: TObject);
begin
  inherited;
  InputControl;
end;

procedure TUI_APP707_NEW_frm.TXT_SELECT(Sender: TObject; var sMemo: TsMemo);
begin
  case (Sender as TsButton).Tag of
    0:
      sMemo.Lines.Add('/ADD/');
    1:
      sMemo.Lines.Add('/DELETE/');
    2:
      begin
        if sMemo.Text <> '' then
        begin
          if MessageBox(Self.Handle, MSG_APP707_CLEARTXT, '초기화 확인', MB_OKCANCEL + MB_ICONQUESTION) = ID_OK then
          begin
            sMemo.Clear;
            sMemo.Lines.Add('/REPALL/');
          end;
        end
        else
          sMemo.Lines.Add('/REPALL/');
      end;
  end;
end;

procedure TUI_APP707_NEW_frm.sButton10Click(Sender: TObject);
begin
  inherited;
  TXT_SELECT(Sender, memo_SPECIAL_PAY);
end;

procedure TUI_APP707_NEW_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  TXT_SELECT(Sender, memo_GoodsDesc);
end;

procedure TUI_APP707_NEW_frm.sButton4Click(Sender: TObject);
begin
  inherited;
  TXT_SELECT(Sender, memo_DOCREQ);
end;

procedure TUI_APP707_NEW_frm.sButton7Click(Sender: TObject);
begin
  inherited;
  TXT_SELECT(Sender, memo_ADD_CONDITION);
end;

procedure TUI_APP707_NEW_frm.edt_C_METHODExit(Sender: TObject);
var
  uDialog: TCodeDialogParent_frm;
  TempStr: string;
begin
  inherited;
  uDialog := nil;
  if ProgramControlType = ctView then
    Exit;

  case (Sender as TsEdit).Tag of
    //개설방법
    1000:
      uDialog := TCD_OPENDOC_frm.Create(Self);
//    1001: uDialog := TCD_BANK_frm.Create(Self);
//    1002: uDialog := TCD_BIC_frm.Create(Self);
    1003, 1004, 1005, 1006, 1007:
      uDialog := TCD_IMP_CD_frm.Create(Self);
    //신용장종류
    1008:
      uDialog := TCD_DOC_CD_frm.Create(Self);
    //운송수단
    1009:
      uDialog := TCD_C_METHOD_frm.Create(Self);
    //통화
//    1010, 1011: uDialog := TCD_AMT_UNIT_frm.Create(Self);
  end;

  try
    if uDialog = nil then
      Exit;

    TempStr := uDialog.GetCodeText((Sender as TsEdit).Text);

    case (Sender as TsEdit).Tag of
      //개설방법
      1000:
        edt_In_Mathod1.Text := TempStr;
    {*
      //개설의뢰은행
      1001 :
      begin
        edt_ApBank1.Text := uDialog.Values[1];
        edt_ApBank2.Text := uDialog.Values[3];
        edt_ApBank3.Text := uDialog.Values[2];
        edt_ApBank4.Text := uDialog.Values[4];
      end;
    *}
      //수입용도
      1003:
        edt_ImpCd1_1.Text := TempStr;
      1004:
        edt_ImpCd1_2.Text := TempStr;
      1005:
        edt_ImpCd1_3.Text := TempStr;
      1006:
        edt_ImpCd1_4.Text := TempStr;
      1007:
        edt_ImpCd1_5.Text := TempStr;
      //수입신용장종류
      1008:
        edt_Doccd1.Text := TempStr;
      //운송수단
      1009:
        edt_C_METHODNM.Text := TempStr;
    end;
  finally
    FreeAndNil(uDialog);
  end;

end;

procedure TUI_APP707_NEW_frm.DeleteDocument;
var
  maint_no, mseq_no, AMD_NO: string;
  nCursor: Integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin

    Connection := DMMssql.KISConnect;
    maint_no := qryListMAINT_NO.AsString;
    mseq_no := qryListMSeq.AsString;
    AMD_NO := qryListAMD_NO.AsString;
    try
      if MessageBox(Self.Handle, PChar(MSG_SYSTEM_LINE + '취소불능화환신용장 조건변경신청서'#13#10 + maint_no + #13#10 + MSG_SYSTEM_LINE + #13#10 + MSG_SYSTEM_DEL_CONFIRM), '삭제확인', MB_OKCANCEL + MB_ICONQUESTION) = ID_OK then
      begin
        nCursor := qryList.RecNo;
        SQL.Text := 'DELETE FROM APP707_1 WHERE MAINT_NO =' + QuotedStr(maint_no) + ' AND MSEQ = ' + QuotedStr(mseq_no)+' AND AMD_NO = '+AMD_NO;
        ExecSQL;

        SQL.Text := 'DELETE FROM APP707_2 WHERE MAINT_NO =' + QuotedStr(maint_no) + ' AND MSEQ = ' + QuotedStr(mseq_no)+' AND AMD_NO = '+AMD_NO;
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0) and (qryList.RecordCount >= nCursor) then
        begin
          qryList.MoveBy(nCursor - 1);
        end
        else
          qryList.Last;
      end;
    finally
      if DMMssql.KISConnect.InTransaction then
        DMMssql.KISConnect.RollbackTrans;
      Close;
      Free;
    end;

    try

    except
      on E: Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle, PCHAR(MSG_SYSTEM_DEL_ERR + #13#10 + E.Message), '삭제확인', MB_OK + MB_ICONERROR);
      end;
    end;
  end;
end;

procedure TUI_APP707_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
//  inherited;
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_APP707_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_APP707_NEW_frm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_APP707_NEW_frm := nil;
end;

procedure TUI_APP707_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  if AnsiMatchText(qryListCHK2.AsString, ['', '1', '5', '6', '8']) then
    ReadyDocument(qryListMAINT_NO.AsString)
  else
    MessageBox(Self.Handle, MSG_SYSTEM_NOT_SEND, '준비실패', MB_OK + MB_ICONINFORMATION);
end;

procedure TUI_APP707_NEW_frm.ReadyDocument(DocNo: string);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvSeq, RecvFlat: string;
  RecvAmd : Integer;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvSeq := qryListMSeq.AsString;
      RecvAmd := qryListAMD_NO.AsInteger;
//      RecvFlat := APP707(RecvDoc, RecvCode);
      RecvFlat := APP707_NEW(RecvDoc, RecvSeq, IntToStr(RecvAmd), RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'APP707';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MSEQ').Value := RecvSeq;
        Parameters.ParamByName('AMDNO').Value := RecvAmd;
        Parameters.ParamByName('tag').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('TableName').Value := 'APP707_1';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
//        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        qryList.Close;
        qrylist.Open;

        if Assigned(DocumentSend_frm) then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_APP707_NEW_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then
    Exit;
  if qryList.RecordCount = 0 then
    Exit;

  if not AnsiMatchText(qryListCHK2.AsString, ['', '0', '1', '5', '6', '8']) then
  begin
    MessageBox(Self.Handle, MSG_SYSTEM_NOT_DEL, '삭제불가', MB_OK + MB_ICONINFORMATION);
    Exit;
  end;

  DeleteDocument;
end;

procedure TUI_APP707_NEW_frm.sButton13Click(Sender: TObject);
begin
  inherited;
  if not Assigned(LivartModule_frm) then
    LivartModule_frm := TLivartModule_frm.Create(Self);
  try
    LivartModule_frm.sPageControl1.ActivePageIndex := 2;
    LivartModule_frm.ShowModal;

    qryList.Close;
    qrylist.Open;

  finally
    FreeAndNil(LivartModule_frm);
  end;
end;

procedure TUI_APP707_NEW_frm.x1Click(Sender: TObject);
begin
  inherited;
  with ADOStoredProc1 do
  begin
    Close;
    Parameters.Refresh;
    Parameters[1].Value := qryListMAINT_NO.AsString;
    Parameters[2].Value := qryListMSeq.AsString;
    Parameters[3].Value := qryListAMD_NO.AsInteger;
    try
      DMMssql.BeginTrans;
      ExecProc;
      DMMssql.CommitTrans;
      ShowMessage('응답서 작성완료');
    except
      on E: Exception do
      begin
        DMMssql.RollbackTrans;
        ShowMessage('에러가 발생하여 작업이 취소되었습니다'#13#10 + E.Message);
      end;
    end;
  end;
end;

procedure TUI_APP707_NEW_frm.btn_PanelClick(Sender: TObject);
begin
  inherited;
//  APP700_NEW(qryListMAINT_NO.AsString, qryListMSEQ.AsString, 'NETD1');
  APP707_NEW(qryListMAINT_NO.AsString, qryListMSEQ.AsString, qryListAMD_NO.AsString, 'NETD1');
end;

procedure TUI_APP707_NEW_frm.FormActivate(Sender: TObject);
var
  BMK : Pointer;
begin
  inherited;
  BMK := qryList.GetBookmark;
  ReadList;
  try
  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);
  finally
    qryList.FreeBookmark(BMK);
  end;
end;

procedure TUI_APP707_NEW_frm.sComboBox1Change(Sender: TObject);
begin
  inherited;
  sComboBox2.ItemIndex := sComboBox1.ItemIndex; 
end;

procedure TUI_APP707_NEW_frm.sComboBox2Change(Sender: TObject);
begin
  inherited;
  sComboBox1.ItemIndex := sComboBox2.ItemIndex; 
end;

function TUI_APP707_NEW_frm.getAMDNO(MAINT_NO, MSEQ : String): integer;
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT ISNULL(MAX(AMD_NO),0) as AMD_NO FROM APP707_1 WHERE MAINT_NO = '+QuotedStr(MAINT_NO)+' AND MSEQ = '+QuotedStr(MSEQ);
      Open;

      Result := FieldByName('AMD_NO').AsInteger+1;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_APP707_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
//  QR_APP707_PRN_frm.MAINT_NO := qryListMAINT_NO.AsString;
//  QR_APP707_PRN_frm.MSEQ := qryListMSeq.AsInteger;
//  QR_APP707_PRN_frm.AMDNO := qryListAMD_NO.AsInteger;
//  QR_APP707_PRN_frm.Preview;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  QR_APP707_PRN_frm := TQR_APP707_PRN_frm.Create(Self);
  Preview_frm := TPreview_frm.Create(Self);
  try
    with QR_APP707_PRN_frm do
    begin
      MAINT_NO := Self.qryListMAINT_NO.AsString;
      MSEQ := Self.qryListMSEQ.AsInteger;
      AMDNO := Self.qryListAMD_NO.AsInteger;
    end;
    Preview_frm.Report := QR_APP707_PRN_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(QR_APP707_PRN_frm);
  end;
end;

procedure TUI_APP707_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 6;
end;

procedure TUI_APP707_NEW_frm.btnSendClick(Sender: TObject);
begin
  inherited;   
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;

end;

end.

