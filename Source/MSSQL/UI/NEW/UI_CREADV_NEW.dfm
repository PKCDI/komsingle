inherited UI_CREADV_NEW_frm: TUI_CREADV_NEW_frm
  Left = 498
  Top = 97
  BorderWidth = 4
  Caption = '[CREADV] '#51077#44552#53685#51648#49436
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 72
    Align = alTop
    
    TabOrder = 0
    object sSpeedButton4: TsSpeedButton
      Left = 233
      Top = 4
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 72
      Top = 13
      Width = 85
      Height = 23
      Caption = #51077#44552#53685#51648#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 77
      Top = 37
      Width = 75
      Height = 21
      Caption = '(CREADV)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton7: TsSpeedButton
      Left = 1054
      Top = 27
      Width = 8
      Height = 35
      Visible = False
      ButtonStyle = tbsDivider
    end
    object sSpeedButton8: TsSpeedButton
      Left = 377
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 1034
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 243
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 310
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 246
      Top = 41
      Width = 866
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 3
      object sSpeedButton1: TsSpeedButton
        Left = 576
        Top = 37
        Width = 11
        Height = 46
        Visible = False
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 3
        Width = 209
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 336
        Top = 3
        Width = 77
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 720
        Top = 3
        Width = 39
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 816
        Top = 3
        Width = 47
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 464
        Top = 3
        Width = 25
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 329
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 327
      Height = 543
      TabStop = False
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 199
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 94
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 327
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 238
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 61
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnChange = sMaskEdit1Change
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 61
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 0
        OnChange = sMaskEdit1Change
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 154
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        OnChange = sMaskEdit1Change
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 254
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 329
    Top = 72
    Width = 785
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 1
      Width = 783
      Height = 599
      ActivePage = sTabSheet1
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabHeight = 26
      TabOrder = 1
      OnChange = sPageControl1Change
      TabPadding = 10
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 775
          Height = 563
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object Shape1: TShape
            Left = 23
            Top = 48
            Width = 717
            Height = 1
            Brush.Color = clGray
            Pen.Color = clGray
          end
          object msk_ADV_DATE: TsMaskEdit
            Left = 93
            Top = 12
            Width = 92
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            TabOrder = 0
            Color = 12582911
            BoundLabel.Active = True
            BoundLabel.Caption = #53685#51648#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object edt_ADREFNO: TsEdit
            Left = 146
            Top = 62
            Width = 221
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AdInfo5: TsEdit
            Left = 669
            Top = 591
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 5
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AdInfo3: TsEdit
            Left = 669
            Top = 547
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 3
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AdInfo4: TsEdit
            Left = 669
            Top = 569
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 4
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel59: TsPanel
            Left = 1
            Top = 548
            Width = 105
            Height = 23
            SkinData.CustomColor = True
            Caption = #50868#49569#49688#45800
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
            Visible = False
          end
          object com_Carriage: TsComboBox
            Left = 107
            Top = 548
            Width = 237
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 1
            TabOrder = 2
            Text = 'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
            Visible = False
            Items.Strings = (
              ''
              'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
              'DT: '#54644#49345#50868#49569'(Sea)/'#54637#44277#50868#49569'(Air)')
          end
          object sPanel89: TsPanel
            Left = 1
            Top = 572
            Width = 343
            Height = 23
            SkinData.CustomColor = True
            Caption = '[PAGE2] 44A/44B'#47484' '#51089#49457#54616#49464#50836
            Color = 16576211
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            Visible = False
          end
          object msk_VAL_DATE: TsMaskEdit
            Left = 277
            Top = 12
            Width = 92
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            TabOrder = 8
            Color = 12582911
            BoundLabel.Active = True
            BoundLabel.Caption = #51088#44552#49688#52712#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object msk_POS_DATE: TsMaskEdit
            Left = 461
            Top = 12
            Width = 92
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            TabOrder = 9
            Color = 12582911
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#51077#44552#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object msk_EXE_DATE: TsMaskEdit
            Left = 645
            Top = 12
            Width = 92
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            TabOrder = 10
            Color = 12582911
            BoundLabel.Active = True
            BoundLabel.Caption = #51648#44553#51060#54665#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object memo_PAYDET: TsMemo
            Tag = 122
            Left = 146
            Top = 89
            Width = 605
            Height = 112
            Hint = 'DesGood'
            Ctl3D = False
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 25600
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 11
            WordWrap = False
            BoundLabel.Caption = #51077#44552#49345#49464#45236#50669
            BoundLabel.Layout = sclLeftTop
            SkinData.CustomFont = True
          end
          object sPanel2: TsPanel
            Left = 23
            Top = 62
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44592#53440#52280#51312#48264#54840
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 12
          end
          object sPanel8: TsPanel
            Left = 23
            Top = 89
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51077#44552#49345#49464#45236#50669
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 13
          end
          object sPanel29: TsPanel
            Left = 23
            Top = 327
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49688#52712#44552#50529
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 14
          end
          object edt_ORD1AMTC: TsEdit
            Tag = 1016
            Left = 146
            Top = 327
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            MaxLength = 3
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object cur_ORD1AMT: TsCurrencyEdit
            Left = 181
            Top = 327
            Width = 158
            Height = 23
            AutoSize = False
            
            Enabled = False
            TabOrder = 16
            Color = 12582911
            SkinData.CustomColor = True
            DecimalPlaces = 4
            DisplayFormat = '#,0.####'
          end
          object sPanel33: TsPanel
            Left = 23
            Top = 351
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51077#44552#44552#50529
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 17
          end
          object edt_ORD2AMTC: TsEdit
            Tag = 1016
            Left = 146
            Top = 351
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            MaxLength = 3
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object cur_ORD2AMT: TsCurrencyEdit
            Left = 181
            Top = 351
            Width = 158
            Height = 23
            AutoSize = False
            
            Enabled = False
            TabOrder = 19
            Color = 12582911
            SkinData.CustomColor = True
            DecimalPlaces = 4
            DisplayFormat = '#,0.####'
          end
          object sPanel91: TsPanel
            Left = 23
            Top = 375
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51648#44553#44552#50529
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 20
          end
          object edt_ORD3AMTC: TsEdit
            Tag = 1016
            Left = 146
            Top = 375
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            MaxLength = 3
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object cur_ORD3AMT: TsCurrencyEdit
            Left = 181
            Top = 375
            Width = 158
            Height = 23
            AutoSize = False
            
            Enabled = False
            TabOrder = 22
            Color = 12582911
            SkinData.CustomColor = True
            DecimalPlaces = 4
            DisplayFormat = '#,0.####'
          end
          object sPanel95: TsPanel
            Left = 23
            Top = 407
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #54872#51204#44552#50529
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 23
          end
          object edt_ORD4AMTC: TsEdit
            Tag = 1016
            Left = 146
            Top = 407
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            MaxLength = 3
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object cur_ORD4AMT: TsCurrencyEdit
            Left = 181
            Top = 407
            Width = 158
            Height = 23
            AutoSize = False
            
            Enabled = False
            TabOrder = 25
            Color = 12582911
            SkinData.CustomColor = True
            DecimalPlaces = 4
            DisplayFormat = '#,0.####'
          end
          object cur_EXCRATE4: TsCurrencyEdit
            Left = 146
            Top = 431
            Width = 111
            Height = 23
            AutoSize = False
            
            Enabled = False
            TabOrder = 26
            BoundLabel.Caption = #54872#50984
            DecimalPlaces = 4
            DisplayFormat = '#,0.####'
          end
          object msk_EXCDATE41: TsMaskEdit
            Left = 146
            Top = 455
            Width = 96
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            TabOrder = 27
            CheckOnExit = True
            EditMask = '9999-99-99;0'
          end
          object sPanel96: TsPanel
            Left = 23
            Top = 455
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #54872#50984#51201#50857#51068#51088
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 28
          end
          object sPanel97: TsPanel
            Left = 23
            Top = 431
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #54872#50984
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object msk_EXCDATE42: TsMaskEdit
            Left = 243
            Top = 455
            Width = 95
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            TabOrder = 30
            CheckOnExit = True
            EditMask = '9999-99-99;0'
          end
          object edt_RATECD41: TsEdit
            Tag = 1000
            Left = 146
            Top = 479
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            MaxLength = 2
            TabOrder = 31
            SkinData.CustomColor = True
            BoundLabel.Caption = #44060#49444#48169#48277
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_RATECD42: TsEdit
            Tag = 1000
            Left = 304
            Top = 479
            Width = 34
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            MaxLength = 2
            TabOrder = 32
            SkinData.CustomColor = True
            BoundLabel.Caption = #44060#49444#48169#48277
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object sPanel26: TsPanel
            Left = 23
            Top = 479
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44592#51456#53685#54868
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 33
          end
          object sPanel28: TsPanel
            Left = 181
            Top = 479
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #47785#51201#53685#54868
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 34
          end
          object sPanel9: TsPanel
            Left = 23
            Top = 204
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44592#53440#51221#48372'1'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 35
          end
          object memo_ADINFO1: TsMemo
            Tag = 122
            Left = 146
            Top = 204
            Width = 605
            Height = 117
            Hint = 'DesGood'
            Ctl3D = False
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 25600
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 36
            WordWrap = False
            BoundLabel.Caption = #44592#53440#51221#48372'1'
            BoundLabel.Layout = sclLeftTop
            SkinData.CustomFont = True
          end
        end
      end
      object sTabSheet3: TsTabSheet
        Caption = 'PAGE 1'
        object sPanel27: TsPanel
          Left = 0
          Top = 0
          Width = 775
          Height = 563
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object Shape2: TShape
            Left = 17
            Top = 128
            Width = 728
            Height = 1
            Brush.Color = clGray
            Pen.Color = clGray
          end
          object memo_ADINFO2: TsMemo
            Tag = 122
            Left = 140
            Top = 10
            Width = 605
            Height = 103
            Hint = 'DesGood'
            Ctl3D = True
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 25600
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 0
            WordWrap = False
            BoundLabel.Caption = #44592#53440#51221#48372'2'
            BoundLabel.Layout = sclLeftTop
            SkinData.CustomFont = True
          end
          object sPanel10: TsPanel
            Left = 17
            Top = 10
            Width = 122
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #44592#53440#51221#48372'2'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object sPanel1: TsPanel
            Left = 25
            Top = 142
            Width = 113
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49688#51061#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object edt_BN1NAME1: TsEdit
            Left = 139
            Top = 142
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BN1NAME2: TsEdit
            Left = 139
            Top = 166
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BN1NAME3: TsEdit
            Left = 139
            Top = 190
            Width = 110
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 5
            Text = '30582105002'
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BN1PCOD4: TsEdit
            Left = 139
            Top = 214
            Width = 39
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #53685#54868#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BN1BANK: TsEdit
            Left = 139
            Top = 238
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 7
            Text = #50668#51032#46020#44552#50997#49468#53552
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#51061#51088#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BN1BANK1: TsEdit
            Left = 139
            Top = 262
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 8
            Text = 'WELLS FARGO BANK, NA.'
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BN1BANK2: TsEdit
            Left = 139
            Top = 286
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BN1FB: TsEdit
            Left = 139
            Top = 310
            Width = 39
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = 'FTC/BOK'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel11: TsPanel
            Left = 384
            Top = 142
            Width = 113
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51648#44553#51032#47280#51064
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 11
          end
          object edt_OD1NAME1: TsEdit
            Left = 499
            Top = 142
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OD1NAME2: TsEdit
            Left = 499
            Top = 166
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OD1NAME3: TsEdit
            Left = 499
            Top = 190
            Width = 110
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 14
            Text = '30582105002'
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OD1PCOD4: TsEdit
            Left = 499
            Top = 214
            Width = 39
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #53685#54868#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OD1BANK: TsEdit
            Left = 499
            Top = 238
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#51061#51088#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OD1BANK1: TsEdit
            Left = 499
            Top = 262
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OD1BANK2: TsEdit
            Left = 499
            Top = 286
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_OD1FB: TsEdit
            Left = 499
            Top = 310
            Width = 39
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = 'FTC/BOK'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel12: TsPanel
            Left = 25
            Top = 342
            Width = 113
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51473#44036#44221#50976#51064
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 20
          end
          object edt_AX1NAME1: TsEdit
            Left = 139
            Top = 342
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AX1NAME2: TsEdit
            Left = 139
            Top = 366
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.Caption = #44592#53440#52280#51312#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AX1NAME3: TsEdit
            Left = 139
            Top = 390
            Width = 110
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 23
            Text = '30582105002'
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#51340#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AX1PCOD4: TsEdit
            Left = 139
            Top = 414
            Width = 39
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #53685#54868#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AX1BANK: TsEdit
            Left = 139
            Top = 438
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 25
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#51061#51088#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AX1BANK1: TsEdit
            Left = 139
            Top = 462
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AX1BANK2: TsEdit
            Left = 139
            Top = 486
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 27
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AX1FB: TsEdit
            Left = 139
            Top = 510
            Width = 39
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 28
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = 'FTC/BOK'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel13: TsPanel
            Left = 384
            Top = 342
            Width = 113
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #48156#49888#44592#44288' '#51204#51088#49436#47749
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
          end
          object edt_TRN3NAME1: TsEdit
            Left = 498
            Top = 342
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 30
            SkinData.CustomColor = True
            BoundLabel.Caption = #49688#51061#51088#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN3NAME2: TsEdit
            Left = 498
            Top = 366
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 31
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN3NAME3: TsEdit
            Left = 498
            Top = 390
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 32
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel14: TsPanel
            Left = 384
            Top = 438
            Width = 113
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51648#49884#45817#49324#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 33
          end
          object edt_BEBANK1: TsEdit
            Left = 498
            Top = 438
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 34
            SkinData.CustomColor = True
            BoundLabel.Caption = #49688#51061#51088#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BENEFC1: TsEdit
            Left = 498
            Top = 486
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 35
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BEBANK2: TsEdit
            Left = 498
            Top = 462
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 36
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BENEFC2: TsEdit
            Left = 498
            Top = 510
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 37
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
        end
      end
      object sTabSheet2: TsTabSheet
        Caption = 'PAGE 2 - '#51077#44552#44288#47144#49436#47448
        object sPanel34: TsPanel
          Left = 0
          Top = 0
          Width = 775
          Height = 563
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object Shape3: TShape
            Left = 23
            Top = 189
            Width = 728
            Height = 1
            Brush.Color = clGray
            Pen.Color = clGray
          end
          object sPanel15: TsPanel
            Left = 23
            Top = 14
            Width = 113
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #49688#51061#51088'('#49345#54840'/'#51452#49548')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object edt_TRN1NAME1: TsEdit
            Left = 137
            Top = 14
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.Caption = #49688#51061#51088#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN1NAME2: TsEdit
            Left = 137
            Top = 38
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN1NAME3: TsEdit
            Left = 137
            Top = 62
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN1STR1: TsEdit
            Left = 137
            Top = 86
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN1STR2: TsEdit
            Left = 137
            Top = 110
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel16: TsPanel
            Left = 383
            Top = 14
            Width = 128
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #51648#44553#51032#47280#51064'('#49345#54840'/'#51452#49548')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
          object edt_TRN2NAME1: TsEdit
            Left = 513
            Top = 14
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Caption = #49688#51061#51088#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN2NAME2: TsEdit
            Left = 513
            Top = 38
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN2NAME3: TsEdit
            Left = 513
            Top = 62
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN2STR1: TsEdit
            Left = 513
            Top = 86
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN2STR2: TsEdit
            Left = 513
            Top = 110
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sDBGrid2: TsDBGrid
            Left = 24
            Top = 210
            Width = 727
            Height = 319
            Color = clWhite
            Constraints.MaxWidth = 727
            Constraints.MinWidth = 727
            Ctl3D = True
            DataSource = dsDoclist
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 12
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            SkinData.CustomFont = True
            Columns = <
              item
                Alignment = taCenter
                Color = clBtnFace
                Expanded = False
                FieldName = 'SEQ'
                Title.Alignment = taCenter
                Title.Caption = #49692#48264
                Width = 38
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RELIST_NO'
                Title.Caption = #49436#47448#48264#54840
                Width = 164
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ViewType'
                Title.Caption = #44288#47144#49436#47448
                Width = 224
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RELIST_ADDNO'
                Title.Caption = #48512#44032#49436#47448#48264#54840
                Width = 189
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'RELIST_APPDT'
                Title.Alignment = taCenter
                Title.Caption = #47928#49436#48156#54665#51068
                Width = 88
                Visible = True
              end>
          end
          object edt_TRN1STR3: TsEdit
            Left = 137
            Top = 134
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRN2STR3: TsEdit
            Left = 513
            Top = 134
            Width = 234
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
        end
      end
      object sTabSheet8: TsTabSheet
        Caption = 'PAGE 3 - '#48512#44032#49688#49688#47308
        object sPanel30: TsPanel
          Left = 0
          Top = 0
          Width = 775
          Height = 563
          Align = alClient
          
          TabOrder = 0
          object sDBGrid4: TsDBGrid
            Left = 24
            Top = 38
            Width = 727
            Height = 121
            Color = clWhite
            Constraints.MaxWidth = 727
            Constraints.MinWidth = 727
            Ctl3D = True
            DataSource = dsD1
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            SkinData.CustomFont = True
            Columns = <
              item
                Alignment = taCenter
                Color = clBtnFace
                Expanded = False
                FieldName = 'SEQ'
                Title.Alignment = taCenter
                Title.Caption = #49692#48264
                Width = 44
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FC1NM'
                Title.Alignment = taCenter
                Title.Caption = #49688#49688#47308#48512#45812#51088
                Width = 280
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FC1AMT11'
                Title.Alignment = taCenter
                Title.Caption = #49688#49688#47308#54633#44228
                Width = 137
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FC1AMT11C'
                Title.Alignment = taCenter
                Title.Caption = #53685#54868#45800#50948
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FC1AMT12'
                Title.Alignment = taCenter
                Title.Caption = #49688#49688#47308#54633#44228
                Width = 137
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FC1AMT12C'
                Title.Alignment = taCenter
                Title.Caption = #53685#54868#45800#50948
                Width = 52
                Visible = True
              end>
          end
          object sPanel17: TsPanel
            Left = 24
            Top = 14
            Width = 154
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #48512#44032#49688#49688#47308' '#48512#45812#51088
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object sPanel18: TsPanel
            Left = 24
            Top = 166
            Width = 154
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = #48512#44032#49688#49688#47308' '#50976#54805
            Color = 9549311
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 2
          end
          object sDBGrid5: TsDBGrid
            Left = 24
            Top = 190
            Width = 727
            Height = 331
            Color = clWhite
            Constraints.MaxWidth = 727
            Constraints.MinWidth = 727
            Ctl3D = True
            DataSource = dsFC1
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 3
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            SkinData.CustomFont = True
            Columns = <
              item
                Alignment = taCenter
                Color = clBtnFace
                Expanded = False
                FieldName = 'FC1CH'
                Title.Alignment = taCenter
                Title.Caption = #50976#54805
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FC1NM'
                Title.Alignment = taCenter
                Title.Caption = #50976#54805#47749
                Width = 188
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FC1SA1'
                Title.Alignment = taCenter
                Title.Caption = #49688#49688#47308#44552#50529
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FC1SA1C'
                Title.Alignment = taCenter
                Title.Caption = #53685#54868
                Width = 36
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FC1SA2'
                Title.Alignment = taCenter
                Title.Caption = #54872#51204#44552#50529
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FC1SA2C'
                Title.Alignment = taCenter
                Title.Caption = #53685#54868
                Width = 36
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FC1RATE1'
                Title.Alignment = taCenter
                Title.Caption = #44592#51456#53685#54868
                Width = 52
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'FC1RATE2'
                Title.Alignment = taCenter
                Title.Caption = #47785#51201#53685#54868
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FC1RATE'
                Title.Alignment = taCenter
                Title.Caption = #54872#50984
                Width = 99
                Visible = True
              end>
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #45936#51060#53552#51312#54924
        object sDBGrid3: TsDBGrid
          Left = 0
          Top = 32
          Width = 775
          Height = 531
          TabStop = False
          Align = alClient
          Color = clGray
          Ctl3D = False
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.CustomColor = True
          Columns = <
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 82
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 189
              Visible = True
            end
            item
              Color = clWhite
              Expanded = False
              FieldName = 'BN1BANK1'
              Title.Caption = #49688#51061#51088#51008#54665
              Width = 162
              Visible = True
            end
            item
              Color = 12582911
              Expanded = False
              FieldName = 'ORD2AMT'
              Title.Alignment = taCenter
              Title.Caption = #51077#44552#44552#50529
              Width = 114
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'ORD2AMTC'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 36
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'USER_ID'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51088
              Width = 51
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clWhite
              Expanded = False
              FieldName = 'DATEE'
              Title.Caption = #51089#49457#51068#51088
              Width = 97
              Visible = True
            end>
        end
        object sPanel24: TsPanel
          Left = 0
          Top = 0
          Width = 775
          Height = 32
          Align = alTop
          
          TabOrder = 1
          object sSpeedButton12: TsSpeedButton
            Left = 230
            Top = 4
            Width = 11
            Height = 23
            ButtonStyle = tbsDivider
          end
          object sMaskEdit3: TsMaskEdit
            Tag = -1
            Left = 57
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 0
            OnChange = sMaskEdit1Change
            BoundLabel.Active = True
            BoundLabel.Caption = #46321#47197#51068#51088
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sMaskEdit4: TsMaskEdit
            Tag = -1
            Left = 150
            Top = 4
            Width = 78
            Height = 23
            AutoSize = False
            
            MaxLength = 10
            TabOrder = 1
            OnChange = sMaskEdit1Change
            BoundLabel.Active = True
            BoundLabel.Caption = '~'
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object sBitBtn5: TsBitBtn
            Tag = 1
            Left = 469
            Top = 5
            Width = 66
            Height = 23
            Caption = #51312#54924
            TabOrder = 2
            OnClick = sBitBtn1Click
          end
          object sEdit1: TsEdit
            Tag = -1
            Left = 297
            Top = 5
            Width = 171
            Height = 23
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
          end
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 152
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20181231'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT H1.MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, ADV_DATE' +
        ', VAL_DATE, POS_DATE, EXE_DATE, ADREFNO, PAYDET, PAYDET1, ADINFO' +
        '1, ADINFO11, ADINFO2, ADINFO21, ORD1AMT, ORD1AMTC, EXCRATE1, EXC' +
        'DATE11, EXCDATE12, RATECD11, RATECD12, ORD2AMT, ORD2AMTC, EXCRAT' +
        'E2, EXCDATE21, EXCDATE22, RATECD21, RATECD22, ORD3AMT, ORD3AMTC,' +
        ' EXCRATE3, EXCDATE31, EXCDATE32, RATECD31, RATECD32, ORD4AMT, OR' +
        'D4AMTC, EXCRATE4, EXCDATE41, EXCDATE42, RATECD41, RATECD42, BN1N' +
        'AME1, BN1NAME2, BN1NAME3, BN1PCOD4, BN1BANK, BN1BANK1, BN1BANK2,' +
        ' BN1FB, OD1NAME1, OD1NAME2, OD1NAME3, OD1PCOD4, OD1BANK, OD1BANK' +
        '1, OD1BANK2, OD1FB, AX1NAME1, AX1NAME2, AX1NAME3, AX1PCOD4, AX1B' +
        'ANK, AX1BANK1, AX1BANK2, AX1FB, TRN1NAME1, TRN1NAME2, TRN1NAME3,' +
        ' TRN1STR1, TRN1STR2, TRN1STR3, CHK1, CHK2, CHK3, PRNO,'
      
        '       TRN2NAME1, TRN2NAME2, TRN2NAME3, TRN2STR1, TRN2STR2, TRN2' +
        'STR3, TRN3NAME1, TRN3NAME2, TRN3NAME3, BEBANK1, BENEFC1, BEBANK2' +
        ', BENEFC2'
      
        'FROM CREADV_H1 H1 LEFT JOIN CREADV_H2 H2 ON H1.MAINT_NO = H2.MAI' +
        'NT_NO'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(H1.MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 48
    Top = 152
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListADV_DATE: TStringField
      FieldName = 'ADV_DATE'
      Size = 8
    end
    object qryListVAL_DATE: TStringField
      FieldName = 'VAL_DATE'
      Size = 8
    end
    object qryListPOS_DATE: TStringField
      FieldName = 'POS_DATE'
      Size = 8
    end
    object qryListEXE_DATE: TStringField
      FieldName = 'EXE_DATE'
      Size = 8
    end
    object qryListADREFNO: TStringField
      FieldName = 'ADREFNO'
      Size = 35
    end
    object qryListPAYDET: TBooleanField
      FieldName = 'PAYDET'
    end
    object qryListPAYDET1: TMemoField
      FieldName = 'PAYDET1'
      BlobType = ftMemo
    end
    object qryListADINFO1: TBooleanField
      FieldName = 'ADINFO1'
    end
    object qryListADINFO11: TMemoField
      FieldName = 'ADINFO11'
      BlobType = ftMemo
    end
    object qryListADINFO2: TBooleanField
      FieldName = 'ADINFO2'
    end
    object qryListADINFO21: TMemoField
      FieldName = 'ADINFO21'
      BlobType = ftMemo
    end
    object qryListORD1AMT: TBCDField
      FieldName = 'ORD1AMT'
      Precision = 18
    end
    object qryListORD1AMTC: TStringField
      FieldName = 'ORD1AMTC'
      Size = 3
    end
    object qryListEXCRATE1: TBCDField
      FieldName = 'EXCRATE1'
      Precision = 18
    end
    object qryListEXCDATE11: TStringField
      FieldName = 'EXCDATE11'
      Size = 8
    end
    object qryListEXCDATE12: TStringField
      FieldName = 'EXCDATE12'
      Size = 8
    end
    object qryListRATECD11: TStringField
      FieldName = 'RATECD11'
      Size = 3
    end
    object qryListRATECD12: TStringField
      FieldName = 'RATECD12'
      Size = 3
    end
    object qryListORD2AMT: TBCDField
      FieldName = 'ORD2AMT'
      DisplayFormat = '#,0.###'
      Precision = 18
    end
    object qryListORD2AMTC: TStringField
      FieldName = 'ORD2AMTC'
      Size = 3
    end
    object qryListEXCRATE2: TBCDField
      FieldName = 'EXCRATE2'
      Precision = 18
    end
    object qryListEXCDATE21: TStringField
      FieldName = 'EXCDATE21'
      Size = 8
    end
    object qryListEXCDATE22: TStringField
      FieldName = 'EXCDATE22'
      Size = 8
    end
    object qryListRATECD21: TStringField
      FieldName = 'RATECD21'
      Size = 3
    end
    object qryListRATECD22: TStringField
      FieldName = 'RATECD22'
      Size = 3
    end
    object qryListORD3AMT: TBCDField
      FieldName = 'ORD3AMT'
      Precision = 18
    end
    object qryListORD3AMTC: TStringField
      FieldName = 'ORD3AMTC'
      Size = 3
    end
    object qryListEXCRATE3: TBCDField
      FieldName = 'EXCRATE3'
      Precision = 18
    end
    object qryListEXCDATE31: TStringField
      FieldName = 'EXCDATE31'
      Size = 8
    end
    object qryListEXCDATE32: TStringField
      FieldName = 'EXCDATE32'
      Size = 8
    end
    object qryListRATECD31: TStringField
      FieldName = 'RATECD31'
      Size = 3
    end
    object qryListRATECD32: TStringField
      FieldName = 'RATECD32'
      Size = 3
    end
    object qryListORD4AMT: TBCDField
      FieldName = 'ORD4AMT'
      Precision = 18
    end
    object qryListORD4AMTC: TStringField
      FieldName = 'ORD4AMTC'
      Size = 3
    end
    object qryListEXCRATE4: TBCDField
      FieldName = 'EXCRATE4'
      Precision = 18
    end
    object qryListEXCDATE41: TStringField
      FieldName = 'EXCDATE41'
      Size = 8
    end
    object qryListEXCDATE42: TStringField
      FieldName = 'EXCDATE42'
      Size = 8
    end
    object qryListRATECD41: TStringField
      FieldName = 'RATECD41'
      Size = 3
    end
    object qryListRATECD42: TStringField
      FieldName = 'RATECD42'
      Size = 3
    end
    object qryListBN1NAME1: TStringField
      FieldName = 'BN1NAME1'
      Size = 35
    end
    object qryListBN1NAME2: TStringField
      FieldName = 'BN1NAME2'
      Size = 35
    end
    object qryListBN1NAME3: TStringField
      FieldName = 'BN1NAME3'
      Size = 17
    end
    object qryListBN1PCOD4: TStringField
      FieldName = 'BN1PCOD4'
      Size = 3
    end
    object qryListBN1BANK: TStringField
      FieldName = 'BN1BANK'
      Size = 11
    end
    object qryListBN1BANK1: TStringField
      FieldName = 'BN1BANK1'
      Size = 35
    end
    object qryListBN1BANK2: TStringField
      FieldName = 'BN1BANK2'
      Size = 35
    end
    object qryListBN1FB: TStringField
      FieldName = 'BN1FB'
      Size = 3
    end
    object qryListOD1NAME1: TStringField
      FieldName = 'OD1NAME1'
      Size = 35
    end
    object qryListOD1NAME2: TStringField
      FieldName = 'OD1NAME2'
      Size = 35
    end
    object qryListOD1NAME3: TStringField
      FieldName = 'OD1NAME3'
      Size = 17
    end
    object qryListOD1PCOD4: TStringField
      FieldName = 'OD1PCOD4'
      Size = 3
    end
    object qryListOD1BANK: TStringField
      FieldName = 'OD1BANK'
      Size = 11
    end
    object qryListOD1BANK1: TStringField
      FieldName = 'OD1BANK1'
      Size = 35
    end
    object qryListOD1BANK2: TStringField
      FieldName = 'OD1BANK2'
      Size = 35
    end
    object qryListOD1FB: TStringField
      FieldName = 'OD1FB'
      Size = 3
    end
    object qryListAX1NAME1: TStringField
      FieldName = 'AX1NAME1'
      Size = 35
    end
    object qryListAX1NAME2: TStringField
      FieldName = 'AX1NAME2'
      Size = 35
    end
    object qryListAX1NAME3: TStringField
      FieldName = 'AX1NAME3'
      Size = 17
    end
    object qryListAX1PCOD4: TStringField
      FieldName = 'AX1PCOD4'
      Size = 3
    end
    object qryListAX1BANK: TStringField
      FieldName = 'AX1BANK'
      Size = 35
    end
    object qryListAX1BANK1: TStringField
      FieldName = 'AX1BANK1'
      Size = 35
    end
    object qryListAX1BANK2: TStringField
      FieldName = 'AX1BANK2'
      Size = 35
    end
    object qryListAX1FB: TStringField
      FieldName = 'AX1FB'
      Size = 3
    end
    object qryListTRN1NAME1: TStringField
      FieldName = 'TRN1NAME1'
      Size = 35
    end
    object qryListTRN1NAME2: TStringField
      FieldName = 'TRN1NAME2'
      Size = 35
    end
    object qryListTRN1NAME3: TStringField
      FieldName = 'TRN1NAME3'
      Size = 35
    end
    object qryListTRN1STR1: TStringField
      FieldName = 'TRN1STR1'
      Size = 35
    end
    object qryListTRN1STR2: TStringField
      FieldName = 'TRN1STR2'
      Size = 35
    end
    object qryListTRN1STR3: TStringField
      FieldName = 'TRN1STR3'
      Size = 35
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListTRN2NAME1: TStringField
      FieldName = 'TRN2NAME1'
      Size = 35
    end
    object qryListTRN2NAME2: TStringField
      FieldName = 'TRN2NAME2'
      Size = 35
    end
    object qryListTRN2NAME3: TStringField
      FieldName = 'TRN2NAME3'
      Size = 35
    end
    object qryListTRN2STR1: TStringField
      FieldName = 'TRN2STR1'
      Size = 35
    end
    object qryListTRN2STR2: TStringField
      FieldName = 'TRN2STR2'
      Size = 35
    end
    object qryListTRN2STR3: TStringField
      FieldName = 'TRN2STR3'
      Size = 35
    end
    object qryListTRN3NAME1: TStringField
      FieldName = 'TRN3NAME1'
      Size = 35
    end
    object qryListTRN3NAME2: TStringField
      FieldName = 'TRN3NAME2'
      Size = 35
    end
    object qryListTRN3NAME3: TStringField
      FieldName = 'TRN3NAME3'
      Size = 35
    end
    object qryListBEBANK1: TStringField
      FieldName = 'BEBANK1'
      Size = 17
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 17
    end
    object qryListBEBANK2: TStringField
      FieldName = 'BEBANK2'
      Size = 17
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 17
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 152
  end
  object qryFC1: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = '0210243545750'
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, FC1SEQ, FC1CH, NAME as FC1NM, FC1SA1, FC1SA1C,' +
        ' FC1SA2, FC1SA2C, FC1RATE1, FC1RATE2, FC1RATE, FC1RD1, FC1RD2'
      
        'FROM CREADV_FC1 LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE' +
        ' prefix = '#39'5189DEBADV'#39') FC1CH_CD ON FC1CH = CODE'
      'WHERE KEYY = :MAINT_NO'
      'AND SEQ = :SEQ')
    Left = 48
    Top = 216
  end
  object dsFC1: TDataSource
    DataSet = qryFC1
    Left = 80
    Top = 216
  end
  object qryDoclist: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = '1601050195435580'
      end>
    SQL.Strings = (
      
        'SELECT KEYY, SEQ, RELIST_TYPE, DOC_TYPE.NAME, '#39'['#39'+RELIST_TYPE+'#39']' +
        ' '#39'+DOC_TYPE.NAME as ViewType, RELIST_NO, RELIST_ADDNO, RELIST_AP' +
        'PDT'
      
        'FROM CREADV_DOCLIST LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD W' +
        'HERE Prefix = '#39'1001CREADV'#39') DOC_TYPE ON CREADV_DOCLIST.RELIST_TY' +
        'PE = DOC_TYPE.CODE'
      'WHERE KEYY = :MAINT_NO')
    Left = 112
    Top = 152
    object qryDoclistKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDoclistSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDoclistRELIST_TYPE: TStringField
      FieldName = 'RELIST_TYPE'
      Size = 3
    end
    object qryDoclistNAME: TStringField
      FieldName = 'NAME'
      Size = 100
    end
    object qryDoclistViewType: TStringField
      FieldName = 'ViewType'
      ReadOnly = True
      Size = 105
    end
    object qryDoclistRELIST_NO: TStringField
      FieldName = 'RELIST_NO'
      Size = 35
    end
    object qryDoclistRELIST_ADDNO: TStringField
      FieldName = 'RELIST_ADDNO'
      Size = 35
    end
    object qryDoclistRELIST_APPDT: TStringField
      FieldName = 'RELIST_APPDT'
      EditMask = '9999-99-99;0'
      Size = 8
    end
  end
  object dsDoclist: TDataSource
    DataSet = qryDoclist
    Left = 144
    Top = 152
  end
  object qryD1: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryD1AfterOpen
    AfterScroll = qryD1AfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = '0210243545750'
      end>
    SQL.Strings = (
      
        'SELECT [KEYY],[SEQ],[FC1CD],'#39'['#39'+FC1CD+'#39'] '#39'+NAME as [FC1NM], [FC1' +
        'AMT11],[FC1AMT11C],[FC1AMT12],[FC1AMT12C]'
      
        'FROM [CREADV_D1] LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHER' +
        'E prefix = '#39#49688#49688#47308#48512#45812#39') CHARGE ON FC1CD = CODE'
      'WHERE KEYY = :MAINT_NO'
      'ORDER BY KEYY, SEQ')
    Left = 48
    Top = 184
    object qryD1KEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryD1SEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryD1FC1CD: TStringField
      FieldName = 'FC1CD'
      Size = 3
    end
    object qryD1FC1NM: TStringField
      FieldName = 'FC1NM'
      ReadOnly = True
      Size = 106
    end
    object qryD1FC1AMT11: TBCDField
      FieldName = 'FC1AMT11'
      Precision = 18
    end
    object qryD1FC1AMT11C: TStringField
      FieldName = 'FC1AMT11C'
      Size = 3
    end
    object qryD1FC1AMT12: TBCDField
      FieldName = 'FC1AMT12'
      Precision = 18
    end
    object qryD1FC1AMT12C: TStringField
      FieldName = 'FC1AMT12C'
      Size = 3
    end
  end
  object dsD1: TDataSource
    DataSet = qryD1
    Left = 80
    Top = 184
  end
end
