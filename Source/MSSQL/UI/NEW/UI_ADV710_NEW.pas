unit UI_ADV710_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_ADV700_NEW, DB, ADODB, sSkinProvider, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, StdCtrls, sMemo, ComCtrls, sPageControl,
  Buttons, sBitBtn, Grids, DBGrids, acDBGrid, sComboBox, Mask, sMaskEdit,
  sEdit, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, StrUtils, DateUtils;

type
  TUI_ADV710_NEW_frm = class(TUI_ADV700_NEW_frm)
    sPanel143: TsPanel;
    sPanel144: TsPanel;
    qryADV710: TADOQuery;
    qryADV710MAINT_NO: TStringField;
    qryADV710CHK1: TBooleanField;
    qryADV710CHK2: TStringField;
    qryADV710CHK3: TStringField;
    qryADV710MESSAGE1: TStringField;
    qryADV710MESSAGE2: TStringField;
    qryADV710USER_ID: TStringField;
    qryADV710DATEE: TStringField;
    qryADV710APP_DATE: TStringField;
    qryADV710ISS_DATE: TStringField;
    qryADV710PRE_DATE1: TStringField;
    qryADV710PRE_DATE2: TStringField;
    qryADV710PRE_DATE3: TStringField;
    qryADV710PRE_DATE4: TStringField;
    qryADV710APP_NO: TStringField;
    qryADV710LIC_NO: TStringField;
    qryADV710ADV_NO: TStringField;
    qryADV710REF_NO: TStringField;
    qryADV710DRAFT: TStringField;
    qryADV710MIX_PAY: TStringField;
    qryADV710DEF_PAY: TStringField;
    qryADV710ADDINFO: TStringField;
    qryADV710ADDINFO_1: TMemoField;
    qryADV710DOC_CD1: TStringField;
    qryADV710DOC_CD2: TStringField;
    qryADV710DOC_CD3: TStringField;
    qryADV710PRNO: TIntegerField;
    qryADV710DRAFT1: TStringField;
    qryADV710DRAFT2: TStringField;
    qryADV710MIX_PAY1: TStringField;
    qryADV710MIX_PAY2: TStringField;
    qryADV710MIX_PAY3: TStringField;
    qryADV710DEF_PAY1: TStringField;
    qryADV710DEF_PAY2: TStringField;
    qryADV710DEF_PAY3: TStringField;
    qryADV710CO_BANK: TStringField;
    qryADV710CO_BANK1: TStringField;
    qryADV710CO_BANK2: TStringField;
    qryADV710CO_BANK3: TStringField;
    qryADV710CO_BANK4: TStringField;
    qryADV710PERIOD_TXT: TStringField;
    qryADV710SPECIAL_DESC: TStringField;
    qryADV710AA_CV1: TStringField;
    qryADV710AA_CV2: TStringField;
    qryADV710AA_CV3: TStringField;
    qryADV710AA_CV4: TStringField;
    qryADV710PSHIP: TStringField;
    qryADV710TSHIP: TStringField;
    qryADV710DESGOOD: TStringField;
    qryADV710DESGOOD_1: TMemoField;
    qryADV710DOCREQU: TStringField;
    qryADV710DOCREQU_1: TMemoField;
    qryADV710ADDCOND: TStringField;
    qryADV710ADDCOND_1: TMemoField;
    qryADV710CHARGE1: TStringField;
    qryADV710CHARGE2: TStringField;
    qryADV710CHARGE3: TStringField;
    qryADV710CHARGE4: TStringField;
    qryADV710CHARGE5: TStringField;
    qryADV710CHARGE6: TStringField;
    qryADV710PD_PRSNT1: TStringField;
    qryADV710PD_PRSNT2: TStringField;
    qryADV710PD_PRSNT3: TStringField;
    qryADV710PD_PRSNT4: TStringField;
    qryADV710CONFIRMM: TStringField;
    qryADV710INSTRCT: TStringField;
    qryADV710INSTRCT_1: TMemoField;
    qryADV710SND_INFO1: TStringField;
    qryADV710SND_INFO2: TStringField;
    qryADV710SND_INFO3: TStringField;
    qryADV710SND_INFO4: TStringField;
    qryADV710SND_INFO5: TStringField;
    qryADV710SND_INFO6: TStringField;
    qryADV710SE_BANK: TStringField;
    qryADV710SE_BANK1: TStringField;
    qryADV710SE_BANK2: TStringField;
    qryADV710SE_BANK3: TStringField;
    qryADV710SE_BANK4: TStringField;
    qryADV710SE_BANK5: TStringField;
    qryADV710RE_BANK: TStringField;
    qryADV710RE_BANK1: TStringField;
    qryADV710RE_BANK2: TStringField;
    qryADV710RE_BANK3: TStringField;
    qryADV710RE_BANK4: TStringField;
    qryADV710RE_BANK5: TStringField;
    qryADV710RE_TEL: TStringField;
    qryADV710ISS_BANK: TStringField;
    qryADV710ISS_BANK1: TStringField;
    qryADV710ISS_BANK2: TStringField;
    qryADV710ISS_BANK3: TStringField;
    qryADV710ISS_BANK4: TStringField;
    qryADV710ISS_BANK5: TStringField;
    qryADV710ISS_ACCNT: TStringField;
    qryADV710APP_BANK: TStringField;
    qryADV710APP_BANK1: TStringField;
    qryADV710APP_BANK2: TStringField;
    qryADV710APP_BANK3: TStringField;
    qryADV710APP_BANK4: TStringField;
    qryADV710APP_BANK5: TStringField;
    qryADV710AV_AIL: TStringField;
    qryADV710AV_AIL1: TStringField;
    qryADV710AV_AIL2: TStringField;
    qryADV710AV_AIL3: TStringField;
    qryADV710AV_AIL4: TStringField;
    qryADV710AV_AIL5: TStringField;
    qryADV710AV_ACCNT: TStringField;
    qryADV710AV_PAY: TStringField;
    qryADV710DR_AWEE: TStringField;
    qryADV710DR_AWEE1: TStringField;
    qryADV710DR_AWEE2: TStringField;
    qryADV710DR_AWEE3: TStringField;
    qryADV710DR_AWEE4: TStringField;
    qryADV710DR_AWEE5: TStringField;
    qryADV710DR_ACCNT: TStringField;
    qryADV710REI_BANK: TStringField;
    qryADV710REI_BANK1: TStringField;
    qryADV710REI_BANK2: TStringField;
    qryADV710REI_BANK3: TStringField;
    qryADV710REI_BANK4: TStringField;
    qryADV710REI_BANK5: TStringField;
    qryADV710REI_ACCNT: TStringField;
    qryADV710AVT_BANK: TStringField;
    qryADV710AVT_BANK1: TStringField;
    qryADV710AVT_BANK2: TStringField;
    qryADV710AVT_BANK3: TStringField;
    qryADV710AVT_BANK4: TStringField;
    qryADV710AVT_BANK5: TStringField;
    qryADV710AVT_ACCNT: TStringField;
    qryADV710AVT_TEL: TStringField;
    qryADV710APPLIC1: TStringField;
    qryADV710APPLIC2: TStringField;
    qryADV710APPLIC3: TStringField;
    qryADV710APPLIC4: TStringField;
    qryADV710BENEFC1: TStringField;
    qryADV710BENEFC2: TStringField;
    qryADV710BENEFC3: TStringField;
    qryADV710BENEFC4: TStringField;
    qryADV710BENEFC5: TStringField;
    qryADV710EX_NAME1: TStringField;
    qryADV710EX_NAME2: TStringField;
    qryADV710EX_NAME3: TStringField;
    qryADV710EX_ADDR1: TStringField;
    qryADV710EX_ADDR2: TStringField;
    qryADV710BEN_TEL: TStringField;
    qryADV710EX_DATE: TStringField;
    qryADV710EX_PLACE: TStringField;
    qryADV710CD_AMT: TBCDField;
    qryADV710CD_CUR: TStringField;
    qryADV710CD_PERP: TIntegerField;
    qryADV710CD_PERM: TIntegerField;
    qryADV710CD_MAX: TStringField;
    qryADV710LOAD_ON: TStringField;
    qryADV710FOR_TRAN: TStringField;
    qryADV710LST_DATE: TStringField;
    qryADV710SHIP_PD: TStringField;
    qryADV710SHIP_PD1: TStringField;
    qryADV710SHIP_PD2: TStringField;
    qryADV710SHIP_PD3: TStringField;
    qryADV710SHIP_PD4: TStringField;
    qryADV710SHIP_PD5: TStringField;
    qryADV710SHIP_PD6: TStringField;
    qryADV710APPLICABLE_RULES_1: TStringField;
    qryADV710APPLICABLE_RULES_2: TStringField;
    qryADV710SUNJUCK_PORT: TStringField;
    qryADV710DOCHACK_PORT: TStringField;
    qryADV710Non_Bank_Issuer1: TStringField;
    qryADV710Non_Bank_Issuer2: TStringField;
    qryADV710Non_Bank_Issuer3: TStringField;
    qryADV710Non_Bank_Issuer4: TStringField;
    qryADV710APP_ACCNT: TStringField;
    qryADV710SPECIAL_DESC_1: TMemoField;
    qryADV710PERIOD_DAYS: TIntegerField;
    sPanel145: TsPanel;
    sPanel146: TsPanel;
    edt_ISSBANK: TsEdit;
    edt_ISSBANK1: TsEdit;
    sPanel147: TsPanel;
    edt_ISSBANK2: TsEdit;
    edt_ISSBANK3: TsEdit;
    edt_ISSBANK4: TsEdit;
    edt_ISSBANK5: TsEdit;
    sPanel148: TsPanel;
    sPanel149: TsPanel;
    sPanel150: TsPanel;
    edt_NISS_BANK1: TsEdit;
    edt_NISS_BANK2: TsEdit;
    edt_NISS_BANK3: TsEdit;
    sPanel151: TsPanel;
    edt_NISS_BANK4: TsEdit;
    edt_ADV_NO: TsEdit;
    edt_DOCCD2: TsEdit;
    edt_CHARGE4: TsEdit;
    edt_CHARGE5: TsEdit;
    edt_CHARGE6: TsEdit;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryADV710AfterOpen(DataSet: TDataSet);
    procedure qryADV710AfterScroll(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sDBgrid1TitleClick(Column: TColumn);
  private
  protected
    procedure ReadList(SortField: TColumn = nil); override;
    procedure ReadData; override;
    { Private declarations }

  public
    { Public declarations }
  end;

var
  UI_ADV710_NEW_frm: TUI_ADV710_NEW_frm;

implementation

uses
  SQLCreator, MessageDefine, MSSQL, ChildForm, QR_ADV710_PRN, QuickRpt, Preview;

{$R *.dfm}

{ TUI_ADV710_NEW_frm }

procedure TUI_ADV710_NEW_frm.ReadList(SortField: TColumn);
var
  tmpFieldNm : String;
begin
  with qryADV710 do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'A.MAINT_NO';

      IF LeftStr(qryADV710.SQL.Strings[qryADV710.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryADV710.SQL.Strings[qryADV710.SQL.Count-1],4)) = 'ASC' Then
          qryADV710.SQL.Strings[qryADV710.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryADV710.SQL.Strings[qryADV710.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryADV710.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_ADV710_NEW_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
//  inherited;

end;

procedure TUI_ADV710_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  inherited;
  Action := caFree;
  UI_ADV710_NEW_frm := nil;
end;

procedure TUI_ADV710_NEW_frm.ReadData;
begin
  //관리번호
  edt_MAINT_NO.Text := qryADV710MAINT_NO.AsString;
  //수신일자
  msk_Datee.Text := qryADV710DATEE.AsString;
  //사용자
  edt_UserNo.Text := qryADV710USER_ID.AsString;
  CM_INDEX(com_func, [':'], 0, qryADV710MESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryADV710MESSAGE2.AsString);

  //통지일자
  msk_APP_DATE.Text := qryADV710APP_DATE.AsString;
  //통지번호
  edt_APPNO.Text := qryADV710APP_NO.AsString;
  //전문발신은행
  edt_APBANK.Text := qryADV710SE_BANK.AsString;
  edt_APBANK1.Text := qryADV710SE_BANK1.AsString;
  edt_APBANK2.Text := qryADV710SE_BANK2.AsString;
  edt_APBANK3.Text := qryADV710SE_BANK3.AsString;
  edt_APBANK4.Text := qryADV710SE_BANK4.AsString;
  //전문수신은행
  edt_ADBANK.Text := qryADV710RE_BANK.AsString;
  edt_ADBANK1.Text := qryADV710RE_BANK1.AsString;
  edt_ADBANK2.Text := qryADV710RE_BANK2.AsString;
  edt_ADBANK3.Text := qryADV710RE_BANK3.AsString;
  edt_ADBANK4.Text := qryADV710RE_BANK4.AsString;
  edt_ADBANK5.Text := qryADV710RE_BANK5.AsString;
  //기타정보(부가정보)
  memo_ADDINFO1.Lines.Text := qryADV710ADDINFO_1.AsString;
  //신용장
  edt_DOCCD1.Text := qryADV710DOC_CD1.AsString;
  edt_DOCCD2.Text := qryADV710DOC_CD2.AsString;
//  edt_DOCCD3.Text := qryADV710DOC_CD3.AsString;
  edt_CDNO.Text := qryADV710LIC_NO.AsString;
  //Applicable Rules
  edt_APPLICABLERULES1.Text := qryADV710APPLICABLE_RULES_1.AsString;
  edt_APPLICABLERULES2.Text := qryADV710APPLICABLE_RULES_2.AsString;
  //선통지 참조사항
  edt_REEPRE.Text := qryADV710REF_NO.AsString;
  //개설일자
  msk_ISSDATE.Text := qryADV710ISS_DATE.AsString;
  //유효기일
  msk_EXDATE.Text := qryADV710EX_DATE.AsString;
  //유효장소
  edt_EXPLACE.Text := qryADV710EX_PLACE.AsString;
  //개설의뢰인은행
  edt_APPBANK.Text := qryADV710APP_BANK.AsString;
  edt_APPBANK1.Text := qryADV710APP_BANK1.AsString;
  edt_APPBANK2.Text := qryADV710APP_BANK2.AsString;
  edt_APPBANK3.Text := qryADV710APP_BANK3.AsString;
  edt_APPBANK4.Text := qryADV710APP_BANK4.AsString;
  edt_APPACCNT.Text := qryADV710APP_ACCNT.AsString;

  //개설은행
  edt_ISSBANK.Text := qryADV710ISS_BANK.AsString;
  edt_ISSBANK1.Text := qryADV710ISS_BANK1.AsString;
  edt_ISSBANK2.Text := qryADV710ISS_BANK2.AsString;
  edt_ISSBANK3.Text := qryADV710ISS_BANK3.AsString;
  edt_ISSBANK4.Text := qryADV710ISS_BANK4.AsString;
  edt_ISSBANK5.Text := qryADV710ISS_BANK5.AsString;

  //개설의뢰인
  edt_APPLIC1.Text := qryADV710APPLIC1.AsString;
  edt_APPLIC2.Text := qryADV710APPLIC2.AsString;
  edt_APPLIC3.Text := qryADV710APPLIC3.AsString;
  edt_APPLIC4.Text := qryADV710APPLIC4.AsString;
  //수익자
  edt_BENEFC1.Text := qryADV710BENEFC1.AsString;
  edt_BENEFC2.Text := qryADV710BENEFC2.AsString;
  edt_BENEFC3.Text := qryADV710BENEFC3.AsString;
  edt_BENEFC4.Text := qryADV710BENEFC4.AsString;
  edt_BENEFC5.Text := qryADV710BENEFC5.AsString;
  edt_BENEFC6.Text := qryADV710BEN_TEL.AsString;
                                 
  //금액정보
  edt_CDCUR.Text := qryADV710CD_CUR.AsString;
  curr_CDAMT.Value := qryADV710CD_AMT.AsCurrency;
  edt_cdPerP.Text := qryADV710CD_PERP.AsString;
  edt_cdPerM.Text := qryADV710CD_PERM.AsString;

  //신용장 지급방식및 은행
  edt_AVPAY.Text := qryADV710AV_PAY.AsString;
  edt_AVAIL.Text := qryADV710AV_AIL.AsString;
  edt_AVAIL1.Text := qryADV710AV_AIL1.AsString;
  edt_AVAIL2.Text := qryADV710AV_AIL2.AsString;
  edt_AVAIL3.Text := qryADV710AV_AIL3.AsString;
  edt_AVAIL4.Text := qryADV710AV_AIL4.AsString;
  edt_AVACCNT.Text := qryADV710AV_ACCNT.AsString;
  //부가금액부담
  edt_AACV1.Text := qryADV710AA_CV1.AsString;
  edt_AACV2.Text := qryADV710AA_CV2.AsString;
  edt_AACV3.Text := qryADV710AA_CV3.AsString;
  edt_AACV4.Text := qryADV710AA_CV4.AsString;
  //화환어음 조건
  edt_DRAFT1.Text := qryADV710DRAFT.AsString;
  edt_DRAFT2.Text := qryADV710DRAFT1.AsString;
  edt_DRAFT3.Text := qryADV710DRAFT2.AsString;
//  edt_DRAFT4.Text := qryADV710DRAFT4.AsString;
  //혼합지급조건명세
  edt_MIXPAY1.Text := qryADV710MIX_PAY.AsString;
  edt_MIXPAY2.Text := qryADV710MIX_PAY1.AsString;
  edt_MIXPAY3.Text := qryADV710MIX_PAY2.AsString;
  edt_MIXPAY4.Text := qryADV710MIX_PAY3.AsString;
  //연지금조건명세
  edt_DEFPAY1.Text := qryADV710DEF_PAY.AsString;
  edt_DEFPAY2.Text := qryADV710DEF_PAY1.AsString;
  edt_DEFPAY3.Text := qryADV710DEF_PAY2.AsString;
  edt_DEFPAY4.Text := qryADV710DEF_PAY3.AsString;
  //어음지급인
  edt_DRAWEE.Text := qryADV710DR_AWEE.AsString;
  edt_DRAWEE1.Text := qryADV710DR_AWEE1.AsString;
  edt_DRAWEE2.Text := qryADV710DR_AWEE2.AsString;
  edt_DRAWEE3.Text := qryADV710DR_AWEE3.AsString;
  edt_DRAWEE4.Text := qryADV710DR_AWEE4.AsString;
  edt_DRACCNT.Text := qryADV710DR_ACCNT.AsString;
  //선적정보
  edt_PSHIP.Text := qryADV710PSHIP.AsString;
  edt_TSHIP.Text := qryADV710TSHIP.AsString;
  edt_LOADON.Text := qryADV710LOAD_ON.AsString;
  edt_FORTRAN.Text := qryADV710FOR_TRAN.AsString;
  edt_SUNJUCKPORT.Text := qryADV710SUNJUCK_PORT.AsString;
  edt_DOCHACKPORT.Text := qryADV710DOCHACK_PORT.AsString;
  msk_LSTDATE.Text := qryADV710LST_DATE.AsString;
  //선적기간
  edt_SHIPPD1.Text := qryADV710SHIP_PD1.AsString+#13#10+
                      qryADV710SHIP_PD2.AsString+#13#10+
                      qryADV710SHIP_PD3.AsString+#13#10+
                      qryADV710SHIP_PD4.AsString+#13#10+
                      qryADV710SHIP_PD5.AsString+#13#10+
                      qryADV710SHIP_PD6.AsString;
  //수수료부담자
  edt_CHARGE1.Text := qryADV710CHARGE1.AsString;
  edt_CHARGE2.Text := qryADV710CHARGE2.AsString;
  edt_CHARGE3.Text := qryADV710CHARGE3.AsString;
  edt_CHARGE4.Text := qryADV710CHARGE4.AsString;
  edt_CHARGE5.Text := qryADV710CHARGE5.AsString;
  edt_CHARGE6.Text := qryADV710CHARGE6.AsString;
  //서류제시기간
  edt_PERIOD_IN_DAYS.Text := qryADV710PERIOD_DAYS.AsString;
  edt_PERIOD_DETAIL.Text := qryADV710PERIOD_TXT.AsString;
//  edt_PDPRSNT1.Text := qryADV710PD_PRSNT1.AsString;
//  edt_PDPRSNT2.Text := qryADV710PD_PRSNT2.AsString;
//  edt_PDPRSNT3.Text := qryADV710PD_PRSNT3.AsString;
//  edt_PDPRSNT4.Text := qryADV710PD_PRSNT4.AsString;
  //확인지시문언
  edt_CONFIRMM.Text := qryADV710CONFIRMM.AsString;
  //확인은행
  edt_REQ_BANK.Text  := qryADV710CO_BANK.AsString;
  edt_REQ_BANK1.Text := qryADV710CO_BANK1.AsString;
  edt_REQ_BANK2.Text := qryADV710CO_BANK2.AsString;
  edt_REQ_BANK3.Text := qryADV710CO_BANK3.AsString;
  edt_REQ_BANK4.Text := qryADV710CO_BANK4.AsString;
  //상환은행
  edt_REIBANK.Text := qryADV710REI_BANK.AsString;
  edt_REIBANK1.Text := qryADV710REI_BANK1.AsString;
  edt_REIBANK2.Text := qryADV710REI_BANK2.AsString;
  edt_REIBANK3.Text := qryADV710REI_BANK3.AsString;
  edt_REIBANK4.Text := qryADV710REI_BANK4.AsString;
  edt_REIACCMT.Text := qryADV710REI_ACCNT.AsString;
  //상품(용역)명세
  memo_DESGOOD1.Lines.Text := qryADV710DESGOOD_1.AsString;
  //구비서류
  memo_DOCREQU1.Lines.Text := qryADV710DOCREQU_1.AsString;
  //부가조건
  memo_ADDCOND1.Lines.Text := qryADV710ADDCOND_1.AsString;
  //매입/지급/인수은행에 대한 지시사항
  memo_INSTRCT1.Lines.Text := qryADV710INSTRCT_1.AsString;
  //최종통지은행
  edt_AVTBANK.Text := qryADV710AVT_BANK.AsString;
  edt_AVTBANK1.Text := qryADV710AVT_BANK1.AsString;
  edt_AVTBANK2.Text := qryADV710AVT_BANK2.AsString;
  edt_AVTBANK3.Text := qryADV710AVT_BANK3.AsString;
  edt_AVTBANK4.Text := qryADV710AVT_BANK4.AsString;
  edt_AVTACCNT.Text := qryADV710AVT_ACCNT.AsString;
  //통지은행
  edt_EXNAME1.Text := qryADV710EX_NAME1.AsString;
  edt_EXNAME2.Text := qryADV710EX_NAME2.AsString;
  edt_EXNAME3.Text := qryADV710EX_NAME3.AsString;
  edt_EXADDR1.Text := qryADV710EX_ADDR1.AsString;
  edt_EXADDR2.Text := qryADV710EX_ADDR2.AsString;
  //수신앞은행정보
  edt_SNDINFO1.Text := qryADV710SND_INFO1.AsString;
  edt_SNDINFO2.Text := qryADV710SND_INFO2.AsString;
  edt_SNDINFO3.Text := qryADV710SND_INFO3.AsString;
  edt_SNDINFO4.Text := qryADV710SND_INFO4.AsString;
  edt_SNDINFO5.Text := qryADV710SND_INFO5.AsString;
  edt_SNDINFO6.Text := qryADV710SND_INFO6.AsString;

  //비은행개설자
  edt_NISS_BANK1.Text := qryADV710Non_Bank_Issuer1.AsString;
  edt_NISS_BANK2.Text := qryADV710Non_Bank_Issuer2.AsString;
  edt_NISS_BANK3.Text := qryADV710Non_Bank_Issuer3.AsString;
  edt_NISS_BANK4.Text := qryADV710Non_Bank_Issuer4.AsString;

  // 수익자 특별지급조건
  memo_Special.Text := qryADV710SPECIAL_DESC_1.AsString;

  //발신은행 참조사항
  edt_ADV_NO.Text := qryADV710ADV_NO.AsString;
end;

procedure TUI_ADV710_NEW_frm.qryADV710AfterOpen(DataSet: TDataSet);
begin
  IF DataSet.RecordCount = 0 Then qryADV710AfterScroll(DataSet);

  btnDel.Enabled := DataSet.RecordCount > 0;
  btnPrint.Enabled := btnDel.Enabled;
end;

procedure TUI_ADV710_NEW_frm.qryADV710AfterScroll(DataSet: TDataSet);
begin
  ReadData;
end;

procedure TUI_ADV710_NEW_frm.btnDelClick(Sender: TObject);
var
  maint_no : String;
  nCursor : integer;
  sc : TSQLCreate;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.BeginTrans;
  sc := TSQLCreate.Create;
  try
    try
      maint_no := qryADV710MAINT_NO.AsString;
      nCursor := qryADV710.RecNo;
          
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+'타행경유신용장통지서:'#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then
      begin
        DMMssql.RollbackTrans;
        Exit;
      end;

      sc.DMLType := dmlDelete; sc.SQLHeader('ADV7104'); sc.ADDWhere('MAINT_NO', maint_no); ExecSQL(sc.CreateSQL);
      sc.DMLType := dmlDelete; sc.SQLHeader('ADV7103'); sc.ADDWhere('MAINT_NO', maint_no); ExecSQL(sc.CreateSQL);
      sc.DMLType := dmlDelete; sc.SQLHeader('ADV7102'); sc.ADDWhere('MAINT_NO', maint_no); ExecSQL(sc.CreateSQL);
      sc.DMLType := dmlDelete; sc.SQLHeader('ADV710');  sc.ADDWhere('MAINT_NO', maint_no); ExecSQL(sc.CreateSQL);

      DMMssql.CommitTrans;

      qryADV710.Close; qryADV710.Open;

      if nCursor > 1 Then
        qryADV710.MoveBy(nCursor-1);

      MessageBox(Self.Handle,MSG_SYSTEM_DEL_OK,'삭제완료',MB_OK+MB_ICONINFORMATION);
    except
      on e:Exception do
      begin
        DMMssql.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제오류',MB_OK+MB_ICONERROR);
      end;
    end;
  finally
    sc.Free;
  end;
end;

procedure TUI_ADV710_NEW_frm.btnPrintClick(Sender: TObject);
begin

  Preview_frm := TPreview_frm.Create(Self);
  QR_ADV710_PRN_frm := TQR_ADV710_PRN_frm.Create(nil);
  try
    QR_ADV710_PRN_frm.MAINT_NO := qryADV710MAINT_NO.AsString;
    Preview_frm.Report := QR_ADV710_PRN_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(QR_ADV710_PRN_frm);
    FreeAndNil(Preview_frm)
  end;

end;

procedure TUI_ADV710_NEW_frm.FormShow(Sender: TObject);
begin       
  DllPatchExecute('ADV710_ADD_FIELD');
  inherited;
end;

procedure TUI_ADV710_NEW_frm.sDBgrid1TitleClick(Column: TColumn);
begin
  inherited;
  ReadList(Column);
end;

end.
