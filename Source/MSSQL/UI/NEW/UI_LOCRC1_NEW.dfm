inherited UI_LOCRC1_NEW_frm: TUI_LOCRC1_NEW_frm
  Left = 264
  Top = 129
  BorderWidth = 4
  Caption = '[LOCRCT] '#45236#44397#49888#50857#51109' '#47932#54408#49688#47161#51613#47749#49436' - '#49688#54812#50629#51088'['#49688#49888']'
  ClientHeight = 673
  ClientWidth = 1114
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 72
    Align = alTop
    
    TabOrder = 0
    object sSpeedButton5: TsSpeedButton
      Left = 1038
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 44
      Top = 13
      Width = 210
      Height = 23
      Alignment = taCenter
      Caption = #45236#44397#49888#50857#51109' '#47932#54408#49688#47161#51613#47749#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 114
      Top = 36
      Width = 71
      Height = 21
      Alignment = taCenter
      Caption = '(LOCRCT)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 299
      Top = 4
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
    end
    object sSpeedButton2: TsSpeedButton
      Left = 443
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 1040
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 309
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 376
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 305
      Top = 41
      Width = 805
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 3
      object sSpeedButton1: TsSpeedButton
        Left = 592
        Top = -9
        Width = 11
        Height = 46
        Visible = False
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 2
        Width = 269
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 392
        Top = 2
        Width = 77
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        BoundLabel.ParentFont = False
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 656
        Top = 2
        Width = 39
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        BoundLabel.ParentFont = False
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 752
        Top = 2
        Width = 47
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        BoundLabel.ParentFont = False
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 516
        Top = 2
        Width = 25
        Height = 23
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        BoundLabel.ParentFont = False
      end
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 357
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 355
      Height = 543
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dslist
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = DrawColumnCell
      OnTitleClick = sDBGrid1TitleClick
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 217
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 102
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 355
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 250
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 69
        Top = 29
        Width = 171
        Height = 23
        TabOrder = 0
        OnChange = edt_SearchNoChange
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Left = 69
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        OnChange = sMaskEdit1Change
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = 1
        Left = 162
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 2
        OnChange = sMaskEdit1Change
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 269
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPageControl1: TsPageControl [2]
    Left = 357
    Top = 72
    Width = 757
    Height = 601
    ActivePage = sTabSheet5
    Align = alClient
    TabHeight = 26
    TabOrder = 2
    OnChange = sPageControl1Change
    TabPadding = 15
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sPanel7: TsPanel
        Left = 0
        Top = 0
        Width = 749
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object Shape1: TShape
          Left = 21
          Top = 179
          Width = 320
          Height = 1
          Brush.Color = 14540252
          Enabled = False
          Pen.Color = 14540252
        end
        object Shape2: TShape
          Left = 20
          Top = 363
          Width = 320
          Height = 1
          Brush.Color = 14540252
          Enabled = False
          Pen.Color = 14540252
        end
        object sSpeedButton3: TsSpeedButton
          Left = 354
          Top = 15
          Width = 15
          Height = 506
          ButtonStyle = tbsDivider
        end
        object Shape3: TShape
          Left = 382
          Top = 222
          Width = 320
          Height = 1
          Brush.Color = 14540252
          Enabled = False
          Pen.Color = 14540252
        end
        object Shape4: TShape
          Left = 382
          Top = 314
          Width = 320
          Height = 1
          Brush.Color = 14540252
          Enabled = False
          Pen.Color = 14540252
        end
        object msk_GET_DAT: TsMaskEdit
          Left = 75
          Top = 15
          Width = 76
          Height = 23
          AutoSize = False
          Ctl3D = True
          
          Enabled = False
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 0
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          BoundLabel.Active = True
          BoundLabel.Caption = #51064#49688#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20161122'
        end
        object msk_ISS_DAT: TsMaskEdit
          Left = 75
          Top = 39
          Width = 76
          Height = 23
          AutoSize = False
          Ctl3D = True
          
          Enabled = False
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 1
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          BoundLabel.Active = True
          BoundLabel.Caption = #48156#44553#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20161122'
        end
        object msk_EXP_DAT: TsMaskEdit
          Left = 248
          Top = 39
          Width = 92
          Height = 23
          AutoSize = False
          Ctl3D = True
          
          Enabled = False
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 2
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          BoundLabel.Active = True
          BoundLabel.Caption = #47928#49436#50976#54952#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20161122'
        end
        object edt_RFF_NO: TsEdit
          Left = 75
          Top = 63
          Width = 265
          Height = 23
          HelpContext = 1
          TabStop = False
          Color = clBtnFace
          Ctl3D = True
          Enabled = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 3
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #48156#44553#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object msk_BSN_HSCODE: TsMaskEdit
          Left = 248
          Top = 87
          Width = 92
          Height = 23
          AutoSize = False
          Ctl3D = True
          
          Enabled = False
          MaxLength = 12
          ParentCtl3D = False
          TabOrder = 4
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          BoundLabel.Active = True
          BoundLabel.Caption = #45824#54364#44277#44553#47932#54408' HS'#48512#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '0000.00-0000;0;'
          Text = '0000112222'
        end
        object edt_BENEFC: TsEdit
          Tag = 100
          Left = 75
          Top = 124
          Width = 49
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          TabOrder = 5
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#54812#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object edt_BENEFC1: TsEdit
          Left = 75
          Top = 148
          Width = 265
          Height = 23
          Color = 12582911
          Enabled = False
          MaxLength = 35
          TabOrder = 6
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object edt_BENEFC2: TsEdit
          Left = 219
          Top = 124
          Width = 121
          Height = 23
          Color = 12582911
          Enabled = False
          MaxLength = 10
          TabOrder = 7
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APPLIC: TsEdit
          Tag = 101
          Left = 75
          Top = 188
          Width = 49
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          TabOrder = 8
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44060#49444#50629#52404
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object edt_APPLIC1: TsEdit
          Left = 75
          Top = 212
          Width = 265
          Height = 23
          Color = 12582911
          Enabled = False
          MaxLength = 35
          TabOrder = 9
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object edt_APPLIC7: TsEdit
          Left = 219
          Top = 188
          Width = 121
          Height = 23
          Color = 12582911
          Enabled = False
          MaxLength = 10
          TabOrder = 10
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APPLIC2: TsEdit
          Left = 75
          Top = 236
          Width = 265
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 11
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #47749#51032#51064
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APPLIC3: TsEdit
          Left = 75
          Top = 260
          Width = 110
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 12
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APPLIC4: TsEdit
          Left = 75
          Top = 284
          Width = 265
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 13
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APPLIC5: TsEdit
          Left = 75
          Top = 308
          Width = 265
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 14
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_APPLIC6: TsEdit
          Left = 75
          Top = 332
          Width = 265
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 15
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AP_BANK: TsEdit
          Tag = 101
          Left = 75
          Top = 372
          Width = 49
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          TabOrder = 16
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44060#49444#51008#54665'*'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object edt_AP_BANK1: TsEdit
          Left = 75
          Top = 396
          Width = 265
          Height = 23
          Color = 12582911
          Enabled = False
          MaxLength = 35
          TabOrder = 17
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AP_BANK2: TsEdit
          Left = 75
          Top = 420
          Width = 265
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 18
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AP_BANK3: TsEdit
          Left = 75
          Top = 444
          Width = 265
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 19
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51648#51216#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AP_BANK4: TsEdit
          Left = 75
          Top = 468
          Width = 265
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 20
          SkinData.CustomColor = True
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_AP_NAME: TsEdit
          Left = 75
          Top = 492
          Width = 110
          Height = 23
          Color = clWhite
          Enabled = False
          MaxLength = 35
          TabOrder = 21
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object sPanel12: TsPanel
          Left = 373
          Top = 16
          Width = 164
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #45236#44397#49888#50857#51109
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 22
        end
        object edt_LOC_NO: TsEdit
          Left = 473
          Top = 45
          Width = 223
          Height = 23
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = True
          Enabled = False
          MaxLength = 35
          ParentCtl3D = False
          TabOrder = 23
          BoundLabel.Active = True
          BoundLabel.Caption = #45236#44397#49888#50857#51109#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object edt_LOC1AMTC: TsEdit
          Tag = 200
          Left = 473
          Top = 69
          Width = 49
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          TabOrder = 24
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44060#49444#44552#50529'('#50808#54868')'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object curr_LOC1AMT: TsCurrencyEdit
          Left = 523
          Top = 69
          Width = 173
          Height = 23
          AutoSize = False
          CharCase = ecUpperCase
          
          Enabled = False
          TabOrder = 25
          SkinData.SkinSection = 'EDIT'
          DisplayFormat = '#,0.##;0;'
        end
        object curr_EX_RATE: TsCurrencyEdit
          Left = 473
          Top = 93
          Width = 80
          Height = 23
          AutoSize = False
          CharCase = ecUpperCase
          
          Enabled = False
          TabOrder = 26
          BoundLabel.Active = True
          BoundLabel.Caption = #47588#47588#44592#51456#50984
          SkinData.SkinSection = 'EDIT'
          DisplayFormat = '#,0.##;0;'
        end
        object curr_LOC2AMT: TsCurrencyEdit
          Left = 523
          Top = 117
          Width = 173
          Height = 23
          AutoSize = False
          CharCase = ecUpperCase
          
          Enabled = False
          TabOrder = 27
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 0
          DisplayFormat = '#,0;0'
        end
        object edt_LOC2AMTC: TsEdit
          Left = 473
          Top = 117
          Width = 49
          Height = 23
          HelpContext = 1
          TabStop = False
          AutoSize = False
          Color = clBtnFace
          Ctl3D = True
          Enabled = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 28
          Text = 'KRW'
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #44060#49444#44552#50529'('#50896#54868')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object msk_LOADDATE: TsMaskEdit
          Left = 473
          Top = 141
          Width = 80
          Height = 23
          AutoSize = False
          Ctl3D = True
          
          Enabled = False
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 29
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          BoundLabel.Active = True
          BoundLabel.Caption = #47932#54408#51064#46020#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20161122'
        end
        object msk_EXPDATE: TsMaskEdit
          Left = 473
          Top = 165
          Width = 80
          Height = 23
          AutoSize = False
          Ctl3D = True
          
          Enabled = False
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 30
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          BoundLabel.Active = True
          BoundLabel.Caption = #50976#54952#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
          SkinData.CustomColor = True
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20161122'
        end
        object edt_AMAINT_NO: TsEdit
          Left = 473
          Top = 189
          Width = 223
          Height = 23
          HelpContext = 1
          TabStop = False
          Color = clBtnFace
          Ctl3D = True
          Enabled = False
          MaxLength = 35
          ParentCtl3D = False
          TabOrder = 31
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #49888#50857#51109#44288#47532#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.ParentFont = False
        end
        object edt_RCT_AMT1C: TsEdit
          Tag = 201
          Left = 473
          Top = 233
          Width = 49
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          TabOrder = 32
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51064#49688#44552#50529'('#50808#54868')'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object curr_RCT_AMT1: TsCurrencyEdit
          Left = 523
          Top = 233
          Width = 173
          Height = 23
          AutoSize = False
          CharCase = ecUpperCase
          
          Enabled = False
          TabOrder = 33
          SkinData.SkinSection = 'EDIT'
          DisplayFormat = '#,0.##;0;'
        end
        object curr_RATE: TsCurrencyEdit
          Left = 473
          Top = 257
          Width = 80
          Height = 23
          AutoSize = False
          CharCase = ecUpperCase
          
          Enabled = False
          TabOrder = 34
          BoundLabel.Active = True
          BoundLabel.Caption = #54872#50984
          SkinData.SkinSection = 'EDIT'
          DisplayFormat = '#,0.##;0;'
        end
        object edt_RCT_AMT2C: TsEdit
          Left = 473
          Top = 281
          Width = 49
          Height = 23
          HelpContext = 1
          TabStop = False
          Color = clBtnFace
          Ctl3D = True
          Enabled = False
          MaxLength = 35
          ParentCtl3D = False
          ReadOnly = True
          TabOrder = 35
          Text = 'KRW'
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #51064#49688#44552#50529'('#50896#54868')'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object curr_RCT_AMT2: TsCurrencyEdit
          Left = 523
          Top = 281
          Width = 173
          Height = 23
          AutoSize = False
          CharCase = ecUpperCase
          
          Enabled = False
          TabOrder = 36
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 0
          DisplayFormat = '#,0;0'
        end
        object edt_TQTY_G: TsEdit
          Tag = 202
          Left = 473
          Top = 321
          Width = 49
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          TabOrder = 37
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52509' '#54633#44228#49688#47049
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object curr_TQTY: TsCurrencyEdit
          Left = 523
          Top = 321
          Width = 173
          Height = 23
          AutoSize = False
          CharCase = ecUpperCase
          
          Enabled = False
          TabOrder = 38
          SkinData.SkinSection = 'EDIT'
          DisplayFormat = '#,0.##;0;'
        end
        object edt_TAMT_G: TsEdit
          Tag = 203
          Left = 473
          Top = 345
          Width = 49
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          Enabled = False
          TabOrder = 39
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.Caption = #52509' '#54633#44228#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          BoundLabel.ParentFont = False
        end
        object curr_TAMT: TsCurrencyEdit
          Left = 523
          Top = 345
          Width = 173
          Height = 23
          AutoSize = False
          CharCase = ecUpperCase
          
          Enabled = False
          TabOrder = 40
          SkinData.SkinSection = 'EDIT'
          DisplayFormat = '#,0.##;0;'
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = 'PAGE2'
      object sPanel27: TsPanel
        Left = 0
        Top = 0
        Width = 749
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object memo_REMARK1: TsMemo
          Left = 13
          Top = 39
          Width = 692
          Height = 226
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 0
          CharCase = ecUpperCase
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeftTop
          BoundLabel.ParentFont = False
        end
        object sPanel1: TsPanel
          Left = 13
          Top = 15
          Width = 164
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #44592#53440#51312#44148
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 1
        end
        object sPanel2: TsPanel
          Left = 13
          Top = 266
          Width = 164
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          BevelOuter = bvNone
          Caption = #52280#44256#49324#54637
          Color = 16042877
          Ctl3D = False
          
          ParentCtl3D = False
          TabOrder = 2
        end
        object memo_LOC_REM1: TsMemo
          Left = 13
          Top = 290
          Width = 692
          Height = 226
          Ctl3D = True
          Enabled = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 3
          CharCase = ecUpperCase
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = 5197647
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeftTop
          BoundLabel.ParentFont = False
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #49345#54408#47749#49464#45236#50669
      object sPanel8: TsPanel
        Left = 0
        Top = 0
        Width = 749
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sDBGrid2: TsDBGrid
          Left = 1
          Top = 3
          Width = 747
          Height = 291
          Align = alClient
          Color = clWhite
          Ctl3D = True
          DataSource = dsGoods
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnDrawColumnCell = DrawColumnCell
          OnTitleClick = sDBGrid2TitleClick
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Color = clBtnFace
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME1'
              Title.Alignment = taCenter
              Title.Caption = #54408#47749
              Width = 263
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'HS_NO'
              Title.Alignment = taCenter
              Title.Caption = 'HS'#48512#54840
              Width = 85
              Visible = True
            end
            item
              Color = 13434879
              Expanded = False
              FieldName = 'QTY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 65
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clBtnFace
              Expanded = False
              FieldName = 'QTY_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 35
              Visible = True
            end
            item
              Color = 13434879
              Expanded = False
              FieldName = 'PRICE'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032
              Width = 65
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clBtnFace
              Expanded = False
              FieldName = 'PRICE_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 35
              Visible = True
            end
            item
              Color = 13434879
              Expanded = False
              FieldName = 'AMT'
              Title.Alignment = taCenter
              Title.Caption = #44552#50529
              Width = 100
              Visible = True
            end
            item
              Alignment = taCenter
              Color = clBtnFace
              Expanded = False
              FieldName = 'AMT_G'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 35
              Visible = True
            end>
        end
        object sPanel10: TsPanel
          Left = 1
          Top = 1
          Width = 747
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          
          TabOrder = 1
        end
        object sPanel11: TsPanel
          Left = 1
          Top = 296
          Width = 747
          Height = 268
          Align = alBottom
          
          TabOrder = 2
          object sSpeedButton10: TsSpeedButton
            Left = 379
            Top = 15
            Width = 15
            Height = 238
            ButtonStyle = tbsDivider
          end
          object Shape5: TShape
            Left = 394
            Top = 120
            Width = 320
            Height = 1
            Brush.Color = 14540252
            Enabled = False
            Pen.Color = 14540252
          end
          object sPanel22: TsPanel
            Left = 12
            Top = 17
            Width = 69
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #54408#47749
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 0
          end
          object sPanel15: TsPanel
            Left = 216
            Top = 17
            Width = 60
            Height = 23
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'DRAGBAR'
            Caption = 'HS'#48512#54840
            Color = clGray
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
          object msk_HS_NO: TsMaskEdit
            Left = 277
            Top = 17
            Width = 92
            Height = 23
            AutoSize = False
            Ctl3D = True
            
            Enabled = False
            MaxLength = 12
            ParentCtl3D = False
            TabOrder = 2
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '0000.00-0000;0;'
            Text = '0000112222'
          end
          object memo_NAME1: TsMemo
            Left = 12
            Top = 41
            Width = 357
            Height = 89
            Ctl3D = False
            Enabled = False
            ParentCtl3D = False
            TabOrder = 3
          end
          object sButton2: TsButton
            Left = 82
            Top = 17
            Width = 25
            Height = 23
            Caption = '...'
            Enabled = False
            TabOrder = 4
          end
          object sPanel14: TsPanel
            Left = 12
            Top = 137
            Width = 69
            Height = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            BevelOuter = bvNone
            Caption = #44508#44201
            Color = 16042877
            Ctl3D = False
            
            ParentCtl3D = False
            TabOrder = 5
          end
          object memo_SIZE1: TsMemo
            Left = 12
            Top = 161
            Width = 357
            Height = 89
            Ctl3D = False
            Enabled = False
            ParentCtl3D = False
            TabOrder = 6
          end
          object edt_QTY_G: TsEdit
            Tag = 300
            Left = 473
            Top = 17
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_QTY: TsCurrencyEdit
            Left = 523
            Top = 17
            Width = 173
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            
            Enabled = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 3
            DisplayFormat = '#,0.###;0;'
          end
          object edt_PRICE_G: TsEdit
            Tag = 301
            Left = 473
            Top = 41
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45800#44032
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_PRICE: TsCurrencyEdit
            Left = 523
            Top = 41
            Width = 173
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            
            Enabled = False
            TabOrder = 10
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 5
            DisplayFormat = '#,0.#####;0;'
          end
          object edt_QTYG_G: TsEdit
            Tag = 302
            Left = 473
            Top = 65
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45800#44032#44592#51456#49688#47049
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_QTYG: TsCurrencyEdit
            Left = 523
            Top = 65
            Width = 173
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            
            Enabled = False
            TabOrder = 12
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 3
            DisplayFormat = '#,0.###;0;'
          end
          object edt_AMT_G: TsEdit
            Tag = 303
            Left = 473
            Top = 89
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_AMT: TsCurrencyEdit
            Left = 523
            Top = 89
            Width = 173
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            
            Enabled = False
            TabOrder = 14
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 3
            DisplayFormat = '#,0.###;0;'
          end
          object edt_STQTY_G: TsEdit
            Tag = 304
            Left = 473
            Top = 129
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#47049#49548#44228
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_STQTY: TsCurrencyEdit
            Left = 523
            Top = 129
            Width = 173
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            
            Enabled = False
            TabOrder = 16
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 3
            DisplayFormat = '#,0.###;0;'
          end
          object edt_STAMT_G: TsEdit
            Tag = 305
            Left = 473
            Top = 153
            Width = 49
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44552#50529#49548#44228
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_STAMT: TsCurrencyEdit
            Left = 523
            Top = 153
            Width = 173
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            
            Enabled = False
            TabOrder = 18
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 3
            DisplayFormat = '#,0.###;0;'
          end
          object edt_NAME_COD: TsEdit
            Tag = 101
            Left = 108
            Top = 17
            Width = 107
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            Enabled = False
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
        end
        object sPanel13: TsPanel
          Left = 1
          Top = 294
          Width = 747
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alBottom
          
          TabOrder = 3
        end
      end
    end
    object sTabSheet5: TsTabSheet
      Caption = #49464#44552#44228#49328#49436
      object sPanel9: TsPanel
        Left = 0
        Top = 0
        Width = 749
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sPanel17: TsPanel
          Left = 1
          Top = 1
          Width = 747
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alTop
          
          TabOrder = 0
        end
        object sDBGrid4: TsDBGrid
          Left = 1
          Top = 3
          Width = 747
          Height = 561
          Align = alClient
          Color = clWhite
          Ctl3D = True
          DataSource = dsTax
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnDrawColumnCell = DrawColumnCell
          OnTitleClick = sDBGrid4TitleClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Color = clBtnFace
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_NO'
              Title.Alignment = taCenter
              Title.Caption = #44228#49328#49436#48264#54840
              Width = 217
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_DATE'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51068#51088
              Width = 95
              Visible = True
            end
            item
              Color = 13434879
              Expanded = False
              FieldName = 'BILL_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #44277#44553#44032#50529
              Width = 140
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 35
              Visible = True
            end
            item
              Color = 13434879
              Expanded = False
              FieldName = 'TAX_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #49464#50529
              Width = 140
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TAX_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 35
              Visible = True
            end>
        end
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sDBGrid3: TsDBGrid
        Left = 0
        Top = 32
        Width = 749
        Height = 533
        Align = alClient
        Color = clGray
        Ctl3D = False
        DataSource = dslist
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = DrawColumnCell
        SkinData.CustomColor = True
        Columns = <
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = -1
            Visible = False
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = -1
            Visible = False
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 254
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 172
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 164
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'ISS_DAT'
            Title.Alignment = taCenter
            Title.Caption = #48156#44553#51068#51088
            Width = 82
            Visible = True
          end
          item
            Color = clWhite
            Expanded = False
            FieldName = 'LOC1AMT'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#44552#50529
            Width = 97
            Visible = True
          end
          item
            Alignment = taCenter
            Color = clWhite
            Expanded = False
            FieldName = 'LOC1AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 38
            Visible = True
          end>
      end
      object sPanel24: TsPanel
        Left = 0
        Top = 0
        Width = 749
        Height = 32
        Align = alTop
        
        TabOrder = 1
        object sSpeedButton12: TsSpeedButton
          Left = 230
          Top = 4
          Width = 11
          Height = 23
          ButtonStyle = tbsDivider
        end
        object sMaskEdit3: TsMaskEdit
          Tag = 2
          Left = 57
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 0
          OnChange = sMaskEdit1Change
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sMaskEdit4: TsMaskEdit
          Tag = 3
          Left = 150
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          
          MaxLength = 10
          TabOrder = 1
          OnChange = sMaskEdit1Change
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          CheckOnExit = True
          EditMask = '9999-99-99;0'
          Text = '20180621'
        end
        object sBitBtn5: TsBitBtn
          Tag = 1
          Left = 469
          Top = 5
          Width = 66
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
        end
        object sEdit1: TsEdit
          Tag = -1
          Left = 297
          Top = 5
          Width = 171
          Height = 23
          TabOrder = 3
          OnChange = sEdit1Change
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 0
    Top = 80
  end
  object qrylist: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qrylistAfterOpen
    AfterScroll = qrylistAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO,USER_ID,DATEE,MESSAGE1,MESSAGE2,RFF_NO,GET_DAT,I' +
        'SS_DAT,EXP_DAT,BENEFC,BENEFC1,APPLIC,APPLIC1,APPLIC2,APPLIC3,APP' +
        'LIC4,APPLIC5,APPLIC6 '
      
        '            ,RCT_AMT1,RCT_AMT1C,RCT_AMT2,RCT_AMT2C,RATE,REMARK,R' +
        'EMARK1,TQTY,TQTY_G,TAMT,TAMT_G,LOC_NO,AP_BANK,AP_BANK1,AP_BANK2,' +
        'AP_BANK3,AP_BANK4,AP_NAME'
      
        '            ,LOC1AMT,LOC1AMTC,LOC2AMT,LOC2AMTC,EX_RATE,LOADDATE,' +
        'EXPDATE,LOC_REM,LOC_REM1,CHK1,CHK2,CHK3,NEGODT,NEGOAMT,BSN_HSCOD' +
        'E,PRNO,APPLIC7,BENEFC2,CK_S'
      'FROM LOCRC1_H')
    Left = 16
    Top = 232
    object qrylistMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qrylistUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qrylistDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qrylistMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qrylistMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qrylistRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qrylistGET_DAT: TStringField
      FieldName = 'GET_DAT'
      Size = 8
    end
    object qrylistISS_DAT: TStringField
      FieldName = 'ISS_DAT'
      Size = 8
    end
    object qrylistEXP_DAT: TStringField
      FieldName = 'EXP_DAT'
      Size = 8
    end
    object qrylistBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qrylistBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qrylistAPPLIC: TStringField
      FieldName = 'APPLIC'
      Size = 10
    end
    object qrylistAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qrylistAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qrylistAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qrylistAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qrylistAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qrylistAPPLIC6: TStringField
      FieldName = 'APPLIC6'
      Size = 35
    end
    object qrylistRCT_AMT1: TBCDField
      FieldName = 'RCT_AMT1'
      Precision = 18
    end
    object qrylistRCT_AMT1C: TStringField
      FieldName = 'RCT_AMT1C'
      Size = 3
    end
    object qrylistRCT_AMT2: TBCDField
      FieldName = 'RCT_AMT2'
      Precision = 18
    end
    object qrylistRCT_AMT2C: TStringField
      FieldName = 'RCT_AMT2C'
      Size = 3
    end
    object qrylistRATE: TBCDField
      FieldName = 'RATE'
      Precision = 18
    end
    object qrylistREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qrylistREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qrylistTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qrylistTQTY_G: TStringField
      FieldName = 'TQTY_G'
      Size = 3
    end
    object qrylistTAMT: TBCDField
      FieldName = 'TAMT'
      Precision = 18
    end
    object qrylistTAMT_G: TStringField
      FieldName = 'TAMT_G'
      Size = 3
    end
    object qrylistLOC_NO: TStringField
      FieldName = 'LOC_NO'
      Size = 35
    end
    object qrylistAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qrylistAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qrylistAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qrylistAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qrylistAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qrylistAP_NAME: TStringField
      FieldName = 'AP_NAME'
      Size = 17
    end
    object qrylistLOC1AMT: TBCDField
      FieldName = 'LOC1AMT'
      Precision = 18
    end
    object qrylistLOC1AMTC: TStringField
      FieldName = 'LOC1AMTC'
      Size = 3
    end
    object qrylistLOC2AMT: TBCDField
      FieldName = 'LOC2AMT'
      Precision = 18
    end
    object qrylistLOC2AMTC: TStringField
      FieldName = 'LOC2AMTC'
      Size = 3
    end
    object qrylistEX_RATE: TBCDField
      FieldName = 'EX_RATE'
      Precision = 18
    end
    object qrylistLOADDATE: TStringField
      FieldName = 'LOADDATE'
      Size = 8
    end
    object qrylistEXPDATE: TStringField
      FieldName = 'EXPDATE'
      Size = 8
    end
    object qrylistLOC_REM: TStringField
      FieldName = 'LOC_REM'
      Size = 1
    end
    object qrylistLOC_REM1: TMemoField
      FieldName = 'LOC_REM1'
      BlobType = ftMemo
    end
    object qrylistCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qrylistCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qrylistCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qrylistNEGODT: TStringField
      FieldName = 'NEGODT'
      Size = 8
    end
    object qrylistNEGOAMT: TBCDField
      FieldName = 'NEGOAMT'
      Precision = 18
    end
    object qrylistBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qrylistPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qrylistAPPLIC7: TStringField
      FieldName = 'APPLIC7'
      Size = 10
    end
    object qrylistBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 10
    end
    object qrylistCK_S: TStringField
      FieldName = 'CK_S'
      Size = 1
    end
  end
  object dslist: TDataSource
    DataSet = qrylist
    Left = 48
    Top = 232
  end
  object qryGoods: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryGoodsAfterOpen
    AfterScroll = qryGoodsAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R0071401'
      end>
    SQL.Strings = (
      
        'SELECT KEYY,SEQ,HS_CHK,HS_NO,NAME_CHK,NAME_COD,NAME,NAME1,SIZE,S' +
        'IZE1,QTY,QTY_G,QTYG,QTYG_G,PRICE,PRICE_G,AMT,AMT_G,STQTY,STQTY_G' +
        ',STAMT,STAMT_G'
      'FROM LOCRC1_D'
      'WHERE KEYY = :MAINT_NO')
    Left = 16
    Top = 264
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryGoodsHS_CHK: TStringField
      FieldName = 'HS_CHK'
      Size = 1
    end
    object qryGoodsHS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 10
    end
    object qryGoodsNAME_CHK: TStringField
      FieldName = 'NAME_CHK'
      Size = 1
    end
    object qryGoodsNAME_COD: TStringField
      FieldName = 'NAME_COD'
      Size = 35
    end
    object qryGoodsNAME: TStringField
      FieldName = 'NAME'
    end
    object qryGoodsNAME1: TMemoField
      FieldName = 'NAME1'
      OnGetText = qryGoodsNAME1GetText
      BlobType = ftMemo
    end
    object qryGoodsSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryGoodsSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryGoodsQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryGoodsQTYG: TBCDField
      FieldName = 'QTYG'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryGoodsPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryGoodsAMT: TBCDField
      FieldName = 'AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsAMT_G: TStringField
      FieldName = 'AMT_G'
      Size = 3
    end
    object qryGoodsSTQTY: TBCDField
      FieldName = 'STQTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryGoodsSTAMT: TBCDField
      FieldName = 'STAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsSTAMT_G: TStringField
      FieldName = 'STAMT_G'
      Size = 3
    end
  end
  object dsGoods: TDataSource
    DataSet = qryGoods
    Left = 48
    Top = 264
  end
  object qryTax: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'R0071401'
      end>
    SQL.Strings = (
      
        'SELECT KEYY,SEQ,BILL_NO,BILL_DATE,BILL_AMOUNT,BILL_AMOUNT_UNIT,T' +
        'AX_AMOUNT,TAX_AMOUNT_UNIT'
      'FROM  LOCRC1_TAX '
      'WHERE KEYY = :MAINT_NO')
    Left = 16
    Top = 296
    object qryTaxKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryTaxBILL_NO: TStringField
      FieldName = 'BILL_NO'
      Size = 35
    end
    object qryTaxBILL_DATE: TStringField
      FieldName = 'BILL_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryTaxBILL_AMOUNT: TBCDField
      FieldName = 'BILL_AMOUNT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryTaxBILL_AMOUNT_UNIT: TStringField
      FieldName = 'BILL_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxTAX_AMOUNT: TBCDField
      FieldName = 'TAX_AMOUNT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryTaxTAX_AMOUNT_UNIT: TStringField
      FieldName = 'TAX_AMOUNT_UNIT'
      Size = 3
    end
  end
  object dsTax: TDataSource
    DataSet = qryTax
    Left = 48
    Top = 296
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = Letter
    Left = 16
    Top = 328
  end
end
