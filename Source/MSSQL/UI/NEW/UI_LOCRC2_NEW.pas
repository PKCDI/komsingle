unit UI_LOCRC2_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, StdCtrls, sButton, sLabel, Buttons,
  sSpeedButton, ExtCtrls, sPanel, sComboBox, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, sMemo, ComCtrls, sPageControl, sBitBtn, Mask, sMaskEdit, sEdit,
  Grids, DBGrids, acDBGrid, DB, ADODB, StrUtils, DateUtils, TypeDefine, sDialogs,
  Clipbrd, sCheckBox, QuickRpt;

type
  TUI_LOCRC2_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    msk_GET_DAT: TsMaskEdit;
    msk_ISS_DAT: TsMaskEdit;
    msk_EXP_DAT: TsMaskEdit;
    edt_RFF_NO: TsEdit;
    msk_BSN_HSCODE: TsMaskEdit;
    edt_BENEFC: TsEdit;
    edt_BENEFC1: TsEdit;
    edt_BENEFC2: TsEdit;
    edt_APPLIC: TsEdit;
    edt_APPLIC1: TsEdit;
    edt_APPLIC7: TsEdit;
    Shape1: TShape;
    edt_APPLIC2: TsEdit;
    edt_APPLIC3: TsEdit;
    edt_APPLIC4: TsEdit;
    edt_APPLIC5: TsEdit;
    edt_APPLIC6: TsEdit;
    Shape2: TShape;
    edt_AP_BANK: TsEdit;
    edt_AP_BANK1: TsEdit;
    edt_AP_BANK2: TsEdit;
    edt_AP_BANK3: TsEdit;
    edt_AP_BANK4: TsEdit;
    edt_AP_NAME: TsEdit;
    sSpeedButton3: TsSpeedButton;
    sPanel12: TsPanel;
    edt_LOC_NO: TsEdit;
    edt_LOC1AMTC: TsEdit;
    curr_LOC1AMT: TsCurrencyEdit;
    curr_EX_RATE: TsCurrencyEdit;
    curr_LOC2AMT: TsCurrencyEdit;
    edt_LOC2AMTC: TsEdit;
    msk_LOADDATE: TsMaskEdit;
    msk_EXPDATE: TsMaskEdit;
    edt_AMAINT_NO: TsEdit;
    Shape3: TShape;
    edt_RCT_AMT1C: TsEdit;
    curr_RCT_AMT1: TsCurrencyEdit;
    curr_RATE: TsCurrencyEdit;
    edt_RCT_AMT2C: TsEdit;
    curr_RCT_AMT2: TsCurrencyEdit;
    Shape4: TShape;
    edt_TQTY_G: TsEdit;
    curr_TQTY: TsCurrencyEdit;
    edt_TAMT_G: TsEdit;
    curr_TAMT: TsCurrencyEdit;
    btn_TOTAL_CALC: TsButton;
    memo_REMARK1: TsMemo;
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    memo_LOC_REM1: TsMemo;
    sTabSheet2: TsTabSheet;
    sTabSheet5: TsTabSheet;
    sPanel8: TsPanel;
    sPanel9: TsPanel;
    Pan_Detail: TsPanel;
    LIne1: TsSpeedButton;
    Btn_GoodsDel: TsSpeedButton;
    Btn_GoodsEdit: TsSpeedButton;
    LIne2: TsSpeedButton;
    Btn_GoodsOK: TsSpeedButton;
    Btn_GoodsCancel: TsSpeedButton;
    sSpeedButton14: TsSpeedButton;
    Btn_GoodsNew: TsSpeedButton;
    sSpeedButton18: TsSpeedButton;
    sSpeedButton13: TsSpeedButton;
    sDBGrid2: TsDBGrid;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    sPanel13: TsPanel;
    sPanel22: TsPanel;
    sPanel15: TsPanel;
    msk_HS_NO: TsMaskEdit;
    memo_NAME1: TsMemo;
    sButton2: TsButton;
    sPanel14: TsPanel;
    memo_SIZE1: TsMemo;
    qryGoods: TADOQuery;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TIntegerField;
    qryGoodsHS_NO: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsAMT: TBCDField;
    qryGoodsAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsSTAMT: TBCDField;
    qryGoodsSTAMT_G: TStringField;
    qryGoodsASEQ: TIntegerField;
    qryGoodsAMAINT_NO: TStringField;
    dsGoods: TDataSource;
    sSpeedButton10: TsSpeedButton;
    edt_QTY_G: TsEdit;
    curr_QTY: TsCurrencyEdit;
    edt_PRICE_G: TsEdit;
    curr_PRICE: TsCurrencyEdit;
    edt_QTYG_G: TsEdit;
    curr_QTYG: TsCurrencyEdit;
    edt_AMT_G: TsEdit;
    curr_AMT: TsCurrencyEdit;
    edt_STQTY_G: TsEdit;
    curr_STQTY: TsCurrencyEdit;
    edt_STAMT_G: TsEdit;
    curr_STAMT: TsCurrencyEdit;
    sPanel16: TsPanel;
    sSpeedButton11: TsSpeedButton;
    Btn_TaxCancel: TsSpeedButton;
    sSpeedButton17: TsSpeedButton;
    Btn_TaxEdit: TsSpeedButton;
    Btn_TaxNew: TsSpeedButton;
    sSpeedButton25: TsSpeedButton;
    sSpeedButton26: TsSpeedButton;
    sPanel17: TsPanel;
    qryTax: TADOQuery;
    qryTaxSEQ: TBCDField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    qryTaxKEYY: TStringField;
    dsTax: TDataSource;
    sDBGrid4: TsDBGrid;
    edt_NAME_COD: TsEdit;
    Shape5: TShape;
    qrylist: TADOQuery;
    qrylistMAINT_NO: TStringField;
    qrylistUSER_ID: TStringField;
    qrylistDATEE: TStringField;
    qrylistMESSAGE1: TStringField;
    qrylistMESSAGE2: TStringField;
    qrylistRFF_NO: TStringField;
    qrylistGET_DAT: TStringField;
    qrylistISS_DAT: TStringField;
    qrylistEXP_DAT: TStringField;
    qrylistBENEFC: TStringField;
    qrylistBENEFC1: TStringField;
    qrylistAPPLIC: TStringField;
    qrylistAPPLIC1: TStringField;
    qrylistAPPLIC2: TStringField;
    qrylistAPPLIC3: TStringField;
    qrylistAPPLIC4: TStringField;
    qrylistAPPLIC5: TStringField;
    qrylistAPPLIC6: TStringField;
    qrylistRCT_AMT1: TBCDField;
    qrylistRCT_AMT1C: TStringField;
    qrylistRCT_AMT2: TBCDField;
    qrylistRCT_AMT2C: TStringField;
    qrylistRATE: TBCDField;
    qrylistREMARK: TStringField;
    qrylistREMARK1: TMemoField;
    qrylistTQTY: TBCDField;
    qrylistTQTY_G: TStringField;
    qrylistTAMT: TBCDField;
    qrylistTAMT_G: TStringField;
    qrylistLOC_NO: TStringField;
    qrylistAP_BANK: TStringField;
    qrylistAP_BANK1: TStringField;
    qrylistAP_BANK2: TStringField;
    qrylistAP_BANK3: TStringField;
    qrylistAP_BANK4: TStringField;
    qrylistAP_NAME: TStringField;
    qrylistLOC1AMT: TBCDField;
    qrylistLOC1AMTC: TStringField;
    qrylistLOC2AMT: TBCDField;
    qrylistLOC2AMTC: TStringField;
    qrylistEX_RATE: TBCDField;
    qrylistLOADDATE: TStringField;
    qrylistEXPDATE: TStringField;
    qrylistLOC_REM: TStringField;
    qrylistLOC_REM1: TMemoField;
    qrylistCHK1: TBooleanField;
    qrylistCHK2: TStringField;
    qrylistCHK3: TStringField;
    qrylistPRNO: TIntegerField;
    qrylistAMAINT_NO: TStringField;
    qrylistBSN_HSCODE: TStringField;
    qrylistBENEFC2: TStringField;
    qrylistAPPLIC7: TStringField;
    dslist: TDataSource;
    qryReSortTaxSeq: TADOQuery;
    excelOpen: TsOpenDialog;
    sPanel18: TsPanel;
    qryReSortGoodsSeq: TADOQuery;
    sSpeedButton2: TsSpeedButton;
    btnCopy: TsButton;
    chk_autoCalc: TsCheckBox;
    qryReady: TADOQuery;
    QRCompositeReport1: TQRCompositeReport;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure qrylistAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qrylistAfterOpen(DataSet: TDataSet);
    procedure qryGoodsNAME1GetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure qryGoodsAfterScroll(DataSet: TDataSet);
    procedure qryGoodsAfterOpen(DataSet: TDataSet);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure Btn_GoodsNewClick(Sender: TObject);
    procedure Btn_GoodsCancelClick(Sender: TObject);
    procedure edt_BENEFCDblClick(Sender: TObject);
    procedure edt_AP_BANKDblClick(Sender: TObject);
    procedure edt_LOC1AMTCDblClick(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure Btn_TaxNewClick(Sender: TObject);
    procedure sSpeedButton26Click(Sender: TObject);
    procedure sSpeedButton25Click(Sender: TObject);
    procedure edt_QTY_GExit(Sender: TObject);
    procedure edt_AMT_GExit(Sender: TObject);
    procedure edt_PRICE_GExit(Sender: TObject);
    procedure curr_QTYChange(Sender: TObject);
    procedure curr_QTYExit(Sender: TObject);
    procedure Btn_GoodsDelClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure curr_RCT_AMT1Change(Sender: TObject);
    procedure curr_LOC1AMTChange(Sender: TObject);
    procedure btn_TOTAL_CALCClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure sSpeedButton13Click(Sender: TObject);
    procedure sSpeedButton18Click(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
  private
//    FINS_MAINT_NO: string;
    FSubWorkTypeGoods: TProgramControlType;
    procedure ReadList;
    procedure ReadGoods;
    procedure ReadGoodsData;
    procedure ReadTax;
    procedure NewDoc;
    procedure ButtonEnable;
    procedure ButtonSubShow(value: Boolean);
    function getMaintHeader: string;
    procedure ButtonSubEnable;
    procedure ReSortTaxSEQ;
    function getGoodsMaxSeq:integer;
    function CHECK_VALIDITY: String;
    procedure ReadyDocument(DocNo : String);
  protected
    procedure ReadData; override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_LOCRC2_NEW_frm: TUI_LOCRC2_NEW_frm;

implementation

uses
  MSSQL, ICON, AutoNo, VarDefine, dlg_CopyLOCRCTfromLOCAPP,
  dlg_CreateDocumentChoice, CodeContents, dlg_SelectCopyDocument, CD_CUST,
  CodeDialogParent, CD_BANK, CD_AMT_UNIT, CD_UNIT, CD_ITEMLIST, dlg_ADDTAX,
  NewExcelWriter, MessageDefine, NewExcelReader, SQLCreator,
  Dlg_ErrorMessage, CreateDocuments, Dlg_RecvSelect, DocumentSend, Preview,
  LOCRC2_PRINT, LOCRC2_PRINT2;

{$R *.dfm}

{ TUI_LOCRC2_NEW_frm }

procedure TUI_LOCRC2_NEW_frm.ReadData;
begin
  inherited;
  ClearControl(sPanel7);

  edt_MAINT_NO.Text := qrylistMAINT_NO.AsString;
  msk_DATEE.Text := qrylistDATEE.AsString;
  edt_userno.Text := qrylistUSER_ID.AsString;
  //문서기능
  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);
//------------------------------------------------------------------------------
// 기본입력사항
//------------------------------------------------------------------------------
  //인수일자
  msk_GET_DAT.Text := qrylistGET_DAT.AsString;
  //발급일자
  msk_ISS_DAT.Text := qrylistISS_DAT.AsString;
  //문서유효일자
  msk_EXP_DAT.Text := qrylistEXP_DAT.AsString;
  //발급번호
  edt_RFF_NO.Text := qrylistRFF_NO.AsString;
  //대표공급물품 HS부호
  msk_BSN_HSCODE.Text := qrylistBSN_HSCODE.AsString;
//------------------------------------------------------------------------------
// 수혜업체/개설업체
//------------------------------------------------------------------------------
  //수혜자
  edt_BENEFC.Text := qrylistBENEFC.AsString;
  edt_BENEFC1.Text := qrylistBENEFC1.AsString;
  edt_BENEFC2.Text := qrylistBENEFC2.AsString;
  //개설업체
  //코드
  edt_APPLIC.Text := qrylistAPPLIC.AsString;
  //상호
  edt_APPLIC1.Text := qrylistAPPLIC1.AsString;
  //명의인
  edt_APPLIC2.Text := qrylistAPPLIC2.AsString;
  //전자서명
  edt_APPLIC3.Text := qrylistAPPLIC3.AsString;
  //주소1
  edt_APPLIC4.Text := qrylistAPPLIC4.AsString;
  //주소2
  edt_APPLIC5.Text := qrylistAPPLIC5.AsString;
  //주소3
  edt_APPLIC6.Text := qrylistAPPLIC6.AsString;
  //사업자등록번호
  edt_APPLIC7.Text := qrylistAPPLIC7.AsString;
//------------------------------------------------------------------------------
// 개설은행
//------------------------------------------------------------------------------
  //개설은행
  edt_AP_BANK.Text := qrylistAP_BANK.AsString;
  edt_AP_BANK1.Text := qrylistAP_BANK1.AsString;
  edt_AP_BANK2.Text := qrylistAP_BANK2.AsString;
  //지점명
  edt_AP_BANK3.Text := qrylistAP_BANK3.AsString;
  edt_AP_BANK4.Text := qrylistAP_BANK4.AsString;
  //전자서명
  edt_AP_NAME.Text := qrylistAP_NAME.AsString;
//------------------------------------------------------------------------------
// 내국신용장
//------------------------------------------------------------------------------
  //내국신용장 번호
  edt_LOC_NO.Text := qrylistLOC_NO.AsString;
  //개설금액(외화)
  edt_LOC1AMTC.Text := qrylistLOC1AMTC.AsString;
  curr_LOC1AMT.Value := qrylistLOC1AMT.AsCurrency;
  //매매기준율
  curr_EX_RATE.Value := qrylistEX_RATE.AsCurrency;
  //개설금액(원화)
  edt_LOC2AMTC.Text := 'KRW';
  curr_LOC2AMT.Value := qrylistLOC2AMT.AsCurrency;
  //물품인도기일
  msk_LOADDATE.Text := qrylistLOADDATE.AsString;
  //유효일자
  msk_EXPDATE.Text := qrylistEXPDATE.AsString;
  //신용장관리번호
  edt_AMAINT_NO.Text := qrylistAMAINT_NO.AsString;
//------------------------------------------------------------------------------
// 인수금액
//------------------------------------------------------------------------------
  //인수금액(외화)
  edt_RCT_AMT1C.Text := qrylistRCT_AMT1C.AsString;
  curr_RCT_AMT1.Value := qrylistRCT_AMT1.AsCurrency;
  //환율
  curr_RATE.Value := qrylistRATE.AsCurrency;
  //인수금액(원화)
  edt_RCT_AMT2C.Text := qrylistRCT_AMT2C.AsString;
  curr_RCT_AMT2.Value := qrylistRCT_AMT2.AsCurrency;
//------------------------------------------------------------------------------
// 총합계
//------------------------------------------------------------------------------
  //총합계수량
  edt_TQTY_G.Text := qrylistTQTY_G.AsString;
  curr_TQTY.Value := qrylistTQTY.AsCurrency;
  //총합계금액
  edt_TAMT_G.Text := qrylistTAMT_G.AsString;
  curr_TAMT.Value := qrylistTAMT.AsCurrency;
//------------------------------------------------------------------------------
// 기타조건/참고사항
//------------------------------------------------------------------------------
  //기타조건
  memo_REMARK1.Lines.Text := qrylistREMARK1.AsString;
  //참고사항
  memo_LOC_REM1.Lines.Text := qrylistLOC_REM1.AsString;
end;

procedure TUI_LOCRC2_NEW_frm.ReadList;
begin
//  FINS_MAINT_NO := '';

  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    Open;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LOCRC2_NEW_frm := nil;
end;

procedure TUI_LOCRC2_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCRC2_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_LOCRC2_NEW_frm.qrylistAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
  ReadGoods;
  ReadTax;
end;

procedure TUI_LOCRC2_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  FSubWorkTypeGoods := ctView;
  ProgramControlType := ctView;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  sBitBtn1Click(nil);

  ReadOnlyControl(sPanel6);
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel27);
  ReadOnlyControl(sPanel11);
//  EnableControl(sPanel7, False);
//  EnableControl(sPanel27, False);
//  EnableControl(sPanel11, False);

  sPageControl1.ActivePageIndex := 0;

//------------------------------------------------------------------------------
// 패널 위치
//------------------------------------------------------------------------------
  sPanel18.top := 49;
  sPanel18.left := 2;
  sPanel18.Width := 719;
  sPanel18.Height := 218;
end;

procedure TUI_LOCRC2_NEW_frm.qrylistAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then
    qrylistAfterScroll(DataSet);
end;

procedure TUI_LOCRC2_NEW_frm.ReadGoods;
begin
  with qryGoods do
  begin
    Close;
//    if FINS_MAINT_NO = '' then
//      Parameters[0].Value := qrylistMAINT_NO.AsString
//    else
//      Parameters[0].Value := FINS_MAINT_NO;
      Parameters[0].Value := edt_MAINT_NO.Text;
    Open;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.qryGoodsNAME1GetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  inherited;
  Text := Sender.AsString;
end;

procedure TUI_LOCRC2_NEW_frm.ReadGoodsData;
begin
  ClearControl(sPanel11);
//------------------------------------------------------------------------------
// 상품
//------------------------------------------------------------------------------
  edt_NAME_COD.Text := qryGoodsNAME_COD.AsString;
  msk_HS_NO.Text := qryGoodsHS_NO.AsString;
  memo_NAME1.Text := qryGoodsNAME1.AsString;
  memo_SIZE1.Text := qryGoodsSIZE1.AsString;
//------------------------------------------------------------------------------
// 수량/단가/금액
//------------------------------------------------------------------------------
  edt_QTY_G.Text := qryGoodsQTY_G.AsString;
  edt_QTYG_G.Text := qryGoodsQTYG_G.AsString;
  edt_PRICE_G.Text := qryGoodsPRICE_G.AsString;
  edt_AMT_G.Text := qryGoodsAMT_G.AsString;
  edt_STQTY_G.Text := qryGoodsSTQTY_G.AsString;
  edt_STAMT_G.Text := qryGoodsSTAMT_G.AsString;

  curr_QTY.Value := qryGoodsQTY.AsCurrency;
  curr_QTYG.Value := qryGoodsQTYG.AsCurrency;
  curr_PRICE.Value := qryGoodsPRICE.AsCurrency;
  curr_AMT.Value := qryGoodsAMT.AsCurrency;
  curr_STQTY.Value := qryGoodsSTQTY.AsCurrency;
  curr_STAMT.Value := qryGoodsSTAMT.AsCurrency;
end;

procedure TUI_LOCRC2_NEW_frm.qryGoodsAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadGoodsData;
end;

procedure TUI_LOCRC2_NEW_frm.qryGoodsAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then
    qryGoodsAfterScroll(DataSet);
end;

procedure TUI_LOCRC2_NEW_frm.ReadTax;
begin
  with qryTax do
  begin
    Close;
//    if FINS_MAINT_NO = '' then
//      Parameters[0].Value := qrylistMAINT_NO.AsString
//    else
//      Parameters[0].Value := FINS_MAINT_NO;
      Parameters[0].Value := edt_MAINT_NO.Text;
    Open;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  if (Sender as TsButton).Tag = 0 then
    NewDoc;

  case (Sender as TsButton).Tag of
    0:
      ProgramControlType := ctInsert;
    1:
      ProgramControlType := ctModify;
  end;

  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel27, False);

//  EnableControl(sPanel7);
//  EnableControl(sPanel27);

  if ProgramControlType = ctmodify then
  begin
    ReadOnlyControl(sPanel6);  
    EnableControl(sPanel6, false);
  end;
  
  //------------------------------------------------------------------------------
  // 버튼활성화
  //------------------------------------------------------------------------------
  ButtonEnable;
  ButtonSubShow(True);
  sPageControl1.ActivePageIndex := 0;
  DMMssql.BeginTrans;
end;

procedure TUI_LOCRC2_NEW_frm.ButtonEnable;
begin
  inherited;
  btnNew.Enabled := ProgramControlType = ctView;
  btnEdit.Enabled := btnNew.Enabled;
  btnDel.Enabled := btnNew.Enabled;
  btnCopy.Enabled := btnNew.Enabled;
  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := not btnNew.Enabled;
  btnCancel.Enabled := not btnNew.Enabled;
  btnPrint.Enabled := btnNew.Enabled;
  btnReady.Enabled := btnNew.Enabled;
  btnSend.Enabled := btnNew.Enabled;

  sDBGrid1.Enabled := btnNew.Enabled;
  sDBGrid3.Enabled := btnNew.Enabled;
end;

procedure TUI_LOCRC2_NEW_frm.NewDoc;
var
  isCopy: Boolean;
begin
  dlg_CreateDocumentChoice_frm := Tdlg_CreateDocumentChoice_frm.Create(Self);
  with dlg_CreateDocumentChoice_frm do
  begin
    sLabel6.Caption := 'LOCRCT';
    sLabel7.Caption := '내국신용장 물품수령증명서';
    sButton1.Caption := '내국신용장 개설신청서에에서 선택(&2)';
  end;

  dlg_CopyLOCRCTfromLOCAPP_frm := Tdlg_CopyLOCRCTfromLOCAPP_frm.Create(Self);
  try
    try
      case dlg_CreateDocumentChoice_frm.ShowModal of
        mrYes:
          begin
            isCopy := dlg_CopyLOCRCTfromLOCAPP_frm.OpenDialog(0);
            if not isCopy then
              Abort;
          end;
        mrCancel:
          begin
            abort;
          end
      end;

    finally
      FreeAndNil(dlg_CreateDocumentChoice_frm);
    end;

    ReadOnlyControl(sPanel6, False);
    ClearControl(sPanel6);
    ClearControl(sPanel7);
    ClearControl(sPanel27);
    ClearControl(sPanel11);
    sPanel29.Visible := True;

  //------------------------------------------------------------------------------
  // 헤더
  //------------------------------------------------------------------------------
    msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
    edt_userno.Text := LoginData.sID;
    com_func.ItemIndex := 5;
    com_type.ItemIndex := 0;
  //------------------------------------------------------------------------------
  // 개설의뢰인 셋팅
  //------------------------------------------------------------------------------
    if DMCodeContents.GEOLAECHEO.Locate('CODE', '00000', []) then
    begin
      edt_APPLIC.Text := DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
      edt_APPLIC1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
      edt_APPLIC2.Text := DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
      edt_APPLIC3.Text := DMCodeContents.GEOLAECHEO.FieldByName('jenja').AsString;
      edt_APPLIC4.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
      edt_APPLIC5.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
      edt_APPLIC6.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR3').AsString;
      edt_APPLIC7.Text := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
    end;
  //------------------------------------------------------------------------------
  // 공통사항
  //------------------------------------------------------------------------------
    msk_GET_DAT.Text := FormatDateTime('YYYYMMDD', Now);
    msk_ISS_DAT.Text := FormatDateTime('YYYYMMDD', Now);

    edt_MAINT_NO.Text := getMaintHeader + '-' + DMAutoNo.GetDocumentNoAutoInc('LOCRCT');
    edt_RFF_NO.Text := edt_MAINT_NO.Text;

    //신규시 KEY값을 미리 저장
//    FINS_MAINT_NO := edt_MAINT_NO.Text;

    edt_LOC2AMTC.Text := 'KRW';
    edt_RCT_AMT2C.Text := 'KRW';
  //------------------------------------------------------------------------------
  // 복사본
  //------------------------------------------------------------------------------
    edt_AMAINT_NO.ReadOnly := False;
    if isCopy then
    begin
      with dlg_CopyLOCRCTfromLOCAPP_frm do
      begin
        //수혜자
        edt_BENEFC.Text := FieldString('BENEFC');
        edt_BENEFC1.Text := FieldString('BENEFC1');
        edt_BENEFC2.Text := StringReplace(FieldString('BENEFC3'),'-','',[rfReplaceAll]);


        //대표물품 세번부호
        msk_BSN_HSCODE.Text := FieldString('BSN_HSCODE');

        //개설은행
        edt_AP_BANK.Text := FieldString('AP_BANK');
        edt_AP_BANK1.Text := FieldString('AP_BANK1');
        edt_AP_BANK2.Text := '';
        edt_AP_BANK3.Text := FieldString('AP_BANK2');
        edt_AP_BANK4.Text := '';
        edt_AP_NAME.Text := '';

        //내국신용장번호
        edt_LOC_NO.Text := FieldString('LCNO');
        //개설금액
        edt_LOC1AMTC.Text := FieldString('LOC_AMTC');
        curr_LOC1AMT.Value := FieldCurr('LOC_AMT');
        //물품인도기일
        msk_LOADDATE.Text := FieldString('DELIVERY');
        //유효일자
        msk_EXPDATE.Text := FieldString('EXPIRY');

        edt_AMAINT_NO.Text := Values[0];
        edt_AMAINT_NO.ReadOnly := True;
      end;
    end;

    if edt_AMAINT_NO.ReadOnly then
      edt_AMAINT_NO.Color := clBtnFace
    else
      edt_AMAINT_NO.Color := clWhite;

    ReadGoods;
    ReadTax;
  finally
    FreeAndNil(dlg_CopyLOCRCTfromLOCAPP_frm);
  end;
end;

procedure TUI_LOCRC2_NEW_frm.btnCancelClick(Sender: TObject);
var
  DOCNO: string;
  SC : TSQLCreate;
  slSQL:TStringList;
begin
  inherited;
  IF (Sender as TsButton).Tag IN [0,1] Then
  begin
    IF not Btn_GoodsNew.Enabled Then
    begin
      sPageControl1.ActivePageIndex := 2;
      ShowMessage('상품명세내역을 입력중입니다');
      Exit;
    end
    else
    IF not Btn_TaxNew.Enabled Then
    begin
      sPageControl1.ActivePageIndex := 3;
      ShowMessage('세금계산서를 입력중입니다');
      Exit;
    end;
  end;

  //--------------------------------------------------------------------------
  // 유효성 검사
  //--------------------------------------------------------------------------
  if (Sender as TsButton).Tag = 1 then
  begin
    Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
    if Dlg_ErrorMessage_frm.Run_ErrorMessage( '내국신용장 물품수령 증명서' ,CHECK_VALIDITY ) Then
    begin
      FreeAndNil(Dlg_ErrorMessage_frm);
      Exit;
    end;
  end;

  //------------------------------------------------------------------------------
  // 공통데이터저장
  //------------------------------------------------------------------------------
  SC := TSQLCreate.Create;
  with SC do
  begin
    DocNo := edt_MAINT_NO.Text;

  //----------------------------------------------------------------------
  // 문서헤더 및 LOCRC2_H.DB
  //----------------------------------------------------------------------

    Case ProgramControlType of
      ctInsert :
      begin
        DMLType := dmlInsert;
        ADDValue('MAINT_NO',DocNo);
      end;

      ctModify :
      begin
        DMLType := dmlUpdate;
        ADDWhere('MAINT_NO',DocNo);
      end;
    end;

    SQLHeader('LOCRC2_H');

     //문서저장구분(0:임시, 1:저장)
    ADDValue('CHK2',IntToStr((Sender as TsButton).Tag));
    //사용자
    ADDValue('USER_ID',edt_userno.Text);
    //등록일자
    ADDValue('DATEE',msk_DATEE.Text);
    //메세지1
    ADDValue('MESSAGE1',CM_TEXT(com_func,[':']));
    //메세지2
    ADDValue('MESSAGE2',CM_TEXT(com_type,[':']));
    //인수일자
    ADDValue('GET_DAT',msk_GET_DAT.Text);
    //발급일자
    ADDValue('ISS_DAT',msk_ISS_DAT.Text);
    //문서유효일자
    ADDValue('EXP_DAT',msk_EXP_DAT.Text);
    //발급번호
    ADDValue('RFF_NO',edt_RFF_NO.Text);
    //대표공급물품 HS부호
    ADDValue('BSN_HSCODE',msk_BSN_HSCODE.Text);
    //수혜업체
    ADDValue('BENEFC',edt_BENEFC.Text);
    ADDValue('BENEFC1',edt_BENEFC1.Text);
    //수혜업체 사업자등록번호
    ADDValue('BENEFC2',edt_BENEFC2.Text);
    //개설업체
    ADDValue('APPLIC',edt_APPLIC.Text);
    ADDValue('APPLIC1',edt_APPLIC1.Text);
    //개설업체 사업자등록번호
    ADDValue('APPLIC7',edt_APPLIC7.Text);
    //명의인
    ADDValue('APPLIC2',edt_APPLIC2.Text);
    //전자서명
    ADDValue('APPLIC3',edt_APPLIC3.Text);
    //주소
    ADDValue('APPLIC4',edt_APPLIC4.Text);
    ADDValue('APPLIC5',edt_APPLIC5.Text);
    ADDValue('APPLIC6',edt_APPLIC6.Text);
    //개설은행
    ADDValue('AP_BANK',edt_AP_BANK.Text);
    ADDValue('AP_BANK1',edt_AP_BANK1.Text);
    ADDValue('AP_BANK2',edt_AP_BANK2.Text);
    //은행(부)지점명
    ADDValue('AP_BANK3',edt_AP_BANK3.Text);
    ADDValue('AP_BANK4',edt_AP_BANK4.Text);
    //전자서명
    ADDValue('AP_NAME',edt_AP_NAME.Text);
    //내국신용장번호
    ADDValue('LOC_NO',edt_LOC_NO.Text);
    //개설금액(외화)
    ADDValue('LOC1AMTC',edt_LOC1AMTC.Text);
    ADDValue('LOC1AMT',curr_LOC1AMT.Text);
    //매매기준율
    ADDValue('EX_RATE',curr_EX_RATE.value);
    //개설금액(원화)
    ADDValue('LOC2AMTC',edt_LOC2AMTC.Text);
    ADDValue('LOC2AMT',curr_LOC2AMT.value);
    //물품인도기일
    ADDValue('LOADDATE',msk_LOADDATE.Text);
    //유효일자
    ADDValue('EXPDATE',msk_EXPDATE.Text);
    //신용장관리번호
    ADDValue('AMAINT_NO',edt_AMAINT_NO.Text);
    //인수금액(외화)
    ADDValue('RCT_AMT1C',edt_RCT_AMT1C.Text);
    ADDValue('RCT_AMT1',curr_RCT_AMT1.Value);
    //환율
    ADDValue('RATE',curr_RATE.Value);
    //인수금액(원화)
    ADDValue('RCT_AMT2C',edt_RCT_AMT2C.Text);
    ADDValue('RCT_AMT2',curr_RCT_AMT2.Value);
    //총합계수량
    ADDValue('TQTY_G',edt_TQTY_G.Text);
    ADDValue('TQTY',curr_TQTY.Value);
    //총합계금액
    ADDValue('TAMT_G',edt_TAMT_G.Text);
    ADDValue('TAMT',curr_TAMT.Value);
    //기타조건
    ADDValue('REMARK1',memo_REMARK1.Text);
    //참고사항
    ADDValue('LOC_REM1',memo_LOC_REM1.Text);
  end;

  with TADOQuery.Create(nil) do
  begin
    try
//      slSQL := TStringList.Create;
      Connection := DMMssql.KISConnect;
      SQL.Text := SC.CreateSQL;
//      ShowMessage(sql.Text);
//      slSQL.Text := sql.Text;
//      slSQL.SaveToFile('C:\TEST.TXT');
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;

//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;
  FSubWorkTypeGoods := ctView;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  ReadOnlyControl(sPanel6);
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel11);
  ReadOnlyControl(sPanel27);
//  EnableControl(sPanel7, False);
//  EnableControl(sPanel11, False);
//  EnableControl(sPanel27, False);

//------------------------------------------------------------------------------
// 버튼활성화
//------------------------------------------------------------------------------
  ButtonEnable;
  ButtonSubShow(False);

  if DMMssql.inTrans then
  begin
    case (Sender as TsButton).Tag of
      0, 1:
        DMMssql.CommitTrans;
      2:
        DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;
  DOCNO := edt_MAINT_NO.Text;

  IF (Sender as TsButton).Tag = 1 then
  begin
    sMaskEdit1.Text := FormatDateTime('YYYYMMDD', Now);
    sMaskEdit2.Text := sMaskEdit1.Text;
    sMaskEdit3.Text := sMaskEdit1.Text;
    smaskEdit4.Text := sMaskEdit1.Text;
  end;

  Readlist;
  qryList.Locate('MAINT_NO', DOCNO, []);
end;

function TUI_LOCRC2_NEW_frm.getMaintHeader: string;
begin
  Result := '';

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT NAME FROM CODE2NDD WHERE CODE = ''LC_BGM1''';
      Open;

      Result := 'LOCRCT' + FieldByName('NAME').AsString;

      if (Trim(Result) = '') then
        raise Exception.Create('[기본설정]-[전자문서 고정번호부여]에 LOCRCT문서 고정번호를 입력하세요');

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.ButtonSubEnable;
begin
  Btn_GoodsNew.Enabled := (ProgramControlType <> ctView) and (FSubWorkTypeGoods = ctView);
  Btn_GoodsEdit.Enabled := Btn_GoodsNew.Enabled;
  Btn_GoodsDel.Enabled := Btn_GoodsNew.Enabled;
  Btn_GoodsOK.Enabled := not Btn_GoodsNew.Enabled;
  Btn_GoodsCancel.Enabled := not Btn_GoodsNew.Enabled;

  sSpeedButton13.Enabled := Btn_GoodsNew.Enabled;
  sSpeedButton18.Enabled := Btn_GoodsNew.Enabled;

  Btn_TaxNew.Enabled := (ProgramControlType <> ctView);
  Btn_TaxEdit.Enabled := Btn_TaxNew.Enabled;
  Btn_TaxCancel.Enabled := Btn_TaxNew.Enabled;

  sSpeedButton25.Enabled := Btn_TaxNew.Enabled;
  sSpeedButton26.Enabled := Btn_TaxNew.Enabled;
end;

procedure TUI_LOCRC2_NEW_frm.Btn_GoodsNewClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsSpeedButton).Tag of
    0:
      FSubWorkTypeGoods := ctInsert;
    1:
      FSubWorkTypeGoods := ctModify;
  end;
  
  ButtonSubEnable;
  EnableControl(sPanel11);
  ReadOnlyControl(sPanel11, False);
  sDBGrid2.Enabled := False;

  IF FSubWorkTypeGoods = ctInsert Then
  begin
    ClearControl(sPanel11);
    sPanel18.Visible := True;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.ButtonSubShow(value: Boolean);
begin
  Btn_GoodsNew.Enabled := value;
  Btn_GoodsEdit.Enabled := value;
  Btn_GoodsDel.Enabled := value;
  Btn_GoodsOK.Enabled := not value and (FSubWorkTypeGoods <> ctView);
  Btn_GoodsCancel.Enabled := Btn_GoodsOK.Enabled;

  sSpeedButton13.Enabled := value;
  sSpeedButton18.Enabled := value;

  Btn_TaxNew.Enabled := value;
  Btn_TaxEdit.Enabled := value;
  Btn_TaxCancel.Enabled := value;

  sSpeedButton25.Enabled := value;
  sSpeedButton26.Enabled := value;
end;

procedure TUI_LOCRC2_NEW_frm.Btn_GoodsCancelClick(Sender: TObject);
var
  SC : TSQLCreate;
  qryINS :TADOQuery;
  arrKeyValues : Variant;
  MaxSeq : Integer;
begin
  inherited;
  //KEY값 설정
  arrKeyValues := VarArrayOf([qryGoodsKEYY.AsString, qryGoodsSEQ.asInteger]);

  IF (Sender as TsSpeedButton).Tag = 0 Then
  begin
    SC := TSQLCreate.Create;
    qryINS := TADOQuery.Create(nil);
    qryINS.Connection := DMMssql.KISConnect;
    try
      Case FSubWorkTypeGoods of
        ctInsert:
        begin
          SC.DMLType := dmlInsert;
//          SC.ADDValue('KEYY', FINS_MAINT_NO);
          SC.ADDValue('KEYY', edt_MAINT_NO.Text);
          MaxSeq := getGoodsMAXSeq;
          SC.ADDValue('SEQ', MaxSeq);
          arrKeyValues := VarArrayOf([edt_MAINT_NO.Text, MaxSeq]);
        end;
        ctModify:
        begin
          SC.DMLType := dmlUpdate;
          SC.ADDWhere('KEYY', qryGoodsKEYY.AsString);
          SC.ADDWhere('SEQ' , qryGoodsSEQ.asInteger);
          arrKeyValues := VarArrayOf([qryGoodsKEYY.AsString, qryGoodsSEQ.asInteger]);
        end;
      end;
      SC.SQLHeader('LOCRC2_D');
      SC.ADDValue('HS_NO',    msk_HS_NO.Text);
      SC.ADDValue('NAME_COD', edt_NAME_COD.Text);
      SC.ADDValue('NAME1',    memo_NAME1.Text);
      SC.ADDValue('SIZE1',    memo_SIZE1.Text);
      SC.ADDValue('QTY',      curr_QTY.value);
      SC.ADDValue('QTY_G',    edt_QTY_G.Text);
      SC.ADDValue('QTYG',     curr_QTYG.value);
      SC.ADDValue('QTYG_G',   edt_QTYG_G.Text);
      SC.ADDValue('PRICE',    curr_PRICE.value);
      SC.ADDValue('PRICE_G',  edt_PRICE_G.Text);
      SC.ADDValue('AMT',      curr_AMT.value);
      SC.ADDValue('AMT_G',    edt_AMT_G.Text);
      SC.ADDValue('STQTY',    curr_STQTY.value);
      SC.ADDValue('STQTY_G',  edt_STQTY_G.Text);
      SC.ADDValue('STAMT',    curr_STAMT.value);
      SC.ADDValue('STAMT_G',  edt_STAMT_G.Text);
      SC.ADDValue('ASEQ', 0);
      SC.ADDValue('AMAINT_NO', '');

      qryINS.SQL.Text := SC.CreateSQL;
      qryINS.ExecSQL;
    finally
      SC.Free;
      qryINS.Free;
    end;
  end;

  FSubWorkTypeGoods := ctView;
  ButtonSubEnable;
  EnableControl(sPanel11, False);
  ReadOnlyControl(sPanel11);
  sDBGrid2.Enabled := True;
  sPanel18.Visible := False;
//  qryGoodsAfterScroll(qryGoods);
  qryGoods.Close;
  qryGoods.Open;
  qryGoods.Locate('KEYY;SEQ', arrKeyValues, []);
end;

procedure TUI_LOCRC2_NEW_frm.edt_BENEFCDblClick(Sender: TObject);
begin
  inherited;
  if ProgramControlType = ctView then
    Exit;

  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_CUST_frm.Values[0];

      case (Sender as TsEdit).Tag of
        //수혜자
        100:
          begin
            edt_BENEFC1.Text := CD_CUST_frm.FieldValues('ENAME');
            edt_BENEFC2.Text := CD_CUST_frm.FieldValues('SAUP_NO');
          end;
        //개설업체
        101:
          begin
            edt_APPLIC7.Text := CD_CUST_frm.FieldValues('SAUP_NO');
            edt_APPLIC1.Text := CD_CUST_frm.FieldValues('ENAME');
            edt_APPLIC2.Text := CD_CUST_frm.FieldValues('REP_NAME');
            edt_APPLIC3.Text := CD_CUST_frm.FieldValues('jenja');
            edt_APPLIC4.Text := CD_CUST_frm.FieldValues('ADDR1');
            edt_APPLIC5.Text := CD_CUST_frm.FieldValues('ADDR2');
            edt_APPLIC6.Text := CD_CUST_frm.FieldValues('ADDR3');

          end;
      end;
    end;
  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_LOCRC2_NEW_frm.edt_AP_BANKDblClick(Sender: TObject);
begin
  inherited;
  CD_BANK_frm := TCD_BANK_frm.Create(Self);
  try
    with CD_BANK_frm do
    begin
      if OpenDialog(0, edt_AP_BANK.Text) then
      begin
        edt_AP_BANK.Text := FieldValues('CODE');
        edt_AP_BANK1.Text := FieldValues('BankName1');
        edt_AP_BANK2.Text := FieldValues('ENAME2');
        edt_AP_BANK3.Text := FieldValues('BankBranch1');
        edt_AP_BANK4.Text := FieldValues('ENAME4');
        edt_AP_NAME.Clear;
      end;
    end;
  finally
    FreeAndNil(CD_BANK_frm);
  end;
end;

procedure TUI_LOCRC2_NEW_frm.edt_LOC1AMTCDblClick(Sender: TObject);
var
  uDialog: TCodeDialogParent_frm;
begin
  inherited;
  case (Sender as TsEdit).Tag of
    //통화단위
    200, 201, 203, 301, 303, 305:
      uDialog := TCD_AMT_UNIT_frm.Create(Self);
    //수량단위
    202, 300, 302, 304:
      uDialog := TCD_UNIT_frm.Create(Self);
  end;

  try
    with uDialog do
    begin
      if OpenDialog(0, (Sender as TsEdit).Text) then
        (Sender as TsEdit).Text := Values[0];
    end;
  finally
    FreeAndNil(uDialog);
  end;

end;

procedure TUI_LOCRC2_NEW_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  if FSubWorkTypeGoods = ctView then
    Exit;

  CD_ITEMLIST_frm := TCD_ITEMLIST_frm.Create(Self);
  try
    if CD_ITEMLIST_frm.OpenDialog(0, edt_NAME_COD.Text) then
    begin
      edt_NAME_COD.Text := CD_ITEMLIST_frm.FieldValues('CODE');
      msk_HS_NO.Text := CD_ITEMLIST_frm.FieldValues('HSCODE');
      memo_NAME1.Text := CD_ITEMLIST_frm.FieldValues('FNAME');
      memo_SIZE1.Text := CD_ITEMLIST_frm.FieldValues('MSIZE');

      edt_QTY_G.Text := CD_ITEMLIST_frm.FieldValues('QTYC');
      edt_STQTY_G.Text := CD_ITEMLIST_frm.FieldValues('QTYC');

      edt_PRICE_G.Text := CD_ITEMLIST_frm.FieldValues('PRICEG');
      curr_PRICE.Value := CD_ITEMLIST_frm.Field('PRICE').AsCurrency;

      edt_AMT_G.Text := CD_ITEMLIST_frm.FieldValues('PRICEG');
      edt_STAMT_G.Text := CD_ITEMLIST_frm.FieldValues('PRICEG');

      edt_QTYG_G.Text := CD_ITEMLIST_frm.FieldValues('QTY_UG');
      curr_QTYG.Value := CD_ITEMLIST_frm.Field('QTY_U').AsCurrency;
    end;
  finally
    FreeAndNil(CD_ITEMLIST_frm);
  end;
end;

procedure TUI_LOCRC2_NEW_frm.Btn_TaxNewClick(Sender: TObject);
var
  nIDX: integer;
  Return: TModalResult;
begin
  inherited;
  dlg_ADDTAX_frm := Tdlg_ADDTAX_frm.Create(Self);
  try
    case (Sender as TsSpeedButton).Tag of
      0:
        begin
//          if FINS_MAINT_NO <> '' then
//            Return := dlg_ADDTAX_frm.WriteTax(FINS_MAINT_NO)
//          else
//            Return := dlg_ADDTAX_frm.WriteTax(qrylistMAINT_NO.AsString);
            Return := dlg_ADDTAX_frm.WriteTax(edt_MAINT_NO.Text);

          if Return = mrOK then
          begin
            qryTax.Close;
            qryTax.Open;
            qryTax.Locate('KEYY;SEQ', dlg_ADDTAX_frm.ApplyKeyValues, []);
          end;
        end;
      1:
        begin
          if qryTax.RecordCount = 0 then
            Exit;

          Return := dlg_ADDTAX_frm.EditTax(qryTax.Fields);
          if Return = mrOK then
          begin
            qryTax.Close;
            qryTax.Open;
            qryTax.Locate('KEYY;SEQ', dlg_ADDTAX_frm.ApplyKeyValues, []);
          end;
        end;
      2:
        begin
          nIDX := qryTax.RecNo;
          if dlg_ADDTAX_frm.DelTax(qryTaxKEYY.AsString, qryTaxSEQ.AsInteger) then
          begin
            ReSortTaxSEQ;
            qryTax.Close;
            qryTax.Open;
            if qryTax.RecordCount > 1 then
            begin
              qryTax.MoveBy(nIDX - 1);
            end;
          end;
        end;
    end;
  finally
    FreeAndNil(dlg_ADDTAX_frm);
  end;
end;

procedure TUI_LOCRC2_NEW_frm.ReSortTaxSEQ;
begin
  with qryReSortTaxSeq do
  begin
    Close;
    Parameters[0].value := edt_MAINT_NO.Text;
    ExecSQL;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.sSpeedButton26Click(Sender: TObject);
var
  EW: TExcelWriter;
begin
  inherited;
  EW := TExcelWriter.Create;
  EW.Cells(1, 1, '작성일자', taCenter);
  EW.Cells(2, 1, '세금계산서번호', taCenter);
  EW.Cells(3, 1, '공급가액', taCenter);
  EW.Cells(4, 1, '세액', taCenter);
  EW.Autofit;
  EW.ShowExcel;
end;

procedure TUI_LOCRC2_NEW_frm.sSpeedButton25Click(Sender: TObject);
var
  ER: TExcelReader;
  i, MaxCount: Integer;
  SC: TSQLCreate;
  qryINS: TADOQuery;
  nCursor: Integer;
begin
  inherited;
  if qryTax.RecordCount > 0 then
  begin
    if MessageBox(Self.Handle, MSG_EXCEL_IMPORT_ALLDATA_DEL, '엑셀가져오기- 세금계산서', MB_OKCANCEL + MB_ICONQUESTION) = ID_CANCEL then
      Exit;
  end;

  if not excelOpen.Execute then
    Exit;

  ER := TExcelReader.Create(excelOpen.Files.Strings[0]);
  SC := TSQLCreate.Create;
  qryINS := TADOQuery.Create(nil);
  qryINS.Connection := DMMssql.KISConnect;
  with ER do
  begin
    try
      qryINS.SQL.Text := 'DELETE FROM LOCRC2_TAX WHERE KEYY = ' + QuotedStr(edt_MAINT_NO.Text);
      qryINS.ExecSQL;

      MaxCount := MaxRow('A');
      SC.DMLType := dmlInsert;
      nCursor := 2;
      for i := 1 to MaxCount - 1 do
      begin
        SC.SQLHeader('LOCRC2_TAX');
        SC.ADDValue('KEYY', edt_MAINT_NO.Text);
        SC.ADDValue('SEQ', i);
        SC.ADDValue('BILL_NO', ER.asString(nCursor, 2));
        SC.ADDValue('BILL_DATE', AnsiReplaceText(ER.asString(nCursor, 1), '-', ''));
        SC.ADDValue('BILL_AMOUNT', ER.asString(nCursor, 3));
        SC.ADDValue('BILL_AMOUNT_UNIT', 'KRW');
        SC.ADDValue('TAX_AMOUNT', ER.asString(nCursor, 4));
        SC.ADDValue('TAX_AMOUNT_UNIT', 'KRW');
        qryINS.SQL.Text := SC.CreateSQL;
//        Clipboard.AsText := qryINS.SQL.Text;
        qryINS.ExecSQL;
        inc(nCursor);
      end;

    finally
      ER.Free;
      SC.Free;
      qryINS.Free;
    end;
  end;

  qryTax.Close;
  qryTax.Open;
  ShowMessage('전환이 완료되었습니다');

end;

procedure TUI_LOCRC2_NEW_frm.edt_QTY_GExit(Sender: TObject);
begin
  inherited;
  if Trim(edt_STQTY_G.Text) = '' then
    edt_STQTY_G.Text := edt_QTY_G.Text;
end;

procedure TUI_LOCRC2_NEW_frm.edt_AMT_GExit(Sender: TObject);
begin
  inherited;
  if Trim(edt_STAMT_G.Text) = '' then
    edt_TAMT_G.Text := edt_QTY_G.Text;
end;

procedure TUI_LOCRC2_NEW_frm.edt_PRICE_GExit(Sender: TObject);
begin
  inherited;
  if Trim(edt_AMT_G.Text) = '' then
    edt_AMT_G.Text := edt_PRICE_G.Text;
  if Trim(edt_STAMT_G.Text) = '' then
    edt_STAMT_G.Text := edt_PRICE_G.Text;
end;

procedure TUI_LOCRC2_NEW_frm.curr_QTYChange(Sender: TObject);
begin
  inherited;
  curr_AMT.Value := curr_QTY.Value * curr_PRICE.Value;
  curr_STAMT.Value := curr_AMT.Value;
end;

procedure TUI_LOCRC2_NEW_frm.curr_QTYExit(Sender: TObject);
begin
  inherited;
  curr_STQTY.Value := curr_QTY.Value;
end;

procedure TUI_LOCRC2_NEW_frm.Btn_GoodsDelClick(Sender: TObject);
var
  nIDX: integer;
begin
  inherited;
  if MessageBox(Self.Handle, MSG_LOCRCT_DELGOODS_QUESTION, '물품내역 삭제', MB_OKCANCEL + MB_ICONQUESTION) = ID_CANCEL then
    Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := 'DELETE FROM LOCRC2_D WHERE KEYY = ' + QuotedStr(qryGoodsKEYY.AsString) + ' AND SEQ = ' + IntToStr(qryGoodsSEQ.AsInteger);
        ExecSQL;
      except
        on E: Exception do
        begin
          ShowMessage(MSG_SYSTEM_DEL_ERR + ':'#13#10 + E.Message);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;

  with qryReSortGoodsSeq do
  begin
    Close;
    Parameters[0].Value := edt_MAINT_NO.Text;
    ExecSQL;
  end;

  nIDX := qryGoods.RecNo;
  qryGoods.Close;
  qryGoods.Open;
  if qryGoods.RecordCount > 1 Then
    qryGoods.MoveBy(nIDX-1);

end;

function TUI_LOCRC2_NEW_frm.getGoodsMaxSeq: integer;
begin
  Result := 1;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT ISNULL(MAX(SEQ),0) as MAX_SEQ FROM LOCRC2_D WHERE KEYY = '+QuotedStr(edt_MAINT_NO.Text);
      Open;

      Result := FieldByName('MAX_SEQ').AsInteger+1;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.btnCopyClick(Sender: TObject);
begin
  inherited;
  IF qrylist.RecordCount = 0 Then Exit;
  IF MessageBox(Self.Handle, PChar(MSG_COPY_QUESTION+#13#10+qrylistMAINT_NO.AsString), '복사확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  ClearControl(sPanel6);
  ClearControl(sPanel27);
  ClearControl(sPanel11);
  sPanel29.Visible := True;

//------------------------------------------------------------------------------
// 헤더
//------------------------------------------------------------------------------
  msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text := LoginData.sID;
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;

//------------------------------------------------------------------------------
// 개설의뢰인 셋팅
//------------------------------------------------------------------------------
  if DMCodeContents.GEOLAECHEO.Locate('CODE', '00000', []) then
  begin
    edt_APPLIC.Text := DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
    edt_APPLIC1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    edt_APPLIC2.Text := DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
    edt_APPLIC3.Text := DMCodeContents.GEOLAECHEO.FieldByName('jenja').AsString;
    edt_APPLIC4.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString;
    edt_APPLIC5.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString;
    edt_APPLIC6.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR3').AsString;
    edt_APPLIC7.Text := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').AsString;
  end;
//------------------------------------------------------------------------------
// 공통사항
//------------------------------------------------------------------------------
  msk_GET_DAT.Text := FormatDateTime('YYYYMMDD', Now);
  msk_ISS_DAT.Text := FormatDateTime('YYYYMMDD', Now);

  edt_MAINT_NO.Text := getMaintHeader + '-' + DMAutoNo.GetDocumentNoAutoInc('LOCRCT');
  edt_RFF_NO.Text := edt_MAINT_NO.Text;

  edt_LOC2AMTC.Text := 'KRW';
  edt_RCT_AMT2C.Text := 'KRW';

  ProgramControlType := ctInsert;


  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel27, False);

//  EnableControl(sPanel7);
//  EnableControl(sPanel27);

//------------------------------------------------------------------------------
// 원본데이터 가져오기
//------------------------------------------------------------------------------
  ClearControl(sPanel7);
//------------------------------------------------------------------------------
// 기본입력사항
//------------------------------------------------------------------------------
  //인수일자
  msk_GET_DAT.Text := FormatDateTime('YYYYMMDD', Now);
  msk_ISS_DAT.Text := FormatDateTime('YYYYMMDD', Now);  
  //발급일자
  //문서유효일자
  msk_EXP_DAT.Clear;
  //발급번호
  edt_RFF_NO.Text := edt_MAINT_NO.Text;
  //대표공급물품 HS부호
  msk_BSN_HSCODE.Text := qrylistBSN_HSCODE.AsString;
//------------------------------------------------------------------------------
// 수혜업체/개설업체
//------------------------------------------------------------------------------
  //수혜자
  edt_BENEFC.Text := qrylistBENEFC.AsString;
  edt_BENEFC1.Text := qrylistBENEFC1.AsString;
  edt_BENEFC2.Text := qrylistBENEFC2.AsString;
  //개설업체
  //코드
  edt_APPLIC.Text := qrylistAPPLIC.AsString;
  //상호
  edt_APPLIC1.Text := qrylistAPPLIC1.AsString;
  //명의인
  edt_APPLIC2.Text := qrylistAPPLIC2.AsString;
  //전자서명
  edt_APPLIC3.Text := qrylistAPPLIC3.AsString;
  //주소1
  edt_APPLIC4.Text := qrylistAPPLIC4.AsString;
  //주소2
  edt_APPLIC5.Text := qrylistAPPLIC5.AsString;
  //주소3
  edt_APPLIC6.Text := qrylistAPPLIC6.AsString;
  //사업자등록번호
  edt_APPLIC7.Text := qrylistAPPLIC7.AsString;
//------------------------------------------------------------------------------
// 개설은행
//------------------------------------------------------------------------------
  //개설은행
  edt_AP_BANK.Text := qrylistAP_BANK.AsString;
  edt_AP_BANK1.Text := qrylistAP_BANK1.AsString;
  edt_AP_BANK2.Text := qrylistAP_BANK2.AsString;
  //지점명
  edt_AP_BANK3.Text := qrylistAP_BANK3.AsString;
  edt_AP_BANK4.Text := qrylistAP_BANK4.AsString;
  //전자서명
  edt_AP_NAME.Text := qrylistAP_NAME.AsString;
//------------------------------------------------------------------------------
// 내국신용장
//------------------------------------------------------------------------------
  //내국신용장 번호
  edt_LOC_NO.Text := qrylistLOC_NO.AsString;
  //개설금액(외화)
  edt_LOC1AMTC.Text := qrylistLOC1AMTC.AsString;
  curr_LOC1AMT.Value := qrylistLOC1AMT.AsCurrency;
  //매매기준율
  curr_EX_RATE.Value := qrylistEX_RATE.AsCurrency;
  //개설금액(원화)
  edt_LOC2AMTC.Text := 'KRW';
  curr_LOC2AMT.Value := qrylistLOC2AMT.AsCurrency;
  //물품인도기일
  msk_LOADDATE.Text := qrylistLOADDATE.AsString;
  //유효일자
  msk_EXPDATE.Text := qrylistEXPDATE.AsString;
  //신용장관리번호
  edt_AMAINT_NO.Text := qrylistAMAINT_NO.AsString;
//------------------------------------------------------------------------------
// 인수금액
//------------------------------------------------------------------------------
  //인수금액(외화)
  edt_RCT_AMT1C.Text := qrylistRCT_AMT1C.AsString;
  curr_RCT_AMT1.Value := qrylistRCT_AMT1.AsCurrency;
  //환율
  curr_RATE.Value := qrylistRATE.AsCurrency;
  //인수금액(원화)
  edt_RCT_AMT2C.Text := qrylistRCT_AMT2C.AsString;
  curr_RCT_AMT2.Value := qrylistRCT_AMT2.AsCurrency;
//------------------------------------------------------------------------------
// 총합계
//------------------------------------------------------------------------------
  //총합계수량
  edt_TQTY_G.Text := qrylistTQTY_G.AsString;
  curr_TQTY.Value := qrylistTQTY.AsCurrency;
  //총합계금액
  edt_TAMT_G.Text := qrylistTAMT_G.AsString;
  curr_TAMT.Value := qrylistTAMT.AsCurrency;
//------------------------------------------------------------------------------
// 기타조건/참고사항
//------------------------------------------------------------------------------
  //기타조건
  memo_REMARK1.Lines.Text := qrylistREMARK1.AsString;
  //참고사항
  memo_LOC_REM1.Lines.Text := qrylistLOC_REM1.AsString;

//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.BeginTrans;

//------------------------------------------------------------------------------
// 상품내역복사
//------------------------------------------------------------------------------
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'INSERT INTO LOCRC2_D'#13#10+
                  'SELECT '+QuotedStr(edt_MAINT_NO.Text)+', SEQ, HS_NO, NAME_COD, NAME1, SIZE1, QTY, QTY_G, QTYG, QTYG_G, PRICE, PRICE_G, AMT, AMT_G, STQTY, STQTY_G, STAMT, STAMT_G, ASEQ, AMAINT_NO FROM LOCRC2_D WHERE KEYY = '+QuotedStr(qrylistMAINT_NO.AsString);
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
//------------------------------------------------------------------------------
// 세금계산서복사
//------------------------------------------------------------------------------
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'INSERT INTO LOCRC2_TAX'#13#10+
                  'SELECT '+QuotedStr(edt_MAINT_NO.Text)+', SEQ, BILL_NO, BILL_DATE, BILL_AMOUNT, BILL_AMOUNT_UNIT, TAX_AMOUNT, TAX_AMOUNT_UNIT FROM LOCRC2_TAX WHERE KEYY = '+QuotedStr(qrylistMAINT_NO.AsString);
      ExecSQL;                  
    finally
      Close;
      Free;
    end;
  end;
//------------------------------------------------------------------------------
// 상품내역/세금계산서 가져오기
//------------------------------------------------------------------------------
  ReadGoods;
  ReadTax;

//------------------------------------------------------------------------------
// 버튼활성화
//------------------------------------------------------------------------------
  ButtonEnable;
  ButtonSubShow(True);
  sPageControl1.ActivePageIndex := 0;

end;

procedure TUI_LOCRC2_NEW_frm.curr_RCT_AMT1Change(Sender: TObject);
begin
  inherited;
  if chk_autoCalc.Checked Then
    curr_RCT_AMT2.Value := curr_RCT_AMT1.Value * curr_RATE.Value;
end;

procedure TUI_LOCRC2_NEW_frm.curr_LOC1AMTChange(Sender: TObject);
begin
  inherited;
  if chk_autoCalc.Checked Then
    curr_LOC2AMT.Value := curr_LOC1AMT.Value * curr_EX_RATE.Value;
end;

procedure TUI_LOCRC2_NEW_frm.btn_TOTAL_CALCClick(Sender: TObject);
begin
  inherited;
  IF qryGoods.RecordCount = 0 Then
  begin
    ShowMessage('입력된 상품내역이 없습니다. 입력후 다시 시도해주세요');
    Exit;
  end;

  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT ISNULL(SUM(ISNULL(QTY,0)),0) as SUM_QTY, ISNULL(SUM(ISNULL(AMT,0)),0) as SUM_AMT FROM LOCRC2_D WHERE KEYY ='+QuotedStr(edt_MAINT_NO.Text);
      Open;

      curr_TQTY.Value := FieldByName('SUM_QTY').AsCurrency;
      curr_TAMT.Value := FieldByName('SUM_AMT').AsCurrency;
      qryGoods.First;
      edt_TQTY_G.Text := qryGoodsQTY_G.Text;
      edt_TAMT_G.Text := qryGoodsAMT_G.Text;
    finally
      Close;
      Free;
    end;
  end;
end;

function TUI_LOCRC2_NEW_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  nYear,nMonth,nDay : Word;
begin
  ErrMsg := TStringList.Create;

  Try
    if Trim(msk_GET_DAT.Text) = '' Then
      ErrMsg.Add('[공통] 인수일자를 입력하세요');
    if Trim(msk_ISS_DAT.Text) = '' Then
      ErrMsg.Add('[공통] 발급일자를 입력하세요');
    if Trim(msk_BSN_HSCODE.Text) = '' Then
      ErrMsg.Add('[공통] 대표공급물품 HS부호를 입력하세요');
    IF Trim(edt_APPLIC7.Text) = '' THEN
      ErrMsg.Add('[공통] 개설의뢰인 사업자등록번호를 입력하세요');
    IF Trim(edt_APPLIC1.Text) = '' THEN
      ErrMsg.Add('[공통] 개설의뢰인 상호를 입력하세요');
    IF Trim(edt_BENEFC2.Text) = '' THEN
      ErrMsg.Add('[공통] 수혜자 사업자등록번호를 입력하세요');
    IF Trim(edt_BENEFC1.Text) = '' THEN
      ErrMsg.Add('[공통] 수혜자 상호를 입력하세요');
    IF Trim(edt_AP_BANK.Text) = '' Then
      ErrMsg.Add('[공통] 개설은행코드(4자리)를 입력하세요');
    IF Trim(edt_AP_BANK1.Text) = '' THEN
      ErrMsg.Add('[공통] 개설은행을 입력하세요');
    IF Trim(edt_LOC_NO.Text) = '' THEN
      ErrMsg.Add('[공통] 내국신용장 번호를 입력하세요');
    IF Trim(msk_LOADDATE.Text) = '' Then
      ErrMsg.Add('[공통] 물품인도기일을 입력하세요');
    IF Trim(msk_EXPDATE.Text) = '' Then
      ErrMsg.Add('[공통] 내국신용장의 유효기일을 입력하세요');

    IF (curr_LOC1AMT.Value = 0) AND (curr_LOC2AMT.Value = 0) Then
      ErrMsg.Add('[공통] 개설금액(외화) 또는 개설금액(원화) 둘중하나는 필수입력입니다');
    IF (curr_LOC1AMT.Value <> 0) and (Trim(edt_LOC1AMTC.Text) = '') then
      ErrMsg.Add('[공통] 개설금액(외화)의 통화단위를 입력하세요');

    IF (curr_RCT_AMT1.Value = 0) AND (curr_RCT_AMT2.Value = 0) Then
      ErrMsg.Add('[공통] 인수금액(외화) 또는 인수금액(원화) 둘중하나는 필수입력입니다');
    IF (curr_RCT_AMT1.Value <> 0) and (Trim(edt_RCT_AMT1C.Text) = '') then
      ErrMsg.Add('[공통] 인수금액(외화)의 통화단위를 입력하세요');

    IF qryGoods.RecordCount = 0 Then
      ErrMsg.Add('[상품명세내역] 상품명세내역이 0건입니다');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.btnDelClick(Sender: TObject);
var
  nIDX : Integer;
begin
  inherited;
  IF qrylist.RecordCount = 0 Then Exit;
  IF MessageBox(Self.Handle, PChar(MSG_SYSTEM_DEL_CONFIRM+#13#10+MSG_SYSTEM_LINE+'내국신용장 물품수령 증명서'#13#10+qrylistMAINT_NO.AsString),'데이터 삭제 확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      IF not DMMssql.inTrans Then DMMssql.BeginTrans;
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := 'DELETE FROM LOCRC2_H WHERE MAINT_NO = '+QuotedStr(edt_MAINT_NO.Text);
        ExecSQL;

        SQL.Text := 'DELETE FROM LOCRC2_D WHERE KEYY = '+QuotedStr(edt_MAINT_NO.Text);
        ExecSQL;

        SQL.Text := 'DELETE FROM LOCRC2_TAX WHERE KEYY = '+QuotedStr(edt_MAINT_NO.Text);
        ExecSQL;
        DMMssql.CommitTrans;

        nIDX := qrylist.RecNo;

        qrylist.Close;
        qrylist.Open;

        if qrylist.RecordCount > 1 then
        begin
          qrylist.MoveBy(nIDX - 1);
        end;

      except
        on E:Exception do
        begin
          DMMssql.RollbackTrans;
          MessageBox(Self.Handle, PChar(E.Message), '삭제오류', MB_OK+MB_ICONERROR);
        end;
      end;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_LOCRC2_NEW_frm.ReadyDocument(DocNo : String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := DocNo;
      RecvFlat := LOCRC2(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'LOCRC2';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'LOCRC2_H';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        qrylist.Close;
        qrylist.Open;
        qrylist.Locate('MAINT_NO', DocNo, []);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_LOCRC2_NEW_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.FormActivate(Sender: TObject);
var
  BMK : Pointer;
begin
  inherited;
  If ProgramControlType <> ctView Then Exit;
  
  BMK := qryList.GetBookmark;
  ReadList;
  try
  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);
  finally
    qryList.FreeBookmark(BMK);
  end;
end;

procedure TUI_LOCRC2_NEW_frm.sSpeedButton13Click(Sender: TObject);
var
  EW: TExcelWriter;
begin
  inherited;
  EW := TExcelWriter.Create;
  try
    EW.Cells(1,  1, '품명코드(선택)', taCenter);
    EW.Cells(2,  1, '품명', taCenter);
    EW.Cells(3,  1, '규격(선택)', taCenter);
    EW.Cells(4,  1, 'HS부호', taCenter);
    EW.Cells(5,  1, '수량', taCenter);
    EW.Cells(6,  1, '수량단위', taCenter);
    EW.Cells(7,  1, '단가', taCenter);
    EW.Cells(8,  1, '단가통화', taCenter);
    EW.Cells(9,  1, '단가기준수량(선택)', taCenter);
    EW.Cells(10, 1, '단가기준수량단위(선택)', taCenter);
    EW.Cells(11, 1, '금액', taCenter);
    EW.Cells(12, 1, '금액통화', taCenter);
    EW.Cells(13, 1, '수량소계', taCenter);
    EW.Cells(14, 1, '수량소계단위', taCenter);
    EW.Cells(15, 1, '금액소계', taCenter);
    EW.Cells(16, 1, '금액소계통화', taCenter);

    EW.Autofit;
  finally
    EW.ShowExcel;
  end;
end;

procedure TUI_LOCRC2_NEW_frm.sSpeedButton18Click(Sender: TObject);
var
  ER: TExcelReader;
  i, MaxCount: Integer;
  SC: TSQLCreate;
  qryINS: TADOQuery;
  nCursor: Integer;
begin
  inherited;
  if qryGoods.RecordCount > 0 then
  begin
    if MessageBox(Self.Handle, MSG_EXCEL_IMPORT_ALLDATA_DEL, '엑셀가져오기- 품목내역', MB_OKCANCEL + MB_ICONQUESTION) = ID_CANCEL then
      Exit;
  end;

  if not excelOpen.Execute then
    Exit;

  ER := TExcelReader.Create(excelOpen.Files.Strings[0]);
  SC := TSQLCreate.Create;
  qryINS := TADOQuery.Create(nil);
  qryINS.Connection := DMMssql.KISConnect;
  with ER do
  begin
    try
      qryINS.SQL.Text := 'DELETE FROM LOCRC2_TAX WHERE KEYY = ' + QuotedStr(edt_MAINT_NO.Text);
      qryINS.ExecSQL;

      MaxCount := MaxRow('B');
      SC.DMLType := dmlInsert;
      nCursor := 2;
      for i := 1 to MaxCount - 1 do
      begin
        SC.SQLHeader('LOCRC2_D');
        SC.ADDValue('KEYY',     edt_MAINT_NO.Text);
        SC.ADDValue('SEQ',      i);
        SC.ADDValue('HS_NO',    AnsiReplaceText( AnsiReplaceText( ER.asString(nCursor, 4) , '-', '') , '.', ''));
        SC.ADDValue('NAME_COD', ER.asString(nCursor, 1));
        SC.ADDValue('NAME1',    ER.asString(nCursor, 2));
        SC.ADDValue('SIZE1',    ER.asString(nCursor, 3));
        SC.ADDValue('QTY',      ER.asString(nCursor, 5));
        SC.ADDValue('QTY_G',    ER.asString(nCursor, 6));
        if ER.asString(nCursor, 9) = '' Then
          SC.ADDValue('QTYG',     0)
        else
          SC.ADDValue('QTYG',     ER.asString(nCursor, 9));
        SC.ADDValue('QTYG_G',   ER.asString(nCursor, 10));
        SC.ADDValue('PRICE',    ER.asString(nCursor, 7));
        SC.ADDValue('PRICE_G',  ER.asString(nCursor, 8));
        SC.ADDValue('AMT',      ER.asString(nCursor, 11));
        SC.ADDValue('AMT_G',    ER.asString(nCursor, 12));

        IF Trim(ER.asString(nCursor, 13)) = '' Then
          SC.ADDValue('STQTY',    ER.asString(nCursor, 5))
        else
          SC.ADDValue('STQTY',    ER.asString(nCursor, 13));
        IF Trim(ER.asString(nCursor, 14)) ='' Then
          SC.ADDValue('STQTY_G',  ER.asString(nCursor, 6))        
        else
          SC.ADDValue('STQTY_G',  ER.asString(nCursor, 14));
        IF Trim(ER.asString(nCursor, 15)) = '' Then
          SC.ADDValue('STAMT',    ER.asString(nCursor, 11))
        else
          SC.ADDValue('STAMT',    ER.asString(nCursor, 15));

        IF Trim(ER.asString(nCursor, 16)) = '' Then
          SC.ADDValue('STAMT_G',  ER.asString(nCursor, 12))
        else
          SC.ADDValue('STAMT_G',  ER.asString(nCursor, 16));

        SC.ADDValue('ASEQ',     0);
        SC.ADDValue('AMAINT_NO','');

        qryINS.SQL.Text := SC.CreateSQL;
        Clipboard.AsText := qryINS.SQL.Text;
        qryINS.ExecSQL;
        inc(nCursor);
      end;
    finally
      ER.Free;
      SC.Free;
      qryINS.Free;
    end;
  end;

  qryGoods.Close;
  qryGoods.Open;
  ShowMessage('전환이 완료되었습니다');

end;

procedure TUI_LOCRC2_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  Preview_frm := TPreview_frm.Create(Self);
  LOCRC2_PRINT_frm := TLOCRC2_PRINT_frm.Create(Self);
  LOCRC2_PRINT2_frm := TLOCRC2_PRINT2_frm.Create(Self);
  try
    LOCRC2_PRINT_frm.DocNo := edt_MAINT_NO.Text;
    LOCRC2_PRINT2.LOCRC2_PRINT2_frm.DocNo := edt_MAINT_NO.Text;
    Preview_frm.CompositeRepost := QRCompositeReport1;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(LOCRC2_PRINT_frm);
    FreeAndNil(LOCRC2_PRINT2_frm);
  end;
end;

procedure TUI_LOCRC2_NEW_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(LOCRC2_PRINT_frm);
  QRCompositeReport1.Reports.Add(LOCRC2_PRINT2_frm);
end;

procedure TUI_LOCRC2_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 4;
end;

end.

