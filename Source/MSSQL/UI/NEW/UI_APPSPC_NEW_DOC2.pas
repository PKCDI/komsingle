unit UI_APPSPC_NEW_DOC2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, Buttons, sSpeedButton, StdCtrls, sMemo, sLabel,
  ComCtrls, sPageControl, Mask, sMaskEdit, sEdit, sBitBtn, Grids, DBGrids,
  acDBGrid, ExtCtrls, sPanel, sSkinProvider, sButton, DB, ADODB,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, sComboBox;
  
type
  TUI_APPSPC_NEW_DOC2_frm = class(TChildForm_frm)
    sPanel17: TsPanel;
    sPanel14: TsPanel;
    sPanel15: TsPanel;
    sPanel19: TsPanel;
    sPanel18: TsPanel;
    sDBGrid4: TsDBGrid;
    sPanel2: TsPanel;
    btnEdtD2: TsBitBtn;
    btnDelD2: TsBitBtn;
    btnSaveD2: TsBitBtn;
    btnCancelD2: TsBitBtn;
    btnAddD2: TsBitBtn;
    sPanel9: TsPanel;
    edt_VB_RENO: TsEdit;
    edt_VB_MAINTNO: TsEdit;
    msk_ISS_DATE: TsMaskEdit;
    edt_VB_SENO: TsEdit;
    edt_VB_FSNO: TsEdit;
    edt_VB_DMNO: TsEdit;
    sPanel33: TsPanel;
    sPageControl2: TsPageControl;
    sTabSheet6: TsTabSheet;
    sTabSheet7: TsTabSheet;
    sTabSheet8: TsTabSheet;
    sPanel37: TsPanel;
    sLabel5: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    edt_PAI_AMTC1: TsEdit;
    edt_PAI_AMTC2: TsEdit;
    edt_PAI_AMTC3: TsEdit;
    edt_PAI_AMTC4: TsEdit;
    edt_TOTCNTC: TsEdit;
    sPanel16: TsPanel;
    btnAdd_SIZE: TsSpeedButton;
    btnAdd_IMD: TsSpeedButton;
    btnAdd_BIGO: TsSpeedButton;
    edt_LINE_NO: TsEdit;
    msk_DE_DATE: TsMaskEdit;
    edt_SIZE: TsEdit;
    edt_IMD_CODE: TsEdit;
    edt_QTYC: TsEdit;
    edt_TOTQTYC: TsEdit;
    edt_PRICEC: TsEdit;
    edt_SUP_AMTC: TsEdit;
    edt_SUP_TOTAMTC: TsEdit;
    edt_BIGO: TsEdit;
    edt_VB_AMTC: TsEdit;
    edt_VB_TOTAMTC: TsEdit;
    edt_VB_TAXC: TsEdit;
    edt_VB_TOTTAXC: TsEdit;
    edt_SE_CODE: TsEdit;
    edt_SE_SNAME: TsEdit;
    edt_SE_NAME: TsEdit;
    edt_SE_ADDR1: TsEdit;
    edt_SE_ADDR2: TsEdit;
    edt_SE_ADDR3: TsEdit;
    edt_SG_UPTAI1_1: TsEdit;
    edt_SG_UPTAI1_2: TsEdit;
    edt_SG_UPTAI1_3: TsEdit;
    edt_HN_ITEM1_1: TsEdit;
    edt_HN_ITEM1_2: TsEdit;
    msk_SE_SAUPNO: TsMaskEdit;
    edt_SE_ELEC: TsEdit;
    edt_HN_ITEM1_3: TsEdit;
    edt_BY_CODE: TsEdit;
    edt_BY_SNAME: TsEdit;
    edt_BY_NAME: TsEdit;
    edt_BY_ADDR1: TsEdit;
    edt_BY_ADDR2: TsEdit;
    edt_BY_ADDR3: TsEdit;
    edt_SG_UPTAI2_1: TsEdit;
    edt_SG_UPTAI2_2: TsEdit;
    edt_SG_UPTAI2_3: TsEdit;
    edt_HN_ITEM2_1: TsEdit;
    edt_HN_ITEM2_2: TsEdit;
    msk_BY_SAUPNO: TsMaskEdit;
    edt_BY_ELEC: TsEdit;
    edt_HN_ITEM2_3: TsEdit;
    edt_AG_CODE: TsEdit;
    edt_AG_SNAME: TsEdit;
    edt_AG_NAME: TsEdit;
    edt_AG_ADDR1: TsEdit;
    edt_AG_ADDR2: TsEdit;
    edt_AG_ADDR3: TsEdit;
    msk_AG_SAUPNO: TsMaskEdit;
    edt_AG_ELEC: TsEdit;
    edt_REMARK1_1: TsEdit;
    edt_REMARK1_2: TsEdit;
    edt_REMARK1_3: TsEdit;
    edt_REMARK1_4: TsEdit;
    edt_REMARK1_5: TsEdit;
    btnSave: TsButton;
    btnCancel: TsButton;
    qryListD2: TADOQuery;
    dsListD2: TDataSource;
    edt_PAI_AMT1: TsCurrencyEdit;
    edt_PAI_AMT2: TsCurrencyEdit;
    edt_PAI_AMT3: TsCurrencyEdit;
    edt_PAI_AMT4: TsCurrencyEdit;
    edt_PAI_AMTU1: TsCurrencyEdit;
    edt_PAI_AMTU2: TsCurrencyEdit;
    edt_PAI_AMTU3: TsCurrencyEdit;
    edt_PAI_AMTU4: TsCurrencyEdit;
    edt_ISS_EXPAMT: TsCurrencyEdit;
    edt_ISS_EXPAMTU: TsCurrencyEdit;
    edt_TOTCNT: TsCurrencyEdit;
    edt_QTY: TsCurrencyEdit;
    edt_PRICE: TsCurrencyEdit;
    edt_SUP_AMT: TsCurrencyEdit;
    edt_VB_AMT: TsCurrencyEdit;
    edt_VB_TAX: TsCurrencyEdit;
    edt_TOTQTY: TsCurrencyEdit;
    edt_PRICE_G: TsCurrencyEdit;
    edt_SUP_TOTAMT: TsCurrencyEdit;
    edt_VB_TOTAMT: TsCurrencyEdit;
    edt_VB_TOTTAX: TsCurrencyEdit;
    edt_CUX_RATE: TsCurrencyEdit;
    sPanel3: TsPanel;
    sLabel2: TsLabel;
    edt_SIZE1: TsEdit;
    edt_SIZE2: TsEdit;
    edt_SIZE3: TsEdit;
    edt_SIZE4: TsEdit;
    sButton2: TsButton;
    edt_SIZE5: TsEdit;
    edt_SIZE6: TsEdit;
    edt_SIZE7: TsEdit;
    edt_SIZE8: TsEdit;
    edt_SIZE9: TsEdit;
    edt_SIZE10: TsEdit;
    sPanel1: TsPanel;
    sLabel1: TsLabel;
    edt_IMD_CODE1: TsEdit;
    edt_IMD_CODE2: TsEdit;
    edt_IMD_CODE3: TsEdit;
    edt_IMD_CODE4: TsEdit;
    sButton1: TsButton;
    sPanel4: TsPanel;
    sLabel3: TsLabel;
    edt_BIGO1: TsEdit;
    edt_BIGO2: TsEdit;
    edt_BIGO3: TsEdit;
    edt_BIGO4: TsEdit;
    sButton3: TsButton;
    edt_BIGO5: TsEdit;
    edt_DOC_NO: TsEdit;
    qryDOCClick: TADOQuery;
    sBitBtn26: TsBitBtn;
    edt_VB_GUBUN: TsEdit;
    edt_VB_DETAILNO: TsEdit;
    qryD1Cancel: TADOQuery;
    qryListD2KEYY: TStringField;
    qryListD2DOC_GUBUN: TStringField;
    qryListD2SEQ: TIntegerField;
    qryListD2LINE_NO: TStringField;
    qryListD2HS_NO: TStringField;
    qryListD2DE_DATE: TStringField;
    qryListD2IMD_CODE1: TStringField;
    qryListD2IMD_CODE2: TStringField;
    qryListD2IMD_CODE3: TStringField;
    qryListD2IMD_CODE4: TStringField;
    qryListD2SIZE1: TStringField;
    qryListD2SIZE2: TStringField;
    qryListD2SIZE3: TStringField;
    qryListD2SIZE4: TStringField;
    qryListD2SIZE5: TStringField;
    qryListD2SIZE6: TStringField;
    qryListD2SIZE7: TStringField;
    qryListD2SIZE8: TStringField;
    qryListD2SIZE9: TStringField;
    qryListD2SIZE10: TStringField;
    qryListD2QTY: TBCDField;
    qryListD2QTYC: TStringField;
    qryListD2TOTQTY: TBCDField;
    qryListD2TOTQTYC: TStringField;
    qryListD2PRICE: TBCDField;
    qryListD2PRICE_G: TBCDField;
    qryListD2PRICEC: TStringField;
    qryListD2CUX_RATE: TBCDField;
    qryListD2SUP_AMT: TBCDField;
    qryListD2SUP_AMTC: TStringField;
    qryListD2VB_TAX: TBCDField;
    qryListD2VB_TAXC: TStringField;
    qryListD2VB_AMT: TBCDField;
    qryListD2VB_AMTC: TStringField;
    qryListD2SUP_TOTAMT: TBCDField;
    qryListD2SUP_TOTAMTC: TStringField;
    qryListD2VB_TOTTAX: TBCDField;
    qryListD2VB_TOTTAXC: TStringField;
    qryListD2VB_TOTAMT: TBCDField;
    qryListD2VB_TOTAMTC: TStringField;
    qryListD2BIGO1: TStringField;
    qryListD2BIGO2: TStringField;
    qryListD2BIGO3: TStringField;
    qryListD2BIGO4: TStringField;
    qryListD2BIGO5: TStringField;
    procedure btnCancelClick(Sender: TObject);
    procedure qryListD2AfterScroll(DataSet: TDataSet);
    procedure btnSaveClick(Sender: TObject);
    procedure edt_SIZEExit(Sender: TObject);
    procedure edt_IMD_CODEExit(Sender: TObject);
    procedure btnAddD2Click(Sender: TObject);
    procedure btnCancelD2Click(Sender: TObject);
    procedure btnEdtD2Click(Sender: TObject);
    procedure btnSaveD2Click(Sender: TObject);
    procedure btnDelD2Click(Sender: TObject);
    procedure btnAdd_IMDClick(Sender: TObject);
    procedure btnAdd_SIZEClick(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure edt_BIGOExit(Sender: TObject);
    procedure edt_SIZE1Exit(Sender: TObject);
    procedure edt_IMD_CODE1Exit(Sender: TObject);
    procedure edt_BIGO1Exit(Sender: TObject);
    procedure btnAdd_BIGOClick(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure CODEDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn26Click(Sender: TObject);
    procedure edt_VB_GUBUNDblClick(Sender: TObject);
  private
    mode_D2:Integer;
    ReadWrite_D2 : Boolean; //True 쓰기 ,False 읽기
    //저장시 예외처리
    function ErrorCheck:String;
    procedure BtnControl(tag:Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_APPSPC_NEW_DOC2_frm: TUI_APPSPC_NEW_DOC2_frm;

implementation
uses
  ICON, MSSQL, UI_APPSPC_NEW, AutoNo,VarDefine,TypeDefine,
  CD_CUST, SQLCreator, CD_AMT_UNIT, CD_BANK, CD_APPSPCBANK, CodeContents,
  CodeDialogParent, CD_UNIT, dlg_CopyAPPSPCfromVATBI2,
  dlg_SelectCopyDocument, Dlg_ErrorMessage, Dialog_CodeList;

  {$R *.dfm}

procedure TUI_APPSPC_NEW_DOC2_frm.btnCancelClick(Sender: TObject);
begin
  inherited;


  case UI_APPSPC_NEW_frm.mode_D1 of
    1://신규-취소
    begin
      with TADOQuery.Create(Nil) do
      begin
        try
          Connection := DMMssql.KISConnect;
          SQL.Text := 'DELETE FROM APPSPC_D1 WHERE KEYY ='+QuotedStr(UI_APPSPC_NEW_frm.Maint_No)+' and DOC_GUBUN = '+QuotedStr('2AJ')+'and SEQ = '+QuotedStr(IntToStr(UI_APPSPC_NEW_frm.SEQ));
          ExecSQL;
        finally
          Close;
          Free;
        end;
      end;
    end;
    2://수정-취소
    begin
      qryD1Cancel.Close;
      qryD1Cancel.Parameters.ParamByName('KEYY').Value := UI_APPSPC_NEW_frm.Maint_No;
      qryD1Cancel.Parameters.ParamByName('SEQ').Value := UI_APPSPC_NEW_frm.SEQ;
      qryD1Cancel.Parameters.ParamByName('DOC_GUBUN').Value := '2AJ';
      qryD1Cancel.Parameters.ParamByName('KEYY2').Value := UI_APPSPC_NEW_frm.Maint_No;
      qryD1Cancel.ExecSQL;
    end;
  end;
  Close;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.qryListD2AfterScroll(DataSet: TDataSet);
begin

  inherited;
  edt_LINE_NO.Text := qryListD2.FieldByName('LINE_NO').AsString;
  msk_DE_DATE.Text := qryListD2.FieldByName('DE_DATE').AsString;

  edt_IMD_CODE.Text := qryListD2.FieldByName('IMD_CODE1').AsString;
  edt_IMD_CODE1.Text := qryListD2.FieldByName('IMD_CODE1').AsString;
  edt_IMD_CODE2.Text := qryListD2.FieldByName('IMD_CODE2').AsString;
  edt_IMD_CODE3.Text := qryListD2.FieldByName('IMD_CODE3').AsString;
  edt_IMD_CODE4.Text := qryListD2.FieldByName('IMD_CODE4').AsString;

  edt_SIZE.Text := qryListD2.FieldByName('SIZE1').AsString;
  edt_SIZE1.Text := qryListD2.FieldByName('SIZE1').AsString;
  edt_SIZE2.Text := qryListD2.FieldByName('SIZE2').AsString;
  edt_SIZE3.Text := qryListD2.FieldByName('SIZE3').AsString;
  edt_SIZE4.Text := qryListD2.FieldByName('SIZE4').AsString;
  edt_SIZE5.Text := qryListD2.FieldByName('SIZE5').AsString;
  edt_SIZE6.Text := qryListD2.FieldByName('SIZE6').AsString;
  edt_SIZE7.Text := qryListD2.FieldByName('SIZE7').AsString;
  edt_SIZE8.Text := qryListD2.FieldByName('SIZE8').AsString;
  edt_SIZE9.Text := qryListD2.FieldByName('SIZE9').AsString;
  edt_SIZE10.Text := qryListD2.FieldByName('SIZE10').AsString;

  edt_QTYC.Text := qryListD2.FieldByName('QTYC').AsString;
  edt_QTY.Text := FloatToStr(qryListD2.FieldByName('QTY').AsFloat);
  edt_TOTQTYC.Text := qryListD2.FieldByName('TOTQTYC').AsString;
  edt_TOTQTY.Text := qryListD2.FieldByName('TOTQTY').AsString;
  edt_PRICE.Text := FloatToStr(qryListD2.FieldByName('PRICE').AsFloat);
  edt_PRICEC.Text := qryListD2.FieldByName('PRICEC').AsString;
  edt_PRICE_G.Text := FloatToStr(qryListD2.FieldByName('PRICE_G').AsFloat);
  edt_SUP_AMTC.Text := qryListD2.FieldByName('SUP_AMTC').AsString;
  edt_SUP_AMT.Text := FloatToStr(qryListD2.FieldByName('SUP_AMT').AsFloat);
  edt_SUP_TOTAMTC.Text := qryListD2.FieldByName('SUP_TOTAMTC').AsString;
  edt_SUP_TOTAMT.Text := FloatToStr(qryListD2.FieldByName('SUP_TOTAMT').AsFloat);
  edt_VB_AMTC.Text := qryListD2.FieldByName('VB_AMTC').AsString;
  edt_VB_AMT.Text := FloatToStr(qryListD2.FieldByName('VB_AMT').AsFloat);
  edt_VB_TOTAMTC.Text := qryListD2.FieldByName('VB_TOTAMTC').AsString;
  edt_VB_TOTAMT.Text := FloatToStr(qryListD2.FieldByName('VB_TOTAMT').AsFloat);
  edt_VB_TAXC.Text :=  qryListD2.FieldByName('VB_TAXC').AsString;
  edt_VB_TAX.Text := FloatToStr(qryListD2.FieldByName('VB_TAX').AsFloat);
  edt_VB_TOTTAXC.Text := qryListD2.FieldByName('VB_TOTTAXC').AsString;
  edt_VB_TOTTAX.Text := FloatToStr(qryListD2.FieldByName('VB_TOTTAX').AsFloat);
  edt_CUX_RATE.Text := FloatToStr(qryListD2.FieldByName('CUX_RATE').AsFloat);

  edt_BIGO.Text := qryListD2.FieldByName('BIGO1').AsString;
  edt_BIGO1.Text := qryListD2.FieldByName('BIGO1').AsString;
  edt_BIGO2.Text := qryListD2.FieldByName('BIGO2').AsString;
  edt_BIGO3.Text := qryListD2.FieldByName('BIGO3').AsString;
  edt_BIGO4.Text := qryListD2.FieldByName('BIGO4').AsString;
  edt_BIGO5.Text := qryListD2.FieldByName('BIGO5').AsString;

end;

procedure TUI_APPSPC_NEW_DOC2_frm.btnSaveClick(Sender: TObject);
var
  SQLCreate_D1 : TSQLCreate;
begin
  inherited;
  //유효성검사
  if ReadWrite_D2 then
  begin
    ShowMessage('작성중인 품목사항이 있습니다.');
    Exit;
  end;
  
  Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
  if Dlg_ErrorMessage_frm.Run_ErrorMessage( '판매대금추심(매입)의뢰서 - 세금계산서' ,ErrorCheck ) Then
  begin
    FreeAndNil(Dlg_ErrorMessage_frm);
    Exit;
  end;
  SQLCreate_D1 := TSQLCreate.Create;

  with SQLCreate_D1 Do
  begin
    DMLType := dmlUpdate;
    SQLCreate_D1.SQLHeader('APPSPC_D1');

    ADDWhere('KEYY',UI_APPSPC_NEW_frm.Maint_No);
    ADDWhere('DOC_GUBUN','2AJ');
    ADDWhere('SEQ',UI_APPSPC_NEW_frm.SEQ);


    //문서번호
    ADDValue('DOC_NO',edt_DOC_NO.Text);
    //권
    ADDValue('VB_RENO',edt_VB_RENO.Text);
    //호
    ADDValue('VB_SENO',edt_VB_SENO.Text);
    //일련번호
    ADDValue('VB_FSNO',edt_VB_FSNO.Text);
    //관리번호
    ADDValue('VB_MAINTNO',edt_VB_MAINTNO.Text);
    //작성일
    ADDValue('ISS_DATE',msk_ISS_DATE.Text);
    //참조번호
    ADDValue('VB_DMNO',edt_VB_DMNO.Text);

    //공급자
    //공급자상호코드
    ADDValue('SE_CODE',edt_SE_CODE.Text);
    //공급자상호
    ADDValue('SE_SNAME',edt_SE_SNAME.Text);
    //대표자
    ADDValue('SE_NAME',edt_SE_NAME.Text);
    //사업자번호
    ADDValue('SE_SAUPNO',msk_SE_SAUPNO.Text);
    //전자서명
    ADDValue('SE_ELEC',edt_SE_ELEC.Text);
    //주소
    ADDValue('SE_ADDR1',edt_SE_ADDR1.Text);
    ADDValue('SE_ADDR2',edt_SE_ADDR2.Text);
    ADDValue('SE_ADDR3',edt_SE_ADDR3.Text);
    //업태
    ADDValue('SG_UPTAI1_1',edt_SG_UPTAI1_1.Text);
    ADDValue('SG_UPTAI1_2',edt_SG_UPTAI1_2.Text);
    ADDValue('SG_UPTAI1_3',edt_SG_UPTAI1_3.Text);
    //종목
    ADDValue('HN_ITEM1_1',edt_HN_ITEM1_1.Text);
    ADDValue('HN_ITEM1_2',edt_HN_ITEM1_2.Text);
    ADDValue('HN_ITEM1_3',edt_HN_ITEM1_3.Text);

    //공급받는자
    //공급받는자상호코드
    ADDValue('BY_CODE',edt_BY_CODE.Text);
    //공급받는자상호
    ADDValue('BY_SNAME',edt_BY_SNAME.Text);
    //대표자
    ADDValue('BY_NAME',edt_BY_NAME.Text);
    //사업자번호
    ADDValue('BY_SAUPNO',msk_BY_SAUPNO.Text);
    //전자서명
    ADDValue('BY_ELEC',edt_BY_ELEC.Text);
    //주소
    ADDValue('BY_ADDR1',edt_BY_ADDR1.Text);
    ADDValue('BY_ADDR2',edt_BY_ADDR2.Text);
    ADDValue('BY_ADDR3',edt_BY_ADDR3.Text);
    //업태
    ADDValue('SG_UPTAI2_1',edt_SG_UPTAI2_1.Text);
    ADDValue('SG_UPTAI2_2',edt_SG_UPTAI2_2.Text);
    ADDValue('SG_UPTAI2_3',edt_SG_UPTAI2_3.Text);
    //종목
    ADDValue('HN_ITEM2_1',edt_HN_ITEM2_1.Text);
    ADDValue('HN_ITEM2_2',edt_HN_ITEM2_2.Text);
    ADDValue('HN_ITEM2_3',edt_HN_ITEM2_3.Text);

    //수탁자
    //수탁자상호코드
    ADDValue('AG_CODE',edt_AG_CODE.Text);
    //수탁자상호
    ADDValue('AG_SNAME',edt_AG_SNAME.Text);
    //대표자
    ADDValue('AG_NAME',edt_AG_NAME.Text);
    //사업자번호
    ADDValue('AG_SAUPNO',msk_AG_SAUPNO.Text);
    //전자서명
    ADDValue('AG_ELEC',edt_AG_ELEC.Text);
    //주소
    ADDValue('AG_ADDR1',edt_AG_ADDR1.Text);
    ADDValue('AG_ADDR2',edt_AG_ADDR2.Text);
    ADDValue('AG_ADDR3',edt_AG_ADDR3.Text);

    //현금
    //원화금액
    ADDValue('PAI_AMT1',edt_PAI_AMT1.Value);
    //통화단위
    ADDValue('PAI_AMTC1',edt_PAI_AMTC1.Text);
    //외화금액
    ADDValue('PAI_AMTU1',edt_PAI_AMTU1.Value);


    //수표
    //원화금액
    ADDValue('PAI_AMT2',edt_PAI_AMT2.Value);
    //통화단위
    ADDValue('PAI_AMTC2',edt_PAI_AMTC2.Text);
    //외화금액
    ADDValue('PAI_AMTU2',edt_PAI_AMTU2.Value);

    //어음
    //원화금액
    ADDValue('PAI_AMT3',edt_PAI_AMT3.Value);
    //통화단위
    ADDValue('PAI_AMTC3',edt_PAI_AMTC3.Text);
    //외화금액
    ADDValue('PAI_AMTU3',edt_PAI_AMTU3.Value);


    //외상
    //원화금액
    ADDValue('PAI_AMT4',edt_PAI_AMT4.Value);
    //통화단위
    ADDValue('PAI_AMTC4',edt_PAI_AMTC4.Text);
    //외화금액
    ADDValue('PAI_AMTU4',edt_PAI_AMTU4.Value);

    //공급가액
    ADDValue('ISS_EXPAMT',edt_ISS_EXPAMT.Value);
    //세액
    ADDValue('ISS_EXPAMTU',edt_ISS_EXPAMTU.Value);
    //영수/청구 구분
    ADDValue('VB_GUBUN',edt_VB_GUBUN.Text);
    //공란수
    ADDValue('VB_DETAILNO',edt_VB_DETAILNO.Text);
    //총수량
    ADDValue('TOTCNT', edt_TOTCNT.Value);
    //총수량단위
    ADDValue('TOTCNTC', edt_TOTCNTC.Text);

    //비고
    ADDValue('REMARK1_1',edt_REMARK1_1.Text);
    ADDValue('REMARK1_2',edt_REMARK1_2.Text);
    ADDValue('REMARK1_3',edt_REMARK1_3.Text);
    ADDValue('REMARK1_4',edt_REMARK1_4.Text);
    ADDValue('REMARK1_5',edt_REMARK1_5.Text);

    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SQLCreate_D1.CreateSQL +#13#10;
        SQL.Add('DELETE FROM APPSPC_D2 WHERE KEYY='+QuotedStr('0'));
        ExecSQL;
        ShowMessage('저장되었습니다.');
      finally
        close;
        free;
      end;
    end;
  end;
  Close;
  with UI_APPSPC_NEW_frm.qryListD1_2AJ do
  begin
    Close;
    Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
    Open;
    UI_APPSPC_NEW_frm.sPanel58.Caption := '세금계산서 : '+IntToStr(RecordCount)+'건';
  end;

end;

procedure TUI_APPSPC_NEW_DOC2_frm.edt_SIZEExit(Sender: TObject);
begin
  inherited;
  edt_SIZE1.Text := edt_SIZE.Text;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.edt_IMD_CODEExit(Sender: TObject);
begin
  inherited;
  edt_IMD_CODE1.Text := edt_IMD_CODE.Text;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.btnAddD2Click(Sender: TObject);
begin
  inherited; 
  mode_D2 := 1;//신규
  ReadWrite_D2 := True;
  EnableControl(sPanel16);
  ClearControl(sPanel16);            
  ClearControl(sPanel4);
  ClearControl(sPanel1);
  ClearControl(sPanel3);
  BtnControl(1);
  edt_LINE_NO.Enabled := False;
  edt_LINE_NO.Text := Format('%0.4d',[qryListD2.recordCount+1]);
end;

procedure TUI_APPSPC_NEW_DOC2_frm.btnCancelD2Click(Sender: TObject);
begin
  inherited;
  EnableControl(sPanel16,False);
  BtnControl(2);
  qryListD2.First;  
  ReadWrite_D2 := False;

end;

procedure TUI_APPSPC_NEW_DOC2_frm.btnEdtD2Click(Sender: TObject);
begin
  inherited;   
  if qryListD2.RecordCount = 0 then
  begin
    ShowMessage('수정할 세금계산서 품목내역이 없습니다.');
    exit;
  end;
  ReadWrite_D2 := True;
  mode_D2 := 2; //수정
  EnableControl(sPanel16);
  BtnControl(3);
  edt_LINE_NO.Enabled := False;

end;

procedure TUI_APPSPC_NEW_DOC2_frm.BtnControl(tag:Integer);
begin
  case Tag of
    1://입력
    begin
      btnAddD2.Enabled := False;
      btnCancelD2.Enabled := True;
      btnEdtD2.Enabled := False;
      btnSaveD2.Enabled := True;
      btnDelD2.Enabled := False;
    end;
    2://취소
    begin
      btnAddD2.Enabled := True;
      btnCancelD2.Enabled := False;
      btnEdtD2.Enabled := True;
      btnSaveD2.Enabled := False;
      btnDelD2.Enabled := True;
    end;
    3://수정
    begin
      btnAddD2.Enabled := False;
      btnCancelD2.Enabled := True;
      btnEdtD2.Enabled := False;
      btnSaveD2.Enabled := True;
      btnDelD2.Enabled := False;
    end;
    4://저장
    begin
      btnAddD2.Enabled := True;
      btnCancelD2.Enabled := False;
      btnEdtD2.Enabled := True;
      btnSaveD2.Enabled := False;
      btnDelD2.Enabled := True;
    end;
    5://삭제
    begin
      btnAddD2.Enabled := True;
      btnCancelD2.Enabled := False;
      btnEdtD2.Enabled := True;
      btnSaveD2.Enabled := False;
      btnDelD2.Enabled := True;
    end;
  end;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.btnSaveD2Click(Sender: TObject);
var
  SQLCreate_D2:TSQLCreate;
  Line_No:String;
begin
  inherited;
  SQLCreate_D2 := TSQLCreate.Create;

  with SQLCreate_D2 do
  begin
    case mode_D2 of
      1://신규-저장
      begin
        DMLType := dmlInsert;
        SQLHeader('APPSPC_D2');

        ADDValue('KEYY',UI_APPSPC_NEW_frm.Maint_No);
        ADDValue('DOC_GUBUN','2AJ');
        ADDValue('SEQ',UI_APPSPC_NEW_frm.SEQ);
        ADDValue('LINE_NO',edt_LINE_NO.Text);

        //공급일자
        ADDValue('DE_DATE',msk_DE_DATE.Text);
        //품목
        ADDValue('IMD_CODE1', edt_IMD_CODE.Text);
        ADDValue('IMD_CODE2', edt_IMD_CODE2.Text);
        ADDValue('IMD_CODE3', edt_IMD_CODE3.Text);
        ADDValue('IMD_CODE4', edt_IMD_CODE4.Text);
        //규격
        ADDValue('SIZE1', edt_SIZE.Text);
        ADDValue('SIZE2', edt_SIZE2.Text);
        ADDValue('SIZE3', edt_SIZE3.Text);
        ADDValue('SIZE4', edt_SIZE4.Text);
        ADDValue('SIZE5', edt_SIZE5.Text);
        ADDValue('SIZE6', edt_SIZE6.Text);
        ADDValue('SIZE7', edt_SIZE7.Text);
        ADDValue('SIZE8', edt_SIZE8.Text);
        ADDValue('SIZE9', edt_SIZE9.Text);
        ADDValue('SIZE10', edt_SIZE10.Text);
        //수량단위
        ADDValue('QTYC',edt_QTYC.Text);
        //수량
        ADDValue('QTY', edt_QTY.Value);
        //수량소계단위
        ADDValue('TOTQTYC', edt_TOTQTYC.Text);
        //수량소계
        ADDValue('TOTQTY', edt_TOTQTY.Value);

        //단가
        ADDValue('PRICE', edt_PRICE.Value);
        //기준수량단위
        ADDValue('PRICEC', edt_PRICEC.Text);
        //기준수량
        ADDValue('PRICE_G', edt_PRICE_G.Value);

        //공급가액 통화단위
        ADDValue('SUP_AMTC', edt_SUP_AMTC.Text);
        //공급가액
        ADDValue('SUP_AMT', edt_SUP_AMT.Value);
        //공급가액소계 통화단위
        ADDValue('SUP_TOTAMTC', edt_SUP_TOTAMTC.Text);
        //공급가액소계
        ADDValue('SUP_TOTAMT', edt_SUP_TOTAMT.Value);

        //공급가액외화 통화단위
        ADDValue('VB_AMTC', edt_VB_AMTC.Text);
        //공급가액외화
        ADDValue('VB_AMT', edt_VB_AMT.Value);
        //공급가액외화소계 통화단위
        ADDValue('VB_TOTAMTC', edt_VB_TOTAMTC.Text);
        //공급가액외화소계
        ADDValue('VB_TOTAMT', edt_VB_TOTAMT.Value);

        //세액 통화단위
        ADDValue('VB_TAXC', edt_VB_TAXC.Text);
        //세액
        ADDValue('VB_TAX', edt_VB_TAX.Value);
        //세액소계 통화단위
        ADDValue('VB_TOTTAXC', edt_VB_TOTTAXC.Text);
        //세액소계
        ADDValue('VB_TOTTAX', edt_VB_TOTTAX.Value);

        //환율
        ADDValue('CUX_RATE', edt_CUX_RATE.Value);
        //참조사항
        ADDValue('BIGO1', edt_BIGO.Text);
        ADDValue('BIGO2', edt_BIGO2.Text);
        ADDValue('BIGO3', edt_BIGO3.Text);
        ADDValue('BIGO4', edt_BIGO4.Text);
        ADDValue('BIGO5', edt_BIGO5.Text);


      end;
      2://수정-저장
      begin
        DMLType := dmlUpdate;
        SQLHeader('APPSPC_D2');

        ADDWhere('KEYY',UI_APPSPC_NEW_frm.Maint_No);
        ADDWhere('DOC_GUBUN','2AJ');
        ADDWhere('SEQ',UI_APPSPC_NEW_frm.SEQ);
        ADDWhere('LINE_NO',edt_LINE_NO.Text);

        //공급일자
        ADDValue('DE_DATE',msk_DE_DATE.Text);
        //품목
        ADDValue('IMD_CODE1', edt_IMD_CODE.Text);
//        ADDValue('IMD_CODE2', edt_IMD_CODE2.Text);
//        ADDValue('IMD_CODE3', edt_IMD_CODE3.Text);
//        ADDValue('IMD_CODE4', edt_IMD_CODE4.Text);
        //규격
        ADDValue('SIZE1', edt_SIZE.Text);
//        ADDValue('SIZE2', edt_SIZE2.Text);
//        ADDValue('SIZE3', edt_SIZE3.Text);
//        ADDValue('SIZE4', edt_SIZE4.Text);
//        ADDValue('SIZE5', edt_SIZE5.Text);
//        ADDValue('SIZE6', edt_SIZE6.Text);
//        ADDValue('SIZE7', edt_SIZE7.Text);
//        ADDValue('SIZE8', edt_SIZE8.Text);
//        ADDValue('SIZE9', edt_SIZE9.Text);
//        ADDValue('SIZE10', edt_SIZE10.Text);
        //수량단위
        ADDValue('QTYC',edt_QTYC.Text);
        //수량
        ADDValue('QTY', edt_QTY.Value);
        //수량소계단위
        ADDValue('TOTQTYC', edt_TOTQTYC.Text);
        //수량소계
        ADDValue('TOTQTY', edt_TOTQTY.Value);

        //단가
        ADDValue('PRICE', edt_PRICE.Value);
        //기준수량단위
        ADDValue('PRICEC', edt_PRICEC.Text);
        //기준수량
        ADDValue('PRICE_G', edt_PRICE_G.Value);

        //공급가액 통화단위
        ADDValue('SUP_AMTC', edt_SUP_AMTC.Text);
        //공급가액
        ADDValue('SUP_AMT', edt_SUP_AMT.Value);
        //공급가액소계 통화단위
        ADDValue('SUP_TOTAMTC', edt_SUP_TOTAMTC.Text);
        //공급가액소계
        ADDValue('SUP_TOTAMT', edt_SUP_TOTAMT.Value);

        //공급가액외화 통화단위
        ADDValue('VB_AMTC', edt_VB_AMTC.Text);
        //공급가액외화
        ADDValue('VB_AMT', edt_VB_AMT.Value);
        //공급가액외화소계 통화단위
        ADDValue('VB_TOTAMTC', edt_VB_TOTAMTC.Text);
        //공급가액외화소계
        ADDValue('VB_TOTAMT', edt_VB_TOTAMT.Value);

        //세액 통화단위
        ADDValue('VB_TAXC', edt_VB_TAXC.Text);
        //세액
        ADDValue('VB_TAX', edt_VB_TAX.Value);
        //세액소계 통화단위
        ADDValue('VB_TOTTAXC', edt_VB_TOTTAXC.Text);
        //세액소계
        ADDValue('VB_TOTTAX', edt_VB_TOTTAX.Value);

        //환율
        ADDValue('CUX_RATE', edt_CUX_RATE.Value);
        //참조사항
        ADDValue('BIGO1', edt_BIGO.Text);
//        ADDVAL ADDValue('BIGO2', edt_BIGO2.Text);
//        ADDVAL ADDValue('BIGO3', edt_BIGO3.Text);
//        ADDVAL ADDValue('BIGO4', edt_BIGO4.Text);
//        ADDVAL ADDValue('BIGO5', edt_BIGO5.Text);

      end;
    end;
  end;

  with TADOQuery.Create(Nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := SQLCreate_D2.CreateSQL;
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;

  with qryListD2 do
  begin
    Close;
    Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
    Parameters[1].Value := '2AJ';
    Parameters[2].Value := UI_APPSPC_NEW_frm.SEQ;
    Open;
    qryListD2.Locate('LINE_NO', Line_No, [] ) ;
  end;

  ReadWrite_D2 := False;
  sDBGrid4.Fields[1].EditMask := '9999-99-99;0;';
  EnableControl(sPanel16, False);
  BtnControl(4);
end;

procedure TUI_APPSPC_NEW_DOC2_frm.btnDelD2Click(Sender: TObject);
var
  SQLCreate_D2:TSQLCreate;
  sSQL_D2:String;
  iLINE_NO,iSize:Integer;
begin
  inherited;
  
  if qryListD2.RecordCount = 0 then
  begin
    ShowMessage('삭제할 세금계산서 품목내역이 없습니다.');
    exit;
  end;
  SQLCreate_D2 := TSQLCreate.Create;
  with SQLCreate_D2 do
  begin
    DMLType := dmlDelete;
    SQLHeader('APPSPC_D2');
    ADDWhere('KEYY',UI_APPSPC_NEW_frm.Maint_No);
    ADDWhere('DOC_GUBUN','2AJ');
    ADDWhere('SEQ',UI_APPSPC_NEW_frm.SEQ);
    ADDWhere('LINE_NO',qryListD2.FieldByName('Line_No').AsString);
  end;
  iLINE_NO := qryListD2.FieldByName('Line_No').AsInteger;
  ShowMessage(IntToStr(iLINE_NO));
  
//  SQL_D2 := 'UPDATE APPSPC_D2 SET SEQ = CONVERT(INT,SEQ-1) WHERE CONVERT(INT,SEQ1)';
//  SQL_D1 := 'UPDATE APPSPC_D1 SET SEQ = SEQ -1 WHERE KEYY = '+QuotedStr(Maint_No)+'AND SEQ > '+IntToStr(SEQ)+'AND DOC_GUBUN = '+QuotedStr('2AJ');
  sSQL_D2 := 'UPDATE APPSPC_D2 ';
  sSQL_D2 := sSQL_D2 + 'SET LINE_NO = REPLICATE('+QuotedStr('0')+',4-LEN(CONVERT(INT,LINE_NO)-1)) + CONVERT(VARCHAR(8),CONVERT(INT,LINE_NO)-1)';
  sSQL_D2 := sSQL_D2 + 'WHERE CONVERT(INT,LINE_NO) > '+IntToStr(iLINE_NO)+' and KEYY ='+UI_APPSPC_NEW_frm.Maint_No+' and SEQ = '+IntToStr(UI_APPSPC_NEW_frm.SEQ)+' and DOC_GUBUN = '+QuotedStr('2AJ');
//  ShowMessage(sSQL_D2);
  with TADOQuery.Create(Nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := SQLCreate_D2.CreateSQL+#13#10+sSQL_D2;
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;

  qryListD2.Close;
  qryListD2.Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
  qryListD2.Parameters[1].Value := '2AJ';
  qryListD2.Parameters[2].Value := UI_APPSPC_NEW_frm.SEQ;
  qryListD2.Open;

  sDBGrid4.Fields[1].EditMask := '9999-99-99;0;';
  
  ReadWrite_D2 := False;
  BtnControl(5);
end;

procedure TUI_APPSPC_NEW_DOC2_frm.btnAdd_IMDClick(Sender: TObject);
begin
  inherited;
  if btnAdd_IMD.Caption = '+' then
  begin
    btnAdd_IMD.Caption := '-';
    sPanel1.Visible := True;
    edt_IMD_CODE1.SetFocus;
  end
  else if btnAdd_IMD.Caption = '-' then
    sButton1Click(Self);
end;

procedure TUI_APPSPC_NEW_DOC2_frm.btnAdd_SIZEClick(Sender: TObject);
begin
  inherited;
  if btnAdd_SIZE.Caption = '+' then
  begin
    btnAdd_SIZE.Caption := '-';
    sPanel3.Visible := True;
    edt_SIZE1.SetFocus;
  end
  else if btnAdd_IMD.Caption = '-' then
    sButton2Click(Self);

end;

procedure TUI_APPSPC_NEW_DOC2_frm.sButton2Click(Sender: TObject);
begin
  inherited;
  sPanel3.Visible := False;
  btnAdd_SIZE.Caption := '+';
end;

procedure TUI_APPSPC_NEW_DOC2_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  sPanel1.Visible := False;
  btnAdd_IMD.Caption := '+'
end;

procedure TUI_APPSPC_NEW_DOC2_frm.edt_BIGOExit(Sender: TObject);
begin
  inherited;
  edt_BIGO1.Text := edt_BIGO.Text;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.edt_SIZE1Exit(Sender: TObject);
begin
  inherited;
  edt_SIZE.Text := edt_SIZE1.Text;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.edt_IMD_CODE1Exit(Sender: TObject);
begin
  inherited;
  edt_IMD_CODE.Text := edt_IMD_CODE1.Text;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.edt_BIGO1Exit(Sender: TObject);
begin
  inherited;
  edt_BIGO.Text := edt_BIGO1.Text;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.btnAdd_BIGOClick(Sender: TObject);
begin
  inherited;
  if btnAdd_BIGO.Caption = '+' then
  begin
    btnAdd_BIGO.Caption := '-';
    sPanel4.Visible := True;
    edt_BIGO1.SetFocus;
  end
  else if btnAdd_BIGO.Caption = '-' then
    sButton3Click(Self);

end;

procedure TUI_APPSPC_NEW_DOC2_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := False;
  btnAdd_BIGO.Caption := '+';

end;

procedure TUI_APPSPC_NEW_DOC2_frm.CODEDblClick(Sender: TObject);
var
  CodeDialog : TCodeDialogParent_frm;
begin
  inherited;
  CodeDialog := nil;
  case TsEdit(Sender).Tag of
    //거래처코드
    101,102,103: CodeDialog := TCD_CUST_frm.Create(Self);
    //통화단위
    104,105,106,107,112,113,114,115,116,117: CodeDialog := TCD_AMT_UNIT_frm.Create(Self);
    //수량단위
    108,109,110,111: CodeDialog := TCD_UNIT_frm.Create(Self);
  end;

  try
    if CodeDialog.OpenDialog(0, TsEdit(Sender).Text) then
    begin
      TsEdit(Sender).Text := CodeDialog.FieldValues('Code');
      case TsEdit(Sender).Tag of
        101://공급자
        begin
          edt_SE_SNAME.Text := CodeDialog.FieldValues('ENAME');
          edt_SE_NAME.Text := CodeDialog.FieldValues('REP_NAME');
          msk_SE_SAUPNO.Text := CodeDialog.FieldValues('SAUP_NO');
          edt_SE_ELEC.Text := CodeDialog.FieldValues('Jenja');
          edt_SE_ADDR1.Text := CodeDialog.FieldValues('ADDR1');
          edt_SE_ADDR2.Text := CodeDialog.FieldValues('ADDR2');
          edt_SE_ADDR3.Text := CodeDialog.FieldValues('ADDR3');
        end;
        102://공급받는자
        begin
          edt_BY_SNAME.Text := CodeDialog.FieldValues('ENAME');
          edt_BY_NAME.Text := CodeDialog.FieldValues('REP_NAME');
          msk_BY_SAUPNO.Text := CodeDialog.FieldValues('SAUP_NO');
          edt_BY_ELEC.Text := CodeDialog.FieldValues('Jenja');
          edt_BY_ADDR1.Text := CodeDialog.FieldValues('ADDR1');
          edt_BY_ADDR2.Text := CodeDialog.FieldValues('ADDR2');
          edt_BY_ADDR3.Text := CodeDialog.FieldValues('ADDR3');

        end;
        103://수탁자
        begin
          edt_AG_SNAME.Text := CodeDialog.FieldValues('ENAME');
          edt_AG_NAME.Text := CodeDialog.FieldValues('REP_NAME');
          msk_AG_SAUPNO.Text := CodeDialog.FieldValues('SAUP_NO');
          edt_AG_ELEC.Text := CodeDialog.FieldValues('Jenja');
          edt_AG_ADDR1.Text := CodeDialog.FieldValues('ADDR1');
          edt_AG_ADDR2.Text := CodeDialog.FieldValues('ADDR2');
          edt_AG_ADDR3.Text := CodeDialog.FieldValues('ADDR3');
        end;
      end;
    end;

  finally
    FreeAndNil(CodeDialog);
  end;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.FormShow(Sender: TObject);
begin
  inherited;
  sDBGrid4.Fields[1].EditMask := '9999-99-99;0;';
end;

procedure TUI_APPSPC_NEW_DOC2_frm.sBitBtn26Click(Sender: TObject);
var
  SQLCreate_D2:TSQLCreate;
  isCopy:Boolean;
  i:Integer;
begin
  inherited;
  dlg_CopyAPPSPCfromVATBI2_frm := Tdlg_CopyAPPSPCfromVATBI2_frm.Create(Self);
  try
    with dlg_CopyAPPSPCfromVATBI2_frm do
    begin
          isCopy := OpenDialog(0);
          IF not isCopy Then
            Abort;
          if qryListD2.RecordCount > 0 then
          begin
            if MessageDlg('작성된 품목내역이 있습니다.  덮어쓰시겠습니까?',mtWarning,mbOKCancel,0) = mrOk then
            begin
              with TADOQuery.Create(Nil) do
              begin
                try
                  Connection := DMMssql.KISConnect;
                  SQL.Text := 'DELETE FROM APPSPC_D2 WHERE KEYY='+QuotedStr(UI_APPSPC_NEW_frm.Maint_No);
                  ExecSQL;
                finally
                  Close;
                  Free;
                end;
              end;
            end
            else
              exit;
          end;
          //문서번호
          edt_DOC_NO.Text := FieldValues('MAINT_NO');
          //권,호,일련번호
          edt_VB_RENO.Text := FieldValues('RE_NO');
          edt_VB_SENO.Text := FieldValues('SE_NO');
          edt_VB_FSNO.Text := FieldValues('FS_NO');
          //관리번호
          edt_VB_MAINTNO.Text := FieldValues('RFF_NO');
          //참조번호
          edt_VB_DMNO.Text := FieldValues('ACE_NO');
          //작성일
          msk_ISS_DATE.Text := FieldValues('DATEE');

          //공급자 코드
          edt_SE_CODE.Text := FieldValues('SE_CODE');
          //상호
          edt_SE_SNAME.Text := FieldValues('SE_NAME1');
          //대표자명
          edt_SE_NAME.Text := FieldValues('SE_NAME3');
          //사업자번호
          msk_SE_SAUPNO.Text := FieldValues('SE_SAUP');
          //주소
          edt_SE_ADDR1.Text := FieldValues('SE_ADDR1');
          edt_SE_ADDR2.Text := FieldValues('SE_ADDR2');
          edt_SE_ADDR3.Text := FieldValues('SE_ADDR3');
          //업태
          edt_SG_UPTAI1_1.Text := FieldValues('SE_UPTA1');
          //종목
          edt_HN_ITEM1_1.Text := FieldValues('SE_ITEM1');

          //공급받는자 코드
          edt_BY_CODE.Text := FieldValues('BY_CODE');
          //상호                         0
          edt_BY_SNAME.Text := FieldValues('BY_NAME1');
          //대표자명
          edt_BY_NAME.Text := FieldValues('BY_NAME3');
          //사업자번호
          msk_BY_SAUPNO.Text := FieldValues('BY_SAUP');
          //주소
          edt_BY_ADDR1.Text := FieldValues('BY_ADDR1');
          edt_BY_ADDR2.Text := FieldValues('BY_ADDR2');
          edt_BY_ADDR3.Text := FieldValues('BY_ADDR3');
          //업태
          edt_SG_UPTAI2_1.Text := FieldValues('BY_UPTA1');
          //종목
          edt_HN_ITEM2_1.Text := FieldValues('BY_ITEM1');

          //수탁자
          edt_AG_CODE.Text := FieldValues('AG_CODE');
          //상호
          edt_AG_SNAME.Text := FieldValues('AG_NAME1');
          //대표자명
          edt_AG_NAME.Text := FieldValues('AG_NAME3');
          //사업자번호
          msk_AG_SAUPNO.Text := FieldValues('AG_SAUP');
          //주소
          edt_AG_ADDR1.Text := FieldValues('AG_ADDR1');
          edt_AG_ADDR2.Text := FieldValues('AG_ADDR2');
          edt_AG_ADDR3.Text := FieldValues('AG_ADDR3');

          //현금
          //원화
          edt_PAI_AMT1.Value := FieldValues('AMT12');
          //외화
          edt_PAI_AMTU1.Value := FieldValues('AMT11');
          //통화단위
          edt_PAI_AMTC1.Text := FieldValues('AMT11C');

          //수표
          //원화
          edt_PAI_AMT2.Value := FieldValues('AMT22');
          //외화
          edt_PAI_AMTU2.Value := FieldValues('AMT21');
          //통화단위
          edt_PAI_AMTC2.Text := FieldValues('AMT21C');

          //어음
          //원화
          edt_PAI_AMT3.Value := FieldValues('AMT32');
          //외화
          edt_PAI_AMTU3.Value := FieldValues('AMT31');
          //통화단위
          edt_PAI_AMTC3.Text := FieldValues('AMT31C');

          //외상
          //원화
          edt_PAI_AMT4.Value := FieldValues('AMT42');
          //외화
          edt_PAI_AMTU4.Value := FieldValues('AMT41');
          //통화단위
          edt_PAI_AMTC4.Text := FieldValues('AMT41C');

          //공급가액
          edt_ISS_EXPAMT.Value := FieldValues('SUP_AMT');
          //세액
          edt_ISS_EXPAMTU.Value := FieldValues('TAX_AMT');
          //공란수
          edt_VB_DETAILNO.text := FieldValues('DETAILNO');
          //총수량
          edt_TOTCNT.Value := FieldValues('TQTY');
          //총수량단위
          edt_TOTCNTC.Text := FieldValues('TQTYC');

          //청수/영구구분
          edt_VB_GUBUN.Text := FieldValues('INDICATOR');

  END;
  finally
    FreeAndNil(dlg_CopyAPPSPCfromVATBI2_frm);//dlg_CopyAPPSPCfromLOCRC1_frm);
  end;

  with qryDOCClick do
  begin
      Close;
      Parameters[0].Value := edt_DOC_NO.Text;
      Open;
  end;

  SQLCreate_D2 := TSQLCreate.Create;
  qryDOCClick.First;

  for i:= 1 to qryDOCClick.RecordCount do
  begin
    with SQLCreate_D2 do
    begin
      DMLType := dmlInsert;
      SQLHeader('APPSPC_D2');

      ADDValue('KEYY', UI_APPSPC_NEW_frm.Maint_No);
      ADDValue('DOC_GUBUN', '2AJ');
      ADDValue('SEQ', UI_APPSPC_NEW_frm.SEQ);
      ADDValue('LINE_NO', Format('%0.4d',[qryDOCClick.FieldByName('SEQ').AsInteger]));

      //공급일자
      ADDValue('DE_DATE', qryDOCClick.FieldByName('DE_DATE').AsString);
      //수량단위
      ADDValue('QTYC', qryDOCClick.FieldByName('QTY_G').AsString);
      //수량
      ADDValue('QTY', qryDOCClick.FieldByName('QTY').AsFloat);
      //수량소계단위
      ADDValue('TOTQTYC', qryDOCClick.FieldByName('STQTY_G').AsString);
      //수량소계
      ADDValue('TOTQTY', qryDOCClick.FieldByName('STQTY').AsFloat);

      //단가
      ADDValue('PRICE', qryDOCClick.FieldByName('PRICE').AsFloat);
      //기순수량단위
      ADDValue('PRICEC', qryDOCClick.FieldByName('QTYG_G').AsString);
      //기준수량
      ADDValue('PRICE_G', qryDOCClick.FieldByName('QTYG').AsFloat);

      //공급가액 통화단위
      ADDValue('SUP_AMTC', 'KRW');
      //공급가액
      ADDValue('SUP_AMT', qryDOCClick.FieldByName('SUPAMT').AsFloat);
      //공급가액소계 통화단위
      ADDValue('SUP_TOTAMTC', 'KRW');
      //공급가액소계
      ADDValue('SUP_TOTAMT', qryDOCClick.FieldByName('SUPSTAMT').AsFloat);

      //공급가액외화 통화단위
      ADDValue('VB_AMTC', qryDOCClick.FieldByName('USAMT_G').AsString);
      //공급가액외화
      ADDValue('VB_AMT', qryDOCClick.FieldByName('USAMT').AsFloat);
      //공급가액소계외화 통화단위
      ADDValue('VB_TOTAMTC', qryDOCClick.FieldByName('USSTAMT_G').AsString);
      //공급가액소계외화
      ADDValue('VB_TOTAMT', qryDOCClick.FieldByName('USSTAMT').AsFloat);

      //세액 통화단위
      ADDValue('VB_TAXC', 'KRW');
      //세액
      ADDValue('VB_TAX', qryDOCClick.FieldByName('TAXAMT').AsFloat);
      //세액소계 통화단위
      ADDValue('VB_TOTTAXC', 'KRW');
      //세액소계
      ADDValue('VB_TOTTAX', qryDOCClick.FieldByName('TAXSTAMT').AsFloat);
      //환율
      ADDValue('CUX_RATE', qryDOCClick.FieldByName('RATE').AsFloat);

    end;
    with TADOQuery.Create(Nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SQLCreate_D2.CreateSQL;
        ExecSQL;
      finally
        Close;
        Free;
      end;
    end;

    qryDOCClick.Next;
  end;

  qryListD2.Close;
  qryListD2.Parameters[0].Value := UI_APPSPC_NEW_frm.Maint_No;
  qryListD2.Parameters[1].Value := '2AJ';
  qryListD2.Parameters[2].Value := UI_APPSPC_NEW_frm.SEQ;
  qryListD2.Open;

  sDBGrid4.Fields[1].EditMask := '9999-99-99;0;';
  BtnControl(5);


end;

function TUI_APPSPC_NEW_DOC2_frm.ErrorCheck:String;
var
  ErrMsg:TStringList;
begin
  ErrMsg := TStringList.Create;
  Try
    IF Trim(edt_DOC_NO.Text) = '' THEN
      ErrMsg.Add('[세금계산서] 문서번호를 입력하세요');
    IF Trim(msk_ISS_DATE.Text) = '' THEN
      ErrMsg.Add('[세금계산서] 작성일자를 입력하세요');

    IF Trim(edt_SE_SNAME.Text) = '' THEN
      ErrMsg.Add('[공급자]상호를 입력하세요');
    IF Trim(edt_SE_NAME.Text) = '' THEN
      ErrMsg.Add('[공급자]대표자명을 입력하세요');
    IF Trim(msk_SE_SAUPNO.Text) = '' THEN
      ErrMsg.Add('[공급자]사업자등록번호를 입력하세요');
    IF Trim(edt_SE_ADDR1.Text) = '' THEN
      ErrMsg.Add('[공급자]주소를 입력하세요');

    IF Trim(edt_BY_SNAME.Text) = '' THEN
      ErrMsg.Add('[공급받는자] 상호를 입력하세요');
    IF Trim(edt_BY_NAME.Text) = '' THEN
      ErrMsg.Add('[공급받는자] 대표자명을 입력하세요');
    IF Trim(msk_BY_SAUPNO.Text) = '' THEN
      ErrMsg.Add('[공급받는자] 사업자등록번호를 입력하세요');
    IF Trim(edt_BY_ADDR1.Text) = '' THEN
      ErrMsg.Add('[공급받는자]주소를 입력하세요');

    IF edt_ISS_EXPAMT.Value = 0 THEN   //원화고정
      ErrMsg.Add('[세금계산서] 공급가액을 입력하세요.');
    if edt_TOTCNT.Value = 0 then
      ErrMsg.Add('[세금계산서] 총수량을 입력하세요.');
    if Trim(edt_TOTCNTC.Text) = '' then
      ErrMsg.Add('[세금계산서] 총수량 단위를 입력하세요.');

    Result := ErrMsg.Text;

  finally

    ErrMsg.Free;

  end;
end;

procedure TUI_APPSPC_NEW_DOC2_frm.edt_VB_GUBUNDblClick(Sender: TObject);
begin
  inherited;
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
       IF Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
       begin
         //선택된 코드 출력
           (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
       end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;

end;

end.
