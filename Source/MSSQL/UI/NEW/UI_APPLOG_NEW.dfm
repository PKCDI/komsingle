inherited UI_APPLOG_NEW_FRM: TUI_APPLOG_NEW_FRM
  Left = 628
  Top = 266
  BorderWidth = 4
  Caption = '[APPLOG] '#49688#51077#54868#47932#49440#52712#48372#51613'('#51064#46020#49849#46973')'#49888#52397#49436
  ClientHeight = 673
  ClientWidth = 1114
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  Scaled = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 72
    Align = alTop
    TabOrder = 0
    object sSpeedButton4: TsSpeedButton
      Left = 795
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton5: TsSpeedButton
      Left = 869
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 21
      Top = 13
      Width = 267
      Height = 23
      Caption = #49688#51077#54868#47932#49440#52712#48372#51613'('#51064#46020#49849#46973')'#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 117
      Top = 36
      Width = 75
      Height = 21
      Caption = '(APPLOG)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton6: TsSpeedButton
      Left = 511
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton7: TsSpeedButton
      Left = 588
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton8: TsSpeedButton
      Left = 304
      Top = 4
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
    end
    object sSpeedButton2: TsSpeedButton
      Left = 1038
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 1040
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnNew: TsButton
      Left = 313
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 25
      ContentMargin = 8
    end
    object btnEdit: TsButton
      Tag = 1
      Left = 379
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      TabStop = False
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 3
      ContentMargin = 8
    end
    object btnDel: TsButton
      Tag = 2
      Left = 445
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 803
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 4
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object btnTemp: TsButton
      Left = 597
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 5
      TabStop = False
      OnClick = btnSaveClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 11
      ContentMargin = 8
    end
    object btnSave: TsButton
      Tag = 1
      Left = 663
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 6
      TabStop = False
      OnClick = btnSaveClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 0
      ContentMargin = 8
    end
    object btnCancel: TsButton
      Tag = 2
      Left = 729
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 7
      TabStop = False
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 17
      ContentMargin = 8
    end
    object btnReady: TsButton
      Left = 878
      Top = 3
      Width = 93
      Height = 35
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 8
      TabStop = False
      OnClick = btnReadyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 28
      ContentMargin = 8
    end
    object btnSend: TsButton
      Left = 972
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 9
      TabStop = False
      OnClick = btnSendClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 21
      ContentMargin = 8
    end
    object btnCopy: TsButton
      Tag = 2
      Left = 520
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 10
      TabStop = False
      OnClick = btnCopyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 27
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 312
      Top = 41
      Width = 800
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      TabOrder = 11
      object sSpeedButton1: TsSpeedButton
        Left = 552
        Top = 45
        Width = 11
        Height = 46
        Visible = False
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 3
        Width = 197
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 328
        Top = 3
        Width = 77
        Height = 23
        AutoSize = False
        MaxLength = 10
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 656
        Top = 3
        Width = 39
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 5
        TabOrder = 2
        TabStop = False
        Text = '9: Original'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 752
        Top = 3
        Width = 47
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 3
        TabStop = False
        Text = 'AB: Message Acknowledgement'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 460
        Top = 3
        Width = 25
        Height = 23
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 374
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 372
      Height = 543
      TabStop = False
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = DrawColumnCell
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 204
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 94
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #51652#54665
          Width = 36
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 372
      Height = 56
      Align = alTop
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 258
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 77
        Top = 29
        Width = 171
        Height = 23
        TabOrder = 2
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 77
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        MaxLength = 10
        TabOrder = 0
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 170
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        MaxLength = 10
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 277
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
        OnClick = sBitBtn1Click
      end
    end
    object sPanel29: TsPanel
      Left = 1
      Top = 57
      Width = 372
      Height = 543
      Align = alClient
      TabOrder = 2
      Visible = False
      object sLabel1: TsLabel
        Left = 60
        Top = 257
        Width = 252
        Height = 21
        Caption = #49688#51077#54868#47932#49440#52712#48372#51613'('#51064#46020#49849#46973')'#49888#52397#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sLabel2: TsLabel
        Left = 60
        Top = 281
        Width = 124
        Height = 15
        Caption = #49888#44508#47928#49436' '#51089#49457#51473#51077#45768#45796
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 374
    Top = 72
    Width = 740
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 1
      Width = 738
      Height = 599
      ActivePage = sTabSheet1
      Align = alClient
      TabHeight = 26
      TabOrder = 1
      TabPadding = 15
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 730
          Height = 563
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          TabOrder = 0
          object Shape1: TShape
            Left = 4
            Top = 324
            Width = 721
            Height = 1
            Brush.Color = clGray
            Pen.Color = clGray
          end
          object edt_MESSAGE1: TsEdit
            Tag = 100
            Left = 122
            Top = 37
            Width = 32
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 1
            Text = '2AZ'
            OnDblClick = edt_MESSAGE1DblClick
            OnExit = edt_MESSAGE1Exit
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49888#52397#49436#51333#47448
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_Message1NM: TsEdit
            Left = 155
            Top = 37
            Width = 398
            Height = 23
            Cursor = crNo
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 30
            Text = #49688#51077#47932#54408#49440#52712#48372#51613'('#51064#46020#49849#46973') '#48143' '#49688#51077#47932#54408#45824#46020#49888#52397#49436' : '#54637#44277#54868#47932'('#44208#51228#51204')'
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_LC_G: TsEdit
            Tag = 101
            Left = 122
            Top = 61
            Width = 32
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 2
            Text = 'CT'
            OnDblClick = edt_MESSAGE1DblClick
            OnExit = edt_MESSAGE1Exit
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49888#50857#51109'/'#44228#50557#49436
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object sEdit2: TsEdit
            Left = 155
            Top = 61
            Width = 120
            Height = 23
            Cursor = crNo
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 31
            Text = #44228#50557#49436#48264#54840
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_LC_NO: TsEdit
            Left = 276
            Top = 61
            Width = 277
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 3
            SkinData.CustomColor = True
          end
          object edt_BL_G: TsEdit
            Tag = 102
            Left = 122
            Top = 85
            Width = 32
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 4
            Text = 'AWB'
            OnDblClick = edt_MESSAGE1DblClick
            OnExit = edt_MESSAGE1Exit
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#54616#51613#44428'/'#50868#49569#51109
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object sEdit5: TsEdit
            Left = 155
            Top = 85
            Width = 120
            Height = 23
            Cursor = crNo
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 32
            Text = #54637#44277#54868#47932' '#50868#49569#51109#48264#54840
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BL_NO: TsEdit
            Left = 276
            Top = 85
            Width = 277
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 5
            SkinData.CustomColor = True
          end
          object msk_APP_DATE: TsMaskEdit
            Left = 122
            Top = 13
            Width = 79
            Height = 23
            AutoSize = False
            MaxLength = 10
            TabOrder = 0
            Color = clWhite
            BoundLabel.Active = True
            BoundLabel.Caption = #49888#52397#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object msk_BL_DATE: TsMaskEdit
            Left = 122
            Top = 109
            Width = 79
            Height = 23
            AutoSize = False
            MaxLength = 10
            TabOrder = 6
            Color = clWhite
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#54616#51613#44428' '#48156#44553#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object edt_CARRIER2: TsEdit
            Left = 466
            Top = 137
            Width = 143
            Height = 23
            Color = clWhite
            MaxLength = 17
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #54637#52264'/'#54200#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_CARRIER1: TsEdit
            Left = 122
            Top = 137
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#48149#47749'/'#44592#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_CR_NAME1: TsEdit
            Left = 122
            Top = 165
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#48149#54924#49324#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_CR_NAME2: TsEdit
            Left = 122
            Top = 189
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 10
            SkinData.CustomColor = True
          end
          object edt_CR_NAME3: TsEdit
            Left = 122
            Top = 213
            Width = 192
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Caption = #49440#49324' '#49464#44288#49885#48324#48512#54840
          end
          object edt_B5_NAME1: TsEdit
            Left = 122
            Top = 245
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51064#49688#50696#51221#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_B5_NAME2: TsEdit
            Left = 122
            Top = 269
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 16
            SkinData.CustomColor = True
          end
          object edt_B5_NAME3: TsEdit
            Left = 122
            Top = 293
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.Caption = 'sEdit11'
          end
          object edt_SE_NAME1: TsEdit
            Left = 466
            Top = 165
            Width = 250
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49569#54616#51064
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_SE_NAME2: TsEdit
            Left = 466
            Top = 189
            Width = 250
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 13
            SkinData.CustomColor = True
          end
          object edt_SE_NAME3: TsEdit
            Left = 466
            Top = 213
            Width = 250
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.Caption = 'sEdit11'
          end
          object edt_CN_NAME1: TsEdit
            Left = 466
            Top = 245
            Width = 250
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#54616#51064
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_CN_NAME2: TsEdit
            Left = 466
            Top = 269
            Width = 250
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 19
            SkinData.CustomColor = True
          end
          object edt_CN_NAME3: TsEdit
            Left = 466
            Top = 293
            Width = 250
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 20
            SkinData.CustomColor = True
            BoundLabel.Caption = 'sEdit11'
          end
          object edt_LOAD_LOC: TsEdit
            Tag = 103
            Left = 122
            Top = 333
            Width = 32
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 21
            Text = 'CH'
            OnDblClick = edt_MESSAGE1DblClick
            OnExit = edt_MESSAGE1Exit
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#51201#54637
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_LOAD_LOC_NM: TsEdit
            Left = 155
            Top = 333
            Width = 142
            Height = 23
            Cursor = crNo
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 33
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_LOAD_TXT: TsEdit
            Left = 122
            Top = 357
            Width = 343
            Height = 23
            Color = clWhite
            TabOrder = 22
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51648#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ARR_LOC: TsEdit
            Tag = 104
            Left = 122
            Top = 389
            Width = 32
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 23
            Text = 'CH'
            OnDblClick = edt_MESSAGE1DblClick
            OnExit = edt_MESSAGE1Exit
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #46020#52265#54637'/'#51648#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ARR_LOC_NM: TsEdit
            Left = 155
            Top = 389
            Width = 142
            Height = 23
            Cursor = crNo
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 34
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_ARR_TXT: TsEdit
            Left = 122
            Top = 413
            Width = 343
            Height = 23
            Color = clWhite
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51648#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object msk_AR_DATE: TsMaskEdit
            Left = 122
            Top = 437
            Width = 79
            Height = 23
            AutoSize = False
            MaxLength = 10
            TabOrder = 25
            Color = clWhite
            BoundLabel.Active = True
            BoundLabel.Caption = #46020#52265#50696#51221#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object edt_INV_AMTC: TsEdit
            Tag = 105
            Left = 122
            Top = 469
            Width = 32
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 26
            OnDblClick = edt_MESSAGE1DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49345#50629#49569#51109#44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object curr_INV_AMT: TsCurrencyEdit
            Left = 155
            Top = 469
            Width = 150
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 27
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_PAC_QTYC: TsEdit
            Tag = 106
            Left = 122
            Top = 493
            Width = 32
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 3
            TabOrder = 28
            OnDblClick = edt_MESSAGE1DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #54252#51109#49688
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_PAC_QTY: TsCurrencyEdit
            Left = 155
            Top = 493
            Width = 150
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 29
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object sButton1: TsButton
            Left = 315
            Top = 213
            Width = 86
            Height = 23
            Cursor = crHandPoint
            Caption = #49885#48324#48512#54840
            TabOrder = 35
            OnClick = sButton1Click
            Images = DMICON.System16
            ImageIndex = 0
          end
        end
      end
      object sTabSheet3: TsTabSheet
        Caption = #54868#47932#48264#54840'/'#49345#54408#47749#49464
        object sPanel27: TsPanel
          Left = 0
          Top = 0
          Width = 730
          Height = 563
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          TabOrder = 0
          object memo_SPMARK: TsMemo
            Left = 122
            Top = 13
            Width = 591
            Height = 170
            ScrollBars = ssVertical
            TabOrder = 0
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #54868#47932#54364#49884' '#48143' '#48264#54840
            BoundLabel.Layout = sclLeftTop
          end
          object memo_GOODS: TsMemo
            Left = 122
            Top = 184
            Width = 591
            Height = 170
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 1
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #49345#54408#47749#49464
            BoundLabel.Layout = sclLeftTop
          end
          object edt_TRM_PAYC: TsEdit
            Tag = 107
            Left = 122
            Top = 365
            Width = 32
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 2
            Text = 'CT'
            OnDblClick = edt_MESSAGE1DblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44208#51228#44396#48516' '#48143' '#44592#44036
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sEdit30: TsEdit
            Left = 155
            Top = 365
            Width = 206
            Height = 23
            Cursor = crNo
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 3
            Text = #44228#50557#49436#48264#54840
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_TRM_PAY: TsEdit
            Left = 122
            Top = 389
            Width = 239
            Height = 23
            Color = clWhite
            TabOrder = 4
            SkinData.CustomColor = True
          end
          object edt_BANK_CD: TsEdit
            Tag = 108
            Left = 122
            Top = 421
            Width = 41
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 5
            Text = 'CCCC'
            OnDblClick = edt_BANK_CDDblClick
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #48156#44553#51008#54665'*'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BANK_TXT: TsEdit
            Left = 122
            Top = 445
            Width = 239
            Height = 23
            Color = clWhite
            TabOrder = 6
            SkinData.CustomColor = True
          end
          object edt_BANK_BR: TsEdit
            Left = 122
            Top = 469
            Width = 239
            Height = 23
            Color = clWhite
            TabOrder = 7
            SkinData.CustomColor = True
          end
          object edt_MS_NAME1: TsEdit
            Left = 434
            Top = 365
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49888#52397#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_MS_NAME2: TsEdit
            Left = 434
            Top = 389
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 9
            SkinData.CustomColor = True
          end
          object edt_MS_NAME3: TsEdit
            Left = 434
            Top = 413
            Width = 103
            Height = 23
            Color = clWhite
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#51088#49436#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_AX_NAME1: TsEdit
            Left = 434
            Top = 445
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #47749#51032#51064
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object edt_AX_NAME2: TsEdit
            Left = 434
            Top = 469
            Width = 279
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 12
            SkinData.CustomColor = True
          end
          object edt_AX_NAME3: TsEdit
            Left = 434
            Top = 493
            Width = 103
            Height = 23
            Color = clWhite
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#51088#49436#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 72
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20181231'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT'#9'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, MESSAGE3, C' +
        'R_NAME1, CR_NAME2, CR_NAME3, SE_NAME1, SE_NAME2, SE_NAME3, MS_NA' +
        'ME1, MS_NAME2, MS_NAME3, AX_NAME1, AX_NAME2, AX_NAME3, INV_AMT, ' +
        'INV_AMTC, LC_G, LC_NO, BL_G, BL_NO, CARRIER1, CARRIER2, AR_DATE,' +
        ' BL_DATE, APP_DATE, LOAD_LOC, LOAD_TXT, ARR_LOC, ARR_TXT, SPMARK' +
        '1, SPMARK2, SPMARK3, SPMARK4, SPMARK5, SPMARK6, SPMARK7, SPMARK8' +
        ', SPMARK9, SPMARK10, PAC_QTY, PAC_QTYC, GOODS1, GOODS2, GOODS3, ' +
        'GOODS4, GOODS5, GOODS6, GOODS7, GOODS8, GOODS9, GOODS10, TRM_PAY' +
        'C, TRM_PAY, BANK_CD, BANK_TXT, BANK_BR, CHK1, CHK2, CHK3, PRNO, ' +
        'CN_NAME1, CN_NAME2, CN_NAME3, B5_NAME1, B5_NAME2, B5_NAME3'
      #9',msg1CODE.MSG1NAME'
      #9',LCGCODE.LCGNAME'
      #9',BLGCODE.BLGNAME'
      #9',LOADCODE.LOADNAME'
      #9',ARRCODE.ARRNAME'
      #9',TRMPAYCCODE.TRMPAYCNAME'
      #9',SUNSACODE.CRNAEM3NAME'
      'FROM APPLOG'
      
        'LEFT JOIN (SELECT CODE,NAME as MSG1NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'APPLOG'#44396#48516#39') msg1CODE ON MESSAGE1 = msg1CODE.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as LCGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39'LC'#44396#48516#39') LCGCODE ON LC_G = LCGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as BLGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#49440#54616#51613#44428#39') BLGCODE ON BL_G = BLGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as LOADNAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') LOADCODE ON LOAD_LOC = LOADCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as ARRNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#44397#44032#39') ARRCODE ON ARR_LOC = ARRCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as TRMPAYCNAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39#44208#51228#44592#44036#39') TRMPAYCCODE ON TRM_PAYC = TRMPAYCCO' +
        'DE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as CRNAEM3NAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39'SUNSA'#39') SUNSACODE ON CR_NAME3 = SUNSACODE.' +
        'CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 8
    Top = 176
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListMESSAGE3: TStringField
      FieldName = 'MESSAGE3'
      Size = 3
    end
    object qryListCR_NAME1: TStringField
      FieldName = 'CR_NAME1'
      Size = 35
    end
    object qryListCR_NAME2: TStringField
      FieldName = 'CR_NAME2'
      Size = 35
    end
    object qryListCR_NAME3: TStringField
      FieldName = 'CR_NAME3'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListMS_NAME1: TStringField
      FieldName = 'MS_NAME1'
      Size = 35
    end
    object qryListMS_NAME2: TStringField
      FieldName = 'MS_NAME2'
      Size = 35
    end
    object qryListMS_NAME3: TStringField
      FieldName = 'MS_NAME3'
      Size = 35
    end
    object qryListAX_NAME1: TStringField
      FieldName = 'AX_NAME1'
      Size = 35
    end
    object qryListAX_NAME2: TStringField
      FieldName = 'AX_NAME2'
      Size = 35
    end
    object qryListAX_NAME3: TStringField
      FieldName = 'AX_NAME3'
      Size = 35
    end
    object qryListINV_AMT: TBCDField
      FieldName = 'INV_AMT'
      DisplayFormat = '#,###.####;0'
      Precision = 18
    end
    object qryListINV_AMTC: TStringField
      FieldName = 'INV_AMTC'
      Size = 3
    end
    object qryListLC_G: TStringField
      FieldName = 'LC_G'
      Size = 3
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListBL_G: TStringField
      FieldName = 'BL_G'
      Size = 3
    end
    object qryListBL_NO: TStringField
      FieldName = 'BL_NO'
      Size = 35
    end
    object qryListCARRIER1: TStringField
      FieldName = 'CARRIER1'
      Size = 17
    end
    object qryListCARRIER2: TStringField
      FieldName = 'CARRIER2'
      Size = 35
    end
    object qryListAR_DATE: TStringField
      FieldName = 'AR_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListBL_DATE: TStringField
      FieldName = 'BL_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListLOAD_LOC: TStringField
      FieldName = 'LOAD_LOC'
      Size = 3
    end
    object qryListLOAD_TXT: TStringField
      FieldName = 'LOAD_TXT'
      Size = 70
    end
    object qryListARR_LOC: TStringField
      FieldName = 'ARR_LOC'
      Size = 3
    end
    object qryListARR_TXT: TStringField
      FieldName = 'ARR_TXT'
      Size = 70
    end
    object qryListSPMARK1: TStringField
      FieldName = 'SPMARK1'
      Size = 35
    end
    object qryListSPMARK2: TStringField
      FieldName = 'SPMARK2'
      Size = 35
    end
    object qryListSPMARK3: TStringField
      FieldName = 'SPMARK3'
      Size = 35
    end
    object qryListSPMARK4: TStringField
      FieldName = 'SPMARK4'
      Size = 35
    end
    object qryListSPMARK5: TStringField
      FieldName = 'SPMARK5'
      Size = 35
    end
    object qryListSPMARK6: TStringField
      FieldName = 'SPMARK6'
      Size = 35
    end
    object qryListSPMARK7: TStringField
      FieldName = 'SPMARK7'
      Size = 35
    end
    object qryListSPMARK8: TStringField
      FieldName = 'SPMARK8'
      Size = 35
    end
    object qryListSPMARK9: TStringField
      FieldName = 'SPMARK9'
      Size = 35
    end
    object qryListSPMARK10: TStringField
      FieldName = 'SPMARK10'
      Size = 35
    end
    object qryListPAC_QTY: TBCDField
      FieldName = 'PAC_QTY'
      Precision = 18
    end
    object qryListPAC_QTYC: TStringField
      FieldName = 'PAC_QTYC'
      Size = 3
    end
    object qryListGOODS1: TStringField
      FieldName = 'GOODS1'
      Size = 70
    end
    object qryListGOODS2: TStringField
      FieldName = 'GOODS2'
      Size = 70
    end
    object qryListGOODS3: TStringField
      FieldName = 'GOODS3'
      Size = 70
    end
    object qryListGOODS4: TStringField
      FieldName = 'GOODS4'
      Size = 70
    end
    object qryListGOODS5: TStringField
      FieldName = 'GOODS5'
      Size = 70
    end
    object qryListGOODS6: TStringField
      FieldName = 'GOODS6'
      Size = 70
    end
    object qryListGOODS7: TStringField
      FieldName = 'GOODS7'
      Size = 70
    end
    object qryListGOODS8: TStringField
      FieldName = 'GOODS8'
      Size = 70
    end
    object qryListGOODS9: TStringField
      FieldName = 'GOODS9'
      Size = 70
    end
    object qryListGOODS10: TStringField
      FieldName = 'GOODS10'
      Size = 70
    end
    object qryListTRM_PAYC: TStringField
      FieldName = 'TRM_PAYC'
      Size = 3
    end
    object qryListTRM_PAY: TStringField
      FieldName = 'TRM_PAY'
      Size = 35
    end
    object qryListBANK_CD: TStringField
      FieldName = 'BANK_CD'
      Size = 4
    end
    object qryListBANK_TXT: TStringField
      FieldName = 'BANK_TXT'
      Size = 70
    end
    object qryListBANK_BR: TStringField
      FieldName = 'BANK_BR'
      Size = 70
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = CHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListCN_NAME1: TStringField
      FieldName = 'CN_NAME1'
      Size = 35
    end
    object qryListCN_NAME2: TStringField
      FieldName = 'CN_NAME2'
      Size = 35
    end
    object qryListCN_NAME3: TStringField
      FieldName = 'CN_NAME3'
      Size = 35
    end
    object qryListB5_NAME1: TStringField
      FieldName = 'B5_NAME1'
      Size = 35
    end
    object qryListB5_NAME2: TStringField
      FieldName = 'B5_NAME2'
      Size = 35
    end
    object qryListB5_NAME3: TStringField
      FieldName = 'B5_NAME3'
      Size = 35
    end
    object qryListMSG1NAME: TStringField
      FieldName = 'MSG1NAME'
      Size = 100
    end
    object qryListLCGNAME: TStringField
      FieldName = 'LCGNAME'
      Size = 100
    end
    object qryListBLGNAME: TStringField
      FieldName = 'BLGNAME'
      Size = 100
    end
    object qryListLOADNAME: TStringField
      FieldName = 'LOADNAME'
      Size = 100
    end
    object qryListARRNAME: TStringField
      FieldName = 'ARRNAME'
      Size = 100
    end
    object qryListTRMPAYCNAME: TStringField
      FieldName = 'TRMPAYCNAME'
      Size = 100
    end
    object qryListCRNAEM3NAME: TStringField
      FieldName = 'CRNAEM3NAME'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 176
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APPLOG'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 96
    Top = 208
  end
end
