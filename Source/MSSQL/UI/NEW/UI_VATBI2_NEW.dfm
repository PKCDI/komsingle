inherited UI_VATBI2_NEW_frm: TUI_VATBI2_NEW_frm
  Left = 546
  Top = 166
  BorderWidth = 4
  Caption = '[VATBIL] '#49464#44552#44228#49328#49436
  ClientHeight = 660
  ClientWidth = 1114
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 41
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      1114
      41)
    object sSpeedButton4: TsSpeedButton
      Left = 105
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 6
      Width = 65
      Height = 17
      Caption = #49464#44552#44228#49328#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 21
      Width = 37
      Height = 13
      Caption = 'VATBIL'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton7: TsSpeedButton
      Left = 1054
      Top = 27
      Width = 8
      Height = 35
      Visible = False
      ButtonStyle = tbsDivider
    end
    object sSpeedButton8: TsSpeedButton
      Left = 160
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 1034
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 109
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 175
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 41
    Width = 341
    Height = 619
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 339
      Height = 561
      TabStop = False
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = DrawColumnCell
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 211
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 94
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 339
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 242
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 61
        Top = 29
        Width = 171
        Height = 23
        TabOrder = 2
        OnKeyUp = edt_SearchNoKeyUp
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 61
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 0
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 154
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 261
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 341
    Top = 41
    Width = 773
    Height = 619
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 56
      Width = 771
      Height = 562
      ActivePage = sTabSheet4
      Align = alClient
      TabHeight = 26
      TabOrder = 1
      OnChange = sPageControl1Change
      TabPadding = 10
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 526
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object Shape1: TShape
            Left = 5
            Top = 39
            Width = 754
            Height = 1
            Brush.Color = clBtnFace
            Pen.Color = clBtnFace
          end
          object sLabel1: TsLabel
            Left = 365
            Top = 52
            Width = 12
            Height = 15
            AutoSize = False
            Caption = #44428
            Enabled = False
          end
          object sLabel2: TsLabel
            Left = 365
            Top = 76
            Width = 12
            Height = 15
            AutoSize = False
            Caption = #54840
            Enabled = False
          end
          object Shape2: TShape
            Left = 5
            Top = 127
            Width = 754
            Height = 1
            Brush.Color = clBtnFace
            Enabled = False
            Pen.Color = clBtnFace
          end
          object edt_AdInfo5: TsEdit
            Left = 669
            Top = 591
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 3
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AdInfo3: TsEdit
            Left = 669
            Top = 547
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 1
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AdInfo4: TsEdit
            Left = 669
            Top = 569
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 2
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel59: TsPanel
            Left = 1
            Top = 548
            Width = 105
            Height = 23
            SkinData.CustomColor = True
            Caption = #50868#49569#49688#45800
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            Visible = False
          end
          object com_Carriage: TsComboBox
            Left = 107
            Top = 548
            Width = 237
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 1
            TabOrder = 0
            Text = 'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
            Visible = False
            Items.Strings = (
              ''
              'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
              'DT: '#54644#49345#50868#49569'(Sea)/'#54637#44277#50868#49569'(Air)')
          end
          object sPanel89: TsPanel
            Left = 1
            Top = 572
            Width = 343
            Height = 23
            SkinData.CustomColor = True
            Caption = '[PAGE2] 44A/44B'#47484' '#51089#49457#54616#49464#50836
            Color = 16576211
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            Visible = False
          end
          object sDBEdit1: TsDBEdit
            Left = 104
            Top = 8
            Width = 41
            Height = 23
            Color = clWhite
            DataField = 'VAT_CODE'
            DataSource = dsList
            Enabled = False
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49464#44552#44228#49328#49436#53076#46300
          end
          object sDBEdit2: TsDBEdit
            Left = 146
            Top = 8
            Width = 103
            Height = 23
            Color = clBtnFace
            DataField = 'VAT_CODE_NM'
            DataSource = dsList
            Enabled = False
            TabOrder = 7
            SkinData.CustomColor = True
          end
          object sDBEdit3: TsDBEdit
            Left = 320
            Top = 8
            Width = 41
            Height = 23
            Color = clWhite
            DataField = 'VAT_TYPE'
            DataSource = dsList
            Enabled = False
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44228#49328#49436#51333#47448
          end
          object sDBEdit4: TsDBEdit
            Left = 362
            Top = 8
            Width = 135
            Height = 23
            Color = clBtnFace
            DataField = 'VAT_TYPE_NM'
            DataSource = dsList
            Enabled = False
            TabOrder = 9
            SkinData.CustomColor = True
          end
          object sDBEdit5: TsDBEdit
            Left = 560
            Top = 8
            Width = 41
            Height = 23
            Color = clWhite
            DataField = 'NEW_INDICATOR'
            DataSource = dsList
            Enabled = False
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#51221#49324#50976
          end
          object sDBEdit6: TsDBEdit
            Left = 602
            Top = 8
            Width = 142
            Height = 23
            Color = clBtnFace
            DataField = 'NEW_INDICATOR_NM'
            DataSource = dsList
            Enabled = False
            TabOrder = 11
            SkinData.CustomColor = True
          end
          object sDBEdit7: TsDBEdit
            Left = 104
            Top = 48
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'RE_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #52293#48264#54840
          end
          object sDBEdit8: TsDBEdit
            Left = 104
            Top = 72
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'SE_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 13
            SkinData.CustomColor = True
          end
          object sDBEdit9: TsDBEdit
            Left = 104
            Top = 96
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'FS_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51068#47144#48264#54840
          end
          object sDBEdit10: TsDBEdit
            Left = 480
            Top = 48
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'ACE_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47144#52280#51312#48264#54840
          end
          object sDBEdit11: TsDBEdit
            Left = 480
            Top = 72
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'RFF_NO'
            DataSource = dsList
            Enabled = False
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49464#44552#44228#49328#49436#48264#54840
          end
          object sDBEdit12: TsDBEdit
            Left = 104
            Top = 136
            Width = 97
            Height = 23
            Color = clWhite
            DataField = 'SE_SAUP'
            DataSource = dsList
            Enabled = False
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#44553#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object sDBEdit13: TsDBEdit
            Left = 104
            Top = 160
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'SE_NAME1'
            DataSource = dsList
            Enabled = False
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49345#54840
          end
          object sDBEdit14: TsDBEdit
            Left = 104
            Top = 184
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'SE_NAME2'
            DataSource = dsList
            Enabled = False
            TabOrder = 19
            SkinData.CustomColor = True
          end
          object sDBEdit15: TsDBEdit
            Left = 267
            Top = 136
            Width = 95
            Height = 23
            Color = clWhite
            DataField = 'SE_NAME3'
            DataSource = dsList
            Enabled = False
            TabOrder = 20
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#54364#51088
          end
          object sDBEdit16: TsDBEdit
            Left = 104
            Top = 208
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'SE_ADDR1'
            DataSource = dsList
            Enabled = False
            TabOrder = 21
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51452#49548
          end
          object sDBEdit17: TsDBEdit
            Left = 104
            Top = 232
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'SE_ADDR2'
            DataSource = dsList
            Enabled = False
            TabOrder = 22
            SkinData.CustomColor = True
          end
          object sDBEdit18: TsDBEdit
            Left = 104
            Top = 256
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'SE_ADDR3'
            DataSource = dsList
            Enabled = False
            TabOrder = 23
            SkinData.CustomColor = True
          end
          object sDBEdit19: TsDBEdit
            Left = 104
            Top = 280
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'SE_ADDR4'
            DataSource = dsList
            Enabled = False
            TabOrder = 24
            SkinData.CustomColor = True
          end
          object sDBEdit20: TsDBEdit
            Left = 104
            Top = 304
            Width = 258
            Height = 23
            Color = clWhite
            DataField = 'SE_ADDR5'
            DataSource = dsList
            Enabled = False
            TabOrder = 25
            SkinData.CustomColor = True
          end
          object sDBEdit21: TsDBEdit
            Left = 104
            Top = 328
            Width = 73
            Height = 23
            Color = clWhite
            DataField = 'SE_SAUP1'
            DataSource = dsList
            Enabled = False
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51333#49324#50629#51109#48264#54840
          end
          object sMemo1: TsMemo
            Left = 104
            Top = 352
            Width = 258
            Height = 65
            Ctl3D = True
            Enabled = False
            Lines.Strings = (
              #45812#45817#48512#49436
              #45812#45817#51088
              #51204#54868#48264#54840
              #51060#47700#51068)
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 27
            WordWrap = False
            Text = #45812#45817#48512#49436#13#10#45812#45817#51088#13#10#51204#54868#48264#54840#13#10#51060#47700#51068#13#10
            BoundLabel.Active = True
            BoundLabel.Caption = #45812#45817#51088
            BoundLabel.Layout = sclLeftTop
          end
          object sDBMemo1: TsDBMemo
            Left = 104
            Top = 418
            Width = 258
            Height = 50
            Color = clWhite
            DataField = 'SE_UPTA1'
            DataSource = dsList
            Enabled = False
            TabOrder = 28
            BoundLabel.Active = True
            BoundLabel.Caption = #50629#53468
            BoundLabel.Layout = sclLeftTop
          end
          object sDBMemo2: TsDBMemo
            Left = 104
            Top = 469
            Width = 258
            Height = 50
            Color = clWhite
            DataField = 'SE_ITEM1'
            DataSource = dsList
            Enabled = False
            TabOrder = 29
            BoundLabel.Active = True
            BoundLabel.Caption = #51333#47785
            BoundLabel.Layout = sclLeftTop
          end
          object sDBEdit22: TsDBEdit
            Left = 480
            Top = 136
            Width = 95
            Height = 23
            Color = clWhite
            DataField = 'BY_SAUP'
            DataSource = dsList
            Enabled = False
            TabOrder = 30
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#44553#51088#48155#45716#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object sDBEdit23: TsDBEdit
            Left = 480
            Top = 160
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'BY_NAME1'
            DataSource = dsList
            Enabled = False
            TabOrder = 31
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49345#54840
          end
          object sDBEdit24: TsDBEdit
            Left = 480
            Top = 184
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'BY_NAME2'
            DataSource = dsList
            Enabled = False
            TabOrder = 32
            SkinData.CustomColor = True
          end
          object sDBEdit25: TsDBEdit
            Left = 643
            Top = 136
            Width = 93
            Height = 23
            Color = clWhite
            DataField = 'BY_NAME3'
            DataSource = dsList
            Enabled = False
            TabOrder = 33
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#54364#51088
          end
          object sDBEdit26: TsDBEdit
            Left = 480
            Top = 208
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'BY_ADDR1'
            DataSource = dsList
            Enabled = False
            TabOrder = 34
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51452#49548
          end
          object sDBEdit27: TsDBEdit
            Left = 480
            Top = 232
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'BY_ADDR2'
            DataSource = dsList
            Enabled = False
            TabOrder = 35
            SkinData.CustomColor = True
          end
          object sDBEdit28: TsDBEdit
            Left = 480
            Top = 256
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'BY_ADDR3'
            DataSource = dsList
            Enabled = False
            TabOrder = 36
            SkinData.CustomColor = True
          end
          object sDBEdit29: TsDBEdit
            Left = 480
            Top = 280
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'BY_ADDR4'
            DataSource = dsList
            Enabled = False
            TabOrder = 37
            SkinData.CustomColor = True
          end
          object sDBEdit30: TsDBEdit
            Left = 480
            Top = 304
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'BY_ADDR5'
            DataSource = dsList
            Enabled = False
            TabOrder = 38
            SkinData.CustomColor = True
          end
          object sDBEdit31: TsDBEdit
            Left = 480
            Top = 328
            Width = 71
            Height = 23
            Color = clWhite
            DataField = 'BY_SAUP1'
            DataSource = dsList
            Enabled = False
            TabOrder = 39
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51333#49324#50629#51109#48264#54840
          end
          object sMemo2: TsMemo
            Left = 480
            Top = 352
            Width = 256
            Height = 65
            Ctl3D = True
            Enabled = False
            Lines.Strings = (
              #45812#45817#48512#49436
              #45812#45817#51088
              #51204#54868#48264#54840
              #51060#47700#51068)
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 40
            WordWrap = False
            Text = #45812#45817#48512#49436#13#10#45812#45817#51088#13#10#51204#54868#48264#54840#13#10#51060#47700#51068#13#10
            BoundLabel.Active = True
            BoundLabel.Caption = #45812#45817#51088
            BoundLabel.Layout = sclLeftTop
          end
          object sDBMemo3: TsDBMemo
            Left = 480
            Top = 418
            Width = 256
            Height = 50
            Color = clWhite
            DataField = 'BY_UPTA1'
            DataSource = dsList
            Enabled = False
            TabOrder = 41
            BoundLabel.Active = True
            BoundLabel.Caption = #50629#53468
            BoundLabel.Layout = sclLeftTop
          end
          object sDBMemo4: TsDBMemo
            Left = 480
            Top = 469
            Width = 256
            Height = 50
            Color = clWhite
            DataField = 'BY_ITEM1'
            DataSource = dsList
            Enabled = False
            TabOrder = 42
            BoundLabel.Active = True
            BoundLabel.Caption = #51333#47785
            BoundLabel.Layout = sclLeftTop
          end
        end
      end
      object sTabSheet2: TsTabSheet
        Caption = #49345#54408#45236#50669
        object sPanel1: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 526
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object sDBGrid4: TsDBGrid
            Left = 1
            Top = 1
            Width = 761
            Height = 223
            Align = alTop
            Color = clWhite
            Ctl3D = True
            DataSource = dsGoods
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            SkinData.CustomFont = True
            Columns = <
              item
                Alignment = taCenter
                Color = clBtnFace
                Expanded = False
                FieldName = 'SEQ'
                Title.Alignment = taCenter
                Title.Caption = #49692#48264
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NAME1'
                Title.Alignment = taCenter
                Title.Caption = #54408#47749
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIZE1'
                Title.Alignment = taCenter
                Title.Caption = #44508#44201
                Width = 88
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'DE_DATE'
                Title.Alignment = taCenter
                Title.Caption = #44277#44553#51068#51088
                Width = 82
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RATE'
                Title.Alignment = taCenter
                Title.Caption = #54872#50984
                Width = 55
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTY'
                Title.Alignment = taCenter
                Title.Caption = #49688#47049
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'QTY_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#50948
                Width = 35
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRICE'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'PRICE_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#50948
                Width = 35
                Visible = True
              end>
          end
          object sPanel2: TsPanel
            Left = 1
            Top = 226
            Width = 761
            Height = 299
            Align = alClient
            
            TabOrder = 1
            DesignSize = (
              761
              299)
            object sSpeedButton11: TsSpeedButton
              Left = 439
              Top = 8
              Width = 11
              Height = 283
              Anchors = [akLeft, akTop, akBottom]
              ButtonStyle = tbsDivider
            end
            object Shape3: TShape
              Left = 450
              Top = 176
              Width = 305
              Height = 1
              Brush.Color = clBtnFace
              Enabled = False
              Pen.Color = clBtnFace
            end
            object sPanel23: TsPanel
              Left = 8
              Top = 167
              Width = 57
              Height = 23
              SkinData.CustomColor = True
              Caption = #44508#44201
              Color = 16765090
              
              TabOrder = 0
            end
            object sDBEdit32: TsDBEdit
              Tag = 501
              Left = 65
              Top = 35
              Width = 121
              Height = 23
              Hint = #54408#47749
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'NAME_COD'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 1
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sDBMemo5: TsDBMemo
              Left = 8
              Top = 59
              Width = 418
              Height = 107
              Color = clWhite
              Ctl3D = True
              DataField = 'NAME1'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 2
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeftTop
              BoundLabel.ParentFont = False
              CharCase = ecUpperCase
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sDBMemo6: TsDBMemo
              Left = 8
              Top = 189
              Width = 418
              Height = 106
              Color = clWhite
              Ctl3D = True
              DataField = 'SIZE1'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 3
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeftTop
              BoundLabel.ParentFont = False
              CharCase = ecUpperCase
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sPanel21: TsPanel
              Left = 8
              Top = 35
              Width = 57
              Height = 23
              SkinData.CustomColor = True
              Caption = #54408#47749
              Color = 16765090
              
              TabOrder = 4
            end
            object sPanel28: TsPanel
              Left = 8
              Top = 7
              Width = 57
              Height = 23
              SkinData.CustomColor = True
              Caption = #49692#48264
              Color = 16765090
              
              TabOrder = 5
            end
            object sDBEdit34: TsDBEdit
              Tag = 501
              Left = 65
              Top = 7
              Width = 41
              Height = 23
              Hint = #54408#47749
              TabStop = False
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'SEQ'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 6
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sDBEdit35: TsDBEdit
              Tag = 502
              Left = 561
              Top = 10
              Width = 36
              Height = 23
              Hint = #45800#50948
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'QTY_G'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 7
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #49688#47049
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object sDBEdit36: TsDBEdit
              Left = 598
              Top = 10
              Width = 142
              Height = 23
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'QTY'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              MaxLength = 10
              ParentCtl3D = False
              TabOrder = 8
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sDBEdit37: TsDBEdit
              Left = 598
              Top = 34
              Width = 142
              Height = 23
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'PRICE'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              MaxLength = 10
              ParentCtl3D = False
              TabOrder = 9
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sDBEdit38: TsDBEdit
              Tag = 503
              Left = 561
              Top = 34
              Width = 36
              Height = 23
              Hint = #53685#54868
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'PRICE_G'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 10
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #45800#44032
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object sDBEdit39: TsDBEdit
              Left = 561
              Top = 58
              Width = 179
              Height = 23
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'QTYG'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              MaxLength = 10
              ParentCtl3D = False
              TabOrder = 11
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #45800#44032#45800#50948#44592#51456
            end
            object sDBEdit40: TsDBEdit
              Tag = 502
              Left = 561
              Top = 90
              Width = 179
              Height = 23
              Hint = #45800#50948
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'SUPAMT'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 12
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #50896#54868#44277#44553#44032#50529
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object sDBEdit33: TsDBEdit
              Tag = 501
              Left = 195
              Top = 7
              Width = 86
              Height = 23
              Hint = #54408#47749
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'DE_DATE'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 13
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sPanel9: TsPanel
              Left = 130
              Top = 7
              Width = 65
              Height = 23
              SkinData.CustomColor = True
              Caption = #44277#44553#51068#51088
              Color = 16765090
              
              TabOrder = 14
            end
            object sPanel10: TsPanel
              Left = 282
              Top = 7
              Width = 63
              Height = 23
              SkinData.CustomColor = True
              Caption = #54872#50984
              Color = 16765090
              
              TabOrder = 15
            end
            object sDBEdit42: TsDBEdit
              Tag = 501
              Left = 345
              Top = 7
              Width = 83
              Height = 23
              Hint = #54408#47749
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'RATE'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 16
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sDBEdit43: TsDBEdit
              Left = 561
              Top = 114
              Width = 179
              Height = 23
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'TAXAMT'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              MaxLength = 10
              ParentCtl3D = False
              TabOrder = 17
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #49464#50529
            end
            object sDBEdit41: TsDBEdit
              Tag = 502
              Left = 561
              Top = 138
              Width = 36
              Height = 23
              Hint = #45800#50948
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'USAMT_G'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 18
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #50808#54868#44277#44553#44032#50529
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object sDBEdit44: TsDBEdit
              Left = 598
              Top = 138
              Width = 142
              Height = 23
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'USAMT'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              MaxLength = 10
              ParentCtl3D = False
              TabOrder = 19
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sDBEdit45: TsDBEdit
              Tag = 502
              Left = 561
              Top = 194
              Width = 179
              Height = 23
              Hint = #45800#50948
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'SUPSTAMT'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 20
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #50896#54868#44277#44553#44032#50529' '#49548#44228
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object sDBEdit46: TsDBEdit
              Left = 561
              Top = 218
              Width = 179
              Height = 23
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'TAXSTAMT'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              MaxLength = 10
              ParentCtl3D = False
              TabOrder = 21
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #49464#50529' '#49548#44228
            end
            object sDBEdit47: TsDBEdit
              Tag = 502
              Left = 561
              Top = 242
              Width = 36
              Height = 23
              Hint = #45800#50948
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'USSTAMT_G'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 22
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #50808#54868#44277#44553#44032#50529' '#49548#44228
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object sDBEdit48: TsDBEdit
              Left = 598
              Top = 242
              Width = 142
              Height = 23
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'USSTAMT'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              MaxLength = 10
              ParentCtl3D = False
              TabOrder = 23
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
            object sDBEdit49: TsDBEdit
              Tag = 502
              Left = 561
              Top = 266
              Width = 36
              Height = 23
              Hint = #45800#50948
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'STQTY_G'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              ParentCtl3D = False
              TabOrder = 24
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
              BoundLabel.Active = True
              BoundLabel.Caption = #49688#47049' '#49548#44228
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.ParentFont = False
            end
            object sDBEdit50: TsDBEdit
              Left = 598
              Top = 266
              Width = 142
              Height = 23
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              DataField = 'STQTY'
              DataSource = dsGoods
              Enabled = False
              ImeName = 'Microsoft IME 2010'
              MaxLength = 10
              ParentCtl3D = False
              TabOrder = 25
              SkinData.CustomColor = True
              SkinData.SkinSection = 'GROUPBOX'
            end
          end
          object sPanel8: TsPanel
            Left = 1
            Top = 224
            Width = 761
            Height = 2
            SkinData.SkinSection = 'TRANSPARENT'
            Align = alTop
            
            TabOrder = 2
          end
        end
      end
      object sTabSheet3: TsTabSheet
        Caption = #44552#50529
        object sPanel11: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 526
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object Shape4: TShape
            Left = 5
            Top = 39
            Width = 754
            Height = 1
            Brush.Color = clBtnFace
            Enabled = False
            Pen.Color = clBtnFace
          end
          object Shape5: TShape
            Left = 5
            Top = 367
            Width = 754
            Height = 1
            Brush.Color = clBtnFace
            Enabled = False
            Pen.Color = clBtnFace
          end
          object sEdit1: TsEdit
            Left = 669
            Top = 591
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 3
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sEdit2: TsEdit
            Left = 669
            Top = 547
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 1
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sEdit3: TsEdit
            Left = 669
            Top = 569
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 2
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel12: TsPanel
            Left = 1
            Top = 548
            Width = 105
            Height = 23
            SkinData.CustomColor = True
            Caption = #50868#49569#49688#45800
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            Visible = False
          end
          object sComboBox1: TsComboBox
            Left = 107
            Top = 548
            Width = 237
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            ItemHeight = 17
            ItemIndex = 1
            TabOrder = 0
            Text = 'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
            Visible = False
            Items.Strings = (
              ''
              'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
              'DT: '#54644#49345#50868#49569'(Sea)/'#54637#44277#50868#49569'(Air)')
          end
          object sPanel13: TsPanel
            Left = 1
            Top = 572
            Width = 343
            Height = 23
            SkinData.CustomColor = True
            Caption = '[PAGE2] 44A/44B'#47484' '#51089#49457#54616#49464#50836
            Color = 16576211
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            Visible = False
          end
          object sDBEdit51: TsDBEdit
            Left = 104
            Top = 8
            Width = 81
            Height = 23
            Color = clWhite
            DataField = 'DRAW_DAT'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51089#49457#51068#51088
          end
          object sDBEdit52: TsDBEdit
            Left = 256
            Top = 8
            Width = 161
            Height = 23
            Color = clWhite
            DataField = 'SUP_AMT'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#44553#44032#50529
          end
          object sDBEdit53: TsDBEdit
            Left = 472
            Top = 8
            Width = 161
            Height = 23
            Color = clWhite
            DataField = 'TAX_AMT'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49464#50529
          end
          object sDBEdit54: TsDBEdit
            Left = 704
            Top = 8
            Width = 25
            Height = 23
            Color = clWhite
            DataField = 'DETAILNO'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#46976#49688
          end
          object sDBMemo7: TsDBMemo
            Left = 104
            Top = 48
            Width = 625
            Height = 308
            Color = clWhite
            DataField = 'REMARK1'
            DataSource = dsList
            Enabled = False
            ScrollBars = ssVertical
            TabOrder = 10
            BoundLabel.Active = True
            BoundLabel.Caption = #48708#44256
            BoundLabel.Layout = sclLeftTop
          end
          object sDBEdit55: TsDBEdit
            Tag = 502
            Left = 105
            Top = 378
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT11C'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            TabOrder = 11
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #54788#44552'('#50808#54868')'
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sDBEdit56: TsDBEdit
            Left = 142
            Top = 378
            Width = 99
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT11'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 12
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit57: TsDBEdit
            Left = 278
            Top = 378
            Width = 126
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT12'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 13
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #50896#54868
          end
          object sDBEdit58: TsDBEdit
            Tag = 502
            Left = 105
            Top = 402
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT21C'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            TabOrder = 14
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#54364'('#50808#54868')'
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sDBEdit59: TsDBEdit
            Left = 142
            Top = 402
            Width = 99
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT21'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 15
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit60: TsDBEdit
            Left = 278
            Top = 402
            Width = 126
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT22'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 16
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #50896#54868
          end
          object sDBEdit61: TsDBEdit
            Tag = 502
            Left = 105
            Top = 426
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT31C'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            TabOrder = 17
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #50612#51020'('#50808#54868')'
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sDBEdit62: TsDBEdit
            Left = 142
            Top = 426
            Width = 99
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT31'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 18
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit63: TsDBEdit
            Left = 278
            Top = 426
            Width = 126
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT32'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 19
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #50896#54868
          end
          object sDBEdit64: TsDBEdit
            Tag = 502
            Left = 105
            Top = 450
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT41C'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            TabOrder = 20
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #50808#49345#48120#49688#44552
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sDBEdit65: TsDBEdit
            Left = 142
            Top = 450
            Width = 99
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT41'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit66: TsDBEdit
            Left = 278
            Top = 450
            Width = 126
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT42'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 22
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #50896#54868
          end
          object sDBEdit67: TsDBEdit
            Tag = 502
            Left = 105
            Top = 482
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'INDICATOR'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            TabOrder = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #50689#49688'/'#52397#44396
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sDBEdit68: TsDBEdit
            Left = 142
            Top = 482
            Width = 262
            Height = 23
            Color = clBtnFace
            DataField = 'INDICATOR_NM'
            DataSource = dsList
            Enabled = False
            TabOrder = 24
            SkinData.CustomColor = True
          end
          object sDBEdit69: TsDBEdit
            Left = 552
            Top = 378
            Width = 177
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'TAMT'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 25
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #52509' '#44552#50529
          end
          object sDBEdit70: TsDBEdit
            Left = 552
            Top = 402
            Width = 177
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'SUPTAMT'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 26
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #52509' '#44277#44553#44032#50529
          end
          object sDBEdit71: TsDBEdit
            Left = 552
            Top = 426
            Width = 177
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'TAXTAMT'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 27
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #52509' '#49464#50529
          end
          object sDBEdit72: TsDBEdit
            Tag = 502
            Left = 553
            Top = 458
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'USTAMTC'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            TabOrder = 28
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #52509' '#50808#54868#44277#44553#44032#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sDBEdit73: TsDBEdit
            Left = 590
            Top = 458
            Width = 139
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'USTAMT'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 29
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit74: TsDBEdit
            Tag = 502
            Left = 553
            Top = 482
            Width = 36
            Height = 23
            Hint = #45800#50948
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'TQTYC'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            TabOrder = 30
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.Caption = #52509' '#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sDBEdit75: TsDBEdit
            Left = 590
            Top = 482
            Width = 139
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            DataField = 'TQTY'
            DataSource = dsList
            Enabled = False
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            TabOrder = 31
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
        end
      end
      object sTabSheet4: TsTabSheet
        Caption = #49688#53441#51088
        object sPanel14: TsPanel
          Left = 0
          Top = 0
          Width = 763
          Height = 526
          Align = alClient
          
          TabOrder = 0
          object Shape6: TShape
            Left = 381
            Top = 8
            Width = 1
            Height = 509
            Brush.Color = clGray
            Pen.Color = clGray
          end
          object sDBEdit76: TsDBEdit
            Left = 104
            Top = 8
            Width = 95
            Height = 23
            Color = clWhite
            DataField = 'AG_SAUP'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 0
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#53441#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
          end
          object sDBEdit77: TsDBEdit
            Left = 104
            Top = 32
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'AG_NAME1'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49345#54840
          end
          object sDBEdit78: TsDBEdit
            Left = 104
            Top = 56
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'AG_NAME2'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 2
            SkinData.CustomColor = True
          end
          object sDBEdit79: TsDBEdit
            Left = 267
            Top = 8
            Width = 93
            Height = 23
            Color = clWhite
            DataField = 'AG_NAME3'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#54364#51088
          end
          object sDBEdit80: TsDBEdit
            Left = 104
            Top = 80
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'AG_ADDR1'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51452#49548
          end
          object sDBEdit81: TsDBEdit
            Left = 104
            Top = 104
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'AG_ADDR2'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 5
            SkinData.CustomColor = True
          end
          object sDBEdit82: TsDBEdit
            Left = 104
            Top = 128
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'AG_ADDR3'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 6
            SkinData.CustomColor = True
          end
          object sDBEdit83: TsDBEdit
            Left = 104
            Top = 152
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'AG_ADDR4'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 7
            SkinData.CustomColor = True
          end
          object sDBEdit84: TsDBEdit
            Left = 104
            Top = 176
            Width = 256
            Height = 23
            Color = clWhite
            DataField = 'AG_ADDR5'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 8
            SkinData.CustomColor = True
          end
          object sDBEdit85: TsDBEdit
            Left = 104
            Top = 200
            Width = 71
            Height = 23
            Color = clWhite
            DataField = 'AG_SAUP1'
            DataSource = dsList
            Enabled = False
            ReadOnly = True
            TabOrder = 9
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51333#49324#50629#51109#48264#54840
          end
          object sMemo3: TsMemo
            Left = 104
            Top = 224
            Width = 256
            Height = 67
            Ctl3D = True
            Enabled = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 10
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #45812#45817#51088
            BoundLabel.Layout = sclLeftTop
          end
          object sDBMemo8: TsDBMemo
            Left = 104
            Top = 292
            Width = 256
            Height = 50
            Color = clWhite
            DataField = 'AG_UPTA1'
            DataSource = dsList
            Enabled = False
            TabOrder = 11
            BoundLabel.Active = True
            BoundLabel.Caption = #50629#53468
            BoundLabel.Layout = sclLeftTop
          end
          object sDBMemo9: TsDBMemo
            Left = 104
            Top = 343
            Width = 256
            Height = 50
            Color = clWhite
            DataField = 'AG_ITEM1'
            DataSource = dsList
            Enabled = False
            TabOrder = 12
            BoundLabel.Active = True
            BoundLabel.Caption = #51333#47785
            BoundLabel.Layout = sclLeftTop
          end
        end
      end
    end
    object sPanel6: TsPanel
      Left = 1
      Top = 1
      Width = 771
      Height = 55
      Align = alTop
      
      TabOrder = 2
      object sSpeedButton1: TsSpeedButton
        Left = 280
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_MAINT_NO: TsEdit
        Left = 64
        Top = 4
        Width = 209
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 64
        Top = 28
        Width = 89
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        ReadOnly = True
        TabOrder = 1
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 360
        Top = 4
        Width = 121
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 360
        Top = 28
        Width = 211
        Height = 23
        BoundLabel.Active = True
        BoundLabel.Caption = #51025#45813#50976#54805
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = -1
        TabOrder = 3
        TabStop = False
        Items.Strings = (
          ''
          'CO: Confirmation of measurements'
          'AP: Accepted')
      end
      object edt_userno: TsEdit
        Left = 216
        Top = 28
        Width = 57
        Height = 23
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 48
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20000101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20181231'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RE_NO, SE_N' +
        'O, FS_NO, ACE_NO, RFF_NO, SE_CODE, SE_SAUP, SE_NAME1, SE_NAME2, ' +
        'SE_ADDR1, SE_ADDR2, SE_ADDR3, SE_UPTA, SE_UPTA1, SE_ITEM, SE_ITE' +
        'M1, BY_CODE, BY_SAUP, BY_NAME1, BY_NAME2, BY_ADDR1, BY_ADDR2, BY' +
        '_ADDR3, BY_UPTA, BY_UPTA1, BY_ITEM, BY_ITEM1, AG_CODE, AG_SAUP, ' +
        'AG_NAME1, AG_NAME2, AG_NAME3, AG_ADDR1, AG_ADDR2, AG_ADDR3, AG_U' +
        'PTA, AG_UPTA1, AG_ITEM, AG_ITEM1, DRAW_DAT, DETAILNO, SUP_AMT, T' +
        'AX_AMT, REMARK, REMARK1, AMT11, AMT11C, AMT12, AMT21, AMT21C, AM' +
        'T22, AMT31, AMT31C, AMT32, AMT41, AMT41C, AMT42, INDICATOR, INDC' +
        '.NAME as INDICATOR_NM, TAMT, SUPTAMT, TAXTAMT, USTAMT, USTAMTC, ' +
        'TQTY, TQTYC, CHK1, CHK2, CHK3, PRNO,'
      
        'VAT_CODE, VC.NAME as VAT_CODE_NM, VAT_TYPE, VT.NAME as VAT_TYPE_' +
        'NM, NEW_INDICATOR, IND.NAME as NEW_INDICATOR_NM, SE_ADDR4, SE_AD' +
        'DR5, SE_SAUP1, SE_SAUP2, SE_SAUP3, SE_FTX1, SE_FTX2, SE_FTX3, SE' +
        '_FTX4, SE_FTX5, BY_SAUP_CODE, BY_ADDR4, BY_ADDR5, BY_SAUP1, BY_S' +
        'AUP2, BY_SAUP3, BY_FTX1, BY_FTX2, BY_FTX3, BY_FTX4, BY_FTX5, BY_' +
        'FTX1_1, BY_FTX2_1, BY_FTX3_1, BY_FTX4_1, BY_FTX5_1, AG_ADDR4, AG' +
        '_ADDR5, AG_SAUP1, AG_SAUP2, AG_SAUP3, AG_FTX1, AG_FTX2, AG_FTX3,' +
        ' AG_FTX4, AG_FTX5, SE_NAME3, BY_NAME3'
      
        'FROM [VATBI2_H] LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE' +
        ' prefix = '#39'VAT_CODE'#39') VC ON VATBI2_H.VAT_CODE = VC.CODE    '
      
        '                LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE' +
        ' Prefix = '#39'VAT_TYPE'#39') VT ON VATBI2_H.VAT_TYPE = VT.CODE'
      
        '                LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE' +
        ' Prefix = '#39'NEW_INDI'#39') IND ON VATBI2_H.NEW_INDICATOR = IND.CODE'
      
        '                LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE' +
        ' Prefix = '#39'7365VATBIL'#39') INDC ON VATBI2_H.INDICATOR = INDC.CODE'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 48
    Top = 152
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRE_NO: TStringField
      FieldName = 'RE_NO'
      Size = 35
    end
    object qryListSE_NO: TStringField
      FieldName = 'SE_NO'
      Size = 35
    end
    object qryListFS_NO: TStringField
      FieldName = 'FS_NO'
      Size = 35
    end
    object qryListACE_NO: TStringField
      FieldName = 'ACE_NO'
      Size = 35
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListSE_CODE: TStringField
      FieldName = 'SE_CODE'
      Size = 10
    end
    object qryListSE_SAUP: TStringField
      FieldName = 'SE_SAUP'
      EditMask = '999-99-99999;0;'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_ADDR1: TStringField
      FieldName = 'SE_ADDR1'
      Size = 35
    end
    object qryListSE_ADDR2: TStringField
      FieldName = 'SE_ADDR2'
      Size = 35
    end
    object qryListSE_ADDR3: TStringField
      FieldName = 'SE_ADDR3'
      Size = 35
    end
    object qryListSE_UPTA: TStringField
      FieldName = 'SE_UPTA'
      Size = 35
    end
    object qryListSE_UPTA1: TMemoField
      FieldName = 'SE_UPTA1'
      BlobType = ftMemo
    end
    object qryListSE_ITEM: TStringField
      FieldName = 'SE_ITEM'
      Size = 1
    end
    object qryListSE_ITEM1: TMemoField
      FieldName = 'SE_ITEM1'
      BlobType = ftMemo
    end
    object qryListBY_CODE: TStringField
      FieldName = 'BY_CODE'
      Size = 10
    end
    object qryListBY_SAUP: TStringField
      FieldName = 'BY_SAUP'
      EditMask = '999-99-99999;0;'
      Size = 35
    end
    object qryListBY_NAME1: TStringField
      FieldName = 'BY_NAME1'
      Size = 35
    end
    object qryListBY_NAME2: TStringField
      FieldName = 'BY_NAME2'
      Size = 35
    end
    object qryListBY_ADDR1: TStringField
      FieldName = 'BY_ADDR1'
      Size = 35
    end
    object qryListBY_ADDR2: TStringField
      FieldName = 'BY_ADDR2'
      Size = 35
    end
    object qryListBY_ADDR3: TStringField
      FieldName = 'BY_ADDR3'
      Size = 35
    end
    object qryListBY_UPTA: TStringField
      FieldName = 'BY_UPTA'
      Size = 35
    end
    object qryListBY_UPTA1: TMemoField
      FieldName = 'BY_UPTA1'
      BlobType = ftMemo
    end
    object qryListBY_ITEM: TStringField
      FieldName = 'BY_ITEM'
      Size = 1
    end
    object qryListBY_ITEM1: TMemoField
      FieldName = 'BY_ITEM1'
      BlobType = ftMemo
    end
    object qryListAG_CODE: TStringField
      FieldName = 'AG_CODE'
      Size = 10
    end
    object qryListAG_SAUP: TStringField
      FieldName = 'AG_SAUP'
      Size = 35
    end
    object qryListAG_NAME1: TStringField
      FieldName = 'AG_NAME1'
      Size = 35
    end
    object qryListAG_NAME2: TStringField
      FieldName = 'AG_NAME2'
      Size = 35
    end
    object qryListAG_NAME3: TStringField
      FieldName = 'AG_NAME3'
      Size = 35
    end
    object qryListAG_ADDR1: TStringField
      FieldName = 'AG_ADDR1'
      Size = 35
    end
    object qryListAG_ADDR2: TStringField
      FieldName = 'AG_ADDR2'
      Size = 35
    end
    object qryListAG_ADDR3: TStringField
      FieldName = 'AG_ADDR3'
      Size = 35
    end
    object qryListAG_UPTA: TStringField
      FieldName = 'AG_UPTA'
      Size = 35
    end
    object qryListAG_UPTA1: TMemoField
      FieldName = 'AG_UPTA1'
      BlobType = ftMemo
    end
    object qryListAG_ITEM: TStringField
      FieldName = 'AG_ITEM'
      Size = 1
    end
    object qryListAG_ITEM1: TMemoField
      FieldName = 'AG_ITEM1'
      BlobType = ftMemo
    end
    object qryListDRAW_DAT: TStringField
      FieldName = 'DRAW_DAT'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryListDETAILNO: TBCDField
      FieldName = 'DETAILNO'
      Precision = 18
    end
    object qryListSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      DisplayFormat = '#,0;'
      Precision = 18
    end
    object qryListTAX_AMT: TBCDField
      FieldName = 'TAX_AMT'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAMT11: TBCDField
      FieldName = 'AMT11'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListAMT11C: TStringField
      FieldName = 'AMT11C'
      Size = 3
    end
    object qryListAMT12: TBCDField
      FieldName = 'AMT12'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListAMT21: TBCDField
      FieldName = 'AMT21'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListAMT21C: TStringField
      FieldName = 'AMT21C'
      Size = 3
    end
    object qryListAMT22: TBCDField
      FieldName = 'AMT22'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListAMT31: TBCDField
      FieldName = 'AMT31'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListAMT31C: TStringField
      FieldName = 'AMT31C'
      Size = 3
    end
    object qryListAMT32: TBCDField
      FieldName = 'AMT32'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListAMT41: TBCDField
      FieldName = 'AMT41'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListAMT41C: TStringField
      FieldName = 'AMT41C'
      Size = 3
    end
    object qryListAMT42: TBCDField
      FieldName = 'AMT42'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListINDICATOR: TStringField
      FieldName = 'INDICATOR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListSUPTAMT: TBCDField
      FieldName = 'SUPTAMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListTAXTAMT: TBCDField
      FieldName = 'TAXTAMT'
      DisplayFormat = '#,0'
      Precision = 18
    end
    object qryListUSTAMT: TBCDField
      FieldName = 'USTAMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListUSTAMTC: TStringField
      FieldName = 'USTAMTC'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListVAT_CODE: TStringField
      FieldName = 'VAT_CODE'
      Size = 4
    end
    object qryListVAT_CODE_NM: TStringField
      FieldName = 'VAT_CODE_NM'
      Size = 100
    end
    object qryListVAT_TYPE: TStringField
      FieldName = 'VAT_TYPE'
      Size = 4
    end
    object qryListVAT_TYPE_NM: TStringField
      FieldName = 'VAT_TYPE_NM'
      Size = 100
    end
    object qryListNEW_INDICATOR: TStringField
      FieldName = 'NEW_INDICATOR'
      Size = 4
    end
    object qryListNEW_INDICATOR_NM: TStringField
      FieldName = 'NEW_INDICATOR_NM'
      Size = 100
    end
    object qryListSE_ADDR4: TStringField
      FieldName = 'SE_ADDR4'
      Size = 35
    end
    object qryListSE_ADDR5: TStringField
      FieldName = 'SE_ADDR5'
      Size = 35
    end
    object qryListSE_SAUP1: TStringField
      FieldName = 'SE_SAUP1'
      Size = 4
    end
    object qryListSE_SAUP2: TStringField
      FieldName = 'SE_SAUP2'
      Size = 4
    end
    object qryListSE_SAUP3: TStringField
      FieldName = 'SE_SAUP3'
      Size = 4
    end
    object qryListSE_FTX1: TStringField
      FieldName = 'SE_FTX1'
      Size = 40
    end
    object qryListSE_FTX2: TStringField
      FieldName = 'SE_FTX2'
      Size = 30
    end
    object qryListSE_FTX3: TStringField
      FieldName = 'SE_FTX3'
    end
    object qryListSE_FTX4: TStringField
      FieldName = 'SE_FTX4'
    end
    object qryListSE_FTX5: TStringField
      FieldName = 'SE_FTX5'
    end
    object qryListBY_SAUP_CODE: TStringField
      FieldName = 'BY_SAUP_CODE'
      Size = 4
    end
    object qryListBY_ADDR4: TStringField
      FieldName = 'BY_ADDR4'
      Size = 35
    end
    object qryListBY_ADDR5: TStringField
      FieldName = 'BY_ADDR5'
      Size = 35
    end
    object qryListBY_SAUP1: TStringField
      FieldName = 'BY_SAUP1'
      Size = 4
    end
    object qryListBY_SAUP2: TStringField
      FieldName = 'BY_SAUP2'
      Size = 4
    end
    object qryListBY_SAUP3: TStringField
      FieldName = 'BY_SAUP3'
      Size = 4
    end
    object qryListBY_FTX1: TStringField
      FieldName = 'BY_FTX1'
      Size = 40
    end
    object qryListBY_FTX2: TStringField
      FieldName = 'BY_FTX2'
      Size = 30
    end
    object qryListBY_FTX3: TStringField
      FieldName = 'BY_FTX3'
    end
    object qryListBY_FTX4: TStringField
      FieldName = 'BY_FTX4'
    end
    object qryListBY_FTX5: TStringField
      FieldName = 'BY_FTX5'
    end
    object qryListBY_FTX1_1: TStringField
      FieldName = 'BY_FTX1_1'
      Size = 40
    end
    object qryListBY_FTX2_1: TStringField
      FieldName = 'BY_FTX2_1'
      Size = 30
    end
    object qryListBY_FTX3_1: TStringField
      FieldName = 'BY_FTX3_1'
    end
    object qryListBY_FTX4_1: TStringField
      FieldName = 'BY_FTX4_1'
    end
    object qryListBY_FTX5_1: TStringField
      FieldName = 'BY_FTX5_1'
    end
    object qryListAG_ADDR4: TStringField
      FieldName = 'AG_ADDR4'
      Size = 35
    end
    object qryListAG_ADDR5: TStringField
      FieldName = 'AG_ADDR5'
      Size = 35
    end
    object qryListAG_SAUP1: TStringField
      FieldName = 'AG_SAUP1'
      Size = 4
    end
    object qryListAG_SAUP2: TStringField
      FieldName = 'AG_SAUP2'
      Size = 4
    end
    object qryListAG_SAUP3: TStringField
      FieldName = 'AG_SAUP3'
      Size = 4
    end
    object qryListAG_FTX1: TStringField
      FieldName = 'AG_FTX1'
      Size = 40
    end
    object qryListAG_FTX2: TStringField
      FieldName = 'AG_FTX2'
      Size = 30
    end
    object qryListAG_FTX3: TStringField
      FieldName = 'AG_FTX3'
    end
    object qryListAG_FTX4: TStringField
      FieldName = 'AG_FTX4'
    end
    object qryListAG_FTX5: TStringField
      FieldName = 'AG_FTX5'
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListBY_NAME3: TStringField
      FieldName = 'BY_NAME3'
      Size = 35
    end
    object qryListINDICATOR_NM: TStringField
      FieldName = 'INDICATOR_NM'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 152
  end
  object qryGoods: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT  KEYY, SEQ, DE_DATE, NAME_COD, NAME1, SIZE1, DE_REM1, QTY' +
        ', QTY_G, QTYG, QTYG_G, PRICE, PRICE_G, SUPAMT, TAXAMT, USAMT, US' +
        'AMT_G, SUPSTAMT, TAXSTAMT, USSTAMT, USSTAMT_G, STQTY, STQTY_G, R' +
        'ATE'
      'FROM VATBI2_D'
      'WHERE KEYY = :MAINT_NO')
    Left = 48
    Top = 184
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryGoodsDE_DATE: TStringField
      FieldName = 'DE_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryGoodsNAME_COD: TStringField
      FieldName = 'NAME_COD'
    end
    object qryGoodsNAME1: TMemoField
      FieldName = 'NAME1'
      OnGetText = qryGoodsNAME1GetText
      BlobType = ftMemo
    end
    object qryGoodsSIZE1: TMemoField
      FieldName = 'SIZE1'
      OnGetText = qryGoodsNAME1GetText
      BlobType = ftMemo
    end
    object qryGoodsDE_REM1: TMemoField
      FieldName = 'DE_REM1'
      BlobType = ftMemo
    end
    object qryGoodsQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryGoodsQTYG: TBCDField
      FieldName = 'QTYG'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryGoodsPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryGoodsSUPAMT: TBCDField
      FieldName = 'SUPAMT'
      DisplayFormat = '#,0;'
      Precision = 18
    end
    object qryGoodsTAXAMT: TBCDField
      FieldName = 'TAXAMT'
      DisplayFormat = '#,0;'
      Precision = 18
    end
    object qryGoodsUSAMT: TBCDField
      FieldName = 'USAMT'
      DisplayFormat = '#,0.####;'
      Precision = 18
    end
    object qryGoodsUSAMT_G: TStringField
      FieldName = 'USAMT_G'
      Size = 3
    end
    object qryGoodsSUPSTAMT: TBCDField
      FieldName = 'SUPSTAMT'
      DisplayFormat = '#,0;'
      Precision = 18
    end
    object qryGoodsTAXSTAMT: TBCDField
      FieldName = 'TAXSTAMT'
      DisplayFormat = '#,0;'
      Precision = 18
    end
    object qryGoodsUSSTAMT: TBCDField
      FieldName = 'USSTAMT'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryGoodsUSSTAMT_G: TStringField
      FieldName = 'USSTAMT_G'
      Size = 3
    end
    object qryGoodsSTQTY: TBCDField
      FieldName = 'STQTY'
      DisplayFormat = '#,0.####'
      Precision = 18
    end
    object qryGoodsSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryGoodsRATE: TBCDField
      FieldName = 'RATE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
  end
  object dsGoods: TDataSource
    DataSet = qryGoods
    Left = 80
    Top = 184
  end
end
