unit UI_APPLOG_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, ComCtrls, sPageControl, Buttons,
  sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids, acDBGrid, sButton,
  sLabel, sSpeedButton, ExtCtrls, sPanel, sSkinProvider, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, sMemo, DB, ADODB, StrUtils, DateUtils;

type
  TUI_APPLOG_NEW_FRM = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    edt_MESSAGE1: TsEdit;
    edt_Message1NM: TsEdit;
    edt_LC_G: TsEdit;
    sEdit2: TsEdit;
    edt_LC_NO: TsEdit;
    edt_BL_G: TsEdit;
    sEdit5: TsEdit;
    edt_BL_NO: TsEdit;
    msk_APP_DATE: TsMaskEdit;
    msk_BL_DATE: TsMaskEdit;
    edt_CARRIER2: TsEdit;
    edt_CARRIER1: TsEdit;
    edt_CR_NAME1: TsEdit;
    edt_CR_NAME2: TsEdit;
    edt_CR_NAME3: TsEdit;
    edt_B5_NAME1: TsEdit;
    edt_B5_NAME2: TsEdit;
    edt_B5_NAME3: TsEdit;
    edt_SE_NAME1: TsEdit;
    edt_SE_NAME2: TsEdit;
    edt_SE_NAME3: TsEdit;
    edt_CN_NAME1: TsEdit;
    edt_CN_NAME2: TsEdit;
    edt_CN_NAME3: TsEdit;
    edt_LOAD_LOC: TsEdit;
    edt_LOAD_LOC_NM: TsEdit;
    edt_LOAD_TXT: TsEdit;
    edt_ARR_LOC: TsEdit;
    edt_ARR_LOC_NM: TsEdit;
    edt_ARR_TXT: TsEdit;
    msk_AR_DATE: TsMaskEdit;
    Shape1: TShape;
    edt_INV_AMTC: TsEdit;
    curr_INV_AMT: TsCurrencyEdit;
    edt_PAC_QTYC: TsEdit;
    curr_PAC_QTY: TsCurrencyEdit;
    memo_SPMARK: TsMemo;
    memo_GOODS: TsMemo;
    edt_TRM_PAYC: TsEdit;
    sEdit30: TsEdit;
    edt_TRM_PAY: TsEdit;
    edt_BANK_CD: TsEdit;
    edt_BANK_TXT: TsEdit;
    edt_BANK_BR: TsEdit;
    edt_MS_NAME1: TsEdit;
    edt_MS_NAME2: TsEdit;
    edt_MS_NAME3: TsEdit;
    edt_AX_NAME1: TsEdit;
    edt_AX_NAME2: TsEdit;
    edt_AX_NAME3: TsEdit;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListMESSAGE3: TStringField;
    qryListCR_NAME1: TStringField;
    qryListCR_NAME2: TStringField;
    qryListCR_NAME3: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_NAME3: TStringField;
    qryListMS_NAME1: TStringField;
    qryListMS_NAME2: TStringField;
    qryListMS_NAME3: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListINV_AMT: TBCDField;
    qryListINV_AMTC: TStringField;
    qryListLC_G: TStringField;
    qryListLC_NO: TStringField;
    qryListBL_G: TStringField;
    qryListBL_NO: TStringField;
    qryListCARRIER1: TStringField;
    qryListCARRIER2: TStringField;
    qryListAR_DATE: TStringField;
    qryListBL_DATE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListLOAD_LOC: TStringField;
    qryListLOAD_TXT: TStringField;
    qryListARR_LOC: TStringField;
    qryListARR_TXT: TStringField;
    qryListSPMARK1: TStringField;
    qryListSPMARK2: TStringField;
    qryListSPMARK3: TStringField;
    qryListSPMARK4: TStringField;
    qryListSPMARK5: TStringField;
    qryListSPMARK6: TStringField;
    qryListSPMARK7: TStringField;
    qryListSPMARK8: TStringField;
    qryListSPMARK9: TStringField;
    qryListSPMARK10: TStringField;
    qryListPAC_QTY: TBCDField;
    qryListPAC_QTYC: TStringField;
    qryListGOODS1: TStringField;
    qryListGOODS2: TStringField;
    qryListGOODS3: TStringField;
    qryListGOODS4: TStringField;
    qryListGOODS5: TStringField;
    qryListGOODS6: TStringField;
    qryListGOODS7: TStringField;
    qryListGOODS8: TStringField;
    qryListGOODS9: TStringField;
    qryListGOODS10: TStringField;
    qryListTRM_PAYC: TStringField;
    qryListTRM_PAY: TStringField;
    qryListBANK_CD: TStringField;
    qryListBANK_TXT: TStringField;
    qryListBANK_BR: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListCN_NAME1: TStringField;
    qryListCN_NAME2: TStringField;
    qryListCN_NAME3: TStringField;
    qryListB5_NAME1: TStringField;
    qryListB5_NAME2: TStringField;
    qryListB5_NAME3: TStringField;
    qryListMSG1NAME: TStringField;
    qryListLCGNAME: TStringField;
    qryListBLGNAME: TStringField;
    qryListLOADNAME: TStringField;
    qryListARRNAME: TStringField;
    qryListTRMPAYCNAME: TStringField;
    qryListCRNAEM3NAME: TStringField;
    dsList: TDataSource;
    sButton1: TsButton;
    btnCopy: TsButton;
    sSpeedButton2: TsSpeedButton;
    qryReady: TADOQuery;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    procedure sBitBtn1Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure edt_MESSAGE1DblClick(Sender: TObject);
    procedure edt_MESSAGE1Exit(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure edt_BANK_CDDblClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
  private
    procedure NewDoc;
    procedure ButtonEnable;
    procedure APPLOG_GB_CHANGED;
    function CHECK_VALIDITY: String;
    function DataProcess(nType: integer): String;
    procedure ReadyDocument(DocNo: String);
  protected
    procedure ReadData; override;
    procedure ReadList;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_APPLOG_NEW_FRM: TUI_APPLOG_NEW_FRM;

implementation

uses
  MSSQL, ICON, MessageDefine, VarDefine, CodeContents, TypeDefine, AutoNo, Commonlib, CodeDialogParent, CD_APPLOG_GB, CD_LC_GB, CD_BL_GB, CD_SUNSA, CD_NATION, CD_AMT_UNIT, CD_UNIT, CD_NK4277, CD_BANK, Dlg_ErrorMessage, SQLCreator, Preview, APPLOG_PRINT, DocumentSend, Dlg_RecvSelect, CreateDocuments;

{$R *.dfm}

{ TUI_APPLOG_NEW_FRM }

procedure TUI_APPLOG_NEW_FRM.ReadData;
var
  sBuf : string;
begin
  inherited;
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;

  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;
  //신청서종류
  edt_MESSAGE1.Text   := qryListMESSAGE1.AsString;
  APPLOG_GB_CHANGED;
  edt_Message1NM.Text := qryListMSG1NAME.AsString;

  CM_INDEX(com_func, [':'], 0, qryListMESSAGE2.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE3.AsString);

  msk_APP_DATE.Text := qryListAPP_DATE.AsString;

  edt_LC_G.Text := qryListLC_G.AsString;
  sEdit2.Text := qryListLCGNAME.AsString;
  edt_LC_NO.Text := qryListLC_NO.AsString;

  edt_BL_G.Text  := qryListBL_G.AsString;
  sEdit5.Text    := qryListBLGNAME.AsString;
  edt_BL_NO.Text := qryListBL_NO.AsString;
  msk_BL_DATE.Text := qryListBL_DATE.AsString;

  edt_CARRIER1.Text := qryListCARRIER1.AsString;
  edt_CARRIER2.Text := qryListCARRIER2.AsString;

  edt_CR_NAME1.Text := qryListCR_NAME1.AsString;
  edt_CR_NAME2.Text := qryListCR_NAME2.AsString;
  edt_CR_NAME3.Text := qryListCR_NAME3.AsString;

  edt_B5_NAME1.Text := qryListB5_NAME1.AsString;
  edt_B5_NAME2.Text := qryListB5_NAME2.AsString;
  edt_B5_NAME3.Text := qryListB5_NAME3.AsString;

  edt_SE_NAME1.Text := qryListSE_NAME1.AsString;
  edt_SE_NAME2.Text := qryListSE_NAME2.AsString;
  edt_SE_NAME3.Text := qryListSE_NAME3.AsString;

  edt_CN_NAME1.Text := qryListCN_NAME1.AsString;
  edt_CN_NAME2.Text := qryListCN_NAME2.AsString;
  edt_CN_NAME3.Text := qryListCN_NAME3.AsString;

  edt_LOAD_LOC.Text := qryListLOAD_LOC.AsString;
  edt_LOAD_LOC_NM.Text := qryListLOADNAME.AsString;
  edt_LOAD_TXT.Text := qryListLOAD_TXT.AsString;

  edt_ARR_LOC.Text := qryListARR_LOC.AsString;
  edt_ARR_LOC_NM.Text := qryListARRNAME.AsString;
  edt_ARR_TXT.Text := qryListARR_TXT.AsString;
  msk_AR_DATE.Text := qryListAR_DATE.AsString;

  edt_INV_AMTC.Text  := qryListINV_AMTC.AsString;
  curr_INV_AMT.Value := qryListINV_AMT.AsCurrency;

  edt_PAC_QTYC.Text := qryListPAC_QTYC.AsString;
  curr_PAC_QTY.Value := qryListPAC_QTY.AsCurrency;

  memo_SPMARK.Clear;
  IF Trim(qryListSPMARK1.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK1.AsString));
  IF Trim(qryListSPMARK2.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK2.AsString));
  IF Trim(qryListSPMARK3.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK3.AsString));
  IF Trim(qryListSPMARK4.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK4.AsString));
  IF Trim(qryListSPMARK5.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK5.AsString));
  IF Trim(qryListSPMARK6.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK6.AsString));
  IF Trim(qryListSPMARK7.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK7.AsString));
  IF Trim(qryListSPMARK8.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK8.AsString));
  IF Trim(qryListSPMARK9.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK9.AsString));
  IF Trim(qryListSPMARK10.AsString) <> '' then memo_SPMARK.Lines.Add(Trim(qryListSPMARK10.AsString));

  memo_GOODS.Clear;
  IF Trim(qryListGOODS1.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS1.AsString));
  IF Trim(qryListGOODS2.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS2.AsString));
  IF Trim(qryListGOODS3.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS3.AsString));
  IF Trim(qryListGOODS4.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS4.AsString));
  IF Trim(qryListGOODS5.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS5.AsString));
  IF Trim(qryListGOODS6.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS6.AsString));
  IF Trim(qryListGOODS7.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS7.AsString));
  IF Trim(qryListGOODS8.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS8.AsString));
  IF Trim(qryListGOODS9.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS9.AsString));
  IF Trim(qryListGOODS10.AsString) <> '' then memo_GOODS.Lines.Add(Trim(qryListGOODS10.AsString));

  edt_TRM_PAYC.Text := qryListTRM_PAYC.AsString;
  sEdit30.Text := qryListTRMPAYCNAME.AsString;
  edt_TRM_PAY.Text := qryListTRM_PAY.AsString;

  edt_BANK_CD.Text  := qryListBANK_CD.AsString;
  edt_BANK_TXT.Text := qryListBANK_TXT.AsString;
  edt_BANK_BR.Text  := qryListBANK_BR.AsString;

  edt_MS_NAME1.Text := qryListMS_NAME1.AsString;
  edt_MS_NAME2.Text := qryListMS_NAME2.AsString;
  edt_MS_NAME3.Text := qryListMS_NAME3.AsString;

  edt_AX_NAME1.Text := qryListAX_NAME1.AsString;
  edt_AX_NAME2.Text := qryListAX_NAME2.AsString;
  edt_AX_NAME3.Text := qryListAX_NAME3.AsString;
end;

procedure TUI_APPLOG_NEW_FRM.ReadList;
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;
    
    Open;
  end;
end;

procedure TUI_APPLOG_NEW_FRM.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList; 
end;

procedure TUI_APPLOG_NEW_FRM.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_APPLOG_NEW_FRM.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then ReadData;
end;

procedure TUI_APPLOG_NEW_FRM.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  sBitBtn1Click(nil);

  EnableControl(sPanel6, false);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_APPLOG_NEW_FRM.btnNewClick(Sender: TObject);
begin
  IF ((Sender as TsButton).Tag = 1) AND AnsiMatchText( qryListCHK2.AsString , ['4', '7', '9'] ) Then
  begin
    MessageBox(Self.Handle, MSG_SYSTEM_NOT_EDIT, '수정불가', MB_OK+MB_ICONINFORMATION);
    Exit;
  end;

  IF (Sender as TsButton).Tag = 0 Then NewDoc;
  IF (Sender as TsButton).Tag = 2 Then
  begin
    //복사 - 문서번호만 새로 가져옴
    edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPLOG');
    msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
    msk_APP_DATE.Text := FormatDateTime('YYYYMMDD', Now);
    sDBGrid1.Enabled := False;
    sPanel29.Visible := True;
  end;

  EnableControl(sPanel6);
  EnableControl(sPanel7);
  EnableControl(sPanel27);

  case (Sender as TsButton).Tag of
    0,2:
      ProgramControlType := ctInsert;
    1:
      ProgramControlType := ctModify;
  end;

  if ProgramControlType = ctmodify then
  begin
    EnableControl(sPanel6, false);
  end;

  //------------------------------------------------------------------------------
  // 버튼활성화
  //------------------------------------------------------------------------------
  ButtonEnable;
//  OriginSection;
  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_APPLOG_NEW_FRM.NewDoc;
var
  TMP_NAME, TMP_SIZE : String;
begin
  ClearControl(sPanel6);
  ClearControl(sPanel7);
  ClearControl(sPanel27);
  sPanel29.Visible := True;
  APPLOG_GB_CHANGED;

//------------------------------------------------------------------------------
// 헤더
//------------------------------------------------------------------------------
  msk_Datee.Text := FormatDateTime('YYYYMMDD', Now);
  edt_userno.Text := LoginData.sID;
  com_func.ItemIndex := 5;
  com_type.ItemIndex := 0;
//------------------------------------------------------------------------------
// PAGE1
//------------------------------------------------------------------------------
  edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPLOG');
  msk_APP_DATE.Text := FormatDateTime('YYYYMMDD', Now);

//------------------------------------------------------------------------------
// 개설의뢰인 셋팅
//------------------------------------------------------------------------------
  IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
  begin
    edt_MS_NAME1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    edt_MS_NAME2.Text := CopyK(Trim(DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').AsString +' '+DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').AsString),1, 35);
    edt_MS_NAME3.Text := DMCodeContents.GEOLAECHEO.FieldByName('jenja').AsString;

    edt_AX_NAME1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
    edt_AX_NAME2.Text := DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
    edt_AX_NAME3.Text := DMCodeContents.GEOLAECHEO.FieldByName('jenja').AsString;
  end;

end;

procedure TUI_APPLOG_NEW_FRM.ButtonEnable;
begin
  inherited;
  btnNew.Enabled := ProgramControlType = ctView;
  btnEdit.Enabled := btnNew.Enabled;
  btnDel.Enabled := btnNew.Enabled;
  btnCopy.Enabled := btnNew.Enabled;
  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := not btnNew.Enabled;
  btnCancel.Enabled := not btnNew.Enabled;
  btnPrint.Enabled := btnNew.Enabled;
  btnReady.Enabled := btnNew.Enabled;
  btnSend.Enabled := btnNew.Enabled;

  sDBGrid1.Enabled := btnNew.Enabled;
//  sDBGrid3.Enabled := btnNew.Enabled;
end;

procedure TUI_APPLOG_NEW_FRM.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_APPLOG_NEW_FRM.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_APPLOG_NEW_FRM := nil;
end;

procedure TUI_APPLOG_NEW_FRM.btnCancelClick(Sender: TObject);
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;

//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sPanel6, False);
  EnableControl(sPanel7, False);  
  EnableControl(sPanel27, False);

//------------------------------------------------------------------------------
// 버튼활성화
//------------------------------------------------------------------------------
  ButtonEnable;

  IF DMMssql.inTrans Then
  begin
    Case (Sender as TsButton).Tag of
      0,1: DMMssql.CommitTrans;
      2: DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  Readlist;
end;

procedure TUI_APPLOG_NEW_FRM.edt_MESSAGE1DblClick(Sender: TObject);
var
  uDialog : TCodeDialogParent_frm;
begin
  inherited;

  uDialog := nil;
  case (Sender as TsEdit).Tag of
    //신청서종류
    100 : uDialog := TCD_APPLOG_GB_frm.Create(Self);
    //신용장/계약서 선택
    101 : uDialog := TCD_LC_GB_frm.Create(Self);
    //선하증권/운송장
    102 : uDialog := TCD_BL_GB_frm.Create(Self);
    //선적항/도착항
    103, 104 : uDialog := TCD_NATION_frm.Create(Self);
    //상업송장금액
    105 : uDialog := TCD_AMT_UNIT_frm.Create(Self);
    //포장수
    106 : uDialog := TCD_UNIT_frm.Create(Self);
    // 결제기간
    107 : uDialog := TCD_NK4277_frm.Create(Self);
  end;

  if uDialog = nil Then Exit;

  try
    if uDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := uDialog.Values[0];

      case (Sender as TsEdit).Tag of
        //신청서종류
        100 :
        begin
          edt_Message1NM.Text := uDialog.Values[1];
          APPLOG_GB_CHANGED;
        end;
        //신용장 계약서 선택
        101 : sEdit2.Text := uDialog.Values[1];
        102 : sEdit5.Text := uDialog.Values[1];
        103 : edt_LOAD_LOC_NM.Text := uDialog.Values[1];
        104 : edt_ARR_LOC_NM.Text := uDialog.Values[1];
        107 : sEdit30.Text := uDialog.Values[1];
      end;
    end;
  finally
    FreeAndNil(uDialog);
  end;
end;

procedure TUI_APPLOG_NEW_FRM.edt_MESSAGE1Exit(Sender: TObject);
var
  uDialog : TCodeDialogParent_frm;
begin
  inherited;

  uDialog := nil;
  case (Sender as TsEdit).Tag of
    //신청서종류
    100 : uDialog := TCD_APPLOG_GB_frm.Create(Self);
    //신용장/계약서 선택
    101 : uDialog := TCD_LC_GB_frm.Create(Self);
    //선하증권/운송장
    102 : uDialog := TCD_BL_GB_frm.Create(Self);
    //선적항/도착항
    103, 104 : uDialog := TCD_NATION_frm.Create(Self);
    // 결제기간
    107 : uDialog := TCD_NK4277_frm.Create(Self);
  end;

  if uDialog = nil Then Exit;

  try
    case (Sender as TsEdit).Tag of
      //신청서종류
      100 :
      begin
        edt_Message1NM.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
        APPLOG_GB_CHANGED;
      end;
      101 : sEdit2.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
      102 : sEdit5.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
      103 : edt_LOAD_LOC_NM.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
      104 : edt_ARR_LOC_NM.Text  := uDialog.GetCodeText((Sender as TsEdit).Text);
      107 : sEdit30.Text := uDialog.GetCodeText((Sender as TsEdit).Text);
    end;
  finally
    FreeAndNil(uDialog);
  end;
end;

procedure TUI_APPLOG_NEW_FRM.APPLOG_GB_CHANGED;
begin
  edt_CR_NAME3.BoundLabel.Active := AnsiMatchText(edt_MESSAGE1.Text , ['2AZ', '2CK']);
  sButton1.Visible := edt_CR_NAME3.BoundLabel.Active;
  IF edt_CR_NAME3.BoundLabel.Active then
    edt_CR_NAME3.Width := 192
  else
    edt_CR_NAME3.Width := 279;  
end;

procedure TUI_APPLOG_NEW_FRM.sButton1Click(Sender: TObject);
begin
  inherited;
  CD_SUNSA_frm := TCD_SUNSA_frm.Create(Self);
  try
    IF CD_SUNSA_frm.OpenDialog(0, edt_CR_NAME3.Text) Then
    begin
      edt_CR_NAME3.Text := CD_SUNSA_frm.Values[0];
    end;
  finally
    FreeAndNil(CD_SUNSA_frm);
  end;
end;

procedure TUI_APPLOG_NEW_FRM.edt_BANK_CDDblClick(Sender: TObject);
begin
  inherited;
  CD_BANK_frm := TCD_BANK_frm.Create(Self);
  try
    IF CD_BANK_frm.OpenDialog(0, edt_BANK_CD.Text) Then
    begin
      edt_BANK_CD.Text  := CD_BANK_frm.Values[0];
      edt_BANK_TXT.Text := Trim(CD_BANK_frm.Field('BankName1').AsString+' '+CD_BANK_frm.Field('ENAME2').AsString);
      edt_BANK_BR.Text  := Trim(CD_BANK_frm.Field('BankBranch1').AsString+' '+CD_BANK_frm.Field('ENAME4').AsString);
    end;
  finally
    FreeAndNil(CD_BANK_frm);
  end;
end;

procedure TUI_APPLOG_NEW_FRM.btnSaveClick(Sender: TObject);
var
  DOCNO :String;
begin
  inherited;
//------------------------------------------------------------------------------
// 데이터저장
//------------------------------------------------------------------------------
  if (Sender as TsButton).Tag = 1 then
  begin
    Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
    if Dlg_ErrorMessage_frm.Run_ErrorMessage( '취소불능화환신용장 개설 신청서' ,CHECK_VALIDITY ) Then
    begin
      FreeAndNil(Dlg_ErrorMessage_frm);
      Exit;
    end;
  end;

  try
    DOCNO := DataProcess((Sender as TsButton).Tag);
  except
    on E:Exception do
    begin
      MessageBox(Self.Handle, PChar(E.Message), '데이터처리 오류', MB_OK+MB_ICONERROR);
      IF DMMssql.inTrans Then DMMssql.RollbackTrans;
    end;
  end;

  ProgramControlType := ctView;
  ButtonEnable;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  EnableControl(sPanel6, False);
  EnableControl(sPanel7, False);
  EnableControl(sPanel27, False);

  IF DMMssql.inTrans Then
  begin
    Case (Sender as TsButton).Tag of
      0,1: DMMssql.CommitTrans;
      2: DMMssql.RollbackTrans;
    end;
  end;

  sPanel29.Visible := false;

  Readlist;
  qryList.Locate('MAINT_NO',DOCNO,[]);
end;

function TUI_APPLOG_NEW_FRM.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  TempStr : String;
begin
  ErrMsg := TStringList.Create;
  try
    //------------------------------------------------------------------------------
    // 공통
    //------------------------------------------------------------------------------
    AddErrMsg(ErrMsg, msk_datee, '[문서공통] 등록일자를 입력하세요');
    AddErrMsg(ErrMsg, msk_APP_DATE, '[문서공통] 신청일자를 입력하세요');
    AddErrMsg(ErrMsg, edt_MESSAGE1, '[문서공통] 신청서종류를 입력하세요');
    AddErrMsg(ErrMsg, edt_LC_G, '[문서공통] 신용장/계약서를 입력하세요');
    AddErrMsg(ErrMsg, edt_LC_NO, '[문서공통] 신용장/계약서번호를 입력하세요');
    AddErrMsg(ErrMsg, edt_BL_G, '[문서공통] 선하증권/운송장을 입력하세요');
    AddErrMsg(ErrMsg, edt_BL_NO, '[문서공통] 선하증권/운송장번호를 입력하세요');
    AddErrMsg(ErrMsg, msk_BL_DATE, '[문서공통] 선하증권 발급일자를 입력하세요');

    IF AnsiCompareText(msk_APP_DATE.Text, msk_BL_DATE.Text) < 0 Then
      ErrMsg.Add('[문서공통] 선하증권의 발급일자는 신청일자보다 이전이어야 합니다');

    AddErrMsg(ErrMsg, edt_CARRIER1, '[문서공통] 선박명/기명을 입력하세요');
    AddErrMsg(ErrMsg, edt_CARRIER2, '[문서공통] 항차/편명을 입력하세요');
    if Trim(edt_CR_NAME1.Text) = '' Then
      ErrMsg.Add('[문서공통] 선박회사명을 입력하세요');
    IF AnsiMatchText(Trim(edt_MESSAGE1.Text), ['2AZ', '2CK']) AND (Trim(edt_CR_NAME3.Text) = '') Then
      ErrMsg.Add('[문서공통] 선박회사의 세관식별번호를 입력하세요');
    AddErrMsg(ErrMsg, edt_SE_NAME1, '[문서공통] 송하인을 입력하세요');
    AddErrMsg(ErrMsg, edt_CN_NAME1, '[문서공통] 수하인을 입력하세요');
    AddErrMsg(ErrMsg, edt_B5_NAME1, '[문서공통] 수하인을 입력하세요');
    AddErrMsg(ErrMsg, msk_AR_DATE, '[문서공통] 도착예정일을 입력하세요');
    AddErrMsg(ErrMsg, edt_INV_AMTC, '[문서공통] 상업송장금액 단위를 입력하세요');
    IF curr_INV_AMT.Value = 0 Then
      ErrMsg.Add('[문서공통] 상업송장금액을 입력하세요');
    AddErrMsg(ErrMsg, edt_MS_NAME1, '[화물번호/상품명세] 신청자를 입력하세요');
    AddErrMsg(ErrMsg, edt_AX_NAME1, '[화물번호/상품명세] 명의인을 입력하세요');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

function TUI_APPLOG_NEW_FRM.DataProcess(nType : integer): String;
var
  MAINT_NO : string;
  SC : TSQLCreate;
  TempStr : String;
  nIdx : integer;
begin
  SC := TSQLCreate.Create;
  try
    Result := edt_MAINT_NO.Text;

    with SC do
    begin
      Case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO',Result);
        end;

        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO',Result);
        end;
      end;

      SQLHeader('APPLOG');

      //문서저장구분(0:임시, 1:저장)
      ADDValue('CHK2',nType);
      //등록일자
      ADDValue('DATEE',msk_Datee.Text);
      //유저ID
      ADDValue('USER_ID',edt_userno.Text);
      //메세지1
      ADDValue('MESSAGE2',CM_TEXT(com_func,[':']));
      //메세지2
      ADDValue('MESSAGE3',CM_TEXT(com_type,[':']));
      //개설신청일
      ADDValue('APP_DATE', msk_APP_DATE.Text);

      //신청서종류
      ADDValue('MESSAGE1', edt_MESSAGE1.Text);

      //선박회사명
      ADDValue('CR_NAME1', edt_CR_NAME1.Text);
      ADDValue('CR_NAME2', edt_CR_NAME2.Text);
      ADDValue('CR_NAME3', edt_CR_NAME3.Text);

      //송하인
      ADDValue('SE_NAME1', edt_SE_NAME1.Text);
      ADDValue('SE_NAME2', edt_SE_NAME2.Text);
      ADDValue('SE_NAME3', edt_SE_NAME3.Text);

      //신청자
      ADDValue('MS_NAME1', edt_MS_NAME1.Text);
      ADDValue('MS_NAME2', edt_MS_NAME2.Text);
      ADDValue('MS_NAME3', edt_MS_NAME3.Text);

      //명의인
      ADDValue('AX_NAME1', edt_AX_NAME1.Text);
      ADDValue('AX_NAME2', edt_AX_NAME2.Text);
      ADDValue('AX_NAME3', edt_AX_NAME3.Text);

      //상업송장금액
      ADDValue('INV_AMT',  curr_INV_AMT.Text, vtInteger);
      ADDValue('INV_AMTC', edt_INV_AMTC.Text);

      //신용장/계약서
      ADDValue('LC_G',  edt_LC_G.Text);
      ADDValue('LC_NO', edt_LC_NO.Text);

      //선하증권/운송장
      ADDValue('BL_G',  edt_BL_G.Text);
      ADDValue('BL_NO', edt_BL_NO.Text);

      //선기명/편명
      ADDValue('CARRIER1', edt_CARRIER1.Text);
      ADDValue('CARRIER2', edt_CARRIER2.Text);

      //도착예정일자
      ADDValue('AR_DATE', msk_AR_DATE.Text);
      //선하증권발급일자
      ADDValue('BL_DATE', msk_BL_DATE.Text);

      //선적항
      ADDValue('LOAD_LOC', edt_LOAD_LOC.Text);
      ADDValue('LOAD_TXT', edt_LOAD_TXT.Text);

      //도착항
      ADDValue('ARR_LOC', edt_ARR_LOC.Text);
      ADDValue('ARR_TXT', edt_ARR_TXT.Text);

      //화물표시및 번호
      for nIdx := 0 to 9 do
      begin
        ADDValue('SPMARK'+IntToStr(nIdx+1), memo_SPMARK.Lines.Strings[nIdx]);
      end;

      //포장수
      ADDValue('PAC_QTYC', edt_PAC_QTYC.Text);
      ADDValue('PAC_QTY', curr_PAC_QTY.Text, vtInteger);

      //상품명세
      for nIdx := 0 to 9 do
      begin
        ADDValue('GOODS'+IntToStr(nIdx+1), memo_GOODS.Lines.Strings[nIdx]);      
      end;

      //결제구분 및 기간
      ADDValue('TRM_PAYC', edt_TRM_PAYC.Text);
      ADDValue('TRM_PAY', edt_TRM_PAY.Text);

      //발급은행
      ADDValue('BANK_CD',  edt_BANK_CD.Text);
      ADDValue('BANK_TXT', edt_BANK_TXT.Text);
      ADDValue('BANK_BR',  edt_BANK_BR.Text);

      //수하인
      ADDValue('CN_NAME1', edt_CN_NAME1.Text);
      ADDValue('CN_NAME2', edt_CN_NAME2.Text);
      ADDValue('CN_NAME3', edt_CN_NAME3.Text);

      //인수예정자
      ADDValue('B5_NAME1', edt_B5_NAME1.Text);
      ADDValue('B5_NAME2', edt_B5_NAME2.Text);
      ADDValue('B5_NAME3', edt_B5_NAME3.Text);

    end;
    ExecSQL(SC.CreateSQL);

  finally
    SC.Free;
  end;
end;

procedure TUI_APPLOG_NEW_FRM.btnDelClick(Sender: TObject);
var
  maint_no : String;
  nCursor : integer;
begin
  IF AnsiMatchText( qryListCHK2.AsString , ['4', '7', '9'] ) Then
  begin
    MessageBox(Self.Handle, MSG_SYSTEM_NOT_DEL_BECAUSE_SEND, '삭제불가', MB_OK+MB_ICONINFORMATION);
    Exit;
  end;
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  IF not DMMssql.inTrans Then
    DMMssql.KISConnect.BeginTrans;
  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
    try
      try
        IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+'수입화물 선취보증(인도승락)신청서'#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;

          SQL.Text :=  'DELETE FROM APPLOG WHERE MAINT_NO =' + QuotedStr(maint_no);
          ExecSQL;

          //트랜잭션 커밋
          DMMssql.CommitTrans;

          qryList.Close;
          qryList.Open;

          IF qryList.RecordCount > 1 Then
            qryList.MoveBy(nCursor-1);
            
        end
        else
        begin
          if DMMssql.inTrans then DMMssql.RollbackTrans;
        end;
      except
        on E:Exception do
        begin
          DMMssql.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
     Close;
     Free;
    end;
  end; 
end;

procedure TUI_APPLOG_NEW_FRM.btnCopyClick(Sender: TObject);
begin
  inherited;
  IF MessageBox(Self.Handle, '선택한 문서를 복사하시겠습니까?', '문서복사 확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  btnNewClick(btnCopy);  
end;

procedure TUI_APPLOG_NEW_FRM.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  APPLOG_PRINT_frm := TAPPLOG_PRINT_frm.Create(Self);
  Preview_frm := TPreview_frm.Create(Self);
  try
    APPLOG_PRINT_frm.MaintNo := qryListMAINT_NO.AsString;
    Preview_frm.Report := APPLOG_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(APPLOG_PRINT_frm);
  end;
end;

procedure TUI_APPLOG_NEW_FRM.btnReadyClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_APPLOG_NEW_FRM.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
  sKEY : String;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := APPLOG(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'APPLOG';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APPLOG';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        sKEY := qryListMAINT_NO.AsString;
        qryList.Close;
        qryList.Open;
        qryList.Locate('MAINT_NO', sKEY, []);

      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_APPLOG_NEW_FRM.FormActivate(Sender: TObject);
var
  KeyValue : String;
begin
  inherited;
  if not qryList.Active then Exit;

  if not DMMssql.inTrans then
  begin
    KeyValue := qryListMAINT_NO.AsString;
    qryList.close;
    qryList.Open;
    qryList.Locate('MAINT_NO',KeyValue,[]);
  end;
end;

procedure TUI_APPLOG_NEW_FRM.btnSendClick(Sender: TObject);
begin
  inherited;  
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;

end;

end.
