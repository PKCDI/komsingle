unit UI_PCRLIC_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, ExtCtrls, StdCtrls, sMemo, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl, Buttons, sBitBtn,
  Grids, DBGrids, acDBGrid, QuickRpt, QRCtrls, sComboBox, Mask, sMaskEdit,
  sEdit, sCheckBox, sButton, sLabel, sSpeedButton, sPanel, sSkinProvider,
  DB, ADODB, DateUtils, StrUtils, sToolEdit ,ComObj;

type
  TUI_PCRLIC_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sSpeedButton8: TsSpeedButton;
    sLabel5: TsLabel;
    sSpeedButton3: TsSpeedButton;
    btnExit: TsButton;
    Btn_Del: TsButton;
    Btn_Print: TsButton;
    sPanel6: TsPanel;
    edt_MAINT_NO_Header: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    comBank: TsComboBox;
    QRShape1: TQRShape;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sSpeedButton10: TsSpeedButton;
    sLabel3: TsLabel;
    edt_APP_NAME1: TsEdit;
    edt_APP_ADDR1: TsEdit;
    edt_APP_ADDR2: TsEdit;
    edt_APP_ADDR3: TsEdit;
    edt_AX_NAME2: TsEdit;
    sPanel11: TsPanel;
    sPanel8: TsPanel;
    edt_APP_NAME2: TsEdit;
    edt_AX_NAME1: TsEdit;
    edt_AX_NAME3: TsEdit;
    Edt_ConfirmAPP_DATE: TsMaskEdit;
    edt_N4025: TsEdit;
    edt_N4025_NM: TsEdit;
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    Edt_DocumentsSection: TsEdit;
    Edt_DocumentsSection_NM: TsEdit;
    Edt_BuyConfirmDocuments: TsEdit;
    Edt_SupplySaupNo: TsEdit;
    Edt_SupplyName: TsEdit;
    Edt_SupplyCEO: TsEdit;
    Edt_SupplyEID: TsEdit;
    Edt_SupplyAddr1: TsEdit;
    Edt_SupplyAddr2: TsEdit;
    edt_CHG_G: TsEdit;
    edt_CHG_G_NM: TsEdit;
    curr_C2_AMT: TsCurrencyEdit;
    sPanel9: TsPanel;
    curr_C1_AMT: TsCurrencyEdit;
    edt_C1_C: TsEdit;
    sPanel10: TsPanel;
    edt_BK_CD: TsEdit;
    edt_BK_NAME1: TsEdit;
    edt_BK_NAME2: TsEdit;
    sButton23: TsButton;
    sPanel31: TsPanel;
    sPanel16: TsPanel;
    edt_TQTYC: TsEdit;
    curr_TQTY: TsCurrencyEdit;
    curr_TAMT2: TsCurrencyEdit;
    edt_TAMT1C: TsEdit;
    curr_TAMT1: TsCurrencyEdit;
    sTabSheetDetail: TsTabSheet;
    sPanel27: TsPanel;
    sDBGrid2: TsDBGrid;
    sPanel12: TsPanel;
    sSpeedButton13: TsSpeedButton;
    sPanel14: TsPanel;
    msk_HS_NO: TsMaskEdit;
    msk_APP_DATE: TsMaskEdit;
    memo_NAME1: TsMemo;
    memo_Size: TsMemo;
    memo_Remark: TsMemo;
    edt_NAMESS: TsEdit;
    sPanel15: TsPanel;
    edt_QTYC: TsEdit;
    curr_QTY: TsCurrencyEdit;
    curr_PRI2: TsCurrencyEdit;
    edt_PRI_BASEC: TsEdit;
    curr_PRI_BASE: TsCurrencyEdit;
    curr_PRI1: TsCurrencyEdit;
    edt_AMT1C: TsEdit;
    curr_AMT1: TsCurrencyEdit;
    curr_AMT2: TsCurrencyEdit;
    edt_ACHG_G: TsEdit;
    curr_AC1_AMT: TsCurrencyEdit;
    curr_AC2_AMT: TsCurrencyEdit;
    sPanel33: TsPanel;
    sEdit2: TsEdit;
    edt_AC1_C: TsEdit;
    edt_COMPAY_CD: TsEdit;
    sPanel13: TsPanel;
    sTabSheetTax: TsTabSheet;
    sPanel24: TsPanel;
    sDBGrid4: TsDBGrid;
    sPanel26: TsPanel;
    Shape1: TShape;
    edt_BILL_NO: TsEdit;
    msk_BILL_DATE: TsMaskEdit;
    edt_ITEM_NAME1: TsEdit;
    edt_ITEM_NAME2: TsEdit;
    edt_ITEM_NAME3: TsEdit;
    edt_ITEM_NAME4: TsEdit;
    edt_ITEM_NAME5: TsEdit;
    memo_DESC: TsMemo;
    edt_BILL_AMOUNT_UNIT: TsEdit;
    curr_BILL_AMOUNT: TsCurrencyEdit;
    edt_TAX_AMOUNT_UNIT: TsEdit;
    curr_TAX_AMOUNT: TsCurrencyEdit;
    edt_QUANTITY_UNIT: TsEdit;
    curr_QUANTITY: TsCurrencyEdit;
    sPanel28: TsPanel;
    sPanel32: TsPanel;
    sTabSheet5: TsTabSheet;
    sPanel30: TsPanel;
    sSpeedButton31: TsSpeedButton;
    sMaskEdit8: TsMaskEdit;
    sMaskEdit9: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit47: TsEdit;
    sDBGrid5: TsDBGrid;
    sSpeedButton2: TsSpeedButton;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListAPP_ADDR1: TStringField;
    qryListAPP_ADDR2: TStringField;
    qryListAPP_ADDR3: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListSUP_CODE: TStringField;
    qryListSUP_NAME1: TStringField;
    qryListSUP_NAME2: TStringField;
    qryListSUP_NAME3: TStringField;
    qryListSUP_ADDR1: TStringField;
    qryListSUP_ADDR2: TStringField;
    qryListSUP_ADDR3: TStringField;
    qryListITM_G1: TStringField;
    qryListITM_G2: TStringField;
    qryListITM_G3: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYC: TStringField;
    qryListTAMT1: TBCDField;
    qryListTAMT1C: TStringField;
    qryListTAMT2: TBCDField;
    qryListTAMT2C: TStringField;
    qryListCHG_G: TStringField;
    qryListC1_AMT: TBCDField;
    qryListC1_C: TStringField;
    qryListC2_AMT: TBCDField;
    qryListLIC_INFO: TMemoField;
    qryListLIC_NO: TStringField;
    qryListBK_CD: TStringField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListBK_NAME3: TStringField;
    qryListBK_NAME4: TStringField;
    qryListBK_NAME5: TStringField;
    qryListAC1_AMT: TBCDField;
    qryListAC1_C: TStringField;
    qryListAC2_AMT: TBCDField;
    qryListComf_dat: TStringField;
    qryListPRNO: TIntegerField;
    qryListPCrLic_No: TStringField;
    qryListChgCd: TStringField;
    qryListBankSend_No: TStringField;
    qryListAPP_DATE: TStringField;
    qryListDOC_G: TStringField;
    qryListDOC_D: TStringField;
    qryListBHS_NO: TStringField;
    qryListBNAME: TStringField;
    qryListBNAME1: TMemoField;
    qryListBAMT: TBCDField;
    qryListBAMTC: TStringField;
    qryListEXP_DATE: TStringField;
    qryListSHIP_DATE: TStringField;
    qryListBSIZE1: TMemoField;
    qryListBAL_CD: TStringField;
    qryListBAL_NAME1: TStringField;
    qryListBAL_NAME2: TStringField;
    qryListF_INTERFACE: TStringField;
    qryListITM_GBN: TStringField;
    qryListITM_GNAME: TStringField;
    qryListISSUE_GBN: TStringField;
    qryListDOC_GBN: TStringField;
    qryListDOC_NO: TStringField;
    qryListBAMT2: TBCDField;
    qryListBAMTC2: TStringField;
    qryListBAMT3: TBCDField;
    qryListBAMTC3: TStringField;
    qryListPCRLIC_ISS_NO: TStringField;
    qryListBK_CD2: TStringField;
    qryListBAL_CD2: TStringField;
    qryListEMAIL_ID: TStringField;
    qryListEMAIL_DOMAIN: TStringField;
    dsList: TDataSource;
    edt_BankSend_No: TsEdit;
    sPanel22: TsPanel;
    sMemo1: TsMemo;
    qryListITM_GBN_NM: TStringField;
    qryListChgCd_NM: TStringField;
    qryListCHG_G_NM: TStringField;
    qryDetail: TADOQuery;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailREMARK: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTYC: TStringField;
    qryDetailAMT1: TBCDField;
    qryDetailAMT1C: TStringField;
    qryDetailAMT2: TBCDField;
    qryDetailPRI1: TBCDField;
    qryDetailPRI2: TBCDField;
    qryDetailPRI_BASE: TBCDField;
    qryDetailPRI_BASEC: TStringField;
    qryDetailACHG_G: TStringField;
    qryDetailAC1_AMT: TBCDField;
    qryDetailAC1_C: TStringField;
    qryDetailAC2_AMT: TBCDField;
    qryDetailLOC_TYPE: TStringField;
    qryDetailNAMESS: TStringField;
    qryDetailSIZESS: TStringField;
    qryDetailAPP_DATE: TStringField;
    qryDetailITM_NUM: TStringField;
    dsDetail: TDataSource;
    qryDetailCOMPANY_CD: TStringField;
    qryTax: TADOQuery;
    qryTaxKEYY: TStringField;
    qryTaxDATEE: TStringField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxITEM_NAME1: TStringField;
    qryTaxITEM_NAME2: TStringField;
    qryTaxITEM_NAME3: TStringField;
    qryTaxITEM_NAME4: TStringField;
    qryTaxITEM_NAME5: TStringField;
    qryTaxITEM_DESC: TMemoField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    qryTaxQUANTITY: TBCDField;
    qryTaxQUANTITY_UNIT: TStringField;
    qryTaxSEQ: TIntegerField;
    dsTax: TDataSource;
    QRCompositeReport1: TQRCompositeReport;
    pan_Excel: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel4: TsLabel;
    sDateEdit1: TsDateEdit;
    sDateEdit2: TsDateEdit;
    sButton2: TsButton;
    sButton3: TsButton;
    qryMst: TADOQuery;
    qryMstMAINT_NO: TStringField;
    qryMstAPP_NAME1: TStringField;
    qryMstAPP_ADDR: TStringField;
    qryMstAPP_NAME2: TStringField;
    qryMstAPP_SAUP_NO: TStringField;
    qryMstConfirmDate: TStringField;
    qryMstSUP_NAME1: TStringField;
    qryMstSUP_NAME2: TStringField;
    qryMstSUP_ADDR: TStringField;
    qryMstSUP_SAUP_NO: TStringField;
    qryMstC1_AMT: TBCDField;
    qryMstC1_C: TStringField;
    qryMstC2_AMT: TBCDField;
    qryMstITM_GBN: TStringField;
    qryMstBANKNAME: TStringField;
    qryMstBANKSIGN: TStringField;
    qryMstLIC_INFO: TMemoField;
    qryMstTQTY: TBCDField;
    qryMstTQTYC: TStringField;
    qryMstTAMT1: TBCDField;
    qryMstTAMT1C: TStringField;
    qryMstTAMT2: TBCDField;
    qryMstLIC_NO: TStringField;
    qryMstSEQ: TIntegerField;
    qryMstHS_NO: TStringField;
    qryMstSupplyDate: TStringField;
    qryMstProductName: TMemoField;
    qryMstSIZE: TStringField;
    qryMstQTY: TBCDField;
    qryMstQTYC: TStringField;
    qryMstPRI2: TBCDField;
    qryMstPRI1: TBCDField;
    qryMstAMT1: TBCDField;
    qryMstAMT1C: TStringField;
    qryMstACHG_G: TStringField;
    qryMstAC2_AMT: TBCDField;
    qryMstAC1_AMT: TBCDField;
    qryMstAC1_C: TStringField;
    qryMstAPP_SIGN: TStringField;
    qryMstAMT2: TBCDField;
    qryTaxDetail: TADOQuery;
    qryTotMst: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sDBGrid1TitleClick(Column: TColumn);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryDetailNAME1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryDetailAfterScroll(DataSet: TDataSet);
    procedure qryDetailAfterOpen(DataSet: TDataSet);
    procedure qryTaxAfterScroll(DataSet: TDataSet);
    procedure qryTaxAfterOpen(DataSet: TDataSet);
    procedure sDBGrid2TitleClick(Column: TColumn);
    procedure sDBGrid4TitleClick(Column: TColumn);
    procedure Btn_DelClick(Sender: TObject);
    procedure Btn_PrintClick(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure sDBGrid5TitleClick(Column: TColumn);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure edt_SearchNoChange(Sender: TObject);
    procedure sEdit47Change(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure qryTotMstAfterScroll(DataSet: TDataSet);
  private
    FSQL : String;
    FDetailSQL : String;
    FTaxSQL : String;
    
    function ReadListOnly(sortField: String=''): integer;
    function ReadDetail(sortField : String = ''):Integer;
    function ReadTax(sortField : String= ''):Integer;

    procedure setData;
    procedure setDataDetail;
    procedure setTax;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_PCRLIC_NEW_frm: TUI_PCRLIC_NEW_frm;

implementation

uses
  MSSQL, SQLCreator, MessageDefine, PCRLIC_PRINT, PCRLIC_PRINT2, Preview, PCRLIC_EXCEL;

{$R *.dfm}

{ TUI_PCRLIC_NEW_frm }

function TUI_PCRLIC_NEW_frm.ReadListOnly(sortField: String): integer;
begin
  ClearGridTitleCaption(sDBGrid1);
  ClearGridTitleCaption(sDBGrid5);
  
  with qryList do
  Begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE between '+QuotedStr(sMaskEdit1.Text)+' and '+QuotedStr(sMaskEdit2.Text));

    IF Length(Trim(edt_SearchNo.Text)) > 0 Then
      SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+Trim(edt_SearchNo.Text)+'%'));

    IF sortField = '' Then
      SQL.Add('ORDER BY DATEE DESC, MAINT_NO DESC')
    else
    begin
      if (qryList.Filter = '') OR (qryList.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryList.Filter := sortField;
        Tag := 0;
      end
      else
      if qryList.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;

      if sortField <> 'MAINT_NO' then
        SQL.Add(', MAINT_NO DESC');
    end;

    Open;
  end;

  result := qryList.Tag;
end;

procedure TUI_PCRLIC_NEW_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qrylist.SQl.Text;
  FDetailSQL := qryDetail.SQL.Text;
  FTaxSQL := qryTax.SQL.Text;

  ReadOnlyControl(sPanel6);
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel12);
  ReadOnlyControl(sPanel26);  
end;

procedure TUI_PCRLIC_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_PCRLIC_NEW_frm := nil;
end;

procedure TUI_PCRLIC_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_PCRLIC_NEW_frm.FormShow(Sender: TObject);
begin
//  inherited;
  sPageControl1.ActivePageIndex := 0;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', IncMonth(StartOfTheMonth( Now ),-1));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', Now);
  sMaskEdit8.Text := sMaskEdit1.Text;
  sMaskEdit9.Text := sMaskEdit2.Text;

  RefreshButton(sPanel7);

  ReadListOnly;      
  ReadOnlyControl(spanel7,false);   
  ReadOnlyControl(spanel27,false);
  ReadOnlyControl(spanel26,false);
end;

procedure TUI_PCRLIC_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  qryList.Filter := '';
  ReadListOnly;
end;

procedure TUI_PCRLIC_NEW_frm.sDBGrid1TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  nRst := ReadListOnly(Column.FieldName);
  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

procedure TUI_PCRLIC_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setData;
  qryDetail.Filter := '';
  ReadDetail;
  qryTax.Filter := '';
  ReadTax;
end;

procedure TUI_PCRLIC_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);

  Btn_Del.Enabled := qryList.RecordCount > 0;
  Btn_Print.Enabled := Btn_Del.Enabled;
end;

function TUI_PCRLIC_NEW_frm.ReadDetail(sortField: String): Integer;
begin
  ClearGridTitleCaption(sDBGrid2);
  with qryDetail do
  begin
    Close;
    SQL.Text := FDetailSQL;
    Parameters[0].Value := qryListMAINT_NO.AsString;

    IF sortField = '' Then
      SQL.Add('ORDER BY SEQ')
    else
    begin
      if (qryDetail.Filter = '') OR (qryDetail.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryDetail.Filter := sortField;
        Tag := 0;
      end
      else
      if qryDetail.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;
    end;

    Open;
  end;

  result := qryDetail.Tag;
end;

function TUI_PCRLIC_NEW_frm.ReadTax(sortField: String): Integer;
begin
  ClearGridTitleCaption(sDBGrid4);
  with qryTax do
  begin
    Close;
    SQL.Text := FTaxSQL;
    Parameters[0].Value := qryListMAINT_NO.AsString;

    IF sortField = '' Then
      SQL.Add('ORDER BY SEQ')
    else
    begin
      if (qryTax.Filter = '') OR (qryTax.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryTax.Filter := sortField;
        Tag := 0;
      end
      else
      if qryTax.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;
    end;

    Open;
  end;

  result := qryTax.Tag;
end;

procedure TUI_PCRLIC_NEW_frm.setData;
begin
  //------------------------------------------------------------------------------
  // 공통사항
  //------------------------------------------------------------------------------
  edt_MAINT_NO_Header.Text := qryListMAINT_NO.AsString;
  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;

  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString, 2);

  edt_AX_NAME2.Text := qryListAX_NAME2.AsString;
  edt_APP_NAME1.Text := qryListAPP_NAME1.AsString;
  edt_APP_NAME2.Text := qryListAPP_NAME2.AsString;
  edt_APP_ADDR1.Text := qryListAPP_ADDR1.AsString;
  edt_APP_ADDR2.Text := qryListAPP_ADDR2.AsString;
  edt_APP_ADDR3.Text := qryListAPP_ADDR3.AsString;
  edt_AX_NAME1.Text := qryListAX_NAME1.AsString;
  edt_AX_NAME3.Text := qryListAX_NAME3.AsString;
  Edt_ConfirmAPP_DATE.Text := qryListAPP_DATE.AsString;

  edt_N4025.Text := qryListITM_GBN.AsString;
  edt_N4025_NM.Text := qryListITM_GBN_NM.AsString;

  Edt_DocumentsSection.Text    := qryListChgCd.AsString;
  Edt_DocumentsSection_NM.Text := qryListChgCd_NM.AsString;
  Edt_BuyConfirmDocuments.Text := qryListPCrLic_No.AsString;
  edt_BankSend_No.Text := qryListBankSend_No.AsString;

  Edt_SupplySaupNo.Text := qryListSUP_ADDR3.AsString;
  Edt_SupplyName.Text   := qryListSUP_NAME1.AsString;
  Edt_SupplyCEO.Text    := qryListSUP_NAME2.AsString;
  Edt_SupplyEID.Text    := qryListEMAIL_ID.AsString+'@'+qryListEMAIL_DOMAIN.AsString;
  IF Edt_SupplyEID.Text = '@' Then Edt_SupplyEID.Clear;
  Edt_SupplyAddr1.Text  := qryListSUP_ADDR1.AsString;
  Edt_SupplyAddr2.Text  := qryListSUP_ADDR2.AsString;

  edt_CHG_G.Text := qryListCHG_G.AsString;
  edt_CHG_G_NM.Text := qryListCHG_G_NM.AsString;
  curr_C2_AMT.Value := qryListC2_AMT.AsFloat;
  curr_C1_AMT.Value := qryListC1_AMT.AsFloat;
  edt_C1_C.Text := qryListC1_C.AsString;

  edt_TQTYC.Text  := qryListTQTYC.AsString;
  curr_TQTY.Value := qryListTQTY.AsFloat;
  curr_TAMT2.Value := qryListTAMT2.AsFloat;
  edt_TAMT1C.Text := qryListTAMT1C.AsString;
  curr_TAMT1.Value := qryListTAMT1.AsFloat;

  edt_BK_CD.Text := qryListBK_CD.AsString;
  edt_BK_NAME1.Text := qryListBK_NAME1.AsString;
  edt_BK_NAME2.Text := qryListBK_NAME2.AsString;

  sMemo1.Text := qrylistLIC_INFO.AsString;
end;

procedure TUI_PCRLIC_NEW_frm.qryDetailNAME1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  Text := AnsiReplaceText(Sender.AsString,#13#10,' ');
end;

procedure TUI_PCRLIC_NEW_frm.qryDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setDataDetail;
end;

procedure TUI_PCRLIC_NEW_frm.setDataDetail;
begin
  msk_HS_NO.Text    := qryDetailHS_NO.AsString;
  msk_APP_DATE.Text := qryDetailAPP_DATE.AsString;
  edt_NAMESS.Text   := qryDetailNAMESS.AsString;

  memo_NAME1.Text  := qryDetailNAME1.AsString;
  memo_Size.Text   := qryDetailSIZE1.AsString;
  memo_Remark.Text := qryDetailREMARK.AsString;

  edt_COMPAY_CD.Text := qryDetailCOMPANY_CD.AsString;

  edt_QTYC.Text  := qryDetailQTYC.AsString;
  curr_QTY.Value := qryDetailQTY.AsFloat;
  curr_PRI2.Value := qryDetailPRI2.AsFloat;
  edt_PRI_BASEC.Text := qryDetailPRI_BASEC.AsString;
  curr_PRI_BASE.Value := qryDetailPRI_BASE.AsFloat;
  curr_PRI1.Value := qryDetailPRI1.AsFloat;
  edt_AMT1C.Text := qryDetailAMT1C.AsString;
  curr_AMT1.Value := qryDetailAMT1.AsFloat;
  curr_AMT2.Value := qryDetailAMT2.AsFloat;

  edt_ACHG_G.Text := qryDetailACHG_G.AsString;
  curr_AC1_AMT.Value := qryDetailAC1_AMT.AsFloat;
  edt_AC1_C.Text := qryDetailAC1_C.AsString;
  curr_AC2_AMT.Value := qryDetailAC2_AMT.AsFloat;
end;

procedure TUI_PCRLIC_NEW_frm.qryDetailAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryDetailAfterScroll(Dataset);
end;

procedure TUI_PCRLIC_NEW_frm.setTax;
begin
  edt_BILL_NO.Text := qryTaxBILL_NO.AsString;
  msk_BILL_DATE.Text := qryTaxBILL_DATE.AsString;
  edt_ITEM_NAME1.Text := qryTaxITEM_NAME1.AsString;
  edt_ITEM_NAME2.Text := qryTaxITEM_NAME2.AsString;
  edt_ITEM_NAME3.Text := qryTaxITEM_NAME3.AsString;
  edt_ITEM_NAME4.Text := qryTaxITEM_NAME4.AsString;
  edt_ITEM_NAME5.Text := qryTaxITEM_NAME5.AsString;
  memo_DESC.Text := qryTaxITEM_DESC.AsString;
  edt_BILL_AMOUNT_UNIT.Text := qryTaxBILL_AMOUNT_UNIT.AsString;
  curr_BILL_AMOUNT.Value := qryTaxBILL_AMOUNT.AsFloat;
  edt_TAX_AMOUNT_UNIT.Text := qryTaxTAX_AMOUNT_UNIT.AsString;
  curr_TAX_AMOUNT.Value := qryTaxTAX_AMOUNT.AsFloat;
  edt_QUANTITY_UNIT.Text := qryTaxQUANTITY_UNIT.AsString;
  curr_QUANTITY.Value := qryTaxQUANTITY.AsFloat;
end;

procedure TUI_PCRLIC_NEW_frm.qryTaxAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setTax;
end;

procedure TUI_PCRLIC_NEW_frm.qryTaxAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryTaxAfterScroll(Dataset);
end;

procedure TUI_PCRLIC_NEW_frm.sDBGrid2TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  If Column.Field.DataType = ftMemo Then
  begin
    MessageBox(Self.Handle, 'Memo필드는 정렬할 수 없습니다', '정렬오류', MB_OK+MB_ICONQUESTION);
    Exit;
  end;

  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  nRst := ReadDetail(Column.FieldName);
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

procedure TUI_PCRLIC_NEW_frm.sDBGrid4TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  If Column.Field.DataType = ftMemo Then
  begin
    MessageBox(Self.Handle, 'Memo필드는 정렬할 수 없습니다', '정렬오류', MB_OK+MB_ICONQUESTION);
    Exit;
  end;

  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  nRst := ReadTax(Column.FieldName);
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

procedure TUI_PCRLIC_NEW_frm.Btn_DelClick(Sender: TObject);
var
  maint_no : String;
  nIdx : integer;
  sc : TSQLCreate;
begin
  if qryList.RecordCount = 0 then Exit;
  IF MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.BeginTrans;
  maint_no := qryListMAINT_NO.AsString;
  nIdx := qryList.RecNo;

  sc := TSQLCreate.Create;
  try
    try
      sc.DMLType := dmlDelete;
      sc.SQLHeader('PCRLIC_H');
      sc.ADDWhere('MAINT_NO', maint_no);
      ExecSQL(sc.CreateSQL);

      sc.DMLType := dmlDelete;
      sc.SQLHeader('PCRLICD1');
      sc.ADDWhere('KEYY', maint_no);
      ExecSQL(sc.CreateSQL);

      sc.DMLType := dmlDelete;
      sc.SQLHeader('PCRLICD2');
      sc.ADDWhere('KEYY', maint_no);
      ExecSQL(sc.CreateSQL);

      sc.DMLType := dmlDelete;
      sc.SQLHeader('PCRLIC_TAX');
      sc.ADDWhere('KEYY', maint_no);
      ExecSQL(sc.CreateSQL);

      DMMssql.CommitTrans;

      qryList.Close;
      qryList.Open;

      if nIdx > 1 Then
        qryList.MoveBy(nIdx-1);

      MessageBox(Self.Handle, MSG_SYSTEM_DEL_OK, '삭제완료', MB_OK+MB_ICONINFORMATION);
    except
      on E:Exception do
      begin
        DMMssql.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);        
      end;
    end;
  finally
    sc.Free;
  end;
end;

procedure TUI_PCRLIC_NEW_frm.Btn_PrintClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  PCRLIC_PRINT_FRM := TPCRLIC_PRINT_FRM.Create(Self);
  PCRLIC_PRINT2_FRM := TPCRLIC_PRINT2_FRM.Create(Self);

  Preview_frm := TPreview_frm.Create(Self);
  try
    PCRLIC_PRINT_FRM.DocNo := qryListMAINT_NO.AsString;
    PCRLIC_PRINT2_FRM.DocNo := qryListMAINT_NO.AsString;
    QRCompositeReport1.Prepare;
    PCRLIC_PRINT2_FRM.Totalpage := PCRLIC_PRINT2_FRM.QRPrinter.PageCount;
    Preview_frm.CompositeRepost := QRCompositeReport1;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(PCRLIC_PRINT_FRM);
    FreeAndNil(PCRLIC_PRINT2_FRM);
  end;

end;

procedure TUI_PCRLIC_NEW_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(PCRLIC_PRINT_FRM);
  QRCompositeReport1.Reports.Add(PCRLIC_PRINT2_FRM);
end;

procedure TUI_PCRLIC_NEW_frm.sSpeedButton2Click(Sender: TObject);
begin
  inherited;
  sDateEdit1.Date := StartOfTheMonth(Now);
  sDateEdit2.Date := Now;
  pan_Excel.Left := 464;
  pan_Excel.Top := 40;
  pan_Excel.Visible := not pan_Excel.Visible;
end;

procedure TUI_PCRLIC_NEW_frm.sButton3Click(Sender: TObject);
begin
  inherited;
  pan_Excel.Visible := False;
end;

procedure TUI_PCRLIC_NEW_frm.sButton2Click(Sender: TObject);
var
  sMaint_No:String;
  i,idx: Integer;
  Excel: OleVariant;
  WorkBook: OleVariant;
  WorkSheet: OleVariant;
begin
  inherited;

  Excel := CreateOleObject('Excel.Application');
  Excel.Visible := True;

  // 워크북 추가
  Excel.WorkBooks.Add;
  WorkBook := Excel.ActiveWorkBook;

  // 워크시트 추가
  Workbook.sheets.add;
  try
    // 작업할  워크시트 선택
    WorkSheet := WorkBook.WorkSheets[1];

    //타이틀
    WorkSheet.Cells[1,1].Value := '구매확인서 응답서(PCRLIC)';
//    WorkSheet.Columns.Columns[1].너비 조절하기.
    //https://www.swissdelphicenter.ch/en/showcode.php?id=156
    WorkSheet.Cells[1,3].Value := '기간 : '+sDateEdit1.Text+' ~ '+sDateEdit2.Text;
    //필드
    WorkSheet.Cells[3,1].Value := '관리번호';

    WorkSheet.Cells[2,2].Value := '신청인';
    WorkSheet.Cells[3,2].Value := '상호';
    WorkSheet.Cells[3,3].Value := '주소';
    WorkSheet.Cells[3,4].Value := '명의인';
    WorkSheet.Cells[3,5].Value := '사업자등록번호';
    WorkSheet.Cells[3,6].Value := '전자서명';
    WorkSheet.Cells[3,7].Value := '확인일자';

    WorkSheet.Cells[2,8].Value := '공급자';
    WorkSheet.Cells[3,8].Value := '상호';
    WorkSheet.Cells[3,9].Value := '대표';
    WorkSheet.Cells[3,10].Value := '주소';
    WorkSheet.Cells[3,11].Value := '사업자번호';

    WorkSheet.Cells[2,12].Value := '공통';
    WorkSheet.Cells[3,12].Value := '변동금액';
    WorkSheet.Cells[3,13].Value := '단위';
    WorkSheet.Cells[3,14].Value := '변동금액(USD)';
    WorkSheet.Cells[3,15].Value := '공급물품명세구분';
    WorkSheet.Cells[3,16].Value := '확인기관';
    WorkSheet.Cells[3,17].Value := '전자서명';
    WorkSheet.Cells[3,18].Value := '기타사항';
    WorkSheet.Cells[3,19].Value := '총수량';
    WorkSheet.Cells[3,20].Value := '단위';
    WorkSheet.Cells[3,21].Value := '총금액';
    WorkSheet.Cells[3,22].Value := '단위';
    WorkSheet.Cells[3,23].Value := '총금액(USD)';
    WorkSheet.Cells[3,24].Value := '구매확인서번호';

    WorkSheet.Cells[2,25].Value := '구매원료 - 기재의 내용';
    WorkSheet.Cells[3,25].Value := '순번';
    WorkSheet.Cells[3,26].Value := 'HS부호';
    WorkSheet.Cells[3,27].Value := '공급일';
    WorkSheet.Cells[3,28].Value := '품명';
    WorkSheet.Cells[3,29].Value := '규격';
    WorkSheet.Cells[3,30].Value := '수량';
    WorkSheet.Cells[3,31].Value := '단위';
    WorkSheet.Cells[3,32].Value := '단가';
    WorkSheet.Cells[3,33].Value := '단가(USD)';
    WorkSheet.Cells[3,34].Value := '금액';
    WorkSheet.Cells[3,35].Value := '단위';
    WorkSheet.Cells[3,36].Value := '금액(USD)';
    WorkSheet.Cells[3,37].Value := '변동구분';
    WorkSheet.Cells[3,38].Value := '변동금액(USD)';
    WorkSheet.Cells[3,39].Value := '변동금액';
    WorkSheet.Cells[3,40].Value := '단위';


    WorkSheet.Cells[2,41].Value := '세금계산서';
    WorkSheet.Cells[3,41].Value := '순번';
    WorkSheet.Cells[3,42].Value := '작성일자';
    WorkSheet.Cells[3,43].Value := '세금계산서번호';
    WorkSheet.Cells[3,44].Value := '수량';
    WorkSheet.Cells[3,45].Value := '단위';
    WorkSheet.Cells[3,46].Value := '공급가액';
    WorkSheet.Cells[3,47].Value := '단위';
    WorkSheet.Cells[3,48].Value := '세액';
    WorkSheet.Cells[3,49].Value := '단위';
    
      qryTotMst.Close;
      qryTotMst.Parameters[0].Value := FormatDateTime('YYYYMMDD', sDateEdit1.Date);
      qryTotMst.Parameters[1].Value := FormatDateTime('YYYYMMDD', sDateEdit2.Date);
      qryTotMst.Open;
      qryTotMst.First;

      i := 4;
      sMaint_No := '';
      for idx := 1 to qryTotMst.RecordCount do
      begin
        while not qryMst.Eof do
        begin
          IF qryMst.RecNo = 1 Then
          begin
            sMaint_No := qryMstMAINT_NO.AsString;
            WorkSheet.Cells[i,1].Value := qryMstMAINT_NO.AsString;
            WorkSheet.Cells[i,2].Value := qryMstAPP_NAME1.AsString;
            WorkSheet.Cells[i,3].Value := qryMstAPP_ADDR.AsString;
            WorkSheet.Cells[i,4].Value := qryMstAPP_NAME2.AsString;
            WorkSheet.Cells[i,5].Value := qryMstAPP_SAUP_NO.AsString;
            WorkSheet.Cells[i,6].Value := qryMstAPP_SIGN.AsString;
            WorkSheet.Cells[i,7].Value := qryMstConfirmDate.AsString;
            WorkSheet.Cells[i,8].Value := qryMstSUP_NAME1.AsString;
            WorkSheet.Cells[i,9].Value := qryMstSUP_NAME2.AsString;
            WorkSheet.Cells[i,10].Value := qryMstSUP_ADDR.AsString;
            WorkSheet.Cells[i,11].Value := qryMstSUP_SAUP_NO.AsString;
            WorkSheet.Cells[i,12].Value := qryMstC1_AMT.AsString;
            WorkSheet.Cells[i,13].Value := qryMstC1_C.AsString;
            WorkSheet.Cells[i,14].Value := qryMstC2_AMT.AsString;
            WorkSheet.Cells[i,15].Value := qryMstITM_GBN.AsString;
            WorkSheet.Cells[i,16].Value := qryMstBANKNAME.AsString;
            WorkSheet.Cells[i,17].Value := qryMstBANKSIGN.AsString;
            WorkSheet.Cells[i,18].Value := qryMstLIC_INFO.AsString;
            WorkSheet.Cells[i,19].Value := qryMstTQTY.AsString;
            WorkSheet.Cells[i,20].Value := qryMstTQTYC.AsString;
            WorkSheet.Cells[i,21].Value := qryMstTAMT1.AsString;
            WorkSheet.Cells[i,22].Value := qryMstTAMT1C.AsString;
            WorkSheet.Cells[i,23].Value := qryMstTAMT2.AsString;
            WorkSheet.Cells[i,24].Value := qryMstLIC_NO.AsString;
          end;

          if (AnsiCompareText(sMaint_No,qryMstMAINT_NO.AsString)=0)then //이전 mst의 관리번호와 현재 mst의 관리번호가 같을때
          begin
            WorkSheet.Cells[i,25].Value := qryMstSEQ.AsString;
            WorkSheet.Cells[i,26].Value := qryMstHS_NO.AsString;
            WorkSheet.Cells[i,27].Value := qryMstSupplyDate.AsString;
            WorkSheet.Cells[i,28].Value := qryMstProductName.AsString;
            WorkSheet.Cells[i,29].Value := qryMstSIZE.AsString;
            IF qryMstQTYC.AsString = 'KRW' Then
              WorkSheet.Cells[i,30].Value := FormatFloat('#,0',qryMstQTY.AsCurrency)
            else
              WorkSheet.Cells[i,30].Value := FormatFloat('#,0.000',qryMstQTY.AsCurrency);

            WorkSheet.Cells[i,31].Value := qryMstQTYC.AsString;
            WorkSheet.Cells[i,32].Value := FormatFloat('#,0.000',qryMstPRI2.AsCurrency);
            WorkSheet.Cells[i,33].Value := FormatFloat('#,0.000',qryMstPRI1.AsCurrency);

            IF qryMstAMT1C.AsString = 'KRW' Then
              WorkSheet.Cells[i,34].Value := FormatFloat('#,0',qryMstAMT1.AsCurrency)
            else
              WorkSheet.Cells[i,34].Value := FormatFloat('#,0.000',qryMstAMT1.AsCurrency);

            WorkSheet.Cells[i,35].Value := qryMstAMT1C.AsString;
            WorkSheet.Cells[i,36].Value := FormatFloat('#,0.000',qryMstAMT2.AsCurrency);
            WorkSheet.Cells[i,37].Value := qryMstACHG_G.AsString;
            WorkSheet.Cells[i,38].Value := FormatFloat('#,0.000',qryMstAC2_AMT.AsCurrency);

            IF qryMstAC1_C.AsString = 'KRW' Then
              WorkSheet.Cells[i,39].Value := FormatFloat('#,0',qryMstAC1_AMT.AsCurrency)
            else
              WorkSheet.Cells[i,39].Value := FormatFloat('#,0.000',qryMstAC1_AMT.AsCurrency);

            WorkSheet.Cells[i,40].Value := qryMstAC1_C.AsString;

            inc(i);

          end;
          qryMst.Next;
        end;
        qryTaxDetail.First;
        while not qryTaxDetail.Eof do
        begin
          with qryTaxDetail do
          begin
            WorkSheet.Cells[i,41].Value := FieldByName('SEQ').AsString;
            WorkSheet.Cells[i,42].Value := FieldByName('BILL_DATE').AsString;
            WorkSheet.Cells[i,43].Value := FieldByName('BILL_NO').AsString;
            WorkSheet.Cells[i,44].Value := FormatFloat('#,0',FieldByName('QUANTITY').AsCurrency );
            WorkSheet.Cells[i,45].Value := FieldByName('QUANTITY_UNIT').AsString;

            IF FieldByName('BILL_AMOUNT_UNIT').AsString = 'KRW' Then
              WorkSheet.Cells[i,46].Value := FormatFloat('#,0',FieldByName('BILL_AMOUNT').AsCurrency )
            else
              WorkSheet.Cells[i,46].Value := FormatFloat('#,0.000',FieldByName('BILL_AMOUNT').AsCurrency);

            WorkSheet.Cells[i,47].Value := FieldByName('BILL_AMOUNT_UNIT').AsString;

            IF FieldByName('TAX_AMOUNT_UNIT').AsString = 'KRW' Then
              WorkSheet.Cells[i,48].Value := FormatFloat('#,0',FieldByName('TAX_AMOUNT').AsCurrency)
            else
              WorkSheet.Cells[i,48].Value := FormatFloat('#,0.000',FieldByName('TAX_AMOUNT').AsCurrency);

            WorkSheet.Cells[i,49].Value := FieldByName('TAX_AMOUNT_UNIT').AsString;

            inc(i);
            Next;
          end;
        end;
        qryTotMst.Next;
      end;

  finally
    // 워크북 닫기
    WorkBook.close;
    WorkBook:=unAssigned;
    WorkSheet:=unAssigned;
    // 엑셀 종료
    Excel.Quit;
    Excel:=unAssigned;
  end ;
//  PCRLIC_EXCEL_FRM := TPCRLIC_EXCEL_FRM.Create(Self);
//
//  PCRLIC_EXCEL_FRM.Run(AnsiReplaceText(sDateEdit1.Text,'-','') ,
//                       AnsiReplaceText(sDateEdit2.Text,'-',''));
//
//  pan_Excel.Visible := False;
//  PCRLIC_EXCEL_FRM.Free;

end;

procedure TUI_PCRLIC_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 3;
end;

procedure TUI_PCRLIC_NEW_frm.sDBGrid5TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  nRst := ReadListOnly(Column.FieldName);
  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;
procedure TUI_PCRLIC_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  case (Sender as TsMaskEdit).Tag of
    0: sMaskEdit8.Text := sMaskEdit1.Text;
    1: sMaskEdit9.Text := sMaskEdit2.Text;
    2: sMaskEdit1.Text := sMaskEdit8.Text;
    3: sMaskEdit2.Text := sMaskEdit9.Text;
  end;
end;

procedure TUI_PCRLIC_NEW_frm.edt_SearchNoChange(Sender: TObject);
begin
  inherited;
  sEdit47.Text := edt_SearchNo.Text;
end;

procedure TUI_PCRLIC_NEW_frm.sEdit47Change(Sender: TObject);
begin
  inherited;
  edt_SearchNo.Text := sEdit47.Text;
end;

procedure TUI_PCRLIC_NEW_frm.sBitBtn5Click(Sender: TObject);
begin
  inherited;
  qrylist.Filter := '';
  ReadListOnly;
end;

procedure TUI_PCRLIC_NEW_frm.qryTotMstAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryMst.Close;
  qryMst.Parameters[0].Value := qryTotMst.FieldByName('MAINT_NO').AsString;
  qryMst.Open;
  
  qryTaxDetail.Close;
  qryTaxDetail.Parameters[0].Value := qryTotMst.FieldByName('MAINT_NO').AsString;
  qryTaxDetail.Open;

end;

end.

