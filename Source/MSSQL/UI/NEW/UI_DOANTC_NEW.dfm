inherited UI_DOANTC_NEW_frm: TUI_DOANTC_NEW_frm
  Left = 866
  Top = 120
  Caption = #49440#51201#49436#47448' '#46020#52265#53685#48372#49436
  ClientWidth = 964
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 964
    Height = 72
    Align = alTop
    
    TabOrder = 0
    DesignSize = (
      964
      72)
    object sSpeedButton4: TsSpeedButton
      Left = 385
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sLabel7: TsLabel
      Left = 48
      Top = 14
      Width = 159
      Height = 23
      Caption = #49440#51201#49436#47448' '#46020#52265#53685#48372#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 88
      Top = 37
      Width = 79
      Height = 21
      Caption = '(DOANTC)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton7: TsSpeedButton
      Left = 1054
      Top = 27
      Width = 8
      Height = 35
      Visible = False
      ButtonStyle = tbsDivider
    end
    object sSpeedButton8: TsSpeedButton
      Left = 304
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
    end
    object sSpeedButton1: TsSpeedButton
      Left = 249
      Top = 4
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
    end
    object btnExit: TsButton
      Left = 884
      Top = 3
      Width = 72
      Height = 35
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 19
      ContentMargin = 12
    end
    object btnDel: TsButton
      Tag = 2
      Left = 253
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 26
      ContentMargin = 8
    end
    object btnPrint: TsButton
      Left = 319
      Top = 3
      Width = 65
      Height = 35
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 255
      Top = 41
      Width = 707
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 3
      object edt_MAINT_NO: TsEdit
        Left = 56
        Top = 3
        Width = 209
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = 'LOCAPP19710312'
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        BoundLabel.ParentFont = False
      end
      object msk_Datee: TsMaskEdit
        Left = 328
        Top = 3
        Width = 77
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        ReadOnly = True
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object com_func: TsComboBox
        Left = 552
        Top = 3
        Width = 39
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 3
        TabOrder = 2
        TabStop = False
        Text = '6: Confirmation'
        Items.Strings = (
          '1: Cancel'
          '2: Delete'
          '4: Change'
          '6: Confirmation'
          '7: Duplicate'
          '9: Original')
      end
      object com_type: TsComboBox
        Left = 656
        Top = 3
        Width = 47
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        VerticalAlignment = taVerticalCenter
        ReadOnly = True
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 2
        TabOrder = 3
        TabStop = False
        Text = 'NA: No acknowledgement needed'
        Items.Strings = (
          'AB: Message Acknowledgement'
          'AP: Accepted'
          'NA: No acknowledgement needed'
          'RE: Rejected')
      end
      object edt_userno: TsEdit
        Left = 456
        Top = 3
        Width = 25
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
      end
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 341
    Height = 609
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 339
      Height = 551
      TabStop = False
      Align = alClient
      Color = clGray
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 211
          Visible = True
        end
        item
          Alignment = taCenter
          Color = clWhite
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 94
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 339
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 242
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 61
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
      end
      object sMaskEdit1: TsMaskEdit
        Tag = -1
        Left = 61
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 0
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sMaskEdit2: TsMaskEdit
        Tag = -1
        Left = 154
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        
        MaxLength = 10
        TabOrder = 1
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        CheckOnExit = True
        EditMask = '9999-99-99;0'
        Text = '20180621'
      end
      object sBitBtn1: TsBitBtn
        Left = 261
        Top = 5
        Width = 66
        Height = 46
        Caption = #51312#54924
        TabOrder = 3
        TabStop = False
        OnClick = sBitBtn1Click
      end
    end
    object sPanel29: TsPanel
      Left = 232
      Top = 576
      Width = 108
      Height = 55
      
      TabOrder = 2
      Visible = False
      object sLabel1: TsLabel
        Left = 55
        Top = 257
        Width = 230
        Height = 21
        Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#44060#49444#49888#52397#49436
        ParentFont = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
      end
      object sLabel2: TsLabel
        Left = 55
        Top = 281
        Width = 126
        Height = 12
        Caption = #49888#44508#47928#49436' '#51089#49457#51473#51077#45768#45796
      end
    end
  end
  object sPanel3: TsPanel [2]
    Left = 341
    Top = 72
    Width = 623
    Height = 609
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 2
    object sPanel20: TsPanel
      Left = 472
      Top = 59
      Width = 321
      Height = 24
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 0
    end
    object sPageControl1: TsPageControl
      Left = 1
      Top = 1
      Width = 621
      Height = 607
      ActivePage = sTabSheet1
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      TabHeight = 26
      TabOrder = 1
      TabPadding = 10
      object sTabSheet1: TsTabSheet
        Caption = #47928#49436#44277#53685
        object sPanel7: TsPanel
          Left = 0
          Top = 0
          Width = 613
          Height = 571
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alClient
          
          TabOrder = 0
          object mask_RESDATE: TsMaskEdit
            Left = 139
            Top = 30
            Width = 79
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            ReadOnly = True
            TabOrder = 0
            Color = 12582911
            BoundLabel.Active = True
            BoundLabel.Caption = #53685#51648#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object edt_AdInfo2: TsEdit
            Left = 669
            Top = 525
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 3
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AdInfo5: TsEdit
            Left = 669
            Top = 591
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 6
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AdInfo3: TsEdit
            Left = 669
            Top = 547
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 4
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AdInfo4: TsEdit
            Left = 669
            Top = 569
            Width = 251
            Height = 23
            Color = clWhite
            MaxLength = 65
            TabOrder = 5
            Visible = False
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel2: TsPanel
            Left = 14
            Top = 5
            Width = 204
            Height = 23
            SkinData.CustomColor = True
            Caption = #53685#51648#51008#54665
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 7
          end
          object sPanel59: TsPanel
            Left = 1
            Top = 548
            Width = 105
            Height = 23
            SkinData.CustomColor = True
            Caption = #50868#49569#49688#45800
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 8
            Visible = False
          end
          object com_Carriage: TsComboBox
            Left = 107
            Top = 548
            Width = 237
            Height = 23
            SkinData.SkinSection = 'EDIT'
            Style = csDropDownList
            Enabled = False
            ItemHeight = 17
            ItemIndex = 1
            TabOrder = 2
            Text = 'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
            Visible = False
            Items.Strings = (
              ''
              'DQ: '#48373#54633#50868#49569'(Complex)/'#44592#53440'(ETC)'
              'DT: '#54644#49345#50868#49569'(Sea)/'#54637#44277#50868#49569'(Air)')
          end
          object sPanel89: TsPanel
            Left = 1
            Top = 572
            Width = 343
            Height = 23
            SkinData.CustomColor = True
            Caption = '[PAGE2] 44A/44B'#47484' '#51089#49457#54616#49464#50836
            Color = 16576211
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 9
            Visible = False
          end
          object edt_BANKCODE: TsEdit
            Left = 139
            Top = 54
            Width = 160
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #53685#51648#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object mask_SETDATE: TsMaskEdit
            Left = 299
            Top = 30
            Width = 79
            Height = 23
            AutoSize = False
            
            Enabled = False
            MaxLength = 10
            ReadOnly = True
            TabOrder = 10
            Color = 12582911
            BoundLabel.Active = True
            BoundLabel.Caption = #52572#51333#44208#51228#51068
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.ParentFont = False
            SkinData.CustomColor = True
            CheckOnExit = True
            EditMask = '9999-99-99;0'
            Text = '20180621'
          end
          object edt_BANK1: TsEdit
            Left = 139
            Top = 78
            Width = 460
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 11
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BANK2: TsEdit
            Left = 139
            Top = 102
            Width = 460
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Caption = 'sEdit2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel1: TsPanel
            Left = 14
            Top = 133
            Width = 204
            Height = 23
            SkinData.CustomColor = True
            Caption = #49888#50857#51109'('#44228#50557#49436') / '#49440#54616#51613#44428
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 13
          end
          object edt_LCG: TsEdit
            Left = 174
            Top = 158
            Width = 44
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 14
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49888#50857#51109'('#44228#50557#49436')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BLG: TsEdit
            Left = 174
            Top = 182
            Width = 44
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 15
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#54616#51613#44428'('#54637#44277#54868#47932#50868#49569#51109')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_LCNO: TsEdit
            Left = 219
            Top = 158
            Width = 380
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BLNO: TsEdit
            Left = 219
            Top = 182
            Width = 380
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 17
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_AMTC: TsEdit
            Left = 174
            Top = 206
            Width = 44
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #50612#51020#44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_CHRGC: TsEdit
            Left = 174
            Top = 230
            Width = 44
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 19
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44592#53440#49688#49688#47308
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object curr_AMT: TsCurrencyEdit
            Left = 219
            Top = 206
            Width = 148
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = True
            
            Enabled = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 20
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_CHRG: TsCurrencyEdit
            Left = 219
            Top = 230
            Width = 148
            Height = 23
            AutoSize = False
            CharCase = ecUpperCase
            Ctl3D = True
            
            Enabled = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 21
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object sPanel8: TsPanel
            Left = 14
            Top = 262
            Width = 165
            Height = 23
            SkinData.CustomColor = True
            Caption = #49688#49888#50629#52404'('#44144#47000#44256#44061')'
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 22
          end
          object edt_APPCODE: TsEdit
            Left = 180
            Top = 262
            Width = 123
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.Caption = #53685#51648#51008#54665
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPNAME1: TsEdit
            Left = 14
            Top = 286
            Width = 289
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 24
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPNAME2: TsEdit
            Left = 14
            Top = 310
            Width = 289
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 25
            SkinData.CustomColor = True
            BoundLabel.Caption = 'sEdit2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_APPNAME3: TsEdit
            Left = 14
            Top = 334
            Width = 289
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 26
            SkinData.CustomColor = True
            BoundLabel.Caption = 'sEdit2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel9: TsPanel
            Left = 310
            Top = 262
            Width = 165
            Height = 23
            SkinData.CustomColor = True
            Caption = #48156#49888#44592#44288' '#48143' '#51204#51088#49436#47749
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 27
          end
          object edt_BKNAME1: TsEdit
            Left = 310
            Top = 286
            Width = 289
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 28
            SkinData.CustomColor = True
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BKNAME2: TsEdit
            Left = 310
            Top = 310
            Width = 289
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 29
            SkinData.CustomColor = True
            BoundLabel.Caption = 'sEdit2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object edt_BKNAME3: TsEdit
            Left = 310
            Top = 334
            Width = 289
            Height = 23
            Color = clWhite
            Enabled = False
            MaxLength = 70
            ReadOnly = True
            TabOrder = 30
            SkinData.CustomColor = True
            BoundLabel.Caption = 'sEdit2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
          object sPanel10: TsPanel
            Left = 14
            Top = 366
            Width = 165
            Height = 23
            SkinData.CustomColor = True
            Caption = #44592#53440#49324#54637
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 31
          end
          object memo_REMARK1: TsMemo
            Left = 14
            Top = 391
            Width = 584
            Height = 154
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 32
            CharCase = ecUpperCase
            BoundLabel.Caption = #44592#53440#51312#44148
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = 5197647
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.ParentFont = False
          end
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 56
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <
      item
        Name = 'FDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20180101'
      end
      item
        Name = 'TDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = '20181231'
      end
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8000
        Value = Null
      end
      item
        Name = 'ALLDATA'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      'SELECT '
      
        'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, APP_CODE, APP_NAME' +
        '1, APP_NAME2, APP_NAME3, BANK_CODE, BANK1, BANK2, LC_G, LC_NO, B' +
        'L_G, BL_NO, AMT, AMTC, CHRG, CHRGC, RES_DATE, SET_DATE, REMARK1,' +
        ' BK_NAME1, BK_NAME2, BK_NAME3, CHK1, CHK2, CHK3, PRNO'
      'FROM DOANTC'
      'WHERE (DATEE BETWEEN :FDATE AND :TDATE)'
      'AND'
      '(MAINT_NO LIKE :MAINT_NO OR (1=:ALLDATA))'
      'ORDER BY DATEE DESC')
    Left = 16
    Top = 88
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryListAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryListAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryListBANK_CODE: TStringField
      FieldName = 'BANK_CODE'
      Size = 10
    end
    object qryListBANK1: TStringField
      FieldName = 'BANK1'
      Size = 70
    end
    object qryListBANK2: TStringField
      FieldName = 'BANK2'
      Size = 70
    end
    object qryListLC_G: TStringField
      FieldName = 'LC_G'
      Size = 3
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListBL_G: TStringField
      FieldName = 'BL_G'
      Size = 3
    end
    object qryListBL_NO: TStringField
      FieldName = 'BL_NO'
      Size = 35
    end
    object qryListAMT: TBCDField
      FieldName = 'AMT'
      Precision = 18
    end
    object qryListAMTC: TStringField
      FieldName = 'AMTC'
      Size = 3
    end
    object qryListCHRG: TBCDField
      FieldName = 'CHRG'
      Precision = 18
    end
    object qryListCHRGC: TStringField
      FieldName = 'CHRGC'
      Size = 3
    end
    object qryListRES_DATE: TStringField
      FieldName = 'RES_DATE'
      Size = 8
    end
    object qryListSET_DATE: TStringField
      FieldName = 'SET_DATE'
      Size = 8
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 35
    end
    object qryListBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 35
    end
    object qryListBK_NAME3: TStringField
      FieldName = 'BK_NAME3'
      Size = 35
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 88
  end
end
