unit UI_MEMO;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, DB, ADODB, StdCtrls, sComboBox, sEdit,
  Grids, DBGrids, acDBGrid, DBCtrls, sDBMemo, Mask, sDBEdit, sButton,
  Buttons, sSpeedButton, ExtCtrls, sPanel;

type
  TUI_MEMO_frm = class(TChildForm_frm)
    sPanel2: TsPanel;
    sPanel3: TsPanel;
    sSpeedButton6: TsSpeedButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnExit: TsButton;
    sPanel1: TsPanel;
    sPanel4: TsPanel;
    sPanel7: TsPanel;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel8: TsPanel;
    edt_FindText: TsEdit;
    sComboBox1: TsComboBox;
    sButton1: TsButton;
    qryList: TADOQuery;
    dsList: TDataSource;
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edt_FindTextKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sButton1Click(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReadList;
    procedure doRefresh;
  public
    { Public declarations }
  end;

var
  UI_MEMO_frm: TUI_MEMO_frm;

implementation

uses
  MSSQL, ICON, Dlg_MEMO, TypeDefine;

{$R *.dfm}

procedure TUI_MEMO_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_MEMO_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_MEMO_frm := nil;
end;

procedure TUI_MEMO_frm.edt_FindTextKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN Then ReadList;
end;

procedure TUI_MEMO_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_MEMO_frm.btnNewClick(Sender: TObject);
var
  TempControl : TProgramControlType;
begin
  inherited;

  Dlg_MEMO_frm := TDlg_MEMO_frm.Create(Self);

  case (Sender as TsButton).Tag of

    0 : TempControl := ctInsert;
    1 : TempControl := ctModify;
    2 : TempControl := ctDelete;

  end;

  try
    if Dlg_MEMO_frm.Run(TempControl,qryList.Fields) = mrOK then
    begin

        Case TempControl of
          ctInsert,ctDelete :
          begin
            qryList.Close;
            qryList.Open;
          end;

          ctModify :
          begin
            doRefresh;
          end;

        end;
    end;
  finally
     FreeAndNil(Dlg_MEMO_frm);
  end;
end;

procedure TUI_MEMO_frm.ReadList;
begin
  with qryList do
  begin
    qryList.Close;
    SQL.Text := 'SELECT * FROM MEMOD';

    Case sComboBox1.ItemIndex of
      0: SQL.Add('WHERE [CODE] LIKE '+QuotedStr('%'+edt_FindText.Text+'%'));
      1: SQL.Add('WHERE [D_MEMO] LIKE '+QuotedStr('%'+edt_FindText.Text+'%'));
    end;

    Open;
  end;
end;

procedure TUI_MEMO_frm.doRefresh;
var
  BMK : TBookmark;
begin
  BMK := qryList.GetBookmark;

  qryList.Close;
  qryList.Open;

  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);

  qryList.FreeBookmark(BMK);
end;

procedure TUI_MEMO_frm.FormShow(Sender: TObject);
begin
  inherited;
  Readlist;
end;

end.
