unit UI_RUNSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, Buttons, sBitBtn, StdCtrls, sButton, DB, ADODB,
  sMemo, ExtCtrls, sPanel, sSkinProvider, sSpeedButton;

type
  TUI_RUNSQL_frm = class(TChildForm_frm)
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    sMemo1: TsMemo;
    ADOCommand1: TADOCommand;
    ADOQuery1: TADOQuery;
    sButton1: TsButton;
    sBitBtn1: TsBitBtn;
    memo2: TsMemo;
    sBitBtn2: TsBitBtn;
    sSpeedButton1: TsSpeedButton;
    sBitBtn3: TsBitBtn;
    ADOQuery2: TADOQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sButton1Click(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sPanel2DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_RUNSQL_frm: TUI_RUNSQL_frm;

implementation

uses
  MSSQL, UI_SQLShowData, dlg_AUTHPWD, LivartConfig, VarDefine;

{$R *.dfm}

procedure TUI_RUNSQL_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_RUNSQL_frm := nil;
  FAuthOK := False;
end;

procedure TUI_RUNSQL_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  memo2.Lines.Clear;

  IF not FAuthOK Then
    memo2.Lines.Add('ERROR AUTH');
  
  IF Trim(sMemo1.Text) = '' Then
  begin
    memo2.Lines.Add(FormatDateTime('YYMMDD HH:NN:SS', now)+': 내용을 입력해주세요');
    sMemo1.SetFocus;
    Exit;
  end;

  with ADOCommand1 do
  begin
    CommandText := sMemo1.Text;
    Execute;
    memo2.Lines.Add(FormatDateTime('YYMMDD HH:NN:SS', now)+': CMD명령이 실행되었습니다');
    try
    except
      on E:Exception do
      begin
        memo2.Lines.Add(FormatDateTime('YYMMDD HH:NN:SS', now)+': '+E.Message);
      end;
    end;
  end;
end;

procedure TUI_RUNSQL_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  memo2.Lines.Clear;

  IF not FAuthOK Then
    memo2.Lines.Add('ERROR AUTH');

  IF Trim(sMemo1.Text) = '' Then
  begin
    memo2.Lines.Add(FormatDateTime('YYMMDD HH:NN:SS', now)+': 내용을 입력해주세요');
    sMemo1.SetFocus;
    Exit;
  end;

  with ADOQuery1 do
  begin
    SQL.Text := sMemo1.Text;
    ExecSQL;
    memo2.Lines.Add(FormatDateTime('YYMMDD HH:NN:SS', now)+': ExecSQL명령이 실행되었습니다');
    try
    except
      on E:Exception do
      begin
        memo2.Lines.Add(FormatDateTime('YYMMDD HH:NN:SS', now)+': '+E.Message);
      end;
    end;
  end;
end;

procedure TUI_RUNSQL_frm.sBitBtn2Click(Sender: TObject);
begin
  inherited;
  memo2.Lines.Clear;

  IF not FAuthOK Then
    memo2.Lines.Add('ERROR AUTH');

  IF Trim(sMemo1.Text) = '' Then
  begin
    memo2.Lines.Add(FormatDateTime('YYMMDD HH:NN:SS', now)+': 내용을 입력해주세요');
    sMemo1.SetFocus;
    Exit;
  end;

  UI_SQLShowData_frm := TUI_SQLShowData_frm.Create(Self);
  try
    if (Sender as TsBitBtn).Tag = 0 Then
    begin
      ADOQuery1.Close;
      UI_SQLShowData_frm.DataSource1.DataSet := ADOQuery1;
      ADOQuery1.SQL.Text := sMemo1.Text;
      ADOQuery1.Open;
    end
    else
    if (sender as TsBitBtn).Tag = 1 Then
    begin
      LivartConfig_frm := TLivartConfig_frm.Create(Self);
      LivartConfig_frm.LivartConn.ConnectionString := LivartConfig_frm.setConnectionString;
      ADOQuery2.Connection := LivartConfig_frm.LivartConn;
      ADOQuery2.SQL.Text := sMemo1.Text;
      ADOQuery2.Open;
      UI_SQLShowData_frm.DataSource1.DataSet := ADOQuery2;
    end;
    UI_SQLShowData_frm.ShowModal;
  finally
    ADOQuery1.Close;
    FreeAndNil(UI_SQLShowData_frm);
    IF LivartConfig_frm <> nil then
      FreeAndNil(LivartConfig_frm);
  end;
end;

procedure TUI_RUNSQL_frm.sPanel2DblClick(Sender: TObject);
begin
  inherited;
  FAuthOK := dlg_AUTHPWD_frm.ShowModal = mrOk;
  sButton1.Enabled := FAuthOK;
  sBitBtn1.Enabled := FAuthOK;
  sBitBtn2.Enabled := FAuthOK;
end;

procedure TUI_RUNSQL_frm.FormShow(Sender: TObject);
begin
  inherited;
  sButton1.Enabled := FAuthOK;
  sBitBtn1.Enabled := FAuthOK;
  sBitBtn2.Enabled := FAuthOK;
end;

end.
