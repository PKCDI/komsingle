unit UI_LOCRC1_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, Mask, sMaskEdit, sEdit, sButton,
  sLabel, Buttons, sSpeedButton, ExtCtrls, sPanel, sSkinProvider, sMemo,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl,
  sBitBtn, Grids, DBGrids, acDBGrid, QuickRpt, DB, ADODB, StrUtils, DateUtils;

type
  TUI_LOCRC1_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton5: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton8: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    Shape1: TShape;
    Shape2: TShape;
    sSpeedButton3: TsSpeedButton;
    Shape3: TShape;
    Shape4: TShape;
    msk_GET_DAT: TsMaskEdit;
    msk_ISS_DAT: TsMaskEdit;
    msk_EXP_DAT: TsMaskEdit;
    edt_RFF_NO: TsEdit;
    msk_BSN_HSCODE: TsMaskEdit;
    edt_BENEFC: TsEdit;
    edt_BENEFC1: TsEdit;
    edt_BENEFC2: TsEdit;
    edt_APPLIC: TsEdit;
    edt_APPLIC1: TsEdit;
    edt_APPLIC7: TsEdit;
    edt_APPLIC2: TsEdit;
    edt_APPLIC3: TsEdit;
    edt_APPLIC4: TsEdit;
    edt_APPLIC5: TsEdit;
    edt_APPLIC6: TsEdit;
    edt_AP_BANK: TsEdit;
    edt_AP_BANK1: TsEdit;
    edt_AP_BANK2: TsEdit;
    edt_AP_BANK3: TsEdit;
    edt_AP_BANK4: TsEdit;
    edt_AP_NAME: TsEdit;
    sPanel12: TsPanel;
    edt_LOC_NO: TsEdit;
    edt_LOC1AMTC: TsEdit;
    curr_LOC1AMT: TsCurrencyEdit;
    curr_EX_RATE: TsCurrencyEdit;
    curr_LOC2AMT: TsCurrencyEdit;
    edt_LOC2AMTC: TsEdit;
    msk_LOADDATE: TsMaskEdit;
    msk_EXPDATE: TsMaskEdit;
    edt_AMAINT_NO: TsEdit;
    edt_RCT_AMT1C: TsEdit;
    curr_RCT_AMT1: TsCurrencyEdit;
    curr_RATE: TsCurrencyEdit;
    edt_RCT_AMT2C: TsEdit;
    curr_RCT_AMT2: TsCurrencyEdit;
    edt_TQTY_G: TsEdit;
    curr_TQTY: TsCurrencyEdit;
    edt_TAMT_G: TsEdit;
    curr_TAMT: TsCurrencyEdit;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    memo_REMARK1: TsMemo;
    sPanel1: TsPanel;
    sPanel2: TsPanel;
    memo_LOC_REM1: TsMemo;
    sTabSheet2: TsTabSheet;
    sPanel8: TsPanel;
    sDBGrid2: TsDBGrid;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    sSpeedButton10: TsSpeedButton;
    Shape5: TShape;
    sPanel22: TsPanel;
    sPanel15: TsPanel;
    msk_HS_NO: TsMaskEdit;
    memo_NAME1: TsMemo;
    sButton2: TsButton;
    sPanel14: TsPanel;
    memo_SIZE1: TsMemo;
    edt_QTY_G: TsEdit;
    curr_QTY: TsCurrencyEdit;
    edt_PRICE_G: TsEdit;
    curr_PRICE: TsCurrencyEdit;
    edt_QTYG_G: TsEdit;
    curr_QTYG: TsCurrencyEdit;
    edt_AMT_G: TsEdit;
    curr_AMT: TsCurrencyEdit;
    edt_STQTY_G: TsEdit;
    curr_STQTY: TsCurrencyEdit;
    edt_STAMT_G: TsEdit;
    curr_STAMT: TsCurrencyEdit;
    edt_NAME_COD: TsEdit;
    sPanel13: TsPanel;
    sTabSheet5: TsTabSheet;
    sPanel9: TsPanel;
    sPanel17: TsPanel;
    sDBGrid4: TsDBGrid;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    qrylist: TADOQuery;
    qrylistMAINT_NO: TStringField;
    qrylistUSER_ID: TStringField;
    qrylistDATEE: TStringField;
    qrylistMESSAGE1: TStringField;
    qrylistMESSAGE2: TStringField;
    qrylistRFF_NO: TStringField;
    qrylistGET_DAT: TStringField;
    qrylistISS_DAT: TStringField;
    qrylistEXP_DAT: TStringField;
    qrylistBENEFC: TStringField;
    qrylistBENEFC1: TStringField;
    qrylistAPPLIC: TStringField;
    qrylistAPPLIC1: TStringField;
    qrylistAPPLIC2: TStringField;
    qrylistAPPLIC3: TStringField;
    qrylistAPPLIC4: TStringField;
    qrylistAPPLIC5: TStringField;
    qrylistAPPLIC6: TStringField;
    qrylistRCT_AMT1: TBCDField;
    qrylistRCT_AMT1C: TStringField;
    qrylistRCT_AMT2: TBCDField;
    qrylistRCT_AMT2C: TStringField;
    qrylistRATE: TBCDField;
    qrylistREMARK: TStringField;
    qrylistREMARK1: TMemoField;
    qrylistTQTY: TBCDField;
    qrylistTQTY_G: TStringField;
    qrylistTAMT: TBCDField;
    qrylistTAMT_G: TStringField;
    qrylistLOC_NO: TStringField;
    qrylistAP_BANK: TStringField;
    qrylistAP_BANK1: TStringField;
    qrylistAP_BANK2: TStringField;
    qrylistAP_BANK3: TStringField;
    qrylistAP_BANK4: TStringField;
    qrylistAP_NAME: TStringField;
    qrylistLOC1AMT: TBCDField;
    qrylistLOC1AMTC: TStringField;
    qrylistLOC2AMT: TBCDField;
    qrylistLOC2AMTC: TStringField;
    qrylistEX_RATE: TBCDField;
    qrylistLOADDATE: TStringField;
    qrylistEXPDATE: TStringField;
    qrylistLOC_REM: TStringField;
    qrylistLOC_REM1: TMemoField;
    qrylistCHK1: TBooleanField;
    qrylistCHK2: TStringField;
    qrylistCHK3: TStringField;
    qrylistNEGODT: TStringField;
    qrylistNEGOAMT: TBCDField;
    qrylistBSN_HSCODE: TStringField;
    qrylistPRNO: TIntegerField;
    qrylistAPPLIC7: TStringField;
    qrylistBENEFC2: TStringField;
    qrylistCK_S: TStringField;
    dslist: TDataSource;
    qryGoods: TADOQuery;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TBCDField;
    qryGoodsHS_CHK: TStringField;
    qryGoodsHS_NO: TStringField;
    qryGoodsNAME_CHK: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE: TStringField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsAMT: TBCDField;
    qryGoodsAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsSTAMT: TBCDField;
    qryGoodsSTAMT_G: TStringField;
    dsGoods: TDataSource;
    qryTax: TADOQuery;
    qryTaxKEYY: TStringField;
    qryTaxSEQ: TBCDField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    dsTax: TDataSource;
    QRCompositeReport1: TQRCompositeReport;
    procedure FormCreate(Sender: TObject);
    procedure sDBGrid1TitleClick(Column: TColumn);
    procedure sBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qrylistAfterScroll(DataSet: TDataSet);
    procedure qrylistAfterOpen(DataSet: TDataSet);
    procedure sDBGrid2TitleClick(Column: TColumn);
    procedure sDBGrid4TitleClick(Column: TColumn);
    procedure qryGoodsNAME1GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure qryGoodsAfterScroll(DataSet: TDataSet);
    procedure qryGoodsAfterOpen(DataSet: TDataSet);
    procedure sPageControl1Change(Sender: TObject);
    procedure sMaskEdit1Change(Sender: TObject);
    procedure edt_SearchNoChange(Sender: TObject);
    procedure sEdit1Change(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FSQL : String;
    FGOODSSQL : String;
    FTAXSQL : String;
    function ReadListOnly(sortField : String = ''):integer;
    function ReadGoods(sortField : String = ''):Integer;
    function ReadTax(sortField : String= ''):Integer;
    procedure setData;
    procedure setGoods;
  public
    { Public declarations }
  end;

var
  UI_LOCRC1_NEW_frm: TUI_LOCRC1_NEW_frm;

implementation

uses
  SQLCreator, MSSQL, MessageDefine, Preview, LOCRC1_PRINT, LOCRC1_PRINT2;

{$R *.dfm}

{ TUI_LOCRC1_NEW_frm }

function TUI_LOCRC1_NEW_frm.ReadGoods(sortField: String): Integer;
begin
  ClearGridTitleCaption(sDBGrid2);
  with qryGoods do
  begin
    Close;
    SQL.Text := FGOODSSQL;
    Parameters[0].Value := qrylistMAINT_NO.AsString;

    IF sortField = '' Then
      SQL.Add('ORDER BY SEQ')
    else
    begin
      if (qryGoods.Filter = '') OR (qryGoods.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryGoods.Filter := sortField;
        Tag := 0;
      end
      else
      if qryGoods.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;
    end;

    Open;
  end;

  result := qryGoods.Tag;
end;

function TUI_LOCRC1_NEW_frm.ReadListOnly(sortField: String): integer;
begin
  ClearGridTitleCaption(sDBGrid1);
  ClearGridTitleCaption(sDBGrid3);

  with qrylist do
  Begin
    Close;
    SQL.Text := FSQL;
    SQL.Add('WHERE DATEE between '+QuotedStr(sMaskEdit1.Text)+' and '+QuotedStr(sMaskEdit2.Text));

    IF Length(Trim(edt_SearchNo.Text)) > 0 Then
      SQL.Add('AND MAINT_NO LIKE '+QuotedStr('%'+Trim(edt_SearchNo.Text)+'%'));

    IF sortField = '' Then
      SQL.Add('ORDER BY DATEE DESC, MAINT_NO DESC')
    else
    begin
      if (qrylist.Filter = '') OR (qrylist.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qrylist.Filter := sortField;
        Tag := 0;
      end
      else
      if qrylist.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;

      if sortField <> 'MAINT_NO' then
        SQL.Add(', MAINT_NO DESC');
    end;

    Open;
  end;

  result := qrylist.Tag;
end;

function TUI_LOCRC1_NEW_frm.ReadTax(sortField: String): Integer;
begin
  ClearGridTitleCaption(sDBGrid4);

  with qryTax do
  begin
    Close;
    SQL.Text := FTaxSQL;
    Parameters[0].Value := qrylistMAINT_NO.AsString;

    IF sortField = '' Then
      SQL.Add('ORDER BY SEQ')
    else
    begin
      if (qryTax.Filter = '') OR (qryTax.Filter <> sortField) Then
      begin
        SQL.Add('ORDER BY '+sortField);
        qryTax.Filter := sortField;
        Tag := 0;
      end
      else
      if qryTax.Filter = sortField Then
      begin
        case Tag of
          //ASC -> DESC
          0:
          begin
            SQL.Add('ORDER BY '+sortField+' DESC');
            Tag := 1;
          end;
          //DESC -> ASC
          1:
          begin
            SQL.Add('ORDER BY '+sortField);
            Tag := 0;
          end;
        end;
      end;
    end;

    Open;
  end;

  result := qryTax.Tag;
end;

procedure TUI_LOCRC1_NEW_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qrylist.SQL.Text;
  FGOODSSQL := qryGoods.SQL.Text;
  FTAXSQL := qryTax.SQL.Text;

  ReadOnlyControl(sPanel6);  
  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel27);
  ReadOnlyControl(sPanel8);
  ReadOnlyControl(sPanel9);
end;

procedure TUI_LOCRC1_NEW_frm.sDBGrid1TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  nRst := ReadListOnly(Column.FieldName);
  sTitle := AnsiReplaceText(AnsiReplaceText(Column.Title.Caption,'▲',''),'▼','');
  case nRst of
    0: Column.Title.Caption := sTitle+'▲';
    1: Column.Title.Caption := sTitle+'▼';
  end;
end;

procedure TUI_LOCRC1_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  qrylist.Filter := '';
  ReadListOnly;
end;

procedure TUI_LOCRC1_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sPageControl1.ActivePageIndex := 0;

  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', IncMonth(StartOfTheMonth( Now ),-1));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', Now);
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  RefreshButton(sPanel7);

  ReadListOnly;       
  ReadOnlyControl(spanel7,false);
  ReadOnlyControl(spanel11,false);
end;

procedure TUI_LOCRC1_NEW_frm.qrylistAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setdata;
  qryGoods.Filter := '';
  ReadGoods;
  qryTax.Filter := '';
  ReadTax;
end;

procedure TUI_LOCRC1_NEW_frm.qrylistAfterOpen(DataSet: TDataSet);
begin
  inherited;
  btnDel.Enabled := DataSet.RecordCount > 0;
  btnPrint.Enabled := btnDel.Enabled;

  if DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_LOCRC1_NEW_frm.sDBGrid2TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  If Column.Field.DataType = ftMemo Then
  begin
    MessageBox(Self.Handle, 'Memo필드는 정렬할 수 없습니다', '정렬오류', MB_OK+MB_ICONQUESTION);
    Exit;
  end;

  nRst := ReadGoods(Column.FieldName);
end;

procedure TUI_LOCRC1_NEW_frm.sDBGrid4TitleClick(Column: TColumn);
var
  nRst : Integer;
  sTitle : String;
begin
  inherited;
  If Column.Field.DataType = ftMemo Then
  begin
    MessageBox(Self.Handle, 'Memo필드는 정렬할 수 없습니다', '정렬오류', MB_OK+MB_ICONQUESTION);
    Exit;
  end;
  nRst := ReadTax(Column.FieldName);
end;
procedure TUI_LOCRC1_NEW_frm.setData;
begin
  ClearControl(spanel7);
  
  edt_MAINT_NO.Text := qrylistMAINT_NO.AsString;
  msk_DATEE.Text := qrylistDATEE.AsString;
  edt_userno.Text := qrylistUSER_ID.AsString;
  CM_INDEX(com_func, [':'], 0, qrylistMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qrylistMESSAGE2.AsString);
//------------------------------------------------------------------------------
// 기본입력사항
//------------------------------------------------------------------------------
  //인수일자
  msk_GET_DAT.Text := qrylistGET_DAT.AsString;
  //발급일자
  msk_ISS_DAT.Text := qrylistISS_DAT.AsString;
  //문서유효일자
  msk_EXP_DAT.Text := qrylistEXP_DAT.AsString;
  //발급번호
  edt_RFF_NO.Text := qrylistRFF_NO.AsString;
  //대표공급물품 HS부호
  msk_BSN_HSCODE.Text := qrylistBSN_HSCODE.AsString;
//------------------------------------------------------------------------------
// 수혜업체/개설업체
//------------------------------------------------------------------------------
  //수혜자
  edt_BENEFC.Text := qrylistBENEFC.AsString;
  edt_BENEFC1.Text := qrylistBENEFC1.AsString;
  edt_BENEFC2.Text := qrylistBENEFC2.AsString;
  //개설업체
  //코드
  edt_APPLIC.Text  := qrylistAPPLIC.AsString;
  //상호
  edt_APPLIC1.Text := qrylistAPPLIC1.AsString;
  //명의인
  edt_APPLIC2.Text := qrylistAPPLIC2.AsString;
  //전자서명
  edt_APPLIC3.Text := qrylistAPPLIC3.AsString;
  //주소1
  edt_APPLIC4.Text := qrylistAPPLIC4.AsString;
  //주소2
  edt_APPLIC5.Text := qrylistAPPLIC5.AsString;
  //주소3
  edt_APPLIC6.Text := qrylistAPPLIC6.AsString;
  //사업자등록번호
  edt_APPLIC7.Text := qrylistAPPLIC7.AsString;
//------------------------------------------------------------------------------
// 개설은행
//------------------------------------------------------------------------------
  //개설은행
  edt_AP_BANK.Text := qrylistAP_BANK.AsString;
  edt_AP_BANK1.Text := qrylistAP_BANK1.AsString;
  edt_AP_BANK2.Text := qrylistAP_BANK2.AsString;
  //지점명
  edt_AP_BANK3.Text := qrylistAP_BANK3.AsString;
  edt_AP_BANK4.Text := qrylistAP_BANK4.AsString;
  //전자서명
  edt_AP_NAME.Text := qrylistAP_NAME.AsString;
//------------------------------------------------------------------------------
// 내국신용장
//------------------------------------------------------------------------------
  //내국신용장 번호
  edt_LOC_NO.Text := qrylistLOC_NO.AsString;
  //개설금액(외화)
  edt_LOC1AMTC.Text := qrylistLOC1AMTC.AsString;
  curr_LOC1AMT.Value := qrylistLOC1AMT.AsCurrency;
  //매매기준율
  curr_EX_RATE.Value := qrylistEX_RATE.AsFloat;
  //개설금액(원화)
  curr_LOC2AMT.Value := qrylistLOC2AMT.AsCurrency;
  //물품인도기일
  msk_LOADDATE.Text := qrylistLOADDATE.AsString;
  //유효일자
  msk_EXPDATE.Text := qrylistEXPDATE.AsString;

//------------------------------------------------------------------------------
// 인수금액
//------------------------------------------------------------------------------
  //인수금액(외화)
  edt_RCT_AMT1C.Text := qrylistRCT_AMT1C.AsString;
  curr_RCT_AMT1.Value := qrylistRCT_AMT1.AsCurrency;
  //환율
  curr_RATE.Value := qrylistRATE.Asfloat;
  //인수금액(원화)
  edt_RCT_AMT2C.Text := qrylistRCT_AMT2C.AsString;
  curr_RCT_AMT2.Value := qrylistRCT_AMT2.AsCurrency;
//------------------------------------------------------------------------------
// 총합계
//------------------------------------------------------------------------------
  //총합계수량
  edt_TQTY_G.Text := qrylistTQTY_G.AsString;
  curr_TQTY.Value := qrylistTQTY.AsCurrency;
  //총합계금액
  edt_TAMT_G.Text := qrylistTAMT_G.AsString;
  curr_TAMT.Value := qrylistTAMT.AsCurrency;
//------------------------------------------------------------------------------
// 기타조건/참고사항
//------------------------------------------------------------------------------
  //기타조건
  memo_REMARK1.Lines.Text := qrylistREMARK1.AsString;
  //참고사항
  memo_LOC_REM1.Lines.Text := qrylistLOC_REM1.AsString;
end;

procedure TUI_LOCRC1_NEW_frm.qryGoodsNAME1GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  Text := AnsiReplaceText(Sender.AsString, #13#10, ' ');
end;

procedure TUI_LOCRC1_NEW_frm.setGoods;
begin
  ClearControl(spanel11);
//------------------------------------------------------------------------------
// 상품
//------------------------------------------------------------------------------
  edt_NAME_COD.Text := qryGoodsNAME_COD.AsString;
  msk_HS_NO.Text := qryGoodsHS_NO.AsString;
  memo_NAME1.Text := qryGoodsNAME1.AsString;
  memo_SIZE1.Text := qryGoodsSIZE1.AsString;
//------------------------------------------------------------------------------
// 수량/단가/금액
//------------------------------------------------------------------------------
  edt_QTY_G.Text := qryGoodsQTY_G.AsString;
  edt_QTYG_G.Text := qryGoodsQTYG_G.AsString;
  edt_PRICE_G.Text := qryGoodsPRICE_G.AsString;
  edt_AMT_G.Text := qryGoodsAMT_G.AsString;
  edt_STQTY_G.Text := qryGoodsSTQTY_G.AsString;
  edt_STAMT_G.Text := qryGoodsSTAMT_G.AsString;

  curr_QTY.Value := qryGoodsQTY.AsCurrency;
  curr_QTYG.Value := qryGoodsQTYG.AsCurrency;
  curr_PRICE.Value :=  qryGoodsPRICE.AsCurrency;
  curr_AMT.Value := qryGoodsAMT.AsCurrency;
  curr_STQTY.Value := qryGoodsSTQTY.AsCurrency;
  curr_STAMT.Value := qryGoodsSTAMT.AsCurrency;
end;

procedure TUI_LOCRC1_NEW_frm.qryGoodsAfterScroll(DataSet: TDataSet);
begin
  inherited;
  setGoods;
end;

procedure TUI_LOCRC1_NEW_frm.qryGoodsAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 Then qryGoodsAfterScroll(DataSet);
end;

procedure TUI_LOCRC1_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 4;
end;

procedure TUI_LOCRC1_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  case (Sender as TsMaskEdit).Tag of
    0: sMaskEdit3.Text := sMaskEdit1.Text;
    1: sMaskEdit4.Text := sMaskEdit2.Text;
    2: sMaskEdit1.Text := sMaskEdit3.Text;
    3: sMaskEdit2.Text := sMaskEdit4.Text;
  end;
end;

procedure TUI_LOCRC1_NEW_frm.edt_SearchNoChange(Sender: TObject);
begin
  inherited;
  sEdit1.Text := edt_SearchNo.Text;
end;

procedure TUI_LOCRC1_NEW_frm.sEdit1Change(Sender: TObject);
begin
  inherited;
  edt_SearchNo.Text := sEdit1.Text;
end;

procedure TUI_LOCRC1_NEW_frm.btnDelClick(Sender: TObject);
var
  MAINT_NO : String;
  sc : TSQLCreate;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  MAINT_NO := qrylistMAINT_NO.AsString;
  nCursor := qrylist.RecNo;
  DMMssql.BeginTrans;
  SC := TSQLCreate.Create;
  try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+MAINT_NO+#13#10+MSG_SYSTEM_LINE+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then
      begin
        DMMssql.RollbackTrans;
        Exit;
      end;

      SC.DMLType := dmlDelete; SC.SQLHeader('LOCRC1_H'); SC.ADDWhere('MAINT_NO', MAINT_NO); ExecSQL(SC.CreateSQL);
      SC.DMLType := dmlDelete; SC.SQLHeader('LOCRC1_D'); SC.ADDWhere('KEYY', MAINT_NO); ExecSQL(SC.CreateSQL);
      SC.DMLType := dmlDelete; SC.SQLHeader('LOCRC1_TAX'); SC.ADDWhere('KEYY', MAINT_NO); ExecSQL(SC.CreateSQL);

      DMMssql.CommitTrans;

      qrylist.Close;
      qrylist.Open;

      IF nCursor > 1 Then
        qrylist.MoveBy(nCursor-1);

      MessageBox(Self.Handle, MSG_SYSTEM_DEL_OK, '삭제완료', MB_OK+MB_ICONINFORMATION);
    except
      on e:Exception do
      begin
        DMMssql.RollbackTrans;
        MessageBox(Self.Handle, PChar('ERROR'#13#10+e.Message), '삭제오류', MB_OK+MB_ICONERROR);
      end;
    end;
  finally
    SC.Free;
  end;
end;

procedure TUI_LOCRC1_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  LOCRC1_PRINT_frm := TLOCRC1_PRINT_frm.Create(Self);
  LOCRC1_PRINT2_frm := TLOCRC1_PRINT2_frm.Create(Self);

  Preview_frm := TPreview_frm.Create(Self);
  try
    LOCRC1_PRINT_frm.MaintNo := edt_MAINT_NO.Text;
    LOCRC1_PRINT2_frm.MaintNo := edt_MAINT_NO.Text;
    QRCompositeReport1.Prepare;

    Preview_frm.CompositeRepost := QRCompositeReport1;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(LOCRC1_PRINT_frm);
    FreeAndNil(LOCRC1_PRINT2_frm);
  end;
end;

procedure TUI_LOCRC1_NEW_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(LOCRC1_PRINT_frm);
  QRCompositeReport1.Reports.Add(LOCRC1_PRINT2_frm);
end;

procedure TUI_LOCRC1_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCRC1_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LOCRC1_NEW_frm := nil;
end;

end.
