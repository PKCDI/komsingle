inherited UI_ITEM_frm: TUI_ITEM_frm
  Left = 878
  Top = 196
  BorderWidth = 4
  Caption = #54408#47749'/'#44508#44201#44288#47532
  ClientHeight = 422
  ClientWidth = 786
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 786
    Height = 46
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alTop
    BorderWidth = 1
    
    TabOrder = 0
    object sPanel3: TsPanel
      Left = 2
      Top = 2
      Width = 782
      Height = 42
      Align = alClient
      
      TabOrder = 0
      object sSpeedButton6: TsSpeedButton
        Left = 204
        Top = 4
        Width = 8
        Height = 35
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'SPEEDBUTTON'
      end
      object btnNew: TsButton
        Left = 6
        Top = 4
        Width = 65
        Height = 35
        Cursor = crHandPoint
        Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
        Caption = #49888#44508
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = btnNewClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 38
        ContentMargin = 8
      end
      object btnEdit: TsButton
        Tag = 1
        Left = 72
        Top = 4
        Width = 65
        Height = 35
        Cursor = crHandPoint
        Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
        Caption = #49688#51221
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = False
        OnClick = btnNewClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 3
        ContentMargin = 8
      end
      object btnDel: TsButton
        Tag = 2
        Left = 138
        Top = 4
        Width = 65
        Height = 35
        Cursor = crHandPoint
        Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
        Caption = #49325#51228
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        TabStop = False
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 27
        ContentMargin = 8
      end
      object btnExit: TsButton
        Left = 705
        Top = 3
        Width = 74
        Height = 35
        Cursor = crHandPoint
        Caption = #45803#44592
        TabOrder = 3
        TabStop = False
        OnClick = btnExitClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 20
        ContentMargin = 12
      end
    end
  end
  object sPanel2: TsPanel [1]
    Left = 0
    Top = 46
    Width = 786
    Height = 376
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 1
    object sPanel4: TsPanel
      Left = 297
      Top = 1
      Width = 488
      Height = 374
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alClient
      
      TabOrder = 0
      object sPanel7: TsPanel
        Left = 1
        Top = 1
        Width = 486
        Height = 372
        Align = alClient
        
        TabOrder = 0
        object sDBEdit1: TsDBEdit
          Left = 88
          Top = 16
          Width = 169
          Height = 23
          Color = clWhite
          DataField = 'CODE'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          BoundLabel.Active = True
          BoundLabel.Caption = #54408#47785#53076#46300
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sDBEdit2: TsDBEdit
          Left = 88
          Top = 40
          Width = 169
          Height = 23
          Color = clWhite
          DataField = 'HSCODE'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          BoundLabel.Active = True
          BoundLabel.Caption = 'HS'#48512#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sDBEdit3: TsDBEdit
          Left = 88
          Top = 64
          Width = 377
          Height = 23
          Color = clWhite
          DataField = 'SNAME'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          BoundLabel.Active = True
          BoundLabel.Caption = #54408#47785#47749'('#45800#52629')'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sDBMemo1: TsDBMemo
          Left = 88
          Top = 92
          Width = 377
          Height = 89
          Color = clWhite
          DataField = 'FNAME'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 3
          BoundLabel.Active = True
          BoundLabel.Caption = #54408#47785#47749'('#51204#52404')'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeftTop
        end
        object sDBMemo2: TsDBMemo
          Left = 88
          Top = 182
          Width = 377
          Height = 89
          Color = clWhite
          DataField = 'MSIZE'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 4
          BoundLabel.Active = True
          BoundLabel.Caption = #44508#44201
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeftTop
        end
        object sDBEdit4: TsDBEdit
          Left = 88
          Top = 276
          Width = 41
          Height = 23
          Color = clWhite
          DataField = 'QTYC'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          BoundLabel.Active = True
          BoundLabel.Caption = #49688#47049#45800#50948
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sDBEdit5: TsDBEdit
          Left = 88
          Top = 300
          Width = 41
          Height = 23
          Color = clWhite
          DataField = 'PRICEG'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          BoundLabel.Active = True
          BoundLabel.Caption = #45800#44032
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sDBEdit6: TsDBEdit
          Left = 130
          Top = 300
          Width = 151
          Height = 23
          Color = clWhite
          DataField = 'PRICE'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
        end
        object sDBEdit7: TsDBEdit
          Left = 88
          Top = 324
          Width = 41
          Height = 23
          Color = clWhite
          DataField = 'QTY_UG'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          BoundLabel.Active = True
          BoundLabel.Caption = #45800#50948#49688#47049
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sDBEdit8: TsDBEdit
          Left = 130
          Top = 324
          Width = 151
          Height = 23
          Color = clWhite
          DataField = 'QTY_U'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
        end
      end
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 296
      Height = 374
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alLeft
      
      TabOrder = 1
      object sPanel6: TsPanel
        Left = 1
        Top = 41
        Width = 294
        Height = 2
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 0
      end
      object sPanel8: TsPanel
        Left = 1
        Top = 1
        Width = 294
        Height = 40
        Align = alTop
        
        TabOrder = 1
        object edt_FindText: TsEdit
          Left = 87
          Top = 8
          Width = 141
          Height = 23
          TabOrder = 0
          OnKeyUp = edt_FindTextKeyUp
        end
        object sComboBox1: TsComboBox
          Left = 8
          Top = 8
          Width = 77
          Height = 23
          VerticalAlignment = taVerticalCenter
          Style = csOwnerDrawFixed
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 1
          Text = #54408#47785#53076#46300
          Items.Strings = (
            #54408#47785#53076#46300
            #54408#47749)
        end
        object sButton1: TsButton
          Left = 229
          Top = 8
          Width = 58
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
          OnClick = sButton1Click
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 1
        Top = 43
        Width = 294
        Height = 330
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            Color = clBtnFace
            Expanded = False
            FieldName = 'CODE'
            Title.Alignment = taCenter
            Title.Caption = #54408#47785#53076#46300
            Width = 94
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SNAME'
            Title.Alignment = taCenter
            Title.Caption = #54408#47749
            Width = 179
            Visible = True
          end>
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 224
  end
  object qryList: TADOQuery
    Active = True
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [CODE]'
      '      ,[HSCODE]'
      '      ,[FNAME]'
      '      ,[SNAME]'
      '      ,[MSIZE]'
      '      ,[QTYC]'
      '      ,[PRICEG]'
      '      ,[PRICE]'
      '      ,[QTY_U]'
      '      ,[QTY_UG]'
      '  FROM [ITEM_CODE]')
    Left = 40
    Top = 224
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 72
    Top = 224
  end
end
