inherited UI_MEMO_frm: TUI_MEMO_frm
  Left = 531
  Top = 166
  BorderWidth = 4
  Caption = #47700#47784#53076#46300'('#49345#50857#44396')'
  ClientHeight = 400
  ClientWidth = 825
  Font.Name = #47569#51008' '#44256#46357
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel2: TsPanel [0]
    Left = 0
    Top = 0
    Width = 825
    Height = 46
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alTop
    BorderWidth = 1
    
    TabOrder = 0
    object sPanel3: TsPanel
      Left = 2
      Top = 2
      Width = 821
      Height = 42
      Align = alClient
      
      TabOrder = 0
      DesignSize = (
        821
        42)
      object sSpeedButton6: TsSpeedButton
        Left = 226
        Top = 4
        Width = 8
        Height = 35
        ButtonStyle = tbsDivider
        SkinData.SkinSection = 'SPEEDBUTTON'
      end
      object btnNew: TsButton
        Left = 5
        Top = 4
        Width = 94
        Height = 35
        Cursor = crHandPoint
        Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
        Caption = #47700#47784#52628#44032
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = btnNewClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 40
        ContentMargin = 8
      end
      object btnEdit: TsButton
        Tag = 1
        Left = 98
        Top = 4
        Width = 65
        Height = 35
        Cursor = crHandPoint
        Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
        Caption = #49688#51221
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = False
        OnClick = btnNewClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 3
        ContentMargin = 8
      end
      object btnDel: TsButton
        Tag = 2
        Left = 162
        Top = 4
        Width = 65
        Height = 35
        Cursor = crHandPoint
        Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
        Caption = #49325#51228
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        TabStop = False
        OnClick = btnNewClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 27
        ContentMargin = 8
      end
      object btnExit: TsButton
        Left = 743
        Top = 3
        Width = 74
        Height = 35
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #45803#44592
        TabOrder = 3
        TabStop = False
        OnClick = btnExitClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 20
        ContentMargin = 12
      end
    end
  end
  object sPanel1: TsPanel [1]
    Left = 0
    Top = 46
    Width = 825
    Height = 354
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 1
    object sPanel4: TsPanel
      Left = 297
      Top = 1
      Width = 527
      Height = 352
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alClient
      
      TabOrder = 0
      object sPanel7: TsPanel
        Left = 1
        Top = 1
        Width = 525
        Height = 350
        Align = alClient
        
        TabOrder = 0
        object sDBEdit1: TsDBEdit
          Left = 72
          Top = 16
          Width = 97
          Height = 23
          Color = clWhite
          DataField = 'CODE'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          BoundLabel.Active = True
          BoundLabel.Caption = #47700#47784#53076#46300
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sDBEdit2: TsDBEdit
          Left = 72
          Top = 40
          Width = 425
          Height = 23
          Color = clWhite
          DataField = 'SUBJECT'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          BoundLabel.Active = True
          BoundLabel.Caption = #51228#47785
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sDBMemo1: TsDBMemo
          Left = 72
          Top = 64
          Width = 425
          Height = 273
          Color = clWhite
          DataField = 'D_MEMO'
          DataSource = dsList
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 2
          BoundLabel.Active = True
          BoundLabel.Caption = #47700#47784#45236#50857
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeftTop
        end
      end
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 296
      Height = 352
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alLeft
      
      TabOrder = 1
      object sPanel6: TsPanel
        Left = 1
        Top = 41
        Width = 294
        Height = 2
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        
        TabOrder = 0
      end
      object sDBGrid1: TsDBGrid
        Left = 1
        Top = 43
        Width = 294
        Height = 308
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            Color = clBtnFace
            Expanded = False
            FieldName = 'CODE'
            Title.Alignment = taCenter
            Title.Caption = #47700#47784#53076#46300
            Width = 94
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SUBJECT'
            Title.Alignment = taCenter
            Title.Caption = #51228#47785
            Width = 177
            Visible = True
          end>
      end
      object sPanel8: TsPanel
        Left = 1
        Top = 1
        Width = 294
        Height = 40
        Align = alTop
        
        TabOrder = 2
        object edt_FindText: TsEdit
          Left = 87
          Top = 8
          Width = 141
          Height = 23
          TabOrder = 0
          OnKeyUp = edt_FindTextKeyUp
        end
        object sComboBox1: TsComboBox
          Left = 8
          Top = 8
          Width = 77
          Height = 23
          VerticalAlignment = taVerticalCenter
          Style = csOwnerDrawFixed
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 1
          Text = #47700#47784#53076#46300
          Items.Strings = (
            #47700#47784#53076#46300
            #45236#50857)
        end
        object sButton1: TsButton
          Left = 229
          Top = 8
          Width = 58
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
          OnClick = sButton1Click
        end
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 120
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [PREFIX]'
      '      ,[CODE]'
      '      ,[SUBJECT]'
      '      ,[D_MEMO]'
      '  FROM [dbo].[MEMOD]')
    Left = 96
    Top = 168
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 128
    Top = 168
  end
end
