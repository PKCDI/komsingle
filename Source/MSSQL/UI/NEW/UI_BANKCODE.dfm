inherited UI_BANKCODE_frm: TUI_BANKCODE_frm
  Left = 281
  Top = 294
  BorderWidth = 4
  Caption = #51008#54665#53076#46300
  ClientHeight = 425
  ClientWidth = 487
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel2: TsPanel [0]
    Left = 0
    Top = 0
    Width = 487
    Height = 46
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alTop
    BorderWidth = 1
    DoubleBuffered = False
    TabOrder = 0
    object sPanel3: TsPanel
      Left = 2
      Top = 2
      Width = 483
      Height = 42
      Align = alClient
      DoubleBuffered = False
      TabOrder = 0
      DesignSize = (
        483
        42)
      object sSpeedButton6: TsSpeedButton
        Left = 218
        Top = 4
        Width = 8
        Height = 35
        ButtonStyle = tbsDivider
      end
      object btnNew: TsButton
        Left = 5
        Top = 4
        Width = 70
        Height = 35
        Cursor = crHandPoint
        Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
        Caption = #49888#44508
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TabStop = False
        OnClick = btnNewClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 32
        ContentMargin = 8
      end
      object btnEdit: TsButton
        Tag = 1
        Left = 76
        Top = 4
        Width = 70
        Height = 35
        Cursor = crHandPoint
        Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
        Caption = #49688#51221
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        TabStop = False
        OnClick = btnNewClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 3
        ContentMargin = 8
      end
      object btnDel: TsButton
        Tag = 2
        Left = 147
        Top = 4
        Width = 70
        Height = 35
        Cursor = crHandPoint
        Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
        Caption = #49325#51228
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        TabStop = False
        OnClick = btnNewClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 27
        ContentMargin = 8
      end
      object btnExit: TsButton
        Left = 401
        Top = 3
        Width = 78
        Height = 35
        Cursor = crHandPoint
        Anchors = [akTop, akRight]
        Caption = #45803#44592
        TabOrder = 3
        TabStop = False
        OnClick = btnExitClick
        SkinData.SkinSection = 'TRANSPARENT'
        Reflected = True
        Images = DMICON.System26
        ImageIndex = 20
        ContentMargin = 12
      end
    end
  end
  object sPanel1: TsPanel [1]
    Left = 0
    Top = 46
    Width = 487
    Height = 379
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    DoubleBuffered = False
    TabOrder = 1
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 485
      Height = 377
      SkinData.SkinSection = 'TRANSPARENT'
      Align = alClient
      DoubleBuffered = False
      TabOrder = 0
      object sPanel6: TsPanel
        Left = 1
        Top = 41
        Width = 483
        Height = 2
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alTop
        DoubleBuffered = False
        TabOrder = 0
      end
      object sPanel8: TsPanel
        Left = 1
        Top = 1
        Width = 483
        Height = 40
        Align = alTop
        DoubleBuffered = False
        TabOrder = 1
        object edt_FindText: TsEdit
          Left = 87
          Top = 8
          Width = 141
          Height = 23
          TabOrder = 0
          OnKeyUp = edt_FindTextKeyUp
        end
        object sComboBox1: TsComboBox
          Left = 8
          Top = 8
          Width = 77
          Height = 23
          VerticalAlignment = taVerticalCenter
          Style = csOwnerDrawFixed
          ItemHeight = 17
          ItemIndex = 0
          TabOrder = 1
          Text = #51008#54665#53076#46300
          Items.Strings = (
            #51008#54665#53076#46300
            #51008#54665#47749)
        end
        object sButton1: TsButton
          Left = 229
          Top = 8
          Width = 58
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
          OnClick = sButton1Click
        end
      end
      object sDBGrid1: TsDBGrid
        Left = 1
        Top = 43
        Width = 483
        Height = 333
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            Color = clBtnFace
            Expanded = False
            FieldName = 'CODE'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = #51008#54665#53076#46300
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENAME1'
            Title.Caption = #51008#54665#47749
            Width = 170
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ENAME3'
            Title.Caption = #51648#51216#47749
            Width = 213
            Visible = True
          end>
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 176
    Top = 384
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM BANKCODE')
    Left = 176
    Top = 352
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 208
    Top = 352
  end
end
