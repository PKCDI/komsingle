unit UI_LOCAM1_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_LOCAMA_NEW, DB, ADODB, sSkinProvider, StdCtrls, sComboBox,
  sMemo, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sButton, sLabel, sSpeedButton, ExtCtrls, sPanel;

type
  TUI_LOCAM1_NEW_frm = class(TUI_LOCAMA_NEW_frm)
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_LOCAM1_NEW_frm: TUI_LOCAM1_NEW_frm;

implementation

{$R *.dfm}

procedure TUI_LOCAM1_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  ReadOnlyControl(spanel7,false);
  ReadOnlyControl(spanel27,false);

end;

end.
