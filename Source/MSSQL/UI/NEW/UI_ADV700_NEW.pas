unit UI_ADV700_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sCheckBox, sMemo, sComboBox,
  sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl,
  Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids, acDBGrid,
  sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, sSkinProvider, DB, ADODB, StrUtils, DateUtils;

type
  TUI_ADV700_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton4: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel4: TsPanel;
    sDBgrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel29: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    msk_APP_DATE: TsMaskEdit;
    sTabSheet3: TsTabSheet;
    sTabSheet2: TsTabSheet;
    sPanel34: TsPanel;
    sTabSheet7: TsTabSheet;
    sPanel102: TsPanel;
    sTabSheet5: TsTabSheet;
    sPanel35: TsPanel;
    sTabSheet6: TsTabSheet;
    sPanel71: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    edt_APPNO: TsEdit;
    edt_APBANK: TsEdit;
    edt_APBANK1: TsEdit;
    edt_APBANK2: TsEdit;
    edt_APBANK3: TsEdit;
    edt_APBANK4: TsEdit;
    edt_ADBANK1: TsEdit;
    edt_ADBANK2: TsEdit;
    edt_ADBANK3: TsEdit;
    edt_ADBANK4: TsEdit;
    sPanel2: TsPanel;
    sPanel8: TsPanel;
    sPanel10: TsPanel;
    sPanel9: TsPanel;
    sPanel11: TsPanel;
    sPanel12: TsPanel;
    sPanel126: TsPanel;
    edt_ADBANK: TsEdit;
    sPanel127: TsPanel;
    sPanel128: TsPanel;
    memo_ADDINFO1: TsMemo;
    sPanel129: TsPanel;
    edt_ADBANK5: TsEdit;
    sPanel130: TsPanel;
    sEdit14: TsEdit;
    sPanel27: TsPanel;
    sPanel15: TsPanel;
    sPanel1: TsPanel;
    edt_DOCCD1: TsEdit;
    sPanel13: TsPanel;
    sPanel14: TsPanel;
    edt_CDNO: TsEdit;
    sPanel16: TsPanel;
    sPanel17: TsPanel;
    edt_APPLICABLERULES1: TsEdit;
    edt_APPLICABLERULES2: TsEdit;
    sPanel18: TsPanel;
    sPanel19: TsPanel;
    edt_REEPRE: TsEdit;
    sPanel21: TsPanel;
    sPanel22: TsPanel;
    msk_ISSDATE: TsMaskEdit;
    sPanel23: TsPanel;
    sPanel25: TsPanel;
    edt_EXPLACE: TsEdit;
    sPanel26: TsPanel;
    sPanel28: TsPanel;
    edt_APPBANK: TsEdit;
    edt_APPBANK1: TsEdit;
    sPanel30: TsPanel;
    edt_APPBANK2: TsEdit;
    edt_APPBANK3: TsEdit;
    edt_APPBANK4: TsEdit;
    edt_APPACCNT: TsEdit;
    sPanel31: TsPanel;
    sPanel32: TsPanel;
    sPanel33: TsPanel;
    edt_APPLIC1: TsEdit;
    edt_APPLIC2: TsEdit;
    edt_APPLIC3: TsEdit;
    edt_APPLIC4: TsEdit;
    sPanel36: TsPanel;
    sPanel37: TsPanel;
    edt_CDCUR: TsEdit;
    sPanel38: TsPanel;
    sPanel40: TsPanel;
    edt_cdPerP: TsCurrencyEdit;
    edt_cdPerM: TsCurrencyEdit;
    sPanel39: TsPanel;
    sPanel41: TsPanel;
    edt_BENEFC1: TsEdit;
    edt_BENEFC2: TsEdit;
    edt_BENEFC3: TsEdit;
    edt_BENEFC4: TsEdit;
    edt_BENEFC5: TsEdit;
    sPanel42: TsPanel;
    sPanel43: TsPanel;
    sPanel44: TsPanel;
    edt_AACV1: TsEdit;
    edt_AACV2: TsEdit;
    edt_AACV3: TsEdit;
    edt_AACV4: TsEdit;
    sPanel45: TsPanel;
    sPanel46: TsPanel;
    sPanel47: TsPanel;
    edt_DRAFT1: TsEdit;
    edt_DRAFT2: TsEdit;
    edt_DRAFT3: TsEdit;
    edt_DRAFT4: TsEdit;
    sPanel48: TsPanel;
    sPanel49: TsPanel;
    sPanel50: TsPanel;
    sPanel51: TsPanel;
    edt_AVPAY: TsEdit;
    sPanel52: TsPanel;
    sPanel53: TsPanel;
    edt_AVAIL: TsEdit;
    sPanel54: TsPanel;
    edt_AVAIL1: TsEdit;
    edt_AVAIL2: TsEdit;
    edt_AVAIL3: TsEdit;
    edt_AVAIL4: TsEdit;
    sPanel55: TsPanel;
    edt_AVACCNT: TsEdit;
    sPanel56: TsPanel;
    sPanel57: TsPanel;
    sPanel58: TsPanel;
    sPanel61: TsPanel;
    edt_DRAWEE: TsEdit;
    sPanel62: TsPanel;
    edt_DRAWEE1: TsEdit;
    edt_DRAWEE2: TsEdit;
    edt_DRAWEE3: TsEdit;
    edt_DRAWEE4: TsEdit;
    sPanel63: TsPanel;
    edt_DRACCNT: TsEdit;
    sPanel60: TsPanel;
    sPanel64: TsPanel;
    edt_MIXPAY1: TsEdit;
    edt_MIXPAY2: TsEdit;
    edt_MIXPAY3: TsEdit;
    edt_MIXPAY4: TsEdit;
    sPanel65: TsPanel;
    sPanel66: TsPanel;
    sPanel67: TsPanel;
    edt_DEFPAY1: TsEdit;
    edt_DEFPAY2: TsEdit;
    edt_DEFPAY3: TsEdit;
    edt_DEFPAY4: TsEdit;
    sPanel68: TsPanel;
    sPanel69: TsPanel;
    edt_PSHIP: TsEdit;
    sPanel70: TsPanel;
    sPanel72: TsPanel;
    edt_TSHIP: TsEdit;
    sPanel73: TsPanel;
    sPanel74: TsPanel;
    msk_LSTDATE: TsMaskEdit;
    sPanel75: TsPanel;
    sPanel76: TsPanel;
    edt_LOADON: TsEdit;
    sPanel77: TsPanel;
    sPanel78: TsPanel;
    sPanel79: TsPanel;
    edt_FORTRAN: TsEdit;
    sPanel80: TsPanel;
    sPanel81: TsPanel;
    sPanel82: TsPanel;
    edt_SUNJUCKPORT: TsEdit;
    sPanel83: TsPanel;
    sPanel84: TsPanel;
    sPanel85: TsPanel;
    edt_DOCHACKPORT: TsEdit;
    sPanel86: TsPanel;
    sPanel87: TsPanel;
    sPanel88: TsPanel;
    sPanel90: TsPanel;
    edt_SHIPPD1: TsMemo;
    sPanel91: TsPanel;
    sPanel92: TsPanel;
    sPanel93: TsPanel;
    memo_DESGOOD1: TsMemo;
    sPanel94: TsPanel;
    sPanel95: TsPanel;
    sPanel96: TsPanel;
    memo_DOCREQU1: TsMemo;
    sPanel97: TsPanel;
    sPanel98: TsPanel;
    sPanel99: TsPanel;
    memo_ADDCOND1: TsMemo;
    sPanel100: TsPanel;
    sPanel101: TsPanel;
    edt_CHARGE1: TsEdit;
    edt_CHARGE2: TsEdit;
    edt_CHARGE3: TsEdit;
    sPanel103: TsPanel;
    sPanel104: TsPanel;
    sPanel105: TsPanel;
    sPanel106: TsPanel;
    edt_PERIOD_IN_DAYS: TsEdit;
    sPanel107: TsPanel;
    edt_PERIOD_DETAIL: TsEdit;
    sPanel108: TsPanel;
    sPanel109: TsPanel;
    edt_CONFIRMM: TsEdit;
    sPanel110: TsPanel;
    sPanel111: TsPanel;
    sPanel112: TsPanel;
    sPanel113: TsPanel;
    edt_REIBANK: TsEdit;
    sPanel114: TsPanel;
    edt_REIBANK1: TsEdit;
    edt_REIBANK2: TsEdit;
    edt_REIBANK3: TsEdit;
    edt_REIBANK4: TsEdit;
    sPanel115: TsPanel;
    edt_REIACCMT: TsEdit;
    sPanel116: TsPanel;
    sPanel117: TsPanel;
    sPanel118: TsPanel;
    sPanel119: TsPanel;
    edt_REQ_BANK: TsEdit;
    sPanel120: TsPanel;
    edt_REQ_BANK1: TsEdit;
    edt_REQ_BANK2: TsEdit;
    edt_REQ_BANK3: TsEdit;
    edt_REQ_BANK4: TsEdit;
    sPanel121: TsPanel;
    sPanel122: TsPanel;
    sPanel123: TsPanel;
    memo_INSTRCT1: TsMemo;
    sTabSheet8: TsTabSheet;
    sPanel124: TsPanel;
    sPanel125: TsPanel;
    sPanel134: TsPanel;
    sPanel135: TsPanel;
    sPanel136: TsPanel;
    edt_AVTBANK: TsEdit;
    sPanel137: TsPanel;
    edt_AVTBANK1: TsEdit;
    edt_AVTBANK2: TsEdit;
    edt_AVTBANK3: TsEdit;
    edt_AVTBANK4: TsEdit;
    sPanel138: TsPanel;
    edt_AVTACCNT: TsEdit;
    sPanel139: TsPanel;
    sPanel140: TsPanel;
    edt_SNDINFO1: TsEdit;
    edt_SNDINFO2: TsEdit;
    edt_SNDINFO3: TsEdit;
    sPanel141: TsPanel;
    edt_SNDINFO4: TsEdit;
    edt_SNDINFO5: TsEdit;
    edt_SNDINFO6: TsEdit;
    edt_EXNAME1: TsEdit;
    edt_EXNAME2: TsEdit;
    edt_EXNAME3: TsEdit;
    edt_EXADDR1: TsEdit;
    sPanel59: TsPanel;
    edt_EXADDR2: TsEdit;
    sPanel132: TsPanel;
    sPanel89: TsPanel;
    sPanel131: TsPanel;
    sPanel133: TsPanel;
    qryList: TADOQuery;
    dsList: TDataSource;
    msk_EXDATE: TsMaskEdit;
    qryListMAINT_NO: TStringField;
    qryListChk1: TBooleanField;
    qryListChk2: TStringField;
    qryListChk3: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListAPP_NO: TStringField;
    qryListAP_BANK: TStringField;
    qryListAP_BANK1: TStringField;
    qryListAP_BANK2: TStringField;
    qryListAP_BANK3: TStringField;
    qryListAP_BANK4: TStringField;
    qryListAP_BANK5: TStringField;
    qryListAD_BANK: TStringField;
    qryListAD_BANK1: TStringField;
    qryListAD_BANK2: TStringField;
    qryListAD_BANK3: TStringField;
    qryListAD_BANK4: TStringField;
    qryListAD_BANK5: TStringField;
    qryListADDINFO: TStringField;
    qryListADDINFO_1: TMemoField;
    qryListDOC_CD1: TStringField;
    qryListDOC_CD2: TStringField;
    qryListDOC_CD3: TStringField;
    qryListCD_NO: TStringField;
    qryListREE_PRE: TStringField;
    qryListISS_DATE: TStringField;
    qryListEX_DATE: TStringField;
    qryListEX_PLACE: TStringField;
    qryListAPP_BANK: TStringField;
    qryListAPP_BANK1: TStringField;
    qryListAPP_BANK2: TStringField;
    qryListAPP_BANK3: TStringField;
    qryListAPP_BANK4: TStringField;
    qryListAPP_BANK5: TStringField;
    qryListAPP_ACCNT: TStringField;
    qryListAPPLIC1: TStringField;
    qryListAPPLIC2: TStringField;
    qryListAPPLIC3: TStringField;
    qryListAPPLIC4: TStringField;
    qryListAPPLIC5: TStringField;
    qryListBENEFC1: TStringField;
    qryListBENEFC2: TStringField;
    qryListBENEFC3: TStringField;
    qryListBENEFC4: TStringField;
    qryListBENEFC5: TStringField;
    qryListCD_AMT: TBCDField;
    qryListCD_CUR: TStringField;
    qryListCD_PERP: TBCDField;
    qryListCD_PERM: TBCDField;
    qryListCD_MAX: TStringField;
    qryListAA_CV1: TStringField;
    qryListAA_CV2: TStringField;
    qryListAA_CV3: TStringField;
    qryListAA_CV4: TStringField;
    qryListBENEFC6: TStringField;
    qryListPRNO: TIntegerField;
    qryListMAINT_NO_1: TStringField;
    qryListAVAIL: TStringField;
    qryListAVAIL1: TStringField;
    qryListAVAIL2: TStringField;
    qryListAVAIL3: TStringField;
    qryListAVAIL4: TStringField;
    qryListAVAIL5: TStringField;
    qryListAV_ACCNT: TStringField;
    qryListAV_PAY: TStringField;
    qryListDRAFT1: TStringField;
    qryListDRAFT2: TStringField;
    qryListDRAFT3: TStringField;
    qryListDRAFT4: TStringField;
    qryListDRAWEE: TStringField;
    qryListDRAWEE1: TStringField;
    qryListDRAWEE2: TStringField;
    qryListDRAWEE3: TStringField;
    qryListDRAWEE4: TStringField;
    qryListDRAWEE5: TStringField;
    qryListDR_ACCNT: TStringField;
    qryListMIX_PAY1: TStringField;
    qryListMIX_PAY2: TStringField;
    qryListMIX_PAY3: TStringField;
    qryListMIX_PAY4: TStringField;
    qryListDEF_PAY1: TStringField;
    qryListDEF_PAY2: TStringField;
    qryListDEF_PAY3: TStringField;
    qryListDEF_PAY4: TStringField;
    qryListLST_DATE: TStringField;
    qryListSHIP_PD: TStringField;
    qryListSHIP_PD1: TStringField;
    qryListSHIP_PD2: TStringField;
    qryListSHIP_PD3: TStringField;
    qryListSHIP_PD4: TStringField;
    qryListSHIP_PD5: TStringField;
    qryListSHIP_PD6: TStringField;
    qryListMAINT_NO_2: TStringField;
    qryListDESGOOD: TStringField;
    qryListDESGOOD_1: TMemoField;
    qryListDOCREQU: TStringField;
    qryListDOCREQU_1: TMemoField;
    qryListADDCOND: TStringField;
    qryListADDCOND_1: TMemoField;
    qryListCHARGE1: TStringField;
    qryListCHARGE2: TStringField;
    qryListCHARGE3: TStringField;
    qryListCHARGE4: TStringField;
    qryListCHARGE5: TStringField;
    qryListCHARGE6: TStringField;
    qryListPD_PRSNT1: TStringField;
    qryListPD_PRSNT2: TStringField;
    qryListPD_PRSNT3: TStringField;
    qryListPD_PRSNT4: TStringField;
    qryListCONFIRMM: TStringField;
    qryListREI_BANK: TStringField;
    qryListREI_BANK1: TStringField;
    qryListREI_BANK2: TStringField;
    qryListREI_BANK3: TStringField;
    qryListREI_BANK4: TStringField;
    qryListREI_BANK5: TStringField;
    qryListREI_ACCNT: TStringField;
    qryListINSTRCT: TStringField;
    qryListINSTRCT_1: TMemoField;
    qryListAVT_BANK: TStringField;
    qryListAVT_BANK1: TStringField;
    qryListAVT_BANK2: TStringField;
    qryListAVT_BANK3: TStringField;
    qryListAVT_BANK4: TStringField;
    qryListAVT_BANK5: TStringField;
    qryListAVT_ACCNT: TStringField;
    qryListSND_INFO1: TStringField;
    qryListSND_INFO2: TStringField;
    qryListSND_INFO3: TStringField;
    qryListSND_INFO4: TStringField;
    qryListSND_INFO5: TStringField;
    qryListSND_INFO6: TStringField;
    qryListEX_NAME1: TStringField;
    qryListEX_NAME2: TStringField;
    qryListEX_NAME3: TStringField;
    qryListEX_ADDR1: TStringField;
    qryListEX_ADDR2: TStringField;
    qryListCHK1_1: TStringField;
    qryListCHK2_1: TStringField;
    qryListCHK3_1: TStringField;
    qryListPRNO_1: TIntegerField;
    qryListMAINT_NO_3: TStringField;
    qryListFOR_TRAN: TStringField;
    qryListLOAD_ON: TStringField;
    qryListTSHIP: TStringField;
    qryListPSHIP: TStringField;
    qryListAPPLICABLE_RULES_1: TStringField;
    qryListAPPLICABLE_RULES_2: TStringField;
    qryListSUNJUCK_PORT: TStringField;
    qryListDOCHACK_PORT: TStringField;
    qryListPERIOD_IN_DAYS: TIntegerField;
    qryListPERIOD_DETAIL: TStringField;
    qryListREQ_BANK: TStringField;
    qryListREQ_BANK1: TStringField;
    qryListREQ_BANK2: TStringField;
    qryListREQ_BANK3: TStringField;
    qryListREQ_BANK4: TStringField;
    sPanel142: TsPanel;
    edt_BENEFC6: TsEdit;
    sPanel6: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    sPanel152: TsPanel;
    sPanel153: TsPanel;
    sPanel154: TsPanel;
    memo_Special: TsMemo;
    qryListPD_PRSNT5: TStringField;
    qryListCO_BANK: TStringField;
    qryListCO_BANK1: TStringField;
    qryListCO_BANK2: TStringField;
    qryListCO_BANK3: TStringField;
    qryListCO_BANK4: TStringField;
    qryListSPECIAL_DESC: TStringField;
    qryListSPECIAL_DESC_1: TMemoField;
    curr_CDAMT: TsCurrencyEdit;
    procedure sMaskEdit1Change(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sPageControl1Change(Sender: TObject);
    procedure edt_SearchNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  protected
    procedure ReadList(SortField: TColumn=nil); virtual;
    procedure ReadData; virtual;
    { Private declarations }


  public
    { Public declarations }
  end;

var
  UI_ADV700_NEW_frm: TUI_ADV700_NEW_frm;

implementation

uses
  TypeDefine, Preview, ADV700_PRINT, MSSQL, MessageDefine;

{$R *.dfm}

procedure TUI_ADV700_NEW_frm.ReadList(SortField: TColumn);
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'ADV700.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;
    Open;
  end;
end;

procedure TUI_ADV700_NEW_frm.sMaskEdit1Change(Sender: TObject);
begin
  inherited;
  if ActiveControl = nil then
    Exit;

  case AnsiIndexText(ActiveControl.Name, ['sMaskEdit1', 'sMaskEdit2', 'sMaskEdit3', 'sMaskEdit4', 'edt_SearchNo', 'sEdit1']) of
    0:
      sMaskEdit3.Text := sMaskEdit1.Text;
    1:
      sMaskEdit4.Text := sMaskEdit2.Text;
    2:
      sMaskEdit1.Text := sMaskEdit3.Text;
    3:
      sMaskEdit2.Text := sMaskEdit4.Text;
    4:
      sEdit1.Text := edt_SearchNo.Text;
    5:
      edt_SearchNo.Text := sEdit1.Text;
  end;
end;

procedure TUI_ADV700_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  IF ProgramControlType = ctView Then
    ReadList;
end;

procedure TUI_ADV700_NEW_frm.ReadData;
begin
  //관리번호
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  //수신일자
  msk_Datee.Text := qryListDATEE.AsString;
  //사용자
  edt_UserNo.Text := qryListUSER_ID.AsString;
  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

  //통지일자
  msk_APP_DATE.Text := qryListAPP_DATE.AsString;
  //통지번호
  edt_APPNO.Text := qryListAPP_NO.AsString;
  //개설은행
  edt_APBANK.Text := qryListAP_BANK.AsString;
  edt_APBANK1.Text := qryListAP_BANK1.AsString;
  edt_APBANK2.Text := qryListAP_BANK2.AsString;
  edt_APBANK3.Text := qryListAP_BANK3.AsString;
  edt_APBANK4.Text := qryListAP_BANK4.AsString;
  //통지은행
  edt_ADBANK.Text := qryListAD_BANK.AsString;
  edt_ADBANK1.Text := qryListAD_BANK1.AsString;
  edt_ADBANK2.Text := qryListAD_BANK2.AsString;
  edt_ADBANK3.Text := qryListAD_BANK3.AsString;
  edt_ADBANK4.Text := qryListAD_BANK4.AsString;
  edt_ADBANK5.Text := qryListAD_BANK5.AsString;
  //기타정보(부가정보)
  memo_ADDINFO1.Lines.Text := qryListADDINFO_1.AsString;
  //신용장
  edt_DOCCD1.Text := qryListDOC_CD1.AsString;
//  edt_DOCCD2.Text := qryListDOC_CD2.AsString;
//  edt_DOCCD3.Text := qryListDOC_CD3.AsString;
  edt_CDNO.Text := qryListCD_NO.AsString;
  //Applicable Rules
  edt_APPLICABLERULES1.Text := qryListAPPLICABLE_RULES_1.AsString;
  edt_APPLICABLERULES2.Text := qryListAPPLICABLE_RULES_2.AsString;
  //선통지 참조사항
  edt_REEPRE.Text := qryListREE_PRE.AsString;
  //개설일자
  msk_ISSDATE.Text := qryListISS_DATE.AsString;
  //유효기일
  msk_EXDATE.Text := qryListEX_DATE.AsString;
  //유효장소
  edt_EXPLACE.Text := qryListEX_PLACE.AsString;
  //개설의뢰인은행
  edt_APPBANK.Text := qryListAPP_BANK.AsString;
  edt_APPBANK1.Text := qryListAPP_BANK1.AsString;
  edt_APPBANK2.Text := qryListAPP_BANK2.AsString;
  edt_APPBANK3.Text := qryListAPP_BANK3.AsString;
  edt_APPBANK4.Text := qryListAPP_BANK4.AsString;
  edt_APPACCNT.Text := qryListAPP_ACCNT.AsString;
  //개설의뢰인
  edt_APPLIC1.Text := qryListAPPLIC1.AsString;
  edt_APPLIC2.Text := qryListAPPLIC2.AsString;
  edt_APPLIC3.Text := qryListAPPLIC3.AsString;
  edt_APPLIC4.Text := qryListAPPLIC4.AsString;
  //수익자
  edt_BENEFC1.Text := qryListBENEFC1.AsString;
  edt_BENEFC2.Text := qryListBENEFC2.AsString;
  edt_BENEFC3.Text := qryListBENEFC3.AsString;
  edt_BENEFC4.Text := qryListBENEFC4.AsString;
  edt_BENEFC5.Text := qryListBENEFC5.AsString;
  edt_BENEFC6.Text := qryListBENEFC6.AsString;
  
  //금액정보
  edt_CDCUR.Text := qryListCD_CUR.AsString;
  curr_CDAMT.Value := qryListCD_AMT.AsCurrency;
  edt_cdPerP.Text := qryListCD_PERP.AsString;
  edt_cdPerM.Text := qryListCD_PERM.AsString;

  //신용장 지급방식및 은행
  edt_AVPAY.Text := qryListAV_PAY.AsString;
  edt_AVAIL.Text := qryListAVAIL.AsString;
  edt_AVAIL1.Text := qryListAVAIL1.AsString;
  edt_AVAIL2.Text := qryListAVAIL2.AsString;
  edt_AVAIL3.Text := qryListAVAIL3.AsString;
  edt_AVAIL4.Text := qryListAVAIL4.AsString;
  edt_AVACCNT.Text := qryListAV_ACCNT.AsString;
  //부가금액부담
  edt_AACV1.Text := qryListAA_CV1.AsString;
  edt_AACV2.Text := qryListAA_CV2.AsString;
  edt_AACV3.Text := qryListAA_CV3.AsString;
  edt_AACV4.Text := qryListAA_CV4.AsString;
  //화환어음 조건
  edt_DRAFT1.Text := qryListDRAFT1.AsString;
  edt_DRAFT2.Text := qryListDRAFT2.AsString;
  edt_DRAFT3.Text := qryListDRAFT3.AsString;
  edt_DRAFT4.Text := qryListDRAFT4.AsString;
  //혼합지급조건명세
  edt_MIXPAY1.Text := qryListMIX_PAY1.AsString;
  edt_MIXPAY2.Text := qryListMIX_PAY2.AsString;
  edt_MIXPAY3.Text := qryListMIX_PAY3.AsString;
  edt_MIXPAY4.Text := qryListMIX_PAY4.AsString;
  //연지금조건명세
  edt_DEFPAY1.Text := qryListDEF_PAY1.AsString;
  edt_DEFPAY2.Text := qryListDEF_PAY2.AsString;
  edt_DEFPAY3.Text := qryListDEF_PAY3.AsString;
  edt_DEFPAY4.Text := qryListDEF_PAY4.AsString;
  //어음지급인
  edt_DRAWEE.Text := qryListDRAWEE.AsString;
  edt_DRAWEE1.Text := qryListDRAWEE1.AsString;
  edt_DRAWEE2.Text := qryListDRAWEE2.AsString;
  edt_DRAWEE3.Text := qryListDRAWEE3.AsString;
  edt_DRAWEE4.Text := qryListDRAWEE4.AsString;
  edt_DRACCNT.Text := qryListDR_ACCNT.AsString;
  //선적정보
  edt_PSHIP.Text := qryListPSHIP.AsString;
  edt_TSHIP.Text := qryListTSHIP.AsString;
  edt_LOADON.Text := qryListLOAD_ON.AsString;
  edt_FORTRAN.Text := qryListFOR_TRAN.AsString;
  edt_SUNJUCKPORT.Text := qryListSUNJUCK_PORT.AsString;
  edt_DOCHACKPORT.Text := qryListDOCHACK_PORT.AsString;
  msk_LSTDATE.Text := qryListLST_DATE.AsString;
  //선적기간
  edt_SHIPPD1.Text := qryListSHIP_PD1.AsString+#13#10+
                      qryListSHIP_PD2.AsString+#13#10+
                      qryListSHIP_PD3.AsString+#13#10+
                      qryListSHIP_PD4.AsString+#13#10+
                      qryListSHIP_PD5.AsString+#13#10+
                      qryListSHIP_PD6.AsString;
  //수수료부담자
  edt_CHARGE1.Text := qryListCHARGE1.AsString;
  edt_CHARGE2.Text := qryListCHARGE2.AsString;
  edt_CHARGE3.Text := qryListCHARGE3.AsString;
//  edt_CHARGE4.Text := qryListCHARGE4.AsString;
//  edt_CHARGE5.Text := qryListCHARGE5.AsString;
//  edt_CHARGE6.Text := qryListCHARGE6.AsString;
  //서류제시기간
  edt_PERIOD_IN_DAYS.Text := qryListPERIOD_IN_DAYS.AsString;
  edt_PERIOD_DETAIL.Text := qryListPERIOD_DETAIL.AsString;
//  edt_PDPRSNT1.Text := qryListPD_PRSNT1.AsString;
//  edt_PDPRSNT2.Text := qryListPD_PRSNT2.AsString;
//  edt_PDPRSNT3.Text := qryListPD_PRSNT3.AsString;
//  edt_PDPRSNT4.Text := qryListPD_PRSNT4.AsString;
  //확인지시문언
  edt_CONFIRMM.Text := qryListCONFIRMM.AsString;
  //확인은행
  edt_REQ_BANK.Text  := qryListREQ_BANK.AsString;
  edt_REQ_BANK1.Text := qryListREQ_BANK1.AsString;
  edt_REQ_BANK2.Text := qryListREQ_BANK2.AsString;
  edt_REQ_BANK3.Text := qryListREQ_BANK3.AsString;
  edt_REQ_BANK4.Text := qryListREQ_BANK4.AsString;
  //상환은행
  edt_REIBANK.Text := qryListREI_BANK.AsString;
  edt_REIBANK1.Text := qryListREI_BANK1.AsString;
  edt_REIBANK2.Text := qryListREI_BANK2.AsString;
  edt_REIBANK3.Text := qryListREI_BANK3.AsString;
  edt_REIBANK4.Text := qryListREI_BANK4.AsString;
  edt_REIACCMT.Text := qryListREI_ACCNT.AsString;
  //상품(용역)명세
  memo_DESGOOD1.Lines.Text := qryListDESGOOD_1.AsString;
  //구비서류
  memo_DOCREQU1.Lines.Text := qryListDOCREQU_1.AsString;
  //부가조건
  memo_ADDCOND1.Lines.Text := qryListADDCOND_1.AsString;
  //매입/지급/인수은행에 대한 지시사항
  memo_INSTRCT1.Lines.Text := qryListINSTRCT_1.AsString;
  //최종통지은행
  edt_AVTBANK.Text := qryListAVT_BANK.AsString;
  edt_AVTBANK1.Text := qryListAVT_BANK1.AsString;
  edt_AVTBANK2.Text := qryListAVT_BANK2.AsString;
  edt_AVTBANK3.Text := qryListAVT_BANK3.AsString;
  edt_AVTBANK4.Text := qryListAVT_BANK4.AsString;
  edt_AVTACCNT.Text := qryListAVT_ACCNT.AsString;
  //통지은행
  edt_EXNAME1.Text := qryListEX_NAME1.AsString;
  edt_EXNAME2.Text := qryListEX_NAME2.AsString;
  edt_EXNAME3.Text := qryListEX_NAME3.AsString;
  edt_EXADDR1.Text := qryListEX_ADDR1.AsString;
  edt_EXADDR2.Text := qryListEX_ADDR2.AsString;
  //수신앞은행정보
  edt_SNDINFO1.Text := qryListSND_INFO1.AsString;
  edt_SNDINFO2.Text := qryListSND_INFO2.AsString;
  edt_SNDINFO3.Text := qryListSND_INFO3.AsString;
  edt_SNDINFO4.Text := qryListSND_INFO4.AsString;
  edt_SNDINFO5.Text := qryListSND_INFO5.AsString;
  edt_SNDINFO6.Text := qryListSND_INFO6.AsString;

  //수익자 특별지급조건
  memo_Special.Text := qryListSPECIAL_DESC_1.AsString;

end;

procedure TUI_ADV700_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_ADV700_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
end;

procedure TUI_ADV700_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  sMaskEdit3.Text := sMaskEdit1.Text;
  sMaskEdit4.Text := sMaskEdit2.Text;

  sBitBtn1Click(nil);

//  ReadOnlyControl(sPanel6,false);
//  ReadOnlyControl(sPanel7,false);
//  ReadOnlyControl(sPanel27,false);
//  ReadOnlyControl(sPanel34,false);
//  ReadOnlyControl(sPanel102,false);
//  ReadOnlyControl(sPanel35,false);
//  ReadOnlyControl(sPanel71,false);
//  ReadOnlyControl(sPanel124,false);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_ADV700_NEW_frm.btnPrintClick(Sender: TObject);
begin
//  inherited;
  Preview_frm := TPreview_frm.Create(Self);
  ADV700_PRINT_frm := TADV700_PRINT_frm.Create(Self);
  try
    ADV700_PRINT_frm.MaintNo := qryListMAINT_NO.AsString;
    Preview_frm.Report := ADV700_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(ADV700_PRINT_frm);
  end;
end;

procedure TUI_ADV700_NEW_frm.btnDelClick(Sender: TObject);
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := qryListMAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+'수출신용장통지서:'#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM ADV700  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV7001  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV7002  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV7003  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM ADV700_D WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제오류',MB_OK+MB_ICONERROR);
      end;
    end;

   finally

    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;

    Close;
    Free;
   end;
  end;

end;

procedure TUI_ADV700_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_ADV700_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_ADV700_NEW_frm := nil;
end;

procedure TUI_ADV700_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 7;
end;

procedure TUI_ADV700_NEW_frm.edt_SearchNoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN then sBitBtn1Click(sBitBtn1);
end;

end.

