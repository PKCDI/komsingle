unit UI_APPCOP_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, acAlphaHints, sSkinProvider, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, Buttons, sBitBtn, Grids, DBGrids, acDBGrid,
  ComCtrls, sPageControl, StdCtrls, sComboBox, Mask, sMaskEdit, sEdit,
  sButton, sCheckBox, ExtCtrls, acImage, sLabel, sSpeedButton, sPanel,
  sMemo, DB, ADODB, CodeDialogParent, StrUtils, DateUtils, Clipbrd, Math;

type
  TUI_APPCOP_NEW_frm = class(TChildForm_frm)
    sAlphaHints1: TsAlphaHints;
    btn_Panel: TsPanel;
    sPanel8: TsPanel;
    sSpeedButton1: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel1: TsLabel;
    sImage2: TsImage;
    sCheckBox1: TsCheckBox;
    sPanel10: TsPanel;
    sSpeedButton6: TsSpeedButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    btnExit: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel7: TsPanel;
    edtMAINT_NO: TsEdit;
    mskDATEE: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edtUSER_ID: TsEdit;
    sPanel1: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sDBGrid1: TsDBGrid;
    sPanel18: TsPanel;
    sLabel2: TsLabel;
    sPanel3: TsPanel;
    sSpeedButton9: TsSpeedButton;
    mskDate1: TsMaskEdit;
    mskDate2: TsMaskEdit;
    btnFind: TsBitBtn;
    comFind: TsComboBox;
    edtFind: TsEdit;
    sPanel2: TsPanel;
    sTabSheet2: TsTabSheet;
    sPanel4: TsPanel;
    sDBGrid2: TsDBGrid;
    sPanel19: TsPanel;
    sLabel3: TsLabel;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    mskOAPP_DATE: TsMaskEdit;
    mskOTRM_DATE: TsMaskEdit;
    mskHS_NO: TsMaskEdit;
    edtMessage1: TsEdit;
    sEdit3: TsEdit;
    edtBUS_G: TsEdit;
    sEdit5: TsEdit;
    edtAMTC: TsEdit;
    currAMT: TsCurrencyEdit;
    edtMS_NAME1: TsEdit;
    edtBUS_D: TsEdit;
    edtMS_CODE: TsEdit;
    edtMS_SAUP: TsEdit;
    edtMS_TEL: TsEdit;
    edtAX_NAME3: TsEdit;
    edtCT_NO: TsEdit;
    sImage3: TsImage;
    edtAFY_NO: TsEdit;
    edtMS_NAME2: TsEdit;
    edtMS_NAME3: TsEdit;
    edtMS_ADDR1: TsEdit;
    edtMS_ADDR3: TsEdit;
    edtMS_ADDR2: TsEdit;
    edtSE_CODE: TsEdit;
    edtSE_NAME1: TsEdit;
    edtSE_NAME2: TsEdit;
    edtSE_NAME3: TsEdit;
    edtGOODS_CD: TsEdit;
    edtGOODS1: TsEdit;
    edtGOODS2: TsEdit;
    edtGOODS3: TsEdit;
    edtGOODS4: TsEdit;
    edtGOODS5: TsEdit;
    mskOCFM_DATE: TsMaskEdit;
    mskNTRM_DATE: TsMaskEdit;
    mskNAPP_DATE: TsMaskEdit;
    sPageControl2: TsPageControl;
    sTabSheet3: TsTabSheet;
    sTabSheet4: TsTabSheet;
    sPanel9: TsPanel;
    sDBGrid3: TsDBGrid;
    sPanel11: TsPanel;
    btnDetailADD: TsButton;
    btnDetailEdit: TsButton;
    btnDetailDel: TsButton;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListMESSAGE3: TStringField;
    qryListCT_NO: TStringField;
    qryListAFY_NO: TStringField;
    qryListMS_CODE: TStringField;
    qryListMS_SAUP: TStringField;
    qryListMS_NAME1: TStringField;
    qryListMS_NAME2: TStringField;
    qryListMS_NAME3: TStringField;
    qryListMS_ADDR1: TStringField;
    qryListMS_ADDR2: TStringField;
    qryListMS_ADDR3: TStringField;
    qryListMS_TEL: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListSE_CODE: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_NAME3: TStringField;
    qryListAMT: TBCDField;
    qryListAMT_C: TStringField;
    qryListREMARK1: TMemoField;
    qryListGOODS_CD: TStringField;
    qryListGOODS1: TStringField;
    qryListGOODS2: TStringField;
    qryListGOODS3: TStringField;
    qryListGOODS4: TStringField;
    qryListGOODS5: TStringField;
    qryListHS_NO: TStringField;
    qryListBUS_G: TStringField;
    qryListBUS_D: TStringField;
    qryListOTRM_DATE: TStringField;
    qryListNTRM_DATE: TStringField;
    qryListOAPP_DATE: TStringField;
    qryListNAPP_DATE: TStringField;
    qryListOCFM_DATE: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListMESSAGE1_NM: TStringField;
    qryListBUS_G_NM: TStringField;
    memoREMARK1: TsMemo;
    sPanelMemo: TsPanel;
    qryDetail: TADOQuery;
    dsDetail: TDataSource;
    qryDetailKEYY: TStringField;
    qryDetailIMP_NO: TStringField;
    qryDetailBANK_CD: TStringField;
    qryDetailD_AMTC: TStringField;
    qryDetailD_AMT: TBCDField;
    qryDetailTERMS: TStringField;
    qryDetailIMP_CD: TStringField;
    qryDetailSEQ: TBCDField;
    qryReady: TADOQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure edtMessage1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtFindKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnFindClick(Sender: TObject);
    procedure edtMS_CODEDblClick(Sender: TObject);
    procedure edtGOODS_CDDblClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure sDBGrid1TitleClick(Column: TColumn);
    procedure btnSaveClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure btnDetailADDClick(Sender: TObject);
    procedure btnDetailDelClick(Sender: TObject);
    procedure edtMessage1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMessage1Change(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    CDDialog : TCodeDialogParent_frm;
    procedure getData;
    function chkDocOver(DocNo : String):Boolean;    
    function ReadList(SortCol: TColumn = nil): Integer;
    function ReadDetail:Integer;
    procedure NumberSort(Maint_No : String);
    procedure ClearColTitle(igoreCol: String);
    procedure NewDocument;
    procedure BtnControl;
    procedure showHintControl(Control: TControl; msg: String;
      showDelay: integer=0);
    procedure EditDocument;
    procedure PanelControl;
    procedure ReadyDocument(DocNo: String);
  public
    { Public declarations }
  end;

var
  UI_APPCOP_NEW_frm: TUI_APPCOP_NEW_frm;

implementation

uses
  MSSQL, ICON, CD_APPCOP_GUBUN, CD_AMT_UNIT, CD_APPCOP_JIGEUP, CD_CUST, CD_ITEMLIST,
  AutoNo, VarDefine, CodeContents, SQLCreator, Commonlib, MessageDefine, dlgAPPCOP_D, Preview,
  QR_APPCOP_PRN, Dlg_RecvSelect, CreateDocuments, DocumentSend;

{$R *.dfm}

procedure TUI_APPCOP_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_APPCOP_NEW_frm := nil;
end;

procedure TUI_APPCOP_NEW_frm.getData;
begin
//------------------------------------------------------------------------------
// HEADER
//------------------------------------------------------------------------------
  // 관리번호
  edtMAINT_NO.Text := qryListMAINT_NO.AsString;
  // 사용자
  edtUSER_ID.Text := qryListUSER_ID.AsString;
  // 등록일자
  mskDATEE.Text := qryListDATEE.AsString;
  CM_INDEX(com_func, [':'], 0, qryListMESSAGE2.AsString, 0);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE3.AsString);
//------------------------------------------------------------------------------
// Common
//------------------------------------------------------------------------------
  edtMessage1.Text := qryListMESSAGE1.AsString;
  sEdit3.Text := qryListMESSAGE1_NM.AsString;
  edtBUS_G.Text := qryListBUS_G.AsString;
  sEdit5.Text := qryListBUS_G_NM.AsString;
  edtBUS_D.Text := qryListBUS_D.AsString;
  edtAMTC.Text := qryListAMT_C.AsString;
  currAMT.Value := qrylistamt.AsFloat;
  mskOTRM_DATE.Text := qryListOTRM_DATE.AsString;
  mskNTRM_DATE.Text := qryListNTRM_DATE.AsString;
  mskOCFM_DATE.Text := qryListOCFM_DATE.AsString;
  edtMS_CODE.Text := qryListMS_CODE.AsString;
  edtMS_SAUP.Text := qryListMS_SAUP.AsString;
  edtMS_NAME1.Text := qryListMS_NAME1.AsString;
  edtMS_NAME2.Text := qryListMS_NAME2.AsString;
  edtMS_NAME3.Text := qryListMS_NAME3.AsString;
  edtMS_ADDR1.Text := qryListMS_ADDR1.AsString;
  edtMS_ADDR2.Text := qryListMS_ADDR2.AsString;
  edtMS_ADDR3.Text := qryListMS_ADDR3.AsString;
  edtMS_TEL.Text   := qryListMS_TEL.AsString;
  edtAX_NAME3.Text := qryListAX_NAME1.AsString;

  edtCT_NO.Text := qryListCT_NO.AsString;
  edtAFY_NO.Text := qryListAFY_NO.AsString;
  mskOAPP_DATE.Text := qryListOAPP_DATE.AsString;
  mskNAPP_DATE.Text := qryListNAPP_DATE.AsString;
  edtSE_CODE.Text := qryListSE_CODE.AsString;
  edtSE_NAME1.Text := qryListSE_NAME1.AsString;
  edtSE_NAME2.Text := qryListSE_NAME2.AsString;
  edtSE_NAME3.Text := qryListSE_NAME3.AsString;
  edtGOODS_CD.Text := qryListGOODS_CD.AsString;
  mskHS_NO.Text := qryListHS_NO.AsString;
  edtGOODS1.Text := qryListGOODS1.AsString;
  edtGOODS2.Text := qryListGOODS2.AsString;
  edtGOODS3.Text := qryListGOODS3.AsString;
  edtGOODS4.Text := qryListGOODS4.AsString;
  edtGOODS5.Text := qryListGOODS5.AsString;

  memoREMARK1.Lines.Text := qryListREMARK1.AsString;

  ReadDetail; 
end;

procedure TUI_APPCOP_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  getData;
  BtnControl;
end;

procedure TUI_APPCOP_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then
  begin
    qryListAfterScroll(DataSet);
  end;
end;

procedure TUI_APPCOP_NEW_frm.edtMessage1DblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  Case (Sender as TsEdit).Tag of
    // 문서구분
    100 : CDDialog := TCD_APPCOP_GUBUN_frm.Create(Self);
    // 거래형태구분
    101 : CDDialog := TCD_APPCOP_JIGEUP_frm.Create(Self);
    // 지급지시서용도
    102 : CDDialog := TCD_AMT_UNIT_frm.Create(Self);
  end;

  try
    if CDDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CDDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        100 : sEdit3.Text := CDDialog.Values[1];
        101 : sEdit5.Text := CDDialog.Values[1];
      end;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

function TUI_APPCOP_NEW_frm.ReadList(SortCol: TColumn): Integer;
begin
  // 입력, 수정중일경우에는 작동불가
  if Self.Tag <> 0 Then Exit;

  Result := -1;

  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    //FDate
    Parameters[0].Value := mskDate1.Text;
    //TDate
    Parameters[1].Value := mskDate2.Text;
    //MAINT_NO
    Parameters[2].Value := Trim('%'+edtFind.Text+'%');
    //All_Data
    if Length(edtFind.Text) > 0 Then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    if SortCol = nil then
    begin
      ClearColTitle('');
      SQL.Add('ORDER BY DATEE');
    end
    else
    begin
      if Pos('↑', SortCol.Title.Caption) > 0 Then
      begin
        SQL.Add('ORDER BY '+SortCol.FieldName+' DESC');
        SortCol.Title.Caption := AnsiReplaceText(SortCol.Title.Caption,'↑','↓')
      end
      else
      if Pos('↓', SortCol.Title.Caption) > 0 Then
      begin
        SQL.Add('ORDER BY '+SortCol.FieldName+' ASC');
        SortCol.Title.Caption := AnsiReplaceText(SortCol.Title.Caption,'↓','↑');
      end
      else
      begin
        SQL.Add('ORDER BY '+SortCol.FieldName+' ASC');
        SortCol.Title.Caption := SortCol.Title.Caption+'↑';
      end;

      ClearColTitle(SortCol.FieldName);
    end;
    Open;
  end;
end;

procedure TUI_APPCOP_NEW_frm.ClearColTitle(igoreCol : String);
var
  i : Integer;
begin
  for i := 0 to sDBGrid1.Columns.Count-1 do
  begin
    if sDBGrid1.Columns[i].FieldName <> igoreCol Then
      sDBGrid1.Columns[i].Title.Caption := AnsiReplaceText(AnsiReplaceText(sDBGrid1.Columns[i].Title.Caption, '↑', ''),'↓','');
  end;
end;

procedure TUI_APPCOP_NEW_frm.FormCreate(Sender: TObject);
begin
  inherited;
  FSQL := qryList.SQL.Text;
end;

procedure TUI_APPCOP_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  mskDate1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  mskDate2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel6);
  ReadOnlyControl(sPanelMemo);

  ReadList;
  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_APPCOP_NEW_frm.edtFindKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  inherited;
  IF Key = VK_RETURN Then
    ReadList;
end;

procedure TUI_APPCOP_NEW_frm.btnFindClick(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_APPCOP_NEW_frm.edtMS_CODEDblClick(Sender: TObject);
begin
  IF (Sender as TsEdit).ReadOnly Then Exit;

  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_CUST_frm.Values[0];

      case (Sender as TsEdit).Tag of
        // 신청인
        101:
        begin
          edtMS_NAME1.Text := CD_CUST_frm.FieldValues('ENAME');
          edtMS_NAME2.Text := CD_CUST_frm.FieldValues('REP_NAME');
          edtMS_SAUP.Text  := CD_CUST_frm.FieldValues('SAUP_NO');
          edtMS_ADDR1.Text := CD_CUST_frm.FieldValues('ADDR1');
          edtMS_ADDR2.Text := CD_CUST_frm.FieldValues('ADDR2');
          edtMS_ADDR3.Text := CD_CUST_frm.FieldValues('ADDR3');
          edtMS_TEL.Text   := CD_CUST_frm.FieldValues('REP_TEL');
          edtAX_NAME3.Text := CD_CUST_frm.FieldValues('jenja');
        end;
        //수익자
        102:
        begin
          edtSE_NAME1.Text := CD_CUST_frm.FieldValues('ENAME');
          edtSE_NAME2.Text := CD_CUST_frm.FieldValues('REP_NAME');
        end;
      end;
    end;
  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_APPCOP_NEW_frm.edtGOODS_CDDblClick(Sender: TObject);
var
  i : integer;
  TMPLIST : TStringList;
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  CD_ITEMLIST_frm := TCD_ITEMLIST_frm.Create(Self);
  try
    if CD_ITEMLIST_frm.OpenDialog(0, edtGOODS_CD.Text) then
    begin
      edtGOODS_CD.Text := CD_ITEMLIST_frm.FieldValues('CODE');
      mskHS_NO.Text  := CD_ITEMLIST_frm.FieldValues('HSCODE');
      TMPLIST := TStringList.Create;
      try
        edtGOODS1.Clear;
        edtGOODS2.Clear;
        edtGOODS3.Clear;
        edtGOODS4.Clear;
        edtGOODS5.Clear;

        TMPLIST.Text := CD_ITEMLIST_frm.FieldValues('FNAME');
        for i := 1 to 5 do
        begin
          if i <= TMPLIST.Count Then
          begin
            case i of
              1: edtGOODS1.Text := TMPLIST.Strings[i-1];
              2: edtGOODS2.Text := TMPLIST.Strings[i-1];
              3: edtGOODS3.Text := TMPLIST.Strings[i-1];
              4: edtGOODS4.Text := TMPLIST.Strings[i-1];
              5: edtGOODS5.Text := TMPLIST.Strings[i-1];
            end;
          end;
        end;
      finally
        TMPLIST.Free;
      end;
    end;
  finally
    FreeAndNil(CD_ITEMLIST_frm);
  end;
end;

procedure TUI_APPCOP_NEW_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  NewDocument;
end;

procedure TUI_APPCOP_NEW_frm.NewDocument;
begin
  IF not DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) then
  begin
    Raise Exception.Create('거래처관리의 개설업체(00000) 식별자부호가 없습니다'#13#10'입력 후 진행하세요');
  end;

  Self.Tag := 1;
  BtnControl;
  PanelControl;
  //컨트롤 입력가능
  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel6, False);
  ReadOnlyControl(sPanelMemo, False);

  //초기화
  ClearControl(sPanel7);
  ClearControl(sPanel6);
  ClearControl(sPanelMemo);

  //공통 데이터 초기화
  edtMAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPCOP');
  mskDATEE.Text := FormatDateTime('YYYYMMDD', Now);
  edtUSER_ID.Text := LoginData.sID;
  com_func.ItemIndex := 0;
  com_type.ItemIndex := 0;

  mskOAPP_DATE.Text := FormatDateTime('YYYYMMDD', Now);

  edtMS_CODE.Text  := '00000';
  edtMS_NAME1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ENAME').asString;
  edtMS_NAME2.Text := DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').asString;
  edtMS_SAUP.Text  := DMCodeContents.GEOLAECHEO.FieldByName('SAUP_NO').asString;
  edtMS_ADDR1.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR1').asString;
  edtMS_ADDR2.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR2').asString;
  edtMS_ADDR3.Text := DMCodeContents.GEOLAECHEO.FieldByName('ADDR3').asString;
  edtMS_TEL.Text   := DMCodeContents.GEOLAECHEO.FieldByName('REP_TEL').asString;
  edtAX_NAME3.Text := DMCodeContents.GEOLAECHEO.FieldByName('jenja').asString;

  sPageControl1.ActivePageIndex := 1;

  DMMssql.BeginTrans;
end;

procedure TUI_APPCOP_NEW_frm.sDBGrid1TitleClick(Column: TColumn);
begin
  inherited;
  ReadList(Column);
end;

procedure TUI_APPCOP_NEW_frm.BtnControl;
begin
  btnNew.Enabled := Self.Tag = 0;
  btnEdit.Enabled := btnNew.Enabled and (qryList.RecordCount > 0);
  btnDel.Enabled := btnEdit.Enabled;
  btnCopy.Enabled := btnEdit.Enabled;

  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := btnTemp.Enabled;
  btnCancel.Enabled := btnTemp.Enabled;

  btnDetailADD.Enabled := btnTemp.Enabled;
  btnDetailEdit.Enabled := btnTemp.Enabled and (qryDetail.RecordCount > 0);
  btnDetailDel.Enabled := btnDetailEdit.Enabled;

  btnPrint.Enabled := btnEdit.Enabled;
  btnSend.Enabled  := btnEdit.Enabled;
  btnReady.Enabled  := btnEdit.Enabled;
end;

procedure TUI_APPCOP_NEW_frm.btnSaveClick(Sender: TObject);
var
  SC : TSQLCreate;
  DocNo : String;
begin
  inherited;
  //------------------------------------------------------------------------------
  // 관리번호 중복 체크
  //------------------------------------------------------------------------------
  if (Self.Tag = 1) AND (chkDocOver(edtMAINT_NO.Text)) Then
  begin
    showHintControl(edtMAINT_NO,'<font color=red><b>관리번호('+edtMAINT_NO.Text+')</b></font>가 이미 존재합니다');
    Exit;
  end;

  if (Sender as TsButton).Tag = 1 Then
  begin
    //------------------------------------------------------------------------------
    // 미입력 체크
    //------------------------------------------------------------------------------
    if Length(Trim(edtMessage1.Text)) = 0 Then
    begin
      showHintControl(edtMessage1,'<font color=red><b>문서구분</b></font>을 선택하세요');
      Exit;
    end;
    if Length(Trim(edtBUS_G.Text)) = 0 Then
    begin
      showHintControl(edtBUS_G,'<font color=red><b>지급구분</b></font>을 선택하세요');
      Exit;
    end;
    if Length(Trim(edtCT_NO.Text)) = 0 Then
    begin
      showHintControl(edtCT_NO,'<font color=red><b>계약서번호</b></font>를 입력해주세요');
      Exit;
    end;
    if Length(Trim(edtAMTC.Text)) = 0 Then
    begin
      showHintControl(edtAMTC,'<font color=red><b>신청금액 통화단위</b></font>를 입력해주세요');
      Exit;
    end;
    if currAMT.Value = 0 Then
    begin
      showHintControl(currAMT,'<font color=red><b>신청금액</b></font>을 입력해주세요');
      Exit;
    end;
    if Length(Trim(mskOTRM_DATE.Text)) = 0 Then
    begin
      showHintControl(mskOTRM_DATE,'<font color=red><b>결제기간(당초)</b></font>를 입력해주세요');
      Exit;
    end;
    if Length(Trim(edtMS_NAME1.Text)) = 0 Then
    begin
      showHintControl(edtMS_NAME1,'<font color=red><b>신청인정보</b></font>를 입력해주세요');
      Exit;
    end;
    if Length(Trim(edtSE_NAME1.Text)) = 0 Then
    begin
      showHintControl(edtSE_NAME1,'<font color=red><b>지급대상인 정보</b></font>를 입력해주세요');
      Exit;
    end;
    if (Trim(edtBUS_G.Text) = '2BV') AND (Trim(edtAFY_NO.Text) = '') Then
    begin
      showHintControl(edtAFY_NO,'<font color=red><b>지급구분 : 2BV(사후송금)</b></font>일 경우에는 <b>지급지시서(PAYORD)번호</b>를 입력하세요');
      Exit;
    end;
  end;

//------------------------------------------------------------------------------
// 데이터 처리
//------------------------------------------------------------------------------
  SC := TSQLCreate.Create;
  try
    DocNo := edtMAINT_NO.Text;
    if Self.Tag = 1 Then
    begin
      SC.DMLType := dmlInsert;
      SC.ADDValue('MAINT_NO', DocNo);
    end
    else
    if Self.Tag = 2 Then
    begin
      SC.DMLType := dmlUpdate;
      SC.ADDWhere('MAINT_NO', DocNo);
    end;

    SC.SQLHeader('APPCOP');
    SC.ADDValue('USER_ID',  edtUSER_ID.Text);
    SC.ADDValue('DATEE',    mskDATEE.Text);
    SC.ADDValue('MESSAGE1', edtMessage1.Text);
    SC.ADDValue('MESSAGE2', CM_TEXT(com_func,[':']));
    SC.ADDValue('MESSAGE3', CM_TEXT(com_type,[':']));
    SC.ADDValue('CT_NO',    edtCT_NO.Text);
    SC.ADDValue('AFY_NO',   edtAFY_NO.Text);
    SC.ADDValue('MS_CODE',  edtMS_CODE.Text);
    SC.ADDValue('MS_SAUP',  edtMS_SAUP.Text);
    SC.ADDValue('MS_NAME1', edtMS_NAME1.Text);
    SC.ADDValue('MS_NAME2', edtMS_NAME2.Text);
    SC.ADDValue('MS_NAME3', edtMS_NAME3.Text);
    SC.ADDValue('MS_ADDR1', edtMS_ADDR1.Text);
    SC.ADDValue('MS_ADDR2', edtMS_ADDR2.Text);
    SC.ADDValue('MS_ADDR3', edtMS_ADDR3.Text);
    SC.ADDValue('MS_TEL',   edtMS_TEL.Text);
//    SC.ADDValue('AX_NAME1', '');
//    SC.ADDValue('AX_NAME2',
    SC.ADDValue('AX_NAME3', edtAX_NAME3.Text);
    SC.ADDValue('SE_CODE',  edtSE_CODE.Text);
    SC.ADDValue('SE_NAME1', edtSE_NAME1.Text);
    SC.ADDValue('SE_NAME2', edtSE_NAME2.Text);
    SC.ADDValue('SE_NAME3', edtSE_NAME3.Text);
    SC.ADDValue('AMT',      FormatFloat('0.####', currAMT.Value));
    SC.ADDValue('AMT_C',    edtAMTC.Text);
    SC.ADDValue('REMARK1',  memoREMARK1.Lines.Text);
    SC.ADDValue('GOODS_CD', edtGOODS_CD.Text);
    SC.ADDValue('GOODS1',   edtGOODS1.Text);
    SC.ADDValue('GOODS2',   edtGOODS2.Text);
    SC.ADDValue('GOODS3',   edtGOODS3.Text);
    SC.ADDValue('GOODS4',   edtGOODS4.Text);
    SC.ADDValue('GOODS5',   edtGOODS5.Text);
    SC.ADDValue('HS_NO',    mskHS_NO.Text);
    SC.ADDValue('BUS_G',    edtBUS_G.Text);
    SC.ADDValue('BUS_D',    edtBUS_D.Text);
    SC.ADDValue('OTRM_DATE', mskOTRM_DATE.Text);
    SC.ADDValue('NTRM_DATE', mskNTRM_DATE.Text);
    SC.ADDValue('OAPP_DATE', mskOAPP_DATE.Text);
    SC.ADDValue('NAPP_DATE', mskNAPP_DATE.Text);
    SC.ADDValue('OCFM_DATE', mskOCFM_DATE.Text);
    //문서저장구분(0:임시, 1:저장)
    SC.ADDValue('CHK2', (Sender as TsButton).Tag);

    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        try
          SQL.Text := SC.CreateSQL;
          if sCheckBox1.Checked then
            Clipboard.AsText := SQL.Text;
          ExecSQL;
          DMMssql.CommitTrans;
        except
          on e:Exception do
          begin
            DMMssql.RollbackTrans;
            MessageBox(Self.Handle, PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message), '오류', MB_OK+MB_ICONERROR);
          end;
        end;
      finally
        Close;
        Free;
      end;
    end;

  finally
    SC.Free;
  end;

//------------------------------------------------------------------------------
// 컨트롤 제어
//------------------------------------------------------------------------------
  if Self.Tag = 2 Then
  begin
    sDBGrid1.Enabled := True;
    sDBGrid2.Enabled := True;
  end;

  Self.Tag := 0;
  BtnControl;
  PanelControl;
  ReadOnlyControl(sPanel7, True);
  ReadOnlyControl(sPanel6, True);
  ReadOnlyControl(sPanelMemo, True);

  mskDate1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  mskDate2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  qryList.Close;
  qryList.Open;
  qryList.Locate('MAINT_NO', DocNo, []);
end;

function TUI_APPCOP_NEW_frm.chkDocOver(DocNo: String): Boolean;
begin
  Result := False;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT MAINT_NO, DATEE FROM APPCOP WHERE MAINT_NO = '+QuotedStr(DocNo);
      Open;
      Result := RecordCount > 0;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_APPCOP_NEW_frm.showHintControl(Control: TControl; msg: String;
  showDelay: integer);
var
  R : TRect;
  P : TPoint;
begin
  IF msg = '' Then Exit;

  if Control is TsEdit Then TsEdit(Control).SetFocus
  else if Control is TsMaskEdit Then TsMaskEdit(Control).SetFocus
  else if Control is TsCurrencyEdit Then TsCurrencyEdit(Control).SetFocus;

  R := Control.BoundsRect;
  R.TopLeft := Control.Parent.ClientToScreen(R.TopLeft);
  P.X := R.Left-7;
  P.Y := R.Top+18;
  sAlphaHints1.HTMLMode := PosMulti(['<b>','<i>','<u>','</br>','<br>','<font'], LowerCase(msg));
  sAlphaHints1.ShowHint(P, msg ,showDelay);
end;

procedure TUI_APPCOP_NEW_frm.btnCancelClick(Sender: TObject);
begin
  inherited;
  if Self.Tag = 2 Then
  begin
    sDBGrid1.Enabled := True;
    sDBGrid2.Enabled := True;
    sDBGrid3.Enabled := True;
  end;

  Self.Tag := 0;
  BtnControl;
  PanelControl;
  ReadOnlyControl(sPanel7, True);
  ReadOnlyControl(sPanel6, True);
  ReadOnlyControl(sPanelMemo, True);

  IF DMMssql.inTrans Then DMMssql.RollbackTrans;

  getData;
end;

procedure TUI_APPCOP_NEW_frm.EditDocument;
begin
  Self.Tag := 2;
  BtnControl;
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;

  //컨트롤 입력가능
//  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel6, False);
  ReadOnlyControl(sPanelMemo, False);

  sPageControl1.ActivePageIndex := 1;
  DMMssql.BeginTrans;  
end;

procedure TUI_APPCOP_NEW_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  EditDocument;
end;

procedure TUI_APPCOP_NEW_frm.btnDelClick(Sender: TObject);
var
  CurrIdx : Integer;
begin
  inherited;
  if MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      CurrIdx := qryList.RecNo;

      Connection := DMMssql.KISConnect;
      DMMssql.BeginTrans;
      try
        SQL.Text := 'DELETE FROM APPCOP_D WHERE MAINT_NO = '+QuotedStr(qryListMAINT_NO.AsString);
        ExecSQL;
        SQL.Text := 'DELETE FROM APPCOP WHERE MAINT_NO = '+QuotedStr(qryListMAINT_NO.AsString);
        ExecSQL;
        DMMssql.CommitTrans;

        qryList.Close;
        qryList.Open;
        if CurrIdx > 1 Then
          qryList.MoveBy(CurrIdx-1);
      except
        on E:Exception do
        begin
          DMMssql.RollbackTrans;
          ShowMessage(E.Message);
        end;
      end;

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_APPCOP_NEW_frm.btnCopyClick(Sender: TObject);
begin
  inherited;
  if MessageBox(Self.Handle, MSG_COPY_QUESTION, '문서 복사확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  Self.Tag := 1;
  BtnControl;
  PanelControl;

  //컨트롤 입력가능
  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel6, False);
  ReadOnlyControl(sPanelMemo, False);

  //공통 데이터 초기화
  edtMAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPCOP');
  mskDATEE.Text := FormatDateTime('YYYYMMDD', Now);
  mskOTRM_DATE.Text := FormatDateTime('YYYYMMDD', Now);

  sPageControl1.ActivePageIndex := 1;

  DMMssql.BeginTrans;
end;

procedure TUI_APPCOP_NEW_frm.PanelControl;
begin
  if Self.Tag = 1 Then
  begin
    sPanel18.Left := 0;
    sPanel18.Top := 43;
    sPanel18.Visible := True;
    sPanel19.Left := 1;
    sPanel19.Top := 1;
    sPanel19.Visible := True;
  end
  else
  begin
    sPanel18.Visible := False;
    sPanel19.Visible := False;
  end;
end;


procedure TUI_APPCOP_NEW_frm.btnDetailADDClick(Sender: TObject);
var
  Modalvalue : TModalResult;
begin
  inherited;
  dlgAPPCOP_D_frm := TdlgAPPCOP_D_frm.Create(Self);
  try
    case (sender as TsButton).Tag of
      0: Modalvalue := dlgAPPCOP_D_frm.NewItem(edtMAINT_NO.Text);
      1: Modalvalue := dlgAPPCOP_D_frm.EditItem(qryDetail.Fields);
    end;

    if Modalvalue = mrOK Then
    begin
      ReadDetail;
      qryDetail.Locate('KEYY;SEQ', VarArrayOf([edtMAINT_NO.Text, dlgAPPCOP_D_frm.affSeq]), []);
    end;
  finally
    FreeAndNil( dlgAPPCOP_D_frm );
  end;
end;

function TUI_APPCOP_NEW_frm.ReadDetail: Integer;
begin
  Result := -1;
  with qrydetail do
  begin
    Close;
    Parameters[0].Value := qryListMAINT_NO.AsString;
    Open;
  end;
end;

procedure TUI_APPCOP_NEW_frm.btnDetailDelClick(Sender: TObject);
var
  CurrIdx : Integer;
begin
  inherited;
  if MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      CurrIdx := qryDetail.RecNo;

      Connection := DMMssql.KISConnect;
      SQL.Text := 'DELETE FROM APPCOP_D WHERE KEYY = '+QuotedStr(qryDetailKEYY.AsString)+' AND SEQ = '+IntToStr(qryDetailSEQ.AsInteger);
      ExecSQL;

      NumberSort(qryDetailKEYY.AsString);

      qryDetail.Close;
      qryDetail.Open;
      if CurrIdx > 1 Then
        qryDetail.MoveBy(CurrIdx-1);

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_APPCOP_NEW_frm.NumberSort(Maint_No: String);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'UPDATE APPCOP_D SET SEQ = UP_TABLE.N_SEQ FROM'#13+
                  '(SELECT KEYY, SEQ, ROW_NUMBER() OVER(ORDER BY SEQ) as N_SEQ FROM APPCOP_D WHERE KEYY = '+QuotedStr(Maint_No)+') UP_TABLE'#13+
                  'WHERE APPCOP_D.KEYY = UP_TABLE.KEYY'#13+
                  'AND APPCOP_D.SEQ = UP_TABLE.SEQ';
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_APPCOP_NEW_frm.edtMessage1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  MAX_LEN : Integer;
  TMP_STR : String;
begin
  inherited;
  MAX_LEN := (Sender as TsEdit).MaxLength;

  if InRange(Length((Sender as TsEdit).Text), MAX_LEN-1, MAX_LEN) Then
  begin
    Case (Sender as TsEdit).Tag of
      // 문서구분
      100 : CDDialog := TCD_APPCOP_GUBUN_frm.Create(Self);
      // 거래형태구분
      101 : CDDialog := TCD_APPCOP_JIGEUP_frm.Create(Self);
    end;

    try
      TMP_STR := CDDialog.GetCodeText((Sender as TsEdit).Text);
      Case (Sender as TsEdit).Tag of
        100 : sEdit3.Text := TMP_STR;
        101 : sEdit5.Text := TMP_STR;
      end;
    finally
      FreeAndNil( CDDialog );
    end;
  end;
end;

procedure TUI_APPCOP_NEW_frm.edtMessage1Change(Sender: TObject);
begin
  inherited;
  IF Length((Sender as TsEdit).Text) = 0 then
  begin
    case (Sender as TsEdit).Tag of
      100: sEdit3.Clear;
      101: sEdit5.Clear;
    end;
  end;
end;

procedure TUI_APPCOP_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  QR_APPCOP_PRN_frm := TQR_APPCOP_PRN_frm.Create(nil);
  Preview_frm := TPreview_frm.Create(Self);
  try
    QR_APPCOP_PRN_frm.MAINT_NO := qryListMAINT_NO.AsString;
    Preview_frm.Report := QR_APPCOP_PRN_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(QR_APPCOP_PRN_frm);
  end;
end;

procedure TUI_APPCOP_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
    ReadyDocument(qryListMAINT_NO.AsString)
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
end;

procedure TUI_APPCOP_NEW_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := APPCOP(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'APPCOP';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APPCOP';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        qryList.Close;
        qrylist.Open;

        qrylist.Locate('MAINT_NO',RecvDoc,[]);

//        Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(ReadyDateTime)),
//                 FormatDateTime('YYYYMMDD',EndOfTheYear(ReadyDateTime)),
//                 RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_APPCOP_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

end.

