inherited UI_PCRLIC_NEW_frm: TUI_PCRLIC_NEW_frm
  Left = 759
  Top = 213
  BorderWidth = 4
  Caption = #50808#54868#54925#46301#50857#50896#47308'('#47932#54408' '#46321') '#44396#47588'('#44277#44553')'#54869#51064#49436'('#49688#49888')'
  ClientHeight = 673
  ClientWidth = 1114
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  object btn_Panel: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 72
    SkinData.SkinSection = 'PANEL'
    Align = alTop
    
    TabOrder = 0
    object sSpeedButton4: TsSpeedButton
      Left = 380
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 1037
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel7: TsLabel
      Left = 47
      Top = 13
      Width = 210
      Height = 23
      Caption = #50808#54868#54925#46301#50857#50896#47308' '#44396#47588#54869#51064#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton
      Left = 299
      Top = 3
      Width = 8
      Height = 64
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel5: TsLabel
      Left = 119
      Top = 37
      Width = 65
      Height = 21
      Caption = '(PCRLIC)'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton3: TsSpeedButton
      Left = 1030
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton2: TsSpeedButton
      Left = 461
      Top = 4
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #50641#49472
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentFont = False
      Spacing = 0
      OnClick = sSpeedButton2Click
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System24
      ImageIndex = 30
      Reflected = True
    end
    object btnExit: TsButton
      Left = 1040
      Top = 2
      Width = 72
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object Btn_Del: TsButton
      Tag = 2
      Left = 311
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = Btn_DelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object Btn_Print: TsButton
      Left = 392
      Top = 2
      Width = 65
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 2
      TabStop = False
      OnClick = Btn_PrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sPanel6: TsPanel
      Left = 306
      Top = 40
      Width = 805
      Height = 28
      SkinData.SkinSection = 'TRANSPARENT'
      
      TabOrder = 3
      object edt_MAINT_NO_Header: TsEdit
        Left = 72
        Top = 4
        Width = 217
        Height = 23
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = [fsBold]
        MaxLength = 35
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        SkinData.CustomColor = True
        SkinData.CustomFont = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
      end
      object msk_Datee: TsMaskEdit
        Left = 355
        Top = 4
        Width = 89
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        SkinData.CustomColor = True
      end
      object com_func: TsComboBox
        Left = 656
        Top = 4
        Width = 41
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#44592#45733
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 2
        TabStop = False
        Text = '1:'
        Items.Strings = (
          '1:'
          '2:'
          '4:'
          '6:'
          '7:'
          '9:')
      end
      object com_type: TsComboBox
        Left = 756
        Top = 4
        Width = 44
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        BoundLabel.Active = True
        BoundLabel.Caption = #47928#49436#50976#54805
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = 0
        TabOrder = 3
        TabStop = False
        Text = 'AB:'
        Items.Strings = (
          'AB:'
          'AP:'
          'NA:'
          'RE:')
      end
      object edt_userno: TsEdit
        Left = 496
        Top = 4
        Width = 29
        Height = 23
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        SkinData.CustomColor = True
        BoundLabel.Active = True
        BoundLabel.Caption = #49324#50857#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object comBank: TsComboBox
        Left = 954
        Top = 28
        Width = 155
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        VerticalAlignment = taVerticalCenter
        Style = csOwnerDrawFixed
        ItemHeight = 17
        ItemIndex = -1
        TabOrder = 5
        Visible = False
      end
    end
    object QRShape1: TQRShape
      Left = 306
      Top = 40
      Width = 805
      Height = 1
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        2.64583333333333
        809.625
        105.833333333333
        2129.89583333333)
      Brush.Color = clBtnFace
      Pen.Color = clBtnFace
      Shape = qrsRectangle
    end
  end
  object sPanel4: TsPanel [1]
    Left = 0
    Top = 72
    Width = 353
    Height = 601
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alLeft
    
    TabOrder = 1
    object sDBGrid1: TsDBGrid
      Left = 1
      Top = 57
      Width = 351
      Height = 543
      Align = alClient
      Color = clWhite
      Ctl3D = False
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnTitleClick = sDBGrid1TitleClick
      SkinData.CustomColor = True
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MAINT_NO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 229
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 89
          Visible = True
        end>
    end
    object sPanel5: TsPanel
      Left = 1
      Top = 1
      Width = 351
      Height = 56
      Align = alTop
      
      TabOrder = 1
      object sSpeedButton9: TsSpeedButton
        Left = 258
        Top = 5
        Width = 11
        Height = 46
        ButtonStyle = tbsDivider
      end
      object edt_SearchNo: TsEdit
        Tag = -1
        Left = 77
        Top = 29
        Width = 171
        Height = 23
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = edt_SearchNoChange
        BoundLabel.Active = True
        BoundLabel.Caption = #44288#47532#48264#54840
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit1: TsMaskEdit
        Left = 77
        Top = 4
        Width = 74
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '20180621'
        OnChange = sMaskEdit1Change
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sMaskEdit2: TsMaskEdit
        Tag = 1
        Left = 170
        Top = 4
        Width = 78
        Height = 23
        AutoSize = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 2
        Text = '20180621'
        OnChange = sMaskEdit1Change
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = '~'
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
      end
      object sBitBtn1: TsBitBtn
        Left = 273
        Top = 4
        Width = 66
        Height = 45
        Caption = #51312#54924
        TabOrder = 3
        OnClick = sBitBtn1Click
      end
    end
  end
  object sPageControl1: TsPageControl [2]
    Left = 353
    Top = 72
    Width = 761
    Height = 601
    ActivePage = sTabSheet5
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    Images = DMICON.System18
    ParentFont = False
    TabHeight = 26
    TabIndex = 3
    TabOrder = 2
    OnChange = sPageControl1Change
    TabPadding = 7
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      ImageIndex = 2
      object sPanel7: TsPanel
        Left = 0
        Top = 0
        Width = 753
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sSpeedButton10: TsSpeedButton
          Left = 389
          Top = 5
          Width = 11
          Height = 555
          ButtonStyle = tbsDivider
        end
        object sLabel3: TsLabel
          Left = 455
          Top = 56
          Width = 280
          Height = 15
          Alignment = taRightJustify
          AutoSize = False
          Caption = #49692#49688#50896#54868' '#46608#45716' '#50808#54868'(USD'#47484' '#51228#50808#54620')'#51068' '#44221#50864' '#49324#50857#50504#54632
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          UseSkinColor = False
        end
        object edt_APP_NAME1: TsEdit
          Left = 115
          Top = 55
          Width = 265
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 1
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
        end
        object edt_APP_ADDR1: TsEdit
          Left = 115
          Top = 103
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 2
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APP_ADDR2: TsEdit
          Left = 115
          Top = 127
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 3
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'edt_pubaddr2'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_APP_ADDR3: TsEdit
          Left = 115
          Top = 151
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 4
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'edt_pubaddr3'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AX_NAME2: TsEdit
          Left = 288
          Top = 31
          Width = 92
          Height = 23
          Color = 12582911
          MaxLength = 17
          TabOrder = 0
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
        end
        object sPanel11: TsPanel
          Left = 408
          Top = 7
          Width = 335
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #52509' '#54624#51064'/'#54624#51613'/'#48320#46041' '#45236#50669
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object sPanel8: TsPanel
          Left = 8
          Top = 286
          Width = 372
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #47928#49436#44396#48516
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object edt_APP_NAME2: TsEdit
          Left = 115
          Top = 79
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 7
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AX_NAME1: TsEdit
          Left = 115
          Top = 175
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 8
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47749#51032#51064
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_AX_NAME3: TsEdit
          Left = 115
          Top = 199
          Width = 126
          Height = 23
          Color = 12582911
          TabOrder = 9
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
        end
        object Edt_ConfirmAPP_DATE: TsMaskEdit
          Left = 299
          Top = 199
          Width = 81
          Height = 23
          AutoSize = False
          Color = clWhite
          EditMask = '9999-99-99;0'
          MaxLength = 10
          TabOrder = 10
          Text = '20180621'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54869#51064#51068#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.CustomColor = True
        end
        object edt_N4025: TsEdit
          Tag = 1000
          Left = 115
          Top = 255
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 11
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44277#44553#47932#54408' '#47749#49464#44396#48516
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_N4025_NM: TsEdit
          Tag = -1
          Left = 154
          Top = 255
          Width = 226
          Height = 23
          TabStop = False
          Color = clBtnFace
          MaxLength = 10
          ReadOnly = True
          TabOrder = 12
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel2: TsPanel
          Left = 8
          Top = 230
          Width = 372
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44277#44553#47932#54408#47749#49464#44396#48516
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 13
        end
        object sPanel3: TsPanel
          Left = 8
          Top = 7
          Width = 372
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #49888#52397#51064
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 14
        end
        object Edt_DocumentsSection: TsEdit
          Tag = 1001
          Left = 115
          Top = 311
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 15
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47928#49436#44396#48516
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
        end
        object Edt_DocumentsSection_NM: TsEdit
          Tag = -1
          Left = 154
          Top = 311
          Width = 226
          Height = 23
          TabStop = False
          Color = clBtnFace
          MaxLength = 10
          ReadOnly = True
          TabOrder = 16
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object Edt_BuyConfirmDocuments: TsEdit
          Left = 154
          Top = 335
          Width = 226
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 17
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#44221#51204' '#44396#47588#54869#51064#49436#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object Edt_SupplySaupNo: TsEdit
          Left = 288
          Top = 413
          Width = 92
          Height = 23
          Color = 12582911
          MaxLength = 17
          TabOrder = 18
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
        end
        object Edt_SupplyName: TsEdit
          Left = 115
          Top = 437
          Width = 265
          Height = 23
          Color = 12582911
          MaxLength = 35
          TabOrder = 19
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
        end
        object Edt_SupplyCEO: TsEdit
          Left = 115
          Top = 461
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 20
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #45824#54364#51060#49324
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object Edt_SupplyEID: TsEdit
          Left = 115
          Top = 485
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 21
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#47700#51068
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object Edt_SupplyAddr1: TsEdit
          Left = 115
          Top = 509
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 22
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object Edt_SupplyAddr2: TsEdit
          Left = 115
          Top = 533
          Width = 265
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 23
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'edt_pubaddr2'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_CHG_G: TsEdit
          Tag = 1002
          Left = 472
          Top = 31
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 1
          TabOrder = 24
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#46041#44396#48516
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_CHG_G_NM: TsEdit
          Tag = -1
          Left = 511
          Top = 31
          Width = 232
          Height = 23
          TabStop = False
          Color = clBtnFace
          MaxLength = 10
          ReadOnly = True
          TabOrder = 25
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object curr_C2_AMT: TsCurrencyEdit
          Left = 472
          Top = 74
          Width = 134
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          TabOrder = 26
          BoundLabel.Active = True
          BoundLabel.Caption = #52509#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object sPanel9: TsPanel
          Left = 607
          Top = 74
          Width = 40
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = 'USD'
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
        end
        object curr_C1_AMT: TsCurrencyEdit
          Left = 472
          Top = 98
          Width = 134
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          TabOrder = 28
          BoundLabel.Active = True
          BoundLabel.Caption = #52509#44552#50529
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object edt_C1_C: TsEdit
          Tag = 2000
          Left = 607
          Top = 98
          Width = 40
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 29
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48320#46041#44396#48516
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object sPanel10: TsPanel
          Left = 408
          Top = 375
          Width = 335
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #54869#51064#44592#44288
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 30
        end
        object edt_BK_CD: TsEdit
          Tag = 101
          Left = 472
          Top = 399
          Width = 89
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          TabOrder = 31
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #54869#51064#44592#44288
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
        end
        object edt_BK_NAME1: TsEdit
          Left = 472
          Top = 423
          Width = 263
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 32
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'edt_pubaddr2'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_BK_NAME2: TsEdit
          Left = 472
          Top = 447
          Width = 263
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 33
          SkinData.CustomColor = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'edt_pubaddr3'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sButton23: TsButton
          Tag = 3
          Left = 648
          Top = 98
          Width = 33
          Height = 23
          Cursor = crHandPoint
          TabOrder = 34
          Images = DMICON.System18
          ImageAlignment = iaCenter
          ImageIndex = 42
        end
        object sPanel31: TsPanel
          Left = 8
          Top = 388
          Width = 372
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #44277#44553#51088
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 35
        end
        object sPanel16: TsPanel
          Left = 408
          Top = 135
          Width = 335
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #52509' '#54633#44228
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 36
        end
        object edt_TQTYC: TsEdit
          Tag = 2001
          Left = 530
          Top = 160
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 37
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52509#49688#47049
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object curr_TQTY: TsCurrencyEdit
          Left = 569
          Top = 160
          Width = 174
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          TabOrder = 38
          BoundLabel.Caption = #52509#49688#47049
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object curr_TAMT2: TsCurrencyEdit
          Left = 569
          Top = 184
          Width = 174
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          TabOrder = 39
          BoundLabel.Active = True
          BoundLabel.Caption = #52509#44552#50529'('#50896#54868')'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 0
          DisplayFormat = '#,0'
        end
        object edt_TAMT1C: TsEdit
          Tag = 2000
          Left = 530
          Top = 208
          Width = 38
          Height = 23
          CharCase = ecUpperCase
          Color = 12775866
          MaxLength = 3
          TabOrder = 40
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52509#44552#50529'('#50808#54868')'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
        end
        object curr_TAMT1: TsCurrencyEdit
          Left = 569
          Top = 208
          Width = 174
          Height = 23
          AutoSelect = False
          AutoSize = False
          CharCase = ecUpperCase
          TabOrder = 41
          BoundLabel.Caption = #52509#44552#50529'('#50808#54868')'
          SkinData.SkinSection = 'EDIT'
          DecimalPlaces = 4
          DisplayFormat = '#,0.####'
        end
        object edt_BankSend_No: TsEdit
          Left = 154
          Top = 359
          Width = 226
          Height = 23
          Color = clWhite
          MaxLength = 35
          TabOrder = 42
          SkinData.CustomColor = True
          BoundLabel.Active = True
          BoundLabel.UseSkinColor = False
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51008#54665' '#48156#49888#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object sPanel22: TsPanel
          Left = 408
          Top = 239
          Width = 335
          Height = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'PANEL'
          Caption = #48156#44553#51312#44148#46321' '#44592#53440#49324#54637
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 43
        end
        object sMemo1: TsMemo
          Left = 408
          Top = 263
          Width = 335
          Height = 103
          Ctl3D = True
          ParentCtl3D = False
          ScrollBars = ssVertical
          TabOrder = 44
          WordWrap = False
          BoundLabel.ParentFont = False
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold, fsUnderline]
        end
      end
    end
    object sTabSheetDetail: TsTabSheet
      Caption = #49345#54408#45236#50669
      ImageIndex = 37
      object sPanel27: TsPanel
        Left = 0
        Top = 0
        Width = 753
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sDBGrid2: TsDBGrid
          Left = 1
          Top = 1
          Width = 751
          Height = 194
          TabStop = False
          Align = alClient
          Color = clWhite
          DataSource = dsDetail
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnTitleClick = sDBGrid2TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'HS_NO'
              Title.Alignment = taCenter
              Title.Caption = 'HS'#48512#54840
              Width = 91
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME1'
              Title.Alignment = taCenter
              Title.Caption = #54408#47749
              Width = 162
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 70
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QTYC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT1'
              Title.Alignment = taCenter
              Title.Caption = #44552#50529
              Width = 108
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'AMT1C'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRI2'
              Title.Alignment = taCenter
              Title.Caption = #45800#44032
              Width = 90
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'APP_DATE'
              Title.Alignment = taCenter
              Title.Caption = #44396#47588'('#44277#44553#51068')'
              Width = 80
              Visible = True
            end>
        end
        object sPanel12: TsPanel
          Left = 1
          Top = 197
          Width = 751
          Height = 367
          Align = alBottom
          
          TabOrder = 1
          DesignSize = (
            751
            367)
          object sSpeedButton13: TsSpeedButton
            Left = 395
            Top = 5
            Width = 11
            Height = 357
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
          end
          object sPanel14: TsPanel
            Left = 8
            Top = 7
            Width = 383
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49345#54408#51221#48372
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 20
          end
          object msk_HS_NO: TsMaskEdit
            Left = 299
            Top = 32
            Width = 92
            Height = 23
            AutoSize = False
            Color = 12582911
            EditMask = '9999.99-9999;0'
            MaxLength = 12
            TabOrder = 0
            Text = '1234567890'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'HS'#48512#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            SkinData.CustomColor = True
          end
          object msk_APP_DATE: TsMaskEdit
            Left = 299
            Top = 57
            Width = 92
            Height = 23
            AutoSize = False
            Color = clWhite
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 2
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44396#47588'('#44277#44553#51068')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            SkinData.CustomColor = True
          end
          object memo_NAME1: TsMemo
            Left = 52
            Top = 82
            Width = 339
            Height = 103
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 3
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54408#47749
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
          end
          object memo_Size: TsMemo
            Left = 52
            Top = 188
            Width = 339
            Height = 58
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 4
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44508#44201
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object memo_Remark: TsMemo
            Left = 52
            Top = 249
            Width = 339
            Height = 60
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 5
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48708#44256
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_NAMESS: TsEdit
            Tag = 101
            Left = 52
            Top = 57
            Width = 133
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 1
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object sPanel15: TsPanel
            Left = 408
            Top = 7
            Width = 335
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49688#47049' / '#45800#44032' / '#44552#50529
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 21
          end
          object edt_QTYC: TsEdit
            Tag = 3001
            Left = 530
            Top = 32
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 7
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
          end
          object curr_QTY: TsCurrencyEdit
            Left = 569
            Top = 32
            Width = 174
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 8
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object curr_PRI2: TsCurrencyEdit
            Left = 530
            Top = 56
            Width = 213
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 9
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_PRI_BASEC: TsEdit
            Tag = 3001
            Left = 530
            Top = 80
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032#44592#51456#49688#47049
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_PRI_BASE: TsCurrencyEdit
            Left = 569
            Top = 80
            Width = 174
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object curr_PRI1: TsCurrencyEdit
            Left = 530
            Top = 104
            Width = 213
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 12
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032'(USD'#44552#50529#48512#44592')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_AMT1C: TsEdit
            Tag = 3000
            Left = 530
            Top = 128
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 13
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold, fsUnderline]
          end
          object curr_AMT1: TsCurrencyEdit
            Left = 569
            Top = 128
            Width = 174
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            Color = 12582911
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 14
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object curr_AMT2: TsCurrencyEdit
            Left = 530
            Top = 152
            Width = 213
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 15
            BoundLabel.Active = True
            BoundLabel.Caption = #44552#50529'(USD'#44552#50529#48512#44592')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object edt_ACHG_G: TsEdit
            Tag = 3002
            Left = 530
            Top = 208
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            MaxLength = 1
            TabOrder = 16
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48320#46041#44396#48516
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_AC1_AMT: TsCurrencyEdit
            Left = 569
            Top = 232
            Width = 174
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 17
            BoundLabel.Active = True
            BoundLabel.Caption = #48320#46041#44552#50529'(USD '#44552#50529' '#48512#44592')'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object curr_AC2_AMT: TsCurrencyEdit
            Left = 569
            Top = 256
            Width = 174
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 19
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 4
            DisplayFormat = '###,###,###.####;-###,###,###.####;0'
          end
          object sPanel33: TsPanel
            Left = 408
            Top = 183
            Width = 335
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #54408#47785#48324' '#54624#51064'/'#54624#51613'/'#48320#46041' '#45236#50669
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 22
          end
          object sEdit2: TsEdit
            Tag = -1
            Left = 569
            Top = 208
            Width = 174
            Height = 23
            TabStop = False
            Color = clBtnFace
            MaxLength = 10
            ReadOnly = True
            TabOrder = 23
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_AC1_C: TsEdit
            Tag = 3000
            Left = 530
            Top = 256
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 18
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #48320#46041#44552#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_COMPAY_CD: TsEdit
            Tag = 101
            Left = 258
            Top = 313
            Width = 133
            Height = 23
            CharCase = ecUpperCase
            Color = clWhite
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51088#49324#44288#47532#53076#46300
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
        end
        object sPanel13: TsPanel
          Left = 1
          Top = 195
          Width = 751
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alBottom
          
          TabOrder = 2
        end
      end
    end
    object sTabSheetTax: TsTabSheet
      Caption = #49464#44552#44228#49328#49436
      ImageIndex = 40
      object sPanel24: TsPanel
        Left = 0
        Top = 0
        Width = 753
        Height = 565
        SkinData.SkinSection = 'TRANSPARENT'
        Align = alClient
        
        TabOrder = 0
        object sDBGrid4: TsDBGrid
          Left = 1
          Top = 1
          Width = 751
          Height = 273
          Align = alClient
          Color = clWhite
          DataSource = dsTax
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          OnTitleClick = sDBGrid4TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 33
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_NO'
              Title.Alignment = taCenter
              Title.Caption = #49464#44552#44228#49328#49436#48264#54840
              Width = 206
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_DATE'
              Title.Alignment = taCenter
              Title.Caption = #51089#49457#51068#51088
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BILL_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #44277#44553#44032#50529
              Width = 127
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'BILL_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TAX_AMOUNT'
              Title.Alignment = taCenter
              Title.Caption = #49464#50529
              Width = 85
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'TAX_AMOUNT_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #53685#54868
              Width = 33
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QUANTITY'
              Title.Alignment = taCenter
              Title.Caption = #49688#47049
              Width = 80
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'QUANTITY_UNIT'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 33
              Visible = True
            end>
        end
        object sPanel26: TsPanel
          Left = 1
          Top = 276
          Width = 751
          Height = 288
          Align = alBottom
          
          TabOrder = 1
          DesignSize = (
            751
            288)
          object Shape1: TShape
            Left = 375
            Top = 7
            Width = 1
            Height = 269
            Anchors = [akLeft, akTop, akBottom]
            Brush.Color = 13421772
            Pen.Color = 13421772
          end
          object edt_BILL_NO: TsEdit
            Left = 99
            Top = 39
            Width = 262
            Height = 23
            Color = 12582911
            MaxLength = 35
            TabOrder = 0
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49464#44552#44228#49328#49436#48264#54840
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object msk_BILL_DATE: TsMaskEdit
            Left = 99
            Top = 66
            Width = 81
            Height = 23
            AutoSize = False
            Color = clWhite
            EditMask = '9999-99-99;0'
            MaxLength = 10
            TabOrder = 1
            Text = '20180621'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #51089#49457#51068#51088
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            SkinData.CustomColor = True
          end
          object edt_ITEM_NAME1: TsEdit
            Left = 99
            Top = 93
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 2
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54408#47785
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ITEM_NAME2: TsEdit
            Left = 99
            Top = 117
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 3
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'edt_pubaddr2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ITEM_NAME3: TsEdit
            Left = 99
            Top = 141
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 4
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'edt_pubaddr3'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ITEM_NAME4: TsEdit
            Left = 99
            Top = 165
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 5
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'edt_pubaddr2'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object edt_ITEM_NAME5: TsEdit
            Left = 99
            Top = 189
            Width = 262
            Height = 23
            Color = clWhite
            MaxLength = 35
            TabOrder = 6
            SkinData.CustomColor = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'edt_pubaddr3'
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object memo_DESC: TsMemo
            Left = 99
            Top = 216
            Width = 262
            Height = 65
            Ctl3D = True
            ParentCtl3D = False
            ScrollBars = ssVertical
            TabOrder = 7
            WordWrap = False
            BoundLabel.Active = True
            BoundLabel.Caption = #44508#44201
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
          end
          object edt_BILL_AMOUNT_UNIT: TsEdit
            Tag = 5000
            Left = 523
            Top = 40
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 8
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44277#44553#44032#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_BILL_AMOUNT: TsCurrencyEdit
            Left = 562
            Top = 40
            Width = 165
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 9
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 0
            DisplayFormat = '#,0;0'
          end
          object edt_TAX_AMOUNT_UNIT: TsEdit
            Tag = 5000
            Left = 523
            Top = 64
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 10
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49464#50529
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_TAX_AMOUNT: TsCurrencyEdit
            Left = 562
            Top = 64
            Width = 165
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 0
            DisplayFormat = '#,0;0'
          end
          object edt_QUANTITY_UNIT: TsEdit
            Tag = 5001
            Left = 523
            Top = 88
            Width = 38
            Height = 23
            CharCase = ecUpperCase
            Color = 12775866
            TabOrder = 12
            SkinData.CustomColor = True
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
          end
          object curr_QUANTITY: TsCurrencyEdit
            Left = 562
            Top = 88
            Width = 165
            Height = 23
            AutoSelect = False
            AutoSize = False
            CharCase = ecUpperCase
            TabOrder = 13
            SkinData.SkinSection = 'EDIT'
            DecimalPlaces = 0
            DisplayFormat = '#,0;0'
          end
          object sPanel28: TsPanel
            Left = 11
            Top = 7
            Width = 350
            Height = 21
            SkinData.CustomColor = True
            SkinData.SkinSection = 'PANEL'
            Caption = #49464#44552#44228#49328#49436
            Color = 16042877
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 14
          end
        end
        object sPanel32: TsPanel
          Left = 1
          Top = 274
          Width = 751
          Height = 2
          SkinData.SkinSection = 'TRANSPARENT'
          Align = alBottom
          
          TabOrder = 2
        end
      end
    end
    object sTabSheet5: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      ImageIndex = 24
      object sPanel30: TsPanel
        Left = 0
        Top = 0
        Width = 753
        Height = 32
        Align = alTop
        
        TabOrder = 0
        object sSpeedButton31: TsSpeedButton
          Left = 230
          Top = 4
          Width = 11
          Height = 23
          ButtonStyle = tbsDivider
        end
        object sMaskEdit8: TsMaskEdit
          Tag = 2
          Left = 57
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          TabOrder = 0
          Text = '20180621'
          OnChange = sMaskEdit1Change
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.Caption = #46321#47197#51068#51088
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sMaskEdit9: TsMaskEdit
          Tag = 3
          Left = 150
          Top = 4
          Width = 78
          Height = 23
          AutoSize = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          TabOrder = 1
          Text = '20180621'
          OnChange = sMaskEdit1Change
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.Caption = '~'
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
        object sBitBtn5: TsBitBtn
          Tag = 1
          Left = 469
          Top = 5
          Width = 66
          Height = 23
          Caption = #51312#54924
          TabOrder = 2
          OnClick = sBitBtn5Click
        end
        object sEdit47: TsEdit
          Tag = -1
          Left = 297
          Top = 5
          Width = 171
          Height = 23
          TabOrder = 3
          OnChange = sEdit47Change
          BoundLabel.Active = True
          BoundLabel.Caption = #44288#47532#48264#54840
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
        end
      end
      object sDBGrid5: TsDBGrid
        Left = 0
        Top = 32
        Width = 753
        Height = 533
        Align = alClient
        Color = clWhite
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnTitleClick = sDBGrid5TitleClick
        SkinData.CustomColor = True
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 36
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 60
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 143
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SUP_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #44277#44553#51088
            Width = 202
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAMT1'
            Title.Alignment = taCenter
            Title.Caption = #52509#44552#50529
            Width = 157
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TAMT1C'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 33
            Visible = True
          end>
      end
    end
  end
  object pan_Excel: TsPanel [3]
    Left = 1016
    Top = 208
    Width = 497
    Height = 89
    SkinData.SkinSection = 'PANEL'
    
    TabOrder = 3
    Visible = False
    object sLabel1: TsLabel
      Left = 171
      Top = 45
      Width = 24
      Height = 12
      Caption = #48512#53552
    end
    object sLabel2: TsLabel
      Left = 299
      Top = 45
      Width = 24
      Height = 12
      Caption = #44620#51648
    end
    object sLabel4: TsLabel
      Left = 16
      Top = 8
      Width = 64
      Height = 21
      Caption = #50641#49472#52636#47141
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sDateEdit1: TsDateEdit
      Left = 72
      Top = 40
      Width = 97
      Height = 25
      AutoSize = False
      EditMask = '!9999/99/99;1; '
      MaxLength = 10
      TabOrder = 0
      Text = '2015-04-24'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
      Date = 42118
    end
    object sDateEdit2: TsDateEdit
      Left = 200
      Top = 40
      Width = 97
      Height = 25
      AutoSize = False
      EditMask = '!9999/99/99;1; '
      MaxLength = 10
      TabOrder = 1
      Text = '2015-04-24'
      BoundLabel.Caption = 'sDateEdit2'
      SkinData.SkinSection = 'EDIT'
      Date = 42118
    end
    object sButton2: TsButton
      Left = 336
      Top = 24
      Width = 145
      Height = 25
      Caption = #50641#49472#52636#47141
      TabOrder = 2
      OnClick = sButton2Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sButton3: TsButton
      Left = 336
      Top = 49
      Width = 145
      Height = 25
      Caption = #45803#44592
      TabOrder = 3
      OnClick = sButton3Click
      SkinData.SkinSection = 'BUTTON'
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT [MAINT_NO]'
      '      ,[USER_ID]'
      '      ,[CHK1]'
      '      ,[CHK2]'
      '      ,[CHK3]'
      '      ,[DATEE]'
      '      ,[MESSAGE1]'
      '      ,[MESSAGE2]'
      '      ,[APP_CODE]'
      '      ,[APP_NAME1]'
      '      ,[APP_NAME2]'
      '      ,[APP_NAME3]'
      '      ,[APP_ADDR1]'
      '      ,[APP_ADDR2]'
      '      ,[APP_ADDR3]'
      '      ,[AX_NAME1]'
      '      ,[AX_NAME2]'
      '      ,[AX_NAME3]'
      '      ,[SUP_CODE]'
      '      ,[SUP_NAME1]'
      '      ,[SUP_NAME2]'
      '      ,[SUP_NAME3]'
      '      ,[SUP_ADDR1]'
      '      ,[SUP_ADDR2]'
      '      ,[SUP_ADDR3]'
      '      ,[ITM_G1]'
      '      ,[ITM_G2]'
      '      ,[ITM_G3]'
      '      ,[TQTY]'
      '      ,[TQTYC]'
      '      ,[TAMT1]'
      '      ,[TAMT1C]'
      '      ,[TAMT2]'
      '      ,[TAMT2C]'
      '      ,[CHG_G]'
      '      ,[C1_AMT]'
      '      ,[C1_C]'
      '      ,[C2_AMT]'
      '      ,[LIC_INFO]'
      '      ,[LIC_NO]'
      '      ,[BK_CD]'
      '      ,[BK_NAME1]'
      '      ,[BK_NAME2]'
      '      ,[BK_NAME3]'
      '      ,[BK_NAME4]'
      '      ,[BK_NAME5]'
      '      ,[AC1_AMT]'
      '      ,[AC1_C]'
      '      ,[AC2_AMT]'
      '      ,[Comf_dat]'
      '      ,[PRNO]'
      '      ,[PCrLic_No]'
      '      ,[ChgCd]'
      '      ,[BankSend_No]'
      '      ,[APP_DATE]'
      '      ,[DOC_G]'
      '      ,[DOC_D]'
      '      ,[BHS_NO]'
      '      ,[BNAME]'
      '      ,[BNAME1]'
      '      ,[BAMT]'
      '      ,[BAMTC]'
      '      ,[EXP_DATE]'
      '      ,[SHIP_DATE]'
      '      ,[BSIZE1]'
      '      ,[BAL_CD]'
      '      ,[BAL_NAME1]'
      '      ,[BAL_NAME2]'
      '      ,[F_INTERFACE]'
      '      ,[ITM_GBN]'
      '      ,[ITM_GNAME]'
      '      ,[ISSUE_GBN]'
      '      ,[DOC_GBN]'
      '      ,[DOC_NO]'
      '      ,[BAMT2]'
      '      ,[BAMTC2]'
      '      ,[BAMT3]'
      '      ,[BAMTC3]'
      '      ,[PCRLIC_ISS_NO]'
      '      ,[BK_CD2]'
      '      ,[BAL_CD2]'
      '      ,[EMAIL_ID]'
      '      ,[EMAIL_DOMAIN]'
      
        '      ,N4025.NAME as ITM_GBN_NM, PCRLIC_G.NAME as ChgCd_NM, CHG_' +
        'G_T.NAME as CHG_G_NM'
      
        '  FROM [PCRLIC_H] LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHE' +
        'RE Prefix = '#39#45236#44397#49888'4025'#39') N4025 ON [PCRLIC_H].ITM_GBN = N4025.CODE '
      
        'LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39'PCRLI' +
        'C'#44396#48516#39') PCRLIC_G ON [PCRLIC_H].ChgCd = PCRLIC_G.CODE'
      
        'LEFT JOIN (SELECT CODE, NAME FROM CODE2NDD WHERE Prefix = '#39#48320#46041#44396#48516#39 +
        ') CHG_G_T ON [PCRLIC_H].CHG_G = CHG_G_T.CODE'
      '')
    Left = 8
    Top = 176
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_NAME1: TStringField
      FieldName = 'APP_NAME1'
      Size = 35
    end
    object qryListAPP_NAME2: TStringField
      FieldName = 'APP_NAME2'
      Size = 35
    end
    object qryListAPP_NAME3: TStringField
      FieldName = 'APP_NAME3'
      Size = 35
    end
    object qryListAPP_ADDR1: TStringField
      FieldName = 'APP_ADDR1'
      Size = 35
    end
    object qryListAPP_ADDR2: TStringField
      FieldName = 'APP_ADDR2'
      Size = 35
    end
    object qryListAPP_ADDR3: TStringField
      FieldName = 'APP_ADDR3'
      Size = 35
    end
    object qryListAX_NAME1: TStringField
      FieldName = 'AX_NAME1'
      Size = 35
    end
    object qryListAX_NAME2: TStringField
      FieldName = 'AX_NAME2'
      Size = 35
    end
    object qryListAX_NAME3: TStringField
      FieldName = 'AX_NAME3'
      Size = 10
    end
    object qryListSUP_CODE: TStringField
      FieldName = 'SUP_CODE'
      Size = 10
    end
    object qryListSUP_NAME1: TStringField
      FieldName = 'SUP_NAME1'
      Size = 35
    end
    object qryListSUP_NAME2: TStringField
      FieldName = 'SUP_NAME2'
      Size = 35
    end
    object qryListSUP_NAME3: TStringField
      FieldName = 'SUP_NAME3'
      Size = 35
    end
    object qryListSUP_ADDR1: TStringField
      FieldName = 'SUP_ADDR1'
      Size = 35
    end
    object qryListSUP_ADDR2: TStringField
      FieldName = 'SUP_ADDR2'
      Size = 35
    end
    object qryListSUP_ADDR3: TStringField
      FieldName = 'SUP_ADDR3'
      Size = 35
    end
    object qryListITM_G1: TStringField
      FieldName = 'ITM_G1'
      Size = 3
    end
    object qryListITM_G2: TStringField
      FieldName = 'ITM_G2'
      Size = 3
    end
    object qryListITM_G3: TStringField
      FieldName = 'ITM_G3'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListTAMT1: TBCDField
      FieldName = 'TAMT1'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListTAMT1C: TStringField
      FieldName = 'TAMT1C'
      Size = 3
    end
    object qryListTAMT2: TBCDField
      FieldName = 'TAMT2'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryListTAMT2C: TStringField
      FieldName = 'TAMT2C'
      Size = 3
    end
    object qryListCHG_G: TStringField
      FieldName = 'CHG_G'
      Size = 3
    end
    object qryListC1_AMT: TBCDField
      FieldName = 'C1_AMT'
      Precision = 18
    end
    object qryListC1_C: TStringField
      FieldName = 'C1_C'
      Size = 3
    end
    object qryListC2_AMT: TBCDField
      FieldName = 'C2_AMT'
      Precision = 18
    end
    object qryListLIC_INFO: TMemoField
      FieldName = 'LIC_INFO'
      BlobType = ftMemo
    end
    object qryListLIC_NO: TStringField
      FieldName = 'LIC_NO'
      Size = 35
    end
    object qryListBK_CD: TStringField
      FieldName = 'BK_CD'
      Size = 4
    end
    object qryListBK_NAME1: TStringField
      FieldName = 'BK_NAME1'
      Size = 70
    end
    object qryListBK_NAME2: TStringField
      FieldName = 'BK_NAME2'
      Size = 70
    end
    object qryListBK_NAME3: TStringField
      FieldName = 'BK_NAME3'
      Size = 35
    end
    object qryListBK_NAME4: TStringField
      FieldName = 'BK_NAME4'
      Size = 35
    end
    object qryListBK_NAME5: TStringField
      FieldName = 'BK_NAME5'
      Size = 10
    end
    object qryListAC1_AMT: TBCDField
      FieldName = 'AC1_AMT'
      Precision = 18
    end
    object qryListAC1_C: TStringField
      FieldName = 'AC1_C'
      Size = 3
    end
    object qryListAC2_AMT: TBCDField
      FieldName = 'AC2_AMT'
      Precision = 18
    end
    object qryListComf_dat: TStringField
      FieldName = 'Comf_dat'
      Size = 8
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListPCrLic_No: TStringField
      FieldName = 'PCrLic_No'
      Size = 35
    end
    object qryListChgCd: TStringField
      FieldName = 'ChgCd'
      Size = 3
    end
    object qryListBankSend_No: TStringField
      FieldName = 'BankSend_No'
      Size = 35
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 8
    end
    object qryListDOC_G: TStringField
      FieldName = 'DOC_G'
      Size = 3
    end
    object qryListDOC_D: TStringField
      FieldName = 'DOC_D'
      Size = 35
    end
    object qryListBHS_NO: TStringField
      FieldName = 'BHS_NO'
      Size = 10
    end
    object qryListBNAME: TStringField
      FieldName = 'BNAME'
    end
    object qryListBNAME1: TMemoField
      FieldName = 'BNAME1'
      BlobType = ftMemo
    end
    object qryListBAMT: TBCDField
      FieldName = 'BAMT'
      Precision = 18
    end
    object qryListBAMTC: TStringField
      FieldName = 'BAMTC'
      Size = 3
    end
    object qryListEXP_DATE: TStringField
      FieldName = 'EXP_DATE'
      Size = 8
    end
    object qryListSHIP_DATE: TStringField
      FieldName = 'SHIP_DATE'
      Size = 8
    end
    object qryListBSIZE1: TMemoField
      FieldName = 'BSIZE1'
      BlobType = ftMemo
    end
    object qryListBAL_CD: TStringField
      FieldName = 'BAL_CD'
      Size = 4
    end
    object qryListBAL_NAME1: TStringField
      FieldName = 'BAL_NAME1'
      Size = 70
    end
    object qryListBAL_NAME2: TStringField
      FieldName = 'BAL_NAME2'
      Size = 70
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListITM_GBN: TStringField
      FieldName = 'ITM_GBN'
      Size = 3
    end
    object qryListITM_GNAME: TStringField
      FieldName = 'ITM_GNAME'
      Size = 70
    end
    object qryListISSUE_GBN: TStringField
      FieldName = 'ISSUE_GBN'
      Size = 3
    end
    object qryListDOC_GBN: TStringField
      FieldName = 'DOC_GBN'
      Size = 3
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListBAMT2: TBCDField
      FieldName = 'BAMT2'
      Precision = 18
    end
    object qryListBAMTC2: TStringField
      FieldName = 'BAMTC2'
      Size = 3
    end
    object qryListBAMT3: TBCDField
      FieldName = 'BAMT3'
      Precision = 18
    end
    object qryListBAMTC3: TStringField
      FieldName = 'BAMTC3'
      Size = 3
    end
    object qryListPCRLIC_ISS_NO: TStringField
      FieldName = 'PCRLIC_ISS_NO'
      Size = 35
    end
    object qryListBK_CD2: TStringField
      FieldName = 'BK_CD2'
      Size = 6
    end
    object qryListBAL_CD2: TStringField
      FieldName = 'BAL_CD2'
      Size = 6
    end
    object qryListEMAIL_ID: TStringField
      FieldName = 'EMAIL_ID'
      Size = 35
    end
    object qryListEMAIL_DOMAIN: TStringField
      FieldName = 'EMAIL_DOMAIN'
      Size = 35
    end
    object qryListITM_GBN_NM: TStringField
      FieldName = 'ITM_GBN_NM'
      Size = 100
    end
    object qryListChgCd_NM: TStringField
      FieldName = 'ChgCd_NM'
      Size = 100
    end
    object qryListCHG_G_NM: TStringField
      FieldName = 'CHG_G_NM'
      Size = 100
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 40
    Top = 176
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryDetailAfterOpen
    AfterScroll = qryDetailAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = 'RG510TD2'
      end>
    SQL.Strings = (
      'SELECT [KEYY]'
      '      ,[SEQ]'
      '      ,[HS_NO]'
      '      ,[NAME]'
      '      ,[NAME1]'
      '      ,[SIZE]'
      '      ,[SIZE1]'
      '      ,[REMARK]'
      '      ,[QTY]'
      '      ,[QTYC]'
      '      ,[AMT1]'
      '      ,[AMT1C]'
      '      ,[AMT2]'
      '      ,[PRI1]'
      '      ,[PRI2]'
      '      ,[PRI_BASE]'
      '      ,[PRI_BASEC]'
      '      ,[ACHG_G]'
      '      ,[AC1_AMT]'
      '      ,[AC1_C]'
      '      ,[AC2_AMT]'
      '      ,[LOC_TYPE]'
      '      ,[NAMESS]'
      '      ,[SIZESS]'
      '      ,[APP_DATE]'
      '      ,[ITM_NUM]'
      '      ,[COMPANY_CD]'
      '  FROM [PCRLICD1]'
      '  WHERE KEYY = :KEYY')
    Left = 8
    Top = 208
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailHS_NO: TStringField
      FieldName = 'HS_NO'
      EditMask = '9999.99-9999;0l'
      Size = 10
    end
    object qryDetailNAME: TStringField
      FieldName = 'NAME'
    end
    object qryDetailNAME1: TMemoField
      FieldName = 'NAME1'
      OnGetText = qryDetailNAME1GetText
      BlobType = ftMemo
    end
    object qryDetailSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryDetailSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryDetailREMARK: TMemoField
      FieldName = 'REMARK'
      BlobType = ftMemo
    end
    object qryDetailQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryDetailQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryDetailAMT1: TBCDField
      FieldName = 'AMT1'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryDetailAMT1C: TStringField
      FieldName = 'AMT1C'
      Size = 3
    end
    object qryDetailAMT2: TBCDField
      FieldName = 'AMT2'
      Precision = 18
    end
    object qryDetailPRI1: TBCDField
      FieldName = 'PRI1'
      Precision = 18
    end
    object qryDetailPRI2: TBCDField
      FieldName = 'PRI2'
      DisplayFormat = '#,0.000'
      Precision = 18
    end
    object qryDetailPRI_BASE: TBCDField
      FieldName = 'PRI_BASE'
      Precision = 18
    end
    object qryDetailPRI_BASEC: TStringField
      FieldName = 'PRI_BASEC'
      Size = 3
    end
    object qryDetailACHG_G: TStringField
      FieldName = 'ACHG_G'
      Size = 3
    end
    object qryDetailAC1_AMT: TBCDField
      FieldName = 'AC1_AMT'
      Precision = 18
    end
    object qryDetailAC1_C: TStringField
      FieldName = 'AC1_C'
      Size = 3
    end
    object qryDetailAC2_AMT: TBCDField
      FieldName = 'AC2_AMT'
      Precision = 18
    end
    object qryDetailLOC_TYPE: TStringField
      FieldName = 'LOC_TYPE'
      Size = 3
    end
    object qryDetailNAMESS: TStringField
      FieldName = 'NAMESS'
    end
    object qryDetailSIZESS: TStringField
      FieldName = 'SIZESS'
    end
    object qryDetailAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999.99.99;0;'
      Size = 8
    end
    object qryDetailITM_NUM: TStringField
      FieldName = 'ITM_NUM'
      Size = 35
    end
    object qryDetailCOMPANY_CD: TStringField
      FieldName = 'COMPANY_CD'
      Size = 35
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 40
    Top = 208
  end
  object qryTax: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryTaxAfterOpen
    AfterScroll = qryTaxAfterScroll
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      'SELECT KEYY, SEQ, DATEE, BILL_NO, BILL_DATE,'
      
        'ITEM_NAME1, ITEM_NAME2, ITEM_NAME3, ITEM_NAME4, ITEM_NAME5, ITEM' +
        '_DESC, BILL_AMOUNT, BILL_AMOUNT_UNIT, TAX_AMOUNT, TAX_AMOUNT_UNI' +
        'T, QUANTITY, QUANTITY_UNIT'
      'FROM APPPCR_TAX'
      'WHERE KEYY = :KEYY')
    Left = 8
    Top = 240
    object qryTaxKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxDATEE: TStringField
      FieldName = 'DATEE'
      Size = 8
    end
    object qryTaxBILL_NO: TStringField
      FieldName = 'BILL_NO'
      Size = 35
    end
    object qryTaxBILL_DATE: TStringField
      FieldName = 'BILL_DATE'
      EditMask = '9999-99-99;0;'
      Size = 8
    end
    object qryTaxITEM_NAME1: TStringField
      FieldName = 'ITEM_NAME1'
      Size = 35
    end
    object qryTaxITEM_NAME2: TStringField
      FieldName = 'ITEM_NAME2'
      Size = 35
    end
    object qryTaxITEM_NAME3: TStringField
      FieldName = 'ITEM_NAME3'
      Size = 35
    end
    object qryTaxITEM_NAME4: TStringField
      FieldName = 'ITEM_NAME4'
      Size = 35
    end
    object qryTaxITEM_NAME5: TStringField
      FieldName = 'ITEM_NAME5'
      Size = 35
    end
    object qryTaxITEM_DESC: TMemoField
      FieldName = 'ITEM_DESC'
      BlobType = ftMemo
    end
    object qryTaxBILL_AMOUNT: TBCDField
      FieldName = 'BILL_AMOUNT'
      DisplayFormat = '#,0;0'
      Precision = 18
    end
    object qryTaxBILL_AMOUNT_UNIT: TStringField
      FieldName = 'BILL_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxTAX_AMOUNT: TBCDField
      FieldName = 'TAX_AMOUNT'
      DisplayFormat = '#,0;0'
      Precision = 18
    end
    object qryTaxTAX_AMOUNT_UNIT: TStringField
      FieldName = 'TAX_AMOUNT_UNIT'
      Size = 3
    end
    object qryTaxQUANTITY: TBCDField
      FieldName = 'QUANTITY'
      DisplayFormat = '#,0;0'
      Precision = 18
    end
    object qryTaxQUANTITY_UNIT: TStringField
      FieldName = 'QUANTITY_UNIT'
      Size = 3
    end
    object qryTaxSEQ: TIntegerField
      FieldName = 'SEQ'
    end
  end
  object dsTax: TDataSource
    DataSet = qryTax
    Left = 40
    Top = 240
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = A4
    Left = 8
    Top = 280
  end
end
