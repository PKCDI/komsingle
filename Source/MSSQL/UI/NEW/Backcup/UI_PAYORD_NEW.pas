unit UI_PAYORD_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, Grids, DBGrids, acDBGrid, Buttons, sBitBtn, QuickRpt,
  QRCtrls, StdCtrls, sComboBox, Mask, sMaskEdit, sEdit, sCheckBox, sButton,
  sLabel, sSpeedButton, ExtCtrls, sPanel, ComCtrls, sPageControl,
  sSkinProvider, acAlphaHints, acImage, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, DB, ADODB, StrUtils, CodeDialogParent, DateUtils, Clipbrd;

type
  TUI_PAYORD_NEW_frm = class(TChildForm_frm)
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sTabSheet3: TsTabSheet;
    btn_Panel: TsPanel;
    sPanel1: TsPanel;
    sPanel3: TsPanel;
    sTabSheet2: TsTabSheet;
    mskDate1: TsMaskEdit;
    mskDate2: TsMaskEdit;
    btnFind: TsBitBtn;
    sSpeedButton9: TsSpeedButton;
    comFind: TsComboBox;
    edtFind: TsEdit;
    sPanel2: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel4: TsPanel;
    sPanel5: TsPanel;
    sDBGrid2: TsDBGrid;
    sPanel6: TsPanel;
    mskAPP_DATE: TsMaskEdit;
    mskEXE_DATE: TsMaskEdit;
    mskIMP_DATE: TsMaskEdit;
    mskHS_CODE: TsMaskEdit;
    edtADREFNO: TsEdit;
    edtBF_CODE: TsEdit;
    sEdit3: TsEdit;
    edtBUS_CD: TsEdit;
    sEdit5: TsEdit;
    edtBUS_CD1: TsEdit;
    sEdit7: TsEdit;
    edtSND_CD: TsEdit;
    sEdit9: TsEdit;
    edtCHARGE_TO: TsEdit;
    sEdit11: TsEdit;
    edtPAY_AMTC: TsEdit;
    edtPAY_AMT2C: TsEdit;
    currPAY_AMT: TsCurrencyEdit;
    currPAY_AMT2: TsCurrencyEdit;
    edtPAYDET1: TsEdit;
    edtPAYDET2: TsEdit;
    edtPAYDET3: TsEdit;
    edtPAYDET4: TsEdit;
    edtREMARK1: TsEdit;
    edtREMARK2: TsEdit;
    edtREMARK3: TsEdit;
    edtREMARK4: TsEdit;
    edtREMARK5: TsEdit;
    sImage1: TsImage;
    sAlphaHints1: TsAlphaHints;
    sPanel9: TsPanel;
    sDBGrid3: TsDBGrid;
    sPanel8: TsPanel;
    sPanel10: TsPanel;
    sPanel7: TsPanel;
    edtMAINT_NO: TsEdit;
    mskDATEE: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edtUSER_ID: TsEdit;
    sSpeedButton1: TsSpeedButton;
    sLabel7: TsLabel;
    sLabel1: TsLabel;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    sSpeedButton6: TsSpeedButton;
    btnCopy: TsButton;
    sSpeedButton7: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    sSpeedButton5: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    btnExit: TsButton;
    btnReady: TsButton;
    btnSend: TsButton;
    sPanel11: TsPanel;
    sPanel12: TsPanel;
    sPanel13: TsPanel;
    edtOD_BANK: TsEdit;
    edtOD_BANK1: TsEdit;
    edtOD_BANK2: TsEdit;
    edtOD_NAME1: TsEdit;
    edtOD_NAME2: TsEdit;
    edtOD_Nation: TsEdit;
    edtOD_CURR: TsEdit;
    edtOD_ACCNT2: TsEdit;
    edtOD_ACCNT1: TsEdit;
    edtEC_BANK: TsEdit;
    edtEC_BANK1: TsEdit;
    edtEC_BANK2: TsEdit;
    edtEC_NAME1: TsEdit;
    edtEC_NAME2: TsEdit;
    edtEC_Nation: TsEdit;
    edtEC_ACCNT2: TsEdit;
    edtEC_ACCNT1: TsEdit;
    sPanel15: TsPanel;
    sPanel14: TsPanel;
    sPanel16: TsPanel;
    sPanel17: TsPanel;
    edtBN_BANK: TsEdit;
    edtBN_BANK1: TsEdit;
    edtBN_BANK2: TsEdit;
    edtBN_NAME1: TsEdit;
    edtBN_NAME2: TsEdit;
    edtBN_Nation: TsEdit;
    edtBN_CURR: TsEdit;
    edtBN_ACCNT2: TsEdit;
    edtBN_ACCNT1: TsEdit;
    edtIT_BANK: TsEdit;
    edtIT_BANK1: TsEdit;
    edtIT_BANK2: TsEdit;
    edtIT_NAME1: TsEdit;
    edtIT_NAME2: TsEdit;
    edtIT_Nation: TsEdit;
    edtIT_CURR: TsEdit;
    edtIT_ACCNT2: TsEdit;
    edtIT_ACCNT1: TsEdit;
    edtAPP_NAME1: TsEdit;
    edtAPP_NAME2: TsEdit;
    edtAPP_CODE: TsEdit;
    edtAPP_NAME3: TsEdit;
    edtAPP_STR1: TsEdit;
    edtAPP_TELE: TsEdit;
    edtBEN_CODE: TsEdit;
    edtDOC_GU: TsEdit;
    sEdit26: TsEdit;
    mskDOC_DTE1: TsMaskEdit;
    edtDOC_NO1: TsEdit;
    edtSND_CODE: TsEdit;
    edtSND_NAME1: TsEdit;
    edtSND_NAME2: TsEdit;
    edtSND_NAME3: TsEdit;
    edtBEN_NAME1: TsEdit;
    edtBEN_NAME2: TsEdit;
    edtBEN_NAME3: TsEdit;
    edtBEN_STR1: TsEdit;
    edtBEN_STR2: TsEdit;
    edtBEN_TELE: TsEdit;
    edtBEN_STR3: TsEdit;
    edtBEN_NATION: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_DATE: TStringField;
    qryListEXE_DATE: TStringField;
    qryListIMP_DATE: TStringField;
    qryListHS_CODE: TStringField;
    qryListADREFNO: TStringField;
    qryListBUS_CD: TStringField;
    qryListBUS_CD1: TStringField;
    qryListSND_CD: TStringField;
    qryListCHARGE_TO: TStringField;
    qryListPAY_AMT: TBCDField;
    qryListPAY_AMTC: TStringField;
    qryListOD_BANK: TStringField;
    qryListOD_BANK1: TStringField;
    qryListOD_BANK2: TStringField;
    qryListOD_NAME1: TStringField;
    qryListOD_NAME2: TStringField;
    qryListOD_ACCNT1: TStringField;
    qryListOD_ACCNT2: TStringField;
    qryListOD_CURR: TStringField;
    qryListOD_NATION: TStringField;
    qryListBN_BANK1: TStringField;
    qryListBN_BANK2: TStringField;
    qryListBN_NAME1: TStringField;
    qryListBN_NAME2: TStringField;
    qryListBN_ACCNT1: TStringField;
    qryListBN_ACCNT2: TStringField;
    qryListBN_CURR: TStringField;
    qryListBN_NATION: TStringField;
    qryListIT_ACCNT: TStringField;
    qryListIT_PCOD4: TStringField;
    qryListIT_BANK: TStringField;
    qryListIT_BANK1: TStringField;
    qryListIT_BANK2: TStringField;
    qryListIT_NAME1: TStringField;
    qryListIT_NAME2: TStringField;
    qryListIT_ACCNT1: TStringField;
    qryListIT_ACCNT2: TStringField;
    qryListIT_CURR: TStringField;
    qryListIT_NATION: TStringField;
    qryListEC_BANK: TStringField;
    qryListEC_BANK1: TStringField;
    qryListEC_BANK2: TStringField;
    qryListEC_NAME1: TStringField;
    qryListEC_NAME2: TStringField;
    qryListEC_ACCNT1: TStringField;
    qryListEC_ACCNT2: TStringField;
    qryListEC_NATION: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListAPP_TELE: TStringField;
    qryListAPP_STR1: TStringField;
    qryListBEN_CODE: TStringField;
    qryListBEN_NAME1: TStringField;
    qryListBEN_NAME2: TStringField;
    qryListBEN_NAME3: TStringField;
    qryListBEN_TELE: TStringField;
    qryListBEN_STR1: TStringField;
    qryListBEN_STR2: TStringField;
    qryListBEN_STR3: TStringField;
    qryListBEN_NATION: TStringField;
    qryListSND_CODE: TStringField;
    qryListSND_NAME1: TStringField;
    qryListSND_NAME2: TStringField;
    qryListSND_NAME3: TStringField;
    qryListREMARK: TBooleanField;
    qryListREMARK1: TStringField;
    qryListREMARK2: TStringField;
    qryListREMARK3: TStringField;
    qryListREMARK4: TStringField;
    qryListREMARK5: TStringField;
    qryListDOC_GU: TStringField;
    qryListDOC_NO1: TStringField;
    qryListDOC_DTE1: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPAY_AMT2: TBCDField;
    qryListPAY_AMT2C: TStringField;
    qryListBF_CODE: TStringField;
    qryListPAYDET1: TStringField;
    qryListPAYDET2: TStringField;
    qryListPAYDET3: TStringField;
    qryListPAYDET4: TStringField;
    qryListPAYDET5: TStringField;
    qryListBN_BANK: TStringField;
    sImage2: TsImage;
    qryListBF_CODE_NM: TStringField;
    qryListBUS_CD_NM: TStringField;
    qryListBUS_CD1_NM: TStringField;
    qryListSND_CD_NM: TStringField;
    qryListCHARGE_TO_NM: TStringField;
    sImage3: TsImage;
    qryListDOC_GU_NM: TStringField;
    sPanel18: TsPanel;
    sLabel2: TsLabel;
    sPanel19: TsPanel;
    sLabel3: TsLabel;
    sPanel20: TsPanel;
    sLabel4: TsLabel;
    sCheckBox1: TsCheckBox;
    qryReady: TADOQuery;
    procedure sImage2Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure edtFindKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sDBGrid1TitleClick(Column: TColumn);
    procedure edtBF_CODEDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure sPageControl1Change(Sender: TObject);
    procedure sPageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure edtAPP_CODEDblClick(Sender: TObject);
    procedure edtBF_CODEChange(Sender: TObject);
    procedure edtBF_CODEKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtOD_BANKDblClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure sDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btnReadyClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure btnExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    FSQL : String;
    CDDialog : TCodeDialogParent_frm;
    function ReadList(SortCol : TColumn = nil):integer;
    function chkDocOver(DocNo : String):Boolean;
    procedure getData;
    procedure ClearColTitle(igoreCol : String);
    procedure NewDocument;
    procedure EditDocument;
    procedure BtnControl;
    procedure PanelControl;
    procedure showHintControl(Control: TControl; msg: String;
      showDelay: integer=0);
    procedure ReadyDocument(DocNo: String);
    procedure RunSQL(sSQL: TStream);
  public
    { Public declarations }
  end;

var
  UI_PAYORD_NEW_frm: TUI_PAYORD_NEW_frm;

implementation

uses
  ICON, MSSQL, dlgPAYORDCopy, CD_PAYORDBFCD, CD_4487, CD_4383, CD_SNDCD, CD_ChargeTo, CD_UNIT, CD_AMT_UNIT, CD_NATION, AutoNo, VarDefine, CodeContents, CD_CUST, CD_PAYORD1001, CD_BANK, Commonlib, SQLCreator, MessageDefine, DocumentSend, Dlg_RecvSelect, CreateDocuments, QR_PAYORD_PRN, Preview;

{$R *.dfm}

procedure TUI_PAYORD_NEW_frm.getData;
begin
//------------------------------------------------------------------------------
// HEADER
//------------------------------------------------------------------------------
  // 관리번호
  edtMAINT_NO.Text := qryListMAINT_NO.AsString;
  // 사용자
  edtUSER_ID.Text := qryListUSER_ID.AsString;
  // 등록일자
  mskDATEE.Text := qryListDATEE.AsString;
  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 1);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);
//------------------------------------------------------------------------------
// PAGE 1
//------------------------------------------------------------------------------
  // 신청일자
  mskAPP_DATE.Text := qryListAPP_DATE.AsString;
  // 이체예정일자
  mskEXE_DATE.Text := qryListEXE_DATE.AsString;
  // 수입예정일
  mskIMP_DATE.Text := qryListIMP_DATE.AsString;
  // 세번부호
  mskHS_CODE.Text := qryListHS_CODE.AsString;
  // 기타참조번호
  edtADREFNO.Text := qryListADREFNO.AsString;
  // 지급총액, 단위
  edtPAY_AMTC.Text  := qryListPAY_AMTC.AsString;
  currPAY_AMT.Value := qryListPAY_AMT.AsFloat;
  // 외화계좌 지급금액, 단위
  edtPAY_AMT2C.Text  := qryListPAY_AMT2C.AsString;
  currPAY_AMT2.Value := qryListPAY_AMT2.AsFloat;
  // 거래형태구분
  edtBF_CODE.Text := qryListBF_CODE.AsString;
  sEdit3.Text := qryListBF_CODE_NM.AsString;
  // 지급지시서용도
  edtBUS_CD.Text := qryListBUS_CD.AsString;
  sEdit5.Text := qryListBUS_CD_NM.AsString;
  // 납부수수료 유형
  edtBUS_CD1.Text := qryListBUS_CD1.AsString;
  sEdit7.Text := qryListBUS_CD1_NM.AsString;
  // 송금방법
  edtSND_CD.Text := qryListSND_CD.AsString;
  sEdit9.Text := qryListSND_CD_NM.AsString;
  // 수수료부담자
  edtCHARGE_TO.Text := qryListCHARGE_TO.AsString;
  sEdit11.Text := qryListCHARGE_TO_NM.AsString;
  // 송금내역
  edtPAYDET1.Text := qryListPAYDET1.AsString;
  edtPAYDET2.Text := qryListPAYDET2.AsString;
  edtPAYDET3.Text := qryListPAYDET3.AsString;
  edtPAYDET4.Text := qryListPAYDET4.AsString;
  // 기타정보
  edtREMARK1.Text := qryListREMARK1.AsString;
  edtREMARK2.Text := qryListREMARK2.AsString;
  edtREMARK3.Text := qryListREMARK3.AsString;
  edtREMARK4.Text := qryListREMARK4.AsString;
  edtREMARK5.Text := qryListREMARK5.AsString;
  // 지급의뢰인
  edtAPP_CODE.Text  := qryListAPP_CODE.AsString;
  edtAPP_NAME1.Text := qryListAPP_NAME1.AsString;
  edtAPP_NAME2.Text := qryListAPP_NAME2.AsString;
  edtAPP_NAME3.Text := qryListAPP_NAME3.AsString;
  edtAPP_STR1.Text  := qryListAPP_STR1.AsString;
  edtAPP_TELE.Text  := qryListAPP_TELE.AsString;
  // 수익자
  edtBEN_CODE.Text  := qryListBEN_CODE.AsString;
  edtBEN_NAME1.Text := qryListBEN_NAME1.AsString;
  edtBEN_NAME2.Text := qryListBEN_NAME2.AsString;
  edtBEN_NAME3.Text := qryListBEN_NAME3.AsString;
  edtBEN_STR1.Text  := qryListBEN_STR1.AsString;
  edtBEN_STR2.Text  := qryListBEN_STR2.AsString;
  edtBEN_STR3.Text  := qryListBEN_STR3.AsString;
  edtBEN_TELE.Text  := qryListBEN_TELE.AsString;
  edtBEN_NATION.Text := qryListBEN_NATION.AsString;
  // 신청업체
  edtSND_CODE.Text := qryListSND_CODE.AsString;
  edtSND_NAME1.Text := qryListSND_NAME1.AsString;
  edtSND_NAME2.Text := qryListSND_NAME2.AsString;
  edtSND_NAME3.Text := qryListSND_NAME3.AsString;
//------------------------------------------------------------------------------
// PAGE 2
//------------------------------------------------------------------------------
  // 지급의뢰인 - 외화원금계좌
  edtOD_BANK.Text := qryListOD_BANK.AsString;
  edtOD_Nation.Text := qryListOD_NATION.AsString;
  edtOD_BANK1.Text := qryListOD_BANK1.AsString;
  edtOD_BANK2.Text := qryListOD_BANK2.AsString;
  edtOD_NAME1.Text := qryListOD_NAME1.AsString;
  edtOD_NAME2.Text := qryListOD_NAME2.AsString;
  edtOD_ACCNT1.Text := qryListOD_ACCNT1.AsString;
  edtOD_ACCNT2.Text := qryListOD_ACCNT2.AsString;
  edtOD_CURR.Text := qryListOD_CURR.AsString;
  // 지급의뢰인 - 원화원금계좌
  edtEC_BANK.Text := qryListEC_BANK.AsString;
  edtEC_Nation.Text := qryListEC_NATION.AsString;
  edtEC_BANK1.Text := qryListEC_BANK1.AsString;
  edtEC_BANK2.Text := qryListEC_BANK2.AsString;
  edtEC_NAME1.Text := qryListEC_NAME1.AsString;
  edtEC_NAME2.Text := qryListEC_NAME2.AsString;
  edtEC_ACCNT1.Text := qryListEC_ACCNT1.AsString;
  edtEC_ACCNT2.Text := qryListEC_ACCNT2.AsString;
  // 수익자
  edtBN_BANK.Text := qryListBN_BANK.AsString;
  edtBN_Nation.Text := qryListBN_NATION.AsString;
  edtBN_BANK1.Text := qryListBN_BANK1.AsString;
  edtBN_BANK2.Text := qryListBN_BANK2.AsString;
  edtBN_NAME1.Text := qryListBN_NAME1.AsString;
  edtBN_NAME2.Text := qryListBN_NAME2.AsString;
  edtBN_ACCNT1.Text := qryListBN_ACCNT1.AsString;
  edtBN_ACCNT2.Text := qryListBN_ACCNT2.AsString;
  edtBN_CURR.Text := qryListBN_CURR.AsString;
  // 중간경유은행
  edtIT_BANK.Text := qryListIT_BANK.AsString;
  edtIT_Nation.Text := qryListIT_NATION.AsString;
  edtIT_BANK1.Text := qryListIT_BANK1.AsString;
  edtIT_BANK2.Text := qryListIT_BANK2.AsString;
  edtIT_NAME1.Text := qryListIT_NAME1.AsString;
  edtIT_NAME2.Text := qryListIT_NAME2.AsString;
  edtIT_ACCNT1.Text := qryListIT_ACCNT1.AsString;
  edtIT_ACCNT2.Text := qryListIT_ACCNT2.AsString;
  edtIT_CURR.Text := qryListIT_CURR.AsString;
  // 입금관련서류
  edtDOC_GU.Text := qryListDOC_GU.AsString;
  sEdit26.Text := qryListDOC_GU_NM.AsString;
  mskDOC_DTE1.Text := qryListDOC_DTE1.AsString;
  edtDOC_NO1.Text := qryListDOC_NO1.AsString;
end;

function TUI_PAYORD_NEW_frm.ReadList(SortCol: TColumn): Integer;
begin
  // 입력, 수정중일경우에는 작동불가
  if Self.Tag <> 0 Then Exit;

  Result := -1;

  with qryList do
  begin
    Close;
    SQL.Text := FSQL;
    //FDate
    Parameters[0].Value := mskDate1.Text;
    //TDate
    Parameters[1].Value := mskDate2.Text;
    //MAINT_NO
    Parameters[2].Value := Trim('%'+edtFind.Text+'%');
    //All_Data
    if Length(edtFind.Text) > 0 Then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    if SortCol = nil then
    begin
      ClearColTitle('');
      SQL.Add('ORDER BY DATEE');
    end
    else
    begin
      if Pos('↑', SortCol.Title.Caption) > 0 Then
      begin
        SQL.Add('ORDER BY '+SortCol.FieldName+' DESC');
        SortCol.Title.Caption := AnsiReplaceText(SortCol.Title.Caption,'↑','↓')
      end
      else
      if Pos('↓', SortCol.Title.Caption) > 0 Then
      begin
        SQL.Add('ORDER BY '+SortCol.FieldName+' ASC');
        SortCol.Title.Caption := AnsiReplaceText(SortCol.Title.Caption,'↓','↑');
      end
      else
      begin
        SQL.Add('ORDER BY '+SortCol.FieldName+' ASC');
        SortCol.Title.Caption := SortCol.Title.Caption+'↑';
      end;

      ClearColTitle(SortCol.FieldName);
    end;
    Open;
  end;
end;

procedure TUI_PAYORD_NEW_frm.sImage2Click(Sender: TObject);
begin
  inherited;
  dlgPAYORDCopy_frm := TdlgPAYORDCopy_frm.Create(Self);
  try
    dlgPAYORDCopy_frm.ShowModal;
  finally
    FreeAndNil(dlgPAYORDCopy_frm);
  end;
end;

procedure TUI_PAYORD_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  getData;
  BtnControl;  
end;

procedure TUI_PAYORD_NEW_frm.edtFindKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  inherited;
  IF Key = VK_RETURN Then
    ReadList;
end;

procedure TUI_PAYORD_NEW_frm.ClearColTitle(igoreCol : String);
var
  i : Integer;
begin
  for i := 0 to sDBGrid1.Columns.Count-1 do
  begin
    if sDBGrid1.Columns[i].FieldName <> igoreCol Then
      sDBGrid1.Columns[i].Title.Caption := AnsiReplaceText(AnsiReplaceText(sDBGrid1.Columns[i].Title.Caption, '↑', ''),'↓','');
  end;
end;

procedure TUI_PAYORD_NEW_frm.sDBGrid1TitleClick(Column: TColumn);
begin
  inherited;
  ReadList(Column);
end;

procedure TUI_PAYORD_NEW_frm.edtBF_CODEDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  Case (Sender as TsEdit).Tag of
    // 통화단위
    98, 99, 107, 110, 112 : CDDialog := TCD_AMT_UNIT_frm.Create(Self);
    // 거래형태구분
    100 : CDDialog := TCD_PAYORDBFCD_frm.Create(Self);
    // 지급지시서용도
    101 : CDDialog := TCD_4487_frm.Create(Self);
    // 납부수수료유형
    102 : CDDialog := TCD_4383_frm.Create(Self);
    // 송금방법
    103 : CDDialog := TCD_SNDCD_frm.Create(Self);
    // 수수료부담자
    104 : CDDialog := TCD_ChargeTo_frm.Create(Self);
    // 국가
    105, 106, 108, 109, 111 : CDDialog := TCD_NATION_frm.Create(Self);
    // 입금관련서류
    113 : CDDialog := TCD_PAYORD1001_frm.Create(Self);
  end;

  try
    if CDDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CDDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        100 : sEdit3.Text := CDDialog.Values[1];
        101 : sEdit5.Text := CDDialog.Values[1];
        102 : sEdit7.Text := CDDialog.Values[1];
        103 : sEdit9.Text := CDDialog.Values[1];
        104 : sEdit11.Text := CDDialog.Values[1];
        113 : sEdit26.Text := CDDialog.Values[1];
      end;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

procedure TUI_PAYORD_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_PAYORD_NEW_frm := nil;
end;

procedure TUI_PAYORD_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  mskDate1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  mskDate2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  ReadOnlyControl(sPanel7);
  ReadOnlyControl(sPanel6);
  ReadOnlyControl(sPanel12);

  ReadList;
  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_PAYORD_NEW_frm.btnFindClick(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_PAYORD_NEW_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  NewDocument;
end;

procedure TUI_PAYORD_NEW_frm.NewDocument;
begin
  Self.Tag := 1;
  BtnControl;
  PanelControl;
  //컨트롤 입력가능
  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel6, False);
  ReadOnlyControl(sPanel12, False);

  //초기화
  ClearControl(sPanel7);
  ClearControl(sPanel6);
  ClearControl(sPanel12);

  //공통 데이터 초기화
  edtMAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('PAYORD');
  mskDATEE.Text := FormatDateTime('YYYYMMDD', Now);
  edtUSER_ID.Text := LoginData.sID;
  com_func.ItemIndex := 1;
  com_type.ItemIndex := 0;

//------------------------------------------------------------------------------
// PAGE 1
//------------------------------------------------------------------------------
  mskAPP_DATE.Text := FormatDateTime('YYYYMMDD',Now);
  mskEXE_DATE.Text := FormatDateTime('YYYYMMDD',Now);
  mskIMP_DATE.Clear;
  mskHS_CODE.Clear;

  // 신청업체
  //------------------------------------------------------------------------------
  // 개설의뢰인 셋팅
  //------------------------------------------------------------------------------
  IF DMCodeContents.GEOLAECHEO.Locate('CODE','00000',[]) Then
  begin
      edtSND_CODE.Text  :=  DMCodeContents.GEOLAECHEO.FieldByName('CODE').AsString;
      edtSND_NAME1.Text :=  DMCodeContents.GEOLAECHEO.FieldByName('ENAME').AsString;
      edtSND_NAME2.Text :=  DMCodeContents.GEOLAECHEO.FieldByName('REP_NAME').AsString;
      edtSND_NAME3.Text := DMCodeContents.GEOLAECHEO.FieldByName('jenja').AsString;
  end;

  sPageControl1.ActivePageIndex := 1;
end;

procedure TUI_PAYORD_NEW_frm.BtnControl;
begin
  btnNew.Enabled := Self.Tag = 0;
  btnEdit.Enabled := btnNew.Enabled and (qryList.RecordCount > 0);
  btnDel.Enabled := btnEdit.Enabled;
  btnCopy.Enabled := btnEdit.Enabled;

  btnTemp.Enabled := not btnNew.Enabled;
  btnSave.Enabled := btnTemp.Enabled;
  btnCancel.Enabled := btnTemp.Enabled;

  btnPrint.Enabled := btnEdit.Enabled;
  btnSend.Enabled  := btnEdit.Enabled;
  btnReady.Enabled  := btnEdit.Enabled;
end;

procedure TUI_PAYORD_NEW_frm.btnCancelClick(Sender: TObject);
begin
  inherited;
  if Self.Tag = 2 Then
  begin
    sDBGrid1.Enabled := True;
    sDBGrid2.Enabled := True;
    sDBGrid3.Enabled := True;
  end;

  Self.Tag := 0;
  BtnControl;
  PanelControl;
  ReadOnlyControl(sPanel7, True);
  ReadOnlyControl(sPanel6, True);
  ReadOnlyControl(sPanel12, True);

  getData;

end;

procedure TUI_PAYORD_NEW_frm.sPageControl1Change(Sender: TObject);
var
  AllowChg : Boolean;
begin
  inherited;
  if (Self.Tag in [1,2]) and (sPageControl1.ActivePageIndex = 0) then
    AllowChg := False
  else
    AllowChg := true;
    
  sPageControl1Changing(Sender, AllowChg);
end;

procedure TUI_PAYORD_NEW_frm.sPageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
//  ShowMessage(IntToStr((Sender as TsPageControl).ActivePageIndex));
end;

procedure TUI_PAYORD_NEW_frm.PanelControl;
begin
  if Self.Tag = 1 Then
  begin
    sPanel18.Left := 0;
    sPanel18.Top := 43;
    sPanel18.Visible := True;
    sPanel19.Left := 1;
    sPanel19.Top := 1;
    sPanel19.Visible := True;
    sPanel20.Left := 1;
    sPanel20.Top := 1;
    sPanel20.Visible := True;
  end
  else
  begin
    sPanel18.Visible := False;
    sPanel19.Visible := False;
    sPanel20.Visible := False;
  end;
end;

procedure TUI_PAYORD_NEW_frm.edtAPP_CODEDblClick(Sender: TObject);
begin
  IF (Sender as TsEdit).ReadOnly Then Exit;

  CD_CUST_frm := TCD_CUST_frm.Create(Self);
  try
    if CD_CUST_frm.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CD_CUST_frm.Values[0];

      case (Sender as TsEdit).Tag of
        //지급의뢰인
        101:
        begin
          edtAPP_NAME1.Text := CD_CUST_frm.FieldValues('ENAME');
          edtAPP_NAME2.Text := CD_CUST_frm.FieldValues('REP_NAME');
          edtAPP_NAME3.Text := CD_CUST_frm.FieldValues('SAUP_NO');
          edtAPP_STR1.Text := CD_CUST_frm.FieldValues('ADDR1');
          edtAPP_TELE.Text := CD_CUST_frm.FieldValues('REP_TEL');
        end;
        //수익자
        102:
        begin
          edtBEN_NAME1.Text := CD_CUST_frm.FieldValues('ENAME');
          edtBEN_NAME2.Text := CD_CUST_frm.FieldValues('REP_NAME');
          edtBEN_NAME3.Text := CD_CUST_frm.FieldValues('jenja');
          edtBEN_STR1.Text  := CD_CUST_frm.FieldValues('ADDR1');
          edtBEN_STR2.Text  := CD_CUST_frm.FieldValues('ADDR2');
          edtBEN_STR3.Text  := CD_CUST_frm.FieldValues('ADDR3');
          edtBEN_TELE.Text  := CD_CUST_frm.FieldValues('REP_TEL');
          edtBEN_NATION.Text := CD_CUST_frm.FieldValues('NAT');
        end;
        //신청업체
        103:
        begin
          edtSND_NAME1.Text := CD_CUST_frm.FieldValues('ENAME');
          edtSND_NAME2.Text := CD_CUST_frm.FieldValues('REP_NAME');
          edtSND_NAME3.Text := CD_CUST_frm.FieldValues('jenja');
        end;
      end;

    end;

  finally
    FreeAndNil(CD_CUST_frm);
  end;
end;

procedure TUI_PAYORD_NEW_frm.edtBF_CODEChange(Sender: TObject);
begin
  inherited;
  IF Length((Sender as TsEdit).Text) = 0 then
  begin
    case (Sender as TsEdit).Tag of
      100: sEdit3.Clear;
      101: sEdit5.Clear;
      102: sEdit7.Clear;
      103: sEdit9.Clear;
      104: sEdit11.Clear;
      113: sEdit26.Clear;
    end;
  end;
end;

procedure TUI_PAYORD_NEW_frm.edtBF_CODEKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TMP_STR : string;
begin
  inherited;
  IF Length((Sender as TsEdit).Text) in [(Sender as TsEdit).MaxLength-1, (Sender as TsEdit).MaxLength] then
  begin
    Case (Sender as TsEdit).Tag of
      // 거래형태구분
      100 : CDDialog := TCD_PAYORDBFCD_frm.Create(Self);
      // 지급지시서용도
      101 : CDDialog := TCD_4487_frm.Create(Self);
      // 납부수수료유형
      102 : CDDialog := TCD_4383_frm.Create(Self);
      // 송금방법
      103 : CDDialog := TCD_SNDCD_frm.Create(Self);
      // 수수료부담자
      104 : CDDialog := TCD_ChargeTo_frm.Create(Self);
      // 입금관련서류
      113 : CDDialog := TCD_PAYORD1001_frm.Create(Self);
    end;

    try
      TMP_STR := CDDialog.GetCodeText((Sender as TsEdit).Text);
      Case (Sender as TsEdit).Tag of
        100 : sEdit3.Text := TMP_STR;
        101 : sEdit5.Text := TMP_STR;
        102 : sEdit7.Text := TMP_STR;
        103 : sEdit9.Text := TMP_STR;
        104 : sEdit11.Text := TMP_STR;
        113 : sEdit26.Text := TMP_STR;
      end;
    finally
      FreeAndNil( CDDialog );
    end;
  end;
end;

procedure TUI_PAYORD_NEW_frm.edtOD_BANKDblClick(Sender: TObject);
begin
  inherited;
  IF (Sender as TsEdit).ReadOnly Then Exit;

  CDDialog := TCD_BANK_frm.Create(Self);
  try
    if CDDialog.OpenDialog(0, (Sender as TsEdit).Text) then
    begin
      (Sender as TsEdit).Text := CDDialog.Values[0];

      Case (Sender as TsEdit).Tag of
        // 지급의뢰인은행 외화원금계좌
        101:
        begin
          edtOD_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtOD_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
        // 수익자 은행
        102:
        begin
          edtBN_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtBN_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
        // 중간경유 은행
        103:
        begin
          edtIT_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtIT_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
        // 지급의뢰인 원화원금계좌
        104:
        begin
          edtEC_BANK1.Text := CDDialog.FieldValues('BankName1');
          edtEC_NAME1.Text := CDDialog.FieldValues('BankBranch1');
        end;
      end;
    end;
  finally
    FreeAndNil( CDDialog );
  end;
end;

procedure TUI_PAYORD_NEW_frm.btnSaveClick(Sender: TObject);
var
  SC : TSQLCreate;
  DocNo : String;
begin
  inherited;
  //------------------------------------------------------------------------------
  // 관리번호 중복 체크
  //------------------------------------------------------------------------------
  if (Self.Tag = 1) AND (chkDocOver(edtMAINT_NO.Text)) Then
  begin
//    sPageControl1.ActivePageIndex := 1;
    showHintControl(edtMAINT_NO,'<font color=blue><b>관리번호('+edtMAINT_NO.Text+')</b></font>가 이미 존재합니다');
    Exit;
  end;

  //------------------------------------------------------------------------------
  // 저장전에 체크
  //------------------------------------------------------------------------------
  if (Sender as TsButton).Tag = 1 Then
  begin
    if Length(Trim(edtMAINT_NO.Text)) = 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(edtMAINT_NO,'<font color=blue><b>관리번호</b></font>를 입력하세요');
      Exit;
    end;
    if Length(Trim(mskAPP_DATE.Text)) = 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(mskAPP_DATE,'<font color=red><b>신청일자</b></font>를 입력하세요');
      Exit;
    end;
    if Length(Trim(mskEXE_DATE.Text)) = 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(mskEXE_DATE,'<font color=red><b>이체희망일자</b></font>를 입력하세요');
      Exit;
    end;
    if Length(Trim(edtPAY_AMTC.Text)) = 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(edtPAY_AMTC,'<font color=red><b>지급총액 통화단위</b></font>를 입력하세요');
      Exit;
    end;
    if currPAY_AMT.Value = 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(currPAY_AMT,'<font color=red><b>지급총액</b></font>을 입력하세요');
      Exit;
    end;
    if Length(Trim(edtBF_CODE.Text)) = 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(edtBF_CODE,'<font color=red><b>거래형태구분</b></font>을 입력하세요');
      Exit;
    end;
    if Length(Trim(edtBUS_CD.Text)) = 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(edtBUS_CD,'<font color=red><b>지급지시서 용도</b></font>를 입력하세요');
      Exit;
    end;
    if Length(Trim(edtCHARGE_TO.Text)) = 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(edtCHARGE_TO,'<font color=red><b>수수료부담자</b></font>를 입력하세요');
      Exit;
    end;
    if ( Length(Trim(edtPAYDET1.Text))+Length(Trim(edtPAYDET2.Text))+Length(Trim(edtPAYDET3.Text))+Length(Trim(edtPAYDET4.Text)) ) = 0 Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(edtPAYDET1,'<font color=red><b>송금내역</b></font>을 입력하세요');
      Exit;
    end;
    // 지급지시서용도 2AJ시에만 납부수수료 입력가능
    if (edtBUS_CD.Text = '2AJ') and (Trim(edtBUS_CD1.Text) = '') Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(edtBUS_CD1,'지급지시서용도가 [<font color=red><b>2AJ : 외국환수수료납부</b></font>]이면 <font color=red><b>납부수수료유형</b></font>을 입력하셔야합니다');
      Exit;
    end;
    if (edtBUS_CD.Text <> '2AJ') and (Trim(edtBUS_CD1.Text) <> '') Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(edtBUS_CD1,'지급지시서용도가 [<font color=red><b>2AJ : 외국환수수료납부</b></font>]일 경우에만 <font color=red><b>납부수수료유형</b></font>을 입력할 수 있습니다');
      Exit;
    end;
    if AnsiMatchText(RightStr(edtBF_CODE.Text, 2), ['11','12','31','33','35']) AND (Trim(mskIMP_DATE.Text) = '')  Then
    begin
      sPageControl1.ActivePageIndex := 1;
      showHintControl(mskIMP_DATE, '<font color=red><b>수입 예정일</b></font>를 입력하세요<br>※거래형태구분이 "사전송금(11, 12, 31, 33, 35로 끝나는 코드)"일 경우에는 필수입력');
      Exit;
    end;
  end;

//------------------------------------------------------------------------------
// 데이터 처리
//------------------------------------------------------------------------------
  SC := TSQLCreate.Create;
  try
    DocNo := edtMAINT_NO.Text;
    if Self.Tag = 1 Then
    begin
      SC.DMLType := dmlInsert;
      SC.ADDValue('MAINT_NO', DocNo);
    end
    else
    if Self.Tag = 2 Then
    begin
      SC.DMLType := dmlUpdate;
      SC.ADDWhere('MAINT_NO', DocNo);
    end;
    SC.SQLHeader('PAYORD');
    SC.ADDValue('USER_ID', edtUSER_ID.Text);
    SC.ADDValue('DATEE', mskDATEE.Text);
    SC.ADDValue('MESSAGE1', CM_TEXT(com_func,[':']));
    SC.ADDValue('MESSAGE2', CM_TEXT(com_type,[':']));
    SC.ADDValue('APP_DATE', mskAPP_DATE.Text);
    SC.ADDValue('EXE_DATE', mskEXE_DATE.Text);
    SC.ADDValue('IMP_DATE', mskIMP_DATE.Text);
    SC.ADDValue('HS_CODE',  mskHS_CODE.Text);
    SC.ADDValue('BF_CODE',  edtBF_CODE.Text);
    SC.ADDValue('ADREFNO',  edtADREFNO.Text);
    SC.ADDValue('BUS_CD',   edtBUS_CD.Text);
    SC.ADDValue('BUS_CD1',  edtBUS_CD1.Text);
    SC.ADDValue('PAYDET1',  edtPAYDET1.Text);
    SC.ADDValue('PAYDET2',  edtPAYDET2.Text);
    SC.ADDValue('PAYDET3',  edtPAYDET3.Text);
    SC.ADDValue('PAYDET4',  edtPAYDET4.Text);
//    SC.ADDValue('PAYDET5',
    SC.ADDValue('SND_CD',   edtSND_CD.Text);
    SC.ADDValue('CHARGE_TO', edtCHARGE_TO.Text);
    SC.ADDValue('PAY_AMT',  currPAY_AMT.Value);
    SC.ADDValue('PAY_AMTC', edtPAY_AMTC.Text);
    SC.ADDValue('PAY_AMT2', currPAY_AMT2.Value);
    SC.ADDValue('PAY_AMT2C', edtPAY_AMT2C.Text);
    SC.ADDValue('OD_BANK',  edtOD_BANK.Text);
    SC.ADDValue('OD_BANK1', edtOD_BANK1.Text);
    SC.ADDValue('OD_BANK2', edtOD_BANK2.Text);
    SC.ADDValue('OD_NAME1', edtOD_NAME1.Text);
    SC.ADDValue('OD_NAME2', edtOD_NAME2.Text);
    SC.ADDValue('OD_ACCNT1', edtOD_ACCNT1.Text);
    SC.ADDValue('OD_ACCNT2', edtOD_ACCNT2.Text);
    SC.ADDValue('OD_CURR',   edtOD_CURR.Text);
    SC.ADDValue('OD_NATION', edtOD_Nation.Text);
    SC.ADDValue('BN_BANK',   edtBN_BANK.Text);
    SC.ADDValue('BN_BANK1',  edtBN_BANK1.Text);
    SC.ADDValue('BN_BANK2',  edtBN_BANK2.Text);
    SC.ADDValue('BN_NAME1',  edtBN_NAME1.Text);
    SC.ADDValue('BN_NAME2',  edtBN_NAME2.Text);
    SC.ADDValue('BN_ACCNT1', edtBN_ACCNT1.Text);
    SC.ADDValue('BN_ACCNT2', edtBN_ACCNT2.Text);
    SC.ADDValue('BN_CURR',   edtBN_CURR.Text);
    SC.ADDValue('BN_NATION', edtBN_Nation.Text);
    SC.ADDValue('IT_BANK',   edtIT_BANK.Text);
    SC.ADDValue('IT_BANK1',  edtIT_BANK1.Text);
    SC.ADDValue('IT_BANK2',  edtIT_BANK2.Text);
    SC.ADDValue('IT_NAME1',  edtIT_NAME1.Text);
    SC.ADDValue('IT_NAME2',  edtIT_NAME2.Text);
    SC.ADDValue('IT_ACCNT1', edtIT_ACCNT1.Text);
    SC.ADDValue('IT_ACCNT2', edtIT_ACCNT2.Text);
    SC.ADDValue('IT_CURR',   edtIT_CURR.Text);
    SC.ADDValue('IT_NATION', edtIT_Nation.Text);
    SC.ADDValue('EC_BANK',   edtEC_BANK.Text);
    SC.ADDValue('EC_BANK1',  edtEC_BANK1.Text);
    SC.ADDValue('EC_BANK2',  edtEC_BANK2.Text);
    SC.ADDValue('EC_NAME1',  edtEC_NAME1.Text);
    SC.ADDValue('EC_NAME2',  edtEC_NAME2.Text);
    SC.ADDValue('EC_ACCNT1', edtEC_ACCNT1.Text);
    SC.ADDValue('EC_ACCNT2', edtEC_ACCNT2.Text);
    SC.ADDValue('EC_NATION', edtEC_Nation.Text);
    SC.ADDValue('APP_CODE',  edtAPP_CODE.Text);
    SC.ADDValue('APP_NAME1', edtAPP_NAME1.Text);
    SC.ADDValue('APP_NAME2', edtAPP_NAME2.Text);
    SC.ADDValue('APP_NAME3', edtAPP_NAME3.Text);
    SC.ADDValue('APP_TELE',  edtAPP_TELE.Text);
    SC.ADDValue('APP_STR1',  edtAPP_STR1.Text);
    SC.ADDValue('BEN_CODE',  edtBEN_CODE.Text);
    SC.ADDValue('BEN_NAME1', edtBEN_NAME1.Text);
    SC.ADDValue('BEN_NAME2', edtBEN_NAME2.Text);
    SC.ADDValue('BEN_NAME3', edtBEN_NAME3.Text);
    SC.ADDValue('BEN_TELE',  edtBEN_TELE.Text);
    SC.ADDValue('BEN_STR1',  edtBEN_STR1.Text);
    SC.ADDValue('BEN_STR2',  edtBEN_STR2.Text);
    SC.ADDValue('BEN_STR3',  edtBEN_STR3.Text);
    SC.ADDValue('BEN_NATION', edtBEN_NATION.Text);
    SC.ADDValue('SND_CODE',   edtSND_CODE.Text);
    SC.ADDValue('SND_NAME1',  edtSND_NAME1.Text);
    SC.ADDValue('SND_NAME2',  edtSND_NAME2.Text);
    SC.ADDValue('SND_NAME3',  edtSND_NAME3.Text);
//    SC.ADDValue('REMARK',
    SC.ADDValue('REMARK1', edtREMARK1.Text);
    SC.ADDValue('REMARK2', edtREMARK2.Text);
    SC.ADDValue('REMARK3', edtREMARK3.Text);
    SC.ADDValue('REMARK4', edtREMARK4.Text);
    SC.ADDValue('REMARK5', edtREMARK5.Text);
    SC.ADDValue('DOC_GU',  edtDOC_GU.Text);
    SC.ADDValue('DOC_NO1', edtDOC_NO1.Text);
    SC.ADDValue('DOC_DTE1', mskDOC_DTE1.Text);
//    SC.ADDValue('CHK1',
    //문서저장구분(0:임시, 1:저장)
    SC.ADDValue('CHK2', (Sender as TsButton).Tag);
//    SC.ADDValue('CHK3',

    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SC.CreateSQL;
        if sCheckBox1.Checked then
          Clipboard.AsText := SQL.Text;
        ExecSQL;
      finally
        Close;
        Free;
      end;
    end;

  finally
    SC.Free;
  end;

//------------------------------------------------------------------------------
// 컨트롤 제어
//------------------------------------------------------------------------------
  if Self.Tag = 2 Then
  begin
    sDBGrid1.Enabled := True;
    sDBGrid2.Enabled := True;
    sDBGrid3.Enabled := True;
  end;

  Self.Tag := 0;
  BtnControl;
  PanelControl;
  ReadOnlyControl(sPanel7, True);
  ReadOnlyControl(sPanel6, True);
  ReadOnlyControl(sPanel12, True);

  mskDate1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  mskDate2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  qryList.Close;
  qryList.Open;
  qryList.Locate('MAINT_NO', DocNo, []);

end;

procedure TUI_PAYORD_NEW_frm.showHintControl(Control: TControl; msg: String;
  showDelay: integer);
var
  R : TRect;
  P : TPoint;
begin
  IF msg = '' Then Exit;

  R := Control.BoundsRect;
  R.TopLeft := Control.Parent.ClientToScreen(R.TopLeft);
  P.X := R.Left-7;
  P.Y := R.Top+18;
  sAlphaHints1.HTMLMode := PosMulti(['<b>','<i>','<u>','</br>','<br>','<font'], LowerCase(msg));
  sAlphaHints1.ShowHint(P, msg ,showDelay);
end;

function TUI_PAYORD_NEW_frm.chkDocOver(DocNo: String): Boolean;
begin
  Result := False;
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.Text := 'SELECT MAINT_NO, DATEE FROM PAYORD WHERE MAINT_NO = '+QuotedStr(DocNo);
      Open;
      Result := RecordCount > 0;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_PAYORD_NEW_frm.btnCopyClick(Sender: TObject);
begin
  inherited;
  if MessageBox(Self.Handle, MSG_COPY_QUESTION, '문서 복사확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  Self.Tag := 1;
  BtnControl;
  PanelControl;

  //컨트롤 입력가능
  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel6, False);
  ReadOnlyControl(sPanel12, False);

  //공통 데이터 초기화
  edtMAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('PAYORD');
  mskDATEE.Text := FormatDateTime('YYYYMMDD', Now);
  mskAPP_DATE.Text := FormatDateTime('YYYYMMDD', Now);  
end;

procedure TUI_PAYORD_NEW_frm.btnDelClick(Sender: TObject);
var
  CurrIdx : Integer;
begin
  inherited;
  if MessageBox(Self.Handle, MSG_SYSTEM_DEL_CONFIRM, '삭제확인', MB_OKCANCEL+MB_ICONQUESTION) = ID_CANCEL Then Exit;

  with TADOQuery.Create(nil) do
  begin
    try
      CurrIdx := qryList.RecNo;

      Connection := DMMssql.KISConnect;
      SQL.Text := 'DELETE FROM PAYORD WHERE MAINT_NO = '+QuotedStr(qryListMAINT_NO.AsString);
      ExecSQL;

      qryList.Close;
      qryList.Open;
      if CurrIdx > 1 Then
        qryList.MoveBy(CurrIdx-1);

    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_PAYORD_NEW_frm.EditDocument;
begin
  Self.Tag := 2;
  BtnControl;
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
  sDBGrid3.Enabled := False;

  //컨트롤 입력가능
//  ReadOnlyControl(sPanel7, False);
  ReadOnlyControl(sPanel6, False);
  ReadOnlyControl(sPanel12, False);

  sPageControl1.ActivePageIndex := 1;  
end;

procedure TUI_PAYORD_NEW_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  EditDocument;
end;

procedure TUI_PAYORD_NEW_frm.sDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
    if (qryList.RecordCount > 0) and (Column.FieldName = 'CHK2') then
    begin
      if qryListCHK2.AsInteger = 1 Then
        Canvas.Font.Style := [fsBold]
      else
        Canvas.Font.Style := [fsItalic]
    end;

    if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
    begin
      Canvas.Brush.Color := $0054CEFC;
      Canvas.Font.Color := clBlack;
    end;
    DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TUI_PAYORD_NEW_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
    ReadyDocument(qryListMAINT_NO.AsString)
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
end;

procedure TUI_PAYORD_NEW_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := PAYORD(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'PAYORD';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'PAYORD';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        qryList.Close;
        qrylist.Open;

        qrylist.Locate('MAINT_NO',RecvDoc,[]);

//        Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(ReadyDateTime)),
//                 FormatDateTime('YYYYMMDD',EndOfTheYear(ReadyDateTime)),
//                 RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;


procedure TUI_PAYORD_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  QR_PAYORD_PRN_New := TQR_PAYORD_PRN_New.Create(nil);
  Preview_frm := TPreview_frm.Create(Self);
  try
    QR_PAYORD_PRN_New.MAINT_NO := qryListMAINT_NO.AsString;
    Preview_frm.Report := QR_PAYORD_PRN_New;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(QR_PAYORD_PRN_New);
  end;

end;

procedure TUI_PAYORD_NEW_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then
    DocumentSend_frm := TDocumentSend_frm.Create(Application);
end;

procedure TUI_PAYORD_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then
  begin
    qryListAfterScroll(DataSet);
  end;
end;

procedure TUI_PAYORD_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_PAYORD_NEW_frm.RunSQL(sSQL: TStream);
begin
  with TADOQuery.Create(nil) do
  begin
    try
      Connection := DMMssql.KISConnect;
      SQL.LoadFromStream(sSQL);
      ExecSQL;
    finally
      Close;
      Free;
    end;
  end;
end;

procedure TUI_PAYORD_NEW_frm.FormCreate(Sender: TObject);
var
  DLL_HAEDER : THandle;
  ResStream : TResourceStream;
begin
  inherited;
  FSQL := qryList.SQL.Text;
  com_type.Perform(CB_SETDROPPEDWIDTH, 300, 0);

  inherited;
  //테이블생성 FROM DLL
  DllPatchExecute('CODE_PAYORD');
  DllPatchExecute('TABLE_PAYORD');
end;

procedure TUI_PAYORD_NEW_frm.FormActivate(Sender: TObject);
var
  BMK : Pointer;
begin
  inherited;
  BMK := qryList.GetBookmark;
  ReadList;
  try
  IF qryList.BookmarkValid(BMK) Then
    qryList.GotoBookmark(BMK);
  finally
    qryList.FreeBookmark(BMK);
  end;
end;

end.
