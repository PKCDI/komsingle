inherited UI_RUNSQL_frm: TUI_RUNSQL_frm
  Left = 498
  Top = 80
  BorderIcons = [biSystemMenu]
  BorderWidth = 4
  Caption = #46356#48260#44613
  ClientHeight = 673
  ClientWidth = 1114
  Font.Name = #47569#51008' '#44256#46357
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 15
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 1114
    Height = 673
    SkinData.SkinSection = 'TRANSPARENT'
    Align = alClient
    
    TabOrder = 0
    object sPanel2: TsPanel
      Left = 1
      Top = 1
      Width = 1112
      Height = 48
      Align = alTop
      
      TabOrder = 0
      OnDblClick = sPanel2DblClick
      object sSpeedButton1: TsSpeedButton
        Left = 208
        Top = 8
        Width = 23
        Height = 31
        ButtonStyle = tbsDivider
      end
      object sButton1: TsButton
        Left = 6
        Top = 5
        Width = 109
        Height = 37
        Caption = #49892#54665'(Command)'
        Enabled = False
        TabOrder = 0
        OnClick = sButton1Click
      end
      object sBitBtn1: TsBitBtn
        Left = 117
        Top = 5
        Width = 90
        Height = 37
        Caption = #49892#54665'(ExecSql)'
        Enabled = False
        TabOrder = 1
        OnClick = sBitBtn1Click
      end
      object sBitBtn2: TsBitBtn
        Left = 229
        Top = 5
        Width = 90
        Height = 37
        Caption = #45936#51060#53552#54869#51064
        Enabled = False
        TabOrder = 2
        OnClick = sBitBtn2Click
      end
      object sBitBtn3: TsBitBtn
        Tag = 1
        Left = 325
        Top = 5
        Width = 124
        Height = 37
        Caption = #45936#51060#53552#54869#51064'(Oracle)'
        TabOrder = 3
        OnClick = sBitBtn2Click
      end
    end
    object sMemo1: TsMemo
      Left = 1
      Top = 49
      Width = 1112
      Height = 534
      Align = alClient
      TabOrder = 1
    end
    object memo2: TsMemo
      Left = 1
      Top = 583
      Width = 1112
      Height = 89
      Align = alBottom
      Color = clBtnFace
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 2
      SkinData.CustomColor = True
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Top = 168
  end
  object ADOCommand1: TADOCommand
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 8
    Top = 96
  end
  object ADOQuery1: TADOQuery
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 8
    Top = 64
  end
  object ADOQuery2: TADOQuery
    Parameters = <>
    Left = 40
    Top = 64
  end
end
