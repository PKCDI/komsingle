unit UI_DOMOFB_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, DB, ADODB, StdCtrls, sComboBox, DBCtrls, sDBMemo,
  sDBEdit, sMemo, sCustomComboEdit, sCurrEdit, sCurrencyEdit, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sButton, sSpeedButton, sLabel, ExtCtrls, sPanel, sSkinProvider, StrUtils, Clipbrd, DateUtils,
  sRadioButton;

type
  TUI_DOMOFB_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sSpeedButton10: TsSpeedButton;
    msk_pubdt: TsMaskEdit;
    edt_pubno: TsEdit;
    edt_pubcd: TsEdit;
    edt_tradeno: TsEdit;
    edt_pubnm: TsEdit;
    edt_pubceo: TsEdit;
    edt_pubsign: TsEdit;
    edt_pubaddr1: TsEdit;
    edt_pubaddr2: TsEdit;
    edt_pubaddr3: TsEdit;
    edt_demCd: TsEdit;
    edt_demSaupno: TsEdit;
    edt_demNm: TsEdit;
    edt_demCeo: TsEdit;
    edt_demSign: TsEdit;
    edt_demAddr1: TsEdit;
    edt_demAddr2: TsEdit;
    edt_demAddr3: TsEdit;
    edt_demRef: TsEdit;
    edt_HSCD: TsEdit;
    sPanel8: TsPanel;
    edt_eff1: TsEdit;
    edt_eff2: TsEdit;
    edt_eff3: TsEdit;
    edt_eff4: TsEdit;
    edt_eff5: TsEdit;
    edt_chk1: TsEdit;
    edt_chk2: TsEdit;
    edt_chk3: TsEdit;
    edt_chk4: TsEdit;
    edt_chk5: TsEdit;
    sPanel9: TsPanel;
    edt_pkg1: TsEdit;
    edt_pkg2: TsEdit;
    edt_pkg3: TsEdit;
    edt_pkg4: TsEdit;
    edt_pkg5: TsEdit;
    sPanel10: TsPanel;
    sPanel11: TsPanel;
    edt_pay1: TsEdit;
    edt_pay2: TsEdit;
    edt_pay3: TsEdit;
    edt_pay4: TsEdit;
    edt_pay5: TsEdit;
    edt_TQTYCUR: TsEdit;
    sBitBtn24: TsBitBtn;
    edt_TQTY: TsCurrencyEdit;
    edt_TAMT: TsCurrencyEdit;
    edt_TAMTCUR: TsEdit;
    sBitBtn25: TsBitBtn;
    sButton1: TsButton;
    sTabSheet3: TsTabSheet;
    sPanel27: TsPanel;
    edt_ori1: TsEdit;
    edt_ori1nm: TsEdit;
    sPanel12: TsPanel;
    btnOri1: TsBitBtn;
    edt_ori2: TsEdit;
    edt_ori2nm: TsEdit;
    btnOri2: TsBitBtn;
    edt_ori3: TsEdit;
    edt_ori3nm: TsEdit;
    btnOri3: TsBitBtn;
    edt_ori4: TsEdit;
    edt_ori4nm: TsEdit;
    btnOri4: TsBitBtn;
    edt_ori5: TsEdit;
    edt_ori5nm: TsEdit;
    btnOri5: TsBitBtn;
    sPanel13: TsPanel;
    mm_remark1: TsMemo;
    edt_Trans: TsEdit;
    sBitBtn14: TsBitBtn;
    edt_TransNm: TsEdit;
    edt_shipNat: TsEdit;
    sBitBtn13: TsBitBtn;
    edt_shipPort: TsEdit;
    edt_LOADTXT1: TsEdit;
    edt_LOADTXT2: TsEdit;
    edt_LOADTXT3: TsEdit;
    edt_LOADTXT4: TsEdit;
    edt_LOADTXT5: TsEdit;
    edt_destNat: TsEdit;
    sBitBtn16: TsBitBtn;
    edt_destPort: TsEdit;
    edt_DESTTXT1: TsEdit;
    edt_DESTTXT2: TsEdit;
    edt_DESTTXT3: TsEdit;
    edt_DESTTXT4: TsEdit;
    edt_DESTTXT5: TsEdit;
    sPanel14: TsPanel;
    sPanel15: TsPanel;
    sPanel18: TsPanel;
    sPanel16: TsPanel;
    sPanel19: TsPanel;
    sTabSheet2: TsTabSheet;
    sPanel17: TsPanel;
    sSpeedButton11: TsSpeedButton;
    sPanel22: TsPanel;
    sPanel23: TsPanel;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    sDBMemo1: TsDBMemo;
    sDBMemo2: TsDBMemo;
    sDBEdit3: TsDBEdit;
    sBitBtn17: TsBitBtn;
    sDBEdit4: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sBitBtn18: TsBitBtn;
    sDBEdit5: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sBitBtn19: TsBitBtn;
    sDBEdit8: TsDBEdit;
    sDBEdit10: TsDBEdit;
    sBitBtn20: TsBitBtn;
    sDBEdit9: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sBitBtn21: TsBitBtn;
    sDBEdit12: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sBitBtn23: TsBitBtn;
    sDBEdit13: TsDBEdit;
    sBitBtn26: TsButton;
    sPanel21: TsPanel;
    sPanel28: TsPanel;
    sDBEdit15: TsDBEdit;
    sPanel25: TsPanel;
    sDBGrid2: TsDBGrid;
    sPanel26: TsPanel;
    sSpeedButton13: TsSpeedButton;
    btnDnew: TsBitBtn;
    btnDEdit: TsBitBtn;
    btnDDel: TsBitBtn;
    btnDok: TsBitBtn;
    btnDcancel: TsBitBtn;
    btnImport: TsBitBtn;
    btnSample: TsBitBtn;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    sPanel30: TsPanel;
    sLabel3: TsLabel;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListMaint_Rff: TStringField;
    qryListChk1: TStringField;
    qryListChk2: TStringField;
    qryListChk3: TStringField;
    qryListUSER_ID: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListDATEE: TStringField;
    qryListSR_CODE: TStringField;
    qryListSR_NO: TStringField;
    qryListSR_NAME1: TStringField;
    qryListSR_NAME2: TStringField;
    qryListSR_NAME3: TStringField;
    qryListSR_ADDR1: TStringField;
    qryListSR_ADDR2: TStringField;
    qryListSR_ADDR3: TStringField;
    qryListUD_CODE: TStringField;
    qryListUD_RENO: TStringField;
    qryListUD_NO: TStringField;
    qryListUD_NAME1: TStringField;
    qryListUD_NAME2: TStringField;
    qryListUD_NAME3: TStringField;
    qryListUD_ADDR1: TStringField;
    qryListUD_ADDR2: TStringField;
    qryListUD_ADDR3: TStringField;
    qryListOFR_NO: TStringField;
    qryListOFR_DATE: TStringField;
    qryListOFR_SQ: TStringField;
    qryListOFR_SQ1: TStringField;
    qryListOFR_SQ2: TStringField;
    qryListOFR_SQ3: TStringField;
    qryListOFR_SQ4: TStringField;
    qryListOFR_SQ5: TStringField;
    qryListREMARK: TStringField;
    qryListREMARK_1: TMemoField;
    qryListPRNO: TIntegerField;
    qryListMAINT_NO_1: TStringField;
    qryListTSTINST: TStringField;
    qryListTSTINST1: TStringField;
    qryListTSTINST2: TStringField;
    qryListTSTINST3: TStringField;
    qryListTSTINST4: TStringField;
    qryListTSTINST5: TStringField;
    qryListPCKINST: TStringField;
    qryListPCKINST1: TStringField;
    qryListPCKINST2: TStringField;
    qryListPCKINST3: TStringField;
    qryListPCKINST4: TStringField;
    qryListPCKINST5: TStringField;
    qryListPAY: TStringField;
    qryListPAY_ETC: TStringField;
    qryListPAY_ETC1: TStringField;
    qryListPAY_ETC2: TStringField;
    qryListPAY_ETC3: TStringField;
    qryListPAY_ETC4: TStringField;
    qryListPAY_ETC5: TStringField;
    qryListMAINT_NO_2: TStringField;
    qryListTERM_DEL: TStringField;
    qryListTERM_NAT: TStringField;
    qryListTERM_LOC: TStringField;
    qryListORGN1N: TStringField;
    qryListORGN1LOC: TStringField;
    qryListORGN2N: TStringField;
    qryListORGN2LOC: TStringField;
    qryListORGN3N: TStringField;
    qryListORGN3LOC: TStringField;
    qryListORGN4N: TStringField;
    qryListORGN4LOC: TStringField;
    qryListORGN5N: TStringField;
    qryListORGN5LOC: TStringField;
    qryListTRNS_ID: TStringField;
    qryListLOADD: TStringField;
    qryListLOADLOC: TStringField;
    qryListLOADTXT: TStringField;
    qryListLOADTXT1: TStringField;
    qryListLOADTXT2: TStringField;
    qryListLOADTXT3: TStringField;
    qryListLOADTXT4: TStringField;
    qryListLOADTXT5: TStringField;
    qryListDEST: TStringField;
    qryListDESTLOC: TStringField;
    qryListDESTTXT: TStringField;
    qryListDESTTXT1: TStringField;
    qryListDESTTXT2: TStringField;
    qryListDESTTXT3: TStringField;
    qryListDESTTXT4: TStringField;
    qryListDESTTXT5: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYCUR: TStringField;
    qryListTAMT: TBCDField;
    qryListTAMTCUR: TStringField;
    qryListCODE: TStringField;
    qryListDOC_NAME: TStringField;
    qryListCODE_1: TStringField;
    qryListDOC_NAME_1: TStringField;
    qryListCODE_2: TStringField;
    qryListDOC_NAME_2: TStringField;
    qryListTRANSNME: TStringField;
    qryListLOADDNAME: TStringField;
    qryListDESTNAME: TStringField;
    qryListHS_CODE: TStringField;
    qryDetail: TADOQuery;
    qryDetailKEYY: TStringField;
    qryDetailSEQ: TIntegerField;
    qryDetailHS_CHK: TStringField;
    qryDetailHS_NO: TStringField;
    qryDetailNAME_CHK: TStringField;
    qryDetailNAME_COD: TStringField;
    qryDetailNAME: TStringField;
    qryDetailNAME1: TMemoField;
    qryDetailSIZE: TStringField;
    qryDetailSIZE1: TMemoField;
    qryDetailQTY: TBCDField;
    qryDetailQTY_G: TStringField;
    qryDetailQTYG: TBCDField;
    qryDetailQTYG_G: TStringField;
    qryDetailPRICE: TBCDField;
    qryDetailPRICE_G: TStringField;
    qryDetailAMT: TBCDField;
    qryDetailAMT_G: TStringField;
    qryDetailSTQTY: TBCDField;
    qryDetailSTQTY_G: TStringField;
    qryDetailSTAMT: TBCDField;
    qryDetailSTAMT_G: TStringField;
    dsDetail: TDataSource;
    dsList: TDataSource;
    qryListDOC_TYPE: TStringField;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sPanel6: TsPanel;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    procedure FormShow(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sPageControl1Change(Sender: TObject);
  private
    procedure ReadData;
    procedure ReadList(SortField: TColumn=nil);
    procedure ReadDetail;
    procedure DeleteDocument;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UI_DOMOFB_NEW_frm: TUI_DOMOFB_NEW_frm;

implementation

uses
  MSSQL, ICON, MessageDefine, DOMOFB_PRINT, Preview;

{$R *.dfm}

procedure TUI_DOMOFB_NEW_frm.ReadData;
begin
  with qryList do
  begin
    ClearControl(sPanel6);
    ClearControl(sPanel7);
    ClearControl(sPanel27);
    if RecordCount > 0 then
    begin

      //------------------------------------------------------------------------------
      // 데이터 가져오기
      //------------------------------------------------------------------------------
      edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
      msk_Datee.Text := qryListDATEE.AsString;
      edt_userno.Text := qryListUSER_ID.AsString;

      CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 3);
      CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString);

      //발행일자
      msk_pubdt.Text := qryListOFR_DATE.AsString;
      //발행번호
      edt_pubno.Text := qryListOFR_NO.AsString;
      //문서코드
      Case AnsiIndexText(qryListMESSAGE1.AsString, ['1BL', '1BK']) of
        0: sRadioButton2.Checked := True;
        1: sRadioButton1.Checked := True;
      else
        IF AnsiMatchText(Trim(qryListMESSAGE1.AsString), ['', '6']) Then
          sRadioButton2.Checked := True
        else
        IF AnsiMatchText(Trim(qryListMESSAGE1.AsString), ['9', '4', '7']) Then
          sRadioButton1.Checked := True;
      end;

      //발행자코드
      edt_pubcd.Text := qryListSR_CODE.AsString;
      edt_tradeno.Text := qryListSR_NO.AsString;
      edt_pubnm.Text := qryListSR_NAME1.AsString;
      edt_pubceo.Text := qryListSR_NAME2.AsString;
      edt_pubsign.Text := qryListSR_NAME3.AsString;
      edt_pubaddr1.Text := qryListSR_ADDR1.AsString;
      edt_pubaddr2.Text := qryListSR_ADDR2.AsString;
      edt_pubaddr3.Text := qryListSR_ADDR3.AsString;

      //수요자
      edt_demCd.Text := qryListUD_CODE.AsString;
      edt_demSaupno.Text := qryListUD_NO.AsString;
      edt_demNm.Text := qryListUD_NAME1.AsString;
      edt_demCeo.Text := qryListUD_NAME2.AsString;
      edt_demSign.Text := qryListUD_NAME3.AsString;
      edt_demAddr1.Text := qryListUD_ADDR1.AsString;
      edt_demAddr2.Text := qryListUD_ADDR2.AsString;
      edt_demAddr3.Text := qryListUD_ADDR3.AsString;
      edt_demRef.Text := qryListUD_RENO.AsString;
      edt_HSCD.Text := qryListHS_CODE.AsString;

      //유효기간
      edt_eff1.Text := qryListOFR_SQ1.AsString;
      edt_eff2.Text := qryListOFR_SQ2.AsString;
      edt_eff3.Text := qryListOFR_SQ3.AsString;
      edt_eff4.Text := qryListOFR_SQ4.AsString;
      edt_eff5.Text := qryListOFR_SQ5.AsString;

      //검사방법
      edt_chk1.Text := qryListTSTINST1.AsString;
      edt_chk2.Text := qryListTSTINST2.AsString;
      edt_chk3.Text := qryListTSTINST3.AsString;
      edt_chk4.Text := qryListTSTINST4.AsString;
      edt_chk5.Text := qryListTSTINST5.AsString;

      //포장방법
      edt_pkg1.Text := qryListPCKINST1.AsString;
      edt_pkg2.Text := qryListPCKINST2.AsString;
      edt_pkg3.Text := qryListPCKINST3.AsString;
      edt_pkg4.Text := qryListPCKINST4.AsString;
      edt_pkg5.Text := qryListPCKINST5.AsString;

      //결제방법
      edt_pay1.Text := qryListPAY_ETC1.AsString;
      edt_pay2.Text := qryListPAY_ETC2.AsString;
      edt_pay3.Text := qryListPAY_ETC3.AsString;
      edt_pay4.Text := qryListPAY_ETC4.AsString;
      edt_pay5.Text := qryListPAY_ETC5.AsString;

      //원산지국가/지방명
      edt_ori1.Text := qryListORGN1N.AsString;
      edt_ori2.Text := qryListORGN2N.AsString;
      edt_ori3.Text := qryListORGN3N.AsString;
      edt_ori4.Text := qryListORGN4N.AsString;
      edt_ori5.Text := qryListORGN5N.AsString;

      edt_ori1nm.Text := qryListORGN1LOC.AsString;
      edt_ori2nm.Text := qryListORGN2LOC.AsString;
      edt_ori3nm.Text := qryListORGN3LOC.AsString;
      edt_ori4nm.Text := qryListORGN4LOC.AsString;
      edt_ori5nm.Text := qryListORGN5LOC.AsString;

      //기타참조사항
      mm_remark1.Lines.Text := qryListREMARK_1.AsString;

      //운송방법
      edt_Trans.Text := qryListTRNS_ID.AsString;
      edt_TransNm.Text := qryListTRANSNME.AsString;

      //선적국가
      edt_shipNat.Text := qryListLOADD.AsString;
      edt_shipPort.Text := qryListLOADLOC.AsString;
      edt_LOADTXT1.Text := qryListLOADTXT1.AsString;
      edt_LOADTXT2.Text := qryListLOADTXT2.AsString;
      edt_LOADTXT3.Text := qryListLOADTXT3.AsString;
      edt_LOADTXT4.Text := qryListLOADTXT4.AsString;
      edt_LOADTXT5.Text := qryListLOADTXT5.AsString;

      //도착국가
      edt_destNat.Text := qryListDEST.AsString;
      edt_destPort.Text := qryListDESTLOC.AsString;
      edt_DESTTXT1.Text := qryListDESTTXT1.AsString;
      edt_DESTTXT2.Text := qryListDESTTXT2.AsString;
      edt_DESTTXT3.Text := qryListDESTTXT3.AsString;
      edt_DESTTXT4.Text := qryListDESTTXT4.AsString;
      edt_DESTTXT5.Text := qryListDESTTXT5.AsString;

      //총합계
      edt_TQTYCUR.Text := qryListTQTYCUR.AsString;
      edt_TQTY.Value := qryListTQTY.AsVariant;
      edt_TAMTCUR.Text := qryListTAMTCUR.AsString;
      edt_TAMT.Value := qryListTAMT.AsVariant;
    end;
  end;

end;

procedure TUI_DOMOFB_NEW_frm.ReadList(SortField : TColumn);
var
  tmpFieldNm : String;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    IF SortField <> nil Then
    begin
      tmpFieldNm := SortField.FieldName;
      IF SortField.FieldName = 'MAINT_NO' THEN
        tmpFieldNm := 'H1.MAINT_NO';

      IF LeftStr(qryList.SQL.Strings[qryList.SQL.Count-1],Length('ORDER BY')) = 'ORDER BY' then
      begin
        IF Trim(RightStr(qryList.SQL.Strings[qryList.SQL.Count-1],4)) = 'ASC' Then
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' DESC'
        else
          qryList.SQL.Strings[qryList.SQL.Count-1] := 'ORDER BY '+tmpFieldNm+' ASC';
      end
      else
      begin
        qryList.SQL.Add('ORDER BY '+tmpFieldNm+' ASC');
      end;
    end;

    Open;
  end;
end;

procedure TUI_DOMOFB_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  {$IFDEF DEBUG}
  sMaskEdit1.Text := '20100101';
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  {$ELSE}
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));
  {$ENDIF}
  sBitBtn1Click(nil);

//  EnableControl(sPanel6, false);
//  EnableControl(sPanel7, False);
//  EnableControl(sPanel27, False);
//  EnableControl(sPanel17, False);

  sPageControl1.ActivePageIndex := 0;
end;

procedure TUI_DOMOFB_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  try
    ReadList;
      Clipboard.AsText := qryList.SQL.Text;    
  except
    on E:Exception do
    begin
      Clipboard.AsText := qryList.SQL.Text;
      ShowMessage(E.Message);
    end;
  end;
end;

procedure TUI_DOMOFB_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if DataSet.RecordCount = 0 then
    qryListAfterScroll(DataSet);
end;

procedure TUI_DOMOFB_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
  ReadDetail;
end;

procedure TUI_DOMOFB_NEW_frm.ReadDetail;
begin
  with qryDetail do
  begin
    Close;
    Parameters[0].value := qryListMAINT_NO.AsString;
    Open;
  end;
end;

procedure TUI_DOMOFB_NEW_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  if not qrylist.Active then Exit;

  DeleteDocument;
end;

procedure TUI_DOMOFB_NEW_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := qryListMAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;

        SQL.Text :=  'DELETE FROM DOMOFB_D WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFB_H1 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFB_H2 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM DOMOFB_H3 WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else if qryList.RecordCount = 0 then
        begin
          ClearControl(sPanel6);
          ClearControl(sPanel7);
          ClearControl(sPanel27);
//          ClearControl(sPanel17);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
    Close;
    Free;
   end;
  end;
end;

procedure TUI_DOMOFB_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DOMOFB_PRINT_frm := TDOMOFB_PRINT_frm.Create(Self);
  Preview_frm := TPreview_frm.Create(Self);
  try
    DOMOFB_PRINT_frm.FKEY := qryListMAINT_NO.AsString;
    DOMOFB_PRINT_frm.PrintDocument(qryList.Fields,qryDetail.Fields,False);
    Preview_frm.Report := DOMOFB_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(DOMOFB_PRINT_frm);
  end;
end;

procedure TUI_DOMOFB_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_DOMOFB_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_DOMOFB_NEW_frm := nil;
end;

procedure TUI_DOMOFB_NEW_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  sPanel4.Visible := sPageControl1.ActivePageIndex <> 3;
end;

end.
