unit UI_LDANTC_NEW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, sMemo, ExtCtrls, ComCtrls,
  sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit, Grids, DBGrids,
  acDBGrid, sButton, sSpeedButton, sLabel, sPanel, sSkinProvider, DBCtrls,
  sDBEdit, DB, ADODB, sDBMemo, sListBox, DateUtils, StrUtils;

type
  TUI_LDANTC_NEW_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sLabel7: TsLabel;
    sLabel6: TsLabel;
    sSpeedButton8: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    sPanel4: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel3: TsPanel;
    sPanel20: TsPanel;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sPanel7: TsPanel;
    sTabSheet4: TsTabSheet;
    sDBGrid3: TsDBGrid;
    sPanel24: TsPanel;
    sSpeedButton12: TsSpeedButton;
    sMaskEdit3: TsMaskEdit;
    sMaskEdit4: TsMaskEdit;
    sBitBtn5: TsBitBtn;
    sEdit1: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListAPP_CODE: TStringField;
    qryListAPP_NAME1: TStringField;
    qryListAPP_NAME2: TStringField;
    qryListAPP_NAME3: TStringField;
    qryListBK_CODE: TStringField;
    qryListBK_NAME1: TStringField;
    qryListBK_NAME2: TStringField;
    qryListBK_NAME3: TStringField;
    qryListLC_NO: TStringField;
    qryListRC_NO: TStringField;
    qryListAMT1: TBCDField;
    qryListAMT1C: TStringField;
    qryListAMT2: TBCDField;
    qryListAMT2C: TStringField;
    qryListRATE: TBCDField;
    qryListRES_DATE: TStringField;
    qryListSET_DATE: TStringField;
    qryListNG_DATE: TStringField;
    qryListREMAKR1: TMemoField;
    qryListBANK1: TStringField;
    qryListBANK2: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListBGM_GUBUN: TStringField;
    qryListBGM_NM: TStringField;
    qryListFIN_NO: TStringField;
    qryListFIN_NO2: TStringField;
    qryListFIN_NO3: TStringField;
    qryListFIN_NO4: TStringField;
    qryListFIN_NO5: TStringField;
    qryListFIN_NO6: TStringField;
    qryListFIN_NO7: TStringField;
    qryListRC_NO2: TStringField;
    qryListRC_NO3: TStringField;
    qryListRC_NO4: TStringField;
    qryListRC_NO5: TStringField;
    qryListRC_NO6: TStringField;
    qryListRC_NO7: TStringField;
    qryListRC_NO8: TStringField;
    sDBEdit1: TsDBEdit;
    sDBEdit2: TsDBEdit;
    Shape1: TShape;
    sDBEdit3: TsDBEdit;
    sDBEdit4: TsDBEdit;
    sDBEdit5: TsDBEdit;
    sDBEdit6: TsDBEdit;
    sDBEdit7: TsDBEdit;
    sDBEdit8: TsDBEdit;
    sDBEdit9: TsDBEdit;
    Shape2: TShape;
    sDBEdit10: TsDBEdit;
    sDBEdit11: TsDBEdit;
    sDBEdit12: TsDBEdit;
    sDBEdit13: TsDBEdit;
    sDBEdit14: TsDBEdit;
    sDBEdit15: TsDBEdit;
    sDBEdit16: TsDBEdit;
    sDBEdit17: TsDBEdit;
    sPageControl2: TsPageControl;
    sTabSheet2: TsTabSheet;
    sTabSheet3: TsTabSheet;
    lst_RCNO: TsListBox;
    lst_FINNO: TsListBox;
    sTabSheet5: TsTabSheet;
    sDBMemo1: TsDBMemo;
    Shape3: TShape;
    sDBEdit18: TsDBEdit;
    sDBEdit19: TsDBEdit;
    sPanel5: TsPanel;
    sSpeedButton9: TsSpeedButton;
    edt_SearchNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    sMaskEdit2: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    btnPrint: TsButton;
    sPanel6: TsPanel;
    edt_MAINT_NO: TsEdit;
    msk_Datee: TsMaskEdit;
    com_func: TsComboBox;
    com_type: TsComboBox;
    edt_userno: TsEdit;
    procedure btnExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sBitBtn1Click(Sender: TObject);
    procedure edt_SearchNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReadList;
  protected
    procedure ReadData; override;
  public
    { Public declarations }
  end;

var
  UI_LDANTC_NEW_frm: TUI_LDANTC_NEW_frm;

implementation

uses
  MSSQL, ICON, LDANTC_PRINT, Preview;

{$R *.dfm}

{ TUI_LDANTC_NEW_frm }

procedure TUI_LDANTC_NEW_frm.ReadData;
var
  i : integer;
begin
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  msk_Datee.Text := qryListDATEE.AsString;
  edt_userno.Text := qryListUSER_ID.AsString;

  CM_INDEX(com_func, [':'], 0, qryListMESSAGE1.AsString, 5);
  CM_INDEX(com_type, [':'], 0, qryListMESSAGE2.AsString, 2);

  //물품수령증명서
  lst_RCNO.Clear;
  for i := 1 to 8 do
  begin
    IF (i = 1) AND (Trim(qryListRC_NO.AsString) <> '') Then
      lst_RCNO.Items.Add(qryListRC_NO.AsString)
    else
    if i > 1 Then
    begin
      IF Trim(qryList.FieldByName('RC_NO'+IntToStr(i)).AsString) <> '' Then
        lst_RCNO.Items.Add(qryList.FieldByName('RC_NO'+IntToStr(i)).AsString);
    end;
  end;
  IF lst_RCNO.Items.Count > 0 Then
    sTabSheet2.Caption := '물품수령증명서(인수증)번호 ('+IntToStr(lst_RCNO.Items.Count)+')'
  else
    sTabSheet2.Caption := '물품수령증명서(인수증)번호';

  //세금계산서
  lst_FINNO.Clear;
  for i := 1 to 7 do
  begin
    IF (i = 1) AND (Trim(qryListFIN_NO.AsString) <> '') Then
      lst_FINNO.Items.Add(qryListFIN_NO.AsString)
    else
    if i > 1 Then
    begin
      IF Trim(qryList.FieldByName('FIN_NO'+IntToStr(i)).AsString) <> '' Then
        lst_FINNO.Items.Add(qryList.FieldByName('FIN_NO'+IntToStr(i)).AsString);
    end;
  end;
  IF lst_FINNO.Items.Count > 0 Then
    sTabSheet4.Caption := '세금계산서번호 ('+IntToStr(lst_FINNO.Items.Count)+')'
  else
    sTabSheet4.Caption := '세금계산서번호';  
end;

procedure TUI_LDANTC_NEW_frm.ReadList;
begin
  with qryList do
  begin
    Close;
    //FDATE
    Parameters[0].Value := sMaskEdit1.Text;
    //TDATE
    Parameters[1].Value := sMaskEdit2.Text;
    //MAINT_NO
    Parameters[2].Value := '%' + edt_SearchNo.Text + '%';
    //ALLDATA
    if Trim(edt_SearchNo.Text) <> '' then
      Parameters[3].Value := 0
    else
      Parameters[3].Value := 1;

    Open;
  end;
end;

procedure TUI_LDANTC_NEW_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LDANTC_NEW_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LDANTC_NEW_frm := nil;
end;

procedure TUI_LDANTC_NEW_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  ReadList;
end;

procedure TUI_LDANTC_NEW_frm.edt_SearchNoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  IF Key = VK_RETURN Then ReadList;
end;

procedure TUI_LDANTC_NEW_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadData;
end;

procedure TUI_LDANTC_NEW_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then qryListAfterScroll(DataSet);
  sTabSheet5.ImageIndex := -1;
  IF Trim(qryListREMAKR1.AsString) <> '' Then sTabSheet5.ImageIndex := 15;
end;

procedure TUI_LDANTC_NEW_frm.FormShow(Sender: TObject);
begin
  inherited;
  sMaskEdit1.Text := FormatDateTime('YYYYMMDD', StartOfTheMonth(Now));
  sMaskEdit2.Text := FormatDateTime('YYYYMMDD', EndOfTheMonth(Now));

  sBitBtn1Click(nil);


  sPageControl2.ActivePageIndex := 0;
  ReadOnlyControl(spanel7,false);
end;

procedure TUI_LDANTC_NEW_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  LDANTC_PRINT_frm := TLDANTC_PRINT_frm.Create(Self);
  LDANTC_PRINT_frm.MaintNo := qryListMAINT_NO.AsString;
  Preview_frm := TPreview_frm.Create(Self);
  try
    Preview_frm.Report := LDANTC_PRINT_frm;
    Preview_frm.Preview;
  finally
    FreeAndNil(Preview_frm);
    FreeAndNil(LDANTC_PRINT_frm);
  end;
end;

end.
