unit UI_APP707_BP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, Grids, DBGrids, acDBGrid, StdCtrls,
  sComboBox, ExtCtrls, sSplitter, sMemo, sLabel, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, ComCtrls, sPageControl, sCheckBox, Buttons,
  sBitBtn, Mask, sMaskEdit, sEdit, sButton, sSpeedButton, sPanel,MSSQL, DB,
  ADODB, TypeDefine, sBevel;

type
  TUI_APP707_BP_frm = class(TChildForm_frm)
    btn_Panel: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sSpeedButton5: TsSpeedButton;
    btnExit: TsButton;
    btnNew: TsButton;
    btnEdit: TsButton;
    btnDel: TsButton;
    btnCopy: TsButton;
    btnPrint: TsButton;
    btnTemp: TsButton;
    btnSave: TsButton;
    btnCancel: TsButton;
    sButton1: TsButton;
    sButton3: TsButton;
    maintno_Panel: TsPanel;
    edt_MaintNo: TsEdit;
    sMaskEdit1: TsMaskEdit;
    edt_UserNo: TsEdit;
    btn_Cal: TsBitBtn;
    sCheckBox1: TsCheckBox;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    page1_LeftPanel: TsPanel;
    page1_RightPanel: TsPanel;
    sPanel5: TsPanel;
    edt_APPDATE: TsMaskEdit;
    edt_In_Mathod: TsEdit;
    edt_ApBank: TsEdit;
    edt_In_Mathod1: TsEdit;
    edt_ApBank1: TsEdit;
    edt_ApBank2: TsEdit;
    edt_ApBank3: TsEdit;
    edt_ApBank4: TsEdit;
    edt_ApBank5: TsEdit;
    sPanel2: TsPanel;
    edt_ImpCd1_1: TsEdit;
    edt_ImpCd1: TsEdit;
    edt_ImpCd2: TsEdit;
    btn_ImpCd1: TsBitBtn;
    btn_ImpCd2: TsBitBtn;
    edt_ImpCd2_1: TsEdit;
    edt_ImpCd3_1: TsEdit;
    btn_ImpCd3: TsBitBtn;
    edt_ImpCd3: TsEdit;
    edt_ImpCd4: TsEdit;
    btn_ImpCd4: TsBitBtn;
    edt_ImpCd4_1: TsEdit;
    edt_ImpCd5_1: TsEdit;
    btn_ImpCd5: TsBitBtn;
    edt_ImpCd5: TsEdit;
    sPanel8: TsPanel;
    edt_AdInfo1: TsEdit;
    edt_AdInfo2: TsEdit;
    edt_AdInfo3: TsEdit;
    edt_AdInfo4: TsEdit;
    edt_AdInfo5: TsEdit;
    edt_EXAddr2: TsEdit;
    edt_EXAddr1: TsEdit;
    edt_EXName3: TsEdit;
    edt_EXName2: TsEdit;
    edt_EXName1: TsEdit;
    sPanel9: TsPanel;
    edt_ILno5: TsEdit;
    btn_LCNumber5: TsBitBtn;
    edt_ILAMT5: TsCurrencyEdit;
    edt_ILAMT4: TsCurrencyEdit;
    btn_LCNumber4: TsBitBtn;
    edt_ILno4: TsEdit;
    edt_ILno3: TsEdit;
    btn_LCNumber3: TsBitBtn;
    edt_ILAMT3: TsCurrencyEdit;
    edt_ILAMT2: TsCurrencyEdit;
    btn_LCNumber2: TsBitBtn;
    edt_ILno2: TsEdit;
    edt_ILno1: TsEdit;
    btn_LCNumber1: TsBitBtn;
    edt_ILAMT1: TsCurrencyEdit;
    sPanel7: TsPanel;
    edt_AdPay: TsEdit;
    btn_GrantingCredit: TsBitBtn;
    edt_AdPay1: TsEdit;
    edt_AdBank4: TsEdit;
    edt_AdBank3: TsEdit;
    edt_AdBank2: TsEdit;
    edt_AdBank1: TsEdit;
    edt_AdBank: TsEdit;
    sPanel6: TsPanel;
    btn_OpenMethod: TsBitBtn;
    btn_ApBank: TsBitBtn;
    btn_NoticeBank: TsBitBtn;
    edt_ILCur1: TsEdit;
    edt_ILCur2: TsEdit;
    edt_ILCur3: TsEdit;
    edt_ILCur4: TsEdit;
    edt_ILCur5: TsEdit;
    sTabSheet2: TsTabSheet;
    page2_LeftPanel: TsPanel;
    page2_RightPanel: TsPanel;
    btn_Beneficiary: TsBitBtn;
    edt_Applic5: TsEdit;
    edt_Applic4: TsEdit;
    edt_Applic3: TsEdit;
    edt_Applic2: TsEdit;
    edt_Applic1: TsEdit;
    sPanel17: TsPanel;
    sPanel19: TsPanel;
    edt_Benefc: TsEdit;
    edt_Benefc2: TsEdit;
    edt_Benefc3: TsEdit;
    edt_Benefc4: TsEdit;
    edt_Benefc5: TsEdit;
    sTabSheet3: TsTabSheet;
    page3_LeftPanel: TsPanel;
    sTabSheet7: TsTabSheet;
    sSplitter2: TsSplitter;
    dataSearch_Panel: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sDBGrid1: TsDBGrid;
    sPanel52: TsPanel;
    sPanel53: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    sDBGrid2: TsDBGrid;
    sPanel1: TsPanel;
    edt_cdNo: TsEdit;
    sPanel3: TsPanel;
    edt_IssDate: TsMaskEdit;
    sPanel4: TsPanel;
    edt_AmdNo: TsCurrencyEdit;
    sPanel11: TsPanel;
    edt_exDate: TsMaskEdit;
    sPanel12: TsPanel;
    edt_Benefc1: TsEdit;
    edt_IncdCur: TsEdit;
    sPanel13: TsPanel;
    edt_DecdCur: TsEdit;
    sBitBtn2: TsBitBtn;
    sPanel14: TsPanel;
    sPanel15: TsPanel;
    sPanel16: TsPanel;
    sPanel18: TsPanel;
    edt_NwcdCur: TsEdit;
    sBitBtn3: TsBitBtn;
    edt_BfcdCur: TsEdit;
    sBitBtn4: TsBitBtn;
    edt_CdMax: TsEdit;
    sBitBtn5: TsBitBtn;
    edt_IncdAmt: TsCurrencyEdit;
    edt_DecdAmt: TsCurrencyEdit;
    edt_NwcdAmt: TsCurrencyEdit;
    edt_BfcdAmt: TsCurrencyEdit;
    edt_CdMax1: TsEdit;
    sLabel1: TsLabel;
    edt_CdPerp: TsCurrencyEdit;
    edt_CdPerm: TsCurrencyEdit;
    page3_RightPanel: TsPanel;
    dsList: TDataSource;
    qryList: TADOQuery;
    sBitBtn6: TsBitBtn;
    edt_chasu: TsEdit;
    edt_msg1: TsEdit;
    sBitBtn7: TsBitBtn;
    edt_msg2: TsEdit;
    sBitBtn8: TsBitBtn;
    sSpeedButton6: TsSpeedButton;
    sSplitter1: TsSplitter;
    sSplitter3: TsSplitter;
    sSplitter4: TsSplitter;
    sSplitter5: TsSplitter;
    sSplitter6: TsSplitter;
    procedure edt_CodeTypeDblClick(Sender: TObject);
    procedure btn_CodeTypeClick(Sender: TObject);
    procedure edt_ApBankDblClick(Sender: TObject);
    procedure btn_ApBankClick(Sender: TObject);
    procedure edt_UserCodeTypeClick(Sender: TObject);
    procedure btn_UserCodeTypeClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { protected declaration }
    ProgramControlType : TProgramControlType;

    procedure ReadDocument; virtual; abstract;
    procedure NewDocument; virtual; abstract;
    procedure CancelDocument; virtual; abstract;
    procedure SaveDocument(Sender : TObject); virtual; abstract;
    procedure DeleteDocument; virtual; abstract;
    procedure EditDocument; virtual; abstract;

    procedure ButtonEnable(Val : Boolean=true);

    function ValidData : Boolean; virtual; abstract;
    function ReadListBetween(fromDate: String; toDate: String; KeyValue : string;KeyCount : integer=0):Boolean; virtual; abstract;

  end;

var
  UI_APP707_BP_frm: TUI_APP707_BP_frm;

implementation

uses Dialog_CodeList, Dialog_BANK, Dialog_SearchCustom;

{$R *.dfm}

{ TUI_APP707_BP_frm }

procedure TUI_APP707_BP_frm.ButtonEnable(Val: Boolean);
begin

  btnNew.Enabled := Val;
  btnEdit.Enabled := Val;
  btnDel.Enabled := Val;

  btnTemp.Enabled := not Val;
  btnSave.Enabled := not Val;
  btnCancel.Enabled := not Val;

  btnCopy.Enabled := Val;
  btnPrint.Enabled := Val;

end;

procedure TUI_APP707_BP_frm.edt_CodeTypeDblClick(Sender: TObject);
begin
  inherited;
  inherited;
  Dialog_CodeList_frm := TDialog_CodeList_frm.Create(Self);
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      if Dialog_CodeList_frm.openDialog((Sender as TsEdit).Hint) = mrOK then
      begin
          //선택된 코드 출력
          (Sender as TsEdit).Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;

          //선택된 코드의 값까지 출력
          case (Sender as TsEdit).Tag of

              //개설방법
              101 : edt_In_Mathod1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //신용공여
              104 : edt_AdPay1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //수입용도
              105 : edt_ImpCd1_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              106 : edt_ImpCd2_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              107 : edt_ImpCd3_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              108 : edt_ImpCd4_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
              109 : edt_ImpCd5_1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;

              //Maximum Credit Amount
              120 : edt_CdMax1.Text := Dialog_CodeList_frm.sDBGrid1.DataSource.DataSet.FieldByName('NAME').AsString;
      
          end;
      end;
    end;
  finally
    FreeAndNil(Dialog_CodeList_frm);
  end;
end;

procedure TUI_APP707_BP_frm.btn_CodeTypeClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    //문서기능
    1 : edt_CodeTypeDblClick(edt_msg1);

    //유형
    2 : edt_CodeTypeDblClick(edt_msg2);

     //개설방법
    101 : edt_CodeTypeDblClick(edt_In_Mathod);

    //통지은행
    103 : edt_CodeTypeDblClick(edt_AdBank);

    //신용공여
    104 : edt_CodeTypeDblClick(edt_AdPay);

    //수입용도
    105 : edt_CodeTypeDblClick(edt_ImpCd1);
    106 : edt_CodeTypeDblClick(edt_ImpCd2);
    107 : edt_CodeTypeDblClick(edt_ImpCd3);
    108 : edt_CodeTypeDblClick(edt_ImpCd4);
    109 : edt_CodeTypeDblClick(edt_ImpCd5);

    //I/L 번호
    110 : edt_CodeTypeDblClick(edt_ILCur1);
    111 : edt_CodeTypeDblClick(edt_ILCur2);
    112 : edt_CodeTypeDblClick(edt_ILCur3);
    113 : edt_CodeTypeDblClick(edt_ILCur4);
    114 : edt_CodeTypeDblClick(edt_ILCur5);

    116 : edt_CodeTypeDblClick(edt_IncdCur);
    117 : edt_CodeTypeDblClick(edt_DecdCur);
    118 : edt_CodeTypeDblClick(edt_NwcdCur);
    119 : edt_CodeTypeDblClick(edt_BfcdCur);

    120 : edt_CodeTypeDblClick(edt_CdMax);

  end;
end;

procedure TUI_APP707_BP_frm.edt_ApBankDblClick(Sender: TObject);
begin
  inherited;
   Dialog_BANK_frm := TDialog_BANK_frm.Create(Self);
  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      IF Dialog_BANK_frm.openDialog((Sender as TsEdit).Text) = mrOK Then
      begin
        (Sender as TsEdit).Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        case (Sender as TsEdit).tag of
          //개설의뢰은행
          102:
            begin
              edt_ApBank1.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKNAME1').AsString;
              edt_ApBank2.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BankName2').AsString;
              edt_ApBank3.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKBRANCH1').AsString;
              edt_ApBank4.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BankBranch2').AsString;
              edt_ApBank5.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BankAccount').AsString;
            end;
          //통지은행
          103:
            begin
              edt_AdBank1.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKNAME1').AsString;
              edt_AdBank2.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BankName2').AsString;
              edt_AdBank3.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BANKBRANCH1').AsString;
              edt_AdBank4.Text := Dialog_BANK_frm.sDBGrid1.DataSource.DataSet.FieldByName('BankBranch2').AsString;
            end;
        end;
      End;
    end;
  finally
     FreeAndNil( Dialog_BANK_frm );
  end;
end;

procedure TUI_APP707_BP_frm.btn_ApBankClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of

    102 : edt_ApBankDblClick(edt_ApBank);

    103 : edt_ApBankDblClick(edt_AdBank);

  end;
end;

procedure TUI_APP707_BP_frm.edt_UserCodeTypeClick(Sender: TObject);
begin
  inherited;
  inherited;
  Dialog_SearchCustom_frm := TDialog_SearchCustom_frm.Create(Self);

  try
    if (Sender as TsEdit).ReadOnly = False then
    begin
      IF Dialog_SearchCustom_frm.openDialog((Sender as TsEdit).Text) = mrOK then
      begin
        (Sender as TsEdit).Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('CODE').AsString;
        case (Sender as TsEdit).Tag of
          //Beneficiary(수혜자)
          115 :
          begin
              edt_Benefc1.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ENAME').AsString;
              edt_Benefc2.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('REP_Name').AsString;
              edt_Benefc3.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR1').AsString;
              edt_Benefc4.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('ADDR2').AsString;
              //계좌번호 필드명 확인해야함
              edt_Benefc5.Text := Dialog_SearchCustom_frm.sDBGrid1.DataSource.DataSet.FieldByName('TRAD_NO').AsString;
          end;
        end;
      end;
    end;
  finally
    FreeAndNil(Dialog_SearchCustom_frm);
  end;
end;

procedure TUI_APP707_BP_frm.btn_UserCodeTypeClick(Sender: TObject);
begin
  inherited;
    case (Sender as TsBitBtn).Tag of

      //Beneficiary(수혜자)
      115 : edt_UserCodeTypeClick(edt_Benefc);

    end;

end;

procedure TUI_APP707_BP_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  //ChildForm에 OnKeyPress이벤트
end;

end.
