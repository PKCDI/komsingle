unit UI_LOCRC1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, sLabel, sMemo, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, Grids, DBGrids, acDBGrid, ComCtrls,
  sPageControl, ExtCtrls, sSplitter, Buttons, sBitBtn, Mask, sMaskEdit,
  sEdit, sButton, sSpeedButton, sPanel, sSkinProvider, DB, ADODB, QuickRpt;

type
  TUI_LOCRC1_frm = class(TChildForm_frm)
    sPanel4: TsPanel;
    edt_MAINT_NO: TsEdit;
    mask_DATEE: TsMaskEdit;
    edt_USER_ID: TsEdit;
    edt_msg1: TsEdit;
    edt_msg2: TsEdit;
    sSplitter1: TsSplitter;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    sSplitter3: TsSplitter;
    sPanel2: TsPanel;
    sSplitter4: TsSplitter;
    sPanel5: TsPanel;
    sPanel6: TsPanel;
    sPanel9: TsPanel;
    sSpeedButton1: TsSpeedButton;
    edt_AP_BANK1: TsEdit;
    edt_RFF_NO: TsEdit;
    mask_EXP_DAT: TsMaskEdit;
    sPanel10: TsPanel;
    edt_BENEFC: TsEdit;
    sBitBtn5: TsBitBtn;
    mask_ISS_DAT: TsMaskEdit;
    edt_BSN_HSCODE: TsEdit;
    mask_GET_DAT: TsMaskEdit;
    edt_AP_BANK: TsEdit;
    sPanel8: TsPanel;
    sPanel12: TsPanel;
    edt_AP_BANK3: TsEdit;
    edt_AP_NAME: TsEdit;
    edt_AP_BANK4: TsEdit;
    edt_AP_BANK2: TsEdit;
    sPanel11: TsPanel;
    sBitBtn3: TsBitBtn;
    mask_EXPDATE: TsMaskEdit;
    mask_LOADDATE: TsMaskEdit;
    curr_LOC2AMT: TsCurrencyEdit;
    edt_LOC2AMTC: TsEdit;
    edt_EX_RATE: TsEdit;
    curr_LOC1AMT: TsCurrencyEdit;
    sBitBtn8: TsBitBtn;
    edt_APPLIC4: TsEdit;
    edt_APPLIC3: TsEdit;
    edt_APPLIC2: TsEdit;
    edt_APPLIC1: TsEdit;
    sBitBtn2: TsBitBtn;
    edt_APPLIC: TsEdit;
    edt_BENEFC1: TsEdit;
    edt_LOC1AMTC: TsEdit;
    sPanel13: TsPanel;
    edt_RCT_AMT1C: TsEdit;
    sBitBtn4: TsBitBtn;
    curr_RCT_AMT1: TsCurrencyEdit;
    edt_RATE: TsEdit;
    edt_RCT_AMT2C: TsEdit;
    curr_RCT_AMT2: TsCurrencyEdit;
    edt_APPLIC5: TsEdit;
    edt_APPLIC6: TsEdit;
    edt_BENEFC2: TsMaskEdit;
    edt_APPLIC7: TsMaskEdit;
    sBitBtn16: TsBitBtn;
    sBitBtn17: TsBitBtn;
    sBitBtn18: TsBitBtn;
    sPanel15: TsPanel;
    memo_LOC_REM1: TsMemo;
    memo_REMARK1: TsMemo;
    sPanel16: TsPanel;
    sTabSheet2: TsTabSheet;
    sSplitter5: TsSplitter;
    sPanel20: TsPanel;
    sSplitter6: TsSplitter;
    sSplitter7: TsSplitter;
    sDBGrid3: TsDBGrid;
    sPanel21: TsPanel;
    sSpeedButton6: TsSpeedButton;
    edt_NAME_COD: TsEdit;
    edt_HS_NO: TsMaskEdit;
    memo_NAME1: TsMemo;
    memo_SIZE1: TsMemo;
    sPanel22: TsPanel;
    sPanel23: TsPanel;
    edt_QTY_G: TsEdit;
    sBitBtn9: TsBitBtn;
    curr_QTY: TsCurrencyEdit;
    edt_PRICE_G: TsEdit;
    sBitBtn10: TsBitBtn;
    curr_PRICE: TsCurrencyEdit;
    edt_QTYG_G: TsEdit;
    sBitBtn11: TsBitBtn;
    curr_QTYG: TsCurrencyEdit;
    edt_AMT_G: TsEdit;
    sBitBtn12: TsBitBtn;
    curr_AMT: TsCurrencyEdit;
    edt_STQTY_G: TsEdit;
    sBitBtn13: TsBitBtn;
    curr_STQTY: TsCurrencyEdit;
    edt_STAMT_G: TsEdit;
    sBitBtn14: TsBitBtn;
    curr_STAMT: TsCurrencyEdit;
    btn_GoodsCalc: TsButton;
    sPanel24: TsPanel;
    sBitBtn15: TsBitBtn;
    sPanel30: TsPanel;
    sPanel31: TsPanel;
    sTabSheet4: TsTabSheet;
    sSplitter9: TsSplitter;
    panel1: TsPanel;
    sSplitter8: TsSplitter;
    sDBGrid4: TsDBGrid;
    sPanel28: TsPanel;
    edt_BILL_NO: TsEdit;
    mask_BILL_DATE: TsMaskEdit;
    edt_BILL_AMOUNT_UNIT: TsEdit;
    curr_BILL_AMOUNT: TsCurrencyEdit;
    edt_TAX_AMOUNT_UNIT: TsEdit;
    curr_TAX_AMOUNT: TsCurrencyEdit;
    sPanel18: TsPanel;
    sTabSheet3: TsTabSheet;
    sSplitter2: TsSplitter;
    sSplitter11: TsSplitter;
    sDBGrid1: TsDBGrid;
    sPanel33: TsPanel;
    sDBGrid5: TsDBGrid;
    sPanel3: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn21: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn23: TsBitBtn;
    sPanel25: TsPanel;
    sPanel1: TsPanel;
    sSpeedButton4: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    btnPrint: TsButton;
    sButton1: TsButton;
    qrylist: TADOQuery;
    qryGoods: TADOQuery;
    dslist: TDataSource;
    dsGoods: TDataSource;
    dsTax: TDataSource;
    qryTax: TADOQuery;
    qrylistMAINT_NO: TStringField;
    qrylistUSER_ID: TStringField;
    qrylistDATEE: TStringField;
    qrylistMESSAGE1: TStringField;
    qrylistMESSAGE2: TStringField;
    qrylistRFF_NO: TStringField;
    qrylistGET_DAT: TStringField;
    qrylistISS_DAT: TStringField;
    qrylistEXP_DAT: TStringField;
    qrylistBENEFC: TStringField;
    qrylistBENEFC1: TStringField;
    qrylistAPPLIC: TStringField;
    qrylistAPPLIC1: TStringField;
    qrylistAPPLIC2: TStringField;
    qrylistAPPLIC3: TStringField;
    qrylistAPPLIC4: TStringField;
    qrylistAPPLIC5: TStringField;
    qrylistAPPLIC6: TStringField;
    qrylistRCT_AMT1: TBCDField;
    qrylistRCT_AMT1C: TStringField;
    qrylistRCT_AMT2: TBCDField;
    qrylistRCT_AMT2C: TStringField;
    qrylistRATE: TBCDField;
    qrylistREMARK: TStringField;
    qrylistREMARK1: TMemoField;
    qrylistTQTY: TBCDField;
    qrylistTQTY_G: TStringField;
    qrylistTAMT: TBCDField;
    qrylistTAMT_G: TStringField;
    qrylistLOC_NO: TStringField;
    qrylistAP_BANK: TStringField;
    qrylistAP_BANK1: TStringField;
    qrylistAP_BANK2: TStringField;
    qrylistAP_BANK3: TStringField;
    qrylistAP_BANK4: TStringField;
    qrylistAP_NAME: TStringField;
    qrylistLOC1AMT: TBCDField;
    qrylistLOC1AMTC: TStringField;
    qrylistLOC2AMT: TBCDField;
    qrylistLOC2AMTC: TStringField;
    qrylistEX_RATE: TBCDField;
    qrylistLOADDATE: TStringField;
    qrylistEXPDATE: TStringField;
    qrylistLOC_REM: TStringField;
    qrylistLOC_REM1: TMemoField;
    qrylistCHK1: TBooleanField;
    qrylistCHK2: TStringField;
    qrylistCHK3: TStringField;
    qrylistNEGODT: TStringField;
    qrylistNEGOAMT: TBCDField;
    qrylistBSN_HSCODE: TStringField;
    qrylistPRNO: TIntegerField;
    qrylistAPPLIC7: TStringField;
    qrylistBENEFC2: TStringField;
    qrylistCK_S: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TBCDField;
    qryGoodsHS_CHK: TStringField;
    qryGoodsHS_NO: TStringField;
    qryGoodsNAME_CHK: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE: TStringField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsAMT: TBCDField;
    qryGoodsAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsSTAMT: TBCDField;
    qryGoodsSTAMT_G: TStringField;
    qryTaxKEYY: TStringField;
    qryTaxSEQ: TBCDField;
    qryTaxBILL_NO: TStringField;
    qryTaxBILL_DATE: TStringField;
    qryTaxBILL_AMOUNT: TBCDField;
    qryTaxBILL_AMOUNT_UNIT: TStringField;
    qryTaxTAX_AMOUNT: TBCDField;
    qryTaxTAX_AMOUNT_UNIT: TStringField;
    edt_LOC_NO: TsEdit;
    QRCompositeReport1: TQRCompositeReport;
    sSpeedButton2: TsSpeedButton;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSplitter10: TsSplitter;
    sPanel14: TsPanel;
    btn_TOTAL_CALC: TsButton;
    edt_TQTY_G: TsEdit;
    sBitBtn6: TsBitBtn;
    curr_TQTY: TsCurrencyEdit;
    edt_TAMT_G: TsEdit;
    sBitBtn7: TsBitBtn;
    curr_TAMT: TsCurrencyEdit;
    sPanel32: TsPanel;
    sPanel7: TsPanel;
    sPanel53: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn22: TsBitBtn;
    Mask_toDate: TsMaskEdit;
    sDBGrid2: TsDBGrid;
    sSplitter12: TsSplitter;
    sPanel19: TsPanel;
    sSplitter13: TsSplitter;
    sPanel26: TsPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qrylistAfterScroll(DataSet: TDataSet);
    procedure qrylistAfterOpen(DataSet: TDataSet);
    procedure qryGoodsAfterScroll(DataSet: TDataSet);
    procedure qryTaxAfterScroll(DataSet: TDataSet);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn21Click(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure sDBGrid5DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure sPageControl1Change(Sender: TObject);
  private
    { Private declarations }
    LOCRC1_SQL : String;

    procedure ReadDocument;
    procedure GoodsReadDocument;
    procedure TaxReadDocument;
    procedure DeleteDocument;

    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;
  public
    { Public declarations }
  end;

var
  UI_LOCRC1_frm: TUI_LOCRC1_frm;

implementation
uses MSSQL, Commonlib, KISCalendar,  MessageDefine, DateUtils, StrUtils, LOCRC1_PRINT, LOCRC1_PRINT2;
{$R *.dfm}

procedure TUI_LOCRC1_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LOCRC1_frm := nil;
end;

procedure TUI_LOCRC1_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOCRC1_frm.FormCreate(Sender: TObject);
begin
  inherited;
  LOCRC1_SQL := qrylist.SQL.Text;
end;

procedure TUI_LOCRC1_frm.FormShow(Sender: TObject);
begin
  inherited;
  EnabledControlValue(sPanel9);
  EnabledControlValue(sPanel14);
  EnabledControlValue(sPanel15);
  EnabledControlValue(sPanel21);
  EnabledControlValue(sPanel28); 
  EnabledControlValue(sPanel4);

  mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  sPageControl1.ActivePageIndex := 0;
  com_SearchKeyword.ItemIndex := 0;
  ReadList(Mask_SearchDate1.Text, Mask_SearchDate2.Text,'');

end;

function TUI_LOCRC1_frm.ReadList(FromDate, ToDate,
  KeyValue: String): Boolean;
begin

  Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := LOCRC1_SQL;
    SQL.Add('WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;

end;

procedure TUI_LOCRC1_frm.ReadDocument;
begin
  IF not qrylist.Active Then Exit;
  IF qrylist.RecordCount = 0 Then Exit;

  edt_MAINT_NO.Text := qrylistMAINT_NO.AsString;
  mask_DATEE.Text := qrylistDATEE.AsString;
  edt_USER_ID.Text := qrylistUSER_ID.AsString;
  edt_msg1.Text := qrylistMESSAGE1.AsString;
  edt_msg2.Text := qrylistMESSAGE2.AsString;
//------------------------------------------------------------------------------
// 기본입력사항
//------------------------------------------------------------------------------
  //인수일자
  mask_GET_DAT.Text := qrylistGET_DAT.AsString;
  //발급일자
  mask_ISS_DAT.Text := qrylistISS_DAT.AsString;
  //문서유효일자
  mask_EXP_DAT.Text := qrylistEXP_DAT.AsString;
  //발급번호
  edt_RFF_NO.Text := qrylistRFF_NO.AsString;
  //대표공급물품 HS부호
  edt_BSN_HSCODE.Text := qrylistBSN_HSCODE.AsString;
//------------------------------------------------------------------------------
// 수혜업체/개설업체
//------------------------------------------------------------------------------
  //수혜자
  edt_BENEFC.Text := qrylistBENEFC.AsString;
  edt_BENEFC1.Text := qrylistBENEFC1.AsString;
  edt_BENEFC2.Text := qrylistBENEFC2.AsString;
  //개설업체
  //코드
  edt_APPLIC.Text  := qrylistAPPLIC.AsString;
  //상호
  edt_APPLIC1.Text := qrylistAPPLIC1.AsString;
  //명의인
  edt_APPLIC2.Text := qrylistAPPLIC2.AsString;
  //전자서명
  edt_APPLIC3.Text := qrylistAPPLIC3.AsString;
  //주소1
  edt_APPLIC4.Text := qrylistAPPLIC4.AsString;
  //주소2
  edt_APPLIC5.Text := qrylistAPPLIC5.AsString;
  //주소3
  edt_APPLIC6.Text := qrylistAPPLIC6.AsString;
  //사업자등록번호
  edt_APPLIC7.Text := qrylistAPPLIC7.AsString;
//------------------------------------------------------------------------------
// 개설은행
//------------------------------------------------------------------------------
  //개설은행
  edt_AP_BANK.Text := qrylistAP_BANK.AsString;
  edt_AP_BANK1.Text := qrylistAP_BANK1.AsString;
  edt_AP_BANK2.Text := qrylistAP_BANK2.AsString;
  //지점명
  edt_AP_BANK3.Text := qrylistAP_BANK3.AsString;
  edt_AP_BANK4.Text := qrylistAP_BANK4.AsString;
  //전자서명
  edt_AP_NAME.Text := qrylistAP_NAME.AsString;
//------------------------------------------------------------------------------
// 내국신용장
//------------------------------------------------------------------------------
  //내국신용장 번호
  edt_LOC_NO.Text := qrylistLOC_NO.AsString;
  //개설금액(외화)
  edt_LOC1AMTC.Text := qrylistLOC1AMTC.AsString;
  curr_LOC1AMT.Value := qrylistLOC1AMT.AsCurrency;
  //매매기준율
  edt_EX_RATE.Text := qrylistEX_RATE.AsString;
  //개설금액(원화)
  curr_LOC2AMT.Value := qrylistLOC2AMT.AsCurrency;
  //물품인도기일
  mask_LOADDATE.Text := qrylistLOADDATE.AsString;
  //유효일자
  mask_EXPDATE.Text := qrylistEXPDATE.AsString;

//------------------------------------------------------------------------------
// 인수금액
//------------------------------------------------------------------------------
  //인수금액(외화)
  edt_RCT_AMT1C.Text := qrylistRCT_AMT1C.AsString;
  curr_RCT_AMT1.Value := qrylistRCT_AMT1.AsCurrency;
  //환율
  edt_RATE.Text := qrylistRATE.AsString;
  //인수금액(원화)
  edt_RCT_AMT2C.Text := qrylistRCT_AMT2C.AsString;
  curr_RCT_AMT2.Value := qrylistRCT_AMT2.AsCurrency;
//------------------------------------------------------------------------------
// 총합계
//------------------------------------------------------------------------------
  //총합계수량
  edt_TQTY_G.Text := qrylistTQTY_G.AsString;
  curr_TQTY.Value := qrylistTQTY.AsCurrency;
  //총합계금액
  edt_TAMT_G.Text := qrylistTAMT_G.AsString;
  curr_TAMT.Value := qrylistTAMT.AsCurrency;
//------------------------------------------------------------------------------
// 기타조건/참고사항
//------------------------------------------------------------------------------
  //기타조건
  memo_REMARK1.Lines.Text := qrylistREMARK1.AsString;
  //참고사항
  memo_LOC_REM1.Lines.Text := qrylistLOC_REM1.AsString;
end;

procedure TUI_LOCRC1_frm.GoodsReadDocument;
begin
//------------------------------------------------------------------------------
// 상품
//------------------------------------------------------------------------------
  edt_NAME_COD.Text := qryGoodsNAME_COD.AsString;
  edt_HS_NO.Text := qryGoodsHS_NO.AsString;
  memo_NAME1.Text := qryGoodsNAME1.AsString;
  memo_SIZE1.Text := qryGoodsSIZE1.AsString;
//------------------------------------------------------------------------------
// 수량/단가/금액
//------------------------------------------------------------------------------
  edt_QTY_G.Text := qryGoodsQTY_G.AsString;
  edt_QTYG_G.Text := qryGoodsQTYG_G.AsString;
  edt_PRICE_G.Text := qryGoodsPRICE_G.AsString;
  edt_AMT_G.Text := qryGoodsAMT_G.AsString;
  edt_STQTY_G.Text := qryGoodsSTQTY_G.AsString;
  edt_STAMT_G.Text := qryGoodsSTAMT_G.AsString;

  curr_QTY.Value := qryGoodsQTY.AsCurrency;
  curr_QTYG.Value := qryGoodsQTYG.AsCurrency;
  curr_PRICE.Value :=  qryGoodsPRICE.AsCurrency;
  curr_AMT.Value := qryGoodsAMT.AsCurrency;
  curr_STQTY.Value := qryGoodsSTQTY.AsCurrency;
  curr_STAMT.Value := qryGoodsSTAMT.AsCurrency;
end;

procedure TUI_LOCRC1_frm.TaxReadDocument;
begin
  //세금계산서번호
  edt_BILL_NO.Text := qryTaxBILL_NO.AsString;
  //작성일자
  mask_BILL_DATE.Text := qryTaxBILL_DATE.AsString;
  //공급가액
  edt_BILL_AMOUNT_UNIT.Text := qryTaxBILL_AMOUNT_UNIT.AsString;
  curr_BILL_AMOUNT.Value := qryTaxBILL_AMOUNT.AsCurrency;
  //세액
  edt_TAX_AMOUNT_UNIT.Text := qryTaxTAX_AMOUNT_UNIT.AsString;
  curr_TAX_AMOUNT.Value := qryTaxTAX_AMOUNT.AsCurrency;
end;

procedure TUI_LOCRC1_frm.qrylistAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadDocument;

//------------------------------------------------------------------------------
// 상품명세내역 OPEN
//------------------------------------------------------------------------------
  with qryGoods do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := qrylistMAINT_NO.AsString;
    Open;
  end;

  GoodsReadDocument;

//------------------------------------------------------------------------------
// 세금계산서 내역 OPEN
//------------------------------------------------------------------------------
  with qryTax do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := qrylistMAINT_NO.AsString;
    Open;
  end;

  TaxReadDocument;

end;

procedure TUI_LOCRC1_frm.qrylistAfterOpen(DataSet: TDataSet);
begin
  inherited;
  IF DataSet.RecordCount = 0 Then
  begin
    qrylistAfterScroll(qrylist);
  end;
end;

procedure TUI_LOCRC1_frm.qryGoodsAfterScroll(DataSet: TDataSet);
begin
  inherited;
  GoodsReadDocument;
end;

procedure TUI_LOCRC1_frm.qryTaxAfterScroll(DataSet: TDataSet);
begin
  inherited;
  TaxReadDocument;
end;

procedure TUI_LOCRC1_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_LOCRC1_frm.sBitBtn21Click(Sender: TObject);
begin
  inherited;
  Case (Sender as TsBitBtn).Tag of
    900: Mask_SearchDate1DblClick(Mask_SearchDate1);
    901: Mask_SearchDate1DblClick(Mask_SearchDate2);
  end;
end;

procedure TUI_LOCRC1_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible  := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel25.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn21.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_LOCRC1_frm.Readlist(OrderSyntax: string);
begin
 if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 3 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := LOCRC1_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 :
      begin
        SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :
      begin
        SQL.Add(' WHERE BENEFC1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end;
    Open;
  end;
end;

procedure TUI_LOCRC1_frm.sBitBtn1Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_LOCRC1_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;


        SQL.Text :=  'DELETE FROM LOCRC1_H  WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM LOCRC1_D WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM LOCRC1_TAX WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;

        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally

    if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;

    Close;
    Free;
   end;
  end;
end;

procedure TUI_LOCRC1_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;
end;

procedure TUI_LOCRC1_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text]) Then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text);
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := Mask_toDate.Text;
end;

procedure TUI_LOCRC1_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(LOCRC1_PRINT_frm);
  QRCompositeReport1.Reports.Add(LOCRC1_PRINT2_frm);
end;

procedure TUI_LOCRC1_frm.sButton1Click(Sender: TObject);
begin
  inherited;
  LOCRC1_PRINT_frm := TLOCRC1_PRINT_frm.Create(Self);
  LOCRC1_PRINT2_frm := TLOCRC1_PRINT2_frm.Create(Self);

  try
    LOCRC1_PRINT_frm.MaintNo := edt_MAINT_NO.Text;
    LOCRC1_PRINT2_frm.MaintNo := edt_MAINT_NO.Text;
    QRCompositeReport1.Prepare;
    case (Sender as TsButton).Tag of
      0 :
      begin
        QRCompositeReport1.PrinterSetup;
        if QRCompositeReport1.Tag = 0 then QRCompositeReport1.Print;
      end;
      1 :
      begin
        QRCompositeReport1.Preview;
      end;
    end;
  finally
    FreeAndNil(LOCRC1_PRINT_frm);
    FreeAndNil(LOCRC1_PRINT2_frm);
  end;
end;

procedure TUI_LOCRC1_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin

    case CHK2Value of
      9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

      6 :
      begin
        if AnsiMatchText( Column.FieldName , ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;

end;

procedure TUI_LOCRC1_frm.sDBGrid5DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_LOCRC1_frm.sPageControl1Change(Sender: TObject);
begin
  inherited;
  if (Sender as TsPageControl).ActivePageIndex = 3 then
  begin
    sPanel7.Visible := not (sPageControl1.ActivePageIndex = 3);
  end
  else
    sPanel7.Visible := True;
end;

end.
