inherited UI_APPSPC_frm: TUI_APPSPC_frm
  Left = 437
  Top = 58
  Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077')'#51032#47280#49436
  ClientHeight = 891
  ClientWidth = 1084
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 41
    Width = 1084
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter14: TsSplitter [1]
    Left = 0
    Top = 77
    Width = 1084
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sPanel1: TsPanel [2]
    Left = 0
    Top = 0
    Width = 1084
    Height = 41
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    DesignSize = (
      1084
      41)
    object sSpeedButton2: TsSpeedButton
      Left = 378
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton3: TsSpeedButton
      Left = 648
      Top = -5
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton4: TsSpeedButton
      Left = 749
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 457
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel
      Left = 8
      Top = 5
      Width = 153
      Height = 17
      Caption = #54032#47588#45824#44552#52628#49900'('#47588#51077')'#51032#47280#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel
      Left = 8
      Top = 20
      Width = 42
      Height = 13
      Caption = 'APPSPC'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton14: TsSpeedButton
      Left = 165
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton16: TsSpeedButton
      Left = 670
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton18: TsSpeedButton
      Left = 928
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object btnExit: TsButton
      Left = 980
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = #45803#44592
      TabOrder = 0
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
      Reflected = True
    end
    object btnNew: TsButton
      Left = 175
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #49888#44508#47928#49436#47484' '#51089#49457#54633#45768#45796'(Ctrl+N)'
      Caption = #49888#44508
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnNewClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 26
      ContentMargin = 8
      Reflected = True
    end
    object btnEdit: TsButton
      Left = 242
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49688#51221#54633#45768#45796'(Ctrl+E)'
      Caption = #49688#51221
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnEditClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 3
      ContentMargin = 8
      Reflected = True
    end
    object btnDel: TsButton
      Left = 309
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
      Reflected = True
    end
    object btnCopy: TsButton
      Left = 388
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #48373#49324
      TabOrder = 4
      OnClick = btnCopyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 28
      ContentMargin = 8
      Reflected = True
    end
    object btnPrint: TsButton
      Left = 680
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52636#47141
      TabOrder = 5
      OnClick = btnPrintClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
      Reflected = True
    end
    object btnTemp: TsButton
      Left = 467
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51076#49884
      Enabled = False
      TabOrder = 6
      OnClick = btnSaveClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 12
      ContentMargin = 8
      Reflected = True
    end
    object btnSave: TsButton
      Tag = 1
      Left = 534
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #51200#51109
      Enabled = False
      TabOrder = 7
      OnClick = btnSaveClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 7
      ContentMargin = 8
      Reflected = True
    end
    object btnCancel: TsButton
      Left = 601
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Caption = #52712#49548
      Enabled = False
      TabOrder = 8
      OnClick = btnCancelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 18
      ContentMargin = 8
      Reflected = True
    end
    object btnReady: TsButton
      Left = 751
      Top = 2
      Width = 93
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569#51456#48708
      TabOrder = 9
      OnClick = btnReadyClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 29
      ContentMargin = 8
      Reflected = True
    end
    object btnSend: TsButton
      Left = 852
      Top = 2
      Width = 74
      Height = 37
      Cursor = crHandPoint
      Caption = #51204#49569
      TabOrder = 10
      OnClick = btnSendClick
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System26
      ImageIndex = 22
      ContentMargin = 8
      Reflected = True
    end
  end
  object sPanel4: TsPanel [3]
    Left = 0
    Top = 44
    Width = 1084
    Height = 33
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    object mask_DATEE: TsMaskEdit
      Left = 270
      Top = 5
      Width = 83
      Height = 23
      TabStop = False
      AutoSize = False
      Color = clWhite
      Ctl3D = False
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      Text = '20161122'
      OnDblClick = mask_DATEEDblClick
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.Caption = #49888#52397#51068#51088
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
      SkinData.SkinSection = 'EDIT'
    end
    object edt_USER_ID: TsEdit
      Left = 432
      Top = 5
      Width = 57
      Height = 23
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object btn_Cal: TsBitBtn
      Tag = 901
      Left = 354
      Top = 5
      Width = 24
      Height = 23
      Cursor = crHandPoint
      TabOrder = 1
      TabStop = False
      OnClick = btn_CalClick
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 9
      Images = DMICON.System18
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 561
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      TabStop = False
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 3
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      OnDblClick = edt_CPCURRDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sBitBtn19: TsBitBtn
      Tag = 338
      Left = 594
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      TabOrder = 4
      TabStop = False
      OnClick = sBitBtn6Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 25
      Images = DMICON.System18
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 649
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      TabStop = False
      CharCase = ecUpperCase
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 3
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      OnDblClick = edt_CPCURRDblClick
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #50976#54805
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
    object sBitBtn20: TsBitBtn
      Tag = 338
      Left = 682
      Top = 5
      Width = 22
      Height = 23
      Cursor = crHandPoint
      TabOrder = 6
      TabStop = False
      OnClick = sBitBtn6Click
      SkinData.SkinSection = 'BUTTON'
      ImageIndex = 25
      Images = DMICON.System18
    end
    object edt_MAINT_NO: TsEdit
      Left = 64
      Top = 5
      Width = 137
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 35
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 7
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Indent = 0
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeft
      BoundLabel.MaxWidth = 0
      BoundLabel.UseSkinColor = True
    end
  end
  object Page_control: TsPageControl [4]
    Left = 0
    Top = 80
    Width = 1084
    Height = 811
    ActivePage = sTabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 0
    TabOrder = 2
    TabStop = False
    TabWidth = 110
    OnChange = Page_controlChange
    OnChanging = Page_controlChanging
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object sSplitter3: TsSplitter
        Left = 0
        Top = 0
        Width = 1076
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter4: TsSplitter
        Left = 291
        Top = 2
        Width = 3
        Height = 769
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page1_Left: TsPanel
        Left = 0
        Top = 2
        Width = 291
        Height = 769
        Align = alLeft
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object page1_Right: TsPanel
        Left = 294
        Top = 2
        Width = 782
        Height = 769
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        object sScrollBox1: TsScrollBox
          Left = 1
          Top = 1
          Width = 780
          Height = 767
          Align = alClient
          TabOrder = 0
          SkinData.SkinSection = 'TRANSPARENT'
          object basicPanel1: TsPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 136
            Align = alTop
            BevelOuter = bvNone
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 0
            TabStop = True
            SkinData.SkinSection = 'TRANSPARENT'
            object sLabel2: TsLabel
              Left = 287
              Top = 68
              Width = 12
              Height = 15
              Caption = #12641
            end
            object sLabel1: TsLabel
              Left = 218
              Top = 68
              Width = 12
              Height = 15
              Caption = #12641
            end
            object sLabel5: TsLabel
              Left = 457
              Top = 70
              Width = 186
              Height = 13
              Caption = #44277#44553#51088#49885#48324#51088'-'#44277#44553#51088#51008#54665'-'#44396#47588#51088#51008#54665
              ParentFont = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
            end
            object sLabel6: TsLabel
              Left = 120
              Top = 110
              Width = 108
              Height = 13
              Caption = 'ex) YYYYMMDD12345'
              ParentFont = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
            end
            object combo_BGMGUBUN: TsComboBox
              Left = 120
              Top = 42
              Width = 250
              Height = 23
              Alignment = taLeftJustify
              BoundLabel.Active = True
              BoundLabel.Caption = #51204#51088#47928#49436#44396#48516
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
              SkinData.CustomFont = True
              SkinData.SkinSection = 'COMBOBOX'
              VerticalAlignment = taAlignTop
              Color = clWhite
              Ctl3D = False
              ItemHeight = 17
              ItemIndex = 0
              ParentCtl3D = False
              TabOrder = 0
              Text = '2BK  :  '#52628#49900
              Items.Strings = (
                '2BK  :  '#52628#49900
                '2BJ  :  '#47588#51077)
            end
            object edt_CPNO: TsEdit
              Left = 120
              Top = 88
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 4
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #49888#52397#48264#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sPanel8: TsPanel
              Left = 0
              Top = 0
              Width = 776
              Height = 24
              Align = alTop
              BevelOuter = bvNone
              Caption = #44592#48376#51077#47141#49324#54637'('#52628#44032#44228#51340' '#51060#50808' '#54596#49688')'
              Color = 16042877
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 5
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
            end
            object edt_BGM1: TsEdit
              Left = 120
              Top = 66
              Width = 97
              Height = 21
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 12
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #47928#49436#48264#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_BGM2: TsEdit
              Tag = 336
              Left = 231
              Top = 66
              Width = 32
              Height = 21
              Hint = #44277#44553#51088#50857' '#47588#51077#52628#49900#51008#54665
              HelpKeyword = 'APPSPC'#51008#54665
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 2
              ParentCtl3D = False
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnDblClick = edt_CPCURRDblClick
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn1: TsBitBtn
              Tag = 336
              Left = 264
              Top = 66
              Width = 22
              Height = 21
              Cursor = crHandPoint
              TabOrder = 6
              TabStop = False
              OnClick = sBitBtn6Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 25
              Images = DMICON.System18
            end
            object edt_BGM3: TsEdit
              Tag = 337
              Left = 300
              Top = 66
              Width = 32
              Height = 21
              Hint = #44396#47588#51088#50857' '#44060#49444#51008#54665
              HelpKeyword = 'APPSPC'#51008#54665
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 2
              ParentCtl3D = False
              ParentFont = False
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnDblClick = edt_CPCURRDblClick
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn2: TsBitBtn
              Tag = 337
              Left = 333
              Top = 66
              Width = 22
              Height = 21
              Cursor = crHandPoint
              TabOrder = 7
              TabStop = False
              OnClick = sBitBtn6Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 25
              Images = DMICON.System18
            end
            object edt_BGM4: TsEdit
              Left = 356
              Top = 66
              Width = 100
              Height = 21
              TabStop = False
              Color = clBtnFace
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              TabOrder = 8
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
          end
          object basicPanel2: TsPanel
            Left = 0
            Top = 136
            Width = 776
            Height = 232
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            TabStop = True
            SkinData.SkinSection = 'TRANSPARENT'
            DesignSize = (
              776
              232)
            object sSpeedButton1: TsSpeedButton
              Left = 389
              Top = 16
              Width = 10
              Height = 217
              Anchors = [akLeft, akTop, akBottom]
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'SPEEDBUTTON'
            end
            object sPanel11: TsPanel
              Left = 0
              Top = 3
              Width = 385
              Height = 24
              BevelOuter = bvNone
              Caption = #49888#52397#51064
              Color = 16042877
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 15
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
            end
            object edt_APPCODE: TsEdit
              Tag = 101
              Left = 112
              Top = 43
              Width = 80
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnDblClick = edt_APPCODEDblClick
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #49888#52397#51064#53076#46300
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn5: TsBitBtn
              Tag = 101
              Left = 193
              Top = 43
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 16
              TabStop = False
              OnClick = sBitBtn5Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 6
              Images = DMICON.System18
            end
            object edt_APPSNAME: TsEdit
              Left = 112
              Top = 65
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #49345#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_APPNAME: TsEdit
              Left = 112
              Top = 87
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #45824#54364#51088
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_APPSAUPNO: TsMaskEdit
              Left = 112
              Top = 109
              Width = 91
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              EditMask = '999-99-99999;0;'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 12
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 3
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object edt_APPADDR1: TsEdit
              Left = 112
              Top = 131
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 4
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #51452#49548
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_APPADDR2: TsEdit
              Left = 112
              Top = 153
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 5
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_APPADDR3: TsEdit
              Left = 112
              Top = 175
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 6
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_APPELEC: TsEdit
              Left = 112
              Top = 197
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 7
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #51204#51088#49436#47749
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPBANK: TsEdit
              Tag = 201
              Left = 480
              Top = 43
              Width = 80
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 11
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 8
              OnDblClick = edt_CPBANKDblClick
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #51008#54665#53076#46300
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn3: TsBitBtn
              Tag = 201
              Left = 561
              Top = 43
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 17
              TabStop = False
              OnClick = sBitBtn3Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 6
              Images = DMICON.System18
            end
            object edt_CPBANKBU: TsEdit
              Left = 480
              Top = 87
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 10
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #51648#51216#47749
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPNAME1: TsEdit
              Left = 480
              Top = 131
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 12
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44228#51340#51452
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPACCOUNTNO: TsEdit
              Left = 480
              Top = 109
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 17
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 11
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44228#51340#48264#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPNAME2: TsEdit
              Left = 480
              Top = 153
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 13
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPCURR: TsEdit
              Left = 480
              Top = 175
              Width = 80
              Height = 21
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 3
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 14
              OnDblClick = edt_CPCURRDblClick
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44228#51340#53685#54868
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn6: TsBitBtn
              Tag = 301
              Left = 561
              Top = 175
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 18
              TabStop = False
              OnClick = sBitBtn6Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 25
              Images = DMICON.System18
            end
            object sPanel19: TsPanel
              Left = 384
              Top = 3
              Width = 386
              Height = 24
              BevelOuter = bvNone
              Caption = #52628#49900#51008#54665
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 19
              SkinData.SkinSection = 'DRAGBAR'
            end
            object edt_CPBANKNAME: TsEdit
              Left = 480
              Top = 65
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 9
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
          end
          object basicPanel3: TsPanel
            Left = 0
            Top = 368
            Width = 776
            Height = 192
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            TabStop = True
            SkinData.SkinSection = 'TRANSPARENT'
            DesignSize = (
              776
              192)
            object sSpeedButton8: TsSpeedButton
              Left = 389
              Top = 11
              Width = 10
              Height = 237
              Anchors = [akLeft, akTop, akBottom]
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'SPEEDBUTTON'
            end
            object sPanel20: TsPanel
              Left = 381
              Top = 4
              Width = 386
              Height = 24
              BevelOuter = bvNone
              Caption = #52628#44032#44228#51340
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 13
              SkinData.SkinSection = 'DRAGBAR'
            end
            object sPanel9: TsPanel
              Left = 0
              Top = 4
              Width = 385
              Height = 24
              BevelOuter = bvNone
              Caption = #44396#47588#51088
              Color = 16042877
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 12
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
            end
            object mask_DFSAUPNO: TsMaskEdit
              Tag = 109
              Left = 112
              Top = 44
              Width = 91
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              EditMask = '999-99-99999;0;'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 12
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.Caption = #49324#50629#51088#46321#47197#48264#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
              SkinData.SkinSection = 'EDIT'
            end
            object edt_DFNAME1: TsEdit
              Tag = 109
              Left = 112
              Top = 66
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #49345#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_DFNAME2: TsEdit
              Left = 112
              Top = 88
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_DFNAME3: TsEdit
              Left = 112
              Top = 110
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 3
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_DFEMAIL1: TsEdit
              Left = 112
              Top = 132
              Width = 116
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 4
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = 'E-MAIL'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_DFEMAIL2: TsEdit
              Left = 248
              Top = 132
              Width = 114
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 5
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = '@'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPADDACCOUNTNO1: TsEdit
              Left = 480
              Top = 44
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 17
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 6
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44228#51340#48264#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPADDNAME1: TsEdit
              Left = 480
              Top = 66
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 7
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44228#51340#51452
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = 15790047
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPADDCURR1: TsEdit
              Left = 480
              Top = 88
              Width = 80
              Height = 21
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 3
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 8
              OnDblClick = edt_CPCURRDblClick
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44228#51340#53685#54868
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = 15790047
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn7: TsBitBtn
              Tag = 302
              Left = 561
              Top = 88
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 14
              TabStop = False
              OnClick = sBitBtn6Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 25
              Images = DMICON.System18
            end
            object edt_CPADDACCOUNTNO2: TsEdit
              Left = 480
              Top = 110
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 17
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 9
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44228#51340#48264#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = 15790047
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPADDNAME2: TsEdit
              Left = 480
              Top = 132
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 10
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44228#51340#51452
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = 15790047
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_CPADDCURR2: TsEdit
              Left = 480
              Top = 154
              Width = 80
              Height = 21
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 3
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 11
              OnDblClick = edt_CPCURRDblClick
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44228#51340#53685#54868
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = 15790047
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn9: TsBitBtn
              Tag = 303
              Left = 561
              Top = 154
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 15
              TabStop = False
              OnClick = sBitBtn6Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 25
              Images = DMICON.System18
            end
            object sBitBtn65: TsBitBtn
              Tag = 109
              Left = 204
              Top = 44
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 16
              TabStop = False
              OnClick = sBitBtn65Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 6
              Images = DMICON.System18
            end
          end
          object basicPanel4: TsPanel
            Left = 0
            Top = 560
            Width = 776
            Height = 76
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 3
            TabStop = True
            SkinData.SkinSection = 'TRANSPARENT'
            object sPanel32: TsPanel
              Left = -3
              Top = 1
              Width = 770
              Height = 24
              BevelOuter = bvNone
              Caption = #52628#49900#44552#50529
              Color = 16042877
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 4
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
            end
            object edt_CPAMTC: TsEdit
              Tag = 102
              Left = 112
              Top = 40
              Width = 33
              Height = 21
              Hint = #53685#54868
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 3
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              OnDblClick = edt_CPCURRDblClick
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #52628#49900#44552#50529'('#50808#54868')'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn4: TsBitBtn
              Tag = 304
              Left = 146
              Top = 40
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 5
              TabStop = False
              OnClick = sBitBtn6Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 25
              Images = DMICON.System18
            end
            object curr_CPAMTU: TsCurrencyEdit
              Left = 171
              Top = 40
              Width = 116
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
              SkinData.SkinSection = 'EDIT'
              GlyphMode.Blend = 0
              GlyphMode.Grayed = False
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object curr_CPCUX: TsCurrencyEdit
              Left = 356
              Top = 40
              Width = 116
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
              BoundLabel.Active = True
              BoundLabel.Caption = #51201#50857#54872#50984
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              GlyphMode.Blend = 0
              GlyphMode.Grayed = False
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
            object edt_RCT_AMT2C: TsEdit
              Tag = 102
              Left = 572
              Top = 40
              Width = 33
              Height = 21
              Hint = #53685#54868
              HelpContext = 1
              TabStop = False
              CharCase = ecUpperCase
              Color = clBtnFace
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 3
              ParentCtl3D = False
              ParentFont = False
              ReadOnly = True
              TabOrder = 6
              Text = 'KRW'
              SkinData.CustomColor = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #52628#49900#44552#50529'('#50896#54868')'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object curr_CPAMT: TsCurrencyEdit
              Left = 606
              Top = 40
              Width = 116
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 3
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
              SkinData.SkinSection = 'EDIT'
              GlyphMode.Blend = 0
              GlyphMode.Grayed = False
              DecimalPlaces = 4
              DisplayFormat = '#,0.####;0'
            end
          end
          object basicPanel5: TsPanel
            Left = 0
            Top = 636
            Width = 776
            Height = 127
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 4
            TabStop = True
            SkinData.SkinSection = 'TRANSPARENT'
            object sPanel22: TsPanel
              Left = 0
              Top = 4
              Width = 765
              Height = 24
              BevelOuter = bvNone
              Caption = #45236#44397#49888#50857#51109
              Color = 16042877
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 7
              SkinData.CustomColor = True
              SkinData.SkinSection = 'DRAGBAR'
            end
            object edt_DOCNO: TsEdit
              Left = 112
              Top = 44
              Width = 226
              Height = 21
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #47928#49436#48264#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #44404#47548#52404
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn10: TsBitBtn
              Tag = 102
              Left = 339
              Top = 44
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 8
              TabStop = False
              OnClick = sBitBtn10Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 25
              Images = DMICON.System18
            end
            object mask_ISSDATE: TsMaskEdit
              Left = 112
              Top = 66
              Width = 73
              Height = 21
              AutoSize = False
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              EditMask = '9999-99-99;0'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -13
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 10
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              CheckOnExit = True
              BoundLabel.Active = True
              BoundLabel.Caption = #44060#49444#51068#51088
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #44404#47548#52404
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
              SkinData.CustomFont = True
              SkinData.SkinSection = 'EDIT'
            end
            object edt_LABANKBUCODE: TsEdit
              Tag = 202
              Left = 112
              Top = 88
              Width = 73
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 11
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
              OnDblClick = edt_CPBANKDblClick
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44060#49444#51008#54665
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #44404#47548#52404
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sBitBtn11: TsBitBtn
              Tag = 202
              Left = 186
              Top = 88
              Width = 24
              Height = 21
              Cursor = crHandPoint
              TabOrder = 9
              TabStop = False
              OnClick = sBitBtn3Click
              SkinData.SkinSection = 'BUTTON'
              ImageIndex = 6
              Images = DMICON.System18
            end
            object edt_LABANKNAMEP: TsEdit
              Left = 211
              Top = 88
              Width = 152
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 3
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object combo_LATYPE: TsComboBox
              Left = 496
              Top = 44
              Width = 250
              Height = 23
              Alignment = taLeftJustify
              BoundLabel.Active = True
              BoundLabel.Caption = #51333#47448
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clBlack
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
              SkinData.CustomFont = True
              SkinData.SkinSection = 'COMBOBOX'
              VerticalAlignment = taAlignTop
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ItemHeight = 17
              ItemIndex = 0
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 4
              Text = #50896#54868#54364#49884' '#50808#54868#48512#44592' '#45236#44397#49888#50857#51109
              Items.Strings = (
                #50896#54868#54364#49884' '#50808#54868#48512#44592' '#45236#44397#49888#50857#51109
                #50808#54868#54364#49884' '#45236#44397#49888#50857#51109
                #50896#54868#54364#49884' '#45236#44397#49888#50857#51109)
            end
            object edt_LRNO: TsEdit
              Left = 496
              Top = 68
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 5
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #45236#44397#49888#50857#51109#48264#54840
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_LABANKBUP: TsEdit
              Left = 496
              Top = 90
              Width = 250
              Height = 21
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 6
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44060#49444#51008#54665#51648#51216#47749
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #47569#51008' '#44256#46357
              BoundLabel.Font.Style = [fsBold]
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
          end
        end
      end
    end
    object sTabSheet2: TsTabSheet
      Caption = #47932#54408#49688#47161#51613#47749#49436
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object sSplitter6: TsSplitter
        Left = 0
        Top = 0
        Width = 1076
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter5: TsSplitter
        Left = 291
        Top = 2
        Width = 3
        Height = 769
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page2_Left: TsPanel
        Left = 0
        Top = 2
        Width = 291
        Height = 769
        Align = alLeft
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object sPanel31: TsPanel
        Left = 294
        Top = 2
        Width = 782
        Height = 769
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        object sPanel44: TsPanel
          Left = 1
          Top = 1
          Width = 780
          Height = 767
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          SkinData.SkinSection = 'TRANSPARENT'
          object GoodsSIZE_Panel: TsPanel
            Left = 288
            Top = 1000
            Width = 465
            Height = 305
            TabOrder = 0
            SkinData.SkinSection = 'PANEL'
            object panel_edt_size1: TsEdit
              Left = 46
              Top = 16
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #44508#44201
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #44404#47548#52404
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsSIZE2: TsEdit
              Left = 46
              Top = 40
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsSIZE3: TsEdit
              Left = 46
              Top = 64
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsSIZE4: TsEdit
              Left = 46
              Top = 88
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 3
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object sButton2: TsButton
              Tag = 401
              Left = 142
              Top = 264
              Width = 87
              Height = 23
              Caption = #54869#51064
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 10
              OnClick = sButton2Click
              SkinData.SkinSection = 'BUTTON'
              Images = DMICON.System18
              ImageIndex = 17
              Reflected = True
            end
            object sButton3: TsButton
              Tag = 401
              Left = 230
              Top = 264
              Width = 87
              Height = 23
              Caption = #52712#49548
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 11
              OnClick = sButton3Click
              SkinData.SkinSection = 'BUTTON'
              Images = DMICON.System18
              ImageIndex = 18
              Reflected = True
            end
            object edt_GoodsSIZE5: TsEdit
              Left = 46
              Top = 112
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 4
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsSIZE6: TsEdit
              Left = 46
              Top = 136
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 5
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsSIZE7: TsEdit
              Left = 46
              Top = 160
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 6
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsSIZE10: TsEdit
              Left = 46
              Top = 232
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 9
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsSIZE9: TsEdit
              Left = 46
              Top = 208
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 8
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsSIZE8: TsEdit
              Left = 46
              Top = 184
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 70
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 7
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
          end
          object GoodsimdcodePanel: TsPanel
            Left = 288
            Top = 1000
            Width = 464
            Height = 185
            TabOrder = 1
            SkinData.SkinSection = 'PANEL'
            object imdcodePanel_edit1: TsEdit
              Left = 46
              Top = 24
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Active = True
              BoundLabel.Caption = #54408#47785
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = ANSI_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -12
              BoundLabel.Font.Name = #44404#47548#52404
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsIMDCODE2: TsEdit
              Left = 46
              Top = 48
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsIMDCODE3: TsEdit
              Left = 46
              Top = 72
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 2
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object edt_GoodsIMDCODE4: TsEdit
              Left = 46
              Top = 96
              Width = 403
              Height = 23
              HelpContext = 1
              CharCase = ecUpperCase
              Color = clWhite
              Ctl3D = True
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -12
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              MaxLength = 35
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 3
              SkinData.SkinSection = 'EDIT'
              BoundLabel.Indent = 0
              BoundLabel.Font.Charset = DEFAULT_CHARSET
              BoundLabel.Font.Color = clWindowText
              BoundLabel.Font.Height = -11
              BoundLabel.Font.Name = 'MS Sans Serif'
              BoundLabel.Font.Style = []
              BoundLabel.Layout = sclLeft
              BoundLabel.MaxWidth = 0
              BoundLabel.UseSkinColor = True
            end
            object imdcodeOk: TsButton
              Tag = 401
              Left = 158
              Top = 144
              Width = 87
              Height = 23
              Caption = #54869#51064
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 4
              OnClick = imdcodeOkClick
              SkinData.SkinSection = 'BUTTON'
              Images = DMICON.System18
              ImageIndex = 17
              Reflected = True
            end
            object sButton1: TsButton
              Tag = 401
              Left = 246
              Top = 144
              Width = 87
              Height = 23
              Caption = #52712#49548
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 5
              OnClick = sButton1Click
              SkinData.SkinSection = 'BUTTON'
              Images = DMICON.System18
              ImageIndex = 18
              Reflected = True
            end
          end
          object sScrollBox2: TsScrollBox
            Left = 0
            Top = 0
            Width = 780
            Height = 767
            Align = alClient
            TabOrder = 2
            SkinData.SkinSection = 'TRANSPARENT'
            object goodsDbPanel: TsPanel
              Left = 0
              Top = 0
              Width = 776
              Height = 135
              Align = alTop
              BevelOuter = bvNone
              PopupMenu = goodsMenu
              TabOrder = 0
              SkinData.SkinSection = 'TRANSPARENT'
              object sDBGrid3: TsDBGrid
                Left = 0
                Top = 25
                Width = 776
                Height = 110
                Align = alTop
                Color = clWhite
                DataSource = dsGoods
                Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                PopupMenu = goodsMenu
                TabOrder = 0
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clBlack
                TitleFont.Height = -12
                TitleFont.Name = #47569#51008' '#44256#46357
                TitleFont.Style = []
                SkinData.SkinSection = 'EDIT'
                Columns = <
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'SEQ'
                    Title.Alignment = taCenter
                    Title.Caption = #49692#48264
                    Width = 30
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DOC_NO'
                    Title.Alignment = taCenter
                    Title.Caption = #47928#49436#48264#54840
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'LR_NO'
                    Title.Alignment = taCenter
                    Title.Caption = #45236#44397#49888#50857#51109#48264#54840
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'BY_SNAME'
                    Title.Alignment = taCenter
                    Title.Caption = #47932#54408#49688#47161#51064
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'LA_BANKNAME'
                    Title.Alignment = taCenter
                    Title.Caption = #44060#49444#51008#54665
                    Width = 150
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'ISS_DATE'
                    Title.Alignment = taCenter
                    Title.Caption = #48156#44553#51068#51088
                    Width = 80
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'ACC_DATE'
                    Title.Alignment = taCenter
                    Title.Caption = #51064#49688#51068#51088
                    Width = 80
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ISS_AMT'
                    Title.Alignment = taCenter
                    Title.Caption = #44060#49444#44552#50529'('#50896#54868')'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ISS_AMTU'
                    Title.Alignment = taCenter
                    Title.Caption = #44060#49444#44552#50529'('#50808#54868')'
                    Width = 100
                    Visible = True
                  end>
              end
              object goodsBtnPanel: TsPanel
                Left = 0
                Top = 0
                Width = 776
                Height = 25
                Align = alTop
                TabOrder = 1
                SkinData.SkinSection = 'TRANSPARENT'
                object sSpeedButton10: TsSpeedButton
                  Left = 1
                  Top = 1
                  Width = 5
                  Height = 23
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object sSpeedButton19: TsSpeedButton
                  Left = 73
                  Top = 1
                  Width = 5
                  Height = 23
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object btn_goodsDel: TsSpeedButton
                  Tag = 3
                  Left = 150
                  Top = 1
                  Width = 67
                  Height = 23
                  Cursor = crHandPoint
                  Caption = #49325#51228
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 1
                  Images = DMICON.System18
                end
                object sSpeedButton22: TsSpeedButton
                  Left = 145
                  Top = 1
                  Width = 5
                  Height = 23
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object btn_goodsMod: TsSpeedButton
                  Tag = 2
                  Left = 78
                  Top = 1
                  Width = 67
                  Height = 23
                  Cursor = crHandPoint
                  Caption = #49688#51221
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 3
                  Images = DMICON.System18
                end
                object sSpeedButton24: TsSpeedButton
                  Left = 217
                  Top = 1
                  Width = 5
                  Height = 23
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object btn_goodsSave: TsSpeedButton
                  Tag = 4
                  Left = 222
                  Top = 1
                  Width = 67
                  Height = 23
                  Cursor = crHandPoint
                  Caption = #51200#51109
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 17
                  Images = DMICON.System18
                end
                object btn_goodsCancel: TsSpeedButton
                  Tag = 5
                  Left = 289
                  Top = 1
                  Width = 67
                  Height = 23
                  Cursor = crHandPoint
                  Caption = #52712#49548
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 18
                  Images = DMICON.System18
                end
                object sSpeedButton27: TsSpeedButton
                  Left = 356
                  Top = 1
                  Width = 5
                  Height = 23
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object btn_goodsNew: TsSpeedButton
                  Tag = 1
                  Left = 6
                  Top = 1
                  Width = 67
                  Height = 23
                  Cursor = crHandPoint
                  Caption = #51077#47141
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 2
                  Images = DMICON.System18
                end
              end
            end
            object goodsPanel1: TsPanel
              Left = 0
              Top = 135
              Width = 776
              Height = 143
              Align = alTop
              BevelOuter = bvNone
              PopupMenu = goodsMenu
              TabOrder = 1
              TabStop = True
              SkinData.SkinSection = 'TRANSPARENT'
              DesignSize = (
                776
                143)
              object sSpeedButton29: TsSpeedButton
                Left = 381
                Top = 4
                Width = 10
                Height = 309
                Anchors = [akLeft, akTop, akBottom]
                ButtonStyle = tbsDivider
                SkinData.SkinSection = 'SPEEDBUTTON'
              end
              object sPanel36: TsPanel
                Left = 381
                Top = 2
                Width = 386
                Height = 19
                BevelOuter = bvNone
                Caption = #47932#54408#49688#47161#51064
                Ctl3D = False
                ParentCtl3D = False
                TabOrder = 13
                SkinData.SkinSection = 'DRAGBAR'
              end
              object sPanel37: TsPanel
                Left = 0
                Top = 2
                Width = 385
                Height = 19
                BevelOuter = bvNone
                Caption = #47932#54408#49688#47161#51613#47749#49436
                Color = 16042877
                Ctl3D = False
                ParentCtl3D = False
                TabOrder = 14
                SkinData.CustomColor = True
                SkinData.SkinSection = 'DRAGBAR'
              end
              object edt_GoodsDOCNO: TsEdit
                Left = 112
                Top = 22
                Width = 225
                Height = 19
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #47928#49436#48264#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #44404#47548#52404
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn31: TsBitBtn
                Tag = 102
                Left = 338
                Top = 22
                Width = 23
                Height = 19
                Cursor = crHandPoint
                TabOrder = 15
                TabStop = False
                OnClick = sBitBtn31Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object edt_GoodsBYNAME: TsEdit
                Left = 480
                Top = 42
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 8
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #45824#54364#51088#47749
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsBYELEC: TsEdit
                Left = 480
                Top = 62
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 9
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51204#51088#49436#47749
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsBYADDR1: TsEdit
                Left = 480
                Top = 82
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 10
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51452#49548
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsBYADDR2: TsEdit
                Left = 480
                Top = 102
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 11
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsBYADDR3: TsEdit
                Left = 480
                Top = 122
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 12
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object mask_GoodsVALDATE: TsMaskEdit
                Left = 112
                Top = 122
                Width = 94
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                EditMask = '9999-99-99;0'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 10
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 5
                Text = '20161122'
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = #50976#54952#44592#51068
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
              end
              object mask_GoodsACCDATE: TsMaskEdit
                Left = 112
                Top = 102
                Width = 94
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                EditMask = '9999-99-99;0'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 10
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 4
                Text = '20161122'
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = #51064#49688#51068#51088
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
              end
              object mask_GoodsISSDATE: TsMaskEdit
                Left = 112
                Top = 82
                Width = 94
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                EditMask = '9999-99-99;0'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 10
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 3
                Text = '20161122'
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = #48156#44553#51068#51088
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.CustomColor = True
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
              end
              object mask_GoodsBSNHSCODE: TsMaskEdit
                Left = 112
                Top = 62
                Width = 94
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                EditMask = '0000.00-0000;0;'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 12
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 2
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = 'HS'#48512#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
              end
              object edt_GoodsLRNO2: TsEdit
                Left = 112
                Top = 42
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 1
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #48156#44553#48264#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsBYCODE: TsEdit
                Tag = 102
                Left = 480
                Top = 22
                Width = 49
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 6
                OnDblClick = edt_APPCODEDblClick
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #49345#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn62: TsBitBtn
                Tag = 102
                Left = 530
                Top = 22
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 16
                TabStop = False
                OnClick = sBitBtn5Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 6
                Images = DMICON.System18
              end
              object edt_GoodsBYSNAME: TsEdit
                Left = 555
                Top = 22
                Width = 175
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 7
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
            end
            object goodsPanel2: TsPanel
              Left = 0
              Top = 278
              Width = 776
              Height = 81
              Align = alTop
              BevelOuter = bvNone
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = #47569#51008' '#44256#46357
              Font.Style = []
              ParentFont = False
              PopupMenu = goodsMenu
              TabOrder = 2
              TabStop = True
              SkinData.SkinSection = 'TRANSPARENT'
              DesignSize = (
                776
                81)
              object sSpeedButton11: TsSpeedButton
                Left = 381
                Top = -47
                Width = 10
                Height = 145
                Anchors = [akLeft, akTop, akBottom]
                ButtonStyle = tbsDivider
                SkinData.SkinSection = 'SPEEDBUTTON'
              end
              object edt_GoodsSECODE: TsEdit
                Tag = 108
                Left = 112
                Top = 20
                Width = 81
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
                OnDblClick = edt_APPCODEDblClick
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #44277#44553#51088
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn32: TsBitBtn
                Tag = 108
                Left = 193
                Top = 20
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 5
                TabStop = False
                OnClick = sBitBtn5Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 6
                Images = DMICON.System18
              end
              object sPanel38: TsPanel
                Left = 384
                Top = 0
                Width = 385
                Height = 19
                BevelOuter = bvNone
                Caption = #45236#44397#49888#50857#51109
                Color = 16042877
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 6
                SkinData.CustomColor = True
                SkinData.SkinSection = 'DRAGBAR'
              end
              object edt_GoodsLRNO: TsEdit
                Left = 480
                Top = 20
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 2
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #48264#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object mask_GoodsGETDATE: TsMaskEdit
                Left = 480
                Top = 40
                Width = 94
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                EditMask = '9999-99-99;0'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 10
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 3
                Text = '20161122'
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = #47932#54408#51064#46020#44592#51068
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
              end
              object mask_GoodsEXPDATE: TsMaskEdit
                Left = 480
                Top = 60
                Width = 94
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                EditMask = '9999-99-99;0'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 10
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 4
                Text = '20161122'
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = #50976#54952#44592#51068
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
              end
              object sPanel39: TsPanel
                Left = 0
                Top = 0
                Width = 385
                Height = 19
                BevelOuter = bvNone
                Caption = #44277#44553#51088
                Color = 16042877
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 7
                SkinData.CustomColor = True
                SkinData.SkinSection = 'DRAGBAR'
              end
              object edt_GoodsSESNAME: TsEdit
                Left = 112
                Top = 40
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 1
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
            end
            object goodsPanel3: TsPanel
              Left = 0
              Top = 359
              Width = 776
              Height = 121
              Align = alTop
              BevelOuter = bvNone
              PopupMenu = goodsMenu
              TabOrder = 3
              TabStop = True
              SkinData.SkinSection = 'TRANSPARENT'
              object sPanel41: TsPanel
                Left = 0
                Top = 0
                Width = 776
                Height = 19
                Align = alTop
                BevelOuter = bvNone
                Caption = #44060#49444#51008#54665
                Color = 16042877
                Ctl3D = False
                ParentCtl3D = False
                TabOrder = 15
                SkinData.CustomColor = True
                SkinData.SkinSection = 'DRAGBAR'
              end
              object edt_GoodsLABANK: TsEdit
                Tag = 203
                Left = 112
                Top = 20
                Width = 49
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
                OnDblClick = edt_CPBANKDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51008#54665#53076#46300
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn33: TsBitBtn
                Tag = 203
                Left = 162
                Top = 20
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 16
                TabStop = False
                OnClick = sBitBtn3Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 6
                Images = DMICON.System18
              end
              object edt_goodsLAELEC: TsEdit
                Left = 112
                Top = 40
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 3
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51204#51088#49436#47749
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object curr_goodsISSAMT: TsCurrencyEdit
                Left = 112
                Top = 60
                Width = 166
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 5
                BoundLabel.Active = True
                BoundLabel.Caption = #44060#49444#44552#50529
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object curr_goodsISSEXPAMT: TsCurrencyEdit
                Left = 112
                Top = 80
                Width = 166
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 8
                BoundLabel.Active = True
                BoundLabel.Caption = #51064#49688#44552#50529
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object edt_goodsTOTCNTC: TsEdit
                Tag = 305
                Left = 112
                Top = 100
                Width = 39
                Height = 19
                Hint = #45800#50948
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 3
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 11
                OnDblClick = edt_CPCURRDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #52509#49688#47049
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object curr_goodsTOTCNT: TsCurrencyEdit
                Left = 177
                Top = 100
                Width = 101
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 12
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object edt_GoodsLABANKBU: TsEdit
                Left = 480
                Top = 20
                Width = 250
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 2
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51648#51216#47749
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object curr_goodsEXCH: TsCurrencyEdit
                Left = 480
                Top = 40
                Width = 116
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = True
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 4
                BoundLabel.Active = True
                BoundLabel.Caption = #47588#47588#44592#51456#50984
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object edt_goodsISSAMTC: TsEdit
                Tag = 102
                Left = 480
                Top = 60
                Width = 33
                Height = 19
                Hint = #53685#54868
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 3
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 6
                OnDblClick = edt_CPCURRDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #44060#49444#44552#50529'('#50808#54868')'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn34: TsBitBtn
                Tag = 306
                Left = 514
                Top = 60
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 17
                TabStop = False
                OnClick = sBitBtn6Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object curr_goodsISSAMTU: TsCurrencyEdit
                Left = 539
                Top = 60
                Width = 142
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 7
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object curr_goodsISSEXPAMTU: TsCurrencyEdit
                Left = 539
                Top = 80
                Width = 142
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 10
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object curr_goodsISSTOTAMT: TsCurrencyEdit
                Left = 539
                Top = 100
                Width = 142
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 14
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object edt_goodsISSEXPAMTC: TsEdit
                Tag = 102
                Left = 480
                Top = 80
                Width = 33
                Height = 19
                Hint = #53685#54868
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 3
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 9
                OnDblClick = edt_CPCURRDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51064#49688#44552#50529'('#50808#54868')'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn35: TsBitBtn
                Tag = 307
                Left = 514
                Top = 80
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 18
                TabStop = False
                OnClick = sBitBtn6Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object sBitBtn36: TsBitBtn
                Tag = 308
                Left = 514
                Top = 100
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 19
                TabStop = False
                OnClick = sBitBtn6Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object edt_goodsISSTOTAMTC: TsEdit
                Tag = 102
                Left = 480
                Top = 100
                Width = 33
                Height = 19
                Hint = #53685#54868
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 3
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 13
                OnDblClick = edt_CPCURRDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #52509#44552#50529
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn37: TsBitBtn
                Tag = 305
                Left = 152
                Top = 100
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 20
                TabStop = False
                OnClick = sBitBtn6Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object edt_GoodsLABANKNAME: TsEdit
                Left = 187
                Top = 20
                Width = 175
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 1
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
            end
            object goodsPanel4: TsPanel
              Left = 0
              Top = 480
              Width = 776
              Height = 121
              Align = alTop
              BevelOuter = bvNone
              PopupMenu = goodsMenu
              TabOrder = 4
              TabStop = True
              SkinData.SkinSection = 'TRANSPARENT'
              DesignSize = (
                776
                121)
              object sSpeedButton12: TsSpeedButton
                Left = 381
                Top = 1
                Width = 10
                Height = 136
                Anchors = [akLeft, akTop, akBottom]
                ButtonStyle = tbsDivider
                SkinData.SkinSection = 'SPEEDBUTTON'
              end
              object sPanel43: TsPanel
                Left = 0
                Top = 0
                Width = 385
                Height = 19
                BevelOuter = bvNone
                Caption = #44592#53440#51312#44148
                Color = 16042877
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 10
                SkinData.CustomColor = True
                SkinData.SkinSection = 'DRAGBAR'
              end
              object sPanel45: TsPanel
                Left = 381
                Top = 0
                Width = 386
                Height = 19
                BevelOuter = bvNone
                Caption = #52280#51312#49324#54637
                Color = 16042877
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 11
                SkinData.CustomColor = True
                SkinData.SkinSection = 'DRAGBAR'
              end
              object edt_GoodsREMARK2_1: TsEdit
                Left = 64
                Top = 20
                Width = 297
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #44592#53440#51312#44148
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsREMARK2_3: TsEdit
                Left = 64
                Top = 60
                Width = 297
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 2
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsREMARK2_4: TsEdit
                Left = 64
                Top = 80
                Width = 297
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 3
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsREMARK2_5: TsEdit
                Left = 64
                Top = 100
                Width = 297
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 4
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsREMARK2_2: TsEdit
                Left = 64
                Top = 40
                Width = 297
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 1
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsREMARK1_3: TsEdit
                Left = 448
                Top = 60
                Width = 305
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 7
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsREMARK1_5: TsEdit
                Left = 448
                Top = 100
                Width = 305
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 9
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsREMARK1_2: TsEdit
                Left = 448
                Top = 40
                Width = 305
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 6
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsREMARK1_4: TsEdit
                Left = 448
                Top = 80
                Width = 305
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 8
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsREMARK1_1: TsEdit
                Left = 448
                Top = 20
                Width = 305
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 5
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #52280#51312#49324#54637
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
            end
            object goodsPanel5: TsPanel
              Left = 0
              Top = 601
              Width = 776
              Height = 162
              Align = alClient
              BevelOuter = bvNone
              PopupMenu = goodsItemMenu
              TabOrder = 5
              TabStop = True
              SkinData.SkinSection = 'TRANSPARENT'
              object sSplitter10: TsSplitter
                Left = 0
                Top = 37
                Width = 776
                Height = 2
                Cursor = crVSplit
                Align = alTop
                AutoSnap = False
                Enabled = False
                SkinData.SkinSection = 'SPLITTER'
              end
              object sPanel47: TsPanel
                Left = 0
                Top = 0
                Width = 776
                Height = 19
                Align = alTop
                BevelOuter = bvNone
                Caption = #54408#47785#45236#50669
                Color = 16042877
                Ctl3D = False
                ParentCtl3D = False
                TabOrder = 15
                SkinData.CustomColor = True
                SkinData.SkinSection = 'DRAGBAR'
              end
              object goodsItemBtnPanel: TsPanel
                Left = 0
                Top = 19
                Width = 776
                Height = 18
                Align = alTop
                TabOrder = 16
                SkinData.SkinSection = 'TRANSPARENT'
                object sSpeedButton13: TsSpeedButton
                  Left = 1
                  Top = 1
                  Width = 5
                  Height = 16
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object sSpeedButton15: TsSpeedButton
                  Left = 73
                  Top = 1
                  Width = 5
                  Height = 16
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object btn_goodsItemDel: TsSpeedButton
                  Tag = 3
                  Left = 150
                  Top = 1
                  Width = 67
                  Height = 16
                  Cursor = crHandPoint
                  Caption = #49325#51228
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsItemNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 1
                  Images = DMICON.System18
                end
                object sSpeedButton17: TsSpeedButton
                  Left = 145
                  Top = 1
                  Width = 5
                  Height = 16
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object btn_goodsItemMod: TsSpeedButton
                  Tag = 2
                  Left = 78
                  Top = 1
                  Width = 67
                  Height = 16
                  Cursor = crHandPoint
                  Caption = #49688#51221
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsItemNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 3
                  Images = DMICON.System18
                end
                object sSpeedButton20: TsSpeedButton
                  Left = 217
                  Top = 1
                  Width = 5
                  Height = 16
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object btn_goodsItemSave: TsSpeedButton
                  Tag = 4
                  Left = 222
                  Top = 1
                  Width = 67
                  Height = 16
                  Cursor = crHandPoint
                  Caption = #51200#51109
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsItemNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 17
                  Images = DMICON.System18
                end
                object btn_goodsItemCancel: TsSpeedButton
                  Tag = 5
                  Left = 289
                  Top = 1
                  Width = 67
                  Height = 16
                  Cursor = crHandPoint
                  Caption = #52712#49548
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsItemNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 18
                  Images = DMICON.System18
                end
                object sSpeedButton32: TsSpeedButton
                  Left = 356
                  Top = 1
                  Width = 5
                  Height = 16
                  Cursor = crHandPoint
                  Layout = blGlyphTop
                  Spacing = 0
                  Align = alLeft
                  ButtonStyle = tbsDivider
                  SkinData.SkinSection = 'TRANSPARENT'
                  Reflected = True
                end
                object btn_goodsItemNew: TsSpeedButton
                  Tag = 1
                  Left = 6
                  Top = 1
                  Width = 67
                  Height = 16
                  Cursor = crHandPoint
                  Caption = #51077#47141
                  Enabled = False
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = #44404#47548#52404
                  Font.Style = [fsBold]
                  ParentFont = False
                  OnClick = btn_goodsItemNewClick
                  Align = alLeft
                  SkinData.CustomFont = True
                  SkinData.SkinSection = 'TRANSPARENT'
                  ImageIndex = 2
                  Images = DMICON.System18
                end
              end
              object sDBGrid4: TsDBGrid
                Left = 0
                Top = 39
                Width = 249
                Height = 123
                Align = alLeft
                Color = clWhite
                DataSource = dsGoodsitem
                Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                PopupMenu = goodsItemMenu
                TabOrder = 17
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clBlack
                TitleFont.Height = -12
                TitleFont.Name = #47569#51008' '#44256#46357
                TitleFont.Style = []
                SkinData.SkinSection = 'EDIT'
                Columns = <
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'LINE_NO'
                    Title.Alignment = taCenter
                    Title.Caption = #51068#47144#48264#54840
                    Width = 90
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'HS_NO'
                    Title.Alignment = taCenter
                    Title.Caption = 'HS'#48512#54840
                    Width = 188
                    Visible = True
                  end>
              end
              object edt_GoodsLINENO: TsEdit
                Left = 322
                Top = 41
                Width = 97
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 8
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51068#47144#48264#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object mask_GoodsHSNO: TsMaskEdit
                Left = 479
                Top = 41
                Width = 94
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                EditMask = '0000.00-0000;0;'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 12
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 1
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = 'HS'#48512#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
              end
              object edt_GoodsIMDCODE1: TsEdit
                Left = 322
                Top = 61
                Width = 385
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 2
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #54408#47785
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsSIZE1: TsEdit
                Left = 322
                Top = 81
                Width = 385
                Height = 19
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 70
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 3
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #44508#44201
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn38: TsBitBtn
                Tag = 401
                Left = 708
                Top = 61
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 18
                TabStop = False
                OnClick = sBitBtn21Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 2
                Images = DMICON.System18
              end
              object sBitBtn39: TsBitBtn
                Tag = 401
                Left = 708
                Top = 81
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 19
                TabStop = False
                OnClick = sBitBtn23Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 2
                Images = DMICON.System18
              end
              object curr_GoodsTOTQTY: TsCurrencyEdit
                Left = 640
                Top = 101
                Width = 101
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 10
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object curr_GoodsPRICEG: TsCurrencyEdit
                Left = 640
                Top = 121
                Width = 101
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 12
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object curr_GoodsSUPTOTAMT: TsCurrencyEdit
                Left = 640
                Top = 141
                Width = 101
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 14
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object sBitBtn40: TsBitBtn
                Tag = 313
                Left = 615
                Top = 141
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 20
                TabStop = False
                OnClick = sBitBtn6Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object sBitBtn41: TsBitBtn
                Tag = 312
                Left = 615
                Top = 121
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 21
                TabStop = False
                OnClick = sBitBtn6Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object sBitBtn42: TsBitBtn
                Tag = 311
                Left = 615
                Top = 101
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 22
                TabStop = False
                OnClick = sBitBtn6Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object edt_GoodsTOTQTYC: TsEdit
                Tag = 102
                Left = 575
                Top = 101
                Width = 39
                Height = 19
                Hint = #45800#50948
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 3
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 9
                OnDblClick = edt_CPCURRDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #49688#47049#49548#44228
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsPRICEC: TsEdit
                Tag = 102
                Left = 575
                Top = 121
                Width = 39
                Height = 19
                Hint = #45800#50948
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 3
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 11
                OnDblClick = edt_CPCURRDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #44592#51456#49688#47049
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsSUPTOTAMTC: TsEdit
                Tag = 102
                Left = 575
                Top = 141
                Width = 39
                Height = 19
                Hint = #53685#54868
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 3
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 13
                OnDblClick = edt_CPCURRDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #44552#50529#49548#44228
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object curr_GoodsQTY: TsCurrencyEdit
                Left = 387
                Top = 101
                Width = 100
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 5
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object curr_GoodsSUPAMT: TsCurrencyEdit
                Left = 387
                Top = 141
                Width = 100
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 8
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
              object sBitBtn43: TsBitBtn
                Tag = 310
                Left = 362
                Top = 141
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 23
                TabStop = False
                OnClick = sBitBtn6Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object edt_GoodsSUPAMTC: TsEdit
                Tag = 102
                Left = 322
                Top = 141
                Width = 39
                Height = 19
                Hint = #53685#54868
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 3
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 7
                OnDblClick = edt_CPCURRDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #44552#50529
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_GoodsQTYC: TsEdit
                Tag = 102
                Left = 322
                Top = 101
                Width = 39
                Height = 19
                Hint = #45800#50948
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 3
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 4
                OnDblClick = edt_CPCURRDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #49688#47049
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn44: TsBitBtn
                Tag = 309
                Left = 362
                Top = 101
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 24
                TabStop = False
                OnClick = sBitBtn6Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 25
                Images = DMICON.System18
              end
              object curr_GoodsPRICE: TsCurrencyEdit
                Left = 322
                Top = 121
                Width = 165
                Height = 19
                AutoSize = False
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 6
                BoundLabel.Active = True
                BoundLabel.Caption = #45800#44032
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
                GlyphMode.Blend = 0
                GlyphMode.Grayed = False
                DecimalPlaces = 4
                DisplayFormat = '#,0.####;0'
              end
            end
          end
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #49464#44552#44228#49328#49436
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object sSplitter8: TsSplitter
        Left = 0
        Top = 0
        Width = 1076
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter9: TsSplitter
        Left = 291
        Top = 2
        Width = 3
        Height = 769
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page3_Left: TsPanel
        Left = 0
        Top = 2
        Width = 291
        Height = 769
        Align = alLeft
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object page3_Right: TsPanel
        Left = 294
        Top = 2
        Width = 782
        Height = 769
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        object taxPanel2: TsPanel
          Left = 1
          Top = 201
          Width = 780
          Height = 328
          Align = alTop
          BevelOuter = bvNone
          PopupMenu = TaxMenu
          TabOrder = 1
          TabStop = True
          SkinData.SkinSection = 'TRANSPARENT'
          DesignSize = (
            780
            328)
          object sLabel3: TsLabel
            Left = 480
            Top = 23
            Width = 44
            Height = 13
            Caption = #50896#54868#44552#50529
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel4: TsLabel
            Left = 592
            Top = 23
            Width = 44
            Height = 13
            Caption = #50808#54868#44552#50529
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sSpeedButton6: TsSpeedButton
            Left = 384
            Top = 17
            Width = 10
            Height = 314
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'SPEEDBUTTON'
          end
          object sPageControl1: TsPageControl
            Left = 0
            Top = 0
            Width = 385
            Height = 351
            ActivePage = sTabSheet9
            TabIndex = 2
            TabOrder = 22
            SkinData.SkinSection = 'PAGECONTROL'
            object sTabSheet7: TsTabSheet
              Caption = #44277#44553#51088
              SkinData.CustomColor = False
              SkinData.CustomFont = False
              object edt_TaxSECODE: TsEdit
                Tag = 103
                Left = 64
                Top = 19
                Width = 50
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
                OnDblClick = edt_APPCODEDblClick
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #49345#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSEADDR2: TsEdit
                Left = 64
                Top = 99
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 5
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object mask_TaxSESAUPNO: TsMaskEdit
                Left = 64
                Top = 59
                Width = 91
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                EditMask = '999-99-99999;0;'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 12
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 3
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = #49324#50629#51088#48264#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
              end
              object edt_TaxSENAME: TsEdit
                Left = 64
                Top = 39
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 2
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #45824#54364#51088
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn30: TsBitBtn
                Tag = 103
                Left = 115
                Top = 19
                Width = 24
                Height = 19
                Cursor = crHandPoint
                TabOrder = 14
                TabStop = False
                OnClick = sBitBtn5Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 6
                Images = DMICON.System18
              end
              object edt_TaxSESNMAE: TsEdit
                Left = 140
                Top = 19
                Width = 209
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 1
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSEADDR1: TsEdit
                Left = 64
                Top = 79
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 4
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51452#49548
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSEADDR3: TsEdit
                Left = 64
                Top = 119
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 6
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSEELEC: TsEdit
                Left = 64
                Top = 139
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 7
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51204#51088#49436#47749
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSGUPTAI1_1: TsEdit
                Left = 64
                Top = 159
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 8
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #50629#53468
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSGUPTAI1_2: TsEdit
                Left = 64
                Top = 179
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 9
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSGUPTAI1_3: TsEdit
                Left = 64
                Top = 199
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 10
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxHNITEM1_1: TsEdit
                Left = 64
                Top = 219
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 11
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51333#47785
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxHNITEM1_2: TsEdit
                Left = 64
                Top = 239
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 12
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Caption = #51333#47785
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxHNITEM1_3: TsEdit
                Left = 64
                Top = 259
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 13
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Caption = #51333#47785
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
            end
            object sTabSheet8: TsTabSheet
              Caption = #44277#44553#48155#45716#51088
              SkinData.CustomColor = False
              SkinData.CustomFont = False
              object edt_TaxBYCODE: TsEdit
                Tag = 104
                Left = 64
                Top = 19
                Width = 50
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
                OnDblClick = edt_APPCODEDblClick
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #49345#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn16: TsBitBtn
                Tag = 104
                Left = 115
                Top = 19
                Width = 24
                Height = 19
                Cursor = crHandPoint
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                ParentFont = False
                TabOrder = 14
                TabStop = False
                OnClick = sBitBtn5Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 6
                Images = DMICON.System18
              end
              object edt_TaxBYSNAME: TsEdit
                Left = 140
                Top = 19
                Width = 209
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 1
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxBYNAME: TsEdit
                Left = 64
                Top = 39
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 2
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #45824#54364#51088
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object mask_TaxBYSAUPNO: TsMaskEdit
                Left = 64
                Top = 59
                Width = 91
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                EditMask = '999-99-99999;0;'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 12
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 3
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = #49324#50629#51088#48264#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
              end
              object edt_TaxBYADDR1: TsEdit
                Left = 64
                Top = 79
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = 5632767
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 4
                SkinData.CustomColor = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51452#49548
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxBYADDR2: TsEdit
                Left = 64
                Top = 99
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 5
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxBYADDR3: TsEdit
                Left = 64
                Top = 119
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 6
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxBYELEC: TsEdit
                Left = 64
                Top = 139
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 7
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51204#51088#49436#47749
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSGUPTAI2_1: TsEdit
                Left = 64
                Top = 159
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 8
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #50629#53468
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSGUPTAI2_2: TsEdit
                Left = 64
                Top = 179
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 9
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxSGUPTAI2_3: TsEdit
                Left = 64
                Top = 199
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 10
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxHNITEM2_1: TsEdit
                Left = 64
                Top = 219
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 11
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51333#47785
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxHNITEM2_2: TsEdit
                Left = 64
                Top = 239
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 12
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Caption = #51333#47785
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxHNITEM2_3: TsEdit
                Left = 64
                Top = 259
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 13
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Caption = #51333#47785
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
            end
            object sTabSheet9: TsTabSheet
              Caption = #49688#53441#51088
              SkinData.CustomColor = False
              SkinData.CustomFont = False
              object edt_TaxAGCODE: TsEdit
                Tag = 105
                Left = 64
                Top = 19
                Width = 50
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 0
                OnDblClick = edt_APPCODEDblClick
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #49345#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object sBitBtn17: TsBitBtn
                Tag = 105
                Left = 115
                Top = 19
                Width = 24
                Height = 21
                Cursor = crHandPoint
                TabOrder = 8
                TabStop = False
                OnClick = sBitBtn5Click
                SkinData.SkinSection = 'BUTTON'
                ImageIndex = 6
                Images = DMICON.System18
              end
              object edt_TaxAGSNAME: TsEdit
                Left = 140
                Top = 19
                Width = 209
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 1
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxAGNAME: TsEdit
                Left = 64
                Top = 41
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 2
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #45824#54364#51088
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object mask_TaxAGSAUPNO: TsMaskEdit
                Left = 64
                Top = 63
                Width = 91
                Height = 23
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                EditMask = '999-99-99999;0;'
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -13
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 12
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 3
                CheckOnExit = True
                BoundLabel.Active = True
                BoundLabel.Caption = #49324#50629#51088#48264#54840
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
                SkinData.SkinSection = 'EDIT'
              end
              object edt_TaxAGADDR1: TsEdit
                Left = 64
                Top = 85
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 4
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51452#49548
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = [fsBold]
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxAGADDR2: TsEdit
                Left = 64
                Top = 107
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 5
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxAGADDR3: TsEdit
                Left = 64
                Top = 129
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 6
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = DEFAULT_CHARSET
                BoundLabel.Font.Color = clWindowText
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = 'MS Sans Serif'
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
              object edt_TaxAGELEC: TsEdit
                Left = 64
                Top = 151
                Width = 285
                Height = 21
                HelpContext = 1
                CharCase = ecUpperCase
                Color = clWhite
                Ctl3D = False
                Font.Charset = ANSI_CHARSET
                Font.Color = clBlack
                Font.Height = -12
                Font.Name = #47569#51008' '#44256#46357
                Font.Style = []
                MaxLength = 35
                ParentCtl3D = False
                ParentFont = False
                TabOrder = 7
                SkinData.CustomFont = True
                SkinData.SkinSection = 'EDIT'
                BoundLabel.Active = True
                BoundLabel.Caption = #51204#51088#49436#47749
                BoundLabel.Indent = 0
                BoundLabel.Font.Charset = ANSI_CHARSET
                BoundLabel.Font.Color = clBlack
                BoundLabel.Font.Height = -11
                BoundLabel.Font.Name = #47569#51008' '#44256#46357
                BoundLabel.Font.Style = []
                BoundLabel.Layout = sclLeft
                BoundLabel.MaxWidth = 0
                BoundLabel.UseSkinColor = True
              end
            end
          end
          object sPanel29: TsPanel
            Left = 388
            Top = 0
            Width = 392
            Height = 19
            BevelOuter = bvNone
            Caption = #44208#51228#48169#48277
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 23
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object curr_TaxISSEXPAMT: TsCurrencyEdit
            Left = 448
            Top = 141
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 12
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#44553#44032#50529
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxPAIAMTU1: TsCurrencyEdit
            Left = 565
            Top = 37
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object sBitBtn12: TsBitBtn
            Tag = 314
            Left = 727
            Top = 37
            Width = 24
            Height = 19
            Cursor = crHandPoint
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 24
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object edt_TaxPAIAMTC1: TsEdit
            Tag = 102
            Left = 682
            Top = 37
            Width = 44
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object curr_TaxPAIAMT2: TsCurrencyEdit
            Left = 448
            Top = 57
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#54364
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxPAIAMTU2: TsCurrencyEdit
            Left = 565
            Top = 57
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TaxPAIAMTC2: TsEdit
            Tag = 102
            Left = 682
            Top = 57
            Width = 44
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn13: TsBitBtn
            Tag = 315
            Left = 727
            Top = 57
            Width = 24
            Height = 19
            Cursor = crHandPoint
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 25
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_TaxPAIAMT3: TsCurrencyEdit
            Left = 448
            Top = 77
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            BoundLabel.Active = True
            BoundLabel.Caption = #50612#51020
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxPAIAMTU3: TsCurrencyEdit
            Left = 565
            Top = 77
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TaxPAIAMTC3: TsEdit
            Tag = 102
            Left = 682
            Top = 77
            Width = 44
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn14: TsBitBtn
            Tag = 316
            Left = 727
            Top = 77
            Width = 24
            Height = 19
            Cursor = crHandPoint
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 26
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_TaxPAIAMT4: TsCurrencyEdit
            Left = 448
            Top = 97
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            BoundLabel.Active = True
            BoundLabel.Caption = #50808#49345
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxPAIAMTU4: TsCurrencyEdit
            Left = 565
            Top = 97
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TaxPAIAMTC4: TsEdit
            Tag = 102
            Left = 682
            Top = 97
            Width = 44
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 11
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn15: TsBitBtn
            Tag = 317
            Left = 727
            Top = 97
            Width = 24
            Height = 19
            Cursor = crHandPoint
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 27
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object sPanel10: TsPanel
            Left = 388
            Top = 118
            Width = 392
            Height = 21
            BevelOuter = bvNone
            Caption = #44552#50529' '#48143' '#49688#47049
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 28
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object curr_TaxPAIAMT1: TsCurrencyEdit
            Left = 448
            Top = 37
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            BoundLabel.Active = True
            BoundLabel.Caption = #54788#44552
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxISSEXPAMTU: TsCurrencyEdit
            Left = 448
            Top = 161
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 13
            BoundLabel.Active = True
            BoundLabel.Caption = #49464#50529
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxTOTCNT: TsCurrencyEdit
            Left = 448
            Top = 181
            Width = 116
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 14
            BoundLabel.Active = True
            BoundLabel.Caption = #52509#49688#47049
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TaxVBGUBUN: TsEdit
            Tag = 102
            Left = 682
            Top = 141
            Width = 44
            Height = 19
            Hint = #50689#49688'/'#52397#44396
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 16
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #50689#49688'/'#52397#44396#44396#48516
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn18: TsBitBtn
            Tag = 318
            Left = 727
            Top = 141
            Width = 24
            Height = 19
            Cursor = crHandPoint
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 29
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object edt_TaxTOTCNTC: TsEdit
            Tag = 102
            Left = 565
            Top = 181
            Width = 44
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 15
            OnDblClick = edt_CPCURRDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn21: TsBitBtn
            Tag = 319
            Left = 610
            Top = 181
            Width = 24
            Height = 19
            Cursor = crHandPoint
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentFont = False
            TabOrder = 30
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object sPanel12: TsPanel
            Left = 388
            Top = 202
            Width = 392
            Height = 19
            BevelOuter = bvNone
            Caption = #48708#44256
            Color = 16042877
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 31
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object edt_TaxREMARK1_1: TsEdit
            Left = 448
            Top = 222
            Width = 304
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 17
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxREMARK1_2: TsEdit
            Left = 448
            Top = 242
            Width = 304
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 18
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxREMARK1_3: TsEdit
            Left = 448
            Top = 262
            Width = 304
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 19
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxREMARK1_4: TsEdit
            Left = 448
            Top = 282
            Width = 304
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 20
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxREMARK1_5: TsEdit
            Left = 448
            Top = 302
            Width = 304
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 21
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxVBDETAILNO: TsEdit
            Tag = 102
            Left = 682
            Top = 161
            Width = 69
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 32
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#46976#49688
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
        end
        object taxDbPanel: TsPanel
          Left = 1
          Top = 1
          Width = 780
          Height = 113
          Align = alTop
          BevelOuter = bvNone
          PopupMenu = TaxMenu
          TabOrder = 3
          SkinData.SkinSection = 'TRANSPARENT'
          object sDBGrid5: TsDBGrid
            Left = 0
            Top = 25
            Width = 780
            Height = 88
            Align = alTop
            Color = clWhite
            DataSource = dsTax
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            PopupMenu = TaxMenu
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clBlack
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            SkinData.SkinSection = 'EDIT'
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'SEQ'
                Title.Alignment = taCenter
                Title.Caption = #49692#48264
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DOC_NO'
                Title.Alignment = taCenter
                Title.Caption = #47928#49436#48264#54840
                Width = 170
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ISS_DATE'
                Title.Alignment = taCenter
                Title.Caption = #51089#49457#51068#51088
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SE_SNAME'
                Title.Alignment = taCenter
                Title.Caption = #44277#44553#51088
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BY_SNAME'
                Title.Alignment = taCenter
                Title.Caption = #44277#44553#48155#45716#51088
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ISS_EXPAMT'
                Title.Alignment = taCenter
                Title.Caption = #44277#44553#44032#50529
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ISS_EXPAMTU'
                Title.Alignment = taCenter
                Title.Caption = #49352#50529
                Width = 100
                Visible = True
              end>
          end
          object taxBtnPanel: TsPanel
            Left = 0
            Top = 0
            Width = 780
            Height = 25
            Align = alTop
            TabOrder = 1
            SkinData.SkinSection = 'TRANSPARENT'
            object sSpeedButton34: TsSpeedButton
              Left = 1
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object sSpeedButton35: TsSpeedButton
              Left = 73
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_taxDel: TsSpeedButton
              Tag = 3
              Left = 150
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #49325#51228
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 1
              Images = DMICON.System18
            end
            object sSpeedButton37: TsSpeedButton
              Left = 145
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_taxMod: TsSpeedButton
              Tag = 2
              Left = 78
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #49688#51221
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 3
              Images = DMICON.System18
            end
            object sSpeedButton39: TsSpeedButton
              Left = 217
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_taxSave: TsSpeedButton
              Tag = 4
              Left = 222
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #51200#51109
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 17
              Images = DMICON.System18
            end
            object btn_taxCancel: TsSpeedButton
              Tag = 5
              Left = 289
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #52712#49548
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 18
              Images = DMICON.System18
            end
            object sSpeedButton42: TsSpeedButton
              Left = 356
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_taxNew: TsSpeedButton
              Tag = 1
              Left = 6
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #51077#47141
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 2
              Images = DMICON.System18
            end
          end
        end
        object taxPanel1: TsPanel
          Left = 1
          Top = 114
          Width = 780
          Height = 87
          Align = alTop
          BevelOuter = bvNone
          PopupMenu = TaxMenu
          TabOrder = 0
          TabStop = True
          SkinData.SkinSection = 'TRANSPARENT'
          object sPanel52: TsPanel
            Left = 0
            Top = 2
            Width = 780
            Height = 19
            BevelOuter = bvNone
            Caption = #44592#48376#51077#47141#49324#54637
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 7
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object edt_TaxVBMAINTNO: TsEdit
            Left = 112
            Top = 63
            Width = 251
            Height = 19
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44288#47532#48264#54840
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn45: TsBitBtn
            Tag = 102
            Left = 338
            Top = 23
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 8
            TabStop = False
            OnClick = sBitBtn45Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object edt_TaxDOCNO: TsEdit
            Left = 112
            Top = 23
            Width = 225
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #47928#49436#48264#54840
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object mask_TaxISSDATE: TsMaskEdit
            Left = 480
            Top = 23
            Width = 94
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #51089#49457#51068
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_TaxVBRENO: TsEdit
            Left = 112
            Top = 43
            Width = 73
            Height = 19
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44428'/'#54840'/'#51068#47144#48264#54840
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxVBSENO: TsEdit
            Left = 186
            Top = 43
            Width = 73
            Height = 19
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxVBFSNO: TsEdit
            Left = 260
            Top = 43
            Width = 103
            Height = 19
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxVBDMNO: TsEdit
            Left = 480
            Top = 43
            Width = 249
            Height = 19
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #52280#51312#48264#54840
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
        end
        object taxPanel3: TsPanel
          Left = 1
          Top = 529
          Width = 780
          Height = 239
          Align = alClient
          BevelOuter = bvNone
          PopupMenu = TaxItemMenu
          TabOrder = 2
          TabStop = True
          SkinData.SkinSection = 'TRANSPARENT'
          object sSplitter7: TsSplitter
            Left = 0
            Top = 39
            Width = 780
            Height = 2
            Cursor = crVSplit
            Align = alTop
            AutoSnap = False
            Enabled = False
            SkinData.SkinSection = 'SPLITTER'
          end
          object sPanel14: TsPanel
            Left = 0
            Top = 0
            Width = 780
            Height = 21
            Align = alTop
            BevelOuter = bvNone
            Caption = #54408#47785#45236#50669
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 25
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object taxItemBtnPanel: TsPanel
            Left = 0
            Top = 21
            Width = 780
            Height = 18
            Align = alTop
            TabOrder = 26
            SkinData.SkinSection = 'TRANSPARENT'
            object sSpeedButton7: TsSpeedButton
              Left = 1
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object sSpeedButton9: TsSpeedButton
              Left = 73
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_taxItemDel: TsSpeedButton
              Tag = 3
              Left = 150
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #49325#51228
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 1
              Images = DMICON.System18
            end
            object sSpeedButton44: TsSpeedButton
              Left = 145
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_taxItemMod: TsSpeedButton
              Tag = 2
              Left = 78
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #49688#51221
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 3
              Images = DMICON.System18
            end
            object sSpeedButton46: TsSpeedButton
              Left = 217
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_taxItemSave: TsSpeedButton
              Tag = 4
              Left = 222
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #51200#51109
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 17
              Images = DMICON.System18
            end
            object btn_taxItemCancel: TsSpeedButton
              Tag = 5
              Left = 289
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #52712#49548
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 18
              Images = DMICON.System18
            end
            object sSpeedButton49: TsSpeedButton
              Left = 356
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_taxItemNew: TsSpeedButton
              Tag = 1
              Left = 6
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #51077#47141
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_taxItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 2
              Images = DMICON.System18
            end
          end
          object sDBGrid2: TsDBGrid
            Left = 0
            Top = 41
            Width = 207
            Height = 198
            Align = alLeft
            Color = clWhite
            DataSource = dsTaxItem
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            PopupMenu = TaxItemMenu
            TabOrder = 27
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clBlack
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            SkinData.SkinSection = 'EDIT'
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'LINE_NO'
                Title.Alignment = taCenter
                Title.Caption = #51068#47144#48264#54840
                Width = 86
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'DE_DATE'
                Title.Alignment = taCenter
                Title.Caption = #44277#44553#51068#51088
                Width = 115
                Visible = True
              end>
          end
          object edt_TaxLINENO: TsEdit
            Left = 288
            Top = 51
            Width = 155
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 8
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #51068#47144#48264#54840
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object mask_TaxDEDATE: TsMaskEdit
            Left = 504
            Top = 51
            Width = 94
            Height = 19
            AutoSize = False
            Color = clWhite
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#44553#51068#51088
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_TaxSIZE1: TsEdit
            Left = 288
            Top = 91
            Width = 449
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44508#44201
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn23: TsBitBtn
            Tag = 402
            Left = 738
            Top = 71
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 28
            TabStop = False
            OnClick = sBitBtn21Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 2
            Images = DMICON.System18
          end
          object sBitBtn24: TsBitBtn
            Tag = 402
            Left = 738
            Top = 91
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 29
            TabStop = False
            OnClick = sBitBtn23Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 2
            Images = DMICON.System18
          end
          object edt_TaxIMDCODE1: TsEdit
            Left = 288
            Top = 71
            Width = 449
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #54408#47785
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxQTYC: TsEdit
            Tag = 102
            Left = 288
            Top = 111
            Width = 39
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#47049
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn25: TsBitBtn
            Tag = 320
            Left = 328
            Top = 111
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 30
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object edt_TaxTOTQTYC: TsEdit
            Tag = 102
            Left = 501
            Top = 111
            Width = 39
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 13
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49548#44228
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn26: TsBitBtn
            Tag = 324
            Left = 541
            Top = 111
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 31
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_TaxTOTQTY: TsCurrencyEdit
            Left = 566
            Top = 111
            Width = 74
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 14
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxPRICEG: TsCurrencyEdit
            Left = 566
            Top = 131
            Width = 74
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 16
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxSUPTOTAMT: TsCurrencyEdit
            Tag = 102
            Left = 566
            Top = 151
            Width = 74
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 18
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object sBitBtn27: TsBitBtn
            Tag = 326
            Left = 541
            Top = 151
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 32
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object sBitBtn29: TsBitBtn
            Tag = 325
            Left = 541
            Top = 131
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 33
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object edt_TaxPRICEC: TsEdit
            Tag = 325
            Left = 501
            Top = 131
            Width = 39
            Height = 19
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 15
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44592#51456#49688#47049
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxSUPTOTAMTC: TsEdit
            Tag = 326
            Left = 501
            Top = 151
            Width = 39
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 17
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49548#44228
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object curr_TaxQTY: TsCurrencyEdit
            Left = 353
            Top = 111
            Width = 90
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxPRICE: TsCurrencyEdit
            Left = 288
            Top = 131
            Width = 155
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            BoundLabel.Active = True
            BoundLabel.Caption = #45800#44032
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TaxSUPAMTC: TsEdit
            Tag = 321
            Left = 288
            Top = 151
            Width = 39
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#44553#44032#50529
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn47: TsBitBtn
            Tag = 321
            Left = 328
            Top = 151
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 34
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_TaxSUPAMT: TsCurrencyEdit
            Tag = 102
            Left = 353
            Top = 151
            Width = 90
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TaxVBAMTC: TsEdit
            Tag = 102
            Left = 288
            Top = 171
            Width = 39
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            OnDblClick = edt_CPCURRDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#44553#44032#50529#50808#54868
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn46: TsBitBtn
            Tag = 322
            Left = 328
            Top = 171
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 35
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_TaxVBAMT: TsCurrencyEdit
            Left = 353
            Top = 171
            Width = 90
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_VBTOTAMTC: TsEdit
            Tag = 327
            Left = 501
            Top = 171
            Width = 39
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 19
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49548#44228
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn48: TsBitBtn
            Tag = 327
            Left = 541
            Top = 171
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 36
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_VBTOTAMT: TsCurrencyEdit
            Left = 566
            Top = 171
            Width = 74
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 20
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TaxVBTAXC: TsEdit
            Tag = 102
            Left = 288
            Top = 191
            Width = 39
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 11
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49464#50529
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn49: TsBitBtn
            Tag = 323
            Left = 328
            Top = 191
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 37
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_TaxVBTAX: TsCurrencyEdit
            Left = 353
            Top = 191
            Width = 90
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 12
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TaxVBTOTTAXC: TsEdit
            Tag = 328
            Left = 501
            Top = 191
            Width = 39
            Height = 19
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 21
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49548#44228
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn50: TsBitBtn
            Tag = 328
            Left = 541
            Top = 191
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 38
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_TaxVBTOTTAX: TsCurrencyEdit
            Left = 566
            Top = 191
            Width = 74
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 22
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_TaxCUXRATE: TsCurrencyEdit
            Left = 678
            Top = 111
            Width = 84
            Height = 19
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 23
            BoundLabel.Active = True
            BoundLabel.Caption = #54872#50984
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_TaxBIGO1: TsEdit
            Left = 288
            Top = 211
            Width = 449
            Height = 19
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 24
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #52280#51312#49324#54637
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = #47569#51008#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn51: TsBitBtn
            Tag = 402
            Left = 738
            Top = 211
            Width = 24
            Height = 19
            Cursor = crHandPoint
            TabOrder = 39
            TabStop = False
            OnClick = sBitBtn51Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 2
            Images = DMICON.System18
          end
        end
        object TaxBIGOPanel: TsPanel
          Left = 270
          Top = 1000
          Width = 489
          Height = 185
          TabOrder = 4
          SkinData.SkinSection = 'PANEL'
          object edt_Panel_TaxBIGO1: TsEdit
            Left = 62
            Top = 24
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #52280#51312#49324#54637
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxBIGO2: TsEdit
            Left = 62
            Top = 48
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxBIGO3: TsEdit
            Left = 62
            Top = 72
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxBIGO4: TsEdit
            Left = 62
            Top = 96
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sButton8: TsButton
            Left = 174
            Top = 152
            Width = 87
            Height = 23
            Caption = #54869#51064
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
            OnClick = sButton8Click
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 17
            Reflected = True
          end
          object sButton9: TsButton
            Tag = 1
            Left = 262
            Top = 152
            Width = 87
            Height = 23
            Caption = #52712#49548
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 6
            OnClick = sButton8Click
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 18
            Reflected = True
          end
          object edt_TaxBIGO5: TsEdit
            Left = 62
            Top = 120
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
        end
        object TaxSIZEPanel: TsPanel
          Left = 296
          Top = 1000
          Width = 465
          Height = 305
          Caption = 'TaxSIZEPanel'
          TabOrder = 5
          SkinData.SkinSection = 'PANEL'
          object edt_Panel_TaxSIZE1: TsEdit
            Left = 46
            Top = 16
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44508#44201
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxSIZE2: TsEdit
            Left = 46
            Top = 40
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxSIZE3: TsEdit
            Left = 46
            Top = 64
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxSIZE4: TsEdit
            Left = 46
            Top = 88
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sButton6: TsButton
            Tag = 402
            Left = 142
            Top = 264
            Width = 87
            Height = 23
            Caption = #54869#51064
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 10
            OnClick = sButton2Click
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 17
            Reflected = True
          end
          object sButton7: TsButton
            Tag = 402
            Left = 230
            Top = 264
            Width = 87
            Height = 23
            Caption = #52712#49548
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 11
            OnClick = sButton3Click
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 18
            Reflected = True
          end
          object edt_TaxSIZE5: TsEdit
            Left = 46
            Top = 112
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxSIZE6: TsEdit
            Left = 46
            Top = 136
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxSIZE7: TsEdit
            Left = 46
            Top = 160
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxSIZE10: TsEdit
            Left = 46
            Top = 232
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxSIZE9: TsEdit
            Left = 46
            Top = 208
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxSIZE8: TsEdit
            Left = 46
            Top = 184
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
        end
        object TaxIMDCODEPanel: TsPanel
          Left = 288
          Top = 1000
          Width = 464
          Height = 185
          TabOrder = 6
          SkinData.SkinSection = 'PANEL'
          object edt_Panel_TaxIMDCODE1: TsEdit
            Left = 46
            Top = 24
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #54408#47785
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxIMDCODE2: TsEdit
            Left = 46
            Top = 48
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxIMDCODE3: TsEdit
            Left = 46
            Top = 72
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_TaxIMDCODE4: TsEdit
            Left = 46
            Top = 96
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sButton4: TsButton
            Tag = 402
            Left = 158
            Top = 144
            Width = 87
            Height = 23
            Caption = #54869#51064
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            OnClick = imdcodeOkClick
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 17
            Reflected = True
          end
          object sButton5: TsButton
            Tag = 402
            Left = 246
            Top = 144
            Width = 87
            Height = 23
            Caption = #52712#49548
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
            OnClick = sButton1Click
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 18
            Reflected = True
          end
        end
      end
    end
    object sTabSheet4: TsTabSheet
      Caption = #49345#50629#49569#51109
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object sSplitter11: TsSplitter
        Left = 0
        Top = 0
        Width = 1076
        Height = 2
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sSplitter12: TsSplitter
        Left = 291
        Top = 2
        Width = 3
        Height = 769
        Cursor = crHSplit
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page4_Right: TsPanel
        Left = 294
        Top = 2
        Width = 782
        Height = 769
        Align = alClient
        PopupMenu = invMenu
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object InvDbPanel: TsPanel
          Left = 1
          Top = 1
          Width = 780
          Height = 144
          Align = alTop
          BevelOuter = bvNone
          PopupMenu = invMenu
          TabOrder = 3
          SkinData.SkinSection = 'TRANSPARENT'
          object sDBGrid6: TsDBGrid
            Left = 0
            Top = 25
            Width = 780
            Height = 119
            Align = alTop
            Color = clWhite
            DataSource = dsInv
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            PopupMenu = invMenu
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clBlack
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            SkinData.SkinSection = 'EDIT'
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'SEQ'
                Title.Alignment = taCenter
                Title.Caption = #49692#48264
                Width = 74
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DOC_NO'
                Title.Alignment = taCenter
                Title.Caption = #47928#49436#48264#54840
                Width = 184
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ISS_DATE'
                Title.Alignment = taCenter
                Title.Caption = #48156#44553#51068#51088
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SE_SNAME'
                Title.Alignment = taCenter
                Title.Caption = #49688#52636#51088
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'BY_SNAME'
                Title.Alignment = taCenter
                Title.Caption = #49688#51077#51088
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ISS_TOTAMT'
                Title.Alignment = taCenter
                Title.Caption = #52509#44552#50529
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TOTCNT'
                Title.Alignment = taCenter
                Title.Caption = #52509#49688#47049
                Width = 91
                Visible = True
              end>
          end
          object invBtnPanel: TsPanel
            Left = 0
            Top = 0
            Width = 780
            Height = 25
            Align = alTop
            TabOrder = 1
            SkinData.SkinSection = 'TRANSPARENT'
            object sSpeedButton51: TsSpeedButton
              Left = 1
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object sSpeedButton52: TsSpeedButton
              Left = 73
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_invDel: TsSpeedButton
              Tag = 3
              Left = 150
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #49325#51228
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 1
              Images = DMICON.System18
            end
            object sSpeedButton54: TsSpeedButton
              Left = 145
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_invMod: TsSpeedButton
              Tag = 2
              Left = 78
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #49688#51221
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 3
              Images = DMICON.System18
            end
            object sSpeedButton56: TsSpeedButton
              Left = 217
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_invSave: TsSpeedButton
              Tag = 4
              Left = 222
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #51200#51109
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 17
              Images = DMICON.System18
            end
            object btn_invCancel: TsSpeedButton
              Tag = 5
              Left = 289
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #52712#49548
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 18
              Images = DMICON.System18
            end
            object sSpeedButton59: TsSpeedButton
              Left = 356
              Top = 1
              Width = 5
              Height = 23
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_invNew: TsSpeedButton
              Tag = 1
              Left = 6
              Top = 1
              Width = 67
              Height = 23
              Cursor = crHandPoint
              Caption = #51077#47141
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 2
              Images = DMICON.System18
            end
          end
        end
        object invPanel2: TsPanel
          Left = 1
          Top = 293
          Width = 780
          Height = 172
          Align = alTop
          BevelOuter = bvNone
          PopupMenu = invMenu
          TabOrder = 1
          TabStop = True
          SkinData.SkinSection = 'TRANSPARENT'
          DesignSize = (
            780
            172)
          object sSpeedButton61: TsSpeedButton
            Left = 383
            Top = 9
            Width = 10
            Height = 168
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'SPEEDBUTTON'
          end
          object sPanel23: TsPanel
            Left = 381
            Top = 2
            Width = 399
            Height = 24
            BevelOuter = bvNone
            Caption = #44277#44553#48155#45716#51088'('#49688#51077#51088')'
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 12
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sPanel24: TsPanel
            Left = 0
            Top = 2
            Width = 385
            Height = 24
            BevelOuter = bvNone
            Caption = #47932#54408#44277#44553#51088'('#49688#52636#51088')'
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 13
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object edt_InvSECODE: TsEdit
            Tag = 106
            Left = 112
            Top = 34
            Width = 80
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            OnClick = edt_APPCODEDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49345#54840
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSESNAME: TsEdit
            Left = 112
            Top = 56
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSENAME: TsEdit
            Left = 112
            Top = 78
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#54364#51088
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSEADDR1: TsEdit
            Left = 112
            Top = 100
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #51452#49548
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSEADDR3: TsEdit
            Left = 112
            Top = 144
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSEADDR2: TsEdit
            Left = 112
            Top = 122
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn52: TsBitBtn
            Tag = 106
            Left = 193
            Top = 34
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 14
            TabStop = False
            OnClick = sBitBtn5Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 6
            Images = DMICON.System18
          end
          object edt_InvBYCODE: TsEdit
            Tag = 107
            Left = 480
            Top = 34
            Width = 80
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            OnClick = edt_APPCODEDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49345#54840
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn53: TsBitBtn
            Tag = 107
            Left = 561
            Top = 34
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 15
            TabStop = False
            OnClick = sBitBtn5Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 6
            Images = DMICON.System18
          end
          object edt_InvBYNAME: TsEdit
            Left = 480
            Top = 78
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #45824#54364#51088
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvBYSNAME: TsEdit
            Left = 480
            Top = 56
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvBYADDR1: TsEdit
            Left = 480
            Top = 100
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #51452#49548
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvBYADDR2: TsEdit
            Left = 480
            Top = 122
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvBYADDR3: TsEdit
            Left = 480
            Top = 144
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 11
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
        end
        object invPanel1: TsPanel
          Left = 1
          Top = 145
          Width = 780
          Height = 148
          Align = alTop
          BevelOuter = bvNone
          PopupMenu = invMenu
          TabOrder = 0
          TabStop = True
          SkinData.SkinSection = 'TRANSPARENT'
          DesignSize = (
            780
            148)
          object sSpeedButton62: TsSpeedButton
            Left = 383
            Top = 9
            Width = 10
            Height = 144
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'SPEEDBUTTON'
          end
          object sPanel26: TsPanel
            Left = 385
            Top = 2
            Width = 395
            Height = 24
            BevelOuter = bvNone
            Caption = #48708#44256
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 11
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sPanel27: TsPanel
            Left = 0
            Top = 2
            Width = 385
            Height = 24
            BevelOuter = bvNone
            Caption = #49345#50629#49569#51109#51613#47749#49436
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 12
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object edt_InvREMARK1_1: TsEdit
            Left = 480
            Top = 34
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #54637#52264
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvREMARK1_2: TsEdit
            Left = 480
            Top = 56
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#47749
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvREMARK1_3: TsEdit
            Left = 480
            Top = 78
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49440#51201#51648
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvREMARK1_4: TsEdit
            Left = 480
            Top = 100
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #46020#52265#51648
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvREMARK1_5: TsEdit
            Left = 480
            Top = 122
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #51064#46020#51312#44148
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object mask_InvISSDATE: TsMaskEdit
            Left = 112
            Top = 56
            Width = 113
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            EditMask = '9999-99-99;0'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Text = '20161122'
            CheckOnExit = True
            BoundLabel.Active = True
            BoundLabel.Caption = #48156#44553#51068#51088
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.CustomColor = True
            SkinData.CustomFont = True
            SkinData.SkinSection = 'EDIT'
          end
          object edt_InvDOCNO: TsEdit
            Left = 112
            Top = 34
            Width = 250
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #47928#49436#48264#54840
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object curr_InvTOTCNT: TsCurrencyEdit
            Left = 112
            Top = 78
            Width = 113
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            BoundLabel.Active = True
            BoundLabel.Caption = #52509#49688#47049
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_InvTOTCNTC: TsEdit
            Tag = 102
            Left = 226
            Top = 78
            Width = 44
            Height = 21
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn54: TsBitBtn
            Tag = 329
            Left = 271
            Top = 78
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 13
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_InvISSTOTAMT: TsCurrencyEdit
            Left = 112
            Top = 100
            Width = 113
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            BoundLabel.Active = True
            BoundLabel.Caption = #52509#44552#50529
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = [fsBold]
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_InvISSTOTAMTC: TsEdit
            Tag = 102
            Left = 226
            Top = 100
            Width = 44
            Height = 21
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            OnDblClick = edt_CPCURRDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn55: TsBitBtn
            Tag = 330
            Left = 271
            Top = 100
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 14
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
        end
        object invPanel3: TsPanel
          Left = 1
          Top = 465
          Width = 780
          Height = 303
          Align = alClient
          BevelOuter = bvNone
          PopupMenu = invItemMenu
          TabOrder = 2
          TabStop = True
          SkinData.SkinSection = 'TRANSPARENT'
          object sSplitter13: TsSplitter
            Left = 0
            Top = 42
            Width = 780
            Height = 2
            Cursor = crVSplit
            Align = alTop
            AutoSnap = False
            Enabled = False
            SkinData.SkinSection = 'SPLITTER'
          end
          object sPanel54: TsPanel
            Left = 0
            Top = 0
            Width = 780
            Height = 24
            Align = alTop
            BevelOuter = bvNone
            Caption = #54408#47785#45236#50669
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 14
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object invItemBtnPanel: TsPanel
            Left = 0
            Top = 24
            Width = 780
            Height = 18
            Align = alTop
            TabOrder = 15
            SkinData.SkinSection = 'TRANSPARENT'
            object sSpeedButton63: TsSpeedButton
              Left = 1
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object sSpeedButton64: TsSpeedButton
              Left = 73
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_invItemDel: TsSpeedButton
              Tag = 3
              Left = 150
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #49325#51228
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 1
              Images = DMICON.System18
            end
            object sSpeedButton66: TsSpeedButton
              Left = 145
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_invItemMod: TsSpeedButton
              Tag = 2
              Left = 78
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #49688#51221
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 3
              Images = DMICON.System18
            end
            object sSpeedButton68: TsSpeedButton
              Left = 217
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_invItemSave: TsSpeedButton
              Tag = 4
              Left = 222
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #51200#51109
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 17
              Images = DMICON.System18
            end
            object btn_invItemCancel: TsSpeedButton
              Tag = 5
              Left = 289
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #52712#49548
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 18
              Images = DMICON.System18
            end
            object sSpeedButton71: TsSpeedButton
              Left = 356
              Top = 1
              Width = 5
              Height = 16
              Cursor = crHandPoint
              Layout = blGlyphTop
              Spacing = 0
              Align = alLeft
              ButtonStyle = tbsDivider
              SkinData.SkinSection = 'TRANSPARENT'
              Reflected = True
            end
            object btn_invItemNew: TsSpeedButton
              Tag = 1
              Left = 6
              Top = 1
              Width = 67
              Height = 16
              Cursor = crHandPoint
              Caption = #51077#47141
              Enabled = False
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = #44404#47548#52404
              Font.Style = [fsBold]
              ParentFont = False
              OnClick = btn_invItemNewClick
              Align = alLeft
              SkinData.CustomFont = True
              SkinData.SkinSection = 'TRANSPARENT'
              ImageIndex = 2
              Images = DMICON.System18
            end
          end
          object sDBGrid7: TsDBGrid
            Left = 0
            Top = 44
            Width = 780
            Height = 101
            Align = alTop
            Color = clWhite
            DataSource = dsInvItem
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            PopupMenu = invItemMenu
            TabOrder = 16
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clBlack
            TitleFont.Height = -12
            TitleFont.Name = #47569#51008' '#44256#46357
            TitleFont.Style = []
            SkinData.SkinSection = 'EDIT'
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'LINE_NO'
                Title.Alignment = taCenter
                Title.Caption = #51068#47144#48264#54840
                Width = 55
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IMD_CODE1'
                Title.Alignment = taCenter
                Title.Caption = #54408#47785
                Width = 243
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SIZE1'
                Title.Alignment = taCenter
                Title.Caption = #44508#44201
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTY'
                Title.Alignment = taCenter
                Title.Caption = #49688#47049
                Width = 55
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRICE'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'SUP_AMT'
                Title.Alignment = taCenter
                Title.Caption = #44277#44553#44032#50529'('#50808#54868')'
                Width = 100
                Visible = True
              end>
          end
          object edt_InvLINENO: TsEdit
            Left = 200
            Top = 157
            Width = 155
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 8
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #51068#47144#48264#54840
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSIZE1: TsEdit
            Left = 200
            Top = 201
            Width = 359
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44508#44201
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn56: TsBitBtn
            Tag = 403
            Left = 560
            Top = 179
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 17
            TabStop = False
            OnClick = sBitBtn21Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 2
            Images = DMICON.System18
          end
          object sBitBtn57: TsBitBtn
            Tag = 403
            Left = 560
            Top = 201
            Width = 24
            Height = 23
            Cursor = crHandPoint
            TabOrder = 18
            TabStop = False
            OnClick = sBitBtn23Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 2
            Images = DMICON.System18
          end
          object edt_InvIMDCODE1: TsEdit
            Left = 200
            Top = 179
            Width = 359
            Height = 21
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #54408#47785
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvQTYC: TsEdit
            Tag = 102
            Left = 200
            Top = 223
            Width = 39
            Height = 21
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#47049
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn58: TsBitBtn
            Tag = 331
            Left = 240
            Top = 223
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 19
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object edt_InvTOTQTYC: TsEdit
            Tag = 102
            Left = 445
            Top = 223
            Width = 39
            Height = 21
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #49688#47049#49548#44228
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn59: TsBitBtn
            Tag = 333
            Left = 485
            Top = 223
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 20
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_InvTOTQTY: TsCurrencyEdit
            Left = 510
            Top = 223
            Width = 74
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_InvPRICEG: TsCurrencyEdit
            Left = 510
            Top = 245
            Width = 74
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 11
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object sBitBtn61: TsBitBtn
            Tag = 334
            Left = 485
            Top = 245
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 21
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object edt_InvPRICEC: TsEdit
            Tag = 102
            Left = 445
            Top = 245
            Width = 39
            Height = 21
            Hint = #45800#50948
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44592#51456#49688#47049
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object curr_InvQTY: TsCurrencyEdit
            Left = 265
            Top = 223
            Width = 90
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object curr_InvPRICE: TsCurrencyEdit
            Left = 200
            Top = 245
            Width = 155
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            BoundLabel.Active = True
            BoundLabel.Caption = #45800#44032
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_InvSUPAMTC: TsEdit
            Tag = 102
            Left = 200
            Top = 267
            Width = 39
            Height = 21
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            OnDblClick = edt_CPCURRDblClick
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44277#44553#44032#50529#50808#54868
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn63: TsBitBtn
            Tag = 332
            Left = 240
            Top = 267
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 22
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_InvSUPAMT: TsCurrencyEdit
            Left = 265
            Top = 267
            Width = 90
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = 5632767
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.CustomColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
          object edt_InvSUPTOTAMTC: TsEdit
            Tag = 102
            Left = 445
            Top = 267
            Width = 39
            Height = 21
            Hint = #53685#54868
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 12
            OnDblClick = edt_CPCURRDblClick
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44552#50529#49548#44228
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sBitBtn64: TsBitBtn
            Tag = 335
            Left = 485
            Top = 267
            Width = 24
            Height = 21
            Cursor = crHandPoint
            TabOrder = 23
            TabStop = False
            OnClick = sBitBtn6Click
            SkinData.SkinSection = 'BUTTON'
            ImageIndex = 25
            Images = DMICON.System18
          end
          object curr_InvSUPTOTAMT: TsCurrencyEdit
            Left = 510
            Top = 267
            Width = 74
            Height = 21
            AutoSize = False
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 13
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Blend = 0
            GlyphMode.Grayed = False
            DecimalPlaces = 4
            DisplayFormat = '#,0.####;0'
          end
        end
        object InvIMCODEPanel: TsPanel
          Left = 165
          Top = 1000
          Width = 464
          Height = 185
          TabOrder = 4
          SkinData.SkinSection = 'PANEL'
          object edt_Panel_InvIMDCODE1: TsEdit
            Left = 46
            Top = 24
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #54408#47785
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvIMDCODE2: TsEdit
            Left = 46
            Top = 48
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvIMDCODE3: TsEdit
            Left = 46
            Top = 72
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvIMDCODE4: TsEdit
            Left = 46
            Top = 96
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 35
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sButton10: TsButton
            Tag = 403
            Left = 158
            Top = 144
            Width = 87
            Height = 23
            Caption = #54869#51064
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 4
            OnClick = imdcodeOkClick
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 17
            Reflected = True
          end
          object sButton11: TsButton
            Tag = 403
            Left = 246
            Top = 144
            Width = 87
            Height = 23
            Caption = #52712#49548
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 5
            OnClick = sButton1Click
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 18
            Reflected = True
          end
        end
        object InvSIZEPanel: TsPanel
          Left = 160
          Top = 1000
          Width = 465
          Height = 305
          Caption = 'TaxSIZEPanel'
          TabOrder = 5
          SkinData.SkinSection = 'PANEL'
          object edt_Panel_InvSIZE1: TsEdit
            Left = 46
            Top = 16
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Active = True
            BoundLabel.Caption = #44508#44201
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSIZE2: TsEdit
            Left = 46
            Top = 40
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSIZE3: TsEdit
            Left = 46
            Top = 64
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSIZE4: TsEdit
            Left = 46
            Top = 88
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object sButton12: TsButton
            Tag = 403
            Left = 142
            Top = 264
            Width = 87
            Height = 23
            Caption = #54869#51064
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 10
            OnClick = sButton2Click
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 17
            Reflected = True
          end
          object sButton13: TsButton
            Tag = 403
            Left = 230
            Top = 264
            Width = 87
            Height = 23
            Caption = #52712#49548
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 11
            OnClick = sButton3Click
            SkinData.SkinSection = 'BUTTON'
            Images = DMICON.System18
            ImageIndex = 18
            Reflected = True
          end
          object edt_InvSIZE5: TsEdit
            Left = 46
            Top = 112
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSIZE6: TsEdit
            Left = 46
            Top = 136
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSIZE7: TsEdit
            Left = 46
            Top = 160
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSIZE10: TsEdit
            Left = 46
            Top = 232
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSIZE9: TsEdit
            Left = 46
            Top = 208
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
          object edt_InvSIZE8: TsEdit
            Left = 46
            Top = 184
            Width = 403
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Color = clWhite
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 70
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            SkinData.SkinSection = 'EDIT'
            BoundLabel.Indent = 0
            BoundLabel.Font.Charset = DEFAULT_CHARSET
            BoundLabel.Font.Color = clWindowText
            BoundLabel.Font.Height = -11
            BoundLabel.Font.Name = 'MS Sans Serif'
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeft
            BoundLabel.MaxWidth = 0
            BoundLabel.UseSkinColor = True
          end
        end
      end
      object page4_Left: TsPanel
        Left = 0
        Top = 2
        Width = 291
        Height = 769
        Align = alLeft
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
      end
    end
    object sTabSheet6: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      PopupMenu = popMenu1
      SkinData.CustomColor = False
      SkinData.CustomFont = False
      object sSplitter2: TsSplitter
        Left = 0
        Top = 32
        Width = 1076
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 35
        Width = 1076
        Height = 736
        TabStop = False
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        PopupMenu = popMenu1
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid8DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 40
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 85
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'BGM_GUBUN'
            Title.Alignment = taCenter
            Title.Caption = #49888#52397#44396#48516
            Width = 52
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TOTDOC_NO'
            Title.Alignment = taCenter
            Title.Caption = #47928#49436#48264#54840
            Width = 230
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CP_NO'
            Title.Alignment = taCenter
            Title.Caption = #49888#52397#48264#54840
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'APP_SNAME'
            Title.Alignment = taCenter
            Title.Caption = #49888#52397#51064
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DF_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #44396#47588#51088
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CP_AMT'
            Title.Alignment = taCenter
            Title.Caption = #52628#49900#44552#50529'('#50896#54868')'
            Width = 118
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CP_AMTU'
            Title.Alignment = taCenter
            Title.Caption = #52628#49900#44552#50529'('#50808#54868')'
            Width = 118
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CP_AMTC'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 82
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51088
            Width = 64
            Visible = True
          end>
      end
      object sPanel5: TsPanel
        Left = 0
        Top = 0
        Width = 1076
        Height = 32
        Align = alTop
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object edt_SearchText: TsEdit
          Left = 86
          Top = 5
          Width = 233
          Height = 25
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          Visible = False
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 81
          Height = 23
          Alignment = taLeftJustify
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
          SkinData.SkinSection = 'COMBOBOX'
          VerticalAlignment = taAlignTop
          Style = csDropDownList
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ItemHeight = 17
          ItemIndex = 0
          ParentFont = False
          TabOrder = 2
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #44396#47588#51088)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 86
          Top = 5
          Width = 82
          Height = 25
          Color = clWhite
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          OnDblClick = mask_DATEEDblClick
          CheckOnExit = True
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn8: TsBitBtn
          Left = 319
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          OnClick = sBitBtn8Click
          SkinData.SkinSection = 'BUTTON'
          ImageIndex = 6
          Images = DMICON.System18
        end
        object sBitBtn22: TsBitBtn
          Tag = 902
          Left = 167
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = btn_CalClick
          SkinData.SkinSection = 'BUTTON'
          ImageIndex = 9
          Images = DMICON.System18
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 214
          Top = 5
          Width = 82
          Height = 25
          Color = clWhite
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = mask_DATEEDblClick
          CheckOnExit = True
          BoundLabel.Indent = 0
          BoundLabel.Font.Charset = DEFAULT_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = 'MS Sans Serif'
          BoundLabel.Font.Style = []
          BoundLabel.Layout = sclLeft
          BoundLabel.MaxWidth = 0
          BoundLabel.UseSkinColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn28: TsBitBtn
          Tag = 903
          Left = 295
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = btn_CalClick
          SkinData.SkinSection = 'BUTTON'
          ImageIndex = 9
          Images = DMICON.System18
        end
        object sPanel6: TsPanel
          Left = 190
          Top = 5
          Width = 25
          Height = 23
          Caption = '~'
          TabOrder = 7
          SkinData.SkinSection = 'PANEL'
        end
      end
    end
  end
  object sPanel56: TsPanel [5]
    Left = 6
    Top = 116
    Width = 289
    Height = 765
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    object sDBGrid8: TsDBGrid
      Left = 1
      Top = 34
      Width = 287
      Height = 730
      TabStop = False
      Align = alClient
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      PopupMenu = popMenu1
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #44404#47548#52404
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid8DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #49345#54889
          Width = 32
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTDOC_NO'
          Title.Alignment = taCenter
          Title.Caption = #47928#49436#48264#54840
          Width = 160
          Visible = True
        end>
    end
    object sPanel57: TsPanel
      Left = 1
      Top = 1
      Width = 287
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      SkinData.SkinSection = 'PANEL'
      object Mask_fromDate: TsMaskEdit
        Left = 115
        Top = 5
        Width = 72
        Height = 23
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20160101'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn60: TsBitBtn
        Left = 261
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = sBitBtn60Click
        SkinData.SkinSection = 'BUTTON'
        ImageIndex = 6
        Images = DMICON.System18
      end
      object mask_toDate: TsMaskEdit
        Left = 188
        Top = 5
        Width = 73
        Height = 23
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20171231'
        CheckOnExit = True
        BoundLabel.Indent = 0
        BoundLabel.Font.Charset = DEFAULT_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -11
        BoundLabel.Font.Name = 'MS Sans Serif'
        BoundLabel.Font.Style = []
        BoundLabel.Layout = sclLeft
        BoundLabel.MaxWidth = 0
        BoundLabel.UseSkinColor = True
        SkinData.SkinSection = 'EDIT'
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 200
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MAINT_NO,[USER_ID],DATEE,MESSAGE1,MESSAGE2,CHK1,CHK2,CHK3' +
        ',BGM_GUBUN,BGM1,BGM2,BGM3,BGM4,CP_NO'
      
        #9'   ,APP_CODE,APP_SNAME,APP_SAUPNO,[APP_NAME],APP_ELEC,APP_ADDR1' +
        ',APP_ADDR2,APP_ADDR3'
      
        #9'   ,CP_BANK,CP_BANKNAME,CP_BANKBU,CP_ACCOUNTNO,CP_NAME1,CP_NAME' +
        '2,CP_CURR'
      #9'   ,DF_SAUPNO,DF_NAME1,DF_NAME2,DF_NAME3,DF_EMAIL1,DF_EMAIL2'
      
        #9'   ,CP_ADD_ACCOUNTNO1,CP_ADD_NAME1,CP_ADD_CURR1,CP_ADD_ACCOUNTN' +
        'O2,CP_ADD_NAME2,CP_ADD_CURR2'
      #9'   ,CP_AMTC,CP_AMT,CP_AMTU,CP_CUX,APP_DATE'
      #9'   ,BGM1+'#39'-'#39'+BGM2+BGM3+BGM4 TOTDOC_NO'
      #9'  '
      
        #9'  ,SPC_D1.KEYY,SPC_D1.DOC_NO,SPC_D1.ISS_DATE,SPC_D1.LA_BANKBUCO' +
        'DE,SPC_D1.LA_BANKNAMEP,SPC_D1.LR_GUBUN,SPC_D1.LR_NO,LA_BANKBUP'
      #9'  ,SPC_D1.DOC_GUBUN,SPC_D1.LA_TYPE'
      ''
      'FROM APPSPC_H AS SPC_H'
      'INNER JOIN APPSPC_D1 AS SPC_D1 ON SPC_H.MAINT_NO = SPC_D1.KEYY')
    Left = 16
    Top = 240
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListBGM_GUBUN: TStringField
      FieldName = 'BGM_GUBUN'
      OnGetText = qryListBGM_GUBUNGetText
      Size = 3
    end
    object qryListBGM1: TStringField
      FieldName = 'BGM1'
      Size = 12
    end
    object qryListBGM2: TStringField
      FieldName = 'BGM2'
      Size = 2
    end
    object qryListBGM3: TStringField
      FieldName = 'BGM3'
      Size = 2
    end
    object qryListBGM4: TStringField
      FieldName = 'BGM4'
      Size = 12
    end
    object qryListCP_NO: TStringField
      FieldName = 'CP_NO'
      Size = 35
    end
    object qryListAPP_CODE: TStringField
      FieldName = 'APP_CODE'
      Size = 10
    end
    object qryListAPP_SNAME: TStringField
      FieldName = 'APP_SNAME'
      Size = 35
    end
    object qryListAPP_SAUPNO: TStringField
      FieldName = 'APP_SAUPNO'
      Size = 10
    end
    object qryListAPP_NAME: TStringField
      FieldName = 'APP_NAME'
      Size = 35
    end
    object qryListAPP_ELEC: TStringField
      FieldName = 'APP_ELEC'
      Size = 35
    end
    object qryListAPP_ADDR1: TStringField
      FieldName = 'APP_ADDR1'
      Size = 35
    end
    object qryListAPP_ADDR2: TStringField
      FieldName = 'APP_ADDR2'
      Size = 35
    end
    object qryListAPP_ADDR3: TStringField
      FieldName = 'APP_ADDR3'
      Size = 35
    end
    object qryListCP_BANK: TStringField
      FieldName = 'CP_BANK'
      Size = 11
    end
    object qryListCP_BANKNAME: TStringField
      FieldName = 'CP_BANKNAME'
      Size = 70
    end
    object qryListCP_BANKBU: TStringField
      FieldName = 'CP_BANKBU'
      Size = 70
    end
    object qryListCP_ACCOUNTNO: TStringField
      FieldName = 'CP_ACCOUNTNO'
      Size = 17
    end
    object qryListCP_NAME1: TStringField
      FieldName = 'CP_NAME1'
      Size = 35
    end
    object qryListCP_NAME2: TStringField
      FieldName = 'CP_NAME2'
      Size = 35
    end
    object qryListCP_CURR: TStringField
      FieldName = 'CP_CURR'
      Size = 3
    end
    object qryListDF_SAUPNO: TStringField
      FieldName = 'DF_SAUPNO'
      Size = 10
    end
    object qryListDF_NAME1: TStringField
      FieldName = 'DF_NAME1'
      Size = 35
    end
    object qryListDF_NAME2: TStringField
      FieldName = 'DF_NAME2'
      Size = 35
    end
    object qryListDF_NAME3: TStringField
      FieldName = 'DF_NAME3'
      Size = 35
    end
    object qryListDF_EMAIL1: TStringField
      FieldName = 'DF_EMAIL1'
      Size = 35
    end
    object qryListDF_EMAIL2: TStringField
      FieldName = 'DF_EMAIL2'
      Size = 35
    end
    object qryListCP_ADD_ACCOUNTNO1: TStringField
      FieldName = 'CP_ADD_ACCOUNTNO1'
      Size = 17
    end
    object qryListCP_ADD_NAME1: TStringField
      FieldName = 'CP_ADD_NAME1'
      Size = 35
    end
    object qryListCP_ADD_CURR1: TStringField
      FieldName = 'CP_ADD_CURR1'
      Size = 3
    end
    object qryListCP_ADD_ACCOUNTNO2: TStringField
      FieldName = 'CP_ADD_ACCOUNTNO2'
      Size = 17
    end
    object qryListCP_ADD_NAME2: TStringField
      FieldName = 'CP_ADD_NAME2'
      Size = 35
    end
    object qryListCP_ADD_CURR2: TStringField
      FieldName = 'CP_ADD_CURR2'
      Size = 3
    end
    object qryListCP_AMTC: TStringField
      FieldName = 'CP_AMTC'
      Size = 3
    end
    object qryListCP_AMT: TBCDField
      FieldName = 'CP_AMT'
      DisplayFormat = '#,###.####;0'
      Precision = 18
    end
    object qryListCP_AMTU: TBCDField
      FieldName = 'CP_AMTU'
      DisplayFormat = '#,###.####;0'
      Precision = 18
    end
    object qryListCP_CUX: TBCDField
      FieldName = 'CP_CUX'
      Precision = 18
    end
    object qryListKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryListDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListLA_BANKBUCODE: TStringField
      FieldName = 'LA_BANKBUCODE'
      Size = 11
    end
    object qryListLA_BANKNAMEP: TStringField
      FieldName = 'LA_BANKNAMEP'
      Size = 70
    end
    object qryListLR_GUBUN: TStringField
      FieldName = 'LR_GUBUN'
      Size = 35
    end
    object qryListLR_NO: TStringField
      FieldName = 'LR_NO'
      Size = 35
    end
    object qryListLA_BANKBUP: TStringField
      FieldName = 'LA_BANKBUP'
      Size = 70
    end
    object qryListDOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryListTOTDOC_NO: TStringField
      FieldName = 'TOTDOC_NO'
      ReadOnly = True
      Size = 29
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      Size = 35
    end
    object qryListLA_TYPE: TStringField
      FieldName = 'LA_TYPE'
      Size = 3
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 48
    Top = 240
  end
  object qryGoods: TADOQuery
    Connection = DMMssql.KISConnect
    AfterInsert = qryGoodsAfterInsert
    AfterEdit = qryGoodsAfterInsert
    AfterPost = qryGoodsAfterCancel
    AfterCancel = qryGoodsAfterCancel
    AfterScroll = qryGoodsAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT KEYY,DOC_GUBUN,SEQ,LR_GUBUN,DOC_NO,LR_NO2,BSN_HSCODE,ISS_' +
        'DATE,ACC_DATE,VAL_DATE'
      #9'  ,BY_CODE,BY_SNAME,BY_NAME,BY_ELEC,BY_ADDR1,BY_ADDR2,BY_ADDR3'
      #9'  ,LR_NO,GET_DATE,EXP_DATE,SE_CODE,SE_SNAME'
      
        #9'  ,LA_BANK,LA_BANKNAME,LA_BANKBU,LA_ELEC,EXCH,ISS_AMT,ISS_AMTC,' +
        'ISS_AMTU,ISS_EXPAMT,ISS_EXPAMTC,ISS_EXPAMTU'
      #9'  ,TOTCNTC,TOTCNT,ISS_TOTAMTC,ISS_TOTAMT,'
      
        #9'  REMARK1_1,REMARK1_2,REMARK1_3,REMARK1_4,REMARK1_5,REMARK2_1,R' +
        'EMARK2_2,REMARK2_3,REMARK2_4,REMARK2_5'
      ''
      'FROM APPSPC_D1 with(nolock)'
      ''
      'WHERE KEYY = :MAINT_NO AND DOC_GUBUN = '#39'2AH'#39'  ')
    Left = 16
    Top = 272
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsDOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryGoodsSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryGoodsDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryGoodsLR_NO2: TStringField
      FieldName = 'LR_NO2'
      Size = 35
    end
    object qryGoodsBSN_HSCODE: TStringField
      FieldName = 'BSN_HSCODE'
      Size = 35
    end
    object qryGoodsISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryGoodsACC_DATE: TStringField
      FieldName = 'ACC_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryGoodsVAL_DATE: TStringField
      FieldName = 'VAL_DATE'
      Size = 8
    end
    object qryGoodsBY_CODE: TStringField
      FieldName = 'BY_CODE'
      Size = 35
    end
    object qryGoodsBY_SNAME: TStringField
      FieldName = 'BY_SNAME'
      Size = 35
    end
    object qryGoodsBY_NAME: TStringField
      FieldName = 'BY_NAME'
      Size = 35
    end
    object qryGoodsBY_ELEC: TStringField
      FieldName = 'BY_ELEC'
      Size = 35
    end
    object qryGoodsBY_ADDR1: TStringField
      FieldName = 'BY_ADDR1'
      Size = 35
    end
    object qryGoodsBY_ADDR2: TStringField
      FieldName = 'BY_ADDR2'
      Size = 35
    end
    object qryGoodsBY_ADDR3: TStringField
      FieldName = 'BY_ADDR3'
      Size = 35
    end
    object qryGoodsLR_NO: TStringField
      FieldName = 'LR_NO'
      Size = 35
    end
    object qryGoodsGET_DATE: TStringField
      FieldName = 'GET_DATE'
      Size = 8
    end
    object qryGoodsEXP_DATE: TStringField
      FieldName = 'EXP_DATE'
      Size = 8
    end
    object qryGoodsSE_CODE: TStringField
      FieldName = 'SE_CODE'
      Size = 35
    end
    object qryGoodsSE_SNAME: TStringField
      FieldName = 'SE_SNAME'
      Size = 35
    end
    object qryGoodsLA_BANK: TStringField
      FieldName = 'LA_BANK'
      Size = 35
    end
    object qryGoodsLA_BANKNAME: TStringField
      FieldName = 'LA_BANKNAME'
      Size = 35
    end
    object qryGoodsLA_BANKBU: TStringField
      FieldName = 'LA_BANKBU'
      Size = 35
    end
    object qryGoodsLA_ELEC: TStringField
      FieldName = 'LA_ELEC'
      Size = 35
    end
    object qryGoodsEXCH: TBCDField
      FieldName = 'EXCH'
      Precision = 18
    end
    object qryGoodsISS_AMT: TBCDField
      FieldName = 'ISS_AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsISS_AMTC: TStringField
      FieldName = 'ISS_AMTC'
      Size = 3
    end
    object qryGoodsISS_AMTU: TBCDField
      FieldName = 'ISS_AMTU'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsISS_EXPAMT: TBCDField
      FieldName = 'ISS_EXPAMT'
      Precision = 18
    end
    object qryGoodsISS_EXPAMTC: TStringField
      FieldName = 'ISS_EXPAMTC'
      Size = 3
    end
    object qryGoodsISS_EXPAMTU: TBCDField
      FieldName = 'ISS_EXPAMTU'
      Precision = 18
    end
    object qryGoodsTOTCNTC: TStringField
      FieldName = 'TOTCNTC'
      Size = 3
    end
    object qryGoodsTOTCNT: TBCDField
      FieldName = 'TOTCNT'
      Precision = 18
    end
    object qryGoodsISS_TOTAMTC: TStringField
      FieldName = 'ISS_TOTAMTC'
      Size = 3
    end
    object qryGoodsISS_TOTAMT: TBCDField
      FieldName = 'ISS_TOTAMT'
      Precision = 18
    end
    object qryGoodsREMARK1_1: TStringField
      FieldName = 'REMARK1_1'
      Size = 70
    end
    object qryGoodsREMARK1_2: TStringField
      FieldName = 'REMARK1_2'
      Size = 70
    end
    object qryGoodsREMARK1_3: TStringField
      FieldName = 'REMARK1_3'
      Size = 70
    end
    object qryGoodsREMARK1_4: TStringField
      FieldName = 'REMARK1_4'
      Size = 70
    end
    object qryGoodsREMARK1_5: TStringField
      FieldName = 'REMARK1_5'
      Size = 70
    end
    object qryGoodsREMARK2_1: TStringField
      FieldName = 'REMARK2_1'
      Size = 70
    end
    object qryGoodsREMARK2_2: TStringField
      FieldName = 'REMARK2_2'
      Size = 70
    end
    object qryGoodsREMARK2_3: TStringField
      FieldName = 'REMARK2_3'
      Size = 70
    end
    object qryGoodsREMARK2_4: TStringField
      FieldName = 'REMARK2_4'
      Size = 70
    end
    object qryGoodsREMARK2_5: TStringField
      FieldName = 'REMARK2_5'
      Size = 70
    end
    object qryGoodsLR_GUBUN: TStringField
      FieldName = 'LR_GUBUN'
      Size = 35
    end
  end
  object dsGoods: TDataSource
    DataSet = qryGoods
    Left = 48
    Top = 272
  end
  object qryGoodsItem: TADOQuery
    Connection = DMMssql.KISConnect
    AfterInsert = qryGoodsItemAfterInsert
    AfterEdit = qryGoodsItemAfterInsert
    AfterScroll = qryGoodsItemAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQNO'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        #9#9'KEYY,DOC_GUBUN,SEQ,LINE_NO,HS_NO,IMD_CODE1,IMD_CODE2,IMD_CODE3' +
        ',IMD_CODE4'
      #9#9',SIZE1,SIZE2,SIZE3,SIZE4,SIZE5,SIZE6,SIZE7,SIZE8,SIZE9,SIZE10'
      
        #9#9',QTYC,QTY,TOTQTYC,TOTQTY,PRICE,PRICEC,PRICE_G,SUP_AMTC,SUP_AMT' +
        ',SUP_TOTAMTC,SUP_TOTAMT'
      ''
      '  FROM APPSPC_D2'
      ''
      'WHERE KEYY = :MAINT_NO AND DOC_GUBUN = '#39'2AH'#39' AND SEQ = :SEQNO')
    Left = 184
    Top = 752
    object qryGoodsItemKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsItemDOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryGoodsItemSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryGoodsItemLINE_NO: TStringField
      FieldName = 'LINE_NO'
      EditMask = '#0000'
      Size = 8
    end
    object qryGoodsItemHS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 35
    end
    object qryGoodsItemIMD_CODE1: TStringField
      FieldName = 'IMD_CODE1'
      Size = 35
    end
    object qryGoodsItemIMD_CODE2: TStringField
      FieldName = 'IMD_CODE2'
      Size = 35
    end
    object qryGoodsItemIMD_CODE3: TStringField
      FieldName = 'IMD_CODE3'
      Size = 35
    end
    object qryGoodsItemIMD_CODE4: TStringField
      FieldName = 'IMD_CODE4'
      Size = 35
    end
    object qryGoodsItemSIZE1: TStringField
      FieldName = 'SIZE1'
      Size = 70
    end
    object qryGoodsItemSIZE2: TStringField
      FieldName = 'SIZE2'
      Size = 70
    end
    object qryGoodsItemSIZE3: TStringField
      FieldName = 'SIZE3'
      Size = 70
    end
    object qryGoodsItemSIZE4: TStringField
      FieldName = 'SIZE4'
      Size = 70
    end
    object qryGoodsItemSIZE5: TStringField
      FieldName = 'SIZE5'
      Size = 70
    end
    object qryGoodsItemSIZE6: TStringField
      FieldName = 'SIZE6'
      Size = 70
    end
    object qryGoodsItemSIZE7: TStringField
      FieldName = 'SIZE7'
      Size = 70
    end
    object qryGoodsItemSIZE8: TStringField
      FieldName = 'SIZE8'
      Size = 70
    end
    object qryGoodsItemSIZE9: TStringField
      FieldName = 'SIZE9'
      Size = 70
    end
    object qryGoodsItemSIZE10: TStringField
      FieldName = 'SIZE10'
      Size = 70
    end
    object qryGoodsItemQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryGoodsItemQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryGoodsItemTOTQTYC: TStringField
      FieldName = 'TOTQTYC'
      Size = 3
    end
    object qryGoodsItemTOTQTY: TBCDField
      FieldName = 'TOTQTY'
      Precision = 18
    end
    object qryGoodsItemPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryGoodsItemPRICEC: TStringField
      FieldName = 'PRICEC'
      Size = 3
    end
    object qryGoodsItemPRICE_G: TBCDField
      FieldName = 'PRICE_G'
      Precision = 18
    end
    object qryGoodsItemSUP_AMTC: TStringField
      FieldName = 'SUP_AMTC'
      Size = 3
    end
    object qryGoodsItemSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      Precision = 18
    end
    object qryGoodsItemSUP_TOTAMTC: TStringField
      FieldName = 'SUP_TOTAMTC'
      Size = 3
    end
    object qryGoodsItemSUP_TOTAMT: TBCDField
      FieldName = 'SUP_TOTAMT'
      Precision = 18
    end
  end
  object dsGoodsitem: TDataSource
    DataSet = qryGoodsItem
    Left = 216
    Top = 752
  end
  object qryTax: TADOQuery
    Connection = DMMssql.KISConnect
    AfterInsert = qryTaxAfterInsert
    AfterEdit = qryTaxAfterInsert
    AfterPost = qryTaxAfterCancel
    AfterCancel = qryTaxAfterCancel
    AfterScroll = qryTaxAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT KEYY,DOC_GUBUN,SEQ,DOC_NO,VB_RENO,VB_SENO,VB_FSNO,VB_MAIN' +
        'TNO,ISS_DATE,VB_DMNO'
      
        '      ,SE_CODE,SE_SNAME,SE_NAME,SE_SAUPNO,SE_ADDR1,SE_ADDR2,SE_A' +
        'DDR3,SE_ELEC,SG_UPTAI1_1,SG_UPTAI1_2,SG_UPTAI1_3,HN_ITEM1_1,HN_I' +
        'TEM1_2,HN_ITEM1_3'
      
        #9'  ,BY_CODE,BY_SNAME,BY_NAME,BY_SAUPNO,BY_ADDR1,BY_ADDR2,BY_ADDR' +
        '3,BY_ELEC,SG_UPTAI2_1,SG_UPTAI2_2,SG_UPTAI2_3,HN_ITEM2_1,HN_ITEM' +
        '2_2,HN_ITEM2_3'
      
        #9'  ,AG_CODE,AG_SNAME,AG_NAME,AG_SAUPNO,AG_ADDR1,AG_ADDR2,AG_ADDR' +
        '3,AG_ELEC'
      
        #9'  ,PAI_AMT1,PAI_AMT2,PAI_AMT3,PAI_AMT4,PAI_AMTC1,PAI_AMTC2,PAI_' +
        'AMTC3,PAI_AMTC4,PAI_AMTU1,PAI_AMTU2,PAI_AMTU3,PAI_AMTU4'
      #9'  ,ISS_EXPAMT,ISS_EXPAMTU,TOTCNT,TOTCNTC,VB_GUBUN,VB_DETAILNO'
      #9'  ,REMARK1_1,REMARK1_2,REMARK1_3,REMARK1_4,REMARK1_5'
      ''
      'FROM APPSPC_D1 '
      ''
      'WHERE KEYY = :MAINT_NO AND DOC_GUBUN = '#39'2AJ'#39' ')
    Left = 16
    Top = 304
    object qryTaxKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxDOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryTaxSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryTaxDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryTaxVB_RENO: TStringField
      FieldName = 'VB_RENO'
      Size = 35
    end
    object qryTaxVB_SENO: TStringField
      FieldName = 'VB_SENO'
      Size = 35
    end
    object qryTaxVB_FSNO: TStringField
      FieldName = 'VB_FSNO'
      Size = 35
    end
    object qryTaxVB_MAINTNO: TStringField
      FieldName = 'VB_MAINTNO'
      Size = 35
    end
    object qryTaxISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryTaxVB_DMNO: TStringField
      FieldName = 'VB_DMNO'
      Size = 35
    end
    object qryTaxSE_CODE: TStringField
      FieldName = 'SE_CODE'
      Size = 35
    end
    object qryTaxSE_SNAME: TStringField
      FieldName = 'SE_SNAME'
      Size = 35
    end
    object qryTaxSE_NAME: TStringField
      FieldName = 'SE_NAME'
      Size = 35
    end
    object qryTaxSE_SAUPNO: TStringField
      FieldName = 'SE_SAUPNO'
      Size = 17
    end
    object qryTaxSE_ADDR1: TStringField
      FieldName = 'SE_ADDR1'
      Size = 35
    end
    object qryTaxSE_ADDR2: TStringField
      FieldName = 'SE_ADDR2'
      Size = 35
    end
    object qryTaxSE_ADDR3: TStringField
      FieldName = 'SE_ADDR3'
      Size = 35
    end
    object qryTaxSE_ELEC: TStringField
      FieldName = 'SE_ELEC'
      Size = 35
    end
    object qryTaxSG_UPTAI1_1: TStringField
      FieldName = 'SG_UPTAI1_1'
      Size = 35
    end
    object qryTaxSG_UPTAI1_2: TStringField
      FieldName = 'SG_UPTAI1_2'
      Size = 35
    end
    object qryTaxSG_UPTAI1_3: TStringField
      FieldName = 'SG_UPTAI1_3'
      Size = 35
    end
    object qryTaxHN_ITEM1_1: TStringField
      FieldName = 'HN_ITEM1_1'
      Size = 35
    end
    object qryTaxHN_ITEM1_2: TStringField
      FieldName = 'HN_ITEM1_2'
      Size = 35
    end
    object qryTaxHN_ITEM1_3: TStringField
      FieldName = 'HN_ITEM1_3'
      Size = 35
    end
    object qryTaxBY_CODE: TStringField
      FieldName = 'BY_CODE'
      Size = 35
    end
    object qryTaxBY_SNAME: TStringField
      FieldName = 'BY_SNAME'
      Size = 35
    end
    object qryTaxBY_NAME: TStringField
      FieldName = 'BY_NAME'
      Size = 35
    end
    object qryTaxBY_SAUPNO: TStringField
      FieldName = 'BY_SAUPNO'
      Size = 17
    end
    object qryTaxBY_ADDR1: TStringField
      FieldName = 'BY_ADDR1'
      Size = 35
    end
    object qryTaxBY_ADDR2: TStringField
      FieldName = 'BY_ADDR2'
      Size = 35
    end
    object qryTaxBY_ADDR3: TStringField
      FieldName = 'BY_ADDR3'
      Size = 35
    end
    object qryTaxBY_ELEC: TStringField
      FieldName = 'BY_ELEC'
      Size = 35
    end
    object qryTaxSG_UPTAI2_1: TStringField
      FieldName = 'SG_UPTAI2_1'
      Size = 35
    end
    object qryTaxSG_UPTAI2_2: TStringField
      FieldName = 'SG_UPTAI2_2'
      Size = 35
    end
    object qryTaxSG_UPTAI2_3: TStringField
      FieldName = 'SG_UPTAI2_3'
      Size = 35
    end
    object qryTaxHN_ITEM2_1: TStringField
      FieldName = 'HN_ITEM2_1'
      Size = 35
    end
    object qryTaxHN_ITEM2_2: TStringField
      FieldName = 'HN_ITEM2_2'
      Size = 35
    end
    object qryTaxHN_ITEM2_3: TStringField
      FieldName = 'HN_ITEM2_3'
      Size = 35
    end
    object qryTaxAG_CODE: TStringField
      FieldName = 'AG_CODE'
      Size = 35
    end
    object qryTaxAG_SNAME: TStringField
      FieldName = 'AG_SNAME'
      Size = 35
    end
    object qryTaxAG_NAME: TStringField
      FieldName = 'AG_NAME'
      Size = 35
    end
    object qryTaxAG_SAUPNO: TStringField
      FieldName = 'AG_SAUPNO'
      Size = 17
    end
    object qryTaxAG_ADDR1: TStringField
      FieldName = 'AG_ADDR1'
      Size = 35
    end
    object qryTaxAG_ADDR2: TStringField
      FieldName = 'AG_ADDR2'
      Size = 35
    end
    object qryTaxAG_ADDR3: TStringField
      FieldName = 'AG_ADDR3'
      Size = 35
    end
    object qryTaxAG_ELEC: TStringField
      FieldName = 'AG_ELEC'
      Size = 35
    end
    object qryTaxPAI_AMT1: TBCDField
      FieldName = 'PAI_AMT1'
      Precision = 18
    end
    object qryTaxPAI_AMT2: TBCDField
      FieldName = 'PAI_AMT2'
      Precision = 18
    end
    object qryTaxPAI_AMT3: TBCDField
      FieldName = 'PAI_AMT3'
      Precision = 18
    end
    object qryTaxPAI_AMT4: TBCDField
      FieldName = 'PAI_AMT4'
      Precision = 18
    end
    object qryTaxPAI_AMTC1: TStringField
      FieldName = 'PAI_AMTC1'
      Size = 3
    end
    object qryTaxPAI_AMTC2: TStringField
      FieldName = 'PAI_AMTC2'
      Size = 3
    end
    object qryTaxPAI_AMTC3: TStringField
      FieldName = 'PAI_AMTC3'
      Size = 3
    end
    object qryTaxPAI_AMTC4: TStringField
      FieldName = 'PAI_AMTC4'
      Size = 3
    end
    object qryTaxPAI_AMTU1: TBCDField
      FieldName = 'PAI_AMTU1'
      Precision = 18
    end
    object qryTaxPAI_AMTU2: TBCDField
      FieldName = 'PAI_AMTU2'
      Precision = 18
    end
    object qryTaxPAI_AMTU3: TBCDField
      FieldName = 'PAI_AMTU3'
      Precision = 18
    end
    object qryTaxPAI_AMTU4: TBCDField
      FieldName = 'PAI_AMTU4'
      Precision = 18
    end
    object qryTaxISS_EXPAMT: TBCDField
      FieldName = 'ISS_EXPAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryTaxISS_EXPAMTU: TBCDField
      FieldName = 'ISS_EXPAMTU'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryTaxTOTCNT: TBCDField
      FieldName = 'TOTCNT'
      Precision = 18
    end
    object qryTaxVB_GUBUN: TStringField
      FieldName = 'VB_GUBUN'
      Size = 3
    end
    object qryTaxVB_DETAILNO: TStringField
      FieldName = 'VB_DETAILNO'
      Size = 6
    end
    object qryTaxREMARK1_1: TStringField
      FieldName = 'REMARK1_1'
      Size = 70
    end
    object qryTaxREMARK1_2: TStringField
      FieldName = 'REMARK1_2'
      Size = 70
    end
    object qryTaxREMARK1_3: TStringField
      FieldName = 'REMARK1_3'
      Size = 70
    end
    object qryTaxREMARK1_4: TStringField
      FieldName = 'REMARK1_4'
      Size = 70
    end
    object qryTaxREMARK1_5: TStringField
      FieldName = 'REMARK1_5'
      Size = 70
    end
    object qryTaxTOTCNTC: TStringField
      FieldName = 'TOTCNTC'
      Size = 3
    end
  end
  object dsTax: TDataSource
    DataSet = qryTax
    Left = 48
    Top = 304
  end
  object qryTaxItem: TADOQuery
    Connection = DMMssql.KISConnect
    AfterInsert = qryTaxItemAfterInsert
    AfterEdit = qryTaxItemAfterInsert
    AfterScroll = qryTaxItemAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQNO'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        #9#9'KEYY,DOC_GUBUN,SEQ,LINE_NO,DE_DATE,IMD_CODE1,IMD_CODE2,IMD_COD' +
        'E3,IMD_CODE4'
      #9#9',SIZE1,SIZE2,SIZE3,SIZE4,SIZE5,SIZE6,SIZE7,SIZE8,SIZE9,SIZE10'
      
        #9#9',QTYC,QTY,TOTQTYC,TOTQTY,CUX_RATE,PRICE,PRICEC,PRICE_G,SUP_AMT' +
        'C,SUP_AMT,SUP_TOTAMTC,SUP_TOTAMT'
      
        #9#9',VB_AMTC,VB_AMT,VB_TOTAMTC,VB_TOTAMT,VB_TAXC,VB_TAX,VB_TOTTAXC' +
        ',VB_TOTTAX'
      #9#9',BIGO1,BIGO2,BIGO3,BIGO4,BIGO5'
      ''
      '  FROM APPSPC_D2'
      ''
      'WHERE KEYY = :MAINT_NO AND DOC_GUBUN = '#39'2AJ'#39' AND SEQ = :SEQNO ')
    Left = 184
    Top = 784
    object qryTaxItemKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryTaxItemDOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryTaxItemSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryTaxItemLINE_NO: TStringField
      FieldName = 'LINE_NO'
      Size = 8
    end
    object qryTaxItemDE_DATE: TStringField
      FieldName = 'DE_DATE'
      EditMask = '9999-99-99;0'
      Size = 35
    end
    object qryTaxItemIMD_CODE1: TStringField
      FieldName = 'IMD_CODE1'
      Size = 35
    end
    object qryTaxItemIMD_CODE2: TStringField
      FieldName = 'IMD_CODE2'
      Size = 35
    end
    object qryTaxItemIMD_CODE3: TStringField
      FieldName = 'IMD_CODE3'
      Size = 35
    end
    object qryTaxItemIMD_CODE4: TStringField
      FieldName = 'IMD_CODE4'
      Size = 35
    end
    object qryTaxItemSIZE1: TStringField
      FieldName = 'SIZE1'
      Size = 70
    end
    object qryTaxItemSIZE2: TStringField
      FieldName = 'SIZE2'
      Size = 70
    end
    object qryTaxItemSIZE3: TStringField
      FieldName = 'SIZE3'
      Size = 70
    end
    object qryTaxItemSIZE4: TStringField
      FieldName = 'SIZE4'
      Size = 70
    end
    object qryTaxItemSIZE5: TStringField
      FieldName = 'SIZE5'
      Size = 70
    end
    object qryTaxItemSIZE6: TStringField
      FieldName = 'SIZE6'
      Size = 70
    end
    object qryTaxItemSIZE7: TStringField
      FieldName = 'SIZE7'
      Size = 70
    end
    object qryTaxItemSIZE8: TStringField
      FieldName = 'SIZE8'
      Size = 70
    end
    object qryTaxItemSIZE9: TStringField
      FieldName = 'SIZE9'
      Size = 70
    end
    object qryTaxItemSIZE10: TStringField
      FieldName = 'SIZE10'
      Size = 70
    end
    object qryTaxItemQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryTaxItemQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryTaxItemTOTQTYC: TStringField
      FieldName = 'TOTQTYC'
      Size = 3
    end
    object qryTaxItemTOTQTY: TBCDField
      FieldName = 'TOTQTY'
      Precision = 18
    end
    object qryTaxItemCUX_RATE: TBCDField
      FieldName = 'CUX_RATE'
      Precision = 18
    end
    object qryTaxItemPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryTaxItemPRICEC: TStringField
      FieldName = 'PRICEC'
      Size = 3
    end
    object qryTaxItemPRICE_G: TBCDField
      FieldName = 'PRICE_G'
      Precision = 18
    end
    object qryTaxItemSUP_AMTC: TStringField
      FieldName = 'SUP_AMTC'
      Size = 3
    end
    object qryTaxItemSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      Precision = 18
    end
    object qryTaxItemSUP_TOTAMTC: TStringField
      FieldName = 'SUP_TOTAMTC'
      Size = 3
    end
    object qryTaxItemSUP_TOTAMT: TBCDField
      FieldName = 'SUP_TOTAMT'
      Precision = 18
    end
    object qryTaxItemVB_AMTC: TStringField
      FieldName = 'VB_AMTC'
      Size = 3
    end
    object qryTaxItemVB_AMT: TBCDField
      FieldName = 'VB_AMT'
      Precision = 18
    end
    object qryTaxItemVB_TOTAMTC: TStringField
      FieldName = 'VB_TOTAMTC'
      Size = 3
    end
    object qryTaxItemVB_TOTAMT: TBCDField
      FieldName = 'VB_TOTAMT'
      Precision = 18
    end
    object qryTaxItemVB_TAXC: TStringField
      FieldName = 'VB_TAXC'
      Size = 3
    end
    object qryTaxItemVB_TAX: TBCDField
      FieldName = 'VB_TAX'
      Precision = 18
    end
    object qryTaxItemVB_TOTTAXC: TStringField
      FieldName = 'VB_TOTTAXC'
      Size = 3
    end
    object qryTaxItemVB_TOTTAX: TBCDField
      FieldName = 'VB_TOTTAX'
      Precision = 18
    end
    object qryTaxItemBIGO1: TStringField
      FieldName = 'BIGO1'
      Size = 70
    end
    object qryTaxItemBIGO2: TStringField
      FieldName = 'BIGO2'
      Size = 70
    end
    object qryTaxItemBIGO3: TStringField
      FieldName = 'BIGO3'
      Size = 70
    end
    object qryTaxItemBIGO4: TStringField
      FieldName = 'BIGO4'
      Size = 70
    end
    object qryTaxItemBIGO5: TStringField
      FieldName = 'BIGO5'
      Size = 70
    end
  end
  object dsTaxItem: TDataSource
    DataSet = qryTaxItem
    Left = 216
    Top = 784
  end
  object qryInv: TADOQuery
    Connection = DMMssql.KISConnect
    AfterInsert = qryInvAfterInsert
    AfterEdit = qryInvAfterInsert
    AfterPost = qryInvAfterCancel
    AfterCancel = qryInvAfterCancel
    AfterScroll = qryInvAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT KEYY,DOC_GUBUN,SEQ,DOC_NO,ISS_DATE,TOTCNT,TOTCNTC,ISS_TOT' +
        'AMT,ISS_TOTAMTC'
      '      ,REMARK1_1,REMARK1_2,REMARK1_3,REMARK1_4,REMARK1_5'
      #9'  ,SE_CODE,SE_SNAME,SE_NAME,SE_ADDR1,SE_ADDR2,SE_ADDR3'
      #9'  ,BY_CODE,BY_SNAME,BY_NAME,BY_ADDR1,BY_ADDR2,BY_ADDR3'
      ''
      'FROM APPSPC_D1 '
      ''
      'WHERE KEYY = :MAINT_NO  AND DOC_GUBUN = '#39'1BW'#39' ')
    Left = 16
    Top = 336
    object qryInvKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryInvDOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryInvSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryInvDOC_NO: TStringField
      FieldName = 'DOC_NO'
      Size = 35
    end
    object qryInvISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryInvTOTCNT: TBCDField
      FieldName = 'TOTCNT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryInvTOTCNTC: TStringField
      FieldName = 'TOTCNTC'
      Size = 3
    end
    object qryInvISS_TOTAMT: TBCDField
      FieldName = 'ISS_TOTAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryInvISS_TOTAMTC: TStringField
      FieldName = 'ISS_TOTAMTC'
      Size = 3
    end
    object qryInvREMARK1_1: TStringField
      FieldName = 'REMARK1_1'
      Size = 70
    end
    object qryInvREMARK1_2: TStringField
      FieldName = 'REMARK1_2'
      Size = 70
    end
    object qryInvREMARK1_3: TStringField
      FieldName = 'REMARK1_3'
      Size = 70
    end
    object qryInvREMARK1_4: TStringField
      FieldName = 'REMARK1_4'
      Size = 70
    end
    object qryInvREMARK1_5: TStringField
      FieldName = 'REMARK1_5'
      Size = 70
    end
    object qryInvSE_CODE: TStringField
      FieldName = 'SE_CODE'
      Size = 35
    end
    object qryInvSE_SNAME: TStringField
      FieldName = 'SE_SNAME'
      Size = 35
    end
    object qryInvSE_NAME: TStringField
      FieldName = 'SE_NAME'
      Size = 35
    end
    object qryInvSE_ADDR1: TStringField
      FieldName = 'SE_ADDR1'
      Size = 35
    end
    object qryInvSE_ADDR2: TStringField
      FieldName = 'SE_ADDR2'
      Size = 35
    end
    object qryInvSE_ADDR3: TStringField
      FieldName = 'SE_ADDR3'
      Size = 35
    end
    object qryInvBY_CODE: TStringField
      FieldName = 'BY_CODE'
      Size = 35
    end
    object qryInvBY_SNAME: TStringField
      FieldName = 'BY_SNAME'
      Size = 35
    end
    object qryInvBY_NAME: TStringField
      FieldName = 'BY_NAME'
      Size = 35
    end
    object qryInvBY_ADDR1: TStringField
      FieldName = 'BY_ADDR1'
      Size = 35
    end
    object qryInvBY_ADDR2: TStringField
      FieldName = 'BY_ADDR2'
      Size = 35
    end
    object qryInvBY_ADDR3: TStringField
      FieldName = 'BY_ADDR3'
      Size = 35
    end
  end
  object dsInv: TDataSource
    DataSet = qryInv
    Left = 48
    Top = 336
  end
  object qryInvItem: TADOQuery
    Connection = DMMssql.KISConnect
    AfterInsert = qryInvItemAfterInsert
    AfterEdit = qryInvItemAfterInsert
    AfterPost = qryInvItemAfterCancel
    AfterCancel = qryInvItemAfterCancel
    AfterScroll = qryInvItemAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'SEQNO'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT '
      
        #9#9'KEYY,DOC_GUBUN,SEQ,LINE_NO,IMD_CODE1,IMD_CODE2,IMD_CODE3,IMD_C' +
        'ODE4'
      #9#9',SIZE1,SIZE2,SIZE3,SIZE4,SIZE5,SIZE6,SIZE7,SIZE8,SIZE9,SIZE10'
      
        #9#9',QTYC,QTY,TOTQTYC,TOTQTY,PRICE,PRICEC,PRICE_G,SUP_AMTC,SUP_AMT' +
        ',SUP_TOTAMTC,SUP_TOTAMT'
      ''
      '  FROM APPSPC_D2'
      ''
      'WHERE KEYY = :MAINT_NO AND DOC_GUBUN = '#39'1BW'#39' AND SEQ = :SEQNO')
    Left = 184
    Top = 816
    object qryInvItemKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryInvItemDOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryInvItemSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryInvItemLINE_NO: TStringField
      FieldName = 'LINE_NO'
      Size = 8
    end
    object qryInvItemIMD_CODE1: TStringField
      FieldName = 'IMD_CODE1'
      Size = 35
    end
    object qryInvItemIMD_CODE2: TStringField
      FieldName = 'IMD_CODE2'
      Size = 35
    end
    object qryInvItemIMD_CODE3: TStringField
      FieldName = 'IMD_CODE3'
      Size = 35
    end
    object qryInvItemIMD_CODE4: TStringField
      FieldName = 'IMD_CODE4'
      Size = 35
    end
    object qryInvItemSIZE1: TStringField
      FieldName = 'SIZE1'
      Size = 70
    end
    object qryInvItemSIZE2: TStringField
      FieldName = 'SIZE2'
      Size = 70
    end
    object qryInvItemSIZE3: TStringField
      FieldName = 'SIZE3'
      Size = 70
    end
    object qryInvItemSIZE4: TStringField
      FieldName = 'SIZE4'
      Size = 70
    end
    object qryInvItemSIZE5: TStringField
      FieldName = 'SIZE5'
      Size = 70
    end
    object qryInvItemSIZE6: TStringField
      FieldName = 'SIZE6'
      Size = 70
    end
    object qryInvItemSIZE7: TStringField
      FieldName = 'SIZE7'
      Size = 70
    end
    object qryInvItemSIZE8: TStringField
      FieldName = 'SIZE8'
      Size = 70
    end
    object qryInvItemSIZE9: TStringField
      FieldName = 'SIZE9'
      Size = 70
    end
    object qryInvItemSIZE10: TStringField
      FieldName = 'SIZE10'
      Size = 70
    end
    object qryInvItemQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryInvItemQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryInvItemTOTQTYC: TStringField
      FieldName = 'TOTQTYC'
      Size = 3
    end
    object qryInvItemTOTQTY: TBCDField
      FieldName = 'TOTQTY'
      Precision = 18
    end
    object qryInvItemPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryInvItemPRICEC: TStringField
      FieldName = 'PRICEC'
      Size = 3
    end
    object qryInvItemPRICE_G: TBCDField
      FieldName = 'PRICE_G'
      Precision = 18
    end
    object qryInvItemSUP_AMTC: TStringField
      FieldName = 'SUP_AMTC'
      Size = 3
    end
    object qryInvItemSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryInvItemSUP_TOTAMTC: TStringField
      FieldName = 'SUP_TOTAMTC'
      Size = 3
    end
    object qryInvItemSUP_TOTAMT: TBCDField
      FieldName = 'SUP_TOTAMT'
      Precision = 18
    end
  end
  object dsInvItem: TDataSource
    DataSet = qryInvItem
    Left = 216
    Top = 816
  end
  object qryMAX_SEQ: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    Left = 104
    Top = 240
  end
  object goodsMenu: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = PopMenuEnabled
    Left = 16
    Top = 376
    object N1: TMenuItem
      Caption = #45936#51060#53552' '#52628#44032
      ImageIndex = 26
      OnClick = PopMenuClick
    end
    object N2: TMenuItem
      Tag = 1
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = PopMenuClick
    end
    object N3: TMenuItem
      Tag = 2
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = PopMenuClick
    end
  end
  object TaxMenu: TPopupMenu
    Tag = 1
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = PopMenuEnabled
    Left = 16
    Top = 408
    object MenuItem1: TMenuItem
      Tag = 6
      Caption = #45936#51060#53552' '#52628#44032
      ImageIndex = 26
      OnClick = PopMenuClick
    end
    object MenuItem2: TMenuItem
      Tag = 7
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = PopMenuClick
    end
    object MenuItem3: TMenuItem
      Tag = 8
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = PopMenuClick
    end
  end
  object invMenu: TPopupMenu
    Tag = 2
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = PopMenuEnabled
    Left = 16
    Top = 440
    object MenuItem4: TMenuItem
      Tag = 12
      Caption = #45936#51060#53552' '#52628#44032
      ImageIndex = 26
      OnClick = PopMenuClick
    end
    object MenuItem5: TMenuItem
      Tag = 13
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = PopMenuClick
    end
    object MenuItem6: TMenuItem
      Tag = 14
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = PopMenuClick
    end
  end
  object goodsItemMenu: TPopupMenu
    Tag = 3
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = PopMenuEnabled
    Left = 152
    Top = 752
    object MenuItem7: TMenuItem
      Tag = 3
      Caption = #45936#51060#53552' '#52628#44032
      ImageIndex = 26
      OnClick = PopMenuClick
    end
    object MenuItem8: TMenuItem
      Tag = 4
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = PopMenuClick
    end
    object MenuItem9: TMenuItem
      Tag = 5
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = PopMenuClick
    end
  end
  object TaxItemMenu: TPopupMenu
    Tag = 4
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = PopMenuEnabled
    Left = 152
    Top = 784
    object MenuItem10: TMenuItem
      Tag = 9
      Caption = #45936#51060#53552' '#52628#44032
      ImageIndex = 26
      OnClick = PopMenuClick
    end
    object MenuItem11: TMenuItem
      Tag = 10
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = PopMenuClick
    end
    object MenuItem12: TMenuItem
      Tag = 11
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = PopMenuClick
    end
  end
  object invItemMenu: TPopupMenu
    Tag = 5
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = PopMenuEnabled
    Left = 152
    Top = 816
    object MenuItem13: TMenuItem
      Tag = 15
      Caption = #45936#51060#53552' '#52628#44032
      ImageIndex = 26
      OnClick = PopMenuClick
    end
    object MenuItem14: TMenuItem
      Tag = 16
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = PopMenuClick
    end
    object MenuItem15: TMenuItem
      Tag = 17
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = PopMenuClick
    end
  end
  object qryMAX_LINENO: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    Left = 104
    Top = 272
  end
  object qryCheckCancel: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'KEYY'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DocGubun'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = Null
      end
      item
        Name = 'SEQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT'
      
        '    KEYY, DOC_GUBUN, SEQ, LINE_NO, HS_NO, DE_DATE, IMD_CODE1, IM' +
        'D_CODE2, IMD_CODE3, IMD_CODE4, SIZE1, SIZE2, SIZE3, SIZE4, '
      
        '    SIZE5, SIZE6, SIZE7, SIZE8, SIZE9, SIZE10, QTY, QTYC, TOTQTY' +
        ', TOTQTYC, PRICE, PRICE_G, PRICEC, CUX_RATE, SUP_AMT, SUP_AMTC, ' +
        'VB_TAX, VB_TAXC,'
      
        '    VB_AMT, VB_AMTC, SUP_TOTAMT, SUP_TOTAMTC, VB_TOTTAX, VB_TOTT' +
        'AXC, VB_TOTAMT, VB_TOTAMTC, BIGO1, BIGO2, BIGO3, BIGO4, BIGO5'#9
      'FROM APPSPC_D2 '
      'WHERE KEYY = :KEYY and DOC_GUBUN = :DocGubun  and SEQ =  :SEQ '
      ' ')
    Left = 152
    Top = 680
    object qryCheckCancelKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryCheckCancelDOC_GUBUN: TStringField
      FieldName = 'DOC_GUBUN'
      Size = 3
    end
    object qryCheckCancelSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryCheckCancelLINE_NO: TStringField
      FieldName = 'LINE_NO'
      Size = 8
    end
    object qryCheckCancelHS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 35
    end
    object qryCheckCancelDE_DATE: TStringField
      FieldName = 'DE_DATE'
      Size = 35
    end
    object qryCheckCancelIMD_CODE1: TStringField
      FieldName = 'IMD_CODE1'
      Size = 35
    end
    object qryCheckCancelIMD_CODE2: TStringField
      FieldName = 'IMD_CODE2'
      Size = 35
    end
    object qryCheckCancelIMD_CODE3: TStringField
      FieldName = 'IMD_CODE3'
      Size = 35
    end
    object qryCheckCancelIMD_CODE4: TStringField
      FieldName = 'IMD_CODE4'
      Size = 35
    end
    object qryCheckCancelSIZE1: TStringField
      FieldName = 'SIZE1'
      Size = 70
    end
    object qryCheckCancelSIZE2: TStringField
      FieldName = 'SIZE2'
      Size = 70
    end
    object qryCheckCancelSIZE3: TStringField
      FieldName = 'SIZE3'
      Size = 70
    end
    object qryCheckCancelSIZE4: TStringField
      FieldName = 'SIZE4'
      Size = 70
    end
    object qryCheckCancelSIZE5: TStringField
      FieldName = 'SIZE5'
      Size = 70
    end
    object qryCheckCancelSIZE6: TStringField
      FieldName = 'SIZE6'
      Size = 70
    end
    object qryCheckCancelSIZE7: TStringField
      FieldName = 'SIZE7'
      Size = 70
    end
    object qryCheckCancelSIZE8: TStringField
      FieldName = 'SIZE8'
      Size = 70
    end
    object qryCheckCancelSIZE9: TStringField
      FieldName = 'SIZE9'
      Size = 70
    end
    object qryCheckCancelSIZE10: TStringField
      FieldName = 'SIZE10'
      Size = 70
    end
    object qryCheckCancelQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryCheckCancelQTYC: TStringField
      FieldName = 'QTYC'
      Size = 3
    end
    object qryCheckCancelTOTQTY: TBCDField
      FieldName = 'TOTQTY'
      Precision = 18
    end
    object qryCheckCancelTOTQTYC: TStringField
      FieldName = 'TOTQTYC'
      Size = 3
    end
    object qryCheckCancelPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryCheckCancelPRICE_G: TBCDField
      FieldName = 'PRICE_G'
      Precision = 18
    end
    object qryCheckCancelPRICEC: TStringField
      FieldName = 'PRICEC'
      Size = 3
    end
    object qryCheckCancelCUX_RATE: TBCDField
      FieldName = 'CUX_RATE'
      Precision = 18
    end
    object qryCheckCancelSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      Precision = 18
    end
    object qryCheckCancelSUP_AMTC: TStringField
      FieldName = 'SUP_AMTC'
      Size = 3
    end
    object qryCheckCancelVB_TAX: TBCDField
      FieldName = 'VB_TAX'
      Precision = 18
    end
    object qryCheckCancelVB_TAXC: TStringField
      FieldName = 'VB_TAXC'
      Size = 3
    end
    object qryCheckCancelVB_AMT: TBCDField
      FieldName = 'VB_AMT'
      Precision = 18
    end
    object qryCheckCancelVB_AMTC: TStringField
      FieldName = 'VB_AMTC'
      Size = 3
    end
    object qryCheckCancelSUP_TOTAMT: TBCDField
      FieldName = 'SUP_TOTAMT'
      Precision = 18
    end
    object qryCheckCancelSUP_TOTAMTC: TStringField
      FieldName = 'SUP_TOTAMTC'
      Size = 3
    end
    object qryCheckCancelVB_TOTTAX: TBCDField
      FieldName = 'VB_TOTTAX'
      Precision = 18
    end
    object qryCheckCancelVB_TOTTAXC: TStringField
      FieldName = 'VB_TOTTAXC'
      Size = 3
    end
    object qryCheckCancelVB_TOTAMT: TBCDField
      FieldName = 'VB_TOTAMT'
      Precision = 18
    end
    object qryCheckCancelVB_TOTAMTC: TStringField
      FieldName = 'VB_TOTAMTC'
      Size = 3
    end
    object qryCheckCancelBIGO1: TStringField
      FieldName = 'BIGO1'
      Size = 70
    end
    object qryCheckCancelBIGO2: TStringField
      FieldName = 'BIGO2'
      Size = 70
    end
    object qryCheckCancelBIGO3: TStringField
      FieldName = 'BIGO3'
      Size = 70
    end
    object qryCheckCancelBIGO4: TStringField
      FieldName = 'BIGO4'
      Size = 70
    end
    object qryCheckCancelBIGO5: TStringField
      FieldName = 'BIGO5'
      Size = 70
    end
  end
  object QRCompositeReport1: TQRCompositeReport
    OnAddReports = QRCompositeReport1AddReports
    Options = []
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Orientation = poPortrait
    PrinterSettings.PaperSize = Letter
    Left = 48
    Top = 376
  end
  object spCopyAPPSPC: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'CopyAPPSPC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CopyDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@NewDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 48
    Top = 200
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APPSPC_H'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 104
    Top = 312
  end
  object popMenu1: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    Left = 16
    Top = 472
    object MenuItem16: TMenuItem
      Caption = #51204#49569#51456#48708
      ImageIndex = 29
      OnClick = sButton1Click
    end
    object MenuItem17: TMenuItem
      Caption = '-'
    end
    object MenuItem18: TMenuItem
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = btnEditClick
    end
    object N4: TMenuItem
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = btnDelClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object d1: TMenuItem
      Caption = #52636#47141#54616#44592
      object N8: TMenuItem
        Caption = #48120#47532#48372#44592
        ImageIndex = 15
        OnClick = btnPrintClick
      end
      object N9: TMenuItem
        Tag = 1
        Caption = #54532#47536#53944
        ImageIndex = 21
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = 'ADMIN'
      Enabled = False
      ImageIndex = 16
    end
  end
end
