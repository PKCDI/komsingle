// 개설업자 세금계산서 수신 -현재 동성화인텍만 사용하고있고
// 수혜업자와 개설업자에 구분이 애매해서 동성화인텍 기준으로 수혜업자만 사용하기로함

unit UI_VATBI1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, sSkinProvider, UI_VATBI_BP, DB, ADODB, StdCtrls,
  sComboBox, sCustomComboEdit, sCurrEdit, sCurrencyEdit, Grids, DBGrids,
  acDBGrid, sLabel, ExtCtrls, sBevel, sMemo, ComCtrls, sPageControl,
  Buttons, sBitBtn, sEdit, Mask, sMaskEdit, sButton, sSpeedButton, sPanel,
  sSplitter,TypeDefine, QuickRpt, Menus, sDialogs;

type
  TUI_VATBI1_frm = class(TUI_VATBI_BP_frm)
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListRE_NO: TStringField;
    qryListSE_NO: TStringField;
    qryListFS_NO: TStringField;
    qryListACE_NO: TStringField;
    qryListRFF_NO: TStringField;
    qryListSE_CODE: TStringField;
    qryListSE_SAUP: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_ADDR1: TStringField;
    qryListSE_ADDR2: TStringField;
    qryListSE_ADDR3: TStringField;
    qryListSE_UPTA: TStringField;
    qryListSE_UPTA1: TMemoField;
    qryListSE_ITEM: TStringField;
    qryListSE_ITEM1: TMemoField;
    qryListBY_CODE: TStringField;
    qryListBY_SAUP: TStringField;
    qryListBY_NAME1: TStringField;
    qryListBY_NAME2: TStringField;
    qryListBY_ADDR1: TStringField;
    qryListBY_ADDR2: TStringField;
    qryListBY_ADDR3: TStringField;
    qryListBY_UPTA: TStringField;
    qryListBY_UPTA1: TMemoField;
    qryListBY_ITEM: TStringField;
    qryListBY_ITEM1: TMemoField;
    qryListAG_CODE: TStringField;
    qryListAG_SAUP: TStringField;
    qryListAG_NAME1: TStringField;
    qryListAG_NAME2: TStringField;
    qryListAG_NAME3: TStringField;
    qryListAG_ADDR1: TStringField;
    qryListAG_ADDR2: TStringField;
    qryListAG_ADDR3: TStringField;
    qryListAG_UPTA: TStringField;
    qryListAG_UPTA1: TMemoField;
    qryListAG_ITEM: TStringField;
    qryListAG_ITEM1: TMemoField;
    qryListDRAW_DAT: TStringField;
    qryListDETAILNO: TBCDField;
    qryListSUP_AMT: TBCDField;
    qryListTAX_AMT: TBCDField;
    qryListREMARK: TStringField;
    qryListREMARK1: TMemoField;
    qryListAMT11: TBCDField;
    qryListAMT11C: TStringField;
    qryListAMT12: TBCDField;
    qryListAMT21: TBCDField;
    qryListAMT21C: TStringField;
    qryListAMT22: TBCDField;
    qryListAMT31: TBCDField;
    qryListAMT31C: TStringField;
    qryListAMT32: TBCDField;
    qryListAMT41: TBCDField;
    qryListAMT41C: TStringField;
    qryListAMT42: TBCDField;
    qryListINDICATOR: TStringField;
    qryListTAMT: TBCDField;
    qryListSUPTAMT: TBCDField;
    qryListTAXTAMT: TBCDField;
    qryListUSTAMT: TBCDField;
    qryListUSTAMTC: TStringField;
    qryListTQTY: TBCDField;
    qryListTQTYC: TStringField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListVAT_CODE: TStringField;
    qryListVAT_TYPE: TStringField;
    qryListNEW_INDICATOR: TStringField;
    qryListSE_ADDR4: TStringField;
    qryListSE_ADDR5: TStringField;
    qryListSE_SAUP1: TStringField;
    qryListSE_SAUP2: TStringField;
    qryListSE_SAUP3: TStringField;
    qryListSE_FTX1: TStringField;
    qryListSE_FTX2: TStringField;
    qryListSE_FTX3: TStringField;
    qryListSE_FTX4: TStringField;
    qryListSE_FTX5: TStringField;
    qryListBY_SAUP_CODE: TStringField;
    qryListBY_ADDR4: TStringField;
    qryListBY_ADDR5: TStringField;
    qryListBY_SAUP1: TStringField;
    qryListBY_SAUP2: TStringField;
    qryListBY_SAUP3: TStringField;
    qryListBY_FTX1: TStringField;
    qryListBY_FTX2: TStringField;
    qryListBY_FTX3: TStringField;
    qryListBY_FTX4: TStringField;
    qryListBY_FTX5: TStringField;
    qryListBY_FTX1_1: TStringField;
    qryListBY_FTX2_1: TStringField;
    qryListBY_FTX3_1: TStringField;
    qryListBY_FTX4_1: TStringField;
    qryListBY_FTX5_1: TStringField;
    qryListAG_ADDR4: TStringField;
    qryListAG_ADDR5: TStringField;
    qryListAG_SAUP1: TStringField;
    qryListAG_SAUP2: TStringField;
    qryListAG_SAUP3: TStringField;
    qryListAG_FTX1: TStringField;
    qryListAG_FTX2: TStringField;
    qryListAG_FTX3: TStringField;
    qryListAG_FTX4: TStringField;
    qryListAG_FTX5: TStringField;
    qryListSE_NAME3: TStringField;
    qryListBY_NAME3: TStringField;
    qryListINDICATOR_NAME: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TBCDField;
    qryGoodsDE_DATE: TStringField;
    qryGoodsNAME_COD: TStringField;
    qryGoodsNAME1: TMemoField;
    qryGoodsSIZE1: TMemoField;
    qryGoodsDE_REM1: TMemoField;
    qryGoodsQTY: TBCDField;
    qryGoodsQTY_G: TStringField;
    qryGoodsQTYG: TBCDField;
    qryGoodsQTYG_G: TStringField;
    qryGoodsPRICE: TBCDField;
    qryGoodsPRICE_G: TStringField;
    qryGoodsSUPAMT: TBCDField;
    qryGoodsTAXAMT: TBCDField;
    qryGoodsUSAMT: TBCDField;
    qryGoodsUSAMT_G: TStringField;
    qryGoodsSUPSTAMT: TBCDField;
    qryGoodsTAXSTAMT: TBCDField;
    qryGoodsUSSTAMT: TBCDField;
    qryGoodsUSSTAMT_G: TStringField;
    qryGoodsSTQTY: TBCDField;
    qryGoodsSTQTY_G: TStringField;
    qryGoodsRATE: TBCDField;
    sBitBtn26: TsBitBtn;
    QRCompositeReport1: TQRCompositeReport;
    spCopyVATBI1: TADOStoredProc;
    goodsMenu: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    sLabel16: TsLabel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton6: TsSpeedButton;
    qryReady: TADOQuery;
    popMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    N4: TMenuItem;
    N7: TMenuItem;
    d1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    GoodsExcelBtn: TsSpeedButton;
    GoodsSampleBtn: TsSpeedButton;
    excelOpen: TsOpenDialog;
    byAddPanel: TsPanel;
    sPanel6: TsPanel;
    edt_BYFTX1_1: TsEdit;
    edt_BYFTX2_1: TsEdit;
    edt_BYFTX3_1: TsEdit;
    edt_BYFTX4_1: TsEdit;
    edt_BYFTX5_1: TsEdit;
    sBitBtn24: TsBitBtn;
    sBitBtn25: TsBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryGoodsAfterScroll(DataSet: TDataSet);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sBitBtn60Click(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btn_goodsNewClick(Sender: TObject);
    procedure qryGoodsAfterInsert(DataSet: TDataSet);
    procedure btnSaveClick(Sender: TObject);
    procedure sBitBtn7Click(Sender: TObject);
    procedure sBitBtn25Click(Sender: TObject);
    procedure sBitBtn8Click(Sender: TObject);
    procedure curr_QTY_DChange(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn26Click(Sender: TObject);
    procedure curr_SUPAMTChange(Sender: TObject);
    procedure QRCompositeReport1AddReports(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure memo_NAME1_DChange(Sender: TObject);
    procedure memoLineLimit(Sender: TObject; var Key: Char);
    procedure goodsMenuPopup(Sender: TObject);
    procedure Page_controlChange(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure qryListCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure GoodsSampleBtnClick(Sender: TObject);
    procedure GoodsExcelBtnClick(Sender: TObject);
  private
    { Private declarations }
    VATBI1_SQL : string;
    procedure ReadDocument; override;
    procedure GoodsReadDocument; override;
    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;
    function ReadListBetween(fromDate: String; toDate: String; KeyValue : string): Boolean;

    procedure ButtonEnable(Val : Boolean=true);
    procedure GoodsButtonEnable(Val : Boolean=true);

    procedure NewDocument;
    procedure CancelDocument;
    procedure EditDocument;
    procedure DeleteDocument;
    procedure SaveDocument(Sender : TObject);
    procedure GoodsSaveDocument;

    function MAX_SEQ(TableName : String):Integer;//마지막순번
    function CHECK_VALIDITY:String; //유효성검사
    function RemoveSpecialChar(sSrc: string): string; //공란수

    procedure ReadyDocument(DocNo : String);
  public
    { Public declarations }
  protected
    ProgramControlType : TProgramControlType;
  end;

var
  UI_VATBI1_frm: TUI_VATBI1_frm;

implementation

{$R *.dfm}

uses MessageDefine, DateUtils, Commonlib, StrUtils, MSSQL, AutoNo, Dlg_Customer,
     VarDefine, SQLCreator, KISCalendar, Dlg_ErrorMessage, VATBI1_PRINT, Dialog_CopyVATBI1,
  DocumentSend, Dlg_RecvSelect, CreateDocuments, ComObj, excel2000, Dlg_VATBIL_NewSelect, dlg_CopyFromVATBIL;

procedure TUI_VATBI1_frm.FormActivate(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;

  if not DMMssql.KISConnect.InTransaction then
  begin
    qryList.Close;
    qryList.Open;
  end;
end;

procedure TUI_VATBI1_frm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
//
end;

procedure TUI_VATBI1_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  UI_VATBI1_frm := nil;
end;

procedure TUI_VATBI1_frm.FormCreate(Sender: TObject);
begin
  inherited;
  VATBI1_SQL := qryList.SQL.Text;
  ProgramControlType := ctView;
end;

procedure TUI_VATBI1_frm.ReadDocument;
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;
  //----------------------------------------------------------------------------
  //기본정보
  //----------------------------------------------------------------------------
    //관리번호
    edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
    //신청일자
    mask_DATEE.Text := qryListDATEE.AsString;
    //사용자
    edt_USER_ID.Text := qryListUSER_ID.AsString;
    //문서기능
    edt_msg1.Text := qryListMESSAGE1.AsString;
    //유형
    edt_msg2.Text := qryListMESSAGE2.AsString;
  //----------------------------------------------------------------------------
  //문서정보
  //----------------------------------------------------------------------------
    //세금계산서코드
    edt_VATCODE.Text := qryListVAT_CODE.AsString;
    //세금계산서종류
    edt_VATTYPE.Text := qryListVAT_TYPE.AsString;
    //수정사유코드
    edt_NEWINDICATOR.Text := qryListNEW_INDICATOR.AsString;
    //책번호 - 권
    edt_RENO.Text := qryListRE_NO.AsString;
    //책번호 - 호
    edt_SENO.Text := qryListSE_NO.AsString;
    //일련번호
    edt_FSNO.Text := qryListFS_NO.AsString;
    //관련참조번호
    edt_ACENO.Text := qryListACE_NO.AsString;
    //세금계산서번호
    edt_RFFNO.Text := qryListRFF_NO.AsString;
    //--------------------------------------------------------------------------
    //공급자
    //--------------------------------------------------------------------------
      //코드
      edt_SECODE.Text := qryListSE_CODE.AsString;
      //사업자번호
      mask_SESAUP.Text := qryListSE_SAUP.AsString;
      //대표자명
      edt_SENAME3.Text := qryListSE_NAME3.AsString;
      //상호
      edt_SENAME1.Text := qryListSE_NAME1.AsString;
      edt_SENAME2.Text := qryListSE_NAME2.AsString;
      //주소
      edt_SEADDR1.Text := qryListSE_ADDR1.AsString;
      edt_SEADDR2.Text := qryListSE_ADDR2.AsString;
      edt_SEADDR3.Text := qryListSE_ADDR3.AsString;
      edt_SEADDR4.Text := qryListSE_ADDR4.AsString;
      edt_SEADDR5.Text := qryListSE_ADDR5.AsString;
      //종사업장번호
      edt_SESAUP1.Text := qryListSE_SAUP1.AsString;
      //담당부서명
      edt_SEFTX1.Text := qryListSE_FTX1.AsString;
      //담당자명
      edt_SEFTX2.Text := qryListSE_FTX2.AsString;
      //전화번호
      edt_SEFTX3.Text := qryListSE_FTX3.AsString;
      //이메일
      edt_SEFTX4.Text := qryListSE_FTX4.AsString;
      //도메인
      edt_SEFTX5.Text := qryListSE_FTX5.AsString;
      //업태
      memo_SEUPTA1.Text := qryListSE_UPTA1.AsString;
      //종목
      memo_SEITEM1.Text := qryListSE_ITEM1.AsString;
    //--------------------------------------------------------------------------
    //공급받는자
    //--------------------------------------------------------------------------
      //코드
      edt_BYCODE.Text := qryListBY_CODE.AsString;
      //사업자등록구분
      edt_BYSAUPCODE.Text := qryListBY_SAUP_CODE.AsString;
      //등록번호
      mask_BYSAUP.Text := qryListBY_SAUP.AsString;
      //대표자명
      edt_BYNAME3.Text := qryListBY_NAME3.AsString;
      //상호
      edt_BYNAME1.Text := qryListBY_NAME1.AsString;
      edt_BYNAME2.Text := qryListBY_NAME2.AsString;
      //주소
      edt_BYADDR1.Text := qryListBY_ADDR1.AsString;
      edt_BYADDR2.Text := qryListBY_ADDR2.AsString;
      edt_BYADDR3.Text := qryListBY_ADDR3.AsString;
      edt_BYADDR4.Text := qryListBY_ADDR4.AsString;
      edt_BYADDR5.Text := qryListBY_ADDR5.AsString;
      //종사업자번호
      edt_BYSAUP1.Text := qryListBY_SAUP1.AsString;
      //담당부서명
      edt_BYFTX1.Text := qryListBY_FTX1.AsString;
      //담당자명
      edt_BYFTX2.Text := qryListBY_FTX2.AsString;
      //전화번호
      edt_BYFTX3.Text := qryListBY_FTX3.AsString;
      //이메일
      edt_BYFTX4.Text := qryListBY_FTX4.AsString;
      edt_BYFTX5.Text := qryListBY_FTX5.AsString;
      //담당부서명(2)
      edt_BYFTX1_1.Text := qryListBY_FTX1_1.AsString;
      //담당자명(2)
      edt_BYFTX2_1.Text := qryListBY_FTX2_1.AsString;
      //전화번호(2)
      edt_BYFTX3_1.Text := qryListBY_FTX3_1.AsString;
      //이메일(2)
      edt_BYFTX4_1.Text := qryListBY_FTX4_1.AsString;
      edt_BYFTX5_1.Text := qryListBY_FTX5_1.AsString;
      //업태
      memo_BYUPTA1.Text := qryListBY_UPTA1.AsString;
      //종목
      memo_BYITEM1.Text := qryListBY_ITEM1.AsString;
  //----------------------------------------------------------------------------
  //금액 및 수탁자
  //----------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //금액
    //--------------------------------------------------------------------------
      //작성일자
      mask_DRAWDAT.Text := qryListDRAW_DAT.AsString;
      //공급가액
      curr_SUPAMT.Value := qryListSUP_AMT.AsCurrency;
      //세액
      curr_TAXAMT.Value := qryListTAX_AMT.AsCurrency;
      //공란수
      curr_DETAILNO.Value := qryListDETAILNO.AsCurrency;
      //비고
      memo_REMARK1.Text := qryListREMARK1.AsString;
      //현금외화
      curr_AMT11.Value := qryListAMT11.AsCurrency;
      //현금외화 (통화)
      edt_AMT11C.Text := qryListAMT11C.AsString;
      //현금
      curr_AMT12.Value := qryListAMT12.AsCurrency;
      //수표외화
      curr_AMT21.Value := qryListAMT21.AsCurrency;
      //수표외화 (통화)
      edt_AMT21C.Text := qryListAMT21C.AsString;
      //수표
      curr_AMT22.Value := qryListAMT22.AsCurrency;
      //어음외화
      curr_AMT31.Value := qryListAMT31.AsCurrency;
      //어음외화 (통화)
      edt_AMT31C.Text := qryListAMT31C.AsString;
      //어음
      curr_AMT32.Value := qryListAMT32.AsCurrency;
      //외상미수금외화
      curr_AMT41.Value := qryListAMT41.AsCurrency;
      //외상미수금 외화 (통화)
      edt_AMT41C.Text := qryListAMT41C.AsString;
      //외상미수금
      curr_AMT42.Value := qryListAMT42.AsCurrency;
      //영수/청구
      edt_INDICATOR.Text := qryListINDICATOR.AsString;
      //영수 / 청구 name
      edt_INDICATORNAME.Text := qryListINDICATOR_NAME.AsString;
      //총금액
      curr_TAMT.Value := qryListTAMT.AsCurrency;
      //총공급가액
      curr_SUPTAMT.Value := qryListSUP_AMT.AsCurrency;
      //총외화공금가액단위
      edt_USTAMTC.Text := qryListUSTAMTC.AsString;
      //총외화공급가액
      curr_USTAMT.Value := qryListUSTAMT.AsCurrency;
      //총수량단위
      edt_TQTYC.Text := qryListTQTYC.AsString;
      //총수량
      curr_TQTY.Value := qryListTQTY.AsCurrency;
      //총세액
      curr_TAXTAMT.Value := qryListTAXTAMT.AsCurrency;
    //--------------------------------------------------------------------------
    //수탁자
    //--------------------------------------------------------------------------
      //수탁자
      edt_AGCODE.Text := qryListAG_CODE.AsString;
      //사업자등록번호
      mask_AGSAUP.Text := qryListAG_SAUP.AsString;
      //대표자명
      edt_AGNAME3.Text := qryListAG_NAME3.AsString;
      //상호
      edt_AGNAME1.Text := qryListAG_NAME1.AsString;
      edt_AGNAME2.Text := qryListAG_NAME2.AsString;
      //주소
      edt_AGADDR1.Text := qryListAG_ADDR1.AsString;
      edt_AGADDR2.Text := qryListAG_ADDR2.AsString;
      edt_AGADDR3.Text := qryListAG_ADDR3.AsString;
      edt_AGADDR4.Text := qryListAG_ADDR4.AsString;
      edt_AGADDR5.Text := qryListAG_ADDR5.AsString;
      //종사업장번호
      edt_AGSAUP1.Text := qryListAG_SAUP1.AsString;
      //담당부서명
      edt_AGFTX1.Text := qryListAG_FTX1.AsString;
      //담당자명
      edt_AGFTX2.Text := qryListAG_FTX2.AsString;
      //전화번호
      edt_AGFTX3.Text := qryListAG_FTX3.AsString;
      //이메일
      edt_AGFTX4.Text := qryListAG_FTX4.AsString;
      edt_AGFTX5.Text := qryListAG_FTX5.AsString;
      //업태
      memo_AGUPTA1.Text := qryListAG_UPTA1.AsString;
      //종목
      memo_AGITEM1.Text := qryListAG_ITEM1.AsString;
end;

procedure TUI_VATBI1_frm.GoodsReadDocument;
begin
  inherited;
//------------------------------------------------------------------------------
//품명 / 규격 / REMARK
//------------------------------------------------------------------------------
  //품명코드
  edt_NAMECOD_D.Text := qryGoodsNAME_COD.AsString;
  //품명
  memo_NAME1_D.Text := qryGoodsNAME1.AsString;
  //규격
  memo_SIZE1_D.Text := qryGoodsSIZE1.AsString;
  //REMARK1
  memo_DEREM1_D.Text := qryGoodsDE_REM1.AsString;
  //공급일자
  mask_DEDATE_D.Text := qryGoodsDE_DATE.AsString;
  //환율
  curr_RATE_D.Value := qryGoodsRATE.AsCurrency;
//------------------------------------------------------------------------------
//수량 / 단가 / 금액
//-----------------------------------------------------------------------------
  //수량단위
  edt_QTY_G_D.Text := qryGoodsQTY_G.AsString;
  //수량
  curr_QTY_D.Value := qryGoodsQTY.AsCurrency;
  //수량소계단위
  edt_STQTY_G_D.Text := qryGoodsSTQTY_G.AsString;
  //수량소계
  curr_STQTY_D.Value := qryGoodsSTQTY.AsCurrency;
  //단가
  curr_PRICE_D.Value := qryGoodsPRICE.AsCurrency;
  //단가기준수량단위
  edt_QTYG_G_D.Text := qryGoodsQTYG_G.AsString;
  //단가기준수량
  curr_QTYG_D.Value := qryGoodsQTYG.AsCurrency;
  //원화공급가액
  curr_SUPAMT_D.Value := qryGoodsSUPAMT.AsCurrency;
  //원화공급가액소계
  curr_SUPSTAMT_D.Value := qryGoodsSUPSTAMT.AsCurrency;
  //세액
  curr_TAXAMT_D.Value := qryGoodsTAXAMT.AsCurrency;
  //세액소계
  curr_TAXSTAMT_D.Value := qryGoodsTAXSTAMT.AsCurrency;
  //외화공급가액단위
  edt_USAMT_G_D.Text := qryGoodsUSAMT_G.AsString;
  //외화공급가액
  curr_USAMT_D.Value := qryGoodsUSAMT.AsCurrency;
  //외화공급가액 소계단위
  edt_USSTAMT_G_D.Text := qryGoodsUSSTAMT_G.AsString;
  //외화공급가액 소계
  curr_USSTAMT_D.Value := qryGoodsUSSTAMT.AsCurrency;
end;

procedure TUI_VATBI1_frm.Readlist(OrderSyntax: string);
begin
if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if Page_control.ActivePageIndex = 3 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := VATBI1_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 :
      begin
        SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :
      begin
        SQL.Add(' WHERE BY_NAME1 LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add(' ORDER BY DATEE ');
    end;

    Open;
  end;
end;

function TUI_VATBI1_frm.Readlist(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
  Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := VATBI1_SQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
   //SQL.Add(' ORDER BY DATEE ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_VATBI1_frm.FormShow(Sender: TObject);
begin
  inherited;
  Page_control.ActivePageIndex := 0;

  mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  ProgramControlType := ctView;

//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4);
  //문서정보
  EnabledControlValue(taxPanel);
  EnabledControlValue(providerPanel);
  EnabledControlValue(buyerPanel);
  //품목내역    
  EnabledControlValue(goodsPanel);
  //금맥 및 수탁자
  EnabledControlValue(moneyPanel);
  EnabledControlValue(trusteePanel);
  EnabledControlValue(sPanel57, False);

  ReadList(Mask_fromDate.Text,mask_toDate.Text,'');

  //메모 행열출력 lable
  sLabel5.Caption := '';
  sLabel6.Caption := '';
  sLabel7.Caption := '';
  sLabel9.Caption := '';
end;

procedure TUI_VATBI1_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
                                   
  with qryGoods do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
    Open;
  end;

  GoodsReadDocument;

  //메모 행렬 출력
  sLabel5.Caption := '';
  sLabel6.Caption := '';
  sLabel7.Caption := '';
  sLabel9.Caption := '';
end;

procedure TUI_VATBI1_frm.qryGoodsAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryGoods.State = dsBrowse then GoodsReadDocument;
end;

procedure TUI_VATBI1_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;
begin
  inherited;
  nIndex := StrToIntDef( qryListCHK2.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TUI_VATBI1_frm.sBitBtn60Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text]) Then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text);
end;

procedure TUI_VATBI1_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
  if qryList.RecordCount = 0 then ReadDocument;

end;

procedure TUI_VATBI1_frm.NewDocument;
begin
  inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctInsert;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Append;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
  GoodsButtonEnable(True);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  Page_control.ActivePageIndex := 0;
  Page_controlChange(Page_control);
//sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
  sDBGrid8.Enabled := False;
//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
    EnabledControlValue(sPanel4 , False);
  //----------------------------------------------------------------------------
  //문서정보
  //----------------------------------------------------------------------------
    ClearControlValue(taxPanel);
    ClearControlValue(providerPanel);
    ClearControlValue(buyerPanel);
    ClearControlValue(byAddPanel);
    EnabledControlValue(taxPanel , False);
    EnabledControlValue(providerPanel , False);
    EnabledControlValue(buyerPanel , False);
    EnabledControlValue(sPanel57);

  //----------------------------------------------------------------------------
  //품목내역
  //----------------------------------------------------------------------------
    ClearControlValue(goodsPanel);

  //----------------------------------------------------------------------------
  //금액 및 수탁자
  //----------------------------------------------------------------------------
    ClearControlValue(moneyPanel);
    ClearControlValue(trusteePanel);

    EnabledControlValue(moneyPanel , False);
    EnabledControlValue(trusteePanel , False);

//------------------------------------------------------------------------------
//기초자료
//------------------------------------------------------------------------------
  //관리번호
  edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPSPC');
  //등록일자
  mask_DATEE.Text := FormatDateTime('YYYYMMDD',Now);
  //사용자
  edt_USER_ID.Text := LoginData.sID;
  //문서기능
  edt_msg1.Text := '9';
  //유형
  edt_msg2.Text := 'AB';
  //작성일자
  mask_DRAWDAT.Text := FormatDateTime('YYYYMMDD',Now);

  //------------------------------------------------------------------------------
  //신청인 정보
  //------------------------------------------------------------------------------
  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try
    with Dlg_Customer_frm do
    begin
      if isValue('00000') then
        edt_SECODE.Text := DataSource1.DataSet.FieldByName('CODE').AsString;
        mask_SESAUP.Text := DataSource1.DataSet.FieldByName('SAUP_NO').AsString;
        edt_SENAME3.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString;
        edt_SENAME1.Text := DataSource1.DataSet.FieldByName('ENAME').AsString;
        edt_SEADDR1.Text := DataSource1.DataSet.FieldByName('ADDR1').AsString;
        edt_SEADDR2.Text := DataSource1.DataSet.FieldByName('ADDR2').AsString;
        edt_SEADDR3.Text := DataSource1.DataSet.FieldByName('ADDR3').AsString;  
        edt_SEFTX4.Text := DataSource1.DataSet.FieldByName('EMAIL_ID').AsString;
        edt_SEFTX5.Text := DataSource1.DataSet.FieldByName('EMAIL_DOMAIN').AsString;
    end;

  finally
    FreeAndNil(Dlg_Customer_frm);
  end;

end;

procedure TUI_VATBI1_frm.ButtonEnable(Val: Boolean);
begin
  btnNew.Enabled := Val;
  btnEdit.Enabled := Val;
  btnDel.Enabled := Val;

  btnTemp.Enabled := not Val;
  btnSave.Enabled := not Val;
  btnCancel.Enabled := not Val;

  btnCopy.Enabled := Val;
  btnPrint.Enabled := Val;
end;

procedure TUI_VATBI1_frm.GoodsButtonEnable(Val: Boolean);
begin
  btn_goodsNew.Enabled := Val;
  btn_goodsMod.Enabled := Val;
  btn_goodsDel.Enabled := Val;
  btn_goodsSave.Enabled := not Val;
  btn_goodsCancel.Enabled := not Val;

  GoodsExcelBtn.Enabled := Val;
  GoodsSampleBtn.Enabled := Val;
end;

procedure TUI_VATBI1_frm.btnNewClick(Sender: TObject);
var
  iChoiceType:Integer;
begin
  inherited;


  Dlg_VATBIL_NewSelect_frm := TDlg_VATBIL_NewSelect_frm.Create(Self);
  try
    iChoiceType := Dlg_VATBIL_NewSelect_frm.ShowModal;
    if iChoiceType = mrOk then
    begin
      dlg_CopyFromVATBIL_frm := Tdlg_CopyFromVATBIL_frm.Create(Self);
      try
        NewDocument;
        if dlg_CopyFromVATBIL_frm.ShowModal = mrOk then
        begin
          with dlg_CopyFromVATBIL_frm.qryList do
          begin
            //세금계산서 항목
            edt_VATCODE.Text := FieldByName('VAT_CODE').AsString;
            edt_VATTYPE.Text := FieldByName('VAT_TYPE').AsString;
            edt_NEWINDICATOR.Text := FieldByName('NEW_INDICATOR').AsString;
            edt_RENO.Text := FieldByName('RE_NO').AsString;
            edt_SENO.Text := FieldByName('SE_NO').AsString;
            edt_FSNO.Text := FieldByName('FS_NO').AsString;
            edt_ACENO.Text := FieldByName('ACE_NO').AsString;
            edt_RFFNO.Text := FieldByName('RFF_NO').AsString;
            //공급자 항목
            edt_SECODE.Text := FieldByName('SE_CODE').AsString; //공급자코드
            mask_SESAUP.Text := FieldByName('SE_SAUP').AsString; //공급자 사업자등록번호
            edt_SENAME1.text := FieldByName('SE_NAME1').AsString; //공급자 상호
            edt_SENAME2.text := FieldByName('SE_NAME2').AsString;
            edt_SENAME3.text := FieldByName('SE_NAME3').AsString; //공급자 대표자명
            edt_SEADDR1.text := FieldByName('SE_ADDR1').AsString; //공급자 주소
            edt_SEADDR2.text := FieldByName('SE_ADDR2').AsString;
            edt_SEADDR3.text := FieldByName('SE_ADDR3').AsString;
            edt_SEADDR4.text := FieldByName('SE_ADDR4').AsString;
            edt_SEADDR5.text := FieldByName('SE_ADDR5').AsString;
            edt_SESAUP1.Text := FieldByName('SE_SAUP1').AsString; //종사업장번호
            edt_SEFTX1.Text := FieldByName('SE_FTX1').AsString; // 담당부서
            edt_SEFTX2.Text := FieldByName('SE_FTX2').AsString; // 담당자명
            edt_SEFTX3.Text := FieldByName('SE_FTX3').AsString; // 전화번호
            edt_SEFTX4.Text := FieldByName('SE_FTX4').AsString; // 이메일 아이디
            edt_SEFTX5.Text := FieldByName('SE_FTX5').AsString; // 이메일 도메인
            memo_SEUPTA1.Text := FieldByName('SE_UPTA1').AsString; // 공급자 업태
            memo_SEITEM1.Text := FieldByName('SE_ITEM1').AsString; // 공급자 종목

            //공급받는자 항목  
            edt_BYCODE.Text := FieldByName('BY_CODE').AsString; //공급받는자코드
            mask_BYSAUP.Text := FieldByName('BY_SAUP').AsString; //공급받는자 사업자등록번호
            edt_BYNAME1.text := FieldByName('BY_NAME1').AsString; //공급받는자 상호
            edt_BYNAME2.text := FieldByName('BY_NAME2').AsString;
            edt_BYNAME3.text := FieldByName('BY_NAME3').AsString; //공급받는자 대표자명
            edt_BYADDR1.text := FieldByName('BY_ADDR1').AsString; //공급받는자 주소
            edt_BYADDR2.text := FieldByName('BY_ADDR2').AsString;
            edt_BYADDR3.text := FieldByName('BY_ADDR3').AsString;
            edt_BYADDR4.text := FieldByName('BY_ADDR4').AsString;
            edt_BYADDR5.text := FieldByName('BY_ADDR5').AsString;
            edt_BYSAUP1.Text := FieldByName('BY_SAUP1').AsString; //종사업장번호
            edt_BYFTX1.Text := FieldByName('BY_FTX1').AsString; // 담당부서
            edt_BYFTX2.Text := FieldByName('BY_FTX2').AsString; // 담당자명
            edt_BYFTX3.Text := FieldByName('BY_FTX3').AsString; // 전화번호
            edt_BYFTX4.Text := FieldByName('BY_FTX4').AsString; // 이메일 아이디
            edt_BYFTX5.Text := FieldByName('BY_FTX5').AsString; // 이메일 도메인
            memo_BYUPTA1.Text := FieldByName('BY_UPTA1').AsString; // 공급받는자 업태
            memo_BYITEM1.Text := FieldByName('BY_ITEM1').AsString; // 공급받는자 종목
            //상품내역(디테일)

            //금액
            mask_DRAWDAT.Text := FieldByName('DRAW_DAT').AsString;//작성일자
            curr_SUPAMT.Value := FieldByName('SUP_AMT').AsFloat;  //공급가액
            curr_TAXAMT.Value := FieldByName('TAX_AMT').AsFloat;  //세액
            curr_DETAILNO.Value := FieldByName('DETAILNO').AsInteger;  //공란수
            memo_REMARK1.Text := FieldByName('REMARK1').AsString; //비고
            curr_AMT12.Value := FieldByName('AMT12').AsFloat; //현금
            curr_AMT22.Value := FieldByName('AMT22').AsFloat; //수표
            curr_AMT32.Value := FieldByName('AMT32').AsFloat; //어음
            curr_AMT42.Value := FieldByName('AMT42').AsFloat; //외상미수금
            curr_AMT11.Value := FieldByName('AMT11').AsFloat; //현금(외화)
            edt_AMT11C.Text := FieldByName('AMT11C').AsString; //통화단위
            curr_AMT21.Value := FieldByName('AMT21').AsFloat;  //수표(외화)
            edt_AMT21C.Text := FieldByName('AMT21C').AsString; //통화단위
            curr_AMT31.Value := FieldByName('AMT31').AsFloat;  //어음(외화)
            edt_AMT31C.Text := FieldByName('AMT31C').AsString; //통화단위
            curr_AMT41.Value := FieldByName('AMT41').AsFloat;  //외상미수금(외화)
            edt_AMT41C.Text := FieldByName('AMT41C').AsString; //통화단위
            edt_INDICATOR.Text := FieldByName('INDICATOR').AsString; //영수/청구
            curr_TAMT.Value := FieldByName('TAMT').AsFloat; //총금액
            curr_USTAMT.Value := FieldByName('USTAMT').AsFloat; //총외화공급가액
            edt_USTAMTC.Text := FieldByName('USTAMTC').AsString; //단위
            curr_TAXTAMT.Value := FieldByName('TAXTAMT').AsFloat; //총세액
            curr_SUPTAMT.Value := FieldByName('SUPTAMT').AsFloat; //총공급가액
            curr_TQTY.Value := FieldByName('TQTY').AsFloat; //총수량
            edt_TQTYC.Text := FieldByName('TQTYC').AsString; //단위

            //수탁자 항목  
            edt_AGCODE.Text := FieldByName('AG_CODE').AsString; //수탁자코드
            mask_AGSAUP.Text := FieldByName('AG_SAUP').AsString; //수탁자 사업자등록번호
            edt_AGNAME1.text := FieldByName('AG_NAME1').AsString; //수탁자 상호
            edt_AGNAME2.text := FieldByName('AG_NAME2').AsString;
            edt_AGNAME3.text := FieldByName('AG_NAME3').AsString; //수탁자 대표자명
            edt_AGADDR1.text := FieldByName('AG_ADDR1').AsString; //수탁자 주소
            edt_AGADDR2.text := FieldByName('AG_ADDR2').AsString;
            edt_AGADDR3.text := FieldByName('AG_ADDR3').AsString;
            edt_AGADDR4.text := FieldByName('AG_ADDR4').AsString;
            edt_AGADDR5.text := FieldByName('AG_ADDR5').AsString;
            edt_AGSAUP1.Text := FieldByName('AG_SAUP1').AsString; //종사업장번호
            edt_AGFTX1.Text := FieldByName('AG_FTX1').AsString; // 담당부서
            edt_AGFTX2.Text := FieldByName('AG_FTX2').AsString; // 담당자명
            edt_AGFTX3.Text := FieldByName('AG_FTX3').AsString; // 전화번호
            edt_AGFTX4.Text := FieldByName('AG_FTX4').AsString; // 이메일 아이디
            edt_AGFTX5.Text := FieldByName('AG_FTX5').AsString; // 이메일 도메인
            memo_AGUPTA1.Text := FieldByName('AG_UPTA1').AsString; // 공급받는자 업태
            memo_AGITEM1.Text := FieldByName('AG_ITEM1').AsString; // 공급받는자 종목

            dlg_CopyFromVATBIL_frm.qryCopy.Close;
            dlg_CopyFromVATBIL_frm.qryCopy.Parameters[0].Value := edt_MAINT_NO.Text;
            dlg_CopyFromVATBIL_frm.qryCopy.Parameters[1].Value := FieldByName('MAINT_NO').AsString;
//            dlg_CopyFromVATBIL_frm.qryCopy.Open;
//            ShowMessage(IntToStr( dlg_CopyFromVATBIL_frm.qryCopy.RecordCount));
            if (dlg_CopyFromVATBIL_frm.qryCopy.ExecSQL > 0) then
            begin
//              ShowMessage(dlg_CopyFromVATBIL_frm.qryCopy.SQL.Text);
              qryGoods.Close;
              qryGoods.Parameters[0].Value := edt_MAINT_NO.Text;
              qryGoods.Open;
            end;
          end;

        end
        else
          btnCancelClick(Self);
      finally
        dlg_CopyFromVATBIL_frm.Close;
        FreeAndNil(dlg_CopyFromVATBIL_frm);
      end;
    end    
    else if iChoiceType = mrYes then
    begin
        NewDocument;
    end
    else if iChoiceType = mrCancel then
    begin
      btnCancelClick(Self);
    end;

  finally
    Dlg_VATBIL_NewSelect_frm.Close;
    Dlg_VATBIL_NewSelect_frm.Free;
  end;

end;

procedure TUI_VATBI1_frm.CancelDocument;
begin
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(True);
  EnabledControlValue(goodsBtnPanel);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  Page_control.ActivePageIndex := 0;
  Page_controlChange(Page_control);
  sDBGrid1.Enabled := True;
  sDBGrid2.Enabled := True;
  sDBGrid8.Enabled := True;

//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4);
  EnabledControlValue(taxPanel);
  EnabledControlValue(providerPanel);
  EnabledControlValue(buyerPanel);
  EnabledControlValue(goodsPanel);
  EnabledControlValue(moneyPanel);
  EnabledControlValue(trusteePanel);
  EnabledControlValue(sPanel57 , False);

  ClearControlValue(taxPanel);
  ClearControlValue(providerPanel);
  ClearControlValue(buyerPanel);
  ClearControlValue(goodsPanel);
  ClearControlValue(moneyPanel);
  ClearControlValue(trusteePanel);


  //트랜잭션 롤백
  if DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.RollbackTrans;
end;

procedure TUI_VATBI1_frm.btnCancelClick(Sender: TObject);
var
  CreateDate : TDateTime;
begin
  inherited;
  CancelDocument;

  //새로고침
  CreateDate := ConvertStr2Date(mask_DATEE.Text);
  Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
  FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
  edt_MAINT_NO.Text);

end;

procedure TUI_VATBI1_frm.EditDocument;
begin
 inherited;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctModify;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Edit;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
  GoodsButtonEnable(True);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  Page_control.ActivePageIndex := 0;
  Page_controlChange(Page_control);
//sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
  sDBGrid8.Enabled := False;
//------------------------------------------------------------------------------
//  데이터제어
//------------------------------------------------------------------------------
    EnabledControlValue(sPanel4 , False);
    EnabledControlValue(taxPanel , False);
    EnabledControlValue(providerPanel , False);
    EnabledControlValue(buyerPanel , False);
    EnabledControlValue(moneyPanel , False);
    EnabledControlValue(trusteePanel , False);
    EnabledControlValue(sPanel57);
end;

procedure TUI_VATBI1_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  if AnsiMatchText(qryListCHK2.AsString,['','0','1','2','5','6']) then
  begin
    EditDocument;
  end
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_EDIT,'수정불가', MB_OK+MB_ICONINFORMATION);
end;

procedure TUI_VATBI1_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;


        SQL.Text :=  'DELETE FROM VATBI1_H WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM VATBI1_D WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;


        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    Close;
    Free;
   end;
  end;

end;

procedure TUI_VATBI1_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;
end;

procedure TUI_VATBI1_frm.btn_goodsNewClick(Sender: TObject);
begin
  inherited;
  
  case (Sender as TsSpeedButton).Tag of
    //품목내역 입력
    1 :
      begin
        sDBGrid1.Enabled := False;
        ClearControlValue(goodsPanel);
        qryGoods.Append;
      end;
    //물품수령증명서 수정
    2 :
      begin
        if qryGoods.RecordCount > 0 then
        begin
          sDBGrid1.Enabled := False;
          qryGoods.Edit;
        end;
      end;
    //물품수령증명서 삭제
    3 :
      begin
        if qryGoods.RecordCount = 0 then
        begin
          MessageBox(Self.Handle,MSG_SYSTEM_DEL_CANCEL,'품목내역 삭제확인',MB_OK+MB_ICONQUESTION)
        end
        else
        BEGIN
          IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'품목내역 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
          qryGoods.Delete;
        end;
      end;
    //물품수령증명서 저장
    4 :
      begin
        GoodsSaveDocument();
        qryGoods.Post;

        sDBGrid1.Enabled := True;
        GoodsButtonEnable(True);
        EnabledControlValue(goodsPanel);
      end;
    //물품수령증명서 취소
    5 :
      begin
        qryGoods.Cancel;

        sDBGrid1.Enabled := True;
        GoodsButtonEnable(True);
        EnabledControlValue(goodsPanel);

        if qryGoods.State in [dsInsert] then
          ClearControlValue(goodsPanel);
      end;
  end;
end;

procedure TUI_VATBI1_frm.qryGoodsAfterInsert(DataSet: TDataSet);
begin
  inherited;
  GoodsButtonEnable(False);
  EnabledControlValue(goodsPanel , False);
end;

procedure TUI_VATBI1_frm.SaveDocument(Sender: TObject);
var
  SQLCreate : TSQLCreate;
  DocNo : String;
  CreateDate : TDateTime;
begin

  try
    //--------------------------------------------------------------------------
    // 유효성 검사
    //--------------------------------------------------------------------------
    if (Sender as TsButton).Tag = 1 then
    begin
      Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
      if Dlg_ErrorMessage_frm.Run_ErrorMessage( '세금계산서' ,CHECK_VALIDITY ) Then
      begin
        FreeAndNil(Dlg_ErrorMessage_frm);
        Exit;
      end;
    end;

  //----------------------------------------------------------------------------
  // SQL생성기
  //----------------------------------------------------------------------------
    SQLCreate := TSQLCreate.Create;

        with SQLCreate do
        begin
          DocNo := edt_MAINT_NO.Text;

        //----------------------------------------------------------------------
        // 문서헤더
        //----------------------------------------------------------------------

          Case ProgramControlType of
            ctInsert :
            begin
              DMLType := dmlInsert;
              ADDValue('MAINT_NO',DocNo);
            end;

            ctModify :
            begin
              DMLType := dmlUpdate;
              ADDWhere('MAINT_NO',DocNo);
            end;
          end;

          SQLHeader('VATBI1_H');

           //문서저장구분(0:임시, 1:저장)
          ADDValue('CHK2' , IntToStr((Sender as TsButton).Tag));
          //등록일자
          ADDValue('DATEE' , mask_DATEE.Text );
          //사용자
          ADDValue('USER_ID' , edt_USER_ID.Text );
          //문서기능
          ADDValue('MESSAGE1' , edt_msg1.Text );
          //유형
          ADDValue('MESSAGE2' , edt_msg2.Text );
          //세금계산서코드
          ADDValue('VAT_CODE' , edt_VATCODE.Text );
          //세금계산서종류
          ADDValue('VAT_TYPE' , edt_VATTYPE.Text );
          //수정사유코드
          ADDValue('NEW_INDICATOR' , edt_NEWINDICATOR.Text );
          //책번호 - 권
          ADDValue('RE_NO' , edt_RENO.Text );
          //책번호 - 호
          ADDValue('SE_NO' , edt_SENO.Text );
          //일련번호
          ADDValue('FS_NO' , edt_FSNO.Text );
          //관련참조번호
          ADDValue('ACE_NO' , edt_ACENO.Text );
          //(-)세금계산서번호
          ADDValue('RFF_NO' , edt_RFFNO.Text );
        //--------------------------------------------------------------------
        //공급자
        //--------------------------------------------------------------------
          //공급자코드
          ADDValue('SE_CODE' , edt_SECODE.Text );
          //사업자등록번호
          ADDValue('SE_SAUP' , mask_SESAUP.Text );
          //대표자명
          ADDValue('SE_NAME3' , edt_SENAME3.Text );
          //상호
          ADDValue('SE_NAME1' , edt_SENAME1.Text );
          ADDValue('SE_NAME2' , edt_SENAME2.Text );
          //주소
          ADDValue('SE_ADDR1' , edt_SEADDR1.Text );
          ADDValue('SE_ADDR2' , edt_SEADDR2.Text );
          ADDValue('SE_ADDR3' , edt_SEADDR3.Text );
          ADDValue('SE_ADDR4' , edt_SEADDR4.Text );
          ADDValue('SE_ADDR5' , edt_SEADDR5.Text );
          //종사업장번호
          ADDValue('SE_SAUP1' , edt_SESAUP1.Text );
          //담당부서명
          ADDValue('SE_FTX1' , edt_SEFTX1.Text );
          //담당자명
          ADDValue('SE_FTX2' , edt_SEFTX2.Text );
          //전화번호
          ADDValue('SE_FTX3' , edt_SEFTX3.Text );
          //이메일
          ADDValue('SE_FTX4' , edt_SEFTX4.Text );
          ADDValue('SE_FTX5' , edt_SEFTX5.Text );
          //업태
          ADDValue('SE_UPTA1' , memo_SEUPTA1.Text );
          //종목
          ADDValue('SE_ITEM1' , memo_SEITEM1.Text);
        //--------------------------------------------------------------------
        //공급받는자
        //--------------------------------------------------------------------
          //공급받는자코드
          ADDValue('BY_CODE' , edt_BYCODE.Text );
          //사업자등록구분
          ADDValue('BY_SAUP_CODE' , edt_BYSAUPCODE.Text);
          //등록번호
          ADDValue('BY_SAUP' , mask_BYSAUP.Text);
          //대표자명
          ADDValue('BY_NAME3' , edt_BYNAME3.Text );
          //상호
          ADDValue('BY_NAME1' , edt_BYNAME1.Text );
          ADDValue('BY_NAME2' , edt_BYNAME2.Text );
          //주소
          ADDValue('BY_ADDR1' , edt_BYADDR1.Text );
          ADDValue('BY_ADDR2' , edt_BYADDR2.Text );
          ADDValue('BY_ADDR3' , edt_BYADDR3.Text );
          ADDValue('BY_ADDR4' , edt_BYADDR4.Text );
          ADDValue('BY_ADDR5' , edt_BYADDR5.Text );
          //종사업장번호
          ADDValue('BY_SAUP1' , edt_BYSAUP1.Text);
          //담당부서명
          ADDValue('BY_FTX1' , edt_BYFTX1.Text );
          //담당자명
          ADDValue('BY_FTX2' , edt_BYFTX2.Text );
          //전화번호
          ADDValue('BY_FTX3' , edt_BYFTX3.Text );
          //이메일
          ADDValue('BY_FTX4' , edt_BYFTX4.Text );
          ADDValue('BY_FTX5' , edt_BYFTX5.Text );
          //담당부서명2
          ADDValue('BY_FTX1_1' , edt_BYFTX1_1.Text );
          //담당자명2
          ADDValue('BY_FTX2_1' , edt_BYFTX2_1.Text );
          //전화번호2
          ADDValue('BY_FTX3_1' , edt_BYFTX3_1.Text );
          //이메일2
          ADDValue('BY_FTX4_1' , edt_BYFTX4_1.Text );
          ADDValue('BY_FTX5_1' , edt_BYFTX5_1.Text );
          //업태
          ADDValue('BY_UPTA1' , memo_BYUPTA1.Text);
          //종목
          ADDValue('BY_ITEM1' , memo_BYITEM1.Text );
        //--------------------------------------------------------------------
        // 금액
        //--------------------------------------------------------------------
          //작성일자
          ADDValue('DRAW_DAT' , mask_DRAWDAT.Text );
          //공급가액
          ADDValue('SUP_AMT' , curr_SUPAMT.Text );
          //세액
          ADDValue('TAX_AMT' , curr_TAXAMT.Text);
          //공란수
          ADDValue('DETAILNO' , curr_DETAILNO.Text);
          //비고
          ADDValue('REMARK1' , memo_REMARK1.Text);
          //현금
          ADDValue('AMT12' , curr_AMT12.Text );
          //현금(외화) 통화
          ADDValue('AMT11C' , edt_AMT11C.Text );
          //현금(외화)
          ADDValue('AMT11' , curr_AMT11.Text );
          //수표
          ADDValue('AMT22' , curr_AMT22.Text);
          //수표(외화) 통화
          ADDValue('AMT21C' , edt_AMT21C.Text );
          //수표(외화)
          ADDValue('AMT21' , curr_AMT21.Text );
          //어음
          ADDValue('AMT32' , curr_AMT32.Text );
          //어음(외화) 통화
          ADDValue('AMT31C' , edt_AMT31C.Text );
          //어음(외화)
          ADDValue('AMT31' , curr_AMT31.Text );
          //외상미수금
          ADDValue('AMT42' , curr_AMT42.Text );
          //외상미수금(외화) 통화
          ADDValue('AMT41C' , edt_AMT41C.Text );
          //외상미수금(외화)
          ADDValue('AMT41' , curr_AMT41.Text );
          //영수/청구
          ADDValue('INDICATOR' , edt_INDICATOR.Text );
          //총금액
          ADDValue('TAMT' , curr_TAMT.Text );
          //총외화공급가액(통화)
          ADDValue('USTAMTC' , edt_USTAMTC.Text);
          //총외화공급가액
          ADDValue('USTAMT' , curr_USTAMT.Text);
          //총세액
          ADDValue('TAXTAMT' , curr_TAXTAMT.Text);
          //총공급가액
          ADDValue('SUPTAMT' , curr_SUPTAMT.Text);
          //총수량(단위)
          ADDValue('TQTYC' , edt_TQTYC.Text);
          //총수량
          ADDValue('TQTY' , curr_TQTY.Text);
        //--------------------------------------------------------------------
        //수탁자
        //--------------------------------------------------------------------
          //수탁자 코드
          ADDValue('AG_CODE' , edt_AGCODE.Text );
          //사업자등록번호
          ADDValue('AG_SAUP' , mask_AGSAUP.Text );
          //대표자명
          ADDValue('AG_NAME3' , edt_AGNAME3.Text );
          //상호
          ADDValue('AG_NAME1' , edt_AGNAME1.Text );
          ADDValue('AG_NAME2' , edt_AGNAME2.Text );
          //주소
          ADDValue('AG_ADDR1' , edt_AGADDR1.Text );
          ADDValue('AG_ADDR2' , edt_AGADDR2.Text );
          ADDValue('AG_ADDR3' , edt_AGADDR3.Text );
          ADDValue('AG_ADDR4' , edt_AGADDR4.Text );
          ADDValue('AG_ADDR5' , edt_AGADDR5.Text );
          //종사업장번호
          ADDValue('AG_SAUP1' , edt_AGSAUP1.Text );
          //담당부서명
          ADDValue('AG_FTX1' , edt_AGFTX1.Text );
          //담당자명
          ADDValue('AG_FTX2' , edt_AGFTX2.Text );
          //전화번호
          ADDValue('AG_FTX3' , edt_AGFTX3.Text );
          //이메일
          ADDValue('AG_FTX4' , edt_AGFTX4.Text );
          ADDValue('AG_FTX5' , edt_AGFTX5.Text );
          //업태
          ADDValue('AG_UPTA1' , memo_AGUPTA1.Text );
          //종목
          ADDValue('AG_ITEM1' , memo_AGITEM1.Text );
        end;

        with TADOQuery.Create(nil) do
        begin
          try
            Connection := DMMssql.KISConnect;
            SQL.Text := SQLCreate.CreateSQL;
            //if sCheckBox1.Checked then
              //Clipboard.AsText := SQL.Text;
            ExecSQL;
          finally
            Close;
            Free;
          end;
        end;

    //------------------------------------------------------------------------------
    // 프로그램 제어
    //------------------------------------------------------------------------------
      ProgramControlType := ctView;
    //------------------------------------------------------------------------------
    // 버튼정리
    //------------------------------------------------------------------------------
      ButtonEnable(True);
      EnabledControlValue(goodsBtnPanel);
    //------------------------------------------------------------------------------
    // 접근제어
    //------------------------------------------------------------------------------
      Page_control.ActivePageIndex := 0;
      Page_controlChange(Page_control);
      sDBGrid1.Enabled := True;
      sDBGrid2.Enabled := True;
      sDBGrid8.Enabled := True;

    //------------------------------------------------------------------------------
    // 데이터제어
    //------------------------------------------------------------------------------
      EnabledControlValue(sPanel4);
      EnabledControlValue(taxPanel);
      EnabledControlValue(providerPanel);
      EnabledControlValue(buyerPanel);
      EnabledControlValue(goodsPanel);
      EnabledControlValue(moneyPanel);
      EnabledControlValue(trusteePanel);
      EnabledControlValue(sPanel57 , False);

    //----------------------------------------------------------------------------
    //새로고침
    //----------------------------------------------------------------------------
    CreateDate := ConvertStr2Date(mask_DATEE.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             DocNo);

    //트랜잭션 저장
    if DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.CommitTrans;

//  qryList.Close;
//   qryList.Open;

  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message),'저장오류',MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TUI_VATBI1_frm.btnSaveClick(Sender: TObject);
begin
  inherited;
  SaveDocument(Sender);
end;

procedure TUI_VATBI1_frm.GoodsSaveDocument;
begin
  with qryGoods do
  begin
    if qryGoods.State = dsInsert then
    begin
      //KEYY
      FieldByName('KEYY').AsString := edt_MAINT_NO.Text;

      //순번
      if qryGoods.RecordCount  = 0 then
        FieldByName('SEQ').AsInteger := 1
      else
      begin
        FieldByName('SEQ').AsInteger := MAX_SEQ('VATBI1_D');
      end;
    end;
  //----------------------------------------------------------------------------
  // 물품수령증명서
  //----------------------------------------------------------------------------
    //품명
    FieldByName('NAME_COD').AsString := edt_NAMECOD_D.Text;
    FieldByName('NAME1').AsString := memo_NAME1_D.Text;
    //공급일자
    FieldByName('DE_DATE').AsString := mask_DEDATE_D.Text;
    //환율
    FieldByName('RATE').AsString := curr_RATE_D.Text;
    //규격
    FieldByName('SIZE1').AsString := memo_SIZE1_D.Text;
    //REMARK
    FieldByName('DE_REM1').AsString := memo_DEREM1_D.Text;
    //수량단위
    FieldByName('QTY_G').AsString := edt_QTY_G_D.Text;
    //수량
    FieldByName('QTY').AsString := curr_QTY_D.Text;
    //단가
    FieldByName('PRICE').AsString := curr_PRICE_D.Text;
    //단가기준수량 단위
    FieldByName('QTYG_G').AsString := edt_QTYG_G_D.Text;
    //단가기준수량
    FieldByName('QTYG').AsString := curr_QTYG_D.Text;
    //원화공급가액
    FieldByName('SUPAMT').AsString := curr_SUPAMT_D.Text;
    //세액
    FieldByName('TAXAMT').AsString := curr_TAXAMT_D.Text;
    //외화공급가액단위
    FieldByName('USAMT_G').AsString := edt_USAMT_G_D.Text;
    //외화공급가액
    FieldByName('USAMT').AsString := curr_USAMT_D.Text;
    //수량소계단위
    qryGoods.FieldByName('STQTY_G').AsString := edt_QTY_G_D.Text;
    //수랑소계
    qryGoods.FieldByName('STQTY').AsCurrency := curr_STQTY_D.Value;
    //원화공급가액소계
    qryGoods.FieldByName('SUPSTAMT').AsCurrency := curr_SUPSTAMT_D.Value;
    //세액소계
    qryGoods.FieldByName('TAXSTAMT').AsCurrency := curr_TAXSTAMT_D.Value;
    //외화공급가액소계단위
    qryGoods.FieldByName('USSTAMT_G').AsString := edt_USAMT_G_D.Text;
    //외화공급가액소계
    qryGoods.FieldByName('USSTAMT').AsCurrency := curr_USSTAMT_D.Value;
  end;
end;

function TUI_VATBI1_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  I : Integer;
  SUPSTAMT_D_SUM : Currency;
begin
  ErrMsg := TStringList.Create;
  Try

    IF Trim(edt_VATCODE.Text) = '' THEN
      ErrMsg.Add('[문서정보] 세금계산서코드를 입력해야합니다.');

    IF Trim(edt_SECODE.Text) = '' THEN
      ErrMsg.Add('[문서정보] 공급자코드를 입력해야합니다.');

    IF Trim(mask_SESAUP.Text) = '' THEN
      ErrMsg.Add('[문서정보] 공급자 사업자등록번호를 입력해야합니다.');

    IF Trim(edt_SENAME3.Text) = '' THEN
      ErrMsg.Add('[문서정보] 공급자 대표자명을 입력해야합니다.');

    IF (Trim(edt_SEADDR1.Text) = '') and (Trim(edt_SEADDR2.Text) = '') and (Trim(edt_SEADDR3.Text) = '') THEN
      ErrMsg.Add('[문서정보] 공급자 주소를 입력해야합니다.');

    IF Trim(edt_BYCODE.Text) = '' THEN
      ErrMsg.Add('[문서정보] 공급받는자 코드를 입력해야합니다.');

    IF Trim(mask_BYSAUP.Text) = '' THEN
      ErrMsg.Add('[문서정보] 공급받는자 사업자등록번호를 입력해야합니다.');

    IF Trim(edt_BYNAME3.Text) = '' THEN
      ErrMsg.Add('[문서정보] 공급받는자 대표자명을 입력해야합니다.');

    IF (Trim(edt_BYADDR1.Text) = '') and (Trim(edt_BYADDR2.Text) = '') and (Trim(edt_BYADDR3.Text) = '') THEN
      ErrMsg.Add('[문서정보] 공급받는자 주소를 입력해야합니다.');


    i := 0;
    if qryGoods.RecordCount = 0 then
    begin
       ErrMsg.Add('[품목내역] 품목내역은 1건이상 작성되어야 합니다.');
    end
    else
    begin
      qryGoods.First;
      while not qryGoods.Eof do
      begin
        Inc(i);

        IF (Trim(qryGoodsNAME1.AsString) = '') THEN
          ErrMsg.Add('[품목내역] '+IntToStr(i)+'행 제품을 입력해야합니다.');

          qryGoods.Next;
      end;
    end;

    SUPSTAMT_D_SUM := 0;
    qryGoods.First;
    while not qryGoods.Eof do
    begin
      SUPSTAMT_D_SUM := SUPSTAMT_D_SUM + qryGoodsSUPSTAMT.AsCurrency;
      qryGoods.Next;
    end;
    IF SUPSTAMT_D_SUM <> curr_SUPTAMT.Value  THEN
      ErrMsg.Add('[금액및수탁자]품목내역의 원화공급가액소계와 총공급가액이 같지않습니다.');
      
    if (11 - Length(RemoveSpecialChar(curr_SUPAMT.Text))) <> curr_DETAILNO.Value then
      ErrMsg.Add('[금액및수탁자] 공란수가 계산치와 맞지않습니다.');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;
end;

function TUI_VATBI1_frm.MAX_SEQ(TableName: String): Integer;
begin
  with qryMAX_SEQ do
  begin
    Close;
    SQL.Text := 'SELECT MAX(SEQ) AS MaxSEQ FROM '+ TableName +' WHERE KEYY =' + QuotedStr(edt_MAINT_NO.Text);
    Open;

    Result := FieldByName('MaxSEQ').AsInteger + 1;
  end;
end;

procedure TUI_VATBI1_frm.sBitBtn7Click(Sender: TObject);
begin
  inherited;
  byAddPanel.Visible := True;
  byAddPanel.Top := 208;
  byAddPanel.Left := 168;
end;

procedure TUI_VATBI1_frm.sBitBtn25Click(Sender: TObject);
begin
  inherited;
  byAddPanel.Visible := False;

  if (Sender as TsBitBtn).Tag = 1 then
  begin
    edt_BYFTX1_1.Clear;
    edt_BYFTX2_1.Clear;
    edt_BYFTX3_1.Clear;
    edt_BYFTX4_1.Clear;
    edt_BYFTX5_1.Clear;
  end;
end;

procedure TUI_VATBI1_frm.sBitBtn8Click(Sender: TObject);
{
var
  qtySum : Double; //수량합
  supamtSum : Double; //원화공급가액합
  taxAmtSum : Double; //세액합
  usamtSum : Double; //외화공급가액합
}
begin
  //품목내역의 소계계산 버튼이벤트
  //수량소계 := 수량
  edt_STQTY_G_D.Text := edt_QTY_G_D.Text;
  curr_STQTY_D.Value := curr_QTY_D.Value;
  //원화공급가액 소계 := 원화공급가액
  curr_SUPSTAMT_D.Value := curr_SUPAMT_D.Value;
  //세액 소계 := 세액
  curr_TAXSTAMT_D.Value := curr_TAXAMT_D.Value;
  //외화공금가액 소계 := 외화공급가액
  edt_USSTAMT_G_D.Text := edt_USAMT_G_D.Text;
  curr_USSTAMT_D.Value := curr_USAMT_D.Value;

{
  if (not (qryGoods.State in [dsEdit,dsInsert])) and (qryGoods.RecordCount > 0 ) then
  begin
    qtySum := 0;
    supamtSum := 0;
    usamtSum := 0;
    taxAmtSum   := 0;

    qrygoods.First;
    while not qryGoods.Eof do
    begin
      qtySum := qryGoodsQTY.AsCurrency + qtySum;

      supamtSum := qryGoodsSUPAMT.AsCurrency + supamtSum;

      taxAmtSum := qryGoodsTAXAMT.AsCurrency + taxAmtSum;

      usamtSum   := qryGoodsUSAMT.AsCurrency + usamtSum;
      qryGoods.Next;
    end;

    qrygoods.First;
    while not qryGoods.Eof do
    begin
      qryGoods.Edit;

      //수량소계단위
      qryGoods.FieldByName('STQTY_G').AsString := edt_QTY_G_D.Text;
      //수랑소계
      qryGoods.FieldByName('STQTY').AsCurrency := qtySum;
      //원화공급가액소계
      qryGoods.FieldByName('SUPSTAMT').AsCurrency := supamtSum;
      //세액소계
      qryGoods.FieldByName('TAXSTAMT').AsCurrency := taxAmtSum;
      //외화공급가액소계단위
      qryGoods.FieldByName('USSTAMT_G').AsString := edt_USAMT_G_D.Text;
      //외화공급가액소계
      qryGoods.FieldByName('USSTAMT').AsCurrency := usamtSum;

      qryGoods.Post;
      qryGoods.Next;
    end
  end
  else if qryGoods.RecordCount = 0 then
  begin
    ShowMessage('작성된 건이 없습니다.');
  end
  else
  begin
    ShowMessage('작성중인 품목이 있습니다.');
  end;
}
end;

procedure TUI_VATBI1_frm.curr_QTY_DChange(Sender: TObject);
begin
  inherited;
   if (curr_QTY_D.Value > 0) and (curr_PRICE_D.Value > 0) then
   begin
     if (curr_QTYG_D.Value = 0) or (Trim(curr_QTYG_D.Text) = '' )then
     begin
       //원화공급가액 := (수량 * 단가) / 단가기준수량(0이거나 ''이면 1로 계산)
      curr_SUPAMT_D.Value :=  (curr_QTY_D.Value  * curr_PRICE_D.Value)
     end
     else
     begin
      //원화공급가액 := (수량 * 단가) / 단가기준수량
      curr_SUPAMT_D.Value :=  (curr_QTY_D.Value  * curr_PRICE_D.Value) / curr_QTYG_D.Value;
     end;
    //세액 := 원화공급가액 * 0.1
    curr_TAXAMT_D.Value := FloatToCurr(curr_SUPAMT_D.Value * 0.1);
   end;
end;

procedure TUI_VATBI1_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_VATBI1_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel23.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn23.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn28.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_VATBI1_frm.sBitBtn26Click(Sender: TObject);
var
  supamtSum : Double; // 원화공급가액 합
  taxAmtSum : Double; // 세액 합
  ustAmtSum : Double; // 총외화공급가액
  tqtySum   : Double; // 총수량
begin
  inherited;
  //금액 및 수탁자 페이지 금액계산 버튼

  supamtSum := 0;
  taxAmtSum := 0;
  ustAmtSum := 0;
  tqtySum   := 0;

  qrygoods.First;
  while not qryGoods.Eof do
  begin
    supamtSum := qryGoodsSUPAMT.AsCurrency + supamtSum;

    taxAmtSum := qryGoodsTAXAMT.AsCurrency + taxAmtSum;

    ustAmtSum := qryGoodsUSAMT.AsCurrency + ustAmtSum;

    tqtySum   := qryGoodsQTY.AsCurrency + tqtySum;

    qryGoods.Next;
  end;

  //금액 및 수탁자 공급가액
  curr_SUPAMT.Value := supamtSum;
  //금액 및 수탁자 세액
  curr_TAXAMT.Value := taxAmtSum;
  //총공급가액
  curr_SUPTAMT.Value := curr_SUPAMT.Value;
  //총세액
  curr_TAXTAMT.Value := curr_TAXAMT.Value;
  //총금액
  curr_TAMT.Value := curr_SUPTAMT.Value + curr_TAXTAMT.Value;
  //총외화공급가액
  edt_USTAMTC.Text := edt_USSTAMT_G_D.Text;
  curr_USTAMT.Value := ustAmtSum;
  //총수량
  edt_TQTYC.Text := edt_STQTY_G_D.Text;
  curr_TQTY.Value := tqtySum;

  //공란수
  curr_DETAILNO.Value := (11 - Length(RemoveSpecialChar(curr_SUPAMT.Text)));  //전채 공랑수 11 - 공급가액의 숫자 갯수
end;

procedure TUI_VATBI1_frm.curr_SUPAMTChange(Sender: TObject);
begin
  inherited;
  //공란수 자동처리
  if curr_DETAILNO.Value = 0 then
    curr_DETAILNO.Value := 11
  else if Length(RemoveSpecialChar(curr_SUPAMT.Text)) >= 12 then
    curr_DETAILNO.Value := 0
  else
    curr_DETAILNO.Value := (11 - Length(RemoveSpecialChar(curr_SUPAMT.Text)));  //전채 공랑수 11 - 공급가액의 숫자 갯수

  //ShowMessage(IntToStr(Length(RemoveSpecialChar(curr_SUPAMT.Text))));
  //ShowMessage(RemoveSpecialChar(curr_SUPAMT.Text));
end;

function TUI_VATBI1_frm.RemoveSpecialChar(sSrc: string): string;
var 
  I: integer; 
  rt_string:string; 
begin 
   rt_string:=''; 
   for I:=1 to Length(sSrc) do
   begin //한글도 삭제하려면 if문에 or (ByteType(sSrc, I)<>mbSingleByte) 이 부분추가
     if (sSrc[I] in ['A'..'Z', 'a'..'z', '0'..'9']) then
       rt_string := rt_string + sSrc[I];
   end;
  result:=rt_string;
end;

procedure TUI_VATBI1_frm.QRCompositeReport1AddReports(Sender: TObject);
begin
  inherited;
  QRCompositeReport1.Reports.Add(VATBI1_PRINT_frm);
end;

procedure TUI_VATBI1_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  VATBI1_PRINT_frm := TVATBI1_PRINT_frm.Create(Self);
  try
    VATBI1_PRINT_frm.MaintNo := edt_MAINT_NO.Text;
//    QRCompositeReport1.Prepare;
    QRCompositeReport1.Preview;
  finally
    FreeAndNil(VATBI1_PRINT_frm);
  end;
end;

procedure TUI_VATBI1_frm.btnCopyClick(Sender: TObject);
var
  copyDocNo , newDocNo : String;
begin
  inherited;

    Dialog_CopyVATBI1_frm := TDialog_CopyVATBI1_frm.Create(Self);
      try
          newDocNo := DMAutoNo.GetDocumentNoAutoInc('VATBI1');
          copyDocNo := Dialog_CopyVATBI1_frm.openDialog;

          //ShowMessage(copyDocNo);

          IF Trim(copyDocNo) = '' then Exit;

          //------------------------------------------------------------------------------
          // COPY
          //------------------------------------------------------------------------------
          with spCopyVATBI1 do
          begin
            Close;
            Parameters.ParamByName('@CopyDocNo').Value := copyDocNo;
            Parameters.ParamByName('@NewDocNo').Value :=  newDocNo;
            Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

            if not DMMssql.KISConnect.InTransaction then
              DMMssql.KISConnect.BeginTrans;

            try
              ExecProc;
            except
              on e:Exception do
              begin
                DMMssql.KISConnect.RollbackTrans;
                MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
              end;
            end;

          end;
        // ReadListBetween(해당년도 1월1일 , 오늘날짜 , 새로복사 된  관리번호)
        if  ReadListBetween(FormatDateTime('YYYYMMDD', StartOfTheYear(Now)),FormatDateTime('YYYYMMDD',Now),newDocNo) Then
        begin

          EditDocument; 
          edt_MAINT_NO.Text := newDocNo;
    
        end
        else
          raise Exception.Create('복사한 데이터를 찾을수 없습니다');


      finally

        FreeAndNil(Dialog_CopyVATBI1_frm);

      end;

end;

function TUI_VATBI1_frm.ReadListBetween(fromDate, toDate,
  KeyValue: string): Boolean;
begin
  Result := False;
  with qryList do
  begin
    Close;
    SQL.Text := VATBI1_SQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC');
    Open;

    //ShowMessage(SQL.Text);

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;

  end;
end;

procedure TUI_VATBI1_frm.N1Click(Sender: TObject);
begin
  inherited;
  if not qryGoods.Active then Exit;

  case (Sender as TMenuItem).Tag of
    0: btn_goodsNewClick(btn_goodsNew);
    1: btn_goodsNewClick(btn_goodsMod);
    2: btn_goodsNewClick(btn_goodsDel);
  end;

  {
  case (Sender as TMenuItem).Tag of
    //품목내역 입력
    0 :
    begin
      qryGoods.Append;
      sDBGrid1.Enabled := False;
      ClearControlValue(goodsPanel);
    end;
    //물품수령증명서 수정
    1 :
    begin
      qryGoods.Edit;
      sDBGrid1.Enabled := False;
    end;
    //물품수령증명서 삭제
    3 :
    begin
      if qryGoods.RecordCount = 0 then
      begin
        MessageBox(Self.Handle,MSG_SYSTEM_DEL_CANCEL,'품목내역 삭제확인',MB_OK+MB_ICONQUESTION)
      end
      else
      BEGIN
        IF MessageBox(Self.Handle,MSG_SYSTEM_DEL_CONFIRM,'품목내역 삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        qryGoods.Delete;
      end;
    end;
  end;
  }
end;

procedure TUI_VATBI1_frm.memo_NAME1_DChange(Sender: TObject);
var
  memoRow , memoCol : Integer;
begin
  inherited;
  //메모의 라인번호
  memorow := (Sender as TsMemo).Perform(EM_LINEFROMCHAR,(Sender as TsMemo).SelStart,0);
  //메모의 컬럼번호
  memoCol := (Sender as TsMemo).SelStart - (Sender as TsMemo).Perform(EM_LINEINDEX,memorow,0);

  if (Sender as TsMemo).Name = 'memo_NAME1_D' then
    sLabel5.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  if (Sender as TsMemo).Name = 'memo_SIZE1_D' then
    sLabel6.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  if (Sender as TsMemo).Name = 'memo_DEREM1_D' then
    sLabel7.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';
  if (Sender as TsMemo).Name = 'memo_REMARK1' then
    sLabel9.Caption := IntToStr(memoRow+1)+'행 ' + IntToStr(memoCol)+'열';

end;

procedure TUI_VATBI1_frm.memoLineLimit(Sender: TObject;
  var Key: Char);
var
  limitcount : Integer;
begin
  if (Sender as TsMemo).Name = 'memo_NAME1_D' then limitcount := 5
  else if (Sender as TsMemo).Name = 'memo_SIZE1_D' then limitcount := 16
  else if (Sender as TsMemo).Name = 'memo_DEREM1_D' then limitcount := 16
  else if (Sender as TsMemo).Name = 'memo_REMARK1' then limitcount := 11;

  if (Key = #13) AND ((Sender as TsMemo).lines.count >= limitCount-1) then Key := #0;

end;

procedure TUI_VATBI1_frm.goodsMenuPopup(Sender: TObject);
begin
  inherited;
   N1.Enabled :=  ProgramControlType in [ctInsert , ctModify];
   N2.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryGoods.RecordCount > 0);
   N3.Enabled := (ProgramControlType in [ctInsert , ctModify]) and (qryGoods.RecordCount > 0);
end;

procedure TUI_VATBI1_frm.Page_controlChange(Sender: TObject);
begin
  inherited;
  //데이터 신규,수정 작성시 데이터조회 페이지 검색부분 비활성화
  if ProgramControlType in [ctInsert , ctModify] then
    EnabledControlValue(sPanel21)
  else
    EnabledControlValue(sPanel21,False);
end;

procedure TUI_VATBI1_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_VATBI1_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := VATBI1(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'VATBI1';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'VATBI1_H';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        Readlist(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;

end;

procedure TUI_VATBI1_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_VATBI1_frm.qryListCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var                                                         
  nIndex : integer;  
begin
  inherited;

  IF Length(qryListCHK3.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(qryListCHK3.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;
end;

procedure TUI_VATBI1_frm.GoodsSampleBtnClick(Sender: TObject);
var
   ExcelApp , ExcelBook , ExcelSheet : Variant;
   i : Integer;
begin
  //상품내역 엑셀샘플----------------------------------------------------------
  inherited;
  try
    ExcelApp := CreateOleObject('Excel.Application');
  except
    ShowMessage('Excel이 설치되어 있지 않습니다.');
    Exit;
  end;

  ExcelApp.WorkBooks.Add; //새로운페이지 생성
  ExcelBook := ExcelApp.ActiveWorkBook; //워크북 추가
//  ExcelBook.Sheets.Add; //워크시트추가

  try
    ExcelSheet := ExcelBook.WorkSheets[1]; //작업할 워크시트 선택

    //셀에 타이틀에 들어갈 내용들 입력
    ExcelSheet.Cells[1,1].Value := '품 명 코 드';
    ExcelSheet.Cells[1,2].Value := '품       명';
    ExcelSheet.Cells[1,3].Value := '규       격';
    ExcelSheet.Cells[1,4].Value := 'REMARK';
    ExcelSheet.Cells[1,5].Value := '공 급 일 자';
    ExcelSheet.Cells[1,6].Value := '환       율';
    ExcelSheet.Cells[1,7].Value := '수       량';
    ExcelSheet.Cells[1,8].Value := '수 량 단 위';
    ExcelSheet.Cells[1,9].Value := '단       가';
    ExcelSheet.Cells[1,10].Value := '단가기준수량';
    ExcelSheet.Cells[1,11].Value := '단가기준수량단위';
    ExcelSheet.Cells[1,12].Value := '원화공급가액';
    ExcelSheet.Cells[1,13].Value := '세      액';
    ExcelSheet.Cells[1,14].Value := '외화공급가액';
    ExcelSheet.Cells[1,15].Value := '외화공급가액단위';
    ExcelSheet.Cells[1,16].Value := '수 량 소 계';
    ExcelSheet.Cells[1,17].Value := '수량소계단위';
    ExcelSheet.Cells[1,18].Value := '원화공급가액소계';
    ExcelSheet.Cells[1,19].Value := '세 액 소 계';
    ExcelSheet.Cells[1,20].Value := '외화공급가액소계';
    ExcelSheet.Cells[1,21].Value := '외화공급가액소계단위'; 
    ExcelSheet.Cells[1,21].FONT.UNDERLINE := xlUnderlineStyleSingle;

    //셀정렬 및 폰트 두깨
    for i := 1 to ExcelApp.ActiveSheet.UsedRange.Columns.count+1 do
    begin
      ExcelApp.Range[ExcelSheet.Cells[1,i],ExcelSheet.Cells[1,i]].HorizontalAlignment := xlHAlignCenter;
      ExcelApp.Range[ExcelSheet.Cells[1,i],ExcelSheet.Cells[1,i]].font.bold := True; //폰트변경
    end;

    ExcelSheet.Columns.AutoFit; //Cell 사이즈 변경

    ExcelApp.Visible := True; //엑셀보이기
  //ExcelApp.DisplayAlerts := True; //메시지표시

  finally
    ExcelBook.Close;
    ExcelBook := Unassigned;
    ExcelSheet := Unassigned;
  end;

end;

procedure TUI_VATBI1_frm.GoodsExcelBtnClick(Sender: TObject);
var
  ExcelApp , ExcelBook , ExcelSheet : Variant;
  ExcelfileName : String;
  ExcelRows, ExcelCols, StartRow : Integer;
  i : Integer;
begin
  //엑셀가져오기-------------------------------------------------------
  inherited;
  try
    ExcelApp := CreateOleObject('Excel.Application');
  except
    ShowMessage('Excel이 설치되어 있지 않습니다.');
    Exit;
  end;

  //excelOpen.Filter := 'All Files (*.*)|*.*|Microsoft Excel 통합문서(*.xls)|*.xls|엑셀2007(*.xlsx)|*.xlsx';

  if not excelOpen.Execute then Exit;

  //excelOpen.FileName을 사용해도 되지만 윈도우상에 버그로 글씨가 깨지거나함
  ExcelfileName := excelOpen.Files.Strings[0]; //선택된 엑셀파일이름

  try
    with TADOQuery.Create(nil) do
    begin
      Connection := DMMssql.KISConnect;
      try
        SQL.Text :=  'DELETE FROM VATBI3_D WHERE KEYY =' + QuotedStr(edt_MAINT_NO.Text);
        ExecSQL;

        qryGoods.Close;
        qryGoods.Parameters.ParamByName('MAINT_NO').Value := Trim(edt_MAINT_NO.Text);
        qryGoods.Open;

      finally
        Close;
        Free;
      end;
    end;

    ExcelApp.Visible := False; //엑셀보이지 않기
    ExcelApp.DisplayAlerts := False; //메시지표시않기

    ExcelBook := ExcelApp.WorkBooks.Open(ExcelfileName);
    ExcelSheet := ExcelBook.Sheets['Sheet1'];

    ExcelRows := ExcelApp.ActiveSheet.UsedRange.Rows.count; //총 row수 구하기
    ExcelCols := ExcelApp.ActiveSheet.UsedRange.Columns.count; //총 Col수 구하기

    for i := 2 to ExcelRows do
    begin
      qryGoods.Append;

      qryGoods.FieldByName('KEYY').AsString := edt_MAINT_NO.Text;
      qryGoods.FieldByName('SEQ').AsInteger := i-1;
      qryGoods.FieldByName('NAME_COD').AsString := ExcelSheet.Cells[i,1].Value; //품명코드
      qryGoods.FieldByName('NAME1').AsString := ExcelSheet.Cells[i,2].Value; //품명
      qryGoods.FieldByName('SIZE1').AsString := ExcelSheet.Cells[i,3].Value; //규격
      qryGoods.FieldByName('DE_REM1').AsString := ExcelSheet.Cells[i,4].Value; //REMARK
      qryGoods.FieldByName('DE_DATE').AsString := ExcelSheet.Cells[i,5].Value; //공급일자
      qryGoods.FieldByName('RATE').AsString := ExcelSheet.Cells[i,6].Value; //환율
      qryGoods.FieldByName('QTY').AsCurrency := ExcelSheet.Cells[i,7].Value; //수량
      qryGoods.FieldByName('QTY_G').AsString := ExcelSheet.Cells[i,8].Value; //수량단위
      qryGoods.FieldByName('PRICE').AsCurrency := ExcelSheet.Cells[i,9].Value; //단가
      qryGoods.FieldByName('QTYG').AsCurrency := ExcelSheet.Cells[i,10].Value; //단가기준수량
      qryGoods.FieldByName('QTYG_G').AsString := ExcelSheet.Cells[i,11].Value; //단가기준수량단위
      qryGoods.FieldByName('SUPAMT').AsCurrency := ExcelSheet.Cells[i,12].Value; //원화공급가액
      qryGoods.FieldByName('TAXAMT').AsCurrency := ExcelSheet.Cells[i,13].Value; //세액
      qryGoods.FieldByName('USAMT').AsCurrency := ExcelSheet.Cells[i,14].Value; //외화공급가액
      qryGoods.FieldByName('USAMT_G').AsString := ExcelSheet.Cells[i,15].Value; //외화공급가액단위
      qryGoods.FieldByName('STQTY').AsCurrency := ExcelSheet.Cells[i,16].Value; //수량소계
      qryGoods.FieldByName('STQTY_G').AsString := ExcelSheet.Cells[i,17].Value; //수량소계단위
      qryGoods.FieldByName('SUPSTAMT').AsCurrency := ExcelSheet.Cells[i,18].Value; //원화공급가액소계
      qryGoods.FieldByName('TAXSTAMT').AsCurrency := ExcelSheet.Cells[i,19].Value; //세액소계
      qryGoods.FieldByName('USSTAMT').AsCurrency := ExcelSheet.Cells[i,20].Value; //외화공급가액소계
      qryGoods.FieldByName('USSTAMT_G').AsString := ExcelSheet.Cells[i,21].Value; //외화공급가액단위소계

      qryGoods.Post;
    end;
    //ShowMessage( ExcelSheet.cells[1,2].value ) ;

    //접근제어------------------------------------------------------------------
    GoodsButtonEnable(True); //버튼정리
    //새로고침------------------------------------------------------------------
    qryGoods.Close;
    qryGoods.Parameters.ParamByName('MAINT_NO').Value := Trim(edt_MAINT_NO.Text);
    qryGoods.Open;

  finally
    ExcelBook.Close;
    ExcelApp.Quit;
    ExcelApp := Unassigned;
    Finalize(ExcelSheet);
    Finalize(ExcelBook);
    Finalize(ExcelApp);
  end;    
end;

end.

