inherited UI_APP700_frm: TUI_APP700_frm
  Left = 635
  Top = 87
  Caption = 'UI_APP700_frm'
  ClientWidth = 1114
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sSplitter1: TsSplitter
    Width = 1114
  end
  inherited sSplitter3: TsSplitter
    Width = 1114
  end
  inherited sPageControl1: TsPageControl
    Width = 1114
    ActivePage = sTabSheet1
    TabIndex = 0
    OnChange = sPageControl1Change
    inherited sTabSheet1: TsTabSheet
      inherited sSplitter4: TsSplitter
        Width = 1106
      end
      inherited sPanel51: TsPanel
        Width = 806
        inherited sSpeedButton6: TsSpeedButton
          Left = 398
          Top = -8
          Height = 569
        end
        inherited sPanel5: TsPanel
          Left = 15
          Top = 2
          Width = 370
        end
        inherited edt_In_Mathod1: TsEdit
          Left = 179
          Top = 50
          Width = 206
          HelpContext = 0
          TabStop = False
          TabOrder = 44
        end
        inherited sPanel2: TsPanel
          Left = 15
          Top = 207
          Width = 370
        end
        inherited edt_ImpCd1_1: TsEdit
          Left = 179
          Top = 231
          Width = 206
          HelpContext = 0
        end
        inherited btn_ImpCd1: TsBitBtn
          Left = 156
          Top = 231
        end
        inherited btn_ImpCd2: TsBitBtn
          Left = 156
          Top = 253
        end
        inherited edt_ImpCd2_1: TsEdit
          Left = 179
          Top = 253
          Width = 206
          HelpContext = 0
        end
        inherited edt_ImpCd3_1: TsEdit
          Left = 179
          Top = 275
          Width = 206
          HelpContext = 0
        end
        inherited btn_ImpCd3: TsBitBtn
          Left = 156
          Top = 275
        end
        inherited btn_ImpCd4: TsBitBtn
          Left = 156
          Top = 297
        end
        inherited edt_ImpCd4_1: TsEdit
          Left = 179
          Top = 297
          Width = 206
          HelpContext = 0
        end
        inherited edt_ImpCd5_1: TsEdit
          Left = 179
          Top = 319
          Width = 206
          HelpContext = 0
        end
        inherited btn_ImpCd5: TsBitBtn
          Left = 156
          Top = 319
        end
        inherited sPanel8: TsPanel
          Left = 15
          Top = 384
          Width = 370
        end
        inherited sPanel9: TsPanel
          Left = 421
          Top = 384
          Width = 370
        end
        inherited btn_LCNumber5: TsBitBtn
          Left = 683
          Top = 319
        end
        inherited btn_LCNumber4: TsBitBtn
          Left = 683
          Top = 297
        end
        inherited btn_LCNumber3: TsBitBtn
          Left = 683
          Top = 275
        end
        inherited btn_LCNumber2: TsBitBtn
          Left = 683
          Top = 253
        end
        inherited btn_LCNumber1: TsBitBtn
          Left = 683
          Top = 231
        end
        inherited sPanel7: TsPanel
          Left = 421
          Top = 207
          Width = 370
        end
        inherited btn_GrantingCredit: TsBitBtn
          Left = 562
          Top = 114
        end
        inherited edt_AdPay1: TsEdit
          Left = 585
          Top = 114
          Width = 206
          HelpContext = 0
        end
        inherited sPanel6: TsPanel
          Left = 421
          Top = 2
          Width = 370
        end
        inherited btn_OpenMethod: TsBitBtn
          Left = 156
          Top = 48
        end
        inherited btn_ApBank: TsBitBtn
          Left = 156
          Top = 70
        end
        inherited btn_NoticeBank: TsBitBtn
          Left = 562
          Top = 26
        end
        inherited edt_Datee: TsMaskEdit
          Left = 106
          Top = 26
          Width = 72
          TabOrder = 0
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_In_Mathod: TsEdit
          Left = 106
          Top = 48
          MaxLength = 3
          TabOrder = 1
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ApBank: TsEdit
          Left = 106
          Top = 70
          MaxLength = 4
          TabOrder = 2
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ApBank1: TsEdit
          Left = 179
          Top = 72
          Width = 206
          MaxLength = 35
          TabOrder = 3
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ApBank2: TsEdit
          Left = 179
          Top = 94
          Width = 206
          MaxLength = 35
          TabOrder = 4
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ApBank3: TsEdit
          Left = 179
          Top = 116
          Width = 206
          MaxLength = 35
          TabOrder = 5
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ApBank4: TsEdit
          Left = 179
          Top = 138
          Width = 206
          MaxLength = 35
          TabOrder = 6
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ApBank5: TsEdit
          Left = 179
          Top = 160
          Width = 206
          Height = 19
          MaxLength = 35
          TabOrder = 7
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdBank: TsEdit
          Left = 512
          Top = 26
          MaxLength = 4
          TabOrder = 18
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdBank1: TsEdit
          Left = 585
          Top = 26
          Width = 206
          MaxLength = 35
          TabOrder = 19
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdBank2: TsEdit
          Left = 585
          Top = 48
          Width = 206
          MaxLength = 35
          TabOrder = 20
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdBank3: TsEdit
          Left = 585
          Top = 70
          Width = 206
          MaxLength = 35
          TabOrder = 21
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdBank4: TsEdit
          Left = 585
          Top = 92
          Width = 206
          MaxLength = 35
          TabOrder = 22
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdPay: TsEdit
          Left = 512
          Top = 114
          MaxLength = 3
          TabOrder = 23
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ImpCd1: TsEdit
          Left = 106
          Top = 231
          MaxLength = 3
          TabOrder = 8
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ImpCd2: TsEdit
          Left = 106
          Top = 253
          MaxLength = 3
          TabOrder = 9
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ImpCd3: TsEdit
          Left = 106
          Top = 275
          MaxLength = 3
          TabOrder = 10
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ImpCd4: TsEdit
          Left = 106
          Top = 297
          MaxLength = 3
          TabOrder = 11
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ImpCd5: TsEdit
          Left = 106
          Top = 319
          MaxLength = 3
          TabOrder = 12
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILno1: TsEdit
          Left = 471
          Top = 231
          MaxLength = 35
          TabOrder = 24
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILCur1: TsEdit
          Left = 647
          Top = 231
          MaxLength = 3
          TabOrder = 25
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILAMT1: TsCurrencyEdit
          Left = 706
          Top = 231
          TabOrder = 26
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILno2: TsEdit
          Left = 471
          Top = 253
          MaxLength = 35
          TabOrder = 27
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILCur2: TsEdit
          Left = 647
          Top = 253
          MaxLength = 3
          TabOrder = 28
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILAMT2: TsCurrencyEdit
          Left = 706
          Top = 253
          TabOrder = 29
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILno3: TsEdit
          Left = 471
          Top = 275
          MaxLength = 35
          TabOrder = 30
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILCur3: TsEdit
          Left = 647
          Top = 275
          MaxLength = 3
          TabOrder = 31
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILAMT3: TsCurrencyEdit
          Left = 706
          Top = 275
          TabOrder = 32
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILno4: TsEdit
          Left = 471
          Top = 297
          MaxLength = 35
          TabOrder = 33
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILCur4: TsEdit
          Left = 647
          Top = 297
          MaxLength = 3
          TabOrder = 34
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILAMT4: TsCurrencyEdit
          Left = 706
          Top = 297
          TabOrder = 35
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILno5: TsEdit
          Left = 471
          Top = 319
          MaxLength = 35
          TabOrder = 36
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILCur5: TsEdit
          Left = 647
          Top = 319
          MaxLength = 3
          TabOrder = 37
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_ILAMT5: TsCurrencyEdit
          Left = 706
          Top = 319
          TabOrder = 38
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdInfo1: TsEdit
          Left = 106
          Top = 410
          Width = 279
          MaxLength = 70
          TabOrder = 13
          OnKeyDown = Mask_fromDateKeyDown
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#51221#48372
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited edt_AdInfo2: TsEdit
          Left = 106
          Top = 432
          Width = 279
          MaxLength = 70
          TabOrder = 14
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdInfo3: TsEdit
          Left = 106
          Top = 454
          Width = 279
          MaxLength = 70
          TabOrder = 15
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdInfo4: TsEdit
          Left = 106
          Top = 476
          Width = 279
          MaxLength = 70
          TabOrder = 16
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_AdInfo5: TsEdit
          Left = 106
          Top = 498
          Width = 279
          MaxLength = 70
          TabOrder = 17
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_EXName1: TsEdit
          Left = 552
          Top = 410
          Width = 239
          MaxLength = 35
          TabOrder = 39
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_EXName2: TsEdit
          Left = 552
          Top = 432
          Width = 239
          MaxLength = 35
          TabOrder = 40
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_EXName3: TsEdit
          Left = 552
          Top = 454
          Width = 103
          MaxLength = 35
          TabOrder = 41
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_EXAddr1: TsEdit
          Left = 552
          Top = 476
          Width = 239
          MaxLength = 35
          TabOrder = 42
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_EXAddr2: TsEdit
          Left = 552
          Top = 498
          Width = 239
          MaxLength = 35
          TabOrder = 43
          OnKeyDown = Mask_fromDateKeyDown
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sSplitter7: TsSplitter
        Width = 1106
      end
      inherited sPanel21: TsPanel
        Width = 806
        inherited sLabel9: TsLabel
          Left = 280
          Top = 428
        end
        object sSpeedButton10: TsSpeedButton [1]
          Left = 398
          Top = -10
          Width = 10
          Height = 569
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        inherited edt_Benefc3: TsEdit [2]
          Left = 530
          Top = 178
          Width = 256
          HelpContext = 0
          MaxLength = 35
          TabOrder = 18
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_Applic2: TsEdit [3]
          Left = 128
          Top = 134
          Width = 256
          HelpContext = 0
          MaxLength = 35
          TabOrder = 2
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sPanel19: TsPanel [4]
          Left = 421
          Top = 88
          Width = 370
          Height = 23
        end
        inherited sPanel24: TsPanel [5]
          Left = 13
          Top = 402
          Width = 370
          Height = 23
          TabOrder = 30
          inherited sLabel5: TsLabel
            Left = 1
            Top = 3
          end
        end
        inherited sPanel26: TsPanel [6]
          Left = 13
          Top = 467
          Width = 370
          Height = 23
          TabOrder = 9
          inherited sLabel7: TsLabel
            Left = 1
            Top = 3
          end
        end
        inherited btn_Beneficiary: TsBitBtn [7]
          Left = 576
          Top = 112
        end
        inherited edt_exPlace: TsEdit [8]
          Left = 530
          Top = 49
          Width = 256
          MaxLength = 35
          TabOrder = 14
          OnKeyDown = Mask_fromDateKeyDown
          BoundLabel.Font.Height = -12
        end
        inherited edt_Benefc5: TsEdit [9]
          Left = 530
          Top = 222
          Width = 256
          HelpContext = 0
          MaxLength = 35
          TabOrder = 20
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sPanel20: TsPanel [10]
          Left = 15
          Top = 250
          Width = 370
          Height = 23
          TabOrder = 28
          inherited sLabel1: TsLabel
            Left = 1
            Top = 3
          end
        end
        inherited sPanel22: TsPanel [11]
          Left = 13
          Top = 324
          Width = 370
          Height = 23
          TabOrder = 27
          inherited sLabel3: TsLabel
            Left = 1
            Top = 3
          end
        end
        inherited edt_Cdcur: TsEdit [12]
          Left = 128
          Top = 274
          HelpType = htKeyword
          HelpKeyword = 'Currency Code'
          MaxLength = 3
          TabOrder = 6
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_CdMax1: TsEdit [13]
          Left = 187
          Top = 348
          Width = 196
          HelpContext = 0
          TabOrder = 31
        end
        inherited edt_cdPerM: TsCurrencyEdit [14]
          Left = 222
          Top = 426
          TabOrder = 10
          OnKeyDown = Mask_fromDateKeyDown
          BoundLabel.Font.Height = -15
        end
        inherited edt_cdPerP: TsCurrencyEdit [15]
          Left = 128
          Top = 426
          TabOrder = 26
          OnKeyDown = Mask_fromDateKeyDown
          BoundLabel.Font.Height = -15
        end
        inherited btn_MaximumCreditCode: TsBitBtn [16]
          Left = 164
          Top = 348
        end
        inherited btn_CurrencyCode: TsBitBtn [17]
          Left = 164
          Top = 274
          TabOrder = 29
        end
        inherited sPanel17: TsPanel [18]
          Left = 15
          Top = 88
          Width = 370
          Height = 23
          inherited sLabel6: TsLabel
            Left = 1
          end
        end
        inherited edt_Applic1: TsEdit [19]
          Left = 128
          Top = 112
          Width = 256
          HelpContext = 0
          MaxLength = 35
          TabOrder = 1
          OnKeyDown = Mask_fromDateKeyDown
          BoundLabel.Font.Height = -12
        end
        inherited btn_TermsOfPrice: TsBitBtn [20]
          Left = 164
          Top = 491
        end
        inherited edt_Applic3: TsEdit [21]
          Left = 128
          Top = 156
          Width = 256
          HelpContext = 0
          MaxLength = 35
          TabOrder = 3
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_Applic4: TsEdit [22]
          Left = 128
          Top = 178
          Width = 256
          HelpContext = 0
          MaxLength = 35
          TabOrder = 4
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_Applic5: TsEdit [23]
          Left = 128
          Top = 200
          Width = 256
          HelpContext = 0
          MaxLength = 35
          TabOrder = 5
          OnKeyDown = Mask_fromDateKeyDown
          BoundLabel.Font.Height = -12
        end
        inherited edt_exDate: TsMaskEdit [24]
          Left = 530
          Top = 27
          HelpContext = 1
          TabOrder = 13
          OnKeyDown = Mask_fromDateKeyDown
          BoundLabel.Font.Height = -12
        end
        inherited btn_Doccd: TsBitBtn [25]
          Left = 164
          Top = 26
        end
        inherited edt_Benefc4: TsEdit [26]
          Left = 530
          Top = 200
          Width = 256
          HelpContext = 0
          MaxLength = 35
          TabOrder = 19
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_Benefc2: TsEdit [27]
          Left = 530
          Top = 156
          Width = 256
          HelpContext = 0
          MaxLength = 35
          TabOrder = 17
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_Benefc1: TsEdit [28]
          Left = 530
          Top = 134
          Width = 256
          HelpContext = 0
          Color = 5632767
          MaxLength = 35
          TabOrder = 16
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
        end
        inherited edt_Cdamt: TsCurrencyEdit [29]
          Left = 187
          Top = 274
          Width = 197
          HelpType = htKeyword
          HelpKeyword = 'Amount'
          HelpContext = 1
          TabOrder = 7
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_Benefc: TsEdit [30]
          Left = 530
          Top = 112
          HelpType = htKeyword
          HelpKeyword = 'Beneficiary'
          Color = clWhite
          MaxLength = 10
          TabOrder = 15
          OnKeyDown = Mask_fromDateKeyDown
          BoundLabel.Font.Height = -12
        end
        inherited edt_CdMax: TsEdit [31]
          Left = 128
          Top = 348
          HelpContext = 0
          MaxLength = 3
          TabOrder = 8
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sPanel16: TsPanel
          Left = 421
          Top = 2
          Width = 370
          Height = 23
          inherited sLabel4: TsLabel
            Left = 1
            Top = 3
          end
        end
        inherited edt_Doccd1: TsEdit
          Left = 187
          Top = 26
          Width = 197
          HelpContext = 0
        end
        inherited sPanel15: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          inherited sLabel2: TsLabel
            Left = 1
            Top = 3
          end
        end
        inherited edt_Doccd: TsEdit
          Left = 128
          Top = 26
          Width = 35
          HelpType = htKeyword
          HelpKeyword = 'Form of Documentary Credit'
          MaxLength = 3
          TabOrder = 0
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_TermPR_M: TsEdit
          Left = 187
          Top = 491
          Width = 196
          HelpContext = 0
          MaxLength = 65
          TabOrder = 12
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_TermPR: TsEdit
          Left = 128
          Top = 491
          HelpType = htKeyword
          HelpKeyword = 'Terms of Price'
          MaxLength = 3
          TabOrder = 11
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sPanel11: TsPanel
          Left = 421
          Top = 250
          Width = 370
          Height = 23
          inherited sLabel10: TsLabel
            Left = 1
            Top = 3
          end
        end
        inherited edt_plTerm: TsEdit
          Left = 455
          Top = 274
          Width = 336
          HelpContext = 0
          MaxLength = 65
          TabOrder = 21
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sPanel12: TsPanel
          Left = 421
          Top = 324
          Width = 370
          Height = 23
          inherited sLabel11: TsLabel
            Left = 1
            Top = 3
          end
        end
        inherited edt_aaCcv1: TsEdit
          Left = 455
          Top = 348
          Width = 336
          HelpContext = 0
          MaxLength = 35
          TabOrder = 22
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_aaCcv2: TsEdit
          Left = 455
          Top = 370
          Width = 336
          HelpContext = 0
          MaxLength = 35
          TabOrder = 23
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_aaCcv3: TsEdit
          Left = 455
          Top = 392
          Width = 336
          HelpContext = 0
          MaxLength = 35
          TabOrder = 24
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_aaCcv4: TsEdit
          Left = 455
          Top = 414
          Width = 336
          HelpContext = 0
          MaxLength = 35
          TabOrder = 25
          OnKeyDown = Mask_fromDateKeyDown
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sSplitter5: TsSplitter
        Width = 1106
      end
      inherited sPanel13: TsPanel
        TabOrder = 1
      end
      inherited sPanel14: TsPanel
        Width = 806
        TabOrder = 0
        inherited sSpeedButton1: TsSpeedButton
          Left = 329
          Top = 0
          Height = 559
        end
        inherited sLabel16: TsLabel
          Left = 646
          Top = 8
        end
        object sLabel56: TsLabel [2]
          Left = 367
          Top = 445
          Width = 4
          Height = 15
        end
        inherited sPanel28: TsPanel
          Left = 15
          Top = 2
          Width = 300
          TabOrder = 12
        end
        inherited edt_Draft1: TsEdit
          Left = 15
          Top = 26
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 0
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_Draft3: TsEdit
          Left = 15
          Top = 70
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 2
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_Draft2: TsEdit
          Left = 15
          Top = 48
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 1
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sPanel29: TsPanel
          Left = 15
          Top = 145
          Width = 300
          TabOrder = 14
        end
        inherited edt_mixPay1: TsEdit
          Left = 15
          Top = 169
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 3
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_mixPay2: TsEdit
          Left = 15
          Top = 191
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 4
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_mixPay3: TsEdit
          Left = 15
          Top = 213
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 5
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_mixPay4: TsEdit
          Left = 15
          Top = 235
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 6
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sPanel30: TsPanel
          Left = 15
          Top = 310
          Width = 300
          TabOrder = 13
        end
        inherited edt_defPay1: TsEdit
          Left = 15
          Top = 334
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 7
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_defPay2: TsEdit
          Left = 15
          Top = 356
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 8
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_defPay3: TsEdit
          Left = 15
          Top = 378
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 9
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited edt_defPay4: TsEdit
          Left = 15
          Top = 400
          Width = 300
          HelpContext = 0
          MaxLength = 35
          TabOrder = 10
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sPanel10: TsPanel
          Left = 345
          Top = 2
          Width = 300
        end
        inherited btn_DescriptionServices: TsBitBtn
          Left = 345
          Top = 26
        end
        inherited memo_Desgood1: TsMemo
          Tag = 122
          Left = 365
          Top = 26
          Width = 412
          Hint = 'DesGood'
          Font.Name = #44404#47548#52404
          TabOrder = 11
          OnChange = memo_RowColPrint
          OnKeyPress = memoLineLimit
          SkinData.CustomFont = True
        end
      end
    end
    inherited sTabSheet4: TsTabSheet
      inherited sSplitter8: TsSplitter
        Width = 1106
      end
      inherited sPanel27: TsPanel
        TabOrder = 1
      end
      inherited sPanel31: TsPanel
        Width = 806
        TabOrder = 0
        object sLabel24: TsLabel
          Left = 424
          Top = 254
          Width = 279
          Height = 17
          Caption = #49688#53441'('#48156#49569')'#51648' : '#50868#49569#49688#45800#51060' DQ '#51068#46412#47564' '#51077#47141#44032#45733
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object sLabel27: TsLabel
          Left = 424
          Top = 332
          Width = 269
          Height = 17
          Caption = #52572#51333#47785#51201#51648' : '#50868#49569#49688#45800#51060' DQ '#51068#46412#47564' '#51077#47141#44032#45733
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object sLabel28: TsLabel
          Left = 400
          Top = 312
          Width = 22
          Height = 17
          Caption = '44B'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel30: TsLabel
          Left = 509
          Top = 419
          Width = 218
          Height = 17
          Caption = #50808#54872#51008#54665#51068#44221#50864' '#47749#52845#51012' '#49325#51228#54644#51452#49464#50836
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object sSpeedButton7: TsSpeedButton
          Left = 398
          Top = 0
          Width = 10
          Height = 559
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object edt_Carriage1: TsEdit
          Left = 561
          Top = 29
          Width = 236
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel32: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          Caption = 'Latest Date of Shipment'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel17: TsLabel
            Left = 1
            Top = 3
            Width = 22
            Height = 17
            Caption = '44C'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_lstDate: TsMaskEdit
          Left = 150
          Top = 29
          Width = 77
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          OnKeyDown = Mask_fromDateKeyDown
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'Latest Date of Shipment'
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel33: TsPanel
          Left = 15
          Top = 109
          Width = 370
          Height = 23
          Caption = 'Shipment Period'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel18: TsLabel
            Left = 1
            Top = 3
            Width = 23
            Height = 17
            Caption = '44D'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_shipPD1: TsEdit
          Left = 15
          Top = 133
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD2: TsEdit
          Left = 15
          Top = 155
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD3: TsEdit
          Left = 15
          Top = 177
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD4: TsEdit
          Left = 15
          Top = 199
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD5: TsEdit
          Left = 15
          Top = 221
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD6: TsEdit
          Left = 15
          Top = 243
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Tship: TsEdit
          Tag = 123
          Left = 88
          Top = 422
          Width = 43
          Height = 21
          Hint = 'TSHIP'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          OnChange = edt_In_MathodChange
          OnDblClick = edt_APP700CodeTypeDblClick
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'Transhipment'
        end
        object sBitBtn26: TsBitBtn
          Tag = 123
          Left = 132
          Top = 422
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 18
          TabStop = False
          OnClick = btn_APP700CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Tship1: TsEdit
          Left = 155
          Top = 422
          Width = 230
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel35: TsPanel
          Tag = 123
          Left = 15
          Top = 395
          Width = 370
          Height = 23
          Caption = 'Transhipment'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel20: TsLabel
            Left = 1
            Top = 2
            Width = 21
            Height = 17
            Caption = '43T'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_Pship: TsEdit
          Tag = 122
          Left = 88
          Top = 335
          Width = 43
          Height = 21
          Hint = 'PSHIP'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          OnChange = edt_In_MathodChange
          OnDblClick = edt_APP700CodeTypeDblClick
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = 'Partial Shipment'
        end
        object sBitBtn17: TsBitBtn
          Tag = 122
          Left = 132
          Top = 335
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 21
          TabStop = False
          OnClick = btn_APP700CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Pship1: TsEdit
          Left = 155
          Top = 335
          Width = 230
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel34: TsPanel
          Left = 15
          Top = 308
          Width = 370
          Height = 23
          Caption = 'Partial Shipment'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel19: TsLabel
            Left = 1
            Top = 2
            Width = 21
            Height = 17
            Caption = '43P'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object sPanel37: TsPanel
          Left = 425
          Top = 109
          Width = 376
          Height = 23
          Caption = 'Port of Loading/Airport of Departure'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel26: TsLabel
            Left = 27
            Top = 3
            Width = 49
            Height = 17
            Caption = '['#49440#51201#54637']'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
          end
          object sLabel21: TsLabel
            Left = 1
            Top = 3
            Width = 21
            Height = 17
            Caption = '44E'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_SunjukPort: TsEdit
          Left = 424
          Top = 136
          Width = 376
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object edt_dochackPort: TsEdit
          Left = 424
          Top = 194
          Width = 376
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel38: TsPanel
          Left = 424
          Top = 167
          Width = 376
          Height = 23
          Caption = 'Port of Discharge/Airport of Destination'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel22: TsLabel
            Left = 1
            Top = 3
            Width = 20
            Height = 17
            Caption = '44F'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
          object sLabel25: TsLabel
            Left = 27
            Top = 3
            Width = 49
            Height = 17
            Caption = '['#46020#52265#54637']'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
          end
        end
        object edt_loadOn: TsEdit
          Left = 424
          Top = 272
          Width = 376
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel39: TsPanel
          Left = 424
          Top = 230
          Width = 376
          Height = 23
          Caption = 
            '     Place of Taking in Charge/Dispatch from '#183#183#183'/Place of Receip' +
            't'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel23: TsLabel
            Left = 1
            Top = 3
            Width = 23
            Height = 17
            Caption = '44A'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object sPanel40: TsPanel
          Left = 424
          Top = 308
          Width = 376
          Height = 23
          Caption = 
            'Place of Final Destination/For Transportation to '#183#183#183'/Place of De' +
            'livery'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_forTran: TsEdit
          Left = 424
          Top = 350
          Width = 376
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Origin: TsEdit
          Tag = 125
          Left = 494
          Top = 437
          Width = 43
          Height = 21
          Hint = #44397#44032
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          OnChange = edt_In_MathodChange
          OnDblClick = edt_APP700CodeTypeDblClick
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn28: TsBitBtn
          Tag = 125
          Left = 538
          Top = 437
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 28
          TabStop = False
          OnClick = btn_APP700CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Origin_M: TsEdit
          Left = 561
          Top = 437
          Width = 236
          Height = 21
          CharCase = ecUpperCase
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel41: TsPanel
          Left = 424
          Top = 395
          Width = 376
          Height = 23
          Caption = 'Country of Origin'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel29: TsLabel
            Left = 1
            Top = 3
            Width = 23
            Height = 17
            Caption = '45A'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object sPanel36: TsPanel
          Left = 421
          Top = 2
          Width = 376
          Height = 23
          Caption = #50868#49569#49688#45800
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_Carriage: TsEdit
          Tag = 124
          Left = 494
          Top = 29
          Width = 43
          Height = 21
          Hint = 'C_METHOD'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          OnChange = edt_In_MathodChange
          OnDblClick = edt_APP700CodeTypeDblClick
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = #50868#49569#49688#45800
        end
        object sBitBtn27: TsBitBtn
          Tag = 124
          Left = 538
          Top = 29
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 31
          TabStop = False
          OnClick = btn_APP700CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
      end
    end
    inherited sTabSheet5: TsTabSheet
      inherited sSplitter6: TsSplitter
        Width = 1106
      end
      inherited sPanel42: TsPanel
        TabOrder = 1
      end
      inherited sPanel43: TsPanel
        Width = 806
        TabOrder = 0
        object sLabel31: TsLabel
          Left = 296
          Top = 35
          Width = 40
          Height = 15
          Caption = 'COPIES'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel33: TsLabel
          Left = 160
          Top = 63
          Width = 352
          Height = 15
          Caption = 'OF CLEAN ON BOARD OCEAN BILLIS OF LADING MADE OUT TO'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel32: TsLabel
          Left = 104
          Top = 63
          Width = 4
          Height = 15
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel35: TsLabel
          Left = 48
          Top = 87
          Width = 4
          Height = 15
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel36: TsLabel
          Left = 40
          Top = 111
          Width = 97
          Height = 15
          Caption = 'MARKED FREIGHT'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel37: TsLabel
          Left = 424
          Top = 111
          Width = 70
          Height = 15
          Caption = 'AND NOTIFY'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel42: TsLabel
          Left = 424
          Top = 166
          Width = 70
          Height = 15
          Caption = 'AND NOTIFY'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel39: TsLabel
          Left = 40
          Top = 166
          Width = 97
          Height = 15
          Caption = 'MARKED FREIGHT'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel40: TsLabel
          Left = 48
          Top = 219
          Width = 82
          Height = 15
          Caption = 'THE ORDER OF'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel43: TsLabel
          Left = 424
          Top = 243
          Width = 70
          Height = 15
          Caption = 'AND NOTIFY'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel38: TsLabel
          Left = 44
          Top = 295
          Width = 638
          Height = 15
          Caption = 
            'EXPRESSLY STIPULATING THAT CLAIMS ARE PAYABLE IN KOREA AND IT MU' +
            'ST INCLUDE : INSTITUTE CARGO CLAUSE'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel44: TsLabel
          Left = 184
          Top = 367
          Width = 40
          Height = 15
          Caption = 'COPIES'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel45: TsLabel
          Left = 224
          Top = 417
          Width = 146
          Height = 17
          Caption = #52572#45824' 400'#54665' 64'#50676' '#51077#47141' '#21487
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel41: TsLabel
          Left = 40
          Top = 243
          Width = 97
          Height = 15
          Caption = 'MARKED FREIGHT'
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
        end
        object sLabel57: TsLabel
          Left = 156
          Top = 537
          Width = 4
          Height = 15
          Alignment = taRightJustify
        end
        object sPanel47: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          Caption = 'Documents Required ...'
          Color = 16042877
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          object sLabel34: TsLabel
            Left = 1
            Top = 3
            Width = 23
            Height = 17
            Caption = '46A'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clGray
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object edt_Doc380_1: TsEdit
          Tag = 500
          Left = 256
          Top = 33
          Width = 33
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_Doc380: TsCheckBox
          Tag = 500
          Left = 24
          Top = 34
          Width = 232
          Height = 19
          Caption = 'ASSIGNED COMMERCIAL INVOICE IN'
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = check_Doc380Click
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object check_Doc705: TsCheckBox
          Tag = 501
          Left = 24
          Top = 62
          Width = 20
          Height = 16
          Hint = 'doc705'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          OnClick = check_Doc380Click
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object edt_Doc705Gubun: TsEdit
          Tag = 501
          Left = 45
          Top = 61
          Width = 33
          Height = 21
          Hint = 'DOC_705CD'
          HelpType = htKeyword
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          OnChange = edt_Doc705GubunChange
          OnDblClick = edt_APP700CodeTypeDblClick
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn29: TsBitBtn
          Tag = 501
          Left = 79
          Top = 61
          Width = 18
          Height = 21
          Cursor = crHandPoint
          Hint = 'DOC_705CD'
          Enabled = False
          TabOrder = 30
          TabStop = False
          OnClick = btn_APP700CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Doc705_1: TsEdit
          Tag = 501
          Left = 141
          Top = 85
          Width = 307
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc705_3: TsEdit
          Tag = 502
          Left = 141
          Top = 109
          Width = 33
          Height = 21
          Hint = 'DOC_705'
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          OnChange = edt_In_MathodChange
          OnDblClick = edt_APP700CodeTypeDblClick
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn30: TsBitBtn
          Tag = 502
          Left = 175
          Top = 109
          Width = 19
          Height = 21
          Cursor = crHandPoint
          Hint = 'doc705'
          Enabled = False
          TabOrder = 31
          TabStop = False
          OnClick = btn_APP700CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Doc705_3_1: TsEdit
          Tag = 502
          Left = 195
          Top = 109
          Width = 224
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 7
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc705_2: TsEdit
          Tag = 501
          Left = 450
          Top = 85
          Width = 307
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc705_4: TsEdit
          Tag = 501
          Left = 499
          Top = 109
          Width = 258
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_Doc740: TsCheckBox
          Tag = 503
          Left = 24
          Top = 141
          Width = 189
          Height = 19
          Hint = 'doc740'
          Caption = 'AIRWAY BILL CONSIGNED TO'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 9
          OnClick = check_Doc380Click
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object edt_Doc740_1: TsEdit
          Tag = 503
          Left = 214
          Top = 140
          Width = 270
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc740_2: TsEdit
          Tag = 503
          Left = 486
          Top = 140
          Width = 270
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 11
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc740_4: TsEdit
          Tag = 503
          Left = 499
          Top = 164
          Width = 258
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 14
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc740_3_1: TsEdit
          Tag = 503
          Left = 196
          Top = 164
          Width = 224
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 13
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn32: TsBitBtn
          Tag = 503
          Left = 175
          Top = 164
          Width = 19
          Height = 21
          Cursor = crHandPoint
          Hint = 'doc740'
          Enabled = False
          TabOrder = 32
          TabStop = False
          OnClick = btn_APP700CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Doc740_3: TsEdit
          Tag = 503
          Left = 141
          Top = 164
          Width = 33
          Height = 21
          Hint = 'DOC_740'
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
          OnChange = edt_In_MathodChange
          OnDblClick = edt_APP700CodeTypeDblClick
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_Doc760: TsCheckBox
          Tag = 504
          Left = 24
          Top = 194
          Width = 442
          Height = 19
          Hint = 'doc760'
          Caption = 'FULL SET OF CLEAN MULTIMODAL TRANSPORT DOCUMENT MADE OUT TO'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 15
          OnClick = check_Doc380Click
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object edt_Doc760_1: TsEdit
          Tag = 504
          Left = 141
          Top = 217
          Width = 307
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 16
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc760_3: TsEdit
          Tag = 504
          Left = 141
          Top = 241
          Width = 33
          Height = 21
          Hint = 'DOC_740'
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 18
          OnChange = edt_In_MathodChange
          OnDblClick = edt_APP700CodeTypeDblClick
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn31: TsBitBtn
          Tag = 504
          Left = 175
          Top = 241
          Width = 19
          Height = 21
          Cursor = crHandPoint
          Hint = 'doc760'
          Enabled = False
          TabOrder = 33
          TabStop = False
          OnClick = btn_APP700CodeTypeClick
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Doc760_3_1: TsEdit
          Tag = 504
          Left = 195
          Top = 241
          Width = 224
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 19
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc760_2: TsEdit
          Tag = 504
          Left = 450
          Top = 217
          Width = 307
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 17
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc760_4: TsEdit
          Tag = 504
          Left = 499
          Top = 241
          Width = 258
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 20
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_Doc530: TsCheckBox
          Tag = 505
          Left = 24
          Top = 274
          Width = 637
          Height = 19
          Caption = 
            'FULL SET OF INSURANCE POLICIES OR CERTIFICATES, ENDORSED'#13#10'IN BLA' +
            'NK FOR 110% OF THE INVOICE VALUE,'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 21
          OnClick = check_Doc380Click
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object edt_Doc530_1: TsEdit
          Tag = 505
          Left = 45
          Top = 312
          Width = 444
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 22
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Doc530_2: TsEdit
          Tag = 505
          Left = 45
          Top = 336
          Width = 444
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 65
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 23
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_Doc271: TsCheckBox
          Tag = 506
          Left = 24
          Top = 366
          Width = 120
          Height = 19
          Caption = 'PACKING LIST IN'
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
          OnClick = check_Doc380Click
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object edt_Doc271_1: TsEdit
          Tag = 506
          Left = 144
          Top = 365
          Width = 33
          Height = 21
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 25
          OnKeyDown = Mask_fromDateKeyDown
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object check_Doc861: TsCheckBox
          Tag = 507
          Left = 24
          Top = 390
          Width = 157
          Height = 19
          Caption = 'CRETIFICATE OF ORIGIN'
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 26
          OnClick = check_Doc380Click
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object check_doc2AA: TsCheckBox
          Tag = 508
          Left = 24
          Top = 414
          Width = 200
          Height = 19
          Hint = 'doc2aa'
          Caption = 'OTHER DOCUMENT(S) ( if any )'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = 5197647
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 27
          OnClick = check_Doc380Click
          ImgChecked = 0
          ImgUnchecked = 0
          SkinData.SkinSection = 'CHECKBOX'
        end
        object memo_doc2AA1: TsMemo
          Tag = 508
          Left = 163
          Top = 437
          Width = 412
          Height = 118
          Hint = 'doc2AA'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548#52404
          Font.Style = []
          MaxLength = 25600
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 28
          OnChange = memo_RowColPrint
          OnDblClick = memo_doc2AA1DblClick
          OnKeyDown = Mask_fromDateKeyDown
          OnKeyPress = memoLineLimit
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn33: TsBitBtn
          Tag = 124
          Left = 143
          Top = 437
          Width = 19
          Height = 21
          Cursor = crHandPoint
          Hint = 'doc2aa'
          Enabled = False
          TabOrder = 34
          TabStop = False
          OnClick = sBitBtn33Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
      end
    end
    inherited sTabSheet6: TsTabSheet
      inherited sSplitter9: TsSplitter
        Width = 1106
      end
      inherited sPanel44: TsPanel
        TabOrder = 1
      end
      inherited sPanel45: TsPanel
        Width = 806
        TabOrder = 0
        inherited sLabel46: TsLabel
          Left = 240
          Top = 116
        end
        inherited sLabel48: TsLabel
          Top = 353
        end
        inherited sLabel50: TsLabel
          Top = 375
        end
        inherited sLabel52: TsLabel
          Top = 442
        end
        inherited sLabel53: TsLabel
          Top = 442
        end
        inherited sLabel54: TsLabel
          Top = 464
        end
        object sLabel58: TsLabel [6]
          Left = 124
          Top = 289
          Width = 4
          Height = 15
        end
        inherited edt_Confirm1: TsEdit [7]
          Left = 141
          Top = 527
          Width = 244
          HelpContext = 0
        end
        inherited check_acd2AA: TsCheckBox [8]
          Tag = 601
          Left = 24
          Top = 31
          Hint = 'acd2AA'
          TabOrder = 0
          OnClick = checkPage6Click
        end
        inherited sPanel46: TsPanel [9]
          Left = 15
          Top = 2
          Width = 370
          TabOrder = 10
          inherited sLabel49: TsLabel
            Caption = '47A'
          end
        end
        inherited edt_acd2AA1: TsEdit [10]
          Tag = 601
          Left = 125
          Top = 31
          Color = clBtnFace
          MaxLength = 35
          TabOrder = 1
          SkinData.CustomColor = True
        end
        inherited check_acd2AB: TsCheckBox [11]
          Tag = 602
          Left = 24
          Top = 53
          Hint = 'acd2AB'
        end
        inherited check_acd2AC: TsCheckBox [12]
          Tag = 603
          Left = 24
          Top = 74
          Hint = 'acd2AC'
        end
        inherited check_acd2AD: TsCheckBox [13]
          Tag = 604
          Left = 24
          Top = 95
          Hint = 'acd2AD'
        end
        inherited check_acd2AE: TsCheckBox [14]
          Tag = 605
          Left = 24
          Top = 115
          Hint = 'acd2AE'
          OnClick = checkPage6Click
        end
        inherited memo_acd2AE1: TsMemo [15]
          Tag = 605
          Left = 131
          Top = 137
          Width = 412
          Hint = 'acd2AE'
          Font.Name = #44404#47548#52404
          TabOrder = 6
          OnChange = memo_RowColPrint
          OnDblClick = memo_doc2AA1DblClick
          OnKeyPress = memoLineLimit
          SkinData.CustomFont = True
        end
        inherited sBitBtn34: TsBitBtn [16]
          Left = 111
          Top = 137
          Hint = 'acd2AE'
        end
        inherited sPanel48: TsPanel [17]
          Left = 15
          Top = 322
          Width = 370
        end
        inherited edt_Charge: TsEdit [18]
          Top = 373
          HelpContext = 1
          MaxLength = 3
          TabOrder = 7
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
          BoundLabel.Caption = 'Charges'
        end
        inherited sBitBtn35: TsBitBtn [19]
          Top = 373
        end
        inherited sPanel49: TsPanel [20]
          Left = 15
          Top = 409
          Width = 370
        end
        inherited edt_period: TsEdit [21]
          Top = 440
          TabOrder = 8
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sPanel50: TsPanel [22]
          Left = 15
          Top = 498
          Width = 370
        end
        inherited edt_Confirm: TsEdit [23]
          Left = 87
          Top = 527
          MaxLength = 3
          TabOrder = 9
          OnChange = edt_In_MathodChange
          OnKeyDown = Mask_fromDateKeyDown
        end
        inherited sBitBtn36: TsBitBtn [24]
          Left = 121
          Top = 527
        end
        inherited edt_Charge1: TsEdit [25]
          Top = 373
          HelpContext = 0
        end
      end
    end
    inherited sTabSheet7: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      inherited sSplitter2: TsSplitter
        Width = 1106
      end
      inherited sDBGrid1: TsDBGrid
        Width = 1106
        DataSource = dsList
        PopupMenu = popMenu1
        OnDrawColumnCell = sDBGrid2DrawColumnCell
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 74
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 213
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 193
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 164
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 82
            Visible = True
          end>
      end
      inherited sPanel3: TsPanel
        Width = 1106
        inherited edt_SearchText: TsEdit
          Height = 23
          TabOrder = 2
        end
        inherited com_SearchKeyword: TsComboBox
          TabOrder = 4
          TabStop = False
          OnSelect = com_SearchKeywordSelect
        end
        inherited Mask_SearchDate1: TsMaskEdit
          Height = 23
          TabOrder = 0
          OnDblClick = sMaskEdit1DblClick
        end
        inherited sBitBtn1: TsBitBtn
          TabOrder = 3
          OnClick = sBitBtn1Click
        end
        inherited sBitBtn21: TsBitBtn
          Tag = 902
          TabOrder = 5
          TabStop = False
          OnClick = btn_CalClick
        end
        inherited Mask_SearchDate2: TsMaskEdit
          Height = 23
          TabOrder = 1
          OnDblClick = sMaskEdit1DblClick
        end
        inherited sBitBtn23: TsBitBtn
          Tag = 903
          TabOrder = 6
          TabStop = False
          OnClick = btn_CalClick
        end
      end
    end
  end
  inherited sPanel1: TsPanel
    Width = 1114
    inherited sSpeedButton2: TsSpeedButton
      Left = 412
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 704
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 781
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 491
    end
    object sLabel59: TsLabel [4]
      Left = 8
      Top = 5
      Width = 187
      Height = 17
      Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#44060#49444#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel [5]
      Left = 8
      Top = 20
      Width = 40
      Height = 13
      Caption = 'APP700'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton8: TsSpeedButton [6]
      Left = 199
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton9: TsSpeedButton [7]
      Left = 960
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    inherited btnExit: TsButton
      Left = 1039
      OnClick = btnExitClick
    end
    inherited btnNew: TsButton
      Left = 209
      OnClick = btnNewClick
    end
    inherited btnEdit: TsButton
      Left = 276
      OnClick = btnEditClick
    end
    inherited btnDel: TsButton
      Left = 343
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      Left = 422
      OnClick = btnCopyClick
    end
    inherited btnPrint: TsButton
      Left = 712
      OnClick = btnPrintClick
    end
    inherited btnTemp: TsButton
      Left = 501
      OnClick = btnTempClick
    end
    inherited btnSave: TsButton
      Left = 568
      OnClick = btnSaveClick
    end
    inherited btnCancel: TsButton
      Left = 635
      OnClick = btnCancelClick
    end
    inherited sButton1: TsButton
      Left = 791
      OnClick = sButton1Click
    end
    inherited sButton3: TsButton
      Left = 884
      OnClick = sButton3Click
    end
  end
  inherited sPanel4: TsPanel
    Width = 1114
    inherited edt_MaintNo: TsEdit
      Color = clWhite
      MaxLength = 35
      OnKeyDown = Mask_fromDateKeyDown
    end
    inherited sMaskEdit1: TsMaskEdit
      TabOrder = 2
      OnChange = sMaskEdit1Change
      OnDblClick = sMaskEdit1DblClick
      OnKeyDown = Mask_fromDateKeyDown
    end
    inherited edt_UserNo: TsEdit
      TabOrder = 1
      OnKeyDown = Mask_fromDateKeyDown
    end
    inherited btn_Cal: TsBitBtn
      OnClick = btn_CalClick
    end
    inherited edt_msg1: TsEdit
      Left = 532
      Color = clWhite
      MaxLength = 3
      OnKeyDown = Mask_fromDateKeyDown
    end
    inherited sBitBtn2: TsBitBtn
      Left = 565
    end
    inherited edt_msg2: TsEdit
      Left = 630
      Color = clWhite
      MaxLength = 3
      OnKeyDown = Mask_fromDateKeyDown
    end
    inherited sBitBtn3: TsBitBtn
      Left = 663
    end
  end
  inherited sPanel52: TsPanel
    Top = 115
    Height = 662
    inherited sDBGrid2: TsDBGrid
      Height = 627
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      PopupMenu = popMenu1
      OnDrawColumnCell = sDBGrid2DrawColumnCell
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #49345#54889
          Width = 41
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 162
          Visible = True
        end>
    end
    inherited sPanel53: TsPanel
      inherited Mask_fromDate: TsMaskEdit
        TabStop = True
        OnKeyDown = Mask_fromDateKeyDown
      end
      inherited sBitBtn22: TsBitBtn
        TabOrder = 2
        TabStop = True
        OnClick = sBitBtn22Click
      end
      inherited Mask_toDate: TsMaskEdit
        TabStop = True
        TabOrder = 1
        OnKeyDown = Mask_fromDateKeyDown
      end
    end
  end
  object queryCheck: TADOQuery [7]
    Connection = DMMssql.KISConnect
    Parameters = <>
    Left = 56
    Top = 224
  end
  inherited qryList: TADOQuery
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      ''
      ''
      
        'SELECT A700_1.MAINT_NO,A700_1.MESSAGE1,A700_1.MESSAGE2,A700_1.[U' +
        'SER_ID],A700_1.DATEE,A700_1.APP_DATE,A700_1.IN_MATHOD,A700_1.AP_' +
        'BANK,A700_1.AP_BANK1,A700_1.AP_BANK2,A700_1.AP_BANK3,A700_1.AP_B' +
        'ANK4,A700_1.AP_BANK5,A700_1.AD_BANK,A700_1.AD_BANK1,A700_1.AD_BA' +
        'NK2,A700_1.AD_BANK3,A700_1.AD_BANK4,A700_1.AD_PAY'
      
        '      ,A700_1.IL_NO1,A700_1.IL_NO2,A700_1.IL_NO3,A700_1.IL_NO4,A' +
        '700_1.IL_NO5,A700_1.IL_AMT1,A700_1.IL_AMT2,A700_1.IL_AMT3,A700_1' +
        '.IL_AMT4,A700_1.IL_AMT5,A700_1.IL_CUR1,A700_1.IL_CUR2,A700_1.IL_' +
        'CUR3,A700_1.IL_CUR4,A700_1.IL_CUR5,A700_1.AD_INFO1,A700_1.AD_INF' +
        'O2,A700_1.AD_INFO3,A700_1.AD_INFO4,A700_1.AD_INFO5'
      
        '      ,A700_1.DOC_CD,A700_1.EX_DATE,A700_1.EX_PLACE,A700_1.CHK1,' +
        'A700_1.CHK2,A700_1.CHK3,A700_1.prno,A700_1.F_INTERFACE,A700_1.IM' +
        'P_CD1,A700_1.IMP_CD2,A700_1.IMP_CD3,A700_1.IMP_CD4,A700_1.IMP_CD' +
        '5'
      ''
      
        #9'  ,A700_2.MAINT_NO,A700_2.APPLIC1,A700_2.APPLIC2,A700_2.APPLIC3' +
        ',A700_2.APPLIC4,A700_2.APPLIC5,A700_2.BENEFC,A700_2.BENEFC1,A700' +
        '_2.BENEFC2,A700_2.BENEFC3,A700_2.BENEFC4,A700_2.BENEFC5,A700_2.C' +
        'D_AMT,A700_2.CD_CUR,A700_2.CD_PERP,A700_2.CD_PERM,A700_2.CD_MAX,' +
        'A700_2.AA_CV1,A700_2.AA_CV2,A700_2.AA_CV3'
      
        '      ,A700_2.AA_CV4,A700_2.DRAFT1,A700_2.DRAFT2,A700_2.DRAFT3,A' +
        '700_2.MIX_PAY1,A700_2.MIX_PAY2,A700_2.MIX_PAY3,A700_2.MIX_PAY4,A' +
        '700_2.DEF_PAY1,A700_2.DEF_PAY2,A700_2.DEF_PAY3,A700_2.DEF_PAY4,A' +
        '700_2.PSHIP,A700_2.TSHIP,A700_2.LOAD_ON,A700_2.FOR_TRAN,A700_2.L' +
        'ST_DATE,A700_2.SHIP_PD1,A700_2.SHIP_PD2'
      
        '      ,A700_2.SHIP_PD3,A700_2.SHIP_PD4,A700_2.SHIP_PD5,A700_2.SH' +
        'IP_PD6,A700_2.DESGOOD_1'
      ''
      
        #9'  ,A700_3.MAINT_NO,A700_3.DOC_380,A700_3.DOC_380_1,A700_3.DOC_7' +
        '05,A700_3.DOC_705_1,A700_3.DOC_705_2,A700_3.DOC_705_3,A700_3.DOC' +
        '_705_4,A700_3.DOC_740,A700_3.DOC_740_1,A700_3.DOC_740_2,A700_3.D' +
        'OC_740_3,A700_3.DOC_740_4,A700_3.DOC_530,A700_3.DOC_530_1,A700_3' +
        '.DOC_530_2,A700_3.DOC_271'
      
        '      ,A700_3.DOC_271_1,A700_3.DOC_861,A700_3.DOC_2AA,A700_3.DOC' +
        '_2AA_1,A700_3.ACD_2AA,A700_3.ACD_2AA_1,A700_3.ACD_2AB,A700_3.ACD' +
        '_2AC,A700_3.ACD_2AD,A700_3.ACD_2AE,A700_3.ACD_2AE_1,A700_3.CHARG' +
        'E,A700_3.PERIOD,A700_3.CONFIRMM,A700_3.INSTRCT,A700_3.INSTRCT_1,' +
        'A700_3.EX_NAME1,A700_3.EX_NAME2'
      
        '      ,A700_3.EX_NAME3,A700_3.EX_ADDR1,A700_3.EX_ADDR2,A700_3.OR' +
        'IGIN,A700_3.ORIGIN_M,A700_3.PL_TERM,A700_3.TERM_PR,A700_3.TERM_P' +
        'R_M,A700_3.SRBUHO,A700_3.DOC_705_GUBUN,A700_3.CARRIAGE,A700_3.DO' +
        'C_760,A700_3.DOC_760_1,A700_3.DOC_760_2,A700_3.DOC_760_3,A700_3.' +
        'DOC_760_4,A700_3.SUNJUCK_PORT,A700_3.DOCHACK_PORT'
      ''
      #9'  ,Mathod700.DOC_NAME as mathod_Name'
      #9'  ,Pay700.DOC_NAME as pay_Name'
      #9'  ,IMPCD700_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD700_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD700_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD700_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD700_5.DOC_NAME as Imp_Name_5'
      #9'  ,DocCD700.DOC_NAME as DOC_Name'
      #9'  ,CDMAX700.DOC_NAME as CDMAX_Name'
      #9'  ,Pship700.DOC_NAME as pship_Name'
      #9'  ,Tship700.DOC_NAME as tship_Name'
      #9'  ,doc705_700.DOC_NAME as doc705_Name'
      #9'  ,doc740_700.DOC_NAME as doc740_Name'
      #9'  ,doc760_700.DOC_NAME as doc760_Name'
      #9'  ,CHARGE700.DOC_NAME as CHARGE_Name'
      #9'  ,CONFIRM700.DOC_NAME as CONFIRM_Name'
      #9'  ,CARRIAGE700.DOC_NAME as CARRIAGE_NAME'
      ''
      'FROM [dbo].[APP700_1] AS A700_1'
      
        'INNER JOIN [dbo].[APP700_2] AS A700_2 ON A700_1.MAINT_NO = A700_' +
        '2.MAINT_NO'
      
        'INNER JOIN [dbo].[APP700_3] AS A700_3 ON A700_1.MAINT_NO = A700_' +
        '3.MAINT_NO'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod700 ON A700_1.IN_MATHOD = Mathod' +
        '700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay700 ON A700_1.AD_PAY = Pay700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_1 ON A700_1.IMP_CD1 = IMPCD' +
        '700_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_2 ON A700_1.IMP_CD2 = IMPCD' +
        '700_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_3 ON A700_1.IMP_CD3 = IMPCD' +
        '700_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_4 ON A700_1.IMP_CD4 = IMPCD' +
        '700_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD700_5 ON A700_1.IMP_CD5 = IMPCD' +
        '700_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_CD'#39') DocCD700 ON A700_1.DOC_CD = DocCD700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX700 ON A700_2.CD_MAX = CDMAX700' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'PSHIP'#39') Pship700 ON A700_2.PSHIP = Pship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'TSHIP'#39') Tship700 ON A700_2.TSHIP = Tship700.C' +
        'ODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_705'#39') doc705_700 ON A700_3.DOC_705_3 = do' +
        'c705_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc740_700 ON A700_3.DOC_740_3 = do' +
        'c740_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'DOC_740'#39') doc760_700 ON A700_3.DOC_760_3 = do' +
        'c760_700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CHARGE'#39') CHARGE700 ON A700_3.CHARGE = CHARGE7' +
        '00.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CONFIRM'#39') CONFIRM700 ON A700_3.CONFIRMM = CON' +
        'FIRM700.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'C_METHOD'#39') CARRIAGE700 ON A700_3.CARRIAGE = C' +
        'ARRIAGE700.CODE'
      ''
      ''
      ''
      '')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 4
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 70
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListDOC_CD: TStringField
      FieldName = 'DOC_CD'
      Size = 3
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListprno: TIntegerField
      FieldName = 'prno'
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListCD_AMT: TBCDField
      FieldName = 'CD_AMT'
      Precision = 18
    end
    object qryListCD_CUR: TStringField
      FieldName = 'CD_CUR'
      Size = 3
    end
    object qryListCD_PERP: TBCDField
      FieldName = 'CD_PERP'
      Precision = 18
    end
    object qryListCD_PERM: TBCDField
      FieldName = 'CD_PERM'
      Precision = 18
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListDRAFT1: TStringField
      FieldName = 'DRAFT1'
      Size = 35
    end
    object qryListDRAFT2: TStringField
      FieldName = 'DRAFT2'
      Size = 35
    end
    object qryListDRAFT3: TStringField
      FieldName = 'DRAFT3'
      Size = 35
    end
    object qryListMIX_PAY1: TStringField
      FieldName = 'MIX_PAY1'
      Size = 35
    end
    object qryListMIX_PAY2: TStringField
      FieldName = 'MIX_PAY2'
      Size = 35
    end
    object qryListMIX_PAY3: TStringField
      FieldName = 'MIX_PAY3'
      Size = 35
    end
    object qryListMIX_PAY4: TStringField
      FieldName = 'MIX_PAY4'
      Size = 35
    end
    object qryListDEF_PAY1: TStringField
      FieldName = 'DEF_PAY1'
      Size = 35
    end
    object qryListDEF_PAY2: TStringField
      FieldName = 'DEF_PAY2'
      Size = 35
    end
    object qryListDEF_PAY3: TStringField
      FieldName = 'DEF_PAY3'
      Size = 35
    end
    object qryListDEF_PAY4: TStringField
      FieldName = 'DEF_PAY4'
      Size = 35
    end
    object qryListPSHIP: TStringField
      FieldName = 'PSHIP'
      Size = 3
    end
    object qryListTSHIP: TStringField
      FieldName = 'TSHIP'
      Size = 3
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 65
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 65
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 65
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 65
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 65
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 65
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 65
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 65
    end
    object qryListDESGOOD_1: TMemoField
      FieldName = 'DESGOOD_1'
      BlobType = ftMemo
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListDOC_380: TBooleanField
      FieldName = 'DOC_380'
    end
    object qryListDOC_380_1: TBCDField
      FieldName = 'DOC_380_1'
      Precision = 18
    end
    object qryListDOC_705: TBooleanField
      FieldName = 'DOC_705'
    end
    object qryListDOC_705_1: TStringField
      FieldName = 'DOC_705_1'
      Size = 35
    end
    object qryListDOC_705_2: TStringField
      FieldName = 'DOC_705_2'
      Size = 35
    end
    object qryListDOC_705_3: TStringField
      FieldName = 'DOC_705_3'
      Size = 3
    end
    object qryListDOC_705_4: TStringField
      FieldName = 'DOC_705_4'
      Size = 35
    end
    object qryListDOC_740: TBooleanField
      FieldName = 'DOC_740'
    end
    object qryListDOC_740_1: TStringField
      FieldName = 'DOC_740_1'
      Size = 35
    end
    object qryListDOC_740_2: TStringField
      FieldName = 'DOC_740_2'
      Size = 35
    end
    object qryListDOC_740_3: TStringField
      FieldName = 'DOC_740_3'
      Size = 3
    end
    object qryListDOC_740_4: TStringField
      FieldName = 'DOC_740_4'
      Size = 35
    end
    object qryListDOC_530: TBooleanField
      FieldName = 'DOC_530'
    end
    object qryListDOC_530_1: TStringField
      FieldName = 'DOC_530_1'
      Size = 65
    end
    object qryListDOC_530_2: TStringField
      FieldName = 'DOC_530_2'
      Size = 65
    end
    object qryListDOC_271: TBooleanField
      FieldName = 'DOC_271'
    end
    object qryListDOC_271_1: TBCDField
      FieldName = 'DOC_271_1'
      Precision = 18
    end
    object qryListDOC_861: TBooleanField
      FieldName = 'DOC_861'
    end
    object qryListDOC_2AA: TBooleanField
      FieldName = 'DOC_2AA'
    end
    object qryListDOC_2AA_1: TMemoField
      FieldName = 'DOC_2AA_1'
      BlobType = ftMemo
    end
    object qryListACD_2AA: TBooleanField
      FieldName = 'ACD_2AA'
    end
    object qryListACD_2AA_1: TStringField
      FieldName = 'ACD_2AA_1'
      Size = 35
    end
    object qryListACD_2AB: TBooleanField
      FieldName = 'ACD_2AB'
    end
    object qryListACD_2AC: TBooleanField
      FieldName = 'ACD_2AC'
    end
    object qryListACD_2AD: TBooleanField
      FieldName = 'ACD_2AD'
    end
    object qryListACD_2AE: TBooleanField
      FieldName = 'ACD_2AE'
    end
    object qryListACD_2AE_1: TMemoField
      FieldName = 'ACD_2AE_1'
      BlobType = ftMemo
    end
    object qryListCHARGE: TStringField
      FieldName = 'CHARGE'
      Size = 3
    end
    object qryListPERIOD: TBCDField
      FieldName = 'PERIOD'
      Precision = 18
    end
    object qryListCONFIRMM: TStringField
      FieldName = 'CONFIRMM'
      Size = 3
    end
    object qryListINSTRCT: TBooleanField
      FieldName = 'INSTRCT'
    end
    object qryListINSTRCT_1: TMemoField
      FieldName = 'INSTRCT_1'
      BlobType = ftMemo
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListORIGIN: TStringField
      FieldName = 'ORIGIN'
      Size = 3
    end
    object qryListORIGIN_M: TStringField
      FieldName = 'ORIGIN_M'
      Size = 65
    end
    object qryListPL_TERM: TStringField
      FieldName = 'PL_TERM'
      Size = 65
    end
    object qryListTERM_PR: TStringField
      FieldName = 'TERM_PR'
      Size = 3
    end
    object qryListTERM_PR_M: TStringField
      FieldName = 'TERM_PR_M'
      Size = 65
    end
    object qryListSRBUHO: TStringField
      FieldName = 'SRBUHO'
      Size = 35
    end
    object qryListDOC_705_GUBUN: TStringField
      FieldName = 'DOC_705_GUBUN'
      Size = 3
    end
    object qryListCARRIAGE: TStringField
      FieldName = 'CARRIAGE'
      Size = 3
    end
    object qryListDOC_760: TBooleanField
      FieldName = 'DOC_760'
    end
    object qryListDOC_760_1: TStringField
      FieldName = 'DOC_760_1'
      Size = 35
    end
    object qryListDOC_760_2: TStringField
      FieldName = 'DOC_760_2'
      Size = 35
    end
    object qryListDOC_760_3: TStringField
      FieldName = 'DOC_760_3'
      Size = 3
    end
    object qryListDOC_760_4: TStringField
      FieldName = 'DOC_760_4'
      Size = 35
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListpay_Name: TStringField
      FieldName = 'pay_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListDOC_Name: TStringField
      FieldName = 'DOC_Name'
      Size = 100
    end
    object qryListCDMAX_Name: TStringField
      FieldName = 'CDMAX_Name'
      Size = 100
    end
    object qryListpship_Name: TStringField
      FieldName = 'pship_Name'
      Size = 100
    end
    object qryListtship_Name: TStringField
      FieldName = 'tship_Name'
      Size = 100
    end
    object qryListdoc705_Name: TStringField
      FieldName = 'doc705_Name'
      Size = 100
    end
    object qryListdoc740_Name: TStringField
      FieldName = 'doc740_Name'
      Size = 100
    end
    object qryListdoc760_Name: TStringField
      FieldName = 'doc760_Name'
      Size = 100
    end
    object qryListCHARGE_Name: TStringField
      FieldName = 'CHARGE_Name'
      Size = 100
    end
    object qryListCONFIRM_Name: TStringField
      FieldName = 'CONFIRM_Name'
      Size = 100
    end
    object qryListCARRIAGE_NAME: TStringField
      FieldName = 'CARRIAGE_NAME'
      Size = 100
    end
  end
  object sp_attachAPP700fromAPP700: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'AttachAPP700fromAPP700;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CopyDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@NewDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 56
    Top = 264
  end
  object popMenu1: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = popMenu1Popup
    Left = 56
    Top = 304
    object N1: TMenuItem
      Caption = #51204#49569#51456#48708
      ImageIndex = 29
      OnClick = sButton1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = btnEditClick
    end
    object N4: TMenuItem
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = btnDelClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object d1: TMenuItem
      Caption = #52636#47141#54616#44592
      object N8: TMenuItem
        Caption = #48120#47532#48372#44592
        ImageIndex = 15
        OnClick = btnPrintClick
      end
      object N9: TMenuItem
        Tag = 1
        Caption = #54532#47536#53944
        ImageIndex = 21
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = 'ADMIN'
      Enabled = False
      ImageIndex = 16
    end
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APP700_1'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 56
    Top = 344
  end
end
