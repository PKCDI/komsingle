inherited UI_APPLOG_frm: TUI_APPLOG_frm
  Left = 508
  Top = 175
  Caption = #49688#51077#54868#47932#49440#52712#48372#51613'('#51064#46020#49849#46973')'#49888#52397#49436
  ClientWidth = 1114
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  inherited sSplitter12: TsSplitter
    Top = 0
    Width = 1114
  end
  inherited sSplitter1: TsSplitter
    Width = 1114
  end
  object sSpeedButton6: TsSpeedButton [2]
    Left = 752
    Top = 10
    Width = 23
    Height = 22
    SkinData.SkinSection = 'SPEEDBUTTON'
  end
  inherited sPanel1: TsPanel
    Top = 3
    Width = 1114
    inherited btnExit: TsButton
      Left = 1038
    end
    inherited btnNew: TsButton
      OnClick = btnNewClick
    end
    inherited btnEdit: TsButton
      OnClick = btnEditClick
    end
    inherited btnDel: TsButton
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      OnClick = btnCopyClick
    end
    inherited btnPrint: TsButton
      OnClick = btnPrintClick
    end
    inherited btnTemp: TsButton
      OnClick = btnSaveClick
    end
    inherited btnSave: TsButton
      OnClick = btnSaveClick
    end
    inherited btnCancel: TsButton
      OnClick = btnCancelClick
    end
    inherited btnReady: TsButton
      OnClick = btnReadyClick
    end
    inherited btnSend: TsButton
      OnClick = btnSendClick
    end
  end
  inherited sPanel4: TsPanel
    Width = 1114
    inherited mask_DATEE: TsMaskEdit
      OnDblClick = mask_DATEEDblClick
    end
    inherited btn_Cal: TsBitBtn
      Tag = 1
      OnClick = btn_CalClick
    end
  end
  inherited sPageControl1: TsPageControl
    Width = 1114
    inherited sTabSheet1: TsTabSheet
      inherited sSplitter3: TsSplitter
        Width = 1106
      end
      inherited sPanel2: TsPanel
        Width = 814
        DesignSize = (
          814
          559)
        inherited edt_msg1: TsEdit
          Tag = 101
          Hint = 'APPLOG'#44396#48516
          TabOrder = 3
        end
        inherited sBitBtn6: TsBitBtn
          TabOrder = 30
        end
        inherited edt_msg1Name: TsEdit
          Width = 214
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 31
          SkinData.CustomColor = True
        end
        inherited edt_LCG: TsEdit
          TabOrder = 4
        end
        inherited sBitBtn1: TsBitBtn
          TabOrder = 32
        end
        inherited edt_LCGNAME: TsEdit
          Width = 214
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 33
          SkinData.CustomColor = True
        end
        inherited edt_LCNO: TsEdit
          Width = 275
          TabOrder = 5
        end
        inherited edt_BLG: TsEdit
          TabOrder = 6
        end
        inherited sBitBtn2: TsBitBtn
          TabOrder = 34
        end
        inherited edt_BLGNAME: TsEdit
          Width = 214
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 35
          SkinData.CustomColor = True
        end
        inherited edt_BLNO: TsEdit
          Width = 275
          TabOrder = 7
        end
        inherited sPanel8: TsPanel
          Left = 15
          Width = 370
          TabOrder = 36
        end
        inherited mask_BLDATE: TsMaskEdit
          TabOrder = 1
          OnDblClick = mask_DATEEDblClick
        end
        inherited sBitBtn16: TsBitBtn
          Tag = 3
          TabOrder = 37
          OnClick = btn_CalClick
        end
        inherited mask_APPDATE: TsMaskEdit
          TabOrder = 0
          OnDblClick = mask_DATEEDblClick
        end
        inherited sBitBtn3: TsBitBtn
          Tag = 2
          TabOrder = 38
          OnClick = btn_CalClick
        end
        inherited mask_ARDATE: TsMaskEdit
          TabOrder = 2
          OnDblClick = mask_DATEEDblClick
        end
        inherited sBitBtn4: TsBitBtn
          Tag = 4
          TabOrder = 39
          OnClick = btn_CalClick
        end
        inherited edt_CARRIER1: TsEdit
          TabOrder = 8
        end
        inherited edt_CARRIER2: TsEdit
          TabOrder = 9
        end
        inherited sPanel9: TsPanel
          Left = 15
          Top = 311
          Width = 370
          TabOrder = 40
        end
        inherited edt_CRNAME1: TsEdit
          Left = 102
          Top = 333
          Width = 283
          TabOrder = 14
        end
        inherited edt_CRNAME2: TsEdit
          Left = 102
          Top = 355
          Width = 283
          TabOrder = 15
        end
        inherited edt_CRNAME3: TsEdit
          Left = 102
          Top = 377
          TabOrder = 16
        end
        inherited sBitBtn5: TsBitBtn
          Left = 170
          Top = 377
          TabOrder = 41
        end
        inherited edt_CRCODENAME3: TsEdit
          Left = 195
          Top = 377
          Width = 190
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 42
          SkinData.CustomColor = True
        end
        inherited edt_B5NAME1: TsEdit
          Left = 102
          Top = 399
          Width = 283
          TabOrder = 17
        end
        inherited edt_B5NAME2: TsEdit
          Left = 102
          Top = 421
          Width = 283
          TabOrder = 18
        end
        inherited edt_B5NAME3: TsEdit
          Left = 102
          Top = 443
          Width = 283
          TabOrder = 19
        end
        inherited sPanel11: TsPanel
          Left = 435
          Top = 2
          Width = 370
          TabOrder = 43
        end
        inherited edt_SENAME1: TsEdit
          Left = 520
          Top = 24
          TabOrder = 20
        end
        inherited edt_SENAME2: TsEdit
          Left = 520
          Top = 46
          TabOrder = 21
        end
        inherited edt_SENAME3: TsEdit
          Left = 520
          Top = 68
          TabOrder = 22
        end
        inherited edt_CNNAME1: TsEdit
          Left = 520
          Top = 90
          TabOrder = 23
        end
        inherited edt_CNNAME2: TsEdit
          Left = 520
          Top = 112
          TabOrder = 24
        end
        inherited edt_CNNAME3: TsEdit
          Left = 520
          Top = 134
          TabOrder = 25
        end
        inherited sPanel10: TsPanel
          Left = 435
          Top = 311
          Width = 370
          TabOrder = 44
        end
        inherited edt_LOADLOC: TsEdit
          Left = 516
          Top = 333
          TabOrder = 26
        end
        inherited sBitBtn7: TsBitBtn
          Left = 552
          Top = 333
          TabOrder = 45
        end
        inherited edt_LOADLOCNAME: TsEdit
          Left = 577
          Top = 333
          Width = 226
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 46
          SkinData.CustomColor = True
        end
        inherited edt_LOADTXT: TsEdit
          Left = 516
          Top = 355
          Width = 287
          TabOrder = 27
        end
        inherited edt_ARRLOC: TsEdit
          Left = 516
          Top = 377
          TabOrder = 28
        end
        inherited sBitBtn8: TsBitBtn
          Left = 552
          Top = 377
          TabOrder = 47
        end
        inherited edt_ARRLOCNAME: TsEdit
          Left = 577
          Top = 377
          Width = 226
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 48
          SkinData.CustomColor = True
        end
        inherited edt_ARRTXT: TsEdit
          Left = 516
          Top = 399
          Width = 287
          TabOrder = 29
        end
        inherited edt_INVAMTC: TsEdit
          Tag = 201
          TabOrder = 10
        end
        inherited sBitBtn9: TsBitBtn
          TabOrder = 49
        end
        inherited curr_INVAMT: TsCurrencyEdit
          TabOrder = 11
        end
        inherited edt_PACQTYC: TsEdit
          Tag = 202
          TabOrder = 12
        end
        inherited sBitBtn10: TsBitBtn
          TabOrder = 50
        end
        inherited curr_PACQTY: TsCurrencyEdit
          TabOrder = 13
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sSplitter5: TsSplitter
        Width = 1106
      end
      inherited sPanel17: TsPanel
        Width = 814
        DesignSize = (
          814
          559)
        inherited edt_SPMARK1: TsEdit [1]
          Left = 120
          Top = 24
          Width = 265
          Height = 21
          TabOrder = 0
        end
        inherited edt_SPMARK3: TsEdit [2]
          Left = 120
          Top = 68
          Width = 265
          Height = 21
          TabOrder = 2
        end
        inherited edt_SPMARK5: TsEdit [3]
          Left = 120
          Top = 112
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK2: TsEdit [4]
          Left = 120
          Top = 46
          Width = 265
          Height = 21
          TabOrder = 1
        end
        inherited sPanel12: TsPanel [5]
          Left = 15
          Width = 370
          TabOrder = 32
        end
        inherited edt_SPMARK10: TsEdit [6]
          Left = 120
          Top = 222
          Width = 265
          Height = 21
          TabOrder = 9
        end
        inherited edt_SPMARK8: TsEdit [7]
          Left = 120
          Top = 178
          Width = 265
          Height = 21
          TabOrder = 7
        end
        inherited edt_SPMARK9: TsEdit [8]
          Left = 120
          Top = 200
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK4: TsEdit [9]
          Left = 120
          Top = 90
          Width = 265
          Height = 21
          TabOrder = 3
        end
        inherited edt_SPMARK7: TsEdit [10]
          Left = 120
          Top = 156
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK6: TsEdit [11]
          Left = 120
          Top = 134
          Width = 265
          Height = 21
          TabOrder = 5
        end
        inherited sPanel13: TsPanel
          Left = 15
          Top = 257
          Width = 370
          TabOrder = 33
        end
        inherited edt_GOODS1: TsEdit
          Left = 120
          Top = 280
          Width = 265
          Height = 21
          TabOrder = 10
        end
        inherited edt_GOODS2: TsEdit
          Left = 120
          Top = 302
          Width = 265
          Height = 21
          TabOrder = 11
        end
        inherited edt_GOODS3: TsEdit
          Left = 120
          Top = 324
          Width = 265
          Height = 21
          TabOrder = 12
        end
        inherited edt_GOODS4: TsEdit
          Left = 120
          Top = 346
          Width = 265
          Height = 21
          TabOrder = 13
        end
        inherited edt_GOODS5: TsEdit
          Left = 120
          Top = 368
          Width = 265
          Height = 21
          TabOrder = 14
        end
        inherited sPanel14: TsPanel
          Left = 435
          Top = 2
          Width = 370
          TabOrder = 34
        end
        inherited edt_MSNAME1: TsEdit
          Left = 542
          Top = 24
          Width = 263
          Height = 21
          TabOrder = 20
        end
        inherited edt_MSNAME2: TsEdit
          Left = 542
          Top = 46
          Width = 263
          Height = 21
          TabOrder = 21
        end
        inherited edt_MSNAME3: TsEdit
          Left = 542
          Top = 68
          Width = 263
          Height = 21
          TabOrder = 22
        end
        inherited edt_AXNAME1: TsEdit
          Left = 542
          Top = 90
          Width = 263
          Height = 21
          TabOrder = 23
        end
        inherited edt_AXNAME2: TsEdit
          Left = 542
          Top = 112
          Width = 263
          Height = 21
          TabOrder = 24
        end
        inherited edt_AXNAME3: TsEdit
          Left = 542
          Top = 134
          Width = 263
          Height = 21
          TabOrder = 25
        end
        inherited sPanel5: TsPanel
          Left = 435
          Top = 263
          Width = 370
          TabOrder = 35
        end
        inherited edt_TRMPAYC: TsEdit
          Left = 542
          Top = 285
          Height = 21
          TabOrder = 26
        end
        inherited sBitBtn11: TsBitBtn
          Left = 578
          Top = 285
          TabOrder = 36
        end
        inherited edt_TRMPAYCNAME: TsEdit
          Left = 603
          Top = 285
          Width = 202
          Height = 21
        end
        inherited edt_TRMPAY: TsEdit
          Left = 542
          Top = 307
          Width = 263
          Height = 21
        end
        inherited edt_BANKCD: TsEdit
          Left = 542
          Top = 329
          Height = 21
        end
        inherited sBitBtn12: TsBitBtn
          Left = 578
          Top = 329
          TabOrder = 37
        end
        inherited edt_BANKTXT: TsEdit
          Left = 603
          Top = 329
          Width = 202
          Height = 21
          TabOrder = 30
        end
        inherited edt_BANKBR: TsEdit
          Left = 542
          Top = 351
          Width = 263
          Height = 21
          TabOrder = 31
        end
        inherited edt_GOODS6: TsEdit
          Left = 120
          Top = 390
          Width = 265
          Height = 21
          TabOrder = 15
        end
        inherited edt_GOODS7: TsEdit
          Left = 120
          Top = 412
          Width = 265
          Height = 21
          TabOrder = 16
        end
        inherited edt_GOODS8: TsEdit
          Left = 120
          Top = 434
          Width = 265
          Height = 21
          TabOrder = 17
        end
        inherited edt_GOODS9: TsEdit
          Left = 120
          Top = 456
          Width = 265
          Height = 21
          TabOrder = 18
        end
        inherited edt_GOODS10: TsEdit
          Left = 120
          Top = 478
          Width = 265
          Height = 21
          TabOrder = 19
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sPanel7: TsPanel
        Width = 1106
        inherited sSplitter2: TsSplitter
          Top = 1
          Width = 1104
          Height = 2
        end
        object sSplitter7: TsSplitter [1]
          Left = 1
          Top = 35
          Width = 1104
          Height = 2
          Cursor = crVSplit
          Align = alTop
          AutoSnap = False
          Enabled = False
          SkinData.SkinSection = 'SPLITTER'
        end
        inherited sPanel6: TsPanel
          Top = 3
          Width = 1104
          inherited com_SearchKeyword: TsComboBox
            Items.Strings = (
              #46321#47197#51068#51088
              #44288#47532#48264#54840
              'L/C'#48264#54840)
          end
          inherited Mask_SearchDate1: TsMaskEdit
            OnDblClick = mask_DATEEDblClick
          end
          inherited sBitBtn13: TsBitBtn
            OnClick = sBitBtn13Click
          end
          inherited sBitBtn14: TsBitBtn
            Tag = 5
            OnClick = btn_CalClick
          end
          inherited Mask_SearchDate2: TsMaskEdit
            OnDblClick = mask_DATEEDblClick
          end
          inherited sBitBtn28: TsBitBtn
            Tag = 6
            OnClick = btn_CalClick
          end
        end
        inherited sDBGrid1: TsDBGrid
          Top = 37
          Width = 1104
          Height = 523
          DataSource = dsList
          Font.Name = #47569#51008' '#44256#46357
          PopupMenu = popMenu1
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHK2'
              Title.Alignment = taCenter
              Title.Caption = #49345#54889
              Width = 40
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'CHK3'
              Title.Alignment = taCenter
              Title.Caption = #52376#47532
              Width = 80
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SE_NAME1'
              Title.Alignment = taCenter
              Title.Caption = #49569#54868#51064
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LC_NO'
              Title.Alignment = taCenter
              Title.Caption = #44228#50557#49436',L/C'#48264#54840
              Width = 180
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'USER_ID'
              Title.Alignment = taCenter
              Title.Caption = #49324#50857#51088
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'INV_AMT'
              Title.Alignment = taCenter
              Title.Caption = #49345#50629#49569#51109#44552#50529
              Width = 130
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'INV_AMTC'
              Title.Alignment = taCenter
              Title.Caption = #45800#50948
              Width = 80
              Visible = True
            end>
        end
      end
    end
  end
  inherited sPanel18: TsPanel
    Top = 121
    Height = 556
    inherited sDBGrid2: TsDBGrid
      Height = 521
      DataSource = dsList
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 190
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 32
    Top = 240
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 64
    Top = 208
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    AfterOpen = qryListAfterScroll
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      
        #9'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, MESSAGE3, CR_NAME' +
        '1, CR_NAME2, CR_NAME3, SE_NAME1, SE_NAME2, SE_NAME3, MS_NAME1, M' +
        'S_NAME2, MS_NAME3, AX_NAME1, AX_NAME2, AX_NAME3, INV_AMT, INV_AM' +
        'TC, LC_G, LC_NO, BL_G, BL_NO, CARRIER1, CARRIER2, AR_DATE, BL_DA' +
        'TE, APP_DATE, LOAD_LOC, LOAD_TXT, ARR_LOC, ARR_TXT, SPMARK1, SPM' +
        'ARK2, SPMARK3, SPMARK4, SPMARK5, SPMARK6, SPMARK7, SPMARK8, SPMA' +
        'RK9, SPMARK10, PAC_QTY, PAC_QTYC, GOODS1, GOODS2, GOODS3, GOODS4' +
        ', GOODS5, GOODS6, GOODS7, GOODS8, GOODS9, GOODS10, TRM_PAYC, TRM' +
        '_PAY, BANK_CD, BANK_TXT, BANK_BR, CHK1, CHK2, CHK3, PRNO, CN_NAM' +
        'E1, CN_NAME2, CN_NAME3, B5_NAME1, B5_NAME2, B5_NAME3'
      #9',msg1CODE.MSG1NAME'
      #9',LCGCODE.LCGNAME'
      #9',BLGCODE.BLGNAME'
      #9',LOADCODE.LOADNAME'
      #9',ARRCODE.ARRNAME'
      #9',TRMPAYCCODE.TRMPAYCNAME'
      #9',SUNSACODE.CRNAEM3NAME'
      'FROM APPLOG'
      
        'LEFT JOIN (SELECT CODE,NAME as MSG1NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'APPLOG'#44396#48516#39') msg1CODE ON MESSAGE1 = msg1CODE.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as LCGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39'LC'#44396#48516#39') LCGCODE ON LC_G = LCGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as BLGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#49440#54616#51613#44428#39') BLGCODE ON BL_G = BLGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as LOADNAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') LOADCODE ON LOAD_LOC = LOADCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as ARRNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#44397#44032#39') ARRCODE ON ARR_LOC = ARRCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as TRMPAYCNAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39#44208#51228#44592#44036#39') TRMPAYCCODE ON TRM_PAYC = TRMPAYCCO' +
        'DE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as CRNAEM3NAME FROM CODE2NDD with(no' +
        'lock) WHERE Prefix = '#39'SUNSA'#39') SUNSACODE ON CR_NAME3 = SUNSACODE.' +
        'CODE'
      '')
    Left = 32
    Top = 208
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListMESSAGE3: TStringField
      FieldName = 'MESSAGE3'
      Size = 3
    end
    object qryListCR_NAME1: TStringField
      FieldName = 'CR_NAME1'
      Size = 35
    end
    object qryListCR_NAME2: TStringField
      FieldName = 'CR_NAME2'
      Size = 35
    end
    object qryListCR_NAME3: TStringField
      FieldName = 'CR_NAME3'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListMS_NAME1: TStringField
      FieldName = 'MS_NAME1'
      Size = 35
    end
    object qryListMS_NAME2: TStringField
      FieldName = 'MS_NAME2'
      Size = 35
    end
    object qryListMS_NAME3: TStringField
      FieldName = 'MS_NAME3'
      Size = 35
    end
    object qryListAX_NAME1: TStringField
      FieldName = 'AX_NAME1'
      Size = 35
    end
    object qryListAX_NAME2: TStringField
      FieldName = 'AX_NAME2'
      Size = 35
    end
    object qryListAX_NAME3: TStringField
      FieldName = 'AX_NAME3'
      Size = 35
    end
    object qryListINV_AMT: TBCDField
      FieldName = 'INV_AMT'
      DisplayFormat = '#,###.####;0'
      Precision = 18
    end
    object qryListINV_AMTC: TStringField
      FieldName = 'INV_AMTC'
      Size = 3
    end
    object qryListLC_G: TStringField
      FieldName = 'LC_G'
      Size = 3
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListBL_G: TStringField
      FieldName = 'BL_G'
      Size = 3
    end
    object qryListBL_NO: TStringField
      FieldName = 'BL_NO'
      Size = 35
    end
    object qryListCARRIER1: TStringField
      FieldName = 'CARRIER1'
      Size = 17
    end
    object qryListCARRIER2: TStringField
      FieldName = 'CARRIER2'
      Size = 35
    end
    object qryListAR_DATE: TStringField
      FieldName = 'AR_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListBL_DATE: TStringField
      FieldName = 'BL_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListLOAD_LOC: TStringField
      FieldName = 'LOAD_LOC'
      Size = 3
    end
    object qryListLOAD_TXT: TStringField
      FieldName = 'LOAD_TXT'
      Size = 70
    end
    object qryListARR_LOC: TStringField
      FieldName = 'ARR_LOC'
      Size = 3
    end
    object qryListARR_TXT: TStringField
      FieldName = 'ARR_TXT'
      Size = 70
    end
    object qryListSPMARK1: TStringField
      FieldName = 'SPMARK1'
      Size = 35
    end
    object qryListSPMARK2: TStringField
      FieldName = 'SPMARK2'
      Size = 35
    end
    object qryListSPMARK3: TStringField
      FieldName = 'SPMARK3'
      Size = 35
    end
    object qryListSPMARK4: TStringField
      FieldName = 'SPMARK4'
      Size = 35
    end
    object qryListSPMARK5: TStringField
      FieldName = 'SPMARK5'
      Size = 35
    end
    object qryListSPMARK6: TStringField
      FieldName = 'SPMARK6'
      Size = 35
    end
    object qryListSPMARK7: TStringField
      FieldName = 'SPMARK7'
      Size = 35
    end
    object qryListSPMARK8: TStringField
      FieldName = 'SPMARK8'
      Size = 35
    end
    object qryListSPMARK9: TStringField
      FieldName = 'SPMARK9'
      Size = 35
    end
    object qryListSPMARK10: TStringField
      FieldName = 'SPMARK10'
      Size = 35
    end
    object qryListPAC_QTY: TBCDField
      FieldName = 'PAC_QTY'
      Precision = 18
    end
    object qryListPAC_QTYC: TStringField
      FieldName = 'PAC_QTYC'
      Size = 3
    end
    object qryListGOODS1: TStringField
      FieldName = 'GOODS1'
      Size = 70
    end
    object qryListGOODS2: TStringField
      FieldName = 'GOODS2'
      Size = 70
    end
    object qryListGOODS3: TStringField
      FieldName = 'GOODS3'
      Size = 70
    end
    object qryListGOODS4: TStringField
      FieldName = 'GOODS4'
      Size = 70
    end
    object qryListGOODS5: TStringField
      FieldName = 'GOODS5'
      Size = 70
    end
    object qryListGOODS6: TStringField
      FieldName = 'GOODS6'
      Size = 70
    end
    object qryListGOODS7: TStringField
      FieldName = 'GOODS7'
      Size = 70
    end
    object qryListGOODS8: TStringField
      FieldName = 'GOODS8'
      Size = 70
    end
    object qryListGOODS9: TStringField
      FieldName = 'GOODS9'
      Size = 70
    end
    object qryListGOODS10: TStringField
      FieldName = 'GOODS10'
      Size = 70
    end
    object qryListTRM_PAYC: TStringField
      FieldName = 'TRM_PAYC'
      Size = 3
    end
    object qryListTRM_PAY: TStringField
      FieldName = 'TRM_PAY'
      Size = 35
    end
    object qryListBANK_CD: TStringField
      FieldName = 'BANK_CD'
      Size = 4
    end
    object qryListBANK_TXT: TStringField
      FieldName = 'BANK_TXT'
      Size = 70
    end
    object qryListBANK_BR: TStringField
      FieldName = 'BANK_BR'
      Size = 70
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListCN_NAME1: TStringField
      FieldName = 'CN_NAME1'
      Size = 35
    end
    object qryListCN_NAME2: TStringField
      FieldName = 'CN_NAME2'
      Size = 35
    end
    object qryListCN_NAME3: TStringField
      FieldName = 'CN_NAME3'
      Size = 35
    end
    object qryListB5_NAME1: TStringField
      FieldName = 'B5_NAME1'
      Size = 35
    end
    object qryListB5_NAME2: TStringField
      FieldName = 'B5_NAME2'
      Size = 35
    end
    object qryListB5_NAME3: TStringField
      FieldName = 'B5_NAME3'
      Size = 35
    end
    object qryListMSG1NAME: TStringField
      FieldName = 'MSG1NAME'
      Size = 100
    end
    object qryListLCGNAME: TStringField
      FieldName = 'LCGNAME'
      Size = 100
    end
    object qryListBLGNAME: TStringField
      FieldName = 'BLGNAME'
      Size = 100
    end
    object qryListLOADNAME: TStringField
      FieldName = 'LOADNAME'
      Size = 100
    end
    object qryListARRNAME: TStringField
      FieldName = 'ARRNAME'
      Size = 100
    end
    object qryListTRMPAYCNAME: TStringField
      FieldName = 'TRMPAYCNAME'
      Size = 100
    end
    object qryListCRNAEM3NAME: TStringField
      FieldName = 'CRNAEM3NAME'
      Size = 100
    end
  end
  object spCopyAPPLOG: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'CopyAPPLOG;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@CopyDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@NewDocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 32
    Top = 272
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APPLOG'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 96
    Top = 208
  end
  object popMenu1: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    Left = 32
    Top = 304
    object N1: TMenuItem
      Caption = #51204#49569#51456#48708
      ImageIndex = 29
      OnClick = btnReadyClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = btnEditClick
    end
    object N4: TMenuItem
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = btnDelClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object d1: TMenuItem
      Caption = #52636#47141#54616#44592
      object N8: TMenuItem
        Caption = #48120#47532#48372#44592
        ImageIndex = 15
        OnClick = btnPrintClick
      end
      object N9: TMenuItem
        Tag = 1
        Caption = #54532#47536#53944
        ImageIndex = 21
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = 'ADMIN'
      Enabled = False
      ImageIndex = 16
    end
  end
end
