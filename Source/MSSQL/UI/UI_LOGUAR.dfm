inherited UI_LOGUAR_frm: TUI_LOGUAR_frm
  Left = 470
  Top = 148
  Caption = #49688#51077#54868#47932#49440#52712#48372#51613'('#51064#46020#49849#46973')'#49436
  ClientWidth = 1114
  Position = poOwnerFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  inherited sSplitter12: TsSplitter
    Width = 1114
  end
  inherited sSplitter1: TsSplitter
    Width = 1114
  end
  inherited sPanel1: TsPanel
    Width = 1114
    inherited sSpeedButton2: TsSpeedButton
      Left = 470
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 679
      Visible = False
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 754
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 518
      Visible = False
    end
    inherited sSpeedButton9: TsSpeedButton
      Left = 192
    end
    inherited sLabel59: TsLabel
      Width = 179
      Caption = #49688#51077#54868#47932#49440#52712#48372#51613'('#51064#46020#49849#46973')'#49436
    end
    inherited sLabel60: TsLabel
      Width = 46
      Caption = 'LOGUAR'
    end
    inherited sSpeedButton11: TsSpeedButton
      Left = 809
      Top = 11
      Visible = False
    end
    inherited btnExit: TsButton
      Left = 1038
    end
    inherited btnNew: TsButton
      Left = 506
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 509
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 200
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      Left = 515
      Visible = False
    end
    inherited btnPrint: TsButton
      Left = 687
      Visible = False
    end
    inherited btnTemp: TsButton
      Visible = False
    end
    inherited btnSave: TsButton
      Left = 545
      Visible = False
    end
    inherited btnCancel: TsButton
      Left = 604
      Visible = False
    end
    inherited btnReady: TsButton
      Left = 706
      Visible = False
    end
    inherited btnSend: TsButton
      Left = 751
      Visible = False
    end
    object sButton4: TsButton
      Left = 267
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 11
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
    object sButton2: TsButton
      Tag = 1
      Left = 366
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 12
      TabStop = False
      OnClick = sButton4Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
  end
  inherited sPanel4: TsPanel
    Width = 1114
    inherited btn_Cal: TsBitBtn [0]
      Left = 424
      Visible = False
    end
    inherited sBitBtn19: TsBitBtn [1]
      Left = 642
      Visible = False
    end
    inherited edt_MAINT_NO: TsEdit [2]
    end
    inherited edt_USER_ID: TsEdit [3]
      Left = 504
    end
    inherited mask_DATEE: TsMaskEdit [4]
    end
    inherited edt_msg2: TsEdit [5]
      Left = 633
    end
    inherited edt_msg3: TsEdit
      Left = 705
    end
    inherited sBitBtn20: TsBitBtn
      Left = 714
      Visible = False
    end
  end
  inherited sPageControl1: TsPageControl
    Width = 1114
    inherited sTabSheet1: TsTabSheet
      inherited sSplitter3: TsSplitter
        Width = 1106
      end
      inherited sPanel2: TsPanel
        Width = 814
        inherited edt_CRNAME3: TsEdit [1]
          Top = 455
          Width = 284
        end
        inherited edt_CRNAME2: TsEdit [2]
          Top = 433
        end
        inherited edt_CRNAME1: TsEdit [3]
          Top = 411
          BoundLabel.Font.Style = []
        end
        inherited edt_B5NAME1: TsEdit [4]
          Top = 477
        end
        inherited edt_CARRIER1: TsEdit [5]
          Width = 275
          BoundLabel.Font.Style = []
        end
        inherited mask_ARDATE: TsMaskEdit [6]
          BoundLabel.Font.Style = []
        end
        inherited mask_APPDATE: TsMaskEdit [7]
          BoundLabel.Font.Style = []
        end
        inherited mask_BLDATE: TsMaskEdit [8]
          BoundLabel.Font.Style = []
        end
        inherited sBitBtn6: TsBitBtn [9]
          Left = 170
          Visible = False
        end
        inherited sBitBtn16: TsBitBtn [10]
          Left = 163
          Visible = False
        end
        inherited sBitBtn1: TsBitBtn [11]
          Left = 178
          Visible = False
        end
        inherited sBitBtn2: TsBitBtn [12]
          Left = 178
          Visible = False
        end
        inherited sPanel8: TsPanel [13]
          Left = 15
          Width = 370
        end
        inherited sBitBtn9: TsBitBtn [14]
          Left = 122
          Visible = False
        end
        object mask_LCDATE: TsMaskEdit [15]
          Left = 110
          Top = 332
          Width = 76
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 53
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49888#50857#51109' '#48156#54665#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        inherited edt_LOADLOCNAME: TsEdit [16]
          Left = 554
          Top = 410
          Width = 256
          Visible = False
        end
        inherited sBitBtn10: TsBitBtn [17]
          Left = 154
          Visible = False
        end
        inherited edt_BLGNAME: TsEdit [18]
          Left = 146
          Width = 239
        end
        inherited edt_BLNO: TsEdit [19]
          Width = 275
          BoundLabel.Font.Style = []
        end
        inherited edt_LCGNAME: TsEdit [20]
          Left = 146
          Width = 239
        end
        inherited sBitBtn4: TsBitBtn [21]
          Left = 163
          Visible = False
        end
        inherited edt_CARRIER2: TsEdit [22]
          Width = 275
          BoundLabel.Font.Style = []
        end
        inherited edt_PACQTYC: TsEdit [23]
        end
        inherited edt_INVAMTC: TsEdit [24]
          BoundLabel.Font.Style = []
        end
        object edt_LGNO: TsEdit [25]
          Left = 110
          Top = 310
          Width = 275
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 52
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#52712#48372#51613#49436' '#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        inherited edt_LOADLOC: TsEdit [26]
          Left = 518
          Top = 410
        end
        inherited sPanel10: TsPanel [27]
          Top = 388
        end
        inherited edt_CNNAME3: TsEdit [28]
          Left = 518
          Top = 134
          Width = 286
        end
        inherited edt_CNNAME2: TsEdit [29]
          Left = 518
          Top = 112
          Width = 286
        end
        inherited edt_CNNAME1: TsEdit [30]
          Left = 518
          Top = 90
          Width = 286
        end
        inherited edt_SENAME3: TsEdit [31]
          Left = 518
          Top = 68
          Width = 286
        end
        inherited edt_SENAME2: TsEdit [32]
          Left = 518
          Top = 46
          Width = 286
        end
        inherited edt_SENAME1: TsEdit
          Left = 518
          Top = 24
          Width = 286
          BoundLabel.Font.Style = []
        end
        inherited sPanel11: TsPanel [34]
          Left = 435
          Top = 2
          Width = 370
        end
        inherited curr_INVAMT: TsCurrencyEdit [35]
          Left = 146
          Width = 239
        end
        inherited edt_B5NAME2: TsEdit [36]
          Top = 499
        end
        inherited curr_PACQTY: TsCurrencyEdit [37]
          Left = 146
          Width = 239
        end
        object mask_LGDATE: TsMaskEdit [38]
          Left = 110
          Top = 288
          Width = 76
          Height = 21
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 51
          Text = '20161122'
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#52712#48372#51613#49436' '#48156#44553#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_USERMAINTNO: TsEdit [39]
          Left = 110
          Top = 354
          Width = 275
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 54
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50629#52404#49888#52397#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        inherited edt_B5NAME3: TsEdit [40]
          Top = 521
        end
        inherited edt_BLG: TsEdit [41]
          BoundLabel.Font.Style = []
        end
        inherited sBitBtn3: TsBitBtn [42]
          Left = 163
          Visible = False
        end
        inherited edt_ARRLOCNAME: TsEdit [43]
          Left = 554
          Top = 454
          Width = 256
          Visible = False
        end
        inherited sBitBtn8: TsBitBtn [44]
          Left = 554
          Top = 455
          Visible = False
        end
        inherited sBitBtn7: TsBitBtn [45]
          Left = 554
          Top = 410
          Visible = False
        end
        inherited edt_CRCODENAME3: TsEdit [46]
          Top = 447
          Visible = False
        end
        inherited sBitBtn5: TsBitBtn [47]
          Top = 447
          Visible = False
        end
        inherited edt_ARRTXT: TsEdit [48]
          Left = 518
          Top = 476
          Width = 292
        end
        inherited edt_LOADTXT: TsEdit [49]
          Left = 518
          Top = 432
          Width = 292
        end
        inherited edt_msg1: TsEdit [50]
          BoundLabel.Font.Style = []
        end
        inherited edt_LCG: TsEdit [51]
          BoundLabel.Font.Style = []
        end
        inherited edt_LCNO: TsEdit [52]
          Width = 275
        end
        inherited edt_msg1Name: TsEdit [53]
          Left = 146
          Width = 239
        end
        inherited sPanel9: TsPanel [54]
          Top = 388
        end
        inherited edt_ARRLOC: TsEdit [55]
          Left = 518
          Top = 454
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sSplitter5: TsSplitter
        Width = 1106
      end
      inherited sPanel17: TsPanel
        Width = 814
        inherited sSpeedButton7: TsSpeedButton
          Height = 563
        end
        inherited sPanel12: TsPanel
          Left = 15
          Width = 370
        end
        inherited edt_SPMARK1: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK2: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK3: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK5: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK4: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK7: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK6: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK9: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK8: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_SPMARK10: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited sPanel13: TsPanel
          Left = 15
          Width = 370
        end
        inherited edt_GOODS1: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_GOODS2: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_GOODS3: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_GOODS4: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_GOODS5: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited sPanel14: TsPanel
          Left = 435
          Top = 2
          Width = 370
        end
        inherited edt_MSNAME1: TsEdit
          Left = 542
          Top = 24
          Width = 263
          Height = 21
        end
        inherited edt_MSNAME2: TsEdit
          Left = 542
          Top = 46
          Width = 263
          Height = 21
        end
        inherited edt_MSNAME3: TsEdit
          Left = 542
          Top = 68
          Width = 263
          Height = 21
        end
        inherited edt_AXNAME1: TsEdit
          Left = 542
          Top = 90
          Width = 263
          Height = 21
        end
        inherited edt_AXNAME2: TsEdit
          Left = 542
          Top = 112
          Width = 263
          Height = 21
        end
        inherited edt_AXNAME3: TsEdit
          Left = 542
          Top = 134
          Width = 263
          Height = 21
        end
        inherited sPanel5: TsPanel
          Left = 435
          Top = 249
          Width = 370
        end
        inherited edt_TRMPAYC: TsEdit
          Left = 542
          Top = 271
          Height = 21
        end
        inherited sBitBtn11: TsBitBtn
          Left = 578
          Top = 271
          Visible = False
        end
        inherited edt_TRMPAYCNAME: TsEdit
          Left = 578
          Top = 271
          Width = 227
          Height = 21
        end
        inherited edt_TRMPAY: TsEdit
          Left = 542
          Top = 293
          Width = 263
          Height = 21
        end
        inherited edt_BANKCD: TsEdit
          Left = 542
          Top = 315
          Height = 21
        end
        inherited sBitBtn12: TsBitBtn
          Left = 580
          Top = 315
          Visible = False
        end
        inherited edt_BANKTXT: TsEdit
          Left = 578
          Top = 315
          Width = 227
          Height = 21
        end
        inherited edt_BANKBR: TsEdit
          Left = 542
          Top = 337
          Width = 263
          Height = 21
        end
        inherited edt_GOODS6: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_GOODS7: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_GOODS8: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_GOODS9: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
        inherited edt_GOODS10: TsEdit
          Left = 120
          Width = 265
          Height = 21
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sPanel7: TsPanel
        Width = 1106
        inherited sSplitter2: TsSplitter
          Width = 1104
        end
        inherited sPanel6: TsPanel
          Width = 1104
          inherited edt_SearchText: TsEdit
            Left = 110
          end
          inherited com_SearchKeyword: TsComboBox
            Width = 105
            Items.Strings = (
              #46321#47197#51068#51088
              #44288#47532#48264#54840
              #44228#50557#49436',LC'#48264#54840)
          end
          inherited Mask_SearchDate1: TsMaskEdit
            Left = 110
            OnDblClick = Mask_SearchDate1DblClick
          end
          inherited sBitBtn13: TsBitBtn
            Left = 343
            OnClick = sBitBtn13Click
          end
          inherited sBitBtn14: TsBitBtn
            Tag = 0
            Left = 191
            OnClick = sBitBtn14Click
          end
          inherited Mask_SearchDate2: TsMaskEdit
            Left = 238
            OnDblClick = Mask_SearchDate1DblClick
          end
          inherited sBitBtn28: TsBitBtn
            Tag = 1
            Left = 319
            OnClick = sBitBtn14Click
          end
          inherited sPanel15: TsPanel
            Left = 214
          end
        end
        inherited sDBGrid1: TsDBGrid
          Width = 1104
          DataSource = dsList
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'DATEE'
              Title.Alignment = taCenter
              Title.Caption = #46321#47197#51068#51088
              Width = 85
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MAINT_NO'
              Title.Alignment = taCenter
              Title.Caption = #44288#47532#48264#54840
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SE_NAME1'
              Title.Alignment = taCenter
              Title.Caption = #49569#54868#51064
              Width = 190
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LC_NO'
              Title.Alignment = taCenter
              Title.Caption = #44228#50557#49436',L/C'#48264#54840
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BL_NO'
              Title.Alignment = taCenter
              Title.Caption = #49440#54616#51613#44428#48264#54840
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LG_NO'
              Title.Alignment = taCenter
              Title.Caption = #49440#52712#48372#51613#49436#48264#54840
              Width = 180
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'USER_ID'
              Title.Alignment = taCenter
              Title.Caption = #49688#49888#51088
              Width = 60
              Visible = True
            end>
        end
      end
    end
  end
  inherited sPanel18: TsPanel
    inherited sDBGrid2: TsDBGrid
      DataSource = dsList
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 190
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 208
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 56
    Top = 240
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    AfterOpen = qryListAfterScroll
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT'
      
        #9'MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, MESSAGE3, CR_NAME' +
        '1, CR_NAME2, CR_NAME3, SE_NAME1, SE_NAME2, SE_NAME3, MS_NAME1, M' +
        'S_NAME2, MS_NAME3, AX_NAME1, AX_NAME2, AX_NAME3, INV_AMT, INV_AM' +
        'TC, LG_NO, LC_G, LC_NO, BL_G, BL_NO, CARRIER1, CARRIER2, AR_DATE' +
        ', BL_DATE, LG_DATE, LOAD_LOC, LOAD_TXT, ARR_LOC, ARR_TXT, SPMARK' +
        '1, SPMARK2, SPMARK3, SPMARK4, SPMARK5, SPMARK6, SPMARK7, SPMARK8' +
        ', SPMARK9, SPMARK10, PAC_QTY, PAC_QTYC, GOODS1, GOODS2, GOODS3, ' +
        'GOODS4, GOODS5, GOODS6, GOODS7, GOODS8, GOODS9, GOODS10, TRM_PAY' +
        'C, TRM_PAY, BANK_CD, BANK_TXT, BANK_BR, CHK1, CHK2, CHK3, USERMA' +
        'INT_NO, PRNO, CN_NAME1, CN_NAME2, CN_NAME3, B5_NAME1, B5_NAME2, ' +
        'B5_NAME3, LC_DATE'
      #9',msg1CODE.MSG1NAME'
      #9',LCGCODE.LCGNAME'
      #9',BLGCODE.BLGNAME'
      ''
      'FROM LOGUAR'
      
        'LEFT JOIN (SELECT CODE,NAME as MSG1NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'APPLOG'#44396#48516#39') msg1CODE ON MESSAGE1 = msg1CODE.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as LCGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39'LC'#44396#48516#39') LCGCODE ON LC_G = LCGCODE.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as BLGNAME FROM CODE2NDD with(nolock' +
        ') WHERE Prefix = '#39#49440#54616#51613#44428#39') BLGCODE ON BL_G = BLGCODE.CODE')
    Left = 24
    Top = 240
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListMESSAGE3: TStringField
      FieldName = 'MESSAGE3'
      Size = 3
    end
    object qryListCR_NAME1: TStringField
      FieldName = 'CR_NAME1'
      Size = 35
    end
    object qryListCR_NAME2: TStringField
      FieldName = 'CR_NAME2'
      Size = 35
    end
    object qryListCR_NAME3: TStringField
      FieldName = 'CR_NAME3'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListMS_NAME1: TStringField
      FieldName = 'MS_NAME1'
      Size = 35
    end
    object qryListMS_NAME2: TStringField
      FieldName = 'MS_NAME2'
      Size = 35
    end
    object qryListMS_NAME3: TStringField
      FieldName = 'MS_NAME3'
      Size = 35
    end
    object qryListAX_NAME1: TStringField
      FieldName = 'AX_NAME1'
      Size = 35
    end
    object qryListAX_NAME2: TStringField
      FieldName = 'AX_NAME2'
      Size = 35
    end
    object qryListAX_NAME3: TStringField
      FieldName = 'AX_NAME3'
      Size = 35
    end
    object qryListINV_AMT: TBCDField
      FieldName = 'INV_AMT'
      Precision = 18
    end
    object qryListINV_AMTC: TStringField
      FieldName = 'INV_AMTC'
      Size = 3
    end
    object qryListLG_NO: TStringField
      FieldName = 'LG_NO'
      Size = 10
    end
    object qryListLC_G: TStringField
      FieldName = 'LC_G'
      Size = 3
    end
    object qryListLC_NO: TStringField
      FieldName = 'LC_NO'
      Size = 35
    end
    object qryListBL_G: TStringField
      FieldName = 'BL_G'
      Size = 3
    end
    object qryListBL_NO: TStringField
      FieldName = 'BL_NO'
      Size = 35
    end
    object qryListCARRIER1: TStringField
      FieldName = 'CARRIER1'
      Size = 17
    end
    object qryListCARRIER2: TStringField
      FieldName = 'CARRIER2'
      Size = 35
    end
    object qryListAR_DATE: TStringField
      FieldName = 'AR_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListBL_DATE: TStringField
      FieldName = 'BL_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListLG_DATE: TStringField
      FieldName = 'LG_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListLOAD_LOC: TStringField
      FieldName = 'LOAD_LOC'
      Size = 3
    end
    object qryListLOAD_TXT: TStringField
      FieldName = 'LOAD_TXT'
      Size = 70
    end
    object qryListARR_LOC: TStringField
      FieldName = 'ARR_LOC'
      Size = 3
    end
    object qryListARR_TXT: TStringField
      FieldName = 'ARR_TXT'
      Size = 70
    end
    object qryListSPMARK1: TStringField
      FieldName = 'SPMARK1'
      Size = 35
    end
    object qryListSPMARK2: TStringField
      FieldName = 'SPMARK2'
      Size = 35
    end
    object qryListSPMARK3: TStringField
      FieldName = 'SPMARK3'
      Size = 35
    end
    object qryListSPMARK4: TStringField
      FieldName = 'SPMARK4'
      Size = 35
    end
    object qryListSPMARK5: TStringField
      FieldName = 'SPMARK5'
      Size = 35
    end
    object qryListSPMARK6: TStringField
      FieldName = 'SPMARK6'
      Size = 35
    end
    object qryListSPMARK7: TStringField
      FieldName = 'SPMARK7'
      Size = 35
    end
    object qryListSPMARK8: TStringField
      FieldName = 'SPMARK8'
      Size = 35
    end
    object qryListSPMARK9: TStringField
      FieldName = 'SPMARK9'
      Size = 35
    end
    object qryListSPMARK10: TStringField
      FieldName = 'SPMARK10'
      Size = 35
    end
    object qryListPAC_QTY: TBCDField
      FieldName = 'PAC_QTY'
      Precision = 18
    end
    object qryListPAC_QTYC: TStringField
      FieldName = 'PAC_QTYC'
      Size = 3
    end
    object qryListGOODS1: TStringField
      FieldName = 'GOODS1'
      Size = 70
    end
    object qryListGOODS2: TStringField
      FieldName = 'GOODS2'
      Size = 70
    end
    object qryListGOODS3: TStringField
      FieldName = 'GOODS3'
      Size = 70
    end
    object qryListGOODS4: TStringField
      FieldName = 'GOODS4'
      Size = 70
    end
    object qryListGOODS5: TStringField
      FieldName = 'GOODS5'
      Size = 70
    end
    object qryListGOODS6: TStringField
      FieldName = 'GOODS6'
      Size = 70
    end
    object qryListGOODS7: TStringField
      FieldName = 'GOODS7'
      Size = 70
    end
    object qryListGOODS8: TStringField
      FieldName = 'GOODS8'
      Size = 70
    end
    object qryListGOODS9: TStringField
      FieldName = 'GOODS9'
      Size = 70
    end
    object qryListGOODS10: TStringField
      FieldName = 'GOODS10'
      Size = 70
    end
    object qryListTRM_PAYC: TStringField
      FieldName = 'TRM_PAYC'
      Size = 3
    end
    object qryListTRM_PAY: TStringField
      FieldName = 'TRM_PAY'
      Size = 35
    end
    object qryListBANK_CD: TStringField
      FieldName = 'BANK_CD'
      Size = 4
    end
    object qryListBANK_TXT: TStringField
      FieldName = 'BANK_TXT'
      Size = 70
    end
    object qryListBANK_BR: TStringField
      FieldName = 'BANK_BR'
      Size = 70
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListUSERMAINT_NO: TStringField
      FieldName = 'USERMAINT_NO'
      Size = 35
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListCN_NAME1: TStringField
      FieldName = 'CN_NAME1'
      Size = 35
    end
    object qryListCN_NAME2: TStringField
      FieldName = 'CN_NAME2'
      Size = 35
    end
    object qryListCN_NAME3: TStringField
      FieldName = 'CN_NAME3'
      Size = 35
    end
    object qryListB5_NAME1: TStringField
      FieldName = 'B5_NAME1'
      Size = 35
    end
    object qryListB5_NAME2: TStringField
      FieldName = 'B5_NAME2'
      Size = 35
    end
    object qryListB5_NAME3: TStringField
      FieldName = 'B5_NAME3'
      Size = 35
    end
    object qryListLC_DATE: TStringField
      FieldName = 'LC_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMSG1NAME: TStringField
      FieldName = 'MSG1NAME'
      Size = 100
    end
    object qryListLCGNAME: TStringField
      FieldName = 'LCGNAME'
      Size = 100
    end
    object qryListBLGNAME: TStringField
      FieldName = 'BLGNAME'
      Size = 100
    end
  end
end
