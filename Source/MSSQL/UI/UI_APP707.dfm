inherited UI_APP707_frm: TUI_APP707_frm
  Left = 2176
  Top = 54
  Caption = 'UI_APP707_frm'
  ClientWidth = 1114
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  inherited sSplitter1: TsSplitter
    Width = 1114
  end
  inherited sSplitter3: TsSplitter
    Width = 1114
  end
  inherited sPageControl1: TsPageControl [2]
    Width = 1114
    ActivePage = sTabSheet3
    TabIndex = 2
    OnChange = sPageControl1Change
    inherited sTabSheet1: TsTabSheet
      inherited sSplitter4: TsSplitter
        Width = 1106
      end
      inherited page1_RightPanel: TsPanel
        Width = 806
        inherited sSpeedButton6: TsSpeedButton
          Left = 398
          Top = 0
          Height = 561
        end
        inherited sPanel5: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          TabOrder = 44
        end
        inherited edt_APPDATE: TsMaskEdit
          Left = 106
          Top = 26
          Width = 72
          TabOrder = 0
        end
        inherited edt_In_Mathod: TsEdit
          Left = 106
          Top = 48
          MaxLength = 3
          TabOrder = 1
          OnChange = edt_In_MathodChange
        end
        inherited edt_ApBank: TsEdit
          Left = 106
          Top = 70
          MaxLength = 11
          TabOrder = 2
        end
        inherited edt_In_Mathod1: TsEdit
          Left = 179
          Top = 48
          Width = 206
          TabOrder = 45
        end
        inherited edt_ApBank1: TsEdit
          Left = 179
          Top = 70
          Width = 206
          MaxLength = 35
          TabOrder = 3
        end
        inherited edt_ApBank2: TsEdit
          Left = 179
          Top = 92
          Width = 206
          MaxLength = 35
          TabOrder = 4
        end
        inherited edt_ApBank3: TsEdit
          Left = 179
          Top = 114
          Width = 206
          MaxLength = 35
          TabOrder = 5
        end
        inherited edt_ApBank4: TsEdit
          Left = 179
          Top = 136
          Width = 206
          MaxLength = 35
          TabOrder = 6
        end
        inherited edt_ApBank5: TsEdit
          Left = 179
          Top = 158
          Width = 206
          MaxLength = 35
          TabOrder = 7
        end
        inherited sPanel2: TsPanel
          Left = 15
          Top = 207
          Width = 370
          Height = 23
        end
        inherited edt_ImpCd1_1: TsEdit
          Left = 179
          Top = 231
          Width = 206
        end
        inherited edt_ImpCd1: TsEdit
          Left = 106
          Top = 231
          MaxLength = 3
          TabOrder = 8
          OnChange = edt_In_MathodChange
          OnDblClick = edt_CodeTypeDblClick
        end
        inherited edt_ImpCd2: TsEdit
          Left = 106
          Top = 253
          MaxLength = 3
          TabOrder = 9
          OnChange = edt_In_MathodChange
          OnDblClick = edt_CodeTypeDblClick
        end
        inherited btn_ImpCd1: TsBitBtn
          Left = 156
          Top = 231
        end
        inherited btn_ImpCd2: TsBitBtn
          Left = 156
          Top = 253
        end
        inherited edt_ImpCd2_1: TsEdit
          Left = 179
          Top = 253
          Width = 206
        end
        inherited edt_ImpCd3_1: TsEdit
          Left = 179
          Top = 275
          Width = 206
        end
        inherited btn_ImpCd3: TsBitBtn
          Left = 156
          Top = 275
        end
        inherited edt_ImpCd3: TsEdit
          Left = 106
          Top = 275
          MaxLength = 3
          TabOrder = 10
          OnChange = edt_In_MathodChange
          OnDblClick = edt_CodeTypeDblClick
        end
        inherited edt_ImpCd4: TsEdit
          Left = 106
          Top = 297
          MaxLength = 3
          TabOrder = 11
          OnChange = edt_In_MathodChange
          OnDblClick = edt_CodeTypeDblClick
        end
        inherited btn_ImpCd4: TsBitBtn
          Left = 156
          Top = 297
        end
        inherited edt_ImpCd4_1: TsEdit
          Left = 179
          Top = 297
          Width = 206
        end
        inherited edt_ImpCd5_1: TsEdit
          Left = 179
          Top = 319
          Width = 206
        end
        inherited btn_ImpCd5: TsBitBtn
          Left = 156
          Top = 319
        end
        inherited edt_ImpCd5: TsEdit
          Left = 106
          Top = 319
          MaxLength = 3
          TabOrder = 12
          OnChange = edt_In_MathodChange
          OnDblClick = edt_CodeTypeDblClick
        end
        inherited sPanel8: TsPanel
          Left = 15
          Top = 384
          Width = 370
          Height = 23
        end
        inherited edt_AdInfo1: TsEdit
          Left = 106
          Top = 408
          Width = 279
          MaxLength = 70
          TabOrder = 13
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44592#53440#51221#48372
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited edt_AdInfo2: TsEdit
          Left = 106
          Top = 430
          Width = 279
          MaxLength = 70
          TabOrder = 14
        end
        inherited edt_AdInfo3: TsEdit
          Left = 106
          Top = 452
          Width = 279
          MaxLength = 70
          TabOrder = 15
        end
        inherited edt_AdInfo4: TsEdit
          Left = 106
          Top = 474
          Width = 279
          MaxLength = 70
          TabOrder = 16
        end
        inherited edt_AdInfo5: TsEdit
          Left = 106
          Top = 496
          Width = 279
          MaxLength = 70
          TabOrder = 17
        end
        inherited edt_EXAddr2: TsEdit
          Left = 552
          Top = 496
          Width = 239
          MaxLength = 35
          TabOrder = 43
        end
        inherited edt_EXAddr1: TsEdit
          Left = 552
          Top = 474
          Width = 239
          MaxLength = 35
          TabOrder = 42
        end
        inherited edt_EXName3: TsEdit
          Left = 552
          Top = 452
          MaxLength = 35
          TabOrder = 41
        end
        inherited edt_EXName2: TsEdit
          Left = 552
          Top = 430
          Width = 239
          MaxLength = 35
          TabOrder = 40
        end
        inherited edt_EXName1: TsEdit
          Left = 552
          Top = 408
          Width = 239
          MaxLength = 35
          TabOrder = 39
        end
        inherited sPanel9: TsPanel
          Left = 421
          Top = 384
          Width = 370
          Height = 23
        end
        inherited edt_ILno5: TsEdit
          Left = 471
          Top = 319
          MaxLength = 35
          TabOrder = 36
        end
        inherited btn_LCNumber5: TsBitBtn
          Left = 683
          Top = 319
        end
        inherited edt_ILAMT5: TsCurrencyEdit
          Left = 706
          Top = 319
          TabOrder = 38
        end
        inherited edt_ILAMT4: TsCurrencyEdit
          Left = 706
          Top = 297
          TabOrder = 35
        end
        inherited btn_LCNumber4: TsBitBtn
          Left = 683
          Top = 297
        end
        inherited edt_ILno4: TsEdit
          Left = 471
          Top = 297
          MaxLength = 35
          TabOrder = 33
        end
        inherited edt_ILno3: TsEdit
          Left = 471
          Top = 275
          MaxLength = 35
          TabOrder = 30
        end
        inherited btn_LCNumber3: TsBitBtn
          Left = 683
          Top = 275
        end
        inherited edt_ILAMT3: TsCurrencyEdit
          Left = 706
          Top = 275
          TabOrder = 32
        end
        inherited edt_ILAMT2: TsCurrencyEdit
          Left = 706
          Top = 253
          TabOrder = 29
        end
        inherited btn_LCNumber2: TsBitBtn
          Left = 683
          Top = 253
        end
        inherited edt_ILno2: TsEdit
          Left = 471
          Top = 253
          MaxLength = 35
          TabOrder = 27
        end
        inherited edt_ILno1: TsEdit
          Left = 471
          Top = 231
          MaxLength = 35
          TabOrder = 24
        end
        inherited btn_LCNumber1: TsBitBtn
          Left = 683
          Top = 231
        end
        inherited edt_ILAMT1: TsCurrencyEdit
          Left = 706
          Top = 231
          TabOrder = 26
        end
        inherited sPanel7: TsPanel
          Left = 421
          Top = 207
          Width = 370
          Height = 23
        end
        inherited edt_AdPay: TsEdit
          Left = 512
          Top = 114
          MaxLength = 3
          TabOrder = 23
          OnChange = edt_In_MathodChange
        end
        inherited btn_GrantingCredit: TsBitBtn
          Left = 562
          Top = 114
        end
        inherited edt_AdPay1: TsEdit
          Left = 585
          Top = 114
          Width = 206
        end
        inherited edt_AdBank4: TsEdit
          Left = 585
          Top = 92
          Width = 206
          MaxLength = 35
          TabOrder = 22
        end
        inherited edt_AdBank3: TsEdit
          Left = 585
          Top = 70
          Width = 206
          MaxLength = 35
          TabOrder = 21
        end
        inherited edt_AdBank2: TsEdit
          Left = 585
          Top = 48
          Width = 206
          MaxLength = 35
          TabOrder = 20
        end
        inherited edt_AdBank1: TsEdit
          Left = 585
          Top = 26
          Width = 206
          MaxLength = 35
          TabOrder = 19
        end
        inherited edt_AdBank: TsEdit
          Left = 512
          Top = 26
          MaxLength = 11
          TabOrder = 18
        end
        inherited sPanel6: TsPanel
          Left = 421
          Top = 2
          Width = 370
          Height = 23
        end
        inherited btn_OpenMethod: TsBitBtn
          Left = 156
          Top = 48
        end
        inherited btn_ApBank: TsBitBtn
          Left = 156
          Top = 70
        end
        inherited btn_NoticeBank: TsBitBtn
          Left = 562
          Top = 26
        end
        inherited edt_ILCur1: TsEdit
          Left = 647
          Top = 231
          MaxLength = 3
          TabOrder = 25
        end
        inherited edt_ILCur2: TsEdit
          Left = 647
          Top = 253
          MaxLength = 3
          TabOrder = 28
        end
        inherited edt_ILCur3: TsEdit
          Left = 647
          Top = 275
          MaxLength = 3
          TabOrder = 31
        end
        inherited edt_ILCur4: TsEdit
          Left = 647
          Top = 297
          MaxLength = 3
          TabOrder = 34
        end
        inherited edt_ILCur5: TsEdit
          Left = 647
          Top = 319
          MaxLength = 3
          TabOrder = 37
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sSplitter5: TsSplitter
        Width = 1106
      end
      inherited page2_RightPanel: TsPanel
        Width = 806
        inherited sLabel1: TsLabel
          Left = 600
          Top = 432
        end
        object sSpeedButton1: TsSpeedButton [1]
          Left = 398
          Top = -2
          Width = 10
          Height = 561
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        inherited btn_Beneficiary: TsBitBtn
          Left = 591
          Top = 94
        end
        inherited edt_Applic5: TsEdit
          Left = 127
          Top = 318
          Width = 258
          MaxLength = 35
          TabOrder = 7
        end
        inherited edt_Applic4: TsEdit
          Left = 127
          Top = 296
          Width = 258
          MaxLength = 35
        end
        inherited edt_Applic3: TsEdit
          Left = 127
          Top = 274
          Width = 258
          MaxLength = 35
        end
        inherited edt_Applic2: TsEdit
          Left = 127
          Top = 252
          Width = 258
          MaxLength = 35
        end
        inherited edt_Applic1: TsEdit
          Left = 127
          Top = 230
          Width = 258
          MaxLength = 35
        end
        inherited sPanel17: TsPanel
          Left = 15
          Top = 206
          Width = 370
          Height = 23
          TabOrder = 26
        end
        inherited sPanel19: TsPanel
          Left = 15
          Top = 366
          Width = 370
          Height = 23
        end
        inherited edt_Benefc: TsEdit
          Left = 127
          Top = 390
          MaxLength = 10
          TabOrder = 8
        end
        inherited edt_Benefc2: TsEdit
          Left = 127
          Top = 434
          Width = 258
          MaxLength = 35
          TabOrder = 10
        end
        inherited edt_Benefc3: TsEdit
          Left = 127
          Top = 456
          Width = 258
          MaxLength = 35
          TabOrder = 11
        end
        inherited edt_Benefc4: TsEdit
          Left = 127
          Top = 478
          Width = 258
          MaxLength = 35
          TabOrder = 12
        end
        inherited edt_Benefc5: TsEdit
          Left = 127
          Top = 500
          Width = 258
          MaxLength = 35
          TabOrder = 13
        end
        inherited sPanel1: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
        end
        inherited edt_cdNo: TsEdit
          Left = 127
          Top = 26
          Width = 258
          MaxLength = 35
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#49888#51008#54665' '#52280#51312#49324#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited sPanel3: TsPanel
          Left = 15
          Top = 70
          Width = 370
          Height = 23
        end
        inherited edt_IssDate: TsMaskEdit
          Left = 127
          Top = 94
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44060#49444#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited sPanel4: TsPanel
          Left = 15
          Top = 138
          Width = 370
          Height = 23
        end
        inherited edt_AmdNo: TsCurrencyEdit
          Left = 127
          Top = 162
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51312#44148#48320#44221#54943#49688
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited sPanel11: TsPanel
          Left = 421
          Top = 2
          Width = 370
          Height = 23
        end
        inherited edt_exDate: TsMaskEdit
          Left = 534
          Top = 26
          TabOrder = 14
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #50976#54952#44592#51068
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited sPanel12: TsPanel
          Left = 421
          Top = 70
          Width = 370
          Height = 23
        end
        inherited edt_Benefc1: TsEdit
          Left = 127
          Top = 412
          Width = 258
          MaxLength = 35
          TabOrder = 9
        end
        inherited edt_IncdCur: TsEdit
          Left = 534
          Top = 94
          MaxLength = 3
          TabOrder = 15
        end
        inherited sPanel13: TsPanel
          Left = 421
          Top = 138
          Width = 370
          Height = 23
        end
        inherited edt_DecdCur: TsEdit
          Left = 534
          Top = 162
          MaxLength = 3
          TabOrder = 17
        end
        inherited sBitBtn2: TsBitBtn
          Left = 591
          Top = 162
        end
        inherited sPanel14: TsPanel
          Left = 421
          Top = 206
          Width = 370
          Height = 23
        end
        inherited sPanel15: TsPanel
          Left = 421
          Top = 274
          Width = 370
          Height = 23
        end
        inherited sPanel16: TsPanel
          Left = 421
          Top = 342
          Width = 370
          Height = 23
        end
        inherited sPanel18: TsPanel
          Left = 421
          Top = 410
          Width = 370
          Height = 23
        end
        inherited edt_NwcdCur: TsEdit
          Left = 534
          Top = 230
          MaxLength = 3
          TabOrder = 19
        end
        inherited sBitBtn3: TsBitBtn
          Left = 591
          Top = 230
        end
        inherited edt_BfcdCur: TsEdit
          Left = 534
          Top = 298
          MaxLength = 3
          TabOrder = 21
        end
        inherited sBitBtn4: TsBitBtn
          Left = 591
          Top = 298
        end
        inherited edt_CdMax: TsEdit
          Tag = 120
          Left = 534
          Top = 366
          Hint = 'CD_MAX'
          MaxLength = 3
          TabOrder = 23
          OnChange = edt_In_MathodChange
        end
        inherited sBitBtn5: TsBitBtn
          Tag = 120
          Left = 591
          Top = 366
        end
        inherited edt_IncdAmt: TsCurrencyEdit
          Left = 611
          Top = 94
          Width = 180
          TabOrder = 16
        end
        inherited edt_DecdAmt: TsCurrencyEdit
          Left = 611
          Top = 162
          Width = 180
          TabOrder = 18
        end
        inherited edt_NwcdAmt: TsCurrencyEdit
          Left = 611
          Top = 230
          Width = 180
          TabOrder = 20
        end
        inherited edt_BfcdAmt: TsCurrencyEdit
          Left = 611
          Top = 298
          Width = 180
          TabOrder = 22
        end
        inherited edt_CdMax1: TsEdit
          Left = 611
          Top = 366
          Width = 180
        end
        inherited edt_CdPerp: TsCurrencyEdit
          Left = 534
          Top = 434
          TabOrder = 24
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #44284#48512#51313#54728#50857#50984
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
        end
        inherited edt_CdPerm: TsCurrencyEdit
          Left = 619
          Top = 434
          TabOrder = 25
        end
        inherited sBitBtn6: TsBitBtn
          Left = 173
          Top = 390
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sSplitter6: TsSplitter
        Width = 1106
      end
      inherited page3_LeftPanel: TsPanel
        Height = 559
      end
      inherited page3_RightPanel: TsPanel
        Width = 806
        Height = 559
        object sLabel24: TsLabel
          Left = 15
          Top = 303
          Width = 279
          Height = 17
          Caption = #49688#53441'('#48156#49569')'#51648' : '#50868#49569#49688#45800#51060' DQ '#51068#46412#47564' '#51077#47141#44032#45733
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object sLabel27: TsLabel
          Left = 15
          Top = 392
          Width = 269
          Height = 17
          Caption = #52572#51333#47785#51201#51648' : '#50868#49569#49688#45800#51060' DQ '#51068#46412#47564' '#51077#47141#44032#45733
          ParentFont = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = [fsBold]
        end
        object sLabel2: TsLabel
          Left = 445
          Top = 540
          Width = 4
          Height = 15
        end
        object sSpeedButton7: TsSpeedButton
          Left = 398
          Top = -2
          Width = 10
          Height = 561
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object sPanel32: TsPanel
          Left = 421
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Latest Date of Shipment'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 17
        end
        object edt_lstDate: TsMaskEdit
          Left = 534
          Top = 26
          Width = 77
          Height = 21
          HelpContext = 1
          CharCase = ecUpperCase
          Ctl3D = False
          EditMask = '9999-99-99;0'
          MaxLength = 10
          ParentCtl3D = False
          TabOrder = 5
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52572#51333#49440#51201#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel33: TsPanel
          Left = 421
          Top = 94
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Shipment Period'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 27
        end
        object edt_shipPD1: TsEdit
          Left = 421
          Top = 118
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 70
          ParentCtl3D = False
          TabOrder = 6
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD2: TsEdit
          Left = 421
          Top = 140
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 70
          ParentCtl3D = False
          TabOrder = 7
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD3: TsEdit
          Left = 421
          Top = 162
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 70
          ParentCtl3D = False
          TabOrder = 8
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD4: TsEdit
          Left = 421
          Top = 184
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 70
          ParentCtl3D = False
          TabOrder = 9
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD5: TsEdit
          Left = 421
          Top = 206
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 70
          ParentCtl3D = False
          TabOrder = 10
          SkinData.SkinSection = 'EDIT'
        end
        object edt_shipPD6: TsEdit
          Left = 421
          Top = 228
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 70
          ParentCtl3D = False
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel36: TsPanel
          Left = 15
          Top = 2
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = #50868#49569#49688#45800
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 18
        end
        object edt_Carriage: TsEdit
          Tag = 124
          Left = 87
          Top = 26
          Width = 43
          Height = 21
          Hint = 'C_METHOD'
          HelpContext = 1
          CharCase = ecUpperCase
          Color = 5632767
          Ctl3D = False
          MaxLength = 3
          ParentCtl3D = False
          TabOrder = 0
          OnChange = edt_In_MathodChange
          OnDblClick = edt_CarriageDblClick
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
          BoundLabel.Caption = #50868#49569#49688#45800
        end
        object sBitBtn27: TsBitBtn
          Tag = 124
          Left = 131
          Top = 26
          Width = 22
          Height = 21
          Cursor = crHandPoint
          TabOrder = 19
          TabStop = False
          OnClick = sBitBtn27Click
          ImageIndex = 25
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object edt_Carriage1: TsEdit
          Left = 154
          Top = 26
          Width = 231
          Height = 21
          TabStop = False
          CharCase = ecUpperCase
          Color = clBtnFace
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel10: TsPanel
          Left = 418
          Top = 410
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Narrative'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 21
          object sLabel46: TsLabel
            Left = 220
            Top = 3
            Width = 147
            Height = 17
            Caption = '['#52572#45824' 35'#54665' 50'#50676' '#51077#47141' '#21487']'
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
          end
        end
        object memo_Narrat1: TsMemo
          Left = 443
          Top = 434
          Width = 328
          Height = 104
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #44404#47548
          Font.Style = []
          MaxLength = 1750
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 16
          OnChange = memo_Narrat1Change
          OnKeyPress = memo_Narrat1KeyPress
          CharCase = ecUpperCase
          BoundLabel.MaxWidth = 5
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel37: TsPanel
          Left = 15
          Top = 94
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Port of Loading/Airport of Departure'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 22
          object sLabel26: TsLabel
            Left = 1
            Top = 3
            Width = 39
            Height = 17
            Caption = #49440#51201#54637
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
          end
        end
        object edt_SunjukPort: TsEdit
          Left = 15
          Top = 118
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 65
          ParentCtl3D = False
          TabOrder = 1
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel38: TsPanel
          Left = 15
          Top = 186
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Port of Discharge/Airport of Destination'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 23
          object sLabel25: TsLabel
            Left = 1
            Top = 3
            Width = 39
            Height = 17
            Caption = #46020#52265#54637
            ParentFont = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
          end
        end
        object edt_dochackPort: TsEdit
          Left = 15
          Top = 210
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 65
          ParentCtl3D = False
          TabOrder = 2
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel39: TsPanel
          Left = 15
          Top = 278
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Place of Taking in Charge/Dispatch from '#183#183#183'/Place of Receipt'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 24
        end
        object edt_loadOn: TsEdit
          Left = 15
          Top = 321
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 65
          ParentCtl3D = False
          TabOrder = 3
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel40: TsPanel
          Left = 15
          Top = 367
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 
            'Place of Final Destination/For Transportation to '#183#183#183'/Place of De' +
            'livery'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 25
        end
        object sPanel20: TsPanel
          Left = 421
          Top = 278
          Width = 370
          Height = 23
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'DRAGBAR'
          Caption = 'Additional Amounts Covered'
          Color = 16042877
          
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 26
        end
        object edt_Aacv1: TsEdit
          Left = 421
          Top = 302
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          TabOrder = 12
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Aacv2: TsEdit
          Left = 421
          Top = 324
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          TabOrder = 13
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Aacv3: TsEdit
          Left = 421
          Top = 346
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          TabOrder = 14
          SkinData.SkinSection = 'EDIT'
        end
        object edt_Aacv4: TsEdit
          Left = 421
          Top = 368
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 35
          ParentCtl3D = False
          TabOrder = 15
          SkinData.SkinSection = 'EDIT'
        end
        object edt_forTran: TsEdit
          Left = 15
          Top = 410
          Width = 370
          Height = 21
          CharCase = ecUpperCase
          Ctl3D = False
          MaxLength = 65
          ParentCtl3D = False
          TabOrder = 4
          SkinData.SkinSection = 'EDIT'
        end
      end
    end
    inherited sTabSheet7: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      inherited sSplitter2: TsSplitter
        Width = 1106
      end
      inherited dataSearch_Panel: TsPanel
        Width = 1106
        inherited edt_SearchText: TsEdit
          Height = 23
          TabOrder = 4
        end
        inherited com_SearchKeyword: TsComboBox
          ItemIndex = -1
          TabOrder = 2
          TabStop = False
          Text = ''
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        inherited Mask_SearchDate1: TsMaskEdit
          Height = 23
          TabOrder = 0
          OnDblClick = sMaskEdit1DblClick
        end
        inherited sBitBtn1: TsBitBtn
          OnClick = sBitBtn22Click
        end
        inherited sBitBtn21: TsBitBtn
          Tag = 902
          TabOrder = 5
          TabStop = False
          OnClick = btn_CalClick
        end
        inherited Mask_SearchDate2: TsMaskEdit
          Height = 23
          TabOrder = 1
          OnDblClick = sMaskEdit1DblClick
        end
        inherited sBitBtn23: TsBitBtn
          Tag = 903
          TabStop = False
          OnClick = btn_CalClick
        end
      end
      inherited sDBGrid1: TsDBGrid
        Width = 1106
        TabStop = False
        DataSource = dsList
        PopupMenu = PopupMenu1
        OnDrawColumnCell = sDBGrid2DrawColumnCell
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK2'
            Title.Alignment = taCenter
            Title.Caption = #49345#54889
            Width = 38
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHK3'
            Title.Alignment = taCenter
            Title.Caption = #52376#47532
            Width = 74
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 213
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'MSeq'
            Title.Alignment = taCenter
            Title.Caption = #51312#44148#48320#44221#54943#49688
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BENEFC1'
            Title.Alignment = taCenter
            Title.Caption = #49688#54812#51088
            Width = 193
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AP_BANK1'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51008#54665
            Width = 164
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'APP_DATE'
            Title.Alignment = taCenter
            Title.Caption = #44060#49444#51068#51088
            Width = 82
            Visible = True
          end>
      end
    end
  end
  inherited btn_Panel: TsPanel [3]
    Width = 1114
    inherited sSpeedButton2: TsSpeedButton
      Left = 412
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 625
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 704
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 917
      Visible = False
    end
    object sSpeedButton8: TsSpeedButton [4]
      Left = 199
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel59: TsLabel [5]
      Left = 8
      Top = 5
      Width = 187
      Height = 17
      Caption = #52712#49548#48520#45733#54868#54872#49888#50857#51109' '#48320#44221#49888#52397#49436
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel [6]
      Left = 8
      Top = 20
      Width = 40
      Height = 13
      Caption = 'APP707'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sSpeedButton9: TsSpeedButton [7]
      Left = 883
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    inherited btnExit: TsButton
      OnClick = btnExitClick
    end
    inherited btnNew: TsButton
      Left = 209
      OnClick = btnNewClick
    end
    inherited btnEdit: TsButton
      Left = 276
      OnClick = btnEditClick
    end
    inherited btnDel: TsButton
      Left = 343
      OnClick = btnDelClick
    end
    inherited btnCopy: TsButton
      Left = 910
      Visible = False
    end
    inherited btnPrint: TsButton
      Left = 635
      OnClick = btnPrintClick
    end
    inherited btnTemp: TsButton
      Left = 422
      OnClick = btnSaveClick
    end
    inherited btnSave: TsButton
      Left = 489
      OnClick = btnSaveClick
    end
    inherited btnCancel: TsButton
      Left = 556
      OnClick = btnCancelClick
    end
    inherited sButton1: TsButton
      Left = 714
      OnClick = sButton1Click
    end
    inherited sButton3: TsButton
      Left = 807
      OnClick = sButton3Click
    end
  end
  inherited maintno_Panel: TsPanel [4]
    Width = 1114
    inherited edt_MaintNo: TsEdit
      Color = clWhite
      MaxLength = 35
    end
    inherited sMaskEdit1: TsMaskEdit
      TabOrder = 3
      OnDblClick = sMaskEdit1DblClick
    end
    inherited edt_UserNo: TsEdit
      TabOrder = 4
    end
    inherited btn_Cal: TsBitBtn
      TabStop = False
      OnClick = btn_CalClick
    end
    inherited sCheckBox1: TsCheckBox
      TabStop = False
      TabOrder = 7
    end
    inherited edt_chasu: TsEdit
      Left = 186
      Color = clWhite
      TabOrder = 1
    end
    inherited edt_msg1: TsEdit
      TabStop = True
      Color = clWhite
      MaxLength = 3
      TabOrder = 5
    end
    inherited sBitBtn7: TsBitBtn
      TabOrder = 8
    end
    inherited edt_msg2: TsEdit
      TabStop = True
      Color = clWhite
      MaxLength = 3
      TabOrder = 6
    end
  end
  inherited sPanel52: TsPanel
    Height = 633
    inherited sPanel53: TsPanel
      inherited sBitBtn22: TsBitBtn
        OnClick = sBitBtn22Click
      end
    end
    inherited sDBGrid2: TsDBGrid
      Height = 598
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      PopupMenu = PopupMenu1
      OnDrawColumnCell = sDBGrid2DrawColumnCell
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'CHK2'
          Title.Alignment = taCenter
          Title.Caption = #49345#54889
          Width = 41
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 162
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
  end
  inherited dsList: TDataSource
    Left = 88
  end
  inherited qryList: TADOQuery
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      
        'SELECT A707_1.MAINT_NO,A707_1.MSeq,A707_1.AMD_NO,A707_1.MESSAGE1' +
        ',A707_1.MESSAGE2,A707_1.[USER_ID],A707_1.DATEE,A707_1.APP_DATE,A' +
        '707_1.IN_MATHOD,'
      
        #9'   A707_1.AP_BANK,A707_1.AP_BANK1,A707_1.AP_BANK2,A707_1.AP_BAN' +
        'K3,A707_1.AP_BANK4,A707_1.AP_BANK5,'
      
        #9'   A707_1.AD_BANK,A707_1.AD_BANK1,A707_1.AD_BANK2,A707_1.AD_BAN' +
        'K3,A707_1.AD_BANK4,A707_1.AD_PAY,'
      
        #9'   A707_1.IL_NO1,A707_1.IL_NO2,A707_1.IL_NO3,A707_1.IL_NO4,A707' +
        '_1.IL_NO5,'
      
        #9'   A707_1.IL_AMT1,A707_1.IL_AMT2,A707_1.IL_AMT3,A707_1.IL_AMT4,' +
        'A707_1.IL_AMT5,'
      
        #9'   A707_1.IL_CUR1,A707_1.IL_CUR2,A707_1.IL_CUR3,A707_1.IL_CUR4,' +
        'A707_1.IL_CUR5,'
      
        #9'   A707_1.AD_INFO1,A707_1.AD_INFO2,A707_1.AD_INFO3,A707_1.AD_IN' +
        'FO4,A707_1.AD_INFO5,'
      
        #9'   A707_1.CD_NO,A707_1.ISS_DATE,A707_1.EX_DATE,A707_1.EX_PLACE,' +
        'A707_1.CHK1,A707_1.CHK2,A707_1.CHK3,A707_1.prno,A707_1.F_INTERFA' +
        'CE,'
      
        '                   A707_1.IMP_CD1,A707_1.IMP_CD2,A707_1.IMP_CD3,' +
        'A707_1.IMP_CD4,A707_1.IMP_CD5,'
      ''
      
        #9'   A707_2.MAINT_NO,A707_2.MSeq,A707_2.Amd_No,A707_2.APPLIC1,A70' +
        '7_2.APPLIC2,A707_2.APPLIC3,A707_2.APPLIC4,A707_2.APPLIC5,'
      
        #9'   A707_2.BENEFC,A707_2.BENEFC1,A707_2.BENEFC2,A707_2.BENEFC3,A' +
        '707_2.BENEFC4,A707_2.BENEFC5,A707_2.INCD_CUR,A707_2.INCD_AMT,'
      
        #9'   A707_2.DECD_CUR,A707_2.DECD_AMT,A707_2.NWCD_CUR,A707_2.NWCD_' +
        'AMT,A707_2.CD_PERP,A707_2.CD_PERM,A707_2.CD_MAX,'
      
        #9'   A707_2.AA_CV1,A707_2.AA_CV2,A707_2.AA_CV3,A707_2.AA_CV4,A707' +
        '_2.LOAD_ON,A707_2.FOR_TRAN,A707_2.LST_DATE,'
      
        #9'   A707_2.SHIP_PD,A707_2.SHIP_PD1,A707_2.SHIP_PD2,A707_2.SHIP_P' +
        'D3,A707_2.SHIP_PD4,A707_2.SHIP_PD5,A707_2.SHIP_PD6,'
      
        #9'   A707_2.NARRAT,A707_2.NARRAT_1,A707_2.EX_NAME1,A707_2.EX_NAME' +
        '2,A707_2.EX_NAME3,A707_2.EX_ADDR1,A707_2.EX_ADDR2,'
      
        #9'   A707_2.SRBUHO,A707_2.BFCD_AMT,A707_2.BFCD_CUR,A707_2.CARRIAG' +
        'E,A707_2.SUNJUCK_PORT,A707_2.DOCHACK_PORT'
      ''
      #9'  ,Mathod707.DOC_NAME as mathod_Name'
      #9'  ,Pay707.DOC_NAME as pay_Name'
      #9'  ,IMPCD707_1.DOC_NAME as Imp_Name_1'
      #9'  ,IMPCD707_2.DOC_NAME as Imp_Name_2'
      #9'  ,IMPCD707_3.DOC_NAME as Imp_Name_3'
      #9'  ,IMPCD707_4.DOC_NAME as Imp_Name_4'
      #9'  ,IMPCD707_5.DOC_NAME as Imp_Name_5'
      #9'  ,CDMAX707.DOC_NAME as CDMAX_Name'
      #9'  ,cMethod707.DOC_NAME as Carriage_Name'
      ''
      'FROM [dbo].[APP707_1] AS A707_1 with(nolock)'
      
        'INNER JOIN [dbo].[APP707_2] AS A707_2 with(nolock) ON A707_1.MAI' +
        'NT_NO = A707_2.MAINT_NO AND A707_1.MSeq = A707_2.MSeq'
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44060#49444#48169#48277#39') Mathod707 ON A707_1.IN_MATHOD = Mathod' +
        '707.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#49888#50857#44277#50668#39') Pay707 ON A707_1.AD_PAY = Pay707.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_1 ON A707_1.IMP_CD1 = IMPCD' +
        '707_1.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_2 ON A707_1.IMP_CD2 = IMPCD' +
        '707_2.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_3 ON A707_1.IMP_CD3 = IMPCD' +
        '707_3.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_4 ON A707_1.IMP_CD4 = IMPCD' +
        '707_4.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'IMP_CD'#39') IMPCD707_5 ON A707_1.IMP_CD5 = IMPCD' +
        '707_5.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'CD_MAX'#39') CDMAX707 ON A707_2.CD_MAX = CDMAX707' +
        '.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39'C_METHOD'#39') cMethod707 ON A707_2.CARRIAGE = cM' +
        'ethod707.CODE')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMSeq: TIntegerField
      FieldName = 'MSeq'
    end
    object qryListAMD_NO: TIntegerField
      FieldName = 'AMD_NO'
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListAPP_DATE: TStringField
      FieldName = 'APP_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListIN_MATHOD: TStringField
      FieldName = 'IN_MATHOD'
      Size = 3
    end
    object qryListAP_BANK: TStringField
      FieldName = 'AP_BANK'
      Size = 4
    end
    object qryListAP_BANK1: TStringField
      FieldName = 'AP_BANK1'
      Size = 35
    end
    object qryListAP_BANK2: TStringField
      FieldName = 'AP_BANK2'
      Size = 35
    end
    object qryListAP_BANK3: TStringField
      FieldName = 'AP_BANK3'
      Size = 35
    end
    object qryListAP_BANK4: TStringField
      FieldName = 'AP_BANK4'
      Size = 35
    end
    object qryListAP_BANK5: TStringField
      FieldName = 'AP_BANK5'
      Size = 35
    end
    object qryListAD_BANK: TStringField
      FieldName = 'AD_BANK'
      Size = 4
    end
    object qryListAD_BANK1: TStringField
      FieldName = 'AD_BANK1'
      Size = 35
    end
    object qryListAD_BANK2: TStringField
      FieldName = 'AD_BANK2'
      Size = 35
    end
    object qryListAD_BANK3: TStringField
      FieldName = 'AD_BANK3'
      Size = 35
    end
    object qryListAD_BANK4: TStringField
      FieldName = 'AD_BANK4'
      Size = 35
    end
    object qryListAD_PAY: TStringField
      FieldName = 'AD_PAY'
      Size = 3
    end
    object qryListIL_NO1: TStringField
      FieldName = 'IL_NO1'
      Size = 35
    end
    object qryListIL_NO2: TStringField
      FieldName = 'IL_NO2'
      Size = 35
    end
    object qryListIL_NO3: TStringField
      FieldName = 'IL_NO3'
      Size = 35
    end
    object qryListIL_NO4: TStringField
      FieldName = 'IL_NO4'
      Size = 35
    end
    object qryListIL_NO5: TStringField
      FieldName = 'IL_NO5'
      Size = 35
    end
    object qryListIL_AMT1: TBCDField
      FieldName = 'IL_AMT1'
      Precision = 18
    end
    object qryListIL_AMT2: TBCDField
      FieldName = 'IL_AMT2'
      Precision = 18
    end
    object qryListIL_AMT3: TBCDField
      FieldName = 'IL_AMT3'
      Precision = 18
    end
    object qryListIL_AMT4: TBCDField
      FieldName = 'IL_AMT4'
      Precision = 18
    end
    object qryListIL_AMT5: TBCDField
      FieldName = 'IL_AMT5'
      Precision = 18
    end
    object qryListIL_CUR1: TStringField
      FieldName = 'IL_CUR1'
      Size = 3
    end
    object qryListIL_CUR2: TStringField
      FieldName = 'IL_CUR2'
      Size = 3
    end
    object qryListIL_CUR3: TStringField
      FieldName = 'IL_CUR3'
      Size = 3
    end
    object qryListIL_CUR4: TStringField
      FieldName = 'IL_CUR4'
      Size = 3
    end
    object qryListIL_CUR5: TStringField
      FieldName = 'IL_CUR5'
      Size = 3
    end
    object qryListAD_INFO1: TStringField
      FieldName = 'AD_INFO1'
      Size = 35
    end
    object qryListAD_INFO2: TStringField
      FieldName = 'AD_INFO2'
      Size = 70
    end
    object qryListAD_INFO3: TStringField
      FieldName = 'AD_INFO3'
      Size = 70
    end
    object qryListAD_INFO4: TStringField
      FieldName = 'AD_INFO4'
      Size = 70
    end
    object qryListAD_INFO5: TStringField
      FieldName = 'AD_INFO5'
      Size = 70
    end
    object qryListCD_NO: TStringField
      FieldName = 'CD_NO'
      Size = 35
    end
    object qryListISS_DATE: TStringField
      FieldName = 'ISS_DATE'
      Size = 8
    end
    object qryListEX_DATE: TStringField
      FieldName = 'EX_DATE'
      Size = 8
    end
    object qryListEX_PLACE: TStringField
      FieldName = 'EX_PLACE'
      Size = 35
    end
    object qryListCHK1: TBooleanField
      FieldName = 'CHK1'
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      OnGetText = qryListCHK2GetText
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      OnGetText = qryListCHK3GetText
      Size = 10
    end
    object qryListprno: TIntegerField
      FieldName = 'prno'
    end
    object qryListF_INTERFACE: TStringField
      FieldName = 'F_INTERFACE'
      Size = 1
    end
    object qryListIMP_CD1: TStringField
      FieldName = 'IMP_CD1'
      Size = 3
    end
    object qryListIMP_CD2: TStringField
      FieldName = 'IMP_CD2'
      Size = 3
    end
    object qryListIMP_CD3: TStringField
      FieldName = 'IMP_CD3'
      Size = 3
    end
    object qryListIMP_CD4: TStringField
      FieldName = 'IMP_CD4'
      Size = 3
    end
    object qryListIMP_CD5: TStringField
      FieldName = 'IMP_CD5'
      Size = 3
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListMSeq_1: TIntegerField
      FieldName = 'MSeq_1'
    end
    object qryListAmd_No_1: TIntegerField
      FieldName = 'Amd_No_1'
    end
    object qryListAPPLIC1: TStringField
      FieldName = 'APPLIC1'
      Size = 35
    end
    object qryListAPPLIC2: TStringField
      FieldName = 'APPLIC2'
      Size = 35
    end
    object qryListAPPLIC3: TStringField
      FieldName = 'APPLIC3'
      Size = 35
    end
    object qryListAPPLIC4: TStringField
      FieldName = 'APPLIC4'
      Size = 35
    end
    object qryListAPPLIC5: TStringField
      FieldName = 'APPLIC5'
      Size = 35
    end
    object qryListBENEFC: TStringField
      FieldName = 'BENEFC'
      Size = 10
    end
    object qryListBENEFC1: TStringField
      FieldName = 'BENEFC1'
      Size = 35
    end
    object qryListBENEFC2: TStringField
      FieldName = 'BENEFC2'
      Size = 35
    end
    object qryListBENEFC3: TStringField
      FieldName = 'BENEFC3'
      Size = 35
    end
    object qryListBENEFC4: TStringField
      FieldName = 'BENEFC4'
      Size = 35
    end
    object qryListBENEFC5: TStringField
      FieldName = 'BENEFC5'
      Size = 35
    end
    object qryListINCD_CUR: TStringField
      FieldName = 'INCD_CUR'
      Size = 3
    end
    object qryListINCD_AMT: TBCDField
      FieldName = 'INCD_AMT'
      Precision = 18
    end
    object qryListDECD_CUR: TStringField
      FieldName = 'DECD_CUR'
      Size = 3
    end
    object qryListDECD_AMT: TBCDField
      FieldName = 'DECD_AMT'
      Precision = 18
    end
    object qryListNWCD_CUR: TStringField
      FieldName = 'NWCD_CUR'
      Size = 3
    end
    object qryListNWCD_AMT: TBCDField
      FieldName = 'NWCD_AMT'
      Precision = 18
    end
    object qryListCD_PERP: TIntegerField
      FieldName = 'CD_PERP'
    end
    object qryListCD_PERM: TIntegerField
      FieldName = 'CD_PERM'
    end
    object qryListCD_MAX: TStringField
      FieldName = 'CD_MAX'
      Size = 3
    end
    object qryListAA_CV1: TStringField
      FieldName = 'AA_CV1'
      Size = 35
    end
    object qryListAA_CV2: TStringField
      FieldName = 'AA_CV2'
      Size = 35
    end
    object qryListAA_CV3: TStringField
      FieldName = 'AA_CV3'
      Size = 35
    end
    object qryListAA_CV4: TStringField
      FieldName = 'AA_CV4'
      Size = 35
    end
    object qryListLOAD_ON: TStringField
      FieldName = 'LOAD_ON'
      Size = 70
    end
    object qryListFOR_TRAN: TStringField
      FieldName = 'FOR_TRAN'
      Size = 70
    end
    object qryListLST_DATE: TStringField
      FieldName = 'LST_DATE'
      Size = 8
    end
    object qryListSHIP_PD: TBooleanField
      FieldName = 'SHIP_PD'
    end
    object qryListSHIP_PD1: TStringField
      FieldName = 'SHIP_PD1'
      Size = 70
    end
    object qryListSHIP_PD2: TStringField
      FieldName = 'SHIP_PD2'
      Size = 70
    end
    object qryListSHIP_PD3: TStringField
      FieldName = 'SHIP_PD3'
      Size = 70
    end
    object qryListSHIP_PD4: TStringField
      FieldName = 'SHIP_PD4'
      Size = 70
    end
    object qryListSHIP_PD5: TStringField
      FieldName = 'SHIP_PD5'
      Size = 70
    end
    object qryListSHIP_PD6: TStringField
      FieldName = 'SHIP_PD6'
      Size = 70
    end
    object qryListNARRAT: TBooleanField
      FieldName = 'NARRAT'
    end
    object qryListNARRAT_1: TMemoField
      FieldName = 'NARRAT_1'
      BlobType = ftMemo
    end
    object qryListEX_NAME1: TStringField
      FieldName = 'EX_NAME1'
      Size = 35
    end
    object qryListEX_NAME2: TStringField
      FieldName = 'EX_NAME2'
      Size = 35
    end
    object qryListEX_NAME3: TStringField
      FieldName = 'EX_NAME3'
      Size = 35
    end
    object qryListEX_ADDR1: TStringField
      FieldName = 'EX_ADDR1'
      Size = 35
    end
    object qryListEX_ADDR2: TStringField
      FieldName = 'EX_ADDR2'
      Size = 35
    end
    object qryListSRBUHO: TStringField
      FieldName = 'SRBUHO'
      Size = 35
    end
    object qryListBFCD_AMT: TBCDField
      FieldName = 'BFCD_AMT'
      Precision = 18
    end
    object qryListBFCD_CUR: TStringField
      FieldName = 'BFCD_CUR'
      Size = 3
    end
    object qryListCARRIAGE: TStringField
      FieldName = 'CARRIAGE'
      Size = 3
    end
    object qryListSUNJUCK_PORT: TStringField
      FieldName = 'SUNJUCK_PORT'
      Size = 65
    end
    object qryListDOCHACK_PORT: TStringField
      FieldName = 'DOCHACK_PORT'
      Size = 65
    end
    object qryListmathod_Name: TStringField
      FieldName = 'mathod_Name'
      Size = 100
    end
    object qryListpay_Name: TStringField
      FieldName = 'pay_Name'
      Size = 100
    end
    object qryListImp_Name_1: TStringField
      FieldName = 'Imp_Name_1'
      Size = 100
    end
    object qryListImp_Name_2: TStringField
      FieldName = 'Imp_Name_2'
      Size = 100
    end
    object qryListImp_Name_3: TStringField
      FieldName = 'Imp_Name_3'
      Size = 100
    end
    object qryListImp_Name_4: TStringField
      FieldName = 'Imp_Name_4'
      Size = 100
    end
    object qryListImp_Name_5: TStringField
      FieldName = 'Imp_Name_5'
      Size = 100
    end
    object qryListCDMAX_Name: TStringField
      FieldName = 'CDMAX_Name'
      Size = 100
    end
    object qryListCarriage_Name: TStringField
      FieldName = 'Carriage_Name'
      Size = 100
    end
  end
  object sp_attachAPP707fromINF700: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'AttachAPP707fromINF700;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DOC_COUNT'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 56
    Top = 240
  end
  object sp_attachAPP707fromINF707: TADOStoredProc
    Connection = DMMssql.KISConnect
    ProcedureName = 'AttachAPP707fromINF707;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@DocNo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 35
        Value = Null
      end
      item
        Name = '@USER_ID'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@DOC_COUNT'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 88
    Top = 240
  end
  object PopupMenu1: TPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DMICON.System26
    OnPopup = PopupMenu1Popup
    Left = 56
    Top = 272
    object N1: TMenuItem
      Caption = #51204#49569#51456#48708
      ImageIndex = 29
      OnClick = sButton1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Caption = #45936#51060#53552' '#49688#51221
      ImageIndex = 3
      OnClick = btnEditClick
    end
    object N4: TMenuItem
      Caption = #45936#51060#53552' '#49325#51228
      ImageIndex = 27
      OnClick = btnDelClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object d1: TMenuItem
      Caption = #52636#47141#54616#44592
      object N8: TMenuItem
        Caption = #48120#47532#48372#44592
        ImageIndex = 15
        OnClick = btnPrintClick
      end
      object N9: TMenuItem
        Tag = 1
        Caption = #54532#47536#53944
        ImageIndex = 21
      end
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = 'ADMIN'
      Enabled = False
      ImageIndex = 16
    end
  end
  object qryReady: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end
      item
        Name = 'DOCID'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'MESQ'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'SRDATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 8
        Value = Null
      end
      item
        Name = 'SRTIME'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 6
        Value = Null
      end
      item
        Name = 'SRVENDOR'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 17
        Value = Null
      end
      item
        Name = 'SRSTATE'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'SRFLAT'
        Attributes = [paNullable, paLong]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2147483647
        Value = Null
      end
      item
        Name = 'Tag'
        Attributes = [paNullable]
        DataType = ftBoolean
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'TableName'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'ReadyUser'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end
      item
        Name = 'ReadyDept'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'CHK3'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @MAINT_NO varchar(35), @DOCID varchar(10)'
      ''
      'SET @MAINT_NO = :MAINT_NO'
      'SET @DOCID = :DOCID'
      ''
      
        'IF EXISTS(SELECT 1 FROM SR_READY WHERE DOCID = @DOCID AND MAINT_' +
        'NO = @MAINT_NO)'
      'BEGIN'
      
        #9'DELETE FROM SR_READY WHERE DOCID = @DOCID AND MAINT_NO = @MAINT' +
        '_NO'
      'END'
      ''
      'INSERT INTO SR_READY'
      
        'VALUES( @DOCID , @MAINT_NO , :MESQ , :SRDATE , :SRTIME , :SRVEND' +
        'OR , :SRSTATE , '#39#39' , :SRFLAT , :Tag , :TableName , :ReadyUser , ' +
        ':ReadyDept )'
      ''
      'UPDATE APP707_1'
      'SET CHK2 = 5'
      '      ,CHK3 = :CHK3'
      'WHERE MAINT_NO = @MAINT_NO')
    Left = 56
    Top = 304
  end
end
