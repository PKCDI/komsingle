inherited UI_VATBI4_frm: TUI_VATBI4_frm
  Left = 492
  Top = 97
  Caption = #49464#44552#44228#49328#49436'('#49688#49888')'
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel1: TsPanel
    inherited sSpeedButton2: TsSpeedButton
      Left = 113
    end
    inherited sSpeedButton3: TsSpeedButton
      Visible = False
    end
    inherited sSpeedButton4: TsSpeedButton
      Left = 787
      Visible = False
    end
    inherited sSpeedButton5: TsSpeedButton
      Left = 390
    end
    object sLabel59: TsLabel [4]
      Left = 8
      Top = 5
      Width = 101
      Height = 17
      Caption = #49464#44552#44228#49328#49436'('#49688#49888')'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel60: TsLabel [5]
      Left = 9
      Top = 20
      Width = 37
      Height = 13
      Caption = 'VATBI4'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    inherited btnExit: TsButton
      Images = DMICON.System18
      ImageIndex = 19
    end
    inherited btnNew: TsButton
      Left = 843
      Visible = False
    end
    inherited btnEdit: TsButton
      Left = 775
      Visible = False
    end
    inherited btnDel: TsButton
      Left = 123
      OnClick = btnDelClick
      Images = DMICON.System18
      ImageIndex = 26
    end
    inherited btnCopy: TsButton
      Left = 526
      Visible = False
    end
    inherited btnPrint: TsButton
      Left = 716
      Visible = False
    end
    inherited btnTemp: TsButton
      Left = 497
      Visible = False
      Images = DMICON.System18
    end
    inherited btnSave: TsButton
      Left = 565
      Visible = False
      Images = DMICON.System18
    end
    inherited btnCancel: TsButton
      Left = 633
      Visible = False
      Images = DMICON.System18
    end
    inherited btnReady: TsButton
      Left = 655
      Visible = False
      Images = DMICON.System18
      ImageIndex = 28
    end
    inherited btnSend: TsButton
      Left = 743
      Visible = False
      Images = DMICON.System18
      ImageIndex = 21
    end
    object sButton4: TsButton
      Left = 190
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 11
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 20
      ContentMargin = 8
    end
    object sButton2: TsButton
      Tag = 1
      Left = 289
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 12
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System18
      ImageIndex = 14
      ContentMargin = 8
    end
  end
  inherited sPanel4: TsPanel
    inherited edt_USER_ID: TsEdit
      Left = 520
    end
    inherited btn_Cal: TsBitBtn
      Visible = False
    end
    inherited edt_msg1: TsEdit
      Left = 641
    end
    inherited sBitBtn19: TsBitBtn
      Left = 674
      Visible = False
    end
    inherited edt_msg2: TsEdit
      Left = 729
    end
    inherited sBitBtn20: TsBitBtn
      Left = 762
      Visible = False
    end
  end
  inherited Page_control: TsPageControl
    inherited sTabSheet1: TsTabSheet
      inherited page1_RIGHT: TsPanel
        inherited taxPanel: TsPanel
          inherited edt_NEWINDICATOR: TsEdit [3]
            Width = 81
            Enabled = False
          end
          inherited sBitBtn4: TsBitBtn [4]
            Visible = False
          end
          inherited sBitBtn6: TsBitBtn [5]
            Visible = False
          end
          inherited sPanel5: TsPanel [6]
          end
          inherited edt_VATCODE: TsEdit [7]
            Width = 81
            Enabled = False
          end
          inherited edt_VATTYPE: TsEdit [8]
            Width = 81
            Enabled = False
          end
          inherited sBitBtn5: TsBitBtn [9]
            Visible = False
          end
          inherited edt_RENO: TsEdit
            Enabled = False
          end
          inherited edt_SENO: TsEdit
            Enabled = False
          end
          inherited edt_FSNO: TsEdit
            Enabled = False
          end
          inherited edt_ACENO: TsEdit
            Enabled = False
          end
          inherited edt_RFFNO: TsEdit
            Enabled = False
          end
        end
        inherited providerPanel: TsPanel
          inherited edt_SENAME3: TsEdit
            Enabled = False
          end
          inherited edt_SENAME2: TsEdit
            Enabled = False
          end
          inherited edt_SENAME1: TsEdit
            Enabled = False
          end
          inherited mask_SESAUP: TsMaskEdit
            Enabled = False
          end
          inherited sBitBtn2: TsBitBtn
            Visible = False
          end
          inherited edt_SEADDR1: TsEdit
            Enabled = False
          end
          inherited edt_SECODE: TsEdit
            Width = 104
            Enabled = False
          end
          inherited edt_SEADDR2: TsEdit
            Enabled = False
          end
          inherited edt_SEADDR3: TsEdit
            Enabled = False
          end
          inherited edt_SEADDR4: TsEdit
            Enabled = False
          end
          inherited edt_SEADDR5: TsEdit
            Enabled = False
          end
          inherited edt_SESAUP1: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_SEFTX1: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_SEFTX2: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_SEFTX3: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_SEFTX4: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_SEFTX5: TsEdit
            Left = 612
            Enabled = False
          end
          inherited memo_SEUPTA1: TsMemo
            Left = 476
            Enabled = False
          end
          inherited memo_SEITEM1: TsMemo
            Left = 476
            Enabled = False
          end
        end
        object byAddPanel: TsPanel
          Left = -328
          Top = 528
          Width = 361
          Height = 209
          
          TabOrder = 2
          Visible = False
          object sPanel6: TsPanel
            Left = 1
            Top = 1
            Width = 359
            Height = 40
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
            Align = alTop
            BevelOuter = bvNone
            Caption = #44277#44553#48155#45716#51088' '#45812#45817#51088' '#51221#48372' '#52628#44032
            Color = 16042877
            Ctl3D = False
            
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -15
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
          end
          object edt_BYFTX1_1: TsEdit
            Tag = 203
            Left = 93
            Top = 56
            Width = 250
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 1
            BoundLabel.Active = True
            BoundLabel.Caption = #45812#45817#48512#49436#47749'(2)'
          end
          object edt_BYFTX2_1: TsEdit
            Tag = 203
            Left = 93
            Top = 80
            Width = 250
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 2
            BoundLabel.Active = True
            BoundLabel.Caption = #45812#45817#51088#47749'(2)'
          end
          object edt_BYFTX3_1: TsEdit
            Tag = 203
            Left = 93
            Top = 104
            Width = 250
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 3
            BoundLabel.Active = True
            BoundLabel.Caption = #51204#54868#48264#54840'(2)'
          end
          object edt_BYFTX4_1: TsEdit
            Left = 93
            Top = 128
            Width = 116
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 4
            BoundLabel.Active = True
            BoundLabel.Caption = 'E-MAIL(2)'
          end
          object edt_BYFTX5_1: TsEdit
            Left = 229
            Top = 128
            Width = 114
            Height = 23
            HelpContext = 1
            CharCase = ecUpperCase
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 5
            BoundLabel.Active = True
            BoundLabel.Caption = '@'
          end
          object sBitBtn24: TsBitBtn
            Left = 100
            Top = 168
            Width = 82
            Height = 25
            Caption = #54869#51064
            TabOrder = 6
            ImageIndex = 17
            Images = DMICON.System18
          end
          object sBitBtn25: TsBitBtn
            Tag = 1
            Left = 183
            Top = 168
            Width = 82
            Height = 25
            Caption = #52712#49548
            TabOrder = 7
            ImageIndex = 18
            Images = DMICON.System18
          end
        end
      end
    end
    inherited sTabSheet4: TsTabSheet
      inherited sPanel11: TsPanel
        inherited buyerPanel: TsPanel
          inherited sBitBtn1: TsBitBtn [0]
            Enabled = False
            Visible = False
          end
          inherited edt_BYADDR1: TsEdit [1]
            Enabled = False
          end
          inherited edt_BYCODE: TsEdit [2]
            Width = 104
            Enabled = False
          end
          inherited edt_BYADDR2: TsEdit [3]
            Enabled = False
          end
          inherited edt_BYFTX3: TsEdit [4]
            Left = 476
            Enabled = False
          end
          inherited edt_BYNAME2: TsEdit [5]
            Enabled = False
          end
          inherited mask_BYSAUP: TsMaskEdit [6]
            Enabled = False
          end
          inherited edt_BYNAME1: TsEdit [7]
            Enabled = False
          end
          inherited edt_BYNAME3: TsEdit [8]
            Enabled = False
          end
          inherited sPanel8: TsPanel [9]
          end
          inherited edt_BYADDR4: TsEdit
            Enabled = False
          end
          inherited edt_BYADDR5: TsEdit
            Enabled = False
          end
          inherited edt_BYSAUP1: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_BYSAUPCODE: TsEdit [13]
            Width = 104
            Enabled = False
          end
          inherited edt_BYADDR3: TsEdit [14]
            Enabled = False
          end
          inherited edt_BYFTX1: TsEdit [15]
            Left = 476
            Enabled = False
          end
          inherited edt_BYFTX2: TsEdit [16]
            Left = 476
            Enabled = False
          end
          inherited edt_BYFTX5: TsEdit
            Left = 612
            Enabled = False
          end
          inherited edt_BYFTX4: TsEdit [18]
            Left = 476
            Enabled = False
          end
          inherited sBitBtn3: TsBitBtn
            Enabled = False
            Visible = False
          end
          inherited memo_BYUPTA1: TsMemo
            Left = 476
            Enabled = False
          end
          inherited memo_BYITEM1: TsMemo
            Left = 476
            Enabled = False
          end
          inherited sBitBtn7: TsBitBtn
            Left = 476
            Enabled = False
            Visible = False
          end
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sPanel10: TsPanel
        inherited sPanel13: TsPanel
          inherited sDBGrid1: TsDBGrid
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'SEQ'
                Title.Alignment = taCenter
                Title.Caption = #49692#48264
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NAME_COD'
                Title.Alignment = taCenter
                Title.Caption = #54408#47749#53076#46300
                Width = 120
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'DE_DATE'
                Title.Alignment = taCenter
                Title.Caption = #44277#44553#51068#51088
                Width = 82
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RATE'
                Title.Alignment = taCenter
                Title.Caption = #54872#50984
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'QTY_G'
                Title.Alignment = taCenter
                Title.Caption = #49688#47049#45800#50948
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTY'
                Title.Alignment = taCenter
                Title.Caption = #49688#47049
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'QTYG_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032#44592#51456#49688#47049#45800#50948
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QTYG'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032#44592#51456#49688#47049
                Width = 137
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'PRICE_G'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032#45800#50948
                Width = 52
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRICE'
                Title.Alignment = taCenter
                Title.Caption = #45800#44032
                Width = 137
                Visible = True
              end>
          end
        end
        inherited goodsPanel: TsPanel
          inherited sSpeedButton14: TsSpeedButton
            Left = 497
            Enabled = False
          end
          inherited sPanel22: TsPanel
            Left = 4
            Width = 489
          end
          inherited edt_NAMECOD_D: TsEdit
            Enabled = False
          end
          inherited sBitBtn15: TsBitBtn
            Enabled = False
            Visible = False
          end
          inherited memo_NAME1_D: TsMemo
            Width = 233
            Height = 55
            Enabled = False
          end
          inherited memo_SIZE1_D: TsMemo
            Top = 141
            Width = 443
            Enabled = False
          end
          inherited sPanel14: TsPanel
            Left = 511
            Width = 290
          end
          inherited mask_DEDATE_D: TsMaskEdit
            Left = 402
            Top = 82
            Enabled = False
          end
          inherited curr_RATE_D: TsCurrencyEdit
            Left = 402
            Top = 106
            Enabled = False
          end
          inherited memo_DEREM1_D: TsMemo
            Top = 227
            Width = 443
            Enabled = False
          end
          inherited edt_QTY_G_D: TsEdit
            Left = 614
            Enabled = False
          end
          inherited sBitBtn44: TsBitBtn
            Left = 654
            Enabled = False
            Visible = False
          end
          inherited curr_QTY_D: TsCurrencyEdit
            Left = 654
            Width = 138
            Enabled = False
          end
          inherited curr_PRICE_D: TsCurrencyEdit
            Left = 614
            Enabled = False
          end
          inherited edt_QTYG_G_D: TsEdit
            Left = 614
            Enabled = False
          end
          inherited sBitBtn47: TsBitBtn
            Left = 652
            Enabled = False
            Visible = False
          end
          inherited curr_QTYG_D: TsCurrencyEdit
            Left = 654
            Width = 138
            Enabled = False
          end
          inherited edt_USAMT_G_D: TsEdit
            Left = 614
            Enabled = False
          end
          inherited sBitBtn49: TsBitBtn
            Left = 654
            Enabled = False
            Visible = False
          end
          inherited curr_USAMT_D: TsCurrencyEdit
            Left = 654
            Width = 138
            Enabled = False
          end
          inherited curr_SUPAMT_D: TsCurrencyEdit
            Left = 614
            Enabled = False
          end
          inherited curr_TAXAMT_D: TsCurrencyEdit
            Left = 614
            Enabled = False
          end
          inherited edt_STQTY_G_D: TsEdit
            Left = 614
            Enabled = False
          end
          inherited sBitBtn9: TsBitBtn
            Left = 654
            Enabled = False
            Visible = False
          end
          inherited curr_STQTY_D: TsCurrencyEdit
            Left = 654
            Width = 138
            Enabled = False
          end
          inherited curr_SUPSTAMT_D: TsCurrencyEdit
            Left = 614
            Enabled = False
          end
          inherited curr_TAXSTAMT_D: TsCurrencyEdit
            Left = 614
            Enabled = False
          end
          inherited edt_USSTAMT_G_D: TsEdit
            Left = 614
            Enabled = False
          end
          inherited sBitBtn10: TsBitBtn
            Left = 654
            Enabled = False
            Visible = False
          end
          inherited curr_USSTAMT_D: TsCurrencyEdit
            Left = 654
            Width = 138
            Enabled = False
          end
          inherited sBitBtn8: TsBitBtn
            Left = 610
            Enabled = False
            Visible = False
          end
        end
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sPanel16: TsPanel
        inherited moneyPanel: TsPanel
          inherited sBevel2: TsBevel
            Width = 808
          end
          inherited sBevel3: TsBevel
            Width = 808
          end
          inherited sBevel4: TsBevel
            Width = 808
          end
          inherited mask_DRAWDAT: TsMaskEdit
            Left = 128
          end
          inherited curr_SUPAMT: TsCurrencyEdit
            Left = 296
          end
          inherited curr_TAXAMT: TsCurrencyEdit
            Left = 476
          end
          inherited curr_DETAILNO: TsCurrencyEdit
            Left = 656
          end
          inherited memo_REMARK1: TsMemo
            Left = 128
          end
          inherited edt_AMT11C: TsEdit
            Left = 340
            Top = 265
            Enabled = False
          end
          inherited sBitBtn11: TsBitBtn
            Left = 398
            Top = 265
            Visible = False
          end
          inherited curr_AMT12: TsCurrencyEdit
            Left = 109
            Top = 265
            Enabled = False
          end
          inherited curr_AMT11: TsCurrencyEdit
            Left = 380
            Top = 265
            Enabled = False
          end
          inherited curr_AMT22: TsCurrencyEdit
            Left = 109
            Top = 289
            Enabled = False
          end
          inherited edt_AMT21C: TsEdit
            Left = 340
            Top = 289
            Enabled = False
          end
          inherited sBitBtn12: TsBitBtn
            Left = 398
            Top = 289
            Visible = False
          end
          inherited curr_AMT21: TsCurrencyEdit
            Left = 380
            Top = 289
            Enabled = False
          end
          inherited curr_AMT32: TsCurrencyEdit
            Left = 109
            Top = 313
            Enabled = False
          end
          inherited edt_AMT31C: TsEdit
            Left = 340
            Top = 313
            Enabled = False
          end
          inherited sBitBtn13: TsBitBtn
            Left = 398
            Top = 313
            Visible = False
          end
          inherited curr_AMT31: TsCurrencyEdit
            Left = 380
            Top = 313
            Enabled = False
          end
          inherited curr_AMT42: TsCurrencyEdit
            Left = 109
            Top = 337
            Enabled = False
          end
          inherited edt_AMT41C: TsEdit
            Left = 340
            Top = 337
            Enabled = False
          end
          inherited sBitBtn14: TsBitBtn
            Left = 398
            Top = 337
            Visible = False
          end
          inherited curr_AMT41: TsCurrencyEdit
            Left = 380
            Top = 337
            Enabled = False
          end
          inherited edt_INDICATOR: TsEdit
            Left = 589
            Top = 265
            Enabled = False
          end
          inherited sBitBtn16: TsBitBtn
            Left = 631
            Top = 265
            Visible = False
          end
          inherited edt_INDICATORNAME: TsEdit
            Left = 629
            Top = 265
            Enabled = False
          end
          inherited curr_TAMT: TsCurrencyEdit
            Left = 109
            Enabled = False
          end
          inherited curr_SUPTAMT: TsCurrencyEdit
            Left = 109
            Enabled = False
          end
          inherited curr_TAXTAMT: TsCurrencyEdit
            Left = 589
            Enabled = False
          end
          inherited edt_USTAMTC: TsEdit
            Left = 340
            Enabled = False
          end
          inherited sBitBtn17: TsBitBtn
            Left = 390
            Visible = False
          end
          inherited curr_USTAMT: TsCurrencyEdit
            Left = 380
            Enabled = False
          end
          inherited edt_TQTYC: TsEdit
            Left = 340
            Enabled = False
          end
          inherited sBitBtn18: TsBitBtn
            Left = 390
            Visible = False
          end
          inherited curr_TQTY: TsCurrencyEdit
            Left = 380
            Enabled = False
          end
        end
      end
    end
    inherited sTabSheet6: TsTabSheet
      inherited sPanel12: TsPanel
        inherited trusteePanel: TsPanel
          inherited edt_AGNAME3: TsEdit
            Enabled = False
          end
          inherited edt_AGNAME2: TsEdit
            Enabled = False
          end
          inherited edt_AGNAME1: TsEdit
            Enabled = False
          end
          inherited mask_AGSAUP: TsMaskEdit
            Enabled = False
          end
          inherited sBitBtn21: TsBitBtn
            Enabled = False
            Visible = False
          end
          inherited edt_AGADDR1: TsEdit
            Enabled = False
          end
          inherited edt_AGCODE: TsEdit
            Width = 104
            Enabled = False
          end
          inherited edt_AGADDR2: TsEdit
            Enabled = False
          end
          inherited edt_AGADDR3: TsEdit
            Enabled = False
          end
          inherited edt_AGADDR4: TsEdit
            Enabled = False
          end
          inherited edt_AGADDR5: TsEdit
            Enabled = False
          end
          inherited edt_AGSAUP1: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_AGFTX1: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_AGFTX2: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_AGFTX3: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_AGFTX4: TsEdit
            Left = 476
            Enabled = False
          end
          inherited edt_AGFTX5: TsEdit
            Left = 612
            Enabled = False
          end
          inherited memo_AGUPTA1: TsMemo
            Left = 476
            Enabled = False
          end
          inherited memo_AGITEM1: TsMemo
            Left = 476
            Enabled = False
          end
        end
      end
    end
    inherited sTabSheet5: TsTabSheet
      inherited sDBGrid2: TsDBGrid
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SE_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #44277#44553#51088
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BY_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #44277#44553#48155#45716#51088
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ACE_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47144#52280#51312#48264#54840
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAMT'
            Title.Alignment = taCenter
            Title.Caption = #52509#44552#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #49688#49888#51088' ID'
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DRAW_DAT'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51068#51088
            Width = 82
            Visible = True
          end>
      end
      inherited sPanel21: TsPanel
        inherited edt_SearchText: TsEdit
          Height = 23
        end
        inherited com_SearchKeyword: TsComboBox
          OnSelect = com_SearchKeywordSelect
        end
      end
    end
  end
  inherited sPanel56: TsPanel
    inherited sDBGrid8: TsDBGrid
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 207
          Visible = True
        end>
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 24
    Top = 176
  end
  inherited qryList: TADOQuery
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    SQL.Strings = (
      
        'SELECT MAINT_NO, USER_ID, DATEE, MESSAGE1, MESSAGE2, RE_NO, SE_N' +
        'O, FS_NO, ACE_NO, RFF_NO, SE_CODE, SE_SAUP, SE_NAME1, SE_NAME2, ' +
        'SE_ADDR1, SE_ADDR2, SE_ADDR3, SE_UPTA, SE_UPTA1, SE_ITEM, SE_ITE' +
        'M1, BY_CODE, BY_SAUP, BY_NAME1, BY_NAME2, BY_ADDR1, BY_ADDR2, BY' +
        '_ADDR3, BY_UPTA, BY_UPTA1, BY_ITEM, BY_ITEM1, AG_CODE, AG_SAUP, ' +
        'AG_NAME1, AG_NAME2, AG_NAME3, AG_ADDR1, AG_ADDR2, AG_ADDR3, AG_U' +
        'PTA, AG_UPTA1, AG_ITEM, AG_ITEM1, DRAW_DAT, DETAILNO, SUP_AMT, T' +
        'AX_AMT, REMARK, REMARK1, AMT11, AMT11C, AMT12, AMT21, AMT21C, AM' +
        'T22, AMT31, AMT31C, AMT32, AMT41, AMT41C, AMT42, INDICATOR, TAMT' +
        ', SUPTAMT, TAXTAMT, USTAMT, USTAMTC, TQTY, TQTYC, CHK1, CHK2, CH' +
        'K3, PRNO, VAT_CODE, VAT_TYPE, NEW_INDICATOR, SE_ADDR4, SE_ADDR5,' +
        ' SE_SAUP1, SE_SAUP2, SE_SAUP3, SE_FTX1, SE_FTX2, SE_FTX3, SE_FTX' +
        '4, SE_FTX5, BY_SAUP_CODE, BY_ADDR4, BY_ADDR5, BY_SAUP1, BY_SAUP2' +
        ', BY_SAUP3, BY_FTX1, BY_FTX2, BY_FTX3, BY_FTX4, BY_FTX5, BY_FTX1' +
        '_1, BY_FTX2_1, BY_FTX3_1, BY_FTX4_1, BY_FTX5_1, AG_ADDR4, AG_ADD' +
        'R5, AG_SAUP1, AG_SAUP2, AG_SAUP3,'
      
        '             AG_FTX1, AG_FTX2, AG_FTX3, AG_FTX4, AG_FTX5, SE_NAM' +
        'E3, BY_NAME3,'
      '  '
      '             VATBI4_NAME.INDICATOR_NAME as INDICATOR_NAME'
      ''
      'From VATBI4_H'
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as INDICATOR_NAME FROM CODE2NDD with' +
        '(nolock) WHERE Prefix = '#39#50689#49688'/'#52397#44396#39') VATBI4_NAME ON VATBI4_H.INDICAT' +
        'OR = VATBI4_NAME.CODE')
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListRE_NO: TStringField
      FieldName = 'RE_NO'
      Size = 35
    end
    object qryListSE_NO: TStringField
      FieldName = 'SE_NO'
      Size = 35
    end
    object qryListFS_NO: TStringField
      FieldName = 'FS_NO'
      Size = 35
    end
    object qryListACE_NO: TStringField
      FieldName = 'ACE_NO'
      Size = 35
    end
    object qryListRFF_NO: TStringField
      FieldName = 'RFF_NO'
      Size = 35
    end
    object qryListSE_CODE: TStringField
      FieldName = 'SE_CODE'
      Size = 10
    end
    object qryListSE_SAUP: TStringField
      FieldName = 'SE_SAUP'
      Size = 35
    end
    object qryListSE_NAME1: TStringField
      FieldName = 'SE_NAME1'
      Size = 35
    end
    object qryListSE_NAME2: TStringField
      FieldName = 'SE_NAME2'
      Size = 35
    end
    object qryListSE_ADDR1: TStringField
      FieldName = 'SE_ADDR1'
      Size = 35
    end
    object qryListSE_ADDR2: TStringField
      FieldName = 'SE_ADDR2'
      Size = 35
    end
    object qryListSE_ADDR3: TStringField
      FieldName = 'SE_ADDR3'
      Size = 35
    end
    object qryListSE_UPTA: TStringField
      FieldName = 'SE_UPTA'
      Size = 35
    end
    object qryListSE_UPTA1: TMemoField
      FieldName = 'SE_UPTA1'
      BlobType = ftMemo
    end
    object qryListSE_ITEM: TStringField
      FieldName = 'SE_ITEM'
      Size = 1
    end
    object qryListSE_ITEM1: TMemoField
      FieldName = 'SE_ITEM1'
      BlobType = ftMemo
    end
    object qryListBY_CODE: TStringField
      FieldName = 'BY_CODE'
      Size = 10
    end
    object qryListBY_SAUP: TStringField
      FieldName = 'BY_SAUP'
      Size = 35
    end
    object qryListBY_NAME1: TStringField
      FieldName = 'BY_NAME1'
      Size = 35
    end
    object qryListBY_NAME2: TStringField
      FieldName = 'BY_NAME2'
      Size = 35
    end
    object qryListBY_ADDR1: TStringField
      FieldName = 'BY_ADDR1'
      Size = 35
    end
    object qryListBY_ADDR2: TStringField
      FieldName = 'BY_ADDR2'
      Size = 35
    end
    object qryListBY_ADDR3: TStringField
      FieldName = 'BY_ADDR3'
      Size = 35
    end
    object qryListBY_UPTA: TStringField
      FieldName = 'BY_UPTA'
      Size = 35
    end
    object qryListBY_UPTA1: TMemoField
      FieldName = 'BY_UPTA1'
      BlobType = ftMemo
    end
    object qryListBY_ITEM: TStringField
      FieldName = 'BY_ITEM'
      Size = 1
    end
    object qryListBY_ITEM1: TMemoField
      FieldName = 'BY_ITEM1'
      BlobType = ftMemo
    end
    object qryListAG_CODE: TStringField
      FieldName = 'AG_CODE'
      Size = 10
    end
    object qryListAG_SAUP: TStringField
      FieldName = 'AG_SAUP'
      Size = 35
    end
    object qryListAG_NAME1: TStringField
      FieldName = 'AG_NAME1'
      Size = 35
    end
    object qryListAG_NAME2: TStringField
      FieldName = 'AG_NAME2'
      Size = 35
    end
    object qryListAG_NAME3: TStringField
      FieldName = 'AG_NAME3'
      Size = 35
    end
    object qryListAG_ADDR1: TStringField
      FieldName = 'AG_ADDR1'
      Size = 35
    end
    object qryListAG_ADDR2: TStringField
      FieldName = 'AG_ADDR2'
      Size = 35
    end
    object qryListAG_ADDR3: TStringField
      FieldName = 'AG_ADDR3'
      Size = 35
    end
    object qryListAG_UPTA: TStringField
      FieldName = 'AG_UPTA'
      Size = 35
    end
    object qryListAG_UPTA1: TMemoField
      FieldName = 'AG_UPTA1'
      BlobType = ftMemo
    end
    object qryListAG_ITEM: TStringField
      FieldName = 'AG_ITEM'
      Size = 1
    end
    object qryListAG_ITEM1: TMemoField
      FieldName = 'AG_ITEM1'
      BlobType = ftMemo
    end
    object qryListDRAW_DAT: TStringField
      FieldName = 'DRAW_DAT'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListDETAILNO: TBCDField
      FieldName = 'DETAILNO'
      Precision = 18
    end
    object qryListSUP_AMT: TBCDField
      FieldName = 'SUP_AMT'
      Precision = 18
    end
    object qryListTAX_AMT: TBCDField
      FieldName = 'TAX_AMT'
      Precision = 18
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK1: TMemoField
      FieldName = 'REMARK1'
      BlobType = ftMemo
    end
    object qryListAMT11: TBCDField
      FieldName = 'AMT11'
      Precision = 18
    end
    object qryListAMT11C: TStringField
      FieldName = 'AMT11C'
      Size = 3
    end
    object qryListAMT12: TBCDField
      FieldName = 'AMT12'
      Precision = 18
    end
    object qryListAMT21: TBCDField
      FieldName = 'AMT21'
      Precision = 18
    end
    object qryListAMT21C: TStringField
      FieldName = 'AMT21C'
      Size = 3
    end
    object qryListAMT22: TBCDField
      FieldName = 'AMT22'
      Precision = 18
    end
    object qryListAMT31: TBCDField
      FieldName = 'AMT31'
      Precision = 18
    end
    object qryListAMT31C: TStringField
      FieldName = 'AMT31C'
      Size = 3
    end
    object qryListAMT32: TBCDField
      FieldName = 'AMT32'
      Precision = 18
    end
    object qryListAMT41: TBCDField
      FieldName = 'AMT41'
      Precision = 18
    end
    object qryListAMT41C: TStringField
      FieldName = 'AMT41C'
      Size = 3
    end
    object qryListAMT42: TBCDField
      FieldName = 'AMT42'
      Precision = 18
    end
    object qryListINDICATOR: TStringField
      FieldName = 'INDICATOR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListSUPTAMT: TBCDField
      FieldName = 'SUPTAMT'
      Precision = 18
    end
    object qryListTAXTAMT: TBCDField
      FieldName = 'TAXTAMT'
      Precision = 18
    end
    object qryListUSTAMT: TBCDField
      FieldName = 'USTAMT'
      Precision = 18
    end
    object qryListUSTAMTC: TStringField
      FieldName = 'USTAMTC'
      Size = 3
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      Precision = 18
    end
    object qryListTQTYC: TStringField
      FieldName = 'TQTYC'
      Size = 3
    end
    object qryListCHK1: TStringField
      FieldName = 'CHK1'
      Size = 1
    end
    object qryListCHK2: TStringField
      FieldName = 'CHK2'
      Size = 1
    end
    object qryListCHK3: TStringField
      FieldName = 'CHK3'
      Size = 10
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListVAT_CODE: TStringField
      FieldName = 'VAT_CODE'
      Size = 4
    end
    object qryListVAT_TYPE: TStringField
      FieldName = 'VAT_TYPE'
      Size = 4
    end
    object qryListNEW_INDICATOR: TStringField
      FieldName = 'NEW_INDICATOR'
      Size = 4
    end
    object qryListSE_ADDR4: TStringField
      FieldName = 'SE_ADDR4'
      Size = 35
    end
    object qryListSE_ADDR5: TStringField
      FieldName = 'SE_ADDR5'
      Size = 35
    end
    object qryListSE_SAUP1: TStringField
      FieldName = 'SE_SAUP1'
      Size = 4
    end
    object qryListSE_SAUP2: TStringField
      FieldName = 'SE_SAUP2'
      Size = 4
    end
    object qryListSE_SAUP3: TStringField
      FieldName = 'SE_SAUP3'
      Size = 4
    end
    object qryListSE_FTX1: TStringField
      FieldName = 'SE_FTX1'
      Size = 40
    end
    object qryListSE_FTX2: TStringField
      FieldName = 'SE_FTX2'
      Size = 30
    end
    object qryListSE_FTX3: TStringField
      FieldName = 'SE_FTX3'
    end
    object qryListSE_FTX4: TStringField
      FieldName = 'SE_FTX4'
    end
    object qryListSE_FTX5: TStringField
      FieldName = 'SE_FTX5'
    end
    object qryListBY_SAUP_CODE: TStringField
      FieldName = 'BY_SAUP_CODE'
      Size = 4
    end
    object qryListBY_ADDR4: TStringField
      FieldName = 'BY_ADDR4'
      Size = 35
    end
    object qryListBY_ADDR5: TStringField
      FieldName = 'BY_ADDR5'
      Size = 35
    end
    object qryListBY_SAUP1: TStringField
      FieldName = 'BY_SAUP1'
      Size = 4
    end
    object qryListBY_SAUP2: TStringField
      FieldName = 'BY_SAUP2'
      Size = 4
    end
    object qryListBY_SAUP3: TStringField
      FieldName = 'BY_SAUP3'
      Size = 4
    end
    object qryListBY_FTX1: TStringField
      FieldName = 'BY_FTX1'
      Size = 40
    end
    object qryListBY_FTX2: TStringField
      FieldName = 'BY_FTX2'
      Size = 30
    end
    object qryListBY_FTX3: TStringField
      FieldName = 'BY_FTX3'
    end
    object qryListBY_FTX4: TStringField
      FieldName = 'BY_FTX4'
    end
    object qryListBY_FTX5: TStringField
      FieldName = 'BY_FTX5'
    end
    object qryListBY_FTX1_1: TStringField
      FieldName = 'BY_FTX1_1'
      Size = 40
    end
    object qryListBY_FTX2_1: TStringField
      FieldName = 'BY_FTX2_1'
      Size = 30
    end
    object qryListBY_FTX3_1: TStringField
      FieldName = 'BY_FTX3_1'
    end
    object qryListBY_FTX4_1: TStringField
      FieldName = 'BY_FTX4_1'
    end
    object qryListBY_FTX5_1: TStringField
      FieldName = 'BY_FTX5_1'
    end
    object qryListAG_ADDR4: TStringField
      FieldName = 'AG_ADDR4'
      Size = 35
    end
    object qryListAG_ADDR5: TStringField
      FieldName = 'AG_ADDR5'
      Size = 35
    end
    object qryListAG_SAUP1: TStringField
      FieldName = 'AG_SAUP1'
      Size = 4
    end
    object qryListAG_SAUP2: TStringField
      FieldName = 'AG_SAUP2'
      Size = 4
    end
    object qryListAG_SAUP3: TStringField
      FieldName = 'AG_SAUP3'
      Size = 4
    end
    object qryListAG_FTX1: TStringField
      FieldName = 'AG_FTX1'
      Size = 40
    end
    object qryListAG_FTX2: TStringField
      FieldName = 'AG_FTX2'
      Size = 30
    end
    object qryListAG_FTX3: TStringField
      FieldName = 'AG_FTX3'
    end
    object qryListAG_FTX4: TStringField
      FieldName = 'AG_FTX4'
    end
    object qryListAG_FTX5: TStringField
      FieldName = 'AG_FTX5'
    end
    object qryListSE_NAME3: TStringField
      FieldName = 'SE_NAME3'
      Size = 35
    end
    object qryListBY_NAME3: TStringField
      FieldName = 'BY_NAME3'
      Size = 35
    end
    object qryListINDICATOR_NAME: TStringField
      FieldName = 'INDICATOR_NAME'
      Size = 100
    end
  end
  inherited qryGoods: TADOQuery
    AfterScroll = qryGoodsAfterScroll
    Parameters = <
      item
        Name = 'MAINT_NO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 35
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT  KEYY, SEQ, DE_DATE, NAME_COD, NAME1, SIZE1, DE_REM1, QTY' +
        ', QTY_G, QTYG, QTYG_G, PRICE, PRICE_G, SUPAMT, TAXAMT, USAMT, US' +
        'AMT_G, SUPSTAMT, TAXSTAMT, USSTAMT, USSTAMT_G, STQTY, STQTY_G, R' +
        'ATE'
      'FROM VATBI4_D'
      'WHERE KEYY = :MAINT_NO')
    object qryGoodsKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryGoodsSEQ: TBCDField
      FieldName = 'SEQ'
      Precision = 18
    end
    object qryGoodsDE_DATE: TStringField
      FieldName = 'DE_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryGoodsNAME_COD: TStringField
      FieldName = 'NAME_COD'
    end
    object qryGoodsNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryGoodsSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryGoodsDE_REM1: TMemoField
      FieldName = 'DE_REM1'
      BlobType = ftMemo
    end
    object qryGoodsQTY: TBCDField
      FieldName = 'QTY'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryGoodsQTYG: TBCDField
      FieldName = 'QTYG'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryGoodsPRICE: TBCDField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryGoodsPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryGoodsSUPAMT: TBCDField
      FieldName = 'SUPAMT'
      Precision = 18
    end
    object qryGoodsTAXAMT: TBCDField
      FieldName = 'TAXAMT'
      Precision = 18
    end
    object qryGoodsUSAMT: TBCDField
      FieldName = 'USAMT'
      Precision = 18
    end
    object qryGoodsUSAMT_G: TStringField
      FieldName = 'USAMT_G'
      Size = 3
    end
    object qryGoodsSUPSTAMT: TBCDField
      FieldName = 'SUPSTAMT'
      Precision = 18
    end
    object qryGoodsTAXSTAMT: TBCDField
      FieldName = 'TAXSTAMT'
      Precision = 18
    end
    object qryGoodsUSSTAMT: TBCDField
      FieldName = 'USSTAMT'
      Precision = 18
    end
    object qryGoodsUSSTAMT_G: TStringField
      FieldName = 'USSTAMT_G'
      Size = 3
    end
    object qryGoodsSTQTY: TBCDField
      FieldName = 'STQTY'
      Precision = 18
    end
    object qryGoodsSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryGoodsRATE: TBCDField
      FieldName = 'RATE'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
  end
end
