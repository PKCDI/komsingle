unit UI_SPCNTC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ChildForm, StdCtrls, sComboBox, Grids, DBGrids, acDBGrid, ExtCtrls,
  sSplitter, ComCtrls, sPageControl, Buttons, sBitBtn, sEdit, Mask, sMaskEdit,
  sButton, sSpeedButton, sPanel, sSkinProvider, sMemo, sCustomComboEdit,
  sCurrEdit, sCurrencyEdit, DB, ADODB, TypeDefine, QuickRpt, sLabel;

type
  TUI_SPCNTC_frm = class(TChildForm_frm)
    sPanel4: TsPanel;
    mask_DATEE: TsMaskEdit;
    edt_USER_ID: TsEdit;
    edt_msg1: TsEdit;
    edt_msg2: TsEdit;
    edt_MAINT_NO: TsEdit;
    Page_control: TsPageControl;
    sTabSheet1: TsTabSheet;
    sSplitter3: TsSplitter;
    sSplitter4: TsSplitter;
    page1_Left: TsPanel;
    sTabSheet2: TsTabSheet;
    sSplitter6: TsSplitter;
    sSplitter5: TsSplitter;
    page2_Left: TsPanel;
    sTabSheet6: TsTabSheet;
    sSplitter2: TsSplitter;
    sDBGrid1: TsDBGrid;
    sPanel5: TsPanel;
    edt_SearchText: TsEdit;
    com_SearchKeyword: TsComboBox;
    Mask_SearchDate1: TsMaskEdit;
    sBitBtn8: TsBitBtn;
    sBitBtn22: TsBitBtn;
    Mask_SearchDate2: TsMaskEdit;
    sBitBtn28: TsBitBtn;
    sPanel6: TsPanel;
    sSplitter1: TsSplitter;
    page2_right: TsPanel;
    page1_Right: TsPanel;
    sPanel56: TsPanel;
    sDBGrid8: TsDBGrid;
    sPanel57: TsPanel;
    Mask_fromDate: TsMaskEdit;
    sBitBtn60: TsBitBtn;
    mask_toDate: TsMaskEdit;
    sPanel1: TsPanel;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    btnExit: TsButton;
    btnDel: TsButton;
    sButton4: TsButton;
    sButton2: TsButton;
    basicPanel: TsPanel;
    sPanel3: TsPanel;
    edt_BGMGUBUN: TsEdit;
    mask_NTDATE: TsMaskEdit;
    mask_CPDATE: TsMaskEdit;
    edt_DMNO: TsEdit;
    edt_LCNO: TsEdit;
    edt_CPNO: TsEdit;
    appPanel: TsPanel;
    sPanel10: TsPanel;
    edt_APPSNAME: TsEdit;
    edt_APPNAME: TsEdit;
    mask_APPNO: TsMaskEdit;
    noPanel: TsPanel;
    sPanel9: TsPanel;
    edt_RCNO: TsEdit;
    edt_RCNO2: TsEdit;
    edt_RCNO3: TsEdit;
    edt_RCNO4: TsEdit;
    edt_RCNO5: TsEdit;
    edt_RCNO6: TsEdit;
    edt_RCNO7: TsEdit;
    bankPanel: TsPanel;
    sPanel12: TsPanel;
    edt_CPBANK: TsEdit;
    edt_CPBANKNAME: TsEdit;
    edt_CPBANKELEC: TsEdit;
    edt_CPACCOUNTNO: TsEdit;
    edt_CPNAME1: TsEdit;
    edt_CPNAME2: TsEdit;
    edt_CPAMTC: TsEdit;
    curr_CPAMTU: TsCurrencyEdit;
    curr_CPAMT: TsCurrencyEdit;
    edt_CPTOTAMTC: TsEdit;
    curr_CPTOTAMT: TsCurrencyEdit;
    edt_CPTOTCHARGEC: TsEdit;
    curr_CPTOTCHARGE: TsCurrencyEdit;
    edt_RCNO8: TsEdit;
    edt_FINNO: TsEdit;
    edt_FINNO2: TsEdit;
    edt_FINNO3: TsEdit;
    edt_FINNO4: TsEdit;
    edt_FINNO5: TsEdit;
    edt_FINNO6: TsEdit;
    edt_FINNO7: TsEdit;
    memoPanel: TsPanel;
    sPanel14: TsPanel;
    memo_CPFTX1: TsMemo;
    sDBGrid4: TsDBGrid;
    billPanel: TsPanel;
    sPanel18: TsPanel;
    edt_CHARGENO: TsEdit;
    edt_CHARGETYPE: TsEdit;
    edt_CHARGETYPENAME: TsEdit;
    curr_CUXRATE: TsCurrencyEdit;
    edt_SESDAY: TsEdit;
    mask_SESSDATE: TsMaskEdit;
    mask_SESSEDATE: TsMaskEdit;
    edt_BASAMTC: TsEdit;
    curr_BASAMT: TsCurrencyEdit;
    edt_CHAAMTC: TsEdit;
    curr_CHAAMT: TsCurrencyEdit;
    goodsBtnPanel: TsPanel;
    sSpeedButton10: TsSpeedButton;
    sSpeedButton19: TsSpeedButton;
    btn_goodsDel: TsSpeedButton;
    sSpeedButton22: TsSpeedButton;
    btn_goodsMod: TsSpeedButton;
    sSpeedButton24: TsSpeedButton;
    btn_goodsSave: TsSpeedButton;
    btn_goodsCancel: TsSpeedButton;
    sSpeedButton27: TsSpeedButton;
    btn_goodsNew: TsSpeedButton;
    edt_CPBANKBU: TsEdit;
    qryList: TADOQuery;
    dsList: TDataSource;
    dsGoods: TDataSource;
    qryGoods: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListBGM_GUBUN: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListNT_DATE: TStringField;
    qryListCP_DATE: TStringField;
    qryListDM_NO: TStringField;
    qryListLC_NO: TStringField;
    qryListCP_ACCOUNTNO: TStringField;
    qryListCP_NAME1: TStringField;
    qryListCP_NAME2: TStringField;
    qryListCP_BANK: TStringField;
    qryListCP_BANKNAME: TStringField;
    qryListCP_BANKBU: TStringField;
    qryListAPP_SNAME: TStringField;
    qryListAPP_NAME: TStringField;
    qryListCP_BANKELEC: TStringField;
    qryListCP_AMTU: TBCDField;
    qryListCP_AMTC: TStringField;
    qryListCP_AMT: TBCDField;
    qryListCP_TOTAMT: TBCDField;
    qryListCP_TOTAMTC: TStringField;
    qryListCP_TOTCHARGE: TBCDField;
    qryListCP_TOTCHARGEC: TStringField;
    qryListCP_FTX: TStringField;
    qryListCP_FTX1: TMemoField;
    qryListCHK1: TStringField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListBSN_HSCODE: TStringField;
    qryListAPP_NO: TStringField;
    qryListCP_NO: TStringField;
    qryGoodsKEYY: TStringField;
    qryGoodsSEQ: TIntegerField;
    qryGoodsCHARGE_NO: TStringField;
    qryGoodsCHARGE_TYPE: TStringField;
    qryGoodsCHARGE_RATE: TStringField;
    qryGoodsBAS_AMT: TBCDField;
    qryGoodsBAS_AMTC: TStringField;
    qryGoodsCHA_AMT: TBCDField;
    qryGoodsCHA_AMTC: TStringField;
    qryGoodsCUX_RATE: TBCDField;
    qryGoodsSES_DAY: TStringField;
    qryGoodsSES_SDATE: TStringField;
    qryGoodsSES_EDATE: TStringField;
    edt_BGMGUBUNNAME: TsEdit;
    qryListRC_NO: TStringField;
    qryListRC_NO2: TStringField;
    qryListRC_NO3: TStringField;
    qryListRC_NO4: TStringField;
    qryListRC_NO5: TStringField;
    qryListRC_NO6: TStringField;
    qryListRC_NO7: TStringField;
    qryListRC_NO8: TStringField;
    qryListFIN_NO: TStringField;
    qryListFIN_NO2: TStringField;
    qryListFIN_NO3: TStringField;
    qryListFIN_NO4: TStringField;
    qryListFIN_NO5: TStringField;
    qryListFIN_NO6: TStringField;
    qryListFIN_NO7: TStringField;
    qryGoodsCHARGE_TYPE_Name: TStringField;
    edt_CHARGERATE: TsEdit;
    sLabel59: TsLabel;
    sLabel60: TsLabel;
    sSplitter7: TsSplitter;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure qryGoodsAfterScroll(DataSet: TDataSet);
    procedure sBitBtn60Click(Sender: TObject);
    procedure qryListAfterOpen(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure com_SearchKeywordSelect(Sender: TObject);
    procedure sBitBtn8Click(Sender: TObject);
    procedure Page_controlChange(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn28Click(Sender: TObject);
    procedure qryListBGM_GUBUNGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure sDBGrid8DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure sButton4Click(Sender: TObject);
  private
    { Private declarations }
    SPCNTC_SQL : string;
    procedure ReadDocument;
    procedure GoodsReadDocument;
    procedure DeleteDocument;
    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;


  public
    { Public declarations }
  protected
    ProgramControlType : TProgramControlType;
  end;

var
  UI_SPCNTC_frm: TUI_SPCNTC_frm;

implementation

{$R *.dfm}

uses
    MessageDefine, DateUtils, Commonlib, StrUtils, MSSQL, VarDefine, KISCalendar, SPCNTC_PRINT;

procedure TUI_SPCNTC_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
  Action := caFree;
  UI_SPCNTC_frm := nil;
end;

procedure TUI_SPCNTC_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_SPCNTC_frm.FormCreate(Sender: TObject);
begin
  inherited;
  SPCNTC_SQL := qryList.SQL.Text;
  ProgramControlType := ctView;
end;

procedure TUI_SPCNTC_frm.FormShow(Sender: TObject);
begin
  inherited;
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  Page_control.ActivePageIndex := 0;

  ProgramControlType := ctView;

  EnabledControlValue(basicPanel);
  EnabledControlValue(appPanel);
  EnabledControlValue(bankPanel);
  EnabledControlValue(noPanel);
  EnabledControlValue(memoPanel);
  EnabledControlValue(billPanel);
  EnabledControlValue(sPanel4);

  ReadList(Mask_fromDate.Text,mask_toDate.Text,'');
end;

procedure TUI_SPCNTC_frm.ReadDocument;
begin
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;
  //----------------------------------------------------------------------------
  //기본정보
  //----------------------------------------------------------------------------
    //관리번호
    edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
    //신청일자
    mask_DATEE.Text := qryListDATEE.AsString;
    //사용자
    edt_USER_ID.Text := qryListUSER_ID.AsString;
    //문서기능
    edt_msg1.Text := qryListMESSAGE1.AsString;
    //유형
    edt_msg2.Text := qryListMESSAGE2.AsString;
  //----------------------------------------------------------------------------
  //기본입력사항
  //----------------------------------------------------------------------------
    //응답서구분
    edt_BGMGUBUN.Text := qryListBGM_GUBUN.AsString;
  {
  CODE2NDD에 해당하는 code가 없어서 직접 입력
  //응답서 구분 코드에 따른 NAME
  공급자
    2CQ : 내국신용장 추심(매입)결과통보 : 추심(매입) 승인, 부도, 취소 통지용
    2CR : 내국신용장 추심(매입)결과통보 : 추심(매입) 신청반려 통지용
  구매자
    2DT : 내국신용장 추심(매입)결과통보 : 승인, 부도, 매입취소 통지용
  }

    if (qryListBGM_GUBUN.AsString = '2CQ') or (qryListBGM_GUBUN.AsString = '2DT' ) then
    begin
      if qryListMESSAGE1.AsString = '9' then edt_BGMGUBUNNAME.Text := '추심(매입) 승인통보';
      if qryListMESSAGE1.AsString = '7' then edt_BGMGUBUNNAME.Text := '추심(매입) 부도통보';
      if qryListMESSAGE1.AsString = '1' then edt_BGMGUBUNNAME.Text := '추심(매입) 취소통보';
    end
    else if qryListBGM_GUBUN.AsString = '2CR' then
    begin
      if qryListMESSAGE1.AsString = '9' then edt_BGMGUBUNNAME.Text := '추심(매입) 신청반려통보'
    end;

    //통지일자
    mask_NTDATE.Text := qryListNT_DATE.AsString;
    //매입일자
    mask_CPDATE.Text := qryListCP_DATE.AsString;
    //추심매입의뢰서 전자문서번호
    edt_DMNO.Text := qryListDM_NO.AsString;
    //내국신용장번호
    edt_LCNO.Text := qryListLC_NO.AsString;
    //추심(매입)신청번호
    edt_CPNO.Text := qryListCP_NO.AsString;
  //----------------------------------------------------------------------------
  //신청인
  //----------------------------------------------------------------------------
    //상호명
    edt_APPSNAME.Text := qryListAPP_SNAME.AsString;
    //대표자명
    edt_APPNAME.Text := qryListAPP_NAME.AsString;
    //사업자등록번호
    mask_APPNO.Text := qryListAPP_NO.AsString;
  //----------------------------------------------------------------------------
  //추심매입은행
  //----------------------------------------------------------------------------
    //은행코드
    edt_CPBANK.Text := qryListCP_BANK.AsString;
    //은행명
    edt_CPBANKNAME.Text := qryListCP_BANKNAME.AsString;
    //(부)점명
    edt_CPBANKBU.Text := qryListCP_BANKBU.AsString;
    //전자서명
    edt_CPBANKELEC.Text := qryListCP_BANKELEC.AsString;
    //계좌번호
    edt_CPACCOUNTNO.Text := qryListCP_ACCOUNTNO.AsString;
    //계좌주
    edt_CPNAME1.Text := qryListCP_NAME1.AsString;
    edt_CPNAME2.Text := qryListCP_NAME2.AsString;
    //외화금액
    edt_CPAMTC.Text := qryListCP_AMTC.AsString;
    curr_CPAMTU.Value := qryListCP_AMTU.AsCurrency;
    //원화금액
    curr_CPAMT.Value := qryListCP_AMT.AsCurrency;
    //최종지급액
    edt_CPTOTAMTC.Text := qryListCP_TOTAMTC.AsString;
    curr_CPTOTAMT.Value := qryListCP_TOTAMT.AsCurrency;
    //수수료합계
    edt_CPTOTCHARGEC.Text := qryListCP_TOTCHARGEC.AsString;
    curr_CPTOTCHARGE.Value := qryListCP_TOTCHARGE.AsCurrency;
  //----------------------------------------------------------------------------
  //물품수령증명서/세금계산서번호
  //----------------------------------------------------------------------------
    //물품수령증명서 번호
    edt_RCNO.Text := qryListRC_NO.AsString;
    edt_RCNO2.Text := qryListRC_NO2.AsString;
    edt_RCNO3.Text := qryListRC_NO3.AsString;
    edt_RCNO4.Text := qryListRC_NO4.AsString;
    edt_RCNO5.Text := qryListRC_NO5.AsString;
    edt_RCNO6.Text := qryListRC_NO6.AsString;
    edt_RCNO7.Text := qryListRC_NO7.AsString;
    edt_RCNO8.Text := qryListRC_NO8.AsString;
    //세금계산서 번호
    edt_FINNO.Text := qryListFIN_NO.AsString;
    edt_FINNO2.Text := qryListFIN_NO2.AsString;
    edt_FINNO3.Text := qryListFIN_NO3.AsString;
    edt_FINNO4.Text := qryListFIN_NO4.AsString;
    edt_FINNO5.Text := qryListFIN_NO5.AsString;
    edt_FINNO6.Text := qryListFIN_NO6.AsString;
    edt_FINNO7.Text := qryListFIN_NO7.AsString;
    //통보내용
    memo_CPFTX1.Text := qryListCP_FTX1.AsString;

end;

procedure TUI_SPCNTC_frm.GoodsReadDocument;
begin
  //계산서번호
  edt_CHARGENO.Text := qryGoodsCHARGE_NO.AsString;
  //수수료(이자) 유형
  edt_CHARGETYPE.Text := qryGoodsCHARGE_TYPE.AsString;
  edt_CHARGETYPENAME.Text := qryGoodsCHARGE_TYPE_Name.AsString;
  //적용일수
  edt_SESDAY.Text := qryGoodsSES_DAY.AsString;
  //적용기간
  mask_SESSDATE.Text := qryGoodsSES_SDATE.AsString;
  mask_SESSEDATE.Text := qryGoodsSES_EDATE.AsString;
  //적용요율
  edt_CHARGERATE.Text := qryGoodsCHARGE_RATE.AsString;
  //대상금액
  edt_BASAMTC.Text := qryGoodsBAS_AMTC.AsString;
  curr_BASAMT.Value := qryGoodsBAS_AMT.AsCurrency;
  //산출금액
  edt_CHAAMTC.Text := qryGoodsCHA_AMTC.AsString;
  curr_CHAAMT.Value := qryGoodsCHA_AMT.AsCurrency;
  //적용환율
  curr_CUXRATE.Value := qryGoodsCUX_RATE.AsCurrency;


end;

procedure TUI_SPCNTC_frm.Readlist(OrderSyntax: string);
begin
 if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if Page_control.ActivePageIndex = 3 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := SPCNTC_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 :
      begin
        SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :
      begin
        SQL.Add(' WHERE CP_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add(' ORDER BY DATEE ASC ');
    end;

    Open;
  end;
end;

function TUI_SPCNTC_frm.Readlist(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
 Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := SPCNTC_SQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_SPCNTC_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.State = dsBrowse then ReadDocument;
                                   
  with qryGoods do
  begin
    Close;
    Parameters.ParamByName('MAINT_NO').Value := qryListMAINT_NO.AsString;
    Open;
  end;

  GoodsReadDocument;

end;

procedure TUI_SPCNTC_frm.qryGoodsAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryGoods.RecordCount = 0 then Exit;
  if qryGoods.State = dsBrowse then GoodsReadDocument;
end;

procedure TUI_SPCNTC_frm.sBitBtn60Click(Sender: TObject);
begin
  inherited;
  IF AnsiMatchText('',[Mask_fromDate.Text, Mask_toDate.Text]) Then
  begin
    MessageBox(Self.Handle,MSG_NOT_VALID_DATE,'조회오류',MB_OK+MB_ICONERROR);
    Exit;
  end;
  Readlist(Mask_fromDate.Text,Mask_toDate.Text);
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := mask_toDate.Text;
end;

procedure TUI_SPCNTC_frm.qryListAfterOpen(DataSet: TDataSet);
begin
  inherited;
    IF DataSet.RecordCount = 0 Then
  begin
    qrylistAfterScroll(qrylist);
  end;
end;

procedure TUI_SPCNTC_frm.DeleteDocument;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
   try
    try
      IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
      begin
        nCursor := qryList.RecNo;


        SQL.Text :=  'DELETE FROM SPCNTC_H WHERE MAINT_NO =' + QuotedStr(maint_no);
        ExecSQL;

        SQL.Text :=  'DELETE FROM SPCNTC_D WHERE KEYY =' + QuotedStr(maint_no);
        ExecSQL;


        //트랜잭션 커밋
        DMMssql.KISConnect.CommitTrans;

        qryList.Close;
        qryList.Open;

        if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
        begin
          qryList.MoveBy(nCursor-1);
        end
        else
          qryList.First;
      end
      else
      begin
          if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
      end;

    except
      on E:Exception do
      begin
        DMMssql.KISConnect.RollbackTrans;
        MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
      end;
    end;

   finally
    Close;
    Free;
   end;
  end;
end;

procedure TUI_SPCNTC_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  DeleteDocument;
end;

procedure TUI_SPCNTC_frm.com_SearchKeywordSelect(Sender: TObject);
begin
  inherited;
  edt_SearchText.Visible := com_SearchKeyword.ItemIndex <> 0;
  Mask_SearchDate1.Visible := com_SearchKeyword.ItemIndex = 0;
  Mask_SearchDate2.Visible := com_SearchKeyword.ItemIndex = 0;
  sPanel6.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn22.Visible := com_SearchKeyword.ItemIndex = 0;
  sBitBtn28.Visible := com_SearchKeyword.ItemIndex = 0;
end;

procedure TUI_SPCNTC_frm.sBitBtn8Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_SPCNTC_frm.Page_controlChange(Sender: TObject);
begin
  //상단의 관리번호 패널 숨김
  //sPanel4.Visible := not (Page_control.ActivePageIndex = 2);
  // 왼쪽 DBGrid 숨김
  sPanel56.Visible := not (Page_control.ActivePageIndex = 2);
end;

procedure TUI_SPCNTC_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

//if not (ProgramControlType in [ctInsert,ctModify,ctView]) then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := Pos.X;
  KISCalendar_frm.Top := Pos.Y;

 (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));
end;

procedure TUI_SPCNTC_frm.sBitBtn28Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    902 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    903 : Mask_SearchDate1DblClick(Mask_SearchDate2);
  end;
end;

procedure TUI_SPCNTC_frm.qryListBGM_GUBUNGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
    if qryListBGM_GUBUN.AsString = '2CR' then
      Text := '반려통보'
    else
      Text := '승인통보';
end;

procedure TUI_SPCNTC_frm.sDBGrid8DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_SPCNTC_frm.sButton4Click(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  SPCNTC_PRINT_frm := TSPCNTC_PRINT_frm.Create(Self);
  try
    SPCNTC_PRINT_frm.MaintNo := edt_MAINT_NO.Text;

    SPCNTC_PRINT_frm.Prepare;
    case (Sender as TsButton).Tag of
      0 :
      begin
        SPCNTC_PRINT_frm.PrinterSetup;
        if SPCNTC_PRINT_frm.Tag = 0 then SPCNTC_PRINT_frm.Print;
      end;

      1: SPCNTC_PRINT_frm.Preview;
    end;
  finally
    FreeAndNil(SPCNTC_PRINT_frm);
  end;
end;

end.

