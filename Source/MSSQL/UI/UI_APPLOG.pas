unit UI_APPLOG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_APPLOG_BP, ChildForm, sSkinProvider, StdCtrls, sComboBox,
  Grids, DBGrids, acDBGrid, sCustomComboEdit, sCurrEdit, sCurrencyEdit,
  ComCtrls, sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit,
  sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, sSplitter, DB, ADODB,
  Menus;

type
  TUI_APPLOG_frm = class(TUI_APPLOG_BP_frm)
    dsList: TDataSource;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListMESSAGE3: TStringField;
    qryListCR_NAME1: TStringField;
    qryListCR_NAME2: TStringField;
    qryListCR_NAME3: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_NAME3: TStringField;
    qryListMS_NAME1: TStringField;
    qryListMS_NAME2: TStringField;
    qryListMS_NAME3: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListINV_AMT: TBCDField;
    qryListINV_AMTC: TStringField;
    qryListLC_G: TStringField;
    qryListLC_NO: TStringField;
    qryListBL_G: TStringField;
    qryListBL_NO: TStringField;
    qryListCARRIER1: TStringField;
    qryListCARRIER2: TStringField;
    qryListAR_DATE: TStringField;
    qryListBL_DATE: TStringField;
    qryListAPP_DATE: TStringField;
    qryListLOAD_LOC: TStringField;
    qryListLOAD_TXT: TStringField;
    qryListARR_LOC: TStringField;
    qryListARR_TXT: TStringField;
    qryListSPMARK1: TStringField;
    qryListSPMARK2: TStringField;
    qryListSPMARK3: TStringField;
    qryListSPMARK4: TStringField;
    qryListSPMARK5: TStringField;
    qryListSPMARK6: TStringField;
    qryListSPMARK7: TStringField;
    qryListSPMARK8: TStringField;
    qryListSPMARK9: TStringField;
    qryListSPMARK10: TStringField;
    qryListPAC_QTY: TBCDField;
    qryListPAC_QTYC: TStringField;
    qryListGOODS1: TStringField;
    qryListGOODS2: TStringField;
    qryListGOODS3: TStringField;
    qryListGOODS4: TStringField;
    qryListGOODS5: TStringField;
    qryListGOODS6: TStringField;
    qryListGOODS7: TStringField;
    qryListGOODS8: TStringField;
    qryListGOODS9: TStringField;
    qryListGOODS10: TStringField;
    qryListTRM_PAYC: TStringField;
    qryListTRM_PAY: TStringField;
    qryListBANK_CD: TStringField;
    qryListBANK_TXT: TStringField;
    qryListBANK_BR: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListPRNO: TIntegerField;
    qryListCN_NAME1: TStringField;
    qryListCN_NAME2: TStringField;
    qryListCN_NAME3: TStringField;
    qryListB5_NAME1: TStringField;
    qryListB5_NAME2: TStringField;
    qryListB5_NAME3: TStringField;
    qryListMSG1NAME: TStringField;
    qryListLCGNAME: TStringField;
    qryListBLGNAME: TStringField;
    qryListLOADNAME: TStringField;
    qryListARRNAME: TStringField;
    qryListTRMPAYCNAME: TStringField;
    qryListCRNAEM3NAME: TStringField;
    spCopyAPPLOG: TADOStoredProc;
    qryReady: TADOQuery;
    popMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N7: TMenuItem;
    d1: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    sSpeedButton6: TsSpeedButton;
    sSplitter7: TsSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure sBitBtn13Click(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure sDBGrid2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure qryListCHK2GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure mask_DATEEDblClick(Sender: TObject);
    procedure btn_CalClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnCopyClick(Sender: TObject);
    procedure btnPrintClick(Sender: TObject);
    procedure btnReadyClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure qryListCHK3GetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    APPLOG_SQL : String;
    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;
    procedure CancelDocument;
    procedure NewDocument;
    procedure SaveDocument(Sender : TObject);
    function CHECK_VALIDITY:String;
    procedure EditDocument;

    procedure ReadyDocument(DocNo : String);

   //마지막 콤포넌트에서 탭이나 엔터키 누르면 다음페이지로 이동하는 프로시져
   procedure DialogKey(var msg : TCMDialogKey); message CM_DIALOGKEY;

  protected
    procedure ReadDocument; override;
    procedure DeleteDocumnet; override;

  public
    { Public declarations }

  end;

var
  UI_APPLOG_frm: TUI_APPLOG_frm;

implementation

{$R *.dfm}

uses MSSQL, strUtils, DateUtils, Commonlib, TypeDefine, messageDefine,
  KISCalendar, AutoNo, Dlg_Customer, VarDefine, SQLCreator, Dlg_ErrorMessage, Dialog_CopyAPPLOG, APPLOG_PRINT
  , Dlg_RecvSelect, DocumentSend, CreateDocuments;

procedure TUI_APPLOG_frm.FormCreate(Sender: TObject);
begin
  inherited;
  APPLOG_SQL := qryList.SQL.Text;
end;

procedure TUI_APPLOG_frm.FormShow(Sender: TObject);
begin
  inherited;
  ProgramControlType := ctView;

  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel2);
  EnabledControlValue(sPanel17);
  EnabledControlValue(sPanel53 , False); //좌측검색패널
  EnabledControlValue(sPanel6 , False); //데이터조회검색패널

  ReadList(Mask_fromDate.Text , Mask_toDate.Text , '');

  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel2);
    ClearControlValue(sPanel17);
  end;


end;

procedure TUI_APPLOG_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_APPLOG_frm := nil;
end;

procedure TUI_APPLOG_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  //
end;

procedure TUI_APPLOG_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 1 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := APPLOG_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 :
      begin
        SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :
      begin
        SQL.Add(' WHERE LC_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add('ORDER BY DATEE ASC ');
    end;
    
    Open;
  end;

end;

function TUI_APPLOG_frm.Readlist(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
  Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := APPLOG_SQL;
    SQL.Add('WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add('ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_APPLOG_frm.sBitBtn13Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_APPLOG_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  ReadList(Mask_fromDate.Text , Mask_toDate.Text , '');
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := Mask_toDate.Text;
end;

procedure TUI_APPLOG_frm.ReadDocument;
begin
  inherited;
  if not qryList.Active then Exit;
  if qryList.RecordCount = 0 then Exit;

  //관리번호
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  //등록일자
  mask_DATEE.Text := qryListDATEE.AsString;
  //사용자
  edt_USER_ID.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg2.Text := qryListMESSAGE2.AsString;
  //유형
  edt_msg3.Text := qryListMESSAGE3.AsString;
  //신청일자
  mask_APPDATE.Text := qryListAPP_DATE.AsString;
  //선하증권발급일자
  mask_BLDATE.Text := qryListBL_DATE.AsString;
  //도착(예정)일
  mask_ARDATE.Text := qryListAR_DATE.AsString;
  //운송유형
  edt_msg1.Text := qryListMESSAGE1.AsString;
  edt_msg1Name.Text := qryListMSG1NAME.AsString;
  //신용장(계약서)
  edt_LCG.Text := qryListLC_G.AsString;
  edt_LCGNAME.Text := qryListLCGNAME.AsString;
  edt_LCNO.Text := qryListLC_NO.AsString;
  //선하증권 or 항공화물운송장
  edt_BLG.Text := qryListBL_G.AsString;
  edt_BLGNAME.Text := qryListBLGNAME.AsString;
  edt_BLNO.Text := qryListBL_NO.AsString;
  //선기명
  edt_CARRIER1.Text := qryListCARRIER1.AsString;
  //항해번호
  edt_CARRIER2.Text := qryListCARRIER2.AsString;
  //상업송장금액
  edt_INVAMTC.Text := qryListINV_AMTC.AsString;
  curr_INVAMT.Value := qryListINV_AMT.AsCurrency;
  //포장수
  edt_PACQTYC.Text := qryListPAC_QTYC.AsString;
  curr_PACQTY.Value := qryListPAC_QTY.AsCurrency;
  //선박회사
  edt_CRNAME1.Text := qryListCR_NAME1.AsString;
  edt_CRNAME2.Text := qryListCR_NAME2.AsString;
  edt_CRNAME3.Text := qryListCR_NAME3.AsString;
  edt_CRCODENAME3.Text := qryListCRNAEM3NAME.AsString;
  //인수예정자
  edt_B5NAME1.Text := qryListB5_NAME1.AsString;
  edt_B5NAME2.Text := qryListB5_NAME2.AsString;
  edt_B5NAME3.Text := qryListB5_NAME3.AsString;
  //송하인
  edt_SENAME1.Text := qryListSE_NAME1.AsString;
  edt_SENAME2.Text := qryListSE_NAME2.AsString;
  edt_SENAME3.Text := qryListSE_NAME3.AsString;
  //수하인
  edt_CNNAME1.Text := qryListCN_NAME1.AsString;
  edt_CNNAME2.Text := qryListCN_NAME2.AsString;
  edt_CNNAME3.Text := qryListCN_NAME3.AsString;
  //선적항
  edt_LOADLOC.Text := qryListLOAD_LOC.AsString;
  edt_LOADLOCNAME.Text := qryListLOADNAME.AsString;
  edt_LOADTXT.Text := qryListLOAD_TXT.AsString;
  //도착항
  edt_ARRLOC.Text := qryListARR_LOC.AsString;
  edt_ARRLOCNAME.Text := qryListARRNAME.AsString;
  edt_ARRTXT.Text := qryListARR_TXT.AsString;
  //화물표시 및 번호
  edt_SPMARK1.Text := qryListSPMARK1.AsString;
  edt_SPMARK2.Text := qryListSPMARK2.AsString;
  edt_SPMARK3.Text := qryListSPMARK3.AsString;
  edt_SPMARK4.Text := qryListSPMARK4.AsString;
  edt_SPMARK5.Text := qryListSPMARK5.AsString;
  edt_SPMARK6.Text := qryListSPMARK6.AsString;
  edt_SPMARK7.Text := qryListSPMARK7.AsString;
  edt_SPMARK8.Text := qryListSPMARK8.AsString;
  edt_SPMARK9.Text := qryListSPMARK9.AsString;
  edt_SPMARK10.Text := qryListSPMARK10.AsString;
  //상품명세
  edt_GOODS1.Text := qryListGOODS1.AsString;
  edt_GOODS2.Text := qryListGOODS2.AsString;
  edt_GOODS3.Text := qryListGOODS3.AsString;
  edt_GOODS4.Text := qryListGOODS4.AsString;
  edt_GOODS5.Text := qryListGOODS5.AsString;
  edt_GOODS6.Text := qryListGOODS6.AsString;
  edt_GOODS7.Text := qryListGOODS7.AsString;
  edt_GOODS8.Text := qryListGOODS8.AsString;
  edt_GOODS9.Text := qryListGOODS9.AsString;
  edt_GOODS10.Text := qryListGOODS10.AsString;
  //신청자
  edt_MSNAME1.Text := qryListMS_NAME1.AsString;
  edt_MSNAME2.Text := qryListMS_NAME2.AsString;
  //신청자 전자서명
  edt_MSNAME3.Text := qryListMS_NAME3.AsString;
  //명의인
  edt_AXNAME1.Text := qryListAX_NAME1.AsString;
  edt_AXNAME2.Text := qryListAX_NAME2.AsString;
  edt_AXNAME3.Text := qryListAX_NAME3.AsString;
  //결제구분 및 기간
  edt_TRMPAYC.Text := qryListTRM_PAYC.AsString;
  edt_TRMPAYCNAME.Text := qryListTRMPAYCNAME.AsString;
  edt_TRMPAY.Text := qryListTRM_PAY.AsString;
  //발급은행
  edt_BANKCD.Text := qryListBANK_CD.AsString;
  edt_BANKTXT.Text := qryListBANK_TXT.AsString;
  edt_BANKBR.Text := qryListBANK_BR.AsString;

end;

procedure TUI_APPLOG_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if qryList.RecordCount <> 0 then
    ReadDocument;
end;

procedure TUI_APPLOG_frm.sDBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  CHK2Value : Integer;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  CHK2Value := StrToIntDef(qryList.FieldByName('CHK2').AsString,-1);

  with Sender as TsDBGrid do
  begin

    case CHK2Value of
      9 :
      begin
        if Column.FieldName = 'CHK2' then
          Canvas.Font.Style := [fsBold]
        else
          Canvas.Font.Style := [];

        Canvas.Brush.Color := clBtnFace;
      end;

      6 :
      begin
        if AnsiMatchText( Column.FieldName , ['CHK2','CHK3']) then
          Canvas.Font.Color := clRed
        else
          Canvas.Font.Color := clBlack;
      end;
    else
      Canvas.Font.Style := [];
      Canvas.Font.Color := clBlack;
      Canvas.Brush.Color := clWhite;
    end;
  end;

  with Sender as TsDBGrid do
  begin
      if (gdSelected in State) or (SelectedRows.CurrentRowSelected) then
      begin
        Canvas.Brush.Color := $008DDCFA;
        Canvas.Font.Color := clBlack;
      end;
      DefaultDrawColumnCell(Rect,DataCol,Column,State);
  end;
end;

procedure TUI_APPLOG_frm.qryListCHK2GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var
  nIndex : integer;
begin
  inherited;
  nIndex := StrToIntDef( qryListCHK2.AsString , -1);
  case nIndex of
    -1 : Text := '';
    0: Text := '임시';
    1: Text := '저장';
    2: Text := '결재';
    3: Text := '반려';
    4: Text := '접수';
    5: Text := '준비';
    6: Text := '취소';
    7: Text := '전송';
    8: Text := '오류';
    9: Text := '승인';
  end;
end;

procedure TUI_APPLOG_frm.mask_DATEEDblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  IF not (ProgramControlType in [ctInsert,ctModify,ctView]) Then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));

end;

procedure TUI_APPLOG_frm.btn_CalClick(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    1 : mask_DATEEDblClick(btn_Cal); //등록일자
    2 : mask_DATEEDblClick(mask_APPDATE); //신청일자
    3 : mask_DATEEDblClick(mask_BLDATE); //선하증권발급일자
    4 : mask_DATEEDblClick(mask_ARDATE); //도착예정일자
    5 : mask_DATEEDblClick(Mask_SearchDate1);
    6 : mask_DATEEDblClick(Mask_SearchDate2);
  end
end;

procedure TUI_APPLOG_frm.btnNewClick(Sender: TObject);
begin
  inherited;
  NewDocument;
end;

procedure TUI_APPLOG_frm.CancelDocument;
begin
  if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctView;
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sDBGrid1.Enabled := True;
  sDBGrid2.Enabled := True;
//------------------------------------------------------------------------------
// 에디트 비활성화
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel2);
  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel17);
  EnabledControlValue(sPanel53 , False); //좌측검색패널
  EnabledControlValue(sPanel6 , False); //데이터조회검색패널
//------------------------------------------------------------------------------
// 버튼제어
//------------------------------------------------------------------------------
  ButtonEnable(True);
//------------------------------------------------------------------------------
// 새로고침
//------------------------------------------------------------------------------
  qryList.Close;
  qryList.Open;
  if qryList.RecordCount = 0 then
  begin
    ClearControlValue(spanel2);
    ClearControlValue(spanel4);
    ClearControlValue(spanel17); 
  end;

end;

procedure TUI_APPLOG_frm.btnCancelClick(Sender: TObject);
begin
  inherited;
  CancelDocument;
end;

procedure TUI_APPLOG_frm.NewDocument;
begin
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctInsert;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex := 0;
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Append;
//------------------------------------------------------------------------------
// 데이터제어
//------------------------------------------------------------------------------
  EnabledControlValue(sPanel4 , False);
  EnabledControlValue(sPanel2 , False);
  EnabledControlValue(sPanel17 , False);
  EnabledControlValue(sPanel53); //좌측검색패널
  EnabledControlValue(sPanel6); //데이터조회검색패널

//------------------------------------------------------------------------------
//기초자료
//------------------------------------------------------------------------------
  //관리번호 문서번호(BGM4)
  edt_MAINT_NO.Text := DMAutoNo.GetDocumentNoAutoInc('APPLOG');
  //등록일자
  mask_DATEE.Text := FormatDateTime('YYYYMMDD' , Now);
  //신청일자
  mask_APPDATE.Text := FormatDateTime('YYYYMMDD' , Now);
  //사용자
  edt_USER_ID.Text := LoginData.sID;
  //문서기능
  edt_msg2.Text := '9';
  //유형
  edt_msg3.Text := 'AB';
//------------------------------------------------------------------------------
 //신청자 명의인
 //------------------------------------------------------------------------------
  Dlg_Customer_frm := TDlg_Customer_frm.Create(Self);
  try
    with Dlg_Customer_frm do
    begin
      if isValue('00000') then
        edt_MSNAME1.Text := DataSource1.DataSet.FieldByName('ENAME').AsString; //상호
        edt_MSNAME2.Text := DataSource1.DataSet.FieldByName('ADDR1').AsString; //주소
        edt_MSNAME3.Text := DataSource1.DataSet.FieldByName('JENJA').AsString; //전자서명

        edt_AXNAME1.Text := DataSource1.DataSet.FieldByName('ENAME').AsString; //상호
        edt_AXNAME2.Text := DataSource1.DataSet.FieldByName('REP_NAME').AsString; //대표자명
        edt_AXNAME3.Text := DataSource1.DataSet.FieldByName('JENJA').AsString; //전자서명
    end;
  finally
    FreeAndNil(Dlg_Customer_frm);
  end;
end;

procedure TUI_APPLOG_frm.SaveDocument(Sender: TObject);
var
  SQLCreate : TSQLCreate;
  DocNo : String;
  CreateDate : TDateTime;
begin

  try
  //--------------------------------------------------------------------------
  // 유효성 검사
  //--------------------------------------------------------------------------
  if (Sender as TsButton).Tag = 1 then
  begin
    Dlg_ErrorMessage_frm := TDlg_ErrorMessage_frm.Create(Self);
    if Dlg_ErrorMessage_frm.Run_ErrorMessage( '수입화물선취보증(인도승락)신청서' ,CHECK_VALIDITY ) Then
    begin
      FreeAndNil(Dlg_ErrorMessage_frm);
      Exit;
    end;
  end;
  //----------------------------------------------------------------------------
  // SQL생성기
  //----------------------------------------------------------------------------
    SQLCreate := TSQLCreate.Create;
    with SQLCreate do
    begin
      DocNo := edt_MAINT_NO.Text;
    //----------------------------------------------------------------------
    // 문서헤더 및 APPLOG.DB
    //----------------------------------------------------------------------
      Case ProgramControlType of
        ctInsert :
        begin
          DMLType := dmlInsert;
          ADDValue('MAINT_NO',DocNo);
        end;
        ctModify :
        begin
          DMLType := dmlUpdate;
          ADDWhere('MAINT_NO',DocNo);
        end;
      end;

      SQLHeader('APPLOG');
      //문서저장구분(0:임시, 1:저장)
      ADDValue('CHK2' , IntToStr((Sender as TsButton).Tag));
      //등록일자
      ADDValue('DATEE' , mask_DATEE.Text);
      //사용자
      ADDValue('USER_ID', edt_USER_ID.Text);
      //문서기능
      ADDValue('MESSAGE2' , edt_msg2.Text);
      //유형
      ADDValue('MESSAGE3' , edt_msg3.Text);
      //신청일자
      ADDValue('APP_DATE' , mask_APPDATE.Text);
      //선하증권발급일
      ADDValue('BL_DATE' , mask_BLDATE.Text);
      //도착예정일
      ADDValue('AR_DATE' , mask_ARDATE.Text);
      //운송유형
      ADDValue('MESSAGE1' , edt_msg1.Text);
      //신용장(계약서)
      ADDValue('LC_G' , edt_LCG.Text);
      ADDValue('LC_NO' , edt_LCNO.Text);
      //선하증권 or 항공화물운송장
      ADDValue('BL_G' , edt_BLG.Text);
      ADDValue('BL_NO' , edt_BLNO.Text);
      //선기명
      ADDValue('CARRIER1' , edt_CARRIER1.Text);
      //항해번호
      ADDValue('CARRIER2' , edt_CARRIER2.Text);
      //상업송장금액
      ADDValue('INV_AMTC' , edt_INVAMTC.Text);
      ADDValue('INV_AMT' , CurrToStr(curr_INVAMT.Value));
      //포장수
      ADDValue('PAC_QTYC' , edt_PACQTYC.Text);
      ADDValue('PAC_QTY' , CurrToStr(curr_PACQTY.Value));
      //선박회사명
      ADDValue('CR_NAME1' , edt_CRNAME1.Text);
      ADDValue('CR_NAME2' , edt_CRNAME2.Text);
      ADDValue('CR_NAME3' , edt_CRNAME3.Text);
      //인수예정자
      ADDValue('B5_NAME1' , edt_B5NAME1.Text);
      ADDValue('B5_NAME2' , edt_B5NAME2.Text);
      ADDValue('B5_NAME3' , edt_B5NAME3.Text);
      //송하인
      ADDValue('SE_NAME1' , edt_SENAME1.Text);
      ADDValue('SE_NAME2' , edt_SENAME2.Text);
      ADDValue('SE_NAME3' , edt_SENAME3.Text);
      //수하인
      ADDValue('CN_NAME1' , edt_CNNAME1.Text);
      ADDValue('CN_NAME2' , edt_CNNAME2.Text);
      ADDValue('CN_NAME3' , edt_CNNAME3.Text);
      //선적항
      ADDValue('LOAD_LOC' , edt_LOADLOC.Text);
      ADDValue('LOAD_TXT' , edt_LOADTXT.Text);
      //도착항
      ADDValue('ARR_LOC' , edt_ARRLOC.Text);
      ADDValue('ARR_TXT' , edt_ARRTXT.Text);
      //화물표시 및 번호
      ADDValue('SPMARK1' , edt_SPMARK1.Text);
      ADDValue('SPMARK2' , edt_SPMARK2.Text);
      ADDValue('SPMARK3' , edt_SPMARK3.Text);
      ADDValue('SPMARK4' , edt_SPMARK4.Text);
      ADDValue('SPMARK5' , edt_SPMARK5.Text);
      ADDValue('SPMARK6' , edt_SPMARK6.Text);
      ADDValue('SPMARK7' , edt_SPMARK7.Text);
      ADDValue('SPMARK8' , edt_SPMARK8.Text);
      ADDValue('SPMARK9' , edt_SPMARK9.Text);
      ADDValue('SPMARK10' , edt_SPMARK10.Text);
      //상품명세
      ADDValue('GOODS1' , edt_GOODS1.Text);
      ADDValue('GOODS2' , edt_GOODS2.Text);
      ADDValue('GOODS3' , edt_GOODS3.Text);
      ADDValue('GOODS4' , edt_GOODS4.Text);
      ADDValue('GOODS5' , edt_GOODS5.Text);
      ADDValue('GOODS6' , edt_GOODS6.Text);
      ADDValue('GOODS7' , edt_GOODS7.Text);
      ADDValue('GOODS8' , edt_GOODS8.Text);
      ADDValue('GOODS9' , edt_GOODS9.Text);
      ADDValue('GOODS10' , edt_GOODS10.Text);
      //신청자
      ADDValue('MS_NAME1' , edt_MSNAME1.Text);
      ADDValue('MS_NAME2' , edt_MSNAME2.Text);
      ADDValue('MS_NAME3' , edt_MSNAME3.Text);
      //명의인
      ADDValue('AX_NAME1' , edt_AXNAME1.Text);
      ADDValue('AX_NAME2' , edt_AXNAME2.Text);
      ADDValue('AX_NAME3' , edt_AXNAME3.Text);
      //결제구분 및 기간
      ADDValue('TRM_PAYC' , edt_TRMPAYC.Text);
      ADDValue('TRM_PAY' , edt_TRMPAY.Text);
      //발급은행
      ADDValue('BANK_CD' , edt_BANKCD.Text);
      ADDValue('BANK_TXT' , edt_BANKTXT.Text);
      ADDValue('BANK_BR' , edt_BANKBR.Text);
    end;

    with TADOQuery.Create(nil) do
    begin
      try
        Connection := DMMssql.KISConnect;
        SQL.Text := SQLCreate.CreateSQL;
        //if sCheckBox1.Checked then
          //Clipboard.AsText := SQL.Text;
        ExecSQL;
      finally
        Close;
        Free;
      end;
    end;
    //------------------------------------------------------------------------------
    // 프로그램 제어
    //------------------------------------------------------------------------------
      ProgramControlType := ctView;
    //------------------------------------------------------------------------------
    // 버튼정리
    //------------------------------------------------------------------------------
      ButtonEnable(True);
    //------------------------------------------------------------------------------
    // 접근제어
    //------------------------------------------------------------------------------
      sPageControl1.ActivePageIndex := 0;
      sDBGrid1.Enabled := True;
      sDBGrid2.Enabled := True;
    //------------------------------------------------------------------------------
    // 데이터제어
    //------------------------------------------------------------------------------
      EnabledControlValue(sPanel4);
      EnabledControlValue(sPanel2);
      EnabledControlValue(sPanel17);
      EnabledControlValue(sPanel53 , False);
      EnabledControlValue(sPanel6 , False);
    //----------------------------------------------------------------------------
    //새로고침
    //----------------------------------------------------------------------------
    CreateDate := ConvertStr2Date(mask_DATEE.Text);
    Readlist(FormatDateTime('YYYYMMDD',StartOfTheYear(CreateDate)),
             FormatDateTime('YYYYMMDD',EndOfTheYear(CreateDate)),
             DocNo);
    //트랜잭션 저장
    if DMMssql.KISConnect.InTransaction Then DMMssql.KISConnect.CommitTrans;

  except
    on E:Exception do
    begin
      MessageBox(Self.Handle,PChar(MSG_SYSTEM_SAVE_ERR+#13#10+E.Message),'저장오류',MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TUI_APPLOG_frm.btnSaveClick(Sender: TObject);
begin
  inherited;
  SaveDocument(Sender);
end;

function TUI_APPLOG_frm.CHECK_VALIDITY: String;
var
  ErrMsg : TStringList;
  i : Integer;
begin
  ErrMsg := TStringList.Create;
  Try
    IF Trim(edt_msg1.Text) = '' THEN
      ErrMsg.Add('[문서정보] 운송유형을 입력해야합니다.');
    IF Trim(edt_LCG.Text) = '' THEN
      ErrMsg.Add('[문서정보] 신용장 구분을 입력해야합니다.');
    IF Trim(edt_LCNO.Text) = '' THEN
      ErrMsg.Add('[문서정보] 신용장 번호를 입력해야합니다.');
    IF Trim(edt_BLG.Text) = '' THEN
      ErrMsg.Add('[문서정보] 선하증권 구분을 입력해야합니다.');
    IF Trim(edt_BLNO.Text) = '' THEN
      ErrMsg.Add('[문서정보] 선하증권 번호를 입력해야합니다.');
    IF Trim(mask_BLDATE.Text) = '' THEN
      ErrMsg.Add('[문서정보] 선하증권 발급일자를 입력해야합니다.');
    IF Trim(edt_CARRIER1.Text) = '' THEN
      ErrMsg.Add('[문서정보] 선박명을 입력해야합니다.');
    IF Trim(edt_CARRIER2.Text) = '' THEN
      ErrMsg.Add('[문서정보] 항해번호를 입력해야합니다.');
    IF Trim(mask_ARDATE.Text) = '' THEN
      ErrMsg.Add('[문서정보] 도착(예정)일을 입력해야합니다.');
    IF (Trim(edt_CRNAME1.Text) = '') and (Trim(edt_CRNAME2.Text) = '') and (Trim(edt_CRNAME3.Text) = '') THEN
      ErrMsg.Add('[문서정보] 선박회사명을 입력해야합니다.');
    IF Trim(edt_LOADLOC.Text) = '' THEN
      ErrMsg.Add('[문서정보] 선적항을 입력해야합니다.');
    IF Trim(edt_ARRLOC.Text) = '' THEN
      ErrMsg.Add('[문서정보] 도착항을 입력해야합니다.');
    IF (Trim(edt_INVAMTC.Text) = '') and (curr_INVAMT.Value = 0) THEN
      ErrMsg.Add('[문서정보] 상업송장금액과 단위를 입력해야합니다.');
    IF (Trim(edt_BANKCD.Text) = '') and (Trim(edt_BANKTXT.Text) = '') and (Trim(edt_BANKBR.Text) = '') THEN
      ErrMsg.Add('[문서정보] 발급은행을 입력해야합니다.');

    Result := ErrMsg.Text;
  finally
    ErrMsg.Free;
  end;

end;

procedure TUI_APPLOG_frm.EditDocument;
begin
//------------------------------------------------------------------------------
// 프로그램 제어
//------------------------------------------------------------------------------
  ProgramControlType := ctModify;
//------------------------------------------------------------------------------
// 트랜잭션 활성
//------------------------------------------------------------------------------
  IF not DMmssql.KISConnect.InTransaction Then DMMssql.KISConnect.BeginTrans;
//------------------------------------------------------------------------------
// 데이터셋 인서트표현
//------------------------------------------------------------------------------
  qryList.Edit;
//------------------------------------------------------------------------------
// 버튼정리
//------------------------------------------------------------------------------
  ButtonEnable(False);
//------------------------------------------------------------------------------
// 접근제어
//------------------------------------------------------------------------------
  sPageControl1.ActivePageIndex := 0;
  sDBGrid1.Enabled := False;
  sDBGrid2.Enabled := False;
//------------------------------------------------------------------------------
//  데이터제어
//------------------------------------------------------------------------------
    EnabledControlValue(sPanel4 , False);
    EnabledControlValue(sPanel2 , False);
    EnabledControlValue(sPanel17 , False);
    EnabledControlValue(sPanel53);
    EnabledControlValue(sPanel6);

end;

procedure TUI_APPLOG_frm.btnEditClick(Sender: TObject);
begin
  inherited;
  if (not qryList.Active) or (qryList.RecordCount = 0) then Exit;
  if AnsiMatchText(qryListCHK2.AsString,['','0','1','2','5','6']) then
  begin
    EditDocument;
  end
  else
    MessageBox(Self.Handle,MSG_SYSTEM_NOT_EDIT,'수정불가', MB_OK+MB_ICONINFORMATION);
end;

procedure TUI_APPLOG_frm.DeleteDocumnet;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;
  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
    try
      try
        IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;

          SQL.Text :=  'DELETE FROM APPLOG WHERE MAINT_NO =' + QuotedStr(maint_no);
          ExecSQL;

          //트랜잭션 커밋
          DMMssql.KISConnect.CommitTrans;

          qryList.Close;
          qryList.Open;

          if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
          begin
            qryList.MoveBy(nCursor-1);
          end
          else
            qryList.First;
        end
        else
        begin
          if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;
    finally
     Close;
     Free;
    end;
  end; 
end;

procedure TUI_APPLOG_frm.btnDelClick(Sender: TObject);
begin
  inherited;
    if (not qryList.Active) or (qryList.RecordCount = 0) then Exit;
    DeleteDocumnet;
    //삭제후
    if qryList.RecordCount = 0 then
    begin
      ClearControlValue(sPanel4);
      ClearControlValue(sPanel2);
      ClearControlValue(sPanel17);
    end;
end;

procedure TUI_APPLOG_frm.btnCopyClick(Sender: TObject);
var
  copyDocNo , newDocNo : String;
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;

  Dialog_CopyAPPLOG_frm := TDialog_CopyAPPLOG_frm.Create(Self);
  try
    newDocNo := DMAutoNo.GetDocumentNoAutoInc('APPLOG');
    copyDocNo :=Dialog_CopyAPPLOG_frm.openDialog;
    //ShowMessage(copyDocNo);

    IF Trim(copyDocNo) = '' then Exit;
    //------------------------------------------------------------------------------
    // COPY
    //------------------------------------------------------------------------------
    with spCopyAPPLOG do
    begin
      Close;
      Parameters.ParamByName('@CopyDocNo').Value := copyDocNo;
      Parameters.ParamByName('@NewDocNo').Value :=  newDocNo;
      Parameters.ParamByName('@USER_ID').Value := LoginData.sID;

      if not DMMssql.KISConnect.InTransaction then
        DMMssql.KISConnect.BeginTrans;

      try
        ExecProc;
      except
        on e:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PChar(MSG_SYSTEM_COPY_ERR+#13#10+e.Message),'복사오류',MB_OK+MB_ICONERROR);
        end;
      end;

    end;
    // ReadList(해당년도 1월1일 , 오늘날짜 , 새로복사 된  관리번호)
    if  ReadList(FormatDateTime('YYYYMMDD', StartOfTheYear(Now)),FormatDateTime('YYYYMMDD',Now),newDocNo) Then
    begin
      EditDocument;
      edt_MAINT_NO.Text := newDocNo;  
    end
    else
      raise Exception.Create('복사한 데이터를 찾을수 없습니다');

  finally
    FreeAndNil(Dialog_CopyAPPLOG_frm);
  end;
end;

procedure TUI_APPLOG_frm.btnPrintClick(Sender: TObject);
begin
  inherited;
  if (not qryList.Active) or (qryList.RecordCount = 0 ) then
    Exit;

  APPLOG_PRINT_frm := TAPPLOG_PRINT_frm.Create(Self);
  try
    APPLOG_PRINT_frm.MaintNo := edt_MAINT_NO.Text;
    APPLOG_PRINT_frm.Preview;
  finally
    FreeAndNil(APPLOG_PRINT_frm);
  end;
end;

procedure TUI_APPLOG_frm.ReadyDocument(DocNo: String);
var
  RecvSelect: TDlg_RecvSelect_frm;
  RecvCode, RecvDoc, RecvFlat: string;
  ReadyDateTime: TDateTime;
//  DocBookMark: Pointer;
begin
  inherited;

  RecvSelect := TDlg_RecvSelect_frm.Create(Self);
  try
    if RecvSelect.ShowModal = mrOK then
    begin
      //문서 준비 시작
      RecvCode := RecvSelect.qryList.FieldByName('CODE').AsString;
      RecvDoc := qryListMAINT_NO.AsString;
      RecvFlat := APPLOG(RecvDoc, RecvCode);
      ReadyDateTime := Now;
      with qryReady do
      begin
        Parameters.ParamByName('DOCID').Value := 'APPLOG';
        Parameters.ParamByName('MAINT_NO').Value := RecvDoc;
        Parameters.ParamByName('MESQ').Value := 0;
        Parameters.ParamByName('SRDATE').Value := FormatDateTime('YYYYMMDD', ReadyDateTime);
        Parameters.ParamByName('SRTIME').Value := FormatDateTime('HHNNSS', ReadyDateTime);
        Parameters.ParamByName('SRVENDOR').Value := RecvCode;

        if StringReplace(qryListCHK3.AsString, ' ', '', [rfReplaceAll]) = '' then
          Parameters.ParamByName('SRSTATE').Value := '신규준비'
        else
          Parameters.ParamByName('SRSTATE').Value := '재준비';

        Parameters.ParamByName('SRFLAT').Value := RecvFlat;
        Parameters.ParamByName('Tag').Value := 0;
        Parameters.ParamByName('TableName').Value := 'APPLOG';
        Parameters.ParamByName('ReadyUser').Value := LoginData.sID;
        Parameters.ParamByName('ReadyDept').Value := '';

        Parameters.ParamByName('CHK3').Value := FormatDateTime('YYYYMMDD', Now) + IntToStr(1);

        ExecSQL;

        Readlist(Mask_SearchDate1.Text , Mask_SearchDate2.Text,RecvDoc);

        IF Assigned( DocumentSend_frm ) Then
        begin
          DocumentSend_frm.FormActivate(nil);
        end;
      end;
    end;
  finally
    FreeAndNil(RecvSelect);
  end;
end;

procedure TUI_APPLOG_frm.btnReadyClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount > 0 then
  begin
    IF AnsiMatchText( qryListCHK2.AsString , ['','1','5','6','8'] ) Then
      ReadyDocument(qryListMAINT_NO.AsString)
    else
      MessageBox(Self.Handle,MSG_SYSTEM_NOT_SEND,'준비실패',MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TUI_APPLOG_frm.btnSendClick(Sender: TObject);
begin
  inherited;
  IF not Assigned(DocumentSend_frm) Then DocumentSend_frm := TDocumentSend_frm.Create(Application)
  else
  begin
    DocumentSend_frm.Show;
    DocumentSend_frm.BringToFront;
  end;
end;

procedure TUI_APPLOG_frm.qryListCHK3GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
var                                                         
  nIndex : integer;  
begin
  inherited;    
  IF Length(qryListCHK3.AsString) = 9 Then
  begin
    nIndex := StrToIntDef( RightStr(qryListCHK3.AsString,1) , -1 );
    case nIndex of
      1: Text := '송신준비';
      2: Text := '준비삭제';
      3: Text := '변환오류';
      4: Text := '통신오류';
      5: Text := '송신완료';
      6: Text := '접수완료';
      9: Text := '미확인오류';
    else
        Text := '';
    end;
  end
  else
  begin
    Text := '';
  end;  
end;

procedure TUI_APPLOG_frm.FormActivate(Sender: TObject);
var
  KeyValue : String;
begin
  inherited;
  if not qryList.Active then Exit;

  if not DMMssql.KISConnect.InTransaction then
  begin
    KeyValue := qryListMAINT_NO.AsString;
    Readlist(Mask_fromDate.Text, Mask_toDate.Text, '');
    qryList.Locate('MAINT_NO',KeyValue,[]);
  end;
end;

procedure TUI_APPLOG_frm.DialogKey(var msg: TCMDialogKey);
begin
   if ActiveControl = nil then Exit;

   if (ActiveControl.Name = 'edt_ARRTXT') and (msg.CharCode in [VK_TAB,VK_RETURN]) then
   begin
     sPageControl1.ActivePageIndex := 1;
     Self.KeyPreview := False;
   end
   else
   begin
     Self.KeyPreview := True;
     inherited;
   end;
end;

end.
