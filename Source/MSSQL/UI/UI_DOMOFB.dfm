inherited UI_DOMOFB_frm: TUI_DOMOFB_frm
  Left = 270
  Top = 156
  Caption = '('#44060#49444#51088')'#44397#45236#48156#54665#47932#54408#47588#46020#54869#50557#49436'('#49688#49888')'
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sSplitter1: TsSplitter [0]
    Left = 0
    Top = 76
    Width = 1122
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object sSplitter3: TsSplitter [1]
    Left = 0
    Top = 41
    Width = 1122
    Height = 3
    Cursor = crVSplit
    Align = alTop
    AutoSnap = False
    Enabled = False
    SkinData.SkinSection = 'SPLITTER'
  end
  object maintno_Panel: TsPanel [2]
    Left = 0
    Top = 44
    Width = 1122
    Height = 32
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object edt_MaintNo: TsEdit
      Left = 64
      Top = 5
      Width = 121
      Height = 23
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #44288#47532#48264#54840
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sMaskEdit1: TsMaskEdit
      Left = 254
      Top = 5
      Width = 83
      Height = 25
      Color = clWhite
      EditMask = '9999-99-99;0'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 1
      Text = '20161122'
      CheckOnExit = True
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #46321#47197#51068#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
      SkinData.SkinSection = 'EDIT'
    end
    object edt_UserNo: TsEdit
      Left = 393
      Top = 5
      Width = 57
      Height = 23
      TabStop = False
      Color = clBtnFace
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #49324#50857#51088
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object sCheckBox1: TsCheckBox
      Left = 944
      Top = 6
      Width = 64
      Height = 16
      TabStop = False
      Caption = #46356#48260#44536
      TabOrder = 5
      ImgChecked = 0
      ImgUnchecked = 0
      SkinData.SkinSection = 'CHECKBOX'
    end
    object edt_msg1: TsEdit
      Tag = 1
      Left = 521
      Top = 5
      Width = 32
      Height = 23
      Hint = #44592#45733#54364#49884
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47928#49436#44592#45733
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
    object edt_msg2: TsEdit
      Tag = 2
      Left = 593
      Top = 5
      Width = 32
      Height = 23
      Hint = #51025#45813#50976#54805
      Color = clWhite
      Ctl3D = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #50976#54805
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #47569#51008' '#44256#46357
      BoundLabel.Font.Style = []
    end
  end
  object sPanel5: TsPanel [3]
    Left = 0
    Top = 0
    Width = 1122
    Height = 41
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton2: TsSpeedButton
      Left = 196
      Top = 3
      Width = 8
      Height = 35
      Caption = 'a'
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sSpeedButton5: TsSpeedButton
      Left = 485
      Top = 3
      Width = 8
      Height = 35
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'SPEEDBUTTON'
    end
    object sLabel7: TsLabel
      Left = 8
      Top = 5
      Width = 184
      Height = 17
      Caption = #44397#45236#48156#54665#47932#54408' '#47588#46020#54869#50557#49436'('#49688#49888')'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object sLabel6: TsLabel
      Left = 8
      Top = 20
      Width = 50
      Height = 13
      Caption = 'DOMOFB'
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object btnExit: TsButton
      Left = 1039
      Top = 2
      Width = 75
      Height = 37
      Cursor = crHandPoint
      Caption = #45803#44592
      TabOrder = 0
      TabStop = False
      OnClick = btnExitClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 20
      ContentMargin = 12
    end
    object btnDel: TsButton
      Left = 212
      Top = 2
      Width = 67
      Height = 37
      Cursor = crHandPoint
      Hint = #47928#49436#47484' '#49325#51228#54633#45768#45796'('#44536#47532#46300#50640#49436' '#49440#53469' '#54980' DELETE)'
      Caption = #49325#51228
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      TabStop = False
      OnClick = btnDelClick
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 27
      ContentMargin = 8
    end
    object sButton2: TsButton
      Tag = 1
      Left = 378
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48120#47532#48372#44592
      TabOrder = 2
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 15
      ContentMargin = 8
    end
    object sButton4: TsButton
      Left = 279
      Top = 2
      Width = 99
      Height = 37
      Cursor = crHandPoint
      Caption = #48148#47196#52636#47141
      TabOrder = 3
      TabStop = False
      OnClick = sButton2Click
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
      Images = DMICON.System26
      ImageIndex = 21
      ContentMargin = 8
    end
  end
  object Page_control: TsPageControl [4]
    Left = 0
    Top = 79
    Width = 1122
    Height = 602
    ActivePage = sTabSheet6
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #47569#51008' '#44256#46357
    Font.Style = []
    ParentFont = False
    TabHeight = 30
    TabIndex = 3
    TabOrder = 2
    TabStop = False
    TabWidth = 120
    OnChange = Page_controlChange
    SkinData.SkinSection = 'PAGECONTROL'
    object sTabSheet1: TsTabSheet
      Caption = #47928#49436#44277#53685
      object sSplitter9: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page1_Left: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
      end
      object page1_Right: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          814
          559)
        object sSpeedButton9: TsSpeedButton
          Left = 381
          Top = -6
          Width = 10
          Height = 565
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object edt_SRADDR3: TsEdit
          Left = 80
          Top = 229
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_SRADDR2: TsEdit
          Left = 80
          Top = 207
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_SRADDR1: TsEdit
          Left = 80
          Top = 185
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_SRNAME3: TsEdit
          Left = 80
          Top = 163
          Width = 97
          Height = 21
          Hint = 'APP_NAME1'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_SRNAME2: TsEdit
          Left = 80
          Top = 141
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_SRNAME1: TsEdit
          Left = 80
          Top = 119
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_SRNO: TsEdit
          Left = 80
          Top = 97
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #47924#50669#45824#47532#51216#13#10'    '#46321#47197#48264#54840' '
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_SRCODE: TsEdit
          Tag = 101
          Left = 80
          Top = 75
          Width = 97
          Height = 21
          Hint = 'APP_NAME1'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#54665#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_OFRNO: TsEdit
          Left = 80
          Top = 53
          Width = 270
          Height = 21
          Hint = 'APP_NAME1'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#54665#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_OFRDATE: TsMaskEdit
          Left = 80
          Top = 29
          Width = 73
          Height = 23
          Color = clWhite
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 9
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #48156#54665#51068#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = [fsBold]
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'EDIT'
        end
        object edt_UDCODE: TsEdit
          Tag = 102
          Left = 80
          Top = 308
          Width = 97
          Height = 21
          Hint = 'APP_NAME1'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49688#50836#51088
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_UDNO: TsEdit
          Left = 80
          Top = 330
          Width = 97
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 11
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49324#50629#51088#46321#47197#13#10'          '#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_UDNAME1: TsEdit
          Left = 80
          Top = 352
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49345#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = [fsBold]
        end
        object edt_UDNAME2: TsEdit
          Left = 80
          Top = 374
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_UDNAME3: TsEdit
          Left = 80
          Top = 396
          Width = 97
          Height = 21
          Hint = 'APP_NAME1'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51204#51088#49436#47749
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_UDADDR1: TsEdit
          Left = 80
          Top = 418
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #51452#49548
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_UDADDR2: TsEdit
          Left = 80
          Top = 440
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_UDADDR3: TsEdit
          Left = 80
          Top = 462
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_UDRENO: TsEdit
          Left = 80
          Top = 484
          Width = 270
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 35
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #52280#51312#48264#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -11
          BoundLabel.Font.Name = #44404#47548#52404
          BoundLabel.Font.Style = []
        end
        object edt_OFRSQ1: TsEdit
          Left = 400
          Top = 27
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_OFRSQ2: TsEdit
          Left = 400
          Top = 49
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_OFRSQ3: TsEdit
          Left = 400
          Top = 71
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_OFRSQ4: TsEdit
          Left = 400
          Top = 93
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_OFRSQ5: TsEdit
          Left = 400
          Top = 115
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sPanel23: TsPanel
          Left = 9
          Top = 1
          Width = 368
          Height = 24
          BevelOuter = bvNone
          Caption = #48156#54665#51088
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel6: TsPanel
          Left = 9
          Top = 280
          Width = 368
          Height = 24
          BevelOuter = bvNone
          Caption = #49688#50836#51088
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_HSCODE: TsMaskEdit
          Left = 80
          Top = 506
          Width = 97
          Height = 23
          AutoSize = False
          Color = clWhite
          Ctl3D = False
          EditMask = '0000.00-0000;0;'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 12
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          CheckOnExit = True
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = 'HS'#48512#54840
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clWindowText
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
          SkinData.CustomColor = True
          SkinData.SkinSection = 'EDIT'
        end
        object sPanel7: TsPanel
          Left = 395
          Top = 1
          Width = 408
          Height = 24
          BevelOuter = bvNone
          Caption = #50976#54952#44592#44036
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel8: TsPanel
          Left = 395
          Top = 140
          Width = 408
          Height = 24
          BevelOuter = bvNone
          Caption = #44160#49324#48169#48277
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel10: TsPanel
          Left = 395
          Top = 280
          Width = 408
          Height = 24
          BevelOuter = bvNone
          Caption = #54252#51109#48169#48277
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel11: TsPanel
          Left = 394
          Top = 419
          Width = 408
          Height = 24
          BevelOuter = bvNone
          Caption = #44208#51228#48169#48277
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_TSTINST1: TsEdit
          Left = 400
          Top = 166
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 31
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Caption = #44160#49324#48169#48277
        end
        object edt_TSTINST2: TsEdit
          Left = 400
          Top = 188
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 32
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_TSTINST3: TsEdit
          Left = 400
          Top = 210
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 33
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_TSTINST4: TsEdit
          Left = 400
          Top = 232
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 34
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_TSTINST5: TsEdit
          Left = 400
          Top = 254
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 35
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PCKINST1: TsEdit
          Left = 400
          Top = 306
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 36
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PCKINST2: TsEdit
          Left = 400
          Top = 328
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 37
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PCKINST3: TsEdit
          Left = 400
          Top = 350
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 38
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PCKINST4: TsEdit
          Left = 400
          Top = 372
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 39
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PCKINST5: TsEdit
          Left = 400
          Top = 394
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 40
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PAYETC1: TsEdit
          Left = 400
          Top = 445
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 41
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PAYETC2: TsEdit
          Left = 400
          Top = 467
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 42
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PAYETC3: TsEdit
          Left = 400
          Top = 489
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 43
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PAYETC4: TsEdit
          Left = 400
          Top = 511
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 44
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_PAYETC5: TsEdit
          Left = 400
          Top = 533
          Width = 400
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 45
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
      end
    end
    object sTabSheet3: TsTabSheet
      Caption = #47928#49436#44277#53685'2'
      object sSplitter7: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object page3_Left: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
      end
      object page3_Right: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
        DesignSize = (
          814
          559)
        object sSpeedButton14: TsSpeedButton
          Left = 403
          Top = -6
          Width = 10
          Height = 384
          Anchors = [akLeft, akTop, akBottom]
          ButtonStyle = tbsDivider
          SkinData.SkinSection = 'SPEEDBUTTON'
        end
        object edt_ORGN1N: TsEdit
          Tag = 301
          Left = 26
          Top = 37
          Width = 49
          Height = 21
          Hint = #44397#44032
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 0
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN1LOC: TsEdit
          Left = 76
          Top = 37
          Width = 309
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN2LOC: TsEdit
          Left = 76
          Top = 59
          Width = 309
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN2N: TsEdit
          Tag = 302
          Left = 26
          Top = 59
          Width = 49
          Height = 21
          Hint = #44397#44032
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN3N: TsEdit
          Tag = 303
          Left = 26
          Top = 81
          Width = 49
          Height = 21
          Hint = #44397#44032
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN3LOC: TsEdit
          Left = 76
          Top = 81
          Width = 309
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN4LOC: TsEdit
          Left = 76
          Top = 103
          Width = 309
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN4N: TsEdit
          Tag = 304
          Left = 26
          Top = 103
          Width = 49
          Height = 21
          Hint = #44397#44032
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN5N: TsEdit
          Tag = 305
          Left = 26
          Top = 125
          Width = 49
          Height = 21
          Hint = #44397#44032
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_ORGN5LOC: TsEdit
          Left = 76
          Top = 125
          Width = 309
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sPanel12: TsPanel
          Left = 9
          Top = 1
          Width = 384
          Height = 24
          BevelOuter = bvNone
          Caption = #50896#49328#51648#44397#44032'/'#51648#48169#47749
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 10
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object memo_REMARK1: TsMemo
          Left = 186
          Top = 422
          Width = 505
          Height = 129
          Color = clWhite
          MaxLength = 1050
          ScrollBars = ssVertical
          TabOrder = 11
          SkinData.SkinSection = 'EDIT'
        end
        object edt_TRNSID: TsEdit
          Tag = 401
          Left = 26
          Top = 237
          Width = 49
          Height = 21
          Hint = #50868#49569#49688#45800
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 12
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_TRNSIDNAME: TsEdit
          Left = 76
          Top = 237
          Width = 309
          Height = 21
          Hint = 'APP_CODE'
          Color = clBtnFace
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 13
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sPanel14: TsPanel
          Left = 9
          Top = 201
          Width = 384
          Height = 24
          BevelOuter = bvNone
          Caption = #50868#49569#48169#48277
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 14
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object sPanel13: TsPanel
          Left = 425
          Top = 1
          Width = 384
          Height = 24
          BevelOuter = bvNone
          Caption = #49440#51201#51221#48372
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 15
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_LOADD: TsEdit
          Tag = 402
          Left = 514
          Top = 32
          Width = 49
          Height = 21
          Hint = #44397#44032
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 16
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#44397#44032'/'#49440#51201#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_LOADLOC: TsEdit
          Left = 564
          Top = 32
          Width = 213
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 17
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_LOADTXT1: TsEdit
          Left = 514
          Top = 54
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 18
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #49440#51201#49884#44592
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_LOADTXT2: TsEdit
          Left = 514
          Top = 76
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 19
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_LOADTXT3: TsEdit
          Left = 514
          Top = 98
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 20
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_LOADTXT4: TsEdit
          Left = 514
          Top = 120
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 21
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_LOADTXT5: TsEdit
          Left = 514
          Top = 142
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 22
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sPanel15: TsPanel
          Left = 425
          Top = 201
          Width = 384
          Height = 24
          BevelOuter = bvNone
          Caption = #46020#52265#51221#48372
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 23
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
        object edt_DESTTXT5: TsEdit
          Left = 514
          Top = 350
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 24
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_DESTTXT4: TsEdit
          Left = 514
          Top = 328
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 25
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_DESTTXT3: TsEdit
          Left = 514
          Top = 306
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 26
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_DESTTXT2: TsEdit
          Left = 514
          Top = 284
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 27
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object edt_DESTTXT1: TsEdit
          Left = 514
          Top = 262
          Width = 263
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 28
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #46020#52265#49884#44592
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DEST: TsEdit
          Tag = 403
          Left = 514
          Top = 240
          Width = 49
          Height = 21
          Hint = #44397#44032
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 3
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 29
          SkinData.CustomColor = True
          SkinData.CustomFont = True
          SkinData.SkinSection = 'GROUPBOX'
          BoundLabel.Active = True
          BoundLabel.ParentFont = False
          BoundLabel.Caption = #46020#52265#44397#44032'/'#46020#52265#54637
          BoundLabel.Font.Charset = ANSI_CHARSET
          BoundLabel.Font.Color = clBlack
          BoundLabel.Font.Height = -12
          BoundLabel.Font.Name = #47569#51008' '#44256#46357
          BoundLabel.Font.Style = []
        end
        object edt_DESTLOC: TsEdit
          Left = 564
          Top = 240
          Width = 213
          Height = 21
          Hint = 'APP_CODE'
          Color = clWhite
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 70
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 30
          SkinData.CustomColor = True
          SkinData.SkinSection = 'GROUPBOX'
        end
        object sPanel16: TsPanel
          Left = 9
          Top = 385
          Width = 800
          Height = 24
          BevelOuter = bvNone
          Caption = #44592#53440#52280#51312#49324#54637' ('#52572#45824' 15'#54665' 70'#50676' '#51077#47141' '#44032#45733')'
          Color = 16042877
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 31
          SkinData.CustomColor = True
          SkinData.SkinSection = 'DRAGBAR'
        end
      end
    end
    object sTabSheet5: TsTabSheet
      Caption = #49345#54408#45236#50669
      object sSplitter5: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sPanel3: TsPanel
        Left = 300
        Top = 3
        Width = 814
        Height = 559
        Align = alClient
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object detailDBGrid: TsDBGrid
          Left = 1
          Top = 26
          Width = 812
          Height = 176
          Align = alTop
          Color = clWhite
          DataSource = dsDetail
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clBlack
          TitleFont.Height = -12
          TitleFont.Name = #47569#51008' '#44256#46357
          TitleFont.Style = []
          SkinData.SkinSection = 'EDIT'
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'SEQ'
              Title.Alignment = taCenter
              Title.Caption = #49692#48264
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME_COD'
              Title.Caption = #54408#47749#53076#46300
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'HS_NO'
              Title.Caption = 'HS'#48264#54840
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY_G'
              Title.Caption = #49688#47049#45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTY'
              Title.Caption = #49688#47049
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTYG_G'
              Title.Caption = #45800#44032#44592#51456#49688#47049#45800#50948
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTYG'
              Title.Caption = #45800#44032#44592#51456#49688#47049
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRICE_G'
              Title.Caption = #45800#44032#45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PRICE'
              Title.Caption = #45800#44032
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT_G'
              Title.Caption = #44552#50529#45800#50948
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'AMT'
              Title.Caption = #44552#50529
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STQTY_G'
              Title.Caption = #49548#47049#49548#44228#45800#50948
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STQTY'
              Title.Caption = #49548#47049#49548#44228
              Width = 137
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STAMT_G'
              Title.Caption = #44552#50529#49548#44228#45800#50948
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STAMT'
              Title.Caption = #44552#50529#49548#44228
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NAME1'
              Title.Caption = #54408#47749
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SIZE1'
              Title.Caption = #44508#44201
              Width = 100
              Visible = True
            end>
        end
        object sPanel9: TsPanel
          Left = 1
          Top = 1
          Width = 812
          Height = 25
          Align = alTop
          TabOrder = 1
          SkinData.SkinSection = 'TRANSPARENT'
          object sSpeedButton10: TsSpeedButton
            Left = 1
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailAdd: TsSpeedButton
            Tag = 1
            Left = 6
            Top = 1
            Width = 67
            Height = 23
            Caption = #51077#47141
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 2
          end
          object sSpeedButton13: TsSpeedButton
            Left = 73
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailMod: TsSpeedButton
            Tag = 2
            Left = 78
            Top = 1
            Width = 67
            Height = 23
            Caption = #49688#51221
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 3
          end
          object sSpeedButton11: TsSpeedButton
            Left = 145
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailDel: TsSpeedButton
            Tag = 3
            Left = 150
            Top = 1
            Width = 67
            Height = 23
            Caption = #49325#51228
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 1
          end
          object sSpeedButton15: TsSpeedButton
            Left = 217
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailOk: TsSpeedButton
            Tag = 4
            Left = 222
            Top = 1
            Width = 67
            Height = 23
            Caption = #51200#51109
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 17
          end
          object sSpeedButton12: TsSpeedButton
            Left = 356
            Top = 1
            Width = 5
            Height = 23
            Cursor = crHandPoint
            Layout = blGlyphTop
            Spacing = 0
            Align = alLeft
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'TRANSPARENT'
            Reflected = True
          end
          object Btn_DetailCancel: TsSpeedButton
            Tag = 5
            Left = 289
            Top = 1
            Width = 67
            Height = 23
            Caption = #52712#49548
            Enabled = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #44404#47548#52404
            Font.Style = [fsBold]
            ParentFont = False
            Align = alLeft
            SkinData.CustomFont = True
            SkinData.SkinSection = 'TRANSPARENT'
            Images = DMICON.System18
            ImageIndex = 18
          end
        end
        object sPanel17: TsPanel
          Left = 1
          Top = 202
          Width = 812
          Height = 239
          Align = alTop
          TabOrder = 2
          SkinData.SkinSection = 'PANEL'
          DesignSize = (
            812
            239)
          object sSpeedButton16: TsSpeedButton
            Left = 404
            Top = -59
            Width = 10
            Height = 298
            Anchors = [akLeft, akTop, akBottom]
            ButtonStyle = tbsDivider
            SkinData.SkinSection = 'SPEEDBUTTON'
          end
          object sPanel22: TsPanel
            Left = 4
            Top = 3
            Width = 400
            Height = 24
            BevelOuter = bvNone
            Caption = #49345#54408' '#54408#47749
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 0
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sDBEdit1: TsDBEdit
            Tag = 501
            Left = 37
            Top = 35
            Width = 74
            Height = 23
            Hint = #54408#47749
            Color = clWhite
            Ctl3D = True
            DataField = 'NAME_COD'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #54408#47749
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object sDBEdit2: TsDBEdit
            Left = 189
            Top = 35
            Width = 100
            Height = 23
            Color = clWhite
            Ctl3D = True
            DataField = 'HS_NO'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = 'HS NO'
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sDBMemo1: TsDBMemo
            Left = 37
            Top = 59
            Width = 252
            Height = 86
            Color = clWhite
            Ctl3D = True
            DataField = 'NAME1'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 3
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBMemo2: TsDBMemo
            Left = 37
            Top = 146
            Width = 348
            Height = 86
            Color = clWhite
            Ctl3D = True
            DataField = 'SIZE1'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 4
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44508#44201
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #47569#51008' '#44256#46357
            BoundLabel.Font.Style = []
            BoundLabel.Layout = sclLeftTop
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sPanel24: TsPanel
            Left = 414
            Top = 3
            Width = 394
            Height = 24
            BevelOuter = bvNone
            Caption = #49688#47049'/'#45800#44032'/'#44552#50529
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 5
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
          object sDBEdit3: TsDBEdit
            Tag = 502
            Left = 541
            Top = 37
            Width = 36
            Height = 23
            Hint = #45800#50948
            Color = clWhite
            Ctl3D = True
            DataField = 'QTY_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 6
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sDBEdit4: TsDBEdit
            Left = 578
            Top = 37
            Width = 129
            Height = 23
            Color = clWhite
            Ctl3D = True
            DataField = 'QTY'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 7
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit6: TsDBEdit
            Left = 578
            Top = 61
            Width = 129
            Height = 23
            Color = clWhite
            Ctl3D = True
            DataField = 'PRICE'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 8
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit5: TsDBEdit
            Tag = 503
            Left = 541
            Top = 61
            Width = 36
            Height = 23
            Hint = #53685#54868
            Color = clWhite
            Ctl3D = True
            DataField = 'PRICE_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 9
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sDBEdit7: TsDBEdit
            Tag = 504
            Left = 541
            Top = 85
            Width = 36
            Height = 23
            Hint = #45800#50948
            Color = clWhite
            Ctl3D = True
            DataField = 'QTYG_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 10
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #45800#44032#44592#51456#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sDBEdit8: TsDBEdit
            Left = 578
            Top = 85
            Width = 129
            Height = 23
            Color = clWhite
            Ctl3D = True
            DataField = 'QTYG'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 11
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit9: TsDBEdit
            Tag = 505
            Left = 541
            Top = 109
            Width = 36
            Height = 23
            Hint = #53685#54868
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 12
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sDBEdit10: TsDBEdit
            Left = 578
            Top = 109
            Width = 129
            Height = 23
            Color = clWhite
            Ctl3D = True
            DataField = 'AMT'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 13
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit12: TsDBEdit
            Left = 578
            Top = 133
            Width = 129
            Height = 23
            Color = clWhite
            Ctl3D = True
            DataField = 'STQTY'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 14
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit14: TsDBEdit
            Left = 578
            Top = 157
            Width = 129
            Height = 23
            Color = clWhite
            Ctl3D = True
            DataField = 'STAMT'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            MaxLength = 10
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 15
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
          end
          object sDBEdit13: TsDBEdit
            Tag = 507
            Left = 541
            Top = 157
            Width = 36
            Height = 23
            Hint = #53685#54868
            Color = clWhite
            Ctl3D = True
            DataField = 'STAMT_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 16
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #44552#50529#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
          object sDBEdit11: TsDBEdit
            Tag = 506
            Left = 541
            Top = 133
            Width = 36
            Height = 23
            Hint = #45800#50948
            Color = clWhite
            Ctl3D = True
            DataField = 'STQTY_G'
            DataSource = dsDetail
            Font.Charset = ANSI_CHARSET
            Font.Color = 4013373
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            ImeName = 'Microsoft IME 2010'
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 17
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #49548#47049#49548#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = []
          end
        end
        object sPanel18: TsPanel
          Left = 1
          Top = 441
          Width = 812
          Height = 117
          Align = alClient
          TabOrder = 3
          SkinData.SkinSection = 'PANEL'
          object edt_TQTYCUR: TsEdit
            Tag = 508
            Left = 309
            Top = 42
            Width = 36
            Height = 21
            Hint = #45800#50948
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#49688#47049
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_TAMTCUR: TsEdit
            Tag = 509
            Left = 309
            Top = 65
            Width = 36
            Height = 21
            Hint = #53685#54868
            Color = clWhite
            Ctl3D = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = #47569#51008' '#44256#46357
            Font.Style = []
            MaxLength = 3
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            SkinData.CustomColor = True
            SkinData.SkinSection = 'GROUPBOX'
            BoundLabel.Active = True
            BoundLabel.ParentFont = False
            BoundLabel.Caption = #52509#54633#44228
            BoundLabel.Font.Charset = ANSI_CHARSET
            BoundLabel.Font.Color = clBlack
            BoundLabel.Font.Height = -12
            BoundLabel.Font.Name = #44404#47548#52404
            BoundLabel.Font.Style = [fsBold]
          end
          object edt_TQTY: TsCurrencyEdit
            Left = 346
            Top = 42
            Width = 207
            Height = 21
            AutoSize = False
            Color = clWhite
            TabOrder = 2
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Grayed = False
            GlyphMode.Blend = 0
            DisplayFormat = '###,###,###.0000;-###,###,###.0000;0'
          end
          object edt_TAMT: TsCurrencyEdit
            Left = 346
            Top = 65
            Width = 207
            Height = 21
            AutoSize = False
            Color = clWhite
            TabOrder = 3
            SkinData.SkinSection = 'EDIT'
            GlyphMode.Grayed = False
            GlyphMode.Blend = 0
            DisplayFormat = '###,###,###.0000;-###,###,###.0000;0'
          end
          object sPanel32: TsPanel
            Left = 1
            Top = 1
            Width = 810
            Height = 24
            Align = alTop
            BevelOuter = bvNone
            Caption = #52509' '#54633#44228
            Color = 16042877
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 4
            SkinData.CustomColor = True
            SkinData.SkinSection = 'DRAGBAR'
          end
        end
      end
      object sPanel4: TsPanel
        Left = 0
        Top = 3
        Width = 300
        Height = 559
        Align = alLeft
        TabOrder = 1
        SkinData.SkinSection = 'PANEL'
      end
    end
    object sTabSheet6: TsTabSheet
      Caption = #45936#51060#53552#51312#54924
      object sSplitter4: TsSplitter
        Left = 0
        Top = 0
        Width = 1114
        Height = 3
        Cursor = crVSplit
        Align = alTop
        AutoSnap = False
        Enabled = False
        SkinData.SkinSection = 'SPLITTER'
      end
      object sDBGrid1: TsDBGrid
        Left = 0
        Top = 35
        Width = 1114
        Height = 527
        TabStop = False
        Align = alClient
        Color = clWhite
        DataSource = dsList
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        ParentFont = False
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -12
        TitleFont.Name = #47569#51008' '#44256#46357
        TitleFont.Style = []
        OnDrawColumnCell = sDBGrid2DrawColumnCell
        SkinData.SkinSection = 'EDIT'
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'DATEE'
            Title.Alignment = taCenter
            Title.Caption = #46321#47197#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MAINT_NO'
            Title.Alignment = taCenter
            Title.Caption = #44288#47532#48264#54840
            Width = 200
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'OFR_SQ1'
            Title.Alignment = taCenter
            Title.Caption = #50976#54952#44592#44036
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'OFR_DATE'
            Title.Alignment = taCenter
            Title.Caption = #48156#54665#51068#51088
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'OFR_NO'
            Title.Alignment = taCenter
            Title.Caption = #48156#54665#48264#54840
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UD_NAME1'
            Title.Alignment = taCenter
            Title.Caption = #49688#50836#51088
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TQTY'
            Title.Alignment = taCenter
            Title.Caption = #52509#49688#47049
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TQTYCUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TAMT'
            Title.Alignment = taCenter
            Title.Caption = #52509#44552#50529
            Width = 100
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'TAMTCUR'
            Title.Alignment = taCenter
            Title.Caption = #45800#50948
            Width = 50
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'USER_ID'
            Title.Alignment = taCenter
            Title.Caption = #51089#49457#51088
            Width = 50
            Visible = True
          end>
      end
      object sPanel1: TsPanel
        Left = 0
        Top = 3
        Width = 1114
        Height = 32
        Align = alTop
        TabOrder = 0
        SkinData.SkinSection = 'PANEL'
        object edt_SearchText: TsEdit
          Left = 86
          Top = 5
          Width = 233
          Height = 25
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Visible = False
          SkinData.SkinSection = 'EDIT'
        end
        object com_SearchKeyword: TsComboBox
          Left = 4
          Top = 5
          Width = 81
          Height = 23
          Alignment = taLeftJustify
          SkinData.SkinSection = 'COMBOBOX'
          VerticalAlignment = taAlignTop
          Style = csDropDownList
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          ItemHeight = 17
          ItemIndex = 0
          ParentFont = False
          TabOrder = 4
          TabStop = False
          Text = #46321#47197#51068#51088
          OnSelect = com_SearchKeywordSelect
          Items.Strings = (
            #46321#47197#51068#51088
            #44288#47532#48264#54840
            #49688#54812#51088)
        end
        object Mask_SearchDate1: TsMaskEdit
          Left = 86
          Top = 5
          Width = 82
          Height = 25
          Color = clWhite
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 0
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn2: TsBitBtn
          Left = 321
          Top = 5
          Width = 70
          Height = 23
          Cursor = crHandPoint
          Caption = #44160#49353
          TabOrder = 3
          OnClick = sBitBtn2Click
          ImageIndex = 6
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sBitBtn3: TsBitBtn
          Tag = 902
          Left = 167
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 5
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object Mask_SearchDate2: TsMaskEdit
          Left = 214
          Top = 5
          Width = 82
          Height = 25
          Color = clWhite
          EditMask = '9999-99-99;0'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = #47569#51008' '#44256#46357
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 1
          Text = '20161125'
          OnDblClick = Mask_SearchDate1DblClick
          CheckOnExit = True
          SkinData.SkinSection = 'EDIT'
        end
        object sBitBtn4: TsBitBtn
          Tag = 903
          Left = 295
          Top = 5
          Width = 24
          Height = 23
          Cursor = crHandPoint
          TabOrder = 6
          TabStop = False
          OnClick = btn_CalClick
          ImageIndex = 9
          Images = DMICON.System18
          SkinData.SkinSection = 'BUTTON'
        end
        object sPanel2: TsPanel
          Left = 190
          Top = 5
          Width = 25
          Height = 23
          Caption = '~'
          TabOrder = 7
          SkinData.SkinSection = 'PANEL'
        end
      end
    end
  end
  object LeftGrid_Panel: TsPanel [5]
    Left = 4
    Top = 119
    Width = 300
    Height = 558
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    object sDBGrid2: TsDBGrid
      Left = 1
      Top = 34
      Width = 298
      Height = 523
      TabStop = False
      Align = alClient
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = #47569#51008' '#44256#46357
      TitleFont.Style = []
      OnDrawColumnCell = sDBGrid2DrawColumnCell
      SkinData.SkinSection = 'EDIT'
      Columns = <
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'DATEE'
          Title.Alignment = taCenter
          Title.Caption = #46321#47197#51068#51088
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MAINT_NO'
          Title.Alignment = taCenter
          Title.Caption = #44288#47532#48264#54840
          Width = 204
          Visible = True
        end>
    end
    object sPanel53: TsPanel
      Left = 1
      Top = 1
      Width = 298
      Height = 33
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      SkinData.SkinSection = 'PANEL'
      object Mask_fromDate: TsMaskEdit
        Left = 121
        Top = 5
        Width = 74
        Height = 23
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #46321#47197#51068#51088
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = []
        SkinData.SkinSection = 'EDIT'
      end
      object sBitBtn22: TsBitBtn
        Left = 269
        Top = 5
        Width = 25
        Height = 23
        Cursor = crHandPoint
        TabOrder = 2
        OnClick = sBitBtn22Click
        ImageIndex = 6
        Images = DMICON.System18
        SkinData.SkinSection = 'BUTTON'
      end
      object Mask_toDate: TsMaskEdit
        Left = 196
        Top = 5
        Width = 74
        Height = 23
        Color = clWhite
        Ctl3D = False
        EditMask = '9999-99-99;0'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = #47569#51008' '#44256#46357
        Font.Style = []
        MaxLength = 10
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        Text = '20161122'
        CheckOnExit = True
        BoundLabel.Active = True
        BoundLabel.ParentFont = False
        BoundLabel.Caption = #51312#54924
        BoundLabel.Font.Charset = ANSI_CHARSET
        BoundLabel.Font.Color = clWindowText
        BoundLabel.Font.Height = -12
        BoundLabel.Font.Name = #47569#51008' '#44256#46357
        BoundLabel.Font.Style = [fsBold]
        SkinData.SkinSection = 'EDIT'
      end
    end
  end
  inherited sSkinProvider1: TsSkinProvider
    Left = 16
    Top = 192
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    AfterOpen = qryListAfterOpen
    AfterScroll = qryListAfterScroll
    Parameters = <>
    SQL.Strings = (
      ''
      'SELECT *  ,'
      '      TRNS_NAME.DOC_NAME as TRANSNME,'
      '      LOADD_NAME.DOC_NAME as LOADDNAME,'
      '      DEST_NAME.DOC_NAME as DESTNAME'
      #9#9'  '#9#9#9#9'   '
      ' FROM DOMOFB_H1 AS H1'
      ''
      'INNER JOIN DOMOFB_H2 AS H2 ON H1.MAINT_NO = H2.MAINT_NO '
      'INNER JOIN DOMOFB_H3 AS H3 ON H1.MAINT_NO = H3.MAINT_NO'
      ''
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#50868#49569#49688#45800#39') TRNS_NAME ON H3.TRNS_ID = TRNS_NAME.CO' +
        'DE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') LOADD_NAME ON H3.LOADD = LOADD_NAME.CODE'
      
        'LEFT JOIN (SELECT CODE,NAME as DOC_NAME FROM CODE2NDD with(noloc' +
        'k) WHERE Prefix = '#39#44397#44032#39') DEST_NAME ON H3.DEST = DEST_NAME.CODE')
    Left = 48
    Top = 192
    object qryListMAINT_NO: TStringField
      FieldName = 'MAINT_NO'
      Size = 35
    end
    object qryListMaint_Rff: TStringField
      FieldName = 'Maint_Rff'
      Size = 35
    end
    object qryListChk1: TStringField
      FieldName = 'Chk1'
      Size = 1
    end
    object qryListChk2: TStringField
      FieldName = 'Chk2'
      OnGetText = qryListChk2GetText
      Size = 1
    end
    object qryListChk3: TStringField
      FieldName = 'Chk3'
      Size = 10
    end
    object qryListUSER_ID: TStringField
      FieldName = 'USER_ID'
      Size = 10
    end
    object qryListMESSAGE1: TStringField
      FieldName = 'MESSAGE1'
      Size = 3
    end
    object qryListMESSAGE2: TStringField
      FieldName = 'MESSAGE2'
      Size = 3
    end
    object qryListDATEE: TStringField
      FieldName = 'DATEE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListSR_CODE: TStringField
      FieldName = 'SR_CODE'
      Size = 10
    end
    object qryListSR_NO: TStringField
      FieldName = 'SR_NO'
      Size = 17
    end
    object qryListSR_NAME1: TStringField
      FieldName = 'SR_NAME1'
      Size = 35
    end
    object qryListSR_NAME2: TStringField
      FieldName = 'SR_NAME2'
      Size = 35
    end
    object qryListSR_NAME3: TStringField
      FieldName = 'SR_NAME3'
      Size = 10
    end
    object qryListSR_ADDR1: TStringField
      FieldName = 'SR_ADDR1'
      Size = 35
    end
    object qryListSR_ADDR2: TStringField
      FieldName = 'SR_ADDR2'
      Size = 35
    end
    object qryListSR_ADDR3: TStringField
      FieldName = 'SR_ADDR3'
      Size = 35
    end
    object qryListUD_CODE: TStringField
      FieldName = 'UD_CODE'
      Size = 10
    end
    object qryListUD_RENO: TStringField
      FieldName = 'UD_RENO'
      Size = 35
    end
    object qryListUD_NO: TStringField
      FieldName = 'UD_NO'
      Size = 10
    end
    object qryListUD_NAME1: TStringField
      FieldName = 'UD_NAME1'
      Size = 35
    end
    object qryListUD_NAME2: TStringField
      FieldName = 'UD_NAME2'
      Size = 35
    end
    object qryListUD_NAME3: TStringField
      FieldName = 'UD_NAME3'
      Size = 10
    end
    object qryListUD_ADDR1: TStringField
      FieldName = 'UD_ADDR1'
      Size = 35
    end
    object qryListUD_ADDR2: TStringField
      FieldName = 'UD_ADDR2'
      Size = 35
    end
    object qryListUD_ADDR3: TStringField
      FieldName = 'UD_ADDR3'
      Size = 35
    end
    object qryListOFR_NO: TStringField
      FieldName = 'OFR_NO'
      Size = 35
    end
    object qryListOFR_DATE: TStringField
      FieldName = 'OFR_DATE'
      EditMask = '9999-99-99;0'
      Size = 8
    end
    object qryListOFR_SQ: TStringField
      FieldName = 'OFR_SQ'
      Size = 1
    end
    object qryListOFR_SQ1: TStringField
      FieldName = 'OFR_SQ1'
      Size = 70
    end
    object qryListOFR_SQ2: TStringField
      FieldName = 'OFR_SQ2'
      Size = 70
    end
    object qryListOFR_SQ3: TStringField
      FieldName = 'OFR_SQ3'
      Size = 70
    end
    object qryListOFR_SQ4: TStringField
      FieldName = 'OFR_SQ4'
      Size = 70
    end
    object qryListOFR_SQ5: TStringField
      FieldName = 'OFR_SQ5'
      Size = 70
    end
    object qryListREMARK: TStringField
      FieldName = 'REMARK'
      Size = 1
    end
    object qryListREMARK_1: TMemoField
      FieldName = 'REMARK_1'
      BlobType = ftMemo
    end
    object qryListPRNO: TIntegerField
      FieldName = 'PRNO'
    end
    object qryListMAINT_NO_1: TStringField
      FieldName = 'MAINT_NO_1'
      Size = 35
    end
    object qryListTSTINST: TStringField
      FieldName = 'TSTINST'
      Size = 1
    end
    object qryListTSTINST1: TStringField
      FieldName = 'TSTINST1'
      Size = 70
    end
    object qryListTSTINST2: TStringField
      FieldName = 'TSTINST2'
      Size = 70
    end
    object qryListTSTINST3: TStringField
      FieldName = 'TSTINST3'
      Size = 70
    end
    object qryListTSTINST4: TStringField
      FieldName = 'TSTINST4'
      Size = 70
    end
    object qryListTSTINST5: TStringField
      FieldName = 'TSTINST5'
      Size = 70
    end
    object qryListPCKINST: TStringField
      FieldName = 'PCKINST'
      Size = 1
    end
    object qryListPCKINST1: TStringField
      FieldName = 'PCKINST1'
      Size = 70
    end
    object qryListPCKINST2: TStringField
      FieldName = 'PCKINST2'
      Size = 70
    end
    object qryListPCKINST3: TStringField
      FieldName = 'PCKINST3'
      Size = 70
    end
    object qryListPCKINST4: TStringField
      FieldName = 'PCKINST4'
      Size = 70
    end
    object qryListPCKINST5: TStringField
      FieldName = 'PCKINST5'
      Size = 70
    end
    object qryListPAY: TStringField
      FieldName = 'PAY'
      Size = 3
    end
    object qryListPAY_ETC: TStringField
      FieldName = 'PAY_ETC'
      Size = 1
    end
    object qryListPAY_ETC1: TStringField
      FieldName = 'PAY_ETC1'
      Size = 70
    end
    object qryListPAY_ETC2: TStringField
      FieldName = 'PAY_ETC2'
      Size = 70
    end
    object qryListPAY_ETC3: TStringField
      FieldName = 'PAY_ETC3'
      Size = 70
    end
    object qryListPAY_ETC4: TStringField
      FieldName = 'PAY_ETC4'
      Size = 70
    end
    object qryListPAY_ETC5: TStringField
      FieldName = 'PAY_ETC5'
      Size = 70
    end
    object qryListMAINT_NO_2: TStringField
      FieldName = 'MAINT_NO_2'
      Size = 35
    end
    object qryListTERM_DEL: TStringField
      FieldName = 'TERM_DEL'
      Size = 3
    end
    object qryListTERM_NAT: TStringField
      FieldName = 'TERM_NAT'
      Size = 3
    end
    object qryListTERM_LOC: TStringField
      FieldName = 'TERM_LOC'
      Size = 70
    end
    object qryListORGN1N: TStringField
      FieldName = 'ORGN1N'
      Size = 3
    end
    object qryListORGN1LOC: TStringField
      FieldName = 'ORGN1LOC'
      Size = 70
    end
    object qryListORGN2N: TStringField
      FieldName = 'ORGN2N'
      Size = 3
    end
    object qryListORGN2LOC: TStringField
      FieldName = 'ORGN2LOC'
      Size = 70
    end
    object qryListORGN3N: TStringField
      FieldName = 'ORGN3N'
      Size = 3
    end
    object qryListORGN3LOC: TStringField
      FieldName = 'ORGN3LOC'
      Size = 70
    end
    object qryListORGN4N: TStringField
      FieldName = 'ORGN4N'
      Size = 3
    end
    object qryListORGN4LOC: TStringField
      FieldName = 'ORGN4LOC'
      Size = 70
    end
    object qryListORGN5N: TStringField
      FieldName = 'ORGN5N'
      Size = 3
    end
    object qryListORGN5LOC: TStringField
      FieldName = 'ORGN5LOC'
      Size = 70
    end
    object qryListTRNS_ID: TStringField
      FieldName = 'TRNS_ID'
      Size = 3
    end
    object qryListLOADD: TStringField
      FieldName = 'LOADD'
      Size = 3
    end
    object qryListLOADLOC: TStringField
      FieldName = 'LOADLOC'
      Size = 70
    end
    object qryListLOADTXT: TStringField
      FieldName = 'LOADTXT'
      Size = 1
    end
    object qryListLOADTXT1: TStringField
      FieldName = 'LOADTXT1'
      Size = 70
    end
    object qryListLOADTXT2: TStringField
      FieldName = 'LOADTXT2'
      Size = 70
    end
    object qryListLOADTXT3: TStringField
      FieldName = 'LOADTXT3'
      Size = 70
    end
    object qryListLOADTXT4: TStringField
      FieldName = 'LOADTXT4'
      Size = 70
    end
    object qryListLOADTXT5: TStringField
      FieldName = 'LOADTXT5'
      Size = 70
    end
    object qryListDEST: TStringField
      FieldName = 'DEST'
      Size = 3
    end
    object qryListDESTLOC: TStringField
      FieldName = 'DESTLOC'
      Size = 70
    end
    object qryListDESTTXT: TStringField
      FieldName = 'DESTTXT'
      Size = 1
    end
    object qryListDESTTXT1: TStringField
      FieldName = 'DESTTXT1'
      Size = 70
    end
    object qryListDESTTXT2: TStringField
      FieldName = 'DESTTXT2'
      Size = 70
    end
    object qryListDESTTXT3: TStringField
      FieldName = 'DESTTXT3'
      Size = 70
    end
    object qryListDESTTXT4: TStringField
      FieldName = 'DESTTXT4'
      Size = 70
    end
    object qryListDESTTXT5: TStringField
      FieldName = 'DESTTXT5'
      Size = 70
    end
    object qryListTQTY: TBCDField
      FieldName = 'TQTY'
      DisplayFormat = '#,##0.####;'
      EditFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListTQTYCUR: TStringField
      FieldName = 'TQTYCUR'
      Size = 3
    end
    object qryListTAMT: TBCDField
      FieldName = 'TAMT'
      DisplayFormat = '#,##0.####;'
      Precision = 18
    end
    object qryListTAMTCUR: TStringField
      FieldName = 'TAMTCUR'
      Size = 3
    end
    object qryListCODE: TStringField
      FieldName = 'CODE'
      Size = 12
    end
    object qryListDOC_NAME: TStringField
      FieldName = 'DOC_NAME'
      Size = 100
    end
    object qryListCODE_1: TStringField
      FieldName = 'CODE_1'
      Size = 12
    end
    object qryListDOC_NAME_1: TStringField
      FieldName = 'DOC_NAME_1'
      Size = 100
    end
    object qryListCODE_2: TStringField
      FieldName = 'CODE_2'
      Size = 12
    end
    object qryListDOC_NAME_2: TStringField
      FieldName = 'DOC_NAME_2'
      Size = 100
    end
    object qryListTRANSNME: TStringField
      FieldName = 'TRANSNME'
      Size = 100
    end
    object qryListLOADDNAME: TStringField
      FieldName = 'LOADDNAME'
      Size = 100
    end
    object qryListDESTNAME: TStringField
      FieldName = 'DESTNAME'
      Size = 100
    end
    object qryListHS_CODE: TStringField
      FieldName = 'HS_CODE'
      Size = 10
    end
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 80
    Top = 192
  end
  object qryDetail: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM DOMOFB_D')
    Left = 48
    Top = 224
    object qryDetailKEYY: TStringField
      FieldName = 'KEYY'
      Size = 35
    end
    object qryDetailSEQ: TIntegerField
      FieldName = 'SEQ'
    end
    object qryDetailHS_CHK: TStringField
      FieldName = 'HS_CHK'
      Size = 1
    end
    object qryDetailHS_NO: TStringField
      FieldName = 'HS_NO'
      Size = 10
    end
    object qryDetailNAME_CHK: TStringField
      FieldName = 'NAME_CHK'
      Size = 1
    end
    object qryDetailNAME_COD: TStringField
      FieldName = 'NAME_COD'
      Size = 35
    end
    object qryDetailNAME: TStringField
      FieldName = 'NAME'
    end
    object qryDetailNAME1: TMemoField
      FieldName = 'NAME1'
      BlobType = ftMemo
    end
    object qryDetailSIZE: TStringField
      FieldName = 'SIZE'
    end
    object qryDetailSIZE1: TMemoField
      FieldName = 'SIZE1'
      BlobType = ftMemo
    end
    object qryDetailQTY: TBCDField
      FieldName = 'QTY'
      Precision = 18
    end
    object qryDetailQTY_G: TStringField
      FieldName = 'QTY_G'
      Size = 3
    end
    object qryDetailQTYG: TBCDField
      FieldName = 'QTYG'
      Precision = 18
    end
    object qryDetailQTYG_G: TStringField
      FieldName = 'QTYG_G'
      Size = 3
    end
    object qryDetailPRICE: TBCDField
      FieldName = 'PRICE'
      Precision = 18
    end
    object qryDetailPRICE_G: TStringField
      FieldName = 'PRICE_G'
      Size = 3
    end
    object qryDetailAMT: TBCDField
      FieldName = 'AMT'
      Precision = 18
    end
    object qryDetailAMT_G: TStringField
      FieldName = 'AMT_G'
      Size = 3
    end
    object qryDetailSTQTY: TBCDField
      FieldName = 'STQTY'
      Precision = 18
    end
    object qryDetailSTQTY_G: TStringField
      FieldName = 'STQTY_G'
      Size = 3
    end
    object qryDetailSTAMT: TBCDField
      FieldName = 'STAMT'
      Precision = 18
    end
    object qryDetailSTAMT_G: TStringField
      FieldName = 'STAMT_G'
      Size = 3
    end
  end
  object dsDetail: TDataSource
    DataSet = qryDetail
    Left = 80
    Top = 224
  end
end
