unit UI_LOGUAR;
                               
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UI_APPLOG_BP, ChildForm, sSkinProvider, StdCtrls, sComboBox,
  Grids, DBGrids, acDBGrid, sCustomComboEdit, sCurrEdit, sCurrencyEdit,
  ComCtrls, sPageControl, Buttons, sBitBtn, Mask, sMaskEdit, sEdit,
  sButton, sLabel, sSpeedButton, ExtCtrls, sPanel, sSplitter, DB, ADODB, MSSQL, TypeDefine;

type
  TUI_LOGUAR_frm = class(TUI_APPLOG_BP_frm)
    mask_LGDATE: TsMaskEdit;
    edt_LGNO: TsEdit;
    mask_LCDATE: TsMaskEdit;
    edt_USERMAINTNO: TsEdit;
    dsList: TDataSource;
    qryList: TADOQuery;
    qryListMAINT_NO: TStringField;
    qryListUSER_ID: TStringField;
    qryListDATEE: TStringField;
    qryListMESSAGE1: TStringField;
    qryListMESSAGE2: TStringField;
    qryListMESSAGE3: TStringField;
    qryListCR_NAME1: TStringField;
    qryListCR_NAME2: TStringField;
    qryListCR_NAME3: TStringField;
    qryListSE_NAME1: TStringField;
    qryListSE_NAME2: TStringField;
    qryListSE_NAME3: TStringField;
    qryListMS_NAME1: TStringField;
    qryListMS_NAME2: TStringField;
    qryListMS_NAME3: TStringField;
    qryListAX_NAME1: TStringField;
    qryListAX_NAME2: TStringField;
    qryListAX_NAME3: TStringField;
    qryListINV_AMT: TBCDField;
    qryListINV_AMTC: TStringField;
    qryListLG_NO: TStringField;
    qryListLC_G: TStringField;
    qryListLC_NO: TStringField;
    qryListBL_G: TStringField;
    qryListBL_NO: TStringField;
    qryListCARRIER1: TStringField;
    qryListCARRIER2: TStringField;
    qryListAR_DATE: TStringField;
    qryListBL_DATE: TStringField;
    qryListLG_DATE: TStringField;
    qryListLOAD_LOC: TStringField;
    qryListLOAD_TXT: TStringField;
    qryListARR_LOC: TStringField;
    qryListARR_TXT: TStringField;
    qryListSPMARK1: TStringField;
    qryListSPMARK2: TStringField;
    qryListSPMARK3: TStringField;
    qryListSPMARK4: TStringField;
    qryListSPMARK5: TStringField;
    qryListSPMARK6: TStringField;
    qryListSPMARK7: TStringField;
    qryListSPMARK8: TStringField;
    qryListSPMARK9: TStringField;
    qryListSPMARK10: TStringField;
    qryListPAC_QTY: TBCDField;
    qryListPAC_QTYC: TStringField;
    qryListGOODS1: TStringField;
    qryListGOODS2: TStringField;
    qryListGOODS3: TStringField;
    qryListGOODS4: TStringField;
    qryListGOODS5: TStringField;
    qryListGOODS6: TStringField;
    qryListGOODS7: TStringField;
    qryListGOODS8: TStringField;
    qryListGOODS9: TStringField;
    qryListGOODS10: TStringField;
    qryListTRM_PAYC: TStringField;
    qryListTRM_PAY: TStringField;
    qryListBANK_CD: TStringField;
    qryListBANK_TXT: TStringField;
    qryListBANK_BR: TStringField;
    qryListCHK1: TBooleanField;
    qryListCHK2: TStringField;
    qryListCHK3: TStringField;
    qryListUSERMAINT_NO: TStringField;
    qryListPRNO: TIntegerField;
    qryListCN_NAME1: TStringField;
    qryListCN_NAME2: TStringField;
    qryListCN_NAME3: TStringField;
    qryListB5_NAME1: TStringField;
    qryListB5_NAME2: TStringField;
    qryListB5_NAME3: TStringField;
    qryListLC_DATE: TStringField;
    qryListMSG1NAME: TStringField;
    qryListLCGNAME: TStringField;
    qryListBLGNAME: TStringField;
    sButton4: TsButton;
    sButton2: TsButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sBitBtn22Click(Sender: TObject);
    procedure sBitBtn13Click(Sender: TObject);
    procedure qryListAfterScroll(DataSet: TDataSet);
    procedure btnDelClick(Sender: TObject);
    procedure Mask_SearchDate1DblClick(Sender: TObject);
    procedure sBitBtn14Click(Sender: TObject);
    procedure sButton4Click(Sender: TObject);
  private
    { Private declarations }
    LOGUAR_SQL : String;
    procedure Readlist(OrderSyntax : string = ''); overload;
    function ReadList(FromDate, ToDate : String; KeyValue : String=''):Boolean; overload;
  public
    { Public declarations }
  protected
    ProgramControlType : TProgramControlType;
    procedure ReadDocument; override;
    procedure DeleteDocumnet; override;
  end;

var
  UI_LOGUAR_frm: TUI_LOGUAR_frm;

implementation

uses strUtils, DateUtils, Commonlib, messageDefine, KISCalendar, LOGUAR_PRINT;
{$R *.dfm}


procedure TUI_LOGUAR_frm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
  UI_LOGUAR_frm := nil;
end;

procedure TUI_LOGUAR_frm.btnExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TUI_LOGUAR_frm.FormCreate(Sender: TObject);
begin
  inherited;
  LOGUAR_SQL := qryList.SQL.Text;
end;

procedure TUI_LOGUAR_frm.FormShow(Sender: TObject);
begin
  inherited;
  Mask_fromDate.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_toDate.Text := FormatDateTime('YYYYMMDD',Now);
  Mask_SearchDate1.Text := FormatDateTime('YYYYMMDD',StartOfTheYear(Now));
  Mask_SearchDate2.Text := FormatDateTime('YYYYMMDD',Now);

  sPageControl1.ActivePageIndex := 0;

  ProgramControlType := ctView;

  EnabledControlValue(sPanel4);
  EnabledControlValue(sPanel2);
  EnabledControlValue(sPanel17);

  ReadList(Mask_fromDate.Text,mask_toDate.Text,'');

  If qryList.RecordCount = 0 then
  begin
    ClearControlValue(sPanel2);
    ClearControlValue(sPanel4);
    ClearControlValue(sPanel17);
  end;
end;

procedure TUI_LOGUAR_frm.Readlist(OrderSyntax: string);
begin
  if com_SearchKeyword.ItemIndex in [1,2] then
  begin
    IF Trim(edt_SearchText.Text) = '' then
    begin
      if sPageControl1.ActivePageIndex = 1 Then edt_SearchText.SetFocus;

      MessageBox(Self.Handle,MSG_SYSTEM_SEARCH_ERR_NO_KEYWORD,'검색오류',MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;

  with qryList do
  begin
    Close;
    sql.Text := LOGUAR_SQL;
    case com_SearchKeyword.ItemIndex of
      0 :
      begin
        SQL.Add(' WHERE DATEE between' + QuotedStr(Mask_SearchDate1.Text)+' AND '+QuotedStr(Mask_SearchDate2.Text));
        Mask_fromDate.Text := Mask_SearchDate1.Text;
        mask_toDate.Text := Mask_SearchDate2.Text;
      end;
      1 :
      begin
        SQL.Add(' WHERE MAINT_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
      2 :
      begin
        SQL.Add(' WHERE LC_NO LIKE ' + QuotedStr('%'+edt_SearchText.Text+'%' ));
      end;
    end;

    if OrderSyntax <> '' then
    begin
      SQL.Add(OrderSyntax);
    end
    else
    begin
      SQL.Add(' ORDER BY DATEE ASC ');
    end;

    Open;
  end;
end;

function TUI_LOGUAR_frm.Readlist(FromDate, ToDate,
  KeyValue: String): Boolean;
begin
 Result := false;
  with qrylist do
  begin
    Close;
    SQL.Text := LOGUAR_SQL;
    SQL.Add(' WHERE DATEE Between '+QuotedStr(fromDate)+' AND '+QuotedStr(toDate));
    SQL.Add(' ORDER BY DATEE ASC ');
    Open;

    IF Trim(KeyValue) <> '' Then
    begin
      Result := Locate('MAINT_NO',KeyValue,[]);
    end;
  end;
end;

procedure TUI_LOGUAR_frm.sBitBtn22Click(Sender: TObject);
begin
  inherited;
  ReadList(Mask_fromDate.Text , Mask_toDate.Text , '');
  Mask_SearchDate1.Text := Mask_fromDate.Text;
  Mask_SearchDate2.Text := Mask_toDate.Text;
end;

procedure TUI_LOGUAR_frm.sBitBtn13Click(Sender: TObject);
begin
  inherited;
  Readlist();
end;

procedure TUI_LOGUAR_frm.ReadDocument;
begin
  inherited;
  if (not qryList.Active) or (qryList.RecordCount = 0) then
    Exit;

  //관리번호
  edt_MAINT_NO.Text := qryListMAINT_NO.AsString;
  //등록일자
  mask_DATEE.Text := qryListDATEE.AsString;
  //사용자
  edt_USER_ID.Text := qryListUSER_ID.AsString;
  //문서기능
  edt_msg2.Text := qryListMESSAGE2.AsString;
  //유형
  edt_msg3.Text := qryListMESSAGE3.AsString;
  //신청일자
  mask_APPDATE.Text := '';// 신청일자 필드가 존재하지않음
  //선하증권발급일자
  mask_BLDATE.Text := qryListBL_DATE.AsString;
  //도착(예정)일
  mask_ARDATE.Text := qryListAR_DATE.AsString;
  //운송유형
  edt_msg1.Text := qryListMESSAGE1.AsString;
  edt_msg1Name.Text := qryListMSG1NAME.AsString;
  //신용장(계약서)
  edt_LCG.Text := qryListLC_G.AsString;
  edt_LCGNAME.Text := qryListLCGNAME.AsString;
  edt_LCNO.Text := qryListLC_NO.AsString;
  //선하증권 or 항공화물운송장
  edt_BLG.Text := qryListBL_G.AsString;
  edt_BLGNAME.Text := qryListBLGNAME.AsString;
  edt_BLNO.Text := qryListBL_NO.AsString;
  //선기명
  edt_CARRIER1.Text := qryListCARRIER1.AsString;
  //항해번호
  edt_CARRIER2.Text := qryListCARRIER2.AsString;
  //상업송장금액
  edt_INVAMTC.Text := qryListINV_AMTC.AsString;
  curr_INVAMT.Value := qryListINV_AMT.AsCurrency;
  //포장수
  edt_PACQTYC.Text := qryListPAC_QTYC.AsString;
  curr_PACQTY.Value := qryListPAC_QTY.AsCurrency;
  //선취보증서 발급일
  mask_LGDATE.Text := qryListLG_DATE.AsString;
  //선취보증서번호
  edt_LGNO.Text := qryListLG_NO.AsString;
  //신용장발행일
  mask_LCDATE.Text := qryListLC_DATE.AsString;
  //업체신청번호
  edt_USERMAINTNO.Text := qryListUSERMAINT_NO.AsString;
  //선박회사
  edt_CRNAME1.Text := qryListCR_NAME1.AsString;
  edt_CRNAME2.Text := qryListCR_NAME2.AsString;
  edt_CRNAME3.Text := qryListCR_NAME3.AsString;
  //인수예정자
  edt_B5NAME1.Text := qryListB5_NAME1.AsString;
  edt_B5NAME2.Text := qryListB5_NAME2.AsString;
  edt_B5NAME3.Text := qryListB5_NAME3.AsString;
  //송하인
  edt_SENAME1.Text := qryListSE_NAME1.AsString;
  edt_SENAME2.Text := qryListSE_NAME2.AsString;
  edt_SENAME3.Text := qryListSE_NAME3.AsString;
  //수하인
  edt_CNNAME1.Text := qryListCN_NAME1.AsString;
  edt_CNNAME2.Text := qryListCN_NAME2.AsString;
  edt_CNNAME3.Text := qryListCN_NAME3.AsString;
  //선적항
  edt_LOADLOC.Text := qryListLOAD_LOC.AsString;
  edt_LOADTXT.Text := qryListLOAD_TXT.AsString;
  //도착항
  edt_ARRLOC.Text := qryListARR_LOC.AsString;
  edt_ARRTXT.Text := qryListARR_TXT.AsString;
  //화물표시 및 번호
  edt_SPMARK1.Text := qryListSPMARK1.AsString;
  edt_SPMARK2.Text := qryListSPMARK2.AsString;
  edt_SPMARK3.Text := qryListSPMARK3.AsString;
  edt_SPMARK4.Text := qryListSPMARK4.AsString;
  edt_SPMARK5.Text := qryListSPMARK5.AsString;
  edt_SPMARK6.Text := qryListSPMARK6.AsString;
  edt_SPMARK7.Text := qryListSPMARK7.AsString;
  edt_SPMARK8.Text := qryListSPMARK8.AsString;
  edt_SPMARK9.Text := qryListSPMARK9.AsString;
  edt_SPMARK10.Text := qryListSPMARK10.AsString;
  //상품명세
  edt_GOODS1.Text := qryListGOODS1.AsString;
  edt_GOODS2.Text := qryListGOODS2.AsString;
  edt_GOODS3.Text := qryListGOODS3.AsString;
  edt_GOODS4.Text := qryListGOODS4.AsString;
  edt_GOODS5.Text := qryListGOODS5.AsString;
  edt_GOODS6.Text := qryListGOODS6.AsString;
  edt_GOODS7.Text := qryListGOODS7.AsString;
  edt_GOODS8.Text := qryListGOODS8.AsString;
  edt_GOODS9.Text := qryListGOODS9.AsString;
  edt_GOODS10.Text := qryListGOODS10.AsString;
  //신청자
  edt_MSNAME1.Text := qryListMS_NAME1.AsString;
  edt_MSNAME2.Text := qryListMS_NAME2.AsString;
  //신청자 전자서명
  edt_MSNAME3.Text := qryListMS_NAME3.AsString;
  //명의인
  edt_AXNAME1.Text := qryListAX_NAME1.AsString;
  edt_AXNAME2.Text := qryListAX_NAME2.AsString;
  edt_AXNAME3.Text := qryListAX_NAME3.AsString;
  //결제구분 및 기간
  edt_TRMPAYC.Text := qryListTRM_PAYC.AsString;
  edt_TRMPAY.Text := qryListTRM_PAY.AsString;
  //발급은행
  edt_BANKCD.Text := qryListBANK_CD.AsString;
  edt_BANKTXT.Text := qryListBANK_TXT.AsString;
  edt_BANKBR.Text := qryListBANK_BR.AsString;

end;

procedure TUI_LOGUAR_frm.qryListAfterScroll(DataSet: TDataSet);
begin
  inherited;
  ReadDocument;
end;

procedure TUI_LOGUAR_frm.btnDelClick(Sender: TObject);
begin
  inherited;
  if qryList.RecordCount = 0 then Exit;
  DeleteDocumnet;
end;

procedure TUI_LOGUAR_frm.DeleteDocumnet;
var
  maint_no : String;
  nCursor : integer;
begin
//------------------------------------------------------------------------------
// 트랜잭션 시작
//------------------------------------------------------------------------------
  DMMssql.KISConnect.BeginTrans;

  with TADOQuery.Create(nil) do
  begin
    Connection := DMMssql.KISConnect;
    maint_no := edt_MAINT_NO.Text;
    try
      try
        IF MessageBox(Self.Handle,PChar(MSG_SYSTEM_LINE+#13#10+maint_no+#13#10+MSG_SYSTEM_LINE+#13#10+MSG_SYSTEM_DEL_CONFIRM),'삭제확인',MB_OKCANCEL+MB_ICONQUESTION) = ID_OK Then
        begin
          nCursor := qryList.RecNo;

          SQL.Text :=  'DELETE FROM LOGUAR WHERE MAINT_NO =' + QuotedStr(maint_no);
          ExecSQL;

          //트랜잭션 커밋
          DMMssql.KISConnect.CommitTrans;

          qryList.Close;
          qryList.Open;

          if (qryList.RecordCount > 0 ) AND (qryList.RecordCount >= nCursor) Then
          begin
            qryList.MoveBy(nCursor-1);
          end
          else
            qryList.First;
        end
        else
        begin
          if DMMssql.KISConnect.InTransaction then DMMssql.KISConnect.RollbackTrans;
        end;
      except
        on E:Exception do
        begin
          DMMssql.KISConnect.RollbackTrans;
          MessageBox(Self.Handle,PCHAR(MSG_SYSTEM_DEL_ERR+#13#10+E.Message),'삭제확인',MB_OK+MB_ICONERROR);
        end;
      end;

    finally
      Close;
      Free;
    end;
  end;

end;

procedure TUI_LOGUAR_frm.Mask_SearchDate1DblClick(Sender: TObject);
var
  POS : TPoint;
begin
  inherited;

  IF not (ProgramControlType in [ctInsert,ctModify,ctView]) Then Exit;

  POS.X := (Sender as TsMaskEdit).Left;
  POS.Y := (Sender as TsMaskEdit).Top+(Sender as TsMaskEdit).Height;

  POS := (Sender as TsMaskEdit).Parent.ClientToScreen(POS);

  KISCalendar_frm.Left := POS.X;
  KISCalendar_frm.Top := POS.Y;

  (Sender as TsMaskEdit).Text := FormatDateTime('YYYYMMDD',KISCalendar_frm.OpenCalendar(ConvertStr2Date((Sender as TsMaskEdit).Text)));


end;

procedure TUI_LOGUAR_frm.sBitBtn14Click(Sender: TObject);
begin
  inherited;
  case (Sender as TsBitBtn).Tag of
    0 : Mask_SearchDate1DblClick(Mask_SearchDate1);
    1 : Mask_SearchDate1DblClick(Mask_SearchDate2);
  end;
end;

procedure TUI_LOGUAR_frm.sButton4Click(Sender: TObject);
begin
  inherited;
  LOGUAR_PRINT_frm := TLOGUAR_PRINT_frm.Create(Self);
  try
    LOGUAR_PRINT_frm.MaintNo := edt_MAINT_NO.Text;
    LOGUAR_PRINT_frm.Prepare;
    case (Sender as TsButton).Tag of
      0 :
      begin
        LOGUAR_PRINT_frm.PrinterSetup;
        if LOGUAR_PRINT_frm.Tag = 0 then
          LOGUAR_PRINT_frm.Print;
      end;
      1 : LOGUAR_PRINT_frm.Preview;
    end;                           
  finally
    FreeAndNil(LOGUAR_PRINT_frm);
  end;

end;

end.
