inherited Memo_MSSQL_frm: TMemo_MSSQL_frm
  Left = 1054
  Top = 173
  Caption = #47700' '#47784' '#53076' '#46300
  ClientHeight = 506
  ClientWidth = 513
  OldCreateOrder = True
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object sPanel1: TsPanel [0]
    Left = 0
    Top = 0
    Width = 513
    Height = 43
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton3: TsSpeedButton
      Left = 377
      Top = 1
      Width = 11
      Height = 41
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object Btn_Close: TsSpeedButton
      Left = 457
      Top = 1
      Width = 55
      Height = 41
      Cursor = crHandPoint
      Caption = #45803#44592
      Layout = blGlyphTop
      Spacing = 0
      OnClick = Btn_CloseClick
      Align = alRight
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System18
      ImageIndex = 20
      Reflected = True
    end
    object sSpeedButton4: TsSpeedButton
      Left = 388
      Top = 1
      Width = 55
      Height = 41
      Cursor = crHandPoint
      Caption = #52636#47141
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      Visible = False
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System18
      ImageIndex = 21
      Reflected = True
    end
    object sSpeedButton5: TsSpeedButton
      Left = 446
      Top = 1
      Width = 11
      Height = 41
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alRight
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
    object sSplitter2: TsSplitter
      Left = 1
      Top = 1
      Width = 200
      Height = 41
      Cursor = crHSplit
      AutoSnap = False
      Enabled = False
      SkinData.SkinSection = 'TRANSPARENT'
    end
    object sLabel1: TsLabel
      Left = 63
      Top = 11
      Width = 82
      Height = 21
      Alignment = taCenter
      Caption = #47700' '#47784' '#53076' '#46300
      ParentFont = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = #47569#51008' '#44256#46357
      Font.Style = [fsBold]
    end
    object Btn_New: TsSpeedButton
      Left = 212
      Top = 1
      Width = 55
      Height = 41
      Cursor = crHandPoint
      Caption = #49888#44508
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = Btn_NewClick
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System18
      ImageIndex = 5
      Reflected = True
    end
    object Btn_Modify: TsSpeedButton
      Tag = 1
      Left = 267
      Top = 1
      Width = 55
      Height = 41
      Cursor = crHandPoint
      Caption = #49688#51221
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = Btn_NewClick
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System18
      ImageIndex = 8
      Reflected = True
    end
    object Btn_Del: TsSpeedButton
      Tag = 2
      Left = 322
      Top = 1
      Width = 55
      Height = 41
      Cursor = crHandPoint
      Caption = #49325#51228
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = [fsBold]
      Layout = blGlyphTop
      ParentFont = False
      Spacing = 0
      OnClick = Btn_NewClick
      Align = alLeft
      SkinData.CustomFont = True
      SkinData.SkinSection = 'TRANSPARENT'
      Images = DMICON.System18
      ImageIndex = 19
      Reflected = True
    end
    object sSpeedButton1: TsSpeedButton
      Left = 201
      Top = 1
      Width = 11
      Height = 41
      Cursor = crHandPoint
      Layout = blGlyphTop
      Spacing = 0
      Align = alLeft
      ButtonStyle = tbsDivider
      SkinData.SkinSection = 'TRANSPARENT'
      Reflected = True
    end
  end
  object sPanel7: TsPanel [1]
    Left = 0
    Top = 43
    Width = 513
    Height = 32
    Align = alTop
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object sEdit1: TsEdit
      Left = 94
      Top = 6
      Width = 121
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnKeyUp = sEdit1KeyUp
      SkinData.CustomColor = True
      SkinData.SkinSection = 'GROUPBOX'
    end
    object sBitBtn1: TsBitBtn
      Left = 216
      Top = 6
      Width = 47
      Height = 21
      Caption = #51312#54924
      TabOrder = 1
      OnClick = sBitBtn1Click
      SkinData.SkinSection = 'BUTTON'
    end
    object sComboBox1: TsComboBox
      Left = 4
      Top = 6
      Width = 89
      Height = 21
      Alignment = taLeftJustify
      SkinData.SkinSection = 'COMBOBOX'
      VerticalAlignment = taAlignTop
      Style = csDropDownList
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ItemHeight = 15
      ItemIndex = 0
      ParentFont = False
      TabOrder = 2
      Text = #44396#48516#53076#46300
      Items.Strings = (
        #44396#48516#53076#46300
        #47749#52845)
    end
  end
  object sDBGrid1: TsDBGrid [2]
    Left = 0
    Top = 75
    Width = 513
    Height = 210
    Align = alTop
    Color = clWhite
    DataSource = dsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = #44404#47548#52404
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = #44404#47548#52404
    TitleFont.Style = []
    SkinData.SkinSection = 'EDIT'
    Columns = <
      item
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #53076#46300
        Width = 136
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUBJECT'
        Title.Caption = #47749#52845
        Width = 262
        Visible = True
      end>
  end
  object sPanel2: TsPanel [3]
    Left = 0
    Top = 285
    Width = 513
    Height = 221
    Align = alClient
    TabOrder = 3
    SkinData.SkinSection = 'PANEL'
    object sSpeedButton7: TsSpeedButton
      Left = 130
      Top = 376
      Width = 23
      Height = 21
      Enabled = False
      SkinData.SkinSection = 'SPEEDBUTTON'
      Images = DMICON.System16
      ImageIndex = 0
    end
    object sDBEdit1: TsDBEdit
      Left = 44
      Top = 16
      Width = 169
      Height = 21
      Color = clBtnFace
      DataField = 'CODE'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #53076#46300
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBEdit2: TsDBEdit
      Left = 44
      Top = 39
      Width = 169
      Height = 21
      Color = clBtnFace
      DataField = 'SUBJECT'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #47749#52845
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
    end
    object sDBMemo1: TsDBMemo
      Left = 44
      Top = 62
      Width = 449
      Height = 147
      Color = clBtnFace
      DataField = 'D_MEMO'
      DataSource = dsList
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = #44404#47548#52404
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 2
      BoundLabel.Active = True
      BoundLabel.ParentFont = False
      BoundLabel.Caption = #45236#50857
      BoundLabel.Font.Charset = ANSI_CHARSET
      BoundLabel.Font.Color = clWindowText
      BoundLabel.Font.Height = -12
      BoundLabel.Font.Name = #44404#47548#52404
      BoundLabel.Font.Style = []
      BoundLabel.Layout = sclLeftTop
      SkinData.CustomColor = True
      SkinData.SkinSection = 'EDIT'
    end
  end
  object qryList: TADOQuery
    Connection = DMMssql.KISConnect
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT [PREFIX]'
      '      ,[CODE]'
      '      ,[SUBJECT]'
      '      ,[D_MEMO]'
      '  FROM [dbo].[MEMOD]')
    Left = 96
    Top = 168
  end
  object dsList: TDataSource
    DataSet = qryList
    Left = 128
    Top = 168
  end
end
