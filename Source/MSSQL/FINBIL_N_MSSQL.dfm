inherited FINBIL_N_MSSQL_frm: TFINBIL_N_MSSQL_frm
  Left = 630
  Top = 193
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 12
  inherited sPanel1: TsPanel
    inherited sSplitter2: TsSplitter
      Width = 88
      SkinData.SkinSection = 'TRANSPARENT'
    end
    inherited sSpeedButton3: TsSpeedButton
      Left = 304
      Visible = False
    end
    inherited Btn_Close: TsSpeedButton
      OnClick = Btn_CloseClick
    end
    inherited Btn_New: TsSpeedButton
      Left = 197
    end
    inherited Btn_Modify: TsSpeedButton
      Left = 251
    end
    inherited Btn_Del: TsSpeedButton
      Left = 89
    end
    inherited sSpeedButton1: TsSpeedButton
      Left = 412
      Visible = False
    end
    inherited sSpeedButton2: TsSpeedButton
      Left = 195
    end
    inherited Btn_Print: TsSpeedButton
      Left = 142
      OnClick = Btn_PrintClick
    end
    inherited Btn_Ready: TsSpeedButton
      Left = 359
    end
    inherited Btn_Save: TsSpeedButton
      Left = 414
    end
    inherited Btn_Cancel: TsSpeedButton
      Left = 469
    end
    inherited Btn_Temp: TsSpeedButton
      Left = 306
    end
    inherited sSpeedButton26: TsSpeedButton
      Left = 522
    end
    inherited sSpeedButton27: TsSpeedButton
      Left = 467
    end
  end
  inherited sPageControl1: TsPageControl
    inherited sTabSheet1: TsTabSheet
      inherited sPanel2: TsPanel
        inherited sButton2: TsButton
          OnClick = sButton2Click
        end
      end
      inherited sDBGrid1: TsDBGrid
        OnDblClick = sDBGrid1DblClick
      end
    end
    inherited sTabSheet3: TsTabSheet
      inherited sPanel4: TsPanel
        inherited sDBEdit33: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit34: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit35: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit36: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit37: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit38: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit39: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit2: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit3: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit4: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit5: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit6: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit7: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit8: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit9: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit10: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit11: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit12: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit13: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit14: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit15: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit16: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit17: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
        inherited sDBEdit66: TsDBEdit
          Enabled = False
          ReadOnly = False
        end
      end
    end
    inherited sTabSheet2: TsTabSheet
      inherited sPanel6: TsPanel
        inherited sDBEdit18: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit19: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit20: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit21: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit22: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit24: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit25: TsDBEdit
          Enabled = False
          BoundLabel.Caption = #50808#54868#45824#52404
        end
        inherited sDBEdit26: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit27: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit28: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit29: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit30: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit31: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit32: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit40: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit41: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit42: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit43: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit44: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit45: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit46: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit47: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit48: TsDBEdit
          Top = 398
          Enabled = False
        end
        inherited sDBEdit49: TsDBEdit
          Top = 423
          Enabled = False
        end
        inherited sDBEdit50: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit51: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit52: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit53: TsDBEdit
          Enabled = False
        end
      end
    end
    inherited sTabSheet4: TsTabSheet
      inherited sDBGrid2: TsDBGrid
        ReadOnly = True
        Columns = <
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SEQ'
            Title.Alignment = taCenter
            Title.Caption = #49692#48264
            Width = 30
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHCODE'
            Title.Alignment = taCenter
            Title.Caption = #53076#46300
            Width = 33
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'CHCODE_NAME'
            Title.Alignment = taCenter
            Title.Caption = #50976#54805
            Width = 148
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BILLNO'
            Title.Caption = #44228#49328#49436#48264#54840
            Width = 144
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHRATE'
            Title.Alignment = taCenter
            Title.Caption = #51201#50857#50836#50984
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT1'
            Title.Caption = #45824#49345#44552#50529
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT1C'
            Title.Caption = #45800#50948
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT2'
            Title.Caption = #49328#52636#44552#50529
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AMT2C'
            Title.Caption = #45800#50948
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KRWRATE'
            Title.Caption = #54872#50984
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHDAY'
            Title.Caption = #51201#50857#51068#49688
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHDATE1'
            Title.Caption = #49884#51089#44592#44036
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CHDATE2'
            Title.Caption = #51333#47308#44592#44036
            Width = 80
            Visible = True
          end>
      end
      inherited sGroupBox5: TsGroupBox
        inherited sDBEdit54: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit55: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit56: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit57: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit58: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit59: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit60: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit61: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit62: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit63: TsDBEdit
          Width = 65
          Enabled = False
        end
        inherited sDBEdit64: TsDBEdit
          Enabled = False
        end
        inherited sDBEdit65: TsDBEdit
          Enabled = False
        end
      end
    end
  end
  inherited qryList: TADOQuery
    AfterScroll = qryListAfterScroll
  end
  inherited qryDetail: TADOQuery
    AfterScroll = qryDetailAfterScroll
  end
end
