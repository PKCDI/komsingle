unit VarDefine;

interface

uses
  TypeDefine;

var
  ConnectionType : TConnectionType;
  LoginData : TLoginData;
  FProgramName : String;
  MenuList : array of string;
  BeforeActiveProgram : string;
  FAuthOK : Boolean;  
Const
  WINMATE_ROOT : String = 'C:\WINMATE\';
  WINMATE_BIN : STRING = 'C:\WINMATE\BIN\';
  WINMATE_IN : String = 'C:\WINMATE\IN\';
  WINMATE_OUT : String = 'C:\WINMATE\OUT\';
  WINMATE_FLATOUT : string = 'C:\WINMATE\FLATOUT\';
  WINMATE_FLATIN : string = 'C:\WINMATE\FLATIN\';
  WINMATE_TEMP : STRING = 'C:\WINMATE\IN\hm_tmp\';
  WINMATE_HERMES : STRING = 'C:\WINMATE\BIN\hermes.exe -m3';
Const
  WINMATE_CONVERTEXE_OUT = 'C:\WINMATE\BIN\txout.exe';
  WINMATE_CONVERTEXE_IN = 'C:\WINMATE\BIN\txin.exe';
  WINMATE_SENDEXE = 'C:\WINMATE\BIN\Hermes.exe -m2';
Const
  WINMATE_UPLOADS = 'C:\WINMATE\UPLOADS\';
  WINMATE_LOG = 'C:\WINMATE\LOG\';
  WINMATE_LOG_IN  = 'InBound.LOG';
  WINMATE_LOG_OUT = 'OutBound.LOG';

const
  DLL_NAME : PChar = 'komsingle.dll';  
implementation

end.
 