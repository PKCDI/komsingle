<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xalan:component prefix="js" functions="getKey">
		<xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
	</xalan:component>
	<xsl:template match="/">
		<xsl:element name="EDIADVBE">
			<xsl:element name="DocumentHeader">
				<xsl:element name="UniqueReferenceNo">
					<xsl:value-of select="/EDIADV/ApplicationArea/ApplicationAreaMessageIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentNo">
					<xsl:value-of select="/EDIADV/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<xsl:element name="SenderID">
					<xsl:value-of select="/EDIADV/ApplicationArea/ApplicationAreaSenderIdentifier"/>
				</xsl:element>
				<xsl:element name="RecipientID">
					<xsl:value-of select="/EDIADV/ApplicationArea/ApplicationAreaReceiverIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentType">
					<xsl:value-of select="/EDIADV/ApplicationArea/ApplicationAreaMessageTypeIndicator"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="EDIADV_M">
				<xsl:variable name="sMAINKEY" select="js:getKey(position())"/>
				<xsl:element name="MAINKEY">
					<xsl:value-of select="$sMAINKEY"/>
				</xsl:element>
				<DocName>EDIADV</DocName>
				<DocCode>BE</DocCode>
				<xsl:element name="DocId">
					<xsl:value-of select="/EDIADV/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<xsl:element name="FunctionCode">
					<xsl:value-of select="/EDIADV/DataArea/Header/DocumentFunctionCode"/>
				</xsl:element>
				<xsl:element name="ResponseTypeCode">
					<xsl:value-of select="/EDIADV/DataArea/Header/DocumentResponseTypeCode"/>
				</xsl:element>
				<xsl:element name="SP_OrgName">
					<xsl:value-of select="/EDIADV/DataArea/Header/SenderParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="SP_OrgId1">
					<xsl:value-of select="/EDIADV/DataArea/Header/SenderParty/Organization/OrganizationIdentifier[1]"/>
				</xsl:element>
				<xsl:element name="SP_OrgId2">
					<xsl:value-of select="/EDIADV/DataArea/Header/SenderParty/Organization/OrganizationIdentifier[2]"/>
				</xsl:element>
				<xsl:element name="SP_BranchOrgName">
					<xsl:value-of select="/EDIADV/DataArea/Header/SenderParty/Branch/OrganizationName"/>
				</xsl:element>
				<xsl:element name="SP_AuthValueId">
					<xsl:value-of select="/EDIADV/DataArea/Header/SenderParty/AuthenticationValueIdentifier"/>
				</xsl:element>
				<xsl:element name="RP_OrgName">
					<xsl:value-of select="/EDIADV/DataArea/Header/RecipientParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="RP_OrgId">
					<xsl:value-of select="/EDIADV/DataArea/Header/RecipientParty/Organization/OrganizationIdentifier"/>
				</xsl:element>
				<xsl:element name="RP_AuthValueId">
					<xsl:value-of select="/EDIADV/DataArea/Header/RecipientParty/AuthenticationValueIdentifier"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
