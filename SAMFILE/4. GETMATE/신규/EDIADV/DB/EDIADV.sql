CREATE TABLE  EDIADV_M  (
	MAINKEY			VARCHAR(35), 
	DocName			VARCHAR(35),
	DocCode			VARCHAR(3),
	DocId			VARCHAR(35), 
	FunctionCode		VARCHAR(3), 
	ResponseTypeCode	VARCHAR(3), 
	SP_OrgName		VARCHAR(35),
	SP_OrgId1		VARCHAR(4),
	SP_OrgId2		NUMERIC(7),
	SP_BranchOrgName	VARCHAR(35),	
	SP_AuthValueId		NUMERIC(10),
	RP_OrgName		VARCHAR(35),
	RP_OrgId		NUMERIC(10),
	RP_AuthValueId		NUMERIC(10),
	CONSTRAINT EDIADV_M_PK	PRIMARY KEY (MAINKEY)
);