<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xalan:component prefix="js" functions="getKey">
		<xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
	</xalan:component>
	<xsl:template match="/">
		<xsl:element name="GENRES2DO">
			<xsl:element name="DocumentHeader">
				<xsl:element name="UniqueReferenceNo">
					<xsl:value-of select="/GENRES2DO/ApplicationArea/ApplicationAreaMessageIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentNo">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<xsl:element name="SenderID">
					<xsl:value-of select="/GENRES2DO/ApplicationArea/ApplicationAreaSenderIdentifier"/>
				</xsl:element>
				<xsl:element name="RecipientID">
					<xsl:value-of select="/GENRES2DO/ApplicationArea/ApplicationAreaReceiverIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentType">
					<xsl:value-of select="/GENRES2DO/ApplicationArea/ApplicationAreaMessageTypeIndicator"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="GENRES_M">
				<xsl:variable name="sMAINKEY" select="js:getKey(position())"/>
				<xsl:element name="MAINKEY">
					<xsl:value-of select="$sMAINKEY"/>
				</xsl:element>
				<xsl:element name="DocumentCode">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/DocumentCode"/>
				</xsl:element>
				<xsl:element name="DocumentIdentifier">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<xsl:element name="FunctionCode">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/DocumentFunctionCode"/>
				</xsl:element>
				<xsl:element name="ResponseType">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/DocumentResponseTypeCode"/>
				</xsl:element>
				<xsl:element name="MessageInformation1">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[1]"/>
				</xsl:element>
				<xsl:element name="MessageInformation2">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[2]"/>
				</xsl:element>
				<xsl:element name="MessageInformation3">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[3]"/>
				</xsl:element>
				<xsl:element name="MessageInformation4">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[4]"/>
				</xsl:element>
				<xsl:element name="MessageInformation5">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[5]"/>
				</xsl:element>
				<xsl:element name="MessageInformation6">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[6]"/>
				</xsl:element>
				<xsl:element name="MessageInformation7">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[7]"/>
				</xsl:element>
				<xsl:element name="MessageInformation8">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[8]"/>
				</xsl:element>
				<xsl:element name="MessageInformation9">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[9]"/>
				</xsl:element>
				<xsl:element name="MessageInformation10">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/MessageInformationText[10]"/>
				</xsl:element>
				<xsl:element name="SendDate">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/DocumentSendDate"/>
				</xsl:element>
				<xsl:element name="RecipientName1">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/RecipientParty/Organization/OrganizationName[1]"/>
				</xsl:element>
				<xsl:element name="RecipientName2">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/RecipientParty/Organization/OrganizationName[2]"/>
				</xsl:element>
				<xsl:element name="RecipientName3">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/RecipientParty/Organization/OrganizationName[3]"/>
				</xsl:element>
				<xsl:element name="ContactEmailAccount">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/RecipientParty/Contact/ContactEmailAccountText"/>
				</xsl:element>
				<xsl:element name="ContactEmailDomain">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/RecipientParty/Contact/ContactEmailDomainText"/>
				</xsl:element>
				<xsl:element name="SenderName1">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/SenderParty/Organization/OrganizationName[1]"/>
				</xsl:element>
				<xsl:element name="SenderName2">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/SenderParty/Organization/OrganizationName[2]"/>
				</xsl:element>
				<xsl:element name="SenderName3">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/SenderParty/Organization/OrganizationName[3]"/>
				</xsl:element>
				<xsl:element name="RecipientIdentifier">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/DocumentRecipientIdentifier"/>
				</xsl:element>
				<xsl:element name="SenderIdentifier">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/DocumentSenderIdentifier"/>
				</xsl:element>
				<xsl:element name="ReferenceIdentifier">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/OtherReferenceIdentifier"/>
				</xsl:element>
				<xsl:element name="ReferenceDocument1">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/ReferenceDocumentName[1]"/>
				</xsl:element>
				<xsl:element name="ReferenceDocument2">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/ReferenceDocumentName[2]"/>
				</xsl:element>
				<xsl:element name="ReferenceDocument3">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/ReferenceDocumentName[3]"/>
				</xsl:element>
				<xsl:element name="ReferenceDocument4">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/ReferenceDocumentName[4]"/>
				</xsl:element>
				<xsl:element name="ReferenceDocument5">
					<xsl:value-of select="/GENRES2DO/DataArea/Header/ReferenceDocumentName[5]"/>
				</xsl:element>
				<xsl:element name="RegistDate">
					<xsl:value-of select="substring(date:date(), 1, 4)"/>
					<xsl:value-of select="substring(date:date(), 6, 2)"/>
					<xsl:value-of select="substring(date:date(), 9, 2)"/>
					<xsl:value-of select="substring(date:time(), 1, 2)"/>
					<xsl:value-of select="substring(date:time(), 4, 2)"/>
					<xsl:value-of select="substring(date:time(), 7, 2)"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
