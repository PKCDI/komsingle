<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xsl:template match="/">
		<xsl:element name="GENRES2DO">
			<xsl:element name="ApplicationArea">
				<xsl:element name="ApplicationAreaSenderIdentifier">
					<xsl:value-of select="/GENRES2DO/DocumentHeader/SenderID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaReceiverIdentifier">
					<xsl:value-of select="/GENRES2DO/DocumentHeader/RecipientID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaCreationDateTime">
					<xsl:value-of select="substring(date:date(), 1, 4)"/>
					<xsl:value-of select="substring(date:date(), 6, 2)"/>
					<xsl:value-of select="substring(date:date(), 9, 2)"/>
					<xsl:value-of select="substring(date:time(), 1, 2)"/>
					<xsl:value-of select="substring(date:time(), 4, 2)"/>
					<xsl:value-of select="substring(date:time(), 7, 2)"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageIdentifier">
					<xsl:value-of select="/GENRES2DO/DocumentHeader/DocumentNo"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageTypeIndicator">
					<xsl:value-of select="/GENRES2DO/DocumentHeader/DocumentType"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="DataArea">
				<xsl:element name="Header">
					<xsl:element name="DocumentName"><xsl:text>GENRES</xsl:text></xsl:element>
					<xsl:element name="DocumentCode">2DO</xsl:element>
					<xsl:element name="DocumentIdentifier">
						<xsl:value-of select="/GENRES2DO/GENRES_M/DocumentIdentifier"/>
					</xsl:element>
					<xsl:element name="DocumentFunctionCode">
						<xsl:value-of select="/GENRES2DO/GENRES_M/FunctionCode"/>
					</xsl:element>
					<xsl:element name="DocumentResponseTypeCode">
						<xsl:value-of select="/GENRES2DO/GENRES_M/ResponseType"/>
					</xsl:element>
					<xsl:element name="MessageInformationText">
						<xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation1"/>
					</xsl:element>
					<xsl:if test="/GENRES2DO/GENRES_M/MessageInformation2 !=''">
						<xsl:element name="MessageInformationText"><xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation2"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES2DO/GENRES_M/MessageInformation3 !=''">
						<xsl:element name="MessageInformationText"><xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation3"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES2DO/GENRES_M/MessageInformation4 !=''">
						<xsl:element name="MessageInformationText"><xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation4"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES2DO/GENRES_M/MessageInformation5 !=''">
						<xsl:element name="MessageInformationText"><xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation5"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES2DO/GENRES_M/MessageInformation6 !=''">
						<xsl:element name="MessageInformationText"><xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation6"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES2DO/GENRES_M/MessageInformation7 !=''">
						<xsl:element name="MessageInformationText"><xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation7"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES2DO/GENRES_M/MessageInformation8 !=''">
						<xsl:element name="MessageInformationText"><xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation8"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES2DO/GENRES_M/MessageInformation9 !=''">
						<xsl:element name="MessageInformationText"><xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation9"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES2DO/GENRES_M/MessageInformation10 !=''">
						<xsl:element name="MessageInformationText"><xsl:value-of select="/GENRES2DO/GENRES_M/MessageInformation10"/></xsl:element>
					</xsl:if>
					<xsl:element name="DocumentSendDate">
						<xsl:value-of select="/GENRES2DO/GENRES_M/SendDate"/>
					</xsl:element>
					<xsl:element name="RecipientParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/GENRES2DO/GENRES_M/RecipientName1"/>
							</xsl:element>
							<xsl:if test="/GENRES2DO/GENRES_M/RecipientName2 !=''">
								<xsl:element name="OrganizationName">
									<xsl:value-of select="/GENRES2DO/GENRES_M/RecipientName2"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="/GENRES2DO/GENRES_M/RecipientName3 !=''">
								<xsl:element name="OrganizationName">
									<xsl:value-of select="/GENRES2DO/GENRES_M/RecipientName3"/>
								</xsl:element>
							</xsl:if>
						</xsl:element>
						<xsl:if test="/GENRES2DO/GENRES_M/ContactEmailAccount !=''">
						<xsl:element name="Contact">
							<xsl:element name="ContactEmailAccountText">
								<xsl:value-of select="/GENRES2DO/GENRES_M/ContactEmailAccount"/>
							</xsl:element>
							<xsl:element name="ContactEmailDomainText">
								<xsl:value-of select="/GENRES2DO/GENRES_M/ContactEmailDomain"/>
							</xsl:element>
						</xsl:element>
						</xsl:if>
					</xsl:element>
					<xsl:element name="SenderParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/GENRES2DO/GENRES_M/SenderName1"/>
							</xsl:element>
							<xsl:if test="/GENRES/GENRES_M/SenderName2 !=''">
								<xsl:element name="OrganizationName">
									<xsl:value-of select="/GENRES2DO/GENRES_M/SenderName2"/>
								</xsl:element>
							</xsl:if>
							<xsl:if test="/GENRES/GENRES_M/SenderName3 !=''">
								<xsl:element name="OrganizationName">
									<xsl:value-of select="/GENRES2DO/GENRES_M/SenderName3"/>
								</xsl:element>
							</xsl:if>
						</xsl:element>
					</xsl:element>
					<xsl:element name="DocumentRecipientIdentifier">
						<xsl:value-of select="/GENRES2DO/GENRES_M/RecipientIdentifier"/>
					</xsl:element>
					<xsl:element name="DocumentSenderIdentifier">
						<xsl:value-of select="/GENRES2DO/GENRES_M/SenderIdentifier"/>
					</xsl:element>
					<xsl:element name="OtherReferenceIdentifier">
						<xsl:value-of select="/GENRES2DO/GENRES_M/ReferenceIdentifier"/>
					</xsl:element>
					<xsl:element name="ReferenceDocumentName">
						<xsl:value-of select="/GENRES2DO/GENRES_M/ReferenceDocument1"/>
					</xsl:element>
					<xsl:if test="/GENRES/GENRES_M/ReferenceDocument2 !=''">
						<xsl:element name="ReferenceDocumentName"><xsl:value-of select="/GENRES2DO/GENRES_M/ReferenceDocument2"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES/GENRES_M/ReferenceDocument3 !=''">
						<xsl:element name="ReferenceDocumentName"><xsl:value-of select="/GENRES2DO/GENRES_M/ReferenceDocument3"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES/GENRES_M/ReferenceDocument4 !=''">
						<xsl:element name="ReferenceDocumentName"><xsl:value-of select="/GENRES2DO/GENRES_M/ReferenceDocument4"/></xsl:element>
					</xsl:if>
					<xsl:if test="/GENRES/GENRES_M/ReferenceDocument5 !=''">
						<xsl:element name="ReferenceDocumentName"><xsl:value-of select="/GENRES2DO/GENRES_M/ReferenceDocument5"/></xsl:element>
					</xsl:if>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
