<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xalan:component prefix="js" functions="getKey">
		<xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
	</xalan:component>
	<xsl:template match="/">
		<xsl:element name="LOCAPP">
			<xsl:element name="ApplicationArea">
				<xsl:element name="ApplicationAreaSenderIdentifier">
					<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/SRID_M/LocalID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaReceiverIdentifier">
					<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/SRID_M/PartnerID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaDetailSenderIdentifier">
					<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/SRID_M/LocalDivisionID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaDetailReceiverIdentifier">
					<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/SRID_M/PartnerDivisionID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaCreationDateTime">
					<xsl:value-of select="substring(date:date(), 1, 4)"/>
					<xsl:value-of select="substring(date:date(), 6, 2)"/>
					<xsl:value-of select="substring(date:date(), 9, 2)"/>
					<xsl:value-of select="substring(date:time(), 1, 2)"/>
					<xsl:value-of select="substring(date:time(), 4, 2)"/>
					<xsl:value-of select="substring(date:time(), 7, 2)"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageIdentifier">
					<xsl:value-of select="/LOCAPP2AD/DocumentHeader/UniqueReferenceNo"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageTypeIndicator">LOCAPP</xsl:element>
				<xsl:element name="ApplicationAreaMessageVersionText">2AD</xsl:element>
			</xsl:element>
			<xsl:element name="DataArea">
				<xsl:element name="Header">
					<xsl:element name="DocumentName">LOCAPP</xsl:element>
					<!-- 전자문서의 코드 -->
					<xsl:element name="DocumentCode">2AD</xsl:element>
					<!-- 문서번호 -->
					<xsl:element name="DocumentIdentifier">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/DocumentIdentifier"/>
					</xsl:element>
					<!-- 문서기능 -->
					<xsl:element name="DocumentFunctionCode">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/FunctionCode"/>
					</xsl:element>
					<!-- 응답유형 -->
					<xsl:element name="DocumentResponseTypeCode">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ResponseTypeCode"/>
					</xsl:element>
					<!-- 개설근거별 용도 -->
					<xsl:element name="OpenBasedBusinessFunctionCode">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OpenBaseFunctionCode"/>
					</xsl:element>
					<!-- HS 부호 -->
					<xsl:element name="ClassIdHSIdentifier">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/HSCode"/>
					</xsl:element>
					<!-- 물품매도확약서번호 -->
					<xsl:element name="OfferSheetReferenceIdentifier">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetReference1"/>
					</xsl:element>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/OfferSheetReference2 !=''">
						<xsl:element name="OfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetReference2"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/OfferSheetReference3 !=''">
						<xsl:element name="OfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetReference3"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/OfferSheetReference4 !=''">
						<xsl:element name="OfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetReference4"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/OfferSheetReference5 !=''">
						<xsl:element name="OfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetReference5"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/OfferSheetReference6 !=''">
						<xsl:element name="OfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetReference6"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/OfferSheetReference7 !=''">
						<xsl:element name="OfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetReference7"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/OfferSheetReference8 !=''">
						<xsl:element name="OfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetReference8"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/OfferSheetReference9 !=''">
						<xsl:element name="OfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetReference9"/>
						</xsl:element>
					</xsl:if>
					<!-- 개설회차 -->
					<xsl:element name="LocalLetterOfCreditOpenDegreeNumber">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/LocalLCDegree"/>
					</xsl:element>
					<!-- 개설신청일자 -->
					<xsl:element name="LocalLetterOfCreditApplicationDate">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ApplicationDate"/>
					</xsl:element>
					<!-- 서류제시기간 -->
					<xsl:element name="DocumentPresentationPeriodDate">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/DocumentPresentDate"/>
					</xsl:element>
					<!-- 물품인도기일 -->
					<xsl:element name="DeliveryPromisedDateTime">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/DeliveryPromisDate"/>
					</xsl:element>
					<!-- 유효기일 -->
					<xsl:element name="LocalLetterOfCreditEffectiveDate">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/EffectiveDate"/>
					</xsl:element>
					<!-- 분할인도 허용여부 -->
					<xsl:element name="TransportPartialShipmentMethodCode">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/PartialShipmentCode"/>
					</xsl:element>
					<!-- 대표 공급물품명 -->
					<xsl:element name="SupplyGoodsDescriptionText">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/GoodsDescription1"/>
					</xsl:element>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/GoodsDescription2 !=''">
						<xsl:element name="SupplyGoodsDescriptionText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/GoodsDescription2"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/GoodsDescription3 !=''">
						<xsl:element name="SupplyGoodsDescriptionText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/GoodsDescription3"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/GoodsDescription4 !=''">
						<xsl:element name="SupplyGoodsDescriptionText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/GoodsDescription4"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/GoodsDescription5 !=''">
						<xsl:element name="SupplyGoodsDescriptionText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/GoodsDescription5"/>
						</xsl:element>
					</xsl:if>
					<!-- 기타정보 -->
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/AdditionalInformation1 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/AdditionalInformation1"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/AdditionalInformation2 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/AdditionalInformation2"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/AdditionalInformation3 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/AdditionalInformation3"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/AdditionalInformation4 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/AdditionalInformation4"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAPP2AD/LOCAPP_M/AdditionalInformation5 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/AdditionalInformation5"/>
						</xsl:element>
					</xsl:if>
					<!-- 개설은행 -->
					<xsl:element name="IssuingBank">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/IssuingBankCode"/>
							</xsl:element>
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/IssuingBankName"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="Branch">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/IssuingBankBranch"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<!-- 개설의뢰인  -->
					<xsl:element name="ApplicantParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ApplicantName"/>
							</xsl:element>
							<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ApplicantCEOName"/>
							</xsl:element>
					<!--	<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ApplicantCEOName2"/>
							</xsl:element> -->
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ApplicantId"/>
							</xsl:element><!-- add 20110624 -->
						</xsl:element>
						<xsl:element name="Address">
							<xsl:if test="/LOCAPP2AD/LOCAPP_M/ApplicantAddLine1 !=''"> 
							<xsl:element name="AddressLine1Text">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ApplicantAddLine1"/>
							</xsl:element>
							</xsl:if>
							<xsl:if test="/LOCAPP2AD/LOCAPP_M/ApplicantAddLine2A !=''"> 
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ApplicantAddLine2A"/>
							</xsl:element>
							</xsl:if>
							<xsl:if test="/LOCAPP2AD/LOCAPP_M/ApplicantAddLine2B !=''"> 
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ApplicantAddLine2B"/>
							</xsl:element>
							</xsl:if>
						</xsl:element>
					</xsl:element>
					<!-- 수혜자  -->
					<xsl:element name="BeneficiaryParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/BeneficiaryName"/>
							</xsl:element>
							<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/BeneficiaryCEOName"/>
							</xsl:element>
					<!-- 	<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/BeneficiaryCEOName2"/>
							</xsl:element> -->
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/BeneficiaryId"/>
							</xsl:element><!-- add 20110624 -->
						</xsl:element>
						<xsl:element name="Address">
							<xsl:if test="/LOCAPP2AD/LOCAPP_M/BeneficiaryAddLine1 !=''">
							<xsl:element name="AddressLine1Text">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/BeneficiaryAddLine1"/>
							</xsl:element>
							</xsl:if>
							<xsl:if test="/LOCAPP2AD/LOCAPP_M/BeneficiaryAddLine2A !=''">
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/BeneficiaryAddLine2A"/>
							</xsl:element>
							</xsl:if>
							<xsl:if test="/LOCAPP2AD/LOCAPP_M/BeneficiaryAddLine2B !=''">
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/BeneficiaryAddLine2B"/>
							</xsl:element>
							</xsl:if>
						</xsl:element>
						<xsl:element name="Contact">
							<xsl:element name="ContactEmailAccountText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ContactEmailAccount"/>
							</xsl:element>
							<xsl:element name="ContactEmailDomainText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ContactEmailDomain"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<!-- 전자서명 -->
					<xsl:element name="SignatureParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/SignatureName"/>
							</xsl:element>
							<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/SignatureCEOName"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="PartySignatureValueText">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/SignatureValue"/>
						</xsl:element>
					</xsl:element>
					<!-- 구비서류 시작 -->
					<xsl:element name="RequiredDocuments">
						<!-- 물품수령증명서 -->
						<xsl:element name="ReceiptTestimonyCopyNumber">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ReceiptCopyNumber"/>
						</xsl:element>
						<!-- 공급자발행 세금계산서 사본 -->
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/TaxInvoiceCopyNumber !=''">
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/TaxInvoiceCopyNumber !='0'">
							<xsl:element name="TaxInvoiceCopyNumber">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/TaxInvoiceCopyNumber"/>
							</xsl:element>
						</xsl:if>
						</xsl:if>
						<!-- 물품명세가 기재된 송장 -->
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/InvoiceCopyNumber !=''">
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/InvoiceCopyNumber !='0'">
							<xsl:element name="InvoiceCopyNumber">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/InvoiceCopyNumber"/>
							</xsl:element>
						</xsl:if>
						</xsl:if>
						<!-- 공급자발행 물품매도확약서 사본  -->
						<xsl:element name="LocalLetterOfCreditCopyNumber">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/LocalLCCopyNumber"/>
						</xsl:element>
						<!-- 본 내국신용장의 사본 -->
						<xsl:element name="OfferSheetCopyNumber">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OfferSheetCopyNumber"/>
						</xsl:element>
						<!-- 기타 구비서류 -->
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/OtherDocument1 !=''">
							<xsl:element name="OtherRequiredDocumentDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OtherDocument1"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/OtherDocument2 !=''">
							<xsl:element name="OtherRequiredDocumentDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OtherDocument2"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/OtherDocument3 !=''">
							<xsl:element name="OtherRequiredDocumentDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OtherDocument3"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/OtherDocument4 !=''">
							<xsl:element name="OtherRequiredDocumentDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OtherDocument4"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/OtherDocument5 !=''">
							<xsl:element name="OtherRequiredDocumentDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OtherDocument5"/>
							</xsl:element>
						</xsl:if>
					</xsl:element>
					<!-- 내국신용장 종류 -->
					<xsl:element name="LocalLetterOfCreditTypeCode">
						<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/LocalLCTypeCode"/>
					</xsl:element>
					<xsl:element name="LocalLetterOfCreditOpenAmount">
						<!-- 개설금액 유형 -->
						<xsl:element name="AmountTypeCode">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/LocalLCAmountTypeCode"/>
						</xsl:element>
						<!-- 개설금액 -->
						<xsl:element name="AmountBasisAmount">
							<xsl:attribute name="currency"><xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OpenAmountCurrency"/></xsl:attribute>
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OpenAmount"/>
						</xsl:element>
						<xsl:element name="AllowanceOrChargeRateNumeric">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/AOCRNum"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="OpenBasedInformation">
						<!-- 개설근거서류 종류 -->
						<xsl:element name="OpenBasedDocumentCode">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/BasedDocumentCode"/>
						</xsl:element>
						<!-- 신용장(계약서)번호 -->
						<xsl:element name="LetterOfCreditIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/LCIdentifier"/>
						</xsl:element>
						<!-- 결제통화 및 금액 -->
						<xsl:element name="PaymentAmount">
							<xsl:attribute name="currency"><xsl:value-of select="/LOCAPP2AD/LOCAPP_M/PaymentAmountCurrency"/></xsl:attribute>
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/PaymentAmount"/>
						</xsl:element>
						<!-- 선적(인도)기일 -->
						<xsl:element name="ShipmentDate">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ShipmentDate"/>
						</xsl:element>
						<!-- 유효기일 -->
						<xsl:element name="LetterOfCreditEffectiveDate">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/LCEffectiveDate"/>
						</xsl:element>
						<!-- 수출(공급)상대방 -->
						<xsl:element name="ImporterParty">
							<xsl:element name="Organization">
								<xsl:element name="OrganizationName">
									<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ImporterName"/>
								</xsl:element>
								<xsl:element name="OrganizationCEOName">
									<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ImporterCEOName1"/>
								</xsl:element>
								<xsl:element name="OrganizationCEOName">
									<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ImporterCEOName2"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
						<!-- 수출지역 -->
						<xsl:element name="ExportLocation">
							<xsl:element name="LocationIdentifier">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ExportLocationCode"/>
							</xsl:element>
						</xsl:element>
						<!-- 발행은행 -->
						<xsl:element name="IssuingBank">
							<xsl:element name="Organization">
								<xsl:element name="OrganizationName">
									<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OpenBaseIssuingBankName"/>
								</xsl:element>
							</xsl:element>
							<xsl:element name="Branch">
								<xsl:element name="OrganizationName">
									<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/OpenBaseIssuingBankBranch"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
						<!-- 대금결제조건 -->
						<xsl:element name="PaymentTermsIdentifier">
							<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/PaymentTermsCode"/>
						</xsl:element>
						<!-- 대표 수출물품명 -->
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription1 !=''">
							<xsl:element name="ExportGoodsDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription1"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription2 !=''">
							<xsl:element name="ExportGoodsDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription2"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription3 !=''">
							<xsl:element name="ExportGoodsDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription3"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription4 !=''">
							<xsl:element name="ExportGoodsDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription4"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription5 !=''">
							<xsl:element name="ExportGoodsDescriptionText">
								<xsl:value-of select="/LOCAPP2AD/LOCAPP_M/ExportGoodsDescription5"/>
							</xsl:element>
						</xsl:if>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
