<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xalan:component prefix="js" functions="getKey">
		<xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
	</xalan:component>
	<xsl:template match="/">
		<xsl:element name="LOCRCT">
			<xsl:element name="ApplicationArea">
				<xsl:element name="ApplicationAreaSenderIdentifier">
					<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/SRID_M/LocalID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaReceiverIdentifier">
					<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/SRID_M/PartnerID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaDetailSenderIdentifier">
					<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/SRID_M/LocalDivisionID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaDetailReceiverIdentifier">
					<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/SRID_M/PartnerDivisionID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaCreationDateTime">
					<xsl:value-of select="substring(date:date(), 1, 4)"/>
					<xsl:value-of select="substring(date:date(), 6, 2)"/>
					<xsl:value-of select="substring(date:date(), 9, 2)"/>
					<xsl:value-of select="substring(date:time(), 1, 2)"/>
					<xsl:value-of select="substring(date:time(), 4, 2)"/>
					<xsl:value-of select="substring(date:time(), 7, 2)"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageIdentifier">
					<xsl:value-of select="/LOCRCT2AH/DocumentHeader/UniqueReferenceNo"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageTypeIndicator">LOCRCT</xsl:element>
				<xsl:element name="ApplicationAreaMessageVersionText">2AH</xsl:element>
			</xsl:element>
			<xsl:element name="DataArea">
				<xsl:element name="Header">
					<xsl:element name="DocumentName">LOCRCT</xsl:element>
					<!-- 전자문서의 코드 -->
					<xsl:element name="DocumentCode">2AH</xsl:element>
					<!-- 문서번호 -->
					<xsl:element name="DocumentIdentifier">
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/DocumentIdentifier"/>
					</xsl:element>
					<!-- 문서기능 -->
					<xsl:element name="DocumentFunctionCode">
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/FunctionCode"/>
					</xsl:element>
					<!-- 응답유형 -->
					<xsl:element name="DocumentResponseTypeCode">
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/ResponseTypeCode"/>
					</xsl:element>
					<!-- 물품수령증명서의 발급번호 -->
					<xsl:element name="IssueIdentifier">
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/IssueIdentifier"/>
					</xsl:element>
					<!-- HS 부호 -->
					<xsl:element name="ClassIdHSIdentifier">
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/ClassIdHSIdentifier"/>
					</xsl:element>
					<!-- 물품의 인수일자 -->
					<xsl:element name="AcceptanceDate">
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AcceptanceDate"/>
					</xsl:element>
					<!-- 물품수령증명서의 발급일자 -->
					<xsl:element name="IssueDate">
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/IssueDate"/>
					</xsl:element>
					<!-- 물품수령증명서의 유효기일 -->
					<xsl:element name="EffectiveDate">
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/EffectiveDate"/>
					</xsl:element>
					<!-- 물품공급자 -->
					<xsl:element name="SupplierParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/SupplierOrgId"/>
							</xsl:element>
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/SupplierName"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<!-- 물품수령인 -->
					<xsl:element name="AcceptanceParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AcceptOrgId"/>
							</xsl:element>
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AcceptanceName"/>
							</xsl:element>
							<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AcceptanceCEOName"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="Address">
							<xsl:element name="AddressLine1Text">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AcceptanceAddressLine1"/>
							</xsl:element>
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AcceptanceAddressLine2"/>
							</xsl:element>
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AcceptanceAddressLine3"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="PartySignatureValueText">
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AcceptanceSignature"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="AcceptanceAmount">
						<!-- 물품 인수금액(외화표시) -->
						<xsl:element name="AmountBasisAmount">
							<xsl:attribute name="currency"><xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AmountCurrency"/></xsl:attribute>
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/Amount"/>
						</xsl:element>
						<!-- 물품 인수금액(원화표시) -->
						<xsl:element name="AmountConvertedAmount">
							<xsl:attribute name="currency">KRW</xsl:attribute>
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/ConvertedAmount"/>
						</xsl:element>
						<!-- 전신환매입률 -->
						<xsl:element name="Exchange">
							<xsl:element name="ExchangeRateCalculationRateNumeric">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/ExchangeRate"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<!-- 기타 조건 -->
					<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalConditions1 !=''">
						<xsl:element name="AdditionalConditionsDescriptionText">
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalConditions1"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalConditions2 !=''">
						<xsl:element name="AdditionalConditionsDescriptionText">
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalConditions2"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalConditions3 !=''">
						<xsl:element name="AdditionalConditionsDescriptionText">
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalConditions3"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalConditions4!=''">
						<xsl:element name="AdditionalConditionsDescriptionText">
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalConditions4"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalConditions5 !=''">
						<xsl:element name="AdditionalConditionsDescriptionText">
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalConditions5"/>
						</xsl:element>
					</xsl:if>
				</xsl:element>
				<xsl:for-each select="/LOCRCT2AH/LOCRCT_M/LOCRCT_ITEM">
					<xsl:element name="Line">
						<!-- 품목번호 -->
						<xsl:element name="LineNumber">
							<xsl:value-of select="./LineNumber"/>
						</xsl:element>
						<xsl:element name="LineItem">
							<xsl:element name="Item">
								<!-- HS 코드 -->
								<xsl:element name="ClassId">
									<xsl:element name="ClassIdHSIdentifier">
										<xsl:value-of select="./HSCode"/>
									</xsl:element>
								</xsl:element>
								<!-- 품명 -->
								<xsl:if test="./ItemName1 !=''">
									<xsl:element name="ItemName">
										<xsl:value-of select="./ItemName1"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./ItemName2 !=''">
									<xsl:element name="ItemName">
										<xsl:value-of select="./ItemName2"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./ItemName3 !=''">
									<xsl:element name="ItemName">
										<xsl:value-of select="./ItemName3"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./ItemName4 !=''">
									<xsl:element name="ItemName">
										<xsl:value-of select="./ItemName4"/>
									</xsl:element>
								</xsl:if>
								<!-- 규격 -->
								<xsl:if test="./Definition1 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition1"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./Definition2 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition2"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./Definition3 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition3"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./Definition4 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition4"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./Definition5 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition5"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./Definition6 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition6"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./Definition7 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition7"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./Definition8 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition8"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./Definition9 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition9"/>
									</xsl:element>
								</xsl:if>
								<xsl:if test="./Definition10 !=''">
									<xsl:element name="ItemDefinitionText">
										<xsl:value-of select="./Definition10"/>
									</xsl:element>
								</xsl:if>
							</xsl:element>
							<!-- 수량 -->
							<xsl:element name="LineItemQuantity">
								<xsl:attribute name="unitCode"><xsl:value-of select="./QuantityUnitCode"/></xsl:attribute>
								<xsl:value-of select="./Quantity"/>
							</xsl:element>
							<!-- 수량소계 -->
							<xsl:element name="LineItemSubTotalQuantity">
								<xsl:attribute name="unitCode"><xsl:value-of select="./SubTotalQuantityUnitCode"/></xsl:attribute>
								<xsl:value-of select="./SubTotalQuantity"/>
							</xsl:element>
							<!-- 단가 -->
							<xsl:element name="BasePrice">
								<xsl:element name="BasePriceAmount">
									<xsl:value-of select="./BasePriceAmount"/>
								</xsl:element>
								<xsl:element name="BaseQuantity">
									<xsl:attribute name="unitCode"><xsl:value-of select="./BaseQuantityUnitCode"/></xsl:attribute>
									<xsl:value-of select="./BaseQuantity"/>
								</xsl:element>
							</xsl:element>
							<!-- 금액 -->
							<xsl:element name="LineItemAmount">
								<xsl:attribute name="currency"><xsl:value-of select="./AmountCurrency"/></xsl:attribute>
								<xsl:value-of select="./Amount"/>
							</xsl:element>
							<!-- 금액소계 -->
							<xsl:element name="LineItemSubTotalAmount">
								<xsl:attribute name="currency"><xsl:value-of select="./SubTotalAmountCurrency"/></xsl:attribute>
								<xsl:value-of select="./SubTotalAmount"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:for-each>
				<xsl:element name="Summary">
					<!-- 인수물품의 총수량 -->
					<xsl:element name="TotalQuantity">
						<xsl:attribute name="unitCode"><xsl:value-of select="/LOCRCT2AH/LOCRCT_M/TotalQuantityUnitCode"/></xsl:attribute>
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/TotalQuantity"/>
					</xsl:element>
					<xsl:element name="TotalAmount">
						<xsl:attribute name="currency"><xsl:value-of select="/LOCRCT2AH/LOCRCT_M/TotalAmountCurrency"/></xsl:attribute>
						<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/TotalAmount"/>
					</xsl:element>
					<xsl:element name="LocalLetterOfCreditInformation">
						<!-- 내국신용장 번호 -->
						<xsl:element name="DocumentReferenceIdentifier">
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/LocalLCIdentifier"/>
						</xsl:element>
						<!-- 개설은행 -->
						<xsl:element name="IssuingBank">
							<xsl:element name="Organization">
								<xsl:element name="OrganizationIdentifier">
									<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/IssuingBankCode"/>
								</xsl:element>
								<xsl:element name="OrganizationName">
									<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/IssuingBankName"/>
								</xsl:element>
							</xsl:element>
							<xsl:element name="Branch">
								<xsl:element name="OrganizationName">
									<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/IssuingBankBranch"/>
								</xsl:element>
							</xsl:element>
							<xsl:element name="PartySignatureValueText">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/IssuingBankSignature"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="OpenAmount">
							<!-- Local L/C 금액(외화) -->
							<xsl:element name="AmountBasisAmount">
								<xsl:attribute name="currency"><xsl:value-of select="/LOCRCT2AH/LOCRCT_M/LocalLCAmountCurrency"/></xsl:attribute>
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/LocalLCAmount"/>
							</xsl:element>
							<!-- Local L/C 금액(원화) -->
							<xsl:element name="AmountConvertedAmount">
								<xsl:attribute name="currency">KRW</xsl:attribute>
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/LocalLCConvertedAmount"/>
							</xsl:element>
							<!-- 전신환 매입률 -->
							<xsl:element name="Exchange">
								<xsl:element name="ExchangeRateCalculationRateNumeric">
									<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/LocalLCExchangeRate"/>
								</xsl:element>
							</xsl:element>
						</xsl:element>
						<!-- 인도기일 -->
						<xsl:element name="ItemDeliveryDate">
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/ItemDeliveryDate"/>
						</xsl:element>
						<!-- 유효기일 -->
						<xsl:element name="LocalLetterOfCreditEffectiveDate">
							<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/LocalLCEffectiveDate"/>
						</xsl:element>
						<!-- 참조사항 -->
						<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalInformation1 !=''">
							<xsl:element name="AdditionalInformationDescriptionText">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalInformation1"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalInformation2 !=''">
							<xsl:element name="AdditionalInformationDescriptionText">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalInformation2"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalInformation3 !=''">
							<xsl:element name="AdditionalInformationDescriptionText">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalInformation3"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalInformation4 !=''">
							<xsl:element name="AdditionalInformationDescriptionText">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalInformation4"/>
							</xsl:element>
						</xsl:if>
						<xsl:if test="/LOCRCT2AH/LOCRCT_M/AdditionalInformation5 !=''">
							<xsl:element name="AdditionalInformationDescriptionText">
								<xsl:value-of select="/LOCRCT2AH/LOCRCT_M/AdditionalInformation5"/>
							</xsl:element>
						</xsl:if>
					</xsl:element>
					<xsl:if test="/LOCRCT2AH/LOCRCT_M/LOCRCT_TAX/TAX_ID !=''">
						<xsl:for-each select="/LOCRCT2AH/LOCRCT_M/LOCRCT_TAX">
							<xsl:element name="TaxInvoiceInformation">
								<xsl:element name="TaxInvoiceIdentifier">
									<xsl:value-of select="./TAX_ID"/>
								</xsl:element>
								<xsl:element name="ChargeAmount">
									<xsl:attribute name="currency"><xsl:value-of select="./TAX_ChargeAmount_CUR"/></xsl:attribute>
									<xsl:value-of select="./TAX_ChargeAmount"/>
								</xsl:element>	
								<xsl:element name="TaxAmount">
									<xsl:attribute name="currency"><xsl:value-of select="./TAX_TaxAmount_CUR"/></xsl:attribute>
									<xsl:value-of select="./TAX_TaxAmount"/>
								</xsl:element>	
								<xsl:element name="TaxInvoiceIssueDate">
									<xsl:value-of select="./TAX_InvoiceIssueDate"/>
								</xsl:element>		
							</xsl:element>
						</xsl:for-each>
					</xsl:if>
				</xsl:element><!-- Summary -->
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
