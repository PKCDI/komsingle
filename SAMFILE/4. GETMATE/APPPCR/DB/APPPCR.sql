CREATE TABLE  APPPCR_M  (
	MAINKEY                     VARCHAR(35),
	DocumentCode                VARCHAR(17),
	BeforeIdentifier            VARCHAR(35),
	DocumentIdentifier          VARCHAR(35),
	FunctionCode                VARCHAR(17),
	ResponseTypeCode            VARCHAR(17),
	ApplicantName               VARCHAR(70),
	ApplicantCEOName            VARCHAR(70),
	ApplicantIdentifier         VARCHAR(35),  -- 구매신청인 사업자등록번호
	ApplicantAddressLine1       VARCHAR(70),
	ApplicantAddressLine2       VARCHAR(70),
	ApplicantAddressLine3       VARCHAR(70),
	SupplierName                VARCHAR(70),
	SupplierCEOName             VARCHAR(70),
	SupplierIdentifier          VARCHAR(70),  -- 공급자 사업자등록번호
	SupplierAddressLine1        VARCHAR(70),
	SupplierAddressLine2        VARCHAR(70),
	SupplierAddressLine3        VARCHAR(70),
	ContactEmailAccount	    VARCHAR(70),  
	ContactEmailDomain	    VARCHAR(70),	
	UsageTypeCode               VARCHAR(17),
	UsageDescription            VARCHAR(70),
	IssueClassificationCode     VARCHAR(17),    -- 0905 Add no used
	TotalQuantity               NUMERIC(18,4),
	TotalQuantityUunitCode      VARCHAR(3),
	TotalAmount                 NUMERIC(18,4),
	TotalAmountCurrency         VARCHAR(3),
	TotalForeignAmount          NUMERIC(18,4),
	AllowanceIndicator          VARCHAR(17),
	AllowanceAmount             NUMERIC(18,4),
	AllowanceAmountCurrency     VARCHAR(3),   
	AllowanceForeignAmount      NUMERIC(18,4),	 
	SignatureName               VARCHAR(70),
	SignatureCEOName            VARCHAR(70),
	SignatureValue              VARCHAR(70),
	ConfirmBankCode             VARCHAR(35),
	ConfirmBankName             VARCHAR(70),
	ConfirmBankBranch           VARCHAR(70),
	IssuingBankCode             VARCHAR(35),	-- no used	
	IssuingBankName             VARCHAR(70),	-- no used
	IssuingBankBranch           VARCHAR(70),	-- no used
	PurchaseConfirmIssueID      VARCHAR(35),    -- 0905 Add no used
	SRIDKEY                     VARCHAR(35),
	CONSTRAINT APPPCR_M_PK	PRIMARY KEY (MAINKEY)
);
-- Line
CREATE TABLE  APPPCR_ITEM  (
	ITEMKEY                     VARCHAR(35),
	MAINKEY                     VARCHAR(35),
	LineNumber                  NUMERIC(6),
	HSCode                      VARCHAR(35),
	ItemName1                   VARCHAR(70),
	ItemName2                   VARCHAR(70),
	ItemName3                   VARCHAR(70),
	ItemName4                   VARCHAR(70),
	ItemName5                   VARCHAR(70),
	Quantity                    NUMERIC(15,4),
	QuantityUunitCode           VARCHAR(3),
	Description1                VARCHAR(70),
	Description2                VARCHAR(70),
	Description3                VARCHAR(70),
	Description4                VARCHAR(70),
	Description5                VARCHAR(70),
	Amount                      NUMERIC(18,4),
	AmountCurrency              VARCHAR(3),
	ForeignAmount               NUMERIC(18,4),
	ForeignBaseAmount           NUMERIC(18,4),
	BasePriceAmount             NUMERIC(18,4),
	BasePriceQuantity           VARCHAR(9),
	BasePriceQuantityUnitCod    VARCHAR(3),
	PurchaseDate                VARCHAR(35),
	AllowanceIndicator          VARCHAR(17),
	AllowanceAmount             NUMERIC(18,4),
	AllowanceAmountCurrency     VARCHAR(3),	   	
	AllowanceForeignAmount      NUMERIC(18,4), 
	CONSTRAINT  APPPCR_ITEM_PK  PRIMARY KEY ( ITEMKEY ),
	CONSTRAINT  APPPCR_ITEM_FK  FOREIGN KEY ( MAINKEY )  REFERENCES APPPCR_M(MAINKEY)
);
CREATE TABLE  APPPCR_ITEM_S  (
	ITEMSUBKEY                  VARCHAR(35),
	ITEMKEY                     VARCHAR(35),
	MAINKEY                     VARCHAR(35),
	TextQualifier               VARCHAR(3),
	Text1                       VARCHAR(70),
	Text2                       VARCHAR(70),
	Text3                       VARCHAR(70),
	Text4                       VARCHAR(70),
	Text5                       VARCHAR(70),
	Sort                        NUMERIC(3),
	CONSTRAINT  APPPCR_ITEM_S_PK  PRIMARY KEY ( ITEMSUBKEY ),
	CONSTRAINT  APPPCR_ITEM_S_FK  FOREIGN KEY ( ITEMKEY )  REFERENCES APPPCR_ITEM(ITEMKEY),
	CONSTRAINT  APPPCR_ITEM_S_FK2  FOREIGN KEY ( MAINKEY )  REFERENCES APPPCR_M(MAINKEY)
);
-- ReferenceInformation
CREATE TABLE  APPPCR_REF  (
	REFKEY                      VARCHAR(35),
	MAINKEY                     VARCHAR(35),
	DocumentCode                VARCHAR(17),
	DocumentIdentifier          VARCHAR(35),
	Issue_OrgId		    VARCHAR(11), -- 근거서류 발급기관 코드 1112 Add
	Issue_OrgName		    VARCHAR(70), -- 근거서류 발급기관명 1112 Add
	Issue_BranchOrgName	    VARCHAR(70), -- 근거서류 발급기관 지점명 1112 Add
	Issue_CountryCode	    VARCHAR(3),  -- 근거서류 발급기관 국가코드 1112 Add	
	AmendmentTypeCode           VARCHAR(17),    -- 0905 Add no used 
	BasedDocumentID             VARCHAR(35),    -- 0905 Add no used
	DocConfirmBankCode          VARCHAR(35),    -- 0905 Add no used
	DocConfirmBankName          VARCHAR(70),    -- 0905 Add no used
	DocConfirmBankBranch        VARCHAR(70),    -- 0905 Add no used
	HSCode                      NUMERIC(10),    -- 1112 Mod	
	ItemName1                   VARCHAR(70),
	ItemName2                   VARCHAR(70),
	ItemName3                   VARCHAR(70),
	ItemName4                   VARCHAR(70),
	ItemName5                   VARCHAR(70),
	LCAmount                    NUMERIC(18,4),
	LCAmountCurrency            VARCHAR(3),
	RelatedAmount               NUMERIC(18,4),  -- 0905 Add no used
	RelatedAmountCurrency       VARCHAR(3),     -- 0905 Add no used
	OriginalAmount              NUMERIC(18,4),  -- 0905 Add no used
	OriginalAmountCurrency      VARCHAR(3),     -- 0905 Add no used
	ShipingDate                 VARCHAR(70),
	Sort                        NUMERIC(3),
	CONSTRAINT  APPPCR_REF_PK  PRIMARY KEY ( REFKEY ),
	CONSTRAINT  APPPCR_REF_FK  FOREIGN KEY ( MAINKEY )  REFERENCES APPPCR_M(MAINKEY)
);
CREATE TABLE  APPPCR_REF_S  (
	REFSUBKEY                   VARCHAR(35),
	REFKEY                      VARCHAR(35),
	MAINKEY                     VARCHAR(35),
	TextQualifier               VARCHAR(3),
	Text1                       VARCHAR(70),
	Text2                       VARCHAR(70),
	Text3                       VARCHAR(70),
	Text4                       VARCHAR(70),
	Text5                       VARCHAR(70),
	Sort                        NUMERIC(3),
	CONSTRAINT  APPPCR_REF_S_PK  PRIMARY KEY ( REFSUBKEY ),
	CONSTRAINT  APPPCR_REF_S_FK  FOREIGN KEY ( REFKEY )  REFERENCES APPPCR_REF (REFKEY),
	CONSTRAINT  APPPCR_REF_S_FK2  FOREIGN KEY ( MAINKEY )  REFERENCES APPPCR_M (MAINKEY)
);
-- 1112 Add
CREATE TABLE  APPPCR_TAX  (
	TAXKEY                      VARCHAR(35),
	MAINKEY                     VARCHAR(35),	
	TaxInvoiceId		    VARCHAR(35),   
	TaxInvoiceIssueDate         VARCHAR(8),   
	ChargeAmount           	    NUMERIC(18,4),      
	TaxAmount              	    NUMERIC(18,4),  
	Tax_ItemName1          	    VARCHAR(35),     
	Tax_ItemName2		    VARCHAR(35),   
	Tax_ItemName3		    VARCHAR(35),   
	Tax_ItemName4		    VARCHAR(35),   
	Tax_ItemName5		    VARCHAR(35),  
	Tax_ItemQuantity	    NUMERIC(18,4), 
	Tax_ItemQuantityunit	    VARCHAR(3),
	Sort			    NUMERIC(3),	
	CONSTRAINT  APPPCR_TAX_PK  PRIMARY KEY ( TAXKEY ),
	CONSTRAINT  APPPCR_TAX_FK  FOREIGN KEY ( MAINKEY )  REFERENCES APPPCR_M(MAINKEY)
);
-- 1112 Add
CREATE TABLE  APPPCR_TAX_S  (
	TAXSUBKEY                   VARCHAR(35),
	TAXKEY                      VARCHAR(35),
	MAINKEY                     VARCHAR(35),	
	TextQualifier               VARCHAR(3),
	Text1                       VARCHAR(70),
	Text2                       VARCHAR(70),
	Text3                       VARCHAR(70),
	Text4                       VARCHAR(70),
	Text5                       VARCHAR(70),
	Sort                        NUMERIC(3),
	CONSTRAINT  APPPCR_TAX_S_PK  PRIMARY KEY ( TAXSUBKEY ),
	CONSTRAINT  APPPCR_TAX_S_FK  FOREIGN KEY ( TAXKEY )  REFERENCES APPPCR_TAX (TAXKEY),
	CONSTRAINT  APPPCR_TAX_S_FK2  FOREIGN KEY ( MAINKEY )  REFERENCES APPPCR_M (MAINKEY)
);