CREATE TABLE  PCRLIC_M  (
	MAINKEY                 VARCHAR(35),
	DocumentCode            VARCHAR(17),
	BeforeIdentifier	VARCHAR(35),
	DocumentIdentifier	VARCHAR(35),
	FunctionCode		VARCHAR(17),
	ResponseTypeCode	VARCHAR(17),
	ApplicantName		VARCHAR(70),
	ApplicantCEOName	VARCHAR(70),
	ApplicantIdentifier	VARCHAR(35),
	ApplicantAddressLine1	VARCHAR(70),
	ApplicantAddressLine2	VARCHAR(70),
	ApplicantAddressLine3	VARCHAR(70),
	SupplierName		VARCHAR(70),
	SupplierCEOName		VARCHAR(70),
	SupplierIdentifier	VARCHAR(35),
	SupplierAddressLine1	VARCHAR(70),
	SupplierAddressLine2	VARCHAR(70),
	SupplierAddressLine3	VARCHAR(70),
	ContactEmailAccount	VARCHAR(70),
	ContactEmailDomain	VARCHAR(70),
	UsageTypeCode		VARCHAR(35),
	UsageDescription	VARCHAR(70),
	IssueClassificationCode VARCHAR(17),     -- 0905 Add -- no use
	ConfirmationDate 	VARCHAR(8),
	-- Summary
	TotalQuantity		NUMERIC(18,4),
	TotalQuantityUunitCode	VARCHAR(3),
	TotalAmount		NUMERIC(18,4),
	TotalAmountCurrency	VARCHAR(3),
	TotalForeignAmount	NUMERIC(18,4),
	AllowanceIndicator	VARCHAR(3),
	AllowanceAmount		NUMERIC(18,4),
	AllowanceAmountCurrency	VARCHAR(3),
	AllowanceForeignAmount	NUMERIC(18,4),
	-- 
	ConfirmIdentifier	VARCHAR(35),
	ApplicationIdentifier	VARCHAR(35),
	ConfirmBankCode		VARCHAR(35),
	ConfirmBankName		VARCHAR(70),
	ConfirmBankBranch	VARCHAR(70),
	IssuingBankCode		VARCHAR(35),  -- no use
	IssuingBankName		VARCHAR(70),  -- no use
	IssuingBankBranch	VARCHAR(70),  -- no use
    	PurchaseConfirmIssueID  VARCHAR(35),  -- 0905 Add -- no use
	BankSignaturName	VARCHAR(70),
	BankSignaturCEOName	VARCHAR(70),
	BankSignaturValue	VARCHAR(70),
	CONSTRAINT PCRLIC_M_PK	PRIMARY KEY (MAINKEY)
);
-- �߱�����
CREATE TABLE  PCRLIC_S  (
	SUBKEY			VARCHAR(35),
	MAINKEY			VARCHAR(35),
	TextQualifier		VARCHAR(3),
	Text1			VARCHAR(70),
	Text2			VARCHAR(70),
	Text3			VARCHAR(70),
	Text4			VARCHAR(70),
	Text5			VARCHAR(70),
	Sort			NUMERIC(3),
	CONSTRAINT  PCRLIC_S_PK  PRIMARY KEY ( SUBKEY ),
	CONSTRAINT  PCRLIC_S_FK  FOREIGN KEY ( MAINKEY )  REFERENCES PCRLIC_M(MAINKEY)
);
-- Line
CREATE TABLE  PCRLIC_ITEM  (
	ITEMKEY			VARCHAR(35),
	MAINKEY			VARCHAR(35),
	LineNumber		NUMERIC(6),
    	ItemCode                VARCHAR(35),    -- 0905 Add -- no use
	HSCode			VARCHAR(35),
	ItemName1		VARCHAR(70),
	ItemName2		VARCHAR(70),
	ItemName3		VARCHAR(70),
	ItemName4		VARCHAR(70),
	ItemName5		VARCHAR(70),
	Quantity		NUMERIC(18,4),
	QuantityUunitCode	VARCHAR(3),
	Description1		VARCHAR(70),
	Description2		VARCHAR(70),
	Description3		VARCHAR(70),
	Description4		VARCHAR(70),
	Description5		VARCHAR(70),
	Amount			NUMERIC(18,4),
	AmountCurrency		VARCHAR(3),
	ForeignAmount		NUMERIC(18,4),
	ForeignBaseAmount	NUMERIC(18,4),
	BasePriceAmount		NUMERIC(18,4),	
	BasePriceQuantity	NUMERIC(18,4),
	BasePriceQuantityUnitCod	VARCHAR(3),
	PurchaseDate		VARCHAR(35),
	AllowanceIndicator	VARCHAR(3),
	AllowanceAmount		NUMERIC(18,4),
	AllowanceAmountCurrency		VARCHAR(3),
	AllowanceForeignAmount		NUMERIC(18,4),
	CONSTRAINT  PCRLIC_ITEM_PK  PRIMARY KEY ( ITEMKEY ),
	CONSTRAINT  PCRLIC_ITEM_FK  FOREIGN KEY ( MAINKEY )  REFERENCES PCRLIC_M(MAINKEY)
);
CREATE TABLE  PCRLIC_ITEM_S  (
	ITEMSUBKEY		VARCHAR(35),
	ITEMKEY			VARCHAR(35),
	MAINKEY			VARCHAR(35),
	TextQualifier		VARCHAR(3),
	Text1			VARCHAR(70),
	Text2			VARCHAR(70),
	Text3			VARCHAR(70),
	Text4			VARCHAR(70),
	Text5			VARCHAR(70),
	Sort			NUMERIC(3),
	CONSTRAINT  PCRLIC_ITEM_S_PK  PRIMARY KEY ( ITEMSUBKEY ),
	CONSTRAINT  PCRLIC_ITEM_S_FK  FOREIGN KEY ( ITEMKEY )  REFERENCES PCRLIC_ITEM(ITEMKEY),
	CONSTRAINT  PCRLIC_ITEM_S_FK2  FOREIGN KEY ( MAINKEY )  REFERENCES PCRLIC_M(MAINKEY)
);
-- 1112 Add
CREATE TABLE  PCRLIC_TAX  (
	TAXKEY                  VARCHAR(35),
	MAINKEY                 VARCHAR(35),	
	TaxInvoiceId		VARCHAR(35),   
	TaxInvoiceIssueDate     VARCHAR(8),   
	ChargeAmount            NUMERIC(18,4),      
	TaxAmount               NUMERIC(18,4),  
	Tax_ItemName1           VARCHAR(35),     
	Tax_ItemName2		VARCHAR(35),   
	Tax_ItemName3		VARCHAR(35),   
	Tax_ItemName4		VARCHAR(35),   
	Tax_ItemName5		VARCHAR(35),  
	Tax_ItemQuantity	NUMERIC(18,4), 
	Tax_ItemQuantityunit	VARCHAR(3),
	Sort			NUMERIC(3),	
	CONSTRAINT  PCRLIC_TAX_PK  PRIMARY KEY ( TAXKEY ),
	CONSTRAINT  PCRLIC_TAX_FK  FOREIGN KEY ( MAINKEY )  REFERENCES PCRLIC_M(MAINKEY)
);
-- 1112 Add
CREATE TABLE  PCRLIC_TAX_S  (
	TAXSUBKEY               VARCHAR(35),
	TAXKEY                  VARCHAR(35),
	MAINKEY                 VARCHAR(35),	
	TextQualifier           VARCHAR(3),
	Text1                   VARCHAR(70),
	Text2                   VARCHAR(70),
	Text3                   VARCHAR(70),
	Text4                   VARCHAR(70),
	Text5                   VARCHAR(70),
	Sort                    NUMERIC(3),
	CONSTRAINT  PCRLIC_TAX_S_PK  PRIMARY KEY ( TAXSUBKEY ),
	CONSTRAINT  PCRLIC_TAX_S_FK  FOREIGN KEY ( TAXKEY )  REFERENCES PCRLIC_TAX (TAXKEY),
	CONSTRAINT  PCRLIC_TAX_S_FK2  FOREIGN KEY ( MAINKEY )  REFERENCES PCRLIC_M (MAINKEY)
);
-- no use
CREATE TABLE  PCRLIC_REF  (
	REFKEY			VARCHAR(35),
	MAINKEY			VARCHAR(35),
	DocumentCode		VARCHAR(17),
	DocumentIdentifier	VARCHAR(35),
    	AmendmentTypeCode       VARCHAR(17),    -- 0905 Add
    	BasedDocumentID         VARCHAR(35),    -- 0905 Add
	HSCode			VARCHAR(35),
	ItemName1		VARCHAR(70),
	ItemName2		VARCHAR(70),
	ItemName3		VARCHAR(70),
	ItemName4		VARCHAR(70),
	ItemName5               VARCHAR(70),
	LCAmount                NUMERIC(18,4),
	LCAmountCurrency        VARCHAR(3),
	RelatedAmount           NUMERIC(18,4),   -- 0905 Add
	RelatedAmountCurrency	VARCHAR(3),      -- 0905 Add
	OriginalAmount		NUMERIC(18,4),   -- 0905 Add
	OriginalAmountCurrency  VARCHAR(3),      -- 0905 Add
	ShipingDate		VARCHAR(35),
	Sort                    NUMERIC(3),
	CONSTRAINT  PCRLIC_REF_PK  PRIMARY KEY ( REFKEY ),
	CONSTRAINT  PCRLIC_REF_FK  FOREIGN KEY ( MAINKEY )  REFERENCES PCRLIC_M(MAINKEY)
);
-- no use
CREATE TABLE  PCRLIC_REF_S  (
	REFSUBKEY		VARCHAR(35),
	REFKEY			VARCHAR(35),
	MAINKEY			VARCHAR(35),
	TextQualifier		VARCHAR(3),
	Text1			VARCHAR(70),
	Text2			VARCHAR(70),
	Text3			VARCHAR(70),
	Text4			VARCHAR(70),
	Text5			VARCHAR(70),
	Sort			NUMERIC(3),
	CONSTRAINT  PCRLIC_REF_S_PK  PRIMARY KEY ( REFSUBKEY ),
   	CONSTRAINT  PCRLIC_REF_S_FK  FOREIGN KEY ( REFKEY )  REFERENCES PCRLIC_REF(REFKEY),
	CONSTRAINT  PCRLIC_REF_S_FK2  FOREIGN KEY ( MAINKEY )  REFERENCES PCRLIC_M(MAINKEY)
);