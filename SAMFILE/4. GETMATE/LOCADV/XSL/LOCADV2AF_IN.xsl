<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xalan:component prefix="js" functions="getKey">
		<xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
	</xalan:component>
	<xsl:include href="template.xsl"/>
	<xsl:template match="/">
		<xsl:element name="LOCADV2AF">
			<xsl:element name="DocumentHeader">
				<xsl:element name="UniqueReferenceNo">
					<xsl:value-of select="/LOCADV/ApplicationArea/ApplicationAreaMessageIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentNo">
					<xsl:value-of select="/LOCADV/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<xsl:element name="SenderID">
					<xsl:value-of select="/LOCADV/ApplicationArea/ApplicationAreaSenderIdentifier"/>
				</xsl:element>
				<xsl:element name="RecipientID">
					<xsl:value-of select="/LOCADV/ApplicationArea/ApplicationAreaReceiverIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentType">
					<xsl:value-of select="/LOCADV/ApplicationArea/ApplicationAreaMessageTypeIndicator"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="LOCADV_M">
				<xsl:variable name="sMAINKEY" select="js:getKey(position())"/>
				<xsl:element name="MAINKEY">
					<xsl:value-of select="$sMAINKEY"/>
				</xsl:element>
				<!-- 문서번호 -->
				<xsl:element name="DocumentIdentifier">
					<xsl:value-of select="/LOCADV/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<!-- 문서기능 -->
				<xsl:element name="FunctionCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/DocumentFunctionCode"/>
				</xsl:element>
				<!-- 응답유형 -->
				<xsl:element name="ResponseTypeCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/DocumentResponseTypeCode"/>
				</xsl:element>
				<!-- 개설근거별 용도 -->
				<xsl:element name="OpenBaseFunctionCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedBusinessFunctionCode"/>
				</xsl:element>
				<!-- 신청번호 -->
				<xsl:element name="ApplicationIdentifier">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditApplicationIdentifier"/>
				</xsl:element>
				<!-- 내국신용장번호 -->
				<xsl:element name="LocalLCIdentifier">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditIdentifier"/>
				</xsl:element>
				<!-- HS 부호 -->
				<xsl:element name="HSCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/ClassIdHSIdentifier"/>
				</xsl:element>
				<!-- 물품매도확약서번호 -->
				<xsl:element name="OfferSheetReference1">
					<xsl:value-of select="/LOCADV/DataArea/Header/OfferSheetReferenceIdentifier[1]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference2">
					<xsl:value-of select="/LOCADV/DataArea/Header/OfferSheetReferenceIdentifier[2]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference3">
					<xsl:value-of select="/LOCADV/DataArea/Header/OfferSheetReferenceIdentifier[3]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference4">
					<xsl:value-of select="/LOCADV/DataArea/Header/OfferSheetReferenceIdentifier[4]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference5">
					<xsl:value-of select="/LOCADV/DataArea/Header/OfferSheetReferenceIdentifier[5]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference6">
					<xsl:value-of select="/LOCADV/DataArea/Header/OfferSheetReferenceIdentifier[6]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference7">
					<xsl:value-of select="/LOCADV/DataArea/Header/OfferSheetReferenceIdentifier[7]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference8">
					<xsl:value-of select="/LOCADV/DataArea/Header/OfferSheetReferenceIdentifier[8]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference9">
					<xsl:value-of select="/LOCADV/DataArea/Header/OfferSheetReferenceIdentifier[9]"/>
				</xsl:element>
				<!-- 개설회차 -->
				<xsl:element name="LocalLCDegree">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditOpenDegreeNumber"/>
				</xsl:element>
				<!-- 통지일자 -->
				<xsl:element name="NotificationDate">
					<xsl:value-of select="/LOCADV/DataArea/Header/NotificationDate"/>
				</xsl:element>
				<!-- 개설일자 -->
				<xsl:element name="IssueDate">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditIssueDate"/>
				</xsl:element>
				<!-- 서류제시기간 -->
				<xsl:element name="DocumentPresentDate">
					<xsl:value-of select="/LOCADV/DataArea/Header/DocumentPresentationPeriodDate"/>
				</xsl:element>
				<!-- 물품인도기일 -->
				<xsl:element name="DeliveryPromisDate">
					<xsl:value-of select="/LOCADV/DataArea/Header/DeliveryPromisedDateTime"/>
				</xsl:element>
				<!-- 유효기일 -->
				<xsl:element name="EffectiveDate">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditEffectiveDate"/>
				</xsl:element>
				<!-- 분할인도 허용여부 -->
				<xsl:element name="PartialShipmentCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/TransportPartialShipmentMethodCode"/>
				</xsl:element>
				<!-- 대표 공급물품명 -->
				<xsl:element name="GoodsDescription1">
					<xsl:value-of select="/LOCADV/DataArea/Header/SupplyGoodsDescriptionText[1]"/>
				</xsl:element>
				<xsl:element name="GoodsDescription2">
					<xsl:value-of select="/LOCADV/DataArea/Header/SupplyGoodsDescriptionText[2]"/>
				</xsl:element>
				<xsl:element name="GoodsDescription3">
					<xsl:value-of select="/LOCADV/DataArea/Header/SupplyGoodsDescriptionText[3]"/>
				</xsl:element>
				<xsl:element name="GoodsDescription4">
					<xsl:value-of select="/LOCADV/DataArea/Header/SupplyGoodsDescriptionText[4]"/>
				</xsl:element>
				<xsl:element name="GoodsDescription5">
					<xsl:value-of select="/LOCADV/DataArea/Header/SupplyGoodsDescriptionText[5]"/>
				</xsl:element>
				<!-- 기타정보 -->
				<xsl:element name="AdditionalInformation1">
					<xsl:value-of select="/LOCADV/DataArea/Header/AdditionalInformationDescriptionText[1]"/>
				</xsl:element>
				<xsl:element name="AdditionalInformation2">
					<xsl:value-of select="/LOCADV/DataArea/Header/AdditionalInformationDescriptionText[2]"/>
				</xsl:element>
				<xsl:element name="AdditionalInformation3">
					<xsl:value-of select="/LOCADV/DataArea/Header/AdditionalInformationDescriptionText[3]"/>
				</xsl:element>
				<xsl:element name="AdditionalInformation4">
					<xsl:value-of select="/LOCADV/DataArea/Header/AdditionalInformationDescriptionText[4]"/>
				</xsl:element>
				<xsl:element name="AdditionalInformation5">
					<xsl:value-of select="/LOCADV/DataArea/Header/AdditionalInformationDescriptionText[5]"/>
				</xsl:element>
				<!-- 개설은행 -->
				<xsl:element name="IssuingBankCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/IssuingBank/Organization/OrganizationIdentifier"/>
				</xsl:element>
				<xsl:element name="IssuingBankName">
					<xsl:value-of select="/LOCADV/DataArea/Header/IssuingBank/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="IssuingBankBranch">
					<xsl:value-of select="/LOCADV/DataArea/Header/IssuingBank/Branch/OrganizationName"/>
				</xsl:element>
				<!-- 개설의뢰인 -->
				<xsl:element name="ApplicantName">
					<xsl:value-of select="/LOCADV/DataArea/Header/ApplicantParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="ApplicantCEOName">
					<xsl:value-of select="/LOCADV/DataArea/Header/ApplicantParty/Organization/OrganizationCEOName"/>
				</xsl:element>
		<!-- <xsl:element name="ApplicantCEOName2">
					<xsl:value-of select="/LOCADV/DataArea/Header/ApplicantParty/Organization/OrganizationCEOName[2]"/>
				</xsl:element> -->
				<xsl:element name="ApplicantId">
					<xsl:value-of select="/LOCADV/DataArea/Header/ApplicantParty/Organization/OrganizationIdentifier"/>
				</xsl:element><!-- add 20110624 -->
				<xsl:element name="ApplicantAddLine1">
					<xsl:value-of select="/LOCADV/DataArea/Header/ApplicantParty/Address/AddressLine1Text"/>
				</xsl:element>
				<xsl:element name="ApplicantAddLine2A">
					<xsl:value-of select="/LOCADV/DataArea/Header/ApplicantParty/Address/AddressLine2Text[1]"/>
				</xsl:element>
				<xsl:element name="ApplicantAddLine2B">
					<xsl:value-of select="/LOCADV/DataArea/Header/ApplicantParty/Address/AddressLine2Text[2]"/>
				</xsl:element>
				<!-- 수혜자 -->
				<xsl:element name="BeneficiaryName">
					<xsl:value-of select="/LOCADV/DataArea/Header/BeneficiaryParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="BeneficiaryCEOName">
					<xsl:value-of select="/LOCADV/DataArea/Header/BeneficiaryParty/Organization/OrganizationCEOName"/>
				</xsl:element>
		<!--	<xsl:element name="BeneficiaryCEOName2">
					<xsl:value-of select="/LOCADV/DataArea/Header/BeneficiaryParty/Organization/OrganizationCEOName[2]"/>
				</xsl:element> -->
				<xsl:element name="BeneficiaryId">
					<xsl:value-of select="/LOCADV/DataArea/Header/BeneficiaryParty/Organization/OrganizationIdentifier"/>
				</xsl:element><!-- add 20110624 -->
				<xsl:element name="BeneficiaryAddLine1">
					<xsl:value-of select="/LOCADV/DataArea/Header/BeneficiaryParty/Address/AddressLine1Text"/>
				</xsl:element>
				<xsl:element name="BeneficiaryAddLine2A">
					<xsl:value-of select="/LOCADV/DataArea/Header/BeneficiaryParty/Address/AddressLine2Text[1]"/>
				</xsl:element>
				<xsl:element name="BeneficiaryAddLine2B">
					<xsl:value-of select="/LOCADV/DataArea/Header/BeneficiaryParty/Address/AddressLine2Text[2]"/>
				</xsl:element>
				<xsl:element name="BP_ContactEmailAccount">
					<xsl:value-of select="/LOCADV/DataArea/Header/BeneficiaryParty/Contact/ContactEmailAccountText"/>
				</xsl:element>
				<xsl:element name="BP_ContactEmailDomain">
					<xsl:value-of select="/LOCADV/DataArea/Header/BeneficiaryParty/Contact/ContactEmailDomainText"/>
				</xsl:element>
				<!-- 전자서명 -->
				<xsl:element name="SignatureName">
					<xsl:value-of select="/LOCADV/DataArea/Header/SignatureParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="SignatureCEOName">
					<xsl:value-of select="/LOCADV/DataArea/Header/SignatureParty/Organization/OrganizationCEOName"/>
				</xsl:element>
				<xsl:element name="SignatureValue">
					<xsl:value-of select="/LOCADV/DataArea/Header/SignatureParty/PartySignatureValueText"/>
				</xsl:element>
				<!-- 구비서류 시작 -->
				<!-- 물품수령증명서 -->
				<xsl:element name="ReceiptCopyNumber">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/ReceiptTestimonyCopyNumber"/>
				</xsl:element>
				<!-- 공급자발행 세금계산서 사본 -->
				<xsl:element name="TaxInvoiceCopyNumber">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/TaxInvoiceCopyNumber"/>
				</xsl:element>
				<!-- 물품명세가 기재된 송장 -->
				<xsl:element name="InvoiceCopyNumber">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/InvoiceCopyNumber"/>
				</xsl:element>
				<!-- 본 내국신용장의 사본 -->
				<xsl:element name="LocalLCCopyNumber">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/LocalLetterOfCreditCopyNumber"/>
				</xsl:element>
				<!-- 공급자발행 물품매도확약서 사본    -->
				<xsl:element name="OfferSheetCopyNumber">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/OfferSheetCopyNumber"/>
				</xsl:element>
				<!-- 기타 구비서류 -->
				<xsl:element name="OtherDocument1">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/OtherRequiredDocumentDescriptionText[1]"/>
				</xsl:element>
				<xsl:element name="OtherDocument2">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/OtherRequiredDocumentDescriptionText[2]"/>
				</xsl:element>
				<xsl:element name="OtherDocument3">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/OtherRequiredDocumentDescriptionText[3]"/>
				</xsl:element>
				<xsl:element name="OtherDocument4">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/OtherRequiredDocumentDescriptionText[4]"/>
				</xsl:element>
				<xsl:element name="OtherDocument5">
					<xsl:value-of select="/LOCADV/DataArea/Header/RequiredDocuments/OtherRequiredDocumentDescriptionText[5]"/>
				</xsl:element>
				<!-- 구비서류 끝 -->
				<!-- 내국신용장 종류 -->
				<xsl:element name="LocalLCTypeCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditTypeCode"/>
				</xsl:element>
				<!-- 개설금액 코드 -->
				<xsl:element name="LocalLCAmountTypeCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditOpenAmount/AmountTypeCode"/>
				</xsl:element>
				<!-- 개설외화금액 -->
				<xsl:element name="ConvertedAmount">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditOpenAmount/AmountConvertedAmount"/>
				</xsl:element>
				<xsl:element name="ConvertedAmountCurrency">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditOpenAmount/AmountConvertedAmount/@currency"/>
				</xsl:element>
				<!-- 개설원화금액 -->
				<xsl:element name="OpenAmount">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditOpenAmount/AmountBasisAmount"/>
				</xsl:element>
				<xsl:element name="OpenAmountCurrency">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditOpenAmount/AmountBasisAmount/@currency"/>
				</xsl:element>
				<xsl:element name="AOCRateNumeric">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditOpenAmount/AllowanceOrChargeRateNumeric"/>
				</xsl:element>
				<!-- 전신환매입율 -->
				<xsl:element name="ExchangeRate">
					<xsl:value-of select="/LOCADV/DataArea/Header/LocalLetterOfCreditOpenAmount/ExchangeRate/ExchangeRateCalculationRateNumeric"/>
				</xsl:element>
				<!-- 개설근거서류 종류 -->
				<xsl:element name="BasedDocumentCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/OpenBasedDocumentCode"/>
				</xsl:element>
				<!-- 신용장(계약서)번호 -->
				<xsl:element name="LCIdentifier">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/LetterOfCreditIdentifier"/>
				</xsl:element>
				<!-- 결제통화 및 금액 -->
				<xsl:element name="PaymentAmount">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/PaymentAmount"/>
				</xsl:element>
				<xsl:element name="PaymentAmountCurrency">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/PaymentAmount/@currency"/>
				</xsl:element>
				<!-- 선적(인도)기일 -->
				<xsl:element name="ShipmentDate">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ShipmentDate"/>
				</xsl:element>
				<!-- 유효기일 -->
				<xsl:element name="LCEffectiveDate">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/LetterOfCreditEffectiveDate"/>
				</xsl:element>
				<!-- 수출(공급)상대방 -->
				<xsl:element name="ImporterName">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ImporterParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="ImporterCEOName1">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ImporterParty/Organization/OrganizationCEOName[1]"/>
				</xsl:element>
				<xsl:element name="ImporterCEOName2">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ImporterParty/Organization/OrganizationCEOName[2]"/>
				</xsl:element>
				<!-- 수출지역 -->
				<xsl:element name="ExportLocationCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ExportLocation/LocationIdentifier"/>
				</xsl:element>
				<!-- 발행은행 -->
				<xsl:element name="OpenBaseIssuingBankName">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/IssuingBank/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="OpenBaseIssuingBankBranch">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/IssuingBank/Branch/OrganizationName"/>
				</xsl:element>
				<!-- 대금결제조건 -->
				<xsl:element name="PaymentTermsCode">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/PaymentTermsIdentifier"/>
				</xsl:element>
				<!-- 대표 수출물품명 -->
				<xsl:element name="ExportGoodsDescription1">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ExportGoodsDescriptionText[1]"/>
				</xsl:element>
				<xsl:element name="ExportGoodsDescription2">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ExportGoodsDescriptionText[2]"/>
				</xsl:element>
				<xsl:element name="ExportGoodsDescription3">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ExportGoodsDescriptionText[3]"/>
				</xsl:element>
				<xsl:element name="ExportGoodsDescription4">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ExportGoodsDescriptionText[4]"/>
				</xsl:element>
				<xsl:element name="ExportGoodsDescription5">
					<xsl:value-of select="/LOCADV/DataArea/Header/OpenBasedInformation/ExportGoodsDescriptionText[5]"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
