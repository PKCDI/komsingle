<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xalan:component prefix="js" functions="getKey">
		<xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
	</xalan:component>
	<xsl:template match="/">
		<xsl:element name="LOCAMR">
			<xsl:element name="ApplicationArea">
				<xsl:element name="ApplicationAreaSenderIdentifier">
					<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/SRID_M/LocalID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaReceiverIdentifier">
					<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/SRID_M/PartnerID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaDetailSenderIdentifier">
					<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/SRID_M/LocalDivisionID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaDetailReceiverIdentifier">
					<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/SRID_M/PartnerDivisionID"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaCreationDateTime">
					<xsl:value-of select="substring(date:date(), 1, 4)"/>
					<xsl:value-of select="substring(date:date(), 6, 2)"/>
					<xsl:value-of select="substring(date:date(), 9, 2)"/>
					<xsl:value-of select="substring(date:time(), 1, 2)"/>
					<xsl:value-of select="substring(date:time(), 4, 2)"/>
					<xsl:value-of select="substring(date:time(), 7, 2)"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageIdentifier">
					<xsl:value-of select="/LOCAMR2AE/DocumentHeader/UniqueReferenceNo"/>
				</xsl:element>
				<xsl:element name="ApplicationAreaMessageTypeIndicator">LOCAMR</xsl:element>
				<xsl:element name="ApplicationAreaMessageVersionText">2AE</xsl:element>
			</xsl:element>
			<xsl:element name="DataArea">
				<xsl:element name="Header">
					<xsl:element name="DocumentName">LOCAMR</xsl:element>
					<!-- 전자문서의 코드 -->
					<xsl:element name="DocumentCode">2AE</xsl:element>
					<!-- 문서번호 -->
					<xsl:element name="DocumentIdentifier">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/DocumentIdentifier"/>
					</xsl:element>
					<!-- 문서기능 -->
					<xsl:element name="DocumentFunctionCode">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/FunctionCode"/>
					</xsl:element>
					<!-- 응답유형 -->
					<xsl:element name="DocumentResponseTypeCode">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/ResponseTypeCode"/>
					</xsl:element>
					<!-- 내국신용장번호 -->
					<xsl:element name="LocalLetterOfCreditIdentifier">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/LocalLCIdentifier"/>
					</xsl:element>
					<!-- 최종 물품매도확약서번호 -->
					<xsl:element name="FinalOfferSheetReferenceIdentifier">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OfferSheetReference1"/>
					</xsl:element>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OfferSheetReference2 !=''">
						<xsl:element name="FinalOfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OfferSheetReference2"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OfferSheetReference3 !=''">
						<xsl:element name="FinalOfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OfferSheetReference3"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OfferSheetReference4 !=''">
						<xsl:element name="FinalOfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OfferSheetReference4"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OfferSheetReference5 !=''">
						<xsl:element name="FinalOfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OfferSheetReference5"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OfferSheetReference6 !=''">
						<xsl:element name="FinalOfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OfferSheetReference6"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OfferSheetReference7 !=''">
						<xsl:element name="FinalOfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OfferSheetReference7"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OfferSheetReference8 !=''">
						<xsl:element name="FinalOfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OfferSheetReference8"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OfferSheetReference9 !=''">
						<xsl:element name="FinalOfferSheetReferenceIdentifier">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OfferSheetReference9"/>
						</xsl:element>
					</xsl:if>
					<!-- 조건변경신청일자 -->
					<xsl:element name="LocalLetterOfCreditAmendmentApplicationDate">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/AmendmentDate"/>
					</xsl:element>
					<!-- 개설일자 -->
					<xsl:element name="LocalLetterOfCreditIssueDate">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/IssueDate"/>
					</xsl:element>
					<!-- 기타정보 -->
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/AdditionalInformation1 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/AdditionalInformation1"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/AdditionalInformation2 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/AdditionalInformation2"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/AdditionalInformation3 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/AdditionalInformation3"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/AdditionalInformation4 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/AdditionalInformation4"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/AdditionalInformation5 !=''">
						<xsl:element name="AdditionalInformationDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/AdditionalInformation5"/>
						</xsl:element>
					</xsl:if>
					<!-- 개설은행 -->
					<xsl:element name="IssuingBank">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/IssuingBankCode"/>
							</xsl:element>
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/IssuingBankName"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="Branch">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/IssuingBankBranch"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<!-- 개설의뢰인  -->
					<xsl:element name="ApplicantParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/ApplicantName"/>
							</xsl:element>
							<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/ApplicantCEOName"/>
							</xsl:element>
					<!--	<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/ApplicantCEOName2"/>
							</xsl:element> -->
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/ApplicantId"/>
							</xsl:element><!-- add 20110624 -->
						</xsl:element>
						<xsl:element name="Address">
							<xsl:if test="/LOCAMR2AE/LOCAMR_M/ApplicantAddLine1 !=''">
							<xsl:element name="AddressLine1Text">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/ApplicantAddLine1"/>
							</xsl:element>
							</xsl:if>
							<xsl:if test="/LOCAMR2AE/LOCAMR_M/ApplicantAddLine2A !=''">
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/ApplicantAddLine2A"/>
							</xsl:element>
							</xsl:if>
							<xsl:if test="/LOCAMR2AE/LOCAMR_M/ApplicantAddLine2B !=''">
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/ApplicantAddLine2B"/>
							</xsl:element>
							</xsl:if>
						</xsl:element>
					</xsl:element>
					<!-- 수혜자  -->
					<xsl:element name="BeneficiaryParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/BeneficiaryName"/>
							</xsl:element>
							<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/BeneficiaryCEOName"/>
							</xsl:element>
					<!--	<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/BeneficiaryCEOName2"/>
							</xsl:element> -->
							<xsl:element name="OrganizationIdentifier">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/BeneficiaryId"/>
							</xsl:element><!-- add 20110624 -->
						</xsl:element>
						<xsl:element name="Address">
							<xsl:if test="/LOCAMR2AE/LOCAMR_M/BeneficiaryAddLine1 !=''">
							<xsl:element name="AddressLine1Text">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/BeneficiaryAddLine1"/>
							</xsl:element>
							</xsl:if>
							<xsl:if test="/LOCAMR2AE/LOCAMR_M/BeneficiaryAddLine2A !=''">
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/BeneficiaryAddLine2A"/>
							</xsl:element>
							</xsl:if>
							<xsl:if test="/LOCAMR2AE/LOCAMR_M/BeneficiaryAddLine2B !=''">
							<xsl:element name="AddressLine2Text">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/BeneficiaryAddLine2B"/>
							</xsl:element>
							</xsl:if>
						</xsl:element>
						<xsl:element name="Contact">
							<xsl:element name="ContactEmailAccountText">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/BP_ContactEmailAccount"/>
							</xsl:element>
							<xsl:element name="ContactEmailDomainText">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/BP_ContactEmailDomain"/>
							</xsl:element>
						</xsl:element>
					</xsl:element>
					<!-- 전자서명  -->
					<xsl:element name="SignatureParty">
						<xsl:element name="Organization">
							<xsl:element name="OrganizationName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/SignatureName"/>
							</xsl:element>
							<xsl:element name="OrganizationCEOName">
								<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/SignatureCEOName"/>
							</xsl:element>
						</xsl:element>
						<xsl:element name="PartySignatureValueText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/SignatureValue"/>
						</xsl:element>
					</xsl:element>
					<!-- 조건변경횟수 -->
					<xsl:element name="DocumentRevisionNumber">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/RevisionNumber"/>
					</xsl:element>
					<!-- 변경후 물품인도기일 -->
					<xsl:element name="DeliveryAmendmentPromisedDateTime">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/DeliveryPromisDate"/>
					</xsl:element>
					<!-- 변경 후 유효기일 -->
					<xsl:element name="LocalLetterOfCreditAmendmentEffectiveDate">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/EffectiveDate"/>
					</xsl:element>
					<!-- 기타조건변경사항 -->
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment1 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment1"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment2 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment2"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment3 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment3"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment4 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment4"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment5 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment5"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment6 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment6"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment7 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment7"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment8 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment8"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment9 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment9"/>
						</xsl:element>
					</xsl:if>
					<xsl:if test="/LOCAMR2AE/LOCAMR_M/OtherAmendment10 !=''">
						<xsl:element name="OtherAmendmentDescriptionText">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OtherAmendment10"/>
						</xsl:element>
					</xsl:if>
					<!-- 내국신용장 유형 -->
					<xsl:element name="LocalLetterOfCreditTypeCode">
						<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/LocalLCTypeCode"/>
					</xsl:element>
					<!-- 변경금액 -->
					<xsl:element name="LocalLetterOfCreditAmendmentAmount">
						<xsl:element name="AmountTypeCode">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/LocalLCAmountTypeCode"/>
						</xsl:element>
						<xsl:element name="AmountBasisAmount">
							<xsl:attribute name="currency"><xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OpenAmountCurrency"/></xsl:attribute>
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/OpenAmount"/>
						</xsl:element>
						<xsl:element name="AllowanceOrChargeRateNumeric">
							<xsl:value-of select="/LOCAMR2AE/LOCAMR_M/AOCRateNumeric"/>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
