<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan" xmlns:date="http://exslt.org/dates-and-times" xmlns:str="http://exslt.org/strings" xmlns:js="ext1" extension-element-prefixes="date js" exclude-result-prefixes="str">
	<xalan:component prefix="js" functions="getKey">
		<xalan:script lang="javascript">
			function getKey(pos)
			{
				return Packages.com.ktnet.gmate.common.util.IDGenerator.genId() + "_" + pos;
			}
		</xalan:script>
	</xalan:component>
	<xsl:include href="template.xsl"/>
	<xsl:template match="/">
		<xsl:element name="LOCAMA2AG">
			<xsl:element name="DocumentHeader">
				<xsl:element name="UniqueReferenceNo">
					<xsl:value-of select="/LOCAMA/ApplicationArea/ApplicationAreaMessageIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentNo">
					<xsl:value-of select="/LOCAMA/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<xsl:element name="SenderID">
					<xsl:value-of select="/LOCAMA/ApplicationArea/ApplicationAreaSenderIdentifier"/>
				</xsl:element>
				<xsl:element name="RecipientID">
					<xsl:value-of select="/LOCAMA/ApplicationArea/ApplicationAreaReceiverIdentifier"/>
				</xsl:element>
				<xsl:element name="DocumentType">
					<xsl:value-of select="/LOCAMA/ApplicationArea/ApplicationAreaMessageTypeIndicator"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="LOCAMA_M">
				<xsl:variable name="sMAINKEY" select="js:getKey(position())"/>
				<xsl:element name="MAINKEY">
					<xsl:value-of select="$sMAINKEY"/>
				</xsl:element>
				<!-- 문서번호 -->
				<xsl:element name="DocumentIdentifier">
					<xsl:value-of select="/LOCAMA/DataArea/Header/DocumentIdentifier"/>
				</xsl:element>
				<!-- 문서기능 -->
				<xsl:element name="FunctionCode">
					<xsl:value-of select="/LOCAMA/DataArea/Header/DocumentFunctionCode"/>
				</xsl:element>
				<!-- 응답유형 -->
				<xsl:element name="ResponseTypeCode">
					<xsl:value-of select="/LOCAMA/DataArea/Header/DocumentResponseTypeCode"/>
				</xsl:element>
				<!-- 신청번호 -->
				<xsl:element name="AmendmentIdentifier">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentIdentifier"/>
				</xsl:element>
				<!-- 내국신용장번호 -->
				<xsl:element name="LocalLCIdentifier">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditIdentifier"/>
				</xsl:element>
				<!-- 최종 물품매도확약서번호-->
				<xsl:element name="OfferSheetReference1">
					<xsl:value-of select="/LOCAMA/DataArea/Header/FinalOfferSheetReferenceIdentifier[1]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference2">
					<xsl:value-of select="/LOCAMA/DataArea/Header/FinalOfferSheetReferenceIdentifier[2]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference3">
					<xsl:value-of select="/LOCAMA/DataArea/Header/FinalOfferSheetReferenceIdentifier[3]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference4">
					<xsl:value-of select="/LOCAMA/DataArea/Header/FinalOfferSheetReferenceIdentifier[4]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference5">
					<xsl:value-of select="/LOCAMA/DataArea/Header/FinalOfferSheetReferenceIdentifier[5]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference6">
					<xsl:value-of select="/LOCAMA/DataArea/Header/FinalOfferSheetReferenceIdentifier[6]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference7">
					<xsl:value-of select="/LOCAMA/DataArea/Header/FinalOfferSheetReferenceIdentifier[7]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference8">
					<xsl:value-of select="/LOCAMA/DataArea/Header/FinalOfferSheetReferenceIdentifier[8]"/>
				</xsl:element>
				<xsl:element name="OfferSheetReference9">
					<xsl:value-of select="/LOCAMA/DataArea/Header/FinalOfferSheetReferenceIdentifier[9]"/>
				</xsl:element>
				<!-- 조건변경통지일자 -->
				<xsl:element name="AmendmentNotificationDate">
					<xsl:value-of select="/LOCAMA/DataArea/Header/AmendmentNotificationDate"/>
				</xsl:element>
				<!-- 조건변경일자 -->
				<xsl:element name="AmendmentDate">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentDate"/>
				</xsl:element>
				<!-- 개설일자 -->
				<xsl:element name="IssueDate">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditIssueDate"/>
				</xsl:element>
				<!-- 기타정보 -->
				<xsl:element name="AdditionalInformation1">
					<xsl:value-of select="/LOCAMA/DataArea/Header/AdditionalInformationDescriptionText[1]"/>
				</xsl:element>
				<xsl:element name="AdditionalInformation2">
					<xsl:value-of select="/LOCAMA/DataArea/Header/AdditionalInformationDescriptionText[2]"/>
				</xsl:element>
				<xsl:element name="AdditionalInformation3">
					<xsl:value-of select="/LOCAMA/DataArea/Header/AdditionalInformationDescriptionText[3]"/>
				</xsl:element>
				<xsl:element name="AdditionalInformation4">
					<xsl:value-of select="/LOCAMA/DataArea/Header/AdditionalInformationDescriptionText[4]"/>
				</xsl:element>
				<xsl:element name="AdditionalInformation5">
					<xsl:value-of select="/LOCAMA/DataArea/Header/AdditionalInformationDescriptionText[5]"/>
				</xsl:element>
				<!-- 개설은행 -->
				<xsl:element name="IssuingBankCode">
					<xsl:value-of select="/LOCAMA/DataArea/Header/IssuingBank/Organization/OrganizationIdentifier"/>
				</xsl:element>
				<xsl:element name="IssuingBankName">
					<xsl:value-of select="/LOCAMA/DataArea/Header/IssuingBank/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="IssuingBankBranch">
					<xsl:value-of select="/LOCAMA/DataArea/Header/IssuingBank/Branch/OrganizationName"/>
				</xsl:element>
				<!-- 개설의뢰인 -->
				<xsl:element name="ApplicantName">
					<xsl:value-of select="/LOCAMA/DataArea/Header/ApplicantParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="ApplicantCEOName">
					<xsl:value-of select="/LOCAMA/DataArea/Header/ApplicantParty/Organization/OrganizationCEOName"/>
				</xsl:element>
				<!-- <xsl:element name="ApplicantCEOName2">
					<xsl:value-of select="/LOCAMA/DataArea/Header/ApplicantParty/Organization/OrganizationCEOName[2]"/>
				</xsl:element>-->
				<xsl:element name="ApplicantId">
					<xsl:value-of select="/LOCAMA/DataArea/Header/ApplicantParty/Organization/OrganizationIdentifier"/>
				</xsl:element><!-- add 20110624 -->
				<xsl:element name="ApplicantAddLine1">
					<xsl:value-of select="/LOCAMA/DataArea/Header/ApplicantParty/Address/AddressLine1Text"/>
				</xsl:element>
				<xsl:element name="ApplicantAddLine2A">
					<xsl:value-of select="/LOCAMA/DataArea/Header/ApplicantParty/Address/AddressLine2Text[1]"/>
				</xsl:element>
				<xsl:element name="ApplicantAddLine2B">
					<xsl:value-of select="/LOCAMA/DataArea/Header/ApplicantParty/Address/AddressLine2Text[2]"/>
				</xsl:element>
				<!-- 수혜자 -->
				<xsl:element name="BeneficiaryName">
					<xsl:value-of select="/LOCAMA/DataArea/Header/BeneficiaryParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="BeneficiaryCEOName">
					<xsl:value-of select="/LOCAMA/DataArea/Header/BeneficiaryParty/Organization/OrganizationCEOName"/>
				</xsl:element>
				<!--  <xsl:element name="BeneficiaryCEOName2">
					<xsl:value-of select="/LOCAMA/DataArea/Header/BeneficiaryParty/Organization/OrganizationCEOName[2]"/>
				</xsl:element> -->
				<xsl:element name="BeneficiaryId">
					<xsl:value-of select="/LOCAMA/DataArea/Header/BeneficiaryParty/Organization/OrganizationIdentifier"/>
				</xsl:element><!-- add 20110624 -->
				<xsl:element name="BeneficiaryAddLine1">
					<xsl:value-of select="/LOCAMA/DataArea/Header/BeneficiaryParty/Address/AddressLine1Text"/>
				</xsl:element>
				<xsl:element name="BeneficiaryAddLine2A">
					<xsl:value-of select="/LOCAMA/DataArea/Header/BeneficiaryParty/Address/AddressLine2Text[1]"/>
				</xsl:element>
				<xsl:element name="BeneficiaryAddLine2B">
					<xsl:value-of select="/LOCAMA/DataArea/Header/BeneficiaryParty/Address/AddressLine2Text[2]"/>
				</xsl:element>
				<xsl:element name="BP_ContactEmailAccount">
					<xsl:value-of select="/LOCAMA/DataArea/Header/BeneficiaryParty/Contact/ContactEmailAccountText"/>
				</xsl:element>
				<xsl:element name="BP_ContactEmailDomain">
					<xsl:value-of select="/LOCAMA/DataArea/Header/BeneficiaryParty/Contact/ContactEmailDomainText"/>
				</xsl:element>
				<!-- 전자서명 -->
				<xsl:element name="SignatureName">
					<xsl:value-of select="/LOCAMA/DataArea/Header/SignatureParty/Organization/OrganizationName"/>
				</xsl:element>
				<xsl:element name="SignatureCEOName">
					<xsl:value-of select="/LOCAMA/DataArea/Header/SignatureParty/Organization/OrganizationCEOName"/>
				</xsl:element>
				<xsl:element name="SignatureValue">
					<xsl:value-of select="/LOCAMA/DataArea/Header/SignatureParty/PartySignatureValueText"/>
				</xsl:element>
				<!-- 조건변경횟수 -->
				<xsl:element name="RevisionNumber">
					<xsl:value-of select="/LOCAMA/DataArea/Header/DocumentRevisionNumber"/>
				</xsl:element>
				<!-- 변경후 물품인도기일 -->
				<xsl:element name="DeliveryPromisDate">
					<xsl:value-of select="/LOCAMA/DataArea/Header/DeliveryAmendmentPromisedDateTime"/>
				</xsl:element>
				<!-- 변경후 유효기일 -->
				<xsl:element name="EffectiveDate">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentEffectiveDate"/>
				</xsl:element>
				<!-- 기타 조건변경사항 -->
				<xsl:element name="OtherAmendment1">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[1]"/>
				</xsl:element>
				<xsl:element name="OtherAmendment2">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[2]"/>
				</xsl:element>
				<xsl:element name="OtherAmendment3">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[3]"/>
				</xsl:element>
				<xsl:element name="OtherAmendment4">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[4]"/>
				</xsl:element>
				<xsl:element name="OtherAmendment5">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[5]"/>
				</xsl:element>
				<xsl:element name="OtherAmendment6">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[6]"/>
				</xsl:element>
				<xsl:element name="OtherAmendment7">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[7]"/>
				</xsl:element>
				<xsl:element name="OtherAmendment8">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[8]"/>
				</xsl:element>
				<xsl:element name="OtherAmendment9">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[9]"/>
				</xsl:element>
				<xsl:element name="OtherAmendment10">
					<xsl:value-of select="/LOCAMA/DataArea/Header/OtherAmendmentDescriptionText[10]"/>
				</xsl:element>
				<!-- 내국신용장 종류 -->
				<xsl:element name="LocalLCTypeCode">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditTypeCode"/>
				</xsl:element>
				<!-- 내국신용장 개설 금액 유형 -->
				<xsl:element name="LocalLCAmountTypeCode">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/AmountTypeCode"/>
				</xsl:element>
				<!-- 변경후 외화금액 -->
				<xsl:element name="ConvertedAmount">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/AmountConvertedAmount"/>
				</xsl:element>
				<xsl:element name="ConvertedAmountCurrency">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/AmountConvertedAmount/@currency"/>
				</xsl:element>
				<!-- 변경후 원화금액 -->
				<xsl:if test="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/AmountBasisAmount != ''">
				     <xsl:element name="OpenAmount">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/AmountBasisAmount"/>
				     </xsl:element>
				</xsl:if>
				<xsl:if test="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/AmountBasisAmount = ''">
				     <TaxInvoiceCopyNumber>0</TaxInvoiceCopyNumber>
				</xsl:if>
				<xsl:element name="OpenAmountCurrency">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/AmountBasisAmount/@currency"/>
				</xsl:element>
				<xsl:element name="AOCRateNumeric">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/AllowanceOrChargeRateNumeric"/>
				</xsl:element>
				<!-- 전신환매입율 -->
				<xsl:if test="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/ExchangeRate!= ''">
				     <xsl:element name="ExchangeRate">
					<xsl:value-of select="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/ExchangeRate"/>
				     </xsl:element>
				</xsl:if>
				<xsl:if test="/LOCAMA/DataArea/Header/LocalLetterOfCreditAmendmentAmount/ExchangeRate= ''">
				     <TaxInvoiceCopyNumber>0</TaxInvoiceCopyNumber>
				</xsl:if>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
