*WinMate 변경사항
 
- 문서 추가 -
  1911 : EDIADV - 은행EDI약정 통보서

- 정정문서 - 
  1911 : APPPCR NAD 세그먼트의 C058 항목 사용
                PIA 조건변경 C --> M
                IMD 조건변경 C --> M
  2911 : GENRES NAD 세그먼트의 C058 항목 사용
  1911 : LOCADV BUS 세그먼트그룹에 PCD 세그먼트 추가
                NAD 세그먼트의 C058 항목 사용
  1911 : LOCAMA BUS 세그먼트그룹에 PCD 세그먼트 추가
                NAD 세그먼트의 C058 항목 사용
  1911 : LOCAMR BUS 세그먼트그룹에 PCD 세그먼트 추가
                NAD 세그먼트의 C058 항목 사용
  1911 : LOCAPP BUS 세그먼트그룹에 PCD 세그먼트 추가
                NAD 세그먼트의 C058 항목 사용
  1911 : PCRLIC NAD 세그먼트의 C058 항목 사용
                PIA 조건변경 C --> M
                IMD 조건변경 C --> M

- 코드 추가 -
  |0065|EDIADV|은행EDI약정 통보서|
  |1001|BE|은행EDI약정통보서|
  |1001|2DO|내국신용장 취소통보서|
  |4025|2A0|(위탁가공)원자재|
  
- 코드 변경 -
  |4487|2AA|원화표시 외화부기 내국신용장|
  |4487|2AC|원화표시 내국신용장|